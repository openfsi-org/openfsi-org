#-------------------------------------------------
#
# Project created by QtCreator 2012-04-05T09:10:14
#
#-------------------------------------------------

QT       += core
QT       -= gui

TARGET = rxsolver
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += f90
TEMPLATE = app
rxsolver.depends +=  ../thirdparty/hbio
include (../openfsicompilerconfig.pri)

F90_CFLAGS += -m64 -fpp -heap-arrays 10 -par-report1

DEFINES += _CONSOLE
QMAKE_LINK = ifort
    QMAKE_LFLAGS +=  -cxxlib
    QMAKE_LFLAGS +=  -recursive
    QMAKE_LFLAGS += -g  -nofor_main
     QMAKE_LFLAGS += -m64
    QMAKE_CXXFLAGS_RELEASE += -openmp
    QMAKE_LFLAGS_RELEASE +=  -parallel -openmp
  #  QMAKE_LFLAGS +=$$PETERSINTELLIBS
#LIBS += -L../thirdparty/sparskit -lskit
LIBS +=  $${LINKTAIL} -lmkl_lapack95_lp64  $$PETERSINTELLIBS

SOURCES += main.cpp \
    ../src/preprocess/RXHarwellBoeingIO.cpp \
    ../src/preprocess/RXSparseMatrix1.cpp \
    ../src/preprocess/RXHarwellBoeingIO1.cpp

HEADERS += ../src/preprocess/RXHarwellBoeingIO.h \
    ../thirdparty/opennurbs/opennurbs.h \
    ../src/stringtools/stringtools.h \
    ../src/stringtools/UtfConverter.h \
      ../thirdparty/hbio/hb_io.h \
    ../thirdparty/MTParserLib/mtlinux.h \
    ../src/preprocess/RXSparseMatrix1.h \
    ../src/preprocess/RXHarwellBoeingIO1.h

INCLUDEPATH +=     ../thirdparty/bandmin
INCLUDEPATH +=     ../src/preprocess
INCLUDEPATH +=     ../thirdparty/opennurbs
INCLUDEPATH +=     ../src/stringtools
INCLUDEPATH +=     ../thirdparty/MTParserLib
INCLUDEPATH +=     ../src/x/inc
INCLUDEPATH +=     /usr/include/boost


F90_SOURCES += \
    ../thirdparty/sparskit/FORMATS/formats.f \
    ../thirdparty/sparskit/FORMATS/unary.f  \
    ../thirdparty/sparskit/INOUT/inout.f \
    ../thirdparty/sparskit/BLASSM/blassm.f \
    ../ofsifortran/rxdirectsolver.f90
OTHER_FILES += $$F90_SOURCES


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../thirdparty/hbio/release/ -lhbio
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../thirdparty/hbio/debug/ -lhbio
else:symbian: LIBS += -lhbio
else:unix: LIBS += -L../thirdparty/hbio -lhbio

INCLUDEPATH += ../thirdparty/hbio
DEPENDPATH += ../thirdparty/hbio

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/thirdparty/hbio/release/hbio.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/thirdparty/hbio/debug/hbio.lib
else:unix:!symbian: PRE_TARGETDEPS += $$OUT_PWD/../thirdparty/hbio/libhbio.a
