// this is rxsolver main
// it provides a solution to our equation system (Jt.m.J).x = rhs.
// Currently it uses the boost implementation of Cuthill-McKee for the bandwidth reordering
// and then the most basic MKL lapack implementation for the solution.
// there is a crude attempt at iterative refinement.
// it makes use of some sparskit utilities for things like changing storage scheme.

// the resulting bandwidth  is 3x bigger than Mathematica's RCMD algorithm.  (633 vs approx +250 to - 195 )

// For testing , see 'solververif.nb'

// we have been testing using a beam cantilever. Up to about 5000 elements  it seems accurate:
// at 10000 elements it introduces enough noise to degrade the non-linear solution scheme.
// the reciprocal condition number is about  1e-21
// with 2000 elements in the cantilever the rcond is about 4e-18 and the nonlinear solution seems stable, but there
// is noise which limits the solution to a few newtons.
// with a condition number of about 1e15, the solution is excellent. (and pardiso fails)

#include "StdAfx.h"

#include <boost/config.hpp>
#include <vector>
#include <iostream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/cuthill_mckee_ordering.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/bandwidth.hpp>

#include <QtCore>
#define MKL_INT int

#include <iostream>

#include "f90_to_c.h"
#include "RXHarwellBoeingIO.h"
#include "RXSparseMatrix1.h"

double g_diagonalfac;
int doMyTests (QStringList a  ) ;
int boostbandwidth(std::map<std::pair<int,int>,double> &mmm, const int nr, int *result, int &bw );

int main(int argc, char *argv[])
{
    QStringList a ;
    for(int i=0;i<argc;i++) a<<argv[i];

    int na = a.size();
    if(na<2) {
        cout<<endl<<endl<<"rxsolver  solves the equation system [ Jt S J x = rhs] , or does various tests "<<endl<<endl;
        //                         1       2       3       4       5
        cout<<"syntax is 'rxsolver ,path, Smatrix,      rhs,    Jay, result [flags]"<<endl;
        cout<<"unless the first word is 'testing' in which case "<<endl;

        cout<<"syntax is "<<argv[0]<<  "\ntesting, flags, filein, rhsin, fileout "<<endl;
        exit(1);
    }

    if(na<5) {
        qDebug()<<a;
        return 0;
    }

    if(a[1].contains("testing", Qt::CaseInsensitive)) {
        return doMyTests(a);
    }
    //             1       2       3       4       5
    //rxsolver  Smatrix,  path, rhs,    Jay, result, [flags]
    double *rhs=0;
    int rc=0;
    QDir path(a.value(1));
    RXHarwellBoeingIO mrhs;
    RXSparseMatrix1  m(RXM_RECTANGLE)   ;
    RXSparseMatrix1 jay(RXM_RECTANGLE);  // argument is 'm_flags'
    QFile fm(path.absoluteFilePath(a.value(2)) );
    m.HB_Read( qPrintable(fm.fileName()  ));
    QFile fj (path.absoluteFilePath(a.value(4)) );
    jay.HB_Read( qPrintable(fj.fileName()  ));
    QFile frhs(path.absoluteFilePath(a.value(3)) );
    QFileInfo fi(frhs);
    QString fileout (path.absoluteFilePath(a.value(5)) );
    qDebug()<<a;
    if(fi.suffix().contains("r", Qt::CaseInsensitive )){

        mrhs.Read(qPrintable(frhs.fileName()) );
        rhs=mrhs.values;
    }
    else if(fi.suffix().contains("txt", Qt::CaseInsensitive )){
        rhs  =new double[jay.Nc() ];
        if (!frhs.open(QIODevice::ReadOnly | QIODevice::Text))
            return 0;
        QTextStream in(&frhs);
        for(int i=0; i< jay.Nc() ;i++)
            in>> rhs[i] ;
        frhs.close();
    }
    //  start with m and Jay
    m.Transpose();
    class RXSparseMatrix1 aa(RXM_RECTANGLE),c(m.GetFlags());
    rc= m.Multiply (jay,aa);
    if(rc !=0) {
        cout<< " first multiply failed: code="<<rc<<endl; return -11;
    }

    jay.Transpose();
    jay.SetStorageType(CSC);

    //    int count=0;
    //    double myfac=1.0;
    do{  // come back here if it isnt SPD We have to redo the mult because Solve alters the matrix

        jay.SetStorageType(CSC); aa.SetStorageType(CSC);

        rc=jay.Multiply (aa,c) ;	if(rc !=0) {cout<< " second multiply failed"; 	return -5;	}
        int nr = c.Nr(); int nc=c.Nc();
        if(nr<1) return 1;

        c.SetStorageType(MAPSTYLE );
        int bw;

        int *perm = new int[nr];
        double*result = new double[nr];
        boostbandwidth(c.m_map,nr,perm,bw); // Reverse Cuthill-McKee

        c.SetStorageType(CSC);
        int myerr = cf_solveDirect(c.m_hbio.colptr, c.m_hbio.rowind,
                                   c.m_hbio.values  , c.m_hbio.nnzero,
                                   nr, bw, perm,rhs, result  );  // one for each value Indexed from 1. IA in sparsekit notation.

        if(!myerr) {
            QFile  fo(fileout);
            if (!fo.open(QIODevice::WriteOnly | QIODevice::Text))
                return 0;

            QTextStream out(&fo);
            double maxd = -1;
            for(int i=0;i<nr;i++) {
                // if(i<200) cout<< i<<"     "<<result[i]<<endl;
                out<< result[i]<<endl;
                maxd=max(maxd,fabs (result[i]) );
            }
            //  cout<< "abs max val in result = "<<maxd<<endl;
            fo.close();
        }

        delete[] perm; delete[] result;


    }
    while(0);




}// main





int doMyTests (QStringList a  )
{
    double *rhs=0;
    RXSparseMatrix1  m(RXM_RECTANGLE);//,  m2(RXM_RECTANGLE); // argument is 'm_flags'
    QString fileout = a.value(5);
    QString flags = a.value(2);
    {
        QFile fin(a.value(3) );
        m.HB_Read( qPrintable(fin.fileName()  ));
        //  m2.HB_Read( qPrintable(fin.fileName()  ));
    }
    QFile frhs(a.value(4) );
    QFileInfo fi(frhs);

    if(fi.suffix().contains("r", Qt::CaseInsensitive )){
        RXHarwellBoeingIO mrhs;
        mrhs.Read(qPrintable(frhs.fileName()) );
        rhs=mrhs.values;
    }
    else if(fi.suffix().contains("txt", Qt::CaseInsensitive )){
        rhs  =new double[m.Nr() ];
        if (!frhs.open(QIODevice::ReadOnly | QIODevice::Text))
            return 0;
        QTextStream in(&frhs);
        for(int i=0; i< m.Nr();i++)
            in>> rhs[i] ;
        frhs.close();
    }
    if(flags.contains("pardiso", Qt::CaseInsensitive)){
        double *result =new double[m.Nr()];
        //    m.SolveIterative()   .SolveOnce(rhs ,result);
        QFile  fo(fileout);
        if (!fo.open(QIODevice::WriteOnly | QIODevice::Text))
            return 0;

        QTextStream out(&fo);

        out.setRealNumberNotation(QTextStream::ScientificNotation);
        out.setFieldWidth(18);
        for(int i=0; i< m.Nr();i++){
            qDebug()<<rhs[i]<<result[i];
            out<< qSetRealNumberPrecision (12 )<< result[i]<<"\n";
        }
        delete[] result;
        return 1;
    }
    //  Solve using a lapack direct solver, using banded storage


    if(flags.contains("boost", Qt::CaseInsensitive)){
        if(m.Nr()<1) return 1;
        m.PrettyPrint(cout);
        m.SetStorageType(MAPSTYLE );
        int bw;
        int *perm = new int[m.Nr()];
        double*result = new double[m.Nr()];
        boostbandwidth(m.m_map,m.Nr(),perm,bw); // Reverse Cuthill-McKee
        rhs  =new double[m.Nr() ];;// WRONG - for testing
        for(int i=0;i<m.Nr();i++)
            rhs[i]=0.1;
        m.SetStorageType(CSC);
        int myerr = cf_solveDirect(m.m_hbio.colptr, m.m_hbio.rowind,
                                   m.m_hbio.values  , m.m_hbio.nnzero,
                                   m.Nr(), bw, perm,rhs, result  );  // one for each value Indexed from 1. IA in sparsekit notation.
        //    cout<<"m after solveDirect"<<endl;
        //     m.PrettyPrint(cout);
        for(int i=0;i<m.Nr();i++)
            cout<< i<<"     "<<result[i]<<endl;
        delete[] perm; delete[] result;

    }
    //    if(flags.contains("band", Qt::CaseInsensitive)){
    //     //   map<pair<int,int>, char> mat;
    //     //   int r,c;

    //        manziniDelCorso mdc( m.m_map,m.Nr() );
    //        // -------------- calcola la banda minima -----------------
    //        vector<int>  result;
    //    // this is    minimize_band(result);
    //        risultato ris;
    //            mdc.bida(&ris);fflush(stdout);
    //            printf("Minimum band: %d\n",ris.band);  fflush(stdout);
    //            printf("Node gen: %10.0lf  Time: %10.4lf(u)",ris.nodi_gen,ris.elapsed_time); fflush(stdout);
    //            printf("  %10.4lf(u+s)\n",ris.elapsed_time2);fflush(stdout);
    //            result = ris.m_p; ris.m_p.clear();

    //    }
    return 1;

}
int filename_copy_local(char *out,int p_length,const char *in) {
    strncpy(out,in,p_length);
    return 1;
}
//=======================================================================
// Copyright 1997, 1998, 1999, 2000 University of Notre Dame.
// Authors: Andrew Lumsdaine, Lie-Quan Lee, Jeremy G. Siek
//          Doug Gregor, D. Kevin McGrath
//
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//=======================================================================


/*
  Sample Output
  original bandwidth: 8
  Reverse Cuthill-McKee ordering starting at: 6
    8 3 0 9 2 5 1 4 7 6
    bandwidth: 4
  Reverse Cuthill-McKee ordering starting at: 0
    9 1 4 6 7 2 8 5 3 0
    bandwidth: 4
  Reverse Cuthill-McKee ordering:
    0 8 5 7 3 6 4 2 1 9
    bandwidth: 4
 */
int boostbandwidth(std::map<std::pair<int,int>,double> &mmm, const int nr, int *result , int &bw )
{
    using namespace boost;
    using namespace std;
    typedef adjacency_list<vecS, vecS, undirectedS,
            property<vertex_color_t, default_color_type,
            property<vertex_degree_t,int> > > Graph;
    typedef graph_traits<Graph>::vertex_descriptor Vertex;
    typedef graph_traits<Graph>::vertices_size_type size_type;

    typedef std::pair<std::size_t, std::size_t> Pair;

    int i,j, *p;
    Graph G(nr-1);
    map<pair<int,int>,double>::iterator it, e = mmm.end();
    for(it=mmm.begin();it!=e;++it) {
        i = it->first.first -1;
        j = it->first.second -1 ;
        //  if(i==j) continue;
        //  add_edge(min(i, j),max(i,j), G);
        add_edge(i,j, G); //add_edge(j,i, G);
    }

    graph_traits<Graph>::vertex_iterator ui, ui_end;

    property_map<Graph,vertex_degree_t>::type deg = get(vertex_degree, G);
    for (boost::tie(ui, ui_end) = vertices(G); ui != ui_end; ++ui)
        deg[*ui] = degree(*ui, G);

    property_map<Graph, vertex_index_t>::type
            index_map = get(vertex_index, G);

    //   std::cout << "original bandwidth: " << bandwidth(G);

    std::vector<Vertex> inv_perm(num_vertices(G));
    std::vector<size_type> perm(num_vertices(G));
    if (0) {
        {
            Vertex s = vertex(0, G);
            //reverse cuthill_mckee_ordering
            cuthill_mckee_ordering(G, s, inv_perm.rbegin(), get(vertex_color, G),
                                   get(vertex_degree, G));
            //  cout << "Reverse Cuthill-McKee ordering starting at: " << s << endl;
            //    cout << "  ";
            //    for (std::vector<Vertex>::const_iterator i = inv_perm.begin();
            //         i != inv_perm.end(); ++i)
            //      cout << index_map[*i] << " ";
            //    cout << endl;

            for (size_type c = 0; c != inv_perm.size(); ++c)
                perm[index_map[inv_perm[c]]] = c;
            std::cout << "  bandwidth: "
                      << bandwidth(G, make_iterator_property_map(&perm[0], index_map, perm[0]))
                      << std::endl;
        }
        {
            Vertex s = vertex(0, G);
            //reverse cuthill_mckee_ordering
            cuthill_mckee_ordering(G, s, inv_perm.rbegin(), get(vertex_color, G),
                                   get(vertex_degree, G));
            cout << "Reverse Cuthill-McKee ordering starting at: " << s << endl;
            //    cout << "  ";
            //    for (std::vector<Vertex>::const_iterator i=inv_perm.begin();
            //       i != inv_perm.end(); ++i)
            //      cout << index_map[*i] << " ";
            //    cout << endl;

            for (size_type c = 0; c != inv_perm.size(); ++c)
                perm[index_map[inv_perm[c]]] = c;
//            std::cout << "  bandwidth: "
//                      << bandwidth(G, make_iterator_property_map(&perm[0], index_map, perm[0]))
//                      << std::endl;
        }
    }
    {
        //reverse cuthill_mckee_ordering
        cuthill_mckee_ordering(G, inv_perm.rbegin(), get(vertex_color, G),
                               make_degree_map(G));

#define PLEASEPRINT (0)
        if(PLEASEPRINT) cout << " Reverse Cuthill-McKee ordering: " ;
        if(PLEASEPRINT) cout << " { ";
        p=result;  j=1; // J STARTS AT 1
        for (std::vector<Vertex>::const_iterator it=inv_perm.begin();
             it != inv_perm.end(); ++it)
        {
            if(PLEASEPRINT) cout << index_map[*it] +1 << ", ";
            result[index_map[*it] ]=j; j++;
        }
        if(PLEASEPRINT) cout << "}" << endl;
        if(0 &&( j<200))
        {
            cout << "inverted = {";
            for(i=0;i<j-1;i++)
                cout<< result[i]<<", ";
            cout << "}" << endl;
        }
        for (size_type c = 0; c != inv_perm.size(); ++c)
            perm[index_map[inv_perm[c]]] = c;
        bw =  bandwidth(G, make_iterator_property_map(&perm[0], index_map, perm[0]));
        if(PLEASEPRINT) std::cout << "  bandwidth: " << bw << std::endl;
    }
    return 0;
}
