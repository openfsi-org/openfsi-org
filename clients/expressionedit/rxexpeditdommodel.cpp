/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtXml>

#include "rxexpeditdomitem.h"
#include "rxexpeditdommodel.h"
#include "rxexpeditdomdocument.h"
//! [0]
RXExpEditDomModel::RXExpEditDomModel(RXExpEditDomDocument document, QObject *parent)
    : QAbstractItemModel(parent), domDocument(document)
{
    rootItem = new RXExpEditDomItem(domDocument, 0);
}
//! [0]

//! [1]
RXExpEditDomModel::~RXExpEditDomModel()
{

    delete rootItem;
}
//! [1]

//! [2]
int RXExpEditDomModel::columnCount(const QModelIndex &/*parent*/) const
{
    return 5; // GUESS
}
//! [2]

//! [3]
QVariant RXExpEditDomModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    RXExpEditDomItem *item = static_cast<RXExpEditDomItem*>(index.internalPointer());

    QDomNode node = item->node();
//! [3] //! [4]
    QStringList atts;
    QDomNamedNodeMap attributeMap = node.attributes();

    switch (index.column()) {
        case 0:
            return node.nodeName();
        case 1:
            for (int i = 0; i < attributeMap.count(); ++i) {
                QDomNode attribute = attributeMap.item(i);
                atts << attribute.nodeName() + "=\""
                              +attribute.nodeValue() + "\"";
            }
            return atts.join(" ");
        case 2:
            return node.nodeValue().split("\n").join(" ");
        default:
            return QVariant();
    }
}
//! [4]

//! [5]
Qt::ItemFlags RXExpEditDomModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;
    if(index.column()==2)
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable| Qt::ItemIsEditable;//peter added Editable
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable ;
}
//! [5]
//! [99]
bool RXExpEditDomModel::setData(const QModelIndex &index,
                         const QVariant &value, int role)//peter added
{

    if (index.isValid() && role == Qt::EditRole) {
        RXExpEditDomItem *item = static_cast<RXExpEditDomItem*>(index.internalPointer());

        QDomNode node = item->node();
        const QString s = value.toString();

        QDomElement *el =  static_cast<QDomElement*>(index.internalPointer());
        if(!el) return false;
        if(el->isText()) {
             QDomText xx= el->toText();
          //  value = xx.data();
          //  el->setNodeValue ( s);
             xx.setData(s);
             emit dataChanged(index, index);
             return true;
        }
        else{
                node.setNodeValue(s);
                return true;

            QDomNode child = el->firstChild();
            if(!child.isNull()) qDebug()<<"child isnt null "<< child.nodeName()<<child.nodeValue();

            if(child.isText()) {
                qDebug()<<" it has a child text";
                QDomText xx= child.toText();
                xx.setData(s);
                emit dataChanged(index, index);
                return true;
            }
            else
                qDebug()<<" it isnt Text :" ;

        }

    }
    return   false;

}
//! [99]


//! [6]
QVariant RXExpEditDomModel::headerData(int section, Qt::Orientation orientation,
                              int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        switch (section) {
            case 0:
                return tr("Name");
            case 1:
                return tr("Attributes");
            case 2:
                return tr("Value");
            default:
                return QVariant();
        }
    }

    return QVariant();
}
//! [6]

//! [7]
QModelIndex RXExpEditDomModel::index(int row, int column, const QModelIndex &parent)
            const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    RXExpEditDomItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<RXExpEditDomItem*>(parent.internalPointer());
//! [7]

//! [8]
    RXExpEditDomItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}
//! [8]

//! [9]
QModelIndex RXExpEditDomModel::parent(const QModelIndex &child) const
{
    if (!child.isValid())
        return QModelIndex();

    RXExpEditDomItem *childItem = static_cast<RXExpEditDomItem*>(child.internalPointer());
    RXExpEditDomItem *parentItem = childItem->parent();

    if (!parentItem || parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}
//! [9]

//! [10]
int RXExpEditDomModel::rowCount(const QModelIndex &parent) const
{
    if (parent.column() > 0)
        return 0;

    RXExpEditDomItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<RXExpEditDomItem*>(parent.internalPointer());

    return parentItem->node().childNodes().count();
}
//! [10]
