/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QDomDocument>
#include <QFile>
 //#include <QtGui>

#include <QFileDialog>
#include <QPushButton>
#include <QGridLayout>
#include <QTreeView>
#include <iostream>
#include "rxclientbase.h"

#include "rxexpeditdommodel.h"
#include "rxexpeditdomdocument.h"
#include "rxexpeditsubwindow.h"

RXExpEditSubWindow::RXExpEditSubWindow() : QWidget(), m_model(0)
{
    QWidget *subwindow = this;
    subwindow->setObjectName(QString("Expression Editor"));
    subwindow->setWindowTitle(tr("Expression Editor"));
    subwindow->setAttribute(Qt::WA_DeleteOnClose);
    QGridLayout *gridLayout_2 = new QGridLayout(subwindow);
    gridLayout_2->setSpacing(6);
    gridLayout_2->setContentsMargins(11, 11, 11, 11);
    gridLayout_2->setObjectName(QString("gridLayout_2"));


    m_view = new QTreeView(this);
    m_view->setObjectName(QString::fromUtf8("treeView"));

    gridLayout_2->addWidget(m_view, 0, 0, 1, 3);

    QPushButton *pushButton_0 = new QPushButton(subwindow);
    pushButton_0->setObjectName(QString::fromUtf8("pushButton"));
    gridLayout_2->addWidget(pushButton_0, 1, 2, 1, 1);
    QPushButton *pushButton_2 = new QPushButton(subwindow);
    pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
    gridLayout_2->addWidget(pushButton_2, 1, 0, 1, 1);
    QPushButton *pushButton_3 = new QPushButton(subwindow);
    pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
    gridLayout_2->addWidget(pushButton_3, 1, 1, 1, 1);

//    pushButton_0->setText(QApplication::translate("MainWindow", "Refresh", 0, QApplication::UnicodeUTF8));
//    pushButton_2->setText(QApplication::translate("MainWindow", "Apply", 0, QApplication::UnicodeUTF8));
//    pushButton_3->setText(QApplication::translate("MainWindow", "Cancel", 0, QApplication::UnicodeUTF8));

    pushButton_0->setText( tr("Refresh"));
    pushButton_2->setText(tr ("Apply"));
    pushButton_3->setText(tr("Cancel"));

    connect(pushButton_0,SIGNAL(clicked()),this,SLOT(on_refresh()));
    connect(pushButton_2,SIGNAL(clicked()),this,SLOT(on_apply()));
    connect(pushButton_3,SIGNAL(clicked()),this,SLOT(on_cancel()));

    connect(this,SIGNAL( PleaseNewExpEdit()), g_cb,SLOT(NewExpressionEditor()));

    m_model = new RXExpEditDomModel(RXExpEditDomDocument(), this);;
    m_view->setModel(m_model);
    m_view->setItemDelegateForColumn(2,&m_tbDelegate);
    m_view->setColumnHidden(1,true);
    m_view->setColumnWidth(0,180);
    m_view->setColumnWidth(2,180);

}

void RXExpEditSubWindow::openFile()
{

    QString filePath = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    xmlPath, tr("XML files (*.xml);;HTML files (*.html);;"
                                                                "SVG files (*.svg);;User Interface files (*.ui)"));

    if (!filePath.isEmpty()) {
        QFile file(filePath);
        if (file.open(QIODevice::ReadOnly)) {
            RXExpEditDomDocument document;
            if (document.setContent(&file)) {
                RXExpEditDomModel *newModel = new RXExpEditDomModel(document, this);
                m_view->setModel(newModel);
                delete m_model;
                m_model = newModel;
                xmlPath = filePath;
            }
            file.close();
        }
    }
}
void RXExpEditSubWindow::SaveFileAs()
{
    QString filePath = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                    xmlPath, tr("XML files (*.xml);;HTML files (*.html);;"
                                                                "SVG files (*.svg);;User Interface files (*.ui)"));

    if (!filePath.isEmpty()) {
         //m_model->Document().Write(filePath);
    }
}
void RXExpEditSubWindow::openText(QString &text)
{
    RXExpEditDomDocument document;

    if (document.SetContentByString(text)) {
        RXExpEditDomModel *newModel = new RXExpEditDomModel(document, this);
        m_view->setModel(newModel);
        delete m_model;
        m_model = newModel;
        xmlPath =  "text";

    }
    return;
}
void RXExpEditSubWindow::on_refresh()
{
    qDebug()<<"REFRESH";
    QString text;

    RXExpEditDomModel *m = static_cast<RXExpEditDomModel *>(this->m_model);
    if(m){
        RXExpEditDomDocument d = m->Document();
        d.clear();
        delete m;
        this->m_model=0;
    }

    emit this->PleaseNewExpEdit();
    emit this->parent()->deleteLater();
}
QList<QDomNode>  RXExpEditSubWindow::GetEditedElements()
{
    QList<QDomNode>  rc;
    RXExpEditDomModel *m = static_cast<RXExpEditDomModel *>(this->m_model);
    if(m){
        RXExpEditDomDocument d = m->Document();
        rc=  d.GetEditedElements(d.firstChild());
    }
    return rc;
}

void RXExpEditSubWindow::on_apply()
{
    RXExpEditDomModel *m = static_cast<RXExpEditDomModel *>(this->m_model);
    QString TheMessage;
    if(m){
        RXExpEditDomDocument d = m->Document();
        QDomNode  root = d.firstChild();
        QList<QDomNode>  mylist= GetEditedElements();

        for(int i=0;i<mylist.length();i++){
            QDomNode n = mylist[i];
            QDomElement e =n.toElement();
            QString buf = e.nodeValue() ;
            if(buf.isEmpty())
                continue;
            buf.prepend("=");
            while (n !=root){
                buf.prepend(n.nodeName()); buf.prepend("$");
                n=n.parentNode();
            }
            buf.remove(0,1);
            buf.prepend("change:");
            e.removeAttribute("edited");
            TheMessage += buf + QString(" ; ");
        }
             emit Send(TheMessage);
     }
}

void RXExpEditSubWindow::on_cancel()
{
    qDebug()<<"CANCEL";
    on_refresh();

}


