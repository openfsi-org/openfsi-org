#ifndef RXEXPEDITDOMDOCUMENT_H
#define RXEXPEDITDOMDOCUMENT_H
#include <QObject>
 #include <QDomDocument>

 class QString;

//! [0]
class RXExpEditDomDocument:
        public QDomDocument
{
 //     Q_OBJECT



public:
    RXExpEditDomDocument();
    RXExpEditDomDocument(const QString&name);
    ~RXExpEditDomDocument();
    int SetContentByString(const QString &lp);
    bool Write(const QString &fname);
    QList<QDomNode>  GetEditedElements(QDomNode seed );
signals:

public slots:

};
//! [0]

#endif // RXRCDOMDOCUMENT_H
