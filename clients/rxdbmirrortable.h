#ifndef RXDBMIRRORTABLE_H
#define RXDBMIRRORTABLE_H

#include <QSqlTableModel>
#include "rxdbcelledit.h"

class RXdbMirrorModel : public QSqlTableModel
{
    Q_OBJECT
public:
    explicit RXdbMirrorModel(QObject *parent,QSqlDatabase db);

signals:

public slots:
    void refresh();
public:
        void PauseTimer()  const ;
        void ContinueTimer()  const;
        class RXDBCellEdit m_tbDelegate;
        class QTimer * m_DBMirrorTimer;
private:
        bool m_IsPaused;
};

#endif // RXDBMIRRORTABLE_H
