/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QDomDocument>
#include <QFile>
#include <QtGui>

#include "rxruncontrolsdommodel.h"
#include "rxruncontrolsdomdocument.h"
#include "rxruncontrolsmainwindow.h"

RXRunControlsMainWindow::RXRunControlsMainWindow() : QMainWindow(), model(0)
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(tr("&Open..."), this, SLOT(openFile()),
                        QKeySequence::Open);
    fileMenu->addAction(tr("&SaveAs..."), this, SLOT(SaveFileAs()),
                        QKeySequence::Open);
    fileMenu->addAction(tr("&Text..."), this, SLOT(openText()),
                        QKeySequence::Open);
    fileMenu->addAction(tr("E&xit"), this, SLOT(close()),
                        QKeySequence::Quit);

    model = new RXRunControlsDomModel(RXRunControlsDomDocument(), this);
    view = new QTreeView(this);
    view->setModel(model);

    view->setItemDelegateForColumn(2,&m_tbdelegate);
    setCentralWidget(view);
    setWindowTitle(tr("OFSI Analysis Control"));
}

void RXRunControlsMainWindow::openFile()
{
    QString filePath = QFileDialog::getOpenFileName(this, tr("Open File"),
        xmlPath, tr("XML files (*.xml);;HTML files (*.html);;"
                    "SVG files (*.svg);;User Interface files (*.ui)"));

    if (!filePath.isEmpty()) {
        QFile file(filePath);
        if (file.open(QIODevice::ReadOnly)) {
            RXRunControlsDomDocument document;
            if (document.setContent(&file)) {
                RXRunControlsDomModel *newModel = new RXRunControlsDomModel(document, this);
                view->setModel(newModel);
                delete model;
                model = newModel;
                xmlPath = filePath;
            }
            file.close();
        }
    }
}
void RXRunControlsMainWindow::SaveFileAs()
{
    QString filePath = QFileDialog::getSaveFileName(this, tr("Save File"),
        xmlPath, tr("XML files (*.xml);;HTML files (*.html);;"
                    "SVG files (*.svg);;User Interface files (*.ui)"));

    if (!filePath.isEmpty()) {
           model->Document().Write(filePath);
    }
}
void RXRunControlsMainWindow::openText()
{
    RXRunControlsDomDocument document;
    QString lp = "1$4$-120.$0$0$/FI/HC/S6/LV/CC $0$ $1$NONE $  2$4$-120.0$0$0$ $0$ $0$phase1.wak $  3$4$-120.0$0$0$/FI/S6/D2 $0$ $99$phase2.wak $";
    if (document.SetContentByACString(lp)) {
        RXRunControlsDomModel *newModel = new RXRunControlsDomModel(document, this);
        view->setModel(newModel);
        delete model;
        model = newModel;
        xmlPath =  "text";
    }

}
