/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QDomDocument>
#include <QGridLayout>
#include <QFile>
#include <QDebug>
#include <QTreeView>
#include <QFileDialog>

#include "rxruncontrolsdommodel.h"
#include "rxruncontrolsdomdocument.h"
#include "rxruncontrolsubwindow.h"

RXRunControlSubWindow::RXRunControlSubWindow() : QWidget(), m_model(0)
{
    if(1){
        QWidget *subwindow = this;
        subwindow->setObjectName(QString::fromUtf8("Run Control"));
        subwindow->setWindowTitle(tr("Run Control"));
        subwindow->setAttribute(Qt::WA_DeleteOnClose);
        QGridLayout *gridLayout_2 = new QGridLayout(subwindow);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));


        m_view = new QTreeView(this);
        m_view->setObjectName(QString::fromUtf8("treeView"));

        gridLayout_2->addWidget(m_view, 0, 0, 1, 3);
    }
    else
        m_view = new QTreeView(this);

    m_model = new RXRunControlsDomModel(RXRunControlsDomDocument(), this);
    m_view->setModel(m_model);

    m_view->setItemDelegateForColumn(2,&m_tbDelegate);
    this->setMinimumSize(500,500);
    m_view->setColumnWidth(0,180);
    m_view->setColumnWidth(1,120);
    m_view->setColumnWidth(2,120);
}
RXRunControlSubWindow::~RXRunControlSubWindow()
{
    qDebug()<<"close RXRCSW";
}

void RXRunControlSubWindow::openFile()
{
    QString filePath = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    xmlPath, tr("XML files (*.xml);;HTML files (*.html);;"
                                                                "SVG files (*.svg);;User Interface files (*.ui)"));

    if (!filePath.isEmpty()) {
        QFile file(filePath);
        if (file.open(QIODevice::ReadOnly)) {
            RXRunControlsDomDocument document;
            if (document.setContent(&file)) {
                RXRunControlsDomModel *newModel = new RXRunControlsDomModel(document, this);
                m_view->setModel(newModel);
                delete m_model;
                m_model = newModel;
                xmlPath = filePath;
            }
            file.close();
        }
    }
}
void RXRunControlSubWindow::SaveFileAs()
{
    QString filePath = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                    xmlPath, tr("XML files (*.xml);;HTML files (*.html);;"
                                                                "SVG files (*.svg);;User Interface files (*.ui)"));

    if (!filePath.isEmpty()) {
        m_model->Document().Write(filePath);
    }
}
void RXRunControlSubWindow::openText()
{
    RXRunControlsDomDocument document;

    const  char *llp = "1$4$-120.$0$0$/FI/HC/S6/LV/CC $0$ $1$NONE $  2$4$-120.0$0$0$ $0$ $0$phase1.wak $  3$4$-120.0$0$0$/FI/S6/D2 $0$ $99$phase2.wak $";
    QString lp = llp; qDebug()<<"llp  "<<llp;
    // as a decode test
    QByteArray ba = lp.toLatin1(); // was toAscii
    QByteArray bac = qCompress(ba,9);
    QByteArray bacc = bac.toBase64();
    qDebug()<<"bacc "<< bacc ;

    bac= QByteArray::fromBase64(bacc );
    ba = qUncompress(bac);
    qDebug()<<"ba   "<<ba;
    qDebug()<<"bac  "<<bac;

    if (document.SetContentByACString(lp)) {
        RXRunControlsDomModel *newModel = new RXRunControlsDomModel(document, this);
        m_view->setModel(newModel);
        delete m_model;
        m_model = newModel;
        xmlPath =  "text";
    }

}
