#ifndef RXAEROTYPECHOOSER_H
#define RXAEROTYPECHOOSER_H

#include <QComboBox>
// see control_menu.h
//#define RELAX_PRESSURE   (1)
//#define WERNER_PRESSURE (2)
//#define CONSTANT_PRESSURE (4)
//#define FILE_PRESSURE (8)
//#define CUSTOM_PRESSURE (16)
//#define SCRIPT_PRESSURE (32)
//#define expression_PRESSURE (64)


class RXAeroTypeChooser : public QComboBox
{
    Q_OBJECT
public:
    explicit RXAeroTypeChooser(QWidget *parent = 0);
    int IndexToCode(const int i);
    int CodeToIndex(const int i);
signals:

public slots:

};

#endif // RXAEROTYPECHOOSER_H
