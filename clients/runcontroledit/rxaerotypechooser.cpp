#include <QDebug>
#include "rxaerotypechooser.h"

RXAeroTypeChooser::RXAeroTypeChooser(QWidget *parent) :
    QComboBox(parent)
{
    addItem("Script");
    addItem("File");
    addItem("Custom");
    addItem("Constant");
    addItem("Relax(dodgy)");
    // see control_menu.h
    //#define RELAX_PRESSURE   (1)
    //#define PANSAIL_PRESSURE (2)
    //#define CONSTANT_PRESSURE (4)
    //#define FILE_PRESSURE (8)
    //#define CUSTOM_PRESSURE (16)
    //#define SCRIPT_PRESSURE (32)

}
int RXAeroTypeChooser::IndexToCode(const int i)
{
   // qDebug()<<"Aerotype IndexToCode "<<i;
    switch(i){
    case 0:
        return  32 ;  //script
    case 1:
        return  8 ;
    case 2:
        return  16 ;
    case 3:
        return  4 ;
    case 4:
        return  1 ;
    case 5:
        return 64 ;
    default:
          qDebug()<<"Aerotype IndexToCode unknown index "<<i;;

    };
return 1;
}

int RXAeroTypeChooser::CodeToIndex(const int i)
{
   //qDebug()<<"aerotype CodeToIndex code "<<i;
       switch(i){
    case 32:        //#define SCRIPT_PRESSURE (32)
        return   0;
    case 8:         //#define FILE_PRESSURE (8)
        return   1;
    case 16:        //CUSTOM_PRESSURE (16)
        return   2;
    case 4:         //#define CONSTANT_PRESSURE (4)
        return   3;
    case 1:         //#define RELAX_PRESSURE   (1)
        return   4;
       case 64:         //#define RELAX_PRESSURE   (1)
           return   5;
    default:
           qDebug()<<"aerotype CodeToIndex unknown code "<<i;
       };
       return 4;
}

