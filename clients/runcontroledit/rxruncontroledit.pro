HEADERS     = \
    rxaerotypechooser.h \
    rxruncontrolsmainwindow.h \
    rxruncontrolsdomitem.h \
    rxruncontrolsdommodel.h \
    rxruncontrolsdomdocument.h \
    ../textboxdelegate/rxruncontrolcelledit.h
HEADERS +=


SOURCES     = \
         main.cpp \
        rxruncontrolsdomdocument.cpp \
    rxaerotypechooser.cpp \
    rxruncontrolsdomitem.cpp \
    rxruncontrolsdommodel.cpp \
    rxruncontrolsmainwindow.cpp \
    ../textboxdelegate/rxruncontrolcelledit.cpp

CONFIG  += qt
QT      += xml
#debug:CONFIG +=DEBUG

INCLUDEPATH +=  ../src/clients
INCLUDEPATH +=  ../spinboxdelegate
INCLUDEPATH +=  ../textboxdelegate

# install
target.path = $$[QT_INSTALL_EXAMPLES]/itemviews/simpledommodel
sources.files = $$SOURCES $$HEADERS $$RESOURCES *.pro
sources.path = $$[QT_INSTALL_EXAMPLES]/itemviews/simpledommodel
INSTALLS += target sources

symbian: include($$PWD/../../symbianpkgrules.pri)
maemo5: include($$PWD/../../maemo5pkgrules.pri)
