#include "rxdbmirrortable.h"

RXdbMirrorModel::RXdbMirrorModel(QObject *parent,QSqlDatabase db) :
    QSqlTableModel(parent,db),
    m_DBMirrorTimer(0),
         m_IsPaused(false)
{
}
// public slots:
    void RXdbMirrorModel::refresh()

    {
        if(m_IsPaused)
            return;
        select();
    }

    void  RXdbMirrorModel::PauseTimer()  const
    {
      //  m_IsPaused=true;
        if(m_DBMirrorTimer)
            m_DBMirrorTimer->stop();
    }

    void RXdbMirrorModel::ContinueTimer()  const
    {
      //  m_IsPaused=false;
        if(m_DBMirrorTimer)
            m_DBMirrorTimer->start();
    }
