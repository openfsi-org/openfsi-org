/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

/*
    rxexpeditcelledit.cpp

    A delegate that allows the user to change string values from the model
    This one works with our XML model.

*/

#include <QSpinBox>
#include "rxexpeditdommodel.h"
#include "rxexpeditdomdocument.h"
#include "rxexpeditdomitem.h"
#include "rxaerotypechooser.h"
#include "rxexpeditcelledit.h"


//! [0]
RXExpEditCellEdit::RXExpEditCellEdit(QObject *parent)
    : QItemDelegate(parent)
{
}
//! [0]

//! [1]
QWidget *RXExpEditCellEdit::createEditor(QWidget *parent,
    const QStyleOptionViewItem & /*option */,
    const QModelIndex &  index ) const
{
      QModelIndex i =index;
      QDomElement  *pe ;;
      QString myatt;
      myatt="text";

      for(;;){
          pe =  static_cast<QDomElement*>(i.internalPointer());
          if(!pe)
              break;
          if(pe->isElement() && pe->hasAttribute("rxwhat")){
                  myatt = pe->attribute("rxwhat" );
                  break;
              }
              else {
                  i=i.parent();
                  if(!i.isValid())
                      break;
              }
      }
      // what<<"AeroType"<<"float"   <<"int"     <<"int"     <<"RFlags"<<"int"<<"file$*.uvp"<<"int"<<"file$*.wak";
      if(myatt=="int")
            return new QSpinBox(parent);
      if(myatt=="float")
            return new QSpinBox(parent);
      if(myatt=="AeroType")
            return new RXAeroTypeChooser(parent);


    QLineEdit *editor = new QLineEdit(parent);

    return editor;
}
//! [1]

//! [2]
void RXExpEditCellEdit::setEditorData(QWidget *editor,
                                    const QModelIndex &index) const
{
    QString value = index.model()->data(index, Qt::EditRole).toString();

    QDomElement *el =  static_cast<QDomElement*>(index.internalPointer());
    QLineEdit *textbox = dynamic_cast<QLineEdit*>(editor);
    if(textbox) {
        if(el->isText()) {
            QDomText xx= el->toText();
            value = xx.data();
        }
        else{
            QDomNode child = el->firstChild();
            if(child.isText()) {
                QDomText xx= child.toText();
                value = xx.data();
            }
            else
                value = el->nodeValue ();
        }
        textbox->setText(value);
        return;
    }
    QSpinBox *spinBox = dynamic_cast<QSpinBox*>(editor);
    if( spinBox) {
        int value;
        if(el->isText()) {
            QDomText xx= el->toText();
            QString s  = xx.data();
            value = s.toInt();
        }
        // else with luck it an int.
        else
            value = index.model()->data(index, Qt::EditRole).toInt();

        spinBox->setValue(value);
        return;
    }
    RXAeroTypeChooser *chooser = dynamic_cast<RXAeroTypeChooser*>(editor);
    if(chooser){
        int value;
        if(el->isText()) {
            QDomText xx= el->toText();
            QString s  = xx.data();
            value = s.toInt();
        }
        // else with luck it an int.
        else
            value = index.model()->data(index, Qt::EditRole).toInt();
        chooser->setCurrentIndex(chooser->CodeToIndex(value));
    }
}
//! [2]

//! [3]
void RXExpEditCellEdit::setModelData(QWidget *editor, QAbstractItemModel *model,
                                   const QModelIndex &index) const
{
    QLineEdit *textbox = dynamic_cast<QLineEdit*>(editor);
    if(textbox){
        QString value = textbox->text();
        model->setData(index, value, Qt::EditRole);
      QDomElement *el =  static_cast<QDomElement*>(index.internalPointer());
      el->setAttribute("edited",1);
        return;
    }
    QSpinBox *spinBox = dynamic_cast<QSpinBox*>(editor);
    if(spinBox){
        spinBox->interpretText();
        int value = spinBox->value();
        model->setData(index, value, Qt::EditRole);
        return;
    }
     RXAeroTypeChooser *chooser = dynamic_cast<RXAeroTypeChooser*>(editor);
     if(chooser){
         QString tt = chooser->currentText();
         int ii = chooser->currentIndex();
         int value = chooser->IndexToCode(ii);
         model->setData(index, value, Qt::EditRole);
     }

}
//! [3]

//! [4]
void RXExpEditCellEdit::updateEditorGeometry(QWidget *editor,
    const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    editor->setGeometry(option.rect);
}
//! [4]
