#-------------------------------------------------
#
# Project created by QtCreator 2011-01-05T07:27:16
#
#-------------------------------------------------
#
#  this is the rxclient which does trimming, analysis control, expressionEdit and DB watcher.
#
#-------------------------------------------------------
QT       += core gui widgets
QT           += network
QT      += xml
QT      += sql

#version check qt
contains(QT_VERSION, ^4\\.[0-6]\\..*) {
message("Cannot build Qt Creator with Qt version $${QT_VERSION}.")
error("Use at least Qt 4.7.")
}


 message(Qt is installed in $$[QT_INSTALL_PREFIX])

 message("RXClientBase for msvc2005 needs mysql support, otherwise its OK")
#CONFIG += coin3d
#CONFIG += soqt

#LIBS += -lSoQt

TARGET = rxclientbase
TEMPLATE = app

DEFINES      += RLX

SOURCES +=     rxclientbase.cpp \
    ../../rxserver/rxcommandlinebox.cpp \
    ../runcontroledit/rxruncontrolsubwindow.cpp \
    ../textboxdelegate/rxruncontrolcelledit.cpp \
    ../runcontroledit/rxruncontrolsdommodel.cpp \
    ../runcontroledit/rxruncontrolsdomitem.cpp \
    ../runcontroledit/rxruncontrolsdomdocument.cpp \
    ../runcontroledit/rxaerotypechooser.cpp \
    ../rxdbmirrortable.cpp \
    ../textboxdelegate/rxdbcelledit.cpp \
    ../expressionedit/rxexpeditdomdocument.cpp \
    ../expressionedit/rxexpeditdommodel.cpp \
    ../expressionedit/rxexpeditdomitem.cpp \
    ../expressionedit/rxexpeditsubwindow.cpp \
    ../textboxdelegate/rxexpeditcelledit.cpp \
    ../../thirdparty/qtsnippets/databasedialog.cpp \# \
    rxcbmain.cpp
#    ../runcontroledit/rxdomdocument.cpp

HEADERS  += rxclientbase.h \
    ../../rxserver/rxcommandlinebox.h \
    ../runcontroledit/rxruncontrolsdomdocument.h \
    ../runcontroledit/rxruncontrolsdomitem.h \
    ../runcontroledit/rxruncontrolsubwindow.h \
    ../runcontroledit/rxaerotypechooser.h \
    ../textboxdelegate/rxruncontrolcelledit.h \
    ../runcontroledit/rxruncontrolsdommodel.h \
    ../rxdbmirrortable.h \
    ../textboxdelegate/rxdbcelledit.h \
    ../expressionedit/rxexpeditdomdocument.h \
    ../expressionedit/rxexpeditdomitem.h \
    ../expressionedit/rxexpeditdommodel.h \
    ../expressionedit/rxexpeditsubwindow.h \
    ../textboxdelegate/rxexpeditcelledit.h \
    ../../thirdparty/qtsnippets/databasedialog.h \
    ../../thirdparty/opennurbs/opennurbs.h

FORMS    += rxclientbase.ui

INCLUDEPATH += ..
INCLUDEPATH += ../../rxcommon
INCLUDEPATH += ../../rxserver
INCLUDEPATH += ../../src/preprocess
INCLUDEPATH += ../expressionedit
INCLUDEPATH += ../runcontroledit
INCLUDEPATH += ../textboxdelegate
INCLUDEPATH += ../../thirdparty/qtsnippets


OTHER_FILES += \
    ../../thirdparty/soqttest/myfirstsoqt/soqtdev.txt







