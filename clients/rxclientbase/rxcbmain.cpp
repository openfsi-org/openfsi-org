#include <QApplication>

#include "rxclientbase.h"
class rxclientbase * g_cb;
int main(int argc, char *argv[])
{

#ifndef NEVER
    QApplication a(argc, argv);
    rxclientbase w(0);
    g_cb=&w;
     w.ProcessArguments();
    w.show();

    return a.exec();
#else
    QApplication a(argc, argv);
     QWidget * Mainwin = SoQt::init(argc, argv, argv[0]);
     rxclientbase w (0);//Mainwin
     w.show();

     w.ProcessArguments();
     SoQt::mainLoop();

// Clean up resources.
     SoQt::done();
#endif
}
