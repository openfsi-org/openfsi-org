#ifndef RXCLIENTBASE_H
#define RXCLIENTBASE_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QHostAddress>
#include <QSqlDatabase>

class QTcpSocket;
extern class rxclientbase * g_cb;
#include "StdRedirector.h"

namespace Ui {
class rxclientbase;
}

class rxclientbase : public QMainWindow
{
    Q_OBJECT

public:
    explicit rxclientbase(QWidget *parent = 0);
    ~rxclientbase();
    int ProcessArguments();
private:
    Ui::rxclientbase *ui;
    static void outcallback( const char* ptr, std::streamsize count,void *pTextBox );
    StdRedirector<> * myRedirector;

    void PrintHelp();
    void SetServer();
    void SetPort();
    void SetExec(const QString &s);
    void SetOnce( );


private slots:
    void ConnectToServer();
    void readOnSocket();
    void WriteOnSocket(const QString &s);
    void displayError(QAbstractSocket::SocketError socketError);
    void onSendTestMessage();
    void onDBDlg();
    void DisplayText(const QString &s);
    void on_rxcommandline_returnPressed();
#ifdef COIN3D
    void NewViewer();
#endif
    void NewDBMirrorEditor();
    void NewTrimEditor();
    void NewExpressionEditor();
    void NewRunControlEditor();
    void PopulateExpressionEditor(const QString &s);
    void HelloMessage( );
    void FirstMessage( );
    void on_subwindow_destroyed();
    void OnAbout();
    void slotHandleNewDatabaseConnection(QSqlDatabase&);

private:
    QTcpSocket *tcpSocket;

    QHostAddress m_ServerAddress;
    quint16 m_ServerPort;
    QStringList m_args;
    bool m_HasCommands;
    QString m_CommandToRun;
    bool m_once; int m_millisecs;
    class DatabaseConnectionDialog* m_dbdialog;
    qint64 m_bytesOutstandingCount  ;
    QString m_dataComingInBuffer;
    qint64 m_bytesAvailable;

#ifdef Q_OS_SYMBIAN
    bool isDefaultIapSet;
#endif
signals:
    void onReceiveText(const QString &s);

};

#endif // RXCLIENTBASE_H
