project (R3 CXX C Fortran)

# The name of our project is "R3". CMakeLists files in this project can
# refer to the root source directory of the project as
 #${R3_SOURCE_DIR} (/home/r3/build/r3bld/src )and
# to the root binary directory of the project as ${R3_BINARY_DIR}.
cmake_minimum_required (VERSION 2.6)
SET(CMAKE_VERBOSE_MAKEFILE "ON")

set(R3_SOURCE_DIR "/home/r3/Documents/qt/src"  )
find_package(Qt4 REQUIRED) # find and setup Qt4 for this project



 message(" qt use file is" ${QT_USE_FILE})
# see http://qtnode.net/wiki/Qt4_with_cmake
set(qtproject_SRCS   )
qt4_automoc(${qtproject_SRCS})

set (TARGET "rxlib")
set(CMAKE_C_COMPILER "icc")
set(CMAKE_CXX_COMPILER "icpc")
# C language
# for debug
set (CMAKE_PETERS_FLAGS  "-m64 -g -diag-disable 161,186,858,654,1125,279")
#ADD_DEFINITIONS(-D_DEBUG)
ADD_DEFINITIONS(-DDEBUG)



## or for release
#set (CMAKE_PETERS_FLAGS  "-m64 -O2 -diag-disable 161,186,858,654,1125,279")

#ADD_DEFINITIONS(-DNDEBUG)




 enable_language (Fortran)
# FFLAGS depend on the compiler
get_filename_component (Fortran_COMPILER_NAME ${CMAKE_Fortran_COMPILER} NAME)

if (Fortran_COMPILER_NAME STREQUAL "gfortran")
  # gfortran
  set (CMAKE_Fortran_FLAGS_RELEASE "-funroll-all-loops -fno-f2c -O3 -heap-arrays 100")
  set (CMAKE_Fortran_FLAGS_DEBUG   "-fno-f2c -O0 -g -heap-arrays 10")
  message(" found gfortran - compile with flags: ${CMAKE_Fortran_FLAGS}")
elseif (Fortran_COMPILER_NAME STREQUAL "ifort")
 
#/DNO_DOFOCLASS /DWINTEST /DDOFO_CONNECTS /recursive /Qopenmp /Qopenmp-report2 /Qpar-report1 /Qvec-report1 
#/warn:declarations /names:uppercase /module:"Release\\" /object:"Release\\" /ccdefault:list /check:none #/libs:dll /threads /c
#-DWINTEST  --parallel --heap-arrays 10 /names:uppercase



# fortran -openmp-stubs
  set (CMAKE_PETERS_FFLAGS "-mkl -m64 -multiple-processes=6 -DNO_DOFOCLASS -DDOFO_CONNECTS -fpp -recursive -heap-arrays 10 -fexceptions")

#-openmp maybe -fast -fast-transcendentals -fp-stack-check
  set (CMAKE_Fortran_FLAGS_RELEASE " -O3  ${CMAKE_PETERS_FFLAGS}  -parallel -openmp ")
  set (CMAKE_Fortran_FLAGS_DEBUG   " -g -O0 -check all  -traceback -openmp ${CMAKE_PETERS_FFLAGS}")
  set (CMAKE_Fortran_FLAGS   "${CMAKE_Fortran_FLAGS_RELEASE}")

  message(" found ifort - compile with flags: ${CMAKE_Fortran_FLAGS}")
elseif (Fortran_COMPILER_NAME STREQUAL "g77")
  # g77
  set (CMAKE_Fortran_FLAGS_RELEASE "-funroll-all-loops -fno-f2c -O3 -m32")
  set (CMAKE_Fortran_FLAGS_DEBUG   "-fno-f2c -O0 -g -m32")
else (Fortran_COMPILER_NAME STREQUAL "gfortran")
  message ("CMAKE_Fortran_COMPILER full path: " ${CMAKE_Fortran_COMPILER})
  message ("Fortran compiler: " ${Fortran_COMPILER_NAME})
  message ("No optimized Fortran compiler flags are known, we just try -O2...")
  set (CMAKE_Fortran_FLAGS_RELEASE "-O2")
  set (CMAKE_Fortran_FLAGS_DEBUG   "-O0 -g")
endif (Fortran_COMPILER_NAME STREQUAL "gfortran")

set(CMAKE_LINKER "ifort")  



IF(NOT CMAKE_Fortran_COMPILER_WORKS)
  message("Fortran doesnt work")
ENDIF(NOT CMAKE_Fortran_COMPILER_WORKS)

file(GLOB C_hdrs preproc/c/inc/*.h)
file(GLOB CPP_hdrs preproc/cpp/inc/*.h)

#add_executable
add_library (${TARGET}  ${C_hdrs}  ${CPP_hdrs}
#./motifmain/main.cpp

 
#amgclass/AMGAngleIntegration.cpp	   
#amgclass/AMGBrentTapePlacement.cpp	   
#amgclass/AMGHoopsObject.cpp	   
#amgclass/AMGLevel.cpp	   
#amgclass/AMGLevelMaterial.cpp	   
#amgclass/AMGListLevelMaterials.cpp	   
#amgclass/AMGNelderMead.cpp	   
#amgclass/AMGPolygon.cpp	   
#amgclass/AMGTape.cpp	   
#amgclass/AMGTapingZone.cpp	   
#amgclass/AMGTapingZone_V2.cpp	   
#amgclass/AMGTapingZone_V3.cpp	   
#amgclass/AMGVertex.cpp	   
#amgclass/AMGVlink.cpp	   
#amgclass/AMGZERef.cpp	   
#amgclass/AMGZoneCycle.cpp	   
#amgclass/AMGZoneEdge.cpp	   
#amgclass/AMGZoneNode.cpp	   
#amgclass/ModulusMap.cpp	   

 #post/add_hoops.cpp
#post/adjlen.cpp
post/anchors.cpp
#post/attach.cpp
#post/attachmod.cpp
post/CheckAll.cpp
post/drawforces.cpp
post/drawwind.cpp
#post/example.cpp
#post/find_include.cpp
#post/getshell.cpp
post/intPress.cpp
#post/mast.cpp
#post/panout.cpp
post/postutil.cpp
post/r2af.cpp
#post/readedg.cpp
post/resid.cpp
#post/script.cpp
#post/settrim.cpp

#post/stripe.cpp
#post/trivals.cpp
#post/wrstate.cpp



preproc/c/aero.cpp
preproc/c/aeromik.cpp
preproc/c/arclngth.cpp
preproc/c/appwind.cpp
#preproc/c/basecurv.cpp
#preproc/c/bend.cpp
preproc/c/boatgen.cpp
preproc/c/boundary.cpp
preproc/c/boundingbox.cpp
preproc/c/breakc.cpp
preproc/c/cam2.cpp
preproc/c/cfromf.cpp
preproc/c/cut.cpp
preproc/c/compute.cpp
preproc/c/compute_stripes.cpp
preproc/c/akmutil.cpp
preproc/c/batpatch.cpp		
preproc/c/batstiff.cpp	
preproc/c/boxsearch.cpp
preproc/c/drawing.cpp

preproc/c/delete.cpp
preproc/c/dxfout.cpp
preproc/c/dxf.cpp
preproc/c/dxfin.cpp
preproc/c/editword.cpp
preproc/c/entities.cpp	
preproc/c/entutils.cpp	   
preproc/c/etypes.cpp	   
preproc/c/fields.cpp	   
preproc/c/filament.cpp	   
preproc/c/findtransform.cpp	   
preproc/c/finish.cpp	   
preproc/c/freeze.cpp	   
preproc/c/gauss.cpp	   
preproc/c/gcurve.cpp	   
preproc/c/gen.cpp	   
preproc/c/geodesic.cpp	   
preproc/c/getglobal.cpp	   
preproc/c/gle.cpp	   
preproc/c/global_declarations.cpp	   
preproc/c/iangles.cpp	   
preproc/c/iges.cpp	   
preproc/c/interpln.cpp	   
  
preproc/c/material.cpp	   
preproc/c/matinvd.cpp	   
preproc/c/nlm.cpp	   
preproc/c/nurbs.cpp	   
preproc/c/oncut.cpp	   
preproc/c/panel.cpp	   
preproc/c/paneldata.cpp	   
preproc/c/pansin.cpp	   
preproc/c/pc_solvecubic.cpp	   
preproc/c/pcspline.cpp	   
preproc/c/pcwin.cpp	   
preproc/c/peterfab.cpp	   
preproc/c/peternew.cpp	   
preproc/c/preproc.cpp	   
preproc/c/pressure.cpp	   
preproc/c/printall.cpp	   
   
#preproc/c/rdpanfab.cpp
   
preproc/c/read.cpp	   
preproc/c/ReadBagged.cpp	   
preproc/c/ReadBoat.cpp	   
preproc/c/ReadFix.cpp	   
preproc/c/readname.cpp	   
	   
preproc/c/readstring.cpp	   
preproc/c/ReadStripe.cpp	   
preproc/c/redraw.cpp	   
preproc/c/reledit.cpp	   
   
preproc/c/rlx_to_dxf.cpp	   
preproc/c/RX_OncIntersect.cpp	   
   
preproc/c/scedit.cpp	   
preproc/c/seamcurve.cpp	   
preproc/c/slowcut.cpp	   
preproc/c/stringutils.cpp	   
preproc/c/summary.cpp	   
	   
preproc/c/table.cpp	   
preproc/c/targets.cpp	   
preproc/c/text.cpp	   
   
preproc/c/transform.cpp	   
preproc/c/uvcurve.cpp	   
#preproc/c/velocity.cpp
preproc/c/words.cpp	   
preproc/cpp/AMG3dm.cpp	   
preproc/cpp/BrentMethod.cpp	   
preproc/cpp/curveintersection.cpp	   
preproc/cpp/curveplaneintersection2.cpp	   
   
#preproc/c/scanfile.cpp	
#preproc/cpp/dovecheckerboard.cpp	   
#preproc/cpp/dovechoose.cpp	   
#preproc/cpp/doveforstack.cpp	   
#preproc/cpp/doveGeometric.cpp	   
#preproc/cpp/dovestructure.cpp	   
#preproc/cpp/dovestructurebylayersurfs.cpp	   
#preproc/cpp/doveTriradial.cpp	   
#preproc/cpp/doveTriradialLamdbaNe.cpp	   
#preproc/cpp/layerangle.cpp	   
#preproc/cpp/layercontour.cpp	   
#preproc/cpp/layerlayer.cpp	   
#preproc/cpp/layerlink.cpp	   
#preproc/cpp/layernode.cpp	   
preproc/cpp/layerobject.cpp	   
#preproc/cpp/layertst.cpp	   
#preproc/cpp/layervertex.cpp
	   
#motifmain/AKM_Update_Display.cpp
#motifmain/Attributes.cpp
motifmain/CalcNow.cpp	   
motifmain/commslib.cpp
#motifmain/CreateMsgArea.cpp
motifmain/drop.cpp	   
motifmain/mainstuf.cpp
	   
#motifmain/menuCB.cpp
#motifmain/motif_err_dialog.cpp
motifmain/OpenBuildFile.cpp
#motifmain/OpenMat.cpp
motifmain/opentext.cpp
motifmain/question.cpp
motifmain/SaveSail.cpp
#motifmain/SaveSailAs.cpp
#motifmain/snapshot.cpp
#motifmain/telnet.cpp
motifmain/traceback.cpp	   
#motifmain/trimmenu.cpp
#motifmain/trimmenu_s.cpp
#motifmain/utility.cpp

preproc/cpp/NelderMead.cpp	   
preproc/cpp/RelaxWorld.cpp	   
preproc/cpp/RLX3dm.cpp	   
preproc/cpp/RLX3dmbuffer.cpp	   
preproc/cpp/rlxNearestPoint.cpp	   
   
preproc/cpp/RX_FEBeam.cpp	   
preproc/cpp/RX_FEedge.cpp	   
preproc/cpp/RX_FELinearObject.cpp	   
preproc/cpp/RX_FEObject.cpp	   
preproc/cpp/RX_FEPocket.cpp	   
preproc/cpp/RX_FESite.cpp	   
preproc/cpp/RX_FEString.cpp	   
preproc/cpp/RX_FETri3.cpp	   
preproc/cpp/RX_integrate.cpp	   
preproc/cpp/RX_PCFile.cpp	   
preproc/cpp/RX_SeamcurveHelper.cpp	   
preproc/cpp/RX_SimpleTri.cpp	 
preproc/cpp/RXCompoundCurve.cpp	   
preproc/cpp/RXContactSurface.cpp	   
preproc/cpp/RXCrossing2.cpp	   
preproc/cpp/RXCurve.cpp
preproc/cpp/RXCurveIntersection.cpp	   
#preproc/cpp/RXDBLink.cpp
#preproc/cpp/RXdovefile.cpp	   
preproc/cpp/RXDrawingLayer.cpp	   
preproc/cpp/RXEntity.cpp	   
preproc/cpp/RXEntityDefault.cpp	
preproc/c/resolve.cpp   
preproc/cpp/RXExceptions.cpp	   
preproc/cpp/RXExpression.cpp
preproc/cpp/rxfixity.cpp
preproc/cpp/RXGridDensity.cpp	   
preproc/cpp/RXHarwellBoeingIO.cpp	   
preproc/cpp/RXHarwellBoeingIO1.cpp
preproc/cpp/rximportentity.cpp
preproc/cpp/RXIntegration.cpp	   
preproc/cpp/RXInterpolationI.cpp	   
preproc/cpp/RXLayer.cpp	   
preproc/cpp/RXLogFile.cpp	   
#preproc/cpp/RXmaterialMatrix.cpp	   
preproc/cpp/RXMesh.cpp	   
preproc/cpp/RXMTEnquire.cpp	   
preproc/cpp/RXObject.cpp	   
preproc/cpp/RXOffset.cpp	   
preproc/cpp/RXON_3dPoint.cpp	
preproc/cpp/RXON_Extensions.cpp
preproc/cpp/RXON_Matrix.cpp	   
preproc/cpp/RXON_Surface.cpp	   
preproc/cpp/RXOptimisation.cpp	   
preproc/cpp/RXOptVariable.cpp	   
preproc/cpp/RXPanel.cpp	   
preproc/cpp/RXAttributes.cpp	   
preproc/cpp/RXPointOnCurve.cpp
preproc/cpp/RXPolyline.cpp	   
preproc/cpp/RXPressureInterpolation.cpp	   
preproc/cpp/RXPside.cpp	   
preproc/cpp/RXQuantity.cpp	   
preproc/cpp/RXSail.cpp	   
preproc/cpp/RXScalarProperty.cpp	   
preproc/cpp/RXSeamcurve.cpp	   
preproc/cpp/RXShapeInterpolation.cpp	   
preproc/cpp/RXSiteBase.cpp	   
preproc/cpp/RXSitePt.cpp	   
preproc/cpp/RXSparseMatrix.cpp	   
preproc/cpp/RXSparseMatrix1.cpp
preproc/cpp/rxspline.cpp
preproc/cpp/rxbcurve.cpp
preproc/cpp/RXSRelSite.cpp	   
	   
preproc/cpp/RXSummaryTolerance.cpp	   
preproc/cpp/RXTriangle.cpp	   
preproc/cpp/RXTriplet.cpp	   
preproc/cpp/RXTRObject.cpp	   
preproc/cpp/RXUpoint.cpp
preproc/cpp/rxvectorfield.cpp
preproc/cpp/TensylLink.cpp	   
preproc/cpp/TensylNode.cpp	   
preproc/cpp/TensylProp.cpp	   
   
preproc/meshing/barycentric.cpp	   
preproc/meshing/polyline.cpp	   
preproc/meshing/vectors.cpp	   
 
preproc/c/debug.cpp

x/OpenView.cpp
x/report.cpp
x/tree.cpp

f90/dofolist.f90
f90/feaPrint.f90     

f90/fspline.f90         
f90/rxfoperators.f90
f90/dofoprototypes.f90  
f90/Fslidecurve.f90  
f90/hoopsinterface.f90
f90/angcheck.f90
f90/bar_elements.f90
f90/basictypes.f90
f90/batn_els.f90
f90/batstuf.f90
f90/batten.f90
f90/battenf90.f90
f90/beamelementlist.f90
f90/beamElementMethods.f90
f90/beamelementtype.f90
f90/bspline.f90
f90/cfromf_declarations.f90
f90/connects.f90
f90/connectstuf.f90
f90/coordinates.f90
f90/dofoarithmetic.f90
f90/dofo.f90
f90/dofo_subtypes.f90
f90/edgelist.f90
f90/f90_to_c.f90
f90/fglobals.f90
f90/flagset.f90
f90/flinklist.f90
f90/flopen.f90
f90/fnoderef.f90
f90/fstrings.f90
f90/ftypes.f90
f90/gle.f90
f90/gle_types.f90
f90/gletypes.f90
f90/gravity.f90
f90/invert.f90
f90/isnan.f90
f90/kbat_els.f90
f90/kbatten.f90
f90/linklist.f90
f90/links_pure.f90
f90/mathconstants.f90
f90/math.f90
f90/msvc_fortran_utils.f90
f90/mtkaElement.f90
f90/nastraninterface.f90
f90/nlstuf.f90
f90/nodalrotations.f90
f90/nodelist.f90
f90/nograph.f90
f90/nonlin.f90
f90/oonatristuf.f90
f90/parse.f90
f90/plotps.f90
f90/realloc.f90
f90/relax32.f90
f90/relaxsq.f90
f90/removeelement.f90
f90/reset.f90
f90/rlxflags.f90
f90/rximplicitsolution.f90
f90/saillist.f90
f90/sf_tris_pure.f90
f90/sf_vectors.f90
f90/shape.f90
f90/shapegen.f90
f90/solvecubic.f90
f90/solve.f90
f90/spline.f90
f90/sspline.f90
f90/stiffness.f90
f90/stringlist.f90
f90/stringsort.f90
# f90/stub12.f90
f90/tanpure.f90
f90/tohoops.f90
f90/trilist.f90
f90/tris.f90
f90/tris_pure.f90
f90/ttmass9.f90
f90/vectors.f90
f90/wrinkleFromMathematica.f90
thirdparty/sparskit/FORMATS/formats.f
thirdparty/sparskit/BLASSM/blassm.f
thirdparty/sparskit/FORMATS/unary.f

)

# we may need
# FindJPEG.cmake
#FindLibXml2.cmake
#FindMotif.cmake
#FindVTK.cmake
#FindX11.cmake
#FindZLIB.cmake

find_package(MySQL)
IF(MySQL_FOUND)
        INCLUDE_DIRECTORIES(${MySQL_INCLUDE_DIR})
        TARGET_LINK_LIBRARIES(${TARGET} ${MySQL_LIBRARY})
        message("MySQL included in build: ${MySQL_LIBRARY}   ")
ELSE(MySQL_FOUND)
        message("no MySQL, so using /usr/include/mysql ")
        include_directories ("/usr/include/mysql")
ENDIF(MySQL_FOUND)

set (ONPATH "${PROJECT_SOURCE_DIR}/thirdparty/opennurbs")
# set (MKLROOT "/opt/intel/composerxe-2011.1.107/mkl")

include_directories (${MKLROOT}/include)
include_directories ("${PROJECT_SOURCE_DIR}/../rxkernel")
include_directories(${R3_SOURCE_DIR}/inc)
include_directories(${R3_SOURCE_DIR}/preproc/cpp/inc)
include_directories(${R3_SOURCE_DIR}/preproc/c/inc)
include_directories(${R3_SOURCE_DIR}/hoops/inc)
include_directories(${R3_SOURCE_DIR})
include_directories(${R3_SOURCE_DIR}/x/inc)
include_directories(${ONPATH})

include_directories ("${PROJECT_SOURCE_DIR}/MTParserLib")
include_directories ("${PROJECT_SOURCE_DIR}/thirdparty/triangle")
include_directories ("${PROJECT_SOURCE_DIR}/thirdparty/nr")
include_directories ("${PROJECT_SOURCE_DIR}/thirdparty/hbio")
include_directories ("${PROJECT_SOURCE_DIR}/post/inc") 

include_directories ("${PROJECT_SOURCE_DIR}/amgclass/inc")

include_directories ("/home/r3/QtSDK/Desktop/Qt/473/gcc/include/QtCore")
include_directories ("/home/r3/QtSDK/Desktop/Qt/473/gcc/include")
include_directories ("/home/r3/QtSDK/Desktop/Qt/473/gcc/include/QtOpenGL")


message("this cmakelist is specific to /home/r3/QtSDK/Desktop/Qt/473/gcc ")


ADD_DEFINITIONS(-DRXQT)
ADD_DEFINITIONS(-DRXLIB)
ADD_DEFINITIONS(-DRLX_64)

ADD_DEFINITIONS(-D_UNICODE)
ADD_DEFINITIONS(-DUNICODE)
ADD_DEFINITIONS(-DRX_ICC)

ADD_DEFINITIONS(-DFORTRANLINKED)

ADD_DEFINITIONS(-DANSI_DECLARATORS)
ADD_DEFINITIONS(-DTRILIBRARY)
ADD_DEFINITIONS(-DRELAX_TOO)
ADD_DEFINITIONS(-DCUSTOM)
ADD_DEFINITIONS(-Dlinux)

 #181 is format string
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_PETERS_FLAGS} ")



add_subdirectory (stringtools)
set (EXTRA_LIBS ${EXTRA_LIBS} ${PROJECT_SOURCE_DIR}/stringtools )
target_link_libraries (${TARGET} "stringtools")

add_subdirectory (MTParserLib)
set (EXTRA_LIBS ${EXTRA_LIBS} -l${PROJECT_SOURCE_DIR}/MTParserLib )
target_link_libraries (${TARGET} "MTParserLib")

add_subdirectory (hbio)
set (EXTRA_LIBS ${EXTRA_LIBS} ${PROJECT_SOURCE_DIR}/hbio )
target_link_libraries (${TARGET} "hbio")

add_subdirectory (triangle)
set (EXTRA_LIBS ${EXTRA_LIBS} ${PROJECT_SOURCE_DIR}/triangle )
target_link_libraries (${TARGET} "triangle")

set(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS}  -nofor_main -cxxlib -g  -openmp-stubs" )

SET_TARGET_PROPERTIES(${TARGET}
      PROPERTIES
     LINKER_LANGUAGE Fortran)
  SET(LINKER_LANGUAGE Fortran)
 SET_TARGET_PROPERTIES(${TARGET} PROPERTIES LINKER_LANGUAGE Fortran)

