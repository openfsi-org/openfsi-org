# -------------------------------------------------
# Project created by QtCreator 2011-05-04T12:38:28
# -------------------------------------------------
QT +=network
QT += opengl

TARGET = rxGraphicsStub
TEMPLATE = lib
CONFIG += staticlib  # qt5 is case-sensitive here
include (../../openfsicompilerconfig.pri)

DEFINES += RXGRAPHICSSTUB_LIBRARY
DEFINES += RXKERNEL_LIBRARY
#DEFINES += NO_RXGRAPHICS

SOURCES += \
    ../preprocess/RXGraphicCalls.cpp
HEADERS += rxgraphicsstub.h \
    rxGraphicsStub_global.h \
    ../preprocess/RXGraphicCalls.h

INCLUDEPATH +=../preprocess

INCLUDEPATH +=  ../../rxserver
INCLUDEPATH +=  ../../rxcommon
#INCLUDEPATH +=  ../../src/stringtools


