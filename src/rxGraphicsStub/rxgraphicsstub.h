#ifndef RXGRAPHICSSTUB_H
#define RXGRAPHICSSTUB_H

#include "rxGraphicsStub_global.h"
#include "RXGraphicCalls.h"

class RXGRAPHICSSTUBSHARED_EXPORT RxGraphicsStub {
public:
    RxGraphicsStub();
};

#endif // RXGRAPHICSSTUB_H
