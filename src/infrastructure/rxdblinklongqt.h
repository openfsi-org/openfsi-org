#ifndef RXDBLINKLONGQT_H
#define RXDBLINKLONGQT_H
#include "rxdblinkqt.h"

class RXDBlinkLongQt : public RXDBlinkQt
{
public:
    RXDBlinkLongQt();
    RXDBlinkLongQt(const QString &s);
    virtual ~RXDBlinkLongQt();
    std::string ScriptCommand() const;

    // operations on the persistent DB
        //extract-column returns a list of values for  header = head, for all record rows
        virtual int Extract_Column(const char*fname,const char*head,char***clist,int*Nn) ;
        // transfer from mirror to persistent
        virtual int Write_Line (MIRRORPTR mirrorSource) ;
        // transfer from  persistent DB to  Mirror DB
        virtual int read_and_post_one_row(const int row, const QString runName,MIRRORPTR mirrorDest) ;
        virtual int MakeUnique(const QString &header, QString &value);


private:
        QString m_runTable, m_DataTable;
};

#endif // RXDBLINKLONGQT_H
