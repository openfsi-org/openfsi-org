#ifndef RXMIRRORQT_H
#define RXMIRRORQT_H
#include <QObject>
#include <RXDBLink.h>
#include <QThread>
#include <QSqlDatabase>
#include <map>


class RXDBLinkThreadLibQT : public QThread
{
    Q_OBJECT
public:
    explicit RXDBLinkThreadLibQT(QObject *parent, const QString &s) ;
    ~RXDBLinkThreadLibQT();
 //   QMutex m_mtx;
protected:
    void run();
signals:
    void RXDBCommand(QObject *client, const QString &s);
protected slots:

private:
    QSqlDatabase m_dbP ;
    QString m_s;
    bool  m_PleaseEndPump;
};

class RXMirrorQt : public  QObject ,
       public RXMirrorDBI
{
   Q_OBJECT
public:
    RXMirrorQt();
    RXMirrorQt(QString &s);
    virtual ~RXMirrorQt();
    int Open();//const char*filename,const char*what ) ;
    int Open( const char*filename,const char*what ) { qFatal("dont use this method"); return 0;}
    int Close (); // the delete
    int StartTransaction ();
    int Commit();
    int FlushMirrorTable (); // partial delete
    int post_summary_value(const char*label,const char*value);
    int post_summary_value(const std::string &label,const std::string &value);
    std::string ScriptCommand()const;
    std::string GetFileName() const ;
    void SetLast(const QString h, const QString v);
    int PostFile( const QString &label,const QString &fname) ;


    //searches in the mirror for 'label'. returns value.
    int Extract_One_Text(const char *label,char*value,const int buflength) ;

    std::string Extract_Headers(const  std::string &what) ; //is 'input', 'output' or 'all'
    int Replace_Words_From_Summary(char **strptr, const int append) ;
    int RemoveAllWithPrefix(const std::string & prefix) ;     //remove-all-with-prefix operates on the mirror
    int  Apply_Summary_Values() ;     //  synch the app to the Mirror
    int Write_MirrorDB_As_CSV(char *filename) ; // only used for the statefile header
    virtual int Create_Filtered_Summary(MIRRORPTR snew,char *filter) ; //   posts from a mirror to a mirror  in write_ND_state
    // dump mirror to script format
    virtual int Write_Script (FILE *fp) ;

    QSqlDatabase m_db ;
signals:
      void SetLastSignal(const QString &what, const QString &val);
private slots:

private:

    class RXDBLinkThreadLibQT  *m_PumpThrd;// will hold an independent connection because it runs in another thread
};


#endif // RXMIRRORQT_H
