#include "StdAfx.h"
#include "stringutils.h"
#include "summary.h"
#include "RXDatabaseI.h"
#include "words.h"
#include "RelaxWorld.h"
#include "rxmirrorsimple.h"

RXMirrorSimple::RXMirrorSimple()
{
}


RXMirrorSimple:: ~RXMirrorSimple()
{

}

int RXMirrorSimple::Open(const char*filename,const char*what )
{
    return 1;
}

int RXMirrorSimple::Close ()
{
    return 1;
}
int RXMirrorSimple::StartTransaction () {return 0;}
int RXMirrorSimple::Commit() {return 0;}

int RXMirrorSimple:: FlushMirrorTable ()
{
    m_map.clear();
    return 1;
}

int  RXMirrorSimple::post_summary_value(const char*label,const char*value)
{
    m_map[std::string(label)] = std::string(value);
    return 1;
}

std::string  RXMirrorSimple::ScriptCommand() const
{
    return std::string("open_mirror :  internal");
}
std::string  RXMirrorSimple::GetFileName() const
{ return std::string("(internal)");}

void  RXMirrorSimple::SetLast(const QString h, const QString v){
    // std::cout<<" TODO: RXDBLinkMySQL::SetLast" <<endl;
}

int RXMirrorSimple:: post_summary_value(const std::string &label,const  std::string &value)
{
    return post_summary_value(label.c_str(),value.c_str());
}
int RXMirrorSimple:: PostFile( const QString &label,const QString &fname)
{
    return post_summary_value(qPrintable(label),qPrintable(fname));
}
//searches in the mirror for 'label'. returns value.
int  RXMirrorSimple::Extract_One_Text(const char *label,char*value,const int buflength)
{
    std::map<std::string,std::string>::iterator it;
    it=m_map.find(std::string(label));
    if(it==m_map.end())
        return 0;
    {
        strncpy(value,(*it).second.c_str() ,buflength);
        return 1;
    }

}

std::string  RXMirrorSimple::Extract_Headers(const string &what)   //is 'input', 'output' or 'all'
{
    assert(0); return 0;
}

int  RXMirrorSimple::Replace_Words_From_Summary(char **strptr, const int append)
{
    /* if $header is found in buf, replace it with text
 If 'append' is non-zero replace $AWA with $AWA=<value>
  or replace any string from $AWA to > (but not containing '$')  with $AWA=<value>
  */

    int   c=0 ;
    char buf[512], bnew[512], *cp, *a,*b, *d,*stmp;
    string what,val;

    string q ;
    if(!strptr) return 0;
    if(! *strptr) return 0;
    std::map<std::string,std::string>::iterator it, itend=m_map.end();
    for(it=m_map.begin(); it!=itend;++it)
    {
        what = it->first;
        val = it->second;

        sprintf(buf,"$%s",what.c_str());
        if(append) {
            sprintf(bnew,"%s=<%s>", buf, val.c_str());
            stmp=STRDUP(*strptr);	RXFREE(*strptr);
            cp = Replace_String(&stmp, buf,bnew);
            *strptr=stmp;
            if(cp) {// printf(" replace_String '%s'->'%s' in '%s' gave\n cp='%s'\n===\n", buf,new,str, cp);
                /* now strip $AWA=<21.936>=<22.157> to $AWA=<21.936> */
                cp += strlen(bnew);
                a = strchr(cp,'$'); b = strchr(cp,'>');
                if((b &&a &&(b < a)) || ( b && !a)) {
                    for( d = cp; d<=b; d++) *d = ' ';
                    PC_Strip_Leading_Chars(cp," ");
                }
                c++;
            }

        }
        else  {
            if(NULL !=Replace_String(strptr, buf,val.c_str()))
                c++;;
        }

    }
    return c;
}

//remove-all-with-prefix operates on the mirror
int  RXMirrorSimple::RemoveAllWithPrefix(const std::string & prefix)
{
    // this loop is very inefficient.
    int rc=0;
    string header;
    std::map<std::string,std::string>::iterator it, itend=m_map.end();
    bool stillGoing;
    do {
        stillGoing = false;
        for(it=m_map.begin(); it!=itend;++it)
        {
            header = it->first;
            if (header.find(prefix)!=std::string::npos ||  prefix.find(header)!= std::string::npos )
            {
                m_map.erase(it);
                rc++;
                stillGoing = true;
                break;
            }

        }
    }while (stillGoing);

    return rc;
}

//  synch the app to the Mirror
int RXMirrorSimple::Apply_Summary_Values()
{
    int rc=0;
    int  type, flag;;
    string header,text;
    std::map<std::string,std::string>::iterator it, itend=m_map.end();
    for(it=m_map.begin(); it!=itend;++it)
    {
        header = it->first;
        text = it->second;
        type   = RXMirrorDBI::Summary_Type(header.c_str(),&flag);
        Justapply_one_summary_item(header.c_str(),text.c_str(),  type, flag);

    }
    rc= g_World->Bring_All_Models_To_Date();

    return rc;
}
int RXMirrorSimple::Write_Script (FILE *fp)
{
    if(!fp) return 0;
    fprintf(fp,"\n!  (from internal mirror)\n");
    int rc=0;
    int  type, flag;;
    string header,text;
    std::map<std::string,std::string>::iterator it, itend=m_map.end();
    for(it=m_map.begin(); it!=itend;++it)
    {
        header = it->first;
        text = it->second;
        type   = RXMirrorDBI::Summary_Type(header.c_str(),&flag);
        if(!(flag &SUM_INPUT))
            continue;
        if(text.find("N/A") !=string::npos)
            continue;
        rc+= fprintf(fp,"apply: %s : %s\n",  header.c_str(),text.c_str()  );

    }
    return rc;
}

// create-filtered-  posts from a mirror to a mirror   - called in write_ND_state
int RXMirrorSimple::Create_Filtered_Summary(class RXMirrorDBI *snew,char *filter)
{
    int flag;
    int retVal=0;
    // should call this with the sd not the sd->sailsum and then check existence of the ent.
    // To prevent virussing of historic data.

    if(!snew)
        return 0;
    string header,text;
    std::map<std::string,std::string>::iterator it, itend=m_map.end();
    for(it=m_map.begin(); it!=itend;++it)
    {
        header = it->first;
        text = it->second;
        if(stristr(filter,header.c_str()) || stristr(header.c_str(),filter )) {
            RXMirrorDBI::Summary_Type(header.c_str(),&flag);
            if(!(flag &SUM_INPUT))
                continue;
            if( !strieq( text.c_str(),"N/A")){ // should also test that its a sum_input;
                snew->post_summary_value(header, text);
                retVal++;
            }
        }
    }

    return(retVal);
}

