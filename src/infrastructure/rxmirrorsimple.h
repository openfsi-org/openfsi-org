#ifndef RXMIRRORSIMPLE_H
#define RXMIRRORSIMPLE_H
// MirrorSimple is a minimal logger class.  It has no persistent DB
// nor external connection.  It just operates on a std::map.

#include <RXDBLink.h>

#include <map>
class RXMirrorSimple : public RXMirrorDBI
{
public:
    RXMirrorSimple();
    virtual ~RXMirrorSimple();
    int Open(const char*filename,const char*what ) ;
    int Close (); // the delete
    int StartTransaction ();
    int Commit();
    int FlushMirrorTable (); // partial delete
    int post_summary_value(const char*label,const char*value);
    int post_summary_value(const std::string &label,const std::string &value);
    std::string ScriptCommand()const;
    std::string GetFileName() const ;
    virtual void SetLast(const QString h, const QString v);

    int PostFile( const QString &label,const QString &fname) ;

    //searches in the mirror for 'label'. returns value.
    int Extract_One_Text(const char *label,char*value,const int buflength) ;

    std::string Extract_Headers(const string &what) ; //is 'input', 'output' or 'all'
    int Replace_Words_From_Summary(char **strptr, const int append) ;
    //remove-all-with-prefix operates on the mirror
    int RemoveAllWithPrefix(const std::string & prefix) ;

    //  synch the app to the Mirror
    int  Apply_Summary_Values() ;

    int Write_MirrorDB_As_CSV(char *filename) ; // only used for the statefile header

    // create-filtered-  posts from a mirror to a mirror   - called in write_ND_state
    virtual int Create_Filtered_Summary(MIRRORPTR snew,char *filter) ;

    // dump mirror to script format - useful for snapshot
    virtual int Write_Script (FILE *fp);

private:
    std::map<std::string,std::string> m_map;
};

#endif // RXMIRRORSIMPLE_H
