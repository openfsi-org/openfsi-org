#include "StdAfx.h"
#include <QtCore>
#include <QSqlQuery>
#include <QSqlField>
#include <QtSql>
#include <QDesktopServices>
#include <QDebug>
#include "akmutil.h"
#include "stringutils.h"
#include "summary.h"
#include "RXDatabaseI.h"
#include "words.h"
#include "global_declarations.h"
#include "RelaxWorld.h"
#include "rxmirrorqt.h"
#include "rxdblinkqt.h"

RXDBlinkQt::RXDBlinkQt()
{
}
RXDBlinkQt::RXDBlinkQt(const QString &s)
{
    bool ok=false;
    QStringList w = s.split("$");
    if(w.size()>6 )
    {
        w[0]= w[0].toUpper();
        if(!w[0].startsWith("Q"))  w[0].prepend("Q");
        while(QSqlDatabase::contains(w[6]))
            w[6]+='l';
        m_db = QSqlDatabase::addDatabase(w[0],w[6]);
        m_db.setHostName(w[1]);
        m_db.setDatabaseName(w[2]);
        m_table = w[3];
        m_db.setUserName(w[4]);
        m_db.setPassword(w[5]);
        ok = m_db.open();
    }
    if(!ok) {
        cout<<"cannot open DB with command "<<qPrintable(s)<<endl;
        cout<<"expecting something like 'MYSQL$localhost$ofsidb$tablename$username$password$something' "<<endl;
    }
}
std::string  RXDBlinkQt::ScriptCommand() const
{
    if(m_DBtype&RXDB_INPUT)
        return string("open_input: qt: ") +this->GetFileName();

    if(m_DBtype&RXDB_OUTPUT)
        return string("open_log:qt: ") +this->GetFileName();
    assert(0);
    return this->GetFileName();
}
std::string  RXDBlinkQt::GetFileName() const
{
    QString rv = m_db.driverName(); rv.remove(0,1); rv.toUpper();
    rv+= "$" +m_db.hostName()+"$";
    rv+= m_db.databaseName()+"$"  ;
    rv+= this->m_table +"$"  ;
    rv+= m_db.userName()+"$"+m_db.password()+"$";
    rv+=m_db.connectionName();
    return rv.toStdString()   ;
}
RXDBlinkQt:: ~RXDBlinkQt()
{
    Close();
}
/// setlast is called by apply_...  so that the DB doesnt then
/// think that it has to do another update on the same value
void RXDBlinkQt::SetLast(const QString h, const QString v)
{
    // TODO: pass v to m_lastbuffer[h] in the pumper thread
}

int RXDBlinkQt::Open( const char*p_fileName,const char*what)//strictly openMirror
{
    return 1;
}

int RXDBlinkQt::Close ()
{
    if(!m_db.isOpen()) return 0;
    m_db.close();
    QSqlDatabase::removeDatabase( m_db.connectionName());
    return 1;
}
// operations on the persistent DB

//extract-column returns a list of values for  header = head, for all record rows
int RXDBlinkQt::Extract_Column(const char*fname,const char*head,char***clist,int*Nn)
{
    int i=0 ;
    assert(m_DBtype&RXDB_INPUT );

    QString qs = QString("SELECT " )+QString(head)
            +QString(" FROM ") + this->m_table;
    // +QString(" ORDER BY ") +QString(head) ;
    QString header ;

    QSqlQuery q(qs,m_db);
    if(!q.isActive())
        qDebug()<<  "( Extract_Column) "<< q.lastError().text();
    QSqlRecord rec = q.record();
    qDebug() << "Number of columns: " << rec.count();
    qDebug() << "Number of rows: " << q.size();
    *Nn = q.size(); //int nc = r[0].size ();
    if(*clist) RXFREE (*clist) ; *clist=(char**) MALLOC((*Nn)*sizeof(void*));
    while (q.next())
    {
        header = q.value(0).toString();
        (*clist)[i++]=STRDUP(qPrintable(header ));
    }

    return (*Nn);
}
// SELECT Run_Name FROM `ofsidb`.`table`;
//  modify 'value' to ensure that it differs from all existing records
int  RXDBlinkQt::MakeUnique(const QString &header, QString &value)
{
    int rc=0;
   // qDebug()<<  "(MakeUnique) "<< value;

    QString qs = QString("SELECT ")+header
            +QString(" FROM ") + this->m_table;

    QSqlQuery q(qs,m_db);
    if(!q.isActive())
        qDebug()<<  "(MakeUnique) "<< q.lastError().text();
    else {
        int DupFound=0;
        do {
            q.seek(-1);
            DupFound=0;
            while (q.next())
            {
                QString qexist = q.value(0).toString();
                if( 0==value .compare(qexist,Qt::CaseInsensitive) )
                {
                    DupFound++; rc++;
                    break;
                }
            }
            if(!DupFound)
                break;
            Increment_String(value);
        } while (DupFound);
    }
  //  qDebug()<<  "           >>>>>>>>>> "<< value;
    return rc;
}

#define WHATSEP "` , `"
#define VALSEP "' , '"  //note DIFFERENT quotes
#define WHATSEPLENGTH 5
#define VALSEPLENGTH 5


// transfer from mirror to persistent
int RXDBlinkQt::Write_Line (MIRRORPTR  pmirrorSource)
{
    /* Logic
1) we can cold-start a table from here;
 so if the table doesnt exist, we create it
 The minimal table has a unique, auto incrementing ID
*/
    /*
  thESE work in command-line
 insert into output SET leeway=4,course=092, Run_Name="eric";
  insert into output(VMC) VALUES(7.1);
  INSERT INTO OUTPUT(RUN_NAME)VALUES('GOATINBOOTS');
*/

    class RXMirrorQt *mirrorSource =dynamic_cast<class RXMirrorQt*>(pmirrorSource);
    if(!mirrorSource)
    {
        QStringList errmsg; errmsg<<QString("open_log and open_mirror commands are incompatible");
        errmsg << QString::fromStdString(this->ScriptCommand());
        if(pmirrorSource )
            errmsg<< QString::fromStdString (pmirrorSource->ScriptCommand());
        else
            errmsg<<"(unknown mirror)"   ;
        g_World->OutputToClient(errmsg.join("\n"),101);
        return 0;

    }
    assert(m_DBtype&RXDB_OUTPUT);
    int rc=0; unsigned int err;

    QString q, qs,what, swhat,sval,qr_type,qr_flag,header,val,bd;
    int l_type,l_flag;
    bool tableexists=false;
    //
    //  check for existance of the table
    ///
    qs= QString("SHOW TABLES LIKE '%1'") .arg(this->m_table);
    QSqlQuery qq(qs,m_db);
    if(!qq.isActive())
        qDebug()<<  "( 2 ) "<< qq.lastError().text();
    while (qq.next())
    {
        what = qq.value(0).toString() ;
        if(what == this->m_table)
        {tableexists=true; break;}
    }
    qq.finish();
    if(!tableexists) {
        qs=   QString("CREATE TABLE ")  + this->m_table
                +QString(" ( id INT UNSIGNED NOT NULL AUTO_INCREMENT,PRIMARY KEY(id)) ");
        QSqlQuery qq(qs ,m_db);    qq.finish();
    }
    //
    //    read the contents of the Mirror table
    //
    // r1 will be the contents of the mirror table
    qs= QString("SELECT * FROM ") + QString(RXMIRRORTABLE) ;// WRONG. select header,text,type or something
    QSqlQuery qqmirror(qs,mirrorSource->m_db);

    // check that the columns exist in the output table
    // and create them if they do not

    while (qqmirror.next())
    {
        header = qqmirror.value(0).toString();
        qr_type =qqmirror.value(2).toString();
        l_type = qr_type.toInt();
        l_flag =qqmirror.value(3).toInt();
        qs="SELECT `"+header+"` FROM "+this->m_table;
        QSqlQuery qq(qs ,m_db);
        QSqlError qse = qq.lastError();
        err=0;
        if(qse.type() !=QSqlError::NoError) { //QSqlError::TransactionError?
            err = qse.number();
        }
        qq.finish();
        if(err==1054) {
            if (l_flag &SUM_BLOB ) // was type till april 2013
                q = "ALTER TABLE `"+this->m_table +"` ADD `"+ header+"` MEDIUMBLOB DEFAULT NULL";
            else
                q = "ALTER TABLE `"+this->m_table +"` ADD `"+header+"` VARCHAR(64) DEFAULT NULL";
            //ALTER TABLE `sailpack` ADD `$boat$string$cable_d1p-cable_d1p_cr_50$trim` VARCHAR(256) DEFAULT NULL
            QSqlQuery qqq(q ,m_db);
            qse = qqq.lastError();
            err=0;
            if(qse.type() !=QSqlError::NoError) { //QSqlError::TransactionError?
                err = qse.number();
                qDebug()<< "err="<<err<< " while executing "<<q;
                qDebug()<< qse.text();
            }
        }
    }// cycle over columns in mirror

    // reset the mirror query so we can traverse it again.
    //
    qqmirror.seek(-1);

    // traverse it a second time, to create a new   line in the output table
    // HOWever. doing this via text gives problems.  Can we do it via a transaction
    //   or via bound values???

    //    we build  up a string of type

    int count=0;

    swhat = " (`";
    sval = " VALUES( '";//note DIFFERENT quotes
    while (qqmirror.next())
    {
        header = qqmirror.value(0).toString();
        val = qqmirror.value(1).toString();
        qr_type =qqmirror.value(2).toString();
        l_type = qr_type.toInt();
        qr_flag= qqmirror.value(3).toString();
        l_flag =qqmirror.value(3).toInt();
        if (l_flag &SUM_BLOB ) {
            bd = this->FileToBlobData(val);
            swhat += header;
            sval += bd;
        }
        else {
            swhat += header;
            sval += val;
        }

        count++;
        swhat+= WHATSEP;
        sval +=VALSEP;
    }

    // swhat may have a " , ``" to remove
    //sval a ", ''"
    if(swhat.endsWith( WHATSEP)) swhat.chop(WHATSEPLENGTH);
    if(sval.endsWith( VALSEP)) sval.chop(VALSEPLENGTH);
    swhat += "` ) "; sval+= "' ) ";//note DIFFERENT quotes

    if(count) {
        q="INSERT INTO "  + this->m_table + swhat + sval; // try to insert the whole record
        QSqlQuery query(m_db);
        rc=  query.exec(q);
        if(!rc) {

            QSqlError sqle = query.lastError () ;
            qDebug()<<sqle.text();
            QStringList dbg = q.split(",");
            qDebug()<<" command was\n"<<dbg<<"\nend";
            QFile erfile("rxerror.csv");
            if (erfile.open(QIODevice::WriteOnly | QIODevice::Text)){
                QTextStream out(&erfile);
                QStringList::const_iterator constIterator;
                for (constIterator = dbg.constBegin(); constIterator != dbg.constEnd();
                     ++constIterator)
                    out << (*constIterator).toLocal8Bit().constData() << endl;
                erfile.close();
            }
        }
    }
    rc=1; // sb  the error on r2
    return rc;
}
// transfer from  persistent DB to  Mirror DB
int RXDBlinkQt::read_and_post_one_row(const int row, const QString runName,MIRRORPTR mirrorDest)
{
    int rc=0;
    assert(m_DBtype&RXDB_INPUT );
    QString q,header;
    QString qbuf,ftype ;
    q = QString("SELECT * FROM ") + this->m_table
            +QString(" WHERE Run_Name = '") + runName +QString("'") ;
    QSqlQuery rq(q,m_db);
    if(!rq.isActive())
        qDebug()<<  "(3) "<< rq.lastError().text();
    QSqlRecord rec = rq.record(); int reccount = rec.count();
    rq.next();

    for(int i = 0; i < reccount;i++)
    {
        header = rec.fieldName(i);

        // find out what sort of object
        QString  qd = QString("DESCRIBE ") + this->m_table +QString(" '") + header +QString("'") ;
        QSqlQuery rqd(qd,m_db);       rqd.next();

        qbuf = rqd.value(0).toString(); //id, Run_Name, &c
        ftype = rqd.value(1).toString();// "int(10) unsigned"  //"varchar(256)"//"varchar(64)" //"mediumblob"

        if(ftype=="mediumblob") //decode the data , write it to a file. we try to concoct the filename from Field
        {

            if(header.contains("download",Qt::CaseInsensitive)) {
                qDebug()<< " qvariant::type  is "<< rq.value(i).type();
                qDebug()<< " qvariant::typeName is "<< rq.value(i).typeName();
                QByteArray val;
                val = rq.value(i).toByteArray();
                QString filename  ;
                int i = header.lastIndexOf("$");
                if(i>=0)
                    filename =header.mid(i+1);
                else filename = "download.txt";

                qbuf= BlobDataToFile(val, filename);
                if(mirrorDest){rc++; mirrorDest->post_summary_value(header,qbuf);}  // this isnt quite the same as RXLogFIle
                if(header.contains("open", Qt::CaseInsensitive))
                    QDesktopServices::openUrl (   QUrl(filename,QUrl::TolerantMode )  );
            }
            else {
                QByteArray val;
                val = rq.value(i).toByteArray();
                if(mirrorDest){rc++; mirrorDest->post_summary_value(header,val);}  // this isnt quite the same as RXLogFIle
            }
        }
        else // take it as a varChar
        {
            QByteArray val ;
            val = rq.value(i).toByteArray();
            if(!mirrorDest)
                qDebug()<<"posting (but no mirror destination) "<<header  <<" val="<<val;
            else if(header !="id")
            {
                mirrorDest->post_summary_value(header,val);  // this isnt quite the same as RXLogFIle
                rc++;
            }
        }
    }
    return rc;
}
// GetBlobData returns the text that we want to store in the database
// if its a text file, we just return its contents
// else we return the base64 of the its contents
QByteArray RXDBlinkQt::FileToBlobData(const QString &fname) /// label is a filename
{
    QByteArray theFileContents;
    QFile thefile ( fname);
    if(thefile.open(QIODevice::ReadOnly))
    { theFileContents=thefile.readAll(); thefile.close();}
    else
        g_World->OutputToClient(QString("cannot open file for blobbing: ")+fname,101);

    if(fname.endsWith(".txt"))
        return theFileContents;
    return theFileContents.toBase64() ;
}
QString  RXDBlinkQt::BlobDataToFile(const QByteArray &data,  QString &filename)
{
    //use QByteArray::fromBase64  but note that we cannot use the <<operator to
    // write it because Qt makes it too smart.  Use the low-level writeRawData instead

    if(filename.endsWith(".txt"))
    {
        QFile f(filename);
        f.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out(&f);
        out <<data; f.close();
    }
    else
    {
        if(0){ //debug - use the system base64 not qt
            QFile f( "rawout.b64" );
            if (!f.open(QIODevice::WriteOnly | QIODevice::Text))
                return filename ;
            QTextStream out(&f);
            out <<data; f.close();
            QString syscommand=QString ( "base64 -d  rawout.b64  > sys_%1").arg(filename);
            system(qPrintable(syscommand));
        } // debug

        QByteArray d = QByteArray::fromBase64(data  );
        QFile f(filename);
        if (!f.open(QIODevice::WriteOnly ))
        {  g_World->OutputToClient(QString("cannot open file for deblobbing: ")+filename,101); return filename ;}
        QDataStream out(&f);
        out.writeRawData(d.data(),d.size() );
        f.close();
    }
    return filename;
}
