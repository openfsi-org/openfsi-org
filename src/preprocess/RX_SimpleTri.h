#pragma once
#include "opennurbs.h"

#include "RXSitePt.h"

class RX_TriBase
{
//	RX_TriBase(void);
//	virtual ~RX_TriBase(void);  

public:
	ON_3dVector baseVector(void) const;
	ON_3dVector  adjVector(void) const;
	RXSitePt *Centroid( RXSitePt *q) const;
	double Area(void) const; // uses the Vertices method.
	ON_Xform Trigraph( const double ang=0.0);


	RXSTRING TellMeAbout() const;
//protected:
	virtual int Vertices(const RXSitePt *v[4])const =0;//  derived classes should override this

};

class RX_SimpleTri: public RX_TriBase
{
public:
	RX_SimpleTri(void);
	virtual ~RX_SimpleTri(void);

        virtual int Vertices(const RXSitePt *v[4]) const;//  derived classes should override this

	RXSTRING TellMeAbout() const;

	int SetPt(const int k,  RXSitePt p) { assert(k<3); pts[k]=p; return 1;}
private: 
	RXSitePt pts[3];

private:

};
