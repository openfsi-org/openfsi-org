#include "StdAfx.h"
#include "TensylProp.h"

CTensylProp::CTensylProp(void)
: m_prop_no(0)
, m_ea(0)
, m_SelfWeight(0)
, m_RenderSize(0)
{
}
CTensylProp::CTensylProp(int p_p,double p_e,double p_sw,double p_rs)
: m_prop_no(p_p)
, m_ea(p_e)
, m_SelfWeight(p_sw)
, m_RenderSize(p_rs)
{
}

CTensylProp::~CTensylProp(void)
{
}
int CTensylProp::Print(FILE *fp,const double scale){
//  'Prop_No, EA, SelfWeight, RenderSize 
//       Type Description
//       Size Description
return fprintf(fp," %d %f %f %f\n%s\n%s\n",
		m_prop_no,m_ea*scale,
		m_SelfWeight*scale,
		m_RenderSize*sqrt(scale),
		s1.Array(),s2.Array());

}