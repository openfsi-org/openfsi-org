// layernode.h: interface for the layernode class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LAYERNODE_H__2DA90CD7_4BF9_4ED2_B8DD_18A849CF27E6__INCLUDED_)
#define AFX_LAYERNODE_H__2DA90CD7_4BF9_4ED2_B8DD_18A849CF27E6__INCLUDED_

#include "opennurbs.h"

#include "layerobject.h"
#include "layerangle.h"
#include "layerlink.h"
#include "opennurbs_point.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class layernodestack   : public layerobject 
{
public:
	void SetWidthFactor( const double t);
	double GetWidthFactor(){ return m_WidthFactor;}
	void SetModulusFactor( const double t);
	double GetModulusFactor(){ return m_ModulusFactor;}
	char * Deserialize(char *p_s);// Fills out the object from p_s. returns the ptr to the next char
	ON_String Serialize(); // allocates and returns a character array image of the object
//	int Print(FILE *fp); doesnt seem to be implemented
	layernodestack();
	virtual ~layernodestack();

	layerangle m_Angle;
protected:
	double m_ModulusFactor; // 1 for a single tape 36 yarns
	double m_WidthFactor; // normally for a single ply, up to 2 for dark blue.
private:

};


//EXTERN_C int  CompareIncreasing( layerlink const *a,   layerlink const *b);
class layernode   :public layerobject
{
public:
	char * Deserialize(char *p_s);// Fills out the object from p_s. returns the ptr to the next char
	ON_String  Serialize(); // allocates and returns a character array image of the object
	int AbsorbNode(layernode *pn);
	int SwapOutLinks();
	double DistSq(layernode *n2);
	int BoundarySeedSpecialCriterion(const int p_level);
	int HasStackAtLevel(const int level);
	layernode(layertst* p_p);
	layernode();
	~layernode();
	void Init();

	int Sort_Links();
	HC_KEY DrawHoops(const double &dl);


	int Print(FILE *fp, int p_flag=0) const;

	int SW_FindNearestStackItem(layerangle p_a, int p_flag=0);
	int AW_Mark_Interesting_Levels();
	int AW_Improve_Node(); // returns the number of swaps it made to the node
	int ClearDaDu();
	int CalcNodalDaDu(const int level,	ON_2dVector*result );
	int IsConnectedTo(const layernode *pn) const;
	int SetPoleStack();
	
	double m_u;
	double m_v;
	int m_r;
	int m_c;
	double m_NpliesExact;			// this is tr(DD)/tr(qply)
	double m_NpliesRounded;			// this is used for contouring. should be based on m_NpliesExact but maybe round up or down
	layerangle m_PivotAngle;

	ON_ClassArray<layernodestack> m_stack;
	double m_stackerror;
	ON_SimpleArray< layerlink *>  m_link;

protected:


	int AW_FindBestSwapAndDoIt(const int pFlag);
	ON_ClassArray<ON_2dVector> m_dadu;

private:

	int Cruciform(const layernode * nlow,const layernode * nn,const layernode * nhi);

	int Colinear(layernode*n1,layernode*n2, double tol = 1e-6);



	int m_nswaps;
	double AW_calculateMaxError(const int pFlag);
};

#endif // !defined(AFX_LAYERNODE_H__2DA90CD7_4BF9_4ED2_B8DD_18A849CF27E6__INCLUDED_)
