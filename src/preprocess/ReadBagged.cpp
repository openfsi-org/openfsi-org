/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
  *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

/* NOTE: This whole file NEEDS REWRITING !!!!!!!!!!!!!!!!!! */
/* AKM 11 Aug 1994 */

#include "StdAfx.h"
#include "RXSail.h"
#include "RXEntity.h"
#include "files.h"
#include "stringutils.h"

#include "akmutil.h" // for validfile
#include "global_declarations.h"

#include "ReadBagged.h"


int Resolve_Input_Card(RXEntity_p e)
{
    /* Input card is of type
             0    1
    INPUT:sailname:fileIN  ! comment  fileIn may be a relative or an absolute filepath*/
QString header;
    int i;
    int retVal=0;   /* failure */
    QString qline = e->GetLine();
    QStringList wds= rxParseLine(qline,RXENTITYSEPREGEXP );
    if(wds.size()<3)  wds<<"*.script";
    QString & qname=  wds[1];
    QString &qfname = wds[2];

    qfname = e->Esail->AbsolutePathName( qfname);
    e->Esail->SetFileIN(qfname);
    if(validfile(qfname ) != OK_TO_READ) {
        header= "Choose .script file: "+ wds[1];
       qfname = getfilename(NULL,qfname,header,g_currentDir,PC_FILE_READ);
        if(!qfname.isEmpty()) {
            printf("using '%s' as script file\n",qPrintable(qfname));
            e->Esail->SetFileIN(qPrintable(qfname));
        }
        else
            return 0;
    } // validfile
    qfname = e->Esail->RelativeFileName(qfname);
    qline = wds.join(RXENTITYSEPS);
    e->edited = !(qline ==e->GetLine());
    e->SetLine(qPrintable(qline) );

    e->PC_Finish_Entity("infile",qPrintable(wds[0]),NULL,(long) 0,NULL,NULL,e->GetLine());

    retVal = 1;

    if(retVal)  {
        e->Needs_Resolving= 0;
        e->Needs_Finishing = 0;
        e->SetNeedsComputing(0);
    }
    return(retVal);

}

