#include "StdAfx.h"
#include "RXRelationDefs.h"
#include "RXEntityDefault.h"
#include "RX_FETri3.h"
#include "RXSail.h"
#include "RXLayer.h"
#include "griddefs.h"
#include "etypes.h"
#include "fielddef.h"
#include "rxpshell.h"

rxpshell::rxpshell()
{
}
rxpshell:: rxpshell(SAIL *s):
    RXPanel(s)
{

}
rxpshell::~rxpshell()
{
}
int rxpshell::BuildTriMaterials(){
    // the te should already have mx corresponding to the pshell's material

    int rc=0;
    class RX_FETri3* te;
#pragma omp critical (getfillist)
    {
        this->GetFilamentList();
    }
    this-> CollectFieldLayers() ;
    int ntris = m_FEtris.size();
    for(unsigned int i=0;i<ntris;i++) {
        te = m_FEtris[i];

        te->m_TriArea = te->Area();
        double checkangle = BuildOneTriMaterial(i);
        if(fabs(checkangle-te->RefAngle()) > ON_SQRT_EPSILON ){
            cout<<"trielement angle check imprecision:  "<<checkangle <<" isnt "<<te->RefAngle()<<endl;
            cout <<"Probable cause : panel '"<<this->name()<<"' has no materials defined"<<endl;
        }
        assert( te->IsInDatabase()); // so we dont need the AddFtri3
        te->SetProperties();
    }

    return rc;
}
double  rxpshell::BuildOneTriMaterial(const int index) { //  UGLY but it works!
    /*
the ugly thing about this is that the element itslef is passed to the Mfunc.
However most of the Mfuncs ignore it.
Compute_Filament_Field  reads its  area,  m_refangle and vertices via Tri_Vertices.
and  WRITEs the filaments to its linklist 'fils'
*/
    class RX_FETri3 *p_fte = m_FEtris[index ];  // the material properties are collected in it.

    set<RXObject*>::iterator it;
    set<RXObject*> lyrs= this->FindInMap(RXO_LAYER, RXO_ANYINDEX);

    class RXSitePt l_qq;
    MatValType mx[MXLENGTH];
    double this_mat_to_matref_angle,matref_to_base_angle, this_mat_to_Element_Base;

    ON_3dVector  matrefVector,vdash  ;
    MatDataType xv; xv.resize(20); //float xv[20]; // Our trigraph stuff is inaccurate. Use double precision ON_XForm
    RXEntity_p  matEnt;
    class RXLayer *fab;

    struct PC_MATERIAL   *TheMaterial;
    double thick; /* thickness of current layer */
    double prestress[3];
    struct PC_FIELD *l_field ;

    this_mat_to_matref_angle=0;matref_to_base_angle= p_fte->RefAngle();

    p_fte->creaseable = 1; /* start off creasable */

    memset(p_fte->prestress,0,sizeof(double)*3); /* set the matrix attached to Fortran tri to 0's */
    thick=0;
    fab=NULL;
    TheMaterial=NULL;
    /* Get the angle between the triangle base and this layer material x-axis*/

    ON_Xform trigraph1= p_fte->Trigraph ();
    vdash = ON_3dVector(cos(p_fte->RefAngle() ),-sin(p_fte->RefAngle() ) ,0);
    matrefVector= trigraph1.Inverse() * vdash; // matrefvector is in  red/black space, same as l_t;
    // vdash is the laminate ref vector in element coordinates.

    for(it=lyrs.begin();it!=lyrs.end();++it) {// Now we have ref fab, loop thru the other layers
        fab =  dynamic_cast<class RXLayer* >(*it);
        matEnt = fab->matptr;
        switch ( matEnt->TYPE) {
        case FABRIC:
            TheMaterial = (struct PC_MATERIAL *) matEnt->dataptr;
            break;
        case FIELD:
            l_field = (PC_FIELD *)matEnt->dataptr;
            if(!( l_field->flag & DOVE_FIELD))
                TheMaterial = 0;//(struct PC_MATERIAL *) l_field->mat->dataptr;
            else
                TheMaterial=0; // we won't get color but until we have inheritance its a pain otherwise
            if(( l_field->flag & FILAMENT_FIELD))
            {
                assert( fab->Use_Black_Space  );
                xv[0]=  matrefVector.x ;  //  blackspace coordinates
                xv[1]=  matrefVector.y ;
                xv[2] = matrefVector.z ;
            }
            break;
        case PCE_DOVE: // never happens
            assert(0);
            break;
        default:
            assert(0);
        };
        p_fte->Centroid(&l_qq);// coord space may have changed

        memset(prestress,0,sizeof(double)*3);
  //     matok=
               (fab->matstruct.function)(&thick,mx,&l_qq,p_fte,fab,xv,prestress);		// mx is Dmatrix at centroid. its ref dir (redorblack) is xv.
  //     assert(matok);
        if(TheMaterial &&(!TheMaterial->creaseable)) {		// this lets a field be non-creasable eg for a corner patch
            if(thick > 0.0)
                p_fte->creaseable = 0;			// cout<< " if any part NOT creaseable turn whole triangle  OFF \n"<<endl;
        }

        ON_Xform trigraph= p_fte->Trigraph ();
        vdash = trigraph*ON_3dVector(xv[0],xv[1],xv[2]); // in the same space as the fab.

        this_mat_to_Element_Base = (-(atan2(vdash.y,vdash.x)));	// angle from this material to element base.
        this_mat_to_matref_angle = (matref_to_base_angle - this_mat_to_Element_Base);// angle from this material to ref mat

        rotate_material(mx,-this_mat_to_matref_angle);

        for(int ii=0;ii<9;ii++)    p_fte->mx[ii] += mx[ii]*thick;

        rotate_stress(prestress,-this_mat_to_matref_angle);

        for(int ii=0;ii<3;ii++) p_fte->prestress[ii] += prestress[ii]*thick;
    }

    p_fte->m_laminae =0;// lam_list;
    assert(fabs(p_fte->RefAngle() - matref_to_base_angle)< ON_EPSILON); // was a redundant assignment
    p_fte->laycnt = lyrs.size();

    if(!p_fte->m_colour )
        p_fte->m_colour = STRDUP("magenta");

    return(matref_to_base_angle);
} // buildonetriMaterial
