/* date:  Friday 12 November 1993 PH version

file: batdefs.h
 March 1996. change on offset declaration.
 UV limits added to PC_PANSAIL. name changed to PCN PANSAIL to force re-compilation
Declarations to go with batpatch.c. temporary. to be added to griddefs.h */

#ifndef _HDR_BATDEFS_
#define _HDR_BATDEFS_ 1

#include <vector>
#include <string>
#include "vectors.h"
#include "RXOffset.h"

struct PC_BOUNDARY{
	std::map<int,Site *> m_corners;
    std::map<int,sc_ptr> m_edges;
	std::map<int,string> m_names;
    int nc,ne;
};  //struct PC_BOUNDARY

//struct PC_LUFF_EXTENSION {
//	float chord[10];
//	int nc;
//	double angle;
//};  //struct PC_LUFF_EXTENSION


struct PCN_PANSAIL{
  RXEntity_p leading;
  RXEntity_p trailing;
  char *basename;
  char *leechWakeString;
  char *footWakeString;
  int lrev,trev;
  float U,V,Xf,Yf,x,y,z;  /* last values */
  double  Lower_V, Upper_V;
  double  Lower_U, Upper_U;	   /* so v=0 is at Lower_V from bottom, etc
  		   		so we can miss out a wrinkled foot-round */ 
  struct PC_LUFF_EXTENSION *le;
	void *elast; // cast to edge*

};  //struct PCN_PANSAIL

struct PC_PATCH {

	float d;            /* depth.  May be zero */

	VECTOR *p;  /* must be malloced*/
	int c;
	int m_type;
	int inserted;
	int N_Angles;
	struct IMPOSED_ANGLE *a;
        RXEntity_p basecurveptr;
	int partial;

	/* end<n>type will be <(*end<n>ptr).type> */        /* site, seam,curve */

	RXEntity_p end1ptr;
	

	class RXOffset *End1dist;
	class RXOffset *End2dist;

	
	int end1sign;
	RXEntity_p end2ptr;  /* ptr to RXEntity of end2 {SITE|seam|curve} */
	int end2sign;

	double m_arc;      /* current arclength */
	double patch_delta_arc;  /* imposed length change on the batten in this pocket */
	double strt_A;   /* an estimate of the initial slope of the IA curve */

	double end1angle; /* ready for export */
	double end2angle;

	Site *e[2]; /* the SITES or relsites at the ends. */

   	RXEntity_p mat;
	RXEntity_p m_corner;
	RXEntity_p cc;
} ;  //struct PC_PATCH

struct PC_INTERSECTION {
   sc_ptr seam;   /* the seam */
   Site* site;   /* the site */
   double s1,s2;
   VECTOR v;
	int i;		 		/* a polyline knot index */
	int flag;			/* 1 for a cross, 2 for a parallel */
}	 ;  // struct PC_INTERSECTION 
   
 
struct PC_BATTEN {
	VECTOR *p;
	int c;
//	double length,force,EA;
	double k[4];
	float*a;
	} ;  //struct PC_BATTEN 

#endif  //#ifndef _HDR_BATDEFS_
