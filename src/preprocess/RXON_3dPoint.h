#ifndef _RXON_3DPOINT_H_
#define _RXON_3DPOINT_H_
#include "opennurbs.h"
#include <map>
class RXON_3dPoint :
        public ON_3dPoint
{
public:
    RXON_3dPoint(void);
    RXON_3dPoint(ON_3dPoint p);
public:
    ~RXON_3dPoint(void);
public:
    bool MapToSurface(	const ON_Surface * p_From,
                        const ON_Surface * p_To,
                        ON_3dPoint &       p_point) const;

    ON_3dPoint Flow(const ON_Curve * p_c1, const ON_Curve * p_c2);
public:
    int Print(FILE * fp);
};
ON_PolylineCurve* CurveToPolyline(const ON_Curve *c, const double tol, int &err);
int RXONCurveDivide(const ON_Curve *p,	std::map<double,ON_3dPoint>&m,
                    std::map<double,ON_3dPoint>::iterator &lo,
                    std::map<double,ON_3dPoint>::iterator &hi,const double tolsq);

#endif
