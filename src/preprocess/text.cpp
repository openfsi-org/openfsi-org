/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
  Sept 97  if(data) RXFREE(data) To save an annoying message
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


/* routine to read in current file contents 
 * into a buffer.
 */
 #include "StdAfx.h"

 //#include "debug.h"

#ifdef AMGUI_2005
#include "AMGMacro.h"
#endif


#ifdef _X
	//#include "Global.h"
	#include "Gflags.h"
	#include "opentext.h"	
	#include "global_declarations.h"
#else
	#define FCLOSE fclose
	#define RXFOPEN fopen
#endif


#include "text.h"


 
char *readdata(const char *filename)
{
FILE *fp;
long end;
char *data;


fp = RXFOPEN(filename,"r");
if(!fp) 
	return(NULL);
fseek(fp,0L,SEEK_END);
end = ftell(fp);
if(end < 0) {
	return(NULL);
}

fseek(fp,0L,SEEK_SET);

data = (char *) MALLOC((end+100)*sizeof(char));
fread(data,sizeof(char),end,fp);
data[end] = '\0';
FCLOSE(fp);	   
#ifdef WIN32
// in MSW (vista) there is a binary tail on the file.
int kk;
char *lp = &(data[end]); lp--;
while (lp>data) {
//	if(*lp<=0)
	kk = (int) *lp;
	if(! __isascii(*lp))
		*lp=0;
	else
		break;
	lp--;
}
   #endif
return(data);
}
 
char *writedata(char *filename,char *data)
{
FILE *fp;
long end;



fp = RXFOPEN(filename,"w");
if(!fp) 
	return(NULL);

end = strlen(data);
if(end < 0) {
	return(NULL);
}

fwrite(data,sizeof(char),end,fp);
FCLOSE(fp);
return(data);
}
#ifdef _X

int settext(char *data,Widget textwidget)
{
	int  l_ac=0;
	Arg al[64];

XtSetArg(al[l_ac],XmNvalue,data);l_ac++;
XtSetValues(textwidget,al,l_ac);


if(data) RXFREE(data); data=NULL; /* Peter I think this is a leak */
// july 2007 its bad style to do the free here. Should do it in the calling function

return(0);
}


int initTextWindows()
{
g_TextWindows[0] = g_PansailTextWindow = OpenText("Empty File","PANSAIL results");	
g_TextWindows[1] = g_RelaxTextWindow = OpenText("Empty File","Shape Data");	
	
g_TextFiles[0] = g_PansailTextFileName;
g_TextFiles[1] = g_RelaxTextFileName;
#ifdef _DEBUG
	g_TextWindows[2] = g_DebugTextWindow = OpenText("Empty File","Debug Display");
	g_TextFiles[2] = g_DebugTextFileName;
#endif
updateAllTextWindows();
return(0);
}

int updateAllTextWindows()
{
int i;
for(i=0;i<MAX_TEXT_WINDOWS;i++)
	settext(readdata((char *) g_TextFiles[i]),g_TextWindows[i]);
return(0);
}
#endif
int updateOneTextWindow(int which)
{
#ifndef _X
        // cout<< "updateOneTextWindow needs X"<<endl;
#else
   switch(which) {
	case GLOBAL_DO_PANSAIL_TEXT:
		settext(readdata((char *) g_PansailTextFileName),g_PansailTextWindow);
	break;
	case GLOBAL_DO_RELAX_TEXT:
		settext(readdata((char *) g_RelaxTextFileName),g_RelaxTextWindow);
	break;
	case GLOBAL_DO_DEBUG_TEXT:
#ifdef _DEBUG
		settext(readdata((char *) g_DebugTextFileName),g_DebugTextWindow);
#endif
	break;
	
	default:	
		return(1); /* bad argumant */
	
   }

#endif
return(0);
}



