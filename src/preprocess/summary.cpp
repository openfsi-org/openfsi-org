/* Dec 2009, separate into two parts:
1) a set of global functions such as 'bring all models to date'

2) An abstract class interface class RXDatabaseI which contains methods
designed to connect to any database as well as to the old relax logfile.

*/
/* SUMMARY.C  22/11/94 
June 2003  Filtered summary only passes INPUT items which are not 'N/A'
Jan 2003  reference to unresolved string used to crash
july 2001.  Don't apply a site edit if the site doesnt currently have the 'editable' attribute.
jul 2000.  Use User_Index. Dont use User_Value on any segment containing geometry as that
will stuff up our hit-testing where we assume the use value is a  

Sept 97. A functional change. Evaluate_Summary_Data doesnt change a float, double or trim
  if the string is 'N/A'
 Previously, this was interpreted as  zero. Ignoring it is more sensible.

July 1997 cleaning an empty SFile caused tmp files to be generated and left open
This was mainly a problem in PAassive VPP which does this every loop, on
SGI which doesnt let you open many files.

 July 1997  Some flags werent set on a wind change. Done
 This would have given trouble for a run where (for instance)
 only heading and or mastlength changed
June 1997 Insulation in Decode Global Controls for failed strtok


THEN 
1) If something isnt type 'Input' its value is 'not set' until its posted. This is good for displays.
 Equally, if the display refers to a non-existent summary column, it is called 'disconnected'
2) We can clean the file by deleting all the columns which dont have a  HAS_BEEN_READ or HAS_BEEN_POSTED bit 


 Last_Line_Accessed is set by a

 4/9/96  editword$genoa$seamcurve$lojo$5 enabled as a header
 In the first implementation it unresolves the entity in question
 but doesnt re-build it
  22/8/96 Read Summary Line skips any lines commented out or "endblock"
Thus it can read an AIRFRAME export file

  12/7/96  if clean summary file returns < 0 open filename not "filename"
  6/6/96  text in display always visible
   Routines to generate summary files ( Excel tables of selected data )
   26/2/96 for DELL, ifdef out PostS By Modelno
   13/1/96 trying to find out why we always regenerate. some printfs
   21/12/95 some printfs PH


  24 10 95  Adam. Added String Tensions and a few others to summary list
                Also re-wrote the update and a clean_summary_file
  to make sure it correctly deals with mixes of
  end of lines.
  Also changed logic to write N/A where no real info
  is known about the values in the summary list
  eg. when a file is updated with previously non-existent
      data it now correctly shows that.
  Size of maximum line length is now 32000 as set in MAX_LINE_LENGTH

   MODIFIED 12/3/95
   (1) Cleanfile now on filename NOT sum->filename
   (2) sum->List_Has_Changed set to 0 after Open_Summary_File to suppress regeneration
   (3) HOOKING OF DISPLAYS TO SUMMARY ITEMS
   A) Displays may only be coupled to OUTPUT (g_summout)
   B) When a display is resloved, we go looking for a g_summout item
   If we find one, we set HC_"Set_User_Index on the display to its index
   Its OK not to find one.
   C) When a summary item is FIRST posted, we go looking for a display as currently
   If we find one, we set its User_Index  to the index
   If we dont, we keep hoops key zero
   D) When we delete a summary item we DONT delete any display but we
   DO set the User_Index on any display to -1
   E) WHen a display is deleted, we look up any associated Summary Item via the User Value
   and we set that summary items  hoops key to 0. In Entities.c
   
   
   
   2) Post_Summary_By_Model(int*model, char*label,char*value);
   3) Post_Summary_By_Model(RXEntity_p sail, char*label,char*value);
   
   
   
   HOW IT IS USED

   A) At various points in the program we Post Summary data
   eg Post_Summary_By_Model(1,"windspeed True"," 5.0m/s");
   
   B) At the same time as updating the text results windows, we  call
   
   
   
   
   */

#include "StdAfx.h"
#include <QDebug>
#include <QString>
#include <QDesktopServices>
#include <QUrl>
#include <QFile>
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RelaxWorld.h"
#include "RXQuantity.h"
#include "RXLogFile.h"
#include "rxspline.h"

#define  SUMDB  (0) //(0) or (1)
#include <time.h> 
#include <memory.h>
#include <ctype.h>

#include "RXEntityDefault.h" // nov 2008
#include "RXSRelSite.h" // nov 2008
#include "RXExpression.h" // nov 2008
#include "RX_FEPocket.h"
#include "RX_FEString.h"

#include "stringutils.h"
#include "words.h"

#include "RXSummaryTolerance.h"
#include  "f90_to_c.h"
#include "RX_UI_types.h"

#include "entities.h"
#include "script.h"
#include "RXExpression.h"

#include "trimmenu_s.h"
#include "drawwind.h"
#include "Gflags.h"  /* to set reflection-flags on heading change */


#include "summary.h"


int SUMMARY_ITEM::Init(){

    header=0;
    type=0; /* int, string, double */
    m_Has_Changed=0;
    m_input_data=0; /* string, int double depending on type
   ONLY USED ON INPUT SIDE */
    text=0;
    m_tol=0;
    value=0;
    m_oldvalue=0;
    m_check_Tol=0l;

    in_memory=0;       /* set to 1 if currently in memory */
    /* else   0 if just read from file and hence OK to write 'live' data */
    /* only used in regenerate routine */
    m_flag=0;
    owner=0;
    return 1;
};



int DeCode_Global_Controls(const char *text) {
    int k;
    char buf[2048];

    control_data_t *cp;
    char *lp;
    strncpy(buf,text,2048);
    if (SUMDB) printf(" Decode Global Controls on <%s>\n",buf);

    for(k=0;k<3;k++) {
        cp = &(g_ControlData[k]);
        memset(cp->extra_flags_str,0,255); // dec 2006

        if(!k)	lp = strtok(buf,"$");
        else 	lp = strtok(NULL,"$");
        if (SUMDB) printf(" Decode Global Controls (%d) <%s>\n",k,lp);
        if(lp) 			 {		cp->phase = atoi(lp);
            if((lp= strtok(NULL,"$"))) {		cp->press = atoi(lp);
                if((lp= strtok(NULL,"$"))) {		cp->value = atof(lp);
                    if((lp= strtok(NULL,"$"))) {		cp->nrelax = atoi(lp);
                        if((lp= strtok(NULL,"$"))) {		cp->relax_flags = atoi(lp);
                            if((lp= strtok(NULL,"$"))) {      	strcpy(cp->extra_flags_str ,lp);
                                if((lp= strtok(NULL,"$"))) {	cp->pansail_flags = atoi(lp);
                                    if((lp= strtok(NULL,"$"))) {     	strcpy( cp->windfile ,lp);
                                        if((lp= strtok(NULL,"$"))) {	cp->ncycle = atoi(lp);
                                            if((lp= strtok(NULL,"$"))) {   	strcpy(cp->wakefile ,lp);
                                            } else printf("(phase %d)cant parse wakefile from <%s>\n",k,lp);
                                        } else printf("(phase %d)cant parse ncycle from <%s>\n",k,lp);
                                    }else printf("(phase %d)cant parse windfile from <%s>\n",k,lp);
                                }else printf("(phase %d)cant parse pansail Flags from <%s>\n",k,lp);
                            }else printf("(phase %d)cant parse extra Flags from <%s>\n",k,lp);
                        }else printf("(phase %d)cant parse RelaxFlags from <%s>\n",k,lp);
                    }else printf("(phase %d)cant parse N Relax from <%s>\n",k,lp);
                }else printf("(phase %d)cant parse value from <%s>\n",k,lp);
            }else printf("(phase %d)cant parse press from <%s>\n",k,lp);
        } else printf("(phase %d)cant parse phase from <%s>\n",k,lp);


    } // for K
    return 1;
}

int Post_Global_Controls() {
    int k;
    char buf[4096], s[1024];
    control_data_t *cp;
    static const char *DFAC="Analysis_Controls" ;
    strcpy(buf," ");
    for(k=0;k<3;k++) {
        cp = &(g_ControlData[k]);
        PC_Strip_Trailing( cp->extra_flags_str   ) ;    PC_Strip_Trailing( cp->windfile);   PC_Strip_Trailing( cp->wakefile);

        sprintf(s,"  %d$%d$%f$%d$%d$%s $%d$%s $%d$%s $",cp->phase ,cp->press,cp->value ,cp->nrelax ,cp->relax_flags,cp->extra_flags_str ,cp->pansail_flags,cp->windfile ,cp->ncycle,cp->wakefile);
        strcat(buf,s);
    }
    if(SUMDB) printf(" posting <%s> length %d\n",buf,(int)strlen(buf));
    if (g_World->DBMirror() )
        g_World->DBMirror()->post_summary_value(DFAC,buf);
    return 1;
}

//#error("what is the return value?? ")
int SUMMARY_ITEM::apply_one_summary_item( )const
{  assert(this->text);
    return Justapply_one_summary_item( this->header, this->text, this->type, this->m_flag);
}
int Justapply_one_summary_item(const char*header, const char*text, const int type, const int flag )   // the workhorse for acting on DB items.
{


    //type is  /* int, string, double */
    int retVal=0;
    int err;
    if( !( flag&SUM_INPUT  ))
        return 0;
    if(g_World->DBMirror())
        g_World->DBMirror()->SetLast(header,text);
     if(flag&SUM_BLOB) {
         return 1;
     }
    char  *lp;
    string hcopy;
    int changed=0;
     assert(text);
 if(text && !(rxIsEmpty(text))) { //std=c++11 reckons this is ambiguois

        switch(type) {
        case STRING:
            if((lp = stristr(header,"run_name"))) {
                g_World->ModifyRunName(text);
            }
            else if((lp = stristr(header,"editword"))) {
                QString qline(header); qline+=":"; qline+= text;
                qline.replace("$",":");
                  Do_EditWord(qPrintable(qline),NULL);
            }

            else if((lp = stristr(header,"Wind File"))) {
                if(g_World->m_wind.WindName()  != text ) {
                    g_World->m_wind.SetWindName( text);
                    cout<< " Wind File now "<< qPrintable(g_World->m_wind.WindName())<<endl;
                    mkGFlag(GLOBAL_DO_APPWIND);
                    mkGFlag(GLOBAL_DO_REFLECTION);
                    mkFlag(g_Analysis_Flags,WIND_CHANGE);
#ifdef USE_PANSAIL
                    Draw_Wind((float)0.0,g_PansailIn.Reference_Length);
#endif
                }
            }

            else if((lp = stristr(header,"Analysis_Controls"))) {
                DeCode_Global_Controls(text);
            }
            else if(stristr(header,"Solid Angle$")){
            }
            else if(stristr(header,"$area")){
            }
            else if(stristr(header,"reflection plane")){
            }
            else if(stristr(header,"file$")){
            }
            else if(stristr(header,"String_Tension")){
            }
            else if(strieq(header,"time")){
            }
            else if(strieq(header,"ncalls")){
            }
            else if(strieq(header,"Status")){
            }
            else if(strstr(header,"AWS_")){
            }
            else if(strstr(header,"AWUP_")){
            }
            else if(strstr(header,"AWA_")){
            }
            else {
                if(SUMDB) printf("CANNOT apply a type STRING (%s)\n",header);
            }
            break;

        case FLOAT:{
            if(strieq(text,"N/A"))			// 8 Feb 2003 because it gets interpreted as 0.0
            {
                return 0;}

            double  value =RXQuantity::OneTimeEvaluate(TOSTRING(text)  ,L"m",SUMMARY_ITEM::Context(header),&err);
            if((lp = stristr(header,"$node$"))) {
                int i;
                string mdl,obj,which;
                vector<string>words;
                changed=0;
                if(SUMDB)	cout<< "processing FLOAT 'node', hdr='"<<header<<"'\n";
                hcopy = string(header);
                words = rxparsestring(hcopy,"$");
                i = words.size(); //make_into_words(hcopy,&words,"$");
                int OK=0;
                switch (i) {
                case 4: // node, model, object,[ABC]
                    if(words[0]==string("node")) OK=1;
                    mdl=words[1];
                    obj=words[2];
                    which=words[3];
                    break;
                case 5:		//	empty, model,node,object,[ABC]

                    if(words[0].empty() && words[2]==string("node") ) OK=1;
                    mdl=words[1];
                    obj=words[3];
                    which=words[4];
                    break;
                default:
                    char buf[512];
                    sprintf(buf," corrupt Node header in Summary File '%s'\n Needs '$model$node$name$[ABC] ", header);
                    g_World->OutputToClient(buf,2);
                    retVal = 0;
                    words.clear();
                    break;

                };
                if(!OK)
                    break;
                if(SUMDB) cout<<" edit of (rel)site named '"<<obj.c_str()<<"' in model '"<<mdl.c_str()<<"', exp = '"<<which.c_str()<<"'"<<endl;
                if(SUMDB) cout<< " new value (dble) is "<<value<<"  from text '"<<text<<"'"<<endl;
                class RXEntity *ent = (QPC_Get_Key(mdl,"relsite,site",obj)); // this call OK
                if(ent && (g_World->m_StateFile ||
                           ( ent->AttributeGet("editable")
                            || ent->AttributeGet("$trimmable") ) ))
                {  // OR we are reading a state file.
                    double ABC;
                    RXSRelSite *p = dynamic_cast<RXSRelSite *> (ent);
               //     HC_Open_Segment_By_Key(ent->hoopskey);
                    RXExpressionI *l_a = p->FindExpression(TOSTRING(which.c_str()),rxexpLocalAndTree);
                    if(l_a) {
                        if(SUMDB)
                        {
                            ABC = l_a->evaluate();
                            printf(" changing A from %f to %f\n",ABC, value);
                        }
                        RXSTRING buf =TOSTRING(text);
                        if(l_a->Change(buf) )
                            changed = 1;
                        if(SUMDB)  printf("afterwards = %f\n",l_a->evaluate() );
                    }
                    else {
                      //  cout<< "Site "<< obj<<" has no variable named "<<which<<endl;
                        // if(SUMDB) p->Dump(stdout);
                    }
                    if(changed) {
                        if(SUMDB)  cout<<"changed on NODE "<<words[2]<<endl;
                        ent->SetNeedsComputing();
                    }
                    else
                        if(SUMDB) cout<<"NO change on NODE "<<words[2]<<endl;
                }
                else
                    if(SUMDB)cout<<"no change on node <"<<words[2]<<"> (not editable) in model <"<<words[1]<<">"<<endl;
                hcopy.clear();
                break;
            }
            /*
typically  
 $mast_spline$_side_0
 $mast_spline$_aft_0
 $mast_spline$_side_1
 $mast_spline$_aft_1
 $mast_spline$_side_2
 $mast_spline$_aft_2
or boat$mast_spline$_side_0
and we want it to be:
 $boat$mast_spline$_side_$0

*/
            else if(
                    (lp = stristr(header,"$_side_")) || (lp = stristr(header,"$_aft_"))
                    ||(lp = stristr(header,"$side$")) || (lp = stristr(header,"$aft$"))
                    ) {
                int off=-1;  // the index in the polyline
                int ind=0; // 1 is side(Y), 2 is aft(Z)
                int c;	// word count
                int ok;
                string Name ,wh,bn ,offstr;
                vector<string>words;
                hcopy = string(header);
                Replace_String(hcopy,"$_side_","$side$");
                Replace_String(hcopy,"$_aft_","$aft$");
                words=rxparsestring(hcopy,"$",true);
                //grandfather
                if(words[0].empty()&&words[1]=="mast_spline" )   //$mast_spline$_side_0
                    words[0]="boat";
                if(words[0]=="mast_spline" )   //$mast_spline$_side_0
                    words.insert(words.begin(),string("boat"));
                c = words.size();
                switch(c) {
                case 4:		// 	boat$mast_spline$side$0
                    bn = words[0];
                    Name = words[1]; ok=1;
                    wh=words[2];
                    offstr=words[3];
                    break;
                case 5:		//		$boat$mast_spline$side$0
                    bn = words[1];
                    Name = words[2]; ok=1;
                    wh=words[3]; // _side_
                    offstr=words[4];
                    break;
                default:	// don't try
                    cout<< "cant parse spline edit(1) <"<<hcopy<<"> wordcount= "<<c<<endl;
                    ok=0; // later, just skip
                }

                if(wh=="side" ) ind=1; else if(wh=="aft" ) ind=2; else ok=0;
                if(ok){
                    off = atoi(offstr.c_str());
                    class RXSplineI* bc=0;
                    if((!Name.empty()) && ind && off >=0)
                        bc =  dynamic_cast<RXSplineI*>(QPC_Get_Key(bn,"spline",Name)); // this call OK

                    if(bc) {
                        double x;
                        if(bc && bc->size() > off) {
                            float* target=NULL;
                            switch(ind) {
                            case 1:
                                x = bc->Y(off);
                                if(fabs(x-value)> MAST_TRIM_TOL)
                                { changed = 1;bc->SetY(off,TOSTRING(text));}
                                break;
                            case 2:
                                x = bc->Z(off);
                                if(fabs(x-value)> MAST_TRIM_TOL)
                                {  changed = 1;	bc->SetZ(off,TOSTRING(text)); }
                                break;
                            default:
                                assert(0);
                            }

                            retVal++;
                        }
                        if(changed) {
                            bc->SetNeedsComputing();
                            bc->Esail->SetFlag(FL_NEEDS_TRANSFORM ); // don't know why
                        }
                    }// if Ent
                    hcopy.clear();
                    words.clear();
                } // if OK
                else
                    cout<< "cant parse spline edit(2) <"<<hcopy<<"> wordcount= "<<c<<endl;
                break;
            } // end of if _side_ or _aft_
            if((lp = stristr(header,"True Wind"))) {
                g_World->m_wind.Set(header,value);
                break;
            }
            if((lp = stristr(header,"TWS"))) {
                g_World->m_wind.Set(header,value);
                break;
            }
            else if((lp = stristr(header,"boat s"))) {
                /* boatspeed */
                g_World->m_wind.Set(header,value);
                break;
            }
            else if((lp = stristr(header,"Course"))) {
                /* direction in which vmc is measured*/
                g_World->m_wind.Set(header,value);
                break;
            }
            else if((lp = stristr(header,"reference height"))) {
                g_World->m_wind.Set(header,value);
                break;
            }
            else if((lp = stristr(header,"heelangle"))) {
                g_World->SetHeelAngle( value);
                break;
            }
            else if((lp = stristr(header,"reflection height"))) {
                /* reflection plane height */
                g_World->m_wind.Set(header,value);
                break;
            }

            else if((lp = stristr(header,"leeway"))) {
                g_World->m_wind.Set(header,value);
                break;
            }
            else if((lp = stristr(header,"heading"))) {
                g_World->m_wind.Set(header,value);
                break;
            }
            else if((lp = stristr(header,"twa"))) {
                g_World->m_wind.Set(header,value);
                break;
            }
            else {
                printf("NOT COPING WITH FLOAT '%s'\n",header);
            }
            break;
        }
            break;
        case DOUBLE:
        {
            if(strieq(text,"N/A"))			// 8 Feb 2003 because it gets interpreted as 0.0
            {
                return 0;
            }
            double dvalue =RXQuantity::OneTimeEvaluate(TOSTRING(text)  ,L"m",SUMMARY_ITEM::Context(header),&err);
            if(strieq(header,"Pitch Velocity")) {
                g_World->m_wind.Pitch_Velocity = dvalue; printf("Pitch_Velocity set to %f\n",g_World->m_wind.Pitch_Velocity);
            }
            else if(strieq(header,"Roll Velocity")) {
                g_World->m_wind.Roll_Velocity = dvalue; printf("Roll_Velocity set to %f\n",g_World->m_wind.Roll_Velocity);
            }

            else if((lp = stristr(header,"$string$"))) {
                /* The R2 form was
  'STRING$boat$gen_sheet$Tension'

due to an R3 dev bug, this became for a while
 'boat$STRING$boat$gen_sheet$Tension'
For a while in 2009 we had
 'boat$STRING$gen_sheet$Tension'  or 'boat$STRING$gen_sheet$Trim'
The R3 form is
 '$boat$STRING$gen_sheet$Tension'  or 'boat$STRING$gen_sheet$Trim'
because that is consistent with the other posts (model name first)
WE need to wrap the modelname in two '$'s 
*/
                int i;
                string str ,model, Sname,  what;
                char buffer[1024];
                changed=0; strncpy(buffer,header,1024);
                str = string(strtolower(buffer));
                i = RXSUM_ParseStringHeader(  str,  model,  Sname,  what) ;
                if( !i)
                    break;

                class RX_FEString *sdata = dynamic_cast<  class RX_FEString *> (QPC_Get_Key(model,"string",Sname));
                if(sdata) { //Jan 2003

                    if(SUMDB) printf("(Justapply_one_summary_item) %s %s text=%s x=%f\n", header, what.c_str(),text, dvalue);
                    //first of all, if 'what' is ea, ti or trim we do a change expression
                    RXSTRING expName = TOSTRING(what.c_str());
                    if(expName==L"prestress") expName=TOSTRING("ti"); // a synonym
                    RXSTRING  stringvalue=TOSTRING(text);
                    RXExpressionI*exp = sdata->FindAndChange(expName,stringvalue,rxexpLocal);
                    if (exp){
                        exp->evaluate();
                        break;
                    }

                    // else do it the old way

                    if((what=="prestress")) {
                        if((sdata->m_which!= 1) || (fabs(sdata->d.TIS  - dvalue) > STRING_TENSION_TOL)) {
                            //	printf("Request ignored:  prestress from %f to %f\n", sdata->Ti, s->value);
                            // 	sdata->which = 1;
                            //	sdata->Ti = s->value;
                            //	changed = 1;
                        }
                        changed = 0;
                    }
                    else if(what=="length" || what=="trim" ){ /* Nov 96 see trimmenu_s.c  */
                        if((sdata->m_which != 0) || (fabs(*sdata->GetTrimPtr() - dvalue) > STRING_LENGTH_TOL)) {
                            sdata->m_which = 0;
                            cout<<"model:"<<model<<",Sname:"<<Sname<<" trim was "<< *(sdata->GetTrimPtr())<< " new value="<<dvalue  <<endl;
                            sdata->SetTrim( dvalue);
                            changed = 1;
                            sdata->Esail->SetFlag(FL_HASLACKOFFIT);
                        }
                    }
                    else if((what=="absolute") ||(what=="tension_mean") ){
                        //	cout<< " ABSOLUTE string lengths now Superceded by Trim\n"<<endl;
                        /* 	    if((sdata->which != 0) || (fabs(sdata->Ltot+sdata->Li  - s->value) > STRING_LENGTH_TOL)) {
  IGNORED sdata->which = 0;
       sdata->Ltot = s->value- sdata->Li;

       changed = 1;
     }*/
                    }
                    else if((what=="tension")){

                    }
                    else {
                        printf("%s    " ,what.c_str());
                        g_World->OutputToClient("There is a corrupt STRING$ header in the summary file",2);
                        changed = 0;
                    }
                    if(changed) {
                        if(SUMDB)  printf("changed on STRING %s\n",Sname.c_str());
                        if(SUMDB)   printf("EA=%f Ti=%f Trim= %f\n", sdata->d.EAS  ,  sdata->d.TIS  , *(sdata->GetTrimPtr()));
                        sdata->Esail->SetFlag(FL_HASLACKOFFIT);
                    }

                } // if sdata, else the string doesnt exist.
                break;
            } // if string

            else if( (lp = stristr(header,"$pocket$"))   ) //(blank)$model$pocket$object$trim
            {
                if(SUMDB) printf(" Summary <%s> may 2008 pocket\n",header);
                int i;
                vector<string>words; string mdl,obj;
                changed=0;
                hcopy = string(header);
                words=rxparsestring(hcopy,"$");i = words.size();// the r3 form for pocket is 'genoa$pocket$p0$Trim'
                if(i != 5) {
                    RXSTRING s(L"corrupt pocket header :" );
                    g_World->OutputToClient(s+TOSTRING(header),1);
                    retVal = 0;
                    break;
                }
                mdl = words[1]; obj=words[3];
                class RX_FEPocket* bdata = dynamic_cast<class RX_FEPocket*>(QPC_Get_Key(mdl,"pocket",obj)); // only picks up entities in Hoisted sails
                if(bdata && fabs(bdata->GetTrim() - dvalue) > STRING_LENGTH_TOL) {
                    bdata->SetTrim( dvalue);
                    changed = 1;
                    bdata->Esail->SetFlag(FL_HASLACKOFFIT);
                }
                hcopy.clear();
                break;
            } // if pocket
            else if( (lp = stristr(header,"batpatch"))   ) {
                printf("VERIF Summary <%s> may 2008  batpatch to patch/pocket\n",header);
                int i;
                vector<string>words;
                changed=0;
                hcopy = string(header);
                words=rxparsestring(hcopy,"$");i = words.size();
                if(i != 4) {
                    g_World->OutputToClient("corrupt batten header",2);
                    retVal = 0;
                    words.clear();
                    break;
                }

                class RX_FEPocket * ent = dynamic_cast<class RX_FEPocket *>(QPC_Get_Key(words[1],"batpatch",words[2])); // call OK
                if(ent ) {
                    // cout<< "(Summary)  batpatch no longer exists "<<endl;
                    if(ent && fabs(ent->GetTrim() - dvalue) > STRING_LENGTH_TOL) {
                        ent->SetTrim( dvalue);
                        changed = 1;
                        ent->Esail->SetFlag(FL_HASLACKOFFIT);
                    }
                }
                hcopy.clear();
                break;
            } // if batpatch
            else if(strieq(header,"Course"))
                break;
            else {
                printf("DB Read NOT COPING WITH DOUBLE '%s' yet\n",header);
                break;
            }
            // end of case double
            break;
        }
        default:
            printf("DB read NOT COPING WITH '%s' yet\n",header);
            break;
        } // end switch
    }
    return(retVal);
}

// the logic here is ghastly: using AND we dont need two return values
//static
int RXMirrorDBI::Summary_Type(const char* label,int *flag) {
    *flag=0;
    assert(!cout.bad());
    assert(label);
    assert(*label);
    if(stristr(label,"true wind")) 		{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(FLOAT);  }
    if(strieq(label,"true wind speed")) 	{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(FLOAT);  }
    if(strieq(label,"TWS")) 			{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(FLOAT);  }
    if(strieq(label,"AWS")) 			{ *flag=SUM_OUTPUT;               return(DOUBLE);  }
    if(strieq(label,"AWA")) 			{ *flag=SUM_OUTPUT;               return(DOUBLE);  }
    if(strieq(label,"VMC")) 			{ *flag=SUM_OUTPUT;               return(DOUBLE);  }
    if(strieq(label,"Course")) 			{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(DOUBLE);  }

    if(strieq(label,"wind file")) 		{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(STRING);  }
    if(strieq(label,"boat speed")) 		{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(FLOAT);  }
    if(strieq(label,"heading")) 		{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(FLOAT);  }
    if(strieq(label,"twa")) 		{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(FLOAT);  }
    if(strieq(label,"HeelAngle")) 		{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(FLOAT);  }
    if(strieq(label,"Pitch Velocity")) 		{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(DOUBLE);  }
    if(strieq(label,"Roll Velocity")) 		{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(DOUBLE);  }
    if(strieq(label,"Reflection Height")) 	{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(FLOAT);  }
    if(strieq(label,"Reflection Plane")) 	{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(STRING);  }
    if(strieq(label,"fx_relax")) 		{ *flag=SUM_OUTPUT; 	  	return(DOUBLE);  }
    if(strieq(label,"fy_relax")) 		{ *flag=SUM_OUTPUT; 	  	return(DOUBLE);  }
    if(strieq(label,"fz_relax")) 		{ *flag=SUM_OUTPUT; 	 	 return(DOUBLE);  }
    if(strieq(label,"mx_relax")) 		{ *flag=SUM_OUTPUT; 	 	 return(DOUBLE);  }
    if(strieq(label,"my_relax")) 		{ *flag=SUM_OUTPUT; 	 	 return(DOUBLE);  }
    if(strieq(label,"mz_relax")) 		{ *flag=SUM_OUTPUT; 	 	 return(DOUBLE);  }
    if(stristr(label,"Solid Angle$")) 		{ *flag=SUM_OUTPUT; 	 	 return(DOUBLE);  }//Apr 2003
    if(stristr(label,"$Area")) 			{ *flag=SUM_OUTPUT; 	 	 return(DOUBLE);  }//Apr 2003

    if(strieq(label,"Analysis_Controls")) 	{ *flag=SUM_OUTPUT+SUM_INPUT; 	 return(STRING);  }

    if(stristr(label,"EditWord"))		{ *flag=SUM_INPUT; 	  	 return(STRING);  }
    if(stristr(label,"PRSINT_"))		{ *flag=SUM_OUTPUT; 	 	 return(DOUBLE);  }
    if(stristr(label,"PRS_Int_"))		{ *flag=SUM_OUTPUT; 	 	 return(DOUBLE);  }
    if(stristr(label,"Press_Integration_"))	{ *flag=SUM_OUTPUT; 	 	 return(DOUBLE);  }

    if(strieq(label,"Viscous_Drag")) 		{ *flag=SUM_OUTPUT; 	  	  return(DOUBLE);  }
    if(strieq(label,"leeway")) 			{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(FLOAT);  }
    if(strieq(label,"Reference Height")) 	{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(FLOAT);   }
    if(strieq(label,"Wind File"))        	{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(STRING);  }
    if(strieq(label,"Run_Summary")) 		{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(STRING);  }
    if(strieq(label,"Run_Name")) 		{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(STRING);  }
    if(strieq(label,"ncalls")) 			{ *flag=SUM_OUTPUT; 	  	return(STRING);  }
    if(strieq(label,"time")) 			{ *flag=SUM_OUTPUT; 	  	return(STRING);  }
    if(strieq(label,"status")) 			{ *flag=SUM_OUTPUT; 	 	 return(STRING);  }
    if(strieq(label,"analysis report")) 	{ *flag=SUM_OUTPUT; 	 	 return(STRING);  }

    if(stristr(label,"$side$")) 		{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(FLOAT);  }
    if(stristr(label,"$aft$")) 			{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(FLOAT);  }

    // tension overrides string
    if(stristr(label,"$Tension")) 		{ *flag=SUM_OUTPUT; 		  return(DOUBLE);  }
    if(stristr(label,"string$")) 		{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(DOUBLE);}
    if(stristr(label,"batpatch$")) 		{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(DOUBLE);  }
    if(stristr(label,"patch$")) 		{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(DOUBLE);  }
    if(stristr(label,"pocket$")) 		{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(DOUBLE);  }

    if(stristr(label,"Depth_")) 		{ *flag=SUM_OUTPUT; 	 	 return(DOUBLE);  }
    if(stristr(label,"Draft_Pos_")) 		{ *flag=SUM_OUTPUT; 	 	 return(DOUBLE);  }
    if(stristr(label,"twist_")) 		{ *flag=SUM_OUTPUT; 	  	return(DOUBLE);  }
    if(stristr(label,"fwd%_")) 			{ *flag=SUM_OUTPUT; 	  	return(DOUBLE);  }
    if(stristr(label,"aft%_")) 			{ *flag=SUM_OUTPUT; 	  	return(DOUBLE);  }
    if(stristr(label,"aftdepth%_"))		{ *flag=SUM_OUTPUT; 	  	return(DOUBLE);  }
    if(stristr(label,"entry_angle")) 		{ *flag=SUM_OUTPUT; 	 	 return(DOUBLE);  }
    if(stristr(label,"Exit_Angle")) 		{ *flag=SUM_OUTPUT; 	 	 return(DOUBLE);  }
    if(stristr(label,"node$"))                  { *flag=SUM_OUTPUT+SUM_INPUT; 	 return(FLOAT);  }
    if(stristr(label,"Trefftz_"))  		{ *flag=SUM_OUTPUT; 	 	 return(FLOAT);  }
    if(stristr(label,"mean_"))			{ *flag=SUM_OUTPUT; 	 	 return(DOUBLE);  }
    if(stristr(label,"Tape_Area"))		{ *flag=SUM_OUTPUT; 	 	 return(DOUBLE);  }
    if(stristr(label,"file$"))			{ *flag=SUM_OUTPUT; 	  	return(STRING);  }

    if(stristr(label,"AWS_"))			{ *flag=SUM_OUTPUT; 	  	return(DOUBLE);  }
    if(stristr(label,"AWA_"))			{ *flag=SUM_OUTPUT; 	  	return(DOUBLE);  }
    if(stristr(label,"AWUP_"))			{ *flag=SUM_OUTPUT; 	  	return(DOUBLE);  }
    if(strieq(label,"StateFile"))		{ *flag=0; 		  	return(STRING);  }
    if(stristr(label,"reaction$")) 		{ *flag=SUM_OUTPUT; 		  return(DOUBLE);  }
    if(stristr(label,"disp$"))     		{ *flag=SUM_OUTPUT; 		  return(DOUBLE);  }
    if(stristr(label,"$X$"))     		{ *flag=SUM_OUTPUT; 		  return(DOUBLE);  }
    if(stristr(label,"$Y$"))     		{ *flag=SUM_OUTPUT; 		  return(DOUBLE);  }
    if(stristr(label,"$Z$"))     		{ *flag=SUM_OUTPUT; 		  return(DOUBLE);  }
    if(stristr(label,"$Cl"))     		{ *flag=SUM_OUTPUT; 		  return(DOUBLE);  }
    if(stristr(label,"$TotalLength"))     		{ *flag=SUM_OUTPUT; 		  return(DOUBLE);  }
    if(strieq(label,"residual"))     		{ *flag=SUM_OUTPUT; 		  return(DOUBLE);  }
    if(strieq(label,"quality"))     		{ *flag=SUM_OUTPUT; 		  return(DOUBLE);  }
    if(stristr(label,"$textresults"))     	{ *flag=SUM_BLOB+SUM_OUTPUT; 	  return(STRING);}
    if(stristr(label,"upload"))
    {
        *flag=SUM_BLOB+SUM_OUTPUT;
        if(stristr(label,"download"))
            *flag+=SUM_INPUT;
        return(*flag);
    }
    if(stristr(label,"download"))     	        { *flag=SUM_BLOB+SUM_INPUT; 	  return(*flag );}
    if(stristr(label,"filedelivery"))     	    { *flag=SUM_BLOB+SUM_INPUT; 	  return(*flag );}
    if(stristr(label,"$beam$"))       		    { *flag=SUM_OUTPUT; 		  return(DOUBLE);  }

    // obsolete
    if(stristr(label,"_side_")) 		{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(FLOAT);  }
    if(stristr(label,"_aft_")) 			{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(FLOAT);  }
    if(strieq(label,"Mast Length")) 		{ *flag=SUM_OUTPUT+SUM_INPUT; 	  return(FLOAT);} //obsolete

    g_World->OutputToClient(QString(" SummaryType doesnt know about ")+QString(label) ,1);

    { *flag=SUM_INPUT;  return(STRING);  }
}
class RXENode *SUMMARY_ITEM::Context(const char*header)
{
    // if the header begins with $<model>$  we find a model with that name and return it
    // else return the World;
    class RXENode *   rv= g_World ;
    QString qhead(header);
    QStringList ll = qhead.split("$");
    if(ll.size()>3){
        QString &mdl = ll[1];
        rv = g_World->FindModel(mdl);
        if(!rv)
            return g_World;
    }
    return rv ;
}

// looks like an updateclass
// the problem here is that we dont know the context (ie the ENode) in which to evaluate the text
// so if the text refers to a variable that is local to a model the evaluation will fail.

int SUMMARY_ITEM::PCS_Evaluate_Summary_Data(const char* value) {
    assert(!cout.bad());
    if(this->type == STRING) {
        if(this->text){
            this->m_Has_Changed =(this->m_check_Tol) &&( !strieq(this->text,value));
            RXFREE(this->text);
            this->text = NULL;
        }
        this->text = STRDUP(value);
        return(1);
    }
    if(this->type == DOUBLE || this->type == FLOAT|| this->type == TRIM) {
        double val; int err;
        if(!strieq(value,"N/A") && !strieq(value,"Not Set") ) { /* Peter 9.97 */
            this->m_Has_Changed = 0;
            val =RXQuantity::OneTimeEvaluate(TOSTRING(value)  ,L"m",Context(this->header   ),&err);
            this->m_Has_Changed = this->m_check_Tol;
            this->m_oldvalue =  this->value;
            this->value = val;
            assert(!cout.bad());
        }
        else if(SUMDB)
            printf(" in '%s' dont change val=%f to new text '%s'\n", this->header, this->value, value);
        assert(!cout.bad());

        if(this->text)
            RXFREE(this->text);
        this->text = STRDUP(value);
        assert(!cout.bad());
        return(1);
    } // if....
    return(0);
}

int Post_Summary_By_SLI( int*p_sli, const char*label,const char*value) {
    class RXSail *s;
#ifdef FORTRANLINKED
    int sli = *p_sli;
    int err=0;
    s = cf_getsailpointer(sli, &err);
#else
    s = NULL;
#endif
    return Post_Summary_By_Sail(s,label,value);
}//int Post_S _By_SLI

int Post_Summary_By_Sail(SAIL *sail, const char*label,const char*value){
    if(!g_World )
        return 0;
    if(!g_World->DBMirror())
        return 0;
    stringstream b;
    if(sail) {
        b<<"$"<<sail->GetType()<<"$"<<label;
        return(g_World->DBMirror()->post_summary_value(b.str().c_str() ,value));
    }
    return(g_World->DBMirror()->post_summary_value(label,value));
}

// sname is the entity, what is 'tension', etc, model
int RXSUM_ParseStringHeader( string str, string &model, string &Sname, string &what) {

    int rc=0;
    /*  The R2 form was
  'STRING$boat$gen_sheet$Tension'

due to an R3 dev bug, this became for a while
 '$boat$STRING$boat$gen_sheet$Tension'
then 
 'boat$STRING$gen_sheet$Tension'
The R3 form is
 '$boat$STRING$gen_sheet$Tension'
because that is consistent with the other posts (model name first)
*/
    char buf[512];
    int nw;
    vector<string> Words;
    string toks("$");
    Words = rxparsestring(str,toks);

    while(Words.size() && Words[0].empty()) // remove the
        Words.erase(Words.begin());

    nw = Words.size ();

    switch (nw) {
    case 5: // 	'boat$STRING$boat$gen_sheet$Tension'
        model = Words[0]; //assert(Words[0]==Words[2]);
        Sname = Words[3];
        what=Words[4];
        return 5;
    case 4:
        Sname = Words[2];
        what=Words[3];
        if(Words[0]=="string") {// 'STRING$boat$gen_sheet$Tension'
            model = Words[1];
            return 4;
        }
        if(Words[1]=="string") {// 'boat$STRING$gen_sheet$Tension'
            model = Words[0];
            return 4;
        }
    default :
        sprintf(buf," corrupt string header in Summary File '%s'\n Needs 4 words 'string',sailname,entname,what) separated by '$' ", str.c_str() );
        g_World->OutputToClient(buf,2);
        return 0;
    }; //switch
    //	return 0;
} //RXSUM_ParseStringHeader

