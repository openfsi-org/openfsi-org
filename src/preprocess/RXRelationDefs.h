#pragma once

// these are the definitions of the 'what'  parameter for RXObject::SetRelationOf

//RXO_NOTYPE is defined  RXObject.h
//  RXO_ANYINDEX  must be less than 0 so that the map sequence is ok.
// RXO_ANYINDEX and RXO_ANYTYPE should be different but we have to be very careful about making this change

#define	RXO_ANYINDEX		0   // we have a horrible confusion between  RXO_ANYINDEX  and  RXO_ANYTYPE
#define	RXO_MAXINDEX		32767
#define	RXO_ANYTYPE		0
#define	RXO_FACTORYVARIABLE	99
		
#define	RXO_AERO_OF_ISURF	1001
#define	RXO_BAT_OF_POCKET	1002
#define	RXO_BC_OF_PATCH  	1003
#define	RXO_BC_OF_POCKET	1004
#define	RXO_BC_OF_SC    	1005
#define	RXO_C1_OF_CS    	1006
#define	RXO_C2_OF_CS    	1007
#define	RXO_CC_OF_BNDY  	1008
#define	RXO_CC_OF_PATCH 	1009
#define	RXO_CC_OF_POCKET	1010
#define	RXO_CC_OF_SC    	1011
#define	RXO_CONNECT_OF_SITE	1012
#define	RXO_CORNER_OF_PATCH	1013
#define	RXO_ENT_OF_FUNC   	1014
#define	RXO_ENT_OF_INCFILE	1015
#define	RXO_EXP_OF_ENT  	1016
#define	RXO_EXP_OF_GRID 	1017
#define	RXO_EXP_OF_SET  	1018
#define	RXO_FIX_OF_SITE     1019
#define	RXO_FLD_OF_SC   	1020
#define	RXO_FOCUS_OF_FLD	1021
#define	RXO_GAUSS_OF_SC 	1022
#define	RXO_GRID_OF_SC  	1023
#define	RXO_ISURF_OF_CS 	1024
#define	RXO_LAYER       	1025
#define	RXO_LAYER_OF_FIELD	1026
#define	RXO_LAYER_OF_PATCH	1027
#define	RXO_LDG_OF_AERO 	1028
#define	RXO_MAT_OF_BEAM 	1029
#define	RXO_MAT_OF_FIELD	1030
#define	RXO_MAT_OF_LYR  	1031
#define	RXO_MAT_OF_PATCH	1032
#define	RXO_MAT_OF_POCKET	1033
#define	RXO_MAT1_OF_SC  	1034
#define	RXO_MAT2_OF_SC  	1035
#define	RXO_N1_OF_PATCH 	1036
#define	RXO_N1_OF_POCKET	1037
#define	RXO_N1_OF_SC    	1038
#define	RXO_N2_OF_PATCH 	1039
#define	RXO_N2_OF_POCKET	1040
#define	RXO_N2_OF_SC    	1041
#define	RXO_ORGN_OF_SITE	1042
#define	RXO_PATCH_OF_SC 	1043
#define	RXO_PD_OF_PANEL  	1044
#define	RXO_POCKET_OF_SC	1045
#define	RXO_PSIDE_OF_CC 	1046
#define	RXO_PSIDE_OF_PANEL 	1047
#define	RXO_PSIDE_OF_SC  	1048
#define	RXO_RD_OF_PANEL  	1049
#define	RXO_REF_OF_FIELD	1050
#define	RXO_SC_OF_BEAM  	1051
#define	RXO_SC_OF_CC    	1052
#define	RXO_SC_OF_DXF   	1053
#define	RXO_SC_OF_FANNED	1054
#define	RXO_SC_OF_ISURF 	1055
#define	RXO_SC_OF_SITE  	1056
#define	RXO_SC_OF_STRING	1057
#define	RXO_SC1_OF_FIELD	1058
#define	RXO_SC1_OF_SITE 	1059
#define	RXO_SC2_OF_FIELD	1060
#define	RXO_SITE_OF_PANEL	1061
#define	RXO_SITE_OF_PATCH	1062
#define	RXO_SITE_OF_POCKET	1063
#define	RXO_SITE_OF_PSIDE	1064
#define	RXO_TLG_OF_AERO 	1065
#define	RXO_VECFLD_OF_MOULD	1066
#define	RXO_ZONE_OF_SC  	1067
#define	RXO_X_OF_SPLINE 	1068
#define	RXO_Y_OF_SPLINE 	1069
#define	RXO_Z_OF_SPLINE 	1070
#define	RXO_IA_OF_SC    	1071
#define	RXO_PANEL_OF_NAS   	1072
#define	RXO_DEPTH_OF_SC    	1073
#define	RXO_RIGIDBODY_NODE 	1077
#define	RXO_NODECURVE_NODE 	1078
#define	RXO_MESHSITE_OF_SC	1079

#define	RXO_STRING_TI    	1074
#define	RXO_STRING_EA    	1075
#define	RXO_STRING_TRIM   	1076
#define	RXO_STRING_ZI   	1081
#define	RXO_STRING_ZT   	1082
#define	RXO_STRING_TQ   	1083
#define	RXO_STRING_MASS   	1084
#define	RXO_MESH_OF_PAN   	1085
#define	RXO_X_OF_VF     	1086
#define	RXO_Y_OF_VF       	1087
#define	RXO_Z_OF_VF       	1088
#define	RXO_CONNECT_CHILD   1089
#define	RXO_SUBMODEL        1090
#define	RXO_SM_CONNECT      1091
#define	RXO_IASITE_OF_SC    1092
#define	RXO_SITE_OF_STRING  1093
#define	RXO_CONNECT_OF_STRG	1094
#define	RXO_RB_CHILD    	1095
#define	RXO_OFF_OF_PS    	1096
#define	RXO_POS_OF_RSITE   	1097
