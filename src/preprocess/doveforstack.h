// doveforstack.h: interface for the doveforstack class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DOVEFORSTACK_H__59B0F2F8_B208_47AD_AA92_84B94FE60722__INCLUDED_)
#define AFX_DOVEFORSTACK_H__59B0F2F8_B208_47AD_AA92_84B94FE60722__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AMGLevel.h"
#include "dovestructure.h"

class doveforstack : public dovestructure  
{
public:
	int SaveAsPLT(const char *,const int,const int,const int);
	int Save(const char*p_filename=0);
	int PrintLayerStrain(FILE *fp, const double p_u, const double p_v,  double p_eps[3]);
	int PlyStrains(const double p_u, const double p_v,
							 double p_eps[3],
							 double &plyminX,double &plymaxX,
							 double &plyminY,double &plymaxY,
							 double &plymaxXY);

	double LayerThickness(const AMGLevel level, const ON_2dPoint &ptUV);
	int Rebuild_Nu();
	int Rebuild_Kg();
	int Filter(int type, int p1,int p2);
	int Filter(ON_NurbsSurface*p_s, HC_KEY p_surf,int type, int p1,int p2);
	int SaveAsPLT(const char *fname ,
				  const int p_nr, 
				  const int p_nc,
				  const int FromStack, 
				  ON_NurbsSurface * p_blankmembrain = NULL);
	int BuildFromMM(const char *filename, const RXmaterialMatrix p_t);
	int DD_From_PliesWRTRefDir(const double u, const double v, 
		ON_Matrix&p_m,
		ON_2dVector *pref2d=0, // the reference vector in U,V space.
		ON_3dVector *pref3d=0);

	ON_2dVector GetReferenceDirection2D(const double pu, const double pv);
	int HowManyLayers(const int flag);
	doveforstack();
	doveforstack(const int i);
	virtual ~doveforstack();
	int GradAlpha(
		const double u, // in domain 0 to 1
		const double v, // in domain 0 to 1
		const AMGLevel  level,// lowest is level 0; If the level doesnt exist, the fn returns 0;
		ON_2dVector *gradAlpha,// the directional derivative {da/du,da/dv}
		int p_DALPHADUV 
		) ;
	int AlphaFromRefDir(
		const double u, // in domain 0 to 1
		const double v, // in domain 0 to 1
		const AMGLevel level,// lowest is level 0; If the level doesnt exist, the fn returns 0;
		double *a, double*wf,	// the angle of this ply in radians from the ref vector.
		ON_2dVector *pref2d=0, // the reference vector in U,V space.
		ON_3dVector *pref3d=0// the reference vector in cartesian space, calculated from ON_Surface *m_mould
);

};
#if !defined(RXLIB) && !defined(AMGTAPER)
EXTERN_C int PutCV(HC_KEY p_Surf,int i, HPoint pc); 
#endif
#define _PUTCV_DEFINED
//#endif
#endif // !defined(AFX_DOVEFORSTACK_H__59B0F2F8_B208_47AD_AA92_84B94FE60722__INCLUDED_)
