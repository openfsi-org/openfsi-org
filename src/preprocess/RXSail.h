#ifndef _RXSAIL_HDR_A38E7B6B9C6C4341A5CA6DEBF5F9C02A
#define _RXSAIL_HDR_A38E7B6B9C6C4341A5CA6DEBF5F9C02A

#include "layerobject.h"
#include "RXENode.h"
#include "vectors.h"
#include "offset.h" // for trisstruct
#include <QString>
#include "reportform.h" 
#include "stringtools.h"
#include <vector>
#include <map>
#include <string>
#include <queue>

// Werner sail++ definitions
#define SPP_U_COSINE 2
#define SPP_V_COSINE 4

using namespace std;
class RX_FETri3;
class RX_FEEdge;
class RX_FEObject;
class RX_FESite;
class RelaxWorld;
class RXEntity;
class RXEnode;
class RXON_NurbsSurface;
class GLC_StructOccurence;

struct ltstr
{
    bool operator()(const char* s1, const char* s2) const
    {
        return strcmp(s1, s2) < 0;
    }
};

typedef map<string, RXEntity * >		elist_t;
typedef elist_t::iterator			entiter_t;
typedef elist_t::const_iterator	ent_citer;

#define GNBLOCKSIZE  100


#define NUM_OF_FLAGS 20

// options for model searching
#define RXSAIL_ALL   		-1
#define RXSAIL_ISBOAT		2
#define RXSAIL_ISHOISTED	4 	// by default, not the boat
#define RXSAIL_ALLHOISTED	8	// meant to include the boat
#define RXSAIL_NOTBOAT_NORHOISTED	0

// options for MapExtractList. 
#define  PCE_NOFILTER      0
#define  PCE_ISNOTRESOLVED 1
#define  PCE_ISRESOLVED	2

//options for sail flags  
// BUT THEY ARE A MESS>  WHAT DO THEY MEAN????????????
#define FL_HASGEOMETRY 0
#define FL_HASPANELS 1
#define FL_HASMESH 	2
#define FL_HASNEWGEOMETRY 3
#define FL_HASLACKOFFIT 4 // synonym of Needs_RUnning

#define FL_HASNEWTOPOLOGY 5
#define FL_ONFLAG	6

#define FL_NEEDS_GAUSSING	7
#define FL_NEEDS_POSTP  	8
#define FL_NEEDS_TRANSFORM	9
#define FL_NEEDS_RUNNING	4 // synonym of Has Lack of fit
#define FL_NEEDS_SAVING		11

#define FL_HAS_AERODATA 12
#define FL_HAS_PRESSURES 13 // OFf when we hoist. ON when we set pressures. Used for FSI relaxation

#define FL_ISFROZEN	18
#define FL_ISBOAT	19
#define FL_ISTPN	17
#define NUM_OF_FLAGS 20

//definitions for  RXSail::Flag_xxx
#define NO_BEAM_POST    2
#define NO_UV_MAP       4

class RXSail : 
        public layerobject, public RXENode
{
public:
    RXSail(void);
    RXSail(RelaxWorld *p_rw);
    ~RXSail(void);
    RXSTRING GetName() const;
    RXEntity_p Insert_Entity(const char *type, const char *name,
                             void *dp,const HC_KEY hk,const char*att,const char *line,
                             const int GuaranteeUnique=0) ; // its ugly that 'name' isnt const.  Maybe the code relies on a side-effect.

    class RXEntity * NewEntity(const int p_type, const char*name);
    int Pre_Process(const char*fname) ;
private:
    int Initialize();
    RXSTRING  m_SName;
    void SetName(const RXSTRING & p_Name);

public:

    class RXViewFrame *m_viewFrame;
    int Needs_Saving(void ) const;
    int Hoist(void);
    int Drop(void);

    int RemoveFromSummary();
    int AddToSummary( );
    int DeleteSailReferences(void);
    void SetGraphic(Graphic *g);
    class RXSubWindowHelper *GetGraphic( ) const{ return m_Graphic;}
    ON_String Serialize();
    int Deserialize(const char *p_s);
    bool SaveStateIfNecessary(void);
    bool LoadStateIfNecessary(void);
    int PostProcessOneModel(const int wh);
    int Peters_Generate_Geometry();
    int Mesh();
    int Read_Macro( FILE *cycleptr,int *depth);

    double MeshSize(const RXSitePt &p) const;
    class RXQuantity* MeshExpression() const {return this->m_mexp;}
    void SetMeshExpression(class RXQuantity* e ) {this->m_mexp = e;}
    int SetUpTransform(const int On_Deflected, ON_Xform &mtx) const;//double mt[4][4],
    int EvaluateAllReadOnly();
    int PostSomeResults() ;
private:
    int Mesh_All_Panels();
    int Mesh_All_PSides();
    int Mesh_All_Strings(); //pockets too
    int Mesh_All_BeamStrings();
    int Mesh_All_RigidBodies();
    int PostMesh();


private:
    class RXQuantity* m_mexp; // Mesh Expression

public: 
    bool IsOK(const char*p_s=0) const;
    int SetFlag(const int which,const int value=1) ;
    int ClearFlag(const int which);
    int GetFlag(const int which) const;

    // we keep the model scriptfile pathname and use it
    // to  contract filenmes for saving and lengthen them for use.
    void SetFileIN( const QString &lp) ;
    QString ScriptFile() ;
    QString RelativeFileName(const QString & longFile ,const QString &base = QString());
    QString AbsolutePathName(const QString & shortFile ,const QString &base = QString());

    // for Werner's sail++
    int WernerPoints(const int N, std::vector<RXSitePt> &vv , std::vector<RXSitePt> &cps, const int flags );
    int SetUpWernerPressures( std::vector<RXSitePt> cps, std::vector<double> dp );

    int startmacro( const char *File,int *depth);
    bool IsHoisted()const;
    bool IsBoat()const;
    int PrintModelSummary(FILE *fp,const int p_short=1);
    int ZeroSpecialLists(RXEntity * const p_p=0);
    std::string GetType()const;
    const QString GetQType()const {return QString::fromStdString(m_type);}


    RelaxWorld* GetWorld() const{return m_rw;}
    int Delete_Whole_Database();
    int DeleteSomeEnts(const int type );
    RX_FESite* GetSiteByNumber(const int p_n) const;

    double Get_Init_Edge_Length(const int theEdge)const;


    ON_String m_attributes;
    ON_3dVector Normal()const{
        return ON_3dVector(m_Zaxis.x,m_Zaxis.y, m_Zaxis.z );}
    ON_Xform Trigraph(void)const ; // same  calc as RXEntity::Trigraph
    QString m_fileBAG;		/* xxxx.bag */


private:
    QString m_fileIN;
    ON_3dVector m_Zaxis;
    int m_Flags[NUM_OF_FLAGS];
    std::string m_type;		/* eg GENOA */
    RXEntity_p m_mould, GDPField;
public:
    RXEntity_p GetMould()const{return this->m_mould;}
    void SetMould(RXEntity_p p){ this->m_mould=p;}

    RXEntity_p m_QuadToUnitSquare; // used in RXShapeInterpolation::ToUnitSquare only
    /* HOOPS DISPLAY STUFF */
#ifdef HOOPS
    HC_KEY Hoops_Strings;
    HC_KEY Hoops_Residuals;
    HC_KEY Hoops_Nodes;
#elif defined (RXQT)
    RXGRAPHICSEGMENT m_postProc;
#endif
    class RXMirrorDBI *m_pSailSum;
    /// tried  class RXLogFile
#define NODEBLOCKSIZE  7
#define EDGEBLOCKSIZE  17
#define TRIBLOCKSIZE   11

    vector<RX_FESite*> m_Fnodes;
    vector<RX_FEedge*> m_Fedges;
    vector<RX_FETri3*> m_FTri3s;

private:
    queue<int> m_freeNNs;
    queue<int> m_freeENs;
    queue<int> m_freeTNs;
public:

    int PopNodeNo();
    int PushNodeNo(const  int i);
    int NoteNode(RX_FESite*s);

    int PopEdgeNo();
    int PushEdgeNo(const  int i);
    int NoteEdge(RX_FEedge*e);

    int PopTriNo();
    int PushTriNo(const  int i);
    int NoteTri(RX_FETri3*e);

    //  loading  and coordinate mapping data
private:

    class RXShapeInterpolationI *m_uv2xyz;
    class RXShapeInterpolationI *m_xyz2uv;
    int DumpInterpolations() const;
    class RXPressureInterpolationP *m_pressInt;
public:
    double m_Pressure_Factor;
    class RXShapeInterpolationI *GetMesh() {return this->m_uv2xyz ;}
    int ReadOneFilePressure(const RXSTRING fname, int guess_at_V );

    int MakeUVMapping_XYPlane(); // the original
    int MakeUVMappingFromFEMesh(const int flag=0); // doesnt generate a nw triangulation so it is independent of orientation
    ON_3dPoint CoordsAt(const ON_2dPoint& p,const RX_COORDSPACE s);
    ON_3dVector DeflectionsAt(const ON_2dPoint& p,const RX_COORDSPACE s);
    ON_3dVector VelocityAt(const ON_2dPoint& p,const RX_COORDSPACE s);
    void LocateInit();
    int Tidy() ;
public:
    int panel_count;
    set<class RXEntity*> m_garbage;

protected:
    elist_t m_emap;
    string EntityKey(const char *name,const char *type) const;
    string EntityKey(const RXEntity* p) ;
    bool m_maphaschanged;
public:
    ent_citer MapStart(void){m_maphaschanged=false; return m_emap.begin();}
    ent_citer MapEnd(void)const{ return m_emap.end();}
    ent_citer MapIncrement(ent_citer it);
    size_t MapSize()const { return m_emap.size();}
    int MapInsertObject( RXEntity * p);
    int MapRemoveObject( RXEntity * p); // It may not exist
    int MapDump( FILE *fp=stdout);
    size_t MapExtractList(const int type, vector<RXEntity * >&thelist, const int filter=PCE_NOFILTER) const;

    int ListAllExpressions(FILE *fp);
    int ExportAllExpressions(wostream &s);

    //Entity_Exists is the fastest searcher: no checks. 'type' must be a valid entity type. No alias
    RXEntity_p Entity_Exists(const char *name,const char *type) const;

    // getKey (const RXSTRING..  is like Entity_Exists except spin can be a comma separated list of entity types. no alias

    RXEntity_p GetKeyNoAlias(const RXSTRING spin, const RXSTRING name) const;// deprecated.

    // unlike getKey (const RXSTRING.. , this variant permits  alias'
    RXEntity_p GetKeyWithAlias(const char*spin, const char*name) const;


    //		a highly doctored entity search designed for the Resolve system.
    //		only returns resolved entities. except if PleaseResolve,  launches a recursive resolve on any unresolved hits
    // 		on top of that, it interprets various special strings
    RXEntity_p Get_Key_With_Reporting(const RXSTRING spin,const RXSTRING name,bool PleaseResolve=false) {
        return this->Get_Key_With_Reporting(ToUtf8(spin).c_str(),ToUtf8(name).c_str(), PleaseResolve);
    }
    RXEntity_p Get_Key_With_Reporting(const char *spin,const char *name,bool PleaseResolve=false);
    int Parse_Seam_Position(const char*name, char *seamname,char *offset) ;
    RXEntity_p GetFirst(const RXSTRING spin) const;
    RXEntity_p GetFirst(const char* spin) const;
    RXEntity_p Get_Aliassed_Entity( const char *spin,const char *name) const;

    HC_KEY DrawPlotps(RXGRAPHICSEGMENT k) const;
    HC_KEY DrawStrainVectors (RXGRAPHICSEGMENT k) const;
    HC_KEY DrawPrinStiff(RXGRAPHICSEGMENT k) const;
    HC_KEY DrawMatRefVector(RXGRAPHICSEGMENT k) const;

    int CheckAndDisplayPanels( ) ;

    HC_KEY DrawFilaments( const HC_KEY p_k) const;
    int DrawStrings(RXGRAPHICSEGMENT p_k) ;
    int DrawPanelEdges(RXGRAPHICSEGMENT p_k) ;
    int DrawTriangles(double scale,RXGRAPHICSEGMENT p_k);
    int Display_Panel_Colours(HC_KEY theShell);
    HC_KEY DrawDesignWindow( ) ;
    HC_KEY DrawDeflected(RXGRAPHICSEGMENT p_k);

public:
    RXEntity* m_Selected_Entity[RX_MAX_TYPES]; // feb 2005
    int Some_Deleted;

#ifdef __cplusplus
    struct PC_3DM_Model *m_p3dmModel;//PC_3DM_Model
#else
    void *m_p3dmModel;
#endif
    double m_Linear_Tol;//=0.001;
    double m_Angle_Tol;// = 0.0002;
    double m_Cut_Dist ; // normally at least 2x Linear Tol

    struct TRISTRESREULTS  *m_StressResults;

protected:
    RelaxWorld *m_rw;
    int m_SailListIndex;
    int m_LoadingIndex;
    class  RXSubWindowHelper *m_Graphic;

    int SetSailListIndex(const int p_i)  { m_SailListIndex=p_i; return m_SailListIndex;}

    int R_Solve();
    int GoalSeek(  int *RE_PANEL_FLAG);
public:
    int ReBuild_All_Materials(const RX_COORDSPACE space);
    int SetLoadingIndex(const int i=0); // was 'pansail index' the loading index, starting with 1,  hoisted only
    int SailListIndex() const { return m_SailListIndex;}
    int GetLoadingIndex() const { return m_LoadingIndex;}
private:
    ON_String m_tempstatefile;
    class rxON_Mesh *MakeONMeshDeflected( );

public:
    int FreezeStrings(const int flags);
    int Write_Attributes(const QString &p_Filename);
    int Write_3dm(const char * p_Filename);
    int Write_Script(const char *File,const int which) ;
    int Refresh_PC_File( const char *filename=0);
    int ReDove( );
    int WriteNastran(const char*p_fname);
    int Write_Iges(const char*fname,const int nu,const int nv,const int degree=3);
    int Write_uvdp(const char* fname);
    int WriteVtkSurface(const QString fname);
    int WriteVtkStrings(const QString fname);
    int Write_stl(const char* fname);
    int save_state(const char *filename)  ;
#ifdef USE_PANSAIL
    int WritePansailPressures( const char*filename);
    int m_N;
    int m_M;
#endif
public: 


    HC_KEY PostProcExclusive()const ;
    HC_KEY PostProcNonExclusive( )const;
    HC_KEY PostProcTransformSeg()const;

    int MakeLastKnownGoodDxDyDz();
    int Post_Displacements();
    int Post_GlobalMeasures();
    static int GetFortranNoDeno(const char *model,const char* nname);
    ON_3dPoint XToGlobal(const ON_3dPoint &p_in);
    ON_3dPoint XToLocal(const ON_3dPoint &p_in);
    ON_3dPoint VToGlobal(const ON_3dPoint &p_in);
    ON_3dPoint VToLocal(const ON_3dPoint &p_in);

    ON_2dPoint CoordModelXToUV(RXSitePt *q,int*err) const;// uses (uv) of q as a seed. Fills in UV of q and returns (uv)


    static class RXSail *LoadIndexToModel(const int mno) ;
    int read_ND_state(const char *filename);
    int write_ND_state(const char *filename)  ;
    double ReferenceLength();
    class RXON_NurbsSurface* GetMouldSurface() const;

    int SetUpCustomPressures(const RXSTRING file, char*p_args=0);
    int SetUpFilePressures(const RXSTRING file, char*p_args=0);
#ifdef USE_PANSAIL
    int SetUpPansailPressures();
#endif
    int SetOneConstantPressure(const double val);
    int SetOneFieldPressure(); // looks for a scalar field named 'pressure'
    int SetOneInterpolatedPressure(); // looks for a 'interpolation surface' named 'pressure'
    int SetOnePressures(map<int,double>*plist=0); // wasteful to use a map but it saves bounds overflow
    int AddOnePressures(map<int,double>*plist);
    int ApplyPressureList(map< int,double> plist);
    int SetOneConstantPreStress( const double val[3]);
    struct REPORT_FORM *GetReportForm(void){return (&m_ReportForm);}

protected:
    struct REPORT_FORM m_ReportForm;

    void SortReportForm();
    int Resolve_All_Entities();

public:

    virtual int Compute();
    virtual int Resolve();
    virtual int Finish();

    int UnResolveEntities(const char *type, const char *name );
    // a variable format for exporting part-built models
    int Dump(const char* fname);
    HC_KEY  LoadSailResults(int p_state,int what,int updateGeom);
private:
#ifdef HOOPS
    VECTOR *m_HoopsCoords; int m_HoopsPcount;
    int *m_HoopsFlist;
    int m_HoopsFcount;
    HC_KEY m_HoopsShell;
     int ntri;
#endif

    TriStressStruct *m_tridata;

};

EXTERN_C int write_pcfile(RXSail **ps, const char *filename);


#endif

