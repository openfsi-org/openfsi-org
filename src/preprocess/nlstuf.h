 /* headers for  nlstuf.f */

 /*   SUBROUTINE Uni_Stress_Test(theta,etheta,N,stheta,retVal)
        IMPLICIT NONE
        REAL*8 theta, etheta
        INTEGER N

C  returns
        REAL*8 Stheta
         INTEGER retval  ! o for failure */

#ifndef NLSTUF_16NOV04
#define NLSTUF_16NOV04


extern "C" void uni_stress_test_(double*th, double*et, int *N, double*sthm, int*r);



#endif //#ifndef NLSTUF_16NOV04
