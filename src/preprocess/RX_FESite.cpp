#include "StdAfx.h"
#include <QDebug>
#include "traceback.h"
#include "RXSRelSite.h"
#include "RXSail.h"
#include "f90_to_c.h"
#include "cfromf.h"
#include "words.h"
#include "panel.h"
#include "etypes.h"

#include "RX_FESite.h"


RX_FESite::RX_FESite(void)
    : m_InRedSpace(true)
{
    cout<< "don't use the default RX_FESite constructor"<<endl; assert(0);
    Init();
}

RX_FESite::RX_FESite(class RXSail *p)
    :  RX_FEObject(p), m_InRedSpace(true)
{
    int err,n;
    assert(p);

    Init();
    assert(RX_FEObject::GetSail()==p);
    n=GetSail()->PopNodeNo();
    SetN(n);

    GetSail()->NoteNode(this); // means put into m_Fnodes
#ifdef FORTRANLINKED
    cf_createfortrannode(p->SailListIndex(),GetN(),this,&err);//add a fortan FNODE to nodelist
#else
    assert(0);
#endif
    SetIsInDatabase();
}


RX_FESite::~RX_FESite(void)
{
    int n, err=0; n=GetN();
    assert(n != RXOBJ_INITIAL_N) ; // would mean that its a local
    GetSail()->m_Fnodes[n]=0;
    GetSail()->PushNodeNo(n);

    if( IsInDatabase()) {
        assert(n<= GetSail()->m_Fnodes.capacity());

#ifdef FORTRANLINKED
        assert(GetSail() );
        cf_removefortrannode(GetSail()->SailListIndex(),n,&err);
#else
        assert(0);
#endif
        SetIsInDatabase(false);
    }
    else
        printf(" deleting FESite %d NOT in DB(!!??)\n", n);
} //RX_FESite::~RX_FESite(void)


int RX_FESite::Init(void)
{
    assert(GetSail());
    x=y=z=ON_UNSET_FLOAT;
    m_defls.x=m_defls.y=m_defls.z=0.0;
    SetN(RXOBJ_INITIAL_N);
    RXSitePt::Init();	// Analysis deflections  in this models black-space
    SetIsInDatabase(false);
    return 1;
}

bool RX_FESite::Set(const ON_3dPoint &p) // p is model space, undeflected
{

    if(IsInDatabase() )  {
        int err=0;
        SAIL *sail=this->GetSail();
        ON_3dVector dd = this->Deflection(); // modelSpace
        ON_3dPoint pt;// =  GetLastKnownGood(RX_MODELSPACE) ; // new coords + old deflections
        pt = p+dd;
        double xx[3]; xx[0]=pt.x; xx[1]=pt.y; xx[2]=pt.z;
        cf_FillFNode(sail->SailListIndex(),GetN(),xx,&err);
        return (err ==0);
    }
    return true;
}
bool RX_FESite::SetDeflection(const ON_3dVector d)// d in Model space
{
    m_defls = d;
    return Set(ToONPoint());  // this picks up the new defl and updates the FEA DB
}

const ON_3dPoint RX_FESite::DeflectedPos( RX_COORDSPACE base  ) const
{ 
    double v[3];
    SAIL *sail=this->GetSail();
    switch (base) {
    case RX_GLOBAL:
        cf_get_coords( sail->SailListIndex(), this->GetN(),v);
        return ON_3dPoint (v);
    case RX_MODELSPACE:
        cf_get_coords( sail->SailListIndex(), this->GetN(),v);
        return  sail->XToLocal(v);
    default:
        assert(0);

    };
    return ON_3dPoint(ON_UNSET_VALUE, 0,0);
}


bool RX_FESite::IsRedSpace(void)const
{
    return m_InRedSpace;
}


int RX_FESite::PrintShort(FILE *ff) const
{
    return( fprintf(ff,"%6f %6f %6f :%d : %6f\n",x, y, z, GetN(), getDs()) );
}
int RX_FESite::Dump(FILE *fp) const
{
    int rc=0;

    rc+=fprintf(fp,"\tSail is '%s' '%S'\n", GetSail()->GetType().c_str(),
                GetSail()->GetName().c_str());

    rc+=fprintf(fp,"\t%11d\tx=%f\ty=%f\tz=%f\n", GetN(), x, y, z);
    ON_3dPoint pp= DeflectedPos( RX_GLOBAL) ;
    rc+=fprintf(fp,"\tGlobalspace\tx=%f\ty=%f\tz=%f \t(current analysis coords)\n",  pp.x,pp.y, pp.z);
    pp= DeflectedPos( RX_MODELSPACE) ;
    rc+=fprintf(fp,"\tModel space\tx=%f\ty=%f\tz=%f\n",  pp.x,pp.y, pp.z);

    ON_3dPoint vvv = GetLastKnownGood(RX_MODELSPACE) ; // model space.
    rc+=fprintf(fp,"\tlastknowngd\tx=%f\ty=%f\tz=%f\n", vvv[0],vvv[1],vvv[2]);

    rc+=fprintf(fp," \t  N   \t  U   \t   V\n");
    rc+=fprintf(fp," \t%d \t%12.12g \t%12.12g\n", GetN(), m_u, m_v);
    rc+=fprintf(fp,"     dS, gDp,ldp \t%f\n", getDs());
//    rc+=fprintf(fp,"     deflections \t%f \t%f \t%f\n", m_ d.x, m_ d.y,m _d.z);

    rc+=fprintf(fp,"\t   \tdx=%f\tdy=%f\tdz=%f\n",Deflection().x,Deflection().y,Deflection().z);

    rc+=fprintf(fp," flags:\t");
    if(m_Site_Flags&MOULDED)  			rc+=fprintf(fp,"Moulded...");
    if(m_Site_Flags&PCF_SKETCH)			rc+=fprintf(fp,"sketch Site...");
    if(m_Site_Flags&PCF_EXPORT_DISP)	rc+=fprintf(fp,"Export_Displacement request...");
    if(m_Site_Flags&PCF_EXPORT_COORDS)	rc+=fprintf(fp,"Export_Coordinate request...");
    if(m_Site_Flags&PCF_ISONEDGE)		rc+=fprintf(fp,"edge node..");
    if(m_Site_Flags&PCF_ISMESHSITE )		rc+=fprintf(fp,"mesh site ..");
    if(m_Site_Flags&PCF_ISFIELDNODE )		rc+=fprintf(fp,"field node.."); else 	rc+=fprintf(fp,"NOTfieldNode..");


    rc+=fprintf(fp,  "\nEnd RX_FESite Dump\n"); fflush(fp);

    return rc;
}
int RX_FESite::IsEdgeNode()const {
    int rc=0;
    // its an edgenode if:
    // A) one of its  CUrve s an edge
    // B) one of its PSList ->dataptr->sc  ->edge is true.
    if(m_Site_Flags&PCF_ISONEDGE)
        return 1;
    return 0;
}// IsEdgeNode
RXSTRING  RX_FESite::TellMeAbout(void)const{
    return RXSTRING  (_T("RX_FESite:: ")) +RXSitePt::TellMeAbout ()+RX_FEObject::TellMeAbout ();
}
ON_3dPoint RX_FESite::GetLastKnownGood(RX_COORDSPACE base ) const //World space NOT THREADSAFE
{
    ON_3dPoint q, xw;
    q.x = x+m_defls.x;
    q.y = y+m_defls.y;
    q.z = z+m_defls.z;

    switch (base) {
    case RX_GLOBAL:
        xw =  this->GetSail()-> XToGlobal(q);
        break;
    case RX_MODELSPACE:
        xw=q;
        break;
    default:
        assert(0);
    };
    return xw;
}
int fc_getlastknowngood(RX_FESite*cptr, double *x){

    ON_3dPoint p = cptr->GetLastKnownGood(RX_GLOBAL); //
    x[0]=p.x;
    x[1]=p.y;
    x[2]=p.z;
    return 1;
}
int fc_getundeflected(RX_FESite*cptr, double *x){ //global

    class RXSRelSite *r = dynamic_cast<class RXSRelSite *>(cptr);
    if(!r) {
        qDebug()<< " cant cast in fc_getundeflected "; assert(0);
        x[0]=x[1]=x[2]=0;
        return 0;
    }
    ON_3dPoint p , pl= *r;
    p= r->Esail->XToGlobal(pl);
    x[0]=p.x;
    x[1]=p.y;
    x[2]=p.z;
    return 1;
}

int unhookfromfea( class RX_FESite * const ptr){
    if(!ptr) return 0;
    ptr->SetIsInDatabase(false);
    return 1;
}

int RX_FESite::SetTrace(const int i) {

#ifdef FORTRANLINKED
    cf_set_trace( this->GetSail()->SailListIndex(), this->GetN(),i);
#endif
    return 0;
}
int RX_FESite::SetLoads(const double p[3]){
#ifdef FORTRANLINKED
    cf_set_nodalload( this->GetSail()->SailListIndex(), this->GetN(),p);
#endif
    return 0;
}
int RX_FESite::MakeSixNode(void)
{	int rc=0;
    QString  sbuf;
    char buf[256]; int L=256;
    int sli = this->GetSail()->SailListIndex();
    int nn = this->GetN();
    if(0 !=cf_issixnode(sli,nn) )
        return 0;
    RX_FESite*s = new RX_FESite(this->GetSail());
    int n2 = s->GetN();
    s->x=s->y=s->z=0;

    RXSRelSite *ee = dynamic_cast<RXSRelSite *> (this);
    if(ee->AttributeGet( "$rotfixity" ,sbuf)){
        strcpy(buf,qPrintable(sbuf ) );
        cf_setnodalfixing (sli,n2,buf,L); // scary to call this here
    }
    // to set NeedsComputing on any rigid bodies which refer to this node
    RXSRelSite *rs = dynamic_cast<RXSRelSite *>(this);
    rs->OnValueChange(); rs->Esail->SetFlag(FL_NEEDS_GAUSSING);
    rc=cf_makesixnode(sli,nn,n2) ;
    return rc;
}

