
#include "StdAfx.h"

#include "RLX3dm.h"

#include "nurbs.h"
#include "assert.h"
#include "rlx3dmbuffer.h"
#include "entities.h"

//int Resolve_3DM_Card(RXEntity_p e)
//{
  
//	return Resolve_3DM_Card_cpp(e);
//}//int Resolve_3DM_Card


double EvaluateONNurbsSurface(struct PC_NURB* p_pNurbs,const double u,const double v,ON_3dPoint *r) // Peter Sept 2005 we should forget about the [0->1 ] domain
{	
	assert(r);
	assert(p_pNurbs ); assert(p_pNurbs->m_pONobject );
	if (!p_pNurbs)
		return 0;
	if (!p_pNurbs->m_pONobject)
		return 0;
	assert(ON_IsValid(u)); // peter sept 2006
	assert(ON_IsValid(v));
	ON_Surface * l_pSurface = ON_Surface::Cast((ON_Surface*)(p_pNurbs->m_pONobject));
	
	double l_u, l_v, l_l[2],l_min[2];
	l_u=l_v=l_l[0]=l_min[0]=l_l[1]=l_min[1]=0.0;
	if (l_pSurface)
	{
		ON_Interval l_domain[2]; //TO convert a [0,1] doamin to a variable domain
		for (int i=0;i<2;i++) l_domain[i] = l_pSurface->Domain(i);
		l_min[0]=l_domain[0].Min();
		l_l[0] = l_domain[0].Length();
		l_u = l_min[0]+u*l_l[0];
		l_min[1]=l_domain[1].Min();
		l_l[1] = l_domain[1].Length();
		l_v = l_min[1]+v*l_l[1];
		if (!(l_pSurface->EvPoint(l_u,l_v,(*r))))
			return 0;
	}
	else {assert("(EvaluateONNurbsSurface) ::its not a surface "==0);
		return 0;
	}

	
	return r->z;
}//double EvaluateONNurbsSurface


int EvaluateONNurbsDerivatives(struct PC_NURB* p_pNurbs,const double u,const double v,
										 ON_3dPoint*r,
										 ON_3dVector*r10, ON_3dVector*r01,
										 ON_3dVector*r11, ON_3dVector*r20, ON_3dVector*r02)



{
	if (!p_pNurbs)
		return 0;
	
	ON_Surface * l_pSurface = ON_Surface::Cast((ON_Surface*)(p_pNurbs->m_pONobject));

	if (!l_pSurface)
		return 0;
	
	ON_3dPoint l_pt;
	ON_3dVector l_du;
	ON_3dVector l_dv;
	ON_3dVector l_duu;
	ON_3dVector l_duv;
	ON_3dVector l_dvv;

	ON_Interval l_domain[2]; //TO convert a [0,1] doamin to a variable domain
	int i;
	for (i=0;i<2;i++) l_domain[i] = l_pSurface->Domain(i);

	double uscale =l_domain[0].Length() , Vscale = l_domain[1].Length();	

	double l_u = l_domain[0].Min()+u*uscale;
	double l_v = l_domain[1].Min()+v*Vscale;


	if (!(l_pSurface->Ev2Der(l_u,l_v,l_pt,l_du,l_dv,l_duu,l_duv,l_dvv)))
		return 0;
	
	assert(r);assert(r10);assert(r01);assert(r11);assert(r20);assert(r02);

	
	*r = l_pt;

	*r10 =	l_du	*uscale;	 
	*r01 =	l_dv	*Vscale;	
	*r11=	l_duv	*uscale*Vscale;	
	*r20 =	l_duu	*uscale*uscale;	
	*r02 =	l_dvv	*Vscale*Vscale;	
	return 1;
}//int EvaluateONNurbsDerivatives
