#include "RXGraphicCalls.h"
#include "rxmainwindow.h"
#ifdef USE_GLC
#include "glwidgetpreprocess.h"
#endif
#include <iostream>
#include <assert.h>
using namespace std;
//#ifdef NO_RXGRAPHICS
//#define DISABLE_GRAPHICS (1)
//#else
//extern "C" int RXGraphicsEnabledQ();
//#define DISABLE_GRAPHICS (!RXGraphicsEnabledQ())
//#endif
int silent = 1;

void HC_Update_Display()
{
    //if(!silent)
    //       cout<<"update Display "<<endl;
    // rxWorker::UpdateAllGraphics();   this should do them all:
}


RXGRAPHICSEGMENT RXNewWindow(const std::string caption, void*ptr)
{
    RXGRAPHICSEGMENT rc = rxWorker::NewWindow( QString(caption.c_str()) , ptr);
    return rc;
}
int RXDeleteWindow( void*ptr)  // called with a ptr to RXViewFrame
{
       return rxWorker::DeleteWindow(ptr);
}

RXGRAPHICSEGMENT RXNewGNode(const char*caption,RXGRAPHICSEGMENT pParent)
{
    return rxWorker::NewGNode(caption,pParent);
}

RXGRAPHICSEGMENT RXGNodeAddChild(RXGRAPHICSEGMENT pParent,RXGRAPHICSEGMENT pChild)
{
    return rxWorker::GNodeAddChild(  pParent, pChild);
}

RXGRAPHICSEGMENT RXGNodeRemoveChild(RXGRAPHICSEGMENT  pParent, RXGRAPHICSEGMENT  pChild)
{
    return rxWorker::GNodeRemoveChild(  pParent, pChild);
}


int RXGNodeWrite(const char*filename,RXGRAPHICSEGMENT root)
{
    return rxWorker::GNodeWrite(filename,root);
}

RXGRAPHICOBJECT RX_Insert_Polyline(const int count,const float*poly,RXGRAPHICSEGMENT o, const char*color)
{
     if( DISABLE_GRAPHICS ) return 0;
    if(!o) return 0;

    RXGRAPHICOBJECT rc = rxWorker::InsertPolyline(count,poly,o);
    if(color) rxWorker::SetColor(rc,color);
    return rc;
}
RXGRAPHICOBJECT RX_Insert_NurbsCurve(const class ON_NurbsCurve*c,RXGRAPHICSEGMENT  o, const char*color)
{
         if( DISABLE_GRAPHICS ) return 0;
    if(!o) return 0;
    RXGRAPHICOBJECT rc=rxWorker::InsertNurbsCurve(c,o);
    rxWorker::SetColor(rc,color);
    return rc;
}
RXGRAPHICOBJECT HC_KInsert_Polygon(const int count,const float*poly,RXGRAPHICSEGMENT o,const char*color)
{
         if( DISABLE_GRAPHICS ) return 0;
    if(!o) return 0;
    RXGRAPHICOBJECT rc;
    rc=rxWorker::InsertPolyline(count,poly,o);
    if(color) rxWorker::SetColor(rc,color);
    return rc;
}


RXGRAPHICOBJECT  RX_Insert_Line( const float a, const float b, const float c,
                                 const float d, const float e, const float f,
                                 RXGRAPHICSEGMENT o,const char*color)
{
     if( DISABLE_GRAPHICS ) return 0;
    float poly[6];
    poly[0]=a;  poly[1]=b;  poly[2]=c;  poly[3]=d;  poly[4]=e;  poly[5]=f;
    RXGRAPHICOBJECT rc;
    rc=rxWorker::InsertPolyline(2,poly,o);
    if(color) rxWorker::SetColor(rc,color);
    return rc;
}
EXTERN_C RXGRAPHICOBJECT RX_InsertPolyhedron(const float* xyz,const int cp,
                                             const int*faceSet, const int cf,
                                             RXGRAPHICSEGMENT o)
{
   if( DISABLE_GRAPHICS ) return 0;
    return rxWorker::InsertPolyhedron(xyz,cp, faceSet,cf,o);
}
//  static RXGRAPHICOBJECT InsertPolyhedron(const float* xyz,const int cp, const int*faceSet, const int cf, RXGRAPHICSEGMENT o);

void HC_Flush_Segment (RXGRAPHICSEGMENT n)
{
    HC_Flush_Geometry(n);
}

void  HC_Flush_Geometry (RXGRAPHICSEGMENT n)
{
    if( DISABLE_GRAPHICS)
        return ;
    if(n)
        rxWorker::FlushGraphicsNode(n);
}
void HC_Exit_Program()
{
    exit(1);
}
/////////////////////// stubs below //////////////////////////////////

RXGRAPHICOBJECT  RX_Insert_Marker ( const float a, const float b, const float c,RXGRAPHICSEGMENT o )
{
    if(!silent)
        cout<< "Insert Marker"<<endl;
    return -0;
}

void HC_Delete_By_Key(const HC_KEY k)
{
    if(!silent)
        cout<<"Delete_By_Key "<<k<<endl;
}
void HC_Delete_Segment(const char *s)
{
    if(!silent)
        cout<<"Delete_Segment "<<s<<endl;
}



void HC_Flush_Contents(const char *a, const char *b)
{
    if(!silent)
        cout<<"Flush_Contents "<<a<<b<<endl;
}


///////////////////////// Now the standard ones///////////////////////////////////
void HC_Flush_Segment (const char*s)
{
    if(!silent)
        cout<<"Flush_Segment "<<s<<endl;
}
void HC_Flush_Geometry(const char*s)
{
    if(!silent)
        cout<<"Flush_Geometry "<<s<<endl;
}

void 	HC_Set_Line_Weight(const double a)
{
    if(!silent)
        cout<<"Set_Line_Weight"<<a<< endl;
}


int List_Open_Segments(void) {return 0;}


//void  HC_Begin_Segment_Search(const char *s)
//{
//    if(!silent)
//        cout<<"Begin_Segment_Search "<<s<<endl;
//}

//int  HC_Find_Segment( char*buf)
//{
//    if(!silent)
//        cout<<" Find Segment "<<endl; *buf=0;
//    return 0;
//}

//void HC_End_Segment_Search()
//{
//    if(!silent)
//        cout<<"End_Segment_Search "<<endl;
//}

//void HC_Begin_Contents_Search(const char *s, const char*t)
//{
//    if(!silent)
//        cout<<"Begin_Contents_Search "<<s<<","<<t<<endl;
//}

//int  HC_Find_Contents(const char *type, HC_KEY *k)
//{
//    if(!silent)
//        cout<<" Find COntents "<<type<<endl; *k=0;
//    return 0;
//}

//void HC_End_Contents_Search()
//{
//    if(!silent)
//        cout<<"End_Contents_Search "<<endl;
//}



void HC_Open_Segment(const char *s)
{
    if(!silent)
        cout<<"Open_Segment "<<s<<endl;
}

void HC_Include_Segment(const char *s)
{
    if(!silent)
        cout<<"Include_Segment "<<s<<endl;
}


void HC_Style_Segment(const char *s)
{
    if(!silent)
        cout<<"Style_Segment "<<s<<endl;
}

void HC_Set_User_Options (const char *s)
{
    if(!silent)
        cout<<"Set_User_Options "<<s<<endl;
}

void HC_Open_Segment_By_Key( const HC_KEY  k)
{
    if(!silent)
        cout<<"KOpen_Segment "<<k<<endl;
}

void HC_Set_Selectability (const char *s)
{
    if(!silent)
        cout<<"Set_Selectability "<<s<<endl;
}

HC_KEY HC_KOpen_Segment(const char *s)
{
    if(!silent)
        cout<<"KOpen_Segment "<<s<<endl;
    return 0;
}

void HC_Include_Segment_By_Key(const HC_KEY k) 
{
    if(!silent)
        cout<<"Include_Segment_By_Key  "<<k<<endl;
}


//void  HC_Style_Segment_By_Key(const HC_KEY k)
//{
//    if(!silent)
//        cout<<"Style_Segment_By_Key  "<<k<<endl;
//}

void HC_Show_Segment(const HC_KEY k, char*segname)
{
    if(!silent)
        cout<<"Show_Segment "<<k<<endl;  strcpy(segname,"Show_Segment ");
}

void HC_Close_Segment()
{
    if(!silent)
        cout<<"Close_Segment "<<endl;
}


void HC_MSet_Vertex_Colors_By_FIndex(const HC_KEY k,const char*what ,const int i,const int j,float*vv) 
{

}
;
//void  HC_Open_Geometry(const HC_KEY k )
//{
//}

//void  HC_Close_Geometry()
//{
//}

void HC_Set_User_Index(const int i, const void *v) 
{
    if(!silent)
        cout<<"Set_User_Index "<<i<<v<<endl;
}

void HC_Show_One_Net_User_Index(const int i, RXEntity_p *v) 
{
    if(!silent)
        cout<<"Show_One_Net_User_Index"<<i<<endl;  *v=0;
}

void HC_Show_One_User_Index(const int i, RXEntity_p *v) 
{
    if(!silent)
        cout<<"Show_One_User_Index"<<i<<endl;  *v=0;
}


void  HC_Show_Key_Status(const HC_KEY k, char*segname)
{
    if(!silent)
        cout<<"Show_Key_Status "<<k<<endl;  strcpy(segname,"Show_Key_Type ");
}

void HC_Show_Include_Segment(const HC_KEY k, char*segname)
{
    if(!silent)
        cout<<"Show_Include_Segment  "<<k<<endl;  strcpy(segname,"Show_Include_Segment ");
}

void HC_QShow_One_Color(const char *a, const char *b, char *c)
{
    if(!silent)
        cout<<"QShow_One_Color"<<a<<b<<endl;  strcpy(c,"red");
}

int HC_QShow_Existence(const char *a, const char *b)
{
    if(!silent)
        cout<<"QShowExistence "<<a<<b<<endl;
    return 0;
}


void HC_Show_Pathname_Expansion(const char *a,  char *b)
{
    strcpy(b,a);  if(!silent)
        cout<<"Show_Pathname_Expansion "<<a<<endl;
}

void HC_QSet_Color(const char *a, const char *b)
{
    if(!silent)
        cout<<"QSet_Color "<<a<<b<<endl;
}

void  HC_Set_Color_By_Index(const char *a, const int i)
{
    if(!silent)
        cout<<"Set_Color B I "<<a<<i<<endl;
}

//void HC_QSet_Selectability(const char *a, const char *b)
//{
//    if(!silent)
//        cout<<"QSet_Selectability "<<a<<b<<endl;
//}

void HC_QShow_Color(const char *a, char *b)
{
    if(!silent)
        cout<<"QShow_Color "<<a<<endl; strcpy(b,"red");
}

void HC_Set_Color(const char *a) 
{
    if(!silent)
        cout<<"Set Color "<<a<<endl;
}

//void HC_Set_Text_Alignment(const char *a)
//{
//    if(!silent)
//        cout<<"Set_Text_Alignment "<<a<<endl;
//}

void HC_Set_Visibility(const char *a)
{
    if(!silent)
        cout<<"Set Visibility "<<a<<endl;
}

//void HC_Set_Line_Pattern(const char *a)
//{
//    if(!silent)
//        cout<<"Set_Line_Pattern "<<a<<endl;
//}

//void HC_Set_Rendering_Options(const char *a)
//{
//    if(!silent)
//        cout<<"Set Rendering_Options "<<a<<endl;
//}

//void HC_Set_Text_Font(const char *a)
//{
//    if(!silent)
//        cout<<"Set_Text_Font"<<a<<endl;
//}

//void HC_QSet_Text_Font(const char *a, const char*b)
//{
//    if(!silent)
//        cout<<"QSet_Text_Font"<<a<<b<<endl;
//}

//void HC_QSet_Marker_Symbol(const char *a, const char*b)
//{
//    if(!silent)
//        cout<<"QSet_Marker_Symbol"<<a<<b<<endl;
//}


//void HC_QUnSet_Rendering_Options (const char *a)
//{
//    if(!silent)
//        cout<<"QUnSet_Rendering_Options "<<a<<endl;
//}

void HC_Define_System_Options(const char *a)
{
    if(!silent)
        cout<<"Define_System_Options "<<a<<endl;
}


void HC_Set_Color_By_Value(const char *a,const char *b,  float p, float q, float r) 
{
    if(!silent)
        cout<<"Set Color by value "<<a<<","<<b<<p<<q<<r<<endl;
}

void HC_QSet_User_Options(const char *a, const char *b)
{
    if(!silent)
        cout<<"QSet_User_Options "<<a<<b<<endl;
}

void HC_Show_One_Net_User_Option(const char *a, char *b)
{
    if(!silent)
        cout<<"Show_One_Net_User_Option "<<a<<endl; strcpy(b,a);
}

void HC_QShow_User_Options(const char *a, char *b)
{
    if(!silent)
        cout<<"QShow_User_Options "<<a<<endl; strcpy(b,a);
}

void HC_Show_User_Options(char *b)
{
    if(!silent)
        cout<<"QShow_User_Options "<<endl; strcpy(b,"nohoops");
}

void 	HC_Insert_Ink ( const float a, const float b, const float c ) 
{
    if(!silent)
        cout<< "Insert Ink"<<endl;
}

void 	HC_Insert_Text   ( const float a, const float b, const float c, const char*s ) 
{
    if(!silent)
        cout<< "Insert Text"<<endl;
}

void 	HC_Move_Text(const HC_KEY k, const float a, const float b, const float c ) 
{
    if(!silent)
        cout<< "Move Text"<<endl;
}

void 	HC_Set_Marker_Symbol(const char *a)
{
    if(!silent)
        cout<<"Set_Marker_Symbol "<<a<<endl;
}

void HC_Restart_Ink()
{
    if(!silent)
        cout<<"Restart_Ink"<<endl;
}


void 	  HC_Translate_Object( const float a, const float b, const float c ) 
{
    if(!silent)
        cout<< "Translate"<<endl;
}
