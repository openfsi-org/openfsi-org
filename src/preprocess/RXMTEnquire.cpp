#include "StdAfx.h"
#include "RelaxWorld.h"
#include "RXCurve.h"
#include "RXRelationDefs.h"

#ifndef linux
#include "..\stringtools\stringtools.h" // defines FromUtf8 and ToUtf8

#else
#include "../../stringtools/stringtools.h"
#endif


#ifndef RXEDEV
#include "RXSail.h"
#include "RXSRelSite.h"
#include "RXEntityDefault.h"
#include "RXSeamcurve.h"

#endif
#include "RXMTEnquire.h"
/*
NOTE:  These functions use GetKey(), which requires that the entity exists
but doesnt care if its resolved.
so its down to the user to make sure that there isnt a reference to an unresolve-able entity.

*/

RXCurveLength::RXCurveLength(void)
{
    assert(0);
}
RXCurveLength::RXCurveLength(class RXSail *const p, class RXObject *const o) :
    RXStrFuncI(p,o)
{
}

RXCurveLength::~RXCurveLength(void)
{
}

MTDOUBLE RXCurveLength::evaluate(const MTSTRING*s)
{
    RXSTRING str = *s; //ToUtf8(*s);
    vector<RXSTRING> words= rxparsestring(str, _T("%"));
#ifdef RXEDEV
    cout << "please get the Length  of <";
    wcout <<   *s ;
    cout << "> "  << endl;
    return 5.0;

#else
    RXSTRING buf;
    RXSTRING type(_T("seamcurve,curve,seam,edge,compound curve"));
    class RXSail *sail =dynamic_cast< class RXSail *> (this->m_l);
    class RXSeamcurve *q=0;
    size_t nw = words.size();

    switch(nw) {
    case 0:
        wcout << "  cant parse '"<<str<<"'"<<endl;
        buf=TOSTRING("can't parse "); buf+=  str ;
        RXTHROW(buf);
        return 0.0;
    case 2: // first is a model type, second is
        sail =  sail->GetWorld()->FindModel(words[0]);
        if(!sail) {
            buf=TOSTRING("can't parse: no sail named ");
            buf+= words[0];
            RXTHROW(buf);
            return 0;
        }
        q = dynamic_cast<class RXSeamcurve *>(sail->Get_Key_With_Reporting(type,words[1],true) );
    case 1: // an entity in the current sail.

        q = dynamic_cast<class RXSeamcurve *> (sail->Get_Key_With_Reporting(type,words[0],true) );
        if(!q) {
            buf=TOSTRING("resolved SC not found: ");
            buf+= TOSTRING(words[0] );
            RXTHROW(buf);
            return 0;
        }
        this->m_rxo->SetRelationOf(q, aunt,RXO_ENT_OF_FUNC);
        if(q->Needs_Resolving) {	 // this cannot happen.
            buf=TOSTRING(" SC needs resolving: ");
            buf+= TOSTRING(q->name() );
            RXTHROW(buf);
        }
        if ( q->m_pC[0]->IsValid ())
            return q->m_pC[0]->Get_arc ();
        if ( q->m_pC[2]->IsValid ())
            return q->m_pC[2]->Get_arc ();
        if ( q->m_pC[1]->IsValid ())
            return q->m_pC[1]->Get_arc ();
    default:
        buf=TOSTRING(" SC isnt ready: ")+ str;
        RXTHROW(buf);
        return 0;
    }
#endif
}
RXSiteX::RXSiteX(void)
{ 
    assert(0);
}
RXSiteX::RXSiteX(class RXSail *const p, class RXObject *const o) :RXStrFuncI(p,o)
{
}

RXSiteX::~RXSiteX(void)
{
}

MTDOUBLE RXSiteX::evaluate(const MTSTRING*s)
{
    const RXSTRING &str = *s;
    vector<RXSTRING> words= rxparsestring(str, _T("%"));
#ifdef RXEDEV
    cout << "please get the X  of <";
    wcout <<   *s ;
    cout << "> "  << endl;
    return 5.0;
#else
    const RXSTRING type(_T("site,relsite"));
    class RXSail *sail =dynamic_cast< class RXSail *> (this->m_l);
    class RXSRelSite *q=0;
    size_t nw = words.size();

    switch(nw) {
    case 0:
        wcout << "  cant parse '"<<str<<"'"<<endl;
        return 0.0;
    case 2: // first is a model type, second is the (rel)site
        sail =  sail->GetWorld()->FindModel(words[0]);
        if(!sail){
            RXSTRING buf=TOSTRING("this model needs another named: ");
            buf+= words[0];
            RXTHROW(buf);
            return 0;
        }
        q = dynamic_cast<class RXSRelSite *> ( sail->Get_Key_With_Reporting(type,words[1],true ));
    case 1: // an entity in the current sail.
        q = dynamic_cast<class RXSRelSite *> ( sail->Get_Key_With_Reporting(type,words[0],true ));
        if(!q) {
            RXSTRING buf=TOSTRING("resolved site not found: ");
            buf+= TOSTRING(words[0] ); if(nw>1){ buf+= L" % ";buf+= TOSTRING(words[1] );}
            RXTHROW(buf);
            return 0;
        }
        this->m_rxo->SetRelationOf(q, aunt,RXO_ENT_OF_FUNC);
        if(q->Needs_Resolving) 	{  cout<<"shouldnt ever get here"<<endl;
            int c = q->Resolve();}
        return q->x;

    default:
        return 0;
    }
#endif
}


RXSiteY::RXSiteY(void)
{ assert(0);
}
RXSiteY::RXSiteY(class RXSail *const p, class RXObject *const o) :RXStrFuncI(p,o)
{
}

RXSiteY::~RXSiteY(void)
{
}

MTDOUBLE RXSiteY::evaluate(const MTSTRING*s)
{
    RXSTRING str = *s; //ToUtf8(*s);
    vector<RXSTRING> words= rxparsestring(str, _T("%"));
#ifdef RXEDEV
    cout << "please get the X  of <";
    wcout <<   *s ;
    cout << "> "  << endl;
    return 5.0;
#else
    RXSTRING type(_T("site,relsite"));
    class RXSail *sail =dynamic_cast< class RXSail *> (this->m_l);
    class RXSRelSite *q=0;
    size_t nw = words.size();

    switch(nw) {
    case 0:
        wcout << "  cant parse '"<<str<<"'"<<endl;
        return 0.0;
    case 2: // first is a model type, second is
        sail =  sail->GetWorld()->FindModel(words[0]);
        if(!sail){
            RXSTRING buf=TOSTRING("this model needs another named: ");
            buf+= words[0];
            RXTHROW(buf);
            return 0;
        }
        q = dynamic_cast<class RXSRelSite *> ( sail->Get_Key_With_Reporting(type,words[1],true ));
    case 1: // an entity in the current sail.
        q = dynamic_cast<class RXSRelSite *> ( sail->Get_Key_With_Reporting(type,words[0],true ));
        if(!q) {
            RXSTRING buf=TOSTRING("resolved site not found: ");
            buf+= TOSTRING(words[0] ); if(nw>1){ buf+= L" % ";buf+= TOSTRING(words[1] );}
            RXTHROW(buf);
            return 0;
        }
        this->m_rxo->SetRelationOf(q, aunt,RXO_ENT_OF_FUNC);
        if(q->Needs_Resolving)
            int c = q->Resolve();
        return q->y;
    default:
        return 0;
    }
#endif
}


RXSiteZ::RXSiteZ(void)
{
    assert(0);
}
RXSiteZ::RXSiteZ(class RXSail *const p, class RXObject *const o) :RXStrFuncI(p,o)
{
}

RXSiteZ::~RXSiteZ(void)
{
}

MTDOUBLE RXSiteZ::evaluate(const MTSTRING*s)
{
    RXSTRING str = *s; //ToUtf8(*s);
    vector<RXSTRING> words= rxparsestring(str, _T("%"));
#ifdef RXEDEV
    cout << "please get the X  of <";
    wcout <<   *s ;
    cout << "> "  << endl;
    return 5.0;
#else
    RXSTRING type(_T("site,relsite"));
    class RXSail *sail =dynamic_cast< class RXSail *> (this->m_l);
    class RXSRelSite *q=0;
    size_t nw = words.size();

    switch(nw) {
    case 0:
        wcout << "  cant parse '"<<str<<"'"<<endl;
        return 0.0;
    case 2: // first is a model type, second is
        sail =  sail->GetWorld()->FindModel(words[0]);
        if(!sail){
            RXSTRING buf=TOSTRING("this model needs another named: ");
            buf+= words[0];
            RXTHROW(buf);
            return 0;
        }
        q = dynamic_cast<class RXSRelSite *> ( sail->Get_Key_With_Reporting(type,words[1],true ));
    case 1: // an entity in the current sail.
        q = dynamic_cast<class RXSRelSite *> ( sail->Get_Key_With_Reporting(type,words[0],true ));
        if(!q) {
            RXSTRING buf=TOSTRING("resolved site not found: ");
            buf+= TOSTRING(words[0] ); if(nw>1){ buf+= L" % ";buf+= TOSTRING(words[1] );}
            RXTHROW(buf);
            return 0;
        }
        this->m_rxo->SetRelationOf(q, aunt,RXO_ENT_OF_FUNC);
        if(q->Needs_Resolving)
            int c = q->Resolve();
        return q->z;
    default:
        return 0;
    }
#endif
}
