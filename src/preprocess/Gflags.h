

#ifndef GFLAGS_15NOV04
#define GFLAGS_15NOV04

#define  GLOBAL_DO_TRANSFORMS 	1 // Set by SetHeelAngle
//#define  GLOBAL_DO_CONNECTS 	2
//#define  GLOBAL_DO_SLIDING	4
#define  GLOBAL_DO_CAMERAS	8
#define  GLOBAL_DO_APPWIND	16
#define  GLOBAL_DO_PANSAIL_TEXT	32
#define  GLOBAL_DO_RELAX_TEXT	64
#define  GLOBAL_DO_DEBUG_TEXT	128
#define  GLOBAL_DO_REFLECTION	256

#define mkGFlag(f)   (g_Flag |= (f))
#define clGFlag(f)   (g_Flag &=  ~(f))
#define isGFlag(f)   (g_Flag & (f))



#endif //#ifndef GFLAGS_15NOV04


