
/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 30 sept 2005 getArc now calls the RXCurve method.
   3 jan 2005 catch a small leak in SC_Finish_SCD
  28/4/00. split the Compute_Seamcurve routines out of panel . c

 */

//#define SC_DEBUG   

#include "StdAfx.h"
#include <QDebug>
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "global_declarations.h"
#include "RXSRelSite.h"
#include "RX_FEEdge.h"
#include "RXOffset.h"
#include "RXSeamcurve.h"
#include "RXPside.h"
#include "rxspline.h"
#include "RXCurve.h"

#ifdef linux
#include "../../stringtools/stringtools.h"
#else
#include "..\stringtools\stringtools.h"
#endif
#include "etypes.h"
#include "polyline.h"
#include "printall.h"
#include "panel.h"
#include "gauss.h"
#include "cut.h"
#include "geodesic.h"

#include "RX_SeamcurveHelper.h"

int RXSeamcurve::Compute(void){


    /*IF the number or order of relSITES has altered,
   it returns -2 having set 'Make Pside Request' */
    /* RETURNS : 	-1 if an error found
     0  if no errors
     and the endangle changes less than ANGLETOL
     and no internal angles	overtake
     OR basecurveptr is null (ie TPN);
     1  otherwise */
    class RX_SeamcurveHelper scd;
    if(this->Needs_Resolving)
    {
        cout<<endl<<" Why Compute SC "<<name()<<" which needs Resolving ???"<<endl;
        return 0;
    }
    int  iret =0;
#ifdef HOOPS
    HC_Open_Segment_By_Key(this->hoopskey);
#endif

    if(! scd.SeamCurve_Prelims(this))  scd.NoEnds=1;

    scd.m_type = this->SeamCurve_Type();

    switch (scd.m_type) {
    case SCTYPE_TPN:
        break;
    case SCTYPE_XYZABSSKFIL:{
        iret = scd.Compute_SeamCurve_XYZabsSK_Fil( this);
        break;
    }
    case SCTYPE_XYZABS_ONLY:{

        iret = scd.Compute_SeamCurve_XYZabsOnly(this);
        break;
    }
    case SCTYPE_XYZABS_CUT:{
        iret = scd.Compute_SeamCurve_XYZabs_Cut(this);// doesnt do SNAP. ONObject is programmed
        break;
    }
    case (SCTYPE_XYZABS_CUT_GEO): {
        cout<< " Enter Compute_SC_XYZAbs_Geo_Cut. Doesnt do SNAP\n"<<endl;
        iret = scd.Compute_SeamCurve_XYZAbs_Geo_Cut(this);
        printf(" back with iret = %d\n",iret);

        break;
    }
    case SCTYPE_SEAM_G_M:{
        iret = scd.Compute_SeamCurve_Geo_Mould_Seam(this);
        break;
    }
    case SCTYPE_SCALED_GEO_MOULD_CURVE : {
        iret = scd.Compute_SeamCurve_Geo_Mould_Scaled_Curve(this);
        break;
    }

    default:{
        iret = scd.Compute_SeamCurve_Sample(this);
    }
    }
    // draw SC needs scd->ev1,&scd->ev2,&scd->xl,&scd->yl,&scd->zl

//   if( (iret != PCE_DELETED) )   // the iret is a speed test july 2005
//        if( (iret ==0 ) )  // only draw if it didntchange. a speed optimisation
//            if(!g_Janet)
//                scd.DrawSeamcurve(); // Dra_Sc is slow, so lets try not to draw too often.
#ifdef HOOPS
    HC_Close_Segment(); // wierd. Would this give us an open/close mismatch for the deleted case?
#endif

    if(iret == PCE_DELETED)	// Peter Feb 2005
        this->Kill();

    scd.Clear();


    return iret;
}

int RX_SeamcurveHelper::SeamCurve_XYZAbsGeometry() {

    /*  sets the geometry of the SC. Skips the copy from the basecurve if
already done, thus preserving any end snaps, etc. 
returns 1 if the geometry is different from the las time SC prelims was called. 
*/	
    sc_ptr p = (sc_ptr  )this->m_sc;
    RXEntity_p bceptr = p->BorGcurveptr;
    class RXBCurve *bcptr= (class RXBCurve*)bceptr;
    int i;

    for(i=0;i<3;i++)
    {
        if(p->m_pC[i]->HasGeometry())
            continue;
        if ( bceptr->m_pONMO  && this->m_sc->Esail->GetFlag( FL_ONFLAG))
        {
            p->m_pC[i]->SetONCurve((ON_Curve *)(bceptr->m_pONMO->m_object),this->m_sc->name());  //To connect the object associated to RXEntity's Model objet to the base curve
        }
        else
            if(!p->c[i])
            {
                p->m_pC[i]->Fill_Curve(bcptr->p,bcptr->c);
                p->p[i] = p->m_pC[i]->Get_p();
                p->c[i] = p->m_pC[i]->Get_c();
                p->m_arcs[i] = p->GetArc(i);
            }
    } // for i

    if(p->m_scMoulded)
        PC_Pull_SC_To_Mould(this->m_sc);
    for(i=0;i<3;i++)
    {
        p->m_pC[i]->Update(p->p[i] ,p->c[i]);
        p->m_arcs[i] = (float) p->m_pC[i]->Get_arc(); // Feb 2004
    }
    // a short-cut, aimed at simple 3dm filament curves.
    if(! p->m_scMoulded && !p->End1site && !p->End2site && p->sketch)
        return 0;

    return (this->SeamCurveHasMoved(this->m_sc->Esail->m_Linear_Tol)); // doesnt work  for ON_CURVES
}//int SeamCurve_XYZAbsGeometry



int RXSeamcurve::SnapEnds(){ // just moves the ends of the curve to exactly match the endpts
     Site *e1=NULL, *e2=NULL;
    int l, retval=0;
    double stsq = this->Esail->m_Linear_Tol * this->Esail->m_Linear_Tol/4.0; // june 2003 GUESS
    if(this->End1site )
        e1=(Site *)this->End1site;
    if(	this->End2site)
        e2=(Site *)this->End2site;
    if(e1 || e2)
    {
        for(l=0;l<3;l++)  // snap to ends
        {
            if (this->m_pC[l]->GetONCurve())
            {
                ON_Curve * l_crv = dynamic_cast<ON_Curve*>(this->m_pC[l]->GetONCurve());
                if (!l_crv)
                    OutputToClient("RXCurve::m_ONCurve is not a ON_Curve object, in   Seamcurve SnapEnds() ",2);

                ON_3dPoint l_ptStart = l_crv->PointAtStart();
                ON_3dPoint l_ptEnd = l_crv->PointAtEnd();
                if(e1) {
                    ON_3dVector dd = e1->ToONPoint() -l_ptStart;
                    if(dd.LengthSquared() > stsq)
                    {
                        retval=1;
                        this->m_pC[l]->GetONCurve()->SetStartPoint(ON_3dPoint(e1->x,e1->y,e1->z));
                    }
                }
                if(e2)  {
                    //l_v.x = l_ptEnd.x;l_v.y=l_ptEnd.y;l_v.z=l_ptEnd.z;
                    ON_3dVector dd =  e2->ToONPoint()-l_ptEnd;
                    if(dd.LengthSquared() > stsq)
                    {
                        retval=1;
                        this->m_pC[l]->GetONCurve()->SetEndPoint(ON_3dPoint(e2->x,e2->y,e2->z));
                    }
                } // e2
                //	if (l_crv) l_crv = NULL; Peter removed doesnt do anything as going our of scope
            }//if (p->C[l].GetONCurve())
            else
            {
                if(this->c[l] > 1)
                {
                    if(e1) {
                        ON_3dVector dd =  e1->ToONPoint()-ON_3dPoint(this->p[l]->x, this->p[l]->y,this->p[l]->z);
                        if(dd.LengthSquared() > stsq){
                            retval=1;
                            this->p[l][0].x = e1->x;
                            this->p[l][0].y = e1->y;
                            this->p[l][0].z = e1->z;
                        }
                    } //if e1
                    if(e2) {
                        ON_3dVector dd =  e2->ToONPoint()-ON_3dPoint(this->p[l][this->c[l]-1].x, this->p[l][this->c[l]-1].y,this->p[l][this->c[l]-1].z);
                        if(dd.LengthSquared() > stsq) {
                            retval=1;
                            this->p[l][this->c[l]-1].x = e2->x;
                            this->p[l][this->c[l]-1].y = e2->y;
                            this->p[l][this->c[l]-1].z = e2->z;
                        }
                    }// if e2
                    if(retval)
                    {		//June 2003 for speed DANGER
                        this->m_pC[l]->Update(this->p[l] ,this->c[l]);
                        this->m_arcs[l]=(float)this->m_pC[l]->Get_arc();
                    }//if(retval)
                }//if(p->c[l] > 1)
            } //else if (p->C[l].GetONCurve())
        } //for(l=0;l<3;l++)
    } //if(e1 || e2)
    //	if(retval) NO do it anyway
    this->Update_IAOffsets();
    return retval;
}
int RXSeamcurve::SeamCurve_Type( ) const{


    if(this->XYZabs
            && !this->Geo_Or_UV
            && !(this->FlagQuery(CUT_FLAG))
            && !this->seam && this->sketch
            && this->FlagQuery(FILAMENT))
        return SCTYPE_XYZABSSKFIL;
    if(this->FlagQuery(TPN_SC)) {
        return SCTYPE_TPN;
    }
    if(this->FlagQuery(SC_SUB_SEGMENT)) {
        assert("subsegments not coded"==0);
        HC_Exit_Program();
    }
    if(!this->XYZabs && !this->BorGcurveptr && !this->seam)
        return SCTYPE_RLX;

    if(this->XYZabs && ! Geo_Or_UV && !( FlagQuery(CUT_FLAG)) && !this->seam)
        return SCTYPE_XYZABS_ONLY ;

    if(this->XYZabs && !this->Geo_Or_UV && ( FlagQuery(CUT_FLAG)) && !this->seam)
        return SCTYPE_XYZABS_CUT;

    if(this->XYZabs && this->Geo_Or_UV && (this->FlagQuery(CUT_FLAG)) && !this->seam)
        return SCTYPE_XYZABS_CUT_GEO;

    if(!this->XYZabs && this->seam && this->Geo_Or_UV)
        return SCTYPE_SEAM_G_M ;

    if(!this->XYZabs && !this->seam && this->Geo_Or_UV)
        return SCTYPE_SCALED_GEO_MOULD_CURVE ;

    if(!this->XYZabs && !this->Geo_Or_UV)
        return SCTYPE_SCALED_NON_GEO ;

    if(!this->XYZabs && this->Geo_Or_UV)
        return SCTYPE_SCALED_GEO ;


    return SCTYPE_UNDEFINED;

}
int RX_SeamcurveHelper::SeamCurve_Prelims( sc_ptr  p) {

    int err;

    this->m_sc = p;
    SAIL *sail= p->Esail;
    ON_3dVector l_onv =p->Normal();
    RXExpressionI * ex = p->FindExpression(L"depth",rxexpLocal);
    if(ex) this->depth =   ex->evaluate();
    else this->depth=0;
    RXExpressionI * le = p->FindExpression(L"length",rxexpLocal );
    if(le)
        this->length =  le->evaluate(); // PC_Value_Of_Q(p->length);	/* fixed length */
    else
        this->length =0;



    this->m_Old_Plines[0] = (VECTOR *)MALLOC( (p->c[0]+1) *sizeof(VECTOR));
    this->m_oldc[0]= p->c[0];
    this->m_Old_Plines[2] = (VECTOR *)MALLOC( (p->c[2]+1) * sizeof(VECTOR));  //LEAK
    this->m_oldc[2]= p->c[2];
    if(p->c[0]) PC_Polyline_Copy( this->m_Old_Plines[0], p->p[0],p->c[0]);
    if(p->c[2]) PC_Polyline_Copy( this->m_Old_Plines[2], p->p[2],p->c[2]);
    int i;

    //Thomas 29 07 04
    for (i=0;i<3;i++)
    {
        if(this->m_OldCurves[i]  ) delete this->m_OldCurves[i] ;
        this->m_OldCurves[i] = new RXCurve;
        this->m_OldCurves[i]->Set_Ent(p);
        if (i!=1 && (p->m_pC[i]) )  // peter april 2013  insulate for unresolved. BAD
            this->m_OldCurves[i]->operator =(*(p->m_pC[i]));
    }
    //Thomas 29 07 04

    if(sail)
        this->mould = sail->GetMould();

    if(p->End1site)
        this->end1site=(Site *)p->End1site;
    else
        this->end1site=NULL;
    if(p->End2site)
        this->end2site=(Site *)p->End2site;
    else
        this->end2site=NULL;

    if(this->end1site== this->end2site ) {
        this->NoEnds = 1;
        return 0;
    }
    // seems to fill in e1b (Yuch)
    err= p->Compute_Seamcurve_Trigraph(1, 1,&(this->m_schE1b),&this->m_schE2b ,&(this->xlb),&(this->ylb),&(this->zlb),&(this->Black_Chord),l_onv);
    if(err) {
        this->NoEnds = 1;
        return 0;
    }

    err=p-> Compute_Seamcurve_Trigraph(p->end1sign+1,p->end2sign+1,&this->ev1,&this->ev2,&(this->xl),&(this->yl),&(this->zl),&(this->Red_Chord),l_onv);
    if(err) {
        this->NoEnds = 1;
        return 0;
    }
    return 1;
}
int RX_SeamcurveHelper ::SeamCurveHasMoved(const double tol){
    /* return 1 if the curves have changed
 0 if they havent*/
    int k;
     sc_ptr p =  this->m_sc;

    for(k=0;k<3;k+=2)
    {
        if ((this->m_OldCurves[k]->GetONCurve())&&(p->m_pC[k]->GetONCurve()))
        {
            return this->m_OldCurves[k]->Curve_Compare(*(p->m_pC[k]),tol);
        }
        else if ((!this->m_OldCurves[k]->GetONCurve())&&(!p->m_pC[k]->GetONCurve()))
        {
            if(PC_Polyline_Compare(this->m_Old_Plines[k], this->m_oldc[k], p->p[k],p->c[k],tol))
                return 1;
        }
        else
            return 1; // exactly one ONCurve  is non-NULL
    }
    return 0;
}




int RX_SeamcurveHelper::Compute_SeamCurve_Geo_Mould_Scaled_Curve(RXEntity_p e) {

    // Specifically, a not-XYZAbs sc CURVE on  a mould. May be flat or fanned.

    /*	 Compute_ Seamcurve. IF the number or order of relSITES has altered,  it returns -2
  having set 'Make Pside Request' in the seamcurve structure*/
    /* METHOD.

     RETURNS  : 	-1 if an error found
     0  if no errors
     and the endangle changes less than ANGLETOL
     and no internal angles	overtake
     OR basecurveptr is null (ie TPN);
     1  otherwise
     
     <seamflag> is a flag which is 1 if its a seam, 0 if its a curve.
     
     We have two end points. They may be SITEs or offsets on curves or seams.
     First we find them. (courtesy of Com_SC_Trigraph)
     Then we call up the basecurve. We rotate it to fit
     between the endpoints taking the adjectives into account.

     Dealing with the IMPOSED_ANGLE.
     For seams we halve the IMPOSED_Angles.
     Also, we ignore the flag SIGN of an imposed_ANGLE.
     All flags are considered Positive
     
     For Curves, we use full IMPOSED_ANGLES and their SIGN flags.
     
     */
    sc_ptr  p = (sc_ptr  )e;
    assert(p==  this->m_sc);
    int seamflag= p->seam;
    int flag=0;
    double e1a=0.0,e2a=0.0;
    int k, l,bc= 0 ,Old_NA=0 , iret=0 ,Use_Gauss_EA=0;
    RXEntity_p *olde=NULL;
    RXEntity_p bceptr = p->BorGcurveptr;
    class RXBCurve *bcptr=NULL;
    class RXGaussCurve *gcptr=dynamic_cast<class RXGaussCurve *>(bceptr);
    VECTOR* base=NULL;

    assert(bceptr->TYPE != SEAMCURVE); // ie its not a subseamcurve.

    if(this->NoEnds) {
        RXSTRING buf;
        buf=L"NoEnds(GeoMouldScaled ) for ent <" + TOSTRING(e->name() ) + L">";
        rxerror(buf,3);
        return -1;
    }
    assert(!p->XYZabs);

    assert(p->Geo_Or_UV) ;

    if(! Find_Geodesic(p,  &this->p_slr,&this->c_slr,&(this->G_Chord)) ) rxerror("Find_Geodesic returned 0",2);
    /* returns a straight from e1 to e2 if no mould. also geodesic arclength in p->arcs[0]*/

    if(! p->Geo_Or_UV) /* block 41 the flag may have been changed by the call */
        SeamCurve_Find_Straight( this->ev1.ToONPoint(),this->ev2.ToONPoint(), &this->p_slr,&this->c_slr,&(this->G_Chord));

    /* returns a straight from e1 to e2 if no mould*/

    this->Red_Chord=p->GetArc(0); //p->arcs[0];//  Red Chord now the geodesic arclength


    assert(bceptr); { 	 /*block 5 else its a RLX curve. just draw it */


        if(bceptr->TYPE== GAUSSCURVE){  /* block 51 */
            float tempchord= (float) 1.0; /* p->arcs[0];	*/
          gcptr-> Compute_GaussCurve( tempchord,seamflag,  this->m_sc->Esail->m_Linear_Tol );
            p->Xabs=0;
            p->Yabs=0;
            p->YasX=2; /* to trigger special treatment in Scale_BaseCurve. scale by (1+depth) */
        }
        else { /* block 52 */
            if(seamflag) this->depth=this->depth/(float)2.0;
        }

        bcptr =  (class RXBCurve*) bceptr ;
        bc=bcptr->c;
        base = (VECTOR *)MALLOC(sizeof(VECTOR)*(bc+1));

        Scale_Base_Curve(bcptr->p, base,bc,this->depth,p->Xabs,p->Yabs,p->YasX,this->length);



        /* base is the base-curve. Y in m and x from 0 to 1
       UNLESS Xabs, in which case x is left unchanged
       OR scaled to the sc length (if non-zero) */


        /* attribute options are:
       flat/fanned.        Flat means ignore imposed angles.
       Xabs/Xscaled        Xabs means trim curve to fit (eg preserve length)
       Yabs/Yscaled /YasX  Yabs means use Y offsets, and depth as a multiplier
       Yscaled = use Y offsets, but scale so d=depth
       YasX means preserve aspect ratio during scaling	of x
       Yscaled is the default.
       */

        if (seamflag==1)
            flag=SEAMFLAG;
        else if (seamflag==0)
            flag=CURVEFLAG;
        else
            flag=UNKNOWNFLAG;
        if (p->m_isFlat )
            flag=FLATFLAG;

        olde = (RXEntity_p *)MALLOC((1 + p->n_angles)*sizeof(RXEntity_p ));

        for(k=0;k<p->n_angles;k++)
            olde[k] = (*p).ia[k]. s;
        Old_NA = p->n_angles;
        l = (*p).c[0];

        float l_arc = float(p->GetArc(0));
        Make_Fanned_(p,flag,&l_arc,&(*p).strt_A,this->Red_Chord,&bc,&base);

        /* maybe bc is not (*p).c[0] any more, or (*p).N_Angles is not as before  or some of the pointers
  in a are not as before. In any of these cases we return (-2) and set  Make Pside Request */
        if( Old_NA != p->n_angles)
            p->Make_Pside_Request =1;
        else {
            for(k=0;k<p->n_angles && k<Old_NA;k++) {
                if(p->ia[k]. s != olde[k]) {
                    p->Make_Pside_Request = 1;
                    break;
                }
            }
        }
        RXFREE(olde); olde=NULL;


        /* 	else leave where it is */

        /* the polylines
       seams. 0 and 2 either side, based on red ends
       1 straight, based on black
       
       Curves  0 and 2 same side, based on red ends
       1 is same curve,  based on black  ends
       */
        Use_Gauss_EA = 0;

        assert(this->mould && p->Geo_Or_UV);
        newBuild_SC_Curves( base,bc);

        iret =  Measure_End_Angles(p,seamflag, Use_Gauss_EA , e1a,e2a ) || iret;	  /* 1 if endangles changed, else 0 */

        iret = iret || PC_Polyline_Compare(this->m_Old_Plines[0],this->m_oldc[0], p->p[0], p->c[0], this->m_sc->Esail->m_Linear_Tol);
        iret = iret || PC_Polyline_Compare(this->m_Old_Plines[2],this->m_oldc[2], p->p[2], p->c[2], this->m_sc->Esail->m_Linear_Tol);

        if(this->length > FLOAT_TOLERANCE) {	 //WHY should it ever be zero? WHAT does it represent??
            if(fabs(this->length-p->GetArc(1)) > this->m_sc->Esail->m_Linear_Tol) {
                p->End2site->SetNeedsComputing();
            }
        }
        RXFREE(base); base=NULL;

    } /*end of block 5 if basecurve non-null */

    if(p->Xabs) {
        for(l=0;l<3;l++)
            p->m_pC[l]->Update(p->p[l] ,p->c[l]);
        Update_Xabs_Offset(p);
    }
    else  {
        PC_Pull_SC_To_Mould(e);
        for(l=0;l<3;l++)
            p->m_pC[l]->Update(p->p[l] ,p->c[l]);
    }


    if((p->FlagQuery(CUT_FLAG)) && p->Needs_Cutting && !p->sketch) {
        char str[256];
        sprintf(str," cannot CUT this kind of seam (Geo_Mould_Scaled_Curve) - %s\n so deleting it", e->name());
        rxerror(str,2);
        p->FlagClear(CUT_FLAG);
        e->Kill( );

        return PCE_DELETED;
    }


    return(iret);
}
int RX_SeamcurveHelper::Compute_SeamCurve_XYZabsSK_Fil(RXSeamcurve *p ) {
//    class RX_SeamcurveHelper *this=this;

    /*	 Compute_ Seamcurve. IF the number or order of relSITES has altered,  it returns -2
  having set 'Make Pside Request' in the seamcurve structure*/
    /* METHOD.

     RETURNS  : 	-1 if an error found
     0  if no errors
     and the endangle changes less than ANGLETOL
     and no internal angles	overtake
     OR basecurveptr is null (ie TPN);
     1  otherwise

     */
    assert(p==this->m_sc);
    int k, iret=0;
    RXEntity_p bceptr = p->BorGcurveptr;
    int err = 0;

    assert(bceptr->TYPE !=SEAMCURVE);
    assert(p->XYZabs );
    assert(p->sketch );
    p->Xabs=1;
    p->Yabs=0;
    p->YasX=1;
    p->m_isFlat=1;
    iret = this->SeamCurve_XYZAbsGeometry();

    if(this->NoEnds && !p->sketch) {
        this->Clear();
        err= (0==this->SeamCurve_Prelims(p));
        if(err) { 	/* we are here because a DRAW-XYZAbs SC hasnt got any endsites.  */
            return -1; /* bad topological spec*/
        }
    }
    return(iret);
}


int RX_SeamcurveHelper::Compute_SeamCurve_XYZabsOnly(RXEntity_p e ) {


    /*	 Compute_ Seamcurve. IF the number or order of relSITES has altered,  it returns -2
  having set 'Make Pside Request' in the seamcurve structure*/
    /* METHOD.

     RETURNS  : 	-1 if an error found
     0  if no errors
     and the endangle changes less than ANGLETOL
     and no internal angles	overtake
     OR basecurveptr is null (ie TPN);
     1  otherwise

     */
    sc_ptr  p = (sc_ptr  )e; 	assert(p==this->m_sc);
    int  iret=0;
    RXEntity_p bceptr = p->BorGcurveptr;
    int err = 0;

    assert(bceptr->TYPE !=SEAMCURVE);
    assert(p->XYZabs );

    p->Xabs=1;
    p->Yabs=0;
    p->YasX=1;
    p->m_isFlat=1;
    iret = this->SeamCurve_XYZAbsGeometry();

    if(p->Needs_Cutting && (p->FlagQuery(SNAP_TO_NODES)||p->FlagQuery(SNAP_TO_ENDS_ONLY)))
        Compute_Snap_Intersections(e);

    if(this->NoEnds && !p->sketch) {
        this->Clear();
        err= (0==this->SeamCurve_Prelims(p));
        if(err) { 	/* we are here because a DRAW-XYZAbs SC hasnt got any endsites.  */
            return -1; /* bad topological spec*/
        }
    }
    iret =  p->SnapEnds() || iret;

    return(iret);
}


int RX_SeamcurveHelper::Compute_SeamCurve_XYZabs_Cut(sc_ptr sc ) {

    /*
  EndNptrs are NOT required.  If they exist, they serve as end markers in
  the cutting process.
      */

    int k=0, iret=0;assert(sc==this->m_sc);

    assert(sc->XYZabs && !sc->Geo_Or_UV && (sc->FlagQuery(CUT_FLAG))&& !sc->seam);

    if(PCE_DELETED ==PCC_Check_Cut_Parameters(sc)){ // uses bc polyline
        return PCE_DELETED;
    }

    sc->Xabs=1;
    sc->Yabs=0;
    sc->YasX=1;
    sc->m_isFlat=1;
    iret = this->SeamCurve_XYZAbsGeometry();

    if(sc->Needs_Finishing)
        return 1; // we have geometry but still need to cut

    if(sc->Needs_Cutting && (sc->FlagQuery(SNAP_TO_NODES)||sc->FlagQuery(SNAP_TO_ENDS_ONLY)))
        k = Compute_Snap_Intersections(sc);

    if(sc->Needs_Cutting && !sc->sketch) {
        //put Compute_Snap_Intersections here
        k = (0!=Compute_Cut_Intersections(sc)); // May delete 'e'
        if(!sc->sketch)
            sc->Needs_Cutting  = k;
        else
            return PCE_DELETED;
    }

    /* The following is very slow, but it forces all ending-on SCs exactly to touch this one */
    for (k=0;k<sc->n_angles && iret;k++) {
        if(sc->ia[k].s->TYPE==RELSITE || sc->ia[k].s->TYPE==SITE  || sc->ia[k].s->TYPE==SEAMCURVE ) {
            if(sc->ia[k].IA_Owner == sc) //not sure about this
                sc->ia[k].s->SetNeedsComputing();
        }
    }
    k = sc->SnapEnds();
    if(k) sc->Needs_Cutting =1;
    iret= k || iret;
    // this is dangerous because the EndNptrs offsets are now out of synch with
    // the coordinates.  The offsets won't get updated if the EndNptrs geometry
    // doesnt come from this SC (ie its a cross of two others)
    // if we DO snap to ends, we need to update the offsets in our IAList
    // the call for this made from SnapEnds


    return(iret);
}



int RX_SeamcurveHelper::Compute_SeamCurve_XYZAbs_Geo_Cut(RXEntity_p e ) {

    /*	 Compute_ Seamcurve. IF the number or order of relSITES has altered,  it returns -2
  having set 'Make Pside Request' in the seamcurve structure*/
    /* METHOD.

     RETURNS  : 	-1 if an error found
     0  if no errors
     and the endangle changes less than ANGLETOL
     and no internal angles	overtake
     OR basecurveptr is null (ie TPN);
     1  otherwise
     
     <seamflag> is a flag which is 1 if its a seam, 0 if its a curve.

     */
    /* in this variant we ALWAYS have XYZabs, cutflag and Geo_Or_Uv flags
 The XYZabs curve is generated, cuts are taken, then a geodesic is formed between cuts
 If the sc's curve[1] already exist, it is used instead for the cuts.
 */

    sc_ptr  p = (sc_ptr  )e; 	assert(p==  this->m_sc);
    int i, flag=0, err = 0, iret=0 ;
    class RXBCurve* bcptr;

    assert(p->XYZabs && p->Geo_Or_UV && (p->FlagQuery(CUT_FLAG)) &&!p->seam);
    assert(p->BorGcurveptr->TYPE!=SEAMCURVE);

    p->Xabs=1;
    p->Yabs=0;
    p->YasX=1;
    p->m_isFlat=1;
    bcptr = (class RXBCurve*)p->BorGcurveptr;

    PC_Pull_Pline_To_Mould( this->mould, &(bcptr->p),&(bcptr->c),0,this->m_sc->Esail->m_Linear_Tol);/* dangerous because it may well be outside the mould */
    /*LOOKS WRONG*/
    if(!p->c[1]) {
        rxerror("(Compute_SeamCurve_XYZAbs_Geo_Cut )No 1-curve",2);
        for(i=0;i<3;i++) {
            p->m_pC[i]->Fill_Curve(bcptr->p,bcptr->c);
            p->p[i] = p->m_pC[i]->Get_p();
            p->c[i] = p->m_pC[i]->Get_c();
            p->m_arcs[i] = (float) PC_Polyline_Length(p->c[i],p->p[i]);
            if(p->GetArc(i) < 4.0*this->m_sc->Esail->m_Linear_Tol) {
                p->sketch = 1;
                p->Needs_Cutting=0;
                rxerror("demoote to Sketch",2);
            }
        }
    }

    //	else  rx error("1-curve exists",0);

    if(p->Needs_Cutting) {
        Compute_Cut_Intersections(p);
        Make_End_Ptrs(p);
    }

    if(this->NoEnds) { /* flag set in SeamCurve_Prelims when ne endsites. Try again now we've done the cutting */
        this->Clear();
        err= (0==this->SeamCurve_Prelims(p));
        if(err) { rxerror( "SC still has failed prelim",1); return -1;}
    }

    flag = Find_Geodesic(p, &this->p_slr,&this->c_slr,&(this->G_Chord)) ;

    switch (flag) {
    case 0: { /* an error */
        rxerror("Find_Geodesic returned 0",2);
        iret = -1;
        break;
    }
    case 1: { /* it didnt change */
        iret = 0;
        printf(" fill with %d\n",this->c_slr);
        for(i=0;i<3;i++){
            printf(" CASE 1 realloc p[%d] with %d\n", i,  this->c_slr);
            p->p[i] = (VECTOR *)REALLOC(p->p[i] , this->c_slr*sizeof(VECTOR));
            p->c[i] = this->c_slr;
            PC_Polyline_Copy(p->p[i],this->p_slr, this->c_slr);
            PolyPrint(" case 1 p->p[i]\n", p->p[i],p->c[i] );
            p->m_pC[i]->Update(p->p[i],p->c[i]);
            p->m_arcs[i] = (float) p->m_pC[i]->Get_arc();
        }
        break;
    }
    case 2: { /* it moved */
        iret = 1;
        printf(" fill with %d\n",this->c_slr);
        for(i=0;i<3;i++){
            printf(" CASE 2 realloc p[%d] with %d\n", i,  this->c_slr);
            p->p[i] = (VECTOR *)REALLOC(p->p[i] , this->c_slr*sizeof(VECTOR));
            p->c[i] = this->c_slr;
            PC_Polyline_Copy(p->p[i],this->p_slr, this->c_slr);
            PolyPrint(" case 1 p->p[i]\n", p->p[i],p->c[i] );
            p->m_pC[i]->Update(p->p[i],p->c[i]);
            p->m_arcs[i] = (float) p->m_pC[i]->Get_arc();
        }

        break;
    }
    default : {
        assert(0);
    }
    }

    return(iret);
}

int RX_SeamcurveHelper::FudgeTrigraph( ) {
//    class  RX_SeamcurveHelper*this=this;
    if(!this->m_sc) return 0;
    assert(!this->m_sc->Needs_Resolving);
    sc_ptr sc = this->m_sc;

    if(this->ev2.DistanceTo (this->ev1) < this->m_sc->Esail->m_Linear_Tol) {
        this->ev1= sc->m_pC[1]->GetStartPoint() ;
        this->ev2 = sc->m_pC[1]->GetEndPoint() ;
    }
    ON_3dVector l_z =this->m_sc->Normal();
    this->xl = this->ev2-this->ev1;
    this->xl.Unitize();
    this->yl =ON_CrossProduct(l_z ,this->xl);
    this->zl=ON_CrossProduct(this->xl,this->yl);
    this->yl.Unitize ();
    this->zl.Unitize ();
    if(this->Red_Chord< this->m_sc->Esail->m_Linear_Tol)
        this->Red_Chord = sc->GetArc(0);
    return 1;
}
#ifdef RXQT
int RX_SeamcurveHelper::DrawSeamcurve( ) {
    sc_ptr p = this->m_sc;	if(!p) return 0;
    RXGRAPHICSEGMENT n = this->m_sc->GNode(rxexpLocal);
    if(!FudgeTrigraph()) return 0;
    HC_Flush_Geometry(m_sc->GNode(rxexpLocal));
    if (!g_Janet &&!(m_type==SCTYPE_XYZABSSKFIL )) {
        if (!((p->FlagQuery(CUT_FLAG)) && !p->seam) ) {
            p->m_pC[0]->Draw(n,"red");
            if(p->seam)
                p->m_pC[2]->Draw(n,"red"); // grey for sketch
        }
    } // janet
    p->m_pC[1]->Draw(n,"black");
    return 1;
}

#elif HOOPS
int RX_SeamcurveHelper::DrawSeamcurve( ) {
    char buf[256];
    sc_ptr p = this->m_sc;	if(!p) return 0;

    if(!FudgeTrigraph()) return 0;

    if (!g_Janet &&!(m_type==SCTYPE_XYZABSSKFIL )) {
        HC_Flush_Contents(".","geometry,segment");
        HC_Open_Segment("orientation");
        HC_Flush_Contents(".","geometry");
        Draw_Orientation_Vector(p,this->ev1.ToONPoint(),this->ev2.ToONPoint(),this->xl,this->yl,this->zl,this->Red_Chord);
        HC_Set_Visibility("off");
        HC_Close_Segment();

        HC_Open_Segment("Material Type");
        HC_Flush_Contents(".","geometry");
        Draw_Material_Ref(p,this->ev1.ToONPoint(),this->ev2.ToONPoint(),this->xl,this->yl,this->zl );
        HC_Set_Visibility("off");
        HC_Close_Segment();

        HC_Open_Segment("arrow");
        HC_Flush_Contents(".","geometry");
        Draw_SC_Arrow(p,this->ev1.ToONPoint(),this->ev2.ToONPoint(),this->xl,this->yl,this->zl );
        HC_Set_Visibility("off");
        HC_Close_Segment();

        if (!((p->FlagQuery(CUT_FLAG)) && !p->seam) ) {

            HC_Open_Segment("reds");
            HC_Flush_Contents(".","geometry");
            p->m_pC[0]->Draw();
            if(p->seam)
                p->m_pC[2]->Draw();
            if(p->sketch) HC_Set_Color("lines=gray");
            else HC_Set_Color("lines=red");
            HC_Close_Segment();
        }
    } // janet

    HC_Open_Segment("middle");
    HC_Flush_Contents(".","geometry");
#ifndef WIN32
    if(p->sketch) HC_Set_Color("lines=gray,text=gray");
    else HC_Set_Color("lines=black,text=black");
#else
    HC_Set_Color("lines=blue grey");
#endif
    p->m_pC[0]->Draw();	  //SHOULDNT THIS BE CURVE 1???????????????
    HC_Close_Segment();

    return 1;
}
#else
#error(" need RXQT or HOOPS")
#endif
int RX_SeamcurveHelper::oldBuild_SC_Curves( VECTOR*base,int bc){

    /* method.
 (base,bc) consists of (s,y,z)
   RED-SPACE
 (p_slr,c_clr)	is the reference curve in red-space
  So we construct a new reference curve which is on that, but with S offsets from base
  Then we make the 0 and 2 curves by displacing this curve

  BLACK-SPACE
  strictly, we should generate a geodesic between the black-space ends, displace it by (base y,z)
  and use that fo}r the black curve
    return 1 if it moved, else 0
 */
    int oldc,  changed = 0;
    int j,k;
    double f;
    VECTOR newp, *e1, *e2;
    sc_ptr p = this->m_sc;
    e1 =  this->p_slr;
    e2 = &( (this->p_slr)[this->c_slr-1]);

    //if(streq(p->name(),"pocket4")){
    //	cout<< "pocket4 L971"<<endl; p->Dump(stdout);
    //	printf(" curve lengths are %d %d %d\n", p->c[0],p->c[1],p->c[2]);
    //	PolyPrint(" base \n", base,bc);
    //}
    if (p->seam) {
        for (j=0;j<3;j+=2)   {
            f= -(double) (j-1);
            if(p->c[j] !=bc) {
                p->c[j]=bc;
                p->p[j] = (VECTOR *)REALLOC(p->p[j],(bc)*sizeof(VECTOR));
            }
            for (k=0;k<bc;k++){
                (((*p).p)[j][k]).x = e1->x + this->xl.x*base[k].x + f* this->yl.x*base[k].y ;
                (((*p).p)[j][k]).y = e1->y + this->xl.y*base[k].x + f* this->yl.y*base[k].y ;
                (((*p).p)[j][k]).z = e1->z + this->xl.z*base[k].x + f* this->yl.z*base[k].y ;
            }
        }

        f= this->Black_Chord/this->Red_Chord;
        if(p->c[1]!=bc ) {
            p->c[1]=bc;
            p->p[1] = (VECTOR *)REALLOC(p->p[1],p->c[1]*sizeof(VECTOR));
        }
        for (k=0;k<p->c[1];k++){
            ((p->p)[1][k]).x = this->m_schE1b.x + f*this->xlb.x*base[k].x;	  /* draw straight */
            ((p->p)[1][k]).y = this->m_schE1b.y + f*this->xlb.y*base[k].x;
            ((p->p)[1][k]).z = this->m_schE1b.z + f*this->xlb.z*base[k].x;
        }
    }
    else {	 /* a curve */
        /* we construct the curves from (p_slr, c_slr)  All have 'bc' pts */
        for (j=0;j<3;j+=2)   {
            oldc = p->c[j];
            if(oldc-bc) changed=1;
            if(p->c[j]!=bc ) {
                p->c[j]=bc;
                p->p[j] = (VECTOR *)REALLOC(p->p[j],(bc+1)*sizeof(VECTOR));
            }
            for (k=0;k<bc;k++){
                newp.x = e1->x + this->xl.x*base[k].x + this->yl.x*base[k].y + this->zl.x*base[k].z ;
                newp.y = e1->y + this->xl.y*base[k].x + this->yl.y*base[k].y + this->zl.y*base[k].z ;
                newp.z = e1->z + this->xl.z*base[k].x + this->yl.z*base[k].y + this->zl.z*base[k].z ;
                if(!changed) {
                    if(k<oldc) {
                        if(PC_Dist(&newp,&(p->p[j][k])) > this->m_sc->Esail->m_Linear_Tol)
                            changed=1;
                    }
                }
                (((*p).p)[j][k]).x = newp.x ;
                (((*p).p)[j][k]).y = newp.y ;
                (((*p).p)[j][k]).z = newp.z ;
            }
        }
        f= this->Black_Chord/this->Red_Chord;  /* scale factor from red to black */
        if(p->c[1] != bc ) {
            p->c[1]=bc;
            p->p[1] = (VECTOR *)REALLOC(p->p[1],p->c[1]*sizeof(VECTOR));
        }
        for (k=0;k<bc;k++){
            (((*p).p)[1][k]).x = this->m_schE1b.x + f*this->xlb.x*base[k].x + f* this->ylb.x*base[k].y + f*this->zlb.x*base[k].z;
            (((*p).p)[1][k]).y = this->m_schE1b.y + f*this->xlb.y*base[k].x + f* this->ylb.y*base[k].y + f*this->zlb.y*base[k].z;
            (((*p).p)[1][k]).z = this->m_schE1b.z + f*this->xlb.z*base[k].x + f* this->ylb.z*base[k].y + f*this->zlb.z*base[k].z;
        }

    }// end else a curve

    
    if(!p->Xabs) {
        for(j=0;j<3;j+=2){
            p->p[j][p->c[j]-1].x = e2->x;
            p->p[j][p->c[j]-1].y = e2->y;
            p->p[j][p->c[j]-1].z = e2->z;
        }

        p->p[1][p->c[1]-1].x = this->m_schE2b.x;
        p->p[1][p->c[1]-1].y = this->m_schE2b.y;
        p->p[1][p->c[1]-1].z = this->m_schE2b.z;
    }
    if(p->seam)
        for(k=0;k<3;k++)
            p->m_arcs[k] =  p->GetArc(k);
    else
        for(k=0;k<3;k++)
            p->m_arcs[k] =  p->GetArc(1);
    return changed;
}


int RX_SeamcurveHelper::newBuild_SC_Curves( VECTOR*base,int bc){

    /* method.
 (base,bc) consists of (s,y,z)  Ideally it should already be scaled to length G_Chord
   RED-SPACE
 (p_slr,c_clr)	is the reference curve in red-space. It has length G_Chord
  So we construct a new reference curve which is on that, but with S offsets from base
  Then we make the 0 and 2 curves by displacing this curve

  BLACK-SPACE
   we generate a geodesic between the black-space ends, displace it by (base y,z)
  and use that for the black curve
  returns 1 if changed, else 0

 */
    VECTOR*p_slr1=NULL;
    int c_slr1=0;
    int l,k;

    double *x, *y, *z;
    ON_3dPoint  ep1, ep2;
    sc_ptr sc =  this->m_sc;

    int changed = 0;

    assert( (this->c_slr) > 0);

    x= (double *)CALLOC((bc+1) ,sizeof(VECTOR));
    y = (double *)CALLOC((bc+1) ,sizeof(VECTOR));
    z = (double *)CALLOC((bc+1) ,sizeof(VECTOR));

    PC_Vector_Copy(this->p_slr[0],&ep1);
    PC_Vector_Copy(this->p_slr[this->c_slr-1] ,&ep2);
    assert (fabs(ep1.z -this->m_schE1b.z) < 0.0001);
    assert (fabs(ep2.z -this->m_schE2b.z) < 0.0001);

    /* 1) generate the red-space curves from this */
    if(sc->seam) {
        for(k=0;k<bc;k++) {
            x[k] = base[k].x; y[k] = base[k].y; z[k] = base[k].z;
        }
        Create_Displaced_Polyline(sc,this->mould,2,bc,x,y,z,base);

        for(k=0;k<bc;k++) {
            x[k] = base[k].x;  y[k] = -base[k].y; z[k] = -base[k].z;
        }
        Create_Displaced_Polyline(sc,this->mould,0,bc,x,y,z,base);
    }
    else {	/* a curve */
        for(k=0;k<bc;k++) {
            x[k] = base[k].x;  y[k] =  base[k].y; z[k] =  base[k].z;
        }

        Create_Displaced_Polyline(sc,this->mould,0,bc,x,y,z,base);
        sc->p[2] = (VECTOR *)REALLOC(sc->p[2] , sc->c[0]*sizeof(VECTOR));
        sc->c[2] = sc->c[0];
        PC_Polyline_Copy(sc->p[2],sc->p[0], sc->c[0]);
    }

    /* 4 generate the black-space curve */
#ifdef _NOT_DEFINED
    if(0) {//either end  is a seam
        if(sc->Geo_Or_UV)
            if(!Find_Geodesic(sc, &p_slr1,&c_slr1,&this->Black_Chord)) rxerror("Find_Geodesic returned 0",2);
        if(!sc->Geo_Or_UV)  /* the above call may have unset the flag */
            SeamCurve_Find_Straight( &this->e1b,&this->e2b, &p_slr1,&c_slr1,&(this->Black_Chord));
    }
    else
#endif
    {
        this->Black_Chord = sc->GetArc(0);
        c_slr1 = this->c_slr;
        p_slr1 = this->p_slr;
    }

    /* 5 allocate space for polyline 1 and copy the black-space curve into it */
    if(sc->seam) {
        if(c_slr1 != sc->c[1]) {
            sc->p[1] = (VECTOR *)REALLOC(sc->p[1] , ((c_slr1)+1)*sizeof(VECTOR));
            sc->c[1] = c_slr1;
        }
        PC_Polyline_Copy(sc->p[1],p_slr1, c_slr1);
    }
    else { /* a curve. displace from  that*/
        for(k=0;k<bc;k++) {
            x[k] = base[k].x; y[k] =  base[k].y; z[k] =  base[k].z;
        }
        Create_Displaced_Polyline(sc,this->mould,1,bc,x,y,z,base);

    }



    /* 7 Copy the first and last points from e1,e2 to save rounding errors */

    if(!sc->Xabs) {
        for(l=0;l<3;l+=2){
            sc->p[l][0].x = ep1.x;
            sc->p[l][0].y = ep1.y;
            sc->p[l][0].z = ep1.z;
            if(	 sc->c[l] < 1) { printf(" curve %d",l); rxerror("TOO SHORT",2);}
            sc->p[l][sc->c[l]-1].x = ep2.x;
            sc->p[l][sc->c[l]-1].y = ep2.y;
            sc->p[l][sc->c[l]-1].z = ep2.z;
        }
        sc->p[1][0].x = this->m_schE1b.x;
        sc->p[1][0].y = this->m_schE1b.y;
        sc->p[1][0].z = this->m_schE1b.z;	 if(	 sc->c[1] < 1) rxerror("TOO SHORT",2);
        sc->p[1][sc->c[1]-1].x = this->m_schE2b.x;
        sc->p[1][sc->c[1]-1].y = this->m_schE2b.y;
        sc->p[1][sc->c[1]-1].z = this->m_schE2b.z;
    }
    for(l=0;l<3;l++){
        sc->m_arcs[l] = (float)sc->GetArc(l);
    }
    if(p_slr1 &&(p_slr1!=this->p_slr)) RXFREE( p_slr1);
    RXFREE(x);
    RXFREE(y);
    RXFREE(z);

    return changed;
}

int RX_SeamcurveHelper::Compute_SeamCurve_Geo_Mould_Seam(RXEntity_p e ) {

    /*	 Compute_ Seamcurve. IF the number or order of relSITES has altered,  it returns -2
  having set 'Make Pside Request' in the seamcurve structure*/
    /* METHOD.

     RETURNS  : 	-1 if an error found
     0  if no errors
     and the endangle changes less than ANGLETOL
     and no internal angles	overtake
     OR basecurveptr is null (ie TPN);
     1  otherwise
     
     <seamflag> is a flag which is 1 if its a seam, 0 if its a curve.
     
     We have two end points. They may be SITEs or offsets on curves or seams.
     First we find them. (courtesy of Com_SC_Trigraph)
     Then we call up the basecurve. We rotate it to fit
     between the endpoints taking the adjectives into account.
     We insert two HOOPS polylines and return the key of their containing segment.
     Those HOOPS polylines might later be used to interpolate relSITEs,
     but actually we use the arrays p1 and p2 of the seam
     
     Dealing with the IMPOSED_ANGLE.
     For seams we halve the IMPOSED_Angles.
     Also, we ignore the flag SIGN of an imposed_ANGLE.
     All flags are considered Positive
     
     For Curves, we use full IMPOSED_ANGLES and their SIGN flags.
     
     */
    sc_ptr  p = (sc_ptr  )e; assert(p==this->m_sc);

    int seamflag= p->seam;
    int flag=0;
    double e1a=0.0,e2a=0.0;
    int k, l,bc= 0 ,Old_NA=0 , iret=0 ,Use_Gauss_EA=0;
    RXEntity_p *olde=NULL;
    RXEntity_p bceptr = p->BorGcurveptr;
    class RXBCurve *bcptr= (class RXBCurve*) bceptr;
    VECTOR* base=NULL;

    assert(bceptr->TYPE != SEAMCURVE); // ie its not a subseamcurve.

    if(this->NoEnds) {
        RXSTRING buf;
        buf=L"NoEnds(GeoMouldSeam) for ent <"+  TOSTRING(e->name() ) + L">" ;
        rxerror(buf,3);
        return -1;
    }


    { /* block 4 not XYZabs */
        /* the red and black trigraphs. If both ends are on sites or curves, they should be identical. I fthey are on seams, they will be
   different.
   So  lets calculate the black one first and only the red one if end1ptr or end2ptr is a  seam else copy */


        if(p->Geo_Or_UV) /* block 40 */
            if(! Find_Geodesic(p,  &this->p_slr,&this->c_slr,&(this->G_Chord)) ) rxerror("Find_Geodesic returned 0",2);
        /* returns a straight from e1 to e2 if no mould*/

        if(! p->Geo_Or_UV) /* block 41 the flag may have been changed by the call */
            SeamCurve_Find_Straight( this->ev1.ToONPoint (),this->ev2.ToONPoint (), &this->p_slr,&this->c_slr,&(this->G_Chord));

        /* returns a straight from e1 to e2 if no mould*/

        assert(fabs(this->ev1.z -this->p_slr[0].z) < 0.01) ; // pretty weak assertion.
        assert (fabs(this->ev2.z -this->p_slr[this->c_slr-1].z) < 0.01);

        this->Red_Chord=p->GetArc(0); //  this->G_Chord;  // G_Chord is chord. arcs[0] is geodesic arclength.

        assert(bceptr);
        assert(bceptr->TYPE != GAUSSCURVE);

            if(seamflag) this->depth=this->depth/(float)2.0;


        bcptr = (class RXBCurve*)bceptr;
        bc=bcptr->c;
        base = (VECTOR *)MALLOC(sizeof(VECTOR)*(bc+1));
        // scaleBaseCurve just does anisotropic scaling from origin - no rotations or translations
        Scale_Base_Curve(bcptr->p, base,bc,this->depth,p->Xabs,p->Yabs,p->YasX,this->length);



        /* base is the base-curve. Y in m and x from 0 to 1
       UNLESS Xabs, in which case x is left unchanged    */

        /* attribute options are:
       flat/fanned.        Flat means ignore imposed angles.
       Xabs/Xscaled        Xabs means trim curve to fit (eg preserve length)
       Yabs/Yscaled /YasX  Yabs means use Y offsets, and depth as a multiplier
       Yscaled = use Y offsets, but scale so d=depth
       YasX means preserve aspect ratio during scaling	of x
       Yscaled is the default.
       */

        if(!p->Xabs) { /* block 53 */
            if (seamflag==1)
                flag=SEAMFLAG;
            else if (seamflag==0)
                flag=CURVEFLAG;
            else
                flag=UNKNOWNFLAG;
            if (p->m_isFlat )
                flag=FLATFLAG;

            olde = (RXEntity_p *)MALLOC((1 + p->n_angles)*sizeof(RXEntity_p ));

            for(k=0;k<p->n_angles;k++)
                olde[k] = (*p).ia[k]. s;
            Old_NA = p->n_angles;
            l = (*p).c[0];
            /* nasty fix-up for geodesics & UVs  WRONG */
            //	  assert(p->arcs[0]>0.0);
            if(p->GetArc(0) ==0.0) {
                p->m_arcs[0] = this->Red_Chord;
                printf(" fixup chord (2) in %s\n", p->name());
                rxerror("exit with zero arcs\n",5);
            }

            float l_len = p->GetArc(0);
            Make_Fanned_Experimental(p,flag,&l_len,&p->strt_A,this->Red_Chord,&bc,&base);

            /* maybe bc is not (*p).c[0] any more, or (*p).N_Angles is not as before  or some of the pointers
  in a are not as before. In any of these cases we return (-2) and set  Make Pside Request */
            if( Old_NA != p->n_angles)
                p->Make_Pside_Request =1;
            else {
                for(k=0;k<p->n_angles && k<Old_NA;k++) {
                    if(p->ia[k]. s != olde[k]) {
                        p->Make_Pside_Request = 1;
                        break;
                    }
                }
            }
            RXFREE(olde); olde=NULL;
        } /* end of block 53 if(!p->Xabs) */
        else	/* Xabs */
            Make_Absolute_BaseCurve(base,bc,this->Red_Chord,e->Esail ->m_Linear_Tol);


        /* 	else leave where it is */

        /* the polylines
       seams. 0 and 2 either side, based on red ends
       1 straight, based on black
       
       Curves  0 and 2 same side, based on red ends
       1 is same curve,  based on black  ends
       */
        Use_Gauss_EA = 0;
        if (p->seam) { /* block 10 */

            VECTOR*gcurve=NULL; int gc = 0;
            if(Get_Gaussian_Seam(p, &gcurve,&gc,this->m_schE1b,this->m_schE2b,this->ev1,this->ev2,this->Red_Chord,&e1a,&e2a))
            {/* block 11 */
                Use_Gauss_EA = 1;
#define ADDPOLY
#ifdef ADDPOLY
                //	if(g_TRACE) PolyPrint("Base before Add\n", base,bc);
                //	if(g_TRACE)PolyPrint("GC    before Add\n", gcurve,gc);
                PC_Add_Polylines(&base,&bc,&gcurve,&gc);
                /*		 Modifies base to include gaussian seaming.reallocs base */
                if(g_TRACE)PolyPrint("Base after  Add\n", base,bc);
                if(gcurve)RXFREE(gcurve); gcurve=NULL;
#else
                RXFREE(base); base=NULL;
                base=gcurve; bc=gc;
#endif
            }	/* end block 11 */
        }/* end block 10 if (p->seam)    */

        assert(this->mould && p->Geo_Or_UV);/* block 12 */
        newBuild_SC_Curves( base,bc);

        p->end1angle = - p->end1angle;
        p->end2angle = - p->end2angle;
        iret =  Measure_End_Angles(p,seamflag, Use_Gauss_EA , e1a,e2a ) || iret;	  /* 1 if endangles changed, else 0 */

        // Nasty fixup for M5
        p->end1angle = - p->end1angle;
        p->end2angle = - p->end2angle;




        iret = iret || PC_Polyline_Compare(this->m_Old_Plines[0],this->m_oldc[0], p->p[0], p->c[0], this->m_sc->Esail->m_Linear_Tol);
        iret = iret || PC_Polyline_Compare(this->m_Old_Plines[2],this->m_oldc[2], p->p[2], p->c[2], this->m_sc->Esail->m_Linear_Tol);

        /* The following is very slow, but it forces all ending-on SCs exactly to touch this one */
        //   	for (k=1;k<p->n_angles-1;k++) {
        //    	 	if(p->ia[k].s->TYPE==RELSITE || p->ia[k].s->TYPE==SITE  || p->ia[k].s->TYPE==SEAMCURVE )
        //			p->ia[k].s->SetNeedsComputing();
        //  	 }



        if(this->length > FLOAT_TOLERANCE) {
            if(fabs(this->length-p->GetArc(1)) > this->m_sc->Esail->m_Linear_Tol) {
                p->End2site->SetNeedsComputing();
            }
        }
        RXFREE(base); base=NULL;



        for(l=0;l<3;l++)
            p->m_pC[l]->Update(p->p[l] ,p->c[l]);

        if(p->Xabs) Update_Xabs_Offset(p);/* block 20 */

    } /* end of block 4 (its not an XYZabs */


    if(p->m_scMoulded && !p->Geo_Or_UV){ /* block 17 */
        PC_Pull_SC_To_Mould(e);
        for(l=0;l<3;l++)
            p->m_pC[l]->Update(p->p[l] ,p->c[l]);
    }
    if((p->FlagQuery(CUT_FLAG)) && p->Needs_Cutting && !p->sketch) {
        char str[256];
        sprintf(str," cannot CUT this kind of seam (Geo_Mould_Seam) - %s\n so deleting it", e->name());
        rxerror(str,2);
        p->FlagClear(CUT_FLAG);
        e->Kill( );
        //	k = (0!=Compute_Cut_Intersections(e));  May delete 'e' or demote it

        //	if(e->dat aptr&&!p->sketch)
        //		p->Needs_Cutting  = k;
        //	else
        return PCE_DELETED;
    }


    return(iret);
}
int RX_SeamcurveHelper::Compute_SeamCurve_Sample(RXEntity_p ee ) {

    assert(ee==(RXEntity_p) this->m_sc);
    /*	 Compute_ Seamcurve. IF the number or order of relSITES has altered,  it returns -2
  having set 'Make Pside Request' in the seamcurve structure*/
    /* METHOD.

     RETURNS  : 	-1 if an error found
     0  if no errors
     and the endangle changes less than ANGLETOL
     and no internal angles	overtake
     OR basecurveptr is null (ie TPN);
     1  otherwise
     
     <seamflag> is a flag which is 1 if its a seam, 0 if its a curve.
     
     We have two end points. They may be SITEs or offsets on curves or seams.
     First we find them. (courtesy of Com_SC_Trigraph)
     Then we call up the basecurve. We rotate it to fit
     between the endpoints taking the adjectives into account.
     We insert two HOOPS polylines and return the key of their containing segment.
     Those HOOPS polylines might later be used to interpolate relSITEs,
     but actually we use the arrays p1 and p2 of the seam
     
     Dealing with the IMPOSED_ANGLE.
     For seams we halve the IMPOSED_Angles.
     Also, we ignore the flag SIGN of an imposed_ANGLE.
     All flags are considered Positive
     
     For Curves, we use full IMPOSED_ANGLES and their SIGN flags.
     
     */
    sc_ptr  p = this->m_sc ;

    int seamflag= p->seam;
    int flag=0;
    double e1a=0.0,e2a=0.0;		//MSVC init check
    int k, l,bc= 0 ,Old_NA=0 , iret=0 ,Use_Gauss_EA=0;
    RXEntity_p *olde=NULL;
    RXEntity_p bceptr = p->BorGcurveptr;
    class RXBCurve*  bcptr=NULL;
    VECTOR* base=NULL;
    class RXGaussCurve *gcptr = dynamic_cast< class RXGaussCurve *>(bceptr);

    assert(bceptr->TYPE != SEAMCURVE); // ie its not a subseamcurve.

    if(this->NoEnds) {
        RXSTRING buf;
        buf=L"NoEnds(SC_default) ) for ent <" + TOSTRING(p->name()) +L">";
        rxerror(buf,3);
        return -1;
    }
    assert(!p->XYZabs);

    { /* block 4 not XYZabs */
        /* the red and black trigraphs. If both ends are on sites or curves, they should be identical. I fthey are on seams, they will be
   different.
   So  lets calculate the black one first and only the red one if end1ptr or end2ptr is a  seam else copy */


        if(p->Geo_Or_UV) /* block 40 */
            if(! Find_Geodesic(p,  &this->p_slr,&this->c_slr,&(this->G_Chord)) ) rxerror("Find_Geodesic returned 0",2);
        /* returns a straight from e1 to e2 if no mould*/

        if(! p->Geo_Or_UV) /* block 41 the flag may have been changed by the call */
            SeamCurve_Find_Straight(this->ev1.ToONPoint(),this->ev2.ToONPoint(), &this->p_slr,&this->c_slr,&(this->G_Chord));

        /* returns a straight from e1 to e2 if no mould*/
        if(fabs(this->ev1.z -this->p_slr[0].z) > this->m_sc->Esail->m_Linear_Tol) {
            PolyPrint(" p_slr", this->p_slr,this->c_slr);
            //PC_Insert_Arrow( this->ev1.ToONPoint() ,(float)30.0,"End1 error");
        }

        if(fabs(this->ev2.z -this->p_slr[this->c_slr-1].z) > this->m_sc->Esail->m_Linear_Tol){
            PolyPrint(" p_slr", this->p_slr,this->c_slr);
           // PC_Insert_Arrow(this->ev2.ToONPoint()  ,(float)30.0,"end2 error");
        }

        this->Red_Chord=this->G_Chord;

        if(bceptr) { 	 /*block 5 else its a RLX curve. just draw it */
            if(bceptr->TYPE== GAUSSCURVE){  /* block 51 */
                float tempchord= (float) 1.0; /* p->arcs[0];	*/
                gcptr-> Compute_GaussCurve( tempchord,seamflag ,this->m_sc->Esail->m_Linear_Tol);
                p->Xabs=0;
                p->Yabs=0;
                p->YasX=2; /* to trigger special treatment in Scale_BaseCurve. scale by (1+depth) */
            }
            else { /* block 52 */
                if(seamflag) this->depth=this->depth/(float)2.0;
             }

            bcptr = (class RXBCurve*) bceptr;
            bc=bcptr->c;
            base = (VECTOR *)MALLOC(sizeof(VECTOR)*(bc+1));

            Scale_Base_Curve(bcptr->p, base,bc,this->depth,p->Xabs,p->Yabs,p->YasX,this->length);



            /* base is the base-curve. Y in m and x from 0 to 1
       UNLESS Xabs, in which case x is left unchanged
       OR scaled to the sc length (if non-zero) */


            /* attribute options are:
       flat/fanned.        Flat means ignore imposed angles.
       Xabs/Xscaled        Xabs means trim curve to fit (eg preserve length)
       Yabs/Yscaled /YasX
                            Yabs means use Y offsets, and depth as a multiplier
       Yscaled = use Y offsets, but scale so d=depth
       YasX means preserve aspect ratio during scaling	of x
       Yscaled is the default.
       */

            if(!p->Xabs) { /* block 53 */
                // we MUST have bc from 0 to 1 here
                double x0 = base[0].x;
                double x1 = base [bc-1].x;
                int k,k2;
                VECTOR temp;
                if(x1<x0)
                {
                    for( k = 0; k<bc/2;k++)
                    {
                        k2 = bc-1 -k;
                        temp=base[k];
                        base[k]=base[k2];
                        base[k2]=temp;
                    }
                    if(!(bc&1)) {
                        qDebug()<<"TODO: verify reverse BC for EVEN in"<<m_sc->name();
                        for(k=0;k<bc;k++)
                            qDebug()<< base[k].x<< base[k].y<< base[k].z;
                    }
                }
                if (seamflag==1)
                    flag=SEAMFLAG;
                else if (seamflag==0)
                    flag=CURVEFLAG;
                else
                    flag=UNKNOWNFLAG;
                if (p->m_isFlat )
                    flag=FLATFLAG;

                olde = (RXEntity_p *)MALLOC((1 + p->n_angles)*sizeof(RXEntity_p ));

                for(k=0;k<p->n_angles;k++)
                    olde[k] = (*p).ia[k]. s;
                Old_NA = p->n_angles;
                l = (*p).c[0];
                /* nasty fix-up for geodesics & UVs  WRONG */
                //	  assert(p->arcs[0]>0.0);
                double l_redLen=0;
                if(p->seam) {
                    if( p->m_pC[0]->IsValid ()) l_redLen= p->GetArc(0);
                    else if ( p->m_pC[2]->IsValid ()) l_redLen= p->GetArc(2);
                    //else
                    //	cout<< " a seam without valid m_pC0 and 2"<<endl;
                }
                else // a curve
                    l_redLen= p->GetArc(1);
                if(l_redLen ==0.0) {   // this is OK while FInishing a Xscaled SC.
                    l_redLen=p->m_arcs[0] = this->Red_Chord;
                    //#ifdef DEBUG
                    //			printf(" fixup chord in %s  %f\n", p->name(), p->m_arcs[0] );
                    //#endif
                }
                float l_len = l_redLen;
// makeFanned creates bc and base
                Make_Fanned_(p,flag,&l_len,&p->strt_A,this->Red_Chord,&bc,&base); // can change BC

                /* maybe bc is not (*p).c[0] any more, or (*p).N_Angles is not as before  or some of the pointers
  in a are not as before. In any of these cases we return (-2) and set  Make Pside Request */
                if( Old_NA != p->n_angles)
                    p->Make_Pside_Request =1;
                else {
                    for(k=0;k<p->n_angles && k<Old_NA;k++) {
                        if(p->ia[k]. s != olde[k]) {
                            p->Make_Pside_Request = 1;
                            break;
                        }
                    }
                }
                RXFREE(olde); olde=NULL;
            } /* end of block 53 if(!p->Xabs) */
            else   { /* block 9 for Xabs */
                Make_Absolute_BaseCurve(base,bc,this->Red_Chord,p->Esail ->m_Linear_Tol); /* Xabs */
            }/* end block 9 */

            /* 	else leave where it is */

            /* the polylines
       seams. 0 and 2 either side, based on red ends
       1 straight, based on black
       
       Curves  0 and 2 same side, based on red ends
       1 is same curve,  based on black  ends
       */
            Use_Gauss_EA = 0;
            if (p->seam) { /* block 10 */

                VECTOR*gcurve=NULL; int gc = 0;
                if(Get_Gaussian_Seam(p, &gcurve,&gc,this->m_schE1b,this->m_schE2b,this->ev1 ,this->ev2 ,this->Red_Chord,&e1a,&e2a))
                {/* block 11 */
                    Use_Gauss_EA = 1;
#define ADDPOLY
#ifdef ADDPOLY
                    //	if(g_TRACE) PolyPrint("Base before Add\n", base,bc);
                    //	if(g_TRACE)PolyPrint("GC    before Add\n", gcurve,gc);
                    PC_Add_Polylines(&base,&bc,&gcurve,&gc);
                    /*		 Modifies base to include gaussian seaming.reallocs base */
                    if(g_TRACE)PolyPrint("Base after  Add\n", base,bc);
                    if(gcurve)RXFREE(gcurve); gcurve=NULL;
#else
                    RXFREE(base); base=NULL;
                    base=gcurve; bc=gc;
#endif
                }	/* end block 11 */
            }/* end block 10 if (p->seam)    */

            if(this->mould && p->Geo_Or_UV)/* block 12 */
                newBuild_SC_Curves(base,bc);
            else  {
                oldBuild_SC_Curves(base,bc);
                if(PCP_Polyline_Remove_Close_Points(p->p[0],&(p->c[0]),p->Esail->m_Linear_Tol)){
                    //cout<< " curve 0 cleaned"<<endl;
                    p->p[0]=(VECTOR*)REALLOC(p->p[0],p->c[0]*sizeof(VECTOR));
                }
                if(PCP_Polyline_Remove_Close_Points(p->p[2],&(p->c[2]),p->Esail->m_Linear_Tol)){
                    //cout<< "curve 2 cleaned"<<endl;
                    p->p[2]=(VECTOR*)REALLOC(p->p[2],p->c[2]*sizeof(VECTOR));
                }
            }

            iret =  Measure_End_Angles(p,seamflag, Use_Gauss_EA , e1a,e2a ) || iret;	  /* 1 if endangles changed, else 0 */

            iret = iret || PC_Polyline_Compare(this->m_Old_Plines[0],this->m_oldc[0], p->p[0], p->c[0], this->m_sc->Esail->m_Linear_Tol);
            iret = iret || PC_Polyline_Compare(this->m_Old_Plines[2],this->m_oldc[2], p->p[2], p->c[2], this->m_sc->Esail->m_Linear_Tol);

            /* The following is very slow, but it forces all ending-on SCs exactly to touch this one */
            //   	for (k=1;k<p->n_angles-1;k++) {
            //    	 	if(p->ia[k].s->TYPE==RELSITE || p->ia[k].s->TYPE==SITE  || p->ia[k].s->TYPE==SEAMCURVE )
            //			p->ia[k].s->SetNeedsComputing();
            //  	 }

            if(this->length > FLOAT_TOLERANCE) {
                if(fabs(this->length-p->GetArc(1)) > this->m_sc->Esail->m_Linear_Tol) {
                    p->End2site->SetNeedsComputing();
                }
            }
            RXFREE(base); base=NULL;

        } /*end of block 5 if basecurve non-null */
        else {   /* block 14 its a RLX curve */
            p->SnapEnds();			/*OK we get UpdateCurve twice */
        } /* end block 14 its a RLX curve */

        for(l=0;l<3;l++)
            p->m_pC[l]->Update(p->p[l] ,p->c[l]);

        if(p->Xabs) Update_Xabs_Offset(p);/* block 20 */
    } /* end of block 4 (its not an XYZabs */


    if(p->m_scMoulded && !p->Geo_Or_UV){ /* block 17 */
        PC_Pull_SC_To_Mould(p);
        for(l=0;l<3;l++)
            p->m_pC[l]->Update(p->p[l] ,p->c[l]);
    }
    if((p->FlagQuery(CUT_FLAG)) && p->Needs_Cutting && !p->sketch) {
        QString str;
        str= QString("cannot CUT this kind of seam (Generic) - ")
                + QString(p->name() )
                + QString("\n atts=<" )
                + QString( p->AttributeString() )
                + QString(">\n so deleting it")
                ;
        rxerror(qPrintable (str),2);
        p->FlagClear(CUT_FLAG);
        return PCE_DELETED;
    }

    return(iret);
}

int LeftOrRight(const VECTOR p_a, RXEntity_p sce) // only used in picking
//Use to be in curve.h /curve .cpp moved here by thomas 06 07 04 while creating RXCurve
{

    // this routine tests whether the point p is 'left' or 'right' of the SC;  Used in selection

    //  drop a perp to the curve
    //get the cross of the tangent and the perp.
    //dot it with global Z.
    //If its >0 we are left
    //	else we are right
    //or vice versa

    ON_3dVector t,ra,xx, l_z ;
    ON_3dPoint r;

    ON_3dPoint a(p_a.x,p_a.y,p_a.z);
int errflag;
    double dot, s, dist, tol = 0.001;
    sc_ptr sc;

    if(!sce || sce->TYPE !=SEAMCURVE) return 0;
    sc = (sc_ptr  )sce;
    s = sc->m_pC[1]->Get_chord() /2.0;
    assert(!sc->m_pC[1]->GetType());
    errflag = sc->m_pC[1]->Drop_To_Curve(a ,&r, &t,tol,&s,&dist);
    if(errflag <=0)
        sce->OutputToClient(  "LeftOrRight failed  ",5 );
    ra = a-r;
    xx = ON_CrossProduct(ra,t);

    l_z =sce->Normal();
    dot = ON_DotProduct(l_z,xx);

    if(dot > 0.0) return 1;

    if(dot < 0.0) return  -1;

    return 0;
}//int LeftOrRight




