#include "StdAfx.h"
#include <QDebug>
#include <QTemporaryFile>
#include "unixver.h"
#include "RXSummaryTolerance.h"
#include "RelaxWorld.h"
#define  SUMDB  (0) // (1) or (0)
#include "pctable.h"
#include "summary.h"
#include "global_declarations.h"
#include "stringutils.h"
#include "words.h"
#include "akmutil.h"
#include "entities.h"
#include "RX_UI_types.h"

#include "RXLogFile.h"

RXLogFile::RXLogFile(void) :
    filename(0),
    N_Items(0),
    list(0),
    fp(0),
    Fopen_Flags(0),  /* "r" or "w" */
    Run_To_End(0),	/* if set, run all the lines to the end of the file. Else just run this line */
    List_Has_Changed(0),
    sep(0),
    type(0), // ie Summary file, state file, other.
    Last_Summary_Line(0)  // currently its own global. Could be confusing between different summaries.

{
}

RXLogFile::~RXLogFile(void)
{
    Close();
}

int RXLogFile::Open(const char*p_filename,const char*what) {

    /* 	A misnomer. This does not open the file in the classic sense
 Rather it hooks it up to the summary system
 by counting lines, cols, etc */

    FILE *f;
    int  nl=0;
    assert(!cout.bad());
    char string[256];

    /* start by counting no of lines
     then get the last one
     and post it
     */
    /* cleanfile in case Windows had it open */
    if(!p_filename)
        return(0);

    this->filename = STRDUP(p_filename);
    this->sep = PCT_Table_Separator(p_filename);
    this->Run_To_End=0;
    this->N_Items=0;
    if(this->fp){
        cout<< "Close Summary because open\n"<<endl;
        this->Close ();
    }
    this->fp=NULL;
    this->List_Has_Changed=0;assert(!cout.bad());

    nl = clean_summary_file(p_filename);assert(!cout.bad());

    if(nl < 0) {
        f = RXFOPEN(p_filename,"w"); if(SUMDB) rxerror(" clean_summary_file returned <0",1);
        if(!f) {
            rxerror("Cannot Create new summary file",2);
            return(0);
        }
        else
        {FCLOSE(f); f=NULL;}
    }
    this->fp=RXFOPEN(p_filename,"a+"); assert(!cout.bad());
    if (!this->fp) {
        /* put shareing check in here */
        sprintf(string," Please CLOSE \n%s in excel\nBEFORE pressing Continue",p_filename);
        rxerror(string,2);
    }
    FCLOSE(this->fp);

    this->fp=RXFOPEN(p_filename,"r");

    FCLOSE(this->fp);
    this->fp=NULL;

    nl = nl-2;
    if(nl < 0) {
        if(SUMDB) rxerror("file has 1 row or 2nd row not newline terminated",1);
        return(0);
    }
    assert(!cout.bad());
    read_and_post_one_row(nl,QString(""),this);assert(!cout.bad());
    List_Has_Changed = 0;
    return(1);
}

char * RXLogFile::SetFileName( const char *f){ 
    if( filename) RXFREE (filename);
    filename = STRDUP(f);
    return filename;
}
std::string RXLogFile::ScriptCommand() const{
    if(m_DBtype&RXDB_INPUT) {
        if(!this->GetFileName().empty()
                //                && (!is_empty(g_World->m_Last_Summary_File)
                //                    &&(this->GetFileName()!=string( g_World->m_Last_Summary_File) ) )
                ) {
            return string("open_input: ") +this->GetFileName();
        }
        else{
            cout<<" RXLogFile::ScriptCommand RXDB_INPUT with " <<this->GetFileName()<<endl;
            return string("! (RXLogFile::ScriptCommand)");
        }
    }
    if(m_DBtype&RXDB_OUTPUT) {
        return string("open_log : ") +this->GetFileName();
    }
    return string("! (RXLogFile::ScriptCommand)  conditions not met");

}
void  RXLogFile::SetLast(const QString h, const QString v){
    qDebug()<<" TODO:  RXDBLinkMySQL::SetLast" ;
}

int  RXLogFile::Apply_Summary_Values()  //   applies the currently set values to the models 
{
    int retVal=0;
    int i;
    class SUMMARY_ITEM *s;

    for(i=0,s=this->GetListHead();i<this->GetNCols();i++,s++) {
        retVal +=  s->apply_one_summary_item();//// the workhorse for acting on DB items.
    }

    retVal = g_World->Bring_All_Models_To_Date();
    return retVal;
}
// we identify the row by both rownumber and rowname because
// the csv file uses row number but the database uses runname
int RXLogFile::read_and_post_one_row(int Row, const QString pRunName,MIRRORPTR dest) {



    char **d=NULL, **h=NULL;
    int n,kh=0,kd=0, k=0;
    int retVal=0;
    char buf[MAX_LINE_LENGTH];
    int curline,flag;
    assert(!cout.bad());
    if(!this->filename) {
        return(retVal);
    }
    if(Row==SUMMARY_TAIL ){//count the lines
        rxerror(" Summary Tail not tested ",2);
        Row = this->RowCount();
    }
    this->fp=RXFOPEN(this->filename,"r");
    this->sep = PCT_Table_Separator(this->filename);assert(!cout.bad());
    if(!this->fp) return 0;

    Read_Next_Table_Line(this->fp,&h,&kh,this->sep); /* the headers */
    assert(!cout.bad());
    kd = kh;
    curline = 0;
    while(curline < Row) {
        if(!fgets(buf,MAX_LINE_LENGTH,this->fp)) {
            rxerror("File does not appear to have enough lines in it",2);
            return(retVal);
        }
        assert(!cout.bad());
        if(strlen(buf) > MAX_LINE_LENGTH-1)
            rxerror(" (RAP) summary file is > 128000 characters. Crash Likely. Please remove some columns",3);
        curline++;
    }
    assert(!cout.bad());
    Read_Next_Table_Line(this->fp,&d,&kd,this->sep);	 /* the values */
    assert(!cout.bad());
    if(kd != kh) {
        sprintf(buf,"row (%d) in '%s' has different no. of columns to the Header Line\n%d not %d\n crash likely",Row,this->filename,kd,kh);
        rxerror(buf,3);
    }
    n = min(kd,kh);
    for(k=0;k<n;k++) { assert(!cout.bad());
        if(SUMDB) printf(" (read and post)  %s = %s\n", h[k],d[k]); assert(!cout.bad());
        Summary_Type(h[k], &flag);
        assert(!cout.bad());
        if(dest )
            dest->post_summary_value(h[k],d[k]);
        assert(!cout.bad());
        retVal++;
    }
    //    if(this== g_World->DBin() || this== g_World->DBMirror()) { // HORRIBLE HEURISTIC. A summary should carry a flag for this.
    //        g_World->m_Last_Summary_Line =Row;
    //        if(g_World->m_Last_Summary_File) RXFREE (g_World->m_Last_Summary_File) ;
    //        g_World->m_Last_Summary_File = STRDUP(this->filename);
    //        if(SUMDB) printf("(rpol) g_Last_Summary_File ='%s' line %d\n",g_World->m_Last_Summary_File ,g_World->m_Last_Summary_Line);
    //    }
    for(k=0;k<kh;k++) RXFREE(h[k]);
    for(k=0;k<kd;k++) RXFREE(d[k]);
    if(d)
        RXFREE(d);
    if(h)
        RXFREE(h);

    FCLOSE(this->fp);
    this->fp = NULL;
    assert(!cout.bad());
    return(retVal);
}

class SUMMARY_ITEM * RXLogFile::add_new_item(char *label,char *v_in)
{ 
    int k,flag;
    char *value = v_in;
    SUMMARY_ITEM * i;

    /* a new item to add */
    k=(this->N_Items)++;
    this->list=(SUMMARY_ITEM *)REALLOC(this->list,(k+1)*sizeof(SUMMARY_ITEM));
    i = &(this->list[k]);
    i->Init();assert(!cout.bad());
    this->list[k].type   = Summary_Type(label,&flag);assert(!cout.bad());
    this->list[k].m_flag = flag;
    this->list[k].owner = this;

    this->list[k].header = STRDUP(label);assert(!cout.bad());
    // RXSummaryTolerance::Set_Tolerance(&(this->list[k]),label);  /* set up current tolerances on summary data */
    this->list[k].PCS_Evaluate_Summary_Data(value);assert(!cout.bad());
    this->list[k].m_Has_Changed = 0;
    this->List_Has_Changed=1;
    if SUMDB
            printf(" adding item %d %s , data %s\n",k, label,value);
    assert(!cout.bad());
    return(i);
}
int RXLogFile::Create_Filtered_Summary(class RXMirrorDBI *snew,char *filter){ // INPUT flag only
    int i;
    int retVal=0;
    // should call this with the sd not the sd->sailsum and then check existence of the ent.
    // To prevent virussing of historic data.

    if(snew) {
        for(i=0;i<this->N_Items - 1;i++) {
            if(stristr(filter,this->list[i].header) || stristr(this->list[i].header,filter)) {

                if(this->list[i].m_flag&SUM_INPUT && (!strieq(this->list[i].text,"N/A"))){
                    snew->post_summary_value(this->list[i].header,this->list[i].text);
                    retVal++;
                }
            }
        }
    }
    return(retVal);
}

int RXLogFile::Write_MirrorDB_As_CSV(char *filename)
{
    class RXLogFile *s=this;
    FILE *fnew;
    int i; char sep;

    int retVal = 0;

    fnew =RXFOPEN(filename,"w");
    sep = PCT_Table_Separator(filename);
    if(!fnew)
        rxerror(" couldnt open fnew",2);
    for(i=0;i<s->N_Items - 1;i++) {
        fprintf(fnew,"%s%c",s->list[i].header,sep);
    }
    if(s->N_Items > 0)
        fprintf(fnew,"%s\n",s->list[i].header);
    else
        fprintf(fnew,"\n");

    FCLOSE(fnew);
    s->filename = STRDUP(filename);
    s->sep = PCT_Table_Separator(s->filename);
    s->List_Has_Changed = 0;
    s->Write_Line(g_World->DBMirror() );
    /* that is a copy of current data */
    return(retVal);
}
int RXLogFile::Write_Script (FILE *fp  )
// for all inputs, go 'apply ':header: value'
{
    int i, rc=0;
    if(fp) {
        fprintf(fp,"\n!the mirror (a logfile) \n" );
        for(i=0;i<this->N_Items - 1;i++) {
            // if(stristr(filter,this->list[i].header) || stristr(this->list[i].header,filter)) {
            if(this->list[i].m_flag&SUM_INPUT && (!strieq(this->list[i].text,"N/A"))){
                rc+= fprintf(fp, "apply: %s : %s\n",this->list[i].header,this->list[i].text) ;
            }
        }
        //  }
    }
    return rc;
}

int RXLogFile::Update_Summary_File () {
    FILE*fold, *fnew;class RXLogFile *s=this;
    int row;
    /* copy old summary values
     read each line
     write each new line
     copy old summary back
     */
    int nl=0;
    int i;

    char *new_fname=NULL;
    char *current_fname,*old_fname;
    char buf[256], sep;

    if(s->List_Has_Changed)
        rxerror("regenerating summary database ",1);
    else
        rxerror("regenerating summary database BUT WHY  ",1);

    s->List_Has_Changed=0;
    nl = clean_summary_file(s->filename);
    if(nl < 0) {
        rxerror("Summary File seems to have been deleted - RECREATING",2);
        fold = RXFOPEN(s->filename,"w");
        s->sep = PCT_Table_Separator(s->filename);
        if(!fold) {
            rxerror("Failed to create summary file",3);
            return(0);
        }
        else
            FCLOSE(fold);
    }

    new_fname = s->filename;
    sprintf(buf,"%s_copy",s->filename);
    old_fname = STRDUP(buf);

    sprintf(buf,"%s %s %s",RX_SYSCOPY, s->filename,old_fname);
    if(SUMDB) printf("Update_Summary_File  . system %s\n",buf);
    system(buf);

    current_fname = STRDUP("this_line.txt");
    /* make file copy of current settings */
    fnew =RXFOPEN(current_fname,"w");
    sep = PCT_Table_Separator(current_fname);
    if(!fnew)
        rxerror(" couldnt open fnew",2);

    for(i=0;i<s->N_Items - 1;i++) {
        fprintf(fnew,"%s%c",s->list[i].header,sep);

    }
    if(s->N_Items) {
        fprintf(fnew,"%s\n",s->list[i].header);

    }
    else
        fprintf(fnew,"\n");

    FCLOSE(fnew);
    s->filename = current_fname;
    s->Write_Line(g_World->DBMirror());


    for(i=0;i<s->N_Items;i++)
        s->list[i].in_memory = 1;

    s->filename = old_fname;

    /* that is a copy of current data */


    fold =RXFOPEN(s->filename,"r");
    s->sep = PCT_Table_Separator(s->filename);
    if(!fold){
        sprintf(buf,"PLEASE CLOSE sumfile '%s'",s->filename);
        rxerror(buf,2);
    }
    else
        FCLOSE(fold);

    /* header in new file */
    fnew =RXFOPEN(new_fname,"w");
    sep = PCT_Table_Separator(new_fname);
    if(!fnew) rxerror(" couldnt open new_fname",2);

    for(i=0;i<s->N_Items - 1;i++) {
        fprintf(fnew,"%s%c",s->list[i].header,sep);
    }
    if(s->N_Items > 0)
        fprintf(fnew,"%s\n",s->list[i].header);
    else
        fprintf(fnew,"\n");

    if(fnew) FCLOSE(fnew);

    nl = nl-1;
    for(row=0;row<nl;row++) {
        this->filename = old_fname;
        this->read_and_post_one_row(row,QString(""),this);
        this->filename = new_fname;
        this->Write_Line(g_World->DBMirror());
    }

    /* put back last data line */
    row = 0;
    this->filename = current_fname;
    this->read_and_post_one_row(row,QString(""),this);
    this->filename = new_fname;
    RXFREE(current_fname);
    RXFREE(old_fname);

    return(1);
}
int RXLogFile::RowCount()
{
    class RXLogFile * sum=this;
    int row = 0;
    struct PC_TABLE*t;
    FILE *fp = RXFOPEN(sum->filename,"r");
    sum->sep = PCT_Table_Separator(sum->filename);
    if(fp){
        if(PCT_Read_Table( fp,&t ,sum->sep)) {
            if(SUMDB) printf(" Nr = %d Nc = %d\n", t->nr, t->nc);
            row = t->nr-1;
            if(SUMDB) printf(" File %s has %d rows", sum->filename,row);
            PCT_Free_Table(t);
        }

        FCLOSE(fp);

    }
    return row;

}
int RXLogFile::Extract_Column(const char*fname,const char*head,char***clist,int*N) {

    /* read the file and return  the column headed 'head' in (list,N). */
    FILE*f;
    char buf[256],sep;
    struct PC_TABLE*t;
    int k;

    f =RXFOPEN(fname,"r");
    if(!f)  {
        if(SUMDB) printf(" cant open <%s> to read\n",  fname);
        return(0);
    }
    sep = PCT_Table_Separator(fname);
    if(SUMDB) printf(" Extract Column with %s  '%c'\n", fname,sep);
    *clist=NULL;
    if(PCT_Read_Table( f,&t,sep)) {
        if(SUMDB) printf(" Nr = %d Nc = %d\n", t->nr, t->nc);
        PCT_Extract_Series(t,VERTICAL,head,N,clist);
        PCT_Free_Table(t);
        if(SUMDB) {
            cout<< "PCT_Extract_Series got \n"<<endl;
            for(k=0;k<*N;k++) printf(" Row %d  = %s\n", k,(*clist)[k]);
        }
    }
    else{
        sprintf(buf,"Cannot read a rectangular table from \n%s",fname);
        rxerror(buf,2);
    }

    FCLOSE(f);

    return(1);
}
//  modify 'value' to ensure that it differs from all existing records
int  RXLogFile::MakeUnique(const QString &header, QString &value)
{
    return 0;

}

int RXLogFile::Write_Line  ( class RXMirrorDBI*MirrorSource)
{
    /* 	3.1) check to see if the header list has changed.
         If it has, re-write the existing file but with the new header lists.
         set the file marker to the end.
         3.2	append the new line		 */

    int k=-1; /* dont prepend model name */

    if(!MirrorSource) MirrorSource=this;


    if(!this->filename) {
        rxerror("NO summary file open - NO SUMMARY DATA WRITTEN'",1);
        return(0);
    }
    if(this->List_Has_Changed)
        this->Update_Summary_File();

    if(validfile(this->filename) != 1) {
        /* regenerate it anyway */
        char buf[200];
        sprintf(buf,"Summary file '%s' has been deleted ?",this->filename);
        rxerror(buf,2);
        this->fp=RXFOPEN(this->filename,"a");
        if(this->fp) {
            this->sep = PCT_Table_Separator(this->filename);
            for(k=0;k< this->N_Items-1;k++) {
                fprintf(this->fp,"%s%c",this->list[k].header,this->sep);
            }
            if(this->N_Items > 0)
                fprintf(this->fp,"%s\n",this->list[k].header);
            else
                fprintf(this->fp,"\n");
            FCLOSE(this->fp);
        }
        this->fp=NULL;
    }

    this->fp=RXFOPEN(this->filename,"a");
    this->sep = PCT_Table_Separator(this->filename);
    if(! this->fp) {
        char buf[200];
        sprintf(buf,"Summary file '%s' cant be opened",this->filename);
        rxerror(buf,2);
        return 0;
    }
    for(k=0;k< this->N_Items-1;k++) {
        if(this->list[k].in_memory == 0)
            fprintf(this->fp,"%s%c",this->list[k].text,this->sep);
        else
            fprintf(this->fp,"N/A%c",this->sep);
    }
    if(this->N_Items > 0)
        if(this->list[k].in_memory == 0)
            fprintf(this->fp,"%s\n",this->list[k].text);
        else
            fprintf(this->fp,"N/A\n");
    else
        fprintf(this->fp,"\n");

    FCLOSE(this->fp); this->fp=NULL;

    return(1);
}

int RXLogFile::Close () {
    // Usually  Executes following a menu selection.

    if(this->fp)
        FCLOSE(this->fp);
    this->fp = NULL;
    if(this->filename)
        RXFREE(this->filename);
    this->filename=NULL;
    this->FlushMirrorTable ();
    return (1);
}
int RXLogFile::StartTransaction ()
{
    return 0;
}

int RXLogFile::Commit() {
    return 0;
}

int RXLogFile::FlushMirrorTable ( ) {
    /*  Empties the list but leaves filename and fp intact */
    int k;
    for(k=0;k<this->N_Items;k++)  {
        //if(this->list[k].hoopskey) {
        //		   HC_Open_Segment_By_Key(this->list[k].hoopskey);
        //		   HC_Set_User_Index(RXINDEX_SUMMARY,(void*)-1);
        // 		  HC_Open_Segment("new");
        // 		  HC_Insert_Text(0.0,0.0,26.0,"disconnected");
        //		   HC_Close_Segment();
        // 		  HC_Close_Segment();
        //	 }

        if(this->list[k].header) {RXFREE (this->list[k].header);this->list[k].header=NULL;}
        if(this->list[k].text) 	{ RXFREE (this->list[k].text);this->list[k].text=NULL;}

    }
    if(this->list)
        RXFREE(this->list);

    this->list=NULL;
    this->N_Items=0;
    return(1);
}//int Flush 	 
int  RXLogFile::PostFile( const QString &label,const QString &fname)
{
    int rc=0;

    return rc;
}

int RXLogFile::post_summary_value( const string &label,const string &value){
    return this->post_summary_value(label.c_str(),value.c_str());
}
int RXLogFile::post_summary_value( const char*labelin,const char*valuein){
    int k;
    SUMMARY_ITEM *nsi;
    char label[256], value[512];
    assert(!cout.bad());
    if(rxIsEmpty(labelin) || rxIsEmpty(valuein))
        return 0;
    strncpy(label, labelin,255);  // insulation  April 2003
    strncpy(value, valuein,510);
    if(strlen(valuein) > 255) printf(" caught truncated posting <%s> \n",valuein);
    PC_Strip_Trailing(label);
    PC_Strip_Trailing(value);
    PC_Strip_Leading(label);
    PC_Strip_Leading(value);
    if(rxIsEmpty(label) || rxIsEmpty(value)) {
        rxerror("posted all spaces !",1);
    }
    assert(!cout.bad());
    if(SUMDB)
        printf("post '%s' '%s' \n",  label,value);
    for(k=0;k<this->N_Items;k++) {
        if(strieq(label,this->list[k].header)) {
            this->list[k].PCS_Evaluate_Summary_Data( value);//,FROM_PROG);
            this->list[k].in_memory = 0;assert(!cout.bad());
            return(1);
        }
    }
    /* a new item to add */
    assert(!cout.bad());
    nsi = this->add_new_item(label,value);//,FROM_PROG);
    assert(!cout.bad());
    nsi->in_memory = 0;
    return(1);
}
double RXLogFile::Extract_One_Value( const char*p_label,double*value){
    class RXLogFile *sum=this;
    int k;
    double V;
    char buf[256],label[512];
    assert(strlen(p_label) < 500);
    strcpy(label,p_label);
    PC_Strip_Trailing(label);
    PC_Strip_Leading(label);

    if(rxIsEmpty(label)) {
        rxerror("CANT extract a blank !",1);
    }

    for(k=0;k<sum->N_Items;k++) {
        if(strieq(label,sum->list[k].header)) {
            *value =V = sum->list[k].value;
            return(V);
        }
    }
    if(SUMDB) {
        sprintf(buf,"cant extract %s. Not found",label);
        rxerror(buf,1);
    }
    return(0.0);
}



int RXLogFile::Read_Next_Table_Line(FILE *f,char***wlist,int *d,char sep)
{
    assert(f==this->fp);
    int cnt,i,bad=0;
    char **words,**wd,**ls, seps[4];
    char line[MAX_LINE_LENGTH],*lp;

    *d = 0;
    do {
        if(bad) cout<< " trying again\n"<<endl;
        if(!fgets(line,MAX_LINE_LENGTH,f))
            return(0);
        bad=0;

        if(strieq(line,"endblock\n")) {  cout<< " ignoring endblock\n"<<endl; bad=1;}
        PC_Strip_Leading(line);
        if(*line=='!') {  cout<< "ignoring comment line\n"<<endl; bad=1;}
    } while (bad);

    if(strlen(line) > MAX_LINE_LENGTH-1)
        rxerror("width of summary file is > 128000 characters. Crash likely.\n Please remove some columns",3);
    while((lp = strchr(line,13))) {
        printf("\n changing \n<%c> to blank in Read_Next_Table_Line\n",*lp);
        *lp = ' ';
    }
    sprintf(seps,"%c\n",sep);
    cnt = make_into_words(line,&words,seps); // was akm_
    if(!cnt)
        return(0);/*  wasteful */
    (*wlist) = (char **) CALLOC(cnt,sizeof(char *));
    for(ls = (*wlist),i=0,wd = words;i<cnt;i++,wd++,ls++) {
        (*ls) = STRDUP(*wd);
    }
    RXFREE(words);
    *d = cnt;
    return(cnt);
}	


int RXLogFile::Print(FILE *f){
    int k;
    fprintf(f,"\n  SUMMARY LISTING \n\n");
    fprintf(f,"  Filename         %s\n",this->filename);
    fprintf(f,"  Fopen_Flags      %s\n",this->Fopen_Flags);
    fprintf(f,"  Run_To_End       %d\n",this->Run_To_End);	/* if set, run all the lines to the end of the file. Else just run this line */
    fprintf(f,"  N_Items          %d\n",this->N_Items);
    fprintf(f,"  List_Has_Changed %d\n",this->List_Has_Changed);
    for(k=0;k<this->N_Items;k++) {
        SUMMARY_ITEM &i = this->list[k];
        fprintf(f," no %d\n  header         %s\n",k,i.header);
        fprintf(f,"  type           %d\n",i.type);
        fprintf(f,"  Has_Changed    %d\n",i.m_Has_Changed);
        /* void*input_data; string, int double depending on type
       ONLY USED ON INPUT SIDE */
        fprintf(f,"  text           %s\n",i.text);
        fprintf(f,"  tol            %f\n",i.m_tol);
        fprintf(f,"  value          %f\n",i.value);
        fprintf(f,"  oldvalue       %f\n",i.m_oldvalue);
        fprintf(f,"  Check_Tol      %d\n",i.m_check_Tol);
        //fprintf(f,"  hoopskey       %ld\n",i.hoopskey);

    }

    return(1);
}


int RXLogFile::clean_summary_file(const char *fname)
{
    /* This takes the filename and makes sure that there are the
   correct endings and also that there are no blank lines at the end.
 */
    char *line;
    int retVal = -1, quotes = 0;
    FILE *fin ;
    char   buf[512];
    char *lp;

    line = (char*)MALLOC(sizeof(char)*MAX_LINE_LENGTH);
    if(!line) {
        rxerror("out of memory in clean_summary_file",3);
    }
    QTemporaryFile tfile;

    fin = RXFOPEN(fname,"r");
    tfile.open();

    if(!fin || !tfile.isWritable()) {
        if(SUMDB)
            rxerror("Failed to open files in clean_summary_file",1);
        if(!fin)  printf("(clean_summary_file) couldnt open '%s' for reading\n",fname);
        else 	FCLOSE(fin);

    }
    else {
        retVal = 0;
        while((lp = fgets(line,MAX_LINE_LENGTH,fin))) {
            if(strlen(line) >= MAX_LINE_LENGTH-1)
                rxerror("(clean) summary file width > 128000 characters.Crash Likely\n Please remove some columns",3);
            while(*lp) {
                if(*lp == '\r' || *lp == '\n')
                    *lp = '\0';
                lp++;
            }
            PC_Strip_Leading(line);
            PCS_Strip_Trailing(line); // special to strip commas too
            if((*line)) {    /* this is a non - blank line */
                if (strchr(line,34)) quotes++;
                // fprintf(fout,"%s\n",line);
                tfile.write(line,strlen(line)) ; tfile.write("\n",1);
                retVal++;
            }
        }
        FCLOSE(fin);
        tfile.close();
        if(quotes) {
            sprintf(buf, "Log file '%s' contains %d quotation marks\nDid you save it from a spreadsheet with incorrect parameters?",fname, quotes);
            rxerror(buf,2);
        }
        sprintf(line,"$RLX_LOGFILE_PROCESS %s %s",qPrintable(tfile.fileName()),fname);
#ifndef linux
        cout<<" TODO:  Logfile process on MSW"<<endl;
#endif
        system(line);
    }

    RXFREE(line);

    return(retVal);
}




int RXLogFile::Extract_One_Text(const char *label,char*value,const int buflength){
    int k;
    class RXLogFile *sum=this;
    //   PC_Strip_Trailing(label);   PC_Strip_Leading(label);

    if(rxIsEmpty(label))
        rxerror("CANT extract a blank !",1);

    *value=0;
    for(k=0;k<sum->N_Items;k++) {
        if(strieq(label,sum->list[k].header)) {
            strncpy(value,sum->list[k].text,buflength);
            return(1);
        }
    }
    return 0;
}
int RXLogFile::RemoveAllWithPrefix(const std::string & prefix)
{
    int k,nn, rc=0;
    const char*n;
    std::string  pp = "$"+prefix+"$";
    for(k=0;k<this->N_Items;k++) {
        n = this->list[k].header;
        if(0==strncmp(n,pp.c_str(),pp.length() )){
            class SUMMARY_ITEM *p1, *p2;

            if(k==this->N_Items-1 ) {
                this->N_Items--;
                break;
            }
            p1 = &(this->list[k] );
            delete p1;
            p2 = &(this->list[k+1] );
            nn =  this->N_Items-k-1;
            memcpy(p1,p2,nn*sizeof( class SUMMARY_ITEM  ));
            this->N_Items--;
            k--;
        }

    }
    return rc;
}

std::string RXLogFile::Extract_Headers(const string &what){ //is 'input', 'output' or 'all'
    // returns a comma-separated list of the output summary headers.
    std::string lp;
    char *n;
    class RXLogFile *sum = this;
    int k,flag;

    if(what=="input") flag = SUM_INPUT;
    else if(what=="output" ) flag = SUM_OUTPUT;
    else flag = SUM_OUTPUT+SUM_INPUT;

    for(k=0;k<sum->N_Items;k++) {
        if(sum->list[k].m_flag&flag) {
            n = sum->list[k].header;
            if(!rxIsEmpty(n)) {
                lp+=n ; lp+=",";
            }
            else {
                lp+="  ,";
            }
        }
    }
    return lp;
}
int RXLogFile::Replace_Words_From_Summary(char **strptr, const int append){
    /* if $header is found in buf, replace it with text
If 'append' is non-zero replace $AWA with $AWA=<value>
 or replace any string from $AWA to > (but not containing '$')  with $AWA=<value>
 */

    int i,c=0;
    char buf[256], bnew[256], *cp, *a,*b, *d,*stmp;

    class SUMMARY_ITEM *s;

    if(strptr && *strptr) {
        for(i=0,s=GetListHead();i<GetNCols();i++,s++) {
            sprintf(buf,"$%s",s->header);
            if(append) {
                sprintf(bnew,"%s=<%s>", buf, s->text);
                stmp=STRDUP(*strptr);
                RXFREE(*strptr);
                //printf("enter Replace_String with '%s' '%s'->'%s'\n",stmp,buf,bnew);
                cp = Replace_String(&stmp, buf,bnew);

                *strptr=stmp;

                if(cp) {
                    // printf(" replace_String '%s'->'%s' in '%s' gave\n cp='%s'\n===\n", buf,new,str, cp);
                    /* now strip $AWA=<21.936>=<22.157> to $AWA=<21.936> */
                    cp += strlen(bnew);
                    a = strchr(cp,'$'); b = strchr(cp,'>');
                    if((b &&a &&(b < a)) || ( b && !a)) {
                        for( d = cp; d<=b; d++) *d = ' ';
                        PC_Strip_Leading_Chars(cp," ");
                    }
                    c++;
                }

            }
            else  {
                if(NULL !=Replace_String(strptr, buf,s->text))
                    c++;;
            }

        }
    }

    return c;
}


int RXLogFile::post_summary_value_from_file(char*label,char*value){

    int k;
    SUMMARY_ITEM *nsi;
    PC_Strip_Trailing(label);
    PC_Strip_Trailing(value);
    PC_Strip_Leading(label);
    PC_Strip_Leading(value);
    if(rxIsEmpty(label) || rxIsEmpty(value)) {
        rxerror("posted all spaces !",1);
    }
    if(SUMDB)
        printf("post %s %s \n",  label,value);
    for(k=0;k<this->N_Items;k++) {
        if(strieq(label,this->list[k].header)) {
            this->list[k].PCS_Evaluate_Summary_Data(value);//,FROM_FILE);
            this->list[k].in_memory = 0;
            return(1);
        }
    }
    /* a new item to add */
    nsi = this->add_new_item(label,value);//,FROM_FILE);
    nsi->in_memory = 0;
    return(1);
}


int RXLogFile::PCS_Strip_Trailing(char *s){// does commas too
    /* repeatedly, if the last character is a " ", strip it */
    char *p;

    if(!s)
        return 0;
    p = s;

    while(*p)
        p++;
    p--;
    while( (p >= s) &&

           ( isspace(*p) || ((*p) == 13) || ((*p) == 44) || ((*p) == 12)) ) {
        *p = '\0';
        p--;
    }
    return 1;
}
