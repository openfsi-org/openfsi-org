

#ifndef _FILAMENT_H_
#define  _FILAMENT_H_

#include "vectors.h"
#include "griddefs.h"

class RXCrossing2;
class RXTriangle;
#include "RXTriangle.h"

struct PC_FILAMENT_CROSS {
   	double s1,m_t;	  /* arc lengths of curve1 , each end */
   	int i1,flag;
	int m_j;		// index (0 to 2) of the edge no on the tri. 
	VECTOR v2;		// point on tri edge.  the pos on the edge is m_t
	};
//ONLY ever use as a local.  It refers to some automatic variables.
class FILAMENT_LOCAL_DATA {

public:
	FILAMENT_LOCAL_DATA ();
	ON_3dVector Create_Material_Axes(const RXCrossing2 & p_l1,const RXCrossing2 & p_l2);// really should be a method of RXTriang

	class RX_FETri3 *m_FE_Tri; // need to get rid of this member, its dangerous
	class RXTriangle  m_rxt; // local
	const RXSitePt *v[4];// the last is the same as the first
 
	ON_Xform m_MatRefTrigraph; 

	double *DD_Tot;
	double *ps;
//	sc_ptr m_fldsc;
};
//EXTERN_C double  PCF_Measure_Tri_Width(struct FILAMENT_LOCAL_DATA *fld);
 
//EXTERN_C int PCF_Create_Filament_Matrix(struct FILAMENT_LOCAL_DATA *fld,RXCrossing*l,int c);

//EXTERN_C int 	PCF_Create_Material_Axes(class FILAMENT_LOCAL_DATA *fld,const RXCrossing2 & l1,const RXCrossing2 & l2); 

EXTERN_C int PCF_Append(struct PC_FILAMENT_CROSS**List, int*c, double s1,int i1,int j,double t, VECTOR *v2,int flag);
EXTERN_C int PCF_Sort_Filament_Xlist(struct PC_FILAMENT_CROSS*l, int *c) ;
EXTERN_C int PCF_Intersection_Type(struct PC_FILAMENT_CROSS*l, int c,int *j);
//EXTERN_C int PCF_Draw_Triangle(class RX_FETri3 *t,const char*color);
EXTERN_C int PCF_Print_Filament_Xlist(class FILAMENT_LOCAL_DATA *fld,struct PC_FILAMENT_CROSS*l, int c) ;
EXTERN_C int PCF_DeleteFilamentList(FortranTriangle *ft);

EXTERN_C int PCF_PushToTriFilamentList(FortranTriangle *p_ft,const int j1,const double t1 ,
							const int j2,const double t2,
							RXEntity_p sce,const double theta,const double p_w,
							const double p_EA, const double prestess);

extern ON_Xform Get_DeflectedTriangleAxes(SAIL *p_sail, const FortranTriangle *ft );
extern  ON_3dPoint Get_Position_On_Edge(const SAIL *p_sail,FortranTriangle *te, int ie  ,double t);

EXTERN_C HC_KEY PCF_Draw_Colored_Filaments(const SAIL *p_sail);
EXTERN_C int PCF_PrintTriFilamentList(const FortranTriangle *ft, FILE *fp);
EXTERN_C int PCF_Compare_Xs( const void *va,const void *vb);


#define  PCF_DISREGARD  32768
#define PCF_SMALL  1E-5
#define PCF_ONE (1.0 - PCF_SMALL)
#define PCF_A	1
#define PCF_B	2
#define PCF_C	3
#define PCF_D	4
#define PCF_E	5
#define PCF_F	6
#define PCF_G	7
#define PCF_Inc(a)  (((a)+1) * ((a)<2) + ((a)-2) * ((a)>=2))

#endif

