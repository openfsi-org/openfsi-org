#pragma once
#include "RX_FELinearObject.h"
 
#include "RXEntity.h"
class RXOffset; 
#include "vectors.h"


class RX_FEedge;




class RX_FEPocket :
	public RX_FELinearObject,public RXEntity
{
public:
	RX_FEPocket(void);
	RX_FEPocket(SAIL *p_sail);
	~RX_FEPocket(void);

		int CClear();
		virtual int Dump(FILE *fp) const;
		virtual int Compute(void);
		virtual int Resolve(void);
		virtual int ReWriteLine(void);
		virtual int Finish(void);

public:
	RXEntity_p basecurveptr;
	RXEntity_p e[2]; /* the SITES or relsites at the ends. */
	int inserted;
	double m_arc;      /* current arclength */
// from struct PC_BATPATCH  
protected:
	VECTOR *m_p;  // malloced 
	int c;
//
	float d;            /* depth.  May be zero */
//	int type;  MUST be PATCH

	int N_Angles;
	struct IMPOSED_ANGLE *a;
	int partial;
	RXEntity_p end1ptr;	
	int end1sign;
	RXEntity_p end2ptr;  /* ptr to RXEntity of end2 {SITE|seam|curve} */
	int end2sign;

	double strt_A;   /* an estimate of the initial slope of the IA curve */
	double end1angle; /* ready for export */
	double end2angle;

  	RXEntity_p mat;
	RXEntity_p corner;
	class RXCompoundCurve *cc;
// end from } struct PC_BATPATCH

private:
	double m_Trim; // imposed length change on the batten in this pocket  
	RXOffset *m_pEnd1dist;
	RXOffset *m_pEnd2dist;

protected:
	int Init(void);
	double m_ZiLessTrim;
public:
	void SetTrim( const double x);
	double GetTrim( void) const { return m_Trim; }
	double* GetTrimPtr(){ return &(m_Trim ) ;}

public:
	/// generate the psides
	int Mesh(void);
	// put the string into the FEA database
	int AddFEA(void);
	// removes the string from the FEA database
	int ClearFEA(void);
	// collects the pside list and the attributes into the P structure
	int PostMesh(void);
	 int Compute_Pocket_Geometry();
	 int Compute_Pocket();


public:
	// // really post-mesh.  Pockets are assumed to share edges and nods with the membrane
	static int MeshAll(SAIL *sail);
	int Find_All_CrossesForPocket(struct PC_INTERSECTION**is,int *ni);
	int Create_Bat_BasecurveForPocket(int k,struct PC_INTERSECTION*a,int ni);
	int Create_Bat_CurveForPocket(RXEntity_p site1,RXEntity_p site2,const int k);

	int Insert_Pocket();


};
