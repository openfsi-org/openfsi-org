/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *           3/march/95  name is now second word. WAS third word in error
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


#include "StdAfx.h"
#include "RXEntity.h"
#include "elements.h"

#include "stringutils.h"
#include "global_declarations.h"

#include "ReadStripe.h"


#define UV_STEP 4

int Resolve_Stripe_Card(RXEntity_p e)
{ 
  
  /* Stripe card is of type
     0    1           2                3     4
     stripes:name:type(u || v):u1:u2(:u3.....):colour=,weight=:steps   ! comment 
     
     */
  
  
  const char *name; char  attrib[200],buf[120];
  char *words[200],*lp,**wd;
  StripeData *bd;
  struct LINKLIST *plist=NULL;
  int i,nsites,j;
  float aval;
  char line[512];
  int retVal = 0; /* failure */
  
  strcpy(line,e->GetLine());
  wd=words;
  
  strrep(line,'!','\0'); 
  lp=strtok(line,RXENTITYSEPS);
  name = e->name();
  i=0;
  while((lp=strtok(NULL,RXENTITYSEPS))!=NULL) {
    words[i]=lp;
    PC_Strip_Leading(words[i]);
    PC_Strip_Trailing(words[i]);
    i++;
  }
  if(i < 4) 
    goto End;
  
 /*  name = *wd; */
  
  wd++;
  
  if(strstr(*wd,"u")) {
    sprintf(buf,"Adding 'u' stripes : %s",name);
    e->OutputToClient(buf,0);
    nsites = i-4;
    if(nsites < 1) 
      goto End;
    
    bd = (StripeData *) CALLOC(1,sizeof(StripeData));
    if(!bd) 
      goto End;
    
    bd->name = STRDUP(name);
    bd->val = (double *) CALLOC(nsites+1,sizeof(double));
    
    bd->type = STRDUP((*wd));
    
    for(j=0;j<nsites;j++)  {
      wd++;
      aval = atof((*wd));
      sprintf(buf,"Stripe at %6.3f",aval);
      e->OutputToClient(buf,0);
      if(strstr((*wd),"%")) 
	aval = aval/100;
      
      if(aval < 0 ) {
	e->OutputToClient("Stripe at -ve % is not allowed ",2);
	continue;
      }
      if(aval > 1 ) {
	e->OutputToClient("Stripe at > 100 % is not allowed ",2);
	continue;
      }
      bd->val[j] = aval;
      bd->n++;
    }
  }
  else if(strstr(*wd,"v")) {
    sprintf(buf,"Adding 'v' stripes : %s",name);
        e->OutputToClient(buf,0);
    
    nsites = i-4;
    if(nsites < 1) 
      goto End;
    
    bd = (StripeData *) CALLOC(1,sizeof(StripeData));
    if(!bd) 
      goto End;
    
    bd->name = STRDUP(name);
    bd->val = (double *) CALLOC(nsites+1,sizeof(double));
    
    bd->type = STRDUP((*wd));
    
    
    for(j=0;j<nsites;j++)  {
      wd++;
      aval = atof((*wd));
      if (g_ArgDebug) printf("Stripe at %6.3f\n",aval);
      
      if(strstr((*wd),"%")) 
	aval = aval/100;
      
      if(aval < 0 ) {
	e->OutputToClient("Stripe at -ve % is not allowed ",2);
	continue;
      }
      if(aval > 1 ) {
	e->OutputToClient("Stripe at > 100 % is not allowed ",2);
	continue;
      }
      bd->val[j] = aval;
      bd->n++;
    }
  }
  else if(strstr(*wd,"between")) {
    sprintf(buf,"Adding 'between' stripe : %s",name);
    e->OutputToClient(buf,0);
    
    nsites = i-4;
    if(nsites < 1) 
      goto End;
    
    bd = (StripeData *) CALLOC(1,sizeof(StripeData));
    if(!bd) 
      goto End;
    
    bd->name = STRDUP(name);
    bd->val = (double *) CALLOC(nsites+1,sizeof(double));
    
    bd->type = STRDUP((*wd));
    
    
    for(j=0;j<nsites;j++)  {
      wd++;
      aval = atof((*wd));
      if(g_ArgDebug)printf("Stripe at %6.3f\n",aval);
      
      if(strstr((*wd),"%")) 
	aval = aval/100;
      
      if(aval < 0 ) {
	e->OutputToClient("Stripe at -ve % is not allowed ",2);
	continue;
      }
      if(aval > 1 ) {
	e->OutputToClient("Stripe at > 100 % is not allowed ",2);
	continue;
      }
      bd->val[j] = aval;
      bd->n++;
    }
    if(bd->n != 4) {
      sprintf(buf,"Incorrect 'between' strip definition\n'%s'",name);
      e->OutputToClient(buf,2);
      SAIL *sail = e->Esail;
      e->Kill();
      goto End;
    }
    
    bd->u1 = bd->val[0];
    bd->v1 = bd->val[1];
    bd->u2 = bd->val[2];
    bd->v2 = bd->val[3];
    
  }
  else {
      sprintf(buf,"Unknown stripe type '%s' \n'%s'",*wd,name);
      e->OutputToClient(buf,2);
      SAIL *sail = e->Esail;
      e->Kill();
      goto End;
    }

  wd++;
  bd->color = STRDUP(*wd);wd++;
  if(g_ArgDebug)printf("color = '%s'\n",bd->color);
  bd->step = atoi(*wd);
  if(bd->step < 1)
    bd->step = UV_STEP;
  if(bd->step > 100)
    bd->step = 100;
  
  bd->FULLY_DEFINED = 1;
  strcpy(attrib,bd->color);

  e->PC_Finish_Entity("stripe",name,(void *)bd,0,plist,attrib,e->GetLine());
 
   retVal = 1;	/* got here so OK ! */
  
 End:
  if(retVal)  {
    e->Needs_Resolving= 0;
    e->Needs_Finishing = 0;
    e->SetNeedsComputing(0);
  }
  return(retVal);
}


