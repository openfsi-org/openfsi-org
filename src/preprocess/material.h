/* material.h , prototypes for material.c machine-generates 17/11/94 PH

11/3/97 added the griddefs include for HP 

 */ 
#ifndef _HDR_MATERIAL_
#define _HDR_MATERIAL_ 1

EXTERN_C int Compute_Material_Card(RXEntity_p e);  
EXTERN_C void Prepare_NL_Mat(double *ex,double *fx,double *fy,double *f45,int *N,double *ref,
                             double *nu,double *zk,double*SW,double *en,double *fn,double *es,double *fs,int*err);
EXTERN_C int Resolve_Material_Card(RXEntity_p e);
EXTERN_C int PCM_Make_Linear_D(double d[3][3],double ex,double ey,double e45,double nuxy);
EXTERN_C int Draw_Cube(float X1,float Y1,float Z1,float x2,float y2,float z2);
EXTERN_C int Mat_MSW_Disp(float emin,float emax,float smin,float smax, RXEntity_p e);
#endif  //#ifndef _HDR_MATERIAL_



