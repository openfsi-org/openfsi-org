#include "StdAfx.h"
#include "RXException.h"
#ifdef AMGTAPER
	#include "stringtools.h"
#endif
RXException::RXException(void)
: m_i(0)
{
	m_s=std::string("void");
	m_type=0;
}
RXException::RXException(int a)
{
	m_i=a;
	m_s=std::string("int");
	m_type=1;
}
RXException::RXException(char *a)
{
	m_i=0;
	m_s=ON_String(a);
	m_type=2;
}
RXException::RXException(std::string a)
{
	m_i=0;
	m_s=a;
	m_type=3;
}

RXException::~RXException(void)
{
}
int RXException::Print(FILE *fp){
	int rc = fputs("Exception Handler:",fp);
	rc+=fprintf(fp,"  i = %d\n type = %d\nmessage = %s\n",m_i,m_type,m_s.c_str());
	return rc;
}

void _rxthrow (int a){
	throw(RXException(a));
}
void _rxthrow (char *a){
	throw(RXException(a));
}
void _rxthrow (std::string a){
	throw(RXException(a));
}

void _rxthrow (std::wstring a){
	throw(RXException(ToUtf8(a)));
}

