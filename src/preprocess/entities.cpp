
/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 Aug 2 2004  the destructor for a Pressure entity.
 Aug 1 2004 the script reader sensitive to '$table'
 It preserves the file separators except for stripping trailing whitespace between keyword '$table'
 and the end of the record, defined as usual by an entity type keyword at the beginning of a line.

 Dec 2003 	Do we rename alias entities "	 "%s(%s)$alias",name,spec Otherwise GetAliassed Entity is wrong.
 Speed.  Get_Aliased uses type not TYPE. We've put in an assert
 Entity_Exists uses findnode only - good

*/

/* thats because there are various DBG lines triggered by VeJoP
 *
   * Modified :
    2 feb 2003  UnR-solve now doesnt Finish Entity.  It stuffed up strings because Clear-Entity rewrites the line.
 Another method would be to note the original line AFTER clearing not before
   5 oct 02 makeValidSegmane doesnt accept parentheses

25 3 01  warn of long atts in re w r ite script
Oct 98     write_script writes all edited entities and Just_Read_Card gives precedence to duplicate entities of least depth.
 This enables edits to entities in include files to be preserved by moving them up to the script.
 Also, as a tidiness, in write_script we dont write any 'include' entities if they have no dependents. This should be tested before
 shipping.

Jan 98 	Remove_Pside_From_Node_Lists now calls SortOneNode to keep el and er uptodate
Nov 97    Insert Entity and Finish Entity do a make valid segname. Also Finish Entity sets HK if it was zero
 I dont know if that will have side-effects; It should be all for the good
 Nov 97  JustReadCard sets needs gaussing on the owning sail. Hopefully this will kick off
 correct response to UpdateSliding.
  Nov 97  The include card takes the LAST line as the filename, not the second. Needs testing

 Aug 97 rm to rm -f
 May 1997.
  Finish Entity creates a segment (type/name) and in it, sets the User Value
  to the entity pointer
  MAYBE DANGEROUS


  The mode below REMOVED. It stuffs up the mainsail tack via the curve connect
 also it stuffs up things like SETS that may have funny names
  22/4/97  ALL entities have their names made valid via MakeValidSegname in JustReaCard
 A Functional change. We permit '/' in names. because the name might be a filename
 We Dont allow the '%' because hoops doesnt. That was a bug

 4/97  Get First Words called strtok erroneously if line was blank or NULL.
 This gave random corruption in the script reader
 4/97 Push_Entity returns 0 if the entity was already on the specified stack
12/3/97	 the .gen file now contains the boat name. It should be complete for building
 It will be slow because ALL the materials will be displayed
7/3/97 	PC_Read_Line now breaks on a semi-colon and on EOL
 This is nice for macro files but probably WRONG for scanFile
 1/97 Finish Entity and Insert_Entity now strip trailing from type and name
 11/96  Include is now PC_Inserted before the entities in its file
 so that they can be made dependent on it via the global
 g_CurrentIncludeFile
   10/10/96 PC_Set Separators doesnt strip out commas. What are the implications??
  *  1/10/96  Read_Macro accepts multi-line cards.  It continues reading
 until it finds a line whose first word isnt a known keyword
    A file is considered to be a sequence of words separated by tabs and EOLs

    A CARD is a sequence of records, of which the first word	is a recognised keyword which
    follows a BOF or a EOL.




 * 14.6.96  Un Re solve added (moved from drop.c )
    and now executes Clear-Entity/Finish_E/ Needs_Res=1

  * 25/5/96 edit lines may be multi-line
   Get_First Word traps words longer than 255 chars.
   NOTE this requires buffers of 256 chars. Take care when coding

    terminated by an EOR line or at the end of the line in which the brackets balance.

 *   28/4/96 rewrite line enables for some entities. This is meant to facilitate
 *	  form-found designs by merging any edits


 * 3/3/96  all RXFREEs followed by setting to NULL, for safety.
 *     20/2/96 COmpute Compund Curve. Psides added in reverse if revflag set
 *		5/12/95   sc->basecurve changed to BorGcurve to ensure consistent re-build
 *      10/7/95 etypes added to speed up entity testing
 *      27/4/95  write script deals with materials and interpolation surfaces
 *     12/3/95   Delete deals with displays
 *    BUG 12/3/95 Peter. In PC_Finish_Entity  name and type may be pointers to inside line
 This was first notes in call from Compute_Stripe
 *    16/1/95    validfile is in akmutil.c
 *   12/1/95     write script has special treatment for material card.
 *  2/1/94   aunts/nieces re-jigged
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 *      2.12.94                 AKM             always reads a scratch copy whichhas ctrlM removed
 */


/* this is entities.c 22/2/94 */
/* 4/10/94 PC read line would be happier if it could MALLOC the buffer. But does Adam use it? */

#include "StdAfx.h"
#include <QStringList>
#include <QTemporaryFile>
#include <QDebug>
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RX_FEString.h"
#include "RXSRelSite.h"
#include "RX_FEPocket.h"
#include "RXSeamcurve.h"
#include "RXPside.h"
#include "RXPanel.h"
#include "RXPolyline.h"
#include "RelaxWorld.h"
#include "files.h"
#include "griddefs.h"
#ifdef linux
#include "../../stringtools/stringtools.h"
#else
#include "..\stringtools\stringtools.h"
#endif
#include "rxsubwindowhelper.h"
#include "traceback.h"

#include "stringutils.h"
#include "RXRelationDefs.h"

#include "hoopcall.h"

#include "akmutil.h"
#include "boatgen.h"

#include "polyline.h"
#include "fielddef.h"
#include "printall.h"
#include "panel.h"
#include "batpatch.h"

#include "alias.h"
#include "script.h"

#include "entutils.h"
#include "question.h"  /* for Replace_Word */
#include "interpln.h"
#include "dxf.h"

#ifdef _NOT_MSW
#define _max max
#define _min min
#endif

#include "uselect.h"
#include "vectors.h"
#include "delete.h"
#include "etypes.h"
#include "gcurve.h"
#include "camera.h"
#include "RXExpression.h"
#include "arclngth.h"

#include "iangles.h"
#include "readstring.h"
#include "connect_f.h"
#include "gle.h"
#include "iges.h"
#include "RXdovefile.h"

#include  "pressure.h"
#include "RXCurve.h"
#include "global_declarations.h"
#include "RX_UI_types.h"
#include "RXContactSurface.h"


#include "entities.h"
#ifndef strieq
#define  strieq(a,b) (_stricmp(a,b)==0)
#endif

RXEntity_p g_CurrentIncludeFile;
struct LINKLIST *g_parList=NULL;



#ifdef WIN32
#define _max max
#define _min min
#endif 

#ifdef HOOPS
int PC_UnHighlight_All_Entities(void){ // DANGEROUS IF IN ROOT
    HC_KEY key; char type[256];

    HC_Begin_Contents_Search("(.,*,...)","style");

    while (HC_Find_Contents(type,&key))
        HC_Delete_By_Key(key);
    HC_End_Contents_Search();
    HC_QUnSet_Rendering_Options("(*,...)");

    if(HC_QShow_Existence("highlight/*","self"))
        HC_Delete_Segment("highlight/*");

    return  1;
}
#endif
int PC_Highlight_Entity(RXEntity_p e){

#ifdef HOOPS
    // in MSW typically /driver/opengl/window0/scene/data
    //return 0;
    HC_Open_Segment("Highlight");
    HC_Set_Color("yellow");
    HC_Set_Line_Weight(2.0);
    HC_Set_Rendering_Options("attribute lock = (color,line pattern,line weight)");


    switch(e->TYPE) {
    case PCE_PSIDE :{
        break;
    }
    case SEAMCURVE :
    case COMPOUND_CURVE:
    case SITE:
    case RELSITE :
    case TWODCURVE  :
    case BASECURVE :
    case THREEDCURVE  :
    case PCE_SPLINE :
    case PATCH :
    case POCKET :
    case BATTEN  :
    case FABRIC :
    case PANEL  :

    {
     char buf[256];
        if (e->hoopskey) {
            //HC_Move_By_Key(e->hoopskey,".");

            //printf(" Highlight '%s'  to %s\n",e->name(),buf);
            HC_Show_Key_Type(e->hoopskey,buf);
            if(strieq(buf,"segment") ) {
                HC_Show_Pathname_Expansion(".",buf);
                HC_Open_Segment_By_Key(e->hoopskey);
                HC_Flush_Contents(".","style");
                HC_Style_Segment(buf);
                HC_Set_Rendering_Options("attribute lock =(color,line pattern,line weight)");
                HC_Close_Segment();
            }
        }
    }

    default:
        break;
    }
    HC_Close_Segment(); //highlight
    HC_Update_Display();
#endif
    return 0;
}


RXEntity_p PC_Get_Key_Across_Models(const char*type,const char*name) {
    int i;
    RXEntity_p k =0;

    vector<RXSail*> lsl = g_World->ListModels(RXSAIL_ALLHOISTED); // boat too
    for(i=0;i<lsl.size();i++)  {
        SAIL *theSail = lsl[i];
        k = theSail->GetKeyWithAlias(type,name);
        if(k)
            return(k);
    }
    return(NULL);
}
class RXEntity* QPC_Get_Key(const string sailname,const string spin,const string name)//   ONLY picks up entities in Hoisted sails 
{
    return QPC_Get_Key(sailname.c_str(),spin.c_str(),name.c_str());
}
class RXEntity *QPC_Get_Key(const char *sailname,const char *spin,const char *name) 
{
    //   ONLY picks up entities in Hoisted sails

    class RXEntity *ent=0;

    if(sailname) {
        SAIL *s = g_World->FindModel(sailname);
        if(s)
            ent = s->GetKeyWithAlias(spin,name);
    }
    return(ent);
} //QPC_Get_Key




/* deleting d-pendents non-recursively is wierd.
we use it specifically for clearing the field of a dove without wiping out the panels too.
This is a dangerous speed optimisation.
It would be better to delete deps too and rebuild.
*/


void Free_Entity_List(struct LINKLIST **ptr) {
    /* ptr points to the beginning of a list.
     this routine deletes the chain beyond *ptr  without deleting the
     objects pointed to in the chain*/

    if ( (*ptr)==NULL ) {
        return;
    }
    if ((*ptr)->next!=NULL) {
        Free_Entity_List(&((*ptr)->next));
    }

    RXFREE(*ptr);
    *ptr=NULL;
}

struct LINKLIST *Check_Ptr_List(struct LINKLIST *plist,RXEntity_p target)
{
    struct LINKLIST *p;
    p=plist;

    while(p) {

        if(p->data == target)
            return(p); /* May 98  used to return the ENT,  */
        p=p->next;
    }
    return(NULL);
}
int Push_Pointer(void**ptr, struct LINKLIST**bottomptr) {
    struct LINKLIST *a;
    a = (struct LINKLIST *)MALLOC(sizeof(struct LINKLIST));

    a->data  = (void*) *ptr;
    a->next = *bottomptr;
    *bottomptr = a;
    return(1);
}
void* Pop_Pointer(void**ptr, struct LINKLIST**bottomptr) {  
    struct LINKLIST *a;
    if (!bottomptr)  return NULL;
    if (!(*bottomptr)) { return NULL;   }
    a =  (*bottomptr)->next;
    *ptr = (*bottomptr)->data;
    RXFREE(*bottomptr);
    *bottomptr = a;
    return *ptr;
}
void* Peek_Pointer(void**ptr, struct LINKLIST**bottomptr) {  

    if (!bottomptr)  return NULL;
    if (!(*bottomptr)) { return NULL;   }

    return ( (*bottomptr)->data);
}
int Print_Pointer_LinkList(FILE *fp, const char*s, struct LINKLIST**bottomptr) {
    struct LINKLIST *a;
    RXEntity_p e;
    fprintf(fp, " Pointer_LinkList %s\n", s);
    a = *bottomptr;
    while(a) {
        e =  (RXEntity_p )a->data ;
        if(e)
            fprintf(fp," List %s %s\n", e->type(),e->name());
        a=a->next;
    }
    fprintf(fp, "\n");
    return 1;
} 

int Push_Entity(RXEntity_p S_in,struct LINKLIST **bottomptr) {
    /* push a record on the bottom of the stack (First out Last in)
     BUT ONLY IF ITS NOT THERE ALREADY
   May 98, If its there, increment the count flag */

    struct LINKLIST *a;

    if(!S_in )  return(0);
    if(*bottomptr) {
        //	assert(S_in->prents != (*bottomptr));
        //	assert(S_in->dpendents != (*bottomptr)) ;
        //	assert(S_in->aunts != (*bottomptr)) ;
        //	assert(S_in->NIECES != (*bottomptr)) ;
    }
    if((a = Check_Ptr_List(*bottomptr,S_in))) {

        return(0);
    }

    a = ( struct LINKLIST*)MALLOC(sizeof(struct LINKLIST));
    if (!a)
        S_in->OutputToClient("out of memory",3);

    a->data  = S_in;

    a->next = *bottomptr;
    // a->count=1;
    *bottomptr = a;
    return(1);
}

int Pop_Entity(RXEntity_p *ptr,struct LINKLIST **bottomptr) {
    /* removes the entry at the bottom of the stack. */
    struct LINKLIST *a;

    if (!bottomptr)  return 0;
    if (!(*bottomptr))  return 0;
    a =  (*bottomptr)->next;
    *ptr = (RXEntity_p )(*bottomptr)->data;
    RXFREE(*bottomptr);
    *bottomptr = a;
    return 1;
}


void Print_Entity_List(struct LINKLIST *p,FILE *fp){

    RXEntity_p ep;
    if (p==NULL) return;

    ep = (RXEntity_p )p->data;
    if(fp) {
        if(ep)
            fprintf(fp,"         %s    %s   \n",ep->type(),ep->name());
        else
            fprintf(fp," NULL\n");
    }
    else {
        if(ep)
            printf("          T=        <%s> N= <%s> \n",ep->type(),ep->name());
        else
            cout<< " NULL\n"<<endl;
    }
    if (p->next==NULL) return;
    Print_Entity_List(p->next,fp);
}

RXEntity_p Just_Read_Card(SAIL * const p_sail, 
                          const char *linein,const char *typein,const char*namein,
                          int *depth,const bool GuaranteeUnique){
    /* makes an entity from a line
 owners is a null-terminated array of entities */
    RXEntity_p e, olde;
    char *Firstword, *name;

    Firstword = strtolower(STRDUP(typein));
    name = strtolower(STRDUP(namein));
    char *line = STRDUP(linein); // dec 2008 this stuffs up linux filenames

    PC_Strip_Leading(Firstword ); PC_Strip_Trailing(Firstword );
    PC_Strip_Leading(name); 	    PC_Strip_Trailing(name);

    /* Check for another with the same name  Lowest depth wins */
    if(!GuaranteeUnique) { // a speed optimisation
        if((olde = p_sail->GetKeyWithAlias(Firstword,name))) {
            if(olde->generated > (*depth)) { printf(" Delete older %s %s because of depth\n", olde->type(),olde->name());
                olde->Kill();  }
            if(olde->generated < (*depth)) {
                printf(" SKIP %s %s because of depth\n", olde->type(),olde->name());
                return(NULL);
            }
        }
    }
    /* check for any other  with identical data apart from name and type Especially Mikke's VeCo/VeJo */
    /*  THIS WORKS, but it catches too much. For instance all the fixity cards
It should be moved to a 'check model completeness' or somewhere. In that case it should generate
a delete entity and a rename entity. 
*/
    e=p_sail->Insert_Entity(Firstword,name,NULL,(HC_KEY)0,NULL,line,GuaranteeUnique);
    RXFREE(Firstword); RXFREE(name); RXFREE(line);
    if(!e )
        return(NULL);
    e->SetNeedsComputing(0);	 /* set to 1 in Finish_Entity */
    e->Needs_Resolving=1;
    e->generated=*depth;

    if( g_CurrentIncludeFile) {
        g_CurrentIncludeFile->SetRelationOf(e,child|spawn,RXO_ENT_OF_INCFILE,RX_AUTOINCREMENT);
    }
    return( e);
} 
QString  Make_Valid_Segname(QString name)
{
    int k = name.length();
    char *lp = (char*)MALLOC(k +2);
    strcpy(lp,qPrintable(name) );
    lp[k+1]=0;
    Make_Valid_Segname(lp);
    name=QString(lp);
    RXFREE(lp);
    return name;
}

ON_String  Make_Valid_Segname(ON_String name){
    int k = name.Length();
    char *lp = (char*)MALLOC(k +2);
    strcpy(lp, name.Array());
    lp[k+1]=0;
    Make_Valid_Segname(lp);
    name=ON_String(lp);
    RXFREE(lp);
    return name;
}
int Make_Valid_Segname(char*name) { /* returns 1 if it made a change */ 
    char s[6];
    int iret=0;
    size_t i,n=strlen(name);
    int changed=0;
    /* should permit () */
    const char *OKs ="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz #+-%_./()"; //7/6/00 removed ()
    if(rxIsEmpty(name)) return(0);
    s[1]= 0;
    for(i=0;i<n;i++) {
        s[0]=name[i];
        if((strstr(OKs,s))==NULL){
            name[i]='_';
            changed=1;
        }
    }
    while(*name=='/'){
        *name=' ';
        PC_Strip_Leading(name);
        changed=1;
    }
    if(changed)
        iret=1;
    return(iret);
}

int  Make_Valid_Segname(RXSTRING &nameW){
    std::string name = ToUtf8(nameW);
    int k = name.length();
    char *lp = (char*)MALLOC(k +2);
    strcpy(lp, name.c_str());
    lp[k+1]=0;
    k= Make_Valid_Segname(lp);
    nameW=TOSTRING(lp);
    RXFREE(lp);
    return k;
}

int Get_First_Words(const char*line,char*Firstword,char*second) {	/* assumes they are both MALLOCed already */
    int iret = 1;
    char*fw, *sw;
    char*s1;
    char separators[10] = ":,";
    assert(!cout.bad());
    if( !line || !(*line)) {   strcpy(Firstword," ");  strcpy(second," ");   return 1;}
    QString swork;
    s1=STRDUP(line);
    //Grandfather
    // we want to discourage the use of ':' as delimiter because it is contained in Windows filenames.
    //    BUT to be friendly, we want to let the first delimiter be a ':' or a RXSCRIPTSEPS
    //     subsequent separators must be  RXSCRIPTSEPS
    fw=strtok(s1,separators);
    sw=NULL;

    if(fw){
        sw=strtok(NULL,separators); /* moved inside for insulation */
        if(strlen(fw) > 255){
            printf(" Long firstword '%s'\n",fw);
            printf("  in line '%s'\n",line);
            g_World->OutputToClient(" truncating First Word ",2);
            iret =0;
            fw[254]='\000';
        }
        strncpy(Firstword,fw,254);
        PC_Strip_Trailing(Firstword);
    }
    else   strcpy(Firstword," ");

    if(sw){
        if(strlen(sw) > 255){
            printf(" Long second word '%s'\n",sw);
            printf("  in line '%s'\n",line);
            g_World->OutputToClient(" truncating second Word ",2);
            iret =0;
            sw[254]='\000';
        }
        strncpy(second,sw,254);
        PC_Strip_Trailing(second);
        PC_Strip_Leading(second);
    }
    else   strcpy(second," ");

    RXFREE(s1);
    assert(!cout.bad());
    return(iret);
}
#ifdef NEVER

int PC_Set_Separators(char*line) { 	/*  convert ALL tabs to colons */
    char*p = line;
    int cnt=MAXLINELENGTH;
    while(*p) {
        /*   if(*p==',' || *p == '\t') */
        if(*p == '\t')
            *p=':';
        p++;
        cnt--;
    }
    if(cnt < 1)
        g_World->OutputToClient("suspicious string in PC-Set-Separators",2);

    return(1);
}

/****************************************************************/
int Just_PC_Read_Line(FILE *cycleptr,char *line, char *Firstword,char *name,int N) {
    /* line is the entire line  up to a shriek
     Firstword consists of those characters up to the first ": or comma" */

    /* max line length. is N chars */
#ifdef _NOT_MSW
    char *string = (char*) MALLOC(N*sizeof(char));
#else
    /* char string[MAXLINELENGTH];   */
    char *string = (char*) MALLOC(N*sizeof(char));
#endif
    size_t length;
    int retVal = 0;
    char *lp;
    if(!string) g_World->OutputToClient("memory!!!!!!!!!!",3);
    strcpy(Firstword," ");
    strcpy(line," ");

    if  (fgets(string,N-1,cycleptr)) {

        /*# ifndef _NOT_MSW	 */
        length = strlen(string);
        if(length >= MAXLINELENGTH-2) g_World->OutputToClient("line too long",3);
        /* # endif	*/

        if((lp = strchr(string,'!')))
            *lp = '\n';
        if((lp = strchr(string,'\r')))
            *lp = '\n';

        PC_Strip_Leading(string);
        PC_Strip_Trailing(string);
        if(*string) {  /* not blank line */
            strcpy(line,string);
            if (strlen(line) >  (N-2)*sizeof(char) ) {
                g_World->OutputToClient(" Line may be truncated",1);
            }
            PC_Set_Separators(line);  /* tabs and commas to colons */
            if(!Get_First_Words(line,Firstword,name)) cout<< " that is in Just-PC_Read-Line\n"<<endl;
        }
        retVal = 1;
    }
#ifdef _NOT_MSW
    RXFREE(string);
#else
    RXFREE(string);
#endif
    return(retVal);
}
#endif
char *PC_fgets(char*string, int nmax, FILE *fp,const char*seps) {
    /* slightly different from fgets
it reads up to nmax chars into <string> and stops at the first char which is in seps
it terminates the string with a NULL
return values
 if it encounters EOF before reading anything, it returns NULL
if a read error occurs it returns NULL
else it returns a pointer to <string>
*/
    int n;
    //#ifdef WIN32
    int  k = 0;
    char *lp;
    lp  = string;
    *lp=0;
    do {
        n = fscanf(fp, "%c",lp);
        if(0==n) {
            *lp=0;
            return(NULL);}

        if (EOF==n){
            if(!k)
                return(NULL);
            else
                break;
        }
        if (strchr(seps,*lp))
            break;
        lp++; k++;

    }while (k < (nmax-1));
    *lp=0;
    /*#else
 char c;
    if(!fgets(string, nmax, fp)) return NULL;
 if(fscanf(fp,"%c",&c) && c !=10 && c!=13)
  ungetc(c,fp);

 n = strlen(string)-1;
 while (n>=0) {
  if(string[n]== 10  || string[n]== 13 )
   string[n]=0;
  else
   break;
  n = strlen(string)-1;
 }

 printf(" PC_fgets got <%s>\n", string);
#endif*/

    return (string);
}

/*******************************************************/
/* read_Macro just reads the line and creates an entity type 'empty'
   It sets Needs_Resolving to 1 and generated to *depth.   and edited to 0
   Then Resolve_All_Entities looks at all entities with	Needs_Resolving.
   It returns the number that remain un-resolved.
   */
int PC_Grandfather(SAIL*p_sail, char *line,char *Firstword,char *second){

    if(strieq(Firstword,"string")) {
        RXEntity_p old = p_sail->GetKeyWithAlias("string",second);
        if(old) {
            PCS_Combine_String_Lines(old,line);
            return PCE_DELETED;
        }
        else
            PCS_Convert_String_Line(&line,Firstword,second);
    }

    // include: // old style: filename is last,// new style filename is 3rd
    if(strieq(Firstword,"include")) {
        QStringList qsl0, qsl1,qsl2;
        QString qline(line),newline,fname ;
        // 1  parse out any trailing comment
        qsl0 = qline.split("!");
        //2 get the words
        qsl1 = qsl0[0].split(RXENTITYSEPS);
        for (QStringList::iterator it = qsl1.begin(); it != qsl1.end(); ++it)
            *it = (*it).trimmed();

        if(qsl1.count() !=3) {
            fname = qsl1.last().trimmed();
            //  decide a name
            //             if(qsl1.count() >3) ename = qsl1[1] ;
            //             else ename = qsl1.last();

            qsl2<<Firstword<<second<<fname;
            if(qsl0.count()>1)
                qsl2<<  qsl0[1]   ;
            qsl2 <<"! grandfathered";
            newline = qsl2.join(RXENTITYSEPS );
            strcpy(line, qPrintable(newline) );
            //     strcpy(second, qPrintable(qsl2[1]) );
        }

    }
    return 0;
}
int RXSail::Read_Macro( FILE *cycleptr, int *depth){ // absolutely horrible logic
    RXEntity_p e=0;
    char  Firstword[256],second[256]; ;
    char*line;
    int vf, ig;
    struct BOATGEN bg ;
    line =(char*) MALLOC(MAXLINELENGTH*sizeof(char)); // our mallocs throw if they fail

    *line=0;
    Allocate_MultiLine_Data();
    while(Read_Multi_Line(cycleptr,line,Firstword,second)) {
        ig = PC_Grandfather(this,line,Firstword,second);
        if (strieq(Firstword,"do_boat")) {
            if(Process_Boat_Card(this,line,&bg)) {
                e=Just_Read_Card(this,line,Firstword,second,depth);	*line=0;
                if(e) e->Needs_Resolving=0;
            }
        }
        else if (strieq(Firstword,"include"))
        {
            QString qline(line); qline=qline.trimmed();
            QStringList qsl= rxParseLine(qline,RXENTITYSEPREGEXP );
            if(qsl.size()<2)  qsl<<"myinclude";
            if(qsl.size()<3)  qsl<<"(*.txt,*.in)";
            QString &fname =qsl[2] ;
            int nw =qsl.size();

            int SUCCESS=0;
            bool l_bEdited= false;
            fname = this->AbsolutePathName(fname);
            if((vf=VerifyFileName(fname,second,Firstword))) { /* exists for reading */
                SUCCESS=1;
                if(vf==2) {
                    fname = this->RelativeFileName(fname);
                    l_bEdited =1;
                }
            }
            fname =  this->RelativeFileName(fname);
            qline = qsl.join(RXENTITYSEPSTOWRITE );
            strncpy(line,qPrintable(qline),MAXLINELENGTH);
            if(SUCCESS) {
                e=Just_Read_Card(this,line,Firstword,second,depth); *line=0; /* makes an entity from a line */
                if(e) {
                    e->edited=l_bEdited;
                    if(l_bEdited && e->Esail)
                        e->Esail->SetFlag(FL_NEEDS_SAVING);
                    e->Needs_Resolving=0;
                    g_CurrentIncludeFile = e; Push_Entity( e, &g_parList);
                    (*depth)++;
                    this->startmacro(qPrintable(fname),depth);
                    (*depth)--;
                    if(!Pop_Entity( (&g_CurrentIncludeFile), &g_parList))
                        g_CurrentIncludeFile = NULL;
                    if(g_parList) g_CurrentIncludeFile = (RXEntity_p ) g_parList->data;
                    else  g_CurrentIncludeFile = NULL;
                }
                else cout<< " Read of include eline FAILED\n"<<endl;
            }
        }   /* end of (if include) */

        else if(strieq(Firstword,"name")) {
            this->Deserialize(line);
        }
        else if (rxIsEmpty(line)) {/* ignore blank line */}
        else if (ig == PCE_DELETED) { /*  ignore this entity */ }

        else if (strieq(Firstword,"EOF")) break;

        else if(0==INTEGER_TYPE(Firstword))	  { /* keep going. Try to append it*/}

        else {Just_Read_Card(this,line,Firstword,second,depth); /* makes an entity from a line - UNLESS one exists with less depth */
            *line=0;
        }
    }	  /* end of while */
    RXFREE(line);
    Free_MultiLine_Data();
    return(0);
}



char *WORDML = NULL, *NEXTSEP=NULL, *LASTSEP = NULL;
struct LINKLIST *WLIST=NULL, *NLIST=NULL, *LLIST=NULL;


int Allocate_MultiLine_Data(void){

    /* allocate new values and initialise
  push the 3ptrs onto a stack   	  */

    Push_Pointer((void**) &WORDML, &WLIST);
    Push_Pointer((void**)&NEXTSEP, &NLIST);
    Push_Pointer((void**)&LASTSEP, &LLIST);
    /*	if(WORDML) printf(" PUSH WORDML<%s>\n",WORDML);
 if(NEXTSEP) printf(" PUSH NEXTSEP<%s>\n",NEXTSEP); */
    WORDML =(char*)CALLOC(255,sizeof(char));
    NEXTSEP=(char*)CALLOC(255,sizeof(char));
    LASTSEP=(char*)CALLOC(255,sizeof(char));


    strcpy(LASTSEP,"\n");/* not necessary. THe line read always sets it to zero */
    return 1;
}	
int Free_MultiLine_Data(void){
    /* Free the 3 ptrs and then pop the previous ptrs off a stack */
    RXFREE(WORDML);
    RXFREE(NEXTSEP);
    RXFREE(LASTSEP);
    Pop_Pointer((void**)&WORDML,  &WLIST);
    Pop_Pointer((void**)&NEXTSEP, &NLIST);
    Pop_Pointer((void**)&LASTSEP, &LLIST);

    /*	if(WORDML)  printf(" POPPED WORDML <%s>\n",WORDML);
 if(NEXTSEP) printf(" POPPED NEXTSEP<%s>\n",NEXTSEP);  */

    return 1;
}

int Read_Next_Word(FILE *cycleptr,char**w,int *N, char*sep) {
    /* *w is a malloced space to put the word ,size (*N).
  sep is a string into which the next sep is placed
  We start reading into *w, skipping any comments
  We return when a valid separator is found
  We return the length of the word
  */
    int i=0 ,n=0,comment=0;
    char c;  char*lp;
    lp= *w;
    *lp= *sep= 0;
    do {
        i=fscanf(cycleptr,"%c",&c);
        if(EOF==i) {
            *(sep++) = 'E';
            *(sep--) = 0;
            return EOF;
        }
        if(c=='!')
            comment=1;
        else if(c=='\n')
            comment=0;
        if(!comment) {
            if(strchr("\n\t",c)) {	/* a separator */
                *(sep++) = c;
                *(sep--) = 0;
                return (n);
            }
            else {  /* append to the word */
                (*lp)=c; lp++; *lp=0; n++;
                if(n>= (*N-2) ) {
                    lp--; n--; *lp=0;
                    //		*N = (*N) *2;
                    //		*w=REALLOC (*w, (*N)*sizeof(char));
                }
            }
        }


    } while(1);
    //  return 1;
}
int PC_Read_As_Table(FILE *cycleptr,char *line){

    /* We have just read a word  '$table'
  So we read the file until the next keyword, maintaining the separators. IE don't convert to ':'
  tabs and eols are significant
 but even so, dont write blank trailing fields in the Table: */

    size_t l; int i=0 , l_one_line=1,nw=0,k ;
    long int count= 100000;
    int N=255;

    do {
        PC_Strip_Trailing(WORDML); PC_Strip_Leading(WORDML);
        if(0==strncasecmp(WORDML,"EOF",3) )
            goto End;
        if(strstr(LASTSEP,"\n") && (INTEGER_TYPE(WORDML))) { /* its the start of a line*/
            goto End;  // normal termination
        }

        if(strlen(line) > MAXLINELENGTH) {
            printf("word is '%s'\n",WORDML);
            g_World->OutputToClient(" line too long. The limit is 256000",4);
        }

        if(l_one_line==1 && stristr(NEXTSEP,"\t") ){ // fields in first line
            strcat(line,WORDML);
            strcat(line,NEXTSEP);
        }
        else if(l_one_line==1 && stristr(NEXTSEP,"\n")) {// End of first line
            strcat(line,WORDML);
            PC_Strip_Trailing_Chars(line," \n\t:");
            strcat(line,NEXTSEP);
            l_one_line++;
        }
        else if(l_one_line>1 && stristr(NEXTSEP,"\n")){ /* End of successive line*/
            strcat(line,WORDML);
            PC_Strip_Trailing(line);/* strip. SO dont care */
            strcat(line,"\n");
        }
        else if(l_one_line>1 && stristr(NEXTSEP,"\t") && !rxIsEmpty(WORDML)) {/* fields in first line*/
            strcat(line,WORDML);
            strcat(line,"\t");
        }
        nw++;

        strcpy(LASTSEP,NEXTSEP);
        i= Read_Next_Word(cycleptr,&WORDML,&N, NEXTSEP);
        if((i==EOF) && (!(*WORDML))) goto End;
    }while(count--);
    if(count <=0) g_World->OutputToClient("line not completely read. More than 100000 words",2);
End:
    do{
        l = strlen(line);
        if((k= strchr(" :",line[l-1]) && l)) line[l-1]=0;
    }	  while(k);
    return ((*line !=0));
}

int Read_Multi_Line(FILE *cycleptr,char *line,char *First,char *Second){
    size_t l;
    int i=0 , one_line=0,nw=0,k ;
    long int count= 100000;
    int start=1;
    int N=255; /* WRONG. N should belong to the */
    strcpy(LASTSEP,"\n");
#ifdef PETERSMSW
    strcpy( NEXTSEP,"\n"); // without this we don't read the very first line
#endif

    *First=*Second=*line=0;
    do {
        PC_Strip_Trailing(WORDML); PC_Strip_Leading(WORDML);
        if(0==strncasecmp(WORDML,"EOF",3) )
            goto End;
        if(strstr(LASTSEP,"\n") && (INTEGER_TYPE(strtolower(WORDML)))) { /* its the start of a line*/
            if(*line)
                goto End;
            one_line=(strieq(WORDML,"material")||strieq(WORDML,"interpolation surface"));

            if(!rxIsEmpty(WORDML) || (*NEXTSEP=='\t') ) {
                strcat(line,WORDML);
                strcat(line,":");
                strcpy(First, WORDML); nw++; assert(strlen(line) < MAXLINELENGTH);
            }
        }
        else if(!start) { 			/* NOT a line start */
            if(strlen(line) > MAXLINELENGTH) {
                printf("word is '%s'\n",WORDML);
                g_World->OutputToClient(" line too long. The limit is 256000",4);
            }
            if(strieq(WORDML,"$table") ) {
                i=PC_Read_As_Table(cycleptr,line);
                break;
            }

            if(one_line) {
                /* special treatment of old TABLE. tabs and eols significant
          but even so, dont write blank fields in the Table: */
                /*	if(one_line==1 && stristr(NEXTSEP,"\t") && !is_empty(WORDML)) */
                if(one_line==1 && stristr(NEXTSEP,"\t") ){
                    /* fields in first line*/		 				strcat(line,WORDML);
                    strcat(line," :");
                }
                else if(one_line==1 && stristr(NEXTSEP,"\n")) {
                    strcat(line,WORDML);
                    /* End of first line*/							strcat(line," :table:");
                    one_line++;
                }
                else if(one_line>1 && stristr(NEXTSEP,"\n")){
                    /* End of successive line*/	     						strcat(line,WORDML);
                    /* strip. SO dont care */							PC_Strip_Trailing(line);
                    strcat(line,"\n");
                }
                else if(one_line>1 && stristr(NEXTSEP,"\t") && !rxIsEmpty(WORDML)) {
                    /* fields in first line*/						strcat(line,WORDML);
                    strcat(line,"\t");
                }

                if(nw==0) strcpy(First,WORDML);
                if(nw==1) strcpy(Second,WORDML);
                nw++;
            }
            else {
                if(!rxIsEmpty(WORDML) || (*NEXTSEP=='\t') ) {
                    if(nw==0) strcpy(First,WORDML);
                    if(nw==1) strcpy(Second,WORDML);
                    strcat(line,WORDML);
                    strcat(line," :");
                    nw++;
                }
                if( *line && (*NEXTSEP=='\n' )) {
                    /* 	printf(" line so far <%s> BEFORE STRIP\n", line);  */
                    PC_Strip_Trailing_Chars(line," \n\t:");
                    strcat(line," :");
                    /* 	printf(" line <%s> STRIPPED\n", line);  */
                }
            }
        }

        start=0;
        strcpy(LASTSEP,NEXTSEP);
        i= Read_Next_Word(cycleptr,&WORDML,&N, NEXTSEP);
        if((i==EOF) && (!(*WORDML))) goto End;
    }while(count--);
    if(count <=0) g_World->OutputToClient("line not completely read. More than 100000 words",2);
End:
    const char*myk;
    do{
        l = strlen(line);
        if(!l)
            break;
        myk= strchr(" :",line[l-1] );
        if( myk  && l)
            line[l-1]=0;
    } while(myk);

    return ((*line !=0));
}

#ifndef NEVER
int RXSail::startmacro( const char *File,int *depth) {// return 0 is OK
    assert(File);
    FILE *cycleptr;
    /*	we use our system utility cleanfile which mainly deals with EOL characters, &c between operating systems */

    QDir directory(g_CodeDir);
    QString buf = directory.absoluteFilePath("cleanfile");

    QTemporaryFile  scr, scr2;
    if(scr.open()) scr.close();
    if(scr2.open()) scr2.close();

    buf += "  " ; buf+= QString( File);
    buf+= QString(" "); buf+= scr2.fileName();
    buf+= QString(" "); buf+= scr.fileName();

    system(qPrintable(buf));

    if (( cycleptr = RXFOPEN(qPrintable(scr.fileName()),"r")) == NULL) {
        buf= "could not open macro&<"+QString(File)+QString(">") ;
        OutputToClient(buf,2);
        return(1);
    }
    else {
        Read_Macro(cycleptr,depth);
        FCLOSE (cycleptr);
    }
    return(0);
}
#else

int RXSail::startmacro( const char *File,int *depth) {

    FILE *cycleptr;
    char line[128];
    /*	Convert_Old_Format(File); */
    char scratch[256];
#if defined(linux)
    char buf[512],scratch2[256]; // ,*scr;


    strcpy(scratch,"Rlx_XXXXXX");
    mkstemp(scratch);

    strcpy(scratch2,"Rlx_XXXXXX");
    mkstemp(scratch2);

    assert(File);
    sprintf(buf,"%s/cleanfile %s  %s  %s",g_CodeDir,File,scratch2,scratch);
    system(buf);
    sprintf(buf,"rm  -f %s %s ",scratch2,scratch );
#else
    strcpy(scratch,File);

#endif

    /* change File to scratch to get this to auto remove
     ctrl-M s */

    if (( cycleptr = RXFOPEN(scratch,"r")) == NULL) {
        sprintf(line,"could not open macro&<%s>", File);
        OutputToClient(line,2);
#if defined(linux)
        system(buf);
#endif
        return(1);
    }
    else {
        Read_Macro(cycleptr,depth);

        FCLOSE (cycleptr);
    }


#if defined(linux)
    system(buf);
#endif

    return(0);
}
#endif
int  write_one_line(RXEntity_p e,FILE *fp) {
    /* doesnt just print a line.
  First it strips out the word 'table' from any table entities
  Then it prints the line about 64 chars at a time.
  It doesnt regenerate the line incorporating edits, etc. Thats the job of rewrite_line
 It does change a MS file name C:\My documents to C$\My documents
 see VerifyFileName for the inverse
  */
    static int lasttype;
    char *line;   char *a, *lp,  trailer[32];
#ifdef WIN32
    char *lp2;
#endif
    int k;
    if(!e )
        return 0;

    if(!(e->GetLine()))
        return 0;

    if(e->TYPE != lasttype) fprintf(fp,"\n");
    lasttype=e->TYPE ;
    if(!e->Needs_Resolving)
        e->ReWriteLine();
    /* WRONG
      strictly, only rewrite if user has requested edits to be merged.
      Then delete edit cards at the same time */

    line=STRDUP(e->GetLine());
    trailer[0] = 0;
    if(strieq(e->type(),"fabric")  ||  strieq(e->type(),"interpolation surface")) {
        a = strstr(line,":table:");
        if(a) {
            if(Peter_Debug) cout<< " special treatment of table in write script\n"<<endl;
            strcpy(trailer,"! end of table\n");
            for(k=0;k<6;k++) {
                *a=' '; a++;
            }
            *a='\n';
        }
    }
    //  if(e->TYPE== PCE_DOVE) {
    // RXdovefile *theDove=(RXdovefile *)e->dataptr;
    //theDove->Save("dovesave.txt");
    //cout<< "RXDoveFile Saved to 'dovesave.txt'"<<endl;
    //  }

    /* the following from printall, breaks up the line into manageable width */
    lp = line;
    for(k=0; *lp; lp++, k++) {
        if(*lp=='\n' ) k=0;
#ifdef WIN32					/* a filename is C:\dir. We want to keep the colon */
        lp2 = lp; lp2++;
        if(*lp==':' ){
            if(lp2 && *lp2 == '\\')
                *lp='$';
            else
                *lp='\t';
        }
        fprintf(fp,"%c",*lp);
        cout<<*lp;  // to warn us that there's a problem
#else
        if(*lp==':' )
            fprintf(fp,"%c",'\t');
        else
            fprintf(fp,"%c",*lp);
#endif

        if(k>132&& ((*lp=='\t' ) || (*lp==':')))  {
            fprintf(fp,"\n ");  k=0;
        }
    }

    fprintf(fp,"\n%s",trailer);

    RXFREE(line);
    return 1;
}

int Get_Next_Entity_Name(const char *type,char *name, SAIL *const sail) {  /* assumes name is of form <type>002 */

    int i;
    /* 23/6/94 just use panelcount */

    sprintf(name,"%s%3.3d",type,sail->panel_count);
    sail->panel_count++;
    return(1);
}

