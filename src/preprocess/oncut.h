#ifndef _ONCUT_H_
#define _ONCUT_H_

#if defined (_X) ||defined (WIN32)||defined(RXLIB)
#include "rxON_Extensions.h"
#include "RX_OncIntersect.h"
namespace oncut{

EXTERN_C int ONC_Find_3dm_Cuts(SAIL *sail);
EXTERN_C int ONC_Create_PointList(SAIL *sail,ON_ClassArray<RX_ONC_Cpoint> &clist, double tol ) ;
EXTERN_C int ONC_Create_CurveList(SAIL *sail,ON_ClassArray<RX_ONC_Cpoint> &clist);

EXTERN_C int ONC_Tidy_PointList(ON_ClassArray<RX_ONC_Cpoint> &clist);
EXTERN_C int ONC_Finish_PointList(SAIL *sail,ON_ClassArray<RX_ONC_Cpoint> &clist);
EXTERN_C int ONC_Finish_CurveList(SAIL *sail,ON_ClassArray<RX_ONC_Cpoint> &clist);

EXTERN_C int ONC_Remove_UnFinished(ON_ClassArray<RX_ONC_Cpoint> &CurveList);
EXTERN_C int ONC_Delete_Coincident_Curves(ON_ClassArray<RX_ONC_Cpoint> &CurveList, double tol);

EXTERN_C int ONC_Extend_All_Curves(ON_ClassArray<RX_ONC_Cpoint> &CurveList, double dist );

EXTERN_C int ONC_Find_All_Nearest(
						 ON_ClassArray<RX_ONC_Cpoint> &PointList,
						 ON_ClassArray<RX_ONC_Cpoint> &CurveList,
						 double p_Tol);

EXTERN_C int ONC_SnapEnds(ON_ClassArray<RX_ONC_Cpoint> &clist,double par_tol ,double per_Tol );
EXTERN_C int ONC_Find_All_Intersections(ON_ClassArray<RX_ONC_Cpoint> &Clist,
										ON_ClassArray<RX_ONC_Cpoint> &Plist,
										double tol);

EXTERN_C int ONC_Coalesce(ON_ClassArray<RX_ONC_Cpoint> &pList, double tol );
EXTERN_C int ONC_CreateSitesFromClist(SAIL*sail, ON_ClassArray<RX_ONC_Cpoint> &pointList);
EXTERN_C int ONC_CompleteCurveList(ON_ClassArray<RX_ONC_Cpoint> &PointList,
								   ON_ClassArray<RX_ONC_Cpoint> &CurveList);

EXTERN_C int ONC_ClistSnapToPoints(ON_ClassArray<RX_ONC_Cpoint> &clist,
								   ON_ClassArray<RX_ONC_Cpoint> &Plist,
								   double tol);
EXTERN_C int ONC_CreateAllIALists(ON_ClassArray<RX_ONC_Cpoint> &clist); // turns off needs_cutting (cut?) flag too. 
							// also fills in End1site,End2site on the SC;

EXTERN_C int ONC_Print_Clist(ON_ClassArray<RX_ONC_Cpoint> &clist ,ON_TextLog &dump);

EXTERN_C bool ONC_Print(ON_NurbsCurve *p) ;

EXTERN_C bool ONC_Grow_BoundingBox(rxON_BoundingBox &bb, double dx);

EXTERN_C int ONC_Find_One_Intersections(ON_Curve &c1,
							    ON_Curve &c2,
								double p_tol,
							   ON_ClassArray<RX_OncIntersect> *list1,
							   ON_ClassArray<RX_OncIntersect> *list2);
EXTERN_C int ONC_Remove(ON_ClassArray<RX_ONC_Cpoint> &List, RXEntity_p p_e1, RXEntity_p p_e2);
} // end namespace oncut
#endif
namespace oncut{
extern "C" bool ONC_GetLocalBB(const ON_NurbsCurve *c, rxON_BoundingBox *bb, int n, double tol );
}// end namespace oncut{


#endif
