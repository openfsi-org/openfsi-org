// rlxMasterReaction.h : header file
//

#if !defined(AFX_RLXMASTERREACTION_H__A5AB1385_777A_4224_9885_CCB8A780E656__INCLUDED_)
#define AFX_RLXMASTERREACTION_H__A5AB1385_777A_4224_9885_CCB8A780E656__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CMastReactionDlg dialog

#include "griddefs.h"

//Compute the reaction at all the given master points for a given load p_Force
// Each master point is given by integer which is the adress of the related Site.
// p_Force.x : position of the applyed force [0,1]
// p_Force.y : value of the force
// p_MasterPts[i][3] : position of the i th data point [0,1], p_MasterPts need to be sorted first
//p_degree : defines the degree of the b-spline that is used for reaction computation
// p_knots: defines the knot sequence of the b-spline that is used for reaction computation
// return the reaction for each of the data point
ON_SimpleArray<double> ComputeReactions(const ON_SimpleArray<int> & p_MasterPts, const ON_2dPoint & p_Force,
										const int & p_degree, const ON_SimpleArray<int> & p_knots);


void writetofile(FILE* fp,const ON_2dPointArray & p_ptarray);
void writetofile(FILE* fp,const ON_3dPointArray & p_ptarray);
void writetofile(FILE* fp,const ON_4dPointArray & p_ptarray);

//Find the to item of p_List which are bounding p_t
//pList need to be sorted before
ON_2dPoint find2closestItems(const ON_SimpleArray<float> & p_List, const float & p_t);

float N(const ON_SimpleArray<int> & p_knots, const int & i,const int & m,const float & u);

ON_SimpleArray<int> ReadKnotSequence();

ON_SimpleArray<int> SetKnotSequence(const int & p_m, const int & p_KnotNb);
//Set the knot sequence with a degree p_m and p_p number of knots.
//the first and the last knot are set with a order (p_m+1)
//the other nodes are uniform.
//Example: if p_m = 3 and p_p = 5 the knot sequence will be 
//	{0,0,0,0,1,2,3,4,5,5,5,5}

//summ of all the basis functions which are not null at p_tforce and p_tmaster at p_tpt 
//p_tmaster  and p_tpt [0,1]
//p_knots a B-spline knot sequence [0,0,0,1,2.....7,8,8,8] for instance
//p_degree degree of the Basis curves
//p_tforce is the position where the force is applyed
//p_tmaster is position of the related master point master 
//p_tpt is the position where we want evaluate the reaction
float GetBasisForceAt(ON_SimpleArray<int> p_knots, 
					   const int & p_degree, 
					   const float &p_tforce, 
					   const float & p_tmaster, 
					   const float & p_tpt);

//To read the master points from a file each master point is only defined by its parameter value t [0,1]
ON_SimpleArray<float> ReadMasterPoints();

//!Returns the site define by the adress p_ptr , returns NULL if failled
Site * GetSite(const int & p_ptr);

ON_3dPoint GetCoord(int & p_ptr);
ON_3dPoint GetRawResidual(int & p_ptr);
ON_SimpleArray<double> GetDofValues(int & p_ptr, int & p_dim);

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RLXMASTERREACTION_H__A5AB1385_777A_4224_9885_CCB8A780E656__INCLUDED_)
