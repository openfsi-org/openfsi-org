/* RelaxII source code
 * Copyright Peter Heppel Associates 1995
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 * Modified :
   Nov 97 First shot at NURBS.
  Sept 97 Sample Enough Surface Values now takes 3 points ACROSS the chord and does a Simpson mean.
   It checks whether there are enough chordwise points via Simpson_Correction

   6/1/96  PC_Interpolate allows NULL estart in which case we evaluate Factor at
   current (x,y,z) and return that.
   This is triggered by the keyword 'fn' in the type field of the card

 */
#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include <iostream>
#include "RXEntityDefault.h"
#include"RXScalarProperty.h"
#include "RXSRelSite.h"
#include "RXQuantity.h"
#include "RXSeamcurve.h"
#include "rxON_Extensions.h"
#include "RXRelationDefs.h"

#include "stringutils.h"	
#include "entities.h"
#include "panel.h"
//#include "rdpfile.h" can we get by without??
#include "resolve.h"
#include "etypes.h"
#include "pansin.h"

#include "pcspline.h"
#include "RXGraphicCalls.h"
#include "RX_UI_types.h"
#include "RXSail.h"
#include "interpln.h"

int Sample_Enough_Surface_Values(const RXSitePt &e1b,const RXSitePt &e2b,double e1w,double e2w, RXEntity_p g,double **Ks,double**Sb,int *nks){
    /* return at least *nks values from 	g, on a straight line between e1b and e2b.
   Return the values in Ks and the S's in Sb
   Sept 97  samples 3 values cross-ways and returns the mean weighted by Simpsons
 */

    int k,	Keep_Going;
    double f,w,corr;//,x,y,z
    RXSitePt pt;
    ON_3dVector chord, crossV,l_z;
    chord.x = e2b.x-e1b.x; 	chord.y = e2b.y-e1b.y; 	chord.z = e2b.z-e1b.z;
    l_z =g ->Normal();

    crossV=ON_CrossProduct(l_z,chord);
    if(!crossV.Unitize())
        crossV.x=crossV.y=crossV.z=0.0;

    do {
        *Ks = (double *)REALLOC(*Ks, (*nks+1)* sizeof(double));
        *Sb = (double *)REALLOC(*Sb, (*nks+1)* sizeof(double));
        (*Sb)[0]=0.0;
        for (k=0;k<*nks;k++) {
            f = (double)k / ((double)( *nks-1)) ;
            pt.x = e1b.x *(1.0-f) + f * e2b.x ;
            pt.y = e1b.y *(1.0-f) + f * e2b.y ;
            pt.z = e1b.z *(1.0-f) + f * e2b.z ;
            pt.m_u  = e1b.m_u   *(1.0-f) + f * e2b.m_u   ;
            pt.m_v  = e1b.m_v   *(1.0-f) + f * e2b.m_v   ;
            //it would be very nice to have U and V here
            (*Sb)[k] = sqrt( (pt.x-e1b.x)*  (pt.x-e1b.x) +  (pt.y-e1b.y)*  (pt.y-e1b.y) +  (pt.z-e1b.z)*  (pt.z-e1b.z));
            (*Ks)[k]  = PC_Interpolate(g,pt);

            if(k!=0 && k!=(*nks)-1)  {
                w = e1w *(1.0-f) + f * e2w;
                (*Ks)[k]  = (*Ks)[k]  *2.0/3.0;
                pt.x=pt.x+w/2.0*crossV.x;	pt.y=pt.y+w/2.0*crossV.y;	pt.z=pt.z+w/2.0*crossV.z;
                (*Ks)[k]  += PC_Interpolate(g,pt)/6.0;
                pt.x=pt.x-w*crossV.x;	pt.y=pt.y-w*crossV.y;	pt.z=pt.z-w*crossV.z;
                (*Ks)[k]  += PC_Interpolate(g,pt)/6.0;
            }
        }
        /* we should now use a sagitta method to see if the gaps need filling in
We dont need a very fine tolerance because (1)  this correction will be applied later
in the integration*/
        Keep_Going=0;
        corr = 1.0;
        corr = Simpson_Correction(*nks, *Sb, *Ks);
        if((fabs(1.0-corr) > 0.1) ) {
            Keep_Going=1;
            *nks = (*nks)*2   -1 ;
        }
        if((*nks)  > 126) Keep_Going=0;
    }while(Keep_Going);

    return 1;
}
int Draw_Interpolation_Surface(RXEntity_p e) {
    double u_,v_,x,y;
    double u,v,du,dv,h;
    struct PC_INTERPOLATION *p = (PC_INTERPOLATION *)e->dataptr;
    double zmax=0.0,xmin=0.0,xmax=0.0,ymin=0.0,ymax=0.0,diag; //gcc
#ifndef HOOPS

    return 0;
#else
    int ku,kv;
    int k,nu=11,nv=11;
    ON_3dPoint res;
    VECTOR *pts;

    HC_Open_Segment(e->type());
    e->hoopskey = HC_KOpen_Segment(e->name());
    HC_Flush_Contents(".","everything, no user values");
    HC_Set_User_Index(RXCLASS_ENTITY,e); // switched order so the UI doesnt get flushed



    // if we can cast it to a nurbss we can use ON->drawHoops.  otherwise we draw by sampling

    if (p->NurbData&& p->NurbData->m_pONobject){
        RXON_NurbsSurface np2 ;
        const ON_Surface *np   = ON_NurbsSurface::Cast(p->NurbData->m_pONobject);
        if(np && np->GetNurbForm(np2)) {
            np2.DrawHoops("interpln");
            HC_Close_Segment();
            HC_Close_Segment();
            return(1);
        }
        //	else cout<< " cant draw this ON surface\n"<<endl;
    }
    {
        k=0;
        pts = (VECTOR *)MALLOC(((nu+1)*(nv+1)+2) *sizeof(VECTOR));
        du = 1.0/(float)(nu-1*1)  ;
        dv = 1.0/(float)(nv-1*1) ;
        u = .0 - 0 * du/2.0;

        for(ku=0 ;ku< nu ; ku++, u=u+du) {
            v = .0 -0 * dv/2.0;
            for(kv=0 ;kv< nv ;kv++, v=v+dv) {

                {
                    h =Evaluate_NURBS(e, u , v,&res);
                    x = res.x; y=res.y;
                    //printf(" u v %f %f xyz %f %f %f\n", u,v,x,y,h);
                }

                if(!k) {
                    zmax=h;
                    xmax=xmin=x;
                    ymax=ymin=y;
                }
                if(x>xmax) xmax=x;
                if(x<xmin) xmin =x;

                if(y>ymax) ymax=y;
                if(y< ymin) ymin=y;

                if(h>zmax) zmax=h;
                pts[k].x = (float) x;
                pts[k].y = (float) y;
                pts[k].z = (float) h;
                k++;
            }
        }
        HC_Set_Color("edges=blue");
        HC_Insert_Mesh(nu,nv,pts);
        HC_Insert_Distant_Light(-1.0,1.0,1.0);
        HC_Insert_Distant_Light(0.0,0.0,-1.0);
        RXFREE(pts);
        if(p->NurbFlag && 0) {
            Draw_Control_Graph(p->NurbData);
            Draw_Control_Graph_Mesh(p->NurbData); // will need this for editing. If ever....
            Draw_Surface_Trigraphs(e);
            //#ifdef _DEBUG
            Draw_Nurb_Labels(e, nu,nv);
            //#endif
        }

        /* if its a mould,just translate. Else scale */
        if(0) {
            SAIL *sail = e->Esail;
            if(e == sail->GetMould() || p->NurbFlag)
                HC_Translate_Object(0.0,0.0,zmax*1.0);
            else {
                diag = sqrt( (xmax-xmin)*  (xmax-xmin) + (ymax-ymin)*(ymax-ymin));
                if(zmax > 0.00001) HC_Scale_Object(1.0,1.0,(float) (0.25*diag/zmax));
            }
        }
    }// not a nurbs
    HC_Close_Segment();
    HC_Close_Segment();
    return(1);
#endif
}


int Draw_All_Int_Surfaces(SAIL *sail){

    ent_citer it;
    for (it = sail->MapStart(); it != sail->MapEnd(); ++it) {
        RXEntity_p ep  = dynamic_cast<RXEntity_p>( it->second);
        if(ep->TYPE==INTERPOLATION_SURFACE)    Draw_Interpolation_Surface(ep);
    }
    HC_Update_Display();
    return(1);
}



int PC_Surface_Normal( RXEntity_p gauss, double xx,double yy,double zz, ON_3dVector &n){
    struct PC_INTERPOLATION *p;
    ON_3dVector v0,v1,v2; ON_3dPoint r;
    double delta = 0.005;
    double u,v;
    int flag;

    if(!gauss || gauss->TYPE != INTERPOLATION_SURFACE ) {
        assert(0);
        //	PC_Vector_Copy(sail->Zax is, n);
        return 1;
    }
    p =  (struct PC_INTERPOLATION *) gauss->dataptr;

    if(p->NurbFlag) {
        int l_err;
        Evaluate_NURBS_By_XY(gauss, xx, yy,&u,&v,&l_err,0); assert(!l_err);
        flag = PCN_Evaluate_NURBS_Tangents(gauss,  u, v,&r,&v1, &v2);
        printf("(PC_Surface_Normal )Evaluate_NURBS_Tangents returned %d\n", flag);
    }
    else {
        v0.x=(float)xx; 	v0.y=(float)yy; 	   v0.z= (float)PC_Interpolate(gauss, xx,yy,zz);
        v1.x=(float)(xx+delta); v1.y=(float)yy; v1.z= (float)PC_Interpolate(gauss, (xx+delta),yy,zz);
        v2.x=(float)xx; v2.y=(float)(yy+delta); v2.z= (float)PC_Interpolate(gauss, xx,(yy+delta),zz);
        v1=v1-v0;
        v2=v2-v0;
    }
    n=ON_CrossProduct(v1,v2);
    return n.Unitize();
}

double PC_Interpolate(RXEntity_p gauss, const RXSitePt &p){
    RXScalarProperty *s;
    switch(gauss->TYPE){
    case PCE_SCALAR:

        s = dynamic_cast<RXScalarProperty *>(gauss);
        return s->ValueAt(p);

    case INTERPOLATION_SURFACE:
        return  PC_Interpolate(gauss, p.x,p.y,p.z);
    default:
        assert("Interpolation not coded for thie TYPE"==0);

    };
    return 0.0;
}
double PC_Interpolate(RXEntity_p e, double xx,double yy,double zz){
    struct PC_INTERPOLATION *p;
    //  double x;//,y;
    double xout,yout,zout,z_dummy;
    double u=0.5,v=0.5,retval;

    RXQuantity *ff;
    if(!e) return 0.0;
    p = (PC_INTERPOLATION *)e->dataptr;
    if(!p){
        e->OutputToClient("  PC_Interpolate with NULL",1); return 0.0;
    }

    if(p->NurbFlag) {
        int l_err;
        retval = Evaluate_NURBS_By_XY(e, xx, yy,&u,&v,&l_err,0); // assert(!l_err);
        if(l_err) {
            printf(" Evaluate_NURBS_By_XY fails atxy=( %f %f) giving uv=( %g %g)\n", xx,yy,u,v);
            fflush(stdout);
        }
        if(p->refSC) {
            sc_ptr sc = (sc_ptr  )p->refSC;
            if (sc->m_arcs[1] > 0.001)
                retval = retval /pow((double)sc->m_arcs[1],p->Scale_Power);
        }

        if(ff=dynamic_cast<RXQuantity *>(e->FindExpression (L"factor",rxexpLocal)))
            retval = retval * ff->evaluate();
        return(retval);
    }//nurbflag

    //x = xx;
    //y = yy;
    {	 /* estart NULL so a value_of */
        e->OutputToClient("interpolation no longer allows functions",2); return 0;
    }
}

int Compute_Interpolation_Card(RXEntity_p e) {

    int retVal = 0;
    char *cline=NULL;// char*cl=NULL;

    struct PC_INTERPOLATION *ptr =  (PC_INTERPOLATION *)e->dataptr;
    int pn=0;
    //   int sli = 999;
    //  float background = 0.0;
    int errorflag = 0;
    int i=0;


    if(!e->NeedsComputing())  /* why is this here ??? */
        return(1);

    if(ptr->type[0] == 'F') {		  /* F-style has not interpolation data */
        retVal =1;
        goto End;
    }
    if(ptr->type[0] == 'N') {
        ptr->NurbFlag=1;
        retVal =Compute_NURBS(e);

        goto End;
    }

    cline = stristr(e->GetLine(),"table");
    if(cline)	{
        cline = strchr(cline,':');
        if(cline) cline++;
    }

    if(!cline){
        errorflag = 1;
        goto End;
    }
    cline = STRDUP(cline);
    //cl = cline;


    /* now have a pointer to the text data of the table only in cline */


    retVal = 0; pn=0;// read_nlet_from_text(cl,&slist,&pn,3); /* is 3 correct ??? */
    if(!retVal || (pn < 3)){
        errorflag = 2;
        goto End;
    }

    /*  Remove_Duplicates_From_SiteList( slist,&pn);  */


    if(pn < 3){
        errorflag = 3;
        goto End;  /* cant make any triangle out of something less than 3 points !! */
    }
    if(ptr->type[0] == 'U') {
        cout << "U type ISurfs are no longer supported"<<endl;
        /* for(i=0,s=slist;i<pn;i++,s++) {
   if(!AKM_Pansail_UVtoXYZ(e->Esail,((*s)->x),((*s)->y),&((*s)->x),&((*s)->y),&((*s)->z))) {
     retVal=0;
     errorflag = -1;
     goto End; // usually because leading and trailing not yet Finished
    }
  }*/
    }


End:
    if (errorflag>0) {
        char *buf = (char*)MALLOC(128+strlen(e->GetLine()));;
        sprintf(buf,"In Compute_Interpolation Surface, error flag = %d on \n%s",errorflag,e->GetLine());
        e->OutputToClient(buf,2);
        RXFREE(buf);
    }

    if(cline)
        RXFREE(cline);

    if(ptr->data) { assert(0);
        ptr->nsites=0;
        ptr->data = NULL;
    }

    if(retVal) {
        e->SetNeedsComputing(0);
        ptr->data = 0;
        ptr->nsites = pn;
    }

    return(retVal);
}

int Resolve_Interpolation_Card (RXEntity_p e) {
    /* card is of type
     interpolation surface: <name> :  <(uv|xy|fn)>:  [[[refedge] : sfactor]	 :power]
     [ a table] */

    HC_KEY key=0;
    RXQuantity*q;
    int retval=0;
    RXEntity_p ep;


    const char*name,*text,*dummy;
    std::string sline(e->GetLine() );
    rxstriptrailing(sline);
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();
    if(nw<3)
        return 0;

    name=wds[1].c_str();{
        text=wds[2].c_str();{
            struct PC_INTERPOLATION *ptr = (PC_INTERPOLATION *)CALLOC(1,sizeof(struct PC_INTERPOLATION));
            q= dynamic_cast< RXQuantity*>( e->AddExpression (new RXQuantity ( L"factor",L"1.0",L"",e->Esail )));

            ptr->Scale_Power=2.0;

            if (nw>3){
                dummy=wds[3].c_str();
                if( (!rxIsEmpty(dummy)) && (!strieq(dummy,"table"))) {
                    if (!rxIsEmpty(dummy)  && (!strieq(dummy,"null"))) {
                        ep = e->Esail->Get_Key_With_Reporting("seam,curve,compound curve",dummy);
                        if(!ep) { RXFREE(ptr); return 0;}
                        ptr->refSC = ep;
                        ep->SetRelationOf(e,child,RXO_SC_OF_ISURF);
                    }
                    if (nw>4){
                        dummy=wds[4].c_str();
                        if((!rxIsEmpty(dummy)) && (!strieq(dummy,"table"))){
                            q->Change(TOSTRING(dummy)); // PCQ_Set_Quantity_By_Text(&ptr->Factor,dummy);
                            if(!q->ResolveExp()) {
                                delete q;
                                RXFREE(ptr);
                                return 0;
                            }
                        }
                        if (nw>5){
                            dummy=wds[5].c_str();
                            if((!rxIsEmpty(dummy)) && (!strieq(dummy,"table")))
                                ptr->Scale_Power = atof(dummy);
                            if (nw>6){
                                dummy=wds[6].c_str();
                                if((!rxIsEmpty(dummy)) && (!strieq(dummy,"table")))
                                    e->AttributeAdd(dummy);
                            } // if 6

                        } // if 5
                    }  //if parse word 4
                }
            } // if parse word 3
            if(strieq(text,"xy")) {
                ptr->type[0] = 'X';
            }
            else if(strieq(text,"uv")) {
                ptr->type[0] = 'U';
                // debug only ep = Get_Key_With_Reporting(e->Esail->GetReportForm(),"pansail","namepansail");
                // assert(ep == e->Esail->pansailEnt);
                ep = 0;// e->Esail->m_pansailEnt;
                if(!ep) {e->OutputToClient("(Resolve_Interpolation_Card) UV variant not supported",4); RXFREE(ptr); return(0); }

                ep->SetRelationOf(e,child|niece,RXO_AERO_OF_ISURF);
            }
            else if(strieq(text,"FN")) {
                ptr->type[0] = 'F';
            }
            else if(strieq(text,"nurb") || strieq(text,"nurbs")   ) {
                ptr->type[0] = 'N';
            }
            else
            {
                char buf[256];
                sprintf(buf,"only XY or UV  or FN or nurb surfaces currently supported\n Cant interpret type '%s'",text);
                e->OutputToClient (buf,2);
                return(0);
            }

            if(e->PC_Finish_Entity("interpolation surface",name,ptr,key,NULL,qPrintable( e->AttributeString()) ,e->GetLine())) {
                e->SetNeedsComputing();
                e->Needs_Finishing=1;
                retval=1;
            }
            else
                retval=0;

        }
    }
    e->Needs_Resolving=(!retval);
    return(retval);
} 
