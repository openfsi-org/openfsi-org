// RXPointOnCurve.cpp: implementation of the RXPointOnCurve class.
//
//////////////////////////////////////////////////////////////////////


#include "StdAfx.h"

#include "RXPointOnCurve.h"

#ifdef _DEBUG_NEVER
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "assert.h"

RXPointOnCurve::RXPointOnCurve()
{
	Init();
}//RXPointOnCurve::RXPointOnCurve

//Copy constructor
RXPointOnCurve::RXPointOnCurve(const RXPointOnCurve & p_Obj)
{	
	Init();
	this->operator =(p_Obj);
}//RXPointOnCurve::RXPointOnCurve(double p_x,double p_y,double p_z)

RXPointOnCurve::~RXPointOnCurve()
{
	Clear();
}//RXPointOnCurve::~RXPointOnCurve

void RXPointOnCurve::Init()
//meme alloc + init val to 0 0 0
{
	m_Ncurve.Initialize(); // really a NURBS Curve?????????
	m_t = 0.0;
}//void RXPointOnCurve::Init

int RXPointOnCurve::Clear()
{ 
	m_t = 0.0;
	m_Ncurve.Destroy();

	return(1);
}//int RXPointOnCurve::Clear

RXPointOnCurve& RXPointOnCurve::operator = (const RXPointOnCurve & p_Obj)
{
	//Make a copy
	Init();

	m_t = p_Obj.m_t;
	assert(dynamic_cast<const ON_Curve*>(&(p_Obj.m_Ncurve)));

#ifdef _DEBUG_NEVER
	assert(p_Obj.m_Ncurve.IsValid()); //NURBS
#endif  //#ifdef _DEBUG
	
	m_Ncurve = p_Obj.m_Ncurve;  //  COPY the nurbs curve  Really??????
	return *this;
}//void RXPointOnCurve::operator =

double RXPointOnCurve::Gett() const
{
	return m_t;
}//int RXPointOnCurve::Sett

ON_3dPoint RXPointOnCurve::GetPos() const
{
	assert(dynamic_cast<const ON_Curve*>(&m_Ncurve));
	ON_3dPoint l_pt(0.0,0.0,0.0);
	if (m_Ncurve.IsValid())
		l_pt = m_Ncurve.PointAt(m_t); 
	return l_pt;
}//ON_3dPoint RXPointOnCurve::GetPos

ON_3dVector RXPointOnCurve::GetTangent() const
{
	assert(dynamic_cast<const ON_Curve*>(&m_Ncurve));
	ON_3dVector l_tan(0.0,0.0,0.0);
	if (m_Ncurve.IsValid())
		l_tan = m_Ncurve.TangentAt(m_t);
		
	return l_tan; 
}//ON_3dPoint RXPointOnCurve::GetPos


int RXPointOnCurve::SetToMin()
{
	assert(dynamic_cast<const ON_Curve*>(&m_Ncurve));
	if (m_Ncurve.IsValid())
		m_t = m_Ncurve.Domain().Min(); 
	return 1;
}//int RXPointOnCurve::SetToMin

int RXPointOnCurve::SetToMax()
{
	assert(dynamic_cast<const ON_Curve*>(&m_Ncurve));
	if (m_Ncurve.IsValid())
		m_t = m_Ncurve.Domain().Max(); 
	return 1;
}//int RXPointOnCurve::SetToMin

int RXPointOnCurve::SetToMid()
{
	assert(dynamic_cast<const ON_Curve*>(&m_Ncurve));
	if (m_Ncurve.IsValid())
		m_t = m_Ncurve.Domain().Mid(); 
	return 1;
}//int RXPointOnCurve::SetToMid

ON_Interval RXPointOnCurve::Domain() const
{
	assert(dynamic_cast<const ON_Curve*>(&m_Ncurve));
	return m_Ncurve.Domain();
}//ON_Interval RXPointOnCurve::

double RXPointOnCurve::GetLength(double fractional_tolerance) const
{
	double l_len=0.0; 
	assert(dynamic_cast<const ON_Curve*>(&m_Ncurve));
	m_Ncurve.GetLength(&l_len,fractional_tolerance);
	return l_len;
}//double RXPointOnCurve::GetLength

int RXPointOnCurve::Set(const double &p_t, const ON_Curve * p_crv)
{
	assert(p_crv); 
	assert(dynamic_cast<const ON_Curve*>(p_crv));
	
	ON_Interval l_cdomain = p_crv->Domain();
	if (p_crv->IsValid())
	{
		ON_NurbsCurve l_temp;
		p_crv->GetNurbForm(l_temp);
		assert(l_temp.IsValid());
		if (!l_temp.IsDuplicate(m_Ncurve,1))
			m_Ncurve = l_temp;//	SetCurve(p_Obj.m_curve);
	}
	assert(p_crv->IsValid());
	
	m_t = p_t;
//	assert((p_t>=m_curve.Domain().Min())&&(p_t<=m_curve.Domain().Max()));

	ON_Interval l_domain = m_Ncurve.Domain();

// VERY BAD AND WRONG  t is in the old p_crv.Domain(); and this function changes the domain.
	
/*	m_t = p_t; 
	if (m_t<l_domain.Min())
		m_t = l_domain.Min(); 
	if (m_t>l_domain.Max())
		m_t = l_domain.Max(); 
*/
	return RXPOINTONCURVE_OK;
}//int RXPointOnCurve::Set

int RXPointOnCurve::Print(FILE *fp) const
{
	int i = fprintf(fp," curve = \tP%p \t t = %f\n",&m_Ncurve,m_t);	
	return i;
		
// compiler warning p:168: warning: cannot pass objects of non-POD type `
		

}//int RXPointOnCurve::Print

int RXPointOnCurve::IsValid() const
{
	assert(dynamic_cast<const ON_Curve*>(&(m_Ncurve)));
	assert(ON_NurbsCurve::Cast(&(m_Ncurve)));
	return m_Ncurve.IsValid();
}//int RXPointOnCurve::IsValid
