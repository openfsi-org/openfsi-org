#pragma once
#include <map>
#include <vector>
using namespace std;

#include "RXHarwellBoeingIO.h"
// RXSparseMatrix is the class used for texture mapping

//conversely,  RXSparseMatrix1 is used for the finite element stiffness matrix.
// I think that's because it is indexed from 1 - for pardiso
// values for m_StorageType, 
#ifndef RXM_SMTYPE_DEF
enum  smtype{NONE,MAPSTYLE,CSC };
#define  RXM_SMTYPE_DEF
#endif
class RXSparseMatrix

{
public:
	RXSparseMatrix(const MatrixType t);
	RXSparseMatrix(const map<pair<int,int>,double> m,const MatrixType t);
	virtual ~RXSparseMatrix(void);
	int Init(void);
	

// careful. These operators return a matrix of type NONE
// if the matrices don't conform 
	const RXSparseMatrix operator+(const RXSparseMatrix&) const;
	const RXSparseMatrix operator*(const double&);
	const RXSparseMatrix& operator*=(const double&);

	int ZeroRow(const int i);
	int ZeroColumn(const int i);
	int SolveOnce(double*result); // return value 0 means OK. this is not our usual convention
    int SolveDSS(double*presult); // return value 0 means OK. this is not our usual convention
	int SolveIterative( double*result,const double tol);
	enum  smtype SetType(const enum  smtype t);
	int SetRHS(const int nrhs, double* p_rhs);
	int MTXWrite(const char*fname);
	int MTXRead(const char*fname);
	int HB_Write(const char*fname);
	int HB_Read(const char*fname);
	void SetMatrixType(const MatrixType t){ m_MatType =t;}
protected:
	int MapToCSC(void);
	int CSCToMap(void);


public:
	map<pair<int,int>,double> m_map;
	vector<double> rHs;// may be a multiple of nr.
	int m_nRhsVecs;
	enum  smtype m_StorageType;
    int Nr() const { return m_hbio.nrow;}
    int Nc() const { return m_hbio.ncol;}
 protected:
    void SetNr(const int n){ m_hbio.nrow=n;}
    void SetNc(const int n){ m_hbio.ncol=n;}

public:
	class RXHarwellBoeingIO m_hbio;
    enum  MatrixType m_MatType;

public:
static int IsSymmetric(const map<pair<int,int>,double> &m,const double &tol );
static int MakeUpperTriangular( map<pair<int,int>,double> &m  );
};


