/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *  2009 using stl containers.
 *
 * Modified :
 *            10/2/95                 rxerror messages improved 
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */
#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include "RXSail.h"
#include "RXEntityDefault.h"
#include "RXSRelSite.h"
#include "RXCompoundCurve.h"
#include "RXRelationDefs.h"

#include "stringutils.h"
#include "batdefs.h"

#include "boundary.h"


int Included_In(Site * ps,std::map<int,Site *> &list) {
 	int k;
	 for (k=0;k<list.size();k++) {
	  	if(ps==list[k]) 
			return(1);
	 }
	return(0);
}

int Included_In(RXEntity_p ps,std::map<int,RXEntity_p> &list) {
 	int k;
	 for (k=0;k<list.size();k++) {
	  	if(ps==list[k]) 
			return(1);
	 }
	return(0);
}
RXEntity_p  Find_Connecting_Edge( Site**s1, RXEntity_p NotThis,int*rev) {
  
     sc_ptr  sc;
 
	assert(s1 && *s1);
	SAIL *sail = (*s1)->Esail;
	ent_citer it;
    for (it = sail->MapStart(); it != sail->MapEnd(); ++it) {
		RXEntity_p ps  =dynamic_cast<RXEntity_p>( it->second);
			if(ps!=NotThis && strieq(ps->type(),"seamcurve") ){ 
				sc=(sc_ptr  )ps;
				if(sc->edge) {
					if (sc->End1site==*s1) { *s1= sc->End2site; *rev=0; return(ps);}
					if (sc->End2site==*s1) { *s1= sc->End1site; *rev=1; return(ps);}
				}
			}
		}
return(NULL);
}
int Trace_Boundary(RXEntity_p e) {
 /* generates compound curves.
 	For each pair of sites in the corners list, try to find
	a sequence of seamcurves (with edge flag set) which go from
	one to the next NOT via any other	*/
	   RXEntity_p ps, pso, pstart=NULL;
	   sc_ptr ec=0;
	   Site *s1, *s0,*s2;
	   std::map<int,RXEntity_p> dumlist;
	   class RXCompoundCurve *   bc;
	   int nd=0;
	   struct PC_BOUNDARY*p;
	   char str[256];
	   int count, k,k2,l;
	   int revflag;
		std::map<int,int> revlist;

	    if (!e->NeedsComputing()) return(1);
	
	    p=(PC_BOUNDARY *)e->dataptr;
		p->nc = p->m_corners.size();
		for(	k=0;k<p->nc;k++) {
			pso=NULL;
	   		k2 = increment(k,p->nc);
			s1 = p->m_corners[k];
			s2 = p->m_corners[k2];
			s0=s1;
			nd=0;
			dumlist.clear(); revlist.clear();		count=0;	
 	   		do {
				nd = dumlist.size();
				if(!nd&&count!=0) pso=pstart;
		 		ps = Find_Connecting_Edge( &s1,pso,&revflag);
				if(!nd&&!count) pstart=ps;
		 		if(ps&&(!ps->Needs_Resolving)) {
			 	
		 			if(!Included_In(ps,dumlist)){ /* one found */
					pso=ps;
	 				nd++;
					dumlist[nd-1]	= ps;
					revlist[nd-1] = revflag;
	 
		 	 		if(s1==s2) {
							for(l=0;l<nd;l++) {
							HC_KEY key = dumlist[l]->hoopskey;
							if(key) {
								HC_Open_Segment_By_Key(key);
								HC_Set_Line_Weight(3.0);
								HC_Close_Segment();
								}
							}
							/* create a compound curve */
							assert("new SC"==0);;
							bc=0;//(sc_ptr  )CALLOC(1,sizeof(struct PC_SEAMCURVE));

							ec = p->m_edges[k];
							ec->SetDataPtr(bc);
			
							ec->Needs_Finishing=1;
							ec->Needs_Resolving=0;
		
							ec->SeamCurve_Init(); // Peter 2004

											
							bc->m_ccCurves=( sc_ptr *)MALLOC(dumlist.size() *sizeof(void*)); for(int q=0;q<dumlist.size();q++)  bc->m_ccCurves[q]=(sc_ptr) dumlist[q];
							bc->m_ccNcurves =nd;
							bc->m_ccRevflags=(int*)MALLOC(dumlist.size() *sizeof(int)); for(int q=0;q<revlist.size();q++)  bc->m_ccRevflags[q]=revlist[q];

							 break;
					}
					else {
						if(Included_In(s1, p->m_corners)) // we've reached a corner
 			 				{
			 					s1=s0;  
			 					assert(!(!dumlist.size() || !revlist.size()) );
								dumlist.clear(); revlist.clear();
								nd=0; 
			 					count++; 
			  				}
					}
					} // if not in dumlist
					else {
						e->OutputToClient(" There is a  loop in the edge of the model\n Has the TPN file been cleaned? ",2);
						}
					}
				else { /* No hit */
					sprintf(str, "(boundary finding) edge not found from %s to %s ",p->m_corners[k]->name(),p->m_corners[k2]->name());	
					e->OutputToClient(str,1);
					return(0);	
	 			 }
			 } while (s1 != p->m_corners[k2]&&count<2);
	   } // for k

  	  e->SetNeedsComputing(0);
return(1); 
} 

int Resolve_Boundary_Card (RXEntity_p e) {
	 /* boundary card is of type
	                    nodes       edge names
		     0       1   2  3  4      5  6
	boundary : <name> : n1: n2:n3:n4 ...:e1:e2:... 
	NOTE: no of nodes = no of edges
 
	 */
	char *name;
	char*words[200],*lp,**wd,**Edgename,**firstsite;
	int i,nsites,nedges;
	char line[2048];
	struct PC_BOUNDARY *p;
	Site* ec;
	int retval=0;

	strcpy(line,e->GetLine());
	wd=words;

	strrep(line,'!','\0');
        lp=strtok(line,RXENTITYSEPS);
	i=0;
        while((lp=strtok(NULL,RXENTITYSEPS))!=NULL) {
		words[i]=lp;
		PC_Strip_Leading(words[i]);
		PC_Strip_Trailing(words[i]);
		i++;
	}
	name = *wd;
	i--;
	nedges=nsites = i/2;
	if(nedges < 3 || i < 6) {
		e->OutputToClient("Incorrect boundary card",2);
		return(0);
	}
 
	Edgename=wd; Edgename+=nedges;
	wd++;
	firstsite = wd;

 /* We now have a list of sites and a list of future compound curves */

	p = new struct PC_BOUNDARY; p->nc=0;p->ne=0;
 	if (!p) {
		e->OutputToClient("out of memory",2); 
		return(0);
	}
	for(i=0;i<nsites;i++,wd++) {
	
		ec= (Site*)e->Esail->Get_Key_With_Reporting("site,relsite",*wd);
		if(ec) {
			p->m_corners[p->nc]=ec;
			(p->nc)++; 
			}
		if(i < (nsites-1))	/* just add another Global_Report_Form entry */
			ec= (Site*) e->Esail->Get_Key_With_Reporting("site,relsite",*(wd+1));
		else
			ec= (Site*)e->Esail->Get_Key_With_Reporting("site,relsite",*firstsite);
	} // for I
	if(p->nc==nsites) { // means we resolved all the corners
		for(i=0;i<nsites;i++) {
			Edgename++;
			p->m_edges[i]=dynamic_cast<sc_ptr>(e->Esail->Insert_Entity("compound curve",*Edgename,NULL,(long)0," ","!cc generatedby boundary"));
			e->SetRelationOf(p->m_edges[i],spawn,RXO_CC_OF_BNDY,i);  
 			(p->ne)++; 
			p->m_edges[i]->Needs_Resolving=1;
		}

	/* copy edge names into names field */

		for(i=0;i<nedges;i++,wd++) {
			p->m_names[i] = *wd;
		}

		if(e->PC_Finish_Entity("boundary",name,p,(long)0,NULL," ",e->GetLine())){
			e->SetNeedsComputing();
			if(Trace_Boundary(e)) {
				e->Needs_Resolving=0;
				retval = 1;
			}
   	   }
 	} //  if(p->nc==nedges)
	else { // some corners wrent resolved
		p->m_corners.clear();  
		p->m_edges.clear();	
		p->m_names .clear();
		delete p;
		e->SetDataPtr( NULL);
		retval = 0;
	}

return(retval);
}
