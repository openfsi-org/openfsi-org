#include "StdAfx.h"
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <mkl_spblas.h>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
using namespace std;
#ifdef OMP
#include <omp.h>
#endif

#include "hb_io.h"

#include "mkl_pardiso.h"
#include "mkl_types.h"
#include "RXHarwellBoeingIO.h"
#ifdef linux
#define  PDDBG (0)
#else
#define  PDDBG (0)
#endif
/*
This is a wrapper for 'hb_io.c/h' the official harwell-boeing reader/writer. 
The idea is that to write a HB file you  create an instance of this class using the appropriate constructor
and then call the Write method.
We've changed to double precision and altered the formatting somewhat. 
Currently impl/home/r3/build/thirdparty

To make it industrial-strength it needs:
1) remove the memory bottleneck in the Write method by translating in blocks
2) Reading, including RHS, exact and guess
3) its aware of what sort of matrix it is   
 //= 1 real and structurally symmetric matrix
 //= 2 real and symmetric positive definite matrix  = usual FE matrix, properly restrained.
 //= -2 real and symmetric indefinite matrix
 //= 11 real and unsymmetric matrix   = for surface parameterization.
   this affects:
   A) Whether we skip attempts to set lower-diagonal entries. DONE
   B) The pardiso solver parameters
   C) The HB write parameters - and file extension.
   D) Any checking to see whether a matrix complies with its given type.
   Note M_MatType must be defined in the constructor. It is fixed for the lifetime of the matrix.

4)	separate solution steps, to allow the first stage to be carried over where the matrix structure hasnt changed
 note this is difficult to ensure because DOFOs can change the structure.
*/

// horrible static to ensure pt is ony zeroed once.
//bool g_pardisostarted=0;
int RXHarwellBoeingIO::Test(const char*filename){
    RXHarwellBoeingIO hb;
    if(!hb.Read (filename))
        return 0;
    int i, n = hb.nrow;
    cout<<" RXHarwellBoeingIO::Test "<<filename<<endl;
    double*rhs = new double[n+1];
    double*res = new double[n+1];
    for(i=0;i<=n;i++) { rhs[i]=1.0;  res[i] = 999;}
    hb.m_nRhsVs=1; hb.rhsval=rhs;
    hb.rhstyp= new char[5]; strcpy(hb.rhstyp,"F  "); // full, no guess, no exact
    hb.SolveSym(rhs,res);
    for(i=0;i<=n;i++) cout<<i<<" "<< setprecision (12)<<res[i]<<endl;
    delete [] res;
    return 1;
}
RXHarwellBoeingIO::RXHarwellBoeingIO()
    :m_MatType(MTNONE)
{
    ptrfmt=0 ;	indfmt=0;	valfmt=0;
    rhsfmt=0;	rhstyp=0;	totcrd=0;	ptrcrd=0;	indcrd=0;
    valcrd=0;	rhscrd=0;	nrow=0;		ncol=0;		nnzero=0;
    neltvl=0;	m_nRhsVs=0;	nrhsix=0;	colptr=0;	rowind=0;

    values=0;	rhsval=0;	rhsptr=0;	rhsind=0;	rhsvec=0;
    guess=0;	exact =0;
}
int RXHarwellBoeingIO::Init() {
    title.clear() ;
    key.clear();
    if( ptrfmt) delete []ptrfmt ;
    if( indfmt) delete []indfmt;
    if( valfmt) delete []valfmt;

    if( rhsfmt) delete []rhsfmt;
    if( rhstyp) delete []rhstyp;
    mxtype.clear();
    if(colptr) delete []colptr;

    if(rowind) delete []rowind;
    if(values) delete []values;
    if(rhsval) delete []rhsval;	rhsval=0;
    if(rhsptr) delete []rhsptr;
    if(rhsind) delete []rhsind;

    if(rhsvec) delete []rhsvec;
    if(guess) delete []guess;
    if(exact) delete  []exact;

    ptrfmt=0 ;	indfmt=0;	valfmt=0;
    rhsfmt=0;	rhstyp=0;	totcrd=0;	ptrcrd=0;	indcrd=0;
    valcrd=0;	rhscrd=0;	nrow=0;		ncol=0;		nnzero=0;
    neltvl=0;	m_nRhsVs=0;	nrhsix=0;	colptr=0;	rowind=0;
    values=0;	rhsval=0;	rhsptr=0;	rhsind=0;	rhsvec=0;
    guess=0;	exact =0;
    return 1;
}
RXHarwellBoeingIO::~RXHarwellBoeingIO(void)
{
    if( ptrfmt) delete[] ptrfmt ;
    if( indfmt) delete []indfmt;
    if( valfmt) delete []valfmt;

    if( rhsfmt) delete []rhsfmt;
    if( rhstyp) delete []rhstyp;

    if(colptr) delete[] colptr;

    if(rowind) delete []rowind;
    if(values) delete []values;
    if(rhsval) delete []rhsval; rhsval=0;
    if(rhsptr) delete []rhsptr;
    if(rhsind) delete []rhsind;

    if(rhsvec) delete []rhsvec;
    if(guess) delete []guess;
    if(exact) delete  []exact;

}
RXHarwellBoeingIO::RXHarwellBoeingIO(map<pair<int,int>,double> m,
                                     const char*p_title,
                                     const char*p_key,
                                     const MatrixType t)
    :m_MatType(t)
{
    Fill(m,p_title,p_key);
}

void RXHarwellBoeingIO::Fill(map<pair<int,int>,double> m,const char*p_title, const char*p_key ){
    int ni=0;
    int nj=0;
    int nel=0;

    map<pair<int,int>,double> ::const_iterator it;
    for(it=m.begin(); it!=m.end();++it) {
        nel++;
        ni=max(ni,it->first.first);
        nj=max(nj,it->first.second);
    }
    ni++; nj++; // we write in index 1
    /*	int  */  nrow =ni;
    /*	int  */  ncol=nj;

    title =  p_title ;
    key =  p_key ;
    if(rhstyp) delete []rhstyp;
    /*char*  */   rhstyp= new char[5]; strcpy(rhstyp,"F  "); // full, no guess, no exact
    /*	char*  */  mxtype= "RSA" ; // write as unsymmetric

    /*	int  */  nnzero=nel;
    /*	int */   neltvl =-999;

    /*	int  */  m_nRhsVs=0; // no of RHS vectors, each length nrow
    /*	int  */  nrhsix=0;

    /*	float * */  rhsval=0;
    /*	int * */  rhsptr=0;
    /*	int * */  rhsind=0;
    /*	float * */  rhsvec=0;
    /*	float * */  guess=0;
    /*	float * */  exact =0;
    //cout << "TO HB format. we should do this in blocks, destroying the old as we go"<<endl;
    if(colptr) delete []colptr;
    if(rowind) delete []rowind;
    if(values) delete[] values;
    /*	int * */  colptr =new int[max(ni,nj)+1];
    /*	int * */  rowind =new int[nel+1];
    /*	float * */ values =new double[nel];
    int i,n=0;
    for(i=0;i<max(ni,nj)+1;i++) colptr[i]=-1;
    colptr[max(ni,nj)]=nel+1;

    i=0; colptr[i]=1;
    for(it=m.begin(); it!=m.end();++it) {
        ni=it->first.first;
        nj=it->first.second;
        if(ni != i) {
            i=ni; colptr[i]=n+1;
        }
        rowind[n] = nj+1;
        values[n] = it->second;
        n++;
    }
}
int RXHarwellBoeingIO::SetupForWrite()
{
    //start cut
    pair<string,string> ifmts,fltfmts;
    if(valfmt) delete []valfmt;
    if(ptrfmt) delete[] ptrfmt;
    if(indfmt ) delete []indfmt;
    if(rhsfmt ) delete []rhsfmt;


    valfmt = new char[12]; strcpy(valfmt,"(3E26.16)"); // 3 per line
    cfltfmt=string("g26.16 g26.16 g26.16");
    ifmts = MakeFormats(max(nrow,ncol)+1,72,ptrcrd);
    ptrfmt=new char[1+strlen(ifmts.second.c_str())];strcpy(ptrfmt,ifmts.second.c_str());

    Cintfmt = ifmts.first;
    ifmts = MakeFormats(nnzero*10,72,indcrd);
    indfmt= new char[1+strlen(ifmts.second.c_str())]; strcpy(indfmt,ifmts.second.c_str());

    valcrd =(int)ceil(((double)nnzero)/3.0);

    rhsfmt=new char[12]; strcpy(rhsfmt,"(3g20.9)");

    rhscrd =(int) ceil ((double)(this->nrow *this->m_nRhsVs )/3.0);
    totcrd = indcrd+ptrcrd+valcrd+rhscrd;
    // end cut
    return 1;
}
int RXHarwellBoeingIO::Write(const char*filename){
    ofstream output;
    output.open ( filename  );
    if ( !output )
    {
        cout << "  Error opening  file: "<<filename<<endl;
        return 0;
    }
    try{
        this-> SetupForWrite();
        hb_file_write ( output, title.c_str(), key.c_str(), totcrd, ptrcrd, indcrd,
                        valcrd, rhscrd, mxtype.c_str(), nrow, ncol, nnzero, neltvl, ptrfmt, indfmt,
                        valfmt, rhsfmt, rhstyp, m_nRhsVs, nrhsix, colptr, rowind, values,
                        rhsval, rhsptr, rhsind, rhsvec, guess, exact );
    }
    catch(HBIO_X &e){
        e.Print();  output.close ( ); return 0;
    }
    catch(...){
        cout <<"caught an undefined exception"<<endl;
        output.close ( ); return 0;
    }
    output.close ( );
    if(output.fail()){
        cout << "file write failed "<<filename<<endl;
        return 0;
    }
    else{
        return 1;
    }

}
int RXHarwellBoeingIO::Read(const char*filename)
{
    char buf[512];
    filename_copy_local(buf,512,filename );	ifstream input;    input.open ( buf  );
    if ( !input )
    {
        cout << "  Error opening  file: "<<filename<<endl;
        return 0;
    }
    try{
        this->Init ();
        hb_file_read ( input, title, key, &totcrd,
                       &ptrcrd, &indcrd,&valcrd, &rhscrd, mxtype, &nrow,
                       &ncol, &nnzero,&neltvl, &ptrfmt, &indfmt, &valfmt,
                       &rhsfmt, &rhstyp, &(this->m_nRhsVs  ),&nrhsix, &colptr,
                       &rowind, &values, &rhsval, &rhsptr, &rhsind,
                       &rhsvec,&guess, &exact );

    }
    catch(...)
    {cout<<"HBIO read error"<<endl;  return 0;}


    return 1;
}
// first is C format, second is fortran format
pair<string,string> RXHarwellBoeingIO::MakeFormats(const int n, const int linewidth, int&nlines)
{
    int i,j,k,nw;
    int c=72;
    pair<string,string>rc ;
    stringstream s,f;
    j=1;
    for(i=1;;i++) {
        j = 10*j;
        if(j > n) break;
    }

    nw = linewidth/(i+1);  // no of words per line
    s.clear();
    for(k=0;k<nw;k++) {
        s<<"%"<<i<<"d "; c-=(i+1);
    }
    for(k=0;k<c;k++)
        s<<" ";
    s<<"\\n";
    rc.first = s.str();

    f <<"("<<nw<<"i"<<i+1<<" )";
    rc.second  =f.str();
    nlines = (int) ceil((double)n/((double)nw));
    return rc;
}


int RXHarwellBoeingIO::SetRHS(vector<double> p_rhs){  // single RHS only

    double dum;
    int i;

    if(this->rhsval) delete []rhsval;
    this->rhsval=new double[p_rhs.size()];

    for(i=0; i<p_rhs.size();i++){
        dum=p_rhs[i];
        rhsval[i]= dum ;
    }
    m_nRhsVs = p_rhs.size()/this->nrow;
    return 0;
}

int RXHarwellBoeingIO::solveunsym(double *x)
{
    if (PDDBG)   printf ("\nSolve the assym matrix... \n");
    /* Matrix data. */
    MKL_INT n = this->ncol;

    MKL_INT *ia=this->colptr;
    MKL_INT *ja=this->rowind;
    double *a =  this->values;

    MKL_INT mtype = 11;       /* Real unsymmetric matrix */
    /* RHS and solution vectors. */
    double  bs[n], res, res0;
    double *b = this->rhsval; // assume a full vector
    MKL_INT nrhs = 1;     /* Number of right hand sides. */
    /* Internal solver memory pointer pt, */
    /* 32-bit: int pt[64]; 64-bit: long int pt[64] */
    /* or void *pt[64] should be OK on both architectures */
    void *pt[64];
    /* Pardiso control parameters. */
    MKL_INT iparm[64];
    MKL_INT maxfct, mnum, phase, error, msglvl;
    /* Auxiliary variables. */
    MKL_INT i, j;
    double ddum;          /* Double dummy */
    MKL_INT idum;         /* Integer dummy. */
    char *uplo;
    /* -------------------------------------------------------------------- */
    /* .. Setup Pardiso control parameters. */
    /* -------------------------------------------------------------------- */
    for ( i = 0; i < 64; i++ )
    {
        iparm[i] = 0;
    }
    iparm[0] = 1;         /* No solver default */
    iparm[1] = 2;         /* Fill-in reordering from METIS */
    iparm[3] = 0;         /* No iterative-direct algorithm */
    iparm[4] = 0;         /* No user fill-in reducing permutation */
    iparm[5] = 0;         /* Write solution into x */
    iparm[6] = 0;         /* Not in use */
    iparm[7] = 2;         /* Max numbers of iterative refinement steps */
    iparm[8] = 0;         /* Not in use */
    iparm[9] = 13;        /* Perturb the pivot elements with 1E-13 */
    iparm[10] = 1;        /* Use nonsymmetric permutation and scaling MPS */
    iparm[11] = 0;        /* Conjugate transposed/transpose solve */
    iparm[12] = 1;        /* Maximum weighted matching algorithm is switched-on (default for non-symmetric) */
    iparm[13] = 0;        /* Output: Number of perturbed pivots */
    iparm[14] = 0;        /* Not in use */
    iparm[15] = 0;        /* Not in use */
    iparm[16] = 0;        /* Not in use */
    iparm[17] = 0;       /* Output: Number of nonzeros in the factor LU */
    iparm[18] = 0;       /* Output: Mflops for LU factorization */
    iparm[19] = 0;        /* Output: Numbers of CG Iterations */
    maxfct = 1;           /* Maximum number of numerical factorizations. */
    mnum = 1;         /* Which factorization to use. */
    msglvl = 0;           /* Print statistical information in file */
    error = 0;            /* Initialize error flag */
    /* -------------------------------------------------------------------- */
    /* .. Initialize the internal solver memory pointer. This is only */
    /* necessary for the FIRST call of the PARDISO solver. */
    /* -------------------------------------------------------------------- */

    for ( i = 0; i < 64; i++ )
    {
        if (PDDBG)
            printf(" iparm[%d]   %d\n",i,iparm[i]);
        pt[i] = 0;
    }
    /* -------------------------------------------------------------------- */
    /* .. Reordering and Symbolic Factorization. This step also allocates */
    /* all memory that is necessary for the factorization. */
    /* -------------------------------------------------------------------- */
    phase = 11;
    PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
             &n, a, ia, ja, &idum, &nrhs, iparm, &msglvl, &ddum, &ddum, &error);
    /* Debugging: If we have stdout redirection
     * valgrind says
     * ==4958== Conditional jump or move depends on uninitialised value(s)
   ==4958==    at 0x6A718E5: mkl_pds_lp64_alloc_data (in /opt/intel/composer_xe_2015.0.040/mkl/lib/intel64/libmkl_core.so)
   ==4958==    by 0x6A85ED1: mkl_pds_lp64_pardiso_c (in /opt/intel/composer_xe_2015.0.040/mkl/lib/intel64/libmkl_core.so)
   ==4958==    by 0x6B5F241: mkl_pds_lp64_pardiso (in /opt/intel/composer_xe_2015.0.040/mkl/lib/intel64/libmkl_core.so)
   ==4958==    by 0x10654BD: RXHarwellBoeingIO::solveunsym(double*) (RXHarwellBoeingIO.cpp:400)
   ==4958==    by 0x104F81B: RXSparseMatrix::SolveOnce(double*) (RXSparseMatrix.cpp:176)
   ==4958==    by 0x9F49D8: RXMesh::Flatten(std::vector<RX_FESite*, std::allocator<RX_FESite*> >&) (RXMesh.cpp:441)
   ==4958==    by 0x1052B9F: RXShapeInterpolation::Flatten(double) (RXShapeInterpolation.cpp:187)
   ==4958==    by 0x1052482: RXShapeInterpolation::Prepare(double, int) (RXShapeInterpolation.cpp:161)
   ==4958==    by 0x9307D7: RXSail::MakeUVMapping_XYPlane() (RXSail.cpp:4148)
   ==4958==    by 0x929A99: RXSail::Mesh() (RXSail.cpp:3084)
   ==4958==    by 0x45A04F: Do_Mesh (script.cpp:3099)
   ==4958==    by 0x45A19C: Do_Mesh(char const*, RXSubWindowHelper*) (script.cpp:3106)
   ==4958==  Uninitialised value was created by a heap allocation
   ==4958==    at 0x4C2AB80: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
   ==4958==    by 0x5ECF694: mkl_serv_malloc (in /opt/intel/composer_xe_2015.0.040/mkl/lib/intel64/libmkl_core.so)
   ==4958==    by 0x6A87671: mkl_pds_lp64_pardiso_c (in /opt/intel/composer_xe_2015.0.040/mkl/lib/intel64/libmkl_core.so)
   ==4958==    by 0x6B5F241: mkl_pds_lp64_pardiso (in /opt/intel/composer_xe_2015.0.040/mkl/lib/intel64/libmkl_core.so)
   ==4958==    by 0x10654BD: RXHarwellBoeingIO::solveunsym(double*) (RXHarwellBoeingIO.cpp:400)
   ==4958==    by 0x104F81B: RXSparseMatrix::SolveOnce(double*) (RXSparseMatrix.cpp:176)
   ==4958==    by 0x9F49D8: RXMesh::Flatten(std::vector<RX_FESite*, std::allocator<RX_FESite*> >&) (RXMesh.cpp:441)
   ==4958==    by 0x1052B9F: RXShapeInterpolation::Flatten(double) (RXShapeInterpolation.cpp:187)
   ==4958==    by 0x1052482: RXShapeInterpolation::Prepare(double, int) (RXShapeInterpolation.cpp:161)
   ==4958==    by 0x9307D7: RXSail::MakeUVMapping_XYPlane() (RXSail.cpp:4148)
   ==4958==    by 0x929A99: RXSail::Mesh() (RXSail.cpp:3084)
   ==4958==    by 0x45A04F: Do_Mesh (script.cpp:3099)


   */
    if ( error != 0 )
    {
        printf ("\nERROR during symbolic factorization: %d", error);
        return (1);
    }
    if(PDDBG) printf ("\nReordering completed ... ");
    if(PDDBG) printf ("\nNumber of nonzeros in factors = %d", iparm[17]);
    if(PDDBG) printf ("\nNumber of factorization MFLOPS = %d", iparm[18]);
    /* -------------------------------------------------------------------- */
    /* .. Numerical factorization. */
    /* -------------------------------------------------------------------- */
    phase = 22;
    PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
             &n, a, ia, ja, &idum, &nrhs, iparm, &msglvl, &ddum, &ddum, &error);
    if ( error != 0 )
    {
        printf ("\nERROR during numerical factorization: %d", error);
        return (2);
    }
     if(PDDBG) printf ("\nFactoriZation completed ... ");
    /* -------------------------------------------------------------------- */
    /* .. Back substitution and iterative refinement. */
    /* -------------------------------------------------------------------- */
    phase = 33;
    if (PDDBG>1) {
        printf ("\nstart solution with: ");
        for ( j = 0; j < n; j++ )
        {
            printf ("b [%d] = %f , ( x [%d] = %f \n ", j, b[j],j,x[j]);
        }
        printf ("\n"); fflush(stdout);
    }
    //  Loop over 3 solving steps: Ax=b, AHx=b and ATx=b
    for ( i = 0; i < 1; i++ )
    {
        iparm[11] = i;        /* Conjugate transposed/transpose solve */
        if ( i == 0 )
            uplo = "non-transposed";
        else if ( i == 1 )
            uplo = "conjugate transposed";
        else
            uplo = "transposed";

        if (PDDBG>1) printf ("\n\nSolVing %s system...\n", uplo);
        PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
                 &n, a, ia, ja, &idum, &nrhs, iparm, &msglvl, b, x, &error);
        if ( error != 0 )
        {
            printf ("\nERROR during solution: %d", error);
            return (3);
        }
        if (PDDBG>1) {
            printf ("\nThe solution of the system is: ");
            for ( j = 0; j < n; j++ )
            {
                printf ("\n x [%d] = %f", j, x[j]);
            }
            printf ("\n");
        }
        // Compute residual
        mkl_dcsrgemv (uplo, &n, a, ia, ja, x, bs);
        res = 0.0;
        res0 = 0.0;
        for ( j = 1; j <= n; j++ )
        {
            res += (bs[j - 1] - b[j - 1]) * (bs[j - 1] - b[j - 1]);
            res0 += b[j - 1] * b[j - 1];
        }
        res = sqrt (res) / sqrt (res0);
        if (PDDBG>0) printf ("\nRelative residual = %e\n", res);
        // Check residual
        if ( res > 1e-10 )
        {
            printf ("Error: residual is too high!\n");
            return (10 + i);
        }
    }

    /* -------------------------------------------------------------------- */
    /* .. Termination and release of memory. */
    /* -------------------------------------------------------------------- */
    phase = -1;           /* Release internal memory. */
    PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
             &n, &ddum, ia, ja, &idum, &nrhs,
             iparm, &msglvl, &ddum, &ddum, &error);
    return 0;
}
int RXHarwellBoeingIO::SolveSym(double*b, double* p_x)// single RHS only (b).  result is x
{
    assert(sizeof(MKL_INT)==sizeof(int));
    /* Matrix data. */
    MKL_INT n = this->nrow; //8
    MKL_INT *ia =this->colptr; // the short one
    MKL_INT *ja = this->rowind ;  // the long one
    double *a = this->values;
    MKL_INT mtype = 11;
    std::cout<<"RXHarwellBoeingIO::SolveOnce  Is sensitive, Better use solveunsym"<<std::endl;

    //This scalar value defines the matrix type. The PARDISO solver supports the following matrices:
    //
    //= 1 real and structurally symmetric matrix
    //= 2 real and symmetric positive definite matrix
    //= -2 real and symmetric indefinite matrix
    //= 3 complex and structurally symmetric matrix
    //= 4 complex and Hermitian positive definite matrix
    //= -4 complex and Hermitian indefinite matrix
    //= 6 complex and symmetric matrix
    //= 11 real and unsymmetric matrix
    //= 13 complex and unsymmetric matrix

    /* Internal solver memory pointer pt, */
    /* 32-bit: int pt[64]; 64-bit: long int pt[64] */

    /* or void *pt[64] should be OK on both architectures */
    static void *pt[64];
    /* Pardiso control parameters. */
    MKL_INT iparm[64];
    MKL_INT maxfct, mnum, phase, error, msglvl;
    /* Auxiliary variables. */
    MKL_INT i;
    //   double ddum; /* Double dummy */
    MKL_INT idum[n]; /* Integer dummy. */ memset(idum,0,n*sizeof(MKL_INT));

    if(PDDBG) {
        cout<<"SolveOnce with "<<this->nrow<<"  rows.. first few numbers are"<<endl;
        cout<<"values "; for(i=0;i<10;i++) cout<< this->values[i]<<" ";  cout<<endl;
        cout<<"rowind "; for(i=0;i<10;i++) cout<< this->rowind [i]<<" ";  cout<<endl;
        cout<<"colptr "; for(i=0;i<10;i++) cout<< this->colptr[i]<<" ";  cout<<endl;
        this->Write("matrixtobesolved.rua");
    }
    /* -------------------------------------------------------------------- */
    /* .. Setup Pardiso control parameters. */
    /* -------------------------------------------------------------------- */
    for (i = 0; i < 64; i++)       iparm[i] = 0;
    //#define PARDISONODEFAULTS
#ifndef PARDISONODEFAULTS
    iparm[0] = 1; /* No solver default */
    // TRY 2  june 2014, was 0
    iparm[1] = 0; /*0= multiple minimum degree scheme  1=MMD reordering (undcumented) 2=Fill-in reordering from METIS 3==parallel nested dissection */
    // 1  was suggested by Sergey as a work-around to the hang-up, but its undocumented. (june 2014 lets try changing 1 to 2
    iparm[1] = 2;//  MKL PARDISO uses the instead of METIS

    /* Numbers of processors, value of OMP_NUM_THREADS */
#ifdef OMP
    iparm[2] = 1;// omp_get_num_procs() ;//1;
    cout<<"do we really want just one processor for the solver??"<<endl;
#else
#ifdef _DEBUG
    iparm[2] =1;
#else
    iparm[2] =1;
#endif
#endif
    iparm[2]= 0; // june 2014
    //s the  however the DOC says the number of procs comes ONLY from MKL_NUM_THREADS

    iparm[3] = 0; /* No iterative-direct algorithm */
    iparm[4] = 0; /* No user fill-in reducing permutation */
    iparm[5] = 0; /* Write solution into x */
    iparm[6] = 0; /* Not in use */
    iparm[7] = 2; /* Max numbers of iterative refinement steps */
    iparm[8] = 0; /* Not in use */
    /*10*/	iparm[9] = 13; /* Perturb the pivot elements with 1E-13 */
    iparm[10] = 1; /* Use nonsymmetric permutation and scaling MPS */
    iparm[11] = 0; /* Not in use */
    iparm[12] = 1; /* Maximum weighted matching algorithm is switched-off (default for symmetric). Try iparm[12] = 1 in case of inappropriate accuracy */
    // [12] used to be 0 . may 2011 we try 1 to see if it improves accuracy for our Meyer matrices.
    iparm[13] = 0; /* Output: Number of perturbed pivots */
    iparm[14] = 0; /* Not in use */
    iparm[15] = 0; /* Not in use */
    iparm[16] = 0; /* Not in use */
    iparm[17] = -1; /* Output: Number of nonzeros in the factor LU */
    iparm[18] = 0;//-1; /* Output: Mflops for LU factorization */
    /*20*/	iparm[19] = 0; /* Output: Numbers of CG Iterations */


    //peters
    iparm[26]=1 ;  //1= internal checking before run
#endif
    maxfct = 1; /* Maximum number of numerical factorizations. */
    mnum = 1; /* Which factorization to use. */
    msglvl = 0; /* Print statistical information in file */
    error = 0; /* Initialize error flag */

    // NOTE that iparm[34]  (35 in the doc)  =0 (default) means fortran indexing. Otherwise C indexing.
    /* -------------------------------------------------------------------- */
    /* .. Initialize the internal solver memory pointer. This is only */
    /* necessary for the FIRST call of the PARDISO solver. */
    /* -------------------------------------------------------------------- */

    for (i = 0; i < 64; i++)  {printf(" iparm %d  %d\n",i,iparm[i]); pt[i] = 0;}

    assert(m_nRhsVs>=1  );
    assert(mtype==11); // ddebug.  real unsymmetric
    if(PDDBG>2) {
        printf(" neqs = %d\n ia is\n",n);
        for(i=0;i<=n;i++) printf("%d %d\n",i,ia[i]);
        int jj = ia[n]-1;
        printf("\n there are %d values\n",jj);
        for(i=0;i<jj;i++) printf("%d  %d  %f\n",i,ja[i],a[i]);
        fflush(stdout);
    }


    /* -------------------------------------------------------------------- */
    /* .. Reordering and Symbolic Factorization. This step also allocates */
    /* all memory that is necessary for the factorization. */
    /* -------------------------------------------------------------------- */

    phase = 11;
    if(PDDBG) cout<<"enter PD"<<endl;
    PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
             &n, a, ia, ja, idum, &m_nRhsVs,
             iparm, &msglvl, b,p_x, &error);// was    iparm, &msglvl, &ddum, &ddum, &error);
    if (error != 0) {
        cout<<" during symbolic factorization: "<< error<<endl;
        return 1;
    }

    if(PDDBG) {
        cout<< "\nReordering completed ... "<<endl;
        cout<<"Number of nonzeros in factors ="<< iparm[17]<<endl;
        cout<<"Number of factorization MFLOPS = "<< iparm[18]<<endl;
    }
    /* -------------------------------------------------------------------- */
    /* .. Numerical factorization. */
    /* -------------------------------------------------------------------- */
    phase = 22;
    PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
             &n, a, ia, ja, idum, &m_nRhsVs,
             iparm, &msglvl, b, p_x, &error);// was    iparm, &msglvl, &ddum, &ddum, &error);
    if (error != 0) {
        cout<<"during numerical factorization: "<<error<<endl;
        return (2);
    }
    if(PDDBG) cout<< "Factorization completed ... "<<endl;
    /* -------------------------------------------------------------------- */
    /* .. Back substitution and iterative refinement. */
    /* -------------------------------------------------------------------- */
    phase = 33;
    iparm[7] = 2; // we've tried 10 /* Max numbers of iterative refinement steps. */
    /*  right hand side is b. */

    PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
             &n, a, ia, ja, idum, &m_nRhsVs,
             iparm, &msglvl, b, p_x, &error);
    if (error != 0) {
        cout<<"\nERROR during solution:  "<< error <<endl;
        return (3);
    }
    if(PDDBG>1)
    {
        cout<< "\nPD Solve completed ... \n i  B    X "<<endl;
        for (i=0;i<n;i++) cout<<i<<" "<<b[i]<<"  "<<p_x[i]<<endl;
    }

    /* -------------------------------------------------------------------- */
    /* .. Termination and release of memory. */
    /* -------------------------------------------------------------------- */
    phase = -1; /* Release internal memory. */
    PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
             &n, a, ia, ja, idum, &m_nRhsVs,
             iparm, &msglvl, b, p_x, &error);// was    iparm, &msglvl, &ddum, &ddum, &error);
    if(PDDBG)  cout<<" pardiso memory released"<<endl;
    return 0;
}

