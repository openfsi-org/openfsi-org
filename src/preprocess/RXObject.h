// RXObject.h: interface for the RXObject class.
//
//////////////////////////////////////////////////////////////////////

#ifndef _RXHDR_RXOBJECT__
#define  _RXHDR_RXOBJECT__

#ifdef RXEXP_DEBUG
#define RXODBG  (true)
#else
#define RXODBG  (false)
#endif

#include <iostream>
#include <map>
#include <set>
#include <QString>
#include "RXRelationDefs.h"
using namespace std;

#define	RXO_NOTYPE			-77

#ifdef __cplusplus

#define RXTAB(i) {for (int k=0;k<i;k++) cout<< " . ";}
EXTERN_C int dbgc;
#include "opennurbs.h"

class RelaxWorld;
// If you change or add to these values  you should make sure that RXOKey methods InvertType, AndType are consistent
enum  RXOKeyType {
    notype=0,
    parent =1,
    aunt=2,
    frog=4,
    child=8,
    niece=16,
    spawn=32
};
#define RXO_ALL 63
/*
My Niece gets calculated when I move - because its value depends on mine.
My Child gets unresolved when I'm deleted- because it points at me
My Spawn gets deleted when I'm deleted (March 2013 NO - when I am unresolved)

Note that Expressions and Entities have slightly different destruction.
For Entities  deletion is a two-stage process.
First Kill - which places it on garbage
then call the destructor.
*/

/** @brief  A helper class for RXObject;
 RXOKey is the key for a RXObject's list of pointers-to-objects.
  */

class RXOKey{
    friend  class RXObject;
public:
    RXOKey();
    inline RXOKey(const int what,const  int index=RXO_ANYINDEX,const  int type=notype);
    inline virtual ~RXOKey();
    inline bool operator < (const RXOKey & p_Obj) const;
private:
    int what; // might be  'Node1'  or 'basecurveptr' or  'pslist'
    int index;// equivalent to (eg) the  index of a psidelist.
    int type; // parent|aunt|spawn
    static int InvertType(const int t  );
    static int AndType (const int p1, const int p2  );
};
typedef pair<RXOKey,class RXObject*>		orectype;
typedef multimap<RXOKey,class RXObject*>	olist_t;
typedef olist_t::iterator					obiter;
typedef olist_t::const_iterator			obciter;

/** @brief Base class for RXEntity and RXExpression

  This class manages the relations between objects.
  All objects are(will be eventually) derived from RXObject, which has a static member ptr to RelaxWorld,
  and a flag like layerobject.
  It implements  pointers between objects and the parent/child functionality.
  In fact it only contains a name and a map of objects it points to, with the map's associated control flags.

  It has a constructor taking RelaxWorld* argument

  RX expressions and RX entities are derived from it.

  Whenever we iterate the Omap we have to protect it from being changed elsewhere. That includes places like printOmap
  */
#define RXO_DBG	31
#define RX_AUTOINCREMENT (INT_MAX-1)
class RXObject
{
public:
    RXObject();
    RXObject(const RXObject & p_Obj);
    virtual ~RXObject();

    RXSTRING GetOName() const;
    QString GetQName() const { return QString::fromStdWString( GetOName() );}
    void SetOName(const RXSTRING& p_Name);
    int PrintOMap(FILE *fp,const QString &space="    ",const int what = RXO_ALL, const bool recur=false)const ;
virtual  int PrintOMapAsGraph(wostream &s, const int what = RXO_ALL, const bool recur=false) ;

    int SetRelationOf(RXObject*,const int type,const int what,const int index=0);
    RXObject* GetOneRelativeByIndex( const int what,const int index=0) const;
    RXObject* GetOneRelativeByLien(  const int what,const int type) const;
    bool FindInMap(RXObject*o, const int type)const;
    set<RXObject*> FindInMap(const int what, const int index,const int type=RXO_NOTYPE)const;
    RXSTRING  GetExpressionText(const int what, const int index,const int type=RXO_NOTYPE)const;
    int OnValueChange();
    size_t OMapN()const { return m_omap.size();}

    int NeedsComputing()const{return  m_NeedsComputing;}
    virtual void SetNeedsComputing(const int i=1);
    bool IsBeingKilled()const{return m_ObeingKilled ;}
    bool IsBeingIterated()const{return m_OBeingIterated ;}
    int UnHookCompletely(class RXObject*p);
    int UnHook(class RXObject*o, const int what);
    virtual int Kill()=0;
    virtual int Recursive_Solve(const int depth , const class RXSail *cSail);
    void SetInStack(int k){In_Stack=k;}
    int IsInStack() const { return In_Stack;}
protected:
    virtual int OnDelete();
    virtual int CClear();
    virtual int Compute()=0;
    virtual int Resolve()=0;
    virtual int Finish()=0;
    virtual int UnResolve();
    virtual QString Descriptor()const=0;

private:
    int OnTopologyChange();
    int RemoveFromMap(class RXObject*o );
    void Init();
    RXObject& operator = (const RXObject & p_Obj);
    int UnHook(const obiter &it);
    int __SetRelation(RXObject*,const int type,const int what,const int index);  // internal

    RXSTRING m_Name;
    bool m_listhaschanged; int m_olistChangeCount;
    olist_t m_omap;

#ifndef NDEBUG
    short int m_dbgval;
#endif
    bool m_OBeingIterated;
    bool m_ObeingKilled;
    bool In_Stack;
 protected:
    short int m_NeedsComputing; // only ever read as boolean, but recursive Solve used to set value 2

    int m_ComputeCount;
};

#endif //#ifdef __cplusplus
#endif

