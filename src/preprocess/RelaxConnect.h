#ifndef _RELAXCONNECT_H_
#define _RELAXCONNECT_H_

#include "Graphic Struct.h"
#include "opennurbs.h"

EXTERN_C int RX_HNet_ConnectGraphic(const Graphic* g);
EXTERN_C int RX_HNet_DisConnectGraphic(const Graphic* g);
EXTERN_C int RX_HNet_StartHnet(const Graphic* g);
EXTERN_C int RX_HNet_HnetMsg(const char *line);
#endif
