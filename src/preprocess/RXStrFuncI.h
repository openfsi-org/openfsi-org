#pragma once
#include <iostream>
#include <assert.h>
using namespace std;

#ifndef RXSTRING
	#include <string>
	#define RXSTRING std::wstring
#endif

#include "MTParserPublic.h"
#include "MTParserPrivate.h"
#include "MTParserCompiler.h"
#include "MTTools.h"
class RXStrFuncI :
	public MTFunctionI
{
public:
	RXStrFuncI(void); // dont use
	RXStrFuncI(class RXSail *const p, class RXObject *const o);
	~RXStrFuncI(void);
	virtual MTDOUBLE evaluate(unsigned int nbArgs, const MTDOUBLE *pArg){ assert(0);
		cout << "shouldnt be here in virtual RXStrFuncI::evaluate(unsigned int nbArgs, const MTDOUBLE *pArg) "  << endl;
		return MTTools::generateNaN();
	}
//		virtual MTDOUBLE evaluate(const char*s,unsigned int nbArgs, const MTDOUBLE *pArg) = 0;
	virtual MTDOUBLE evaluate(const MTSTRING*s) = 0;
	virtual class MTCompilerStateI* getCompilerState();
	protected:
	class RXSail *m_l;
	class MTCompilerStateI* m_pCompilerState;
	class RXObject * m_rxo; 
};

class MTRXStrCompilerState :
	public MTCompilerDefState
{ 
public:
	void onOpenBracket();
	void onCloseBracket();
}; //MTRXStrCompilerState


class RXDBFnc :
	public  RXStrFuncI
{
public:
	RXDBFnc(void);
	RXDBFnc(class RXSail *const p, class RXObject *const o);
	~RXDBFnc(void);

	virtual MTDOUBLE evaluate(const MTSTRING*s) ;
	virtual const MTCHAR* getSymbol(){return _T("db"); }

	virtual const MTCHAR* getHelpString(){ return _T("db(string)"); }
	virtual const MTCHAR* getDescription(){ return _T("look up a value"); }	
	virtual int getNbArgs(){ return 1; }
	
	virtual MTFunctionI* spawn() { return new RXDBFnc(); }
};

class RXMTStringVariable : public MTVariableI
{
public:
	RXMTStringVariable();
	virtual ~RXMTStringVariable();
	virtual const MTCHAR* getSymbol();   
	virtual MTDOUBLE evaluate(unsigned int nbArgs,const MTDOUBLE *pArg);
	virtual MTSTRING evaluate();
	virtual MTVariableI* spawn()    throw(MTParserException);
	void SetExpression( 	class RXExpressionI* e)   {  m_exp =e ; }
	class RXExpressionI*  GetExpressionPtr(void)const { return  m_exp;}

	void SetMTIVName(const RXSTRING &s) {m_MTIVname =s ; }
	RXSTRING GetMTIVName(void) const {return m_MTIVname; }
	class RXEntity * m_ent;
protected:
	class RXExpressionI* m_exp;

private:
	RXSTRING m_MTIVname;
};
