#ifndef _RXDATABASEI_HDR__
#define  _RXDATABASEI_HDR__
#include <string>
#include <map>
#include <QString>
#include <QMutex>

enum  RXDBTYPE {
    RXDB_NONE   =0,
    RXDB_INPUT  =1,
    RXDB_OUTPUT =2
    //    RXDB_MIRROR =4
};

//#define RXMIRRORTABLE  (qPrintable ("mirror_"+QString::number( QCoreApplication::applicationPid ())))
#define RXMIRRORTABLE  "`mirror`"

#define MIRRORPTR  class RXMirrorDBI*
#define DBRESULT   std::map <int , std::map <int ,std::string > >

class RXDatabaseCommon {
public:
    virtual ~RXDatabaseCommon(void) {}
    void SetName(const QString s)  {m_name=s;}
protected:
    QString m_name;
};

/// class RXMirrorDBI only handles the Mirror DB
class RXMirrorDBI
{
public:
    RXMirrorDBI(void) {}
    virtual ~RXMirrorDBI(void) {}
public:
    /////////////////////////////////////////////////////////
    // operations common to all types

    bool IsMirror(){return true;}
    virtual int Open(const char*p_filename,const char*what )=0;
    virtual int Close ()=0; // the full clear
    virtual int StartTransaction ()=0;
    virtual int Commit()=0;
    // script-command generates the macro line to reproduce this connection.
    virtual std::string ScriptCommand()const =0;
    // returns a human-readable identifier of the DB
    virtual  std::string GetFileName()const=0;
    virtual void SetLast(const QString h, const QString v)=0;

protected:
    int OutputToClient(const char *s, const int level) const { return 0;}// g_World-> OutputToClient(s, level);}
    int OutputToClient(const std::string &s, const int level)const { return 0;}//g_World-> OutputToClient(s, level);}

public:
    ///////////////////////////////////////////////////////////
    // operations on mirror only
    virtual int FlushMirrorTable ()=0; //  deletes the m_List

    // low-level work-horse insert and/or update the value of a mirror  item
    virtual int post_summary_value(const char*label,const char*value)=0;
    virtual int post_summary_value( const QString &label,const QString &value) {
        return post_summary_value(qPrintable(label ),qPrintable(value));
    }

    virtual int post_summary_value(const std::string &label,const std::string &value)=0;
    virtual int PostFile( const QString &label,const QString &fname)=0;

    //searches in the mirror for 'label'. returns value.
    virtual int Extract_One_Text(const char *label,char*value,const int buflength)=0;

    virtual std::string Extract_Headers(const std::string &what)=0; //is 'input', 'output' or 'all'
    virtual int Replace_Words_From_Summary(char **strptr, const int append)=0;
    //remove-all-with-prefix operates on the mirror
    virtual int RemoveAllWithPrefix(const std::string & prefix) =0;

    //  synch the app to the Mirror
    virtual int  Apply_Summary_Values()=0 ;

    int Write_MirrorDB_As_CSV(char *filename) ; // only used for the statefile header

    // create-filtered-  posts from a mirror to a mirror   - called in write_ND_state
    virtual int Create_Filtered_Summary(MIRRORPTR  snew,char *filter)=0;
    static int Summary_Type(const char* label,int*flag);
    // dump mirror to script format
    virtual int Write_Script (FILE *fp)=0;

}; // class RXMirrorDBI

class RXDataBaseLogI   ///a database table - in or out , NO Mirror

{
public:
    RXDataBaseLogI(void):m_DBtype(0) {}
    virtual ~RXDataBaseLogI(void){}
public:
    /////////////////////////////////////////////////////////
    // operations common to all types

    virtual int Open(const char*filename,const char*what)=0;
    virtual int Close ()=0; // the full clear

    // script-command generates the macro line to reproduce this connection.
    virtual std::string ScriptCommand()const =0;
    // returns a human-readable identifier of the DB
    virtual  std::string GetFileName()const=0;
    void SetType(const int i){m_DBtype=i;}

public:

    // operations on the persistent DB
    //extract-column returns a list of values for  header = head, for all record rows
    virtual int Extract_Column(const char*fname,const char*head,char***clist,int*Nn)=0;

    // transfer from mirror to persistent
    virtual int Write_Line (MIRRORPTR mirrorSource)=0;
    // transfer from  persistent DB to  Mirror DB
    virtual int read_and_post_one_row(const int row, const QString runName,MIRRORPTR mirrorDest)=0 ;
    virtual int MakeUnique(const QString &header, QString &value) =0;
protected:
    int m_DBtype;
}; // class RXDataBaseLogI

#endif

