//RXOffset.cpp implementation of the class RXOffset
//Thomas Ricard 05 07 04

#include "StdAfx.h"
#include "RXSRelSite.h"
#include "RXSeamcurve.h"
#include "RXCurve.h"
#include "RXSail.h"

#include "MTParserRegistrar.h"
#include "RXOffset.h"


RXOffset::RXOffset():
	m_PerCent	(RXO_NONE),
	m_O_Flag	(RXO_NONE),
	m_sc    	(0)
{
	cout<< "RXOffset default constructor"<<endl;
 
}//RXOffset::RXOffset

RXOffset::RXOffset(RXSTRING  name,RXSTRING exp, sc_ptr  sc,  class RXSail *const p_sail):
RXExpression(name,exp ,p_sail),
	m_PerCent	(RXO_NONE),
	m_O_Flag	(RXO_NONE),
	m_sc    	(0)
{
	   this->m_OwnerNode= p_sail;
		Fill(exp);  
		m_sc = sc;

 /* leave it for the next call to Evaluate to transcribe */

}
int RXOffset::Fill(const RXSTRING &newexp){
	RXSTRING temp(newexp);
	this->m_PerCent =RXO_NONE;
// we  replace '%' by an unlikely name like _a_a_ and then defineVar it at evaluation time. 

	size_t found=temp.find(L"%");

	while( found!=wstring::npos){// wcout << "offset has a percent "<< newexp<<endl;
		 this->m_PerCent=RXO_IS_PERCENT;
		 temp.replace (found,1,RXSTRING(L"*( _a_a_)")); 
		found=temp.find(L"%");
	}	
	int rc=0;
	double x=1.0;
	try {
		rc= RXExpression::Fill (temp);
	}
	catch(...) {wcout <<L"can't compile  "<<newexp <<endl;}
	return rc;
}

int RXOffset::Change(const RXSTRING &newexp){
    RXSTRING temp(newexp);
    this->m_PerCent =RXO_NONE;
    // we  replace '%' by an unlikely name like _a_a_ and then defineVar it at evaluation time.

    size_t found=temp.find(L"%");

    while( found!=wstring::npos){// wcout << "offset has a percent "<< newexp<<endl;
        this->m_PerCent=RXO_IS_PERCENT;
        temp.replace (found,1,RXSTRING(L"*( _a_a_)"));
        found=temp.find(L"%");
    }
    int rc=0;
    double x=1.0;
    try {
        rc= RXExpression::Change (temp);
    }
    catch(...) {wcout <<L"can't compile  "<<newexp <<endl;}
    return rc;
}

//Copy constructor
RXOffset::RXOffset(const RXOffset & p_Obj)
{
    Init();
    this->operator =(p_Obj);
} 

RXOffset::~RXOffset()
{
    ClearO();
}//RXOffset::~RXOffset

void RXOffset::Init()
//meme alloc + init val to 0 0 0
{
    m_PerCent	=	RXO_NONE;
    m_O_Flag	=	RXO_NONE;
    m_sc		= NULL;
}//void RXOffset::Init

int RXOffset::ClearO()
{
    m_Text.erase(); // shouldnt this be in a RXExpression::Clear method???
    m_PerCent	=	RXO_NONE;
    m_O_Flag	=	RXO_NONE;
    m_sc = NULL;
    return(1);
}//int RXOffset::ClearO

RXOffset& RXOffset::operator = (const RXOffset & in)
{
    //Make a copy
    Init();

    Set_Offstring(in.GetText());

    Set_Percent(in.m_PerCent);
    m_O_Flag = in.m_O_Flag;
    assert (m_O_Flag>=0);

    m_sc = in.m_sc;

    return *this;
}

int RXOffset::Set_Offstring(char * p_str)
{
    assert(p_str);
    m_Text = TOSTRING(p_str); m_InputText=m_Text;
    return 1;  /* from the script */

}//int RXOffset::Set_Offstring

int RXOffset::Set_Offstring(const RXSTRING & p_str)
{
    m_Text = p_str; m_InputText=p_str;
    return 1;

}//int RXOffset::Set_Offstring

RXSTRING RXOffset::GetText() const // returns the printable version??InputText??
{
    assert(m_Text.size());

    if(this->Is_Percent()) {
        RXSTRING t = m_Text;
        size_t found = t.find(_T("*( _a_a_)"));
        while (found!=string::npos){
            t.replace (found,9,_T("%"));
            found = t.find(_T("*( _a_a_)"));
        }
        return t;
    }

    return m_Text;
}//int RXOffset::GetText


void RXOffset::Set_Percent(const int & p_Percent)
{
    m_PerCent = p_Percent;
    assert(m_PerCent== RXO_NONE || m_PerCent == RXO_IS_PERCENT|| m_PerCent ==RXO_IS_PARAM );
}//void RXOffset::Set_Percent

int RXOffset::Is_Percent()const {	return(m_PerCent==RXO_IS_PERCENT);}

int RXOffset::Is_Parameter(){return(m_PerCent==RXO_IS_PARAM);}


double RXOffset::EvaluateBySpace(sc_ptr scin,const RX_COORDSPACE space){

    switch (space) {
    case RX_BLACKSPACE:
        return this->Evaluate(scin,1);
    case RX_REDSPACE:
#ifdef DEBUG
    {
        double a0=ON_UNSET_VALUE,a2=ON_UNSET_VALUE;
        if(scin->m_pC[0]->IsValid()) a0= this->Evaluate(scin,0);
        if(scin->m_pC[2]->IsValid()) a2= this->Evaluate(scin,2);
        if(ON_IsValid(a0) && ON_IsValid(a2)){
            if(2.*(a0-a2)/(a0+a2) > 0.001)
                printf("(%s ) arc0=%f  arc2=%f, diff=%f\n", scin->name(),a0,a2,a0-a2);
        }
    }
#endif

    if(scin->m_pC[0]->IsValid()) return this->Evaluate(scin,0);
    if(scin->m_pC[2]->IsValid()) return this->Evaluate(scin,2);
    default:
        assert(0);
    };

    return -1.;
}

double RXOffset::evaluate() 
{
    return Evaluate(m_sc,1);
}
double RXOffset::Evaluate(sc_ptr  scin, int which) 
{
    double x;
    sc_ptr  sc;
    RXSTRING errstr;
    MTDOUBLE  arcBy100B;
    if( Is_Percent())
    {
        if(scin) sc=scin; else sc=m_sc;
        arcBy100B = sc->GetArc(which)/100.0e00;
        if(arcBy100B <0.00000001)
            if(sc->Needs_Finishing) {
                cout<<" cant get percent offset on short unFinished SC "<< sc->name()<<endl;
                return 0;
            }


        if(this->getRegistrar()->isVarDefined(_T("_a_a_") ))
            this->undefineVar(_T("_a_a_"));
        this->defineVar(_T("_a_a_"),&arcBy100B); this->m_needscompiling=true;
        x= RXExpression::evaluate() ;

        if(arcBy100B < FLOAT_TOLERANCE/100) // shouldnt happen.
        {
            errstr=RXSTRING(_T(" trying to EvOf on short side ")) + TOSTRING(which) +RXSTRING(L" of '")
                    + RXSTRING( FromUtf8(sc->type())) + RXSTRING(_T(" , ")) +RXSTRING(FromUtf8( sc->name()));
            sc->Print_One_Entity(stdout);

            sc->OutputToClient(errstr,2);
            MTDOUBLE  arcBy100 = sc->GetArc(1)/100.0;
            this->defineVar(L"_a_a_",&arcBy100);
            this->undefineVar(_T("_a_a_"));	this->defineVar(_T("_a_a_"),&arcBy100); this->m_needscompiling=true;
            x= RXExpression::evaluate() ;

        }
        return(x);
    }//if( o->Percent)

    x = RXExpression::evaluate() ;

    return(x);
}//double Evaluate 



int RXOffset::Print( FILE *fp)
{
    fprintf(fp,"     OFFSET  \t");
    fprintf(fp,"     string  %S \t",m_Text.c_str());
    fprintf(fp,"inputstring  %S \t",m_InputText.c_str());
    fprintf(fp,"     percent %d \t",m_PerCent);
    fprintf(fp,"     flag    %d \t",m_O_Flag);
    if(m_sc)
        fprintf(fp,"     sc     %s \n",m_sc->name());
    else
        fprintf(fp,"     SC NULL \n");

    return(1);
}
int PCO_Free_Offset_Record(void* whoami,RXOffset **o){
    assert("PCO_FOR"==0);
    return 0;
}

