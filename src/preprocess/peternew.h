#pragma once
#include <list>
#include "RXOffset.h"

class RXPosOnCurve {
public:
	RXPosOnCurve(const class RXOffset &o1,const class RXOffset &o2,const RXEntity_p ptr, const int si);
	RXPosOnCurve();

public:
	RXOffset  m_off1; 
	RXOffset  m_off2;   
	RXEntity_p ptr;
	int side;
}; //class RXPosOnCurve

EXTERN_C int Put_Pside_Mats_Safe(sc_ptr np, std::list <class RXPosOnCurve> &mats);
EXTERN_C int Restore_Pside_Mats (sc_ptr np, std::list <class RXPosOnCurve> &mats);



