/* prototypes for doview.c   */

#ifndef DOVIEW_16NOV04
#define DOVIEW_16NOV04

EXTERN_C int DoViewUp(const char *objName,HC_KEY hoops_data_key);
EXTERN_C int ViewOneUp(char *camerabase,const char *objName);
//EXTERN_C  int Close_All_Options(void); this is is in drop.h

#error(" dont use  doview.h")
#endif //#ifndef DOVIEW_16NOV04
