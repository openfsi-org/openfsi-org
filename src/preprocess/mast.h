
#ifndef _HDR_MAST_
#define _HDR_MAST_ 1

#include "controlmenu.h"
#include "trimmenu.h"

#define MAX_SPLINE_POINTS 100  /* should be consistent with trimmenu.h */

#define SIDE 1
#define AFT 0


EXTERN_C int set_current_camera(char *seg, HC_KEY k); //   for monarch

//EXTERN_C int find_all_mast_splines(int *mast_count, MastData **mast_list);

#endif  //#ifndef _HDR_MAST_

