#include "StdAfx.h"
#include <QDebug>
#include "RXSail.h"
#include <iostream>
using namespace std;
#if !defined(RXRHINOPLUGIN) && !defined(RXPHYSX) 
#include "RXEntityDefault.h"
#include "RX_FESite.h"
#include "RX_FEString.h"
#include "RXSeamcurve.h"
#include "TensylLink.h"
#include "words.h"
#include "etypes.h"
#include "entities.h"
#include "stringutils.h"
#include "akmutil.h"
#include "resolve.h"
#include "finish.h"
#include "f90_to_c.h"
#else
#ifndef linux
#include "stringutils.h"
#include "akmutil.h"
#else
#include "../../stringtools/stringtools.h"

#endif
#endif
#ifdef HOOPS
#include "HUtilityLocaleString.h"
#endif

#include "RX_PCFile.h"
#define PCF_DELTA (0.00001)
double PCFILE_NODETOL =0.2 ; // inches  g_Gauss_Cut_Dist*39.37 *2.5

#define BUFFER_SIZE 512
/*
January 2011 the logic of changing between length-controlled ('type EA') and tension-controlled. ('type TI')
m_slacklength is always the value read from the PC file. 
It is in inches or pounds. depending on the value of m_Control. 1 for EA and 0 for TI.

On loading a model...
 we create the CTensylLink and call cf_setflinklength with m_slacklength converted to Metres.
 That sets flink%m_Zlink0

 we call   CTensylLink::MakeElementProperties which..
If EA,  returns:
  EA from its material type, converted to SI.
  ZI which is m_slacklength converted to SI (!)
  TO which is zero

If TI  returns:
  EA zero
  ZI which is the distance between undeflected coordinates.
  TI which is  m_slacklength converted to SI (!)
 then we do a cf_insert and a cf_setlength with these values.

 On refreshing, we may have changed the property (EA) or the links m_Slacklength or m_Control

 NO TYPE CHANGE...
 If we have just changed the length of a EA type
  its OK to call cf_setflinklength with m_slacklength converted to Metres.
  but we should also set EA. It may have changed.

 If we have changed m_slacklength of a TI type, we just want to change flink%ti.

TYPE CHANGE EA to TI
  we want to zero flink's EA, set %TI to the new value
TYPE CHANGE TI to EA
 here we want to set zlink0 to (zt - tq/newEA)
  and m_slacklength to the equivalent in inches
  and flink%tI to 0, %EA=> new value,


*/


/* Ian writes
Fixity:

//3--z 
Means the point is fixed in the z direction and can only move in a plane perpendicular to Z.

//4--xy 
Means the point is fixed in the x and y directions and can only move in the z direction.

Elastic Control:

That relationship is true.
The prestress values IS the slack length specified for the link.

Model Units:
All length units are inches, because that's what we were modelling in Rhino.
Forces should be pounds force.
Mass per length units should be pounds per inch.
We are doing all of our projects with these assumptions. Units will be the same for all.
 */


RX_PCFile::RX_PCFile(void)
    : m_HangerFac(0.53)
{
    Init();
    m_owner=0;
    PCFILE_NODETOL =0.2; // the default value
}

RX_PCFile::~RX_PCFile(void)
{
    // I think these dont need freeing
    m_props.clear();
    m_nodes.clear();
    m_links.clear();
    m_restraints.clear();
    m_masters.clear();
}
int RX_PCFile::SpecialAdjustZI(){
    /*if the prop is 4 and  n divides by 4,
 we have a L(odd)
we swap the ZI of N <> N-3
 and  n-1  <> n-2 */
    int n; double dum,delta= 0.0; int c=0;

    map<int, CTensylLink >  ::iterator it;
    for (it = m_links.begin(); it != (m_links).end(); ++it) {
        CTensylLink &n0 = it->second;
        n=n0.m_nn;
        if(n ==0)
            continue;
        if(n0.m_prop!=4 ) continue;
        div_t d = div(n,4);
        if(d.rem !=0) continue;
        CTensylLink &n3= m_links[n-3]; assert(n3.m_nn==n-3);
        CTensylLink &n2= m_links[n-2];assert(n2.m_nn==n-2);
        CTensylLink &n1= m_links[n-1];assert(n1.m_nn==n-1);
        // swap N and N-3
        dum = n0.m_Slacklength;
        n0.m_Slacklength =n3.m_Slacklength+delta;
        n3.m_Slacklength =dum+delta;
        //swpa N-1 and N-2;
        dum = n1.m_Slacklength;
        n1.m_Slacklength =n2.m_Slacklength+delta;
        n2.m_Slacklength =dum+delta;
    }
    cout << "SpecialAdjustZI:: "<<c <<" swap and short "<<delta<<" inch"<<endl;
    return c;
}


int RX_PCFile::Print(FILE *fp,const double p_scale){
    fprintf(fp,"<TITLE>\n%s\nWritten by RXPCFile::Print\n</TITLE>\n"
            "// First line is number of Nodes\n"
            "//Then BHNodeNo, X, Y, Z, Boundary, Field, Fixity, AxSym, (atts)\n",m_titlestring.Array());
    cout<<"start PCFile print "<<endl;
    int nn =(int) this->m_nodes.size ();
    fprintf(fp,"<NODES>\n%d\n\n",nn);
    map<int, CTensylNode >  ::iterator pos;
    for (pos = (m_nodes).begin(); pos != (m_nodes).end(); ++pos) {
        CTensylNode &n = pos->second;
        //		if(n.m_nn ==0)
        //			continue;
        n.Print(fp);
    }
    fprintf(fp,"</NODES>\n\n");
    fprintf(fp,"// First line is number of CProps\n"
            "//  Then Prop_No, EA, SelfWeight, RenderSize\n"
            "//       Type Description\n"
            "//       Size Description\n"
            "//  For each property\n"
            "<CPROPS>\n");
    //nprops\n\n then the props
    nn=(int) m_props.size(); 	fprintf(fp,"   %d\n\n",nn);

    {map<int,CTensylProp>  ::iterator it;
        for (it = m_props.begin(); it != (m_props).end(); ++it) {
            CTensylProp &n = it->second;
            //if(n.m_nn ==0)
            //	continue;
            n.Print(fp,p_scale);
        }}
    fprintf(fp,"</CPROPS>\n\n"
            "// First line is number of Links\n"
            "//Number, StartNode, EndNode, SlackLength, PropNo, Control, Boundary, Field, row, col\n"
            "<LINKS>\n");
    nn=(int) m_links.size(); 	fprintf(fp,"   %d\n\n",nn);

    map<int, CTensylLink >  ::iterator it;
    for (it = m_links.begin(); it != (m_links).end(); ++it) {
        CTensylLink &n = it->second;
        if(n.m_nn ==0)
            continue;
        fprintf(fp,"%d %d %d %17.12f %d %d %d %d %d %d\n",
                n.m_nn,n.GetStartNode(),n.GetDestNode()   ,n.m_Slacklength,n.m_prop,
                n.m_Control,n.m_Boundary,n.m_FieldForLink,n.m_Row,n.m_Col );
    }
    fprintf(fp,"</LINKS>\n\n" );

    fprintf(fp,	"//  Link Number, Tension, zt\n"
            "<TENSIONS>\n");
    //	nn=(int) m_links.size();

    //	map<int, CTensylLink >  ::iterator it;
    for (it = m_links.begin(); it != (m_links).end(); ++it) {
        CTensylLink &n = it->second;
        if(n.m_nn ==0)
            continue;
        double zt = m_nodes[n.GetStartNode()].m_pt.DistanceTo(m_nodes[n.GetDestNode()].m_pt  );

#ifndef FORTRANLINKED
        double tension = 0;
#else
        double tension = cf_getflinktension(m_owner->Esail->SailListIndex (),n.m_fnn);
#endif

        fprintf(fp,"%d  %17.12f  %17.12f\n",n.m_nn,tension ,zt);

    }

    fprintf(fp,"</TENSIONS>\n\n"


            "//  The following End-Of-File marker must be present\n"
            "</EOF>");
    return 0;
}
int RX_PCFile::comparebypos(const CTensylNode*a,const CTensylNode*b){


    if( b->m_pt.x < a->m_pt.x-PCFILE_NODETOL) return 1;
    if( b->m_pt.x > a->m_pt.x+PCFILE_NODETOL) return -1;

    if( b->m_pt.y < a->m_pt.y-PCFILE_NODETOL) return 1;
    if( b->m_pt.y > a->m_pt.y+PCFILE_NODETOL) return -1;

    if( b->m_pt.z < a->m_pt.z-PCFILE_NODETOL) return 1;
    if( b->m_pt.z > a->m_pt.z+PCFILE_NODETOL) return -1;

    return 0;
}
int RX_PCFile::ShimmerDuplicates(const double tol){
    int c=0;
    /*
The sequence is:
Requires cut tol 1mm and gauss tol 0.5mm
1) Read all the sites and all the links
2) Trawl for short links 
Where a link is shorter than 0.1", replace it by a Master.
3) Now sort the nodes by (xyz)  Where a pair are close, we shimmer the second one
*/
    size_t originalsize=m_nodes.size();
    cout<< "RX_PCFile::ShimmerDuplicates start"<<endl;
    PCFILE_NODETOL =tol; // inches  g_Gauss_Cut_Dist*39.37 *2.5

    ON_ClassArray<CTensylNode> v;
    // first walk thru the map of nodes, set their restraints and their loads
    do{
        c=0;
        v.Empty();
        v.SetCapacity ((int)m_nodes.size ());
        map<int, CTensylNode >  ::iterator pos;
        for (pos = (m_nodes).begin(); pos != (m_nodes).end(); ++pos) {
            CTensylNode &n = pos->second;
            v.Append(n);
        }
        v.HeapSort( comparebypos ); // or QuickSort

        for(int i=1;i<v.Count();i++) {
            if(comparebypos(v.At(i-1),v.At(i))==0) {
                printf(" close nodes %d  %d ...", v.At(i-1)->m_nn,v.At(i)->m_nn);
                if(0) {
                    m_masters[v.At(i)->m_nn]= v.At(i-1)->m_nn;
                    cout<<"deleting "<< v.At(i)->m_nn<<endl;
                    m_nodes.erase(v.At(i)->m_nn);
                    v.Remove(i); i--; c++;

                }
                else {

                    CTensylNode&qq = m_nodes[v.At(i)->m_nn];
                    if(qq.m_fixity) qq= m_nodes[v.At(i-1)->m_nn];
                    if(!qq.m_fixity) {
                        qq.m_pt.x += (double)rand()/((double) RAND_MAX) *5.;
                        qq.m_pt.y += (double)rand()/((double) RAND_MAX) *5.;
                        qq.m_pt.z += (double)rand()/((double) RAND_MAX) *5.;
                        cout<<"shimmer node "<<  qq.m_nn<<endl; c++;
                    }
                    else
                        cout<< "skip shimmer. Both are fixed"<<endl;

                }
            }
        }
    }while(c);
    cout<< " node list size was "<<originalsize  <<" is "<<  m_nodes.size() <<endl;
    cout<< "RX_PCFile::ShimmerDuplicates end"<<endl;
    return c;
}
int RX_PCFile::MasterNodeNo(const int i){
    // if i is an index in m_masters return the second value, else return i
    int j;
    map<int, int >::iterator it = m_masters.find(i);
    if(it!=m_masters.end()){
        j=it->second;
        return(j);
    }
    return i;
}

ON_3dPoint RX_PCFile::GetNodePoint(const int i){
    assert(m_nodes[i].m_nn>0);
    return  m_nodes[i].m_pt    ;
}
CTensylNode& RX_PCFile::GetTNode(const int i) {
    assert(m_nodes[i].m_nn>0);
    return  m_nodes[i]   ;
}
CTensylProp& RX_PCFile::GetTProp(const int i) {
    assert(m_props[i].m_prop_no>0);
    return  m_props[i]   ;
}

#if !defined(RXRHINOPLUGIN) && !defined(RXPHYSX) 

const double l_sf = ON::UnitScale(ON::inches,ON::meters);
int RX_PCFile::RefreshNodes(FILE *fp, const int nn)  // returns change count
{
    int nodecnt=0;
    /*// Then Node Number, X, Y, Z, Boundary, Field, Fixity, AxSym
<NODES>
 10681
 9 -8317.46837313235 -8325.55940338391 492.074000620648 0 0 7 0 */
    int i,k ,nnum,bdy, field, fixity, AxSym;
    double xx,yy,zz;
    for(i=0;i<nn;i++) {
        k=fscanf(fp,"%d  %lf  %lf %lf %d %d %d %d", &nnum, &xx,&yy,&zz,&bdy,&field,&fixity,&AxSym);
        ON_3dPoint pt(xx,yy,zz);  // inches
        CTensylNode &q = m_nodes[nnum];
        if(q.m_nn>0 && fixity==7) {
            q.m_pt=pt;
            RXSRelSite *e = q.m_e;
            RX_FESite *s = dynamic_cast<RX_FESite*> (e);
            pt=pt*l_sf;// pt now metres
            if(s) {
                if(s->DistanceTo(pt)>1e-4){
                    s->Set(pt);
                    nodecnt++;
                }
            }
        }
    } // for i
    return nodecnt;
}
int RX_PCFile::RefreshProps(FILE *fp, const int nn)  // returns change count
{int PropsHaveChanged=0;
    //			Then Prop_No, EA, SelfWeight, RenderSize
    //	m_props.clear();
    double l_ea,l_sw,l_rs;
    int i,k,nnum;
    for(i=0;i<nn;i++) {

        k=fscanf(fp,"%d  %lf  %lf %lf ", &nnum, &l_ea,&l_sw,&l_rs);
        char buf1[BUFFER_SIZE],buf2[BUFFER_SIZE];
        fgets(buf1,BUFFER_SIZE,fp); //       Type Description
        fgets(buf2,BUFFER_SIZE,fp); //       Size Description

        //				m_props[nnum]=CTensylProp(nnum,l_ea,l_sw,l_rs);
        CTensylProp &q = m_props[nnum];
        if(fabs(l_ea - q.m_ea) > PCF_DELTA) { cout<<"Property "<<nnum<<" has new EA "<<l_ea<<" was "<< q.m_ea <<endl; q.m_ea=l_ea;  PropsHaveChanged++;  }
        if(fabs(l_sw - q.m_SelfWeight) > PCF_DELTA) { cout<<"Property "<<nnum<<" has new selfWeight "<<l_sw<<" was "<< q.m_SelfWeight <<endl; q.m_SelfWeight=l_sw; PropsHaveChanged++;  }
        if(fabs(l_rs - q.m_RenderSize) > PCF_DELTA) { cout<<"Property "<<nnum<<" has new RS "<<l_rs<<" was "<< q.m_RenderSize <<endl;q.m_RenderSize=l_rs; PropsHaveChanged++;  }
        q.m_prop_no=nnum;

        q.s1=ON_String(buf1); m_props[nnum].s2=ON_String(buf2);
        q.s1.TrimLeftAndRight(); m_props[nnum].s1.Remove(' ');
        q.s2.TrimLeftAndRight(); m_props[nnum].s2.Remove(' ');
    } // for i

    return PropsHaveChanged;
}
int RX_PCFile::RefreshLinks(FILE *fp, const int nn, const int flags, const int pPropsHaveChanged )  // returns change count
{
    /*On refreshing, we may have changed the property (EA) or the links m_Slacklength or m_Control

 NO TYPE CHANGE...
 If we have just changed the length of a EA type
  its OK to call cf_setflinklength with m_slacklength converted to Metres.
  but we should also set EA. It may have changed.

 If we have changed m_slacklength of a TI type, we just want to change flink%ti.

TYPE CHANGE EA to TI
  we want to set flink's EA to the new value and set its TI to the new value of m_slacklength
TYPE CHANGE TI to EA
 here we want to set zlink0 to zt/(1+ tq/newEA)
  and m_slacklength to the equivalent in inches
  and flink%tI to 0, %EA=> new value,
*/
    char buf[512];
    int c1=0,c2=0,c3=0,c4=0;
    double  tq ,zt,ea1=0, ea2=0, ti1=0, ti2=0, mass=0,zi1=0,zi2=0,denom;
    int i,k,nnum;
    int l_n1,l_n2,l_prop,l_control,l_bdy,l_field; double l_sl;
    for(i=0;i<nn;i++) {
        int lRow=-2,lCol=-2;
        if(!fgets(buf,500,fp)) break;
        if(rxIsEmpty(buf))
        {i--; continue;}
        if (stristr(buf,"</links>"))
            break;
        k=sscanf(buf,"%d %d %d %lf  %d %d %d %d %d %d", &nnum, &l_n1,&l_n2,&l_sl,&l_prop,&l_control,&l_bdy,&l_field,&lRow,&lCol);
        if( k!=10)  {
            lRow=lCol=-1;
            k=sscanf(buf,"%d %d %d %lf  %d %d %d %d", &nnum, &l_n1,&l_n2,&l_sl,&l_prop,&l_control,&l_bdy,&l_field);
        if(k!=8)
            break;
        }
        CTensylLink &q = m_links[nnum];

        if(q.m_nn<=00)  continue;
        if(q.m_e)
            cout<<"I didnt think that q can still be associated with a string"<<endl;
        q.m_prop=l_prop;
        if(l_control == q.m_Control) {
            if(l_control){ // unchanged type 'EA'
                q.m_Slacklength=l_sl;
                if (q.m_fnn && (flags&RX_PCFile_LINK_LENGTHS  ))
                    c1+= cf_setflinklength(m_owner->Esail->SailListIndex (),q.m_fnn, l_sl*l_sf);
                CTensylProp &p = m_props[q.m_prop];
                ea2=p.m_ea*FORCE_UNIT_FACTOR;
                ti2 = 0.0 ;
                mass=p.m_SelfWeight * LINEAR_MASS_FACTOR;
                c1+=cf_setflinkproperties(m_owner->Esail->SailListIndex (),q.m_fnn,ea2, ti2, mass);// returns 1 if something is different
            }
            else  // control=0. type 'TI' Just change TI.
            {
                q.m_Slacklength=l_sl; // actually the prestress
                CTensylProp &p = m_props[q.m_prop];
                ea2=p.m_ea*FORCE_UNIT_FACTOR;
                ti2 = q.m_Slacklength *FORCE_UNIT_FACTOR;
                mass=p.m_SelfWeight * LINEAR_MASS_FACTOR;
                c2+=cf_setflinkproperties(m_owner->Esail->SailListIndex (),q.m_fnn,ea2, ti2, mass);// returns 1 if something is different
            }
        }
        else if (l_control) // changing from TI style to EA style IE setting TI2 to 0 and EA2 to input value,
            //		set zlink0 to  zt/(1+ tq/newEA)
            //		and m_slacklength to the equivalent in inches
            //		and flink%tI to 0, %EA=> new value,
        {
            CTensylProp &p = m_props[q.m_prop];
            tq = cf_getflinktension(m_owner->Esail->SailListIndex (),q.m_fnn);// analysis units
            zt = q.DeflectedLength();
            cf_getflinkproperties(m_owner->Esail->SailListIndex (),q.m_fnn, &ea1,&ti1,&zi1,&mass); // returns 0 if failed
            ea2=  p.m_ea*FORCE_UNIT_FACTOR;
            ti2 = 0.0;
            mass= p.m_SelfWeight * LINEAR_MASS_FACTOR; ;
            denom = (-(ea1*zi1) + ea2*zi1 + ti1*zi1 - ti2*zi1 + ea1*zt);
            if(fabs(denom)>0.001)
                zi2 = (ea2*zi1*zt)/denom;
            else
                zi2 = zt;
            if(zi2<0.001) zi2=zt;

            q.m_Slacklength = zi2/l_sf;
            cf_setflinklength(m_owner->Esail->SailListIndex (),q.m_fnn, zi2);
            c3+=cf_setflinkproperties(m_owner->Esail->SailListIndex (),q.m_fnn,ea2, ti2, mass);// returns 1 if something is different
            q.m_Control=l_control;
        }
        else if( !l_control) // changing from EA style to TI style (t = ti + ea (zt-zi)/zi)
        {
            CTensylProp &p = m_props[q.m_prop];
            tq = cf_getflinktension(m_owner->Esail->SailListIndex (),q.m_fnn);// analysis units
            zt = q.DeflectedLength();
            cf_getflinkproperties(m_owner->Esail->SailListIndex (),q.m_fnn, &ea1,&ti1,&zi1,&mass); // returns 0 if failed

            ea2=  p.m_ea*FORCE_UNIT_FACTOR;
            ti2 = q.m_Slacklength *FORCE_UNIT_FACTOR;
            mass= p.m_SelfWeight * LINEAR_MASS_FACTOR; ;
            denom = (-(ea1*zi1) + ea2*zi1 + ti1*zi1 - ti2*zi1 + ea1*zt);
            if(fabs(denom)>0.001)
                zi2 = (ea2*zi1*zt)/denom;
            else
                zi2 = zt;
            if(zi2<0.001) zi2=zt;
            q.m_Slacklength = zi2/l_sf;
            cf_setflinklength(m_owner->Esail->SailListIndex (),q.m_fnn, zi2);
            c4+=cf_setflinkproperties(m_owner->Esail->SailListIndex (),q.m_fnn,ea2, ti2, mass);// returns 1 if something is different
            q.m_Control=l_control;
        }

    } // for i
    cout<<" PC file Refresh: changed "<<c1<<"/"<<nn<<"EA strings \n";
    cout<<" PC file Refresh: changed "<<c2<<"/"<<nn<<"TI strings \n";
    cout<<" PC file Refresh: changed "<<c3<<"/"<<nn<<"TI strings to EA  \n";
    cout<<" PC file Refresh: changed "<<c4<<"/"<<nn<<"EA strings to TI"<<endl;
    if(pPropsHaveChanged+c1+c2+c3+c4) cout<<" Changes were detected in the Refresh"<<endl;
    return c1+c2+c3+c4;
}
int RX_PCFile::Refresh(const char*p_filename, const int flags)
{
    // flags might be RX_PCFile_LINK_LENGTHS ,  RX_PCFile_LINK_PROPS, RX_PCFile_COORDS


//    size_t   iii;
  //  char filename [ BUFFER_SIZE+1 ];
    int k, nnum,nlc; double  xx,yy,zz; int bdy,field,fixity,AxSym,nodecnt=0;
    int PropsHaveChanged = 0;
    unsigned int i, nn=0;
//    if(!is_empty(p_filename)) strcpy(filename,p_filename);
//    else {    // Conversion
//#ifdef linux
//        strcpy(filename,qPrintable (m_filename)  );
//#elif ! defined(__GNUC__)
//        wcstombs_s(&iii, filename, (size_t)BUFFER_SIZE,
//                   this->m_filename.Array(), (size_t)BUFFER_SIZE );
//#elif  defined(__GNUC__)
//        strcpy(filename,qPrintable (m_filename)  );
//#endif
//    }

    FILE *fp = fopen(qPrintable (m_filename) ,"r");
    if(!fp) return false;

    // find nodes and read them to a keylist

    if(flags& RX_PCFile_COORDS && findline(&fp,ON_String("<NODES>"))){
        if(!fscanf(fp,"%d",&nn)) {fclose(fp); return false;}
        if(nn !=  m_nodes.size()){ cout<< " different node count"<<endl;fclose(fp); return false;}
        nodecnt=RefreshNodes(fp,nn);
    } // if found nodes
    printf(" PC file Refresh: set  %d /%u fixed node positions\n",nodecnt,nn);

    if(findline(&fp,ON_String("<CPROPS>"))){
        if(!fscanf(fp,"%d",&nn)) {fclose(fp); return false;}
        PropsHaveChanged=RefreshProps(fp, nn); // nn is the number of new props in the file
    }

    if(findline(&fp,ON_String("<LINKS>"))){
        int lCount=0,ccount=0;
        if(!fscanf(fp,"%d",&nn)) {fclose(fp);return false; }
        if(nn != this->m_links.size()){ cout<< " different link count"<<endl; fclose(fp); return false;}
        lCount=RefreshLinks( fp,  nn,flags, PropsHaveChanged );  // returns change count
    }

    // find loads
    if(findline(&fp,ON_String("<POINTLOADS>"))){
        if(!fscanf(fp,"%d %d",&nn,&nlc))
        { fclose(fp); return false;}
        if(nlc !=1) {
            m_owner->OutputToClient (" can only read a PC file containing a single load case \n",3);
            { fclose(fp); return false;}
        }
        for(i=0;i<nn;i++) {
            float x,y,z;
            k=fscanf(fp,"%d %f %f %f", &nnum, &x,&y,&z);// this is very sensitive to whitespace
            if(k!=4)
                continue;
            CTensylNode thenode = (m_nodes)[MasterNodeNo(nnum)];
            if(thenode.m_nn==nnum) {
                ON_wString ss; ss.Format("$px=%f,$py=%f,$pz=%f ",-x*FORCE_UNIT_FACTOR,-y*FORCE_UNIT_FACTOR,-z*FORCE_UNIT_FACTOR);
                (m_nodes)[nnum].m_atts+=ss;
            }
            else
                printf( "(POINTLOADS) cant find a node with name %d \n",nnum);
        } // for i
    } // if found cprops

    fclose(fp);
    //	int iret = CreateModel();

    return 1;

}

#endif
#if !defined(RXRHINOPLUGIN) || defined(RXPHYSX) 

int RX_PCFile::Read(const double minlength)
{
    char buf[512] ;

    int i,k, nn,nnum,nlc; double  xx,yy,zz; int bdy,field,fixity,AxSym;

    printf("(PCFile::Read) with <%s>\n", qPrintable(m_filename));
    FILE *fp = fopen(qPrintable(m_filename),"r");
    if(!fp) {
        printf("(PCFIle::Read) cant open <%s>\n", qPrintable(m_filename));
        return false;
    }

    Init();
    if(findline(&fp,ON_String("<TITLE>"))){
        m_titlestring.Empty();
        for(i=0; ;i++) {
            if(!fgets(buf,500,fp)) break;
            if(rxIsEmpty(buf)) continue;
            if (stristr(buf,"</title>"))
                break;
            m_titlestring+=buf;
        }
    }

// find nodes and read them to a keylist
    /*// Then Node Number, X, Y, Z, Boundary, Field, Fixity, AxSym
<NODES>
10681
9 -8317.46837313235 -8325.55940338391 492.074000620648 0 0 7 0 */

    if(findline(&fp,ON_String("<NODES>"))){
        if(!fscanf(fp,"%d",&nn)) { cout<< "cant read NN after <NODES>"<<endl; return false; }
        for(i=0; ;i++) {
            char  atts[512];
            if(!fgets(buf,500,fp)) break;
            if(rxIsEmpty(buf)) continue;
            if (stristr(buf,"</nodes>"))
                break;
            *atts=0;
            k=sscanf(buf,"%d  %lf  %lf %lf %d %d %d %d %s", &nnum, &xx,&yy,&zz,&bdy,&field,&fixity,&AxSym,atts);
            if(k<8) {
                printf(" PC node read error<%s>\n",buf);  continue;}

            ON_3dPoint pt(xx,yy,zz); // inches
            (m_nodes)[nnum]=CTensylNode(nnum,pt, (bdy!=0),fixity,field,AxSym);
            ON_wString sname; sname.Format("%d ,",nnum);
            (m_nodes)[nnum].m_atts += sname;
            (m_nodes)[nnum].m_atts += ON_wString(atts);
        } // for i
    } // if found nodes
    else { cout<< "cant find the line <NODES>"<<endl; return false;}

    ShimmerDuplicates(0.0);

    if(findline(&fp,ON_String("<CPROPS>"))){//			Then Prop_No, EA, SelfWeight, RenderSize
        if(!fscanf(fp,"%d",&nn)) return false;
        double l_ea,l_sw,l_rs;
        for(i=0;i<nn;i++) {
            k=fscanf(fp,"%d  %lf  %lf %lf ", &nnum, &l_ea,&l_sw,&l_rs);
            char buf1[BUFFER_SIZE],buf2[BUFFER_SIZE];
            fgets(buf1,BUFFER_SIZE,fp); //       Type Description
            fgets(buf2,BUFFER_SIZE,fp); //       Size Description
            m_props[nnum]=CTensylProp(nnum,l_ea,l_sw,l_rs);
            m_props[nnum].s1=ON_String(buf1); m_props[nnum].s2=ON_String(buf2);
            m_props[nnum].s1.TrimLeftAndRight(); m_props[nnum].s1.Remove(' ');
            m_props[nnum].s2.TrimLeftAndRight(); m_props[nnum].s2.Remove(' ');
        } // for i
    } // if found cprops
    else return false;

    // find links
    if(findline(&fp,ON_String("<LINKS>"))){
        QString qatts;
        if(!fscanf(fp,"%d",&nn)) return false;
        //Number, StartNode, EndNode, SlackLength, PropNo, Control, Boundary, Field
        int l_n1,l_n2,l_prop,l_control,l_bdy,l_field; double l_sl;
        int lRow,lCol,nchr;
        for(i=0; ;i++) {
            if(!fgets(buf,500,fp)) break;
            if(rxIsEmpty(buf))
            {i--; continue;}
            if (stristr(buf,"</links>"))
                break;
            k=sscanf(buf,"%d %d %d %lf  %d %d %d %d %d %d%n", &nnum, &l_n1,&l_n2,&l_sl,&l_prop,&l_control,&l_bdy,&l_field,&lRow,&lCol,&nchr);
            if(k==10)  // try to read the atts
            {
                char*lp=buf;lp+=nchr; qatts=QString(lp);
                if(!qatts.isEmpty())
                  qDebug()<<" link "<<nnum << "has atts "<<qatts;

            }
            else  {// k!=10)
                lRow=lCol=-1;
                k=sscanf(buf,"%d %d %d %lf  %d %d %d %d", &nnum, &l_n1,&l_n2,&l_sl,&l_prop,&l_control,&l_bdy,&l_field);
                if( k!=8)
                    break;
                qatts.clear();
            }
            if(nnum==0)
                this->m_owner->OutputToClient (" you cant have node number zero in a PC file",5);
            if(m_links.find(nnum)!= m_links.end()){
                cout<<" more than one link with number "<<nnum<<endl << buf<<endl;
#ifndef RXPHYSX
                m_owner->OutputToClient("PCFile numbering",5);
#endif
                continue;
            }

            if(this->m_nodes.find(l_n1)== m_nodes.end()){
                cout<<"link number "<<nnum<< " refers to nonexistent node "<<l_n1<<endl << buf<<endl;
#ifndef RXPHYSX
                m_owner->OutputToClient("PCFile numbering",5);
#endif
                continue;
            }
            if(this->m_nodes.find(l_n2)== m_nodes.end()){
                cout<<"link number "<<nnum<< " refers to nonexistent node "<<l_n2<<endl << buf<<endl;
#ifndef RXPHYSX
                m_owner->OutputToClient("PCFile numbering",5);
#endif
                continue;
            }
            if(l_sl>minlength){
                (m_links)[nnum]=CTensylLink(nnum ,l_n1,l_n2,l_sl,l_prop,l_control,l_bdy,l_field,lRow,lCol,this,qatts);
            }
            else {
                printf("skip short(<%f) link %d (%d->%d) L= %f...",minlength, nnum,l_n1,l_n2,l_sl);
                if(!m_nodes[l_n1].m_fixity) {
                    m_masters[l_n1]= l_n2;
                    printf("deleting node %d\n", l_n1);
                    assert(m_nodes[l_n1].m_fixity==0);
                    m_nodes.erase(l_n1);
                }
                else if(m_nodes[l_n2].m_fixity ) {
                    m_masters[l_n2]= l_n1;
                    printf("deleting node %d\n", l_n2);
                    assert(m_nodes[l_n2].m_fixity==0);
                    m_nodes.erase(l_n2);
                }
                else {
                    cout<< " CANT DELETE. both are fixed"<<endl;}
            } // if short
        } // for i
    } // if found links
    else return false;

    // find loads
    if(findline(&fp,ON_String("<POINTLOADS>"))){
        if(!fscanf(fp,"%d %d",&nn,&nlc))
            return false;
        if(nlc !=1) {
#ifndef RXPHYSX
            m_owner->OutputToClient (" can only read a PC file containing a single load case \n",3);
#endif
            return false;
        }
        for(i=0;i<nn;i++) {
            float x,y,z;
            k=fscanf(fp,"%d %f %f %f", &nnum, &x,&y,&z);// this is very sensitive to whitespace
            if(k!=4)
                continue;
            CTensylNode thenode = (m_nodes)[MasterNodeNo(nnum)];
            if(thenode.m_nn==nnum) {
                ON_wString ss; ss.Format("$px=%f,$py=%f,$pz=%f ",-x*FORCE_UNIT_FACTOR,-y*FORCE_UNIT_FACTOR,-z*FORCE_UNIT_FACTOR);
                (m_nodes)[nnum].m_atts+=ss;
            }
            else
                printf( "(POINTLOADS) cant find a node with name %d \n",nnum);
        } // for i
    } // if found cprops

    fclose(fp);
    //this->SpecialAdjustZI();
#ifndef RXPHYSX
    int iret = CreateModel();
    return iret;
#else
    return 1;
#endif
    // keep the maps for a refresh
#ifdef NEVER
    m_props.clear();
    m_nodes.clear();
    m_links.clear();
    m_restraints.clear();
    m_masters.clear();
#endif

}


#endif
#if !defined(RXRHINOPLUGIN) && !defined(RXPHYSX) 
int RX_PCFile::CreateModel()
{
    int rc=0, depth=1;
    ON_wString line; ON_String name;
    // here we create the nodes, lines, etc with their atts appended to their names, fixities correctly coded
    // and GAUSS_ layer names.

    double l_sf = ON::UnitScale(ON::inches,ON::meters);

    DeleteShortLinks (0.001);// the tolerance is in Metres
    RXEntity_p bce = dynamic_cast<RXEntity_p>(m_owner->Esail->Entity_Exists("straight","basecurve"));
    if(!bce) {
        bce=Just_Read_Card(m_owner->Esail ,"2dcurve:straight:0:0:1:0","2dcurve","straight",&depth,true);
        if(bce ) {
            if(bce->Resolve( ))
                bce ->Needs_Finishing=!bce->Finish();
        }
    }

    // first walk thru the map of nodes, set their restraints and their loads

    map<int, CTensylNode >  ::iterator pos;
    RXSRelSite *theSite;
    int ncount=0, nn = (int) m_nodes.size();

    //#pragma omp parallel shared(nn,l_sf)
    {
        // 	int i;
        //#pragma omp for schedule(dynamic,1)  private ( name,line,pos ,i,theSite  )
        // 	for(i=0;i<nn;i++) {
        //pos= m_nodes.find(i);
        //if(pos==ending)
        //	continue;
        cout<<" this might work in openMP if we protect the generation of node numbers together with fnodelist management"<<endl;
        for (pos = (m_nodes).begin(); pos !=  m_nodes.end(); ++pos) {

            if(pos->second.m_nn ==0)
                continue;

            pos->second.m_atts += ON_wString(",") + ON_String(m_restraints[pos->second.m_fixity]);
            name.Format("%d",pos->second.m_nn);
            line.Format("site: %d :%lf:%lf:%lf:",pos->second.m_nn,pos->second.m_pt.x*l_sf,pos->second.m_pt.y*l_sf,pos->second.m_pt.z*l_sf);

            line+=pos->second.m_atts;

            theSite =dynamic_cast < RXSRelSite *> (Just_Read_Card(m_owner->Esail ,ON_String(line).Array(),"site",name.Array(),&depth,true));
            pos->second.m_e =theSite;
            if(theSite ) {
                if(theSite->Resolve())
                    theSite ->Needs_Finishing=!theSite->Finish( );
                else wcout<<L"cant resolve site with line <"<<line.Array ()<<L">"<<endl;
            }
            else wcout<<L"failed to create site with line <"<<line.Array ()<<L">"<<endl;
            rc=1;
            ncount++;
            div_t divresult;
            if(nn/20>0) {
                divresult = div (ncount,nn/20);
                if(! divresult.rem)
                    printf("processed %d/%d nodes\n", ncount,nn);
            }

        } // end for
    }// end pragma
    printf(" n nodes = %d\n", (int) m_nodes.size());
    // then walk thru the links, creating strings with ea,ti, mass, FLength, notrim, etc
    int n1,n2, count=10000;
    double ea,ti,zi;
    map<int, CTensylLink >  ::iterator it;
    int linkno=0;
    if(true) { // we generate flinks not strings. They are much more efficient.

        for (it = m_links.begin(); it != m_links.end(); ++it) {
            CTensylLink &l =  it->second;

            count++; if(count>=10000){ printf("importing link no %d \n", l.m_nn); count=0;}
            map<int, CTensylNode > ::iterator iit;
            iit = m_nodes.find(l.GetStartNode());
            if(iit==m_nodes.end()){
                cout<<"link "<<l.m_nn<< " wants node "<<l.GetStartNode()<<" which doesnt exist"<<endl;
                continue;
            }
            iit = m_nodes.find(l.GetDestNode());
            if(iit==m_nodes.end()){
                cout<<"link "<<l.m_nn<< " wants node "<<l.GetDestNode()<<" which doesnt exist"<<endl;
                continue;
            }

            CTensylNode &p1 = m_nodes[l.GetStartNode()];
            CTensylNode &p2 = m_nodes[ l.GetDestNode ()];


            Site *s=(Site *) p1.m_e;
            n1   = s->GetN(); // -1; assert("Why not use TensylNode:m_nn"==0); // n1=s->N;
            s=(Site *) p2.m_e;
            n2  = s->GetN(); //= -1; assert("Why not use TensylNode:m_nn"==0); // n2 =  s->N;
            if(!p1.m_nn|| !p2.m_nn ) {
                printf("skipping zero link %d (%d->%d) %f in\n",l.m_nn, n1,n2, l.m_Slacklength*l_sf);
                continue;
            }
            if(n1==n2) {
                printf("skipping short link %d (%d->%d) %f in\n",l.m_nn, n1,n2, l.m_Slacklength*l_sf);
                continue;
            }
            CTensylProp &p = m_props[l.m_prop];
            //MakeElementProperties requires l.m_control l.m_Slacklength(inches), to be set
            l.MakeElementProperties( &p ,this, ea,zi,ti); // returns EA and ZI in SI units.


            QRegExp ex("$string\s*=\s*(.+)(,|\))");
           // QRegExp rxlen("(\\d+)(?:\\s*)(cm|inch)");
            int pos = ex.indexIn(l.Atts());
            if (pos > -1) {
                QString sname = ex.cap();
                qDebug()<<" (createModel) link "<<l.m_nn<< "wants string "<<sname;
            }
//else
            {

            l.m_fnn = cf_insert_flink (m_owner->Esail->SailListIndex (), ++linkno ,n1,n2,
                                       ea,zi, ti,p.m_SelfWeight*LINEAR_MASS_FACTOR, &( it->second) ) ;
            //return value is a ptr to the fortran object
            //double tension = cf_getflinktension(l.fptr);

            cf_setflinklength(m_owner->Esail->SailListIndex (),l.m_fnn, zi);

            }

        }
    }else
        for (it = m_links.begin(); it != m_links.end(); ++it) {
            CTensylLink &l =  it->second;//*it;
            count++; if(count>=1000){ printf("importing link no %d \n", l.m_nn); count=0;}
            if(l.m_Control!=1) {
                cout<< "havent coded the case where Control isnt 1\n"<<endl;
                continue;
            }
            CTensylNode &p1 = m_nodes[l.GetStartNode()];/* (m_nodes)[MasterNodeNo(l.m_n1)]; */ n1 = p1.m_nn;
            CTensylNode &p2 = m_nodes[l.GetDestNode()];/*  (m_nodes)[MasterNodeNo(l.m_n2)];*/ n2 = p2.m_nn;

            if(!p1.m_nn|| !p2.m_nn ) {
                printf("skipping zero link %d (%d->%d) %f in\n",l.m_nn, n1,n2, l.m_Slacklength*l_sf);
                continue;
            }
            if(n1==n2) {
                printf("skipping short link %d (%d->%d) %f in\n",l.m_nn, n1,n2, l.m_Slacklength*l_sf);
                continue;
            }
            CTensylProp &p = m_props[l.m_prop];
            ON_wString l_a;
            cout<< " this is where we create our FLINK "<<endl;
            l_a.Format("$flength,$notrim,$noslide,$string,$Xscaled,$Yscaled,$flat,$sketch,$ea=%f,$zi=%f,$ti=0.0,$mass=%f,$seg=%s",
                       p.m_ea*FORCE_UNIT_FACTOR,l.m_Slacklength*l_sf,p.m_SelfWeight*LINEAR_MASS_FACTOR,p.s1.Array() );
            line.Format("seamcurve: %d:%d:%d:straight:0:",l.m_nn, n1,n2);
            line+=l_a;
            name.Format("%d",l.m_nn);
            l.m_e = dynamic_cast<class RXSeamcurve* >(Just_Read_Card(m_owner->Esail,ON_String(line).Array(),"seamcurve",name.Array(),&depth,true));  // was type
            if(l.m_e ) {
                if(l.m_e->Resolve(bce,p1.m_e,p2.m_e))
                    l.m_e ->Needs_Finishing=!l.m_e ->Finish();
                else
                    l.m_e->Needs_Resolving =1;
            }


        }// for

    printf(" n links = %ld\n",(long int)m_links.size());
    return rc;
}




#endif
#ifdef RXRHINOPLUGIN
#ifndef RX_XML2PC
ON_wString word(const ON_wString a, const ON_wString b)
{
    ON_wString w;
    int k =a.Find(b);
    if(k>=0){
        k+=b.Length (); w = a.Mid(k);
        k = w.Find(",");
        if(k>=0)
            w=w.Left(k);

    }
    return w;
}
size_t RX_PCFile::ReadRhino(const double minlength, CRhinoDoc& doc, const int flags)
{
    char buf[512];
    int lMin=-1, lMax=-1;
    double nsds = 1.25; // no of sds from mean of full color range
    map<int,double> tq;
    size_t   iii;
    char       filename [ BUFFER_SIZE+1 ];
    int i,k, nn,nnum,nlc; double  xx,yy,zz; int bdy,field,fixity,AxSym;
    // Conversion
#ifdef linux
    strcpy(filename, H_ASCII_TEXT(m_filename.Array() ));
#else
    wcstombs_s(&iii, filename, (size_t)BUFFER_SIZE,
               this->m_filename.Array(), (size_t)BUFFER_SIZE );
#endif
    RhinoApp().Print("(PCFile::Read) with <%s>\n", filename);

    BOOL bColorByTension =flags&2;
    BOOL DrawNodes=flags&4;


    FILE *fp = fopen(filename,"r");
    if(!fp) {
        RhinoApp().Print("(PCFIle::Read) cant open <%s>\n", filename);
        return false; // agii
    }

    Init();
    if(findline(&fp,ON_String("<TITLE>"))){
        m_titlestring.Empty();
        for(i=0; ;i++) {
            if(!fgets(buf,500,fp)) break;
            //if(! (*buf))  continue;//
            if(is_empty(buf)) continue;

            if (strstr(buf,"</TITLE>"))
                break;
            m_titlestring+=buf;
        }
    }
    RhinoApp().Print("title of this file is\n<%s>\n",m_titlestring.Array());
    // find nodes and read them to a keylist

    if(findline(&fp,ON_String("<NODES>"))){

        if(!fscanf(fp,"%d",&nn)) {  RhinoApp().Print("cant read NN after <NODES>"); return false; }


        /*// Then Node Number, X, Y, Z, Boundary, Field, Fixity, AxSym
<NODES>
 10681
 [optional blank lines]
 9 -8317.46837313235 -8325.55940338391 492.074000620648 0 0 7 0 */
        char attstring[512];
        for(i=0; ;i++) {
            if(!fgets(buf,500,fp)) break;

            if(is_empty(buf)) /// see c:\Users\Peter Heppel\Documents\Visual Studio 2005\mirror\preproc\c\stringutils.cpp
                continue;
            if (strstr(buf,"</NODES>"))
                break;
            k=sscanf(buf,"%d  %lf  %lf %lf %d %d %d %d %s", &nnum, &xx,&yy,&zz,&bdy,&field,&fixity,&AxSym,attstring);
            if(k<8) {
                RhinoApp().Print(" PC node read error<%s>\n",buf);
                continue;
            }
            if(k<9)
                *attstring=0;
            ON_3dPoint pt(xx,yy,zz); // inches
            (m_nodes)[nnum]=CTensylNode(nnum,pt, (bdy!=0),fixity,field,AxSym);
            ON_wString sname; //sname.Format("%d ,%s",nnum,attstring);
            if(!is_empty(attstring)){
                sname.Format("%s",attstring);
                (m_nodes)[nnum].m_atts += sname;
            }
        } // for i
    } // if found nodes
    else {  RhinoApp().Print("cant find the line <NODES>"); return false;}

    ShimmerDuplicates(0.0);

    if(findline(&fp,ON_String("<CPROPS>"))){
        if(!fscanf(fp,"%d",&nn)) return false;
        //			Then Prop_No, EA, SelfWeight, RenderSize

        double l_ea,l_sw,l_rs;
        for(i=0;i<nn;i++) {
            k=fscanf(fp,"%d  %lf  %lf %lf ", &nnum, &l_ea,&l_sw,&l_rs);
            char buf1[BUFFER_SIZE],buf2[BUFFER_SIZE];
            fgets(buf1,BUFFER_SIZE,fp); //       Type Description
            fgets(buf2,BUFFER_SIZE,fp); //       Size Description
            m_props[nnum]=CTensylProp(nnum,l_ea,l_sw,l_rs);
            m_props[nnum].s1=ON_String(buf1); m_props[nnum].s2=ON_String(buf2);
            m_props[nnum].s1.TrimLeftAndRight(); m_props[nnum].s1.Remove(' ');
            m_props[nnum].s2.TrimLeftAndRight(); m_props[nnum].s2.Remove(' ');
        } // for i
    } // if found cprops
    else return false;

    //  Link Number, Tension, zt
    //<TENSIONS>
    //1131     0.711416892556     2.610852610314
    //1132     1.199403587376     2.198455104979
    double tsum=0,tsqsum=0, nt=0, avg =1.,sd=1;
    if(findline(&fp,ON_String("<TENSIONS>"))){
        int n; double t,zt;
        for(i=0; ;i++) {
            if(!fgets(buf,500,fp)) break;
            //if(! (*buf))  continue;//
            if(is_empty(buf))
            {i--; continue;}
            if (strstr(buf,"</TENSIONS>"))
                break;
            k=sscanf(buf,"%d %lf %lf ", &n, &t,&zt);
            tsum+=t;  tsqsum+= t*t; nt++;

            if( k!=3)
                break;
            tq[n]= t;
        } // for i
        if(nt>0){
            avg = tsum/nt;
            sd = sqrt( tsqsum /nt -avg*avg);
        }
        RhinoApp().Print(" tensions found: count = %d, mean=%f,sd=%f\n", (int)nt, avg,sd);
    } // if found tensions

    // find links
    if(findline(&fp,ON_String("<LINKS>"))){
        if(!fscanf(fp,"%d",&nn)) {
            RhinoApp().Print("first work of 'LINKS' isnt an integer\n");
            return false;
        }


        //Number, StartNode, EndNode, SlackLength, PropNo, Control, Boundary, Field ,row,col
        int l_n1,l_n2,l_prop,l_control,l_bdy,l_field; double l_sl;
        int l_row,l_col;
        for(i=0; ;i++) {
            if(!fgets(buf,500,fp))
                break;
            //if(! (*buf))  continue;//
            if(is_empty(buf))
            {i--; continue;}
            if (strstr(buf,"</LINKS>"))
                break;

            k=sscanf(buf,"%d %d %d %lf  %d %d %d %d %d %d", &nnum, &l_n1,&l_n2,&l_sl,&l_prop,&l_control,&l_bdy,&l_field,&l_row,&l_col);
            if( k!=10)   {
                l_field=l_row=l_col=-1;
                k=sscanf(buf,"%d %d %d %lf  %d %d %d %d", &nnum, &l_n1,&l_n2,&l_sl,&l_prop,&l_control,&l_bdy,&l_field);
                if( k!=8)
                    break;
            }


            if(lMax<0) {
                lMin=lMax=l_prop;
            }
            else { 		lMin=min(lMin,l_prop); 			lMax=max(lMax,l_prop); }
            if(l_sl>minlength)
                m_links[nnum]=CTensylLink(nnum ,l_n1,l_n2,l_sl,l_prop,l_control,l_bdy,l_field,l_row,l_col,this);

            else {
                RhinoApp().Print("skip short(<%f) link %d (%d->%d) L= %f...",minlength, nnum,l_n1,l_n2,l_sl);
                if(!m_nodes[l_n1].m_fixity) {
                    m_masters[l_n1]= l_n2;
                    RhinoApp().Print ("deleting node %d\n", l_n1);
                    assert(m_nodes[l_n1].m_fixity==0);
                    m_nodes.erase(l_n1);
                }
                else if(m_nodes[l_n2].m_fixity ) {
                    m_masters[l_n2]= l_n1;
                    RhinoApp().Print ("deleting node %d\n", l_n2);
                    assert(m_nodes[l_n2].m_fixity==0);
                    m_nodes.erase(l_n2);
                }
                else {
                    RhinoApp().Print (" CANT DELETE. both are fixed");}
            } // if short
        } // for i
    } // if found links
    else
        RhinoApp().Print("no 'LINKS' section\n");

    RhinoApp().Print("no of links = %d\n", m_links.size());

    // find loads
    if(findline(&fp,ON_String("<POINTLOADS>"))){
        if(!fscanf(fp,"%d %d",&nn,&nlc))
            return false;
        if(nlc !=1) {
            RhinoApp().Print (" can only read a PC file containing a single load case \n");
            return false;
        }
        for(i=0;i<nn;i++) {
            float x,y,z;
            k=fscanf(fp,"%d %f %f %f", &nnum, &x,&y,&z);// this is very sensitive to whitespace
            if(k!=4)
                continue;
            CTensylNode thenode = (m_nodes)[MasterNodeNo(nnum)];
            if(thenode.m_nn==nnum) {
                ON_wString ss; ss.Format("$px=%f,$py=%f,$pz=%f ",-x*FORCE_UNIT_FACTOR,-y*FORCE_UNIT_FACTOR,-z*FORCE_UNIT_FACTOR);
                (m_nodes)[nnum].m_atts+=ss;
            }
            else
                RhinoApp().Print( "(POINTLOADS) cant find a node with name %d \n",nnum);
        } // for i
    } // if found cprops

    fclose(fp);
    map<int, CTensylLink >  ::iterator it;
    RhinoApp().Print (" n els = %d\n", m_links.size());
    // before inserting the links we must create some layers

    CRhinoLayerTable& lt =  doc.m_layer_table;

    for(i=lMin;i<=lMax;i++) {
        ON_wString lname; lname.Format("prop_%2.2d",i);
        ON_Layer mylayer ;
        mylayer.SetLayerName(lname);
        lt.AddLayer(mylayer);
    }


    for (it = m_links.begin(); it != (m_links).end(); ++it) {
        CTensylLink &n = it->second;
        int n1 = n.GetStartNode();
        int n2=  n.GetDestNode ();
        CTensylNode &nn1 =this->m_nodes [n1];
        CTensylNode &nn2 =this->m_nodes [n2];

        ON_Line l(nn1.m_pt,nn2.m_pt);
        ON_3dmObjectAttributes  Attribute;
        Attribute.m_name.Format("e_%5.5d",it->first );
        Attribute.SetVisible( true );
        Attribute.m_layer_index= n.GetPropNo();
        Attribute.SetColorSource(ON::color_from_layer);
        if(bColorByTension && tq.size()){
            double t= tq[n.m_nn] ;
            double color = (t -avg)/(sd*nsds ) ;
            color=max(-1.,color); color=min(1.,color);
            Attribute.SetColorSource(ON::color_from_object  );
            int red,green,blue;
            color=-color;
            if(color<-0.5){
                red=255; blue=0; green = 255.*(color*2. + 2.);
            }
            else if(color<0){
                red=255 *(- color*2.) ; blue=0; green = 255;
            }
            else if(color<0.5){
                blue=255 *( color*2.) ; red=0; green = 255;
            }
            else {
                blue=255;   ; red=0; green = 255.*(-color*2. + 2.);
            }

            //red = min(255, 128 + (int) (128.0*color));
            //blue = min(255,128 - (int) (128.0*color));
            //green = 0;//max(0,128 - (int) (128.0*abs(color)));
            Attribute.m_color=ON_Color(red,green,blue);
        }
        doc.AddCurveObject(l, &Attribute );
    }
    {
        map<int, CTensylNode >  ::iterator it;
        RhinoApp().Print (" nNodes = %d\n", m_nodes.size());
        if(DrawNodes)
            for (it = m_nodes.begin(); it != (m_nodes).end(); ++it) {
                CTensylNode &n = it->second;
                ON_3dmObjectAttributes  Attribute;
                Attribute.m_name.Format("n_%5.5d (%S)",it->first,n.m_atts.Array() );
                Attribute.SetVisible( false );
                Attribute.m_layer_index=2;
                doc.AddPointObject(n.m_pt,&Attribute);
            }
    }
    // special for brepnets.
    // the brep edge nodes are grouped with a small jump in nodeno between them.  Draw a polyline
    // break when the jump is big.
    if(this->m_titlestring.Find("brepnet")>=0 )  { RhinoApp().Print("this is a brepnet model\n");
        map<int, CTensylNode >  ::iterator it;
        int nn, oldnn=-10, en=0;

        ON_3dPointArray pa;
        ON_3dPointArray slidecrv; 	//slidecrv.AppendNew()=ON_3dPoint(0,0,0);
        ON_wString cname,  alength;
        for (it = m_nodes.begin(); it != (m_nodes).end(); ++it) {
            CTensylNode &n = it->second;


            nn=it->first;
            if(nn>oldnn+5) { 	// draw the polyline and start a new one
                if(pa.Count()){
                    ON_3dmObjectAttributes  Attribute;
                    Attribute.m_name.Format("e_%5.5d",en++ );
                    Attribute.SetVisible( true );
                    Attribute.m_layer_index=3;
                    doc.AddCurveObject(ON_PolylineCurve(pa)  ,&Attribute);
                    pa.Empty ();
                }
                if(slidecrv.Count()>1){
                    ON_3dmObjectAttributes  Attribute;
                    Attribute.m_name=cname+ON_wString(L"_dist");
                    Attribute.SetVisible( true );
                    Attribute.m_layer_index=3;
                    ON_3dPoint p0 = (slidecrv[0]*1.5 + slidecrv[1]*(-0.5));
                    int m = slidecrv.Count();
                    ON_3dPoint p1 = (slidecrv[m-1]*1.5 + slidecrv[m-2]*(-0.5));
                    slidecrv.Insert(0,p0);slidecrv.AppendNew()=p1;
                    ON_PolylineCurve plc(slidecrv);
                    ON_3dVector dd = ON_3dPoint(0,0,0) - p0;
                    plc.Translate(dd);
                    ON_NurbsCurve  nc; plc.NurbsCurve (&nc);
                    int nk = nc.KnotCount();
                    double kv = (nc.Knot(0) +nc.Knot(1))/2.;
                    nc.SetKnot(0,kv);
                    kv = (nc.Knot(nk-1) +nc.Knot(nk-2))/2.;
                    nc.SetKnot(nk-1,kv);
                    p1 = nc.PointAtEnd();
                    ON_Xform xx; xx.Scale(1.0,1/p1.y,1.0);
                    nc.Transform( xx);

                    doc.AddCurveObject(nc  ,&Attribute);
                    slidecrv.Empty ();
                    //slidecrv.AppendNew()=ON_3dPoint(0,0,0);
                }


            }
            if(nn>oldnn+500) {
                RhinoApp().Print("finish drawing seams with nn=%d oldnn=%d\n", nn,oldnn);
                break;
            }

            // append the pt to the polyline.
            pa.AppendNew()=n.m_pt;
            oldnn=nn;
            // if this is  a slider...
            ON_wString &att = n.m_atts;
            cname=word(att,"$slidecurve="); alength= word(att,"$arclength=");
            if(!cname.IsEmpty()){
                ON_Polyline pp(pa);
                double y= pp.Length();
                double x = (double) slidecrv.Count();
                slidecrv.AppendNew()=ON_3dPoint(x,y,0);
            }

        }
    } // end special for brepnet.
    else
        RhinoApp().Print("no 'brepnet' keyword in title <%s>\n - dont draw seams\n", this->m_titlestring.Array() );
    double scale = 1.0 ;
    ON_String aaa = this->PrintSummary(0,scale);
    RhinoApp().Print(aaa);


    m_props.clear();
    m_nodes.clear();
    m_links.clear();
    m_restraints.clear();
    m_masters.clear();

    return m_links.size() ;
}

#endif
#endif
int RX_PCFile::DeleteShortLinks(const double tol){
    // here we scan the links for short ones.  If we find a short one we coalesce its nodes then we (could) delete it.
    // to coalesce two nodes, we simply copy the one to the place in m_nodes of the other.
    bool changing=false;
    double zi;
    int nn1,nn2, count=0,fixity=0;
    double l_sf = ON::UnitScale(ON::inches,ON::meters);
    map<int, CTensylLink >  ::iterator it;
    do{
        changing=false;
        for (it = m_links.begin(); it != m_links.end(); ++it) {
            CTensylLink &l =  it->second;
            if(l.m_Control !=1)
                continue;
            zi = l.m_Slacklength*l_sf; if(zi>tol) continue;

            nn1 =l.GetStartNode(); nn2 = l.GetDestNode(); if(nn1==nn2) continue;
            CTensylNode &p1 = m_nodes[nn1];
            CTensylNode &p2 = m_nodes[nn2];
            if(!p1.m_fixity  && p2.m_fixity) {
                fixity = p2.m_fixity;
            }
            else if(!p2.m_fixity  && p1.m_fixity) {
                fixity = p1.m_fixity;
            }
            else if(!p1.m_fixity  && !p2.m_fixity) {
                fixity = 0;
            }
            else if(p1.m_fixity  && p2.m_fixity) {
                fixity = max(p2.m_fixity,p1.m_fixity);
                if(p1.m_fixity  != p2.m_fixity)
                    cout<< " merge two nodes with different fixities\nMODEL CHANGE!!!!!!!!!!!!!!\n\n"<<endl;
            }
            p1.m_fixity   = p2.m_fixity = fixity;
            m_nodes[nn2] = m_nodes[nn1];
            printf("(DeleteShortLinks)merging zero link %d (%d->%d) %f (control=%d)\n",l.m_nn, nn1,nn2, l.m_Slacklength*l_sf,l.m_Control );
            //	printf(" Node at map %d is\n",nn1);
            //	m_nodes[nn1].Print(stdout);
            //	printf(" Node at map %d is\n",nn2);
            //	m_nodes[nn2].Print(stdout);

            /* for dbg */ p2 = m_nodes[nn2];
            count++;
            m_links.erase(it);
            changing=true;
            break;


        } // for it
    } while(changing);

    return count;
}
int RX_PCFile::findline(FILE ** fp, ON_String s)
{
    char buf[256];
    ON_String sbuf;
    int npasses=1;
    do{
        while(fgets(buf,256,*fp)){
            sbuf=ON_String(buf); sbuf.TrimLeftAndRight();
            if(0==sbuf.CompareNoCase(s.Array()))
                return true;
        }
        fseek( *fp , 0 , SEEK_SET );
    }while(npasses-- >0);
    return false;
}

int RX_PCFile::Init(void)
{
    m_props.clear();
    m_nodes.clear();
    m_links.clear();
    m_restraints.clear();
    m_masters.clear();


    //fixity is coded as:
    m_restraints[0] = " ";							//0--no fixity
    m_restraints[1] = "($fix= 1 0 0 1)";			//1--x direction
    m_restraints[2] = "($fix= 0 1 0 1)";			//2--y direction
    m_restraints[3] = "($fix= 0 0 1 1)";			//3--z direction
    m_restraints[4] = "($fix= 0 0 1)";		        //4--xy directions
    m_restraints[5] = "($fix= 0 1 0)";		        //5--xz directions
    m_restraints[6] = "($fix= 1 0 0)";		        //6--yz directions
    m_restraints[7] = "($fix=full )";				//7--all fixed
    m_restraints[8] = "($slidecurve=xxxxx)";			// PHA-specific. not supported by tensyl

    /* 	 Ian writes
Fixity:

//3--z 
Means the point is fixed in the z direction and can only move in a plane perpendicular to Z.

//4--xy 
Means the point is fixed in the x and y directions and can only move in the z direction. */

    return 1;

}







#if !defined(RXRHINOPLUGIN) && !defined(RXPHYSX) 


int  Resolve_PCFile_Card( RXEntity_p e)
{
    int retval = 0;
    // pcfile: ename : filename : atts
    QString qline = e->GetLine();
    QStringList wd= rxParseLine(qline,RXENTITYSEPREGEXP );
    if(wd.size()<3)  wd<<"*.PC";
    QString atts = wd.value(3);

    RX_PCFile * p = new RX_PCFile;

    if(e->PC_Finish_Entity("pcfile",qPrintable(wd[1]),p,0,0,qPrintable(atts),e->GetLine())) {// trashes LINE
        retval=1;
        e->Needs_Resolving= 0;
        e->Needs_Finishing = 1;
        e->SetNeedsComputing();
    }// if PC_Finish...

    p->m_owner=e;
    e->Esail->m_Selected_Entity[PCE_PCFILE ]=e;

    QString & fname =wd[2];

    fname = e->Esail->AbsolutePathName(fname);

    if(!VerifyFileName(fname,wd[1],"*.PC")) { //To make sure we are trying to read a PC file
        delete p;
        e->Needs_Resolving= 1;
        printf("VerifyFileName of <%s> <%s> failed\n",qPrintable(fname),qPrintable( wd[1] ));
        return 0;
    }
    fname = e->Esail->RelativeFileName(fname);
    qline = wd.join(RXENTITYSEPS);
    e->edited = !(qline ==e->GetLine());
    if(e->edited){
        e->SetLine ( qPrintable(qline));
    }

    e->hoopskey = 0;
    p->m_filename = fname ;
    p->m_owner=e;
    p->Read(0.0001); // need about 0.01 if we are creating Strings not links
    retval = 1;

    return(retval);
}
#endif
ON_String RX_PCFile::PrintSummary(FILE * fp, const double p_scale)
{
    ON_String r,buf;
    r = ON_String ("RX PCFile , name = <") +qPrintable(m_filename)+ON_String(">\n");

    buf.Format(
                "nn= %d nlinks= %d nprops= %d \n",
                m_nodes.size(),m_links.size(), m_props.size() );
    r+=buf;
    map<int,double> LengthByProp;
    int np; unsigned int i;
    double totlength=0., totweight=0.;
    map<int,CTensylProp>  ::iterator itp;
    for (itp = m_props.begin(); itp != m_props.end(); ++itp) {
        CTensylProp &p = itp->second;
        i = p.m_prop_no;
        LengthByProp[i]=0.0;
    }
    map<int, CTensylLink >  ::iterator it;
    for (it = m_links.begin(); it != m_links.end(); ++it) {
        CTensylLink &l =  it->second;
        totlength+=l.m_Slacklength;
        np= l.m_prop;
        CTensylProp p = m_props[np];
        if(l.m_Control==1)
            LengthByProp[np]= 	LengthByProp[np]+ l.m_Slacklength ;
        else{ // estimate. Only correct for a converged solution.
            CTensylNode &p1 = m_nodes[l.GetStartNode()]; // n1 = p1.m_nn;
            CTensylNode &p2 = m_nodes[l.GetDestNode()];  //n2 = p2.m_nn;
            LengthByProp[np]= 	LengthByProp[np]+ p1.m_pt.DistanceTo ( p2.m_pt);
        }
        totweight+= l.m_Slacklength *p.m_SelfWeight*p_scale;
    }
    buf.Format(" totalMaterialLength= %12.2f inch,weight= %11.1f lb(scale=%f)\n",
               totlength, totweight,p_scale);
    r+=buf;
    r+=ON_String(" breakdownbyproperty\n propNo \tlength(ft) \tpercent\n");

    for (itp = m_props.begin(); itp != m_props.end(); ++itp) {
        CTensylProp &p = itp->second;
        i = p.m_prop_no;
        if(LengthByProp[i]<0.00001)
            continue;
        buf.Format("    %d     \t%8.1f    \t %5.1f%% \t %12s  \t %12s\n", i, LengthByProp[i]/12.,LengthByProp[i]/totlength*100.0 ,p.s1.Array(),p.s2.Array()  );
        r+=buf;
    }

    if(fp) fprintf(fp,"%s",r.Array());
    return r;
}

