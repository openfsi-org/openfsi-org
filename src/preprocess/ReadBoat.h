#ifndef  _READBOAT_H_
#define  _READBOAT_H_
#include <QString>

EXTERN_C int ReadBoat(const QString qfilename, Graphic *gIn);
//EXTERN_C int compare_file_dates(const char *path1,const char *path2 );
EXTERN_C int check_is_boat(const QString qfilename);
#endif  //#ifndef  _READBOAT_H_


