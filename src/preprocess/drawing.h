#ifndef _DRAWING_HDR_
#define _DRAWING_HDR_
#include "griddefs.h"
EXTERN_C int Draw_Ruler(float p_x0,float p_y0,float p_z0,int p_ax,float p_ymax,float p_ymin);
//EXTERN_C int Draw_Orientation_Vector(RXEntity_p e, VECTOR* e1b,VECTOR*e2b,VECTOR*xlb,VECTOR*ylb,VECTOR*zlb,float *Chord);

EXTERN_C int Draw_Orientation_Vector(RXEntity_p e , const ON_3dPoint e1,const ON_3dPoint e2,const ON_3dVector xl,const ON_3dVector yl,const ON_3dVector zl,const float Chord);

EXTERN_C int Draw_Material_Ref(RXEntity_p e, const ON_3dPoint  e1b,const ON_3dPoint e2b,const ON_3dVector xlb,const ON_3dVector ylb,const ON_3dVector zlb );
EXTERN_C  int Draw_SC_Arrow(RXEntity_p e, const ON_3dPoint e1,const ON_3dPoint e2,const ON_3dVector xl,const ON_3dVector yl,const ON_3dVector zl );



 
extern HC_KEY PC_Insert_Arrow(const RXSitePt *q,const float ang, const char *text=NULL);
extern HC_KEY PC_Insert_Arrow(const ON_3dPoint q,const float ang, const char *text=NULL);
extern HC_KEY PC_Insert_Arrow(const VECTOR *q,const float ang, const char *text=NULL); 
 
#endif
