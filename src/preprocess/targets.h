/* targets.h  prototype for targets.c  march 1996 */

#ifndef _TARGETS_H_
#define _TARGETS_H_

#include "griddefs.h"


struct PC_GOALSEEK {
  int whatfor; /* 1 = target, 2 = variable */ 
  RXEntity_p e; 
  void*ptr;
  int Type;	  /* 1 for double*, 2 for float*,3 for offset 4 for Quantity* */
  double oldvalue,value,delta;
  double tol;
};
#define PCT_DOUBLE 	1
#define PCT_FLOAT  	2
#define PCT_OFFSET  	3
#define PCT_QUANTITY  	4
#define PCT_TARGET	5
#define PCT_VARIABLE	6
#define PCT_MEASURE	7
#define PCT_RXEXPRESSION	8


int Resolve_Goalseek_Card(RXEntity_p e);
int Get_Target_Ptr(struct PC_GOALSEEK *p,RXEntity_p ec,const char *o);
int matmult(double*a1,double*a2,double*res,int N);
int get_ecurr(double *ecurr,RXEntity_p *outs, int N);
int Put_v(double *delta,RXEntity_p *ins, int N);
int vmult(double *v,double*A,double*ecurr,int N);
double Target_Value(struct PC_GOALSEEK *g) ;
int GoalSeek(SAIL *sail, int *RE_PANEL_FLAG);
int Print_Target_Summary(char *filename,RXEntity_p *meas,int nm);
#endif  //#ifndef _TARGETS_H_


