#ifndef _WORDSRELAX_H_  //CHANGED from _WORDS_H_ to _WORDSRELAX_H_ to be WINDOWS compatible Thomas 05 03 04
#define  _WORDSRELAX_H_

//extern int Extract_Float (const RXSTRING &att,const RXSTRING &keyw,float*v) ;
//extern int Extract_Float (const char*att, const char*keyw,float*v) ;
//
//extern int Extract_Double  (const RXSTRING &att,const RXSTRING &keyw,double*v) ;
//extern int Extract_Double (const char*att, const char*keyw,double*v) ;
//
//extern int Extract_Integer (const RXSTRING &att, const RXSTRING &keyw,int*v) ;
//extern int Extract_Integer (const char*att, const char*keyw,int*v) ;


EXTERN_C int make_into_words(char *line,char ***wd,const char *sep_toks);
EXTERN_C int Replace_Words(char **line,int Index, char **win, int ls, const char*seps);
EXTERN_C int Replace_Word(char **line,int Index, const char *w, const char*seps);

extern char *Replace_String(char **s,const char *w1,const char *w2);
extern string Replace_String(string &s,const string w1,const string w2);
extern RXSTRING Replace_String(RXSTRING &s,const RXSTRING w1,const RXSTRING w2);
extern char *Replace_String_Multiple(char **s,char *w1,char *w2);
extern  string Replace_String_Multiple(string &s,const string w1,const string w2);
#endif  //#ifndef _WORDSRELAX_H_ 
