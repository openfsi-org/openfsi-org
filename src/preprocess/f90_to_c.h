#ifndef _F90_TO_C_H_
#define _F90_TO_C_H_

// many ae in saillist.h
// prototypes for ../f90/f90_to_c.f90 - a fortran source file
// careful.  No compiler checking for consistency.
// MUST be consistent with f90_to_c.f90   

class RXSiteBase;
#include "connect_f.h"  // some other definitions
extern "C" void   cf_analysis_prep();
extern "C" int cf_SolveSVD(double*a,int nr,int nc, double*b);
extern "C" int cf_solveDirect(int *ja, int*ia,
               double*values  , const int nnzero, const int nr, const int bw,
               int *perm, double* rhs,double* result  );
extern "C" double cf_curvecorrection(const double*c,const double* n1,const double*n2);//chord/arc given normals at ends.
extern "C" void   cf_calc_s_angles(const int sli, const int flag, double *total);
extern "C" void   cf_calc_g_angles(const int sli,const int flag, double *total) ;
extern "C" void  cf_printconnects(const int unit);

extern "C" void  cf_rlx_go(int *iexit,double*tmax, double*V_Damp,const char* flags)  ;
extern "C" void  cf_close_all_fortran_files(void);

extern "C" void  cf_applynewtransform(const int sli, double m[4][4], double im[4][4]);

extern "C"  int  cf_make_nodal_values(const int sli, float*p,float *h,int *nc,float*nsds,float*cmin, float*cmax);

extern "C" void  cf_removesail 		(const int n,		int*Error);  
extern "C" void 	cf_getnextsail		( int* n,		int *err);
extern "C" int 		cf_setactive		(const int n,	const int h,	int *err);// a non-zero means TRUE
extern "C" int 	    cf_sethoisted		(const int n,	const int h,	int *err);
extern "C" int 		cf_setsailptr		(const int n,	SAIL**h,int *err);
EXTERN_C  int 		cf_setsailtype		(const int n, const char*s,const int  l3, int *err);
extern "C" int 		cf_ishoisted		(const int n, int *err);
EXTERN_C int 		cf_isactive			(const int n, int *err);
EXTERN_C class RXSail* 	cf_getsailpointer	(const int n, int *err);
EXTERN_C void 		cf_printsailstructures	(const int u);




extern "C" int  cf_create_gle(const char *m,const int lm, //	CHARACTER (len=lm) , INTENT (in)     ::  m
				   const void *cptr,
				   const int nn,
				   const double *Kmat,
				   const int *nodes,
				   const double *F0, const double *X0,
				   const int p_has_tt,
				   char names[64][64]) ; //	CHARACTER (len=64) , INTENT (in)   ,dimension(64)       ::  names
// msw doesnt like char names[][]
// from sailliat.f90
 extern "C"  void XtoLocal (const int sli, const double *xin, double* xout) ;
 extern "C"  void XtoGlobal(const int sli, const double *xin, double* xout) ;
 extern "C"  void VtoLocal (const int sli, const double *xin, double* xout) ;
 extern "C"  void VtoGlobal(const int sli, const double *xin, double* xout) ;

 extern "C"  void cf_SetTransMatrix (const int sli, const double *xin) ;
 extern "C"  void cf_SetInvTransMatrix (const int sli, const double *xin) ;  

  extern "C" int   cf_GetTransMatrix(const int sli,float *m ); // we'll have to provide memory for the returned value.
  extern "C" int cf_GetInvTransMatrix (const int sli,float *m );


 extern "C" int cf_drawfbeams(const int sli,  RXGRAPHICSEGMENT seg);
 extern "C" int cf_drawlinks(const int sli,  RXGRAPHICSEGMENT seg);
 extern "C" void cf_plotps(const int sli,RXGRAPHICSEGMENT seg ); // checked
 extern "C" void cf_plot_principal_stiff(const int sli,RXGRAPHICSEGMENT seg ); // checked
 extern "C" void cf_plot_ref_direction(const int sli,RXGRAPHICSEGMENT seg ); // checked
 extern "C" void cf_plotpstrain(const int sli,RXGRAPHICSEGMENT seg );

 extern "C" int cf_one_tri_stress_op(const int n,const int sli, double*s,double*e,double*p,double*c,double*pst);

EXTERN_C int  cf_runstress(double* tcon, double*vdamp,char*flags );
EXTERN_C int  cf_reserveforjanet(const int sli,const int n) ;
EXTERN_C int  cf_codetest(double* tcon, double*rdamp,double*vdamp,char*flags );
EXTERN_C int  cf_elementTest(double* a, double*b,double*c);
EXTERN_C int  cf_codetestnoargs (  );
extern "C"  int cf_hookalltris(void);

extern "C"  int cf_findhoistedsailbyname(const char*name,const int lname,class RXSail **ptr  );



extern "C"  int cf_remove_tri3(const int sli,const int n);
extern "C"  int cf_insert_tri3(int p_sli,int p_N ,void*ptr);
extern "C"  int cf_insert_flink (const int p_sn,const int p_N ,const int p_n1,const int p_n2,
								 const double p_ea, const double p_zi, const double p_ti, const double p_mass, const void* p_ptr) ;

extern "C" double cf_getflinktension(const int sli, const int n);
extern "C" int cf_setflinklength(const int sli, const int n, const double zi);
extern "C" int cf_setflinkproperties(const int sli, const int n, const double ea,const double ti,const double mass);// returns 1 if something is different
extern "C" int cf_getflinkproperties(const int sli, const int n, double *ea,double *ti, double*zi, double *mass); // returns 0 if failed
extern "C" int cf_flinkcount(const int sli  );

extern "C" int pf_eigensolution3x3(double *a,double *w,double *v);// result(err) ! double a[9],w[3],v[9]

extern "C" int cf_write_nastran(const int sli,const char *fname,const int L);  
extern "C" int cf_get_coords(const int sli,const int n,double*v);
 
extern "C"  int cf_get_raw_residual (int sli,int n,double*v); // n is global node number
extern "C"  int cf_get_dof_values( int*n,int*dim,double*t); 
// n is global node number
// dim. INPUT the allocated size of array t
//	output	the number of elements placed in t
// t	numbers
// for a spline, very surprising if dim is returned other than 1

// for reference 
//EXTERN_C int cf_get_residual_(int*n,double*v);
// will call GetOneMaster_Reaction which in turn calls calls cf_get_raw_residual_
// So it is safe to call from any function EXCEPT GetOneMaster_Reaction 
//

extern "C"  void cf_model_hardcopy(void);


// STRING METHODS *************************

//extern "C" int cf_Hook_String_Memory(const int sli, const int n, void *d,int *ne,int* se,int* rev ,double* tq_s,int *tl,char *text);

extern "C" int cf_Hook_String_Memory2(const int sli, const int n, void *d,const int ne,
									  int** f_septr ,int**f_revptr, double** f_tqptr,
									  int *tl,char *text);

extern "C"  int cf_get_tension_range(int*model,double*tmax,double*tmin) ;
extern "C"  int cf_get_one_string_tension(int *model,int*i,double*t);
extern "C"  int cf_get_one_string_listing(const int sli,const int i, char*buf);

extern "C"  int cf_createfortranstring( const int sli, int *err);  // returns the index (ie from nextfree
extern "C"  int cf_removefortranstring(const int sli, const int n, int*err);

extern "C"  int  cf_removebeamelement(const int sli, const int n, int*err);
extern "C" int cf_createbeamelement( const int sli, const int n1,const int n2,
				const double beta,const double zi,const double ti,const double mass,
                const double *emp, const int axLoadFlag,
				const char*name, const int lenName,
				int *err); // returns the index (ie from nextfree
extern "C" int cf_changebeamelement( const int sli, const int n,
                const double beta,const double zi,const double ti,const double mass,
                const double *emp, const int axLoadFlag,
                const char*name, const int lenName,
                int *err); // returns the index (ie from nextfree

extern "C" int cf_one_initial_beamaxismatrix(const int sli, const int n, const double *s,const double *e);
extern "C" int cf_one_beamelementresult(const int sli, const int n, double* f); // f is 10 long
extern "C" int cf_one_beamelementtrigraph(const int sli, const int n, double* f); // f is 9 long

extern "C"  int cf_createfortranbatten( const int sli, int *err);  // returns the index (ie from nextfree
extern "C"  int cf_removefortranbatten(const int sli, const int n, int*err);
extern "C"  int cf_initialisebatten(const int sli, const int n,const int count,const void*ptr,const char*text,int*err);
extern "C" int cf_fillbatten( const int sli, const int n ,const double zi,const double trim ,
		const double ea, const int Islide,  const int*const se, const int*const rev,const double* const ei); // sets bo and xib to zero

extern "C" int cf_setbattentrim(const int sli, const int n, const double x);

extern "C"  void cf_get_zlink0bysli_(const int sli,int *ed,float *elen,int *err);

extern "C"  void cf_get_gvalue(const int sli,const int nodno,float *gval,int *err) ;
extern "C"  void cf_get_svalue(const int sli,const int nodno,float *gval,int *err) ;
 
extern "C" void  cf_setallsailstrings(int* n,char*Mname,int*l1,char*MIK,int*, char*sname,int*,char*torep,int*, int* Err);
 
extern "C"  void cf_initallsailstrings (int *sli, int*err);
extern "C"  void cf_updateallteesbysv (void);
// ONLY call from the RX_FESite constructor/destructor
extern "C"  int cf_createfortrannode(const int sli,const int n,const class RX_FESite* ptr,int *err);
extern "C"  int cf_removefortrannode(const int sli,const int n,int *err);
extern "C"  int cf_FillFNode(const int sli,const int n, const double *x, int *err); // X is in model space

extern "C"  int cf_insert_edge(int p_sli,int p_N ,void*ptr);
extern "C"  int cf_remove_edge(const int sli,const int n);

extern "C" class RX_FESite*  cf_get_edgenode( const int sli,const int n, const int k);
extern "C"  double cf_get_edgelength(const int sli,const int n);
extern "C" int    cf_set_edgelength(const int sli,const int n,const double p_zi);
extern "C" int    cf_set_edgeflag(const int sli,const int n,const int i, const double f);
extern "C" int    cf_set_edgenodes(const int sli,const int n,const int n1,const int n2);

extern "C"  void cf_set_trace( const int sli, const int nn,const int i);
extern "C"  void cf_set_nodalload(const int sli, const int i, const double *p);
extern "C"  void cf_get_nodalload(const int sli, const int i, double *p);
extern "C"  int cf_issixnode(const int sli, const int i );
extern "C"  int cf_makesixnode(const int sli,const int n,const int n2);
extern "C"  int cf_setnodalfixing (const int sli,const int n,const char*buf,const int L);

extern "C"  int  cf_update_dofo_origin(const int sli,const int dofoN, double x[3] )  ;
extern "C"  int  cf_putftriproperties(const int sli,const int n, 
							const int* node,const int* m_iEd, 
							const double m_refangle,const double  area, 
							const double * prestress,
							const double*mx	,						 
							const int creaseable,
							const void *ptr,  int* err); 

extern "C"  int cf_putpressure   (const int sn, const int NN,const double p, int*err)  ;
extern "C"  double  cf_getpressure    (const int sn, const int NN, int*err) ;
extern "C"  int cf_IsTriUsed    (const int sn, const int NN, int*err) ;
extern "C"  int cf_TriGlobalStressTensor (const int sn, const int NN,double* t, int*err) ;
extern "C"  int cf_TriGlobalStrainTensor (const int sn, const int NN,double* t, int*err) ;
extern "C"  int cf_TriGlobalStiffnessTensor (const int sn, const int NN,double* t, int*err) ;

extern "C"  int  cf_TriGlobalAxisVector(const int sn, const int NN,double* t, int*err) ;


extern "C"  int cf_delete_dofo (const int sli,const int dofoN);
extern "C"  int cf_create_dofo_rigidbody (const int sli,const int nn, int*nodelist,const class RXObject *e, const char*att, const int attlen);
extern "C"  int cf_test (const int*vv ,const int n);

// from sparskit
 extern "C"  void cf_aplb1(const int nrow,const int ncol,const int job,
      const int nnza,const double *a,
      const int* ja,const int*ia,
      const int nnzb, const double *b,
      const int* jb,const int*ib,
      double *c,
      int* jc, int* ic,
      const int nzmax, int *ierr);

 extern "C"  void cf_transp (int*nrow,int*ncol,double *a,int*ja,int*ia,int*iwk,int*ierr) ;

#endif

