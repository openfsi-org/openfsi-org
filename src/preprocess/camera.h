#ifndef _CAMERA__HDR__
#define   _CAMERA__HDR__

#include "opennurbs.h"
#include "griddefs.h"

 #ifdef linux
	#define _NOT_MSW
#endif

#define SIDEVIEW 1
#define HELMSMAN 2
#define COACH 3
#define IPOINT 4
#define SEAGULL 5


struct CAMERA {
	ON_3fPoint pos;
	ON_3fPoint target;
	ON_3fVector Up;
	float width;
	float height;
	char *projection;
	char *name;
	ON_3fPoint  light_pos;
	char *light_colour;
};
typedef struct CAMERA Camera;

EXTERN_C int nCams;
EXTERN_C RXEntity_p camPlist[30];

 #ifdef _X
	int SetHardCamera(RXEntity_p cEnt,Graphic *g);
 #endif

int Resolve_Camera_Card(RXEntity_p e);
int Record_Default_Camera(void);

 #endif//#ifndef _camera_h_

