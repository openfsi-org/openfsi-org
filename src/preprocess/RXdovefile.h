#ifndef _RXDOVEFILE_H_
#define _RXDOVEFILE_H_
// RXdovefile.h: interface for the RXdovefile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RXDOVEFILE_H__C835E8EA_AAFA_41C8_9066_CBD33326156E__INCLUDED_)
#define AFX_RXDOVEFILE_H__C835E8EA_AAFA_41C8_9066_CBD33326156E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "RXMathlink.h"

#include "rxON_Extensions.h"	

#define RXD_MATH_DOVE			4 
#define RXD_USE_LAYERTST		8 // means get alpha from interpolating the layertst grid
#define RXD_USE_DOVESURFACES	16 // means get the matrix from evaluating the 6 surfaces
#define RXD_USE_LAYERSURFACES	32 // means use dovestructurebylayersurfs
#define RXD_STACK_DOVE			64

class dovestructure;
class RXdovefile
{
public:
	int Print(FILE*fp);
	ON_String rewrite_doveline(void);
	int Save(const char*p_filename=0);
	int m_dovetype;
	int Open(const char*p_path=NULL);
	RXdovefile(char*file, char*func, RXEntity_p e);
	ON_String m_DoveFunctionName;
	int Evaluate(const float p_u,const float p_v, ON_Matrix &p_m,ON_3dVector &p_x);
	int Evaluate(const double p_u,const double p_v, ON_Matrix &p_m,ON_3dVector &p_x);
	RXdovefile();
	virtual ~RXdovefile();
	dovestructure *m_amgdove;

private:

	RXEntity_p  m_ent;
#ifdef MATHLINK
	RX_MathSession m_s;
#endif
	rxON_String m_filename;
};

int Resolve_Dove_File(RXEntity_p e);
#endif // !defined(AFX_RXDOVEFILE_H__C835E8EA_AAFA_41C8_9066_CBD33326156E__INCLUDED_)

#endif
