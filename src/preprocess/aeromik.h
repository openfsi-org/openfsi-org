 #ifndef _AEROMIK_H_
#define _AEROMIK_H_


#include "griddefs.h"
//#error("No more aeromik ")


 struct PC_AEROMIK {
   char*mikfilename;
   double top,bottom;
   int nsects,fair;
   char**files;
   char**shapefiles;
   char **sailnames;
   int Kermarec;
   char*before_script;
   char*after_script;
};

EXTERN_C int Aeromik_V(SAIL *theSail, RXEntity_p ae, const RXEntity_p pe,double u, int level,float*v);

int Make_Aeromik_Geometry(RXEntity_p e);
int Resolve_Aeromik_Card(RXEntity_p e);

#endif  // #ifndef _AEROMIK_H_

