/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 * Mar 2004     g_Appwind_Untransformed  for Relax LL
 * April 2003  strcasecmp
 *  june 2000 rotate by g_AWA
 *  19.6.96   we STRDUP the return valie of getfilename
 *  28.12.94		g_Leeway introduced  ( +ve is to PORT)
      and error messages re-arranged
     and intermediate variables now double
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 *      20.11.94                AKM             asks for wind file if wind not found.
 */

#include "StdAfx.h"
#include "RelaxWorld.h"
//#include "traceback.h"

#ifndef WIN32
#define _DIRSEP_	"/"
#else
#define _DIRSEP_	"\\"
#endif 

#include "files.h"

/*
  
  Apparent wind routine
  
  called with height 'z'
  sets u,v,w based on
  wind velocity profile
  boat heading
  boat speed
  
  */
#ifdef NEVER
#include "global_declarations.h"
#include "files.h"
#include "appwind.h"


/*
  Analysis August 2012
  This is really nasty old C code

  function appwind_ provides the free-stream wind vector for:


  In CalcNow, we use ot to get AWA, aws then we set AWA to 0 (!)
  In drawwind we display the wind vector as f (z)
  in IntPress we call it to get a reference windspeed to denormalise the cps
  in   RelaxWorld::Post_Wind_Details( )  lots of nasty things happen


  Our problem is that we can specify a wind vector (u,v,w)
  OR we can specify a wind triangle.
  and its not clear how we manage the OR.
  */

#define r2d ((float)57.29577951)

void appwind_(float *z,float *u, float *v, float *w)
{

    float au,av,aw;

    static QString Old_Wind_Name;

    static float*zz=NULL;
    static float*vv=NULL;
    static float*th=NULL;
    static int n = 0;
    float theta,speed,Vunfactored;
    au =  g_PansailIn.Wind_u;
    av =  g_PansailIn.Wind_v;
    aw =  g_PansailIn.Wind_w;
    if(g_wind_name.isEmpty()) {
        g_wind_name = QString(g_CodeDir)+ QString(_DIRSEP_)+QString("wind.txt");
       // cout<<"setting windfile to '"<<qPrintable(g_wind_name)<<"'"<<endl;
    }
    *u = au;		   /* defaults*/
    *v = av;
    *w = aw;
    if(Old_Wind_Name.length()) {
        if(Old_Wind_Name!=g_wind_name) {
            cout<<" windname was "<< qPrintable(Old_Wind_Name )<<" is "<<qPrintable(g_wind_name)<<endl;
            RXFREE(zz); RXFREE(vv);RXFREE(th);
            zz=NULL; vv=NULL; th=NULL;
            n=0;
            Old_Wind_Name = g_wind_name;
        }
    }
    else
        Old_Wind_Name=g_wind_name;


    if(zz==NULL ) {
        FILE *cycleptr;
        float p,q,r;

        if ((cycleptr = RXFOPEN(qPrintable(g_wind_name),"rt"))==NULL){
            g_wind_name.clear();
#ifndef _WINDOWS_NEVER   // was currentDir till Aug 22 2003
            g_wind_name = getfilename( NULL ,"w*.txt","Open WIND file",g_CodeDir,PC_FILE_READ);
            if(!g_wind_name.isEmpty()) {// was last Graph->topLevel
                cycleptr = RXFOPEN(qPrintable(g_wind_name),"rt");
            }
#else
            cycleptr=NULL;
#endif
            if(!cycleptr) {
                //error("Trying default wind file in code/wind.txt",1);
                g_wind_name = QString("$R3ROOT/code/wind.txt");
                cycleptr = RXFOPEN(qPrintable(g_wind_name),"rt");
                if(!cycleptr) {
                    return ;
                }
            }
        }

        while (3== fscanf(cycleptr,"%f %f %f",&p,&q,&r)){

            zz=(float*)REALLOC(zz,(n+1)*sizeof(float) );
            vv=(float*)REALLOC(vv,(n+1)*sizeof(float) );
            th=(float*)REALLOC(th,(n+1)*sizeof(float) );

            zz[n]=p;
            vv[n]=q;
            th[n]=r;
            n++;
        }
        FCLOSE(cycleptr);

    }

    if(n<2) {g_World->OutputToClient(" BAD wind file. Need more points ",2); RXFREE(zz); zz=NULL; return ;}

    WindSpeed(&g_PansailIn.Reference_Length,zz,th,vv,n,&theta,&Vunfactored);  /* unfactored speed at MH */
    WindSpeed(z,zz,th,vv,n,&theta,&speed);

    /*   printf("(APPWIND) ref V=%f at %f  Our Vraw = %f at %f\n",Vunfactored,g_PansailIn.Reference_Length,speed,*z);*/
    speed = speed * g_Vwind/Vunfactored;
    /*   printf("so  corrected TWS at %f is %f (vwind=%f)\n" *z,speed,g_Vwind); */

    { double U,V;      /* wind components in global axes */
        double vbx,vby;  /* boat vector in wind axes */
        double cosH,sinH;
        double vappwx,vappwy; /* apparent wind in wind axes */

        U =  speed * cos(theta);
        V =  speed * sin(theta);

        cosH = cos(g_Heading/r2d);
        sinH = sin(g_Heading/r2d);

        vbx =  g_Vboat*cosH ;
        vby = -g_Vboat*sinH ;

        vappwx= vbx+U;
        vappwy= vby+V;
        if(fabs(g_Leeway) > 22.5) {
            g_World->OutputToClient(" unreasonable leeway value",1);
            if(g_Leeway>0.0) g_Leeway= 22.5;
            if(g_Leeway<0.0) g_Leeway=-22.5;
        }

        *u =(float) ( cosH * vappwx - sinH * vappwy);
        *v= (float) ( sinH * vappwx + cosH * vappwy - tan(g_Leeway/r2d)*g_Vboat);
        *w= (float) 0.0;
        *u = *u + (float)g_PansailIn.Pitch_Velocity*(*z);
        *v = *v - (float)g_PansailIn.Roll_Velocity*(*z);

        if(  !g_Appwind_Untransformed){
            float ubuf,vbuf;
            ubuf = *u;
            vbuf = *v;
            *u = ubuf * cos(g_AWA) - vbuf*sin(g_AWA);
            *v = vbuf*cos(g_AWA)   + ubuf* sin(g_AWA);
        }


    }
    return;
}

int appwind_get_ref_length_(double *a){
    *a= (double) g_PansailIn.Reference_Length;
    return 0;
}

int WindSpeed(float *z,float*zz,float*th,float*vv,int n,float*theta,float*speed) {
    double factor;
    int i, l=n-2;

    for(i=0;i<n-1;i++) {
        if(zz[i] > *z) { l=i-1;break;}	  /* i is last one just under *z */
    }

    if(l<0)l=0;
    /* interpolate from i to i+1 */

    factor = (*z-zz[l])/(zz[l+1] - zz[l]);
    *theta = -(th[l]*(1.0-factor) + th[l+1] * factor) /r2d;
    *speed = vv[l]*(1.0-factor) + vv[l+1] * factor;

    return(0);
}
#endif
