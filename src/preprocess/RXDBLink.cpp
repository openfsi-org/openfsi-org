#include "StdAfx.h"
#include <QtCore>
#include "RXGraphicCalls.h"
#ifdef HOOPS
#include "hc.h"
#include "HTools.h"
#endif


#ifndef NOMYSQL
#include "RelaxWorld.h"
#include "stringutils.h"
#include "summary.h"
#include "words.h"
#include <iostream>
#include "rxmainwindow.h"
#include "rxdblinkthread.h"
#include "RXDBLink.h"

using namespace std;

RXDBLinkMySQL ::RXDBLinkMySQL (void):
    m_PleaseEndPump(false),
    m_PumpThrd (0),
    conn(0),
    dbgPrint(0)
{
    QMutexLocker locker(&m_cs);
    this->conn = mysql_init(NULL);

}
RXDBLinkMySQL::~RXDBLinkMySQL (void){

    if(m_PumpThrd)
    {
        m_PleaseEndPump =true;
#ifdef linux
        delete this->m_PumpThrd;

        // dont. it hangs. WHY????????   pthread_join(this->m_PumpThrd,NULL  );
#else
        //  whatever the windows equivalent is of pthread_join(this->m_PumpThrd,NULL  );
#endif
    }

    Close ();
}

int RXDBLinkMySQL::Open(const char*f,const char*what) // return >0 if OK
{int rc=1;
    // f will be   localhost$relax$password$relax$input
    // ie machine user password database table.

    //cout <<"there is the mirror table which has the role of the itemlist."<<endl<<endl;
    //cout<< "which is published so others can read/write it"<<endl;
    //cout<<" we use a threaded process keep it in synch with the app's internal data "<<endl;
    vector<string> wds=rxparsestring(f,"$");
    if(wds.size()<5) {
        cout<<"(database connection) error in format of "<<f<<endl;
        cout <<" expecting something like 'localhost$username$password$ofsidb$mytable'" <<endl;
        return 0;
    }
    m_server=wds[0] ;
    m_user =wds[1] ;
    m_password=wds[2] ;
    m_database=wds[3];
    m_table = wds[4];
    {
        QMutexLocker locker(&m_cs);
        /* Connect to database*/
        if (!mysql_real_connect(conn, m_server.c_str(),
                                m_user.c_str(), m_password.c_str(), m_database.c_str(), 0, NULL, 0)) {
            cout<<endl<<"failed to open DB: '"<<m_database.c_str()<< "' on server: '"<<m_server.c_str();
            cout<<"', user ='"<< m_user.c_str()<< "', pw='"<<m_password.c_str()<<"'"<<endl;
            cout<< mysql_error(conn)<<endl;
            return(0);
        }
    }
    DBRESULT r; unsigned int err;
    rc= QQuery(string("USE ")+this->m_database,r,&err);
    if(rc<0) return 0;

    if(!IsMirror())
        return (rc>=0);

    string q;
    q=string("SET sql_mode='STRICT_ALL_TABLES'");
    QQuery(q,r,&err); print_result(r);

    q=string(" DROP TABLE IF EXISTS  ") + string(RXMIRRORTABLE);
    QQuery(q,r,&err); print_result(r);

    // create the mirror table
    q=string("CREATE TABLE IF NOT EXISTS ")+string(RXMIRRORTABLE)
            +string( "(header VARCHAR(64), text VARCHAR(256), type integer DEFAULT 0,flag integer DEFAULT 0 )");
    // , id INT UNSIGNED NOT NULL AUTO_INCREMENT,PRIMARY KEY(id)

    QQuery(q,r,&err); print_result(r);

    // start the thread which pumps the mirror-table's values to the App

#ifdef WIN32
    assert(0); m_PumpThrd=_beginthread( RXDBLinkThreadProcess, 0, this );
#else
    m_PumpThrd = new RXDBLinkThreadLibMySQL(NULL, this );
    m_PumpThrd->start();

#endif
    this->post_summary_value("Run_Name","Undefined");
    return (rc>=0);
    return 1;

}

//static
#ifdef linux
void*RXDBLinkMySQL::RXDBLinkThreadProcess( void* pParams )
#else
void RXDBLink::RXDBLinkThreadProcess( void* pParams )
#endif
{
    RXDBLinkMySQL *db = (RXDBLinkMySQL * ) pParams;

    string q =  string( "SELECT * FROM " ) + string(RXMIRRORTABLE );
    DBRESULT r,rrr;
    int rc ; unsigned int err;
    while (! db->m_PleaseEndPump )
    {
        rc=db->QQuery(q,r,&err); // insulated
        /*
here we will compare the results with the previous set. 
If something in the table has changed we do a ExecuteDelayed() on it; 
which should then apply_summary_value it. 

special case of 'execute'  'string'.  
the string gets passes to the command interpreter
and the record gets deleted from the mirror table (or else it would repeat for ever)

*/
        string old;
        for(size_t  i=0;i<r.size();i++) {
            string & h = r[i][0];
            string & d = r[i][1];
            //            if(h=="execute" && !d.empty())
            //            {
            //                string q ; unsigned int err;
            //                q=string("DELETE FROM ") + string(RXMIRR ORTABLE )
            //                        +string("WHERE `header` = 'execute'");
            //                rc=db->QQuery(q,rrr,&err);
            //                g_World->ExecuteDelayed (d); // NASTY - this system is disabled
            //            }
            //            else
            {
                if(	db->m_lastbuffer.find(h)!=db->m_lastbuffer.end() ){ // the old exists
                    old = db->m_lastbuffer[h];
                    if(old==d)
                        continue;
                }
                db->m_lastbuffer[h]=d;
                g_World->ExecuteDelayed ("apply: "+h+" : "+d);
            }
        }
#ifdef linux	
        assert(0); //SLEEPABIT(5000000000)
    } //while
    return 0;
#else 
        Sleep(5000);
    } //while
#endif

}//Thread

int RXDBLinkMySQL::Close (){
    int rc=0;
    rc= FlushMirrorTable();// gets rid of the mirror table
    if(!conn)
        return 0;
    if((IsMirror()) && this->conn->db ){
        DBRESULT r;
        string q ; unsigned int err;
        q=string("DROP TABLE ") + string(RXMIRRORTABLE);
        QQuery(q,r,&err); print_result(r);
    }
    if( this->conn->db) {
        QMutexLocker locker(&m_cs);
        mysql_close(conn);
        conn=0;
    }

    return rc;
} // the delete
int RXDBLinkMySQL::FlushMirrorTable ()
{
    int rc=0;
    if(!IsMirror() )
        return rc;
    if(!this->conn->db)
        return 0;

    DBRESULT r;
    string q ; unsigned int err;
    q=string("DELETE FROM ") + string(RXMIRRORTABLE );
    rc=QQuery(q,r,&err);
    //print_result(r);
    m_lastbuffer.clear();
    return rc;
}  


int RXDBLinkMySQL::QQuery(const std::string &q, DBRESULT &rr,unsigned int*err)
{
    int rc=0;
    *err=0;
    rr.clear();
    QMutexLocker locker(&m_cs);
    assert(this->conn->db);
    if (mysql_query(conn, q.c_str())) {
        *err = mysql_errno(conn);
        fprintf(stderr, " on query '%s' err=%d  errmess is\n%s\n",q.c_str(),*err, mysql_error(conn));
        return(-1);
    }
    *err = mysql_errno(conn);
    MYSQL_ROW row;
    MYSQL_RES *res = mysql_use_result(conn);

    if(!res) {
        return 0; // its OK for there to be no result
    }
    int r=0, c,nc=0;
    while ((row = mysql_fetch_row(res)) != NULL)
    {
        nc = mysql_num_fields(res);
        for(c=0;c<nc;c++)
        {
            const char*lp = row[c];
            if(lp)
                rr[ r][ c ] = string(row[c]);//printf("\t%s", row[c]);
            else
                rr[ r][ c ] = string("null");
            rc++;
        }
        r++; //cout<< "\n"<<endl; //rr+=string("\n") ;
    }

    /* Release memory used to store results*/
    mysql_free_result(res);
    return rc;
}



// low-level work-horse insert and/or update the value of a DB item
int RXDBLinkMySQL::post_summary_value(const char*label,const char*pvalue){
    int rc=0,  	l_type,l_flag ;
    if(!label) return 0;
    const char*value="(null)";
    if(pvalue)
        value = pvalue;
    assert( IsMirror());
    DBRESULT r;
    string q ; unsigned int err;
    //q=   string("DESCRIBE ") + string(RXMIRROR TABLE) ;
    //	QQuery(q,r); print_result(r);

    //if the entry 'label' doesnt exist
    // INSERT INTO pet VALUES ('Puffball','Diane','hamster','f','1999-03-30',NULL);
    //else
    //UPDATE pet SET birth = '1989-08-31' WHERE name = 'Bowser';

    q=   string("SELECT * FROM ") + string(RXMIRRORTABLE)
            +string(" WHERE header = '") +string(label)+string("'") ;
    rc= QQuery(q,r,&err); print_result(r);

    if(rc>0) {
        q=   string("UPDATE ") + string(RXMIRRORTABLE)
                +string(" SET     text = '") +string(value)
                +string("' WHERE header = '") +string(label)+string("'") ;
        rc=QQuery(q,r,&err); print_result(r);
    }
    else if(rc==0){
        // INSERT INTO pet VALUES ('Puffball','Diane','hamster','f','1999-03-30',NULL);
        l_type   = Summary_Type(label,&l_flag);
        stringstream qs;
        qs<<"INSERT INTO "<< RXMIRRORTABLE
         <<" VALUES ( '"<<label
        <<"' , '"<<value
        <<"',"<<l_type<<","<<l_flag<<") "  ;
        //q=   string("INSERT INTO ") + string(RXMIRROR TABLE)
        //	+string(" VALUES ( '") +string(label)
        //	+string("' , '") +string(value)
        //	+string("',")+l_type+string(",")+l_flag+string(") ") ;
        rc= QQuery(qs.str(),r,&err); print_result(r);
    }
    return rc>=0;
}
int RXDBLinkMySQL::post_summary_value( const std::string &label,const std::string &value){
    return post_summary_value(label.c_str(),value.c_str());
}
// we just post the filename to the mirrortable. It gets placed in the log DB as a blob

int  RXDBLinkMySQL::PostFile( const QString &label,const QString &fname)
{
    int rc=0,  	l_type=SUM_BLOB,l_flag=SUM_OUTPUT ;
    if(label.isEmpty()) return 0;

    assert( IsMirror());
    DBRESULT r;
    string q ; unsigned int err;

    q=   string("SELECT * FROM ") + string(RXMIRRORTABLE)
            +string(" WHERE header = '") + label.toStdString()+string("'") ;
    rc= QQuery(q,r,&err); print_result(r);

    if(rc>0) { // the entry exists
        q=   string("UPDATE ") + string(RXMIRRORTABLE)
                +string(" SET     text = '") +fname.toStdString()
                +string("' WHERE header = '") +label.toStdString()+string("'") ;
        rc=QQuery(q,r,&err); print_result(r);
    }
    else if(rc==0){
        // INSERT INTO pet VALUES ('Puffball','Diane','hamster','f','1999-03-30',NULL);
        stringstream qs;
        qs<<"INSERT INTO "<< RXMIRRORTABLE
         <<" VALUES ( '"<<label.toStdString()
        <<"' , '"<<fname.toStdString()
        <<"',"<<l_type<<","<<l_flag<<") "  ;

        rc= QQuery(qs.str(),r,&err); print_result(r);
    }
    return rc>=0;
}

// commits the contents of the RXDBI to file, called from Calc Now.
int RXDBLinkMySQL::Write_Line(class RXMirrorDBI*pmirrorSource)
{
    /* Logic
1) we can cold-start a table from here;
 so if the table doesnt exist, we create it
 The minimal table has a unique, auto incrementing ID
*/
    /*
  thESE work in command-line
 insert into output SET leeway=4,course=092, Run_Name="eric";
  insert into output(VMC) VALUES(7.1);
  INSERT INTO OUTPUT(RUN_NAME)VALUES('GOATINBOOTS');
*/

    class RXDBLinkMySQL*mirrorSource =dynamic_cast<class RXDBLinkMySQL*>(pmirrorSource);
    if(!mirrorSource) mirrorSource=this;
    assert(m_DBtype &RXDB_OUTPUT);
    int rc=0; unsigned int err;
    DBRESULT r_mirror,r2,rr3,r1;
    string q,swhat,sval,r_type,r_flag ;
    bool tableexists=false;

    q=   string("SHOW TABLES ")  ;
    rc= QQuery(q,r1,&err);
    int r,nr = r1.size();

    for(r=0;r<nr;r++) {
        if(r1[r][0] == this->m_table)
        {tableexists=true; break;}
    }
    if(!tableexists) {
        q=   string("CREATE TABLE ")  + this->m_table
                + " ( id INT UNSIGNED NOT NULL AUTO_INCREMENT,PRIMARY KEY(id)) ";
        rc= QQuery(q,r1,&err); print_result(r1);
    }
    // r1 will be the contents of the mirror table
    q= string("SELECT * FROM ") + string(RXMIRRORTABLE) ;//
    rc= mirrorSource->QQuery(q,r_mirror,&err); print_result(r_mirror);
    nr = r_mirror.size();

    // check that the columns exist in the DB table
    for(r=0;r<nr;r++) {
        r_type = r_mirror[r][2];
        swhat = r_mirror[r][0];
        q="SELECT `"+swhat+"` FROM "+this->m_table;
        rc= QQuery(q,r2,&err); print_result(r2);
        if(err==1054) {
            if (r_type == "4" )
                q = "ALTER TABLE `"+this->m_table +"` ADD `"+ swhat+"` BLOB DEFAULT NULL";
            else
                q = "ALTER TABLE `"+this->m_table +"` ADD `"+swhat+"` VARCHAR(64) DEFAULT NULL";

            rc= QQuery(q,rr3,&err);print_result(rr3);
        }
    }// for r

    // now we can fill in
    swhat = " (`";
    sval = " VALUES( '";//note DIFFERENT quotes
    for(r=0;r<nr;r++) {
        r_type = r_mirror[r][2];
        r_flag= r_mirror[r][3];
        if (r_type == "4" ) {//blob
            std::string bd = this->GetBlobData(r_mirror[r][1]);
            swhat += r_mirror[r][0];
            sval += bd;
        }
        else {
            swhat += r_mirror[r][0];
            sval += r_mirror[r][1];
        }
        if(r!=nr-1) swhat+="` , `";
        if(r!=nr-1)  sval +="' , '"; //note DIFFERENT quotes
    }
    swhat += "` ) "; sval+= "' ) ";
    // swhat may have a " , ``" to remove
    //sval a ", ''"
    r= rxreplacestring( swhat,", ``"  , " ");
    r= rxreplacestring(sval,", ''"  , " ");
    q="INSERT INTO "  + this->m_table + swhat + sval; // try to insert the whole record
    rc= QQuery(q,r2,&err);print_result(r2);

    if(rc<0) { //error, but we know the columns  exist. lets dump the table
        //  this->dbgPrint=1;
        q = string("DESCRIBE " )+  this->m_table;
        rc= QQuery(q,rr3,&err);      print_result(rr3);
    }
    //else cout<<"it looks like we added line OK "<<endl<<what<<endl<<val<<endl;

    // wrapup
    q = string("SELECT " )+string(" * ")
            +string(" FROM ") + this->m_table;
    rc= QQuery(q,r2,&err);print_result(r2);
    this->dbgPrint=0;

    return rc;
} 
std::string  RXDBLinkMySQL::GetBlobData(const std::string &f)
{
    QByteArray theData;
    QFile thefile;
    QString ttt;
    QString fname(f.c_str());
    theData.clear();
    thefile.setFileName( fname);
    if(thefile.open(QIODevice::ReadOnly))
    { theData=thefile.readAll(); thefile.close();}

    if(fname.contains(".jpg"))
        ttt=QString(theData.toBase64());
    else if(fname.contains(".zip"))
        ttt=QString(theData.toBase64());
    else if(fname.contains(".gz"))
        ttt=QString(theData.toBase64());
    else
        ttt=QString(theData);
    return ttt.toStdString();
}
// GetBlobData returns the text that we want to store in the database
// if its a text file, we just return its contents
// else we return the base64 of the files contents

// When we get the blob from the DB it is a base64
// we go
//QByteArray text = QByteArray::fromBase64(TheTextFromDB);
// we'll need to get the target filename from somewhere
// we open the target filename and we stream the QByteArray  into it.
// we close the file.
// then we go bool QDesktopServices::openUrl (   QUrl(filename,QUrl::TolerantMode )  )
//




//searches in the DB for 'label'. returns value.
int RXDBLinkMySQL::Extract_One_Text(const char *label,char*value,const int buflength)
{
    int rc=0;
    string q;
    DBRESULT r;unsigned int err;
    assert(IsMirror());
    q=   string("SELECT `text` FROM ") + string(RXMIRRORTABLE)
            + " WHERE header= '"+string(label)+"'";
    rc= QQuery(q,r,&err);
    if(rc<0)
        return 0;
    print_result(r);
    int nc, nr = r.size();
    if(nr)
        nc=r[0].size();
    if(!nr)
        return 0;
    q = r[0][0];
    strncpy(value,q.c_str() ,buflength);
    return 1;
}
std::string RXDBLinkMySQL::Extract_Headers(const string &what)
{
    std::string rs;
    int i;
    int flag;

    if(what=="input") flag = SUM_INPUT;
    else if(what=="output" ) flag = SUM_OUTPUT;
    else flag = SUM_OUTPUT+SUM_INPUT;

    DBRESULT rq;unsigned int err;
    string q =string ("SELECT  `header` FROM " ) + string(RXMIRRORTABLE) ;
    i= QQuery(q,rq,&err);
    if(i<0)
        return rs;

    assert(IsMirror());

    int r,c,lflag;
    int nr = rq.size();
    int nc = rq[0].size();
    if(!nr || !nc) { //cout<<" result is empty"<<endl;
        return rs;
    }
    // nc is 1
    for(r=0;r<nr;r++) {
        for(c=0;c<nc;c++){
            int type   = Summary_Type(rq[r][c].c_str(),&lflag);
            if(lflag&flag)
            { rs +=  rq[r][c]; rs+=",";}
        }
    }
    rs.erase(rs.length()-1);
    return rs;
}  

int RXDBLinkMySQL::Extract_Column(const char*fname,const char*head,char***clist,int*Nn)
{
    int i,rc=0;
    assert(m_DBtype&RXDB_INPUT );
    DBRESULT r;unsigned int err;
    string q = string("SELECT " )+string(head)
            +string(" FROM ") + this->m_table
            +string(" ORDER BY ") +string(head) ;
    rc= QQuery(q,r,&err);
    cout<< "column "<<head<<" is \n nrows = "<<r.size () <<" nr = "<<r[0].size ()<< endl;

    *Nn = r.size (); //int nc = r[0].size ();
    if(*clist) RXFREE (*clist) ; *clist=(char**) MALLOC((*Nn)*sizeof(void*));
    for(i=0;i<*Nn;i++) (*clist)[i]=STRDUP(r[i][0].c_str());
    return (*Nn);
}
int RXDBLinkMySQL::Create_Filtered_Summary(class RXMirrorDBI *snew,char *filter){
    cout<<"TODO RXDBLink::Create_Filtered_Summary"<<endl; return 0;

}
int RXDBLinkMySQL::read_and_post_one_row(const int row, const QString pRunName,MIRRORPTR mirrordest)
{
    const std::string runName = pRunName.toStdString();
    int rc=0;
    assert(m_DBtype&RXDB_INPUT );
    size_t n=0,k;
    DBRESULT rq,hdrs;unsigned int err;

    string q = string("DESCRIBE ")+ this->m_table;
    QQuery(q,hdrs,&err);
    this->print_result (hdrs);


    // SELECT * FROM pet WHERE name = 'runName';
    q = string("SELECT * ")
            +string(" FROM ") + this->m_table
            +string(" WHERE Run_Name = '") +string(runName)+string("'") ;
    rc= QQuery(q,rq,&err);
    this->print_result (rq);
    n = rq[0].size(); assert(n==hdrs.size ());
    for(k=0;k<n;k++) {
        //Summary_Type(h[k], &flag);
        cout<<"posting "<<hdrs[k][0]  <<" val="<<rq[0][k] <<endl;
        if(mirrordest )
            mirrordest->post_summary_value(hdrs[k][0],rq[0][k]);  // this isnt quite the same as RXLogFIle
        rc++;
    }

    return rc;
}
int RXDBLinkMySQL::Replace_Words_From_Summary(char **strptr, const int append){
    /* if $header is found in buf, replace it with text
If 'append' is non-zero replace $AWA with $AWA=<value>
 or replace any string from $AWA to > (but not containing '$')  with $AWA=<value>
 */
    int nr,r, c=0 ,rc;
    char buf[256], bnew[256], *cp, *a,*b, *d,*stmp;
    string what,val;
    DBRESULT rq;unsigned int err;
    string q ;
    if(!strptr) return 0;
    if(! *strptr) return 0;

    q=   string("SELECT * FROM ") + string(RXMIRRORTABLE) ;
    rc= QQuery(q,rq,&err);
    nr = rq.size();
    for(r=0;r<nr;r++) {
        what = rq[r][0];
        val = rq[r][1];

        sprintf(buf,"$%s",what.c_str());
        if(append) {
            sprintf(bnew,"%s=<%s>", buf, val.c_str());
            stmp=STRDUP(*strptr);	RXFREE(*strptr);
            cp = Replace_String(&stmp, buf,bnew);
            *strptr=stmp;
            if(cp) {// printf(" replace_String '%s'->'%s' in '%s' gave\n cp='%s'\n===\n", buf,new,str, cp);
                /* now strip $AWA=<21.936>=<22.157> to $AWA=<21.936> */
                cp += strlen(bnew);
                a = strchr(cp,'$'); b = strchr(cp,'>');
                if((b &&a &&(b < a)) || ( b && !a)) {
                    for( d = cp; d<=b; d++) *d = ' ';
                    PC_Strip_Leading_Chars(cp," ");
                }
                c++;
            }

        }
        else  {
            if(NULL !=Replace_String(strptr, buf,val.c_str()))
                c++;;
        }

    }
    return c;
}

int   RXDBLinkMySQL::Apply_Summary_Values()  // this  synchs the app with the mirror DB
{
    int rc=0;
    int nr ,r,type, flag;;
    string what,val;
    DBRESULT rq;unsigned int err;
    string q ;

    q=   string("SELECT * FROM ") + string(RXMIRRORTABLE) ;
    rc= QQuery(q,rq,&err);
    nr = rq.size(); //if(nr) nc = rq[0].size();
    for(r=0;r<nr;r++) {
        what = rq[r][0];
        val = rq[r][1];
        // [2] is type. [3] is flag
        type   = Summary_Type(what.c_str(),&flag);
        Justapply_one_summary_item(what.c_str(),val.c_str(),  type, flag);
    }
    rc= g_World->Bring_All_Models_To_Date(false);
    return rc;
}
// transfer to script format
int RXDBLinkMySQL::Write_Script (FILE *fp)
{
    int rc=0;
    int nr ,r,type, flag;;
    string what,val;
    DBRESULT rq;unsigned int err;
    string q ;
    if(!fp) return 0;
      fprintf(fp,"\n!  (from MYSQL driver mirror)\n");
    q=   string("SELECT * FROM ") + string(RXMIRRORTABLE) ;
    rc= QQuery(q,rq,&err);
    nr = rq.size();
    rc=0;
    for(r=0;r<nr;r++) {
        what = rq[r][0];
        val = rq[r][1];
        // [2] is type. [3] is flag
        type   = Summary_Type(what.c_str(),&flag);
        if(!(flag &SUM_INPUT))
            continue;
        if(val.find("N/A") !=string::npos)
            continue;
        rc+= fprintf(fp,"apply: %s : %s\n",what.c_str(),val.c_str()  );

    }
    return rc;
}

std::string RXDBLinkMySQL::GetFileName()const{
    return string("mysql : ")+this->m_server
            +string("$")+ m_user
            +string("$")+  m_password
            +string("$")+  m_database
            +string("$")+  m_table;
}

int RXDBLinkMySQL::print_result(DBRESULT &rq) {

    if(!dbgPrint )
        return 0;
    int nr = rq.size();
    int nc = rq[0].size();
    if(!nr || !nc) { //cout<<" result is empty"<<endl;
        return 0;
    }
    cout<<"DB result is (nr= "<<nr<<" , nc = "<<nc<<" )\n";
    for(int r=0;r<nr;r++) {
        for(int c=0;c<nc;c++)
            cout<<"\t"<< rq[r][c];
        cout<<endl;
    }

    return nr*nc;
}
std::string RXDBLinkMySQL::ScriptCommand(){
    if(m_DBtype&RXDB_INPUT)
        return string("open_input: ") +this->GetFileName();

    if(m_DBtype&RXDB_OUTPUT)
        return string("open_log : ") +this->GetFileName();

    assert(0); return string("!garbage!");

}
void RXDBLinkMySQL::SetLast(const QString h, const QString v){
    qDebug()<<" TODO: RXDBLinkMySQL::SetLast" ;
}

int RXDBLinkMySQL::RemoveAllWithPrefix(const std::string & prefix)
{
    int rc=0;
    cout<<"TODO:  RXDBLink::RemoveAllWithPrefix"<<prefix<<endl;

    return rc;
}
#endif



