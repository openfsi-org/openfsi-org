// this is the RXTRObject. It manages relationships between RXObjects
// it has a historic use as base classs for RXTriangle, RXCurve, etc.
// it would be better if they had their own base class now that RXTRObject

#include "StdAfx.h"
#include "RXTRObject.h"


RXTRObject::RXTRObject()
{
	Init();
}

RXTRObject::RXTRObject(const RXTRObject & p_Obj)
{
	Init();
	this->operator =(p_Obj);
}

RXTRObject::~RXTRObject()
{
	Clear_SSD();
	
}

void RXTRObject::Init()
{
	m_Name = L"NO_NAME"; 
}

void RXTRObject::Clear_SSD()
{
	m_Name.clear();
}

RXTRObject& RXTRObject::operator = (const RXTRObject & p_Obj)
{
	//Make a copy
	Init();
	m_Name= p_Obj.m_Name;
//	m_IsUpToDate = p_Obj.m_IsUpToDate;

	return *this;
}//RXTRObject::RXTRObject& operator = (const RXTRObject & p_Obj)


char * RXTRObject::GetClassID() const
{
	return RXTRObjectCLASSID;
}
int RXTRObject::IsKindOf(const char * p_ClassID) const
{   // Please verify the strcm preturn value
	if (0==strcmp(p_ClassID,RXTRObjectCLASSID))
		return true;
	else
		return false;
}

RXTRObject * RXTRObject::Cast( RXTRObject* p_pObj)
{
	return(RXTRObject *)Cast((const RXTRObject*)p_pObj);
}

const RXTRObject * RXTRObject::Cast( const RXTRObject* p_pObj)
{
	if (!p_pObj)
		return NULL;
	if (p_pObj->IsKindOf(RXTRObjectCLASSID))
		return (const RXTRObject *)p_pObj;
	else
		return NULL;
}

RXSTRING RXTRObject::GetTROName() const
{
	return m_Name;
}
void RXTRObject::SetTROName(const RXSTRING & p_Name)
{
	m_Name = p_Name;
}



