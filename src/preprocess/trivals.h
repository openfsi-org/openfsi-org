//trivals.h header for trival.cpp
//Thomas 10 06 04 


#ifndef RX_TRIVAL_H
#define RX_TRIVAL_H


int scale_nodal_values(int nnod,float *nvalue, int ncol, float nSDs,float *cmin,float *cmax);

EXTERN_C int AKM_Mset_Face_colors_By_FIndex(HC_KEY theShell,const char *type,int offset,int nfaces,float *tvalue);
EXTERN_C int make_triangle_values(const int sli,float *tvalue,const int ntval,const int ncol,const float nSDs,float *cmin,float *cmax);

#endif //#ifndef RX_TRIVAL_H
