#include "StdAfx.h"

#include "RXSitePt.h"
/*
  A nore on the member m_dsEphemeral
  It appears to be set in  RXPside::GetOffsets( ) where it is set on the end sites to endNoff
  in meshonepside the internal nodes' ds is set to the result of GetOffsets
  and similarly in  RXPside::ReMesh(void)
  The psides internal nodes get it set to the fraction (m_slist.second) in remesh
  it is also set in RXPside::psGetCurveCoords(.
  */
RXSitePtWithData::RXSitePtWithData(void){
    assert("dont use this constructor"==0);
}
RXSitePtWithData::RXSitePtWithData(const double xx,const double yy,const double zz,const double u,const double v,const int nn,const int datasize) 
    :RXSitePt(xx,yy,zz,u,v,nn)
    ,m_nd(datasize)
{
    d=new double[m_nd];
}

RXSitePtWithData::RXSitePtWithData(const ON_2dPoint&uv ,const int datasize,const double* dbuf,const int nn)
    :RXSitePt(ON_UNSET_VALUE,ON_UNSET_VALUE,ON_UNSET_VALUE,uv.x,uv.y,nn)
    ,m_nd(datasize)
{
    d=new double[m_nd];
    for(int i=0;i<m_nd;i++)
        d[i]=dbuf[i];
}
RXSitePtWithData::RXSitePtWithData(const ON_3dPoint&xyz,const ON_2dPoint&uv ,const int datasize,const double* dbuf,const int nn)
    :RXSitePt(xyz.x,xyz.y,xyz.z,uv.x,uv.y,nn)
    ,m_nd(datasize)
{
    d=new double[m_nd];
    for(int i=0;i<m_nd;i++)
        d[i]=dbuf[i];
}

RXSitePtWithData::~RXSitePtWithData(void)
{
    delete[] d;
}

RXSitePt::RXSitePt(void)
{
    Init();
}
RXSitePt::RXSitePt(const double px,const double py,const double pz,const double pu,const double pv,const int nn){
    Init();
    x=px; y=py; z=pz;  m_u=pu; m_v=pv;  this->SetRXSite_N(nn);
}

RXSitePt::~RXSitePt(void)
{
}
int RXSitePt::Init(void)
{
    x=y=z=ON_UNSET_FLOAT;
    m_u=m_v=ON_UNSET_VALUE;    // texture coordinates (0 to 1)
    m_gdp = m_dsEphemeral = 0;
    m_Site_Flags=0;
    return 1;
}

RXSitePt& RXSitePt::operator-=(const RXSitePt& p)
{
    x -= p.x;
    y -= p.y;
    z -= p.z;
    m_u-=p.m_u;
    m_v -=p.m_v;
    return *this;
}
RXSitePt& RXSitePt::operator =(const ON_3dPoint& p)
{
    x = p.x;
    y = p.y;
    z = p.z;
    m_u=m_v=ON_UNSET_VALUE;
    return *this;
}

RXSitePt RXSitePt::operator*(const  double d ) const
{
    if(ON_IsValid(m_u) && ON_IsValid(m_v))
        return RXSitePt(x*d,y*d,z*d,m_u*d, m_v*d,-1);

    return RXSitePt(x*d,y*d,z*d,m_u, m_v,-1);
}
RXSitePt RXSitePt::operator/(const double d ) const
{
    const double one_over_d = 1.0/d;
    if(ON_IsValid(m_u) && ON_IsValid(m_v))
        return RXSitePt(x*one_over_d,y*one_over_d,z*one_over_d,
                        m_u*one_over_d,m_v*one_over_d,-1);
    return RXSitePt(x*one_over_d,y*one_over_d,z*one_over_d,
                    m_u,m_v,-1);
}

RXSitePt RXSitePt::operator+( const RXSitePt& p ) const
{
    int count= ON_IsValid(m_u) + ON_IsValid(m_v) + ON_IsValid(p.m_u) + ON_IsValid(p.m_v);
    if(count==4)
        return RXSitePt(x+p.x,y+p.y,z+p.z,m_u+p.m_u,m_v+p.m_v,-1);
    //assert(count==0);
    // count=0 means neither point has (uv) values.
    // count=4 means both points have (uv) values
    // if count is 2 we need to decide what to do

    return RXSitePt(x+p.x,y+p.y,z+p.z,m_u,m_v,-1);
}

bool RXSitePt::Set(const ON_3dPoint &v) // model space
{
    x=v.x; y=v.y;z=v.z;
    return true;
}

const ON_3dPoint RXSitePt::ToONPoint(void) const
{
    ON_3dPoint r;
    r.x=x; r.y=y; r.z=z;
    return r;
}

RXSTRING RXSitePt::TellMeAbout(void)const{
    RXSTRING rc(_T( " SitePt( "));
#ifndef RXMESH
    rc+= TOSTRING( m_u) +_T(", ");
    rc+= TOSTRING( m_v) +_T(") \t");
    rc+= TOSTRING( m_dsEphemeral)  +_T(",");
#endif
    return rc + RXSiteBase::TellMeAbout ();
}

