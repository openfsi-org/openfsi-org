#pragma once
#include "opennurbs.h"
#include <string.h>
	#ifndef RXVIRTUALSITECLASSID 
		#define RXVIRTUALSITECLASSID "27ECF89E-A28E-4efd-9BAF-6F5901A1B7D4"
	#endif   
#define RXSITEINITIAL_N -99
# define samesite(s1, s2)	((s1)->x == (s2)->x && (s1)->y == (s2)->y && (s1)->z == (s2)->z)

class RXSiteBase: public ON_3dPoint
{
public:
	RXSiteBase(void);
	virtual ~RXSiteBase(void);

	virtual int Init () =0;

	RXSTRING TellMeAbout() const;

	void SetRXSite_N(const int n) {m_RXSite_N=n ; } // deprecated
	int RXSite_N(void) const {return m_RXSite_N; } // deprecated
private:
	int m_RXSite_N;	// not sure what m_RXSite_N is for, but it shouldnt be confused with FEObject::N
};
