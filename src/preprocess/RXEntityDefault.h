#pragma once
#include "RXEntity.h"

// temporary definitions while implementing class RXEntity
#define dataptr DataPtr()

// this class is designed to be equivalent to the old 'struct PC_ENTITY'
// after August 2008 new entity types will be derived from RXEntity.


class RXEntityDefault :
	public RXEntity
{
public:
	RXEntityDefault(void);
	~RXEntityDefault(void);
protected:
	void *m_dataptr;
public:
	void* DataPtr(void) const;
	void  SetDataPtr(void* ptr){m_dataptr=ptr;}
        int PC_Finish_Entity(const char *type,
                                const char *name,void *dp,HC_KEY hk,
                                struct LINKLIST *plist,const char *att,
                                const char *line);
public:
		int CClear();
		int Compute(void);
		int Resolve(void);
		int Finish(void);

		int ReWriteLine(void);

		virtual int Dump(FILE *fp) const;
protected:
		virtual int Cleanup(SAIL *const sail,int Leave_Deps=0);
};
