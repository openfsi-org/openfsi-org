#ifndef _FINDTRANSFORM_H_
#define _FINDTRANSFORM_H_
#include "opennurbs.h"

#ifdef HOOPS
extern int ApplyRotation(float mx[4][4],float alpha, float beta, float Gamma);
extern double *FindTransform  (const ON_3dPoint &s0,const ON_3dPoint &s1,const ON_3dPoint &s2,
				const ON_3dPoint &v0,const ON_3dPoint &v1,const ON_3dPoint &v2,
				double *Mt,float Alpha,float Beta,float Gamma);
EXTERN_C double *FindInverse(double *Mt,double *IMt);
#endif
extern int ApplyRotation(ON_Xform &xx,float alpha, float beta, float Gamma);
extern ON_Xform FindTransform  (const ON_3dPoint &s0,const ON_3dPoint &s1,const ON_3dPoint &s2,
				const ON_3dPoint &v0,const ON_3dPoint &v1,const ON_3dPoint &v2,
				ON_Xform &Mt,float Alpha,float Beta,float Gamma);

EXTERN_C bool FrameFromPoints(const ON_3dPoint &s0,const ON_3dPoint &s1,const ON_3dPoint &s2,
							  ON_3dPoint&   P0,  // initial frame center
							  ON_3dVector&  X0, // initial frame X
							  ON_3dVector&  Y0, // initial frame Y
							  ON_3dVector&  Z0);


#endif  //#ifndef _FINDTRANSFORM_H_



