
/* filament c.  Routines to create the stifness matrix from filament crosses.
	Peter Heppel  24.02.01

  */

#include "StdAfx.h" 
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXSRelSite.h"
#include "RXSeamcurve.h"
#include "RX_FETri3.h"
#include "RXPanel.h"
 
#include "RXCurve.h"

#include "stringutils.h"
#include "words.h"
 
#include "panel.h" // for rxincrement
 
#include "f90_to_c.h"

#include "global_declarations.h"

#include "filament.h"

#define N_COLORS 10
const char *SEGS[N_COLORS]= {"c0","c1","c2","c3","c4","c5","c6","c7","c8","c9"  };


FILAMENT_LOCAL_DATA ::FILAMENT_LOCAL_DATA ():
	m_FE_Tri(0),
	DD_Tot(0),
	ps(0)//,	m_fldsc(0) 
	{
	}
ON_3dVector  FILAMENT_LOCAL_DATA::Create_Material_Axes(const RXCrossing2 & p_l1,const RXCrossing2 & p_l2){

// just returns the filament vector
	double l_d1 = p_l1.Distance();
	double l_d2 = p_l2.Distance();

	ON_3dPoint l_pt1 = p_l1.EvalPt(0);
	ON_3dPoint l_pt2 = p_l2.EvalPt(0);
	
	ON_3dVector l_x = l_pt2 - l_pt1; 
	l_x.Unitize ();  
	return l_x;

}// Create_Material_Axes


int PCF_Intersection_Type(struct PC_FILAMENT_CROSS*l, int c,int *j){

	int j2 =  (*j) + 1; 
	int j1_p, j1_pp;
	struct PC_FILAMENT_CROSS*l1 = &(l[*j]);
	struct PC_FILAMENT_CROSS*l2 = &(l[j2]);

		j1_p  = PCF_Inc( (l1->m_j));
		j1_pp = PCF_Inc(j1_p);

	if(l1->flag == PCF_DISREGARD)
		return PCF_DISREGARD;

	if(l2->flag== PCF_DISREGARD)
		return PCF_DISREGARD;

	if(l1->flag==PCC_OK && l2->flag==PCC_OK) {
//C
		if(l1->m_t>= PCF_ONE   && l2->m_t <PCF_SMALL  && l2->m_j==j1_pp)	// edge Kiss CASE  C
			return(PCF_C);
		if(l1->m_t < PCF_SMALL && l2->m_t >PCF_ONE   && l2->m_j==j1_p)
			return(PCF_C);
//A 
		if(j1_p ==l2->m_j && l1->m_t > PCF_ONE  && l2->m_t<PCF_SMALL) // Kiss one vertex
			return PCF_A;
		return PCF_D;
	}
return PCF_DISREGARD ;
}

int PCF_Append(struct PC_FILAMENT_CROSS**list, int*c, double s1,int i1,int j,double t, VECTOR *v2,int flag){

	*list=(PC_FILAMENT_CROSS *)REALLOC(*list, (*c+2)*sizeof(struct PC_FILAMENT_CROSS));
	if(!(*list)) return 0;
	(*list)[*c].s1 =s1;
	(*list)[*c].i1 = i1;
	(*list)[*c].m_j = j;
	(*list)[*c].flag =flag;
	(*list)[*c].m_t=t;
	memcpy(&((*list)[*c].v2),v2,sizeof(VECTOR));
	(*c)++;
return 0;
}
int PCF_Compare_Xs( const void *va,const void *vb){
  
  /*   precedence to PCC_OKs, then sort by S1 */
  
     struct PC_FILAMENT_CROSS *ia,  *ib;
	
  
	ia = (struct PC_FILAMENT_CROSS *) va;
	ib = (struct PC_FILAMENT_CROSS *) vb;
	if(ia->flag == PCC_OK && ib->flag != PCC_OK ) return -1;
	if(ia->flag != PCC_OK && ib->flag == PCC_OK ) return 1;

	if(ia->s1 < ib->s1-PCF_SMALL) return -1;   
	if(ia->s1 > ib->s1+PCF_SMALL) return  1;   
// here s1 and s2 are identical.  
//	ib->flag = PCF_DISREGARD;//  removed experimentally. this happens case A 

	if(ia->m_j==2 && ib->m_j==0) 
		return -1; // deal with the cycles
	if(ib->m_j==2 && ia->m_j==0) 
		return  1;

	if(ia->m_j < ib->m_j) return -1;   
	if(ia->m_j > ib->m_j) return  1;   
	return   0;

}
int PCF_Sort_Filament_Xlist(struct PC_FILAMENT_CROSS*l, int *c) {
  if (*c >1) {
     qsort(l,*c,sizeof(struct PC_FILAMENT_CROSS),PCF_Compare_Xs);
	 qsort(l,*c,sizeof(struct PC_FILAMENT_CROSS),PCF_Compare_Xs);
	 while(*c && l[(*c)-1].flag==PCF_DISREGARD)
		 (*c)--;


  }

// the first sort is by flag.  PC_OKs at the top
// then sort by s1.  This divides the Xs into pairs in the event that a 
//  SC crosses the tri more than once. 
// if S1 the same, then sort by j
	return 1;
}

int PCF_Print_Filament_Xlist(class FILAMENT_LOCAL_DATA *fld,struct PC_FILAMENT_CROSS*l, int c) {
	int i;
	cout<< " Crosses for tri \n"<<endl;
	for(i=0;i<3;i++)
		printf("%f  %f  %f\n",fld->v[i]->x, fld->v[i]->y,fld->v[i]->z);
	//if(fld->m_sc) printf(" with sc %s\n", fld->m_sc->owner->name());
 

	if(!c) return 0;
		
	cout<< "filamentList\nn flag i1 j  s1  t  ( v2 v2 v2 ) \n"<<endl;
  for(i=0;i<c;i++){
     printf("%d  %d  %d  %d %f %f ",i, l[i].flag,l[i].i1,l[i].m_j,l[i].s1,l[i].m_t);
	 printf("( %f %f %f )\n",  l[i].v2.x,l[i].v2.y,l[i].v2.z);
  }
 
	return 1;
}

int PCF_DeleteFilamentList(FortranTriangle *ft){
		struct PC_FILAMENT_ON_TRI  *f,*g;
		 if(!ft) return 0;
		 f = ft->m_fils;
		 while(f) {
				g = f;
				f=f->next;
				RXFREE(g);
		 }
		 ft->m_fils=NULL;
	return 0;
}
int PCF_PrintTriFilamentList(const FortranTriangle *ft, FILE *fp)
{
	struct PC_FILAMENT_ON_TRI  *f;
	if(!ft) 
		return 0;
		

		f = ft->m_fils;
		if(!f) return 0;
		fprintf(fp,"Fils Tri %d \t e1 ,    t1    \t e2 ,   t2 \t theta (deg)   \t width   \t    EA     \t    Ti    \t strain\tsc\n",ft->GetN());
		while(f) {

			fprintf(fp," \t%d,%f \t%d,%f",	f->j1,f->t1,f->j2,f->t2);
			fprintf(fp," \t%f \t%f \t%f \t%f \t%f\t",f->m_theta*57.29578,f->m_W,f->m_EA,f->m_Ti,f->m_strain);
			fprintf(fp,"%s\n", f->m_sco->name());			
			f=f->next;
		}
	return 1;
}

int PCF_PushToTriFilamentList(FortranTriangle *p_ft,const int j1,const double t1 ,
							const int j2,const double t2,
							RXEntity_p sce,const double theta,const double p_w,
							const double p_EA, const double prestress)
{
		 struct PC_FILAMENT_ON_TRI  *f;

		 assert(p_ft);

		if(!g_Filament_pp )
			return 0;

		f = p_ft->m_fils;
		p_ft->m_fils =(struct PC_FILAMENT_ON_TRI *)CALLOC(1,sizeof(struct PC_FILAMENT_ON_TRI));
		//TO connect the double link list
		p_ft->m_fils->next = f;
		f = p_ft->m_fils;
		//To connect to the seamcurve
		f->m_sco = sce; 

		f->j1=j1; f->j2=j2;
		f->t1=t1; f->t2=t2; 
		f->m_theta =theta; f->m_W=p_w; f->m_EA=p_EA; f->m_Ti=prestress;
	return 1;
}

HC_KEY PCF_Draw_Colored_Filaments(const SAIL *p_sail) {

	char Hname[256], buffer[512];
	//FortranTriangle **te;
	int i=0, iloc=-99, color_band = 0;
	HC_KEY l_key=0;
 
	double stress[3], princ[3],cre, Princ_Stiff;
 
	double l_eTot, l_cTot,l_max ,l_esq, l_mean, l_min, l_sd ,hue,eps[3], eps0[3];
	struct PC_FILAMENT_ON_TRI  *l_fptr;
	ON_3dPoint v1,v2;
	char layername[256];
	char *lp;
	bool KillNegative = (relaxflagcommon.Force_Linear == 0);

	 if(!p_sail->IsHoisted())  //this fails on the 	boat
			return 0;

 	sprintf(Hname,"%s",p_sail->GetType().c_str());
	PC_Strip_Leading(Hname);
	PC_Strip_Trailing(Hname);
	strrep(Hname,47,'+'); 		// replace slashes with '+' 
	
	HC_Open_Segment_By_Key(p_sail->PostProcNonExclusive  ());

	if(HC_QShow_Existence("gauss*","self"))
		HC_Delete_Segment("gauss*"); 

	if(!p_sail->m_FTri3s.size()) {
		HC_Close_Segment(); return 0;
	}
//1) cycle thru all the tris, calculating filament strain and saving it. 
//2)  calculate max mean SD and decide the color bands.
//3) Put each filament section into a segment "layername/bucket_N"
//4) color the buckets by the global colormap.
  
//0  find the last tri/* find last OK Tri in list */

	l_eTot=0; l_cTot=0; l_esq=0.0; l_min = 999999.0; l_max = -l_min;

//1) cycle thru all the tris, calculating filament strain and saving it. 


vector<RX_FETri3*>::const_iterator ii = p_sail->m_FTri3s.begin(); ii++;
	for(;ii != p_sail->m_FTri3s.end();ii++) {
		FortranTriangle *te  = *ii;
	    if(te && (te->GetN() ) && ( te->m_fils )   ) {
			if(te->m_TriArea <0.00000001) continue;
// calc the strain thence the strain on each filament
		eps0[0] = eps0[1] = eps0[2] = 0.0;
 		iloc = te->GetN() ; //maybe
     	cf_one_tri_stress_op(iloc,p_sail->SailListIndex(),(stress),(eps0),(princ),&(cre), &(Princ_Stiff));

		l_fptr = te->m_fils;
		while(l_fptr){ // expects theta to be in Radians.
			memcpy(eps,eps0,3*sizeof(double)); 
			assert(ON_IsValid(l_fptr->m_theta));
			if(!ON_IsValid(eps[0] + eps[1] + eps[2]) ) 		printf(" invalid EPS for tri %d\n",iloc);	
	 
			eps[2] = eps[2]/2.0; 
			RXPanel::rotate_stress(eps, l_fptr->m_theta );
			eps[2] = eps[2]*2.0;

			// this needs verification.
			if(ON_IsValid(eps[0]) )
				l_fptr->m_strain = eps[0]; 
			else {
				printf(" invalid strain for tri %d\n",iloc);
				l_fptr->m_strain = 0;	
			}
			sc_ptr sc =  (sc_ptr ) l_fptr->m_sco;
			if( ! KillNegative || sc->FlagQuery(PCF_FIL_NO_BUCKLE) || l_fptr->m_strain > 0 ){
			//if (l_fptr->m_strain>0. || !KillNegative) {
				l_eTot+= l_fptr->m_strain ; 
				l_cTot+=1.0; 
				l_esq += ( l_fptr->m_strain * l_fptr->m_strain);
				l_min = min(l_min, l_fptr->m_strain);
				l_max = max(l_max, l_fptr->m_strain);
			}
			l_fptr = l_fptr->next;

		} // while ...
	    }
	} // for i

//2)  calculate max mean SD and decide the color bands.
	if(l_cTot > 1) {
		//printf(" ctot, etot,esq %f %f %f\n", l_cTot, l_eTot,	l_esq);
		//printf(" variance = %f\n", (1.0/(l_cTot-1.0) * (l_esq - (l_eTot*l_eTot)/l_cTot )) );

		l_mean = l_eTot/l_cTot;
		l_sd = sqrt(1.0/(l_cTot-1.0) * (l_esq - (l_eTot*l_eTot)/l_cTot)) ;
		l_min = max(l_min,l_mean - g_nSDs * l_sd) ; 
		l_max = min(l_max,l_mean + g_nSDs * l_sd ); 
		if(l_max <= l_min) {
			l_max=1.0; l_min=0.0;
		}
	}
	else {
		l_min =  - 0.01; l_max = l_min + 0.02 ; // temporary
		l_sd = 0.01; l_mean = 0.0;
		//cout<< " default scaling for filament plot\n"<<endl;
	}
//3) Put each filament section into a segment "layername/color"
    RXGRAPHICSEGMENT o = p_sail->GNode();
	ii = p_sail->m_FTri3s.begin(); ii++;
	for(;ii != p_sail->m_FTri3s.end();ii++) {
		FortranTriangle *te  = *ii;
	    if(!te || !(te->GetN()) || te->m_TriArea <0.00000001 ) continue;
		l_fptr = te->m_fils;
		while(l_fptr){
			if(!l_fptr->m_sco->AttributeGet("$layer",layername)) 
				strcpy(layername,"gauss_filaments");
			if(!(strlen(layername)>6)) 
				p_sail->OutputToClient("short layername",5);
			//lp = layername+6;	//strip the leading "GAUSS_"
			lp = layername;	// DONT strip the leading "GAUSS_"			
			hue = (l_fptr->m_strain - l_min)/(l_max-l_min); 

			hue = max(0.0,hue); hue=min(1.0,hue);
			color_band = (int)( hue* ((double) N_COLORS));
			sc_ptr sc =  (sc_ptr ) l_fptr->m_sco;
			if( ! KillNegative || sc->FlagQuery(PCF_FIL_NO_BUCKLE) || l_fptr->m_strain > 0 ){
			//if (l_fptr->m_strain>0. || !KillNegative) {
				HC_Open_Segment(lp);
						color_band =min(color_band,N_COLORS-1);
						color_band =max(color_band,0);
						HC_Open_Segment(SEGS[color_band]); 
							double x1,x2;
							switch (color_band) {
							case 0:
								x1 = ((l_max-l_min) * ( (double)1.0/(double) N_COLORS ) + l_min)*100.0;
								sprintf(buffer,"popuptext= strain band <%5.3f%%",x1); 
								break;
							case N_COLORS-1:
								x1 = ((l_max-l_min) * ( (double)color_band/(double) N_COLORS ) + l_min)*100.0;
								sprintf(buffer,"popuptext= strain band >%5.3f%%",x1); 			
								break;
							default:
								x1 = ((l_max-l_min) * ( (double)color_band/(double) N_COLORS ) + l_min)*100.0;
								x2 = ((l_max-l_min) * ( (double)(1+color_band)/(double) N_COLORS ) + l_min)*100.0;
								sprintf(buffer,"popuptext= strain band  %5.3f%% to %5.3f%%",x1,x2); 
							
							}
#ifdef HOOPS
							HC_Set_User_Options(buffer);
#endif
							hue = (double) color_band/(double) N_COLORS;
							hue = 270.0 - hue*270.0;  // 270 =cold, descending to 0 =hot 

							HC_Set_Color_By_Value("lines", "HSV",(float)hue,(float)1.0,(float)1.0);
	// get the coordinates
							v1 = Get_Position_On_Edge(p_sail,te,l_fptr->j1,l_fptr->t1);
							v2 = Get_Position_On_Edge(p_sail,te,l_fptr->j2,l_fptr->t2);


                            RX_Insert_Line(v1.x,v1.y,v1.z,v2.x,v2.y,v2.z,o);

						HC_Close_Segment(); // SEGS[color_band]
				HC_Close_Segment(); // layer
			}
			l_fptr = l_fptr->next;
		}// while
} //tris
	HC_Close_Segment(); // buffer

//4) create a color key


	HC_Open_Segment_By_Key(p_sail->PostProcNonExclusive());

	HC_Open_Segment("colorkey");
	HC_Show_Pathname_Expansion(".",buffer);
	HC_Close_Segment();
	HC_Close_Segment();

	HC_Open_Segment(buffer);
	HC_Flush_Contents(".","everything"); 
	HC_Set_Visibility("text=on");
	for(i=0;i<N_COLORS;i++) {
		HC_Open_Segment(SEGS[i]); 
			hue = (double) i/(double) N_COLORS;
			sprintf(buffer,"%f%%",((l_max-l_min) * hue + l_min)*100.0);
			hue = 270.0 - hue*270.0;  // 270 =cold, descending to 0 =hot
			HC_Set_Color_By_Value("text", "HSV",(float)hue,(float)1.0,(float)1.0);
			double y = (double)(N_COLORS-i);
			HC_Insert_Text(0.0,y,0.0,buffer);
		HC_Close_Segment();
	}

	HC_Close_Segment();


  return l_key;

}

ON_Xform Get_DeflectedTriangleAxes(SAIL *p_sail,const FortranTriangle *te){
ON_Xform l_xf;
ON_3dPointArray p(3);
ON_3dPoint c,p1,p2,p0;
ON_3dVector v1,v2,v3;
int j, ok=0;
// first get the 3 vertices
// then create an Xform from the 3 nodes
//  then  set its center at the centroid.


// first get the 3 vertices
	for(j=0;j<3;j++)//		p[j] =  Get_Deflected Node(p_sail,te->Node(j));
		p[j] = p_sail->GetSiteByNumber(te->Node(j))->DeflectedPos (RX_GLOBAL);
 

// then create an Xform from the 3 nodes with its center at the centroid.
	p1=p[1]; p0=p[0];p2=p[2];
	v1 = p[1] - p[0];			v1.Unitize ();
	v2 = p[2] - p[0];			v2.Unitize ();
	v3=ON_CrossProduct(v1,v2);	v3.Unitize ();
	v2=ON_CrossProduct(v3,v1);	v2.Unitize ();
	c = (p[0]+p[1]+p[2])/3.0;

	l_xf=ON_Xform(c,v1,v2,v3);
	ok= (fabs(l_xf.Determinant()) > 1e-8);


	if(!ok) {
		printf(" No tri XF - det=%f\n", l_xf.Determinant());
		l_xf.Identity();
	}
return l_xf;

}

ON_3dPoint Get_Position_On_Edge(const SAIL *p_sail,FortranTriangle *te, int j  ,double t){ // temporary version
ON_3dPoint vout;
RX_FESite *s1,*s2;
// j goes from 0 to 2.  return the point at t between vertex j and vertex j+1;
int n1,n2;
	n1 = te->Node(j);
	n2 = te->Node(rxincrement(j,3)); // node nos in tri list. should get from fortran


	s1 = p_sail->GetSiteByNumber(n1);
	s2 = p_sail->GetSiteByNumber(n2);			
	
	if(!s1 || !s2) 
		p_sail->OutputToClient(" bad FFtri ",25);

	double v1[3],v2[3];
	cf_get_coords(p_sail->SailListIndex(), s1->GetN(),v1);	
	cf_get_coords(p_sail->SailListIndex(), s2->GetN(),v2);
	vout.x = v1[0] * (1.0-t) + v2[0] * t;
	vout.y = v1[1] * (1.0-t) + v2[1] * t;
	vout.z = v1[2] * (1.0-t) + v2[2] * t;  


return vout;
}
