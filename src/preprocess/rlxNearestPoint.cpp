
// NearestPoint.cpp: implementation of the NearestPoint.
//
//////////////////////////////////////////////////////////////////////



#include "StdAfx.h"
#include "rxON_Extensions.h"

#include "rlxNearestPoint.h"

#ifdef _DEBUG_NEVER
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

int DropToCurve(const ON_Curve   * p_Curve, 
			    const ON_3dPoint & p_Point,
			    double			 * p_tstart,  //Starting point on p_Curve          
				RXUPoint   & p_PointOUT, ///OUTPUT: a pointer on a curve + a parameter value 
			    const double     & p_e,
			    const double     & p_eEnd,	// if a the projected poin t is off the subdomain we restart the serach from p_eEnd near the end of the subdomain
			    const int        & p_kmax, 
			    const double     & p_Tolerance,
				const double     & p_CutDistance,
				const RXTRObject* p_pPersistentCurve) //persitant curve from which the line is extracted 
																  //if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine
{
#ifdef _DEBUG
	assert(dynamic_cast<const ON_Curve*>(p_Curve));
	assert(p_tstart);
#endif //#ifdef _DEBUG

	int l_res = GetNearestPoint(p_Curve,p_Point,p_tstart,p_PointOUT,
								p_e,p_eEnd,p_kmax,p_pPersistentCurve); // this routine can fail
				
	ON_3dPoint l_pt = p_PointOUT.GetPos(); //p_Curve->PointAt(p_t);

	double l_dist = p_Point.DistanceTo(l_pt);

	if (l_dist>p_CutDistance) 
		return RX_DROP_BUT_FAR;
// if we get here we might be
	// A) within Solve_Tolerance of the curve. Return OK
	// b) within cut_Dist of the start or the end. In this case we return an extrapolated value.

	if (l_dist<p_Tolerance) 
		return RX_DROP_OK;

	double l_Dstart = l_pt.DistanceTo(p_Curve->PointAtStart());
	double l_Dend   = l_pt.DistanceTo(p_Curve->PointAtEnd());

	double l_cutdist = max(0.0,max(p_Tolerance,p_CutDistance));
	double l_tol = max(p_Tolerance,0.0);
	if (l_Dstart<=l_cutdist)
		return RX_DROP_AT_START;
	if (l_Dend<=l_cutdist)
		return RX_DROP_AT_END;

	if (l_dist<=l_tol)
		return RX_DROP_OK; 
	
	return RX_DROP_NOT_CONVERGED;
}//int DropToCurve

int GetNearestPoint(const ON_Curve   * p_Curve, 
					const ON_3dPoint & p_Point,
					double			 * p_tstart,  //Starting point on p_Curve          
				    RXUPoint   & p_PointOUT, ///OUTPUT: a pointer on a curve + a parameter value 
					const double     & p_e,
					const double     & p_eEnd,	// if a the projected poin t is off the subdomain we restart the serach from p_eEnd near the end of the subdomain
					const int        & p_kmax,
					const RXTRObject* p_pPersistentCurve) //persitant curve from which the line is extracted 
																  //if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine
{
#ifdef _DEBUG
	assert(dynamic_cast<const ON_Curve*>(p_Curve));
	assert(p_tstart);
#endif //#ifdef _DEBUG
	
	int l_ret = 0;
	
	//If the curve is a line  Peter this fails WRONG use ON_Methods
	const ON_LineCurve * l_pLC = ON_LineCurve::Cast(p_Curve);
	if (l_pLC)
	{
		l_ret = GetNearestPoint(l_pLC,p_Point,p_tstart,p_PointOUT,p_pPersistentCurve);
		if (l_pLC) l_pLC = NULL;
		return l_ret;
	}

	//If the curve is a polyline, or can be describe as a polyline
//COMMENTED OUT BY THOMAS 01 11 04 to cast instead of doing a copy	ON_SimpleArray<ON_3dPoint> l_pline_points;
//COMMENTED OUT BY THOMAS 01 11 04 to cast instead of doing a copy    ON_SimpleArray<double> l_pline_t;
//COMMENTED OUT BY THOMAS 01 11 04 to cast instead of doing a copy    if (p_Curve->IsPolyline(&l_pline_points,&l_pline_t)) // Peter this doesnt always mean you can cast it.
//COMMENTED OUT BY THOMAS 01 11 04 to cast instead of doing a copy	{
//COMMENTED OUT BY THOMAS 01 11 04 to cast instead of doing a copy		ON_PolylineCurve l_poly; 
//COMMENTED OUT BY THOMAS 01 11 04 to cast instead of doing a copy		l_poly.m_pline = l_pline_points;
//COMMENTED OUT BY THOMAS 01 11 04 to cast instead of doing a copy		l_poly.m_t     = l_pline_t;
//COMMENTED OUT BY THOMAS 01 11 04 to cast instead of doing a copy		double l_len = 0.0;
//COMMENTED OUT BY THOMAS 01 11 04 to cast instead of doing a copy		l_poly.Length(&l_len); // checks 
//COMMENTED OUT BY THOMAS 01 11 04 to cast instead of doing a copy#ifdef _GLOBAL_DECLARATIONS_H_
//COMMENTED OUT BY THOMAS 01 11 04 to cast instead of doing a copy		if (l_len<2*g_Gauss_Cut_Dist*1.05)
//COMMENTED OUT BY THOMAS 01 11 04 to cast instead of doing a copy			//#ifndef RHINO_DEBUG_PLUGIN
//COMMENTED OUT BY THOMAS 01 11 04 to cast instead of doing a copy				error("A Polyline is shorter than 2*g_Gauss_Cut_Dist*1.05",3);
//COMMENTED OUT BY THOMAS 01 11 04 to cast instead of doing a copy			//#endif
//COMMENTED OUT BY THOMAS 01 11 04 to cast instead of doing a copy#endif
//COMMENTED OUT BY THOMAS 01 11 04 to cast instead of doing a copy		return GetNearestPoint(&l_poly,p_Point,p_tstart,p_PointOUT);
//COMMENTED OUT BY THOMAS 01 11 04 to cast instead of doing a copy	}

	const ON_PolylineCurve * l_pPlC = ON_PolylineCurve::Cast(p_Curve);
	if (l_pPlC)	
	{
		l_ret = GetNearestPoint(l_pPlC,p_Point,p_tstart,p_PointOUT,p_pPersistentCurve);
		if (l_pPlC) l_pPlC=NULL;
		return l_ret; 
	}
/*
//Thomas 30 07 04 reset the starting point to improve satbility

	ON_3dPoint l_start = p_Curve->PointAtStart();
	ON_3dPoint l_end = p_Curve->PointAtEnd();
	double l_D1 = l_start.DistanceTo(p_Point);
	double l_D2 = l_end.DistanceTo(p_Point);
	
	double l_newt = 0.0;
	
	double l_length=0.0;
	double l_fractional_tolerance = 1.0e-7;
	p_Curve->GetLength(&l_length);
	
	if (fabs(l_D1)<1.0e-6)
	{
		l_newt = p_Curve->Domain().Min();
		p_PointOUT.Set(l_newt,p_Curve);
		return RX_DROP_AT_START;
	}
	if (fabs(l_D2)<1.0e-6)
	{
		l_newt = p_Curve->Domain().Max();
		p_PointOUT.Set(l_newt,p_Curve);
		return RX_DROP_AT_END;
	}

	assert((l_D1>=1.0e-6)&&(l_D2>=1.0e-6));
	
	double l_K = l_D2/l_D1; 
	assert(l_K>0.0);
	l_D1 = l_length/(1.0+l_K);
	if (GetParamAt(p_Curve, l_D1,l_newt,l_fractional_tolerance)==RXCURVE_ERROR) //1.0e-4 to speed up the computation
	{
		//#ifdef _GLOBAL_DECLARATIONS_H_
//		#ifndef AFX_NEARESTPOINT_H__49F7B905_0899_427C_9898_72E12A4BBD98__INCLUDED_    
//			error("ERROR GetParamAt(p_Curve, l_D1,l_t1,l_fractional_tolerance) in GetNearestPoint",2);
//		#endif
	}
	assert((l_newt>=p_Curve->Domain().Min())&&(l_newt<=p_Curve->Domain().Max()));
	
	*p_tstart=l_newt;
	//Thomas 30 07 04 reset the starting point to improve satbility
*/
	const rxON_PolyCurve *l_pPC = dynamic_cast<const rxON_PolyCurve*>(p_Curve);
	if (l_pPC)	
	{
		l_ret = GetNearestPoint(l_pPC,p_Point,p_tstart,p_PointOUT,p_e,p_eEnd,p_kmax,p_pPersistentCurve);
		if (l_pPC) l_pPC=NULL;
		return l_ret;
	}

	//If the curve is a NURBS
	const ON_NurbsCurve * l_pNC = p_Curve->NurbsCurve(); //ON_NurbsCurve::Cast(&p_Curve);
	if (l_pNC)
	{
		l_ret = GetNearestPoint(l_pNC,p_Point,p_tstart,p_PointOUT,p_e,p_eEnd,p_kmax,p_pPersistentCurve);
		l_pNC->~ON_NurbsCurve();
		return l_ret;
	}
	else
	{
		cout<< "this curve cannot be interpreted"<<endl;
		assert((ON_PolyCurve::Cast(p_Curve))||(ON_PolylineCurve::Cast(p_Curve))||(ON_NurbsCurve::Cast(p_Curve)));
		return RX_ERROR;
	}
	
	assert(0);
	return RX_ERROR;
}//int GetNearestPoint (ON_Curve)

int GetNearestPoint(const rxON_PolyCurve   * p_Curve, 
					const ON_3dPoint & p_Point,
					double			 * p_tstart,  //Starting point on p_Curve          
					RXUPoint   & p_PointOUT, ///OUTPUT: a pointer on a curve + a parameter value 
					const double     & p_e,
					const double     & p_eEnd,	// if a the projected poin t is off the subdomain we restart the serach from p_eEnd near the end of the subdomain
					const int        & p_kmax,
					const RXTRObject* p_pPersistentCurve) //persitant curve from which the line is extracted 
																  //if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine
{
#ifdef _DEBUG
	assert(p_tstart);
#endif //#ifdef _DEBUG

	assert(p_Curve);
	
	RXUPoint l_PointOUT; 

	//If the curve is a polycurve we split the curve at each discontinuty and we look for nearest pooint on each parts
	const ON_PolyCurve * l_PolyCrv  = ON_PolyCurve::Cast(p_Curve);
	assert(l_PolyCrv);
	
	int l_res_polycrv = 0;
	
	if (l_PolyCrv)
	{	
		int l_nbcrv = l_PolyCrv->Count();
		ON_3dPoint l_ProjPt; 
		ON_3dVector l_TanVect;
		int l_res         = 0;
		
		double * l_ts = (double*)onmalloc((l_nbcrv)*sizeof(double));
		ON_SimpleArray<int> l_flags;
		int i; 

		double l_temp = 0.0;
		double l_dist = 0.0; 
		RXCurve l_curve(l_PolyCrv->operator[](0));
		l_curve.Set_Ent(NULL);  // dangerous. 
//We get global tolerances by dereferencing m_e
		
		l_PointOUT.Set(l_PointOUT.Gett(),&l_curve); 

//236: warning: taking address of temporary

		ON_3dPoint l_pt = l_PointOUT.GetPos();
		l_temp = p_Point.DistanceTo(l_pt);

		p_PointOUT = l_PointOUT; //To set a default value

		for (i=0;i<l_nbcrv;i++)
		{
			const ON_Curve * l_crv = l_PolyCrv->operator[](i);
			if (!l_crv)
				return RX_ERROR;
			
			l_res = GetNearestPoint(l_crv,p_Point,p_tstart,l_PointOUT,p_e,p_eEnd,p_kmax,p_pPersistentCurve);
		
			l_pt = l_PointOUT.GetPos();
	
			//We only keep the closest point
			l_dist = p_Point.DistanceTo(l_pt);
			if (l_dist<=l_temp)
			{
				p_PointOUT = l_PointOUT;
				l_temp = l_dist; 
				l_res_polycrv = l_res;
			}
			//Clean up
			if (l_crv) l_crv = NULL; 
		}

		if (l_ts)
		{onfree(l_ts);l_ts=NULL;}
		
		return l_res_polycrv; //l_flags[i];
	}//	if (l_PolyCrv)	

	//CLEAN UP
	if (l_PolyCrv) l_PolyCrv = NULL;
	
	assert(l_res_polycrv!=RX_ERROR);
	return l_res_polycrv;
}//int GetNearestPoint (PolyCurve)


int GetNearestPoint(const ON_PolyCurve   * p_Curve, 
					const ON_3dPoint & p_Point,
					double			 * p_tstart,  //Starting point on p_Curve          
					RXUPoint   & p_PointOUT, ///OUTPUT: a pointer on a curve + a parameter value 
					const double     & p_e,
					const double     & p_eEnd,	// if a the projected poin t is off the subdomain we restart the serach from p_eEnd near the end of the subdomain
					const int        & p_kmax,
					const RXTRObject* p_pPersistentCurve) //persitant curve from which the line is extracted 
																  //if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine
{
#ifdef _DEBUG
	assert(p_tstart);
#endif //#ifdef _DEBUG

	assert(p_Curve);
	
	RXUPoint l_PointOUT; 

	//If the curve is a polycurve we split the curve at each discontinuty and we look for nearest pooint on each parts
	const ON_PolyCurve * l_PolyCrv  = ON_PolyCurve::Cast(p_Curve);
	assert(l_PolyCrv);
	
	int l_res_polycrv = 0;
	
	if (l_PolyCrv)
	{	
		int l_nbcrv = l_PolyCrv->Count();
		ON_3dPoint l_ProjPt; 
		ON_3dVector l_TanVect;
		int l_res         = 0;
		
		double * l_ts = (double*)onmalloc((l_nbcrv)*sizeof(double));
		ON_SimpleArray<int> l_flags;
		int i; 

		double l_temp = 0.0;
		double l_dist = 0.0; 
		RXCurve l_curve(l_PolyCrv->operator[](0));
		l_curve.Set_Ent(NULL);  // dangerous. 
//We get global tolerances by dereferencing m_e
		
		l_PointOUT.Set(l_PointOUT.Gett(),&l_curve); 

//236: warning: taking address of temporary

		ON_3dPoint l_pt = l_PointOUT.GetPos();
		l_temp = p_Point.DistanceTo(l_pt);

		p_PointOUT = l_PointOUT; //To set a default value

		for (i=0;i<l_nbcrv;i++)
		{
			const ON_Curve * l_crv = l_PolyCrv->operator[](i);
			if (!l_crv)
				return RX_ERROR;
			
			l_res = GetNearestPoint(l_crv,p_Point,p_tstart,l_PointOUT,p_e,p_eEnd,p_kmax,p_pPersistentCurve);
		
			l_pt = l_PointOUT.GetPos();
	
			//We only keep the closest point
			l_dist = p_Point.DistanceTo(l_pt);
			if (l_dist<=l_temp)
			{
				p_PointOUT = l_PointOUT;
				l_temp = l_dist; 
				l_res_polycrv = l_res;
			}
			//Clean up
			if (l_crv) l_crv = NULL; 
		}

		if (l_ts)
		{onfree(l_ts);l_ts=NULL;}
		
		return l_res_polycrv; //l_flags[i];
	}//	if (l_PolyCrv)	

	//CLEAN UP
	if (l_PolyCrv) l_PolyCrv = NULL;
	
	assert(l_res_polycrv!=RX_ERROR);
	return l_res_polycrv;
}//int GetNearestPoint (PolyCurve)

int GetNearestPoint(const ON_LineCurve   * p_Curve, 
					const ON_3dPoint & p_Point,
					double      *  p_tstart,  //Starting point on p_Curve          
					RXUPoint   & p_PointOUT, ///OUTPUT: a pointer on a curve + a parameter value 
					const RXTRObject * p_pPersistentCurve) //persitant curve from which the line is extracted 
																  //if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine
{
#ifdef _DEBUG
	assert(p_tstart);
#endif //#ifdef _DEBUG

	// if p_Curve is a Polyline we use some opennurbs functions
	ON_SimpleArray<ON_3dPoint> l_pline_points;

	const ON_LineCurve * l_LC = ON_LineCurve::Cast(p_Curve);
	double l_t; 
	if (p_tstart)
		l_t = *p_tstart; 
	else
		l_t = l_LC->Domain().Mid();
	
	int l_res = RX_ERROR;
	if (l_LC)
	{
		l_LC->GetClosestPoint( p_Point, &l_t);	
		l_res = RX_DROP_OK;	 // peter moved this UP	
		if (l_t<=l_LC->Domain().Min())
		{
			l_t=l_LC->Domain().Min();
			l_res = RX_DROP_AT_START;
		}
		if (l_t>=l_LC->Domain().Max())
		{
			l_t=l_LC->Domain().Max();
			l_res = RX_DROP_AT_END;
		}
	}
	
	if (p_pPersistentCurve)
		p_PointOUT.Set(l_t,p_pPersistentCurve);
	else
		assert(0); //DEBUG FOR NOW 08 11 04   p_PointOUT.Set(l_t,l_LC);
	assert(l_res!=RX_ERROR);

	//clean up 
	if (l_LC) l_LC = NULL;

	return l_res;
}//int GetNearestPoint (ON_LineCurve)

int GetNearestPoint(const ON_PolylineCurve  * p_Curve, 
					const ON_3dPoint & p_Point,
					double			 * p_tstart,  //Starting point on p_Curve          
					RXUPoint   & p_PointOUT, ///OUTPUT: a pointer on a curve + a parameter value 
					const RXTRObject* p_pPersistentCurve) //persitant curve from which the line is extracted 
																  //if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine
{
#ifdef _DEBUG
	assert(p_tstart);
#endif //#ifdef _DEBUG

	const ON_PolylineCurve * l_pPLC = ON_PolylineCurve::Cast(p_Curve);
	
	assert(l_pPLC);

	ON_3dPoint  l_dropped;
	double t;
	
	int l_ret = RX_ERROR;
	if(l_pPLC->GetClosestPoint( p_Point,&t))
	{
		if (p_pPersistentCurve)
			p_PointOUT.Set(t,p_pPersistentCurve);
		else
			assert(0); //for debug 08 11 04 p_PointOUT.Set(t,p_Curve);
		l_ret = RX_DROP_OK; 
	}
	
	//CLEAN UP
	if (l_pPLC) l_pPLC = NULL;

	return l_ret; 
	
}//int GetNearestPoint (polyline)

int GetNearestPoint(const ON_NurbsCurve  * p_Curve, 
					const ON_3dPoint & p_Point,
					double			 * p_tstart,  //Starting point on p_Curve          
					RXUPoint   & p_UPointOUT, ///OUTPUT: a pointer on a curve + a parameter value 
					const double     & p_eDot,
					const double     & p_eEnd,
					const int        & p_kmax,
					const RXTRObject* p_pPersistentCurve) //persitant curve from which the line is extracted 
																  //if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine
// Drops a point onto a curve using recurssively a 3rd order algorithm
// may have some problem converging if the point is off the curve and cannot be projected
{	
#ifdef _DEBUG
	assert(p_tstart);
#endif //#ifdef _DEBUG
	
	const ON_NurbsCurve * l_pNurbs = ON_NurbsCurve::Cast(p_Curve);
	assert(l_pNurbs);

	//If the curve is a polyline, or can be describe as a polyline
	ON_SimpleArray<ON_3dPoint> l_pline_points;
    ON_SimpleArray<double> l_pline_t;
    if (l_pNurbs->IsPolyline(&l_pline_points,&l_pline_t)) // Peter this doesnt always mean you can cast it.
	{
		ON_PolylineCurve l_poly; 
		l_poly.m_pline = l_pline_points;
		l_poly.m_t     = l_pline_t;
		double l_len = 0.0;
		l_poly.GetLength(&l_len); // checks 
#ifdef _GLOBAL_DECLARATIONS_H_
		if (l_len<2*g_Gauss_Cut_Dist*1.05)
			//#ifndef RHINO_DEBUG_PLUGIN
				error("(rlxNP) A Polyline is shorter than 2*g_Gauss_Cut_Dist*1.05",1);
			//#endif
#endif
		return GetNearestPoint(&l_poly,p_Point,p_tstart,p_UPointOUT,p_pPersistentCurve);
	}
	//END //If the curve is a polyline, or can be describe as a polyline

	RXUPoint l_UPointtemp(p_UPointOUT);  //current point on the curve
	
	double l_t;
	ON_Interval l_Domain = p_Curve->Domain();

	if (p_tstart)
		l_t = *p_tstart;
	else
		l_t = l_Domain.Mid();

	int l_ret = 1;
	
	int l_knotNb = l_pNurbs->KnotCount();
	double l_tmin,l_tmax;
	l_tmin=l_tmax=0.0;
	ON_Interval l_SubDomain;

	double l_nextt;
	double l_dmin = 1.0e9;  //distance from the point to drop and the current point on the curve
	double l_d = 0;
	int i;

	int l_near_min,l_near_max;  //flag set to TRUE when we find a projected point off the sub domain

	ON_3dPoint l_pt; //current point on the curve
	int l_result = 0;

	//We do a 3rd search on each part of the curve , the curve is analysed between each concecutive knots
	for (i=0;i<l_knotNb-1;i++)
	{
		l_tmin=l_pNurbs->Knot(i);
		l_tmax=l_pNurbs->Knot(i+1);
		
		l_SubDomain = ON_Interval(l_tmin,l_tmax);

		//If we are on a double knot we jump to the next one
		if (l_tmin==l_tmax)
			continue;

		//to make sure the starting point is in the current subdomain
		if (!l_SubDomain.Includes(l_t))
			l_t = l_SubDomain.Mid();
		
		//set the flag to false
		l_near_min=l_near_max=0;

		while (1)
		{
			NearestPointCubic(l_pNurbs,p_Point,&l_t,l_UPointtemp,p_eDot,p_pPersistentCurve);

			l_pt = l_UPointtemp.GetPos(); 
			l_nextt = l_UPointtemp.Gett();
			assert (!_isnan(l_nextt)); //Peter
			if (l_nextt<=l_tmin)
			{
				if (l_near_min) 
				{   //if a search from a starting point near the start of the subdomain finds a point off the subdomain
					//we set the point to the strating end of the subdomain
					if (p_Point.DistanceTo(l_pt)<l_dmin) 
					{
						p_UPointOUT = l_UPointtemp;
						assert(!_isnan(p_UPointOUT.Gett()));
						l_dmin = p_Point.DistanceTo(l_pt);
					}
					break;
				}
				l_t = l_tmin+p_eEnd*(l_tmax-l_tmin); //restart near the start
				l_near_min=1;
				continue;
			}

			if (l_nextt>=l_tmax)
			{
				if (l_near_max)
				{	//if a search from a starting point near the end of the subdomain finds a point off the subdomain
					//we set the point to the ending end of the subdomain
					if (p_Point.DistanceTo(l_pt)<l_dmin)
					{
						p_UPointOUT = l_UPointtemp;
						assert(!_isnan(p_UPointOUT.Gett()));
						l_dmin = p_Point.DistanceTo(l_pt);
					}
					break;
				}
				l_t = l_tmax+p_eEnd*(l_tmax-l_tmin);  //restart near the end
				l_near_max=1;
				continue;	
			}
		
			ON_3dVector l_V = p_Point-l_pt; 
			double l_dist = l_V.Length();
			assert(l_V.Unitize());
			ON_3dVector l_T = l_UPointtemp.GetTangent();
			assert(l_T.Unitize());
			double l_dot = ON_DotProduct(l_V,l_T);
				
			if (fabs(l_dot)<p_eDot)   //if the point point is near enougth from the perpendicular to the curve
			{
				
				if (l_dist<l_dmin)
				{
					p_UPointOUT = l_UPointtemp;
					assert(!_isnan(p_UPointOUT.Gett()));
					l_dmin = l_dist;
				}
				l_result = l_ret;
#ifdef RHINO_DEBUG_PLUGIN
				ON_3dPoint l_pttemp = l_Pointtemp.GetPos();
				fprintf(fp,"%f\t%f\t%f\n",l_pttemp.x,l_pttemp.y,l_pttemp.z);
				fprintf(fp_t,"%f\n",l_Pointtemp.Gett());
#endif//#ifdef RHINO_DEBUG_PLUGIN
				break;
			}
			else
			{
				l_t = l_nextt; 
				l_ret++;
				if (l_ret>p_kmax)
				{

#ifdef RHINO_DEBUG_PLUGIN
					ON_3dPoint l_pttemp = l_Pointtemp.GetPos();
					fprintf(fp,"%f\t%f\t%f\n",l_pttemp.x,l_pttemp.y,l_pttemp.z);
					fprintf(fp_t,"%f\n",l_Pointtemp.Gett());
#endif //#ifdef RHINO_DEBUG_PLUGIN

#ifdef _GLOBAL_DECLARATIONS_H_
					char l_buff[256];
					ON_3dPoint l_start =p_Curve->PointAtStart();
					ON_3dPoint l_end =p_Curve->PointAtEnd();
					sprintf(l_buff, "Cannot accurately drop the point (%f,%f,%f) on curve starting at (%f,%f,%f) after %d iterations (l_dot=%f)",p_Point.x,p_Point.y,p_Point.z,l_start.x,l_start.y,l_start.z,l_end.x,l_end.y,l_end.z,l_ret,l_dot);
					error(l_buff,2);
#endif //#ifdef _GLOBAL_DECLARATIONS_H_

					if (l_V.Length()<l_dmin)
					{
						p_UPointOUT = l_UPointtemp;
						assert(!_isnan(p_UPointOUT.Gett()));
						l_dmin = l_V.Length();
					}
					break;
				}
			}
		}//	while (1)
		l_ret = 0;
	}//for (i=0;i<l_knotNb;i++)

	if (l_pNurbs) l_pNurbs = NULL;
#ifdef RHINO_DEBUG_PLUGIN
	if (fp)
		fclose(fp);
	if (fp_t)
		fclose(fp_t);
#endif //#ifdef RHINO_DEBUG_PLUGIN

	return l_result;
}//int GetNearestPoint (ON_NurbsCurve)


int NearestPointCubic(const ON_NurbsCurve * p_Curve, 
					const ON_3dPoint & p_Point,
					double			 * p_tstart,  //Starting point on p_Curve          
					RXUPoint   & p_PointOUT, ///OUTPUT: a pointer on a curve + a parameter value 
					const double     & p_e,
					const RXTRObject* p_pPersistentCurve) //persitant curve from which the line is extracted 
																  //if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine
//Drops a point to a curve using a 3rd order technic, only one step
//Low level function doesn't perform any check on the starting point
// if the curve has zero curvature on the starting point an 1st order step is done
{
#ifdef _DEBUG
	assert(p_Curve);
	assert(p_tstart);
#endif //#ifdef _DEBUG

	const ON_NurbsCurve * l_pNurbs = ON_NurbsCurve::Cast(p_Curve);
	assert(l_pNurbs);

	int der_count = 3;
	int l_v_stride = 3;
	double * l_v = (double*)onmalloc((der_count+1)*l_v_stride*sizeof(double));
	assert(p_tstart);
	double l_t = *p_tstart;

	if (!l_pNurbs->Evaluate(l_t,der_count,l_v_stride,l_v) )
		return false;

	ON_3dPoint l_point = ON_3dPoint(l_v[0],l_v[1],l_v[2]); //position 
	ON_3dVector l_Der1 = ON_3dVector(l_v[3],l_v[4],l_v[5]);  //tangent 
	ON_3dVector l_Der2 = ON_3dVector(l_v[6],l_v[7],l_v[8]);  //curvature
	ON_3dVector l_Der3 = ON_3dVector(l_v[9],l_v[10],l_v[11]);  //variation in curvature

	ON_3dVector l_Dist = p_Point-l_point;
	
	double l_denominator = (2.*(-3*l_Der1.x*l_Der2.x -
							3*l_Der1.y*l_Der2.y -
							3*l_Der1.z*l_Der2.z + l_Dist.x*l_Der3.x +l_Dist.y*l_Der3.y +l_Dist.z*l_Der3.z));
	double l_tnext = l_t;
	if (fabs(l_denominator)<p_e)  //mean the curve is straigth near the test point
	{
		ON_3dVector l_tan = l_Der1;
		l_tan.Unitize();
		double l_dot = ON_DotProduct(l_Dist,l_tan);
		if (fabs(l_dot)<p_e)
			l_tnext = l_t;
		double l_dist=ON_UNSET_VALUE; 
		if (l_dot>0)
			l_dist = l_point.DistanceTo(l_pNurbs->PointAtEnd());	
		if (l_dot<0)
			l_dist = l_point.DistanceTo(l_pNurbs->PointAtStart());	
		double l_ratio = l_dot/l_dist; 
		ON_Interval l_domain = l_pNurbs->Domain();
		if (l_dot>0)
			l_tnext += l_ratio*fabs(l_domain.Max()-l_t);
		if (l_dot<0)
			l_tnext += l_ratio*fabs(l_domain.Min()-l_t);
	}
	else
		l_tnext =  -(-2*powf(l_Der1.x,2) - 2*powf(l_Der1.y,2) -2*powf(l_Der1.z,2) +
						2*l_Dist.x*l_Der2.x +6*l_t*l_Der1.x*l_Der2.x + 2*l_Dist.y*l_Der2.y +
						6*l_t*l_Der1.y*l_Der2.y +2*l_Dist.z*l_Der2.z +6*l_t*l_Der1.z*l_Der2.z -
						2*l_t*l_Dist.x*l_Der3.x -2*l_t*l_Dist.y*l_Der3.y - 2*l_t*l_Dist.z*l_Der3.z +
						sqrt(4*powf(powf(l_Der1.x,2) +powf(l_Der1.y,2) +powf(l_Der1.z,2) - l_Dist.x*l_Der2.x -
						3*l_t*l_Der1.x*l_Der2.x -l_Dist.y*l_Der2.y -3*l_t*l_Der1.y*l_Der2.y -
						l_Dist.z*l_Der2.z -3*l_t*l_Der1.z*l_Der2.z +
						l_t*l_Dist.x*l_Der3.x +l_t*l_Dist.y*l_Der3.y + l_t*l_Dist.z*l_Der3.z,2) -
						4*(-3*l_Der1.x*l_Der2.x -3*l_Der1.y*l_Der2.y -
						3*l_Der1.z*l_Der2.z +l_Dist.x*l_Der3.x + l_Dist.y*l_Der3.y +
						l_Dist.z*l_Der3.z)*(2*l_t*powf(l_Der1.x,2) +
						2*l_t*powf(l_Der1.y,2) +2*l_Dist.z*l_Der1.z + 2*l_t*powf(l_Der1.z,2) -
						2*l_t*l_Dist.x*l_Der2.x +l_Der1.x*(2*l_Dist.x - 3*powf(l_t,2)*l_Der2.x) -
						2*l_t*l_Dist.y*l_Der2.y +l_Der1.y*(2*l_Dist.y - 3*powf(l_t,2)*l_Der2.y) -
						2*l_t*l_Dist.z*l_Der2.z -3*powf(l_t,2)*l_Der1.z*l_Der2.z +
						powf(l_t,2)*l_Dist.x*l_Der3.x +powf(l_t,2)*l_Dist.y*l_Der3.y + powf(l_t,2)*l_Dist.z*l_Der3.z)))/
						l_denominator;

	if (p_pPersistentCurve)
		p_PointOUT.Set(l_tnext,p_pPersistentCurve);
	else
		assert(0); //debug for now 08 11 04 p_PointOUT.Set(l_tnext,l_pNurbs);

	//CLEAN UP
	if (l_v)
		onfree(l_v);
	if (l_pNurbs) l_pNurbs = NULL; 

	return 1;
}//int NearestPointCubic

int GetNearestPoint_LinearMethod_ssd(const ON_NurbsCurve * p_Curve, 
									const ON_3dPoint & p_Point,
									double			 * p_tstart,  //Starting point on p_Curve          
									RXUPoint   & p_PointOUT, ///OUTPUT: a pointer on a curve + a parameter value 
									const double     & p_e,
									const double     & p_c,
									const int        & p_cid,
									const int        & p_kmax,
									const RXTRObject* p_pPersistentCurve) //persitant curve from which the line is extracted 
																  //if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine
{
#ifdef _DEBUG
	assert(ON_NurbsCurve::Cast(p_Curve));
	assert(p_tstart);
#endif //#ifdef _DEBUG

	//TO check the domain of the curve
	int span_count = p_Curve->SpanCount();
	assert(span_count);

	double* t = (double*)onmalloc((span_count +1)*sizeof(double));
	assert(t);
	p_Curve->GetSpanVector(t);

	ON_3dPoint l_ptprv = p_Curve->PointAt(t[0]);
	ON_3dPoint l_pt(0,0,0);

	ON_Interval l_Domain = p_Curve->Domain();
	int l_IsAtstart=1;
	int i;
	double l_t;

	if (p_tstart)
		l_t = *p_tstart;
	else
		l_t = l_Domain.Mid();

	int l_res = -1;
	
	assert(0); //debug for now 08 11 04 p_PointOUT.Set(l_t,p_Curve);

	for (i=1;i<span_count +1;i++)
	{
		ON_3dVector l_tangent,l_v;
		int k,kk;
		k=kk=0;
		double l_delta   = 0.0;
		double l_length  = 1.0e10; 
		double l_dotprod = 0.0;
		double l_Alpha   = 0.0;
		double l_c       = 1.0;
		
		ON_3dPoint l_start = p_Curve->PointAtStart();
		ON_3dPoint l_end   = p_Curve->PointAtEnd();

		//TO set the first value
		if (p_tstart)
			l_t = *p_tstart;
		else
			l_t = (t[i]+t[i-1])/2.0;	

		double l_sign = 0; //to check if lAlpha changes sign
		double l_tmax = t[i];
		double l_tmin = t[i-1];
		assert(l_tmax>=l_tmin);

		while (1)
		{
			k++; kk++;

			l_pt = p_Curve->PointAt(l_t);
			l_tangent = p_Curve->TangentAt(l_t);
			
			if (l_tangent.Length()==0)
				break;
			l_v = (p_Point-l_pt);	
			
			double l_newL = l_v.Length();
			if (fabs(l_length-l_newL)<p_e*p_e)
				break;
	
			l_length = l_v.Length();
			if (l_length<1.0e-7)
			{
				l_res = RX_DROP_OK;
				break;
			}
			
			//CONVERGENCE TEST
			l_v.Unitize();
			l_tangent.Unitize();
			l_dotprod = ON_DotProduct(l_v,l_tangent);
			
			//assert(fabs(l_dotprod)<=1.0); peter
			if(l_dotprod > 1.0 || l_dotprod < -1.0) 
				printf(" l_dotprod  BIG  = %f\n", l_dotprod);

			if (fabs(l_dotprod)<p_e)
			{
				l_res = RX_DROP_OK;
				break;
			}
					
			if (l_dotprod>0)  //W e want to go in the direction of tmax == t[i]
			{
				ON_3dPoint  l_ptmax      = p_Curve->PointAt(l_tmax);
				ON_3dVector l_vmax       = (p_Point-l_ptmax);	
				ON_3dVector l_tangentmax = p_Curve->TangentAt(l_tmax);
				l_vmax.Unitize();
				l_tangentmax.Unitize();
				double l_dotprodmax = ON_DotProduct(l_vmax,l_tangentmax);
				//ASSUMPTION CONVEXE CURVE!!!!!
				if (l_dotprodmax>0)
				{
					l_t=l_tmax;
					if (l_t==l_Domain.Min())
						l_res = RX_DROP_AT_END;
					break;
				}
				l_tmin = l_t;
				assert((l_tmax-l_t)>=0);  // Peter  aug 16 2004
				l_t = l_t + l_dotprod*(l_tmax-l_t);
			}
			if (l_dotprod<0)
			{
				ON_3dPoint  l_ptmin      = p_Curve->PointAt(l_tmin);
				ON_3dVector l_vmin       = (p_Point-l_ptmin);	
				ON_3dVector l_tangentmin = p_Curve->TangentAt(l_tmin);
				l_vmin.Unitize();
				l_tangentmin.Unitize();
				double l_dotprodmin = ON_DotProduct(l_vmin,l_tangentmin);
				//ASSUMPTION CONVEXE CURVE!!!!!
				if (l_dotprodmin<0)
				{
					l_t=l_tmin;
					if (l_t==l_Domain.Min())
						l_res = RX_DROP_AT_START;
					break;
				}

				l_tmax = l_t;
				//double l_temp = fabs(l_t-l_tmin);
				//assert(l_temp>=0.0);  // Peter aug16 2004 
				l_t = l_t + l_dotprod*(l_t-l_tmin);
			}

			///END THOMAS 06 08 04

			l_Alpha = asin(l_dotprod);///l_length);
			
			if (kk==1)
			{
				l_sign=l_Alpha;
			}
			else
				if (l_Alpha*l_sign<0) //mean l_Alpha changed sign
				{
					l_c = l_c/2.0;
					l_sign=l_Alpha;
				}

			if ((fabs(l_Alpha)<p_e))
			{
				l_res = RX_DROP_OK;
				break;
			}
			//to reduce the working length
			//Longer but more stable usint this sub domain redefinition
	//		double l_SubDomainLength; 
	//		l_SubDomainLength = min(t[i]-t[i-1],t[i]-l_t); 
			
	//before the 06 08 04		l_delta = l_dotprod/l_curveLength; //l_SubDomainLength*l_dotprod/l_curveLength;
			
	//before the 06 08 04		l_t+=l_delta*l_c; //F and g are also updated within this function

			if (l_t<=l_Domain.Min())
			{
				l_t=l_Domain.Min();
				l_res = RX_DROP_AT_START;
				break;
			}
			if (l_t>=l_Domain.Max())
			{
				l_t=l_Domain.Max();
				l_res = RX_DROP_AT_END;
				break;
			}

			if (k>p_cid)  
			{
				l_c=l_c/p_c;  // to damp the model
				k=0;
				if (kk>p_kmax)	// to stop the solving if too many iterations
				{
					l_res = RX_DROP_NOT_CONVERGED;
					break;
				}
			}
		}//while (1)
		
		ON_3dPoint l_newPoint = p_Curve->PointAt(l_t);
		if (p_Point.DistanceTo(l_newPoint)<p_Point.DistanceTo(p_PointOUT.GetPos()))
		{
			if (p_pPersistentCurve)
				p_PointOUT.Set(l_t,p_pPersistentCurve);
			else
				assert(0); //debug for nw 08 11 04 p_PointOUT.Set(l_t,p_Curve);
		}
	}//for (i=1;i<span_count +1;i++)

	if (t)
	{onfree(t);t=NULL;}

	return l_res;
}//int GetNearestPoint_LinearMethod_ssd

int GetParamAt(const ON_Curve * p_curve, 
			   const double & p_length, 
			   double & p_t,
			   double fractional_tolerance)
//Set p_t the parameter on the curve at p_lenght for the start of the curve
///
///	return RXCURVE_IN_DOMAIN if 0<=p_length<=this->Length
///	return RXCURVE_OFF_DOMAIN_AT_START if p_length<0, p_t is set to Domain.Min()
///	return RXCURVE_OFF_DOMAIN_AT_END if p_length>this->Length, p_t is set to Domain.Max()
///	return RXCURVE_ERROR if error, do not change p_t
//We want to find the root of f(x) = fabs(Arclenght(t) - p_length)

//	cout<< "In curves.cpp/ GetCurveParamAt() this function havn't been tested yet!!!\n"<<endl;
{
	const ON_Curve* l_curve = dynamic_cast<const ON_Curve*>(p_curve);
	assert(l_curve);

	if (!l_curve)
	{
	#ifdef _GLOBAL_DECLARATIONS_H_
		//#ifndef RHINO_DEBUG_PLUGIN
		error(" GetParamAt only works if l_curve is assigned to a ON_Curve Object",1);
	#endif
		return RXCURVE_ERROR;
	}

	const ON_Interval l_domain = l_curve->Domain();
	double l_l1;
	if (p_length<0.0)
	{
		p_t = l_domain.Min();
		return RXCURVE_OFF_DOMAIN_AT_START;
	}
	if (p_length==0)
	{
		p_t = l_domain.Min();
		return RXCURVE_IN_DOMAIN;
	}

	l_curve->GetLength(&l_l1,fractional_tolerance,&l_domain);
	if (p_length>l_l1)
	{
		p_t = l_domain.Max();
		return RXCURVE_OFF_DOMAIN_AT_END;
	}
	if (p_length==l_l1)
	{
		p_t = l_domain.Max();
		return RXCURVE_IN_DOMAIN;
	}

	// if p_Curve is a Polyline 
	ON_SimpleArray<ON_3dPoint> l_pline_points;
    ON_SimpleArray<double> l_pline_t;
    if (l_curve->IsPolyline(&l_pline_points,&l_pline_t))
	{
		ON_PolylineCurve l_poly; 
		l_poly.m_pline = l_pline_points;
		l_poly.m_t     = l_pline_t;
		if (l_curve) l_curve = NULL;
		return GetParamAtPolyline(&l_poly,p_length,p_t);
	}

	double l_t = l_domain.Mid(); //starting point ... the middle of the curve
	ON_Interval l_Subdomain; //(l_domain.Min(),l_t);

	double l_Dfx = 0.0; //f'(t)
	//to compute f(t)
	double l_Epsi = 0.0; //Step length l_Epsi(n) = -f(x(n))/f'(x(n))
	double l_step = 0.0001; 
	double l_Fact=1.0;
	int l_c2, l_loop = 0;
	double l_oldt =l_t;
	l_Subdomain = l_domain;
	l_t = l_Subdomain.Mid();
	double l_tpreMax = l_domain.Max();
	double l_tpreMin = l_domain.Min();
	double l_LpreMin,l_LpreMax;
	l_LpreMin=0.0;
	l_curve->GetLength(&l_LpreMax,fractional_tolerance,&l_Subdomain);	


	l_Subdomain[1] = l_t;

	l_curve->GetLength(&l_l1,fractional_tolerance,&l_Subdomain);	
	for (l_c2 = 0; l_c2 < 8; l_c2++) { 
		l_loop=0;
		while (fabs(l_l1-p_length)>1.0e-7)
		{
			if (l_l1>p_length)
			{
				l_tpreMax= l_t;
				ON_Interval l_d = ON_Interval(l_domain.Min(),l_tpreMax);
				l_curve->GetLength(&l_LpreMax,fractional_tolerance,&l_d);	
			}
			if (l_l1<p_length)
			{
				l_tpreMin = l_t; 
				ON_Interval l_d = ON_Interval(l_domain.Min(),l_tpreMin);
				l_curve->GetLength(&l_LpreMin,fractional_tolerance,&l_d);	
			}
			//l_t = (l_tpreMin+l_tpreMax)/2.0;   //ORDER 1
			double l_Dlmin = p_length-l_LpreMin;
			double l_Dlmax = p_length-l_LpreMax;
		
			assert(l_tpreMin<l_tpreMax);
				
			if (l_Dlmin*l_Dlmax>0)
				assert(l_Dlmin*l_Dlmax<0);
			l_Dlmin=-l_Dlmin;
			if ((fabs(l_Dlmin)<1.0e-7)&&(fabs(l_Dlmin)<1.0e-7))
				l_t = (l_tpreMin+l_tpreMax)/2.0;
			else
				l_t = (l_tpreMax/l_Dlmax+l_tpreMin/l_Dlmin)/((1/l_Dlmin)+(1/l_Dlmax));   //ORDER 2
			
			l_Subdomain[1] = l_t; 
			l_curve->GetLength(&l_l1,fractional_tolerance,&l_Subdomain);
		
			l_loop++;
			if (l_loop>100)
			{
				// printf("GetCurveParamAt cannot converge after %d iterations\n",l_loop);
				break;
			}
		} // end while
		if(l_loop<=100) // we have a solution
			break;
			
		l_Fact=l_Fact/2.0; // try again
	}// end for

	p_t = min(max(l_t,l_domain.Min()),l_domain.Max());

	if (l_loop>100)
		printf("GetCurveParamAt cannot converge after %d iterations\n",l_loop);
	else if(l_Fact < 1.0)
		printf ("GetCurveParamAt converged after %d iterations with fac= %f\n",l_loop,l_Fact);
	
	//clean up 
	if (l_curve) l_curve = NULL;

	return RXCURVE_IN_DOMAIN;
}//double GetParamAt

int GetParamAtPolyline(const ON_PolylineCurve * p_crv, 
					   const double & p_length, 
					   double & p_t) 
{
	assert(ON_PolylineCurve::Cast(p_crv));

	// if p_Curve is a Polyline 
	ON_SimpleArray<ON_3dPoint> l_pline_points;
    ON_SimpleArray<double> l_pline_t;
	
	double l_residual = p_length; 
    
	if (p_crv->IsPolyline(&l_pline_points,&l_pline_t))
	{
		if (p_length<0)
		{
			p_t = p_crv->Domain().Min();
			return RXCURVE_OFF_DOMAIN_AT_START;
		}

		int l_n = l_pline_points.Count();
		double l_len = 0.0;
		double l_newlen = 0.0;
		double l_delta;
		int i;
		for (i=0;i<l_n-1;i++)
		{
			l_delta = l_pline_points[i].DistanceTo(l_pline_points[i+1]);
			l_len += l_delta;
			if (l_len>p_length)
			{
				double l_ratio = l_residual/l_delta;
				p_t = l_pline_t[i]+l_ratio*(l_pline_t[i+1]-l_pline_t[i]);
				return RXCURVE_IN_DOMAIN;
			}
			l_residual -= l_delta;
		}
		
		//if we arrive here it means the point if off the end 
		p_t = p_crv->Domain().Max();
		return RXCURVE_OFF_DOMAIN_AT_END;
	}

	return RXCURVE_ERROR;
}//int GetParamAtPolyline
