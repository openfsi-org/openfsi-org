#ifndef _RX_UI_TYPES_H_
#define _RX_UI_TYPES_H_

// these are values for the first argument of HC_Set_User_Index
#define RX_OBJECT_TYPE			32
#define RX_HKEY					34
#define RX_INDEX_CV_I			35
#define RXCLASS_HNurbsSurface	37
#define RXCLASS_AMGSubwindow	38
#define RXCLASS_DOVESTRUCTURE	39
#define RX_HKEY_OTHER			40
#define RX_HKEY_SINSURF			41
#define RX_HKEY_COSSURF			42
#define RX_HKEY_VECPLOTSEG		43
#define RX_HKEY_COLORSHELL		44
#define RXCLASS_ENTITY  		45
#define RXINDEX_SUMMARY  		46
#define RXCLASS_RXON_SURF   	47
#define RXCLASS_TAPINGZONE_V3   48
#define RXCLASS_TAPINGZONE_V4   49
#define RXCLASS_PC_3DM_Model    50

#define ON_NURBS_SURFACE			67
#define ON_NURBS_SURFACESIN			68
#define ON_NURBS_SURFACECOS			69
#define ON_NURBS_SURFACEFLAT		70


// values for RX_OBJECT_TYPE
#define RX_NURBS_SURFACE	(void*)256
#define RX_NURBS_CURVE		(void*)257
#define RX_POLYLINE			(void*)258
#define RX_ANGLE_SURF_PAIR	(void*)259

#define RXCLASS_SAIL			103 
#define RXCLASS_GRAPHICSTRUCT	104

#endif

