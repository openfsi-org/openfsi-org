#include "StdAfx.h"
#include "RXCurve.h"
#include "RX_SeamcurveHelper.h"

RX_SeamcurveHelper::RX_SeamcurveHelper(void):
		 xl(0,0,0),yl(0,0,0),zl(0,0,0),xlb(0,0,0),ylb(0,0,0),zlb(0,0,0),
		end1site(0), end2site(0),
		 depth(0),
		 length(0),
		Black_Chord(0),Red_Chord(0),G_Chord(0),

		
		 mould(0), m_sc(0),
		p_slr(0) , c_slr(0),
		NoEnds(0),
		m_type(0)
{
	for(int k=0;k<3;k++) {
		m_Old_Plines[k]=0; 
		m_oldc[k]=0;
		m_OldCurves[k]=0;
	}
}
void RX_SeamcurveHelper::Init(void){

                m_schE1b.Init();	m_schE2b.Init();
		ev1.Init();	 ev2.Init();
		xl=yl=zl=xlb=ylb=zlb=ON_3dVector(0,0,0);
		end1site= end2site=0;
		depth=0;
		length=0;
		Black_Chord=Red_Chord=G_Chord=0;


		mould= 0;this->m_sc=0;
		p_slr =0; c_slr=0;
		NoEnds=0; 
		m_type=0;
	for(int k=0;k<3;k++) {
		m_Old_Plines[k]=0; 
		m_oldc[k]=0;
		m_OldCurves[k]=0;
	}
 }
RX_SeamcurveHelper::~RX_SeamcurveHelper(void)
{
	Clear();
}
int RX_SeamcurveHelper::Clear () 
{
		if(p_slr)
			RXFREE(p_slr); 
		if(	m_Old_Plines[0])
				RXFREE(m_Old_Plines[0]);
		if(m_Old_Plines[2] )		
				RXFREE(m_Old_Plines[2]);
		
		for (int i=0;i<3;i++)
			if (m_OldCurves[i])
			{
				m_OldCurves[i]->Clear();
				delete m_OldCurves[i];
			}

		Init();	
	  return 1;
}//int SC_Finish_SCD

