/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 		P Heppel  November 1996
 *
 * 
 *
 * Modified :
 * sept 97  Prototypes into header files.
 * Erroneous second FCLOSE removed
 */
 
#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include "RelaxWorld.h"
#include "RXEntityDefault.h"
#include "RXSail.h"
#include "stringutils.h"
#include "entities.h"
#include "aeromik.h"
#include "stripes.h"
#include "etypes.h"
#include "script.h"
#include "aero.h"


int Write_One_Shape_File(char*a, char*b)   
{

	FILE *fp ;
	printf("Write_One_Shape_File sail=%s file=%s\n",a,b); 

	SAIL *theSail=g_World->FindModel(a,1);
	if(!theSail || !theSail->IsHoisted())  { 
		printf(" sail %s not hoisted \n", a); 
		return 0;
	}
		fp = RXFOPEN(b,"w");
		if(!fp) return 0;
			
		HC_Open_Segment("/StripesTemp");
			Compute_All_Stripes(theSail,fp);
		HC_Close_Segment();
		FCLOSE(fp);
		HC_Delete_Segment("/StripesTemp");
		return 1;		
}//int Write_One_Shape_File

int Make_Aero_Geometry_And_Run(RXEntity_p  e, char*args){

    int k;
    char*a;
    struct PC_AEROMIK *np;
    char buf[32000];
    if (e->TYPE != PCE_AERO) return(0);
    np = (PC_AEROMIK*)e->dataptr;

    if(!rxIsEmpty(np->before_script))
    {	cout<<"run the `before` script "<<np->before_script<<endl;
        Read_Script(np->before_script);
    }

    a = getenv("RELAX_EXT_AERO");
    if(!a) {
        e->OutputToClient("RELAX_EXT_AERO not found in environment",2);
    }
    else  {
        strcpy(buf,a);		 strcat(buf," ");
        for(k=0; !(rxIsEmpty( np->sailnames[k])) ;k++) { // *(np->sailnames[k])  June 8 2007
            printf(" prepare  Write_One_Shape_File %d on %s %s\n", k, np->sailnames[k],np->shapefiles[k]);
            if(strlen(buf) < 1800) strcat(buf, np->sailnames[k]);strcat(buf," ");
            if(strlen(buf) < 1800) strcat(buf, np->shapefiles[k]);strcat(buf," ");
            if(strlen(buf) < 1800) strcat(buf, np->files[k]);strcat(buf," ");
            Write_One_Shape_File(np->sailnames[k],np->shapefiles[k]);  printf("returned from  Write_One_Shape_File on %s\n",np->sailnames[k]);
        }
        if(strlen(buf) < 1800) strcat(buf,args);
        printf(" call system with `%s`\n",buf);
        if(system(buf))
            e->OutputToClient(" Please check external aerodynamic program definition",2);
    }
    if(!rxIsEmpty(np->after_script))
    {
        Read_Script(np->after_script);
    }
    return(1);
}//int Make_Aero_Geometry_And_Run

int Resolve_Aero_Card(RXEntity_p  e) {
  /* 
    aero	name	before_script	after_script	main	shapefile	pres.mai	genoa	shapefile	pres.gen	
     */
  char name[256],s2[264],s3[264],s4[264],s5[264];
 
  int k,retval=0;
  int cnt;
#ifdef HOOPS   
  if(HC_Parse_String(e->GetLine(),":" ,1,name)) {

    if(HC_Parse_String(e->GetLine(),":" ,2,s2)) {
      if(HC_Parse_String(e->GetLine(),":" ,3,s3)) {
	 
	    struct PC_AEROMIK *p = (PC_AEROMIK *)CALLOC(1,sizeof(struct PC_AEROMIK));
	    
	    p->Kermarec=0;
	    p->before_script=STRDUP(s2);
   		p->after_script= STRDUP(s3);
	    k = 4;
	    cnt=0;
	    while(HC_Parse_String(e->GetLine(),":" ,k,s3)) {
 	      if(!HC_Parse_String(e->GetLine(),":" ,k+1,s4))
			    break;
		  if(!HC_Parse_String(e->GetLine(),":" ,k+2,s5))
				break;
	      cnt++;
	      k += 3;
	      p->files=(char**)REALLOC(p->files,(cnt+1) *sizeof(char*));
	      p->sailnames=(char**)REALLOC(p->sailnames,(cnt+1) *sizeof(char*));
		p->shapefiles=(char**)REALLOC(p->shapefiles,(cnt+1) *sizeof(char*));
	      PC_Strip_Leading(s3); PC_Strip_Trailing(s3);
	      PC_Strip_Leading(s4); PC_Strip_Trailing(s4);
 	      PC_Strip_Leading(s5); PC_Strip_Trailing(s5);

	      p->sailnames[cnt-1]= STRDUP(s3);
	      p->sailnames[cnt] = NULL;

 	      p->shapefiles[cnt-1]= STRDUP(s4);
	      p->shapefiles[cnt] = NULL;    /* marks end of array */

	       p->files[cnt-1]= STRDUP(s5);
	      p->files[cnt] = NULL;    /* marks end of array */
	    }
	    e->PC_Finish_Entity("aero",name,p,(long)0,NULL," ",e->GetLine());
	    
	    e->Needs_Resolving=0;
	    e->Needs_Finishing=0;
	    e->SetNeedsComputing(0);
	    retval=1;	    
	 
	}
      }
    }
#endif
   return(retval);
}
