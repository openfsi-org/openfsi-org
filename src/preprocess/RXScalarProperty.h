#ifndef _RXSCALARPROPERTY_H_
#define  _RXSCALARPROPERTY_H_
#include "RXEntity.h"

class RXScalarProperty :
        public RXEntity
{
public:
	RXScalarProperty(void);
		RXScalarProperty( class RXSail *const p_model);
		virtual ~RXScalarProperty(void);
		RXScalarProperty(const RXSTRING &name,const RXSTRING &p_Buf,  class RXSail *const p_model);
		int CClear();
		int Kill(){ return RXEntity::Kill();}
		int Resolve(void);
	 	int Compute(void);
	 	int Finish(void);
	 	int ReWriteLine(void);
        double ValueAt(const RXSitePt&p)const ;

		int Dump(FILE *fp) const;
		RXSTRING TellMeAbout() const;
        virtual const char* What()const {return "SP";}
protected:
                std::string GetText() const;

};
#endif
