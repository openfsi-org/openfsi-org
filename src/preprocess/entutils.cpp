
/* RelaxII source code
 * Copyright Peter Heppel Associates 1999
 * 
 * written by 
 * 		P Heppel
 *
 *  
This file contains utilities - mainly string-handlers - for entities.c and preproc.c 
June 1999  started.
*/
#include "StdAfx.h"
#include "RXEntityDefault.h"
#include "RXSRelSite.h"
#include "RXSail.h"
#include "RXSeamcurve.h"

#include "etypes.h"

#include "entutils.h"

int  PCS_IsNotAmbiguous(RXEntity_p p){ // p is usually a SITE retval 0 means we cant be sure its NOT ambiguous
/*	A site's  'position ' is ambiguous if one of it's curves is a SEAM so return 0
  */
	if(!p)
		return 0;
	if(p->TYPE != SITE && p->TYPE !=RELSITE)
		return 0;
	if (p->Esail->GetFlag(FL_ISTPN))
	 	return 0;

	Site *s = (Site*) p; assert(dynamic_cast<Site*>(p));
	RXEntity_p c1, c2;
	c1 = s->CUrve[0]; c2 = s->CUrve[1];
	if(c1) {
		sc_ptr sc = ( sc_ptr )c1;
		if(sc->seam) return 0;
	}
	if(c2) {
		sc_ptr sc = ( sc_ptr )c2;
		if(sc->seam) return 0;
	}
	return 1;
}

int Make_Name_Unique(SAIL*const p_sail,const RXSTRING ptype, RXSTRING &pname){
int i=0;
string name =ToUtf8(pname);
string type = ToUtf8(ptype);
  	while(p_sail->GetKeyWithAlias(type.c_str(),name.c_str())) {
		name+="A";
		i++;
	}
	if(i) pname=TOSTRING(name.c_str());
  	return i;
}

int Make_Name_Unique(SAIL*const p_sail,const char*type, char *name){
	int i=0;
	char a[12];
	char namein[256];
	strcpy(a,"a");
	strcpy(namein,name);
 
  	while(p_sail->GetKeyWithAlias(type,name)) {
		if(strlen(name) > 64) {
			strcat(namein,a);
			strcpy(name,namein);
			(*a)++;
		}
		strcat(name,a);
		i++;
	}
  	return i;
}
