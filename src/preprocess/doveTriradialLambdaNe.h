#ifndef _DOVE_TRIRADIALLAMBDANE_MAY07_H_
#define _DOVE_TRIRADIALLAMBDANE_MAY07_H_
#include "doveTriradial.h"
#include "RXON_Matrix.h"

#include "AMGListLevelMaterials.h"

class doveTriradialLambdaNe : public doveTriradial
{
public:
	doveTriradialLambdaNe(void);

public:
	int SetEngine(const int & p_ptrEngine);
	virtual ~doveTriradialLambdaNe(void);

	ON_String Serialize();
	char * Deserialize(char *p_lp) ;

	int SetListLevelMaterials(const AMGListLevelMaterials & p_list); 

	virtual int	Stack(const double p_i, const double p_j,
						 ON_ClassArray<layernodestack> &p_TheStack,
						 double*alpha0);
protected:
	
	AMGListLevelMaterials m_ListLevelMaterials;
	int m_ptrEngine; // pointer on the mat lab engine
};
#endif
