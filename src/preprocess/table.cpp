/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 *   PCT_ReadTable had a 256char buffer which gives trouble with the Analysis Controls
 *   6/5/96    table lines may be quote-terminated DIDNT WORK
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 *  17.11.94 	    PCT_Extract_Series behaves differently if it
 *					finds a sequence of only ONE entry
 *					It prefixes an entry of ZERO
 *					and returns a list of TWO entries
 */


/* date:  3rd October 1994
file : TABLE.C 

 Reads a table (eg) from XLS.
 One Routine = Read_Table. Generates a char*** from the table

 Second Routine. Extracts row (or column) data according to the row (or column)
 header 	*/


#include "StdAfx.h"
#include "RelaxWorld.h"
#include "RXEntity.h"
#include "RXSail.h"
#include "RXQuantity.h"
#include "stringutils.h"

#include "pctable.h"


char PCT_Table_Separator(const char *fname){
    if(stristr(fname,".csv") ) return ',';
    return '\t';
}

int PCT_Part_Of(const char*s1,const char*s2) {  /* true if s2 is a prefix of s1 */
    char buf[256];
    size_t n;
    strcpy(buf,s2);
    if(strtok(buf,"({[")) {
        PC_Strip_Trailing(buf);
        n=strlen(buf);
        return(0==strncasecmp(s1,buf,n));
    }
    else
        n=strlen(s2);

    return(0==strncasecmp(s1,s2,n));
}
int PCT_Read_Table( FILE *f,struct PC_TABLE**t,char sep) {
    int n; char c; char item[2048], format[32];
    int k=0;
    char**line=NULL; //gcc
    long int fileptr=0;
    int firstline=1;
    if(!f) return(0);
    *t=(PC_TABLE *)CALLOC(1,sizeof(struct PC_TABLE)); if(!*t) return(0);

    /* a table has columns delimited by tabs and rows delimited by CRLFs
  IT MUST BE RECTANGULAR. The first non-rectangular line is considered
  to be after the end
  */
    sprintf(format,"%%2045[^%c\n]%%c",sep);
    do{
        if(!k) {
            fileptr = ftell(f);

            line=(char **)CALLOC(((*t)->nc+1),sizeof(char*));
        }

        //	   	n=fscanf(	f,"%2045[^\t\n]%c",item,&c);
        n=fscanf(	f,format,item,&c);
        //	printf("fscanf returned %d <%s> c = %d (%c)\n",n,item,c,c);
        //	if(n > 255) cout<< " PCT_Read_Table bug found\n"<<endl;
        if(n<=0) return(1);	/* EOF */

        if(firstline) {
            (*t)->nc++;
            line=(char **)REALLOC(line,((*t)->nc+1)*sizeof(char*));
        }
        line[k++]=STRDUP(item);

        if(c=='\n' ) {
            if(k!=(*t)->nc) {
                fseek(f,fileptr,SEEK_SET);
                printf(" k = %d but (*t)->nc=%d\n", k,(*t)->nc);
                return(1);
            }

            //	printf( "at EOL %d items\n",k);
            firstline=0;
            k = 0;
            (*t)->nr++;
            (*t)->data = (char ***)REALLOC((*t)->data,(*t)->nr *sizeof(char**));
            (*t)->data[(*t)->nr-1]=line;
        }
    } while(n>0);

    return(1);
}
#ifdef NEVER
int NewPCT_Read_Table_From_Text( char*text,struct PC_TABLE**t) {
    int l, k=0,iret=1;
    char**lines,**wd;
    char*f,*f2;
    int nr, nc=0,ncold =-1;

    *t=(PC_TABLE *)CALLOC(1,sizeof(struct PC_TABLE));
    f =STRDUP(text);
    if(!f || !(*t)) return(0);

    /* a table has columns delimited by tabs and rows delimited by CRLFs
  IT MUST BE RECTANGULAR. The first non-rectangular line is considered
  to be after the end
  So first of all we break into lines and then we break each line into words
  */
    f2=f;
    nr=	 make_into_words(f,&lines,"\n");
    (*t)->data=(char ***)MALLOC(nr*sizeof(void*));

    for(l=0;l<nr;l++) {
        PC_Strip_Trailing(lines[l]);
        nc =  make_into_words(lines[l],&wd,"\t");
        (*t)->data[l]=wd;
        for(k=0;k<nc;k++) wd[k]=STRDUP(wd[k]);
        if(ncold>=0 && (nc!=ncold) 	)
            iret=0;
        ncold=nc;
    }
    (*t)->nr=nr;
    (*t)->nc=nc;
    RXFREE(f);RXFREE(lines);
    return(iret);
}
#endif
int PCT_Read_Table_From_Text( const char*p_text,struct PC_TABLE**t) {
    /* a table has columns delimited by tabs and rows delimited by CRLFs
  IT MUST BE RECTANGULAR. The first non-rectangular line is considered
  to be after the end
  */

    char c; char *item;
    int k=0;
    char**l_line=NULL;
    char*f,*f2;
    int firstline=1;
    int ccount = -1;

    *t=(PC_TABLE *)CALLOC(1,sizeof(struct PC_TABLE));

    char* l_text = STRDUP(p_text);
    PC_Strip_Leading(l_text);
    f = STRDUP(l_text);

    f2=f;
    do{
        item =strtok(f2,"\t\n");  f2=NULL;
        if(!item){
            RXFREE(f); RXFREE(l_text);
            if(k==1) { // usually when the material's last line is blank
                RXFREE(l_line[0]);RXFREE(l_line);
            }
            return(1);
        }
        if(!k) {
            l_line=(char **)CALLOC(((*t)->nc+1),sizeof(char*));
        }
        ccount+=strlen(item)+1;
        c = l_text[ccount];
        if(firstline) {
            (*t)->nc++;
            l_line=(char **)REALLOC(l_line,((*t)->nc+1)*sizeof(char*));
        }
        char*ttt=STRDUP(item);
        l_line[k++]=ttt;
        if(c=='\n' ) {
            if(k!=(*t)->nc) {	printf(" K = %d when nc = %d\n",k, (*t)->nc);
                RXFREE(f); RXFREE(l_text);
                return(1);
            }
            firstline=0;
            k = 0;
            (*t)->nr++;
            (*t)->data = (char ***)REALLOC((*t)->data,(*t)->nr *sizeof(char**));
            (*t)->data[(*t)->nr-1]=l_line;
        }
    } while(1);
    RXFREE(f); RXFREE(l_text);
    return(1);
}
// type is something like 'force' 'extension'
int PCT_List_To_Double (SAIL *sail,char***p_list,int N,double**x,const char*type){ // frees p_list !!
    int k;
    *x=(double*)MALLOC(N*sizeof(double)); //LEAK
    for(k=0;k<N;k++) {
        (*x)[k]=Evaluate_Quantity( sail,(*p_list)[k],type); // we dont know the SAIL
        RXFREE( (*p_list)[k]);
    }
    RXFREE(*p_list);
    *p_list = NULL;
    return(1);
}
// type is something like 'force' 'extension'
int PCT_ListToDoubleByQ (SAIL *sail,char***p_list,int N,double**x,const wchar_t*type) // returns 0 if bad
{ // frees p_list !!
    int k,err; double val;
    int rv=1;
    *x=(double*)MALLOC(N*sizeof(double)); //LEAK
    for(k=0;k<N;k++) {
        err=0;
        class RXENode *nn = dynamic_cast<class RXENode *>(sail);
        val= RXQuantity::OneTimeEvaluate(TOSTRING((*p_list)[k])  ,type,nn,&err);
        if(err)
            rv=0;
        (*x)[k]= val;
        RXFREE( (*p_list)[k]);
    }
    RXFREE(*p_list);
    *p_list = NULL;
    return(rv);
}
/*
 * How this works: there is a very crude use of units.
 * The header may include (unitstring)
 * The values may be of form <number><unitstring>
 * The header unitstring is appended to the number if it doesnt have its own.
 * Then Evaluate_Quantity interprets character strings of type  <number><unitstring>
 *
 * If we want units as part of general expressions, we'd have to do something like
 * saying a unit is ((txt|^|/)+)|% preceded by(a number|whitespace) and
 * followed by optionalwhitespace, then an operator or EOL
 * Then we'd pull out the text and replace it by '*scalefactor'), (QregExp may hve to be looped)
 * then pass to the interpreter  in the usual way.
 * IN the specific case of these tables,we'd append the header unitstring if the dataItem doesnt
 * have its own.
 *
 *A Unit consists of a name, powers of mlt, and a scalefactor (to SI ?or to american?)
 * We should put up a dialog whenever we come across an unknown unitstring.
 *
 **/
int PCT_Extract_Series(struct PC_TABLE *t,int whichway,const char*h,int*N,char***s) {
    char buf2[256],unitstring[256];
    char*cp;
    int r,c,n;
    int units = 0;
    double v;
    *N=0;
    if(whichway==HORIZONTAL) {
        for(r=0;r<t->nr;r++) {
            if(PCT_Part_Of(h,t->data[r][0])) 	{
                /* the series we want is row r */
                /* units might be in that string between brackets
                so get the units. Append them to the table entry if it doesnt have its own
                */
                strcpy(buf2,t->data[r][0]);
                if(strtok(buf2,"({["))
                    cp=strtok(NULL,"]})");
                else
                    cp=NULL;
                if(cp) {
                    strcpy(unitstring,cp);
                    units=1;
                }

                *s = (char **)REALLOC(*s,t->nc*sizeof(char*));
                for(c=1;c<t->nc;c++) {
                    if(units) {
                        n = sscanf(t->data[r][c],"%lf%s",&v,buf2);
                        if(n==1) {
                            sprintf(buf2,"%f%s",v,unitstring);
                            RXFREE( t->data[r][c]);
                            t->data[r][c]= STRDUP(buf2);
                        }
                    }
                    (*s)[(*N)++]=STRDUP(t->data[r][c]);
                }
                break;
            }
        }
    }
    else if(whichway==VERTICAL) {
        for(c=0;c<t->nc;c++) {
            if(PCT_Part_Of(h,t->data[0][c])){
                /* the series we want is col c */
                strcpy(buf2,t->data[0][c]);
                if(strtok(buf2,"({["))
                    cp=strtok(NULL,"]})");
                else
                    cp=NULL;
                if(cp) {
                    strcpy(unitstring,cp);
                    units=1;
                }
                *s = (char **)REALLOC(*s,t->nr*sizeof(char*));
                for(r=1;r<t->nr;r++) {
                    if(units) {
                        n = sscanf(t->data[r][c],"%lf%s",&v,buf2);
                        if(n==1) {
                            sprintf(buf2,"%f%s",v,unitstring);
                            RXFREE( t->data[r][c]);
                            t->data[r][c]= STRDUP(buf2);
                        }

                    }
                    (*s)[(*N)++]=STRDUP(t->data[r][c]);
                }
                break;
            }
        }
    }
    else{

        g_World->OutputToClient(" cannot Extract Series from spreadsheet",2); return(0);

    }

    n = *N;
    return(n);
} 

int PCT_Which_Way(struct PC_TABLE *t,const char**h,int nh, const RXEntity_p e) {
    int k,r,c,flag=0;
    if(!(t->data)) { g_World->OutputToClient("No data in table",2); return(0);	}
    for(r=0;r<t->nr;r++) {
        for(k=0;((k<nh) && (k<t->nr));k++){
            if(PCT_Part_Of(h[k],t->data[r][0]))flag++;
        }
        if(flag==nh) return(HORIZONTAL);
    }
    if(flag) cout<<" caught PCTable bug"<<endl; flag=0;
    for(c=0;c<t->nc;c++) {
        for(k=0;( (k<nh)&&(k<t->nc)) ;k++){
            if(PCT_Part_Of(h[k],t->data[0][c]))flag++;
        }
        if(flag==nh) return(VERTICAL);
    }

    cout<< " CANT IDENTIFY TABLE"<<endl;
    for(r=0;r<t->nr;r++) {
        for(c=0;c<t->nc;c++){
            cout<< t->data[r][c]<<"\t";
        }
        cout<<endl;
    }
    QString buf ("Error reading table in " );
    buf += e->type();
    buf+=  ",";
    buf+= e->name();
    e->OutputToClient(buf,2);
    return(0);
}
int Token_Count(char*p_sin,const char*t){
    char*token;
    char*s = STRDUP(p_sin);
    int k=0;
    token = strtok(s,t);
    while(token) {
        token=strtok(NULL,t);
        k++;
    }
    RXFREE(s);
    return(k);
}
int Append_Table_As_Text(FILE *f, char**text){
    int n; char c;
    int k=0;
    long int fileptr;
    int firstline=1;
    char*line;
    char*dummy=NULL;
    int li;
    size_t ccount=0;
    int flc=0;
    //int lastcount = 0; april 2003

    if(!f) return(0);
    if(*text) ccount=strlen(*text);
    *text= (char *)REALLOC(*text,(ccount+8)*sizeof(char));
    strcat(*text,":table:");   /* PH 1/12/94 MAYBE WRONG */
    ccount=strlen(*text);
    line=(char *)MALLOC(2048*sizeof(char));

    /* a table has columns delimited by tabs and rows delimited by CRLFs
  IT MUST BE RECTANGULAR. The first non-rectangular line is considered
  to be after the end
  */

    do{
        fileptr = ftell(f);
        n=fscanf(	f,"%2048[^\n]%c",line,&c);

        if(n<=0) break;	/* EOF */

        for(;line[strlen(line)-1]=='\t';)line[strlen(line)-1]='\00';

        for(k=0;k<(int)(strlen(line));k++) {
            if(line[k]=='!'){ cout<< " strip shriek\n"<<endl; line[k]='\00';}
        }
        li=strlen(line);
        k = Token_Count(line,"\t");
        if(firstline) flc=k;
        firstline=0;
        /* 	count the tokens  */
        /* append the first line to text regardless */
        /* only append successive lines if their token count is the same */


        if(flc==k) {
            ccount+=li+1;
            *text= (char *)REALLOC(*text,(ccount+2)*sizeof(char));
            dummy=(char *)REALLOC(dummy,(li+2)*sizeof(char));
            sprintf(dummy,"%s\n",line);
            strcat(*text,dummy);
        }
        else
        {
            fseek(f,fileptr,SEEK_SET);
            break;
        }
    } while(n>0);
    if(dummy)RXFREE(dummy);
    RXFREE(line);
    return(1);
}

int  PCT_Free_Table(struct PC_TABLE *t){
    int j,k;
    for(j=0;j<t->nr;j++) {
        for(k=0;k<t->nc;k++) {
            RXFREE(t->data[j][k]);
        }
        RXFREE(t->data[j]);  // MAYBE WRONG
    }
    if(t->data) RXFREE(t->data);
    RXFREE(t);
    return(1);
}

int  PCT_Print_Table(FILE *fp,struct PC_TABLE *t){
    int r,c;
    fprintf(fp,"table    nr = %d nc = %d \n", t->nr,t->nc);
    for(r=0;r<t->nr;r++) {
        for(c=0;c<t->nc;c++) {
            fprintf(fp,"\t%s" ,t->data[r][c]);
        }
        fprintf(fp,"\n");

    }
    return(1);
}
