#ifndef _AERO_H_

#define  _AERO_H_

EXTERN_C int Make_Aero_Geometry_And_Run(RXEntity_p e, char*args);
EXTERN_C int Resolve_Aero_Card(RXEntity_p e) ;

EXTERN_C int Write_One_Shape_File(char*a, char*b);

#endif//#define _AERO_H_

