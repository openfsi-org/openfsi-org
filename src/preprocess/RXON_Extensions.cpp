#include "StdAfx.h"
#include <QDebug>
#ifndef RXQT
#include "dovestructure.h"
#endif
#include "RXON_Surface.h"
#include "RXON_3dPoint.h"
#include "boundingbox.h"
#include "rxON_Extensions.h"

#undef HC_POINT
typedef	struct {float x, y, z;}	myPoint;
#ifndef HC_POINT
#define	HC_POINT	myPoint
#endif

#if defined(_AMG2005) //||defined(AMGTAPER)

#define ONPC_SolveCubicEquation ON_SolveCubicEquation 
#else
extern  int PF_solvecubic(double *q,double *r,double *s,double *t,double *p_r1,double *p_r2,double *p_r3);
#endif



class ONX_Model_Object* rxONConvertObject( class ONX_Model_Object *p_mo)
{

    if(dynamic_cast<const ON_Point*>(p_mo->m_object)) {
        return p_mo;
    }
    if(const ON_Curve* cc= dynamic_cast<const ON_Curve*>(p_mo->m_object)) {
        if(const ON_LineCurve* c =dynamic_cast<const ON_LineCurve*>(p_mo->m_object)) {
            rxON_LineCurve*e = new rxON_LineCurve( *c );
            delete c;
            p_mo->m_object=e;
        }
        else if(const ON_NurbsCurve* c =dynamic_cast<const ON_NurbsCurve*>(p_mo->m_object)) {
            rxON_NurbsCurve*e = new rxON_NurbsCurve( *c );
            delete c;
            p_mo->m_object=e;
        }
        else {
            ON_NurbsCurve nc;
            if( cc->GetNurbForm(nc)){
                rxON_NurbsCurve*e = new rxON_NurbsCurve( nc);
                delete cc;
                p_mo->m_object=e;
            }
            else
                cout<<" WIERD cant get a nurbsForm from "<<p_mo->m_object->ClassId()->ClassName() <<endl;
        }
        return p_mo;
    } //curve
    if(dynamic_cast<const ON_Surface*>(p_mo->m_object) ){

        return p_mo;
    }
    if( const ON_Brep* br = dynamic_cast<const ON_Brep*>(p_mo->m_object) ){
        int l_n = br->m_F.Count();
        if(l_n>1) printf(" only the first surface in a Brep is translated (this brep has %d)\n",l_n);
        if(l_n<1) { cout<<" this Brep has no surfaces"<<endl; return p_mo;};
        const ON_Surface *ss = ON_Surface::Cast(&(br->m_F[0])); //			(ON_Surface *)(br->m_F[0]);
        const ON_NurbsSurface *nsp = ON_NurbsSurface::Cast (ss );
        if(nsp) {
            RXON_NurbsSurface *rxon = new RXON_NurbsSurface( *nsp);
            //delete ns;				br->m_F[0]=rxon;
            p_mo->m_object=rxon;
        }
        else {
            ON_NurbsSurface ns;
            if( ss->GetNurbForm(ns)){
                RXON_NurbsSurface *rxon = new RXON_NurbsSurface(ns);
                //delete ss; 	br->m_F[0]=rxon;
                p_mo->m_object=rxon;
            }
            else
                cout<<" WIERD cant get a nurbsForm from "<<p_mo->m_object->ClassId()->ClassName() <<endl;
        }

        return p_mo;
    }

    cout<<	p_mo->m_object->ClassId()->ClassName() <<" rx conversion  not implemented "<<endl;
    return p_mo;
}

rxON_String::rxON_String(void)
{

}
rxON_String::rxON_String(const char *s)
    :ON_String(s)
{
}
rxON_String::~rxON_String(void)
{
}
int rxON_String::Replace_String(const ON_String p_a, const ON_String p_b) //Peters addition
{

    int k,la,lb,count=0;
    ON_String temp;
    la=p_a.Length (); lb=p_b.Length();
    while (1) {
        k=Find(p_a.Array());
        if(k<0) break;
#ifdef _DEBUG
        ON_String tl,tr;
        tl=Left(k);
        tr = Mid(k+lb);
#endif
        temp = Left(k) + p_b + Mid(k+la);
        *this = temp;
        count++;

    }
    return count;
}

rxON_String& rxON_String::operator=(const ON_String& src)
{
    const char*lp = src.Array();
    const int c = src.Length ();
    this->Empty();
    this->Append(lp,c);
    return *this;
}


rxON_LineCurve::rxON_LineCurve(void)
{
}

rxON_LineCurve::~rxON_LineCurve(void)
{
}
rxON_LineCurve::rxON_LineCurve(class ON_3dPoint a,class ON_3dPoint b)
    : ON_LineCurve(a,b)
{
}
rxON_LineCurve::rxON_LineCurve  (const ON_LineCurve &n) 
    : ON_LineCurve(n)
{
}
bool rxON_LineCurve::Offset(double,class rxON_LineCurve &o){
    cout<<"no rxON_LineCurve::Offset "<<endl;
    return false;
}
HC_KEY  rxON_LineCurve::DrawHoops(const char*seg, const char *atts)const{
    HC_KEY k=0;
    cout<<"no rxON_LineCurve::DrawHoops"<<endl;
    return k;
}
rxON_Mesh::rxON_Mesh(void)
{
}
rxON_Mesh::~rxON_Mesh(void)
{
}

rxON_Object::rxON_Object(void)
{
}
rxON_Object::~rxON_Object(void)
{
}

//rxON_NurbsSurface::rxON_NurbsSurface(void)
//{
//}
//rxON_NurbsSurface::~rxON_NurbsSurface(void)
//{
//}
//




rxONX_Model::rxONX_Model(void)
{
}
rxONX_Model::~rxONX_Model(void)
{
}

rxON_BoundingBox::rxON_BoundingBox(void):ON_BoundingBox()
{}

rxON_BoundingBox::rxON_BoundingBox(const ON_BoundingBox a) 
    :ON_BoundingBox (a)
{
}
rxON_BoundingBox::rxON_BoundingBox(const ON_3dPoint a, const ON_3dPoint b)
    :ON_BoundingBox(a,  b)
{
}

rxON_BoundingBox::~rxON_BoundingBox(void)
{
}
bool rxON_BoundingBox::GrowAllRound(const double d)
{
    ON_3dPoint x = Max();
    ON_3dPoint n = Min();

    x+=ON_3dVector(d,d,d);
    n+=ON_3dVector(-d,-d,-d);

    Set(x,true);
    Set(n,true);

    return true;
}

rxON_PolyCurve::rxON_PolyCurve(void)
{}
rxON_PolyCurve::~rxON_PolyCurve(void){}
HC_KEY  rxON_PolyCurve::DrawHoops(const char*seg, const char *atts)const {
    HC_KEY rc=0;
    cout<<"no rxON_PolyCurve::DrawHoops"<<endl;
    return rc;
}




rxON_PolylineCurve::rxON_PolylineCurve(void)
{}
rxON_PolylineCurve::rxON_PolylineCurve(class ON_Polyline a)
    :ON_PolylineCurve( a)
{}
rxON_PolylineCurve::~rxON_PolylineCurve(void)
{}

HC_KEY rxON_PolylineCurve::DrawHoops(char const *,char const *)const
{
    HC_KEY k=0;
    cout<<"no rxON_PolylineCurve::DrawHoops"<<endl;
    return k;
}
int rxON_PolylineCurve::IsInside(class ON_3dPoint p) {
    int rc=0;
    cout<<"no rxON_PolylineCurve::IsInside "<<endl; assert(0);
    return rc;
}
int rxON_PolylineCurve::FirstIntersect(const ON_Line &theline, double *tOnLine, double *tOnP){
    int rc=0;
    cout<<"no rxON_PolylineCurve::FirstIntersect  "<<endl; assert(0);
    return rc;
}

rxON_NurbsCurve::rxON_NurbsCurve(  )
    :m_segLengths(0)
{
}
rxON_NurbsCurve::rxON_NurbsCurve( const ON_NurbsCurve& src )
    : ON_NurbsCurve(src )
    , m_segLengths(0)
{
    this->Reset_Seglengths ();
}
rxON_NurbsCurve::~rxON_NurbsCurve( ) 
{
    if(m_segLengths)
        onfree(m_segLengths);
}
// to do seglengths right we should reset them whenever we detect a change in knot or cv.
//but that it difficult because we dont have access to these methods. 

bool rxON_NurbsCurve::Reset_Seglengths(int i)
// if it were coded, i would be an index telling which seglengths to reset
{
    int j;
    if(!m_segLengths)
        m_segLengths = (double*)onmalloc( this->m_knot_capacity*sizeof(*m_segLengths));
    if(!m_segLengths) return false;
    for(j=0;j<m_knot_capacity; j++)
        m_segLengths[j]=-1.0;
    return false;
}
bool rxON_NurbsCurve:: Extend( const ON_Interval& domain ) 
{
    bool rc = ON_NurbsCurve::Extend (domain);
    Reset_Seglengths();
    return rc;
}

void rxON_NurbsCurve::Destroy()
{
    if(m_segLengths)
        onfree(m_segLengths);
    ON_NurbsCurve::Destroy();
}
void rxON_NurbsCurve::Initialize()
{
    ON_NurbsCurve::Initialize();
    m_segLengths=0;
}
ON_BOOL32 rxON_NurbsCurve::SetCV( int i, ON::point_style style, const double* Point )
{
    Reset_Seglengths();
    return ON_NurbsCurve::SetCV(  i,style,Point );
}
ON_BOOL32 rxON_NurbsCurve::SetCV( int i, const ON_3dPoint& point )
{
    Reset_Seglengths();
    return ON_NurbsCurve::SetCV( i,point);
}
ON_BOOL32  rxON_NurbsCurve::SetCV( int i, const ON_4dPoint& point )
{
    Reset_Seglengths();
    return ON_NurbsCurve::SetCV(i, point);
}
bool rxON_NurbsCurve::ReserveKnotCapacity(int desired_capacity)// safety on the saved lengths.
{
    // If m_knot_capacity == 0 and m_knot != NULL, then the user
    // has hand built the ON_NurbsCurve.m_knot array and is responsible
    // for making sure it's always big enough.
    bool rc = true;
    if ( desired_capacity > m_knot_capacity ) {
        if ( !m_segLengths ) {
            m_segLengths = (double*)onmalloc(desired_capacity*sizeof(*m_segLengths));
        } // end if m_knot
        else if ( m_knot_capacity > 0 ) {
            // existing m_knot[] is too small and the fact that
            // m_knot_capacity > 0 indicates that ON_NurbsCurve is
            // managing the m_knot[] memory, so we need to grow
            // the m_knot[] array.
            m_segLengths = (double*)onrealloc(m_segLengths,desired_capacity*sizeof(*m_segLengths));
        }
    }
    rc=ON_NurbsCurve::ReserveKnotCapacity(desired_capacity);
    Reset_Seglengths();
    return rc;
}
double  rxON_NurbsCurve::SecondOrderDt(const rxON_NurbsCurve &p_c,double p_t,const ON_3dPoint&p_q, int p_Degree, int p_side, int*hint){
    /* this seems unstable if we take a cubic from only 2 derivatives.  We're missing some d3 terms which may be important.
SO lets try second order.  , seeeing Q->0. */

    ON_3dPoint p;
    ON_3dVector p1,p2,p3,p4,v; // 1st and 2nd derivatives.
    double l_q=0,l_r=0,l_s=0,l_t=0,r1,r2,r3;
    double vv[15];
    int rc;

    if(p_Degree >=3) {
        p_c.Evaluate(p_t,4,3,vv,p_side,hint);
        p=ON_3dPoint(vv);
        p1=ON_3dVector(&vv[3]);
        p2=ON_3dVector(&vv[6]);
        p3=ON_3dVector(&vv[9]);
        p4=ON_3dVector(&vv[12]);
        v = p - p_q;
        // term in dt^0
        l_t = ON_DotProduct(p1,v);
        // term in dt^1
        l_s = ON_DotProduct(p2,v) + ON_DotProduct(p1,p1);
        // term in dt^2
        l_r = ON_DotProduct(p2,p1) * 3. /2. + ON_DotProduct( v,p3)/2.;
        // term in dt^3
        l_q = ON_DotProduct(p2,p2) /2.0+ ON_DotProduct( p1, p3) *2./3. + ON_DotProduct(v,p4)/6.;
        // that's all the cubic terms
    }
    else if (p_Degree  ==2 ) {
        p_c.Evaluate(p_t,3,3,vv,p_side,hint);
        p=ON_3dPoint(vv);
        p1=ON_3dVector(&vv[3]);
        p2=ON_3dVector(&vv[6]);

        v = p - p_q;
        // term in dt^0
        l_t = ON_DotProduct(p1,v);
        // term in dt^1
        l_s = ON_DotProduct(p2,v) + ON_DotProduct(p1,p1);
        // term in dt^2
        l_r = ON_DotProduct(p2,p1) * 3. /2. ;
        // term in dt^3
        l_q = 0.0;
    }
    else if(p_Degree ==1 ) {
        p_c.Evaluate(p_t,2,3,vv,p_side,hint);
        p=ON_3dPoint(vv);
        p1=ON_3dVector(&vv[3]);
        v = p - p_q;
        // term in dt^0
        l_t = ON_DotProduct(p1,v);
        // term in dt^1
        l_s =  ON_DotProduct(p1,p1);
        // term in dt^2
        l_r = 0.0;
        // term in dt^3
        l_q = 0.0;
        // that's all the cubic terms
    }


    /*Description:
  Solve the cubic equation q*X^3 + r*X^2 + s*X + t = 0.
Returns:
  0: failure (q = 0, r = 0, s = 0 case)
  1: three real roots (r1 <= r2 <= r3)
  2: one real root (r1) and two complex conjugate roots (r2 +/- (r3)*sqrt(-1))
  3: two real roots (r1 <= r2) (q = 0, r != 0 case)
  4: two complex conjugate roots (r1 +/- (r2)*sqrt(-1)) (q = 0, r != 0 case)
  5: one real root (r1) (q = 0, r = 0, s != 0 case)
*/	
    if(p_Degree < 3)
        l_q=0.0 ;
    if(p_Degree < 2)
        l_r=0.0 ;
    rc= ONPC_SolveCubicEquation(l_q,l_r,l_s,l_t,&r1,&r2,&r3);

    switch (rc) {
    case 0:
        printf(" SolveCubic Failed %d \n",rc);
        return 0;
    case 4:

        // now try the linear solution dt = -v.dp (normalized)
        return(-l_t/p1.LengthSquared());

        return 0;
    case 1: { // return the smallest root in the sense of (v.p1) =l_t
        if((-l_t ) >=0) {
            if(r1>=0) return r1;
            if(r2>=0) return r2;
            if(r3>=0) return r3;
        }
        else {
            if(r3<=0) return r3;
            if(r2<=0) return r2;
            if(r1<=0) return r1;
        }
        rc =  ONPC_SolveCubicEquation(0.0,l_r,l_s,l_t,&r1,&r2,&r3);
        if(!rc)
            printf("Cubic case 1 didnt catch even on  quadratic r1=%f r2=%f r3=%f coefs %f %f %f %f\n",r1,r2,r3,l_q,l_r,l_s,l_t);
        return r1;
    }
    case 2:
        return r1;
    case 3: // return the smallest of (r1,r2) in the sense of -(v.p1)
        assert(r1<=r2);
        if((-l_t) >=0) {
            if(r1>=0) return r1;
            if(r2>=0) return r2;
        }
        else {
            if(r2<=0) return r2;
            if(r1<=0) return r1;
        }
        rc =  ONPC_SolveCubicEquation(0.0,0.0,l_s,l_t,&r1,&r2,&r3);
        if(!rc)
            printf("quadratic case 3 didnt catch even on linear (q = 0, r != 0 case)  -t=%f r1=%f r2=%f coefs %f %f %f %f\n",-l_t,r1,r2,l_q,l_r,l_s,l_t);
        return r1;
    case 5:
        return r1;
    default:
        cout<<"rxON_NurbsCurve::SecondOrderDt default case"<<endl;	assert(0);
    }

    return 0;
}



bool rxON_NurbsCurve::GetClosestPoint( const ON_3dPoint& q,
                                       double* p_t,       // parameter of local closest point returned here
                                       double maximum_distance,
                                       const ON_Interval* sub_domain
                                       ) const
{
    // return value : true if a pt is found within maximum_distance  else FALSE

    // it is VERY SLOW to test every localBB of the curve.  Some 40% of mesh time for filaments.
    //  A better technique would be to recursively sub-divide the curve.
    //	And as it gets called a lot on the SAME CURVE, we should  keep the curve's BB (as User data?) and use it for a prelim check.
    // or even keep the whole tree of sub-BB's
    // or some clever sorted list.

    double dist = 2.0 * maximum_distance;
    double t0,t1;
    double tperp=0, ldist=0;
    if(sub_domain) {
        t0 = sub_domain->Min(); t1 = sub_domain->Max();
    }
    else {
        t0 = Domain().Min(); t1 = Domain().Max();
    }

    // Local BB METHOD  OK if max_dist is small.
    int  n, order = Order();
    ON_4dPointArray l_Pts;
    rxON_BoundingBox bb;
    double kn,kn1;
    bool l_trace;
#ifdef _RX_TRACE
    cout<< " (ON_NurbsCurve::GetClosestPoint)"<<endl;
#endif
    l_trace=false;
    for(n=order-2; n<KnotCount()-order+1;n++) {
        kn = Knot(n); kn1 = Knot(n+1);
        if (kn1 >= t0 && kn <= t1)  {   // we are within scope.

            RXON_GetLocalBB(this, &bb,  n, maximum_distance );// a BB around the relevant CVs, grown by max_dist

            if(bb.IsPointIn(q)) {
                int l_rc;
                kn = max(kn,t0); kn1=min(kn1,t1);
                tperp = (kn + kn1)/2.0; // a bad guess. SLOW
                l_rc = this->PC_Find_Perpendicular(kn,kn1,q,&tperp,&ldist,1.0e-8);

                switch(l_rc) {
                case PCF_OK :
                case PCF_OFF_START:
                case PCF_OFF_END:
                    if(ldist < maximum_distance)
                        l_Pts.AppendNew()=ON_4dPoint(ldist,tperp,(double)n,(double) l_rc);// double x, y, z, w;

                    break;// the case
                case PCF_UNCONVERGED:
                    l_trace=true;
                    break;
                default:
#ifdef WIN32
                    assert(0); // this is windows only (!)
#endif
                    break; // do nothing
                } //end switch
            } //if in BB

        } // if t within scope
    } //for N

    // now we have some candidates in l_Pts
    // complete solutions are
    //		l_rc==PCF_OK;
    //		l_rc==PCF_OFF_START && n == order-2
    //		l_rc==PCF_OFF_END	&& n == KnotCount()-order

    //	other solutions are less preferred
    // if there is a preferred solution we take the one with the least ldist
    // else of all the solutions we take the one with the least ldist
    int nc= l_Pts.Count();
    if(!nc) {
        if(l_trace) cout<< "ON_NC GetClosestPoint::no result"<<endl;
        return false;
    }
    int nfound=-1;

    for(n=0;n<nc;n++) {
        ON_4dPoint *p = l_Pts.At(n);
        double d=p->x;
        double t=p->y;
        int nn = (int) p->z;
        int rc = (int) p->w;

        if		(rc==PCF_OK
                 ||	(rc==PCF_OFF_START && nn == order-2)
                 ||	(rc==PCF_OFF_END	&& nn == KnotCount()-order ) )
        {
            if(d<dist) {
                dist=d;
                *p_t=t;
                nfound=nn;
            }
        }
    }
    if(nfound >=0) {
        if(l_trace) cout<< ".. but got a result"<<endl;
        l_Pts.Empty();
        return true;
    }
    // now search for any solution.

    for(n=0;n<nc;n++) {
        ON_4dPoint *p = l_Pts.At(n);
        double d=p->x;
        double t=p->y;
        int nn = (int) p->z;
        //	int rc = (int) p->w;
        if(d<dist) {
            dist=d;
            *p_t=t;
            nfound=nn;
        }
    }	// for n

    l_Pts.Empty();
    if(nfound >=0) {
        if(l_trace) cout<< ".. but got a result"<<endl;
        return true;
    }
    if(l_trace) cout<< "..GetClosestPoint NO result"<<endl;
    return false;
} //  BOOL rxON_NurbsCurve::GetClosestPoint


HC_KEY rxON_NurbsCurve::DrawHoops(const char *seg, const char *atts) const
{
    HC_KEY  key=0;
    int i,kc_ON,kc_h, k, degree = Degree(),  cpcount =CVCount() ;

    HC_POINT *cps=NULL;
    float *weights =NULL,  *knots;
    double kspan;
    if(!IsValid())
        return 0;
    cps= (HC_POINT *) malloc(cpcount* sizeof(HC_POINT )); // was sizeof(ON_3fPoint !!!!)
    weights=(float*) malloc(cpcount*sizeof(float));
    kc_ON=KnotCount();
    kc_h=cpcount +Order();

    for(i=0;i<cpcount;i++) {
        ON_3dPoint l_p = CV(i);
        cps[i].x=(float) l_p.x; cps[i].y=(float)l_p.y; cps[i].z=(float)l_p.z;
        weights[i]=(float)Weight(i);
    }

    knots=(float*) malloc((kc_h)*sizeof(float));
    int L_OFFSET =0;
    //	L_OFFSET =(kc_h-kc_on);

    // we don't know whether HOOPS wants the extra knots at the beginning or the end.
    // so try L_OFFSET=0 to put them at the end
    // and l_OFFSET = (kc_h-kc_on) to put them at the start
    L_OFFSET = 1;
    kspan = 1.0; // Knot(kc_on-1)-Knot(0);
    for(i=0,k=0;i<L_OFFSET;i++,k++){
        knots[k]=(float)(Knot(0)-Knot(0));
    }
    for(i=0;i<kc_ON;i++,k++) {
        knots[k]=(float)((Knot(i)-Knot(0))/kspan);
    }
    for(;k<kc_h;k++){
        knots[k]=(float)((Knot(kc_ON-1)-Knot(0))/kspan);
    }
    // try normalizing the knots
    knots[0]=0.0;
    for(i=1;i<kc_h;i++) {
        if(knots[i]<knots[i-1])
            knots[i]=knots[i-1];
    }
#ifdef HOOPS
    if(seg) HC_Open_Segment(seg);

    key= HC_KInsert_NURBS_Curve (degree, cpcount,cps,weights,knots,0,1) ;
    if(seg) HC_Close_Segment();
#else
    key=0;
#endif
    free(cps);
    free(weights);
    free(knots);
    return(key);
#undef L_OFFSET

}

ON_Xform rxON_NurbsCurve::TrigraphAt(const double p_t) const  
{
    ON_Xform r;
    ON_3dVector x,y,z; ON_3dPoint p;
    Ev1Der(p_t,p,x); x.Unitize();
    y = ON_CrossProduct(ON_3dVector(0,0,1),x);
    if(!y.Unitize()) {
        y= ON_CrossProduct(ON_3dVector(0,1,0),x);
        y.Unitize();
    }
    z = ON_CrossProduct(x,y);
    r=ON_Xform(p,x,y,z);
    // _DEBUG
    double dd = r.Determinant();
    assert(fabs(dd)> 1e-3);
    //#endif
    return r;
}


bool rxON_NurbsCurve::MapToSurface(const ON_Surface * p_From, 
                                   const ON_Surface * p_To,
                                   ON_Curve *       p_crv) const
{
    rxON_NurbsCurve l_nurbs;
    if(!this->GetNurbForm(l_nurbs)) {cout<<"MapToSurface: no NurbForm"<<endl; return false;}

    int l_nbcv = l_nurbs.CVCount();
    int i;

    if (!ON_Curve::Cast(p_crv))
        return 0;


    for (i=0;i<l_nbcv;i++)
    {
        RXON_3dPoint l_cv;
        l_nurbs.GetCV(i,l_cv);
        ON_3dPoint l_Mapedcv;

        l_cv.MapToSurface(p_From,p_To,l_Mapedcv);
        l_nurbs.SetCV(i,l_Mapedcv);
    }

    ON_NurbsCurve::Cast(p_crv)->operator = (l_nurbs);
    return 1;
}





int rxONX_Model::CountTrailingNumerics(const wchar_t*s) {
    int rc=0;
    int i,n;
    n = (int)wcslen(s);
    for(i= n-1;i>=0;i--) {
        if(isdigit(s[i]) || s[i]=='.' )
            rc++;
        else
            break;
    }
    return rc;
}
ON_String rxONX_Model::ExtractName(const ON_String  & p_Line)
//*Extract the PURE name from a line with attributes and so on 
{   
    if(p_Line.IsEmpty())
        return p_Line;
    const char *lp = p_Line.Array(); assert(lp);

    size_t pos = strcspn(lp,",;()$&[] ");

    ON_String rc = p_Line.Left(pos);
    return rc;
} // ExtractName
int rxONX_Model::IncrementName(  ONX_Model_Object *p_pObject, const double dx )
{
    // if the last character is a number, increment it.
    // if the last character is a letter, append "_0"

    int  len, nnum;
    double val;
    ON_wString l_n,head,tail;
    ON_3dmObjectAttributes l_att;
    ON_wString l_Name,addition;
    ON_wString l_attrib;
    l_n = ExtractName(p_pObject->m_attributes.m_name);

    l_att = p_pObject->m_attributes;
    l_Name =p_pObject->m_attributes.m_name;
    l_attrib = l_Name.Right(l_Name.Length()-l_n.Length());
    l_Name = l_Name.Left(l_n.Length());
    l_Name.TrimLeft();l_Name.TrimRight();
    len = l_Name.Length ();
    if(len) {
        nnum =  CountTrailingNumerics(l_Name.Array());
        if(!nnum)
            addition = ON_String("_001");
        else {
            tail = l_Name.Right(nnum);
            head = l_Name.Left(len-nnum);
            //val= _wtoi(tail.Array());
            val=atof(ON_String(tail).Array());
            val=val+dx;
            addition.Format("%012.8f",val);addition.TrimRight(L"0");
            l_Name = head;

        }
    } // if len
    else //  short
        addition.Format("n_%012.8f",dx); addition.TrimRight(L"0");
    l_Name= l_Name+addition+l_attrib;
    l_att.m_name =l_Name;
    //	RhinoApp().ActiveDoc()->ModifyObjectAttributes(p_pObject,l_att);
    p_pObject->m_attributes.m_name=l_Name;

    return 1;
}
bool  rxONX_Model::ExtractNumericPostfix(const ONX_Model_Object &o, double*x) { // return true if theobjects name has a postfix.
    int  len, nnum;
    ON_wString l_n,tail;

    ON_wString l_Name;
    *x=0.;
    l_n = ExtractName(o.m_attributes.m_name);

    l_Name =o.m_attributes.m_name;

    l_Name = l_Name.Left(l_n.Length());
    l_Name.TrimLeft();l_Name.TrimRight();
    len = l_Name.Length ();
    if(!len) return false;

    nnum =  CountTrailingNumerics(l_Name.Array());
    if(!nnum)
        return false;
    //		now extract  X
    tail = l_Name.Right(nnum);
    *x = atof(ON_String(tail).Array());
    return true;

}

int rxONX_Model::MakeNamesUnique(void)
{
    ONX_Model_Object*onextdiff=0;
    int changing, count,j,k;
    double x1,x2,dx,delta;
    int *indices = new int[m_object_table.Count()];
    do{
        cout<< "/////////////////////////// \n name change pass  "<<endl;
        changing=0;
        m_object_table.Sort (ON::heap_sort,indices,comparebyname);

        for(int i=0;i<m_object_table.Count()-1;i++) {
            ONX_Model_Object &o = m_object_table[ indices[i]];

            if(!o.m_attributes.IsVisible() ) continue;
            if (!this->m_layer_table[o.m_attributes.m_layer_index].IsVisible())
                continue;
            //		walk up the list unti we find a different one - may go to the end. Count how many, including o, are the same.
            onextdiff=0; count=0;
            for(j=i+1; j< m_object_table.Count();j++) {
                ONX_Model_Object *o2 = &(m_object_table[ indices[j]]);
                if(!o2->m_attributes.IsVisible() ) continue;
                if(comparebyname(&o,o2)) {
                    onextdiff=o2;
                    break;
                }
                count++;
                changing++;
            } // for J

            // Extract the number at the end of o, and at the end of  nextdiff.
            // (dx will be (n(firstdiffafter) - n(o)) / count
            dx=1.;
            if(count<1)
                dx=1.0;
            else {
                if( ExtractNumericPostfix(o,&x1) && (onextdiff&&ExtractNumericPostfix(*onextdiff,&x2)))
                    dx = (x2-x1)/((double)(count+1));
            }
            // now increment all the Js
            for( j=i+1,k=0; j< m_object_table.Count() &&k<count ;j++,k++) {
                ONX_Model_Object *o2 = &(m_object_table[ indices[j]]);
                printf("%20s %S ",o2->m_object->ClassId()->ClassName(), o2->m_attributes .m_name.Array()   );		cout<< " >>> "<<endl;
                delta = dx*((double)(k+1));
                IncrementName(o2,delta);
                printf(" %S\n", o2->m_attributes .m_name.Array());
            } // for J
        } // for i
        printf("  %d names changed\n",changing);
    }while( changing);
    /*

we'll typically get a listing like 
 453  428 ON_NurbsCurve <T300_98>
 454  127 ON_NurbsCurve <T300_98>
 455  429 ON_NurbsCurve <T300_99>
 456  128 ON_NurbsCurve <T300_99>
 457  349 ON_Point <clew,$fix=full>
So we don't need to re-sort, we should look ahead for the next indexed object with a different
name and 'interpolate' the intermediate names. 
if we use decimal numbers this shouldnt result in too much duplication.


*/
    delete [] indices;
    return 0;
}
int rxONX_Model::comparebytype(const ONX_Model_Object*a,const ONX_Model_Object*b){
    /*
    unknown_object_type  =          0,

    point_object         =          1, // some type of ON_Point
    pointset_object      =          2, // some type of ON_PointCloud, ON_PointGrid, ...
    curve_object         =          4, // some type of ON_Curve like ON_LineCurve, ON_NurbsCurve, etc.
    surface_object       =          8, // some type of ON_Surface like ON_PlaneSurface, ON_NurbsSurface, etc.
    brep_object          =       0x10, // some type of ON_Brep
    mesh_object
   -1 if *b < *a is true
    1 if *a < *b is true
    0 if niether *a < *b nor *b < *a is true
*/
    int a1,b1;
    if(a->m_object->ObjectType()==ON::mesh_object) a1=-1;
    else a1 = a->m_object->ObjectType();
    if(b->m_object->ObjectType()==ON::mesh_object) b1=-1;
    else b1 = b->m_object->ObjectType();

    if(b1<a1) return 1;
    if(a1<b1) return -1;
    return 0;
}


int rxONX_Model::comparebyname(const ONX_Model_Object*a,const ONX_Model_Object*b){


    return a->m_attributes.m_name.CompareNoCase( b->m_attributes.m_name );
    if( b->m_attributes.m_name <a->m_attributes.m_name ) return 1;
    if( b->m_attributes.m_name > a->m_attributes.m_name) return -1;
    return 0;
}

int rxONX_Model::SortEntities( int (*compar)(const ONX_Model_Object*,const ONX_Model_Object*)  ) {
    int iret=1;
    ON_ClassArray<ONX_Model_Object>  &a =m_object_table;
    a.HeapSort(compar  ); // or QuickSort

    return iret;
}
//static
bool ONX_IsValidNameFirstChar( const wchar_t c )
{
    if ( c > 127 )
        return true;
    if ( c < '0' )
        return false;
    if ( c <= '9' )
        return true;
    if ( c < 'A' )
        return false;
    if ( c <= 'Z' )
        return true;
    if ( c == '_' )
        return true;
    if ( c < 'a' )
        return false;
    if ( c <= 'z' )
        return true;
    return false;
}
#ifndef RXQT
#ifndef AMGTAPER
int dovestructure::GetStackType(void) { cout<<" dovestructure getstack dummy"<<endl; assert(0); return 0;}
#endif
#endif
HC_KEY ONCurveDrawHoops(const ON_Curve *c, const char*seg, const char*atts){ // return -1 for baddraw


#define NPTS 64
#ifdef HOOPS
    HC_KEY key=-1;
    rxON_NurbsCurve l_n;
    int i;
    if(!c->IsValid()) {
        return 0;
    }
    if(!ON_CurveOnSurface::Cast(c))
        if(c->GetNurbForm(l_n)) {
            key = l_n.DrawHoops(c->ClassId()->ClassName(),atts);
            return key;
        }

    if(seg) HC_Open_Segment(seg);
    HC_Restart_Ink();
    for(i=0;i<=NPTS;i++) {
        double t =c-> Domain().Min()*(double) (NPTS-i) + c->Domain().Max()*(double) i;
        t = t/(double) NPTS;
        ON_3dPoint p = c->PointAt(t);
        HC_Insert_Ink(p.x,p.y,p.z);
    }
    if(seg) HC_Close_Segment();
#endif
    return 0;

}
int ONCurveIntersectByGeometry(const ON_Curve *c,const ON_Plane & p_p,
                               ON_3dPointArray &p_Results,
                               double p_SolveTol,
                               const ON_Interval *p_d,
                               double * p_pseed,
                               int p_even)
//bool ON_Curve::IntersectByGeometry(const ON_Plane & p_p,
//						ON_3dPointArray & p_Results,  // SB 3d for u,v,t
//						double p_SolveTol,
//						const ON_Interval * p_d,// interval on p_p
//						double * p_pseed, // default NULL
//						int p_even ) const // default 1 (=even)
{
    //!Finds the intersection between the curve and a Plane
    /*!
 an intersection is found if the curve passes thru the plane
 if an intersection is found set p_pt on the intersection
 returns RX_CRVPL_CROSS if an intersection is found between the end and the start of the curve
 returns RX_CRVPL_NOTCROSS if NO intersection is found between the end and the start of the curve
 returns RX_CRVPL_PARALLEL if the line is parallel to the plane
 returns RX_CRVPL_CRVINPLANE if the line is in the plane
 see curveplaneintersection.cpp ll 23 et seq
 the intersection point is returned in p_results where
 x = parameter on the curve
 y,z parameters of the plane.
*/
    double t0,t1;
    if(p_d)	{
        t0 = p_d->Min();		t1 = p_d->Max();
    }
    else		{
        t0 = c->Domain().Min();	t1 = c->Domain().Max();
    }

    int i,offendcode =0;
    double ta,dt ;
    ON_3dPoint p1;
    ON_3dVector d1;
    double dt_Last=999999.;// init not needed but it shuts up gcc
    bool starting =true;

    // get a starting value for ta
    if(p_pseed)
        ta=*p_pseed;
    else {
        ON_3dPoint r11,r12;
        r11 = c->PointAt(t0); r12 = c->PointAt(t1);

        ON_Line L1 = ON_Line(r11, r12);

        if(ON_Intersect(L1,p_p,&dt)){
            ta = t0*(1-dt) + t1 * dt;
            offendcode= 1*(ta < t0 ) + 2*(ta > t1 );
            ta=min(ta,t1); ta=max(ta,t0);
        }
        else
            ta = (t0+t1)/2.0;
    }
    // now we have a starting value
    if(p_even) { // this is Method 2 in the notes
        double l_Clength;
        c->GetLength(&l_Clength);

        // iterate trying to improve ta.
        for(i=0;i<100;i++) {
            c->Ev1Der(ta,p1,d1);
            ON_Line L1 = ON_Line(p1, p1+d1);

            if(!ON_Intersect(L1,p_p,&dt)){
                //		cout<< "parallel\n"<<endl; verified PH may13 2005
                return RXONC_PARALLEL;
            }
            //one unit of dt is equal to the length of the first derivative.
            // now evaluate the line at dt1
            //ON_3dPoint l_PtOnLine= L1.PointAt (dt);

            // We might improve convergence by dropping a perpendicular to the Curve
            // THere's a problem with GetClosestPoint
            //	double l_dist, l_t = ta + dt;
            //	if( PCF_OK ==PC_Find_Perpendicular(*l_nc,t0,t1,l_PtOnLine,&l_t,&l_dist,p_SolveTol)){
            //			dt1 = l_t-ta;
            //	}

            // the value of parameter at the perp is the new ta.
            // so we set dt1 = (new - ta)
            // if getClosest doesnt converge, use dt1 from the line-plane intersection

            if(fabs(dt) <= p_SolveTol) { // found
                int GoingUp =2* ((ON_DotProduct(d1,p_p.Normal() ) ) > 0.0) -1;
                ta +=dt;
                p_Results.AppendNew() = ON_3dPoint(ta,(double)GoingUp,0.0);
                return 0;
            }
            if (!starting) {
                if(offendcode &&(fabs(dt) > fabs(dt_Last))) {  // we no longer do this
                    //	printf("info: ONC_Intersect(even) growing (i=%d) dom(%f,%f), ta=%f,dt=%f\n",i,t0,t1,ta,dt);
                    //return RXONC_ERR;  WHAT????????
                }
            }
            starting = false;
            dt_Last =  dt;

            ta+=dt;
            if(offendcode) {
                if(offendcode&1 &&dt<0)
                    return RXONC_OFFEND1;
                if(offendcode&2 &&dt>0)
                    return RXONC_OFFEND2;

            } // if offendcode
            if(ta < t0 || ta > t1 ){
                offendcode= 1*(ta < t0 ) + 2*(ta > t1 );
            }
            else
                offendcode=0;
        } // end for i
        // printf(" ONC_Intersect(even) NOT converged (i=%d)\n",i);
    }// if p_even
    else // method 1 in the notes
        // should be quadratic
        // repeatedly drop from line/plane intersect to the curve,
        // from the angle between the plane normal (beware we're not a planar problem)
        // calculate an addition to dt1.
        // this isn't stable, especially for kisses.
        cout<<"this case not handled in curve to plane intersection"<<endl;
    //assert(0);
    return RXONC_ERR;
} //IntersectByGeometry



//bool ON_Curve::Intersect(const ON_Plane & p_p,
//					ON_3dPointArray & p_results,  // SB 3d for u,v,t
//					double p_SolveTol,
//					const ON_Interval *p_d,// interval on p_p
//					double * p_pseed, // default NULL
//					int p_even ) const // default 1 (=even)

bool ONCurveIntersect(const ON_Curve *c,const ON_Plane & p_p, // for 
                      ON_3dPointArray & p_results,  // SB 3d for u,v,t
                      const double p_SolveTol,
                      const ON_Interval *p_d,// interval on p_p
                      double * p_pseed, // default NULL
                      int p_even )
{

    int err = ONCurveIntersectByGeometry(c, p_p, p_results, p_SolveTol,p_d,p_pseed, p_even );
    return (!err);
}
// Dec 2013  speed.  Dont get t0 ,t1, &c until we've passed the BB test
//                  Grow one BB twice as much not both

bool ONCurveIntersect(const ON_Curve *pc1,const  ON_Curve *pc2,
                      ON_2dPointArray &results,
                      const double p_Max_Sep,	//used twice. Once for growing BB once to test distance off
                      const double p_SolveTol,
                      const ON_Interval * p_d1,
                      const ON_Interval *p_d2)
{
    const double p_SolveTolsq = p_SolveTol*p_SolveTol;
    rxON_BoundingBox b1,b2;

    if(p_d1)	{
        rxON_NurbsCurve c1;  pc1->NurbsCurve(&c1);
        if(c1.Trim(*p_d1)) c1.Reset_Seglengths();
        c1.GetBoundingBox(b1);
    }
    else		{
        pc1->GetBoundingBox(b1);
    }
    if(p_d2)	{
        rxON_NurbsCurve c2 ;  pc2->NurbsCurve (&c2);
        if(c2.Trim(*p_d2)) c2.Reset_Seglengths () ;
        c2.GetBoundingBox(b2);// OK
    }
    else {
        pc2->GetBoundingBox(b2);
    }
    // end TR Oct 2005
    // in case of line GetTightBoundingBox() return a boundingboc with min--max
    // this case fails the following test
    // we perform a test on the length of the doagonal if the test fails we do GetBoundingBox instead of GetTight
    if(b1.Diagonal().LengthSquared()<p_SolveTolsq)
        b1 = pc1->BoundingBox();
    if(b2.Diagonal().LengthSquared()<p_SolveTolsq)
        b2 = pc2->BoundingBox();

    // end TR Oct 2005

    b2.GrowAllRound (p_Max_Sep);

    /////////////////////////////////////////////////////////
    if(! b1.Intersection(b2))
        return false;
    ////////////////////////////////////////////
    double t10,t11, t20,t21;

    if(p_d1)	{
        t10 = p_d1->Min();
        t11 = p_d1->Max();
    }
    else		{
        t10 = pc1->Domain().Min();
        t11 = pc1->Domain().Max();
    }
    if(p_d2)	{
        t20 = p_d2->Min();
        t21 = p_d2->Max();
    }
    else {
        t20 = pc2->Domain().Min();
        t21 = pc2->Domain().Max();
    }

    // there might be an intersection on interval (t10,t11) in pc1
    //  and interval (t20,t21) in pc2
    // If the two curves are fairly straight in these intervals it is safe to start Newton's method
    // to find a perpendicular between the two curves within these domains.
    // for the moment we chop into quarters,    A better criterion might be

    //2) recur each half of each curve - unless already chopped too far. TODO  a better criterion
    int OKDiv1 = ((( t11-t10 )> p_SolveTol) &&( t11-t10 ) >= (pc1->Domain().Max()-pc1->Domain().Min()) /2.0);
    int OKDiv2 = ( ( t21-t20 )>p_SolveTol && ( t21-t20 ) >= (pc2->Domain().Max()-pc2->Domain().Min()) /2.0);
    for(;;)
    {
        if(OKDiv1 && OKDiv2) { // all 4
            ON_Interval l_i11 = ON_Interval(t10,(t10+t11)/2.0);
            ON_Interval l_i21 = ON_Interval(t20,(t20+t21)/2.0);
            if(ONCurveIntersect(pc1,pc2,results,p_Max_Sep,p_SolveTol,	&l_i11, &l_i21)) return true;
            ON_Interval l_i22 = ON_Interval((t20+t21)/2.0,t21);
            if(ONCurveIntersect(pc1,pc2,results,p_Max_Sep,p_SolveTol,	&l_i11, &l_i22)) return true;

            ON_Interval l_i12 = ON_Interval((t10+t11)/2.0,t11);
            if(ONCurveIntersect(pc1,pc2,results,p_Max_Sep,p_SolveTol,	&l_i12, &l_i21)) return true;
            if(ONCurveIntersect(pc1,pc2,results,p_Max_Sep,p_SolveTol,	&l_i12, &l_i22)) return true;
            return false;
        }
        else if(OKDiv1) {
            ON_Interval l_i11 = ON_Interval(t10,(t10+t11)/2.0);
            ON_Interval l_i12 = ON_Interval((t10+t11)/2.0,t11);

            if(ONCurveIntersect(pc1,pc2,results,p_Max_Sep,p_SolveTol,	&l_i11, p_d2)) return true;
            if(ONCurveIntersect(pc1,pc2,results,p_Max_Sep,p_SolveTol,	&l_i12, p_d2))return true;
            return false;
        }
        else if(OKDiv2) {
            ON_Interval l_i21 = ON_Interval(t20,(t20+t21)/2.0);
            ON_Interval l_i22 = ON_Interval((t20+t21)/2.0,t21);
            if(ONCurveIntersect(pc1,pc2,results,p_Max_Sep,p_SolveTol,	p_d1, &l_i21)) return true;
            if(ONCurveIntersect(pc1,pc2,results,p_Max_Sep,p_SolveTol,	p_d1, &l_i22)) return true;
            return false;
        }
        else {

            //geometric test
            int rc = ONCurveIntersectByGeometry(pc1,pc2,results, p_Max_Sep,	// to test distance off
                                                p_SolveTol,
                                                ON_Interval(t10,t11),
                                                ON_Interval(t20,t21) );

            if(rc==-1) // needs subdivision
            {
                OKDiv1 =OKDiv2 =1;
                if((( t11-t10 )< p_SolveTol)||   (( t21-t20 )>p_SolveTol))
                    return false;// july 2014
            }
            else
                return (rc!=0);
        }

    } // for
    return false;
}

// ONCurveIntersectByGeometry returns
//  1 if an intersection found,
//  0 if no intersections found
// -1 if it thinks this interval needs subdividing

int ONCurveIntersectByGeometry(const ON_Curve *pc1, const  ON_Curve *pc2,
                               ON_2dPointArray &results,
                               const double p_Max_Sep,	//used twice. Once for growing BB once to test distance off
                               const double p_SolveTol,
                               const ON_Interval p_d1,
                               const ON_Interval p_d2)
{
    int rc=0;
    // we are here because there is a bounding-box intersection between  interval (t10,t11) in pc1
    //  and interval (t20,t21) in pc2  and we've decided (crudely as it happens) that its OK
    // to start a Newton search
    //  To find the seed, we intersect the chords
    // if Newton goes unstable we recur into the halves of each one
    // if Newton goes off the end we report a failure - no intersection between these subsegments.
    // if Newton doesnt converge we  recur into the halves of each one
    // if Newton converges, we push the found point onto Results
    // p_SolveTol is the minimum step - if both steps < that we have a result

    //
    double t10,t11,t20,t21;
    assert( p_d1.IsIncreasing());  assert( p_d2.IsIncreasing());

    t10 = p_d1.Min(); t11 = p_d1.Max();
    t20 = p_d2.Min(); t21 = p_d2.Max();

#ifndef NEVER
    double   ta = (t10+t11)/2.0;
    double   tb = (t20+t21)/2.0;
    double dta,dtb,dtaLast,dtbLast;
    //  seed by intersecting the chords
    int k;
    ON_3dPoint r11,r12,r21,r22;
    ON_3dPoint p1,p2;
    ON_3dVector d1,d2;
    ON_Line T1,T2,C1,C2;

    r11 = pc1->PointAt(t10); r12 = pc1->PointAt(t11);
    r21 = pc2->PointAt(t20); r22 = pc2->PointAt(t21);
    C1 = ON_Line(r11, r12);
    C2 = ON_Line(r21, r22);
    if(ON_IntersectLineLine(C1,C2,&dta,&dtb,0.,true)) // this variant treats the lines as finite
    {
        if(dta<-EPS )
            return 0;
        if(dtb<-EPS )
            return 0;
        if(dta>1.+EPS )
            return 0;
        if(dtb> 1.+EPS )
            return 0;
        ta = t10*(1.-dta) + t11 * dta;
        tb = t20*(1.-dtb) + t21 * dtb;
    }    else {
        ta = (t10+t11)/2.0; tb = (t20 + t21)/2.0;
    }
    int endcount=0;
    dtaLast = t11-t10; dtbLast = t21-t20;
    for(k=0;k<100;k++)
    {
        pc1->Ev1Der(ta,p1,d1);
        pc2->Ev1Der(tb,p2,d2);
        T1 = ON_Line(p1, p1+d1);
        T2 = ON_Line(p2, p2+d2);
        if(!ON_Intersect(T1,T2,&dta,&dtb)){   //		cout<< "parallel\n"<<endl;
            if  ( p1.DistanceTo(p2) > p_Max_Sep)
                return 0;
            results.AppendNew() = ON_2dPoint(ta,tb); // parallel - could be anywhere
            return 1;
        }
        if( fabs (dta)> fabs (dtaLast))
            return -1;
        if( fabs (dtb)>fabs (dtbLast))
            return -1;

        ta += dta; tb+=dtb;
        if(!p_d1.Includes(ta))
            return 0;
        if(!p_d2.Includes(tb))
            return 0;
        if(fabs (dta*d1.Length())< p_SolveTol && fabs (dtb*d2.Length())< p_SolveTol)
        {
            if(endcount) {
//                pc1->Ev1Der(ta,p1,d1);
//                pc2->Ev1Der(tb,p2,d2);
//                qDebug()<< "ONIntersection";
//                qDebug( "  p1 = {%19.15f , %19.15f , %19.15f }; t1 = {%19.15f , %19.15f , %19.15f} ",p1.x,p1.y,p1.z ,d1.x,d1.y,d1.z  );
//                qDebug( "  p2 = {%19.15f , %19.15f , %19.15f }; t2 = {%19.15f,  %19.15f , %19.15f } ",p2.x,p2.y,p2.z ,d2.x,d2.y,d2.z  );
                results.AppendNew() = ON_2dPoint(ta,tb);
                return 1;
            }
            endcount++;
        }

    }// Newton loop
    return -1;

#else
    int i,offendcode =0;
    double ta,tb,dt1,dt2,dist, factor = 1.0, t1trace[100],t2trace[100]; // l_tLim,
    ON_3dPoint p1,p2;
    ON_3dVector d1,d2;
    double dt1_last=999999.,dt2_last=9999999.;// init not needed but it shuts up gcc
    bool starting =true;

    //second  guess start with the intersection of the chords.

    ON_3dPoint r11,r12,r21,r22;
    r11 = pc1->PointAt(t10); r12 = pc1->PointAt(t11);
    r21 = pc2->PointAt(t20); r22 = pc2->PointAt(t21);
    ON_Line L1 = ON_Line(r11, r12);
    ON_Line L2 = ON_Line(r21, r22);
    // is there an intersection between the (finite) chords regardless of separation distance?
    if(ON_IntersectLineLine(L1,L2,&dt1,&dt2,0.,true)) // this variant treats the lines as finite
    {
        ta = t10*(1-dt1) + t11 * dt1;
        //  ta=min(ta,t1); ta=max(ta,t0);
        tb = t20*(1-dt2) + t21 * dt2;
        //  tb=min(tc1,tb) ; tb = max(tb,tc0);
        offendcode= 1*(ta < t10 ) + 2*(ta > t11 ) + 4* (tb < t20) + 8*(tb > t21);
    }
    else { // no intersection so seed the NR search at the midpoints.
        ta = (t10+t11)/2.0; tb = (t20 + t21)/2.0;
    }

    for(i=0;i<100;i++) {
        pc1->Ev1Der(ta,p1,d1);
        pc2->Ev1Der(tb,p2,d2);
        ON_Line L1 = ON_Line(p1, p1+d1);
        ON_Line L2 = ON_Line(p2, p2+d2);
        if(!ON_Intersect(L1,L2,&dt1,&dt2)){ // doesnt give good convergence
            //		cout<< "parallel\n"<<endl; verified PH may13 2005
            return 0;
        }
        //one unit of dt is equal to the length of the first derivative.  This is correct, but
        // we still get slow convergence

        //	printf(" trial intersect in %d at p1=(%f %f %f) p2=(%f,%f,%f)\n",i, p1.x,p1.y,p1.z,p2.x,p2.y,p2.z);
        if(fabs(dt1) <= p_SolveTol && fabs(dt2) <= p_SolveTol) { // found
            // this looks wrong.  we don't test whether the result of the line intersection is within the domain.
            // also we should evaluate at ta+dt1 and tb+dt2 and get the distance (added this 1 july 2005)
            ta +=dt1; tb+=dt2;
            pc1->Ev1Der(ta,p1,d1);
            pc2->Ev1Der(tb,p2,d2);
            dist = p1.DistanceTo (p2);

            if(dist > p_Max_Sep) {	//	cout<< " but far apart"<<endl;
                results.AppendNew() = ON_2dPoint(ta,tb);  // rc=false and results count tells us there was a distant intersection
                return 0;
            }
            if(ta < t10 || ta > t11 || tb < t20 || tb > t21){	//	cout<< " but outside range\n"<<endl;
                return 0;
            }
            results.AppendNew() = ON_2dPoint(ta,tb);
            return 1;
        }
        factor=1;

        if (!starting) {
            if((offendcode&1 ||offendcode&2) &&(fabs(dt1) > fabs(dt1_last))) {
                //	cout<< " growing dt1 ";
                break; }
            if((offendcode&4 || offendcode&8)  && (fabs(dt2) > fabs(dt2_last))) {
                //	cout<< " growing dt2 ";
                break; }
        }
        starting = false;
        dt1_last =  dt1;
        dt2_last =  dt2;

        ta+=dt1*factor;			tb +=dt2*factor;
        if(offendcode) {
            if(offendcode&1 &&dt1<0)
                return 0;
            if(offendcode&2 &&dt1>0)
                return 0;
            if(offendcode&4 &&dt2<0)
                return 0;
            if(offendcode&8 &&dt2>0)
                return 0;
            //	factor = max(factor/2.0,0.5);
        }
        if(ta < t10 || ta > t11 || tb < t20 || tb > t21){
            //	cout<< " outside range... "<<endl;
            offendcode= 1*(ta < t10 ) + 2*(ta > t11 ) + 4* (tb < t20) + 8*(tb > t21);
        }
        else
            offendcode=0;
        t1trace[i] =ta; t2trace[i]=tb;
    } // end for i

    return rc;
    printf("ON_Curve::Intersect not converged \t %d \n",i);
    printf("t0,t1,tc0,tc1,factor \t%f \t%f \t%f \t%f \t%f\n",t10,t11,t20,t21,factor);
    p1 = pc1->PointAt(t10); 	  p2 = pc1->PointAt(t11);
    printf("segmentOfC1From\t %f  \t %f  \t %f  \t \t %f  \t %f  \t %f\n",p1.x,p1.y,p1.z,p2.x,p2.y,p2.z);
    p1 = pc2->PointAt(t20);  p2 = pc2->PointAt(t21);
    printf("segmentOfC2From\t %f  \t %f  \t %f  \t \t %f  \t %f  \t %f\n",p1.x,p1.y,p1.z,p2.x,p2.y,p2.z);
    int j;
    for(j=0;j<i-1;j++)
        printf("%d\t%f\t%f\n", j, t1trace[j],t2trace[j]);
    printf("now\t%f\t%f\n",	ta+dt1*factor,tb +dt2*factor);
    return 0;
#endif
}


ON_BOOL32 rxON_NurbsCurve::GetLength(
        double* length,
        double fractional_tolerance,
        const ON_Interval* sub_domain
        ) const
{

    if ( !length )
        return false;
    // if p_Curve is a Polyline do it that way.


    ON_SimpleArray<ON_3dPoint> l_pline_points;
    ON_SimpleArray<double> l_pline_t;
    if(sub_domain && sub_domain->Length () < fractional_tolerance) {
        *length =0.0;
        return true;
    }
    if (IsPolyline(&l_pline_points,&l_pline_t))
    {
        //double dum ;
        ON_BOOL32 ret;
        ON_PolylineCurve l_poly;
        l_poly.m_pline = l_pline_points;
        l_poly.m_t     = l_pline_t;
        ret = l_poly.GetLength(length,fractional_tolerance,sub_domain);
        if(*length < fractional_tolerance) {
            ON_Interval scratch_sub_domain;
            if(sub_domain ){
                double s0 = sub_domain->Min();
                double s1 =  sub_domain->Max() - 1e-8; // this doesnt work either
                /*dum =*/ sub_domain->Length () ;
                scratch_sub_domain.Set(s0,s1);

                ON_TextLog l_dump_tostdout;
                l_dump_tostdout.Print(" Short NurbsCurve which IsPolyline.\n");
                l_dump_tostdout.Print (" now print the nurbscurve\n");
                Dump(l_dump_tostdout );
                l_dump_tostdout.Print (" now print the polyline\n");
                l_poly.Dump(l_dump_tostdout );
                l_dump_tostdout.Print ("that was the polyline\n\n");

                l_dump_tostdout.~ON_TextLog();
                ret = l_poly.GetLength(length,fractional_tolerance,&scratch_sub_domain);
            }
            //			MakeClampedUniformKnotVector();
            //			ret = GetLength(length,fractional_tolerance,sub_domain);


            //   ON_ASSERT(0);
        } //end fixup for short
        return ret;
    }

    double t1, t2;
    if (sub_domain)
    {
        if(sub_domain->Length () < fractional_tolerance * Domain().Length ()) {
            *length = 0.0;
            return true;
        }
        ON_ASSERT(sub_domain->IsIncreasing());
        if(! sub_domain->IsIncreasing()) {
            cout<< "subdoMain  decreasing"<<endl;
        }
        //To make the subdomain compatible with the curve
#ifndef NEVERDEFINED_DEBUG
        t1 = ON_Max(Domain().Min(),sub_domain->Min());
        t2 = ON_Min(Domain().Max(),sub_domain->Max());
#endif
    }
    else {
        t1 = Domain().Min();
        t2 = Domain().Max();
    }
    if(!this->m_segLengths ) {
        cout<<" cant getlength without segs"<<endl; return false;}
    //	*length = PC_LocalGetLength(*this, t1, t2, fractional_tolerance);
    //	return true;
    //TODO  figure out when subdomain is decreasing
    //TODO  getlocal
    // lets use m_Seglengths
    int i, k1=0, k2=0;
    *length =0.0;
    for( i=0;i<KnotCount(); i++) {
        if( t1 >= Knot(i)) { //increasing so the first one
            k1 = i; break;
        }
    }
    for( i=KnotCount()-1;i>0; i--) {
        if( Knot(i) <=t2) {
            k2 = i; break;
        }
    }
    // now k1 is the knot just under t1 and k2 is the knot just under t2

    *length += PC_LocalGetLength(*this, t1,Knot(k1+1), fractional_tolerance);
    for (i = k1+1; i < k2; i++) {
        if(m_segLengths[i] < 0.0)
            m_segLengths[i] = PC_LocalGetLength(*this,Knot(i),Knot(i+1), fractional_tolerance);
        *length += m_segLengths[i];
    }

    *length += PC_LocalGetLength(*this, Knot(k2), t2, fractional_tolerance);
    return true;


}//BOOL ON_NurbsCurve::GetLength Peters method.
//static 
double rxON_NurbsCurve::PC_LocalGetLength(const ON_NurbsCurve& p_c,double t1, double t2, double tol) {
    ON_3dPoint p1,p2;
    ON_3dVector d1,d2,chord,dummy; // c1,c2
    double l_len =0.0, l_err,b0,b1,b02,b03,b04,b12, ChordLength;
    if(fabs(t2 - t1) < tol)
        return 0.0;
    //	p_c.Ev1Der((t1*0.75 + t2*0.25),p1,d1,1);
    //	p_c.Ev1Der((t1*0.25 + t2*0.75),p2,d2,-1);
    //	d1.Unitize();
    //	d2.Unitize();

    p1 = p_c.PointAt(t1);
    p2 = p_c.PointAt(t2);
    d1 = p_c.TangentAt ( (t1*0.75 + t2*0.25) ); // BEWARE this is the unit tangent.
    d2 = p_c.TangentAt ( (t1*0.25 + t2*0.75)  );
    chord = p2 - p1;
    ChordLength = chord.Length();	//	dom = approx chordlength;
    chord.Unitize();

    dummy = ON_CrossProduct(chord,d1);  // cross-product loses the sign.
    b0 = dummy.Length() *2.0; // strictly asin. //	b0 is true angle from t1 to chord

    dummy = ON_CrossProduct(chord,d2);  //WRONG  we need to do this in d1 coords
    b1 = dummy.Length()*2.0 ;//		b1 ditto from t2 to chord.

    b02 = pow(b0,2); b04 =  pow(b02,2); b03=b02*b0;
    b12 = pow(b1,2);// b14 =  pow(b12,2); b13=b12*b1;

    // estimate the relative error in getting this length.

    l_err = fabs((11.0*(b02) + 3.0*(b04) - 11.0*b0*b1 - 3.0*b03*b1 + 2.0*b12))/(3.0*pow(1.0 + b02,1.5));

    if(l_err > tol && ((t2-t1 > 0.001)  )) { /// horrible heuristic
        l_len = PC_LocalGetLength( p_c, t1, (t1+t2)/2.0, tol)
                +	PC_LocalGetLength( p_c,(t1+t2)/2.0,t2, tol);
        return l_len;
    }
    // now we need to get dom on full segment
    // but we use the quarter-point slopes to estimate the bowing correction
    //		p_c.EvPoint(t1, p1,1);
    //		p_c.EvPoint(t2,p2,-1);
    //		dom = p1.DistanceTo(p2);

    l_len = ((1.0 - (b02) + b0*b1)*ChordLength)/sqrt(1.0 + b02);
    return(l_len);
}
int rxON_NurbsCurve::PC_Find_Perpendicular( double t0, double t1,
                                            const ON_3dPoint& q,
                                            double *tperp, double *p_dist, double p_tol) const{

    //returns PCF_OK if solves within {t0,t1}, regardless of the dist
    // with the parameter in *tperp and the dist in *dist.
    /* otherwise
PCF_UNCONVERGED 
PCF_OFF_START
PCF_OFF_END	
PCF_UNCONVERGED	
PCF_OK
*/

    /*The cheap result is
 evaluate at t to give  {x,y,z} and 1st deriv {xd,yd,zd}
dt = -((-(q->x*d.x) + p.x*d.x - q->y*d.y + p.y*d.y - q->z*d.z + p.z*d.z)/
     (pow(d.x,2) + pow(d.y,2) + pow(d.z,2)))
*/	
    // basic method.  We loop incrementing t. At each loop we may have the following conditions:
    //	1) the error estimate shows that we have to scale back the proposed dt.
    //  2) the proposed dt takes us off the end. (by more than p_tol)
    //		In this case we test at the end to see if the sense is outwards or inwards.
    //	3) a math error - divide by zero for example.
    // 4) none of the above.  It's safe to loop.

    const rxON_NurbsCurve& p_c =*this;

    ON_3dPoint p;
    ON_3dVector d;
    int i,hint=0,side=0;
    double dtl, t = *tperp;
    double l_Factor=1.0;
    double dt_last=0 ,dt = 2*p_c.Domain().Length();
    int SlowButSafe=0;
    int l_SpanIndex,l_trimmed=0,imax,l_Degree;
    int l_bigplus=0, l_bigminus=0, l_growing=0, l_jump=0, l_offend=0, l_offstart=0;
    int l_trimup =0,l_trimdown=0;
    ON_Interval l_Span_Domain;
    imax = p_c.KnotCount() *2;
    l_Degree=p_c.Degree ();
    l_Degree = min(2,l_Degree); // because the cubic solver has numerical difficulties
    for(i=0;i<imax;i++) {
        l_trimmed=0;
        dt_last =dt;
#ifdef _RX_FIRST_ORDER
        p_c.Ev1Der(t,p,d);// this first-order method is slow and unstable.
        dt = -((-(q.x*d.x) + p.x*d.x - q.y*d.y + p.y*d.y - q.z*d.z + p.z*d.z)/
               (pow(d.x,2) + pow(d.y,2) + pow(d.z,2))); // this is really slow
#else
        dt = l_Factor * SecondOrderDt(p_c,t,q,l_Degree, side, &hint); // actually multiple degree now
#endif
        // error estimate and trim HERE

        //  slow down a big dt.
        if(dt > (t1-t0)/2.0) {
            dt = (t1-t0)/2.0  ;
            l_Factor = l_Factor *0.95;
            l_bigplus++;
        }
        else if(dt < (t0-t1)/2.0) {
            dt = (t0-t1)/2.0  ;
            l_Factor = l_Factor *0.95;
            l_bigminus++;
        }
        if(fabs(dt) >= fabs(dt_last)) {
            SlowButSafe++;
            l_Factor *=0.95;
            l_growing++;
        }
        else
            l_Factor=min(l_Factor/0.95,1.0);
        //	l_Factor = max(l_Factor,0.375);

        if(fabs(dt+dt_last) < 0.125*fabs(dt-dt_last)) { // jumping
            //	printf("jump %f %f to ",dt_last,dt);
            dt = (dt+dt_last)/2.0;
            //		printf(" %f\n",dt);
            l_jump++;

        }

        // see if we are off the end
        //		we get different results here in MSW debug and release versions WIERD
        if(t+dt > (t1+p_tol)) { // off end
            p_c.Ev1Der(t1,p,d,-1);
            dtl = -((-(q.x*d.x) + p.x*d.x - q.y*d.y + p.y*d.y - q.z*d.z + p.z*d.z)/
                    d.LengthSquared());
            l_offend++;
            if(dtl > 0.0) {
                *tperp = t1;
                *p_dist = q.DistanceTo(p);
                return PCF_OFF_END	;
            }
        }
        else if (t + dt < (t0-p_tol)) { // off start
            p_c.Ev1Der(t0,p,d,1);
            dtl = -((-(q.x*d.x) + p.x*d.x - q.y*d.y + p.y*d.y - q.z*d.z + p.z*d.z)/
                    (pow(d.x,2) + pow(d.y,2) + pow(d.z,2)));
            l_offstart++;
            if(dtl < 0.0) { // peter 23 Feb 2005
                *tperp = t0;
                *p_dist = q.DistanceTo(p);
                return PCF_OFF_START;
            }
        }
        else {
            if(SlowButSafe) { //only allow movement by one span index.
                p_c.GetSpanVectorIndex(t+dt/1e8,side,&l_SpanIndex,&l_Span_Domain);
                if(dt > l_Span_Domain.Max() -t){
                    dt =  ( l_Span_Domain.Max() -t ) ; side=1; l_trimmed=1;
                    if(l_Degree > 1) l_Degree--;
                    l_trimup++;
                }
                else if(dt < l_Span_Domain.Min()-t  ) {
                    dt =  ( l_Span_Domain.Min()-t); side=-1; l_trimmed=1;
                    if(l_Degree > 1) l_Degree--;
                    l_trimdown++;
                }
            }
        }
        // we get here if we are NOT off the end.
        // Jan 31 2007 do a last linear corection because
        t = t + dt;
        if( !l_trimmed && fabs(dt/l_Factor) < p_tol) {
            p_c.Ev1Der(t,p,d);
            ON_3dVector pq = q-p;
            dt = ON_DotProduct(pq,d) / d.LengthSquared();
            t += dt;
            p_c.Ev1Der(t,p,d);

            *tperp=t;
            *p_dist = q.DistanceTo(p);
            return  PCF_OK ;
            /* WAS		t = t + dt;
  if( !l_trimmed && fabs(dt/l_Factor) < p_tol) {
   p_c.Ev1Der(t,p,d);
   *tperp=t;
   *p_dist = q.DistanceTo(p);
   return  PCF_OK ; */
        }
    } // end for

    printf("(PC_Find_P) not connnverged on pt (%f,%f,%f)\tdt=%f (%f) fac= %f trmd=%d deg=%d \ti=%d\t \n",q.x,q.y,q.z,
           dt,dt_last,l_Factor, l_trimmed, l_Degree,i);
    //printf("jump, bigplus,bigminus,growing offend offstart trimup,trimdown %d %d %d %d %d %d %d %d\n",
    //	l_jump,l_bigplus,l_bigminus,l_growing,l_offend,l_offstart,l_trimup,l_trimdown);


    return PCF_UNCONVERGED;
} //BOOL PC_Find_Perpendicular

#ifndef _AMG2005 // for the part viewer




int ONPC_SolveCubicEquation(
        double q, double r, double s, double t,
        double* p_r1, double* p_r2, double* p_r3
        )
{
    int rc = 0;

    *p_r1 = 0; *p_r2=0; *p_r3=0;
    if( 0 && fabs(q) < 1e-12)     {    // quadratic equation
        double rr0, rr1;
        int qrc = ON_SolveQuadraticEquation( r, s, t, &rr0, &rr1 );
        switch ( qrc )
        {
        case 0:
            // two distinct real roots (rr0 < rr1)
            *p_r1 = rr0;
            *p_r2 = rr1;
            rc = 3;
            break;
        case 1:
            // one real root (rr0 = rr1)
            *p_r1 = rr0;
            *p_r2 = rr1;
            rc = 3;
            break;
        case 2:
            // 2: two complex conjugate roots (rr0 +/- (rr1)*sqrt(-1))
            *p_r1 = rr0;
            *p_r2 = rr1;
            rc = 4;
            break;
        }
        return rc;
    } // end zero q = quadratic

    rc = PF_solvecubic(&q,&r,&s,&t,p_r1,p_r2,p_r3); // fortran. Does quadratic too
    // the roots
    // root 1
    return rc;
}

/*
Description:
  Solve the cubic equation q*X^3 + r*X^2 + s*X + t = 0. // BUT IT'S WRONG
Returns:
  0: failure (q = 0, r = 0, s = 0 case)
  1: three real roots (r1 <= r2 <= r3)
  2: one real root (r1) and two complex conjugate roots (r2 +/- (r3)*sqrt(-1))
  3: two real roots (r1 <= r2) (q = 0, r != 0 case)
  4: two complex conjugate roots (r1 +/- (r2)*sqrt(-1)) (q = 0, r != 0 case)
  5: one real root (r1) (q = 0, r = 0, s != 0 case)
*/

int ONPC_Direct_SolveCubicEquation( // based on NR page 191
                                    double qin, double rin, double sin, double tin,
                                    double* r1, double* r2, double* r3
                                    )
{
    int rc=0;
    cout<<"ONPC_Direct_SolveCubicEquation  "<<endl; assert(0);
#ifdef NEVER
    double a,b,c,Q,R,theta, x,y,rQ;//,x1,x2,x3;q,r,s,t,
    if(qin==0) return 0;

    //	q = 1.0;
    //	a = r = rin/qin;
    //	b = s = sin/qin;
    c = t = tin/qin;

    Q = (a*a - 3.f*b)/9.f; // Q may be complex
    R = ( a* (2.f * a*a  - 9.f *b)  + 27.f*c)/54.f;
    assert(Q>0); // not coded the negative case.
    if(R*R < Q*Q*Q) {
        y = R/pow(Q,(2./3.));
        if(y>=1.0 )
            y=1.0;
        if(y<=-1.0 )
            y=-1.0;
        theta = acos(y);

        rQ=sqrt(Q);
        //x1 = -2. *rQ *cos(theta/3.f) - a/3.f;
        //x2 = -2. *rQ *cos((theta+2.f*ON_PI)/3.f) - a/3.f;
        //x3 = -2. *rQ *cos((theta-2.f*ON_PI)/3.f) - a/3.f;
        //now sort into r1,r2,r3 in ascending order.
    }
    else {
        cout<<" Page 191!!!!!!!!!"<<endl; assert(" Page 191"==0);
    }
#endif
    return rc;
}
#endif //#ifndef _AMG2005
