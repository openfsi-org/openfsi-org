#ifndef  _TPN_H_
#define _TPN_H_
	/* tpn.h 22/2/94 */

EXTERN_C int     Read_TPN(SAIL *sail,char *s);
EXTERN_C int      Mark_Edge_Curves(SAIL *sail);

EXTERN_C int Check_Panel_Sense(SAIL *sail) ;

#endif  //#ifndef  _TPN_H_
