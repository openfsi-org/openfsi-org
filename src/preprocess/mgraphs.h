#ifndef RX_MGRAPHS_H
#define RX_MGRAPHS_H

struct Multigraph_Axes_Structure {
	int ng;
	char *labels[10];
	int mpat_indx[10];
	int col_indx[10];
};
typedef struct Multigraph_Axes_Structure Multi_GR;


int Multi_Graph(int ng, float**data, int nrs,int ncs, float*u,float*v, const char**labels,float upper,float lower,Multi_GR *axis);



#endif //#ifdef RX_MGRAPHS_H
