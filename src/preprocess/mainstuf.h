#ifndef MAINSTUF_H
#define MAINSTUF_H

extern const char *RELAX_VERSION;
EXTERN_C  void  rlx_siig_hand(int oursigal);
#ifdef _X
EXTERN_C  Widget create_main_shell(Graphic *g,Display *arg_display,char *app_name,int app_argc,char **app_argv);
#endif
EXTERN_C void relaxinit_1();
#ifndef RXQT
EXTERN_C  void relaxinit_2( int argc,char **argv, class QObject* TheWorld);
#endif
#endif // MAINSTUF_H
