/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 *     17/12/95 Defined_Length flag in SC printed
 *    5/12/95	Gcurve
 *     6/4/95  typing rxerror in print of gauss entity
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


#include "StdAfx.h"
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "griddefs.h" // horrible order independence.
#include "RX_FETri3.h"
#include "RXSRelSite.h"
#include "RX_FEPocket.h"
#include "RX_PCFile.h"
#include "RXPside.h"
#include "RXOffset.h"
#include "RXSeamcurve.h"
#include "griddefs.h"
#include "RXCurve.h"

#include "rxsubwindowhelper.h"
#include "hoopcall.h"

#include "akmutil.h"
#include "polyline.h"
#include "text.h"
#include "entities.h"

#include "panel.h"
#include "pansin.h"

#include "camera.h"
#include "batpatch.h"

#include "batstiff.h"

#include "aeromik.h"

#include "alias.h"
#include "fielddef.h"
#include "files.h"
#include "interpln.h"
#include "etypes.h"
#include "gcurve.h"

#include "arclngth.h"
#include "targets.h"
#ifdef USE_PANSAIL
#include "velocity.h"
#endif
#include "nurbs.h"
#include "iges.h"
#include "geodesic.h"
#include "pressure.h"
#include "gle.h"

#include "paneldata.h"
#include "offset.h"  // for tristressstruct

#include "f90_to_c.h"
#include "filament.h"
#include "drawing.h"

#include "global_declarations.h"
#include "RXPanel.h"

#include "RXdovefile.h"
#include "script.h"
#include "RX_UI_types.h"
#include "RXContactSurface.h"
#include "RX_FEString.h"

#include "printall.h"

#ifdef WIN32
#define _max max
#define _min min
#else
#define max(x,y)  ((x) > (y) ? (x) : (y))
#endif

int Print_Table(FILE *fp, struct PC_TABLE *t) {
    int j,k;
    for(j=0;j<t->nr;j++) {
        for(k=0;k<t->nc;k++) {
            fprintf(fp," %s\t", t->data[j][k]);
        }
        fprintf(fp,"\n");
    }

    return(1);
}


int Print_End(RXEntity_p ep,const char *text,FILE *fp) {

    if (!ep){
        fprintf(fp,"   %s ptr to NULL\n",text);
        return(0);
    }
    fprintf(fp,"    %s  %10s  %s\n",text,ep->type(),ep->name());

    return(0);
}
int Print_BroadSeam(sc_ptr sc, FILE *fp){
    /* To produce a broadseam listing.  Finc the distance between C0 and C2 at 1/8,2/8.. points
  and write */
    int k;
    double s,b;
    ON_3dPoint p1,p2;
    ON_3dPoint p;
    if(!sc->m_pC[0]||   !sc->m_pC[0]->IsValid()) {
        fprintf(fp,"Cant show Broadseam\t %s \n",sc->name()); return 0;
    }
    fprintf(fp,"Broadseam\t %s \t%s",sc->name(), sc->End1site->name());
    p=sc->Get_End_Position(SC_END);
    //end.x=p.x; end.y=p.y; end.z=p.z;
    for (k=1;k<8;k++) {
        s = sc->GetArc(0) *(double)k/8.0;
        Get_Position_By_Double(sc,s,0,p1);
        Get_Position_By_Double(sc,s,2,p2);
        b = sqrt((p1.x-p2.x)*(p1.x-p2.x) +  (p1.y-p2.y)*(p1.y-p2.y) +  (p1.z-p2.z)*(p1.z-p2.z)) ;

        fprintf(fp,"\t %6.1f ",b*1000.0);
    }
    fprintf(fp," \t%s\t %8.3f\n",sc->End2site->name(), sc->GetArc(0));

    return 1;
}


int Print_All_Entities(	SAIL *p_sail, const char  *stringin) {
    int FOUND;
    double  TotalGaussArea=0.0;
    static int level = 0;
    char buf[256];
    FILE *fpp=NULL;
    if(level>0) {
        return 0;
    }
    level ++;
    TotalGaussArea=0.0;
    if(!fpp) {

#ifdef WIN32
        strcpy(buf,"entities.out");
#else
        sprintf(buf,"%s/entities.out",traceDir);
#endif
        fpp = RXFOPEN(buf,"w");
        if(!fpp) {
            p_sail->OutputToClient("Can't open entity output file ",2);
            level--;
            return(-1);
        }
        cout<<"printing to "<<buf<<endl;
        FOUND=1;
    }
    else{
        cout<<"APPENDING  to "<<buf<<endl;
        FOUND = 0;

    }

    if(FOUND) {
        fprintf(fpp,"*******************************************************\n");
        fprintf(fpp,"*                                                     *\n");
        fprintf(fpp,"*          ENTITY DUMP FOR SAIL '%s'              	*\n",p_sail->GetType().c_str());
        fprintf(fpp,"*       at %s                                   *\n",stringin);
        fprintf(fpp,"*                                                     *\n");
        fprintf(fpp,"*******************************************************\n");
        fprintf(fpp," SLI is %d\n",p_sail->SailListIndex () );
    }
    else {
        fprintf(fpp,"*******************************************************\n");
        fprintf(fpp,"*                                                     *\n");
        fprintf(fpp,"*                UNKNOWN SAIL DUMP !!!                *\n");
        fprintf(fpp,"*       at %s                                   *\n",stringin);
        fprintf(fpp,"*                                                     *\n");
        fprintf(fpp,"*******************************************************\n");
    }
#ifdef HOOPS
    {
        char uo[2048];
        Graphic *g = p_sail->GetGraphic() ;
        HC_Open_Segment_By_Key(g->m_ModelSeg);
        HC_Open_Segment("data");
        HC_Define_System_Options("C string length=2040");
        HC_QShow_User_Options("/",uo);
        fprintf(fpp,"WorldRoot UOs\n%s\n\n",uo);

        HC_Show_Pathname_Expansion(".", uo);
        fprintf(fpp,"Sail Root seg is \n%s\n",uo);
        if(HC_QShow_Existence(".","user options")){
            HC_Show_User_Options(uo);
            fprintf(fpp,"SAIL UOs\n%s\n\n",uo);
        }
        HC_Define_System_Options("C string length=254");
    }
#endif
    p_sail->PrintModelSummary(fpp,0);

    ent_citer it;
    for (it = p_sail->MapStart(); it != p_sail->MapEnd(); ++it){
        RXEntity_p p  = dynamic_cast<RXEntity_p>( it->second);
        if(p->TYPE!=PCE_DELETED){
            p->Print_One_Entity( fpp);
        }
        else
            cout<< "whats a deleted entity doing here??"<<endl;
    }
    fprintf(fpp,"END of ENTITY PRINT. Nitems = %d\n",(int)p_sail->MapSize() );
    printf("END of ENTITY PRINT to %s\n",buf);
    fprintf(fpp," TotalGaussArea = %f\n",TotalGaussArea);

    FCLOSE(fpp);

#ifdef HOOPS
    HC_Close_Segment();
    HC_Close_Segment();
    HC_Define_System_Options("no C string length"); // MVO seems to need this.
#endif
    level --;
    return(1);
}


int write_D_by_triangle(SAIL *sail,const char*file){


    FILE *fp = RXFOPEN(file,"w");
    if(!fp) return 0;

    ent_citer it;
    for (it = sail->MapStart(); it != sail->MapEnd(); ++it) {
        RXEntity_p e  = dynamic_cast<RXEntity_p>( it->second);
        if(e->TYPE != PANEL) continue;
        Panelptr p = (Panelptr ) e;
        p->Print_Panel_DMatrices(fp);
    }

    FCLOSE(fp);
    return 1;
}
int write_stress_by_triangle(SAIL *sail,const char*file){

    // lists the triangles by panel.


    FILE *fp = RXFOPEN(file,"w");
    if(!fp) return 0;
    ent_citer it;
    for (it = sail->MapStart(); it != sail->MapEnd(); ++it) {
        RXEntity_p e  = dynamic_cast<RXEntity_p>( it->second);
        if(e->TYPE != PANEL) continue;
        Panelptr p =  (Panelptr)e;
        p->Print_Panel_Triangles(fp);
    }

    FCLOSE(fp);
    return 1;
}

