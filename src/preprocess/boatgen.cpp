/* boatgen.c a conbination of do_boat.c and boatmenu.
do_boat contains the top copy of the generation code

boatmenu.c has 
Create_Boat_Script(char*names[],char *data[],char*fileroot)
which opens the file, writes the script and the sets
then
      FCLOSE(f);
  return(1);
*/
#include "StdAfx.h"
#include "RXSail.h"
#include "RXQuantity.h"
#include "RelaxWorld.h"

//#ifdef WIN32
//#include "dummynt.h"
//#endif
#include "vectors.h"
#include "griddefs.h"

#include "entities.h"

#include "files.h"

#include "stringutils.h"

#include "words.h"
#include "arclngth.h"
#include "do_boat.h"
#include "global_declarations.h"

#include "boatgen.h"



int PBS_PrintBoatGenVariables(FILE *fp, struct BOATGEN *bg){
    fprintf(fp,"! I,J,EM,P,Rake_Ipt,BAD,Mast_Freeboard %f %f %f %f %f %f %f\n", bg->I,bg->J,bg->EM,bg->P,bg->Rake_Ipt,bg->BAD,bg->Mast_Freeboard);
    fprintf(fp,"! Mast_Size_Shear,Mast_Size_Ipt,Overlap,Sheeting_Base  %f %f %f %f\n", bg->Mast_Size_Shear,bg->Mast_Size_Ipt,bg->Overlap,bg->Sheeting_Base);
    fprintf(fp," ! Bow_Freeboard, HLength  %f  %f\n", bg->Bow_Freeboard, bg->HLength    );
    fprintf(fp," ! BG,BH, profile_length,profile_width  %f %f %f %f\n", bg->BG,bg->BH, bg->profile_length,bg->profile_width   );

    fprintf(fp," !  Track,Clew_Height,Main_Clew_Height,Tube_Length %f %f %f %f\n",bg->Track,bg->Clew_Height,bg->Main_Clew_Height,bg->Tube_Length  );
    fprintf(fp," ! LOA,Beam,x,y,z,EA,MClew_X %f %f %f %f\n",bg->LOA,bg->Beam,bg->EA,bg->MClew_X   );
    fprintf(fp," ! Port_Tack, Custom_Jibsheet  %d %d\n", bg->Port_Tack, bg->Custom_Jibsheet  );

    return 1;
}


double localEvaluate_Offset(const char*offin,double refl) {

    /*    offin  is in form "3.2%" . The trailing string can be one of
 % m mm ft in cm
 */
    const char *units[6] = {"%","cm" , "in" , "ft" ,"mm" ,"m"};
    double x,f[6]     = { 0.01,0.01 , 0.0254,0.3048, 0.001,1.0 };

    float xin =0.0;
    int k,n;
    char s[32];

    strcpy(s," ");
    n=sscanf(offin,"%f%s",&xin,s);
    x=xin;
    if (n==0) {
        printf("(localEvaluate_Offset) bad offset&%s",offin);
        return((float)0.0);
    }
    else if(n==1)  return(x);
    for (k=0;k<6;k++) {
        if (strieq(s,units[k])) {
            x = x * f[k];
            if (k==0)
                x=x*refl;
            return(x);
        }
    }
    return(x); /* default m */
}



int Process_Boat_Card(SAIL*sail,char *line, struct BOATGEN *bg)
{

    FILE *f;

    char *lp=NULL;
    char **data;

    char tfile[256];
    int depth;
    char buf[256];

    char *lp2;
    int i=0;
    char lcopy[2048];

    bg->Port_Tack=0,bg->Custom_Jibsheet=0;
    lp = line;
    lp2 = lcopy;

    while(*lp) {
        *lp2 = *lp;
        if(*lp == ':') {
            lp2++;
            *lp2 = ' ';
        }

        lp2++;
        lp++;
    }
    *lp2 = '\0';
    i = make_into_words(lcopy,&data,":");
    printf("found %d words\n",i);

    if(i < 54) {
        sail->OutputToClient("Not enough words in do_boat card\n 54 are needed",2);
        RXFREE(data);
        return(0);
    }
#ifndef WIN32
    strcpy(tfile,"Rlx_XXXXXX");
    mkstemp(tfile);
#else
    sail->OutputToClient(" new boat file newboat.in",2);
    strcpy(tfile,"newboat.in");
#endif

    f = RXFOPEN(tfile,"w");
    printf("opened boat file as %s\n",tfile);
    if(!f) { cout<< " couldnt open this file"<<endl; return(0);}

    //  START OF Generate_Boat_Script( char**data, FILE *fp){

    bg->I	=					Evaluate_Quantity(sail,data[I_OFF],"length")/1000;
    bg->J = 					Evaluate_Quantity(sail,data[J_OFF],"length")/1000;
    bg->EM = 					Evaluate_Quantity(sail,data[E_OFF],"length")/1000;
    bg->P  = 					Evaluate_Quantity(sail,data[P_OFF],"length")/1000;
    bg->Rake_Ipt	 =			localEvaluate_Offset(  data[RAKE_OFF],bg->I)/1000;
    if(bg->Rake_Ipt < 1e-6) {
        bg->Rake_Ipt		=		localEvaluate_Offset(  data[GRAKE_OFF],bg->I)/1000;
    }
    bg-> BAD	 = 				Evaluate_Quantity(sail,data[BAS_OFF],"length")/1000;
    bg->Mast_Freeboard = 		Evaluate_Quantity(sail,data[FREEB_OFF],"length")/1000;
    if(bg->Mast_Freeboard < 1e-6) {
        bg->Mast_Freeboard = 1.0;
    }
    bg-> Bow_Freeboard = bg->Mast_Freeboard;

    bg->Mast_Size_Shear = 		Evaluate_Quantity(sail,data[PROF_LENGTH_OFF],"length")/1000;
    if(bg->Mast_Size_Shear < 1e-5) {
        bg->Mast_Size_Shear = bg->J*0.03;
    }
    bg->BG		=				Evaluate_Quantity(sail,data[BG_OFF],"length")/1000;
    bg->BH		=				Evaluate_Quantity(sail,data[BH_OFF],"length")/1000;


    bg->Mast_Size_Ipt = bg->Mast_Size_Shear*.8;   /* arbitrary reduction of mast size */

    bg->Overlap	=			Evaluate_Quantity(sail,data[FOOT_OFF],"length")/1000;
    bg->Overlap = bg->Overlap - bg->J;
    bg->Sheeting_Base=0.0;
    /*
if(Overlap < 1e-6) {  
  if(BG > 0.0 && BH > 0.0) {
    Overlap = (BG+BH)/2.0  - J;
  }
}
*/

    bg->Main_Clew_Height	= 	bg->BAD;
    bg->Port_Tack 		=   strieq(            data[16],"Port");
    bg->Tube_Length=bg->BAD + bg->P;
    bg->Rake_Ipt =bg-> Rake_Ipt/bg->Tube_Length;   /* turn into a % */


    bg->LOA = Evaluate_Quantity(sail,data[LOA_OFF],"length");  /* already in metres */

    bg->Clew_Height = bg->J*0.05;


    /* allow for I measured from Shear level */




    bg->MClew_X = bg->EM+bg->Mast_Size_Shear + bg->BAD*bg->Rake_Ipt/bg->I;
    // x = y = z = 0.0;
    bg->EA = 10e7*bg->I*bg->I*bg->J*bg->J/625.0/64.0;
    bg->EA_Cun = bg->EA/10.0;


    fprintf(f,"! I = %f\n",bg->I);
    fprintf(f,"! J = %f\n",bg->J);
    fprintf(f,"! EM = %f\n",bg->EM);
    fprintf(f,"! P = %f\n",bg->P);
    fprintf(f,"! Rake_Ipt = %f\n",bg->Rake_Ipt);
    fprintf(f,"! BAD = %f\n",bg->BAD);
    fprintf(f,"! Mast_Freeboard = %f\n",bg->Mast_Freeboard);
    fprintf(f,"! Overlap = %f\n",bg->Overlap);
    PBS_Write_Boat_Script(f,bg);
    fprintf(f,"EOF\n");

    // END OF GENERATION
    depth=1;

    FCLOSE (f);


    sail->startmacro(tfile,&depth);

    sprintf(buf,"rm %s",tfile);
    system(buf);
    RXFREE(data);
    return(1);
}


int Create_Boat_Script(SAIL*sail,const char*names[],const char *data[],const char*p_fileroot, struct BOATGEN *bg)	{
    FILE *f;
    int k;
    char file[256],fileroot[256];

    strcpy(fileroot,p_fileroot);


    bg->I = 					Evaluate_Quantity(sail,data[ 2],"length");
    bg->J = 					Evaluate_Quantity(sail,data[ 3],"length");
    bg-> EM = 				Evaluate_Quantity(sail,data[ 4],"length");
    bg->P  = 				Evaluate_Quantity(sail,data[ 5],"length");
    bg->Rake_Ipt	 =			localEvaluate_Offset(  data[ 6],bg->I);
    bg->BAD	 = 				Evaluate_Quantity(sail,data[ 7],"length");
    bg->Bow_Freeboard = 		Evaluate_Quantity(sail,data[ 8],"length");
    bg->Mast_Freeboard  = 	Evaluate_Quantity(sail,data[ 9],"length");
    bg->Mast_Size_Shear = 	Evaluate_Quantity(sail,data[10],"length");
    bg->Mast_Size_Ipt= 		Evaluate_Quantity(sail,data[11],"length");
    bg->Overlap 			=	localEvaluate_Offset(  data[12],bg->J);
    bg->Sheeting_Base	=   - fabs(localEvaluate_Offset(data[13],bg->J));

    bg->Track = 0.0;		/*	=	localEvaluate_Offset(data  [14],J); */
    bg->Clew_Height		=	localEvaluate_Offset(data  [15],bg->J) ;
    bg->Main_Clew_Height	= 	bg->BAD;

    bg->Tube_Length=bg->BAD + bg->P;

    bg->Port_Tack 		=   strieq( data[16],"Port");
    bg->MClew_X = bg->EM+bg->Mast_Size_Shear + bg->BAD*bg->Rake_Ipt/bg->I;

    bg->EA = 10e7*bg->I*bg->I*bg->J*bg->J/625.0/64.0; bg->EA_Cun = bg->EA/10.0;
    bg->HLength =  bg->I + bg->Mast_Freeboard -bg->Bow_Freeboard ;
    bg->HLength = bg->HLength *bg->HLength ;
    bg->HLength =  bg->HLength  + (bg->J + bg->Rake_Ipt) *  (bg->J +bg-> Rake_Ipt) ;
    bg->HLength = sqrt(bg->HLength);

    if (strieq(data[14],"Custom")) {
        bg->Custom_Jibsheet = 1;
    }
    else {
        bg->Custom_Jibsheet = 0;
        bg->Track			=	localEvaluate_Offset(data  [14],bg->J);
    }
    if(bg->Port_Tack)
        bg->Sheeting_Base = -bg->Sheeting_Base;

    PC_Strip_Trailing(fileroot);
    sprintf(file,"%s.in",fileroot);
    f=RXFOPEN(file,"w"); if(!f) return(0);

    fprintf(f,"! boat definition file\t%s\n!\n",file);
    fprintf(f,"name\t boat \t %s \t 0 \t 1 \t 0 \t $onflag=0\n",data[0]); // july 2010 dont want ONFlag
    fprintf(f,"! FILE DEFINITIONS\n");
    fprintf(f,"input\tboat\t%s.in\n",fileroot);
    fprintf(f,"analysis\tboat\t%s.dat\n",fileroot);


    fprintf(f,"! original data \n");
    for(k=0;*names[k];k++)
        fprintf(f," ! %d %30s\t%s\n",k, names[k],data[k]);

    fprintf(f,"!  EA for most strings taken as %f\n\n",bg->EA);

    PBS_Write_Boat_Script(f,bg);

    fprintf(f,"\ninclude \t%s\n",data[1]);
    fprintf(f,"\nEOF\n");

    FCLOSE(f);
    return 1;
}
int Generate_Boat_Script (SAIL *const sail,char *data[],FILE *f, struct BOATGEN *bg) {
    //  START OF Generate_Boat_Script( char**data, FILE *fp){

    bg->Port_Tack=0;
    bg->I=                 Evaluate_Quantity(sail,data[I_OFF],"length")/1000;
    bg->J = 					Evaluate_Quantity(sail,data[J_OFF],"length")/1000;
    bg->EM = 					Evaluate_Quantity(sail,data[E_OFF],"length")/1000;
    bg->P  = 					Evaluate_Quantity(sail,data[P_OFF],"length")/1000;
    bg->Rake_Ipt	 =			localEvaluate_Offset(  data[RAKE_OFF],bg->I)/1000;
    if(bg->Rake_Ipt < 1e-6)
        bg->Rake_Ipt  =			localEvaluate_Offset(  data[GRAKE_OFF],bg->I)/1000;

    bg->BAD	 = 				Evaluate_Quantity(sail,data[BAS_OFF],"length")/1000;
    bg->Mast_Freeboard = 		Evaluate_Quantity(sail,data[FREEB_OFF],"length")/1000;
    if(bg->Mast_Freeboard < 1e-6) {
        bg->Mast_Freeboard = 1.0;
    }
    bg->Bow_Freeboard = bg->Mast_Freeboard;
    bg->Mast_Size_Shear = 	Evaluate_Quantity(sail,data[PROF_LENGTH_OFF],"length")/1000;
    if(bg->Mast_Size_Shear < 1e-5) {
        bg->Mast_Size_Shear = bg->J*0.03;
    }
    bg->BG =      Evaluate_Quantity(sail,data[BG_OFF],"length")/1000;
    bg->BH =      Evaluate_Quantity(sail,data[BH_OFF],"length")/1000;

    bg->Mast_Size_Ipt = bg->Mast_Size_Shear*.8;   /* arbitrary reduction of mast size */

    bg->Overlap = Evaluate_Quantity(sail,data[FOOT_OFF],"length")/1000;
    bg->Overlap = bg->Overlap - bg->J;
    bg->Sheeting_Base=0.0;
    /*
if(Overlap < 1e-6) {  
  if(BG > 0.0 && BH > 0.0) {
    Overlap = (BG+BH)/2.0  - J;
  }
}
*/

    bg->Main_Clew_Height	= 	bg->BAD;
    bg->Port_Tack 		=   strieq(            data[16],"Port");
    bg->Tube_Length=bg->BAD + bg->P;
    bg->Rake_Ipt = bg->Rake_Ipt/bg->Tube_Length;   /* turn into a % */

    bg->LOA = Evaluate_Quantity(sail,data[LOA_OFF],"length");  /* already in metres */

    bg->Clew_Height = bg->J*0.05;


    /* allow for I measured from Shear level */





    bg-> MClew_X = bg->EM+bg->Mast_Size_Shear + bg->BAD*bg->Rake_Ipt/bg->I;
    // x = y = z = 0.0;
    bg->EA = 10e7*bg->I*bg->I*bg->J*bg->J/625.0/64.0; bg->EA_Cun = bg->EA/10.0;


    bg->I=Evaluate_Quantity(sail,data[I_OFF],"length")/1000;
    bg->profile_length = Evaluate_Quantity(sail,data[PROF_LENGTH_OFF],"length")/1000;
    bg->profile_width = Evaluate_Quantity(sail,data[PROF_WIDTH_OFF],"length")/1000;

    PBS_Write_Boat_Script(f,bg);

    FCLOSE(f);
    return 1;
}
int PBS_Print_Set(FILE *f, const char*a, double b) {
    fprintf(f,"set     \t  %s  \t %f \t / \n",a,b);
    // fprintf(f,"question\t %s? \t  \t text \t set \t %s \t 2 \t %f \n",a,a,b);
    return 1;
}


int PBS_Write_Boat_Script(FILE *f, struct BOATGEN *bg){
    double y,z,Headstay_PS;
    char xs[256];

    //PBS_PrintBoatGenVariables(f);

    PBS_Print_Set(f,"IG",bg->I);
    PBS_Print_Set(f,"JG",bg->J);
    PBS_Print_Set(f,"EM",bg->EM);
    PBS_Print_Set(f,"P",bg->P);
    PBS_Print_Set(f,"Rake_Ipt",bg->Rake_Ipt);
    PBS_Print_Set(f,"BAS",bg->BAD);

    PBS_Print_Set(f,"Bow_Freeboard",bg->Bow_Freeboard);
    PBS_Print_Set(f,"Mast_Freeboard",bg->Mast_Freeboard);

    fprintf(f,"set\t Tube_Length\t (BAS + P )\t / \n");

    PBS_Print_Set(f,"Overlap",bg->Overlap);
    PBS_Print_Set(f,"Mast_Size_Shear", bg->Mast_Size_Shear );
    PBS_Print_Set(f,"Mast_Size_Ipt", bg->Mast_Size_Ipt );
    PBS_Print_Set(f,"Main_Clew_Height", bg->BAD );

    PBS_Print_Set(f,"Genoa_Sheet_Base",bg->Sheeting_Base);
    PBS_Print_Set(f,"Genoa_Track_Length",bg->Track);
    PBS_Print_Set(f,"Genoa_Clew_Height",bg->Clew_Height);
    //PBS_Print_Set(f,"Headstay_EA", EA );
    //PBS_Print_Set(f,"Headstay_PS", 80000.0 );
    //PBS_Print_Set(f,"CunningHam_EA", EA_Cun );


    Headstay_PS = 80000.0 * (bg->I/25) * (bg->J/8);
    fprintf(f,"Grid Density \t GridDenName \t %s \n", "1.0 ! (P/32.0) (dec 2009 this crashes in meshing");
    //	fprintf(f,"Question : Grid Density : :text:Grid Density: GridDenName: 2 : 1.0\n ");
    fprintf(f,"2Dcurve \t straight \t 0 \t 0 \t 100 \t 0\n");
    fprintf(f,"\n\n!  SETTING OUT\t THE MAST\n");


    fprintf(f,"spline \t mast_spline\t 33 \t0\t0\t0 \t%s\t0.0\t0.0\t %s\t0\t0 \t %s\t0\t 0\t %s\t0\t0\n","(Tube_Length*0.25)", "(Tube_Length*0.5)" , "(Tube_Length * 0.75)" , "(Tube_Length)");



    fprintf(f,"site\t trackbase\t Mast_Size_Shear \t 0 \t Mast_Freeboard \t ($fix=full)\n" );
    fprintf(f,"site\t Cun_base\t (Mast_Size_Shear+0.1) \t 0 \t (Mast_Freeboard *0.5) \t ($grid=9999.0)($fix=full)\n" );

    fprintf(f,"! I POINT \n\n");
    //  fprintf(f,"site\t rakeref\t %s \t 0.0 \t %f\t $editable ($fix=full)\n","(Mast_Size_Shear+Rake_Ipt)",z*0.99);
    fprintf(f,"site\t rakeref\t %s \t 0.0 \t %s\t $editable ,($fix=full)\n","(Mast_Size_Shear+Rake_Ipt)","((IG+Mast_Freeboard)) ");

    fprintf(f,"edge\ttrack\ttrackbase\trakeref\tmast_spline\t0.0\tflat Xabs draw YasX,$string,$ea=0.1,$ti=0.1,$notrim ,($fix=full)\n");

    fprintf(f,"\n! genoa head	 \n");

    fprintf(f,"relsite\t ip1\t track \t (IG*0.99)\t($fix=full)\n");
    fprintf(f,"relsite\t ip2\t track \t (IG*1.01)\t($fix=full)\n\n");

    fprintf(f,"2Dcurve\t square\t 0\t 0\t 10\t100\t90\t100\t 100\t0\n");
    fprintf(f,"curve\tiptc\tip1\tip2\tsquare\t%s\txscaled yscaled flat sketch ,($fix=full)\n","( - Mast_Size_Ipt)");
    fprintf(f,"relsite\t ipoint\t iptc \t 50%%\t ($fix=full)\n");

    fprintf(f,"connect\tg_b_head\t site \t ipoint    \tboat \t site \t  ipt \tgenoa \t \t 2 \n");


    fprintf(f,"\n! MAINSAIL \tATTACHMENT \tDETAILS \n! TACK \n");
    // fprintf(f,"relsite\tb_main_tack\ttrack\t%f \n",BAD);
    fprintf(f,"site \t b_main_tack \t  %s \t 0 \t %s \t !$editable\n","Mast_Size_Shear"   , "(BAS + Mast_Freeboard)"); // $editable is an experiment to try to get it into the NDD file.

    fprintf(f,"connect \t b_m_t_m \t string \t track \t boat \t site \t  b_main_tack  \t boat \t$slide$spline$interp\n");

    fprintf(f,"site \tCun_mid \tMast_Size_Shear\t0.000000 \t(Mast_Freeboard+BAS/2) \t($grid=9999.0)($fix=0 1 0 1 )\n");


    /*
!key word	name	Keyword '%curve' or '%node'	A seam or curve or edge	Keyword'%curve' or '%node'			terminator	EA	length	Prestress	optional attributes
string	headstay	%curve	leech	%node	headfwd	ipoint	%endlist	10E4	0	0	$noslide
  */
    char attsbuf[512]; sprintf(attsbuf,"($ea=%f),($mass=0),($trim=0),($zi=0),($ti=0)", bg->EA_Cun );
    fprintf(f,"string\tMain_Cunningham\t%%node\tCun_base\tCun_mid\tb_main_tack\t%%endlist \t%f \t  0\t  0 \t%s\n",bg->EA_Cun,attsbuf);
    fprintf(f,"question\tCunningham_EA \t   \t text \t string \t Main_Cunningham \t 6 \t %f \n",bg->EA_Cun);

    fprintf(f,"connect\tm_b_tack\t site \t b_main_tack \t boat \t  site \t tack\tmain \t \t 2\n");


    fprintf(f,"\n! HEAD \n");
    fprintf(f,"relsite\tb_main_head\ttrack\t %s \t($fix=full)\n","BAS+P");

    fprintf(f,"connect\tm_b_head\t site \t b_main_head\tboat\t site \t headfwd \t main \t \t1\n");

    fprintf(f,"! LUFF CURVE\t CONNECTION\nconnect\t m_b_luff \t string \ttrack\tboat\t string \t  luff \t main \t $slide$spline$interp\n");


    fprintf(f,"\n! CLEW  \n");
    y = bg->EM / 5.0;
    if(bg->Port_Tack) y = - y;


    // fprintf(f,"site\t main_traveller \t %f \t0m \t %f \teditable($grid=9999.0)\n",MClew_X,	 Mast_Freeboard);
    //  fprintf(f,"site\tb_main_clew\t%f \t0\t %f \n",MClew_X, Main_Clew_Height+Mast_Freeboard);
    //  fprintf(f,"fix\tmain_traveller \t($fix=full)\n ");


    //  fprintf(f,"string\tmain_sheet\t%%node\tb_main_clew\tmain_traveller\t%%endlist \t %f\t  0\t  0 \n",EA);
    // fprintf(f,"string\tmain_boom\t%%node\tb_main_clew\tb_main_tack\t%%endlist\t %f\t 0\t  0	 \n",10.0*EA);

    fprintf(f,"site \t b_main_clew \t %s \t 0 \t %s \t""cylindricalXY, (origin=trackbase)$editable ,($fix=full)""\n","(em+mast_size_shear)", "Main_Clew_Height");
    fprintf(f,"connect\t m_b_clew_fix \t site\t b_main_clew \t boat \t site \t clew \t main\t \t 3 \n\n");


    fprintf(f,"! GENOA\t ATTACHMENT \n");
    fprintf(f,"! tack \n");
    fprintf(f,"site\t b_gen_tack\t (JG*(-1)) \t0\t Bow_Freeboard \t ($fix=full)\n");

    fprintf(f,"connect\t g_b_tack \t site \t b_gen_tack\tboat \t  site \t tack \tgenoa\t \t 1\n");

    fprintf(f,"\n\n! Headstay \n");

    fprintf(f,"edge\theadstay \tb_gen_tack \tipoint \tstraight \t 0.000 \tflat Xscaled draw Yscaled");
    fprintf(f,",$string,$ea=%f,$ti=%f,$noslide\n",bg->EA, Headstay_PS );
    //  fprintf(f,"string \t headstay \t%%curve \theadstay \t%%endlist \t %f \t 0. \t%f\t$noslide\n\n\n", bg->EA, Headstay_PS );

    fprintf(f,"set \t headstaylength \t $stringconst$kkk$headstay$expression$length(kkk)\t / \n");

    fprintf(f,"connect \t g_b_head2 \t site \t ipoint \t boat \t site \t ipoint \t genoa \t 2\n");
    fprintf(f,"connect \t b_g_l \t string \t headstay \t boat \t string \t luff \t genoa \t $slide$spline$interp\n");



    fprintf(f,"\n! clew ");

    if(bg->Port_Tack)
        fprintf(f," (Port Tack)\n");
    else
        fprintf(f," (Starboard Tack)\n");

    if (!bg->Custom_Jibsheet) 	{
        strcpy(xs, " (Overlap + Mast_Size_Shear) ") ;
        z = bg->Mast_Freeboard + bg->Clew_Height*0.7071;

        fprintf(f,"site\tb_gen_clew\t (%s%s) \t%s\t%s\t($grid=9999.0)\n",xs,"-(Genoa_Clew_Height*0.7071)", "Genoa_Sheet_Base", "(Mast_Freeboard + ( Genoa_Clew_Height*0.7071))");

        fprintf(f,"site\tgenoa_car\t%s\t%s\t %s\t$editable ,($fix=full)\n",xs,"Genoa_Sheet_Base","Mast_Freeboard");
        fprintf(f,"site\tgen_car_dum\t%s\t%s\t %s\t($grid=9999.0)\n",xs,"(Genoa_Sheet_Base+0.1)","Mast_Freeboard");

        fprintf(f,"connect\tg_b_hc\t site \t genoa_car    \tboat \t site \t gen_car_dum \tboat \t \t 2 \n");

        fprintf(f,"string\tgen_sheet\t%%node\t b_gen_clew \tgen_car_dum \t%%endlist\t%f\t0\t0 !sb about 10e7\n",bg->EA);

    }
    else {
        fprintf (f,"include \t ?genoa track assembly?\t Genoa Track File \t %s/*.in\n",g_boatDir);
    }

    fprintf(f,"connect\tg_b_clew\t site \t b_gen_clew\t boat \tsite \t clew \tgenoa \t \t 3\n");

    // END OF GENERATION

    return 1;
}



