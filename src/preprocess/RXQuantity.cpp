#include "StdAfx.h"
#include <QDebug>
#include "RXSail.h"
#include "RelaxWorld.h"
#include "RXEntityDefault.h"
#include "RXSeamcurve.h"
#ifndef linux
#include "..\stringtools\stringtools.h" // defines FromUtf8 and ToUtf8

#else
#include "../../stringtools/stringtools.h"
#endif

#include "pctable.h"
#include "stringutils.h"
#include "global_declarations.h"

#include "RXQuantity.h"

// parameter 'units' is hard-coded . It's the units used internally for this quantity.

// parameter 'p_buf' may be a number followed by a unitstring (or '%')
// or it may be a general expression.
// if it has units, we should check for compatibility with internalUnits
// and look up a scale factor for 'evaluate'


RXQuantity::RXQuantity(const RXSTRING &name,const RXSTRING &p_buf,const RXSTRING &internalunits, class RXSail *const p_sail):
    RXExpression(  name, p_buf,  p_sail),
    m_scalefaCTor(1.0),
    m_m(0),
    m_l(0),
    m_t(0)

{
    SetInternalUnits(internalunits);
    RXSTRING l_exp =p_buf;
    m_unitstring = UnitString(l_exp);
    SetScaleFactor();
    if(!m_unitstring.empty())
        this->Fill(l_exp);


}
RXQuantity::RXQuantity(){
    assert(strlen( "dont use this constructor")==0);
}
RXQuantity::~RXQuantity(){

}
MTDOUBLE RXQuantity::evaluate(){
    MTDOUBLE v = RXExpression::evaluate ();
    return v*m_scalefaCTor;

}
// if the last token of s is a recognised unit, strip it from s and return it
// but only if it is preceded by optional whitespace and a number
RXSTRING RXQuantity::UnitString(RXSTRING &s){
    rxstriptrailing(s);
    RXSTRING  head,tail;
    if(s.size()<1) return L"";
    wchar_t lc = s[s.size()-1];
    if(lc==L'%') {
        //wcout<< s<<L" has a percent as last char, becomes ";
        s.erase(s.size()-1);
        //wcout<< s<<endl;
        return L"%";
    }

    wchar_t*lp;
    size_t nc;
    double x = wcstod(s.c_str(),&lp);
    nc = lp-s.c_str();
    if(nc==0){ // there's no numeric prefix
        //wcout<< s<<L"> has no numeric prefix"<<endl;
        return L"";
    }
    if(nc == s.length()){
        //wcout<< s<<L"> is a pure number"<<endl;
        return L"";
    }
    head=s.substr(0,nc); rxstriptrailing(head);
    tail=s.substr(nc,string::npos);  rxstripleading(tail);

    //wcout <<s<<L"> becomes <"<<head<<L"> and <"<<tail<<L">"<<endl;

    if(tail==L"mm"
            ||tail==L"ft"
            ||tail==L"in"
            ||tail==L"m"
            )
    {
        s=head;
        return tail;
    }
    else {
        if(tail.length())
            if ( !(tail[0]==L'*'  || tail [0]==L'/'
                   ||tail [0]==L'+'  ||tail [0] ==L'-'   ) )
                wcout << L"Units '"<<tail<<L"' not recognised"<<endl;

        //        else
        //                wcout << L"Units '"<<tail<<"'CAUGHT!!!!!!!"<<endl;
    }
    return L"";
}

QString RXQuantity::Descriptor()const
{
    QString q("Q_"); q+= this->GetQName();
    RXEntity_p e = dynamic_cast<  RXEntity_p>(this->GetOwner());
    if(e)
        q.prepend(e->Descriptor()+"$");
    else if(this->Model())
        q.prepend(this->Model()->GetQType()+"$");

    return q;

}
// look up a table of units to get the scale factor
double RXQuantity::SetScaleFactor()
{
    if(m_unitstring.empty()) m_scalefaCTor=1.0;
    else if(m_unitstring==L"%")
        m_scalefaCTor =0.01;
    else if(m_unitstring==L"mm")
        m_scalefaCTor =0.001;
    else if(m_unitstring==L"m")
        m_scalefaCTor =1.000;
    else if(m_unitstring==L"ft")
        m_scalefaCTor =0.304800000000000;
    else if(m_unitstring==L"in")
        m_scalefaCTor =0.0254000;
    else {
        cout << "units not recognised '"<<m_unitstring.c_str()<<"'"<<endl;
    }

    return m_scalefaCTor;

}

void  RXQuantity::SetNeedsComputing(const int i){
    if(i)
    {
        OnValueChange();
        if(! this->m_accessflags&RXE_READONLY) {
            RXENode *en = this->GetOwner();
            RXEntity_p ee = dynamic_cast<RXEntity_p> (en);

            if(ee)
               {
                ee->SetNeedsComputing();
                qDebug()<<" Are we sure we need to setNC on a Quantity's  owner???";
            }
            else
                qDebug()<<" Found PM Crash";
        }
    }
    else
        RXObject::SetNeedsComputing(0);
}

void RXQuantity::SetInternalUnits(const RXSTRING &units)
{
    if(units.empty()){
        m_m=0; m_l=0; m_t=0;
    }
    else if(units==L"%"  ) {
        m_m=0; m_l=0; m_t=0;
    }
    else if(units==L"N" || units==L"n") {
        m_m=1; m_l=1; m_t=-2;
    }
    else if(units==L"m" ) {
        m_m=0; m_l=1; m_t=0;
    }
    else if(units==L"Kg/m" ) {
        m_m=1; m_l=-1; m_t=0;
    }
    else {
        QString qq("Havent coded for variables of Units: '" );
        qq+=QString::fromStdWString(units);
       qDebug()<<qq;
        m_m=0; m_l=0; m_t=0;
    }


}


// statics
double Evaluate_Length_String(SAIL *const p_sail,char *s_in){
    /* s will be of type
   <length units>[entityname[modelname]]
   using commas or any brackets as separators */
    const char *u[6] = {"cm" , "in" , "ft" ,"mm" ,"m","%"};
    const double f[6]= {0.01 ,0.0254, 0.3048, 0.001, 1.0, 0.01 };
    int k,n=6;
    double x,y;
    char s[256];
    class RXEntity *pe=0;
    sc_ptr sc;
    char *w1=NULL, *w2=NULL, *w3=NULL;
    const char *seps = ",()[]:;/{}";

    if(!(*s_in)) return(1.0);// LOOKS WRONG

    strcpy(s,s_in);
    w1 = strtok(s,seps);
    if(w1) {
        /* w1 will be units like percent*/
        x=1.0;
        for (k=0;k<n;k++) {
            if (strieq(w1,u[k])) {
                x =  f[k];	 break;
            }
        }
        w2 = strtok(NULL,seps);	   /* entity name */
        if(w2) {
            w3 = strtok(NULL,seps);	 /* model name */
            if(w3) {
                SAIL *theSail = g_World->FindModel(w3,0); // dont care if its hoisted
                if(theSail)
                    pe = theSail->GetKeyWithAlias("curve,seamcurve,compound curve",w2);
            }
            else // no w3
            {
                pe = p_sail->GetKeyWithAlias("curve,seamcurve,compound curve",w2);
            }
            if(pe && !pe->Needs_Finishing) {  /* BEWARE. called from Resolve, freezes the initial value */
                sc = (sc_ptr  )pe;
                y = sc->GetArc(1);
            }
            else
                y=1.0;
        }
        else {
            y=1.0;
        }
        return(y*x);
    }
    else {/* nothing */
        return(1.0);
    }
    return(x);
}

double Evaluate_Quantity(SAIL *const p_Sail, const RXSTRING &offinw, const wchar_t*typew){

    std::string offin = ToUtf8(offinw);
    std::string type = ToUtf8(TOSTRING(typew));
    return Evaluate_Quantity(p_Sail, offin.c_str(), type.c_str());

}


double Evaluate_Quantity(SAIL *const p_Sail, const char* offin,const char *type) {

    /*    offin  is in form "3.2%" . The trailing string can be one of
 % m mm ft in cm

 use QP C_Ge t_Key() to get the arc length of (eg) genoa luff
 */
    const  char *u1[6] = {"cm" , "in" , "ft" ,"mm" ,"m","%"};
    const double f1[6]     = {0.01 , 0.0254, 0.3048,0.001, 1.0, 0.01 };
    int n1=6;
    const char *u2[7] = {"N" , "KN" , "MN" ,"lb" ,"kip","kg","DaN"};
    const double f2[7] = {1.0 ,1000.,1000000.,4.459091,4459.091, 9.81,10.0};
    int n2 = 7;

    const char *u3[6] = {"kg" , "T" , "lb" ,"Tonne" ,"g","Kg"};
    double f3[6] = {1.0 ,1018.18,0.454,1000.0,0.001, 1.0 };
    int n3=6;

    const char *u4[2] = {"%" ,"pts"};
    double f4[2] = {0.01,0.000625};
    const char *u5[4] = {"m/s","Kno","knots","kn"};
    double f5[4] = {1.0,0.5144,0.5144,0.5144};
    int n5=4;

    int n4=2;
    double x = 0.0,y;
    int k,n,nread;
    const char **units;
    const double *f;
    char s[32];
    int length=0;
    strcpy(s," ");
    x = 0.0;
    if(rxIsEmpty(offin)) return (x);
    if(PCT_Part_Of("length",type))    {
        units=u1;
        f = f1;
        n=n1;
        length=1;
    }
    else  if(PCT_Part_Of("unknown",type))    {
        units=u1; f = f1; n=n1;
    }
    else if(PCT_Part_Of("force",type))     {units=u2; f = f2; n=n2;}
    else if(PCT_Part_Of("mass",type))      {units=u3; f = f3; n=n3;}
    else if(PCT_Part_Of("extension",type)) {units=u4; f = f4; n=n4;}
    else if(PCT_Part_Of("speed",type)) {units=u5; f = f5; n=n5;}
    else {
        char str[256];
        sprintf(str,	"(Evaluate Quantity) unknown type\n<%s>" ,type);
        p_Sail->OutputToClient(str,1); return(x);
    }

    if(stristr(offin,"n/a")|| stristr(offin,"nan")) {
        x = 0.0;
        return(x);
    }

    nread=sscanf(offin,"%lf%s",&x,s);
    if (nread<=0) {
        char string[256];
        sprintf(string,"(Evaluate Quantity) cannot evaluate '%s' '%s'  ",type,offin);
        p_Sail->OutputToClient(string,1);
        x = 0.0;
    }
    else if(nread==1) return(x);
    if(length) {
        if(!p_Sail) {
            assert(0);
            return  0.0;
        }
        y = Evaluate_Length_String(p_Sail, s);
        x=x*y;
        return(x);
    }

    for (k=0;k<n;k++) {

        if (strieq(s,units[k])) {
            x = x * f[k];
            return(x);
        }
    }
    return(x); /* default m */
}
// this is to replace Evaluate_Quantity.  It tries to resolve and evaluate p_buf on the current ENode.
// ie it looks up the inheritance tree to find variables it needs.
//static
double RXQuantity::OneTimeEvaluate(const RXSTRING &p_buf, const wchar_t*type,class RXENode *const context,int *err)   {
    double value;
    RXQuantity *expd =new RXQuantity( L"local",p_buf,type,0 );
    context->AddExpression(expd );
    if( !expd->ResolveExp() )
    {
        delete expd;
        *err=1;
        return 0.0;
    }
    value = expd->evaluate();
    delete expd;
    *err=0;
    return value;

}
RXQuantityWithPtr::RXQuantityWithPtr() // keep a dble up to date with the result of the expression
    :m_dblPtr(0)
{
    assert(strlen("dont use this constructor")==0);
}

RXQuantityWithPtr::~RXQuantityWithPtr() {
}

RXQuantityWithPtr::RXQuantityWithPtr(const RXSTRING &name,const RXSTRING &p_buf,const RXSTRING &units, class RXSail *const p_sail, double*dptr)
    :RXQuantity(name,p_buf,units,p_sail)
    ,m_dblPtr(dptr)
{
}

int  RXQuantityWithPtr::Compute(){ // force an evaluate so the target value gets set
    int rc=0;
    double x1 = *(this->m_dblPtr);
    double x = this->evaluate();
    rc = (x1 !=*(this->m_dblPtr) ); assert( x==*(this->m_dblPtr)  );
    return rc;
}
MTDOUBLE RXQuantityWithPtr::evaluate()
{
    double x =  RXQuantity::evaluate();
    *m_dblPtr =x;
    return x;
}
QString  RXQuantityWithPtr::Descriptor()const
{
    QString q("QPTR_"); q+=this->GetQName();
    RXEntity_p e = dynamic_cast<  RXEntity_p>(this->GetOwner());
    if(e)
        q.prepend(e->Descriptor()+"_");
    else if(this->Model())
        q.prepend(this->Model()->GetQType()+"_");
    return q;
}
RXQuantityReadOnlyDble::RXQuantityReadOnlyDble() // keep a dble up to date with the result of the expression
    :m_dblPtr(0)
    ,m_oldValue(0)
{
    assert(strlen("dont use this conStructor")==0);
}

RXQuantityReadOnlyDble::~RXQuantityReadOnlyDble() {
}

RXQuantityReadOnlyDble::RXQuantityReadOnlyDble(const RXSTRING &name,const RXSTRING &p_buf,const RXSTRING &units, class RXSail *const p_sail, double*dptr)
    :RXQuantity(name,p_buf,units,p_sail)
    ,m_dblPtr(dptr)
    ,m_oldValue(0)
{
    this->m_accessflags|= RXE_READONLY;
}

int  RXQuantityReadOnlyDble::Compute(){ // force an evaluate so the target value gets set
    if(*m_dblPtr != m_oldValue) {
        this->Change(TOSTRING(*m_dblPtr));// we should overload this so it doesnt set

        // Feb 2014  OnValueChange is
        this->OnValueChange (); // this is in RXQuantityReadOnlyDble::Compute
        m_oldValue =*m_dblPtr;
        return 1;
    }

    return 0;
}

MTDOUBLE RXQuantityReadOnlyDble::evaluate()
{   double x =  *m_dblPtr;
    Change(TOSTRING(x));
    return  x;
}
RXSTRING RXQuantityReadOnlyDble::GetText() const
{   RXSTRING rv;
    rv = this->RXQuantity::GetText();

    return rv;
}
QString  RXQuantityReadOnlyDble::Descriptor()const
{
    QString q("ROD_"); q+=this->GetQName();
    RXEntity_p e = dynamic_cast<  RXEntity_p>(this->GetOwner());
    if(e)
        q.prepend(e->Descriptor()+"$");

    else if(this->Model())
        q.prepend(this->Model()->GetQType()+"$");
    return q;
}
