/* Relax II source code

 created  PH aug 1 2004
  */

#include "StdAfx.h"
#include "RXSail.h"
#include "RXEntity.h"
#include "stringutils.h"
#include "pctable.h"
#include "akmutil.h"

#include "pressure.h"


int Resolve_Pressure_Card (RXEntity_p e) {

	//pressure card is of type    pressure: <name> :  atts :  $table   [ a table] 

  const char *name,*atts ;
  HC_KEY key=0;
  const char *l_pt;;
  struct PC_TABLE *l_table;

     const char*line = e->GetLine(); 
    int retval=0;
  	std::string sline(e->GetLine());
	rxstriptrailing(sline);
	std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
	int nw = wds.size();
	 
	if(nw<  4) return 0;

	name=wds[1].c_str();{//  if (HC_Parse _String(line,":",1,name)){
	atts=wds[2].c_str();{// if (HC_Parse _String(line,":",2,atts)){
			l_pt = stristr(line,"$table"); if(l_pt) l_pt+=6;
     		if(l_pt && PCT_Read_Table_From_Text(l_pt,&(l_table))) {
				  Pressure *ptr = (Pressure *)MALLOC(sizeof(Pressure));
				  ptr->m_table = l_table;
				  ptr->m_p0wner =e; 
				  ptr->m_flags=0; 
#ifdef USE_PANSAIL
				  e->Esail->m_M = l_table->nc+1; // chordwise
				  e->Esail->m_N = l_table->nr+1 ; // spanwise
#endif
				e->OutputToClient(" 50% chance that pressure entity rows and columns are switched",1);

				  if(e->PC_Finish_Entity("pressure",name,ptr,key,(struct LINKLIST *)NULL,atts,line)) {
						e->SetNeedsComputing(0);
						e->Needs_Finishing=1;
						retval=1; 
				  }
			} else e->OutputToClient(" can't read table",3);
	}
  }
  e->Needs_Resolving=(!retval);
  return(retval);
} 

