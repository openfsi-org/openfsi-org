//curveplaneintersection.h header for curveintersection.cpp
//Thomas 08 09 04

#ifndef RXCURVEPLANEINTERSECTIONS2_H
#define RXCURVEPLANEINTERSECTIONS2_H

#include "RXUPoint.h"
#include "RXTRObject.h"

#ifndef RX_CRVPL_CROSS
	#define RX_CRVPL_CROSS 1
#endif //#ifndef RX_CRVPL_CROSS

#ifndef RX_CRVPL_N0TCROSS
	#define RX_CRVPL_N0TCROSS   -1
#endif //RX_CRVPL_N0TCROSS

#ifndef RX_CRVPL_PARALLEL
	#define RX_CRVPL_PARALLEL -2
#endif //#ifndef RX_CRVPL_PARALLEL


#ifndef RX_CRVPL_CRVINPLANE
	#define RX_CRVPL_CRVINPLANE -3
#endif //#ifndef RX_CRVPL_CRVINPLANE

#ifndef RX_CRVPL_ERROR
	#define RX_CRVPL_ERROR 0
#endif //#ifndef RX_CRVPL_ERROR

//!Thomas 29 10 04 the same as Curveplaneinetresection but works with RXUPoint and RXCrossing2 instead of RXPointOnCurve and RXCrossing

//!Finds the intersection beetween a line and a Plane
/*!
	and intersection is found if the line crosses the plane
	if an intersection is founf set p_pt on the intersection
	p_pPersistentCurve : persitant curve from which the line is extracted 
		if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine
	returns RX_CRVPL_CROSS if an intersection is found beetwene hte end and the start of the curve
	returns RX_CRVPL_N0TCROSS if NO intersection is found beetwene hte end and the start of the curve
	returns RX_CRVPL_COPLANAR if the line is coplanar to the plane
	returns RX_CRVPL_CRVINPLANE if the line is in the plane 
*/
int CurvePlaneIntersections(const ON_LineCurve * p_pLine,
						    const ON_Plane * p_Plane,
					 	    RXUPoint & p_pt,
							const double & p_EpsDot, 
							const RXTRObject * p_pPersistentCurve = NULL);

//!Finds all the intersections between a Polyline and a Plane
/*!
	and intersection is found if the Polyline crosses the plane
	if an intersection is found set p_pt on the intersection
	returns RX_CRVPL_CROSS if an intersection is found beetwene hte end and the start of the curve
	returns RX_CRVPL_N0TCROSS if NO intersection is found beetwene hte end and the start of the curve
	returns RX_CRVPL_COPLANAR if the line is coplanar to the plane
	returns RX_CRVPL_CRVINPLANE if the line is in the plane 
*/
int CurvePlaneIntersections(const ON_PolylineCurve * p_Crv,
							const ON_Plane * p_Plane,
							ON_ClassArray<RXUPoint> & p_pts, 
							const double & p_EpsDot);

//!Finds all the intersections between a CURVE and a Plane (ONLY WORKS IF THE CURVE IS A POLYLINE)
/*!
	and intersection is found if the CROSS crosses the plane
	if an intersection is found set p_pt on the intersection
	returns RX_CRVPL_CROSS if an intersection is found beetwene hte end and the start of the curve
	returns RX_CRVPL_N0TCROSS if NO intersection is found beetwene hte end and the start of the curve
	returns RX_CRVPL_COPLANAR if the line is coplanar to the plane
	returns RX_CRVPL_CRVINPLANE if the line is in the plane 
*/
int CurvePlaneIntersections(const RXTRObject * p_Crv,
							const ON_Plane * p_Plane,
							ON_LineCurve p_line,
							ON_ClassArray<RXUPoint> & p_pts, 
							const double & p_EpsDot,
							const double & p_CutDistance,
							double *p_Pseed=NULL);

#endif  //#ifndef RXCURVEINTERSECTIONS2_H
