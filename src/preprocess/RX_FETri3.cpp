
#include "StdAfx.h"
#include "RXSail.h"
#include "RXPanel.h"
#include "RX_FESite.h"
#include "f90_to_c.h"
#include "filament.h"
#include "RXSeamcurve.h"
#include <fstream>

#define  NO_DOVE_POSTPROC
#ifndef NO_DOVE_POSTPROC
#include "RXmaterialMatrix.h"
#include "RXdovefile.h"
#include "dovestructure.h"
#include "RXEntityDefault.h"
#include "RXLayer.h"
#include "fielddef.h"
#include "etypes.h"
#include "griddefs.h" // for matvaltype
#endif

#include "RX_FETri3.h"

#ifdef MICROSOFT
#define isnan _isnan
#endif

RX_FETri3::RX_FETri3(void)

    : m_CurrentLayer(0)
{
    //	cout<< "shouldnt use the default constructor for feTri3QQ m_N -99??"<<endl;
    assert(0); 		SetN(RXOBJ_INITIAL_N);
}

RX_FETri3::RX_FETri3(SAIL *p_sail)
    :  RX_FEObject(p_sail)
{
    m_sail=p_sail;
    Init(p_sail);
    int n=p_sail->PopTriNo();
    SetN(n);

    p_sail->NoteTri(this); // means put into m_FTri3s
#ifdef FORTRANLINKED
    cf_insert_tri3(p_sail->SailListIndex(),GetN() ,this);//int p_n1,int p_n2,double p_zi);
#else
    assert(0);
#endif
    SetIsInDatabase();
}

RX_FETri3::~RX_FETri3(void)
{
    int n,  ok; n=GetN();
    assert(n != RXOBJ_INITIAL_N) ; // would mean that its a local
    assert(n<= GetSail()->m_FTri3s.capacity());
    GetSail()->m_FTri3s[n]=0;
    GetSail()->PushTriNo(n);

    if( IsInDatabase()) {
        ok=cf_remove_tri3(GetSail()->SailListIndex(),n);
        if(!ok) cout<<"error removing tri "<<n<<endl;
        SetIsInDatabase(false);
    }
    this->ReInit (this->GetSail());
}
void RX_FETri3::Init(SAIL *s) {
    m_sail = s;
    int k;
    m_laminae = NULL;

    for (k=0;k<3;k++) {
        node[k]=0;
        m_iEd[k]=0;
        prestress[k]=0;
    }
    for (k=0;k<9;k++)
        mx[k]=0;

    m_TriArea=-1;
    m_RefAngle=ON_UNSET_VALUE; // the angle of the element base in its material axis system.
    creaseable=0;
    m_laminae=0;    /* array of TriLamDefn's one for each layer in the element */
    laycnt=0;
    m_colour=0;          /* set to current colour we want visible on screen */

    m_fils=0;

    m_CurrentLayer =-999;
    m_panel=0;

    // m_badtp =NULL;
}


void RX_FETri3::ReInit(SAIL *s) {
    m_sail = s;
    int i;
    if(m_laminae) {
        TriLamDefn *tld= m_laminae ;
        for(i=0;i<laycnt;i++,tld++) {
            if(tld->m_flags&TLD_ALLOCED)
                RXFREE(tld->dd);
        }
        RXFREE(m_laminae);
        m_laminae = NULL;
    }

    laycnt=0;

    if( m_colour)
        RXFREE( m_colour);

    m_colour=NULL;// Is it nice and clean???  JAN 2009 YES its OK
    m_RefAngle= m_TriArea=ON_UNSET_VALUE;
    PCF_DeleteFilamentList(this);
    for(int k=0;k<3;k++) {
        node[k]=0;
        m_iEd[k]=0;
        prestress[k]=0;
    }
}


int RX_FETri3::Tri3Print(FILE *ff)const
{
    assert(0);
    fprintf(ff,"\nRX_FETri3:%3d\n",GetN());
    fprintf(ff," InModel=%d uuid=<>\n" , IsInDatabase());

    return(0);
}
// fills in the FEA database entry of this tri
int RX_FETri3::SetProperties(void)
{
    int err=0;
#ifdef _DEBUG
    this->IsValid();
#endif

#ifdef FORTRANLINKED
    int ok=cf_putftriproperties(GetSail()->SailListIndex(),GetN(),
                                node,m_iEd,
                                m_RefAngle, m_TriArea,
                                prestress ,	   mx	,
                                creaseable,
                                this, & err);
    // m_refangle must be in Radians

#else
    assert(0); int ok=0;
#endif
    if(err>0 || !ok ) printf(" error(=%d),(ok=%d) setting tri %d\n",err,ok, GetN());

    return ok;
}
int RX_FETri3::SetPressure(const double p){
    int err=0;
#ifdef FORTRANLINKED
    int ok= cf_putpressure(GetSail()->SailListIndex(),GetN(), p,&err);
    return ok;
#else
    return 0;
#endif
}
double RX_FETri3::GetPressure() const{
    int err=0;
#ifdef FORTRANLINKED
    double rv= cf_getpressure (GetSail()->SailListIndex(),GetN(), &err);
    return rv;
#else
    return 0;
#endif

}
int RX_FETri3::IsUsed() const{
    int err=0;
#ifdef FORTRANLINKED
    int rv= cf_IsTriUsed (GetSail()->SailListIndex(),GetN(), &err);
    return rv;
#else
    return 0;
#endif

}

bool RX_FETri3::SetRefAngle(const ON_3dVector &matrefdir){ // matrefvector in Model space (Not element space).May be red or black
    cout<< "this is all wrong because matrefdir isnt always in the same red/black space as this->Trigraph( which is always in "<<endl;
    ON_Xform t = this->Trigraph();  // relative to the triangle baseline
#ifdef _DEBUG
    ON_3dVector b = this->baseVector();
    cout<< "TODO: check sense"<<endl;
#endif
    ON_3dVector mloc = t * matrefdir;
    if (mloc.LengthSquared() < ON_EPSILON){
        cout<< " TODO: ERROR: Zero matrefVector"<<endl;
        this->m_RefAngle = 0;
        return false;
    }
    double ang = -atan2(mloc.y,mloc.x);
    //	ang = fmod(ang + 4.* ON_PI, ON_PI);
    this->m_RefAngle = ang;

#ifdef _DEBUG
    ON_Xform test= MaterialTrigraph();
    ON_3dVector tv = test * matrefdir; // should be (1,0,0)
    cout << " should be Xaxis " << tv <<endl;

#endif

    return true;
}
ON_Xform RX_FETri3::MaterialTrigraph(){
    ON_Xform rc ,t = this->Trigraph();
    ON_3dVector zg, zl(0,0,1);
    zg = zl * t;
    rc.Rotation(sin(m_RefAngle),cos(m_RefAngle),zg,ON_3dPoint(0,0,0));

    rc = t * rc;
    return rc;

    // checking. Pre-multiplying ( v2 =V . X   )  converts V in XForm  to V2 in global..
#ifdef NEVER
    RXSitePt q;
    this->Centroid (&q);
    cout << "at "<< q.x << " "  << q.y << " " << q.z ;

    zg = zl * this->Trigraph();
    cout <<" elZ= " << zg.x << " "  << zg.y << " " << zg.z ;

    zg = zl * rc;
    cout <<" MatZ= "<< zg.x << " "  << zg.y << " " << zg.z;
    HC_Open_Segment("trig/Z");
    HC_Set_Color("lines=blue");
    HC_Insert_Line(q.x,q.y,q.z, q.x+zg.x,q.y+zg.y,q.z+zg.z);
    HC_Close_Segment();

    zg = ON_3dVector(1,0,0) * rc;
    cout <<" MatX= "<< zg.x << " "  << zg.y << " " << zg.z<< endl;
    HC_Open_Segment("trig/X");
    HC_Set_Color("lines=magenta");
    HC_Insert_Line(q.x,q.y,q.z, q.x+zg.x,q.y+zg.y,q.z+zg.z);
    HC_Close_Segment();

    zg = ON_3dVector(0,1,0) * rc;
    HC_Open_Segment("trig/Y");
    HC_Set_Color("lines=green");
    HC_Insert_Line(q.x,q.y,q.z, q.x+zg.x,q.y+zg.y,q.z+zg.z);
    HC_Close_Segment();

    return rc;
#endif
}


int RX_FETri3::Vertices(const RXSitePt *v[4])const{
    class RX_FESite* p;
    p = GetNode(0);     v[0]	= dynamic_cast<RXSitePt *> (p);
    p = GetNode(1);	v[1]	= dynamic_cast<RXSitePt *> (p);
    p = GetNode(2);	v[2]	= dynamic_cast<RXSitePt *> (p);

    v[3]=v[0];
    return 1;
}
void RX_FETri3::SetNode(const int k, class RX_FESite *const s){
    this->node [k] = s->GetN();
    //   august 2012 for models imported from nastran.  PCF_ISFIELDNODE  means that the node is part of the triangulation
    s->m_Site_Flags |=PCF_ISFIELDNODE;
}
class RX_FESite*  RX_FETri3::GetNode(const int k)const{
    return this->GetSail()->GetSiteByNumber(this->node[k]);
}
int RX_FETri3::AllFilamentStress(double eps0[3],double s[3],bool KillNegative){
    int rc=0;
    s[0]=s[1]=s[2]=0;
    // walk the filaments, adding up the stress due to each one.
    // later each filament may have a non-linear stress-strain curve
    // and later still there may be crimp interchange.
    // but initially we just want to kill off any negative filament stress
    // if the parameter KillNegative is true, unless the flament's SC has the
    // PCF_FIL_NO_BUCKLE set, which is done by the keyword '$nobuckle'

    double eps[3], t, slocal[3], ang;
    struct PC_FILAMENT_ON_TRI  *l_fptr;
    int k;
    sc_ptr sc;
    l_fptr = this->m_fils;
    while(l_fptr){ // expects theta to be in Radians.
        memcpy(eps,eps0,3*sizeof(double));
        sc = (sc_ptr )l_fptr->m_sco;
        eps[2] = eps[2]/2.0;
        ang = l_fptr->m_theta;
        RXPanel::rotate_stress(eps, ang);
        eps[2] = eps[2]*2.0;

        l_fptr->m_strain = eps[0];
        t = l_fptr->m_strain * l_fptr->m_EA/l_fptr->m_W  + l_fptr->m_Ti;
        if( ! KillNegative || sc->FlagQuery(PCF_FIL_NO_BUCKLE) || t > 0 ){
            rc++;
            slocal[0]= t;  slocal[1]= 0.; slocal[2]=0.;
            RXPanel::rotate_stress(slocal, -ang );
            for(k=0;k<3;k++) s[k]+=slocal[k];
        }
        l_fptr = l_fptr->next;

    } // while ...

    return rc;
}
int RX_FETri3::GetStressTensor(double*s)
{
    int rc=0;
    /* the stress tensor is sx, txy,0,txy,sy,0,0,0,0
The transformation is sigWorld = tT sigma t
See  SUBROUTINE PLOT_ref_direction  for the transformation
*/
    int sli = this->GetSail()->SailListIndex ();
    int L = this->GetN();
    int err=0;
    rc = cf_TriGlobalStressTensor (sli, L,s,  &err) ;
    return rc;
}

int RX_FETri3::GetStrainTensor(double*s)
{
    int rc=0;
    // the strain tensor is ex, gxy/2,0,gxy/2,ey,0,0,0,0
    int sli = this->GetSail()->SailListIndex ();
    int L = this->GetN();
    int err=0;
    rc = cf_TriGlobalStrainTensor (sli, L,s,  &err) ;
    return rc;
}

int RX_FETri3::GetStiffnessTensor(double*s )
{
    int rc=0;
    int sli = this->GetSail()->SailListIndex ();
    int L = this->GetN();
    int err=0;

    rc = cf_TriGlobalStiffnessTensor (sli, L,s,  &err) ;
    return rc;
}

int RX_FETri3::GetLocalAxis (double*s)
{
    int rc=0;
    int sli = this->GetSail()->SailListIndex ();
    int L = this->GetN();
    int err=0;

    rc = cf_TriGlobalAxisVector (sli, L,s,  &err) ;
    return rc;
}

int RX_FETri3::WriteStressTensor(ofstream &dst){
    int rc=0;
    double s[9];
    int sli = this->GetSail()->SailListIndex ();
    int L = this->GetN();
    int err=0;

    rc = cf_TriGlobalStressTensor (sli, L,s,  &err) ;
    dst << s[0]<<" "<<s[1] <<" "<<s[2] <<endl;
    dst << s[3]<<" "<<s[4] <<" "<<s[5] <<endl;
    dst << s[6]<<" "<<s[7] <<" "<<s[8] <<endl <<endl;

    return rc;
}
int RX_FETri3:: WriteStrainTensor(ofstream &dst){
    int rc=0;
    // the strain tensor is ex, gxy/2,0,gxy/2,ey,0,0,0,0
    double s[9];
    int sli = this->GetSail()->SailListIndex ();
    int L = this->GetN();
    int err=0;

    rc = cf_TriGlobalStrainTensor (sli, L,s,  &err) ;
    dst << s[0]<<" "<<s[1] <<" "<<s[2] <<endl;
    dst << s[3]<<" "<<s[4] <<" "<<s[5] <<endl;
    dst << s[6]<<" "<<s[7] <<" "<<s[8] <<endl <<endl;

    return rc;
}
int RX_FETri3:: WriteStiffnessTensor (ofstream &dst){
    int rc=0;
    double s[9];
    int sli = this->GetSail()->SailListIndex ();
    int L = this->GetN();
    int err=0;

    rc = cf_TriGlobalStiffnessTensor (sli, L,s,  &err) ;
    dst << s[0]<<" "<<s[1] <<" "<<s[2] <<endl;
    dst << s[3]<<" "<<s[4] <<" "<<s[5] <<endl;
    dst << s[6]<<" "<<s[7] <<" "<<s[8] <<endl <<endl;

    return rc;
}

int RX_FETri3::WriteLocalAxis   (ofstream &dst){
    int rc=0;
    double s[3];
    int sli = this->GetSail()->SailListIndex ();
    int L = this->GetN();
    int err=0;

    rc = cf_TriGlobalAxisVector (sli, L,s,  &err) ;
    dst << s[0]<<" "<<s[1] <<" "<<s[2] <<endl;
    return rc;
}

int RX_FETri3::GetStressResults(TriStressStruct *val) const{
    int sli = this->GetSail()->SailListIndex ();
    int L = this->GetN();
    if (!L ) return 0;

    cf_one_tri_stress_op(L,sli, (val->m_stress),(val->m_eps),(val->m_princ),&(val->m_cre),&(val->m_Princ_Stiff));
#ifdef RX_USE_DOVE
    double q[9] = {2.05e6,0,0,0,0,0,0,0,0};
    RXmaterialMatrix qply = RXmaterialMatrix(q);
    RXdovefile *theDove=0;
    dovestructure *theDoveStructure=0;
    double l_doveAngle=0;
    RXEntity_p doveEnt=0;
    TriLamDefn *tld;
    class RXLayer *lay;
    RXEntity_p matent=0;
    /// get the dove and its angle

    tld = this->m_laminae;
    for(int iii=0;iii<this->laycnt;iii++) {
        lay = tld[iii].Layptr;
        matent = lay->matptr;
        if(matent->TYPE==FIELD) {
            struct PC_FIELD *f = (struct PC_FIELD*)matent->dataptr;
            if(f->flag&DOVE_FIELD) {
                doveEnt = f->mat;
                theDove = (RXdovefile *) doveEnt->dataptr;
                theDoveStructure=theDove->m_amgdove;
                l_doveAngle = tld[iii].Angle ;
                if(!theDoveStructure->GetStackType())
                    theDoveStructure=0;
            }
        }
    }
    // end get dove
    RXmaterialMatrix l_mm = RXmaterialMatrix(this->mx);
    l_mm.ToParameters(qply,&val->m_thk,&val->ke,&val->kg,&val->nu);
    if(theDoveStructure) {// see  print_triangle_data to evaluate theDove
        double epsm[3];
        RXSitePt l_cs(0.,0.,0.,0.,0,0);
        this->Centroid(&l_cs);
        //				epsm[0] = val->eps[0]; 	epsm[1] = val->eps[1];
        //				epsm[2] = val->eps[2] /2.0;rotate_stress(epsm, l_doveAngle );epsm[2] = epsm[2]*2.0;
        //WRONG should use Dinv * stress to get epsm
        ON_Matrix l_onm = l_mm.GetMatrix(); l_onm.Invert(1e-10);
        ON_3dVector l_onstress = ON_3dVector(val->m_stress[0],val->m_stress[1],val->m_stress[2]);
        ON_Xform l_xf = ON_Xform(l_onm);
        ON_3dVector l_one = l_xf*l_onstress;
        epsm[0] = l_one[0]; 	epsm[1] = l_one[1];
        epsm[2] = l_one[2] /2.0;
        RXPanel::rotate_stress(epsm, l_doveAngle );
        epsm[2] = epsm[2]*2.0;

        theDoveStructure->PlyStrains(l_cs.m_u,l_cs.m_v,epsm,
                                     val->plyminX,(val->plymaxX),(val->plyminY),
                                     (val->plymaxY),(val->plymaxXY));
    }
#endif

    for (int k=0;k<3;k++) {
        if(isnan(val->m_stress[k])  )   val->m_stress[k]=0.;
        if(isnan(val->m_eps[k]))   val->m_eps[k]=0.;
        if(isnan(val->m_princ[k]))   val->m_princ[k]=0.;
        if(isnan(val->m_Princ_Stiff))  val->m_Princ_Stiff=0.;
    }

    return 1;
}// getstressresults


int RX_FETri3::IsValid() 
{
    int bad=0;
    for(int k=0;k<9;k++)
        bad +=  _isnan(mx[k]);
    bad +=  _isnan(m_TriArea);
    bad +=  _isnan( m_RefAngle); // the angle of the element base in its material axis system.

    for(int k=0;k<3;k++)
        bad +=  _isnan( prestress[k]);

    if(bad)
        wcout << L"Bad("<<bad<<L") "<<this->TellMeAbout() <<endl;
    return !bad;
}
RXSTRING  RX_FETri3::TellMeAbout() const
{
    RXSTRING r(_T(" FETri3  %d (%d %d %d)   "));
    r+= TOSTRING(this->GetN()) +_T(" \t");
    r+= TOSTRING(this->Node(0)) +_T(" \t");
    r+= TOSTRING(this->Node(1)) +_T(" \t");
    r+= TOSTRING(this->Node(2)) +_T(" \t area= ")+TOSTRING(m_TriArea) +_T(" refangle=") + TOSTRING(m_RefAngle  );
    return r;
}
