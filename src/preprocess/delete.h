 /* delete.h created by MSVC from delete.c  6/1/95 */
#ifndef _HDR_DELETE
#define  _HDR_DELETE  1

EXTERN_C int Resolve_Delete_Card(RXEntity_p e);
EXTERN_C int Delete_Patch(RXEntity_p p);
EXTERN_C int Free_Gcurve_List(sc_ptr sc);

#endif  //_HDR_DELETE
