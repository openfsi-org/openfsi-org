
#include "StdAfx.h"
#include "etypes.h"
#include "RXSitePt.h"
#include "RXSail.h"
#include "MTParserRegistrar.h"
#include "RXQuantity.h"

#include "RXScalarProperty.h"
// the keyword is "scalar"
RXScalarProperty::RXScalarProperty(void)
{
    assert("dont use default RXEntity constructors"==0);
}
RXScalarProperty::RXScalarProperty( class RXSail *const s)
    :RXEntity (s)
{
    RXENode* el = dynamic_cast<RXENode*>(s); assert(el);
    this->SetParent(el);
    //this->m_OwnerNode= s;
}
RXScalarProperty::RXScalarProperty(const RXSTRING &name,const RXSTRING &p_Buf,  class RXSail *const s) 
    :RXEntity (s)
{
    //this->m_OwnerNode = s;
}

RXScalarProperty::~RXScalarProperty(void)
{	this->CClear();
     //   assert(this->TYPE==PCE_DELETED);
}
int RXScalarProperty::Resolve(void)
{
    /* scalar definition is
 scalar 	 Name 	 (u+v)  [atts]! comment */

    vector<RXSTRING> words= rxparsestring( FromUtf8(this->GetLine()),RXSTRING( RXENTITYSEPS_W));
    if(words.size()<3) return 0;

    RXExpressionI *l_A ;
    l_A= this->AddExpression ( new RXQuantity(L"value",words[2],L"m",this->Esail ) ); this->SetRelationOf(l_A,aunt,RXO_EXP_OF_ENT); //done
    if(! l_A->Resolve()) {
        this->CClear();
        return 0; 	}

    this->SetRelationOf(dynamic_cast<RXObject*>( l_A),child|niece|frog,RXO_EXP_OF_ENT);
    if(words.size()>3) this->AttributeAdd(ToUtf8(words[3]).c_str());
    //#ifdef  DEBUG
    double v = this->ValueAt (RXSitePt(1.,2.,3.,0.5,0.5, -1)  );
    //#endif
    this->Needs_Resolving=false;
    return 1;
} 

double RXScalarProperty::ValueAt(const RXSitePt&p) const
{
    double rv;
    int rc=0;
    double u,v,x,y,z,qq;
    u = p.m_u;v=p.m_v;x=p.x;y=p.y;z=p.z;

    RXQuantity *e=0;
    { rv  =0;
        e=dynamic_cast< RXQuantity *>(this->FindExpression(L"value",rxexpLocal));
        if(e){

            if(e->getRegistrar()->getVarInfo(L"u")) {
                try	{
                    e->redefineVar(_T("u"),&u);
                    if(!ON_IsValid(u) ) cout <<"RXVectorField::ValueAt is using an invalid U value"<<endl;
                }
                catch( MTParserException  e )
                {
                } // its OK for these to be undefined
                catch(...) {cout<<"...U ";}
            }

            if(e->getRegistrar()->getVarInfo(L"v")) {
                try	{

                    e->redefineVar(_T("v"),&v);
                    if(!ON_IsValid(v) ) cout <<"RXVectorField::ValueAt is using an invalid V value"<<endl;
                }  catch( MTParserException  e ) {cout<<" ";}
                catch(...) {cout<<"...V ";}
            }
            if(e->getRegistrar()->getVarInfo(L"x"))   {
                try	{
                    if(e->getRegistrar()->isVarDefined(_T("x") ))
                        e->undefineVar(_T("x"));
                    e->defineVar(_T("x"),&x);
                    e->SetNeedsCompiling();
                }
                catch( MTParserException  e ) {cout<<"noX ";}
                catch(...) {cout<<"..X ";}
            }

            if(e->getRegistrar()->getVarInfo(L"y"))   {
                try	{
                    if(e->getRegistrar()->isVarDefined(_T("y") ))
                        e->undefineVar(_T("y"));
                    e->defineVar(_T("y"),&y);
                    e->SetNeedsCompiling();
                }
                catch( MTParserException  e )  {cout<<"noY ";}
                catch(...) {cout<<"..Y ";}
            }
            if(e->getRegistrar()->getVarInfo(L"z"))  {
                try	{
                    if(e->getRegistrar()->isVarDefined(_T("z") ))
                        e->undefineVar(_T("z"));
                    e->defineVar(_T("z"),&z);
                    e->SetNeedsCompiling();
                }
                catch( MTParserException  e )  {cout<<"noZ ";}
                catch(...)  {cout<<"..Z";}
            }
            qq= e->evaluate();
            rv =qq ;
            rc++;
        }
    }
    return rv;
} //RXScalarProperty::ValueAt
int RXScalarProperty::ReWriteLine(void) {
    //   MTSTRING  w;
    QString s, NewLine =  this->type();
    NewLine += RXENTITYSEPSTOWRITE;
    NewLine += this->name();
    NewLine += RXENTITYSEPSTOWRITE;
    s=  QString::fromStdString(this-> GetText()) ;

    NewLine += s;

    NewLine +=this->AttributeString(RXENTITYSEPSTOWRITE  );
    NewLine += " ! rewritten  ";
    this->SetLine( qPrintable(NewLine) );
    return NewLine.length();
}
int RXScalarProperty::Dump(FILE *fp) const{
    int rc=0;
    rc+= fprintf(fp,"Scalar Property  : %S\n", this->GetText().c_str());
    //rc += RXExpression::Dump(fp);
    return rc;
}

RXSTRING RXScalarProperty::TellMeAbout() const {
    return RXSTRING(L" Scalar Property :") + TOSTRING(this->GetText().c_str());
}

int  RXScalarProperty::CClear(){
    int rc=0;
    // clear stuff belonging to this object

    // call base class CClear();
    rc+=RXEntity::CClear ();
    //	rc+=RXExpression::CClear ();
    return rc;
}

int RXScalarProperty::Compute(void){
    return 0;
}

int RXScalarProperty::Finish(void){assert(0); return 0;}

std::string RXScalarProperty::GetText() const
{
    MTSTRING  w;
    std::string s;

    RXQuantity *e=0;

    e=dynamic_cast< RXQuantity *>(this->FindExpression(L"value",rxexpLocal));
    w=L"";
    if(e)
        w= e-> GetText();
    s = ToUtf8(w);

    return s;

}
