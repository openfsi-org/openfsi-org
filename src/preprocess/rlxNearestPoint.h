// NearestPoint.h: interface for the NearestPoint class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NEARESTPOINT_H__49F7B905_0899_427C_9898_72E12A4BBD98__INCLUDED_)
#define AFX_NEARESTPOINT_H__49F7B905_0899_427C_9898_72E12A4BBD98__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "RXUPoint.h"
#include "RXCurve.h"

#define RX_ERROR               0

#define RX_NOT_DROPPED		   -1
#define RX_DROP_OK             1
#define RX_DROP_AT_START       2
#define RX_DROP_AT_END         3

#define RX_DROP_NOT_CONVERGED  -2
#define RX_DROP_BUT_FAR         4

//Returns the number of iteration or 
//      -1 if failled to converge
//      -2 if the point is off the curve at the begin of the curve
//      -3 if the point is off the curve at the end of the curve
int  GetNearestPoint(const ON_Curve   * p_Curve,  // Curve to project the point on 
					 const ON_3dPoint & p_Point,  // Point to project
					 double     * p_tstart,  //if not NULL Starting point on p_Curve          
					 RXUPoint   & p_PointOUT, ///OUTPUT: a pointer on a curve + a parameter value
					 const double     & p_e,      // accuracy criteria
					 const double     & p_eEnd,	// if a the projected poin t is off the subdomain we restart the serach from p_eEnd near the end of the subdomain
					 const int        & p_kmax,
					 const RXTRObject* p_pPersistentCurve = NULL); //persitant curve from which the line is extracted 
																  //if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine


//!if a point on the curve is within the toelrences (p_Tolerence and p_CutDistance) set p_t to this absices.
/* 
	returns 1 if p_Point is in a tube of radius p_Tolerence around the curve 
	returns 2 if p_Point is in a sphere of radius p_CutDistance around the start of the curve 
	returns 3 if p_Point is in a sphere of radius p_CutDistance around the end of the curve 	
*/
int DropToCurve(const ON_Curve   * p_Curve, 
			    const ON_3dPoint & p_Point,
			    double     * p_tstart,  //if not NULL Starting point on p_Curve          
				RXUPoint   & p_PointOUT, ///OUTPUT: a pointer on a curve + a parameter value	 
				const double     & p_e,
			    const double     & p_eEnd,	// if a the projected poin t is off the subdomain we restart the serach from p_eEnd near the end of the subdomain
			    const int        & p_kmax, 
			    const double     & p_Tolerence,
				const double     & p_CutDistance,
				const RXTRObject* p_pPersistentCurve = NULL); //persitant curve from which the line is extracted 
																  //if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine
	
int GetNearestPoint(const rxON_PolyCurve   * p_Curve, 
					const ON_3dPoint & p_Point,
					double     * p_tstart,  //if not NULL Starting point on p_Curve          
					RXUPoint   & p_PointOUT, ///OUTPUT: a pointer on a curve + a parameter value 
					const double     & p_e,
					const double     & p_eEnd,	// if a the projected poin t is off the subdomain we restart the serach from p_eEnd near the end of the subdomain
					const int        & p_kmax, //Curve related to p_t to evaluate 
					const RXTRObject* p_pPersistentCurve = NULL); //persitant curve from which the line is extracted 
																  //if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine
	

//
//int GetNearestPoint_LinearMethod_ssd(const ON_NurbsCurve   * p_Curve, 
//					const ON_3dPoint & p_Point,
//					double     * p_tstart,  //if not NULL Starting point on p_Curve          
//					RXUPoint   & p_PointOUT, ///OUTPUT: a pointer on a curve + a parameter value 
//					const double     & p_e,
//					const double     & p_c,
//					const int        & p_cid,
//					const int        & p_kmax,
//					const RXTRObject* p_pPersistentCurve = NULL); //persitant curve from which the line is extracted 
//															  //if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine


//find the nearest point of a point on a curve using a 3rd oder algorithm
int GetNearestPoint(const ON_NurbsCurve   * p_Curve, 
					const ON_3dPoint & p_Point,
					double			 * p_tstart,   //Starting point on p_Curve          
					RXUPoint   & p_PointOUT, ///OUTPUT: a pointer on a curve + a parameter value 
					const double     & p_eDot,     //stop criteria to stop to test is the project point is near enougth from the perpendicular 
					const double     & p_eEnd,	   // if a the projected poin t is off the subdomain we restart the serach from p_eEnd near the end of the subdomain
					const int        & p_kmax,    // to stop after p_kmax iteration
					const RXTRObject* p_pPersistentCurve = NULL); //persitant curve from which the line is extracted 
																  //if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine
	

int NearestPointCubic(const ON_NurbsCurve  * p_Curve, 
					const ON_3dPoint & p_Point,
					double			 * p_tstart,  //Starting point on p_Curve          
					RXUPoint   & p_PointOUT, ///OUTPUT: a pointer on a curve + a parameter value 
					const double     & p_e,
					const RXTRObject* p_pPersistentCurve = NULL); //persitant curve from which the line is extracted 
																  //if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine
	

int GetNearestPoint(const ON_LineCurve  * p_Curve, 
					const ON_3dPoint & p_Point,
					double      *  p_tstart, //if not NULL Starting point on p_Curve          
					RXUPoint   & p_PointOUT, ///OUTPUT: a pointer on a curve + a parameter value 
					const RXTRObject* p_pPersistentCurve = NULL); //persitant curve from which the line is extracted 
																  //if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine

int GetNearestPoint(const ON_PolylineCurve * p_Curve, 
					const ON_3dPoint & p_Point,
					double     * p_tstart,  //if not NULL Starting point on p_Curve          
					RXUPoint   & p_PointOUT, ///OUTPUT: a pointer on a curve + a parameter value 
					const RXTRObject* p_pPersistentCurve = NULL); //persitant curve from which the line is extracted 
																  //if not NULL the RXUpoint will be set to this curve else it will be set to p_pLine

int GetParamAt(const ON_Curve * p_curve, 
			   const double & p_length, 
			   double & p_t,
			   double fractional_tolerance = 1.0e-8);

int GetParamAtPolyline(const ON_PolylineCurve * p_crv, 
					   const double & p_length, 
					   double & p_t);
	
#endif // !defined(AFX_NEARESTPOINT_H__49F7B905_0899_427C_9898_72E12A4BBD98__INCLUDED_)
