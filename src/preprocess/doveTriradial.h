#ifndef _DOVE_TRIRADIAL_H_
#define _DOVE_TRIRADIAL_H_
#include "doveforstack.h"
#include "RXON_Matrix.h"

class doveTriradial :
	public doveforstack
{
public:
	doveTriradial(void);
	doveTriradial(doveforstack *p_d, RXON_Matrix &p_c,HC_KEY p_rootkey);
//	doveTriradial(const int i);
public:
	virtual ~doveTriradial(void);

	int SetDoldrum(char * p_str); 
	int GetWeights(double & p_w1,double & p_w2,double & p_w3); 

	int UserEditWeights();
	int UserEditFocus();
	void SetMould(const ON_Surface *p);
	ON_String Serialize();
	char * Deserialize(char *p_lp) ;
	double GetAlpha(const double pu, const double pv) ;
	int Print(FILE *fp);
	ON_3dPoint  WeightedCentroid();

protected:
	ON_SimpleArray<double>  xy_to_lambda(const ON_3dPoint &p) const;
//	int CalcBlueOffAxisMatrix( double pu,double pv,double thk,ON_Matrix&p_m);
	double TransverseAngleDerivative(const double pu, const double pv);
	// // 3 points in UV space with their weights
	RXON_Matrix m_c_uv; // to take advantage of existing serialize code.
private:
	ON_3dPointArray C; //  corner points.
	double m_W[3];		// weights (need not sum to unity)
};
#endif
