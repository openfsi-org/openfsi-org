/* slowcut.h */
#ifndef _SLOWCUT_H_
#define _SLOWCUT_H_

#include "griddefs.h"

// constants to define  how Segment_IntersectWithExtensions deals with 'past the ends'
#define  EXT_1_L	1
#define  EXT_1_R	2 
#define  EXT_2_L	4 
#define  EXT_2_R	8 

#define EXT_3D		256
#define EXT_2D		128

#define EXT_ALL	(int)(EXT_1_L+EXT_1_R+EXT_2_L+EXT_2_R)


EXTERN_C double PCC_Angle_Between_Accurate(VECTOR *a1,VECTOR*b1, VECTOR *a2,VECTOR*b2,int*err);
int  Segment_IntersectWithExtensions(const VECTOR *a1,const VECTOR*b1,
									 const  ON_3dPoint *a2,const ON_3dPoint*b2,
									double*s1,double*s2,int flags);

int Segment_Intersect(VECTOR *a1,VECTOR*b1, VECTOR *a2,VECTOR*b2,double*s1,double*s2,double tol) ;

//!Find all the inetersection beween 2 polylines.
/*!
p_e,  //NOT USED  \n
p_p1, p_c1, p_ds1, // Define the first polyline (INPUT), p_p1 arry of vector, p_c1 nb of points,  p_ds1 slope (I guess Thomas 07 07 04)    \n
p_p2, p_c2, p_ds2, // Define the 2nd   polyline (INPUT), p_p2 arry of vector, p_c2 nb of points,  p_ds2 slope (I guess Thomas 07 07 04)    \n
p_ss1, p_ss2, p_nc, //Define the interesections (OUTPUT), p_ss1 position of the interesections on polyline 1, parameter 0<<1               \n
														, p_ss2 position of the interesections on polyline 2, parameter 0<<1	           \n
														, p_nc number of intersections													   \n
*/
int SlowCut(
			const VECTOR*p_p1, int p_c1, const double *p_ds1, 
			const VECTOR*p_p2, int p_c2, const double *p_ds2,  
			double**p_ss1, double**p_ss2, int*p_nc, double p_tol);

int Box_Intersection(VECTOR *a1,VECTOR*b1, VECTOR *a2,VECTOR*b2 );
int Cross_Already(RXEntity_p e, double s1, RXEntity_p p, double s2, RXEntity_p *,RXEntity_p *, double tol ) ;

#endif //#ifndef _SLOWCUT_H_
