// dovecheckerboard.h: interface for the dovecheckerboard class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DOVECHECKERBOARD_H__2DE54E0B_C3D2_4C5B_AF64_473945AFFEC4__INCLUDED_)
#define AFX_DOVECHECKERBOARD_H__2DE54E0B_C3D2_4C5B_AF64_473945AFFEC4__INCLUDED_

//#include "opennurbs.h"	// Added by ClassView (but removed by PH for V4
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class dovecheckerboard  
{
public:
	int SetFlagByUV(const ON_2dPoint puv1,const ON_2dPoint puv2);
	ON_2dPoint CellCentroidUV(const int r, const int c);
	void FlagClear(const int r, const int c);
	void FlagSet(const int r, const int c);
	int CellFlag(const int r, const int c);
	dovecheckerboard();
	dovecheckerboard(const int nr, const int nc);
	virtual ~dovecheckerboard();

protected:





protected:
	int CellIntersects(const int r, const int c, const ON_Line &p);


	int m_nr, m_nc;
	int *m_flags;

private:
	int CellIndex(const int r, const int c);
};

#endif // !defined(AFX_DOVECHECKERBOARD_H__2DE54E0B_C3D2_4C5B_AF64_473945AFFEC4__INCLUDED_)
