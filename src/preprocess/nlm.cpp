/* this is nlm.c
  5 jan 2005 safer handling of invalid tris.
   8/10/98  some tiny speed optimisations
  10/1/96  A) ETYPES
  B) Shear_Correction
    18/8/95  new routine Get_Tri_Ref_Angle for use in DK routines

   1/3/95  PH if (use_me_As _Ref dont rotate stress as theta MUST be zero
   PH
   AKM
   started 16/11/94
   AKM 17.11.94   added get next material_stress which is called from fortran
   
   The C routines for Non Linear Materials
   */

#include "StdAfx.h"
#include "RXSail.h"
#include "RXEntityDefault.h"
#include "RXSRelSite.h"
#include "RX_FETri3.h"
#include "RXLayer.h"
#include "RXQuantity.h"

#include "fielddef.h"
#include "offset.h"
#include "etypes.h"
#ifdef HOOPS
#define USE_DOVE
#include "doveforstack.h"
#include "RXdovefile.h"
#endif
#include "global_declarations.h" 
#include "cfromf.h" 
#include "nlm.h"


/* above is prototype for fortran version */

void Polyline_Interp_clanguage(double* x,double*y,int*N,int*s,double *x0,double*yret,double *grad,int*err)
{

    double dx,fac;
    cout<< " shouldnt use THIS Polyline _Interp"<<endl;
    for( *s = 0;*s < *N-1;(*s)++) {
        if((*s == *N-2) || (x[*s+1] >(*x0)) ) {
            dx =  x[*s+1] - x[*s];
            if( fabs(dx) < 1.0E-12) {
                *yret = 0.5* (y [*s+1] + y[*s]);
                *grad =   (y [*s+1] - y[*s])/dx;
                *err = 0;
                return;
            }
            fac =  (*x0-x[*s])/ dx;
            *yret = y [*s+1]*fac + y[*s] * (1.0-fac)	;
            *grad = (y [*s+1] - y[*s])/dx;
            *err = 0;
            return;
        }
    }
    *err = 1;
}//void Polyline_Interp

//#endif
///////////the call is......
//       flag = 2  
//      	DO WHILE(flag .NE. 0)
//	      	CALL Get_Next_Material_Stress(t%CtriPtr,eps,IForce_Linear,slr,flag)
//		    stress=stress+slr
//      	ENDDO
// so flag input >1 means starting

// we shouldnt ever go here with a DOve. but if we do, we should return safely

void Get_Next_Material_Stress(class RX_FETri3**t,double *eg,
                              int *p_Force_Linear,double *slr,int *p_flag)
{
    /* n - triangle no. of triangle in question
   * eg - strain 1,2,3
   * Force_Linear - 1 for true
   * flag - returns > 0 if another material exists , =0 if not
   */
    TriLamDefn *tld=NULL;
    RXEntity_p matEnt;
    struct PC_MATERIAL *theMat=0;
    double e[3],sx_dash_e1,sx_dash_e2,sx_e1,sx_e2;
    double txy_e3,grad;
    double m=0.0,n=0.0,m2=0.0,n2=0.0,TwoMN=0.0, x,nuxy,nuyx,sl[3],egTwoMN;
    int seedx,seedy,seeds;
    int  errVal=0, atrino=-99; //ks,
    struct PC_FIELD *field;
    bool IsFilamentLayer=false;

    if(*p_flag >1) // means we are starting this element
        (*t)->m_CurrentLayer=0;

    if((*t)->laycnt > (*t)->m_CurrentLayer) {
        tld = &((*t)->m_laminae[(*t)->m_CurrentLayer]);
    }
    else {
        char buf[256];
        sprintf(buf," (GNMS) no laminae found in triangle %d",(*t)->GetN());
        //	(*t)->GetSail()->OutputToClient( buf,1);
        *p_flag = 0;
        return;
    }
    if(!tld)
    { /* (*t)->GetSail()->OutputToClient("no tld in element",1);   */  	*p_flag = 0;  	return;  }
    if(!tld->Layptr)
    { (*t)->GetSail()->OutputToClient("no layptr in element",1);     	*p_flag = 0;  	return;  }
    if(!tld->Layptr->matptr)
    { (*t)->GetSail()->OutputToClient("no matptr in element",1);     	*p_flag = 0;  	return;  }
    matEnt = tld->Layptr->matptr;  assert(matEnt->dataptr);

    if(tld->Layptr->Use_Me_As_Ref) {
        /*  FIRST it cant be a field
    SECOND  the angle will be zero   */
        theMat = (struct PC_MATERIAL *) matEnt->dataptr;
    }
    else {
        switch (matEnt->TYPE) {
        case FABRIC:
            theMat = (struct PC_MATERIAL *) matEnt->dataptr;
            break;
        case FIELD: // dove too
            field = (PC_FIELD *)matEnt->dataptr;
            IsFilamentLayer =  (field->flag&FILAMENT_FIELD);
            if(( field->flag&DOVE_FIELD) || ( field->flag&FILAMENT_FIELD)  ){
                theMat=NULL;
                sl[0] = sl[1] = sl[2] = 0.0;
            }
            else
                theMat = (struct PC_MATERIAL *) field->mat->dataptr;
            break;
        case PCE_DOVE:
            assert(0); // the logic is wrong. we never get here.
            cout<< " WRONG LOGIC Get Next Material Stress DOVE"<<endl;
            theMat = NULL;
            sl[0] = sl[1] = sl[2] = 0.0;
            break;
        default:
            assert(0);
        };
    }
    if(tld->Thickness < 1.0e-12) {
        slr[0] = slr[1] = slr[2] = 0.0;
    }
    else {

        if ( tld->Layptr->Use_Me_As_Ref || IsFilamentLayer ) { // filaments are stored in element axes not material axes
            e[0] = eg[0]; e[1]=eg[1];e[2]=eg[2];
        }
        else {
            m = cos(tld->Angle);
            n = sin(tld->Angle);
            m2 = m*m;
            n2 =  n * n; // 1.0 - m2; /* n*n; */
            TwoMN =  m * n;
            egTwoMN = eg[2] * TwoMN;
            TwoMN = 2.0 * TwoMN ;

            e[0] = eg[0] * m2 + eg[1] * n2 + egTwoMN ; /* eg[2] * m * n; */
            e[1] = eg[0] * n2 + eg[1] * m2 - egTwoMN; /*  eg[2] * m * n; */
            e[2] = TwoMN * (eg[1] - eg[0]) + eg[2] * (m2 - n2);
        }
        if(theMat) {
            if(IsFilamentLayer) {
                bool KillNegative = ((*p_Force_Linear )==0);
                (*t)->AllFilamentStress(e,sl,KillNegative);
            }
            else
                if((theMat->linear == 0) && ((*p_Force_Linear) == 0)) {
                    Polyline_Interp(theMat->en,theMat->fn,&(theMat->npn),&seedx,&(e[0]),&sx_e1,&sx_dash_e1,&errVal);
                    // ks = seedx-1;
                    Polyline_Interp(theMat->en,theMat->fn,&(theMat->npn),&seedy,&(e[1]),&sx_e2,&sx_dash_e2,&errVal);
                    //ks = seedy-1;
                    x = fabs(e[2]);

                    Polyline_Interp(theMat->es,theMat->fs,&(theMat->npn),&seeds,&x,&txy_e3,&grad,&errVal);
                    if(e[2] < 0.0)
                        txy_e3 =  -txy_e3 ;
                    double nu = matEnt->FindExpression (L"nu",rxexpLocal)->evaluate();
                    nuxy = nu * sx_dash_e1;
                    nuyx = theMat->zk * nu * sx_dash_e2;

                    /* constitutive eqns */
                    sl[0] = sx_e1 + theMat->zk * sx_e2*nuxy;
                    sl[1] = sx_e1 * nuyx + theMat->zk * sx_e2;
                    sl[2] = txy_e3;
                }
                else {
                    if(theMat->creaseable && relaxflagcommon.Debug2) {
                        int state = cf_GetWrinkledStress(e, theMat->d, sl);
                    }
                    else { //no wrinkling
                        sl[0] = theMat->d[0][0] * e[0] + theMat->d[0][1] * e[1] + theMat->d[0][2] * e[2];
                        sl[1] = theMat->d[1][0] * e[0] + theMat->d[1][1] * e[1] + theMat->d[1][2] * e[2];
                        sl[2] = theMat->d[2][0] * e[0] + theMat->d[2][1] * e[1] + theMat->d[2][2] * e[2];
                    }
                }
        } // if theMat
        /* convert to tri mat ref direction */
        if ( tld->Layptr->Use_Me_As_Ref || IsFilamentLayer) {
            slr[0] = sl[0] * tld->Thickness;
            slr[1] = sl[1] * tld->Thickness;
            slr[2] = sl[2] * tld->Thickness;
        }
        else {
            slr[0] = ( sl[0] * m2 + sl[1] * n2 - sl[2] * TwoMN ) * tld->Thickness;
            slr[1] = ( sl[0] * n2 + sl[1] * m2 + sl[2] * TwoMN ) * tld->Thickness;
            slr[2] = ( m*n*(sl[0] - sl[1]) + sl[2] * (m2 - n2) ) * tld->Thickness;
        }
    } // end of if finite thickness
    ((*t)->m_CurrentLayer)++;
    if((*t)->m_CurrentLayer < (*t)->laycnt)
        *p_flag = 1;
    else {
        *p_flag = 0;
        (*t)->m_CurrentLayer = 0;
    }

    return;
}//void Get_Next_Material_Stress

/*
#define PI  3.14159265358979323846


int print_triangle_data(FILE *fp,FortranTriangle *te,int ftrino,SAIL *p_sail) {
    TriLamDefn *tld;
    class RXLayer *lay;
    RXEntity_p matent=0;
    char text[256];
    int sli = p_sail->SailListIndex ();
#ifdef USE_DOVE
    RXdovefile *theDove=0;
    RXEntity_p doveEnt=0;
    dovestructure *theDoveStructure=0;
#endif
    Site cs;
    int i,j;
    double l_doveAngle=0;

    double stress[3];
    double eps[3];
    double princ[3];

    if(!fp)
        fp = stdout;
    if(!te)
        return(0);
    tld = te->m_laminae;

    fprintf(fp,"Tri %d: nodes %d,%d,%d : edges %d,%d,%d :area %f\n",te->GetN(),
            te->Node(0),te->Node(1),te->Node(2),
            te->m_iEd[0],te->m_iEd[1],te->m_iEd[2],
            te->m_TriArea);
    fprintf(fp,"\t Layers: %d\t refangle =%12.5f : creasable=%d \nC matrix\n",te->laycnt, (te->RefAngle())*180/PI,te->creaseable);
    for(i=0,j=0;i<3;i++,j+=3) {
        fprintf(fp," \t%f \t %f \t %f \n", te->mx[j],te->mx[j+1],te->mx[j+2]);
    }
    fprintf(fp,"prestress \t%f \t %f \t %f \n", te->prestress[0], te->prestress[1], te->prestress[2]);

    te->Centroid(&cs);
    fprintf(fp,"Centroid x=\t%f\ty=\t%f\tz=\t%f\tu=\t%f\t v=\t %f\n ",cs.x,cs.y,cs.z, cs.m_u,cs.m_v);

    fprintf(fp,"no. text, use_me, use_black, matent, angle, thick\n");

    for(i=0;i<te->laycnt;i++) {
        lay = tld[i].Layptr;
        matent = lay->matptr;
        if(lay->text)
            strcpy(text,lay->text);
        else
            strcpy(text,"empty");

        fprintf(fp," %d : '%s', %d, %d, '%s' ('%s'),%12.5f, %12.5f\n",i,text,lay->Use_Me_As_Ref,lay->Use_Black_Space,
                matent->type(),matent->name(),tld[i].Angle *180/PI,tld[i].Thickness);
        if(matent->TYPE==FIELD) {
#ifdef USE_DOVE
            struct PC_FIELD *f = (struct PC_FIELD*)matent->dataptr;
            if(f->flag&DOVE_FIELD) {
                doveEnt = f->mat;
                theDove = (RXdovefile *) doveEnt->dataptr;
                theDoveStructure=theDove->m_amgdove;
                l_doveAngle = tld[i].Angle ;
                if(!theDoveStructure->GetStackType())
                    theDoveStructure=0;

            }
#endif
        }
    }
    if(ftrino <=0) return 1;

    i = ftrino;//  te->n + offsets_.nts[m]-1;
    fprintf(fp,"Global number () %d\n", i);
#ifdef linux
    assert(0);
#endif
#ifdef NEVER
    if(tricommon_.use_tri[i-1] == 1) {
        //	dp = &(tricommon_.dstf[i-1][0][0] );
        //	fprintf(fp," dstfold \n");
        // 	for(k=0;k<3;k++) {
        // 		fprintf(fp,"\t%f \t %f \t %f\n",tricommon_.dstf[i-1][k][0],tricommon_.dstf[i-1][k][1],tricommon_.dstf[i-1][k][2]);
        //	 }

        cf_one_tri_stress_op(i,sli,stress,eps,princ,&cre, &Princ_Stiff);

        fprintf(fp,"Pressure      \t %f\n",tricommon_.prest[i-1]);

        fprintf(fp,"stress        \t %f \t %f \t %f\n", stress[0],stress[1],stress[2]);
        fprintf(fp,"strain        \t %f \t %f \t %f\n" ,eps[0],eps[1],eps[2]);
        fprintf(fp,"Pstress       \t %f \t %f \t %f\n" ,princ[0],princ[1],princ[2]);
        fprintf(fp,"crease        \t %f \nUpPr Stiffness\t %f \n" ,cre,Princ_Stiff);

        if(theDoveStructure&&theDove && theDove->m_amgdove){ //redundant
            double epsm[3];//  = inverse of dstf times stress
            double  pxn,pxx,pyn,pyx,pxy;
            double *l_dstf = &( tricommon_.dstf[i-1][0][0]);
            ON_Matrix m = ON_Matrix(4,4);
            for(j=0;j<3;j++) {
                for(k=0;k<3;k++, l_dstf++) {
                    m[j][k] = *l_dstf;
                }
            }
            m.Invert (1e-8);
            ON_Xform xf = ON_Xform(m);
            ON_3dVector str = ON_3dVector(stress[0],stress[1],stress[2]);
            ON_3dVector e = xf*str;
            epsm[0] = e.x; 		epsm[1] = e.y; 		epsm[2] = e.z;
            fprintf(fp,"has dove\nmatstrain        \t %f \t %f \t %f\tangle\t%f\n" ,epsm[0],epsm[1],epsm[2],l_doveAngle);
            fprintf(fp,"IS MAtstrAIN THE SAME AS STRAIN?? IT SHOULD BE??\n");


            epsm[2] = epsm[2]/2.0; RXPanel::rotate_stress(epsm, l_doveAngle );epsm[2] = epsm[2]*2.0;

            theDoveStructure->PlyStrains(cs.m_u,cs.m_v,epsm,  pxn,pxx,pyn,pyx,pxy);
            fprintf(fp," ply strain summary  xmin x max ymin ymax xymax\n %f\t %f\t %f\t %f\t%f\n", pxn,pxx,pyn,pyx,pxy);
            theDoveStructure->PrintLayerStrain(fp, cs.m_u, cs.m_v, epsm);
        }

    }
    else
        printf("(Get_Triangle_Results) tri %d has unUsed flag\n",i);
#endif
    return(1);
}
*/










