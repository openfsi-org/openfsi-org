#ifndef _GETSHELL_H_
#define  _GETSHELL_H_

#include "vectors.h"
//#include "off set.h"
#error(" dont use  getshell.h")
class RXSail;
HC_KEY DrawArrow(float ox,float oy,float oz,float dx,float dy,float dz,
			   VECTOR *norm,float L);

EXTERN_C int Get_Shell_Data_Geom(class RXSail *s,VECTOR **pts,int **flist,int *pcount,int *fcount);
EXTERN_C int Set_Shell_Data_Stress(class RXSail *s,HC_KEY theShell,int what,TriStressStruct *tridata,const int simple);

EXTERN_C int Get_Triangle_Results(class RXSail *s,TriStressStruct **Tridata);
#endif  //#ifndef _GETSHELL_H_

