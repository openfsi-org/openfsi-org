#include "StdAfx.h"
/*
Method 'flatten' is Peter's implementation of   eurographics 2002  vol 21(2002), number 2
intrinsic parameterizations of surface meshes
Desbrun, Meyer & Alliez

one change: we non-dimensionalize Mchi by average edge length (squared)

This seems accurate - but not very nice - for lambda >>mu (
(lambda is multiplier of Ma, Mu is multiplier of Mchi. 


*/
#include "rxON_Extensions.h"
#include <QDebug>
#include "RXSail.h"
#include "RXSparseMatrix.h"
#include "RX_FESite.h"
#include "RX_FEEdge.h"
#include "RXException.h"
#include "RXSRelSite.h"
#include "RXMesh.h"
#include "RXSeamcurve.h"
#include "rxqtdialogs.h"
triangle ptr;                         /* Temporary variable used by sym(). (YUCH!) */

#define WRITE_MEYER false

RXMesh::RXMesh(void)
    :
      m_lambda(0.5),
      m_t(NULL)
    , m_m(NULL)
    , m_b(NULL)
    ,m_MeshSpace(NOMESHSPACE)
    ,m_onm(0)
{
    Init();
}

RXMesh::~RXMesh(void)
{
    if(m_t) {
        trifreeIOstruct(*m_t );
        delete m_t;
    }
    if(m_m&&m_b)
        triangledeinit(m_m, m_b);
    if(m_m)
        delete m_m;
    if(m_b)
        delete m_b;
}

void RXMesh::Init(){
    if(m_t) {
        trifreeIOstruct(*m_t );
        delete m_t;
    }

    m_t = new struct triangulateio;
    memset(m_t,0,sizeof(struct triangulateio  ));
    if(m_m&&m_b)
        triangledeinit(m_m, m_b);
    if(m_m)
        delete m_m;
    if(m_b)
        delete m_b;
    m_m=0; m_b=0;
    ShewkIndex.clear(); NodeNo.clear();
    m_searchtri.orient=0;
    m_searchtri.tri=0;
}
/* see eurographics 2002  vol 21(2002), number 2
intrinsic parameterizations of surface meshes
Desbrun, Meyer & Alliez
EXCEPT that we non-dimensionalize the chi matrix by the average edge length squared.
COuld use area but anything with units (Length^2) is OK
*/
int RXMesh::MakeMeyerMatrices(vector<RX_FESite* > &pts3d, map<pair<int,int>,double> &ma,map<pair<int,int>,double> &mchi )
{
    struct otri triangleloop;
    vertex p1, p2, p3;
    long elementnumber;
    int i1, i2, i3;
    int r1, r2, r3;
    int np, bwl, bwu ; // half-bandwidths.
    pair<int,int> ij;
    double L1,L2,L3, L1sq,L2sq,L3sq,CotA1,CotA2,CotA3, c,SumLsq,nlsq;
    SumLsq=0.; nlsq=0.;
    map<int,int>::iterator norev = NodeNo.end();

    if (m_b->order != 1)
        return 0;
    np=  bwl =  this->m_t->numberofpoints; // (int)pts3d.size(); bwu=0;
    elementnumber = m_b->firstnumber;
    // first the off-diagonal terms beta and gamma by walking the triangles twice. Once for beta,gamma, then alpha,delta
    // NOTE that we use the identity Cot[Arcos[x]] === x/sqrt(1-x^2)

    traversalinit(&m_m->triangles);
    triangleloop.tri = triangletraverse(m_m);
    triangleloop.orient = 0;

    while (triangleloop.tri != (triangle *) NULL) {
        org(triangleloop, p1);  	dest(triangleloop, p2); 	apex(triangleloop, p3);
        r1 =  vertexmark(p1);    	r2 = vertexmark(p2);    	r3 = vertexmark(p3);
        if(norev==NodeNo.find(r1) )
            return 0;
        if(norev==NodeNo.find(r2) )
            return 0;
        if(norev==NodeNo.find(r3) )
        { qDebug()<<"no NodeNo for " << *p3<<*(p3+1)<<",vertexmark returns "<<r3 ;return 0;}
        i1 = NodeNo[r1];    		i2 = NodeNo[r2];    		i3 = NodeNo[r3];    // global nodenumbers

        RX_FESite*v1=pts3d.at(i1); 	RX_FESite*v2=pts3d.at(i2); 	RX_FESite*v3=pts3d.at(i3);
        v1->m_Site_Flags = v1->m_Site_Flags |PCF_ISMESHSITE;
        v2->m_Site_Flags = v2->m_Site_Flags |PCF_ISMESHSITE;
        v3->m_Site_Flags = v3->m_Site_Flags |PCF_ISMESHSITE;

        L1sq = (*v2-*v3).LengthSquared(); L2sq = (*v3-*v1).LengthSquared(); L3sq = (*v1-*v2).LengthSquared();
        L1= sqrt(L1sq);  	L2=sqrt(L2sq) ; 	L3= sqrt(L3sq);

        c= (L2sq + L3sq - L1sq)/(2.0*L2*L3);    	CotA1 =c/sqrt(1.-c*c);
        c= (L3sq + L1sq - L2sq)/(2.0*L3*L1);		CotA2 =c/sqrt(1.-c*c);
        c= (L1sq + L2sq - L3sq)/(2.0*L1*L2);		CotA3 =c/sqrt(1.-c*c);
        SumLsq+=(L1sq+L2sq+L3sq ); nlsq+=3.;
        ij.first =r3; ij.second =r2;
        ma[ij] = CotA1;	mchi[ij] = CotA2/L1sq;   bwl = min(bwl,r2-r3);  bwu = max(bwu,r2-r3);
        ij.first =r1; ij.second =r3;
        ma[ij] = CotA2;	mchi[ij] = CotA3/L2sq;   bwl = min(bwl,r3-r1);  bwu = max(bwu,r3-r1);
        ij.first =r2; ij.second =r1;
        ma[ij] = CotA3;	mchi[ij] = CotA1/L3sq;   bwl = min(bwl,r1-r2);  bwu = max(bwu,r1-i2);
        triangleloop.tri = triangletraverse(m_m);
        elementnumber++;
    }

    // now walk the triangles again and add components due to alpha and delta into the matrices
    traversalinit(&m_m->triangles);
    triangleloop.tri = triangletraverse(m_m);
    triangleloop.orient = 0;

    while (triangleloop.tri != (triangle *) NULL) {
        org(triangleloop, p1);  	dest(triangleloop, p2); 	apex(triangleloop, p3);
        r1 =  vertexmark(p1);    	r2 = vertexmark(p2);    	r3 = vertexmark(p3);
        i1 = NodeNo[r1];    		i2 = NodeNo[r2];    		i3 = NodeNo[r3];

        RX_FESite*v1=pts3d.at(i1); 	RX_FESite*v2=pts3d.at(i2); 	RX_FESite*v3=pts3d.at(i3);

        L1sq = (*v2-*v3).LengthSquared(); L2sq = (*v3-*v1).LengthSquared(); L3sq = (*v1-*v2).LengthSquared();
        L1= sqrt(L1sq);  	L2=sqrt(L2sq) ; 	L3= sqrt(L3sq);

        c= (L2sq + L3sq - L1sq)/(2.0*L2*L3);    	CotA1 =c/sqrt(1.-c*c);
        c= (L3sq + L1sq - L2sq)/(2.0*L3*L1);		CotA2 =c/sqrt(1.-c*c);
        c= (L1sq + L2sq - L3sq)/(2.0*L1*L2);		CotA3 =c/sqrt(1.-c*c);

        ij.first =r3; ij.second =r1;
        ma[ij] += CotA2;	mchi[ij] += CotA1/L2sq;   bwl = min(bwl,r1-r3);  bwu = max(bwu,r1-r3);
        ij.first =r1; ij.second =r2;
        ma[ij] += CotA3;	mchi[ij] += CotA2/L3sq;   bwl = min(bwl,r2-r1);  bwu = max(bwu,r2-r1);
        ij.first =r2; ij.second =r3;
        ma[ij] += CotA1;	mchi[ij] += CotA3/L1sq;   bwl = min(bwl,r3-r2);  bwu = max(bwu,r3-r2);

        triangleloop.tri = triangletraverse(m_m);
    }

    if(!elementnumber)
    { cout<< "no elements"<<endl; return 0;}
    // zero the matrix if (ij) is an edge
    int i;//, neighbor  ;
    traversalinit(&m_m->triangles);
    triangleloop.tri = triangletraverse(m_m);

#define ITP(s,i,it) ;// qDebug()<<s<<i<<it->first.first<<it->first.second<< it->second;
    // now the diagonal terms which are minus the sum of the off-diag terms
    // we should be traversing the row space which is usually < np
    map<pair<int,int>,double>::iterator lo,hi,it;
    double sum;
    int JuneTest = 0; //qDebug()<<" Junetest 1 SHOULD SPAN OVER RANGE OF ROWNOS" ;
    for(i=0+JuneTest;i<np;i++) {
        //       lo = ma.lower_bound (pair<int,int>(i-1,np+1));ITP("lo",i,lo);
        //       hi = ma.upper_bound (pair<int,int>(i+1,0)); ITP("hi",i,hi);
        lo = ma.lower_bound (pair<int,int>(i,0));ITP("lo",i,lo);
        hi = ma.upper_bound (pair<int,int>(i+1,-1)); ITP("hi",i,hi);
        sum=0.;
        for(it=lo; it!=hi;++it) { ITP("     it",i,it);
            if(it->first .first ==it->first .second )
                qDebug()<<"it->first .first ==it->first .second  "<<  it->first .first <<it->first .second  ;
            sum+=it->second ;
            if(it->first .first!=i  )
                qDebug()<<" WHY isnt (1) "  <<it->first .first  <<" equal to"   <<i;
        }
        ij.first=i;ij.second =i;  ma[ij] = - sum;
    }

    SumLsq= SumLsq/nlsq; // the average lengthsquared
    // diagonal terms of mchi. Also, non-dimensionalize.  Its dimensions are (1/L^2)

    for(i=0+JuneTest;i<np;i++) {
        lo = mchi.lower_bound (pair<int,int>(i-1,np+1)); ITP("lo",i,lo);
        hi = mchi.upper_bound (pair<int,int>(i+1,-1)); ITP("hi",i,hi);

        sum=0.;
        for(it=lo; it!=hi;++it) {  ITP("it",i,it);
            //   assert(it->first .first==i  );
            if(it->first .first ==it->first .second )
                qDebug()<<" WHY isnt (2) "  <<it->first.first  <<" equal to"   <<it->first .second;
            assert(it->first .first !=it->first .second ) ;
            it->second *=SumLsq;
            sum+=it->second ;
        }
        ij.first=i;ij.second =i;  mchi[ij] = - sum;
    }
    return elementnumber;
} //MakeMeyerMatrices

int RXMesh::MakeMeyerMatricesSP(vector<RXSitePt> &pts3d, map<pair<int,int>,double> &ma,map<pair<int,int>,double> &mchi )
{
    struct otri triangleloop;
    vertex p1, p2, p3;
    long elementnumber;
    int i1,i2,i3;
    int np, bwl, bwu ; // half-bandwidths.
    pair<int,int> ij;
    double L1,L2,L3, L1sq,L2sq,L3sq,CotA1,CotA2,CotA3, c,SumLsq,nlsq;
    SumLsq=0.; nlsq=0.;
    if (m_b->order != 1)
        return 0;
    np=  bwl =  (int)pts3d.size(); bwu=0;
    elementnumber = m_b->firstnumber;
    // first the off-diagonal terms beta and gamma by walking the triangles twice. Once for beta,gamma, then alpha,delta
    // NOTE that we use the identity Cot[Arcos[x]] === x/sqrt(1-x^2)

    traversalinit(&m_m->triangles);
    triangleloop.tri = triangletraverse(m_m);
    triangleloop.orient = 0;

    while (triangleloop.tri != (triangle *) NULL) {
        org(triangleloop, p1);  	dest(triangleloop, p2); 	apex(triangleloop, p3);
        i1 = NodeNo[ vertexmark(p1)];    	i2 = NodeNo[vertexmark(p2)];    	i3 = NodeNo[vertexmark(p3)];
        RXSitePt &v1=pts3d.at(i1); 	RXSitePt &v2=pts3d.at(i2); 	RXSitePt &v3=pts3d.at(i3);
        v1.m_Site_Flags = v1.m_Site_Flags |PCF_ISMESHSITE;
        v2.m_Site_Flags = v2.m_Site_Flags |PCF_ISMESHSITE;
        v3.m_Site_Flags = v3.m_Site_Flags |PCF_ISMESHSITE;

        L1sq = (v2-v3).LengthSquared(); L2sq = (v3-v1).LengthSquared(); L3sq = (v1-v2).LengthSquared();
        L1= sqrt(L1sq);  	L2=sqrt(L2sq) ; 	L3= sqrt(L3sq);
        //printf(" l1,l2,l3  %f %f %f\n", L1,L2,L3);
        c= (L2sq + L3sq - L1sq)/(2.0*L2*L3);    	CotA1 =c/sqrt(1.-c*c);
        c= (L3sq + L1sq - L2sq)/(2.0*L3*L1);		CotA2 =c/sqrt(1.-c*c);
        c= (L1sq + L2sq - L3sq)/(2.0*L1*L2);		CotA3 =c/sqrt(1.-c*c);
        SumLsq+=(L1sq+L2sq+L3sq ); nlsq+=3.;
        ij.first =i3; ij.second =i2;
        ma[ij] = CotA1;	mchi[ij] = CotA2/L1sq;   bwl = min(bwl,i2-i3);  bwu = max(bwu,i2-i3);
        ij.first =i1; ij.second =i3;
        ma[ij] = CotA2;	mchi[ij] = CotA3/L2sq;   bwl = min(bwl,i3-i1);  bwu = max(bwu,i3-i1);
        ij.first =i2; ij.second =i1;
        ma[ij] = CotA3;	mchi[ij] = CotA1/L3sq;   bwl = min(bwl,i1-i2);  bwu = max(bwu,i1-i2);
        triangleloop.tri = triangletraverse(m_m);
        elementnumber++;
    }

    // now walk the triangles again and add components due to alpha and delta into the matrices
    traversalinit(&m_m->triangles);
    triangleloop.tri = triangletraverse(m_m);
    triangleloop.orient = 0;

    while (triangleloop.tri != (triangle *) NULL) {
        org(triangleloop, p1);  	dest(triangleloop, p2); 	apex(triangleloop, p3);
        i1 = NodeNo[ vertexmark(p1)];    	i2 = NodeNo[vertexmark(p2)];    	i3 = NodeNo[vertexmark(p3)];
        RXSitePt &v1=pts3d.at(i1); 	RXSitePt &v2=pts3d.at(i2); 	RXSitePt &v3=pts3d.at(i3);
        L1sq = (v2-v3).LengthSquared(); L2sq = (v3-v1).LengthSquared(); L3sq = (v1-v2).LengthSquared();
        L1= sqrt(L1sq);  	L2=sqrt(L2sq) ; 	L3= sqrt(L3sq);

        c= (L2sq + L3sq - L1sq)/(2.0*L2*L3);    	CotA1 =c/sqrt(1.-c*c);
        c= (L3sq + L1sq - L2sq)/(2.0*L3*L1);		CotA2 =c/sqrt(1.-c*c);
        c= (L1sq + L2sq - L3sq)/(2.0*L1*L2);		CotA3 =c/sqrt(1.-c*c);

        ij.first =i3; ij.second =i1;
        ma[ij] += CotA2;	mchi[ij] += CotA1/L2sq;   bwl = min(bwl,i1-i3);  bwu = max(bwu,i1-i3);
        ij.first =i1; ij.second =i2;
        ma[ij] += CotA3;	mchi[ij] += CotA2/L3sq;   bwl = min(bwl,i2-i1);  bwu = max(bwu,i2-i1);
        ij.first =i2; ij.second =i3;
        ma[ij] += CotA1;	mchi[ij] += CotA3/L1sq;   bwl = min(bwl,i3-i2);  bwu = max(bwu,i3-i2);

        triangleloop.tri = triangletraverse(m_m);
    }

    if(!elementnumber) { cout<< "no elements"<<endl; return 0;}

    // zero the matrix if (ij) is an edge
    int i;//, neighbor  ;
    traversalinit(&m_m->triangles);
    triangleloop.tri = triangletraverse(m_m);

    // now the diagonal terms which are minus the sum of the off-diag terms
    if(1) { // peter test 2012 february
        double sum;
        int j,j0,j1;
        for(i=0;i<np;i++) {
            j0 = max(0,i+bwl); j1=min(np,i+bwu+1); j0=0; j1=np;
            sum=0.;
            ij.first=i;
            for(j=j0;j<j1; j++) {
                if(j==i) continue;
                ij.second = j;
                if(ma.find(ij) != ma.end())
                    sum+=ma[ij];
            }
            ij.second =i;  ma[ij] = - sum;
        }
    }
    else {
        map<pair<int,int>,double>::iterator lo,hi,it;
        double sum;
        for(i=0;i<np;i++) {
            lo = ma.lower_bound (pair<int,int>(i-1,np+1));
            hi = ma.upper_bound (pair<int,int>(i+1,0));
            sum=0.;
            for(it=lo; it!=hi;++it) {
                assert(it->first .first !=it->first .second ) ;
                sum+=it->second ;
            }
            ij.first=i;ij.second =i;  ma[ij] = - sum;
        }
    }

    SumLsq= SumLsq/nlsq; // the average lengthsquared
    // diagonal terms of mchi. Also, non-dimensionalize.  Its dimensions are (1/L^2)

    double sum; int j,j0,j1;
    if(1){
        for(i=0;i<np;i++) {
            j0 = max(0,i+bwl); j1=min(np,i+bwu+1); j0=0; j1=np;
            sum=0.;
            ij.first=i;
            for(j=j0;j<j1; j++) {
                if(j==i) continue;
                ij.second = j;
                if(mchi.find(ij) != mchi.end()) {
                    mchi[ij]*=SumLsq;
                    sum+=mchi[ij];
                }
            }
            ij.second =i;  mchi[ij] = - sum;
        }
    }
    else {
        map<pair<int,int>,double>::iterator lo,hi,it;
        double sum;
        for(i=0;i<np;i++) {
            lo = mchi.lower_bound (pair<int,int>(i-1,np+1));
            hi = mchi.upper_bound (pair<int,int>(i+1,0));
            sum=0.;
            for(it=lo; it!=hi;++it) {
                assert(it->first .first !=it->first .second ) ;
                it->second *=SumLsq;
                sum+=it->second ;
            }
            ij.first=i;ij.second =i;  mchi[ij] = - sum;
        }
    }
    return elementnumber;
} //MakeMeyerMatrices
// RXMesh holds the mesh in Shewk form. 
// map the mesh to 2D, placing the points into the UV values of pts3d 
// pts3d is expected to have (u,v) invalid except for the points we want to fix in UV space.
// at output, the UV values of pts3d have been filled in. 

int RXMesh::Flatten(vector<RX_FESite*> &pts3d ) // this is the variant called on the whole-sail mesh.
{
    //  return value is 0 for an error, else
    int nr,rC=0,r,n;
    spamat  mma,mmchi;
    RXSparseMatrix ma(RU),mchi(RU),m(RU);
    vector<RX_FESite*>::iterator it;
    RX_FESite*p;
    RXSRelSite*q;
    double mu = 1.-m_lambda;
    // the steps are:
    //1) create the matrices Ma and MaChi
    //2) assemble the complete matrix taking into account the given (usually boundary) points
    //3) create the RHS
    //4) Solve the system of equations
    //5) report the new coordinates in pts

    SetMsgText("matrices..");
    int mmrc=MakeMeyerMatrices(pts3d, mma,mmchi );
    if(!mmrc ) {
        cout<<"MakeMeyerMatrices returned 0. see rxmeshBad.txt"<<endl;
        FILE *fp = fopen("rxmeshBad.txt","w");
        int kkk=   m_b->verbose;   m_b->verbose=5;
        report(fp, this->m_t, 1, 0, 0, 1, 1, 1);
        fclose(fp);  m_b->verbose=kkk;
        return rC;
    }
    ma= RXSparseMatrix(mma,RU);
    mchi= RXSparseMatrix(mmchi,RU);

    m = ma*m_lambda   + (mchi*mu);
    SetMsgText("attributes..");
    // for U.
    //make RHS and Assemble
    double  uuvv;
    nr = m.Nr();
    m.rHs.clear(); m.rHs.resize(nr,0.0); 	m.m_nRhsVecs=1;
    int j;
    for(j=0,it=pts3d.begin (); it!=pts3d.end();++it,j++) {// should count over rows and getnodebynumber(revind(row)
        //       m.rHs[j]=0;
        p = *it;   if(!p) continue;
        q=dynamic_cast<RXSRelSite*>(p);
        if(q) {
            if(q->GetUVAttribute(RELSITE_U ,uuvv)){ // DONT FORGET: this inherits atts from q's Curve[0]

                p->m_u = uuvv;
                n=q->GetN();
                r = ShewkIndex[n];
                // qDebug()<< "U att " <<uuvv<< " row="<<r<<q->GetQName();
                m.rHs[r]=p->m_u;
                m.ZeroRow(r); m.m_map[pair<int,int>(r,r)]=1;
                assert( m.m_StorageType==MAPSTYLE);
            }
        }
    }// for i
    if(WRITE_MEYER ){
        m.HB_Write("meyermatrixU.rra");
        FILE*fp = RXFOPEN("meyermatrixUrhs.txt","w")  ;
        for(j=0;j<m.Nr();j++)  fprintf(fp,"%20.12f ",m.rHs[j]);
        FCLOSE(fp);
    }
    double *resultU =new double[(nr+1)* m.m_nRhsVecs];
    QString qq("solve MeyerMatrix: " );
    if(q) qq+= q->GetSail()->GetQType();
    SetMsgText( qPrintable  (qq));
    //    m.SolveIterative(resultU,0.01);
    m.SetType(CSC);
    m.SolveOnce (resultU);


    SetMsgText("attributes (V)..");
    // cout<<" finished for U. now do V"<<endl;
    // for V.
    m = ma*m_lambda   + (mchi*mu);
    nr = m.Nr();
    m.rHs.clear(); m.rHs.resize(nr,0.0); 	m.m_nRhsVecs=1;
    for(j=0,it=pts3d.begin (); it!=pts3d.end();++it,j++) {


        //    m.rHs[j]=0; qDebug()<< "WRONG";
        p = *it;   if(!p) continue;

        q=dynamic_cast<RXSRelSite*>(p);
        if(q){
            if(q->GetUVAttribute(RELSITE_V,uuvv)){
                p->m_v = uuvv;
                n=q->GetN();
                r = ShewkIndex[n];        assert(r<nr);
                m.rHs[r]=p->m_v;
                m.ZeroRow(r);
                m.m_map[pair<int,int>(r,r)]=1;  assert( m.m_StorageType==MAPSTYLE);
            }
        } // if q
    } // for i
    if(WRITE_MEYER ){
        m.HB_Write("meyermatrixV.rra");
        FILE*fp = RXFOPEN("meyermatrixVrhs.txt","w")  ;
        for(j=0;j<m.Nr();j++)     fprintf(fp,"%20.12f",m.rHs[j]);
        FCLOSE(fp);
    }
    double *resultV =new double[m.Nr()* m.m_nRhsVecs];
    SetMsgText("solve(2)..");

    m.SetType(CSC);
    m.SolveOnce (resultV);

    //set UV from solution
    //again, result[r] belongs to TraingleIndex(GetN)
    for(j=0,it=pts3d.begin (); it!=pts3d.end();++it,j++) {
        p = *it;  if(!p) continue;
        if(p->m_Site_Flags&PCF_ISMESHSITE){
            assert(ShewkIndex.find(p->GetN())  != ShewkIndex.end());
            r=ShewkIndex[p->GetN()];
            p->m_u = resultU[r];
            p->m_v = resultV[r];
        }
        //        else
        //            qDebug()<<"Dont set U and V on node(not PCF_ISMESHSITE ) no "<<p->GetN()   <<" at " <<p->x<<p->y<<p->z;
    }
    delete[] resultU;
    delete[] resultV;
    SetMsgText("Flatten done..");
    return rC+1;
}// RXMesh::Flatten

int RXMesh::FlattenSP(vector<RXSitePt> &pts3dd )
{

    assert(strlen("RXMesh::FlattenSP indexingneeds stepping. Maybe WRONG if there are non-field nodes")==0);
    int nr,rc=0;
    spamat  mma,mmchi;
    RXSparseMatrix ma(RU),mchi(RU),m(RU);
    vector<RXSitePt>::iterator it;
    double mu = 1.-m_lambda;
    // the steps are:
    //1) create the matrices Ma and MaChi
    //2) assemble the complete matrix taking into account the given (usually boundary) points
    //3) create the RHS
    //4) Solve the system of equations
    //5) report the new coordinates in pts

    MakeMeyerMatricesSP(pts3dd, mma,mmchi );

    //WritePoints(pts3d,"points.txt");

    ma= RXSparseMatrix(mma,RU);
    mchi= RXSparseMatrix(mmchi,RU);

    m = ma*m_lambda   + (mchi*mu);

    // for U.
    //make RHS and Assemble
    nr = m.Nr();
    m.rHs.clear(); m.rHs.resize(nr,0); 	m.m_nRhsVecs=1;
    int i;
    for(i=0,it=pts3dd.begin (); it!=pts3dd.end();++it,i++) {
        RXSitePt&p = *it;
        if(ON_IsValid(p.m_u)){
            m.rHs[i]=p.m_u;
            m.ZeroRow(i); m.m_map[pair<int,int>(i,i)]=1;
        }
        else{
            m.rHs[i]=0;
        }
    }

    double *resultU =new double[m.Nr()* m.m_nRhsVecs];
    //   m.SolveIterative(resultU,0.01);
    m.SetType(CSC);
    m.SolveOnce (resultU);
    // for V.
    m = ma*m_lambda   + (mchi*mu);

    //make RHS and Assemble

    m.rHs.clear(); m.rHs.resize(nr,0); 	m.m_nRhsVecs=1;

    for(i=0,it=pts3dd.begin (); it!=pts3dd.end();++it,i++) {
        RXSitePt&p = *it;
        if(ON_IsValid(p.m_v)){
            m.rHs[i]=p.m_v;
            m.ZeroRow(i); m.m_map[pair<int,int>(i,i)]=1;
        }
        else{
            m.rHs[i]=0;
        }
    }
    double *resultV =new double[ nr* m.m_nRhsVecs];
    //    m.SolveIterative(resultV,0.01);
    m.SetType(CSC);
    m.SolveOnce (resultV);
    //set UV from solution

    for(i=0,it=pts3dd.begin (); it!=pts3dd.end();++it,i++) {
        RXSitePt&p = *it;
        if(p.m_Site_Flags&PCF_ISMESHSITE){
            p.m_u = resultU[i];
            p.m_v = resultV[i];
        }
        else
            p.m_u = p.m_v =(double) (- 10*i);
    }
    delete []resultU;
    delete []resultV;

    return rc;
}// RXMesh::Flatten
int RXMesh::WritePoints(vector<RXSitePt> pts,const char*fname){
    FILE *fp=fopen(fname,"w");
    if(!fp) return 0;
    int np=0;

    vector<RXSitePt>::const_iterator it;
    for(it=pts.begin(); it!=pts.end();++it) {
        np++;
        fprintf(fp,"%.9g %.9g %.9g %.9g  %.9g\n",it->x,it->y,it->z,it->m_u,it->m_v);
    }
    fclose(fp);
    return np;
}

int RXMesh::WriteLabels(vector<RXSitePt> pts,const char*fname){
    FILE *fp=fopen(fname,"w");
    if(!fp) return 0;
    int np=0;

    vector<RXSitePt>::const_iterator it;
    for(it=pts.begin(); it!=pts.end();++it) {
        np++;
        fprintf(fp,"%.9g %.9g %.9g\n",it->x,it->y,it->z);
    }
    fclose(fp);
    return np;
}
int RXMesh::Triangulate(vector<RXSitePtWithData*> &pts,const char *buf) // this one used by read_ND_State
{
    ShewkIndex.clear(); NodeNo.clear();
    int rc=1;		int i,npts, nct, nodeno ,k1,k2;
    struct triangulateio in;	memset(&in,0,sizeof(struct triangulateio  ));

    RXTRI_REAL *lp;
    int *ilp,*ime;

    assert(sizeof(RXTRI_REAL)==sizeof(double));
    if(!m_m) {m_m = new struct mesh; memset(m_m,0,sizeof(struct mesh)); }
    if(!m_b) { m_b = new struct behavior;memset(m_b,0,sizeof(struct behavior));
        m_b->verbose =0; m_b->userdata=0;}
    // count the points we want in the triangulation;

    npts=0;
    for(vector<RXSitePtWithData*>::iterator it= pts.begin(); it!=pts.end(); ++it)  {
        if(!(*it)) {  continue;}
        npts++;
    }

    in.numberofpoints=npts+8;  // the 8 'distant' points
    assert(strstr(buf,"c"));

    /* Define input points. */
    trifreeIOstruct(*m_t );	memset(this->m_t,0,sizeof(struct triangulateio  ));

    in.numberofpointattributes = 1;
    in.pointlist = (RXTRI_REAL *) malloc(in.numberofpoints * 2 * sizeof(REAL));
    in.pointmarkerlist = (int *) calloc(in.numberofpoints , sizeof(int));
    ilp = in.pointmarkerlist;
    lp=in.pointlist;

    { // UV
        npts=0; i=0;
        for(vector<RXSitePtWithData*>::iterator it= pts.begin(); it!=pts.end(); ++it)  {
            if(!(*it)) { continue;}
            *lp = (*it)->m_u; lp++; *lp = (*it)->m_v; lp++;
            nodeno = i;
            *ilp= nodeno ;
            ShewkIndex[nodeno]=i;  NodeNo[i ]= nodeno ;
            i++;  npts++;
            ilp++;//ilp is point marker
        }
        // in UV push back another 8 points
        *lp = 2; lp++; *lp = 0; lp++;  npts++; 	*ilp= -990; ShewkIndex[*ilp]=-1; NodeNo[-1]=*ilp ;    ilp++;
        *lp = 2; lp++; *lp = 1; lp++;  npts++; 	*ilp= -991; ShewkIndex[*ilp]=-2; NodeNo[-2]=*ilp ;    ilp++;
        *lp = 1; lp++; *lp = 2; lp++;  npts++;	*ilp= -992; ShewkIndex[*ilp]=-13; NodeNo[-13]=*ilp ;  ilp++;
        *lp = 0; lp++; *lp = 2; lp++;  npts++;	*ilp= -993; ShewkIndex[*ilp]=-4; NodeNo[-4]=*ilp ;    ilp++;
        *lp = -1; lp++; *lp = 1; lp++;  npts++;	*ilp= -994; ShewkIndex[*ilp]=-5; NodeNo[-5]=*ilp ;    ilp++;
        *lp = -1; lp++; *lp = 0; lp++;  npts++;	*ilp= -995; ShewkIndex[*ilp]=-6; NodeNo[-6]=*ilp ;    ilp++;
        *lp = 0; lp++; *lp = -1; lp++;  npts++;	*ilp= -996; ShewkIndex[*ilp]=-7; NodeNo[-7]=*ilp ;    ilp++;
        *lp = 1; lp++; *lp = -1; lp++;  npts++;	*ilp= -997; ShewkIndex[*ilp]=-8; NodeNo[-8]=*ilp ;    ilp++;
        // march 2014 revind added, *ilp were all -999
    } // end if UV
    in.pointattributelist = (RXTRI_REAL *) calloc(in.numberofpoints *
                                                  in.numberofpointattributes ,
                                                  sizeof(REAL));
    lp = in.pointattributelist;
    // for(vector<RX_FESite*>::iterator it= pts.begin(); it!=pts.end(); ++it)  {
    for(i=0;i< in.numberofpoints;i++,lp++)
        *lp = 1.0;

    in.numberofholes = 0;
    in.numberofregions = 0;
    in.regionlist =0;

    /* Make necessary initializations so that ShewchukTriangle can return a */
    /*   triangulation in `this->m_t' and a voronoi diagram in `vorout'.  */
    trifreeIOstruct(*m_t );
    this->m_t->pointlist = (REAL *) NULL;            /* Not needed if -N switch used. */
    /* Not needed if -N switch used or number of point attributes is zero: */
    this->m_t->pointattributelist = (REAL *) NULL;
    this->m_t->pointmarkerlist = (int *) NULL; /* Not needed if -N or -B switch used. */
    this->m_t->trianglelist = (int *) NULL;          /* Not needed if -E switch used. */
    /* Not needed if -E switch used or number of triangle attributes is zero: */
    this->m_t->triangleattributelist = (REAL *) NULL;
    this->m_t->neighborlist = (int *) NULL;         /* Needed only if -n switch used. */
    /* Needed only if segments are output (-p or -c) and -P not used: */
    this->m_t->segmentlist = (int *) NULL;
    /* Needed only if segments are output (-p or -c) and -P and -B not used: */
    this->m_t->segmentmarkerlist = (int *) NULL;
    this->m_t->edgelist = (int *) NULL;             /* Needed only if -e switch used. */
    this->m_t->edgemarkerlist = (int *) NULL;   /* Needed if -e used and -B not used. */

    try{
        ///////////////////////////////////////////////////////////////////
        triangulate(buf , &in, this->m_t, NULL , m_m,m_b);
        ////////////////////////////////////////////////////////////////////
        // if it fails we could try -i
    }//end try
    catch (RXException e) {
        e.Print(stdout);
        WriteAsPoly(&in,"inExceptionRXE");rc=0;
    }
    catch( char * str ) {
        printf( "Exception raised:  <%s>\n",str);
        WriteAsPoly(&in,"inExceptionstr");rc=0;
    }
    catch( unsigned int ex ) {
        printf( "Exception raised: no= %d\n",(int)ex);
        WriteAsPoly(&in,"inExceptionUI");rc=0;
    }
#ifdef MICROSOFT
    catch(CException *ex) {
        wchar_t buf[1024];
        ex->GetErrorMessage(buf,1024);
        cout<< "(Triangle) Cexception "<< buf << endl;
        WriteAsPoly(&in); rc=0;
    }
#endif
    catch(...) { // usually get here
        cout<< "(Triangle) otherexception (1)"<<endl;
        WriteAsPoly(&in,"inExceptionOther"); rc=0;
    }
    /* Free all allocated arrays, including those allocated by ShewchukTriangle. */

    free(in.pointlist);
    free(in.pointattributelist);
    free(in.pointmarkerlist);
    free(in.segmentmarkerlist);
    if(in.regionlist )free(in.regionlist);
    free(in.segmentlist );

    m_MeshSpace=UV;
    return rc;
}
// this is the one used by  the UV texture-coordinate calculation. IE  sail::MakeUVMapping_XYPlane
int RXMesh::TriangulateForUV(SAIL *sail,vector<RX_FESite*> &pts,const vector<RX_FEedge*> &edges,const char *buf,const int ffflag)
{
    ShewkIndex.clear(); NodeNo.clear();
    int rc=1;		int i,npts, nct, nodeno ,k1,k2,sno;
    struct triangulateio in;	memset(&in,0,sizeof(struct triangulateio  ));
    ON_Xform  xf = sail->Trigraph();
    ON_3dPoint xtr;
    RXTRI_REAL *lp;
    int *ilp,*ime;

    assert(sizeof(RXTRI_REAL)==sizeof(double));
    if(!m_m) {m_m = new struct mesh; memset(m_m,0,sizeof(struct mesh)); }
    if(!m_b) { m_b = new struct behavior;memset(m_b,0,sizeof(struct behavior));
        m_b->verbose =0; m_b->userdata=0;}
    // count the points we want in the triangulation;
    if(ffflag==0){ //XYZ
        i=0;  nct=-1;
        for(vector<RX_FESite*>::iterator it= pts.begin(); it!=pts.end(); ++it)  {
            nct++;
            if(!(*it))    continue;
            if(!((*it)->m_Site_Flags&PCF_ISFIELDNODE)) {
                continue;
            }
            nodeno = (*it)->GetN();	assert( nodeno==nct);
            i++;
        }
        in.numberofpoints=i;
        assert(!strstr(buf,"c"));
    } // if XYZ
    else{ // UV
        npts=0;
        for(vector<RX_FESite*>::iterator it= pts.begin(); it!=pts.end(); ++it)  {
            if(!(*it)) {  continue;}
            if( !((*it)->m_Site_Flags&PCF_ISFIELDNODE)) {
                continue;
            }
            npts++;
        }
        in.numberofpoints=npts+8;  // the 8 'distant' points
        if(!ONLY_EDGEEDGES)
            assert(strstr(buf,"c"));
    } // end if UV

    // Define input points.
    if(in.numberofpoints <3 ) return 0;

    trifreeIOstruct(*m_t );	memset(this->m_t,0,sizeof(struct triangulateio  ));

    in.numberofpointattributes = 1;
    in.pointlist = (RXTRI_REAL *) malloc(in.numberofpoints * 2 * sizeof(REAL));
    in.pointmarkerlist = (int *) calloc(in.numberofpoints , sizeof(int));
    ilp = in.pointmarkerlist;
    lp=in.pointlist;

    if(ffflag==0){ //XYZ
        i=0;
        for(vector<RX_FESite*>::iterator it= pts.begin(); it!=pts.end(); ++it)  {
            if(!(*it))
            {  continue;}
            if(!((*it)->m_Site_Flags&PCF_ISFIELDNODE))
            {  continue;}
            xtr = xf* (*(*it));
            *lp = xtr.x; lp++; *lp = xtr.y; lp++;// transform by sail::Trigraph()
            nodeno = (*it)->GetN();
            *ilp= nodeno ;
            ShewkIndex[nodeno]=i;  NodeNo[i ]= nodeno ;
            i++;
            ilp++;//ilp is point marker
        }
    }
    else{ // UV
        npts=0; i=0;
        for(vector<RX_FESite*>::iterator it= pts.begin(); it!=pts.end(); ++it)  {
            if(!(*it))
            { continue;}
            if( !((*it)->m_Site_Flags&PCF_ISFIELDNODE))
            {continue;}
            sno= (*it)->RXSite_N();
            class   RX_FESite* s = *it;    // for DEBUG
            nodeno = (*it)->GetN();

            //  if( sno  !=RXSITEINITIAL_N ) {
            if(! ON_IsValid((*it)->m_u) || !ON_IsValid((*it)->m_v   ))
            {
                qDebug()<<" invalid m_u m_v .nodeno= "  <<nodeno<<" sno= "<<sno;
               // continue;
            }
            assert(ON_IsValid((*it)->m_u ));
            assert(ON_IsValid((*it)->m_v ));

            *lp = (*it)->m_u; lp++; *lp = (*it)->m_v; lp++;

            *ilp= nodeno ;
            ShewkIndex[nodeno]=i;  NodeNo[i ]= nodeno ;
            i++;  npts++;
            ilp++;//ilp is point marker
        }
        // in UV push back another 8 points
        *lp = 1002;  lp++; *lp =     0; lp++;  npts++; 	*ilp= -990; ShewkIndex[*ilp]=-1;  NodeNo[-1 ]= *ilp ;ilp++;
        *lp = 1002;  lp++; *lp =     1; lp++;  npts++; 	*ilp= -991; ShewkIndex[*ilp]=-2;  NodeNo[-2 ]= *ilp ;ilp++;
        *lp =    1;  lp++; *lp =  1002; lp++;  npts++;	*ilp= -992; ShewkIndex[*ilp]=-3;  NodeNo[-3 ]= *ilp ;ilp++;
        *lp =    0;  lp++; *lp =  1002; lp++;  npts++;	*ilp= -993; ShewkIndex[*ilp]=-4;  NodeNo[-4 ]= *ilp ;ilp++;
        *lp = -1001; lp++; *lp =     1; lp++;  npts++;	*ilp= -994; ShewkIndex[*ilp]=-5;  NodeNo[-5 ]= *ilp ;ilp++;
        *lp = -1001; lp++; *lp =     0; lp++;  npts++;	*ilp= -995; ShewkIndex[*ilp]=-6;  NodeNo[-6 ]= *ilp ;ilp++;
        *lp =    0;  lp++; *lp = -1001; lp++;  npts++;	*ilp= -996; ShewkIndex[*ilp]=-7;  NodeNo[-7 ]= *ilp ;ilp++;
        *lp =    1;  lp++; *lp = -1001; lp++;  npts++;	*ilp= -997; ShewkIndex[*ilp]=-8;  NodeNo[-8 ]= *ilp ;ilp++;
        // march 2014 ilp was all -999, NodeNo was not filled in
    } // end if UV

    in.pointattributelist = (RXTRI_REAL *) calloc(in.numberofpoints *
                                                  in.numberofpointattributes ,
                                                  sizeof(REAL));
    lp = in.pointattributelist;
    for(i=0;i< in.numberofpoints;i++,lp++)
        *lp = 1.0;

    // the edges, only if we are in XYZ
    if(ffflag==0){
        // first, count them
        vector<RX_FEedge*> ::const_iterator it;
        i=0;
        for(it=edges.begin ();it!=edges.end();it++) {
            RX_FEedge* ee = *it;
            if(!ee)
                continue;
            if(!(ee->GetNode(0)->m_Site_Flags&PCF_ISFIELDNODE))
                continue;
            if(!(ee->GetNode(1)->m_Site_Flags&PCF_ISFIELDNODE))
                continue;
            //  PCF_ISONEDGE

            if(ONLY_EDGEEDGES) {
                if(!(ee->m_Flags&RXFEE_ISMODELEDGE))
                    continue;
            }
            i++;
        }
        // now, copy them to Shewk
        in.numberofsegments= i;

        ilp = in.segmentlist = (int*) malloc(in.numberofsegments*2* sizeof(int));
        ime = in.segmentmarkerlist = (int*) malloc(in.numberofsegments * sizeof(int));

        i=-990;
        for(it=edges.begin ();it!=edges.end();it++) {
            RX_FEedge* ee = *it;
            if(!ee)
                continue;
            if(!(ee->GetNode(0)->m_Site_Flags&PCF_ISFIELDNODE))
                continue;
            if(!(ee->GetNode(1)->m_Site_Flags&PCF_ISFIELDNODE))
                continue;
            if(ONLY_EDGEEDGES) {
                if(!(ee->m_Flags&RXFEE_ISMODELEDGE))
                    continue;
            }
            k1 = ee->GetNode(0)->GetN(); assert(ShewkIndex.end()!=ShewkIndex.find(k1));
            k2 = ee->GetNode(1)->GetN();  assert(ShewkIndex.end()!=ShewkIndex.find(k2));
            *ilp= ShewkIndex[k1]; ilp++;
            *ilp= ShewkIndex[k2]; ilp++;
            *ime=ee->GetN(); ime++;
        }
    } // if XYZ


    in.numberofholes = 0;
    in.numberofregions = 0;
    in.regionlist =0;

    if(m_b->verbose>3){
        cout<< "Input point set:\n\n"<<endl;
        report(stdout,&in, 1, 0, 1, 1, 1, 1);
    }
    /* Make necessary initializations so that ShewchukTriangle can return a */
    /*   triangulation in `this->m_t' and a voronoi diagram in `vorout'.  */
    trifreeIOstruct(*m_t );
    this->m_t->pointlist = (REAL *) NULL;            /* Not needed if -N switch used. */
    /* Not needed if -N switch used or number of point attributes is zero: */
    this->m_t->pointattributelist = (REAL *) NULL;
    this->m_t->pointmarkerlist = (int *) NULL; /* Not needed if -N or -B switch used. */
    this->m_t->trianglelist = (int *) NULL;          /* Not needed if -E switch used. */
    /* Not needed if -E switch used or number of triangle attributes is zero: */
    this->m_t->triangleattributelist = (REAL *) NULL;
    this->m_t->neighborlist = (int *) NULL;         /* Needed only if -n switch used. */
    /* Needed only if segments are output (-p or -c) and -P not used: */
    this->m_t->segmentlist = (int *) NULL;
    /* Needed only if segments are output (-p or -c) and -P and -B not used: */
    this->m_t->segmentmarkerlist = (int *) NULL;
    this->m_t->edgelist = (int *) NULL;             /* Needed only if -e switch used. */
    this->m_t->edgemarkerlist = (int *) NULL;   /* Needed if -e used and -B not used. */

    QString qfilename;
    try{
        if(0) {
            qfilename=QString("InPut_%1").arg(ffflag);
            WriteAsPoly(&in,qPrintable(qfilename) );
        }
        ///////////////////////////////////////////////////////////////////
        triangulate(buf , &in, this->m_t, NULL , m_m,m_b);

        ////////////////////////////////////////////////////////////////////
        if(0) {
            qfilename=QString("OutPut_%1").arg(ffflag);
            WriteAsPoly(m_t,qPrintable(qfilename));
        }

        // if it fails we could try -i
    }//end try
    catch (RXException e) {
        e.Print(stdout);
        WriteAsPoly(&in,"inExceptionRXE");rc=0;
    }
    catch( char * str ) {
        printf( "Exception raised:  <%s>\n",str);
        WriteAsPoly(&in,"inExceptionSTR");rc=0;
    }
    catch( unsigned int ex ) {
        printf( "Exception raised: no= %u\n",ex);
        WriteAsPoly(&in,"inExceptionUI");rc=0;
    }
#ifdef MICROSOFT
    catch(CException *ex) {
        wchar_t buf[1024];
        ex->GetErrorMessage(buf,1024);
        cout<< "(Triangle) Cexception "<< buf << endl;
        WriteAsPoly(&in); rc=0;
    }
#endif
    catch(...) { // usually get here
        cout<< "(Triangle) otherexception (2)"<<endl;
        WriteAsPoly(&in,"inExceptionOTHER"); rc=0;
    }
    /* Free all allocated arrays, including those allocated by ShewchukTriangle.
                       its OTT to zero them because in goes out of scope anyway*/

    free(in.pointlist);  in.pointlist=0;
    free(in.pointattributelist);  in.pointattributelist=0;
    free(in.pointmarkerlist);  in.pointmarkerlist=0;
    free(in.segmentmarkerlist); in.segmentmarkerlist=0;
    if(in.regionlist )free(in.regionlist); in.regionlist=0;
    free(in.segmentlist );  in.segmentlist=0;
    if(ffflag)
        m_MeshSpace=UV;
    else
        m_MeshSpace=XYZ;
    return rc;

}


int RXMesh::Triangulate(vector<RXSitePt> inpts, vector<pair<int,int> > &pedges ,const char *buf,const int ffflag) {

    int rc=1;
    struct triangulateio in;	memset(&in,0,sizeof(struct triangulateio  ));

    RXTRI_REAL *lp;
    int i, *ilp,*ime;
    assert(sizeof(RXTRI_REAL)==sizeof(double));
    if(!m_m) m_m = new struct mesh;
    if(!m_b) m_b = new struct behavior;  m_b->verbose =0; m_b->userdata=0;
    /* Define input points. */

    trifreeIOstruct(*m_t );	memset(this->m_t,0,sizeof(struct triangulateio  ));
    in.numberofpoints = (int)inpts.size();
    if(in.numberofpoints <3 ) return 0;

    in.numberofpointattributes = 1;
    in.pointlist = (RXTRI_REAL *) malloc(in.numberofpoints * 2 * sizeof(REAL));

    lp=in.pointlist;
    if(ffflag==0)
        for(vector<RXSitePt>::iterator it= inpts.begin(); it!=inpts.end(); ++it)  {
            *lp = it->x; lp++; *lp = it->y; lp++;
        }
    else
        for(vector<RXSitePt>::iterator it= inpts.begin(); it!=inpts.end(); ++it)  {
            *lp = it->m_u; lp++; *lp = it->m_v; lp++;
        }

    in.pointattributelist = (RXTRI_REAL *) calloc(in.numberofpoints *
                                                  in.numberofpointattributes ,
                                                  sizeof(REAL));
    lp = in.pointattributelist;
    for(vector<RXSitePt>::iterator it= inpts.begin(); it!=inpts.end(); ++it)  {
        *lp = 1.0; lp++;
    }

    in.pointmarkerlist = (int *) calloc(in.numberofpoints , sizeof(int));
    ilp = in.pointmarkerlist;
    i=0;
    for(vector<RXSitePt>::iterator it= inpts.begin(); it!=inpts.end(); ++it)  {
        *ilp = i++;   ilp++;
    }
    // the edges
    in.numberofsegments=(int)pedges.size();

    ilp = in.segmentlist = (int*) malloc(in.numberofsegments*2* sizeof(int));
    ime = in.segmentmarkerlist = (int*) malloc(in.numberofsegments * sizeof(int));
    vector<pair<int,int> > ::const_iterator it;
    i=0;
    for(it=pedges.begin ();it!=pedges.end();it++) {
        *ilp= it->first; ilp++; *ilp=it->second ; ilp++;
        *ime=i; i++; ime++;
    }


    in.numberofholes = 0;
    in.numberofregions = 0;
    in.regionlist =0;

    if(m_b->verbose>3){
        cout<< "Input point set:\n\n"<<endl;
        report(stdout,&in, 1, 0, 1, 1, 1, 1);
    }
    /* Make necessary initializations so that ShewchukTriangle can return a */
    /*   triangulation in `this->m_t' and a voronoi diagram in `vorout'.  */
    trifreeIOstruct(*m_t );
    this->m_t->pointlist = (REAL *) NULL;            /* Not needed if -N switch used. */
    /* Not needed if -N switch used or number of point attributes is zero: */
    this->m_t->pointattributelist = (REAL *) NULL;
    this->m_t->pointmarkerlist = (int *) NULL; /* Not needed if -N or -B switch used. */
    this->m_t->trianglelist = (int *) NULL;          /* Not needed if -E switch used. */
    /* Not needed if -E switch used or number of triangle attributes is zero: */
    this->m_t->triangleattributelist = (REAL *) NULL;
    this->m_t->neighborlist = (int *) NULL;         /* Needed only if -n switch used. */
    /* Needed only if segments are output (-p or -c) and -P not used: */
    this->m_t->segmentlist = (int *) NULL;
    /* Needed only if segments are output (-p or -c) and -P and -B not used: */
    this->m_t->segmentmarkerlist = (int *) NULL;
    this->m_t->edgelist = (int *) NULL;             /* Needed only if -e switch used. */
    this->m_t->edgemarkerlist = (int *) NULL;   /* Needed if -e used and -B not used. */

    try{
        ///////////////////////////////////////////////////////////////////
        triangulate(buf , &in, this->m_t, NULL , m_m,m_b);
        ////////////////////////////////////////////////////////////////////
        // if it fails we could try -i
    }//end try
    catch (RXException e) {
        e.Print(stdout);
        WriteAsPoly(&in,"inExceptionRXE");rc=0;
    }
    catch( char * str ) {
        printf( "Exception raised:  <%s>\n",str);
        WriteAsPoly(&in,"inExceptionSTR");rc=0;
    }
    catch( unsigned int ex ) {
        printf( "Exception raised: no= %u\n",ex);
        WriteAsPoly(&in,"inExceptionUI");rc=0;
    }
#ifdef MICROSOFT
    catch(CException *ex) {
        wchar_t buf[1024];
        ex->GetErrorMessage(buf,1024);
        cout<< "(Triangle) Cexception "<< buf << endl;
        WriteAsPoly(&in); rc=0;
    }
#endif
    catch(...) { // usually get here
        cout<< "(Triangle) otherexception (3)"<<endl;
        WriteAsPoly(&in,"inExceptionOTHER"); rc=0;
    }
    /* Free all allocated arrays, including those allocated by ShewchukTriangle. */

    free(in.pointlist);
    free(in.pointattributelist);
    free(in.pointmarkerlist);
    free(in.segmentmarkerlist);
    if(in.regionlist )free(in.regionlist);
    free(in.segmentlist );
    if(ffflag)
        m_MeshSpace=UV;
    else
        m_MeshSpace=XYZ;
    return rc;
}

int RXMesh::Triangulate(vector<RXSitePt> inpts, vector<int> eNos,const char* buf, const int flag){

    int rc=1;
    class RXSitePt  *pp;
    struct triangulateio in;
    struct triangulateio &mid = *( this->m_t);
    RXTRI_REAL *lp;
    int i, *ilp;
    assert(sizeof(RXTRI_REAL)==sizeof(double));
    if(!m_m) m_m = new struct mesh;
    if(!m_b) m_b = new struct behavior; m_b->verbose =0; m_b->userdata=0;
    /* Define input points. */
    memset(&in,0,sizeof(struct triangulateio  ));
    memset(&mid,0,sizeof(struct triangulateio  ));
    in.numberofpoints = (int)inpts.size();
    if(in.numberofpoints <3 ) return 0;
    if(flag&PLEASEMAKEREGEN)
        in.numberofpointattributes = in.numberofpoints;
    else
        in.numberofpointattributes = 1;
    in.pointlist = (RXTRI_REAL *) malloc(in.numberofpoints * 2 * sizeof(REAL));

    lp=in.pointlist;
    for(vector<RXSitePt>::iterator it= inpts.begin(); it!=inpts.end(); ++it)  {
        pp = &(*it);
        *lp = it->x; lp++; *lp = it->y; lp++;
    }
    in.pointattributelist = (RXTRI_REAL *) calloc(in.numberofpoints *
                                                  in.numberofpointattributes ,
                                                  sizeof(REAL));
    if(flag&PLEASEMAKEREGEN){ // the attributes are the identity matrix
        lp = in.pointattributelist;
        for(vector<RXSitePt>::iterator it= inpts.begin(); it!=inpts.end(); ++it)  {
            *lp = 1.0; lp+=(in.numberofpoints +1);
        }
    }
    else {
        lp = in.pointattributelist;
        for(vector<RXSitePt>::iterator it= inpts.begin(); it!=inpts.end(); ++it)  {
            *lp = 1.0; lp++;
        }
    }// no regenpts


    in.pointmarkerlist = (int *) calloc(in.numberofpoints , sizeof(int));
    ilp = in.pointmarkerlist;
    i=0;
    for(vector<RXSitePt>::iterator it= inpts.begin(); it!=inpts.end(); ++it)  {
        *ilp = i++;   ilp++;
    }

    in.numberofholes = 0;
    if(0) { // this was copied from the distro 'tricall.c'
        in.numberofregions = 2;
        in.regionlist = (REAL *) malloc(in.numberofregions * 4 * sizeof(REAL));
        in.regionlist[0] = 10.;
        in.regionlist[1] = 1.0;
        in.regionlist[2] = 7.0;            /* Regional attribute (for whole mesh). */
        in.regionlist[3] = 0.1;          /* Area constraint that will not be used. */
        in.regionlist[4] = 12.0;
        in.regionlist[5] = 1.0;
        in.regionlist[6] = 7.0;            /* Regional attribute (for whole mesh). */
        in.regionlist[7] = 0.01;          /* Area constraint that will not be used. */
    }
    else {
        in.numberofregions = 0;
        in.regionlist =0;
    }
    if(m_b->verbose>3){
        cout<< "Input point set:\n\n"<<endl;
        report(stdout,&in, 1, 0, 1, 1, 1, 1);
    }
    /* Make necessary initializations so that ShewchukTriangle can return a */
    /*   triangulation in `mid' and a voronoi diagram in `vorout'.  */

    mid.pointlist = (REAL *) NULL;            /* Not needed if -N switch used. */
    /* Not needed if -N switch used or number of point attributes is zero: */
    mid.pointattributelist = (REAL *) NULL;
    mid.pointmarkerlist = (int *) NULL; /* Not needed if -N or -B switch used. */
    mid.trianglelist = (int *) NULL;          /* Not needed if -E switch used. */
    /* Not needed if -E switch used or number of triangle attributes is zero: */
    mid.triangleattributelist = (REAL *) NULL;
    mid.neighborlist = (int *) NULL;         /* Needed only if -n switch used. */
    /* Needed only if segments are output (-p or -c) and -P not used: */
    mid.segmentlist = (int *) NULL;
    /* Needed only if segments are output (-p or -c) and -P and -B not used: */
    mid.segmentmarkerlist = (int *) NULL;
    mid.edgelist = (int *) NULL;             /* Needed only if -e switch used. */
    mid.edgemarkerlist = (int *) NULL;   /* Needed if -e used and -B not used. */

    /* Triangulate the points.  Switches  to read and write a  */
    /*   PSLG (p),  number everything from  */
    /*   zero (z), assign a regional attribute to each element (A), and  */
    /*   produce an edge list (e), a Voronoi diagram (v), and a triangle */
    /*   neighbor list (n).
                  that gives <"pczAevn"  >*/
    /*
                  WE want: ( jPzYpa0.0002DO )
                  constrained Delaunay triangulation.
                  */

    try{
        ///////////////////////////////////////////////////////////////////
        triangulate(buf , &in, &mid, NULL , m_m,m_b);
        ////////////////////////////////////////////////////////////////////
        // if it fails we could try -i
    }//end try
    catch (RXException e) {
        e.Print(stdout);
        WriteAsPoly(&in,"inExceptionRXE");rc=0;
    }
    catch( char * str ) {
        printf( "Exception raised:  <%s>\n",str);
        WriteAsPoly(&in,"inExceptionStr");rc=0;
    }
    catch( unsigned int ex ) {
        printf( "Exception raised: no= %u\n",ex);
        WriteAsPoly(&in,"inExceptionui");rc=0;
    }
#ifdef MICROSOFT
    catch(CException *ex) {
        wchar_t buf[1024];
        ex->GetErrorMessage(buf,1024);
        cout<< "(Triangle) Cexception " << buf << endl;
        WriteAsPoly(&in); rc=0;
    }
#endif
    catch(...) { // usually get here
        cout<< "(Triangle) otherexception (4)"<<endl;
        WriteAsPoly(&in,"inExceptionoTher"); rc=0;
    }
    /* Free all allocated arrays, including those allocated by ShewchukTriangle. */

    free(in.pointlist);
    free(in.pointattributelist);
    free(in.pointmarkerlist);
    free(in.segmentmarkerlist);
    if(in.regionlist )free(in.regionlist);
    free(in.segmentlist );
    return rc;
}


void RXMesh::LocateInitialize(){

    if(this->m_t->numberofpoints<3){ cout<<"cant locateInitialise on empty mesh"<<endl; return;}
    maketriangle(m_m,m_b, &m_searchtri);
    setorg(m_searchtri, m_m->infvertex1);
    setdest(m_searchtri, m_m->infvertex2);
    setapex(m_searchtri, m_m->infvertex3);
}
int RXMesh::Locate(const ON_2dPoint ppp,int*ii,double*ww) { //seeds on m_searchtri
    //cout << "When this returns an ID to the tri its finished"<<endl;
    REAL searchpoint[2];
    vertex p1,p2,p3;
    double dist1,dist2;//,wtot;
    enum locateresult res;

    searchpoint[0]= ppp.x ; searchpoint[1]=ppp.y;
    //cout <<"try to locate 2D point at "<<ppp.x<<" , "<< ppp.y<<" ...";
    try{
        res = locate(m_m,m_b, searchpoint, &m_searchtri );
        if(res==BAD)
            return BAD;
        org (m_searchtri,p1); // p[0]= p1 ;
        dest(m_searchtri,p2);//  p[1]= p2;
        apex(m_searchtri,p3);//  p[2]= p3;
        ii[0] = myvertexmark(p1);
        ii[1] = myvertexmark(p2);
        ii[2] = myvertexmark(p3);
        if( m_m->eextras){
            REAL att = myelemattribute(m_searchtri, 0,m_m) ;
        }
        //  search for number of triangle attributes from .ele file.
        switch(res) {
        case INTRIANGLE:
        case OUTSIDE :
            ww[0]= counterclockwise(m_m,m_b, p2, p3, searchpoint)/2.0;
            ww[1]= counterclockwise(m_m,m_b, p3, p1, searchpoint)/2.0;
            ww[2]= counterclockwise(m_m,m_b, p1, p2, searchpoint)/2.0;
            // wtot = ww[0]+ww[1]+ww[2]; ww[0]/=wtot; ww[1]/=wtot;ww[2]/=wtot;
            switch(res){
            case OUTSIDE :
                //cout<<"its outside"<<endl;
                this->LocateInitialize(); break;
            case INTRIANGLE:
                //cout<<"its in a tri"<<endl;
                break;
            };
            //cout <<  p1[0]<<","<<p1[1]<<endl<<  p2[0]<<","<<p2[1]<<endl<<  p3[0]<<","<<p3[1]<<endl;
            break;
        case ONEDGE:
            //cout<<"its on an edge. orient = "<<m_searchtri.orient;
            //cout <<  " between "<< p1[0]<<","<<p1[1] ;
            //cout <<  " and pt  "<< p2[0]<<","<<p2[1]<<endl;

            dist1 = ((p1[0] - searchpoint[0]) * (p1[0] - searchpoint[0]) +
                     (p1[1] - searchpoint[1]) * (p1[1] - searchpoint[1]));
            dist2 = ((p2[0] - searchpoint[0]) * (p2[0] - searchpoint[0]) +
                     (p2[1] - searchpoint[1]) * (p2[1] - searchpoint[1]));
            dist1=sqrt(dist1); dist2=sqrt(dist2);
            ww[0] = dist2 /(dist1+dist2);
            ww[1] = dist1 /(dist1+dist2); ww[2]=0.;
            break;
        case ONVERTEX:
            ww[0]=1.0; ww[1]=ww[2]=0;
        } //switch
    }//try
    catch(...) {
        cout<<" failed to locate "<<ppp.x<<" "<<ppp.y <<endl;
        res = BAD;
    }
    double tot = 0; int m2;
    for(int k=0;k<3;k++) {
        int marker = this->m_t->pointmarkerlist[ii[k]];// outside
        if( NodeNo.find(ii[k] ) != NodeNo.end() )
        {
            m2=NodeNo[ii[k]] ;
            if(marker!=m2)
                qDebug()<<"NodeNoMismatch marker="  <<marker<<"m2="<<m2 <<" (we handle marker<0)" ;
            //            res=BAD; return (res);
            assert( marker == m2);
        }
        if(marker<0){
            ii[k]=-1; ww[k]=0.;
        }
        else
            ii[k]=marker;
        tot+=ww[k];
    }
    if(tot<ON_EPSILON)
        return BAD;
    ww[0]/=tot; 	ww[1]/=tot; 	ww[2]/=tot;

    return res;

}
//int RXMesh::L ocate(const ON_Point *op,int*ii,double*ww){

//    ON_2dPoint ppp(op->point );
//    return L ocate(ppp,ii,ww);
//}
int RXMesh::WriteAsPoly(struct triangulateio *io,const char*text) const{
    // write   as a poly
    int i, j;
    char fname[512]; sprintf(fname,"%s.poly",text);// owner->name() );
    FILE *fp = RXFOPEN(fname,"w");
    cout<< "write Mesh as poly "<<fname <<endl;
    if(!fp) return 0;
    fprintf(fp,"#  debug poly file written by OpenFSI.org\n");
    fprintf(fp,"# <# of vertices> <dimension (must be 2)> <# of attributes><# of boundary markers (0 or 1)>\n");
    fprintf(fp,"%d %d %d %d\n", io->numberofpoints,2,io->numberofpointattributes,0);
    for (i = 0; i < io->numberofpoints; i++) {
        fprintf(fp,"%d", i);
        for (j = 0; j < 2; j++) {
            fprintf(fp,"  %.6g", io->pointlist[i * 2 + j]);
        }
        for (j = 0; j < io->numberofpointattributes; j++) {
            fprintf(fp,"  %.6g",
                    io->pointattributelist[i * io->numberofpointattributes + j]);
        }
        fprintf(fp,"\n");
    }
    fprintf(fp,"\n# the segments\n#  <# of segments> <# of boundary markers (0 or 1)>\n");
    fprintf(fp,"%d %d\n", io->numberofsegments,0);

    for (i = 0; i < io->numberofsegments; i++) {
        fprintf(fp,"%4d", i);
        for (j = 0; j < 2; j++) {
            fprintf(fp,"  %4d", io->segmentlist[i * 2 + j]);
        }
        fprintf(fp,"\n");
    }
    fprintf(fp,"\n# no holes\n 0 \n# skipping any regions and regional atts\n");

    FCLOSE(fp);
    return 1;
}// RXMesh::WriteAsPoly

void RXMesh::report(FILE *fp, struct triangulateio *io,
                    int markers,int reporttriangles,int reportneighbors,int reportsegments,
                    int reportedges, int reportnorms)
{
    int i, j;
    if (m_b->verbose <=0){ fprintf(fp,"RXMesh detail report turned off\n"); return;}

    for (i = 0; i < io->numberofpoints; i++) {
        fprintf(fp,"Point %4d:", i);
        for (j = 0; j < 2; j++) {
            fprintf(fp,"  %.6g", io->pointlist[i * 2 + j]);
        }
        if (io->numberofpointattributes > 0) {
            fprintf(fp,"   attributes");
        }
        for (j = 0; j < io->numberofpointattributes; j++) {
            fprintf(fp,"  %.6g",
                    io->pointattributelist[i * io->numberofpointattributes + j]);
        }
        if (markers && io->pointmarkerlist) {
            fprintf(fp,"   marker %d\n", io->pointmarkerlist[i]);
        } else {
            fprintf(fp,"\n");
        }
    }
    fprintf(fp,"\n");

    if (reporttriangles || reportneighbors) {
        for (i = 0; i < io->numberoftriangles; i++) {
            if (reporttriangles) {
                fprintf(fp,"Tri angle %4d points:", i);
                for (j = 0; j < io->numberofcorners; j++) {
                    fprintf(fp,"  %4d", io->trianglelist[i * io->numberofcorners + j]);
                }
                if (io->numberoftriangleattributes > 0) {
                    fprintf(fp,"   attributes");
                }
                for (j = 0; j < io->numberoftriangleattributes; j++) {
                    fprintf(fp,"  %.6g", io->triangleattributelist[i *
                            io->numberoftriangleattributes + j]);
                }
                fprintf(fp,"\n");
            }
            if (reportneighbors) {
                fprintf(fp,"Tri angle %4d neighbors:", i);
                for (j = 0; j < 3; j++) {
                    fprintf(fp,"  %4d", io->neighborlist[i * 3 + j]);
                }
                fprintf(fp,"\n");
            }
        }
        fprintf(fp,"\n");
    }

    if (reportsegments) {
        for (i = 0; i < io->numberofsegments; i++) {
            fprintf(fp,"Segment %4d points:", i);
            for (j = 0; j < 2; j++) {
                fprintf(fp,"  %4d", io->segmentlist[i * 2 + j]);
            }
            if (markers && io->segmentmarkerlist) {
                fprintf(fp,"   marker %d\n", io->segmentmarkerlist[i]);
            } else {
                fprintf(fp,"\n");
            }
        }
        fprintf(fp,"\n");
    }

    if (reportedges) {
        for (i = 0; i < io->numberofedges; i++) {
            fprintf(fp,"Edge %4d points:", i);
            for (j = 0; j < 2; j++) {
                fprintf(fp,"  %4d", io->edgelist[i * 2 + j]);
            }
            if (reportnorms && (io->edgelist[i * 2 + 1] == -1)) {
                for (j = 0; j < 2; j++) {
                    fprintf(fp,"  %.6g", io->normlist[i * 2 + j]);
                }
            }
            if (markers &&io->edgemarkerlist ) {
                fprintf(fp,"   marker %d\n", io->edgemarkerlist[i]);
            } else {
                fprintf(fp,"\n");
            }
        }
        fprintf(fp,"\n");
    }
} // report

rxON_Mesh * RXMesh::MakeONMeshUV(vector<RX_FESite*> &pts)
{
    rxON_Mesh *ro = new rxON_Mesh();

    int i=0, j, i0,c[4];
    //    qDebug()<<  "ONMeshUV takes vertices from GetN";
    //    qDebug()<<  "and face numbering from  NodeNo";
    vector<RX_FESite*>::const_iterator it;
    for(it=pts.begin(); it!=pts.end();++it) {
        RX_FESite*pp = *it;
        if(pp && ON_IsValid(pp->m_u )&& ON_IsValid(pp->m_v))
            ro->SetVertex(pp->GetN(),ON_3dPoint(pp->m_u,pp->m_v,0.));
        else
            ro->SetVertex(i,ON_3dPoint(-1,-1,-1));
        i++;
#ifdef DEBUG
        //if(pp) cout<< "vtx "<< pp->GetN()<<" "<< pp->x<<" "<<pp->y<<" "<<pp->z<<" "<<pp->m_u<<" "<<pp->m_v<<endl;
#endif
    }
    map<int,int>::iterator norev = NodeNo.end();
    int tc=0; assert( m_t->numberofcorners ==3);
    for (i = 0; i < m_t->numberoftriangles; i++) {
        i0 = i * m_t->numberofcorners;
        for (j = 0; j < m_t->numberofcorners; j++)
            c[j]=m_t->trianglelist[i0 + j];

        if(  norev == NodeNo.find(c[0])  )
            continue;
        if(  norev == NodeNo.find(c[1])  )
            continue;
        if(  norev == NodeNo.find(c[2])  )
            continue;
        ro->SetTriangle(tc++,NodeNo[c[0]],NodeNo[c[1]],NodeNo[c[2]]);
#ifdef _DEBUG
        // cout<< "cell "<<c[0]<<" "<< c[1]<<" "<<c[2]<< " , "<<NodeNo[c[0]]<<" "<< NodeNo[c[1]]<<" "<<NodeNo[c[2]]<<   endl;
#endif
    }
    return ro;
}

rxON_Mesh * RXMesh::MakeONMeshXYZ(vector<RX_FESite*> &pts)
{
    rxON_Mesh *ro = new rxON_Mesh();
    int i=0, j, i0,c[4];
    //   qDebug()<<  "ONMeshXYZ takes vertices from GetN";
    //   qDebug()<<  "and face numbering from  NodeNo";
    vector<RX_FESite*>::const_iterator it;
    for(it=pts.begin(); it!=pts.end();++it) {
        RX_FESite*pp = *it;
        if(pp)
            ro->SetVertex(pp->GetN(), pp->ToONPoint() );
        else
            ro->SetVertex(i,ON_3dPoint(-1,-1,-1));
        i++;
    }
    int tc=0;
    map<int,int>::iterator norev = NodeNo.end();
    for (i = 0; i < m_t->numberoftriangles; i++) {
        i0 = i * m_t->numberofcorners;
        for (j = 0; j < m_t->numberofcorners; j++)
            c[j]=m_t->trianglelist[i0 + j];
        if( norev == NodeNo.find(c[0]) || norev == NodeNo.find(c[1])|| norev == NodeNo.find(c[2]))
        {
            // commonly occurs on the triangles between the boundary and the 8 far points
            //   qDebug()<< "(MakeONMeshXYZ) Triangle "<<i<< " no NodeNo found "<< c[0]<<c[1]<<c[2];
            continue;
        }
        ro->SetTriangle(tc++,NodeNo[c[0]],NodeNo[c[1]],NodeNo[c[2]]);
    }
    return ro;
}

rxON_Mesh * RXMesh::MakeONMeshUV(vector<RXSitePt> &pts)
{
    rxON_Mesh *ro = new rxON_Mesh();

    int i=0, j, i0,c[4];
    //    qDebug()<<  "ONMeshUV takes vertices from vector<RXSitePt> &pts";
    //    qDebug()<<  "and face numbering from  NodeNo";
    vector<RXSitePt>::const_iterator it;
    for(it=pts.begin(); it!=pts.end();++it) {
        ro->SetVertex(i,ON_3dPoint(it->m_u,it->m_v,0.));
        i++;
    }
    int tc=0;
    map<int,int>::iterator norev = NodeNo.end();
    for (i = 0; i < m_t->numberoftriangles; i++) {
        for (j = 0; j < m_t->numberofcorners; j++) {
            i0 = i * m_t->numberofcorners;     c[j]=m_t->trianglelist[i0 + j];
        }
        if( norev == NodeNo.find(c[0])  ) continue;
        if( norev == NodeNo.find(c[1])  ) continue;
        if( norev == NodeNo.find(c[2])  ) continue;
        ro->SetTriangle(tc++,NodeNo[c[0]],NodeNo[c[1]],NodeNo[c[2]]);
    }
    return ro;
}
rxON_Mesh * RXMesh::MakeONMeshXYZ(vector<RXSitePt> &pts)
{
    rxON_Mesh *ro = new rxON_Mesh();

    int i=0, j, i0,c[4];
    qDebug()<<  "ONMeshXYZ takes vertices from vector<RXSitePt> &pts";
    qDebug()<<  "and face numbering from  NodeNo";
    vector<RXSitePt>::const_iterator it;
    for(it=pts.begin(); it!=pts.end();++it) {
        ro->SetVertex(i,ON_3dPoint(it->x,it->y,it->z));
        i++;
    }
    int tc=0;
    map<int,int>::iterator norev = NodeNo.end();
    for (i = 0; i < m_t->numberoftriangles; i++) {
        for (j = 0; j < m_t->numberofcorners; j++) {
            i0 = i * m_t->numberofcorners;     c[j]=m_t->trianglelist[i0 + j];
        }
        if(norev == NodeNo.find(c[0])  ) continue;
        if(norev == NodeNo.find(c[1])  ) continue;
        if(norev == NodeNo.find(c[2])  ) continue;
        ro->SetTriangle(tc++,NodeNo[c[0]],NodeNo[c[1]],NodeNo[c[2]]);
    }
    return ro;
}
