 /* pctable.h */

#ifndef _PCTABLE_
#define _PCTABLE_ 

#include <stdio.h>
#define VERTICAL 101
#define HORIZONTAL 102

struct PC_TABLE{
	  char***data;
	  int nr,nc;
}; 

EXTERN_C int PCT_Part_Of(const char*s1,const char*s2);
EXTERN_C int Append_Table_As_Text(FILE *f, char**text);
EXTERN_C int PCT_Which_Way(struct PC_TABLE *t,const char**h,int nh, const RXEntity_p e);
EXTERN_C int PCT_List_To_Double (SAIL *sail,char***list,int N,double**x,const char*type);
EXTERN_C int PCT_ListToDoubleByQ (SAIL *sail, char***list, int N, double**x, const wchar_t *type);
EXTERN_C int PCT_Extract_Series(struct PC_TABLE *t,int whichway,const char*h,int*N,char***s);
EXTERN_C int PCT_Read_Table( FILE *f,struct PC_TABLE**t,char sep);
EXTERN_C  int PCT_Read_Table_From_Text(const char*text,struct PC_TABLE**t);
EXTERN_C char PCT_Table_Separator(const char *fname);
EXTERN_C int  PCT_Free_Table(struct PC_TABLE *t);
EXTERN_C int  PCT_Print_Table(FILE *fp,struct PC_TABLE *t);
	 
#endif //#ifndef _PCTABLE_
