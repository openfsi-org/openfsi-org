#ifndef _HDR_VELOCITY_
#define  _HDR_VELOCITY_

//#include "griddefs.h"

#ifndef USE_PANSAIL
#error("HDR_VELOCITY ");
#endif
struct PC_VELOCITY{
	RXEntity_p s;
	float x,y,z,u,v,w;
};

typedef struct PC_VELOCITY	 Velocity;


EXTERN_C int Compute_All_Velocities(SAIL *s); 
EXTERN_C int Resolve_Velocity_Card(RXEntity_p e);
EXTERN_C int Compute_Velocity(RXEntity_p p);

#endif  //#ifndef _HDR_VELOCITY_
