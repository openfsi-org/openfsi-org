#include "StdAfx.h"
#include "RelaxWorld.h"
// lots of includes for Resolve
#include "RXSail.h"
#include "RXSRelSite.h"
#include "RXPolyline.h"
#include "RXOffset.h"
#include "RXSeamcurve.h"
#include "RXCompoundCurve.h"
#include "RXLogFile.h"
#include "RLX3dm.h"
#include "rlx3dmEntities.h"
#include "rxsubwindowhelper.h"
#include "RXRelationDefs.h"

#include <QStringList>

#include "griddefs.h"
#include "resolve.h"
#include "global_declarations.h"
#include "etypes.h"
#include "iangles.h"
#include "rxbcurve.h"
#include "aero.h"
#include "aeromik.h"
#include "alias.h"
#include "arclngth.h"
#include "batpatch.h"
#include "batstiff.h"
#include "boundary.h"
#include "camera.h"
#include "cam2.h"
#include "connect_f.h"
#include "delete.h"
#include "dxf.h"
#include "editword.h"
#include "fields.h"
#include "hoopcall.h" // for compute relative filename
#include "gcurve.h"
#include "gle.h"

#include "interpln.h"
#include "material.h"
#include "panel.h"
#include "paneldata.h"
#include "pansin.h"

#include "pressure.h"
#include "printall.h"
#include "ReadBagged.h"

#include "geodesic.h"
#include "RX_UI_types.h"
#include "rlx3dmbuffer.h"
#include "RXContactSurface.h"
#include "RXdovefile.h"
#include "RXPanel.h"
#include "RXPside.h"
#include "RX_FEPocket.h"  
#include "RX_FESite.h"
#include "RX_FEString.h"  
#include "RX_PCFile.h"   
#include "ReadStripe.h"

#include "targets.h"
#ifdef USE_PANSAIL
#include "velocity.h"
#endif
#include "batdefs.h"

#include "entities.h"
#include "stringutils.h"
// end includes for Resolve
#ifdef linux
#include "text.h"
#include "script.h"
#endif

#include "RXEntityDefault.h"

int Peter_Debug=0;

RXEntityDefault::RXEntityDefault(void)
    : m_dataptr(NULL)
{
}

RXEntityDefault::~RXEntityDefault(void)
{
    this->CClear();
}

void* RXEntityDefault::DataPtr(void) const
{
    return this->m_dataptr;
}

int RXEntityDefault::PC_Finish_Entity(const char*typein,const char*namein,void*dp,HC_KEY hk,struct LINKLIST *plist,const char *attin,const char*linein)
{
    int rc = RXEntity::PC_Finish_Entity(typein,namein,dp,hk,plist,attin,GetLine());
    this->SetDataPtr(dp);
    return rc;
}
int RXEntityDefault::Compute()
{
    int flag=0;
    int n;
    string sailt;int length;
    switch ( this->TYPE) {

    case DXF:{
        flag=Compute_DXF(this);
        break;
    }
    case PCE_CONSURF:{
        RXContactSurface *l_cs = (RXContactSurface *) this->DataPtr();
        flag=l_cs->Compute();
        break;
    }
    case INTERPOLATION_SURFACE:{
        flag= Compute_Interpolation_Card(this);
        break;
    }
    case PANSAIL :{
        flag= 2; // do nothing, but there may be a dependent coordinate system
        break;
    }
    case  PCE_SETDATA :{
        flag= 2;
        flag+=this->OnValueChange();
        break;
    }
    case FABRIC :
    case  MATERIAL :{
        flag= Compute_Material_Card(this);
        break;
    }
    case CONNECT:
        sailt = this->Esail->GetType(); length = (int) sailt.length();
        if(this->AttributeGetInt("n",&n))
            cf_unresolveOneConnect(n);
        else
            cf_unresolve_connects(sailt.c_str(),&length )  ;  // safe, but not always needed. cf_analysis_prep() will recover from this
        flag=0;
        this->Esail->SetFlag(FL_NEEDS_TRANSFORM);
        break;
    case FIELD:
    default:
        flag+=this->OnValueChange(); // GUESS for Field - Step it
        break;
    };
    return flag;
}  //RXEntityDefault::Compute

int RXEntityDefault::Resolve () {
    assert(!IsInStack() );
    SetInStack (true);

    const char *line;
    char Firstword[256],  second[256];
    int  etype, nc=0;
    if(this->GetLine())
        Get_First_Words(this->GetLine(), Firstword,second);
    else
        strcpy(Firstword , this->type());
    etype = INTEGER_TYPE(Firstword);
    if(this->TYPE !=etype)
        printf(" TYPE mismatch . TYPE=%d etype = %d fw=%s line=%s\n", this->TYPE ,etype, Firstword ,this->GetLine()  );
    // if this were ALWAYS true we could skip INTEGERTYPE as a speed improvement.
    // BUT when we change rig dimensions, it fires.

    line=this->GetLine();

    switch (etype) {
    case PCE_IGES:
        g_CurrentIncludeFile = this; Push_Entity( this, &g_parList);
        nc+= Resolve_iges_Card(this);
        if(!Pop_Entity( (&g_CurrentIncludeFile), &g_parList))
            g_CurrentIncludeFile = NULL;
        if(g_parList) g_CurrentIncludeFile = (RXEntity_p ) g_parList->data;
        else  g_CurrentIncludeFile = NULL;
        break;
    case DXF:
        g_CurrentIncludeFile = this; Push_Entity( this, &g_parList);
        nc+= Resolve_Dxf_Card(this);
        if(!Pop_Entity( (&g_CurrentIncludeFile), &g_parList))
            g_CurrentIncludeFile = NULL;
        if(g_parList) g_CurrentIncludeFile = (RXEntity_p ) g_parList->data;
        else  g_CurrentIncludeFile = NULL;
        break;
    case PCE_3DM:
#ifndef _NO_ON
        if(!g_Janet) {	g_CurrentIncludeFile = this; Push_Entity( this, &g_parList);   }

        nc+= Resolve_3DM_Card(this);
        if(!g_Janet) {
            if(!Pop_Entity( (&g_CurrentIncludeFile), &g_parList))
                g_CurrentIncludeFile = NULL;
            if(g_parList) g_CurrentIncludeFile = (RXEntity_p ) g_parList->data;
            else  g_CurrentIncludeFile = NULL;
        }
        else cout<< "3dm resolve finished"<<endl;
        break;
#endif
    case PCE_PCFILE: // currentinclude file gets slow when the PCfile is big. so we dont use it - which is WRONG.
        cout<< "call Resolve_PCFile_Card"<<endl;
        nc+= Resolve_PCFile_Card(this);
        cout<< "PCFile resolve finished"<<endl;
        break;

    case PCE_PRESSURE :
        nc+=Resolve_Pressure_Card(this);
        break;

    case PCE_DOVE :
#ifdef RX_USE_DOVE
        nc +=Resolve_Dove_File(e);
#endif
        break;
    case PCE_CONSURF:
        nc+=RXContactSurface::Resolve(this);
        break;
    case 99:
    default:
        if (strieq(Firstword,"question")) {/* do nothing */
            this->Needs_Resolving=0;  /* they are special. Dont worry */
        }
        else if (strieq(Firstword,"Linear Substructure")) {
            nc+= Resolve_Gle_Card(this);
        }
        else if (strieq(Firstword,"mould")) {
            nc+= Resolve_Mould_Card(this);
        }
        else if (strieq(Firstword,"pansail")) {
            nc+= Resolve_Pansail_Card(this);
        }
        else if (strieq(Firstword,"material")) {
            nc+= Resolve_Material_Card(this);
        }
        else if (strieq(Firstword,"fix")) {
            nc+= Resolve_Fix_Card(this);
        }
        else if (strieq(Firstword,"field")) {
            nc+= Resolve_Field_Card(this);
        }
        else if (strieq(Firstword,"interpolation surface")) {
            nc+= Resolve_Interpolation_Card(this);
        }
        else if (stristr(Firstword,"stripe")) {
            nc+= Resolve_Stripe_Card(this);
        }
#ifdef USE_PANSAIL
        else if (strieq(Firstword,"windvane")) {
            nc+= Resolve_Velocity_Card(this);
        }
#endif
        else if (strieq(Firstword,"connect")) {
            int  lm, ls;
            void *ptr;
            std::string l_type;
            // strip leading and trailing.
            QString qline ( this->GetLine());
            QStringList wds= rxParseLine(qline,RXENTITYSEPREGEXP );
            qline = wds.join(RXENTITYSEPS);
            this->SetLine(qPrintable(qline));

            l_type = this->Esail->GetType();

            this->Needs_Resolving=0;  /// instead of the Resolve.
            ptr = this;
            lm = (int) l_type.size ();
            this->SetLineLwr();	// because fortran character testing is case sensitive
            ls = (int) strlen(this->GetLine());

#ifdef FORTRANLINKED
            int n = cf_create_connect(l_type.c_str(), &lm, this->GetLine(), &ls,&ptr); //HORRIBLE
            QString a = QString("n=%1").arg(n);
            this->AttributeAdd(a);
#endif
        }

        else if (strieq(Firstword,"camera")) {
            nc+= Resolve_Camera_Card(this);
        }

        else if (strieq(Firstword,"input")) {
            nc+= Resolve_Input_Card(this);
        }
        else if (strieq(Firstword,"rename")) {
            int ret=Resolve_Rename_Card(this);
            if(1 || ret) { // Peter maybe WRONG July 00
                nc+=ret;
            }
        }
        else if (strieq(Firstword,"alias")) {
            nc+=Resolve_Rename_Card(this);
        }
        else if (strieq(Firstword,"do_boat")) {
            /* do nothing */
        }
        else if (strieq(Firstword,"summary")) {
            nc+=Resolve_Summary_Card(this);
        }
        else if (strieq(Firstword,"boundary")) {
            nc+=Resolve_Boundary_Card(this);
        }
        else if (strieq(Firstword,"patch")) {
            nc+=Resolve_Patch_Card(this);
        }

        else if (strieq(Firstword,"batten")) {
            nc+=Resolve_Batten_Card(this);
        }
        else if (strieq(Firstword,"fabric")) {
            nc+=Resolve_Fabric_Card(this);
        }
        else if (strieq(Firstword,"regionData")) {
            nc+=Resolve_RegionData_Card(this);
        }
        else if (strieq(Firstword,"set")) {
            nc+=Resolve_Set_Card(this);
        }
        else if (strieq(Firstword,"edit")) {
            RXEntity_p dum;
            nc+=Resolve_Edit_Card(this,&dum);
        }
        else if (strieq(Firstword,"editword")) {
            nc+=Resolve_EditWord_Card(this);
        }
        else if (strieq(Firstword,"delete")) {
            nc+=Resolve_Delete_Card(this);
        }
        else if (strieq(Firstword,"include")) {
            this->Needs_Resolving=0;
            cout<< " WHY does an include Need Resolving\n"<<endl;
        }
        //else if (strieq(Firstword,"panelfab")) {
        //  nc+=Resolve_PanelFab_Card(this);
        //}
        else if (strieq(Firstword,"aeromik")) {
            nc+=Resolve_Aeromik_Card(this);
        }
        else if (strieq(Firstword,"aero")) {
            nc+=Resolve_Aero_Card(this);
        }
        else if (strieq(Firstword,"target")) {
            nc+=Resolve_Goalseek_Card(this);
        }
        else if (strieq(Firstword,"variable")) {
            nc+=Resolve_Goalseek_Card(this);
        }
        else if (strieq(Firstword,"measure")) {
            nc+=Resolve_Goalseek_Card(this);
        }
        else { // no type
            size_t ll; char*string;
            const char  *a,*b;
            if(line) {ll = strlen(line); a = line;} else { ll=0; a="ph_null";}

            if(!rxIsEmpty(Firstword)){ ll += strlen(Firstword); b = Firstword;} else b = "blank";
            string=(char *)MALLOC((64+ll)*sizeof(char));
            sprintf(string,"(Resolve) line <%s> (fw='%s') NOT UNDERSTOOD",a,b);
            rxerror(string,1);
            RXFREE(string);
            if(this->Esail )
                this->Kill(); //e->Delete Entity(e->Esail );
            else {cout<< " incomplete entity"<<endl;}
        } // no type
    }
    SetInStack (false);
    return nc;

} //RXEntityDefault::Resolve


int RXEntityDefault::ReWriteLine(void){// this routine rewrites the line from the entity data. In case the line and the data are out-of synch

    // This is where we should Compute_Relative_Filename on:
    //  pansail, tpn, iges, dxf, include, analysis, etc
    //   RXEntity_p this	 =this;
    std::string NewLine;
    //  ON_String l_line;
    RXObject*old ;
    RXExpressionI *le ;
    //  char str[128];
    QString qline(this->GetLine());

    switch (this->TYPE) {

    case PCE_DOVE:
    {
#ifdef RX_USE_DOVE
        RXdovefile *theDove = (RXdovefile *) this->dataptr;
        ON_String l_s = theDove->rewrite_doveline();
        RXFREE(this->line); this->line=STRDUP(l_s.Array());
#endif

        break;
    }
        // a note on 'include' filenames
        //  In a model script there can be references to other script files, CADfiles, etc.
        //   In the bag and script files we try to keep these filenames in a portable form.
        //  filenames can begin with an environment (eg $R3ROOT/data/bcurves.txt)   see
        // or they can be absolute paths
        // or they can be relative paths.
        //  we can keep certain environment vars in the internal record of a filename
        // because FOPEN calls filename_copy_local which maps the env variable to its value.

        //  So when we resolve one of these include-entities ( the filename is happily always word[2])
        // may be $...  This is mapped and accepted by VerifyFileName
        // or may be a  path which is relative to the dir holding the model scriptfile
        // so we must precede VerifyFileName  by  fn = this->Esail->AbsoluteFileName(fn);

        //case PCE_TPN: // kw,filename,[  prompt [ pattern]]
    case PCE_3DM: //   kw,name filename atts command
    case PCE_IGES:  // kw,name filename atts command
    case DXF	:   // kw,name,filename,  sc,command
    case PCE_PCFILE:
    {

        QStringList qsl = qline.split(RXENTITYSEPS );
        for (QStringList::iterator it = qsl.begin(); it != qsl.end(); ++it)
            *it = (*it).trimmed();
        qsl<<" "<< " "<<" ";
        QString fn = qsl.at(2);
        fn = this->Esail->RelativeFileName(fn);
        qsl.replace(2,fn);
        qsl.replace(3,this->AttributeString());
        qline = qsl.join(RXENTITYSEPSTOWRITE);
        this->SetLine(qPrintable(qline));
        break;
    }
    case MSHFILE  : // for these 4 it's kw;name:filename
    case DATFILE :
    case ANALYSISFILE :
    case INFILE :
    case INCLUDE: { // kw:name:<filename> (should be :[prompt]:[command])
        QStringList qsl0, qsl1,qsl2 ;
        QString qline(this->GetLine()), newline,fname,ename;
        // 1  parse out any trailing comment
        qsl0 = qline.split("!");
        //2 get the words
        qsl1 = qsl0[0].split(QRegExp("[:\t]" ) );
        for (QStringList::iterator it = qsl1.begin(); it != qsl1.end(); ++it)
            *it = (*it).trimmed();
        while(qsl1.last().isEmpty())
            qsl1.removeLast();

        if(qsl1.count() !=3) {
            fname = this->Esail->RelativeFileName( qsl1.last().trimmed()  );
            //  decide a name
            if(qsl1.count() >3) ename = qsl1[1] ;
            else ename = qsl1.last();
            this->Rename(qPrintable(ename),this->type());
            qsl2<<this->type()<<this->name()<<fname;
            if(qsl0.count()>1)
                qsl2<<  qsl0[1]   ;
            newline = qsl2.join(RXENTITYSEPS ) ;
            if(qsl0.count()>1)
                newline+= "!"+qsl0[1];
            else
                newline+= "! grandfathered";
            this->SetLine(qPrintable( newline));

        }
        //coalFace
        //        QString fname, s2, s(e->line);
        //        QString newline;
        //        k=s.ReverseFind(':');
        //        if(k >=0) {
        //            s2=s.Left(k);
        //            fname = s.Mid(k+1);
        //            QString sq = this->Esail->RelativeFileName(fname.Array() );
        //            newline = s2 + ON_String(RXENTITYSEPSTOWRITE) + ON_String(qPrintable(sq));
        //            RXFREE(e->line);
        //            e->line=(char*) MALLOC(s.Length() + 2* newline.Length () + 512);
        //            strcpy(e->line,qPrintable(sq)); //s.Array());
        //            strcat(e->line,"\n! debug>> ");
        //            strcat(e->line,s.Array());
        //            s.Destroy ();
        //            newline.Destroy ();
        //            s2.Destroy ();
        //            fname.Destroy ();
        //        }
        break;
    }

    case PCE_SAIL: {
        //   0  NAME:
        //1	main:
        //2	description:
        //3	x:
        //4	y:
        //5	z
        //6	[:attributes]
        ON_String ttt = this->Esail->Serialize();
        this->SetLine(ttt.Array());
        break;
    }

    case PCE_SETDATA:
        /* Set card is of type
    set:  variable : value [: <nodal address>]       ! comment */
        NewLine;
        old =this->GetOneRelativeByIndex(RXO_EXP_OF_SET);
        le  = dynamic_cast<RXExpressionI *>(old);
        NewLine+=  this->type();
        NewLine+=  RXENTITYSEPSTOWRITE  ;
        NewLine+= this->name();
        NewLine+=  RXENTITYSEPSTOWRITE  ;

        if(le) {
            NewLine+=   ToUtf8( le->GetText() );
            NewLine+=  RXENTITYSEPSTOWRITE ;
            if(le->GetOwner() == dynamic_cast<  class RXENode *>(this->Esail))
                NewLine+= "?sail";
            else  if(le->GetOwner() == dynamic_cast<  class RXENode *>(g_World))
                NewLine+= "global";
            else
                NewLine+= "global ! (no ownernode)";
        }
        this->SetLine(NewLine.c_str());
        break;
    case CONNECT:
        break;
    default:
        //       cout<<" DONT rewrite line on RXEntityDefault:" <<e->TYPE<<", "<<e->line<<endl;
        break;
    }

    return(1);
}

int RXEntityDefault::Finish()
{
    int rc=0;
    assert(0);
    return rc;
} // RXEntityDefault::Finish


int RXEntityDefault::Dump( FILE *fp) const
{
    const RXEntityDefault* p=this;
    static int level = 0;
    int ii,j,k,side  ;

    float sum;
#ifdef _DEBUG
    //	 char l_dbg_str[256];
#endif
    if(level>0)
        return 0;

    level ++;

    if(!p) return 0;

    if(p->dataptr)  {
        assert(p->TYPE);

        switch (p->TYPE) {
        case PCE_CONSURF:
            RXContactSurface * l_cs;
            l_cs = (RXContactSurface * ) p->dataptr;
            l_cs->Print(fp);
            break;
        case  PCE_DOVE:	{
#ifdef RX_USE_DOVE
            RXdovefile *theDove = ( RXdovefile *) p->dataptr;
            theDove->Print(fp);
#endif
            break;
        }
        case PCE_SETDATA:{
            RXExpressionI *le =    	(RXExpressionI *) DataPtr();
            if(le) {
                fprintf(fp,"its own expression...\n");
                le->Print(fp);
                double x = le->evaluate ();
                fprintf(fp," evaluates to %f\n", x);
            }
            break;
        }
        case PC_PANELDATA:{
            struct PANELDATA *np =  (struct PANELDATA*) p->dataptr;
            if(np->owner)
                fprintf(fp," owner = %s\n",np->owner->name());
            else
                fprintf(fp," no owner this paneldata\n");
            fprintf(fp," no in list=%d\n", np->c);
            for (k=0;k<np->c; k++){
                fprintf(fp,"%d  %s\n", k,np->list[k]->name());
            }
            if(np->mat) fprintf(fp," mat = %s\n",np->mat->name());
            if(np->refSC) fprintf(fp," ref = %s\n",np->refSC->name());
            break;
        }
        case INTERPOLATION_SURFACE :{
            struct PC_INTERPOLATION *np;
            //void *l_estart=NULL;
            np = (struct PC_INTERPOLATION *) p->dataptr;

            fprintf(fp,"TABLE\n");
            fprintf(fp," Line Length %d\n", (int) strlen(p->GetLine()));
            fprintf(fp,"       Type   %s\n",np->type);
            fprintf(fp," Power Factor %f\n",np->Scale_Power);
            if(np->refSC)  fprintf(fp," RefSC   %s\n",np->refSC->name());
            else 			 fprintf(fp," NO RefSC	 \n");

            fprintf(fp,"   Nsites   %d\n",np->nsites);
            if(np->data) {
                fprintf(fp,"yes there is interpolation data here but its not written out\n");
                //l_estart=np->data;
            }
            if(np->NurbFlag)  {
                struct PC_NURB *nurb= np->NurbData;
                fprintf(fp,"this is a NURB surface\n");
                if(nurb&&(!nurb->m_pONobject)) {
                    fprintf(fp,"  LimitU = %d LimitV=%d\n", nurb->LimitU, nurb->LimitV) ;
                    fprintf(fp,"  ku = %d Kv=%d\n", nurb->KValU, nurb->KValV) ;
                    fprintf(fp," knotN,knotK %d %d \n",	 nurb->knotN,nurb->knotK);
                    fprintf(fp," Interpolatory=%d\n", nurb->Interpolatory);
                    PCN_Print_Nurb_Table(nurb,fp);
                    PCN_Print_Nurbs_Samples((RXEntity_p)p,fp);
                    PCN_Print_Nurb_Knots (nurb,fp);

                } else 	fprintf(fp,"  NULL nurb data\n");
            }
            else {
                double u,v,x,y,z,h;
                fprintf(fp,"\n");
                fprintf(fp,"v =\t    ");
                for(v=0.0;v<=1.0;v=v+(float)0.1)  fprintf(fp,"\t %9.4f ",v);
                fprintf(fp,"\n");
            }
            break;
        }
        case PCE_IGES: {
            struct PC_iges *np = ( struct PC_iges *)p->dataptr;
            iges_print(fp,np );
            break;
        }
        case PCE_PCFILE: {
            RX_PCFile *np = (RX_PCFile * )p->dataptr;
            np->PrintSummary(fp,1.0);
            break;
        }
        case  PCE_3DM : {
            fprintf(fp,"print not implemented for 3dm\n");
            /*				l_e = np->m_pOwner; // the command that calls the import
    if(l_e)
     fprintf(fp,"Owner %s %s\n",l_e->type(),l_e->name());
    else
     fprintf(fp," Owner NULL\n");
    l_e = np->m_pe; // The sail (Relax)
    if(l_e)
     fprintf(fp,"pe %s %s\n",l_e->type(),l_e->name());
    else
     fprintf(fp," pe NULL\n");
     fprintf(fp,"m_pModel = %p\n",np-> m_pModel); // The model

     fprintf(fp,"    name = %s\n",np->m_name);
     fprintf(fp,"filename = %s\n",np->m_filename);
     fprintf(fp," segname = %s\n",np->m_segname);
*/
            break;
        }
        case PCE_GLE: {
#ifndef WIN32

            char*buf;
            int i;// = (int) p->dataptr;
            //fprintf(fp,"  F index = %d\n", i);
            i = 199;

            cf_printallgle(&i);
            buf = readdata("fort.199");
            if(buf) {
                fprintf(fp,"file: fort.199\\n\n");
                fprintf(fp,"%s",buf);
                RXFREE(buf);
            }

#endif
            break;
        }

        case PCE_GOAL : {
            struct PC_GOALSEEK*np = (struct PC_GOALSEEK * )p->dataptr;
            double v = Target_Value(np);
            if(np->e) fprintf(fp," entity %s\n",np->e->name());
            fprintf(fp," points to %f\n",v);
            fprintf(fp," value     %f\n",np->value);
            fprintf(fp," oldvalue  %f\n",np->oldvalue);
            fprintf(fp," delta     %f\n",np->delta);
            fprintf(fp," tol       %f\n",np->tol);
            break;
        }
        case FABRIC:
        case MATERIAL :
        {
            struct PC_MATERIAL *np = (struct PC_MATERIAL * )p->dataptr;

            if(np->text) fprintf(fp," text : %s\n",np->text);
            fprintf(fp,"Stiffness \n");
            for(ii=0;ii<3;ii++) {
                fprintf(fp," \t");
                for(j=0;j<3;j++) fprintf(fp, "%f\t",np->d[ii][j]);
                fprintf(fp,"\n");
            }
            fprintf(fp," prestress   \t "); np->m_qprestress.Print(fp);

            fprintf(fp," wrinkle flag\t %d\n",np->creaseable);
            fprintf(fp," zk =        \t %16.11f\n",np->zk);
            fprintf(fp," linear flag \t %d\n",np->linear);
            if(np->table) fprintf(fp," table exists\n");
            if(!np->linear) {
                fprintf(fp,"   en       fn      es     fs\n");
                for(k=0;k<np->npn;k++) {
                    fprintf(fp,"    %f  %f  %f  %f\n",np->en[k],np->fn[k],np->es[k],np->fs[k]);
                }
                fprintf(fp,"  nu = %f  zk = %f\n",p->FindExpression(L"nu",rxexpLocal)->evaluate(),np->zk);
            }

            fprintf(fp," \n");
            break;
        }
        case PCE_PRESSURE: {
            Pressure * np = (Pressure *)p->dataptr;
            PCT_Print_Table(fp, np->m_table);
            break;
        }

        case FIELD :
        {
            struct PC_FIELD *field = (PC_FIELD *)(*p).dataptr;
            if( field->sc2) fprintf(fp,"       curve  1 %s \n",field->sc1->name());
            if( field->sc2) fprintf(fp,"       curve  2 %s \n",field->sc2->name());
            else fprintf(fp,"       curve  2 NULL \n");

            if( field->mat) fprintf(fp,"       material %s \n",field->mat->name());
            else            fprintf(fp,"       material  NULL\n");

            fprintf(fp,"       type()     %s \n",field->m_TyPe);
            fprintf(fp,"       flags= %d (  ", field->flag) ;
            if(field->flag&EVERYWHERE) fprintf(fp,"EVERYWHERE ")   ;
            if(field->flag&UNIFORM) fprintf(fp,"UNIFORM ")   ;
            if(field->flag&TRIANGLE) fprintf(fp,"TRIANGLE ") ;
            if(field->flag&RAMP) fprintf(fp,"RAMP  ") ;
            if(field->flag&COSINE) fprintf(fp,"COSINE  ") ;
            if(field->flag&COSSQ) fprintf(fp,"COSSQ ") ;
            if(field->flag&RADIAL) fprintf(fp,"RADIAL ") ;
            if(field->flag&INVERSE) fprintf(fp,"INVERSE") ;

            fprintf(fp," )\n") ;
            if(field->Local) 	fprintf(fp,"      Local Scope\n");
            else 		fprintf(fp,"      Global Scope\n");
            break;
        }
        case PCE_AERO : // needs checking against monarch
        {
            struct PC_AEROMIK *np = (PC_AEROMIK *)(*p).dataptr;

            if(np->before_script)	fprintf(fp," before    '%s'\n",np->before_script);
            if(np->after_script)	fprintf(fp," after     '%s'\n\n",np->after_script);
            k=0;
            fprintf(fp,"	sail	pressurefile	shapefile\n");
            while(np->sailnames[k]) {
                fprintf(fp,"          '%s'   '%s'   '%s'\n",np->sailnames[k],np->files[k],np->shapefiles[k]);
                k++;
            }
            break;
        }
            /*############################################################*/

        default:
        {

            if (strieq(p->type(),"alias")){
                struct PC_ALIAS *np = (PC_ALIAS *)p->dataptr;

                fprintf(fp,"       Type     %s \n",np->type);
                fprintf(fp,"       Name     %s \n",np->name);
                if(np->p)
                    fprintf(fp,"       refer to %s   a  %s\n",np->p->name(),np->p->type());
                else
                    fprintf(fp,"       refer to NULL\n");
            }
            else if (strieq(p->type(),"aeromik")){
                struct PC_AEROMIK *np = (PC_AEROMIK *)(*p).dataptr;

                fprintf(fp,"       Mikfile  %s \n",np->mikfilename);
                fprintf(fp,"       top,bot  %f %f \n",np->top,np->bottom);
                fprintf(fp,"      nsects,fa %d %d \n",np->nsects,np->fair);

                k=0;
                while(np->sailnames[k]) {
                    fprintf(fp,"          '%s'   '%s'\n",np->sailnames[k],np->files[k]);
                    k++;
                }
            }

            else if (strieq((*p).type(),"boundary")){
                struct PC_BOUNDARY *np = (PC_BOUNDARY *)(*p).dataptr;
                fprintf(fp,"       CORNERS\n");
                for(k=0;k<np->nc;k++) {
                    fprintf(fp,"       %d      %s\n",k,np->m_corners[k]->name());
                }
                fprintf(fp,"       EDGES\n");
                for(k=0;k<np->ne;k++) {
                    fprintf(fp,"       %d      %s\n",k,np->m_edges[k]->name());
                }
            }
            else if (strieq((*p).type(),"batten")){
                struct PC_BATTEN *np = (PC_BATTEN *)(*p).dataptr;
                int count= (*np).c;
                for (k=0;k<count;k++){
                    VECTOR v= ((*np).p)[k];
                    fprintf(fp,"\t\t\t%d %f\t%f\t%f ",k, v.x,v.y,v.z);
                    if(np->a) fprintf(fp," (  %f )",np->a[k]);
                    fprintf(fp,"\n");
                }
                //                fprintf(fp,"K is ( ");
                //                for(k=0;k<4;k++) {

                //                }
                //                fprintf(fp," )\n");

                //                fprintf(fp,"    Length        %f\n",(*np).length);
                //                fprintf(fp,"    Force         %f\n",(*np).force);
                //                fprintf(fp,"    EA            %f\n",(*np).EA);
                if(!p->Needs_Finishing) {
                    fprintf(fp,"   n         EI        KEI       d2ydx2        y\n");
                    for(k=0;k<11;k++) {
                        float x = (float)k/(float)10.0;
                        fprintf(fp,"  %8f  %8f  %8f     %8f  %8f\n",x, (float) Batten_Stiffness((RXEntity_p) p,x),
                                (float) KBatten_Stiffness((RXEntity_p)p,x-(float).05,x,x+(float).05)
                                ,Batten_Curvature(x,np->k),Batten_Spline(x,np->k));
                    }
                }
            }

            else if (strieq(p->type(),"camera") ){
                Camera *np = (CAMERA *)(*p).dataptr;

                fprintf(fp,"Position : %6.3f, %6.3f, %6.3f\n",np->pos.x,np->pos.y,np->pos.z);
                fprintf(fp,"Target : %6.3f, %6.3f, %6.3f\n",np->target.x,np->target.y,np->target.z);
                fprintf(fp,"Up : %6.3f, %6.3f, %6.3f\n",np->Up.x,np->Up.y,np->Up.z);
                fprintf(fp,"width,height : %6.3f, %6.3f\n",np->width,np->height);
                fprintf(fp,"Projection : %s\n",np->projection);
                if(np->light_colour) {
                    fprintf(fp,"light : %s at %12.5f, %12.5f, %12.5f\n",np->light_colour,np->light_pos.x,np->
                            light_pos.y,np->light_pos.z);
                }
            }

            else if (strieq(p->type(),"connect")){
                // should do the Fortran print
            }
            //      else if (strieq(p->type(),"basecurve")||strieq(p->type(),"3dcurve")  ){
            //                        class RXBCurve* np = (class RXBCurve*) p;
            //			if(np->BCtype!=0) {
            //	  			fprintf(fp,"   A spline or UV curve. type=%d\n   Input Points\n",np->BCtype);
            //				np->m_pin.Dump(fp);
            //			  fprintf(fp,"   Output Points\n");
            //			}
            //			for (k=0;k<np->c;k++){
            //			  VECTOR v= ((*np).p)[k];
            //			  fprintf(fp,"\t\t\t%d \t%f\t%f\t%f\n",k, v.x,v.y,v.z);
            //			}
            //      }
            //       else if (strieq((*p).type(),"gausscurve")){
            //                        (class RXGaussCurve*)gc = (class RXGaussCurve*) p;
            //			fprintf(fp," Input\n");
            //			gc->m_pin.Dump(fp);

            //			 fprintf(fp," Output\n");
            //				for (k=0;k<gc->c;k++){
            //			  VECTOR v= (gc->p)[k];
            //			  fprintf(fp,"\t\t\t%d %f\t%f\t%f\n",k, v.x,v.y,v.z);
            //			}
            //      }
            else if (strieq((*p).type(),"pansail")){
                struct PCN_PANSAIL *np = (PCN_PANSAIL *)(*p).dataptr;
                fprintf(fp,"       leading edge  %s (a %s) (rev %d)\n",np->leading->name(),  np->leading->type(),np->lrev);
                fprintf(fp,"      trailing edge  %s (a %s) (rev %d)\n",np->trailing->name(),np->trailing->type(),np->trev);
                fprintf(fp,"      basename  %s \n",np->basename);
                fprintf(fp,"      Lower_V   %f \n",np->Lower_V);
                fprintf(fp,"      Upper_V   %f \n",np->Upper_V);
                fprintf(fp,"      Lower_U   %f \n",np->Lower_U);
                fprintf(fp,"      Upper_U   %f \n",np->Upper_U);
                fprintf(fp,"u=%f v=%f\nxf=%f yf%f\nx=%f y=%f z=%f\n",np->U,np->U,np->Xf,np->Yf,np->x,np->y,np->z);
            }
#ifdef USE_PANSAIL
            else if (strieq((*p).type(),"windvane")){
                struct PC_VELOCITY *np = (PC_VELOCITY *)p->dataptr;
                if(np->s) fprintf(fp,"    site      %s\n",np->s->name());
                fprintf(fp,"    position (\t %f\t %f\t %f\t )\n",np->x,np->y,np->z);
                fprintf(fp,"    velocity (\t %f\t %f\t %f\t )\n",np->u,np->v,np->w);
            }
#endif
            else if (strieq(p->type(),"patch")){
                struct PC_PATCH *np = (PC_PATCH *)(*p).dataptr;
                class RXCompoundCurve *   cc = (class RXCompoundCurve *   )np->cc;
                int count = np->N_Angles;

                fprintf(fp,"    Type      %d\n",(*np).m_type); assert(np->m_type ==PATCH  );
                fprintf(fp,"    depth     %f\n",(*np).d );
                if((np->basecurveptr)!=NULL) {
                    fprintf(fp,"    basecurve %s\n",(*(*np).basecurveptr).name() );
                }
                else
                    fprintf(fp,"     basecurve NULL\n");

                assert(np->m_type == 	PATCH) ;
                {
                    if(np->m_corner) {
                        fprintf(fp,"    corner site %s\n",np->m_corner->name() );
                    }
                    else fprintf(fp,"     Corner NULL\n");
                }
                if(np->mat) {
                    fprintf(fp,"    material %s\n",np->mat->name() );
                }
                else
                    fprintf(fp,"     basecurve NULL\n");

                /* e1,e2 point to the entities containing the end sites */
                if ((*np).e[0] !=NULL) fprintf(fp,"    e1    %10s   %10s\n",(*(*np).e[0]).type(), (*(*np).e[0]).name());
                else fprintf(fp," e1 NULL\n");
                if ((*np).e[1] !=NULL) fprintf(fp,"    e2    %10s   %10s\n",(*(*np).e[1]).type(), (*(*np).e[1]).name());
                else fprintf(fp," e2 NULL\n");

                if(np->end1ptr)
                    fprintf(fp,"    End1ptr   %10s\n",(*(*np).end1ptr).name() );
                else fprintf(fp,"    End1ptr  NULL\n");
                if(np->end2ptr)
                    fprintf(fp,"    End2ptr   %10s\n",(*(*np).end2ptr).name() );
                else fprintf(fp,"    End2ptr  NULL\n");
                fprintf(fp,"    e1sign    %d\n",(*np).end1sign );
                fprintf(fp,"    e2sign    %d\n",(*np).end2sign );
                if(np->End1dist )
                    fprintf(fp,"    e1dist    %S\n",np->End1dist->GetText().c_str ()   );
                if(np->End2dist )
                    fprintf(fp,"    e2dist    %S\n",np->End2dist->GetText().c_str ()  );
                fprintf(fp,"    e1angle   %f\n",(*np).end1angle );
                fprintf(fp,"    e2angle   %f\n",(*np).end2angle );
                fprintf(fp,"    ArcLength %lf\n",np->m_arc);
                fprintf(fp,"    strt_A    %f\n",(*np).strt_A );

                fprintf(fp,"    Npsides   %d\n",cc->npss);
                for (k=0;k<cc->npss;k++) {
                    PSIDEPTR e = cc->pslist[k];
                    fprintf(fp,"          %d     %s\n",k,  (*e).name());
                }
                fprintf(fp,"    Ncurves   %d\n",cc->m_ccNcurves);
                for (k=0;k<cc->m_ccNcurves;k++) {
                    RXEntity_p e = cc->m_ccCurves[k];
                    fprintf(fp,"          %3d     %s   (rev=%d)",k,  e->name(),cc->m_ccRevflags[k]);
                    if(np->partial){
                        if(cc->m_ccE1sites&&cc->m_ccE1sites[k]) fprintf(fp," between %s ",cc->m_ccE1sites[k]->name());
                        if(cc->m_ccE2sites&&cc->m_ccE2sites[k]) fprintf(fp,"  and    %s ",cc->m_ccE2sites[k]->name());
                    }
                    else fprintf(fp," whole length ");
                    fprintf(fp,"\n");
                }

                fprintf(fp,"    n_angles  %d\n\n",(*np).N_Angles );
                sum=(float)0.0;

                for (k=0;k<count;k++) {
                    if(np->a[k].Angle){
                        sum=sum+(float) *(np->a[k].Angle);
                        fprintf(fp,"    %f  ",*(np->a[k].Angle));
                    }
                    else
                        fprintf(fp,"    angle NULL  ");
                    fprintf(fp,"    %8S ",np->a[k].m_Offset->GetText().c_str());
                    if(np->a[k].sign )
                        fprintf(fp,"    %2d ", *(np->a[k].sign));
                    else
                        fprintf(fp,"    sign NULL  ");
                    fprintf(fp,"    %10s", np->a[k].s->name());
                    fprintf(fp," end%d\n",np->a[k].End);
                }
                fprintf(fp,"    Sum %f\n",sum);

                count = (*np).c;

                for (k=0;k<count;k++) {
                    VECTOR v= ((*np).p)[k];
                    fprintf(fp,"\t\t\t%d %f\t%f\t%f\n",k, v.x,v.y,v.z);
                }
            } // patch

            else {
                fprintf(fp," Print_All_Entities doesnt know about %s\n",p->type());
            }
        } //default
        } //switch
    } // if dataptr

    level --;
    return 1;
}
int  RXEntityDefault::CClear(){
    int rc=0;
    // clear stuff belonging to this object
    rc+=this->Cleanup (this->Esail);
    // call base class CClear();
    rc+=RXEntity::CClear ();
    return rc;
}
int RXEntityDefault::Cleanup(SAIL * const p_sail, int p_Leave_Deps) {

    /*  Clear all the entities internal data */

    if(Peter_Debug) {RXTAB(dbgc); printf("start Cleanup of %s %s\n",type(),name());}
    if( TYPE == PCE_DELETED)
        return PCE_DELETED ;

    if(! dataptr  &&  TYPE != CONNECT) {
        if(Peter_Debug) cout<< "Null data so dont clear\n"<<endl;
    }
    else
    {
        if(! TYPE) TYPE = INTEGER_TYPE( type());

        switch ( this->TYPE) {

        case PCE_CONSURF:
        {
            RXContactSurface * c = ( RXContactSurface *)DataPtr();
            cout<< "BUG  Clear Entity\ncontact surface we must flush references to it from the nodes\n"<<endl;
            delete c;
            break;
        }
        case PCE_DOVE:{
#ifdef RX_USE_DOVE
            RXdovefile *theDove = (RXdovefile *)DataPtr();
            delete theDove;
#endif
            break;
        }
        case PATCH:
            Delete_Patch(this);	// not complete
            RXFREE( dataptr);   SetDataPtr(NULL);
            break;

        case PC_PANELDATA:{
            struct PANELDATA*pd =(struct PANELDATA*)DataPtr();
            RXEntity_p o = pd->owner;
            if(o)
                if(o->TYPE == PANEL) {
                    Panelptr pan =( Panelptr ) o;
                    if(pan){
                        assert(pan->m_PanelData ==pd);
                        pan->m_PanelData=0;
                    }


                }
            if(pd->list) RXFREE(pd->list);
            RXFREE( dataptr);  SetDataPtr( NULL);
            break;
        }
        case   BOUNDARY: {
            struct PC_BOUNDARY *np = (struct PC_BOUNDARY * )DataPtr();
            np->m_corners.clear();
            np->m_edges .clear();
            np->m_names .clear();

            delete np;  SetDataPtr(NULL);
            break;
        }
        case PCE_CAMERA : {
            Camera *c = (Camera *) DataPtr();
            if (c->projection )    RXFREE(c->projection);
            if (c->name) 	   RXFREE (c->name );
            if (c->light_colour ) RXFREE (c->light_colour);
            RXFREE(c);  SetDataPtr(NULL);
            break;
        }
        case DXF :
        {
            struct PC_DXF *pp = ( struct PC_DXF *)DataPtr();
            RXFREE(pp->filename);
            RXFREE(pp->segname);
            break;
        }
        case FABRIC :
        case MATERIAL :{
            struct PC_MATERIAL*np = (struct PC_MATERIAL* )DataPtr();
            if(np){
#ifndef WIN32
                if(np->g) Do_Delete_MatWindow((Graphic*) np->g);
#endif
                if(np->text) RXFREE(np->text);
                if(np->table) PCT_Free_Table(np->table);
                if(np->es) RXFREE(np->es);
                if(np->fs) RXFREE(np->fs);
                if(np->en) RXFREE(np->en);
                if(np->fn) RXFREE(np->fn);
                RXFREE(np);  SetDataPtr( NULL);
            }

            break;
        }
        case PCE_IGES:{
            iges_free((struct PC_iges *)DataPtr() );
            SetDataPtr(NULL); // do we need to free dataptr??
            break;
        }
        case INTERPOLATION_SURFACE: {
            struct PC_INTERPOLATION *ptr = (struct PC_INTERPOLATION * )DataPtr();
            if(ptr->type[0] == 'F') {		  /* F-style has not interpolation data */

            }
            else if(ptr->type[0] == 'N')
                PCN_Free_Nurb(ptr->NurbData);

            if(ptr->data) {
                cout<< "(INTERPOLATION_SURFACE) KillEdges is retired ->leak"<<endl;
                //KillEdges(ptr->data,(3* ptr->nsites+20));
                ptr->nsites=0;
                ptr->data = NULL;
            }
            RXFREE(ptr); SetDataPtr (0);
            break;
        }
        case PANSAIL:{
            struct PCN_PANSAIL *np = ( struct PCN_PANSAIL *)DataPtr();
            if(np->basename) RXFREE(np->basename);
            if(np->leechWakeString) RXFREE(np->leechWakeString);
            if(np->footWakeString) RXFREE(np->footWakeString);
            RXFREE(np);  SetDataPtr(NULL);
            break;
        }
        case PCE_SETDATA:
        {
            RXExpressionI *le =    	(RXExpressionI *) DataPtr();
            //			we'd like to delete le but that destroys 'this'
            break;
        }
            //case RENAME:	these are the same
        case PCE_ALIAS: {
            struct PC_ALIAS *np = (struct PC_ALIAS *)DataPtr();
            if(np) {
                if(np->type) RXFREE(np->type);
                if(np->name) RXFREE(np->name);
                RXFREE( dataptr );
            }
            break;
        }

        case BATTEN : {
            struct PC_BATTEN *np = (struct PC_BATTEN * )DataPtr();
            if(np) {
                if(np->p) RXFREE(np->p);
                if(np->a) RXFREE(np->a);
                RXFREE( dataptr );
            }
            break;

        }
        case CONNECT: {
            void *ptr = (void *)this;
            if(Peter_Debug)
                printf(" Clear Connect Entity %s %s\n", type(), name());
#ifdef FORTRANLINKED 
            cf_delete_connect( &ptr);
#else
            assert(0);
#endif
            break;
        }
        case PCE_PRESSURE: {
            Pressure *ptr = (Pressure *)DataPtr();
            if(!ptr) break;

            PCT_Free_Table(ptr->m_table);
            memset(ptr,0,sizeof(Pressure));
            break;
        }
        case FIELD :
        {
            struct PC_FIELD *np = (	struct PC_FIELD *)DataPtr();
            if( np->m_TyPe ) RXFREE(np->m_TyPe); np->m_TyPe=0;
            RXFREE(np);
            break;
        }

        case PCE_AERO:
        case PCE_VELOCITY :
        case STRIPE :

        case PCE_ZONE :
            cout<<" a leak on PCE_ZONE delete "<<endl; //delete p->DataPtr ();
            break;
        case GENDP :	// no action. Puts something in the sail

            //case FIX :  // No action.  APPEnds the site's attrubutes.
        case PCE_GOAL : // variable target measure
        {
            RXFREE(DataPtr() );
            break;
        }

        case  PCE_3DM :
        {
            struct PC_3DM_Model * np = (struct PC_3DM_Model * ) DataPtr();
            if(np->m_name) RXFREE( np->m_name );
            if(np->m_segname) RXFREE( np->m_segname );
            if( Esail &&  Esail->m_p3dmModel == np)
                Esail->m_p3dmModel=0;
            //np->m_pONXModel->Destroy();
            delete np->m_pONXModel;

            break;
        }
        case  PCE_PCFILE :
        {
            cout << "TODO: code cleanup of PC file data"<<endl;
            break;
        }

        case PCE_SAIL :
        {
            break;
        }
        case BASECURVE:
        case GAUSSCURVE:
        case THREEDCURVE:
        case TWODCURVE:
            assert(0);

        default:
            if (strieq( type(),"linear substructure")){
                void *Index = DataPtr();
                if(Peter_Debug) printf(" Clear Entity %s %s\n", type(),  name());
#ifdef FORTRANLINKED
                cf_delete_gle(&Index);
#else
                assert(0);
#endif
            }
            else {
                char str[128];
                sprintf(str," Delete Entity doesnt know about %s\n",type());
                rxerror(str,2);

            }
        } // end switch

    } /* end of (if doesnt need resolving */

    SetDataPtr( NULL);

    if(Peter_Debug){RXTAB(dbgc); printf("End Cleanup of %s %s\n",type(),name());}
    return(1);
}


