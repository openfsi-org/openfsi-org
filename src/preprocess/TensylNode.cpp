#include "StdAfx.h"

#if defined(RXRHINOPLUGIN) || defined(RXPHYSX) 
class RXEntity;
#else
#include "RXEntityDefault.h"
#include "griddefs.h"
#include "RX_FESite.h"
#include "RXSail.h"
#include "f90_to_c.h"
#endif

#include "TensylNode.h"

CTensylNode::CTensylNode(void)
: m_nn(0)
, m_pt(0,0,0)
, m_isbdy(false)
, m_fixity(0)
, m_field(0)
, m_AxSym(0)
, m_e(0)
{
}
CTensylNode::CTensylNode(int p_nn,ON_3dPoint p_pt, bool p_isbdy,int  p_fixity,int p_field,int p_AxSym)
: m_nn(p_nn)
, m_pt(p_pt)
, m_isbdy(p_isbdy)
, m_fixity(p_fixity)
, m_field(p_field)
, m_AxSym(p_AxSym)
, m_e(0)
{

}
int CTensylNode::Print(FILE *fp){
	//Node Number, X, Y, Z, Boundary, Field, Fixity, AxSym // we use AxSym to mean 'even'
	int k;
#ifdef RXRHINOPLUGIN
		return fprintf(fp," %d %f %f %f %d %d %d %d\n",
		m_nn,m_pt.x,m_pt.y,m_pt.z,m_isbdy,m_field,m_fixity,0);
#endif

#define M2I  (39.3700787401575)

#if !defined(RXRHINOPLUGIN) && !defined(RXPHYSX) 
        QString aa;
		RXEntity_p  eee;
		if(this->m_e ) {
				RX_FESite *ss = (RX_FESite *) m_e;
				double xx[3]; 
				if(eee= dynamic_cast<RXEntity_p>(m_e))
                    aa=eee->AttributeString();
				else
                    aa=QString::fromStdWString(TOSTRING(this->m_atts.Array()));
		#ifndef FORTRANLINKED
		assert(0);
		#else	 
				cf_get_coords(m_e->Esail->SailListIndex(), ss->GetN(),xx); 
		#endif
				k = fprintf(fp," %d %f %f %f %d %d %d %d %s \n",
                m_nn,xx[0]*M2I,xx[1]*M2I,xx[2]*M2I,m_isbdy,m_field,m_fixity,0,qPrintable(aa));
			}
	else
#endif
	{
		k = fprintf(fp," %d %f %f %f %d %d %d %d  %S\n",
		m_nn,m_pt.x,m_pt.y,m_pt.z,m_isbdy,m_field,m_fixity,0,this->m_atts.Array());
	}
	return k;
}

CTensylNode::~CTensylNode(void)
{
}
