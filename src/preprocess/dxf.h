#ifndef _DXF_H_
#define _DXF_H_


 struct PC_DXF {
  	 char *filename;
	 char *segname;
	 RXEntity_p sc;
};
EXTERN_C HC_KEY Read_DXF(SAIL *sail,const char *name,const char *filename) ;
int Resolve_Dxf_Card(RXEntity_p e);
int Compute_DXF(RXEntity_p e);

#endif  //#ifndef _DXF_H_
