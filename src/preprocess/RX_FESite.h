#pragma once

#include "vectors.h"

#include "RX_FEObject.h"
#include "RXSitePt.h"


/////////////////////////////////////////////////////////////////////



//  RX_FESite

/** @brief This class maintains a Site ( a 3d point) as a finite element node. 

 (Jan 2009) it also contains all the old struct PC_RELSITE functionality.

 briefly:
  you derive a site from this class if you want it to be an analysis node.
  It maintains an analysis node for as long as it lives.


  summary of the Site classes
  RXSiteBase is a base class for all site-like objects. It just holds 3 coordinates and a number.

  RXSitePt is slightly richer than RXSiteBase. It is used by the meshing routines.

  For a site to be active in the preprocessor (ie if it was read from a file),
  it has to be derived from RXEntity.

  The preprocessor recognizes two classes of Site:
   SITE - derived from RXEntity and from RX_FESite.

   RELSITE - class RXRelSite. takes its position from curves - either by distance along
   or by intersection. Derived from SITE
   both these types are implemented in RXSRelSite, via switches (ugly)

  For a site to be recognized by the FE analysis it has to be derived from RX_FESite

  A note on coordinates and deflections.
    The current deflected position in world-space is held in the FNODE
    The last-known-good deflected position is held (in model space) as
    RX_FESite::{x,y,z)  + RX_FESite::m_d
  Thus RX_FESite::m_d is ONLY changed by MakeLastKnownGood



*/

/////////////////////////////////////////////////////////////////////
class RX_FESite: 
        public RXSitePt,	public RX_FEObject
{


public:
    RX_FESite(void); // deprecated
    RX_FESite(class RXSail *p);
    ~RX_FESite(void);
    int Init(void);
public:
    /////////////////////////////////
    //most  Coordinate access methods. are  in RXSitePt These overrides set the FEA coordinates too.
    bool Set(const ON_3dPoint &v) ;

    ON_3dPoint GetLastKnownGood(RX_COORDSPACE base ) const;

    // retrieve the  current position from the FEA, irrespective of the state of the analysis
    //options are RX_GLOBAL, RX_MODELSPACE

    const ON_3dPoint DeflectedPos( RX_COORDSPACE base = RX_GLOBAL) const;

    bool SetDeflection(const ON_3dVector d); // Model space
    const ON_3dVector Deflection(void) const { return m_defls;}   // Model space from sitept may 2012
    ////////////////////////////////////////////////////////////////

protected:
    bool m_InRedSpace;
    int SetTrace(const int i);
    int SetLoads(const double p[3]);
public:
    int IsEdgeNode() const;
    bool IsRedSpace(void)const;
    void SetIsRedSpace(bool b){	m_InRedSpace=b;	}

    int PrintShort(FILE *ff) const;
    virtual int Dump(FILE * fp) const;

    RXSTRING TellMeAbout() const;
    int MakeSixNode(void);
private:
    ON_3dVector m_defls ;    //last known good deflections, in model space.
};

// Must be consistent with cfromf_declarations.f90
extern "C" int unhookfromfea( class RX_FESite *const ptr);
