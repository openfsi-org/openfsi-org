#pragma  once
#define _RXEXPRESSIONLIST_HDR_

#include <iostream>
#include <string>
#include <map>
#include <set>
#include <ostream>
#include "RXGraphicCalls.h"

#include <QString>
using namespace std;

class RXSail;
// class RXENode, a Base class for Entity, Sail and world.
// this class manages a list of expressions.  
//It is designed to be a node of a tree enabling inheritance of expressions.

// additionally (and its ugly) it holds the graphics node whose tree shape matches
// the shape of the RXENode tree

class  RXExpressionI;
class  RXExpression;

enum  RXEPL_Flags {
    rxexpLocal=0,
    rxexpLocalAndTree =1,
    rxexpJustTree=2
};

#define RXEXMAP map<RXSTRING,RXExpressionI *>
class RXENode
{
public:
    RXENode();
    virtual ~RXENode();
    size_t GetMapSize(void)const  { return this->m_xmap.size();}
    class RXExpressionI* AddExpression( class RXExpressionI* p) ;
    class RXExpressionI* AddExpression(const RXSTRING &name,const RXSTRING &equation,  class RXSail *const p_sail)  ;
    class RXExpressionI* AddExpression(const QString &name,const QString &equation,  class RXSail *const p_sail) ;

    bool RemoveExpression(const RXSTRING &name);
    RXExpressionI * FindExpression(const RXSTRING &text,const enum  RXEPL_Flags flag , RXExpressionI **e=0) const ; //looks in this->explist and recursively in this->parent->explist.

    int CClear();
    void SetParent(RXENode*p); // call with null clears it from its parents m_children.
    int EvaluateAllReadOnly();
    RXExpressionI *FindAndChange(const RXSTRING &exname,const RXSTRING &value, const enum RXEPL_Flags &f ); //rxexpLocal=0, called from the DB, this searches down the tree not up it like FindExpression

    // walk up the parent tree and delete any members with m_model==p
    int ClearExpList( const RXSail*p);
    // writes the expressions in the list as SET entities
    int WriteToScript(FILE * fp,const wchar_t* p_where, const class RXSail *p_owner)const ;
    int List(FILE * fp,  const string &head, const int depth ,const int recur ) const ;
    int List(wostream &s,const string &head, const int depth ,const int recur ) const;
    int Export(wostream &s,const wstring &head, const int recur ) const ;
    int ExportDependencies(wostream &s,const wstring &head, const int recur, const int what ) const ;
    int OutputToClient(const RXSTRING &s, const int level)const;
    int OutputToClient(const char *s, const int level) const;
    int OutputToClient(const std::string &s, const int level)const;
    int OutputToClient(const QString &s, const int level)const;
    void SetClient( class QObject* qo){ this->m_client=qo; }
    class QObject*  Client( ) const ;                   // inherits from parent


    RXGRAPHICSEGMENT GNode(const int flag=rxexpLocalAndTree) const;            // inherits from parent
    void setGNode(RXGRAPHICSEGMENT n ){  m_GlcNode=n;}


public: // dangerous, only for exporting as graph
    RXEXMAP m_xmap;
private:
    int m_xmapChangeCount;
    RXENode* m_parent;
    class QObject * m_client;
    RXGRAPHICSEGMENT m_GlcNode;


public:
    bool m_debug;
private:

#ifdef USE_ENODECHILDREN
    std::set<RXENode*> m_enodechildren;
#endif

};


