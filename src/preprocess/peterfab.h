#ifndef _PETERFAB_H_
#define _PETERFAB_H_

#include "opennurbs.h"

#include "fielddef.h"  // linux picked this up 
 /* peterfab.h made from peterfab.c 9/11/96 */

EXTERN_C MatValType *Mat_Filament (double *th,double *mx,const class RXSitePt *q,class RX_FETri3 *t,class RXLayer *p,MatDataType &xv,double ps[3]);
EXTERN_C MatValType *Mat_Field    (double *th,double *mx,const class RXSitePt *q,class RX_FETri3 *t,class RXLayer *p,MatDataType &xv,double ps[3]);
EXTERN_C MatValType *Mat_Radial   (double *th,double *mx,const class RXSitePt *q,class RX_FETri3 *t,class RXLayer *p,MatDataType &xv,double ps[3]);
EXTERN_C MatValType *Mat_Parallel (double *th,double *mx,const class RXSitePt *q,class RX_FETri3 *t,class RXLayer *p,MatDataType &xv,double ps[3]);
EXTERN_C MatValType *Mat_Uniform  (double *th,double *mx,const class RXSitePt *q,class RX_FETri3 *t,class RXLayer *p,MatDataType &xv,double ps[3]);
EXTERN_C MatValType *Mat_Curve    (double *th,double *mx,const class RXSitePt *q,class RX_FETri3 *t,class RXLayer *p,MatDataType &xv,double ps[3]);

EXTERN_C int Compute_OneCurve_Field(struct PC_FIELD*field,double *thickness,const class RXSitePt *q,MatDataType &xv, double *f, int*inside);
// like Compute_TwoCurve_Field except only one curve for ref dir. Thickness always 1
EXTERN_C int Compute_TwoCurve_Field(struct PC_FIELD*field,double *thickness,const class RXSitePt *q,MatDataType &xv, double *f, int*inside);
EXTERN_C int Compute_Filament_Field   (RXEntity_p efi,double *thk,class RX_FETri3 *t,MatDataType &xv,double*mx, double *f, int*inside,double ps[3]);
EXTERN_C int Compute_Field_Dove       (RXEntity_p efi,double *thk,const class RXSitePt *t,MatDataType &xv,double*mx, double *f, int*inside,double ps[3]);
EXTERN_C int Compute_NURBS_ALPHA_Field(RXEntity_p efi,double *thk,class RX_FETri3*t,MatDataType &xv,double*mx, double *f, int*inside,double ps[3]);
EXTERN_C int Compute_Radial_Field(struct PC_FIELD*field,double *thickness,const class RXSitePt *q,MatDataType &xv, double *f, int*inside);
EXTERN_C int Compute_Radial_Field(struct PC_FIELD*field,double *thickness,const class RXSitePt *q,MatDataType &xv, double *f, int*inside);
MatFuncPtr Get_Material_Function_Key(const char*s) ;
#endif  //#ifndef _PETERFAB_H_
