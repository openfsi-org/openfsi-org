// doveGeometric.h: interface for the doveGeometric class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DOVEGEOMETRIC_H__546513F5_9AE6_4083_93C6_0D5D6E02AF70__INCLUDED_)
#define AFX_DOVEGEOMETRIC_H__546513F5_9AE6_4083_93C6_0D5D6E02AF70__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define DOVEGEO_RADIAL	1
#define DOVEGEO_NS  	2
#define DOVEGEO_CIRCLES	4

#include "doveforstack.h"

class doveGeometric : public doveforstack  
{
public:
	doveGeometric();
	virtual ~doveGeometric();

	char *Deserialize(char *p_l);
	int HowManyLayers(const int p_flag);
	double GetAlpha(const double pu, const double pv);
	int Stack(const double p_i, const double p_j,
						 ON_ClassArray<layernodestack> &p_TheStack,
						 double*alpha0
						 );

	int m_type;
	ON_3dPoint m_Origin;
	double m_dThk;
	double m_dNu;
	ON_SimpleArray<double> m_params;

};

#endif // !defined(AFX_DOVEGEOMETRIC_H__546513F5_9AE6_4083_93C6_0D5D6E02AF70__INCLUDED_)
