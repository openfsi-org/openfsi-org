
#ifndef UVCURVE_16NOV04
#define UVCURVE_16NOV04


#include "griddefs.h"

EXTERN_C int Compute_UV_Curve( RXEntity_p e, RXEntity_p mould,double tol);

#endif //#ifndef UVCURVE_16NOV04

