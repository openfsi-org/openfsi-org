#pragma once
#include <QString>
#include<string>
#include <list>
#include <map>

class RXAttributes
{
public:
    RXAttributes(void);
    RXAttributes(const char* s);
//    RXAttributes(const std::string &s);
    RXAttributes(const RXSTRING &s);
    RXAttributes(const RXAttributes& a);

    ~RXAttributes(void);



    bool Find(const RXSTRING &key )const;
    bool Find(const QString &key )const;
    void Add(const  QString &s);
    void Add(const RXSTRING &s);
    bool Remove (const QString &kw);
    void RemoveAll() {  m_ww.clear(); }

    QString  Serialize(const int stripBad=0) const;

    RXSTRING GetAll(const int stripBad=0) const;
    QString ToQString(const int stripBad=0) const {return Serialize(stripBad); }
     QString GetPart(const QString what) const; // what is a comma-separated list of possibles
    size_t Length() const{ return  Serialize().length();}

    RXSTRING Extract_Word(const RXSTRING &key, bool &found) const;
    QString Extract_Word(const QString  &key, bool &found) const;

    bool Extract_Word(const RXSTRING &key, RXSTRING &result) const;
    bool Extract_Word(const QString  &key,	QString &result ) const;

    int Extract_Float  (const RXSTRING &keyw,float*v) const ;
    int Extract_Float  (const char*keyw,float*v) const ;

    int Extract_Double (const RXSTRING &keyw,double*v) const ;
    int Extract_Double (const char*keyw,double*v) const;

    int Extract_Integer(const RXSTRING &keyw,int*v) const ;
    int Extract_Integer(const char*keyw,int*v)  const;

    bool ChangeValue(const RXSTRING &s, const RXSTRING & value);
    bool ChangeValue(const QString &s, const QString & value);

    // Splice makes m_ww from  the argument
    //   'this' becomes the union of this and a,
    // ThisHasPrecedence true means that the existing values take precedence

     bool Splice(const QString &q, const int ThisHasPrecedence=0); //

    QString PrettyPrint()const;
    int  HeuristicCheck(QString &rv)const;

private:

    static int Grandfather(QString&s );
    static bool isSep(const int i, const  QChar* t,const int nc) ;
    static bool isSep(const int i, const std::wstring t) ;
    static  std::pair<QString,QString> SplitAtt(const QString & s);
    static  std::list<RXSTRING>  Parse(const QString q) ;
     std::list<RXSTRING>  TestParse(const QString q) ;
    static QString Dollar(const QString & q);
    static QStringList kwlist;
    static QStringList InitKWList();

    std::map<QString,QString> m_ww;
public:

    static int Test(const RXSTRING &s);
private:
    class AttParserNode
    {
    public:
        AttParserNode(void);
        AttParserNode(class AttParserNode *p,const QChar op);
        ~AttParserNode(void);
        class AttParserNode* Up();
         class AttParserNode* Down(const QChar op);
         QString Serialize() ;
         void Append(const QChar c);
         int Level() const {return level;}

 private:
         QChar m_op;
         class AttParserNode *parent;
         std::list<class AttParserNode * > children;
         int level;
         QString  m_text;

    };
};
