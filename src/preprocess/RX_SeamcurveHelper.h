#pragma once
#include "RXSitePt.h"
#include "RXSeamcurve.h"
#include "vectors.h"

class RXCurve;

class RX_SeamcurveHelper	// RX_SeamcurveHelper is a grouping of data for computing SCs.
{
	friend class RXSeamcurve;
public:
	RX_SeamcurveHelper(void);
	~RX_SeamcurveHelper(void);

	void Init(void);
	int Clear();
//public:
protected:
		sc_ptr m_sc;
                RXSitePt m_schE1b,m_schE2b;
		RXSitePt ev1,ev2;
		ON_3dVector xl,yl,zl,xlb,ylb,zlb;
		Site *end1site, *end2site;
		double depth;
		double length;
		float Black_Chord,Red_Chord,G_Chord;
		VECTOR*m_Old_Plines[3]; 
		int m_oldc[3];

		RXCurve * m_OldCurves[3];

		RXEntity_p mould;  
		VECTOR*p_slr; int c_slr;
		int NoEnds;
		int m_type;
public:
	int SeamCurve_XYZAbsGeometry();
	int DrawSeamcurve();
	int SeamCurve_Prelims(sc_ptr e);
	int  SeamCurveHasMoved(const double tol);
	int  Compute_SeamCurve_XYZabsSK_Fil(RXSeamcurve *p);
	int  Compute_SeamCurve_Geo_Mould_Scaled_Curve(RXEntity_p e);
	int  Compute_SeamCurve_XYZabsOnly(RXEntity_p e);
    int  Compute_SeamCurve_XYZabs_Cut(sc_ptr e);
	int  Compute_SeamCurve_XYZAbs_Geo_Cut(RXEntity_p e);
	int  Compute_SeamCurve_Geo_Mould_Seam(RXEntity_p e);
	int  Compute_SeamCurve_Sample(RXEntity_p e);
protected:
	int  oldBuild_SC_Curves(VECTOR*base,int bc);
	int  newBuild_SC_Curves(VECTOR*base,int bc); 

	int OutputToClient(const RXSTRING &s, const int level)const		{ return m_sc->OutputToClient(s,level);}
	int OutputToClient(const char *s, const int level) const    	{ return m_sc->OutputToClient(s,level);}
	int OutputToClient(const std::string &s, const int level)const  { return m_sc->OutputToClient(s,level);}
	int FudgeTrigraph();
};
