#include "StdAfx.h"

#include "opennurbs.h"
#include "RXPanel.h"
#include "vectors.h"
#include "entities.h"

#include "RXLayer.h"

RXLayer::RXLayer(SAIL *s)
:RXEntity(s) ,
text(0),
Use_Me_As_Ref(0),
Use_Black_Space(0),
matptr(0) 
{ 
this->matstruct.function=0; 
this->matstruct.m_sizeofdata=0; 
this->matstruct.m_Data.clear();
this->matstruct.ref_sc=0; 
this->matstruct.ll=0; 
}

RXLayer::RXLayer(void)
:
		text(0),
		Use_Me_As_Ref(0),
		Use_Black_Space(0),
		matptr(0) 
	{ 
		assert(" dont use this constructor"==0);
		this->matstruct.function=0; 
		this->matstruct.m_sizeofdata=0; 
		this->matstruct.m_Data.clear();
		this->matstruct.ref_sc=0; 
		this->matstruct.ll=0;  
		 
}
RXLayer::~RXLayer(void)
{ 
		matstruct.m_Data.clear(); 
		Free_Entity_List(&(matstruct.ll) );
		if(text) RXFREE(text);
}
int RXLayer::Compute(void){
    // set needs_Meshing on all its panels RXO_LAYER
    set<RXObject*> z= this->FindInMap(RXO_LAYER,RXO_ANYINDEX);

    for(set<RXObject*>::iterator it=z.begin();it!=z.end();++it){
        class RXPanel* pp= dynamic_cast<class RXPanel*>(*it);
         pp->NeedsMaterials=1;

    }

    return 0;
}
int RXLayer::Resolve(void){assert(0); return 0;}
int RXLayer::Finish(void){assert(0); return 0;}
int RXLayer::ReWriteLine(void){assert(0); return 0;}
int RXLayer::Dump(FILE *fp) const{
			const class RXLayer *np = this;
			if(np->text) fprintf(fp," text : %s\n",np->text);
			
			fprintf(fp," sizeofdata   %d\n",  np->matstruct.m_sizeofdata);
			fprintf(fp," Use_Black    %d\n",  np->Use_Black_Space);
			fprintf(fp," Use_Me_As_Re %d\n",  np->Use_Me_As_Ref);
			if(np->matptr)
			  fprintf(fp," material     %s\n",  np->matptr->name());
			if(np->matstruct.ref_sc)
			  fprintf(fp," ref SC      %s\n",  np->matstruct.ref_sc->name());
			if(np->matstruct.ll){
				LINKLIST *lll= np->matstruct.ll;
			  Print_Pointer_LinkList(fp, "Material", &(lll));
			}
			
			for(int ii=0;ii< np->matstruct.m_sizeofdata;ii++) fprintf(fp," %f    ",np->matstruct.m_Data[ii]);
			fprintf(fp," \n");

return 1;
}
int RXLayer::CClear() 
 {
	 int rc=0;
// clear stuff belonging to this object
		matstruct.m_Data.clear(); 
		Free_Entity_List(&(matstruct.ll) );
		if(text) RXFREE(text); text=0;

// call base class CClear();
		rc+=RXEntity::CClear ();
return rc;
}
ON_3dVector RXLayer::Get_Mat_Vector(const class RXSitePt q) {
 class RXLayer *fab=this;
	ON_3dVector matrefVector;
	MatFuncPtr Mfunc;

	MatValType mx[MXLENGTH];
    MatDataType xv; xv.resize(20,0); // float xv[20];
	double prestress[3];
 //   int mok;
	double thick=0.; 

	Mfunc = (MatFuncPtr) fab->matstruct.function; 
	if(!Mfunc) return ON_3dVector(0,1,0);			 
 //   mok=
    Mfunc(&thick,mx,&q,NULL,fab,xv,prestress);
  //  assert(mok);
	matrefVector.x = xv[0];
	matrefVector.y = xv[1];
	matrefVector.z = xv[2];
return matrefVector;
}
