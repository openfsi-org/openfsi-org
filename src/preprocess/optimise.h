#ifndef _OPTIMISE_H_
#define  _OPTIMISE_H_	


												   		   
#define  O_SOFT 0
#define  O_HARD 1

#define	  O_GT  1
#define	  O_EQ  0
#define	  O_LT  -1
#define	  OPT_MAX	10
  
 typedef struct  {
			double x;
		/* SoftStep is the distance a soft boundary may be pushed */
			double OSoftStep, Otol2,OLower,OUpper; 	/* optimisation space */
			double PSoftStep, Ptol2,PLower,PUpper; 	/* physical space */
			double a,b; 	/* scale as in (opt = a phys + b) */		
			char*name;
		 	int column;
} OVARIABLE;  

typedef struct  {
			double x[20];
			int n;
			double length;
			double y[OPT_MAX+1],dY[OPT_MAX+1];  // 0 is the goal, Then nt constraint values 		
} OVECTOR;

typedef  struct { 
 /* a restraint is a plane (r-p) dot n >0   */
			int type;
			OVECTOR *n, *p;	
			double delta;	/* input. The maximum movement allowed one jump */
			double lambda; /* Transient. = dist of intersection along dv  */
			char *name;
			int pushed;
}	IRestraint ;  
													   
typedef  struct { 
		char *Funcname;
		int order;		//2 for the goal, one for constraints.
		int nCmax;
		double*coeffs; int nc; // nc is no of coeffs
		int index;
		int inequality;  // LT, EQ, GT
		int limiting,Ipiv;	 // true if this is a limiting constraint. 
		double A, *C, *B;
		double CurValue,ConValue;
		int nb;
} Optimisation_Function;  

typedef  struct { 
		double HeightTol,DistTol;
		OVARIABLE * Template;	int ns;
	  	OVECTOR*History;		int np; 
		Optimisation_Function Functions[OPT_MAX]; int nFunctions;
		IRestraint *Restraints; int nres;
		char *filename; /* History */
		char *OFile; /* the optimisation script */
} Optimisation_Structure;  	



int Model_To_Opt(OVARIABLE *v,double Xm,double*Xo);
int Opt_To_Model(OVARIABLE *v,double Xo,double*Xm);

int Create_Soft_Constraints(Optimisation_Structure *A); /* Finished */

 int OPT_DBPrint(Optimisation_Structure *A) ;
  int Optimise( Optimisation_Structure *A );

EXTERN_C int OPT_Climb_Hilltop( Optimisation_Structure *S, OVECTOR*x, OVECTOR*h);
 
EXTERN_C  int OPT_Clip_OVector_To_Restraints (OVECTOR*dv,OVECTOR*x,double *f, Optimisation_Structure *A);


 int Define_Starting_State(Optimisation_Structure *A, OVECTOR*x);
/* 	 Enquire for the current values and fill in x */

int Read_Optimisation(Optimisation_Structure *A, char*fname);
   /* generates the Template
		 name upper lower softstep 
		 Height and dist tolerances
   */
int Read_Optimisation_File(Optimisation_Structure *S,char*fname);
// the keyword-based version

int Read_Optimisation_History(Optimisation_Structure *A,char *filename);   
/* Pulls a set of history vectors from summary file */

int Goal_Values(Optimisation_Structure *A, OVECTOR*X);

double Goal_Value_SSD(Optimisation_Structure *A, OVECTOR*X);
 /*  Scales X into model space, posts the values. Runs the analysis
 and retrieves the goal value  and the Constraint values*/


EXTERN_C double OVECTOR_Dot (OVECTOR *a,OVECTOR *b);
EXTERN_C int Append_To_History( OVECTOR*x, OVECTOR**history,int *np);
EXTERN_C int PCO_vmult(double *v,double*A,double*ecurr,int N);
 	
EXTERN_C int OVVmult(OVARIABLE *v,double*A,double*ecurr,int N);
EXTERN_C int OVECTOR_Add	(OVECTOR *a,OVECTOR *b,OVECTOR *n);
EXTERN_C int OVECTOR_Diff	(OVECTOR *a,OVECTOR *b,OVECTOR *n);
EXTERN_C int  OVECTOR_Copy(OVECTOR*src,OVECTOR*dest);
EXTERN_C int OVector_Print( char *buf, OVECTOR*x, int NoHeader);
EXTERN_C int OVmultWithIndices(double *v,int nv, double*A,double*ecurr,int N,int mca,int*indices);
#endif  //#ifndef _OPTIMISE_H_




