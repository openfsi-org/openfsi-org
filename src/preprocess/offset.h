//  offset.h
//  modifications for ifort 15
//  see  Compiler Reference>Mixed Language Programming
//    !>Standard Tools>Global Data

// 1) extern struct not extern "C"
// 2 remove the typedef
#ifndef _OFFSET_H_HDR
#define  _OFFSET_H_HDR

#define MDIM 64
#define MX 4		
#pragma pack(2)
extern  struct RELAX_FLAG_STRUCT { // MUST match  COMMON/relaxflagcommon in rlxflags.f90
  double factor1,factor2, residualCap, DampingFactor;
  double admasmin, g_UpdateTime, g_RKDamping , g_RKEps,g_RK_H1;
  int Stop_Running;  int NompThreads;  int  Mass_Set_Flag;
  int VPP_Connected;
  int ForceLinearDofoChildren;
  int Mass_By_Test;
  int SSDWrinklingAllowed;
  int K_on_Wrinkled_Matrix;
  int New_DD;
  int HardCopy;
  int dm;
  int LinkCompAllowedFlag;
  int StringCompAllowedFlag;
  int Debug1,Debug2;
  int Force_Linear,Use_DSTF_Old ;
  int prestress;
  int form_2006;
  int BumperSticker;
  int EdgeCurveCorr;
  int SVD_Solver;
  double ElasticModifier[3][3] ;
} relaxflagcommon;
#pragma pack()
//typedef struct RELAX_FLAG_STRUCT Relax_Flag_Type;

struct TRISTRESREULTS {
  double m_stress[3];
  double m_eps[3];
  double m_princ[3];
  double m_cre;
  double m_Princ_Stiff;
  double m_thk,ke,kg,nu, plyminX,plymaxX,plyminY,plymaxY,plymaxXY;
};
typedef struct TRISTRESREULTS TriStressStruct; 

#endif

