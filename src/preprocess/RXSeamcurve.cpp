#include "StdAfx.h"
#include "RelaxWorld.h"
#include "RXGraphicCalls.h"
#include "RXCurve.h"
#include "RXSRelSite.h"
#include "RXSail.h"
#include "RX_FEBeam.h"
#include "RXOffset.h"
#include "RXPside.h"
#include "RXQuantity.h"
#include "RXRelationDefs.h"
#include "RXAttributes.h"
#include "rxbcurve.h"
#include "entities.h" // for push
#include "etypes.h"
#include "global_declarations.h"
#include "stringutils.h"

#include "iangles.h"
#include "panel.h"
#include "words.h"

#include "RXCompoundCurve.h"
#include "RXSeamcurve.h"

RXSeamcurve::RXSeamcurve()
{
    assert("dont use default SC constructor"==0);
}
RXSeamcurve::RXSeamcurve(class RXSail *p )
    :RXEntity(p)
{
    this->Esail=p;
    Init();
}
RXSeamcurve::~RXSeamcurve() {
    this->CClear() ;

    // No longer.  assert(this->TYPE==PCE_DELETED);
}


int RXSeamcurve::SeamCurve_Init() {

    int l;
    //   sc_ptr this = this;

    this->seam=0;  /* 1 for seam, 0 for curve*/
    this->n_angles=0;
    this->ia=NULL;

    this->BorGcurveptr = NULL;

    this->End1dist=NULL;
    this->End2dist=NULL;
    this->end1sign =0;
    this->end2sign =0;
    this->strt_A=(float)0.0;
    /* end<n>type() will be <end<n>ptr->type()> */        /* site, seam,curve */


    this->End1site=NULL;
    this->End2site=NULL;  /* ptr to RXEntity of end2 {SITE} */

    this->strt_A=(float)0.0;    /* an estimate of the initial slope of the IA curve */

    this->sketch=0;  /* 1 if p_aneller is to ignore */

    this->end1angle=0.0; /* ready for export */
    this->end2angle=0.0;
    int i;
    for (i=0;i<3;i++)
        this->Defined_Material[i] = NULL;

    for (l=0;l<3;l++)
    {
        this->m_arcs[l]=(float)0.0;       /* current arclength */
        this->p[l] =  NULL;
        this->c[l] = 0;
        if(m_pC[l])
            delete m_pC[l];
        this->m_pC[l] = new RXCurve;
        assert(!(this->m_pC[l]->Get_ds()));
        this->m_pC[l]->Set_ds(NULL);
        this->m_pC[l]->Set_Ent(this);

        //CHECK
#ifdef _DEBUG
        RXTRObject * l_RX = RXTRObject::Cast(p->m_pC[l]);
        assert(l_RX);
#endif
    }



    this->Make_Pside_Request = 1;
    this->pslist=NULL;
    this->npss=0;


    return(1);
} 


int RXSeamcurve::Resolve(void){ return Resolve(0,0,0);}

int RXSeamcurve::Finish(void){

    // WRONG  This routine is not symmetrical in c1 and c2 of any relsite on the end
    // It probably wont work if an ending relsite is a XSITE
    RXEntity_p s1,s2, sc1=NULL, sc2=NULL;
    Site*np;
    RXOffset*dummyo=NULL;
    class RXBCurve *bcc;
    sc_ptr  bcs;
    ON_3dPoint e1,e2;
    int err=0;
    sc_ptr  ce;

    if(this->m_scMoulded || this->Geo_Or_UV ) {
        RXEntity_p mould;
        mould = this->Esail->GetMould();
        if(mould->Needs_Finishing)
            return 0;
    }

    s1=this->End1site; s2=this->End2site;

    if( this->BorGcurveptr && this->BorGcurveptr->Needs_Finishing)
        return(0);
    if(!s1 || !s2)  {
        this->Needs_Finishing=0;  /*  to accept zero endNptrs */
        if(!this->BorGcurveptr) {
            std::string buf
                    =string("Cant build this model\n Zero endNptrs AND Basecurve " )
                    +string(this->type()) + string("  ")
                    +string(this->name()) ;
            this->Print_One_Entity(stdout);
            rxerror(buf.c_str(),3);
            RXTHROW(buf.c_str() );
        }
        if(this->Flags&SC_SUB_SEGMENT){
            bcs = (sc_ptr  )this->BorGcurveptr;
            this->m_arcs[0]=this->m_arcs[1]=this->m_arcs[2] = bcs->GetArc(1);
        }
        else {
            bcc =  (class RXBCurve*) this->BorGcurveptr ;
            this->m_arcs[0]=this->m_arcs[1]=this->m_arcs[2] = (float) PC_Polyline_Length(bcc->c,bcc->p);//UGH need a 'Basecurve_Length fn
        }
    }
    else {


        if( s1->Needs_Finishing || s2->Needs_Finishing)
            return(0);

        if(!Get_Position(s1,dummyo,1,&e1))err=1;
        if(!Get_Position(s2,dummyo,1,&e2))err=1;
        if(err)
            return(0);

        if(this->BorGcurveptr) 	{	 /* if p3 null arcs were set by ReadRLX */
            this->m_arcs[0]=this->m_arcs[1]=this->m_arcs[2] = e1.DistanceTo (e2);
            if( this->m_arcs[0] <= 0.0){ cout<< "setting arcs to 999(!)"<<endl;  this->m_arcs[0]=this->m_arcs[1]=this->m_arcs[2] = 999.0; }
        }
        // this isnt necessary if we have done a 3dm_Cut.

        if(!this->FlagQuery(SC_3DM_CUT)) { // unless it already is in
            dummyo= dynamic_cast< RXOffset*>( this->AddExpression (new RXOffset( L"E0",L"0.0",this,this->Esail ))); this->SetRelationOf(dummyo,aunt,RXO_EXP_OF_ENT); //done
            dummyo->SetAccessFlags(RXE_PRIVATE);
            (dummyo->Evaluate(this ,1));
            this->Insert_One_IA(s1,dummyo );

            dummyo= dynamic_cast< RXOffset*>( this->AddExpression (new RXOffset( L"E1",L"100.0%",this,this->Esail )));  this->SetRelationOf(dummyo,aunt,RXO_EXP_OF_ENT); //done
            dummyo->SetAccessFlags(RXE_PRIVATE);
            dummyo->Evaluate(this ,1);
            this->Insert_One_IA(s2,dummyo );
        }

        if (strieq(s1->type(),"relsite") ||strieq(s1->type(),"site") ){
            /* we change p1 so it refers
        to the curve on which p1 lies */
            s1->SetRelationOf(this,child|niece,RXO_N1_OF_SC);
            np = (Site *)s1;
            if (np->CUrve[0]) {
                if(np->CUrve[0]->Needs_Finishing)
                    return(0);

                this->end1sign = Compute_Entity_Endsign(s1,e1,e2);
                sc1= np->CUrve[0];
                ce = (sc_ptr  )sc1;
                ce->Make_Pside_Request =1;	 /* so patches within just one	panel mesh OK */
                if(this->seam  && !ce->m_isFlat) { // July 2001  only fanned.// before dec 2011 was if(this->seam  &&

                    this->SetRelationOf(sc1,child|niece,RXO_SC_OF_FANNED);
                }
                this->End1dist = np->Offsets[0]; 	 /* functional change. Used to copy */


            }
        }
        if (strieq(s2->type(),"relsite") || strieq(s2->type(),"site"))  {
            np = (Site *)s2;
            s2->SetRelationOf(this,child|niece,RXO_N2_OF_SC);
            if (np->CUrve[0]) {
                if(np->CUrve[0]->Needs_Finishing)
                    return(0);
                this->end2sign =Compute_Entity_Endsign(s2,e2,e1);
                sc2=np->CUrve[0];
                ce = (sc_ptr  )sc2;
                ce->Make_Pside_Request =1;
                if(this->seam  &&!ce->m_isFlat) {

                    this->SetRelationOf(sc2,child|niece,RXO_SC_OF_FANNED);
                }
                this->End2dist = np->Offsets[0];

            }
            else {
                RXExpressionI * lengthQ = this->FindExpression(L"length",rxexpLocal);
                if(lengthQ) {
                    double length = lengthQ ->evaluate(); //PC_Value_Of_Q(p->length);
                    if(length >FLOAT_TOLERANCE){
                        this->SetRelationOf(s2,child|niece,RXO_SC_OF_FANNED);
                    }
                }
            }
        }

        if(!g_Janet && this->BorGcurveptr) {
            this->BorGcurveptr->SetRelationOf(this,child|niece,RXO_BC_OF_SC);
        }
        Insert_Angle_References(sc1,sc2,(sc_ptr)this);

        this->Needs_Finishing=0; /* if its a cut, it may end up referencing itself
      //				in the following compute call */
        if(this->gauss) {
            this->Needs_Finishing=1; // wierd
            if(this->Flags&CUT_FLAG) {
                rxerror(" cant have CUT GAUSS seams ",3);
                this->Flags-=CUT_FLAG;}
        }

    } /* if zero endNsites */

    err = this->Compute();     // Might have deleted it
    if(err == -1) {
        cout<<this->type()<<","<<this->name()<<" didnt finish: "<<endl;
        return(0);	// error
    }
    else if (err==PCE_DELETED)
        return PCE_DELETED;


    this->SetNeedsComputing(); /* PH was 0 1/1/95 */
    return(1);
}


int RXSeamcurve::CClear(){
    int rc=0;
    // clear stuff belonging to this object
    rc+=this->Delete_Seamcurve();
    // call base class CClear();
    rc+=RXEntity::CClear ();
    return rc;
}


int RXSeamcurve::Init() {

    int i;

    //	SAIL*l_sail = this->Esail ;
    this->seam=0;  /* 1 for seam, 0 for curve*/
    this->edge=0; this->Flags=0; this->Xabs =0; this->Yabs =0; this->YasX =0; this->m_scMoulded =0; this->m_isFlat=0; this->XYZabs =0; this->Needs_Cutting=0;
    this->n_angles=0;
    this->ia=NULL;
    this->Geodesic=0;
    this->m_spare=0;
    this->BorGcurveptr = NULL;
    this->Geo_Or_UV =0;
    this->GeodesicList=0;

    this->End1dist=NULL;
    this->End2dist=NULL;
    this->end1sign =0;
    this->end2sign =0;
    this->strt_A=(float)0.0;

    /* end<n>type will be <(*end<n>ptr).type> */        /* site, seam,curve */


    this->End1site=NULL;
    this->End2site=NULL;  /* ptr to RXEntity of end2 {SITE} */

    this->strt_A=(float)0.0;    /* an estimate of the initial slope of the IA curve */

    this->sketch=0;  /* 1 if p_aneller is to ignore */

    this->end1angle=0.0; 	this->old_end1angle=0.0; /* ready for export */
    this->end2angle=0.0;	this->old_end2angle=0.0;
    this->e1w = this->e2w=0;
    this->GaussArea =0;

    for (i=0;i<3;i++){
        this->Defined_Material[i] = NULL;
        memset(&this->Mat_Angle [i],0,sizeof(struct SIDE_ANGLE));
        this->mats[i]=0;
        this->g1p[i]=0;
        this->g2p[i]=0;
        this->G1Fac[i]=this->G2Fac[i]=0;
    }

    for (i=0;i<3;i++)
    {
        this->m_arcs[i]=(float)0.0;       /* current arclength */
        this->p[i] =  NULL;
        this->c[i] = 0;

        this->m_pC[i] = new RXCurve;
        assert(!(this->m_pC[i]->Get_ds()));
        this->m_pC[i]->Set_ds(NULL);
        this->m_pC[i]->Set_Ent(this);

        //CHECK
#ifdef _DEBUG
        RXTRObject * l_RX = RXTRObject::Cast(this->m_pC[i]);
        assert(l_RX);
#endif
    }



    this->Make_Pside_Request = 1;
    this->pslist=NULL;
    this->npss=0;
    this->gauss=0;
    this->gauss_pts=0;		/* not yet sure what it will be */
    this->gauss_npts=0;		/* this will say how many of the above there are */
    this->moved_relsites_flag=0;	/* means re-copy the offsets */
    this->Defined_Length=0;
    this->Inside_List=0;

    return(1);
} 

double  RXSeamcurve::MeshSize(const RXSitePt&p) const
{ double rv;
    RXQuantity *ee = dynamic_cast<RXQuantity *> (FindExpression(L"grid",rxexpLocal));//AndTree));

    if(!ee)
        return  this->Esail ->MeshSize(p);
    try{
        rv = ee->evaluate();
    }
    catch(...){
        cout<<" cant evaluate mesh on sc so use sail"<<endl;
        return  this->Esail ->MeshSize(p);
    }
    if(rv < 0.00001) {
        const char *lp=" Small Grid Density on SC .  Please check your script file ";
        rxerror(lp ,4);
        rv=0.00001;
    }
    return rv;
}


int RXSeamcurve::Resolve (const RXEntity_p pp_bce,const RXEntity_p pp_p1,const RXEntity_p pp_p2) 
{
    // may 2008 if we know the basecurve we skip searching for it.
    /* 8/12/95 1) basecurve may be blank or omitted. IN either case "straight" is used
   2) there is an optional last field for the length
    The optional 5th field length format is maintained for backwards compatibility
    This should NOT be documented
 */
    /* seam record is of type
   seam:  name  :  SITE1 : SITE2 : basecurve [length] [[[optional adjectives] leftmat] rightmat] !comment
   */

    RXEntity_p bce= pp_bce; RXEntity_p p1=pp_p1;  RXEntity_p p2= pp_p2;
#define SLEN  1024

    char n1[SLEN],n2[SLEN],basecurve[SLEN],l_att[SLEN],string[SLEN];
    const char *l_type, *l_name;
    char  DStringOrig[SLEN];

    int seam,l_IsEdgeFlag,k;

    RXEntity_p p3=NULL, gauss=NULL;

    int retval=0;

    this->SetLineLwr();
    const char *l_Line= this->GetLine();
    memset(l_att,0,SLEN-1);
    std::string sline(l_Line);
    rxstriptrailing(sline);
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();
    if(nw<4) // or whatever
        return 0;
    l_type=wds[0].c_str ();

    if (strieq(l_type,"seam")) seam=1;
    else seam=0;
    if (strieq(l_type,"edge")) {l_IsEdgeFlag=1; seam=0;}
    else l_IsEdgeFlag=0;
    if (strieq(l_type,"cut")) {l_IsEdgeFlag=0; seam=0;}

    rxgetword(wds,6,l_att ,SLEN) ;
    RXAttributes rxa(l_att);
    QString qbuf;
    l_name=wds[1].c_str();

    if (rxgetword( wds  ,2, n1,SLEN)|| rxa.Extract_Word( "$n1",qbuf)){ if(qbuf.length())strcpy(n1,qPrintable(qbuf));
        qbuf.clear();
        if (rxgetword(wds,3,n2,SLEN)|| rxa.Extract_Word( "$n2",qbuf)){if ( qbuf.length() )strcpy(n2,qPrintable(qbuf));
            k = 3;
            if (rxgetword(wds,++k,basecurve ,SLEN)){ // if basecurve is a valid number, try again
            }
            else
                strcpy(basecurve,"straight");

            if (!rxgetword(wds,++k, DStringOrig ,SLEN))
                strcpy(DStringOrig,"000.0");

            assert(*l_type);	if (strieq(l_type,"cut")) rxa.Add("$cut");
            if (rxIsEmpty(n1)&&rxIsEmpty(n2)&&(!rxa.Find("$xyzabs")))
            {
                rxa.Add("$xyzabs");
            }

            if(rxa.Find("edge")) l_IsEdgeFlag=1;

            if(!rxa.Extract_Double("e1w",&e1w))
                e1w=0.0;
            if(!rxa.Extract_Double("e2w",&e2w))
                e2w=0.0;

            if(rxa.Extract_Word("$gaussian_curvature",qbuf)){
                gauss = this->Esail->Get_Key_With_Reporting("interpolation surface,scalar",qPrintable(qbuf));
                if(!gauss) {this->CClear(); HC_Define_System_Options("C string length=255"); return(0); }

            }
            if(rxa.Find("$uv") || rxa.Find("moulded") || rxa.Find("geodesic")){
                if(this->Esail && (!(this->Esail->GetMould())))
                { this->CClear();HC_Define_System_Options("C string length=255"); return(0); }
            }


            this->SeamCurve_Init();
            if(!this->FlagSet( rxa))
            {
                this->CClear();
                HC_Define_System_Options("C string length=255");
                return(0);
            }

            this->gauss = gauss;
            if(this->gauss)  gauss->SetRelationOf(this,child|niece,RXO_GAUSS_OF_SC);

            if(rxa.Extract_Word("mat1",qbuf)) { strcpy(string,qPrintable(qbuf));
                Make_Valid_Segname( string) ; // may 2005
                this->Defined_Material[2] = this->Esail->Get_Key_With_Reporting("fabric",string);
                if(!this->Defined_Material[2] ) {
                    this->CClear();
                    HC_Define_System_Options("C string length=255");
                    return(0);
                }
                this->Defined_Material[2]->SetNeedsComputing();
            }
            if(rxa.Extract_Word("mat2",qbuf)) { strcpy(string,qPrintable(qbuf));
                Make_Valid_Segname( string) ; // may 2005
                this->Defined_Material[0] = this->Esail->Get_Key_With_Reporting("fabric",string);
                if(!this->Defined_Material[0] ) {
                    HC_Define_System_Options("C string length=255");
                    this->CClear();
                    return(0);
                }
                this->Defined_Material[0]->SetNeedsComputing();
            }

            /* if XYZabs we are allowed to skip p1 or p2 */
            if(!p1) {
                if(this->XYZabs && rxIsEmpty(n1)) p1=this; // a flag value  for 10 lines or so
                else p1= this->Esail->Get_Key_With_Reporting("site,relsite",n1,true);
            }
            if(!p2){
                if(this->XYZabs && rxIsEmpty(n2) ) p2=this;
                else  p2= this->Esail->Get_Key_With_Reporting("site,relsite",n2,true);
            }
            if(bce && !bce->Needs_Resolving)
                p3=bce;
            else {
                if(! (this->Flags&SC_SUB_SEGMENT) )
                    p3= this->Esail->Get_Key_With_Reporting("basecurve,gausscurve,3dcurve,2dcurve,spline",basecurve); // 3dCurve mar 2003
                else
                    p3= this->Esail->Get_Key_With_Reporting("edge,seam,curve,seamcurve,nodecurve",basecurve); // SUB-SC not coded
            }
            if ( !p1 ||  !p2 ||  !p3)
            {
                this->CClear(); HC_Define_System_Options("C string length=255"); return(0);
            }
            if(p3==this)
                p3=0;
            if(p1 != this)  this->End1site = (Site*)  p1; else  this->End1site=NULL;
            if(p2 != this)  this->End2site = (Site*)  p2; else  this->End2site=NULL;
            this->BorGcurveptr=p3;
            this->seam=seam;
            this->edge=l_IsEdgeFlag;// type is 'edge' or the atts contain 'edge'

            if(PC_Finish_Entity("seamcurve",l_name, rxa ,l_Line)) {
                retval=1;
                this->Needs_Resolving= 0;
                this->Needs_Finishing = 1;
                this->SetNeedsComputing();
                RXExpressionI *expd ;
                expd=this->AddExpression (new RXQuantity( L"depth",TOSTRING(DStringOrig ),_T("m"),this->Esail ));
                this->SetRelationOf(expd,parent|aunt|spawn,RXO_DEPTH_OF_SC);//the Enode will take care of deletion
                if( !expd->ResolveExp() )
                {
                    this->CClear();HC_Define_System_Options("C string length=255"); return(0);
                }

                if(!g_Janet) {
                    if(p1 !=this){
                        p1->SetRelationOf(this,child|niece,RXO_N1_OF_SC);}
                    if(p2 !=this) {
                        p2->SetRelationOf(this,child|niece,RXO_N2_OF_SC);}
                    if(p3&& p3!=this){
                        p3->SetRelationOf(this,child|niece,RXO_BC_OF_SC);}

                } // janet
                ResolveCommon(rxa);
            }// if PC_Finish...
        }
    }

    HC_Define_System_Options("C string length=255");
    return(retval);
}  // int RXSeamcurve::Resolve
int RXSeamcurve::ResolveCommon(const RXAttributes &att  )
{
    QString qbuf;
    if(this->Defined_Material[0])
        this->Defined_Material[0]->SetRelationOf(this,child|niece,RXO_MAT1_OF_SC);
    if(this->Defined_Material[2])
        this->Defined_Material[2]->SetRelationOf(this,child|niece,RXO_MAT2_OF_SC);;

    if(att.Find("$pocket"))  { this->SetRelationOf( this->Create_Pocket_From_SC(),spawn|child|niece,RXO_POCKET_OF_SC);}
    if(att.Find("$patch"))   { this->SetRelationOf( this->Create_Patch_From_SC() ,spawn|child|niece, RXO_PATCH_OF_SC);}

    if(att.Find("$beam"))	{ RXEntity_p beam =this->Create_Beam_From_SC(); this->SetRelationOf(beam ,spawn|child|niece,RXO_SC_OF_BEAM );}
    if(att.Find("$string"))  {
        m_spare =this->Create_String_From_SC();
    }  // spare is used by PCFile

    //connect	g_b_luff 	string 	headstay 	boat 	string 	luff 	genoa 	$slide $spline $interp $cspline
    if( this->RXEntity::AttributeGet("slave", qbuf)  ) {  // $slave=string%genoa%luff  or $slave=luff;
        if(!qbuf.contains("%"))
            qbuf = QString ("string: ")+qbuf + ":" + this->Esail->GetQType();
        else
            qbuf.replace("%",":");

        QString line, qcname;
        qcname = QString(this->name()) + QString ("_ct");
        RXEntity_p se=0;
        int depth = 1 + this->generated;
        line = QString("connect :") + qcname + " : string : " +this->name() + ": " + this->Esail->GetQType() +":" + qbuf ;
        line += QString(":")  + this->m_rxa.GetPart("$slide,$spline,$interp,$cspline,$noslide");
        se=Just_Read_Card(this->Esail ,qPrintable( line) ,"connect",qPrintable( qcname ),&depth);
        if(se) {
            this->SetRelationOf(se ,spawn|child|niece,	RXO_CONNECT_CHILD );
            se->Resolve();
            se->Needs_Finishing=1;
            se->SetNeedsComputing();
        }
    } //slave
    if( this->RXEntity::AttributeGet("master", qbuf)  ) {
        if(!qbuf.contains("%"))
            qbuf = QString ("string: ")+qbuf + ":" + this->Esail->GetQType();
        else
              qbuf.replace("%",":");
        QString line, qcname;
        qcname = QString(this->name()) + QString ("_ct");
        RXEntity_p se=0;
        int depth = 1 + this->generated;

        line = QString("connect :") + qcname + " : " +qbuf  +": string: " + this->name()  +": "+ this->Esail->GetQType();
        line += QString(":") + this->m_rxa.GetPart("$slide,$spline,$interp,$cspline,$noslide");
        se=Just_Read_Card(this->Esail ,qPrintable( line) ,"connect",qPrintable( qcname ),&depth);
        if(se) {
            this->SetRelationOf(se ,spawn|child|niece,	RXO_CONNECT_CHILD );
            se->Resolve();
            se->Needs_Finishing=1;
            se->SetNeedsComputing();
        }
    } //master

    if(att.Extract_Word("$grid",qbuf)){
        if(!qbuf.isEmpty()){
            RXObject*eo;
            RXExpressionI *olde;
            if((olde= this->FindExpression(L"grid",rxexpLocal) ))		 //looks in this->explist and recursively in this->parent->explist.
                olde->Change(qbuf.toStdWString());
            else{	 // not strictly correct - the relation may not be set
                RXQuantity *q = new RXQuantity(L"grid",qbuf.toStdWString(),L"m",this->Esail);
                RXExpressionI *ee =this->AddExpression (q );
                assert(ee) ;
                if(!q->Resolve ()) {
                    delete q;
                }
                else{
                    //double v =
                    q->evaluate ();
                    eo = dynamic_cast<RXObject*>(q);
                    assert(eo);
                    this->SetRelationOf(eo,aunt|spawn,RXO_GRID_OF_SC);
                }
            }
        }

    }
    return 1;
}

int RXSeamcurve::Dump( FILE *fp) const
{
    static int level = 0;
    if(level>0)
        return 0;

    level ++;

    int start,end;
    int count = this->n_angles;

    fprintf(fp,"    seamflag  %d\t",this->seam);
    fprintf(fp,"    edgeflag  %d\t",this->edge);
    fprintf(fp,"    moulded  %d\t",this->m_scMoulded);
    fprintf(fp,"    Geo_Or_UV flag    %d\t",this->Geo_Or_UV);
    fprintf(fp,"    Geodesic  %d\t",this->Geodesic);
    fprintf(fp,"    Flags     %d\n",this->Flags);

    if(this->Defined_Length) fprintf(fp,"   Fixed_Length. length\n");

    fprintf(fp,"  M_P_Request %d\n" ,this->Make_Pside_Request);
    if((this->BorGcurveptr)!=NULL) {
        fprintf(fp,"    basecurve %s, a %s\n",this->BorGcurveptr->name(),this->BorGcurveptr->type());
    }
    else
        fprintf(fp,"     basecurve NULL\n");

    for(int L=0;L<3;L+=2){
        if(this->Defined_Material[L])
            fprintf(fp,"  defined material %d %s\n",L,this->Defined_Material[L]->name());
        if(this->Mat_Angle[L].flag & 1 )
            fprintf(fp,"  use as ref side  %d angle = %f \n",L,this->Mat_Angle[L].angle  );
        if(this->Mat_Angle[L].flag & 4  ) {
            if(this->Mat_Angle[L].str )
                fprintf(fp,"  use as ref side  %d sc= %s \n",L,this->Mat_Angle[L].str );
            else
                fprintf(fp,"  use as ref side  %d sc= %s \n",L, "NULL" );
        }
    }

    //if(this->seam) Print_BroadSeam(np,fp);
    if(this->End1site)
        fprintf(fp,"    End1Site   %10s\n", this->End1site->name() );
    else fprintf(fp,"    End1Site  NULL\n");
    if(this->End2site)
        fprintf(fp,"    End2Site   %10s\n",this->End2site->name() );
    else fprintf(fp,"    End2Site  NULL\n");
    fprintf(fp,"    e1sign    %d\n",this->end1sign );
    fprintf(fp,"    e2sign    %d\n",this->end2sign );
    if(this->End1dist)
        this->End1dist->Print(fp);
    else
        fprintf(fp,"    e1dist NULL\n");
    if(this->End2dist)
        this->End2dist->Print(fp);
    else
        fprintf(fp,"    e2dist NULL\n");
    fprintf(fp,"    e1angle      %f\n",this->end1angle );
    fprintf(fp,"    e2angle      %f\n",this->end2angle );

    fprintf(fp,"    strt_A       %f\n",this->strt_A );
    fprintf(fp,"    Sketch       %d\n",this->sketch );
    fprintf(fp,"    cut flag     %d\n",this->Flags&CUT_FLAG );
    fprintf(fp,"    snaptonodes  %d\n",this->Flags&SNAP_TO_NODES);
    fprintf(fp,"    snapEndsOnly %d\n",this->Flags&SNAP_TO_ENDS_ONLY);
    fprintf(fp,"    XYZabs       %d\n",this->XYZabs );
    fprintf(fp,"    end1width    %12.7g\n",this->e1w);
    for(int k=0;k<3;k+=2) {
        if(this->g1p[k])
            fprintf(fp,"  g1p[%d]       %s  g1fac=%f\n",k,this->g1p[k]->name(),this->G1Fac[k]);
    }
    fprintf(fp,"    end2width %12.7g\n",this->e2w );
    for(int k=0;k<3;k+=2) {
        if(this->g2p[k])
            fprintf(fp,"  g2p[%d]       %s  g2fac=%f\n",k,this->g2p[k]->name(),this->G2Fac[k]);
    }
    fprintf(fp," GAUSS Area %f\n",this->GaussArea);
    //TotalGaussArea += this->GaussArea;

    if(this->gauss) fprintf(fp," GAUSS %s\n",this->gauss->name());
    fprintf(fp,"    Npsides   %d\n",this->npss);
    for (int k=0;k<this->npss;k++) {
        PSIDEPTR pp = this->pslist[k];
        fprintf(fp,"          %3d     %s  from %S to %S\n",k, pp->name(),pp->End1off->GetText().c_str(),pp->End2off->GetText().c_str());
    }

    fprintf(fp,"    n_angles  %d\n\n",this->n_angles );
    double sum= 0.0;
    fprintf(fp,"     angle       \toffstr\toffX \tOff Val\tsign\tname\tend\n");

    for (int k=0;k<count;k++) {
        if(this->ia[k].Angle) {
            sum=sum+ *this->ia[k].Angle;
            fprintf(fp," %f \t",*this->ia[k].Angle);
        }
        else
            fprintf(fp,"IAKANULL\t");

        if(this->ia[k].m_Offset) {
            this->ia[k].m_Offset->Print( fp);
            fprintf(fp," %8f\t",this->ia[k].m_Offset->Evaluate(NULL,1));
        }
        else
            fprintf(fp," OffsetNULL \t \t \t");
        if(this->ia[k].sign)
            fprintf(fp," %2d\t",*(this->ia[k].sign));
        else
            fprintf(fp,"signNULL\t");

        fprintf(fp," %10s\t",this->ia[k].s->name());
        fprintf(fp," end%d\n",this->ia[k].End);
        fprintf(fp,"\n");
    }
    fprintf(fp,"    Sum %f\n",sum);

    if (this->GeodesicList)
        cout<< "TODO: Print_GList(this->GeodesicList,fp);"<<endl;
    else
        fprintf(fp,"     no geodesic\n");

    if (!this->AttributeGet("rlx")) { /* to save paper only print one curve of RLX seams */
        start=0;end=3;
    }
    else {
        start=1;end=2;
    }
    count = max(this->c[0],max(this->c[1],this->c[2]));
    fprintf(fp,"     ");
    for (int L=start;L<end;L++) {
        if(this->m_pC[L] && this->m_pC[L]->IsValid ()  )
            fprintf(fp,"     Curve %d length %f ",L,GetArc(L));
        else
            fprintf(fp,"     NO Curve %d       ",L);
    }

    for (int k=0;k<count;k++) {
        fprintf( fp,"\n  %4d ",k);
        for (int L=start;L<end;L++) {
            if(k < this->c[L]) {
                VECTOR v= (this->p)[L][k];
                fprintf(fp," ( %7.4f %7.4f %7.4f ) ", v.x,v.y,v.z);
            }
            else
                fprintf(fp,"                                           ");
        }
    }
    fprintf( fp,"\n");

    //    if(this->gauss_pts)
    //        Print_SC_GaussPoints(fp, np);


    level --;
    return 1;
}


int RXSeamcurve::Demote_To_Sketch( ) {
    if(this->sketch) return 0;
    this->sketch=1;
 //   Replace_String(&(this->line),"draw","sketch");
    //   if(this->attri butes)
    //       Replace_String(&(this->attr ibutes),"draw","sketch");
    this->AttributeRemove("draw");
    this->AttributeSet("$sketch");
    this->ReWriteLine();
    for(;this->npss>0;) {
        PSIDEPTR pse = this->pslist[0];
        pse->Kill( );
    }
    if(this->pslist){
        RXFREE(this->pslist);
        this->pslist=NULL;
    }

    return 1;
}



int RXSeamcurve::FlagSet(const int what){
    Flags=Flags|what;
    return Flags;
}

int RXSeamcurve::FlagClear(const int what){
    if(Flags&what) Flags-=what;
    return Flags;
}
int RXSeamcurve::FlagQuery(const int what) const{
    if(Flags&what) return 1;
    return 0;
}
int RXSeamcurve::FlagSet(const RXAttributes &rxa) {

    /* attribute options are:
 flat/fanned.        Flat means ignore imposed angles.
 Xabs/Xscaled        Xabs means trim curve to fit (eg preserve length)
 Yabs/Yscaled /YasX  Yabs means use Y offsets, and depth as a multiplier
      Yscaled = use Y offsets, but scale so d=depth
         YasX means preserve aspect ratio during scaling	of x
 Yscaled is the default.
 if XYZabs we ignore Xabs Yabs flat fanned
 moulded				= pull onto any mould
 geodesic			= QV (requires moulded)
  */
    double v;
    QString qs;
    RXSTRING ss;

    this->edge=(strieq(this->type(),"edge") || rxa.Find("edge")  );

    this->sketch=rxa.Find("sketch");// else "draw");

    if(rxa.Find("xyzabs"))  this->XYZabs=1;
    else 	{
        this->XYZabs=0;
        this->m_isFlat = rxa.Find("flat");// else ,"fanned");
        this->Xabs = rxa.Find("xabs"); // else "xscaled");

        if(this->Xabs && this->seam) { rxerror(" cannot have Xabs on a seam ",1); this->Xabs=0;}
        if(this->Xabs && !this->m_isFlat)    { rxerror(" cannot have Xabs on a fanned curve ",1); this->Xabs=0;}

        this->Yabs = 0; this->YasX=0;
        if (rxa.Find("yscaled")) {
            this->Yabs=0;
            this->YasX=0;
        }
        else if (rxa.Find("yasx")) {
            this->Yabs= 0;
            this->YasX=1;
        }
        else if (rxa.Find("yabs")) {
            this->Yabs=1;
            this->YasX=0;
        }

    }
    if (rxa.Find("$moulded"))
    {this->m_scMoulded=1;  this->FlagSet(MOULDED); this->FlagClear(NOMOULD); }
    else
    { this->m_scMoulded=0;  this->FlagSet(NOMOULD);  this->FlagClear(MOULDED); }
    if (rxa.Find("$nomold")  || rxa.Find("$nomould"))  { // overrides
        this->m_scMoulded=0;
        this->FlagSet( NOMOULD); this->FlagClear(MOULDED);
    }


    if (rxa.Find("$geodesic"))	  {
        this->Geo_Or_UV=1;
        this->Geodesic = 1;
    }
    else {
        this->Geo_Or_UV=0;
        this->Geodesic=0;
    }

    if(rxa.Find("$uv")) this->Geo_Or_UV = 1;

    if(rxa.Find("$stripe"))
        this->FlagSet( SC_STRIPE);

    if(rxa.Find("$filament")) {
        this->Defined_Length = 0;
        this->FlagSet( FILAMENT);
        RXExpressionI  *q;
        if (rxa.Extract_Word(L"$ea" ,ss)) {
            q=this->AddExpression (new RXQuantity( L"ea",TOSTRING(ss),_T("N"),this->Esail )); this->SetRelationOf(q,aunt,RXO_EXP_OF_ENT); //done
        }
        if (rxa.Extract_Word(L"$ti" ,ss)) {
            q=this->AddExpression (new RXQuantity( L"ti",TOSTRING(ss),_T("N"),this->Esail )); this->SetRelationOf(q,aunt,RXO_EXP_OF_ENT); //done
        }
        if(rxa.Find("$nobuckle"))
            this->FlagSet(PCF_FIL_NO_BUCKLE);
    } // if filament
    else
        if(rxa.Extract_Word(L"$fixed_length",ss))
        {
            RXExpressionI *expl;
            expl= this->AddExpression (new RXQuantity( L"length",TOSTRING( ss ),_T("m"),this->Esail ));
            this->SetRelationOf(expl,aunt|spawn,RXO_EXP_OF_ENT); //done
            if(expl->ResolveExp() )
                this->Defined_Length = 1;
            else
                return 0;
        }

    // 0=ignore 1 = ref psidechord 2 ref SC chord 4 = refSC

    if (rxa.Extract_Double("left" ,&v)) {
        this->Mat_Angle[2].flag = 1;
        this->Mat_Angle[2].angle=(float)v;
    }
    else if(rxa.Find("left")) {
        this->Mat_Angle[2].flag = 1;
        this->Mat_Angle[2].angle=0.0;
    }
    if (rxa.Extract_Double("right" ,&v)) {
        this->Mat_Angle[0].flag = 1;
        this->Mat_Angle[0].angle=(float)v;
    }
    else if(rxa.Find("right")) {
        this->Mat_Angle[0].flag = 1;
        this->Mat_Angle[0].angle=0.0;
    }
    if(rxa.Extract_Word("$ref1",qs)){
        this->Mat_Angle[2].flag = 4;
        this->Mat_Angle[2].angle=(float)0.0; //WRONG but fixup for Frank
        this->Mat_Angle[2].str = STRDUP(qPrintable(qs));

    }
    if(rxa.Extract_Word("$ref2",qs)){
        this->Mat_Angle[0].flag = 4;
        this->Mat_Angle[0].angle=(float)0.0; //WRONG but fixup for Frank
        this->Mat_Angle[0].str = STRDUP(qPrintable(qs));
    }

    if(rxa.Find("cut") && !(rxa.Find("nocut") ) ) {
        this->FlagSet( CUT_FLAG);
        this->Needs_Cutting = 1;
    }
    if(rxa.Find("$snapendsonly") ){
        this->FlagSet(SNAP_TO_ENDS_ONLY);
        this->Needs_Cutting +=1;
    }
    else if(rxa.Find("$snap") ){
        this->FlagSet( SNAP_TO_NODES);
        this->Needs_Cutting +=1;
    }
    return(1);
}    



RXEntity_p  RXSeamcurve::Create_Pocket_From_SC( ){
    /* the curve is
seamcurve	luff	tack	clew	parabola	0.5m	Xscaled Yscaled fanned	V270	diax	20m

the pocket is 
compound curve	name_c	name
! keyword	name	(blank)	(blank)	the batten	compound curve */

    char pockline[512];
    int depth = 1;
    RXEntity_p cc, pp;
    sprintf(pockline, "compound curve: %s_c :%s  ",this->name(),  this->name());
    cc= Just_Read_Card(this->Esail,pockline,"compound curve",this->name(),&depth);
    this->SetRelationOf(cc,spawn|child|niece,RXO_CC_OF_SC);
    sprintf(pockline, "pocket: %s : : : %s_bat :%s_c ",this->name(), this->name(), this->name());
    pp= Just_Read_Card(this->Esail,pockline,"pocket",this->name(),&depth);
    return  pp;
}

RXEntity_p   RXSeamcurve::Create_Patch_From_SC(){
    /*
field	clewpatch	CpatchCurve	(blank)	kevlar	inverse radial	clew	*/
    char patchline[512],   patchmat[256], focus[256], type[256];
    int depth = 1 + this->generated;

    if(!AttributeGet( "$mat",patchmat))
        sprintf(patchmat,"mat_%s",name());

    // the focus name is name up to the first blank or .
    if(!AttributeGet( "$focus",focus)){
        strcpy(focus, name());
        PC_Strip_Leading(focus);
        Make_Valid_Segname(focus);
    }
    if(!AttributeGet("$field",type))
        strcpy(type,"inverse radial");

    sprintf(patchline, "field: %s : %s : : %s : %s :%s :l-o-c-a-l ",name(), name(), patchmat,type,focus);

    return Just_Read_Card(this->Esail,patchline,"field",name(),&depth);

}
RXEntity_p  RXSeamcurve::Create_Beam_From_SC(){

    /* beamstring	headstay	%curve	leech	%endlist	EA	length	Prestress	$noslide
*/
    char strline[512] ;
    RXEntity_p se=0;

    double ea=0.0,ti=0.0,zi=0.0;
    int k, depth = 1 + this->generated;

    sprintf(strline, "beamstring: %s :%%curve: %s :%%endlist: %f : %f : %f :%s",this->name(), this->name(), ea,zi,ti, qPrintable ( this->AttributeString())  );

    se=Just_Read_Card(this->Esail , strline,"beamstring",this->name(),&depth);

    if(se) {
        RX_FEBeam *ses= dynamic_cast<RX_FEBeam *>(se);
        k=ses->Resolve(this);  // may fail if its material isnt resolved.
    }
    return se;
}
/************************************************************/
RXEntity_p  RXSeamcurve::Create_String_From_SC(){

    /* string	headstay	%curve	leech	%endlist	EA	length	Prestress	$noslide
*/
    char strline[512],  sl[256];
    RXEntity_p se=0;

    double ea=0.0,ti=0.0,zi=0.0;
    int depth = 1 + this->generated;


    if(this->AttributeGet( "$slide"))
        strcpy(sl,"$slide");
    else
        strcpy(sl,"$noslide");
    sprintf(strline, "string: %s :%%curve: %s :%%endlist: %f : %f : %f :%s,%s",this->name(), this->name(), ea,zi,ti,sl,qPrintable ( this->AttributeString()));
    if(g_Janet)
        se=Just_Read_Card(this->Esail , strline,"string",this->name(),&depth,true);
    else
        se=Just_Read_Card(this->Esail , strline,"string",this->name(),&depth);

    if(se) {
        RX_FEString *ses= dynamic_cast<RX_FEString *>(se);
        ses->Resolve(this);
        if(ses->Needs_Resolving)
        {ses->Kill(); return 0;}
    }
    return se;
}
double RXSeamcurve::GetArc( const int & p_ID,const double fractional_tolerance) const
{	
    double l_len = 0;

    //	if(p_ID !=1) assert(p_sc->C[p_ID]->IsValid());
    if (!&(this->m_pC[p_ID]))
        return l_len;

    //  Sept 2005  this fails for tpn and xyzabs models
    //	l_len =p_sc->C[p_ID]->Get_arc(fractional_tolerance);
    //	return l_len;
    // Sept 2005 if the above works, below here would be DEAD CODE

    if (this->m_pC[p_ID] && this->m_pC[p_ID]->GetONCurve())
    {
        const ON_Curve * l_crv = dynamic_cast<ON_Curve*> (this->m_pC[p_ID]->GetONCurve());

        if (!l_crv)
            rxerror("m_ONCurve is not a ON_Curve object, in seamcurves.cpp GetArc() ",2);

        if(! l_crv->GetLength(&l_len,fractional_tolerance) ){
            //for checking what is going on with l_crv->GetLength
            //int l_test = l_crv->GetLength(&l_len,fractional_tolerance);
            // try as a polyline
            const ON_PolylineCurve * l_p = ON_PolylineCurve::Cast(this->m_pC[p_ID]->GetONCurve());
            if( !l_p)
            {	char str[256];
                sprintf(str,"ERROR: cannot cast '%s' to an ON_PolylineCurve; in seamcurves.cpp GetArc", this->name() );
                rxerror(str,1);
            }
            else
                if( ! l_p->GetLength(&l_len,fractional_tolerance) ){
                    char str[256];
                    sprintf(str,"getlength failed in seamcurves.cpp GetArc\n %s", this->name() );
                    rxerror(str,2);
                }
        }
    }
    else
    {
        if (this->c[p_ID]>1)
            l_len = PC_Polyline_Length(this->c[p_ID],this->p[p_ID]);
        else
            l_len = this->m_arcs[p_ID];  // usually only for compound curves
    }
    return l_len;
}//double GetArc

int RXSeamcurve::Delete_Seamcurve(void ){

    /* may be part of a CC*/
    //    RXSeamcurve *this=this;    assert(this);
    int k,j, m,fin=0;

    class RXCompoundCurve * cc;
    if(!this)
        return 0;


    if(!g_Janet) { //cout<< " should use mapextract(CC)"<<endl;
        vector<RXEntity_p >thelist;
        this->Esail->MapExtractList(COMPOUND_CURVE,  thelist) ;
        vector<RXEntity_p >::iterator it;
        for (it = thelist.begin (); it != thelist.end(); it++)  {
            RXEntity_p p  =*it;

            assert(p->TYPE==COMPOUND_CURVE);
            cc = dynamic_cast<class RXCompoundCurve * > ( p) ;
            if(g_Peter_Debug){
                printf(" FOUND CC %s %s dp=%p", this->type(), this->name(),cc);
                if(cc) printf(" (ncurves = %d)\n",cc->m_ccNcurves);
                else cout<< " NULL CC"<<endl;
            }
            if(cc){
                for(j=0;j<cc->m_ccNcurves;j++) {
                    if(cc->m_ccCurves[j]==this) {
                        if(g_Peter_Debug) printf("removing %s from %s\n",this->name(),this->name());
                        (cc->m_ccNcurves)--;
                        this->SetNeedsComputing();
                        for( k=j;k<cc->m_ccNcurves;k++) {
                            cc->m_ccCurves[k] = cc->m_ccCurves[k+1];
                        }
                        fin=1;
                        break;
                    }
                }
            }

            if(fin)break;
        } // compound curve trawl
    } //not janet
    if(g_Peter_Debug) cout<< "CC check ended"<<endl;


    for(k=0;k<3;k++) {
        if(this->p[k] && this->p[k]!=this->m_pC[k]->Get_p()) RXFREE(this->p[k]);
        this->p[k]=0; this->c[k]=0; this->m_arcs[k]=0.;

        delete this->m_pC[k]; this->m_pC[k]=0;
    }
    /* the SCs at the ends are in the IAList.   We have to remove this SC from their IALists
That will work equally for intersecting SCs. 
 For safety, we also check the Curves of any relsites in the IAList.
 But these curves would normally always be in the IAlist themselves.
 We dont need to check EndNsite->dataptr->Curve because EndNSite will always be in the
 IA List.
 */
    if(this->ia){
        for(k=0;k<this->n_angles;k++) {
            if(this->ia[k].s->TYPE==SEAMCURVE) {
                sc_ptr sc = (sc_ptr )  this->ia[k].s;
                sc->Remove_From_IA_List( this,0) ;
            }
            else  if(this->ia[k].s->TYPE==RELSITE) {
                Site*es =  (Site*) this->ia[k].s;
                for(m=0;m<2;m++) {
                    if(es->CUrve[m]) {
                        sc_t  *sc = (sc_ptr  )es->CUrve[m];
                        sc->Remove_From_IA_List( this,0) ;
                    }
                }
            }
        }
        if(!g_Janet && Remove_From_All_IA_Lists(this)) {
            char buf[256];
            sprintf(buf,"SC '%s' needed big remove",this->name());
            rxerror(buf,1);
        }
        // we may be left with some IA Offs that are owned by this->ia.  Let's free them
        for(k=0;k<this->n_angles ; )// dont increment k because the fn decrements n_angles
            this->Delete_One_IA(k);

        RXFREE(this->ia); this->ia=NULL; this->n_angles =0;
    }

    if(this->pslist)
    { RXFREE(this->pslist); this->pslist=NULL; }
    this->npss=0;
    if(this->GeodesicList)
        cout<< "TODO:: Delete_Geodesic_List(this->GeodesicList);   Free_Gcurve_List(np); "<<endl;


    for(k=0;k<3;k++) {
        if(this->Mat_Angle[k].str   ) {
            RXFREE(this->Mat_Angle [k].str); this->Mat_Angle [k].str=NULL;
        }
    }

    RXExpressionI *rxi;
    if(rxi=this->FindExpression (L"ea",rxexpLocal ) ) delete rxi;
    if(rxi=this->FindExpression (L"ti",rxexpLocal ) ) delete rxi;
    if(g_Peter_Debug) cout<< "returning from Delete:SC"<<endl;
    return(1);
}

int RXSeamcurve::Insert_One_IA(RXEntity_p theSite, RXOffset*offset) {

    /* ps is a relsite. this is the seamcurve it lies on
  */
    assert(theSite);

    if(this->In_IA_List( theSite) >=0)  //the object is already in the IA list
        return(0);

    //sc should become an aunt of the offset;
    this->SetRelationOf(offset,niece,RXO_IA_OF_SC,this->n_angles); //added index Oct 2012
    // but remember that we are going to sort these later

    int k=(this->n_angles)++;
    this->ia = (struct IMPOSED_ANGLE*) REALLOC(this->ia ,(k+1)*sizeof(struct IMPOSED_ANGLE));
    (this->ia[k]).Angle = NULL;
    (this->ia[k]).m_Offset = offset;
    (this->ia[k]).sign  =  NULL;
    (this->ia[k]).s  = theSite;

    (this->ia[k]).End= 0;
    this->ia[k].IA_Owner = NULL;

    Sort_IA(this->ia,this->n_angles);
    this->Make_Pside_Request=1;


    return(1);
}
int RXSeamcurve::Update_IAOffsets() {
    //ensure that the IA offsets are consistent with the new curve  geometry
    int i,iret;
    ON_3dPoint p1; ON_3dVector t1;
    RXEntity_p p;

    double dist, s=0.0, tol = this->Esail->m_Linear_Tol;
    char l_buf[256];
    for (i=0;i<this->n_angles;i++) {
        p = this->ia[i].s;
        if((p->TYPE==SITE || p->TYPE==RELSITE) && !p->Needs_Finishing ){
            p->Compute();// Compute_AnySite(dynamic_cast<Site*>(p)); // updates the offsets of rel- & X-sites
            if(p->TYPE==SITE ) {  // else compute_site has done it
                s=(this->ia[i].m_Offset->Evaluate(this,1));
                Site*qq = (Site*) p ;
                iret = this->m_pC[1]->Drop_To_Curve(qq->ToONPoint () ,&p1,&t1, tol,&s,&dist);
                if(iret==-1) {
                    ON_3dPoint l_p =   qq->ToONPoint();// ON_3dPoint(v.x,v.y,v.z);
                    sprintf(l_buf,"(Update_IAOffsets) Dropping %d '%s' onto '%s' failed",i,p->name(),this->name() );
                    //PC_Insert_Arrow(l_p,45,l_buf);
                    rxerror(l_buf,1);
                }
                else if (iret <=0 )
                    rxerror(" (Update_IAOffsets) Drop_To_Curve not converged ",4);
                this->ia[i].m_Offset->Change (TOSTRING(s));
            }
            //this is dangerous in cases where the curve's shape is pathological. EG a patch surround
            // Safer to use the previous offset value to seed S.
        }
    }
    return 1;
}

int RXSeamcurve:: Delete_One_IA(const int i){
    /* copy the IAlist above i down one, decrement N_angles
  The Key for 'pure' IAs - inserted with Insert_One_IA is the offsets address itself*/
    //sc_ptr this =this;
    int  k,k1;
    if(0)
    {
        // if any of the psides point to the offset, nullify that ptr; LEAK THere may be other things pointing to theis offset
        for(k=0;k<this->npss ; k++) {
            PSIDEPTR ps = this->pslist[k];
            if((ps && ps->End1off == this->ia[i].m_Offset)  && PCO_Free_Offset_Record((this->ia[i].m_Offset),&(this->ia[i].m_Offset)))
                ps->End1off=NULL;
            if((ps && ps->End2off == this->ia[i].m_Offset)  && PCO_Free_Offset_Record((this->ia[i].m_Offset),&(this->ia[i].m_Offset)))
                ps->End2off =NULL;
        }

        assert("PCO_Free_Offset_Record((np->ia[i].Offset),&(np->ia[i].Offset)); "==0);
    }
    for(k=i;k<this->n_angles-1;k++) {
        k1=k+1;

        memcpy(&(this->ia[k]),&(this->ia[k1]),sizeof (struct IMPOSED_ANGLE));
    }
    this->Make_Pside_Request = 1;
    this->SetNeedsComputing();
    if(this->FlagQuery(CUT_FLAG))
        this->Needs_Cutting =1;
    (this->n_angles)--;
    return(1);
}
/// Remove_From_IA_List is a source of crashes when you abort a build and then delete the model

int RXSeamcurve::Remove_From_IA_List(RXEntity_p e,const int DoScsToo) {
    sc_ptr sc=this;
    int l,iret=0;
    sc_ptr sc2;
#ifdef _DEBUG
    if(!sc) {cout<< " remove a NULL sc from ia list"<<endl; return 0; }
    //	if(sc->Needs_Resolving)cout<< "WHY remove an E from IAList of a SC which needs resolving??"<<endl; we could skip.
#else
    if(!sc) return 0;
#endif

    for(l=0;l<sc->n_angles;l++) {
        if(sc->ia[l].s== e ) {
            sc->Delete_One_IA(l); l--; iret++;
        }
        else if(DoScsToo && ((sc->ia[l].s->TYPE==SEAMCURVE))) {
            sc2 = (sc_ptr  )sc->ia[l].s;
            assert(sc2);
            if(sc2->End1site ==e || sc2->End2site ==e ) {
               // cout<< " remove relevant IA"<<endl;
                sc->Delete_One_IA(l); iret++;
                l--;
            }
        }
    }
    return iret;
}

int  RXSeamcurve::In_IA_List(const  RXEntity_p e) const{  /* returns offset of e in sc->a. Else -1 */
    //  sc_ptr this =this;
    int k;
    for (k=0;k<this->n_angles ;k++) {
        if( this->ia[k].s == e) return k;
    }
    return -1;
}



int RXSeamcurve::ReWriteLine(void){
    /* seam card is of type
   seam:  name  :  SITE1 : SITE2 : basecurve [length] [[[optional adjectives] leftmat] rightmat] !comment
    march 2013v lets remove leftmat] rightmat]  beause they are held in the atts
*/
    char nn[256];
    // an SC  may have a spawned string.   When we rewrite the line we should
    // retain the attri butes of the string

    class RX_FEString *s = dynamic_cast< class RX_FEString *>(this->GetOneRelativeByIndex(RXO_SC_OF_STRING));
    if(s && ! s->Needs_Resolving) {

        this->AttributeSet (L"$trim", s->GetExpressionText(RXO_STRING_TRIM, 1 ));
        this->AttributeSet (L"$ea",   s->GetExpressionText(RXO_STRING_EA, 1 ));
        this->AttributeSet (L"$ti",   s->GetExpressionText(RXO_STRING_TI, 1 ));
        this->AttributeSet (L"$mass", s->GetExpressionText(RXO_STRING_MASS, 1 ));

        if(g_World->PleaseWriteZIs&1 )  {
            //setting :writeZi:1
            this->AttributeSet (L"$zi",s->GetExpressionText(RXO_STRING_ZI, 1 ) );
            this->AttributeSet (L"$zt",s->GetExpressionText(RXO_STRING_ZT, 1 ) );
            this->AttributeSet (L"$tq",s->GetExpressionText(RXO_STRING_TQ, 1 ) );
        }
        //        if(g_World->PleaseWriteZIs&2 )  { // freeze strings
        //            //setting :writeZi:2
        //            QString v = QString("( %1 * 1.0)").arg(s->d.m_zt_s );
        //            this->AttributeSet ("$zi",v );
        //            s->AttributeSet ("$zi",v );
        //            this->AttributeSet ("$trim","0*0" );
        //            s->AttributeSet ("$trim","0*0" );
        //            v = QString("( %1 + 0.*1.0)").arg(s->d.m_tqOut );
        //            this->AttributeSet ("$ti",v );
        //            s->AttributeSet ("$ti",v );
        //        }
    }

    std::string sline( this->GetLine() );
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();

    RXSTRING NewLine;
    if(this->seam)		NewLine=TOSTRING(L"seam");
    else if(this->edge)	NewLine=TOSTRING(L"edge");
    else				NewLine=TOSTRING(L"curve");

    NewLine+=RXSTRING(_T(RXENTITYSEPSTOWRITE) );
    NewLine+=  TOSTRING(this->name()) ;
    NewLine+=RXSTRING(_T(RXENTITYSEPSTOWRITE) );

    if(!g_Janet) {

        if(this->End1site && this->End1site->generated <= this->generated) {
            NewLine+=  TOSTRING( this->End1site->name());
        }
        else 	if (rxgetword(wds,2,nn ,256))
            NewLine+=  TOSTRING(nn);
    }
    else {
        if(this->End1site && (
                    (this->End1site->generated <= this->generated)
                    ||this->FlagQuery(SNAP_TO_NODES)
                    ||this->FlagQuery(SNAP_TO_ENDS_ONLY) )
                ) {
            NewLine+=  TOSTRING(this->End1site->name());
        }
        else 	if (rxgetword(wds,2,nn  ,256) )
            NewLine+=  TOSTRING(nn);
    }
    NewLine+=RXSTRING(_T(RXENTITYSEPSTOWRITE) );

    if(!g_Janet) {

        if(this->End2site && this->End2site->generated <= this->generated)
            NewLine+=  TOSTRING( this->End2site->name());
        else if ( rxgetword(wds,3,nn  ,256) )
            NewLine+=  TOSTRING(nn);
    }
    else { // janet
        if(this->End2site && (
                    (this->End2site->generated <= this->generated)
                    ||this->FlagQuery(SNAP_TO_NODES)
                    ||this->FlagQuery(SNAP_TO_ENDS_ONLY) )
                ) {
            NewLine+=  TOSTRING( this->End2site->name());
        }
        else if (rxgetword(wds,3,nn ,256))
            NewLine+=  TOSTRING(nn);
    }

    NewLine+=RXSTRING(_T(RXENTITYSEPSTOWRITE) );
    if(this->BorGcurveptr) NewLine+=  TOSTRING(this->BorGcurveptr->name());// for a sub, this is an SC
    NewLine+=RXSTRING(_T(RXENTITYSEPSTOWRITE) );
    RXSTRING w =  this->FindExpression(L"depth",rxexpLocal)-> GetText();
    NewLine+=w;
    NewLine+=RXSTRING(_T(RXENTITYSEPSTOWRITE) );

    if(this->Defined_Material[0]) // NewLine+=  TOSTRING(this->Defined_Material[0]->name());
        this->AttributeSet("$mat2",this->Defined_Material[0]->name() );
    if(this->Defined_Material[2]) // NewLine+=  TOSTRING(this->Defined_Material[0]->name());
        this->AttributeSet("$mat1",this->Defined_Material[2]->name() ) ;

    NewLine+=  this->AttributeString() .toStdWString();

    NewLine+=RXSTRING(_T(RXENTITYSEPSTOWRITE) );
    //   if(this->Defined_Material[0]) NewLine+=  TOSTRING(this->Defined_Material[0]->name());
    NewLine+=RXSTRING(_T(RXENTITYSEPSTOWRITE) );
    //   if(this->Defined_Material[2]) NewLine+=  TOSTRING(this->Defined_Material[2]->name());
    NewLine+=RXSTRING(_T(RXENTITYSEPSTOWRITE) );
    if(this->Defined_Length) {
        RXExpressionI *lengthQ =  this->FindExpression(L"length",rxexpLocal);
        if(lengthQ) {
            NewLine +=w = lengthQ-> GetText();
        }
    }
    this->SetLine(ToUtf8(NewLine).c_str());

    return 1;
}

//static
RXSeamcurve * RXSeamcurve::Find_Seamcurve_By_Ends(RXEntity_p p1,RXEntity_p p2){// used by string resolve

    sc_ptr sc;
    RXObject *e1;
    set<RXObject*>::iterator it1,it2;
    // logic.
    //p1 and p2 are sites.
    // we are looking for a SC which is in their object maps as parent,RXO_N2_OF_SC) or parent,RXO_N1_OF_SC

    set<RXObject*> list1 =p1->FindInMap(RXO_N1_OF_SC, RXO_ANYINDEX,child);
    set<RXObject*> list2 =p2->FindInMap(RXO_N2_OF_SC, RXO_ANYINDEX,child);
    for(it1=list1.begin(); it1!=list1.end();++it1){
        e1 = *it1;
        for(it2=list2.begin(); it2!=list2.end();++it2){
            if(e1== *it2)
                if((sc=dynamic_cast<RXSeamcurve *>(e1)) )
                    return sc;
        }
    }
    list1 =p2->FindInMap(RXO_N1_OF_SC, RXO_ANYINDEX,child);
    list2 =p1->FindInMap(RXO_N2_OF_SC, RXO_ANYINDEX,child);
    for(it1=list1.begin(); it1!=list1.end();++it1){
        e1 = *it1;
        for(it2=list2.begin(); it2!=list2.end();++it2){
            if(e1== *it2)
                if((sc=dynamic_cast<RXSeamcurve *>(e1)) )
                    return sc;
        }
    }

    return 0;
}
HC_KEY RXSeamcurve::Draw_Deflected_SC(FILE *fp)
{// places it in an invisible segment under p->hoopskey;

    HC_KEY  key;
    Pside *ps;
    int i,k,np;
    class RX_FEedge *ed;
    float x0,Y0,z0,x1,Y1,z1;
    ON_3dPoint v;

    SAIL *s = this->Esail;

    if(!this->hoopskey) return  0;
    if(!this->npss) return 0;

    if(fp)      	 fprintf(fp,"3dcurve \t%s",this->name());
    HC_Open_Segment_By_Key(this->hoopskey);
    HC_Open_Segment("hidden");
    HC_Set_Visibility("off");
    key = HC_KOpen_Segment(this->name());
    HC_Flush_Segment("."); HC_Set_Line_Weight(4.0); HC_Set_Color("lines=red");

    HC_Restart_Ink();
    // the first point
    ps = this->pslist[0];
    ed = ps->m_eList.front();
    v= ed->GetNode(0)->DeflectedPos(RX_GLOBAL);
    x0=v[0];	Y0=v[1];	z0=v[2];
    HC_Insert_Ink(x0,Y0,z0);
    if(fp) fprintf(fp,"\t%f \t%f \t%f ",x0,Y0,z0);
    for(i=0;i<this->npss;i++) {
        ps  = this->pslist[i];
        np = (int)ps->m_eList.size()	;
        for(k=0;k<np;k++)
        {
            ed = ps->m_eList[k];
            if (!ed) 	continue;

            v= ed->GetNode(1)->DeflectedPos(RX_GLOBAL);
            x1=v[0];	Y1=v[1];	z1=v[2];
            HC_Insert_Ink(x1,Y1,z1);  // forwards is always OK
            if(fp) fprintf(fp,"\t%f \t%f \t%f ",x1,Y1,z1);
        }
    }
    HC_Close_Segment();
    HC_Close_Segment();
    HC_Close_Segment();
    if(fp) fprintf(fp,"\n");
    return key;
}



