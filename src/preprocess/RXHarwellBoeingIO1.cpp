#include "StdAfx.h"
#include <QDebug>
#include <sstream>
#include <fstream>
#include <string>
#ifndef _DEBUG
#define OMP
#endif
#ifdef OMP
#undef OMP
// #incl ude <o mp.h> 
#endif

#include "hb_io.h"

#ifndef _CONSOLE  // for sparsetest framework
#include "global_declarations.h"
#endif
#include "mkl_pardiso.h"
#include "mkl_types.h"

#include "RXHarwellBoeingIO1.h"
  extern "C" void spkcsrcsc2(const int n, const int n2,const int job,const int ipos
                          , double*a, int*ja,int*ia,double*ao, int*jao, int*iao);
//void spkcsrcsc2(const int n, const int n2,const int job,const int ipos
//                          , double*a, int*ja,int*ia,double*ao, int*jao, int*iao)
//{ }

/*
This is a wrapper for 'hb_io.c/h' the official harwell-boeing reader/writer. 
The idea is that to write a HB file you  create an instance of this class using the appropriate constructor
and then call the Write method.
We've changed to double precision and altered the formatting somewhat. 
Currently impl/home/r3/build/thirdparty

To make it industrial-strength it needs:
1) remove the memory bottleneck in the Write method by translating in blocks
2) Reading, including RHS, exact and guess
3) its aware of what sort of matrix it is   
	//= 1 real and structurally symmetric matrix
	//= 2 real and symmetric positive definite matrix  = usual FE matrix, properly restrained.
	//= -2 real and symmetric indefinite matrix
	//= 11 real and unsymmetric matrix   = for surface parameterization.
	  this affects:
	  A) Whether we skip attempts to set lower-diagonal entries. DONE
	  B) The pardiso solver parameters
	  C) The HB write parameters - and file extension.
	  D) Any checking to see whether a matrix complies with its given type. 
	  Note M_MatType must be defined in the constructor. It is fixed for the lifetime of the matrix.

4)	separate solution steps, to allow the first stage to be carried over where the matrix structure hasnt changed
	note this is difficult to ensure because DOFOs can change the structure.
*/

RXHarwellBoeingIO1::RXHarwellBoeingIO1()
    :colptr(0),
      rowind(0),
      values(0)
 
{
    ptrfmt=0 ;	indfmt=0;	valfmt=0;
	rhsfmt=0;	rhstyp=0;	totcrd=0;	ptrcrd=0;	indcrd=0;
	valcrd=0;	rhscrd1=0;	nrow1=0;	ncol1=0;	nnzero=0;
	neltvl=0;	m_nRhsVs=0;	nrhsix=0;	colptr=0;	rowind=0;
	values=0;	rhsval=0;	rhsptr=0;	rhsind=0;	rhsvec=0;
    guess=0;	exact =0;



}
int RXHarwellBoeingIO1::Init() {
    title.clear();
    key.clear();
	if( ptrfmt) delete []ptrfmt ;		ptrfmt=0 ;
	if( indfmt) delete []indfmt;		indfmt=0;
	if( valfmt) delete []valfmt;		valfmt=0;

	if( rhsfmt) delete []rhsfmt;		rhsfmt=0;
	if( rhstyp) delete []rhstyp;		rhstyp=0;

    if(colptr)
        delete []colptr;
    colptr=0;

	if(rowind) delete []rowind;		rowind=0;
	if(values) delete []values;		values=0;
	if(rhsval) delete []rhsval;		rhsval=0;
	if(rhsptr) delete []rhsptr;		rhsptr=0;
	if(rhsind) delete []rhsind;		rhsind=0;

	if(rhsvec) delete []rhsvec;		rhsvec=0;
	if(guess) delete []guess;			guess=0;
	if(exact) delete [] exact;		exact =0;

	return 1; // lets treat the scalrs as persistent.

	//if( mxtype) delete []mxtype;	mxtype=0;// NO. MXtype is persistent.
	//totcrd=0;	ptrcrd=0;	indcrd=0;
	//valcrd=0;	rhscrd1=0;	nrow1=0;	ncol1=0;	nnzero=0;
	//neltvl=0;	m_nRhsVs=0;	nrhsix=0;
 //
	//return 1;
}
int RXHarwellBoeingIO1::DeepCopy(const RXHarwellBoeingIO1& src)
{
	  this->Init();
	 nnzero =src.nnzero;
	 nrow1 =src.nrow1; 
	 ncol1 =src.ncol1 ;
	m_nRhsVs  =src.m_nRhsVs ;
    if ( src.rhsval)
    {
        rhsval =new double[nrow1*m_nRhsVs];
        memcpy(rhsval,src.rhsval , (nrow1*m_nRhsVs )*sizeof(double));
    }
    else rhsval=0;

//	int *colptr;// one for each column, +1 more, Indexed from 1
	if(src.colptr)
    {colptr =new int[ncol1+1]; memcpy(colptr,src.colptr, (ncol1+1)*sizeof(int));}
//	int *rowind;   // one for each value Indexed from 1

    if(src.rowind) {rowind=new int[nnzero  ];  memcpy(rowind,src.rowind ,(nnzero)*sizeof(int));}
	if(src.values){ values =new double[nnzero]; memcpy(values,src.values, nnzero*sizeof(double));}

    this->title=src.title;
this->key=src.key;

//	char* ptrfmt;
		if(src.ptrfmt){if(ptrfmt) delete[] ptrfmt; ptrfmt=new char[strlen(src.ptrfmt)+1]; strcpy(ptrfmt,src.ptrfmt);}
//	char* indfmt;
		if(src.indfmt){if(indfmt) delete[] indfmt; indfmt=new char[strlen(src.indfmt)+1]; strcpy(indfmt,src.indfmt);}
//	char* valfmt;
		if(src.valfmt){if(valfmt) delete[] valfmt; valfmt=new char[strlen(src.valfmt)+1]; strcpy(valfmt,src.valfmt);}
//	char* rhsfmt;
		if(src.rhsfmt){if(rhsfmt) delete[] rhsfmt; rhsfmt=new char[strlen(src.rhsfmt)+1]; strcpy(rhsfmt,src.rhsfmt);}
//	char* rhstyp;
	if(src.rhstyp){  if(rhstyp) delete[] rhstyp; rhstyp=new char[strlen(src.rhstyp)+1]; strcpy(rhstyp,src.rhstyp);}
	totcrd  =src.totcrd;
	ptrcrd =src.ptrcrd;
	indcrd =src.indcrd;
	valcrd =src.valcrd;
	rhscrd1 =src.rhscrd1;
//	char* mxtype;
    this->mxtype=src.mxtype;
	neltvl =src.neltvl;
	nrhsix =src.nrhsix;

//	int *rhsptr;
//	int *rhsind;
//	double *rhsvec;
//	double *guess;
//	double *exact ;

	assert(!src.rhsptr);
	assert(!src.rhsind);
	assert(!src.rhsvec);
	assert(!src.guess);
	assert(!src.exact) ;
//	strings
	Cintfmt =src.Cintfmt;
	cfltfmt =src.cfltfmt;
	return 1;
}
  RXHarwellBoeingIO1&  RXHarwellBoeingIO1::operator=(const RXHarwellBoeingIO1& src){

	  this->Init();

 
	 nnzero =src.nnzero;
	 nrow1 =src.nrow1; 
	 ncol1 =src.ncol1 ;
	m_nRhsVs  =src.m_nRhsVs ;
    if(nrow1&&m_nRhsVs && src.rhsval){
        rhsval =new double[nrow1*m_nRhsVs]; memcpy(rhsval,src.rhsval , (nrow1*m_nRhsVs )*sizeof(double));
    }
    //	int *colptr; // one for each value Indexed from 1
	if(src.colptr)
    {colptr =new int[ncol1+1]; memcpy(colptr,src.colptr, (ncol1+1) *sizeof(int));}
//	int *rowind;  // one for each column, +1 more, Indexed from 1  

    if(src.rowind) {rowind=new int[nnzero ];  memcpy(rowind,src.rowind ,nnzero*sizeof(int));}
	if(src.values){ values =new double[nnzero]; memcpy(values,src.values, nnzero*sizeof(double));}


    title=src.title;
key=src.key;
//	char* ptrfmt;
		if(src.ptrfmt){if(ptrfmt) delete[] ptrfmt; ptrfmt=new char[strlen(src.ptrfmt)+1]; strcpy(ptrfmt,src.ptrfmt);}
//	char* indfmt;
		if(src.indfmt){if(indfmt) delete []indfmt; indfmt=new char[strlen(src.indfmt)+1]; strcpy(indfmt,src.indfmt);}
//	char* valfmt;
		if(src.valfmt){if(valfmt) delete []valfmt; valfmt=new char[strlen(src.valfmt)+1]; strcpy(valfmt,src.valfmt);}
//	char* rhsfmt;
		if(src.rhsfmt){if(rhsfmt) delete[] rhsfmt; rhsfmt=new char[strlen(src.rhsfmt)+1]; strcpy(rhsfmt,src.rhsfmt);}
//	char* rhstyp;
	if(src.rhstyp){  if(rhstyp) delete[] rhstyp; rhstyp=new char[strlen(src.rhstyp)+1]; strcpy(rhstyp,src.rhstyp);}
	totcrd  =src.totcrd;
	ptrcrd =src.ptrcrd;
	indcrd =src.indcrd;
	valcrd =src.valcrd;
	rhscrd1 =src.rhscrd1;

     mxtype=src.mxtype;
	neltvl =src.neltvl;
	nrhsix =src.nrhsix;

//	int *rhsptr;
//	int *rhsind;
//	double *rhsvec;
//	double *guess;
//	double *exact ;

	assert(!src.rhsptr);
	assert(!src.rhsind);
	assert(!src.rhsvec);
	assert(!src.guess);
	assert(!src.exact) ;


//	strings
	Cintfmt =src.Cintfmt;
	cfltfmt =src.cfltfmt;
	
return *this;
}
RXHarwellBoeingIO1::~RXHarwellBoeingIO1(void)
{
	if( ptrfmt) delete []ptrfmt ;
	if( indfmt) delete[] indfmt;
	if( valfmt) delete[] valfmt;

	if( rhsfmt) delete[] rhsfmt;
	if( rhstyp) delete []rhstyp;
    if(colptr) delete[] colptr;   colptr=0;

    if(rowind) delete [] rowind; rowind=0;
	if(values) delete [] values;
	if(rhsval) delete [] rhsval;  rhsval=0;
	if(rhsptr) delete [] rhsptr;
	if(rhsind) delete [] rhsind;

	if(rhsvec) delete [] rhsvec;
    if(guess) delete [] guess;  guess=0;
    if(exact) delete [] exact;  exact=0;

}
RXHarwellBoeingIO1::RXHarwellBoeingIO1(map<pair<int,int>,double> m,
									 const char*p_title, 
									 const char*p_key)
//									 const MatrixType t)
//:m_MatType(t) 
{
	totcrd=0;	ptrcrd=0;	indcrd=0;
	valcrd=0;	rhscrd1=0;	nrow1=0;	ncol1=0;	nnzero=0;
	neltvl=0;	m_nRhsVs=0;	nrhsix=0;
	rhsval=0;
	Init();
	assert(" this needs testing for tranpose"==0); 	Fill(m,p_title,p_key,  1 ); //,t);
}

    int RXHarwellBoeingIO1::CheckOrdering()
    {
        return 1;
    }

void RXHarwellBoeingIO1::Fill(map<pair<int,int>,double> m,const char*p_title, const char*p_key ,const int base){
	int nii=0;
    int njj=0;
	int nel=0;

	map<pair<int,int>,double> ::const_iterator it,l,h;
	for(it=m.begin(); it!=m.end();++it) {
		nel++;
		nii=max(nii,it->first.first);
		njj=max(njj,it->first.second);
	}
/*	int  */ // nrow1 =nii; 
/*	int  */ // ncol1=njj;
title = p_title;
key = p_key;
if(rhstyp) delete [] rhstyp;		rhstyp=0;
 
/*char*  */   rhstyp= new char[5]; strcpy(rhstyp,"F  "); // full, no guess, no exact

/*	int  */  nnzero=nel;
/*	int */   neltvl =0; // no of element martix entries

/*	int  */  m_nRhsVs=0; // no of RHS vectors, each length nrow
/*	int  */  nrhsix=0;

/*	float * */  rhsval=0;
/*	int * */  rhsptr=0;
/*	int * */  rhsind=0;
/*	float * */  rhsvec=0;
/*	float * */  guess=0;
/*	float * */  exact =0;


//cout << "TO HB format. nnzero= "<<nnzero<<" we should do this in blocks, destroying the old as we go"<<endl;
    if(colptr)  delete []colptr;    colptr=0;
     if(rowind) delete []rowind;    rowind=0;
     if(values) delete []values;    values=0;
     if(nel) {
    colptr =new int[nrow1+1];
 rowind =new int[nel];
values =new double[nel]; // these are the big arrays.

	int r,c,i,n=0 ;
	double v;

	colptr[0] = base; // 0 or 1
	for(i=base;i<nrow1+base;i++) {

		l = m.lower_bound(pair<int,int>(i,base)); // first element of row(i)
		h = m.upper_bound(pair<int,int>(i+1,0)); // first elment of row(i+1)

		for(it=l;it!=h;++it)   // n is the number of elements so far
		{
				n++;	
				r = it->first.first;  assert(r==i);
				c = it->first.second; 
				v = it->second ; 
				rowind[n-1]=c; 
				values[n-1]=v;
		}
		colptr[i] = n+1;
 	}
	this->Transpose(); // because this made the transpose 
     }
}
int RXHarwellBoeingIO1::Transpose()// use the sparskit function
{
		int nr  , nc, job,ipos,nnz;
		double*a, *ao;
		int*ja,*ia , *jao,*iao;
       //  qDebug()<< "RXHarwellBoeingIO1::Transpose could use the Saad in-place routine";
//allocates and returns the arrays of the CSR form
			nr = this->nrow1;  ; nc=this->ncol1;
			job=1; // fill in a
			ipos =1;// base
			a = this->values;
			ia =this->colptr;
			ja=this->rowind ;   
			nnz = this->nnzero;
			ao=new double[ nnz];
			int temp = max(nr+1,nc+1); // I should know which.
			jao = new int[nnz ];  // rowind 
			iao  = new int[temp ]; /// colptr was nc+1 
			memset(iao,0,temp*sizeof(int));

	 		spkcsrcsc2 (nr,nc,job,ipos,a,ja,ia,ao,jao,iao);

			delete[] this->values;this->values=ao;
			delete[] this->colptr;this->colptr =iao;
			delete[] this->rowind ;this->rowind =jao ;
			return 1;
}
int RXHarwellBoeingIO1::SetupForWrite()
{
//start cut
		pair<string,string> ifmts,fltfmts;
		if(valfmt) delete []valfmt;
		if(ptrfmt) delete []ptrfmt;
		if(indfmt ) delete []indfmt;
		if(rhsfmt ) delete []rhsfmt;


		valfmt = new char[12]; strcpy(valfmt,"(3E26.16)"); // 3 per line
		cfltfmt=string("g26.16 g26.16 g26.16");
		ifmts = MakeFormats(nnzero+1  ,72,ptrcrd);
		ptrfmt=new char[1+strlen(ifmts.second.c_str())];strcpy(ptrfmt,ifmts.second.c_str());

		Cintfmt = ifmts.first;
		ifmts = MakeFormats(max(nrow1,ncol1)+2 ,72,indcrd);
		indfmt= new char[1+strlen(ifmts.second.c_str())]; strcpy(indfmt,ifmts.second.c_str());  

		valcrd =(int)ceil(((double)nnzero)/3.0);

		rhsfmt=new char[12]; strcpy(rhsfmt,"(3g20.9)");

		rhscrd1 =(int) ceil ((double)(this->nrow1 *this->m_nRhsVs )/3.0);
		totcrd = indcrd+ptrcrd+valcrd+rhscrd1;
 // end cut
 return 1;
}
int RXHarwellBoeingIO1::Write(const char*filename) {
    ofstream output; assert(this->mxtype.size() );
  string ext(this->mxtype);  
  string f(filename);
  size_t p = f.find(".");
  if(p != string::npos)
	  f=f.substr(0,p+1)+ext;
  else 
	  f=f+string(".")+ext;

    output.open ( f.c_str()  );
	if ( !output )
	  {
 		cout << "  Error opening  file: "<<f<<endl;
		return 0;
	  }
#ifdef DEBUG
 	cout << "Writing matrix to file: "<<f<<endl;
#endif
   try{
	   this-> SetupForWrite();
        hb_file_write ( output, title.c_str(), key.c_str(), totcrd, ptrcrd, indcrd,
        valcrd, rhscrd1, mxtype.c_str(), nrow1, ncol1, nnzero, neltvl, ptrfmt, indfmt,
		valfmt, rhsfmt, rhstyp, m_nRhsVs, nrhsix, colptr, rowind, values, 
		rhsval, rhsptr, rhsind, rhsvec, guess, exact );
   }
   catch(HBIO_X &e){
	   e.Print();  output.close ( ); return 0;
   }
   catch(...){
	cout <<"caught an undefined exception"<<endl;
	output.close ( ); return 0;
   }
  output.close ( ); 
  if(output.fail()){
	cout << "file write failed "<<filename<<endl;
	return 0;
  }
  else{
	return 1;
  }

}
int RXHarwellBoeingIO1::Read(const char*filename)
	{
	char buf[1024];
        filename_copy_local(buf,1024,filename );	ifstream input;    input.open ( buf  );
   if ( !input )
  {
 	cout << "  Error opening  file: "<<filename<<endl;
    return 0;
  }
   try{
	   this->Init ();
          hb_file_read ( input, title, key, &totcrd,
          &ptrcrd, &indcrd,&valcrd, &rhscrd1, mxtype, &nrow1,
		  &ncol1, &nnzero,&neltvl, &ptrfmt, &indfmt, &valfmt, 
		  &rhsfmt, &rhstyp, &(this->m_nRhsVs  ),&nrhsix, &colptr, 
		  &rowind, &values, &rhsval, &rhsptr, &rhsind,  
		  &rhsvec,&guess, &exact );

   }
   catch(...)
   {cout<<"HBIO read error"<<endl;}


	return 1;
}
// first is C format, second is fortran format
pair<string,string> RXHarwellBoeingIO1::MakeFormats(const int n, const int linewidth, int&nlines)
{
	int i,k,nw;
	int c=72;
	pair<string,string>rc ;
	stringstream s,f;
	{
	int j=1;
	for(i=1;;i++) {
		j = 10*j;
		if(j > n) break;
	}
	}
	i=i+1;  assert(pow( 10.,i)>n);  // 10^i is greater than n
	nw = linewidth/(i+1);  // no of words per line 
	s.clear();
	for(k=0;k<nw;k++) {
	s<<"%"<<i<<"d "; c-=(i+1);
	}
	for(k=0;k<c;k++)
		s<<" ";
	s<<"\\n";
	rc.first = s.str();
 
	f <<"("<<nw<<"i"<<i+1<<" )";
	rc.second  =f.str();
	nlines = (int) ceil((double)n/((double)nw));
	return rc;
}


int RXHarwellBoeingIO1::SetRHS(vector<double> p_rhs){  // single RHS only

	double dum; 
	size_t i;

	if(this->rhsval) delete [] rhsval;  rhsval=0;
     if(!p_rhs.size())
		 return 0;
	this->rhsval=new double[p_rhs.size()];

	for(i=0; i<p_rhs.size();i++){
		dum=p_rhs[i];	
		rhsval[i]= dum ;
	}
	if(! p_rhs.size() )
		m_nRhsVs =0;
	else
		m_nRhsVs =(int) p_rhs.size()/this->nrow1; 
return 0;
}

//  this variant is ONLY used for the global stiffness matrix 
int RXHarwellBoeingIO1::SolveOnce(double*b, double* x, MKL_INT pmtype)
{
//The sparse data storage in PARDISO follows the scheme described in Sparse Matrix Storage
//Format section with ja standing for columns, ia for rowIndex, and a for values. The algorithms
//in PARDISO require column indices ja to be increasingly ordered per row and the presence of
//the diagonal element per row for any symmetric or structurally symmetric matrix. For
//unsymmetric matrices the diagonal elements are not necessary: they may be present or not.


		/* Matrix data. */
	MKL_INT n = this->nrow1; //8
	MKL_INT *ia =this->colptr; // the short one
	MKL_INT *ja = this->rowind ;  // the long one
	double *a = this->values;
 
	assert(m_nRhsVs>=1  );  
	int nrhs=m_nRhsVs;

//This scalar value defines the matrix type. The PARDISO solver supports the following matrices: 
//
//= 1 real and structurally symmetric matrix
//= 2 real and symmetric positive definite matrix
//= -2 real and symmetric indefinite matrix
//= 3 complex and structurally symmetric matrix
//= 4 complex and Hermitian positive definite matrix
//= -4 complex and Hermitian indefinite matrix
//= 6 complex and symmetric matrix
//= 11 real and unsymmetric matrix
//= 13 complex and unsymmetric matrix

	/* Internal solver memory pointer pt, */
	/* 32-bit: int pt[64]; 64-bit: long int pt[64] */
	/* or void *pt[64] should be OK on both architectures */
	void *pt[64];
	/* Pardiso control parameters. */
	MKL_INT iparm[64];
//	int  *perm =new int[n];
        MKL_INT maxfct, mnum, phase, error, msglvl=1;
	/* Auxiliary variables. */
	MKL_INT i;
	double ddum; /* Double dummy */
	MKL_INT idum; /* Integer dummy. */
	
        MKL_INT mtype=pmtype; if(mtype==11) { cout<< "try structurally symmetric"<<endl; mtype=1;}

/* -------------------------------------------------------------------- */
/* .. Setup Pardiso control parameters. */
// from the doc	
//	If iparm(1) = 0, iparm(2) and iparm(4) through iparm(64) are filled with default values,
//		otherwise the user has to supply all values in iparm from iparm(2) to iparm(64).
/* -------------------------------------------------------------------- */
	for (i = 0; i < 64; i++) {
		iparm[i] = 0;
	}
        iparm[1-1] = 1; /* 0 => use solver defaults */
        iparm[1] = 2; /* Fill-in reordering from METIS . might try 3 = parallel metis or 0*/
	/* Numbers of processors, value of OMP_NUM_THREADS */
#ifdef OMP
        iparm[2] = omp_get_num_procs() ; //cout<<"np= "<<iparm[2]; BUT THE DOC SAYS ITS NOT USED
#else
#ifdef _DEBUG
		iparm[2] =1;
#else
		iparm[2] = 1; //omp_get_num_procs() ; 
#endif
#endif//

        iparm[3] = 0; /* No iterative-direct algorithm */
	iparm[4] = 0; /* No user fill-in reducing permutation */
	iparm[5] = 0; /* Write solution into x */
	iparm[6] = 0; /* Not in use */
#ifndef _CONSOLE
        iparm[8-1] = g_NPardisoIterations; /* Max numbers of iterative refinement steps */
#endif
	iparm[8] = 0; /* Not in use */
	iparm[9] = 13; /* Perturb the pivot elements with 1E-13 */
	iparm[10] = 1; /* Use nonsymmetric permutation and scaling MPS */
	iparm[11] = 0; /* Not in use */
	iparm[12] = 1; /* Maximum weighted matching algorithm is switched-off (default for symmetric). Try iparm[12] = 1 in case of inappropriate accuracy */
	iparm[13] = 0; /* Output: Number of perturbed pivots */
	iparm[14] = 0; /* Not in use */
	iparm[15] = 0; /* Not in use */
	iparm[16] = 0; /* Not in use */
	iparm[17] = -1; /* Output: Number of nonzeros in the factor LU */
        iparm[18] = -1;//-1; /* Output: Mflops for LU factorization */
        iparm[19]  = 0; /* Output: Numbers of CG Iterations */
        iparm[21-1] = 1; /* use bunch and Kaufman pivotint */
	maxfct = 1; /* Maximum number of numerical factorizations. */
	mnum = 1; /* Which factorization to use. */
        msglvl = 0; /* Print statistical information in file */
	error = 0; /* Initialize error flag */
	
	//peters
         iparm[27-1]=1 ;  // 1 means internal checking before run
/* -------------------------------------------------------------------- */
/* .. Initialize the internal solver memory pointer. This is only */
/* necessary for the FIRST call of the PARDISO solver. */
/* -------------------------------------------------------------------- */

	for (i = 0; i < 64; i++) {
		pt[i] = 0;
	}
	//cout<<endl<<"solving with mtype= "<<mtype;
/* -------------------------------------------------------------------- */
/* .. Reordering and Symbolic Factorization. This step also allocates */
/* all memory that is necessary for the factorization. */
/* -------------------------------------------------------------------- */
	phase = 11;
	PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
		&n, a, ia, ja, &idum, &nrhs,
		iparm, &msglvl, &ddum, &ddum, &error);
	//cout<<endl<< "(Phase 11) No of +ve eigenvalues = "<<iparm[22]<<endl;
	//cout<< "(Phase 11) No of -ve eigenvalues = "<<iparm[23]<<endl;
	if (error != 0) {
		cout<< " during symbolic factorization: "<< error<<endl;
		if(error== -4)  cout<<" zero pivot at row "<< iparm[30]<<endl<<endl;
		phase = -1; /* Release internal memory. */
		PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
			&n, &ddum, ia, ja, &idum, &m_nRhsVs,
			iparm, &msglvl, &ddum, &ddum, &error);
		return RX_NOT_POSITIVE_DEFINITE;
	}
/* -------------------------------------------------------------------- */
/* .. Numerical factorization. */
/* -------------------------------------------------------------------- */
	phase = 22;
	PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
		&n, a, ia, ja, &idum, &nrhs,
		iparm, &msglvl, &ddum, &ddum, &error);
	//cout<< "(Phase 22) No of +ve eigenvalues = "<<iparm[22]<<endl;
	//cout<< "(Phase 22) No of -ve eigenvalues = "<<iparm[23]<<endl;
	if (error != 0) {
		cout<< " during numerical factorization: "<< error;
		if(error== -4)  cout<<" zero pivot at row "<< iparm[29]<<endl;
		phase = -1; /* Release internal memory. */
		PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
			&n, &ddum, ia, ja, &idum, &m_nRhsVs,
			iparm, &msglvl, &ddum, &ddum, &error);
		return (RX_NOT_POSITIVE_DEFINITE);
	}
/* -------------------------------------------------------------------- */
/* .. Back substitution and iterative refinement. */
/* -------------------------------------------------------------------- */
	phase = 33;
	//iparm[7] = g_NPardisoIterations; // we've tried 10 /* Max numbers of iterative refinement steps. */
	/*  right hand side is b. */

	PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
		&n, a, ia, ja, &idum, &nrhs,
		iparm, &msglvl, b, x, &error);
	if (error != 0) {
		cout<< "\nERROR during  solution: "<< error<<endl;
		if(error== -4)  cout<<" zero pivot at row "<< iparm[30]<<endl<<endl;
		phase = -1; /* Release internal memory. */
		PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
			&n, &ddum, ia, ja, &idum, &m_nRhsVs,
			iparm, &msglvl, &ddum, &ddum, &error);
			return RX_NOT_POSITIVE_DEFINITE;
	}

/* -------------------------------------------------------------------- */
/* .. Termination and release of memory. */
/* -------------------------------------------------------------------- */
	phase = -1; /* Release internal memory. */
	PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
		&n, &ddum, ia, ja, &idum, &m_nRhsVs,
		iparm, &msglvl, &ddum, &ddum, &error);
	return 0;
}

