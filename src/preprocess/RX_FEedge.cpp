
#include "StdAfx.h"
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXSRelSite.h"
#include "f90_to_c.h"
#include "RX_FEEdge.h"

RX_FEedge::RX_FEedge(void)
    :m_initialCorrection(1.0),
      m_Linit(0),
      m_Flags(RXFEE_NOSHELL)
{
    cout<< "dont use the default constructor for feedgeQQ m_N -99??"<<endl;
    m_node[0]=NULL; m_node[1]=NULL;
    SetN(RXOBJ_INITIAL_N);
}

/* when we create an instance we should
     1) put it in the right place in the sails list
     2) put it in the right place in the sail's fortran model edgelist
*/
RX_FEedge::RX_FEedge(class RXSail *p)
    : RX_FEObject(p),
      m_initialCorrection(1.0),
      m_Linit(0),
      m_Flags(RXFEE_NOSHELL)
{
    int err,n;
    assert(p);
    m_node[0]=m_node[1]=0;
    Init(p);
    assert(RX_FEObject::GetSail()==p);
    n=p->PopEdgeNo();
    SetN(n);

    p->NoteEdge(this); // means put into m_Fnodes
#ifdef FORTRANLINKED
    cf_insert_edge(p->SailListIndex(),GetN() ,this);//int p_n1,int p_n2,double p_zi);
#else
    assert(0);
#endif
    SetIsInDatabase();
}

RX_FEedge::~RX_FEedge(void)
{
    int n=GetN();
    assert(n != RXOBJ_INITIAL_N) ; // would mean that its a local
    assert(n<= GetSail()->m_Fedges.capacity());
    GetSail()->m_Fedges[n]=0;
    GetSail()->PushEdgeNo(n);

    if( IsInDatabase()) {
        cf_remove_edge(GetSail()->SailListIndex(),GetN());
        SetIsInDatabase(false);
    }
}


void RX_FEedge::Init(SAIL *s) {
    m_sail = s;
    SetIsInDatabase(false);
}


bool RX_FEedge::IsOnModelEdge(){
    return (this->m_Flags == RXFEE_ISEDGE);
}
bool RX_FEedge::IsOnShell(){
    return (this->m_Flags>= RXFEE_ISEDGE);
}
int RX_FEedge::SetLength(const double p_zi)
{	int k=0;
    m_Linit=p_zi;
    if(IsInDatabase() )
#ifdef FORTRANLINKED
        k=cf_set_edgelength(m_sail->SailListIndex(),GetN(),p_zi);
#else
        assert(0);
#endif
        else
            cout<< " Did you mean to SetLength when not In DB?"<<endl;
    return k;
}
double RX_FEedge::GetLength(void) const
{
    if(IsInDatabase() ){
#ifdef FORTRANLINKED
        double z= cf_get_edgelength( m_sail->SailListIndex(),GetN());
        //	assert(fabs(z-m_Linit)<1e-12);
#else
        assert(0);
#endif

    }
    return  m_Linit;
}

RX_FESite * RX_FEedge::GetNode(const int k)const
{
    assert(k==0 ||k==1);
#ifdef _DEBUG
    RX_FESite * v1;
    if(this->IsInDatabase()){
        v1 = cf_get_edgenode( m_sail->SailListIndex(),GetN(),k);
        if(v1) assert(v1==m_node[k]);
    }
#endif
    return m_node[k];
}
void RX_FEedge::SetNodes(RX_FESite *s1,RX_FESite *s2){

    m_node[0]=s1;	m_node[1]=s2;

    assert(this->IsInDatabase());
    cf_set_edgenodes(m_sail->SailListIndex(),this->GetN(),s1->GetN()   ,s2->GetN()  );

}

int RX_FEedge::EdgePrint(FILE *ff)const
{
    fprintf(ff,"\nRX_FEedge:%3d\n",GetN());
    fprintf(ff," Length %f Corr %f InModel=%d n1 = %d  n2 = %d\n",m_Linit,m_initialCorrection,
            IsInDatabase(),
            GetNode(0)->GetN()  ,GetNode(1)->GetN() );

    GetNode(0)->PrintShort(ff);
    GetNode(1)->PrintShort(ff);
    return(0);
}
RXGRAPHICOBJECT RX_FEedge::DrawDeflected(HC_KEY p_k){
    RX_FESite *s1 = GetNode(0);
    RX_FESite *s2 = GetNode(1);
    double v1[3],v2[3];
#ifndef FORTRANLINKED
    assert(0);
#else
    cf_get_coords(m_sail->SailListIndex(), s1->GetN(),v1);
    cf_get_coords(m_sail->SailListIndex(), s2->GetN(),v2);
#endif
    ON_3dPoint p1(v1);
    if(!p1.IsValid ()) {
        p1.x=p1.y=p1.z=-4.;
    }
    ON_3dPoint p2(v2);
    if(!p2.IsValid ()) {
        p2.x=p2.y=p2.z=-5;
    }
    for(int i=0;i<2;i++){
        if(isnan(p1[i]) || fabs(p1[i])> 10000.0 )	p1[i]=-3;
        if(isnan(p2[i]) || fabs(p2[i])> 10000.0 )	p2[i]=-4;
    }
#ifdef HOOPS
    HC_Insert_Line(p1.x,p1.y,p1.z, p2.x,p2.y,p2.z);
    return 0;
#else
    return RX_Insert_Line(p1.x,p1.y,p1.z, p2.x,p2.y,p2.z,  p_k);
#endif
}

RXSTRING RX_FEedge::TellMeAbout(void) const{
    RXSTRING t, rc (L"RX_FEedge   L= " );
    rc += TOSTRING(m_Linit) + L" initialcurvecorr= "+TOSTRING(m_initialCorrection);
    if ( this->m_node[0]) { t=  L"  n0= " +TOSTRING(m_node[0]->GetN());  rc+=t;}
    if ( this->m_node[1]) { t=  L"  n1= "  +TOSTRING(m_node[1]->GetN());  rc+=t;}
    return rc + RX_FEObject::TellMeAbout ();
}
