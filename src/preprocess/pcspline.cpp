/* pcspline.c started Feb 1997
 A set of routines to handle various kinds of spline
 Ideally there would be a very few enquiry functions
 which figure from the spline type what to internal fns to call

    DIARY
 Sept 97 a BUG. the evaluate routines were skipping the penultimate seg.
 This will cause a lump in the model.

  Start Feb 97 	functions for gauss spline
 Ge t_Gauss_Spline(int nks,double *X, double*Kw,Spline *s);
 Sample _Enough_Spline_Points(Spline *s, double**Yout,int *nout);
 PC _Spline_Eval(Spline *s, double X);
    */

#include "StdAfx.h"
#include "RXSeamcurve.h"

#include "polyline.h"
#include "matinvd.h"

#include "gauss.h"
#include "targets.h"

#include "pcspline.h"	

#define PCSDEBUG (0)
double CC(int i, double x,double *X, double *d) {
    /* d are the curvatures at X */
    double cc,g;
    g = (d[i+1] -d[i])/( X[i+1]-X[i]);
    cc = (d[i]  -g *X[i] ) * x + g*x*x/2.0;
    return cc;
}

double DD(int i, double x,double *X, double *d) {
    /* d are the curvatures at X */
    double dd,g;
    g = (d[i+1] - d[i])/( X[i+1]-X[i]);
    dd = (d[i]  -g *X[i] ) * x*x/2.0 + g*x*x*x/6.0;
    return dd;
}
double Simpson_Correction(int nks, double *X, double*Kw) {
    /* determines an average correction factor
to apply because of the error in linear interpolation
Takes the difference between the Simpson integral and the trapezoidal integral
 */
    int k;
    double si=0.0, ti;
    double correction = 1.0;
    if(nks <2) return 1.0;
    ti = - (Kw[0] + Kw[nks-1])/2.0;
    for(k=0;k<nks;k++) {
        ti = ti + Kw[k];
        si = si + Kw[k] * simpson(k,nks) /3.0;
    }
    if(fabs(ti) < 0.000001)
        return 1.0;
    correction = si/ti;
    /*	if(correction < 0.7) {
  cout<< "  Simpson_Correction Debug "<<endl;
  printf("  TI = %f, SI = %f\n", ti,si);
  for(k=0;k<nks;k++) {
   printf(" %d %f  %f\n", k, simpson(k,nks), Kw[k] );
  }
  printf("  TI = %f, SI = %f\n", ti,si);
 }
*/
    return correction;
}

int Get_Gauss_Spline(sc_ptr sc,int nks,double *X, double*Kw,Spline *s){
    /* there are nks knots with second derivatives K  at positions X
This routine generates a spline - starting and ending on Y=0 - which
has second derivatives linearly interpolated between these knots
But see Gauss_Spline_Correction. It determines a uniform correction factor
to apply because of the error in linear interpolation
 */
    struct PC_SPLINESEG  *seg;
    double g,correction;
    int iret=1;
    int i,j,k,l,r,c,N;
    int n = nks-1;     /* no of intervals */
    int isol, idsol, nr, nc; int mca=11;double det;
   // double*A=NULL;
    double*coeffs=NULL;
    double*rhs=NULL;
    double *a=NULL; /* a[10][11]; */
    correction = 1.0;
    correction = Simpson_Correction(nks, X, Kw);
    N=mca =(nks-1)*2 ;
    coeffs = (double*)CALLOC (N+2,sizeof(double));
    rhs = (double*)CALLOC (N+2,sizeof(double));
    a = (double*)CALLOC((mca*mca+2+2*mca),sizeof(double));
    nr=N; nc = nr;

    /* set up the RHS */
    j=1;
    for(k=1;k<n;k++) {
        rhs[j++]	 = DD(k,X[k],X,Kw) - DD(k-1,X[k],X,Kw);
        rhs[j++]	 = CC(k,X[k],X,Kw) - CC(k-1,X[k],X,Kw);
    }

    rhs[0] = - DD(0,X[0],X,Kw);
    rhs[N-1] = - DD(n-1,X[n],X,Kw);
    if(PCSDEBUG){
        for(k=0;k<N;k++)
            printf(" rhs \t %d \t %15.6g \n", k,rhs[k]);
        for(k=0;k<nks;k++)
            printf("X(%d)=\t %15.6g \t K= %f\n", k,X[k],Kw[k]);
    }

    /* set up the matrix 	  */
    r=1	;
    for(k=1;k<n;k++) {
        c = r-1; 	a[r+mca*c] = X[k];
        c++;		a[r+mca*c] = 1.0;
        c++;		a[r+mca*c] = -X[k];
        c++;		a[r+mca*c] = -1.0 ;
        r++;
        c = r-2;	a[r+mca*c] =1.0;
        c = r;		a[r+mca*c] =-1.0;
        r++;
    }
    a[0]=X[0]; a[mca]=1.0;
    a[N*N-1-mca]=X[n];  a[N*N-1]=1.0;

    if(PCSDEBUG) {
        cout<< " A "<<endl;
        for(i=0;i<N;i++) {
            for(j=0;j<N;j++){  k=i+N*j; printf("%f \t",a[k]);  }  cout<< "\n"<<endl;}
    }
    matinvd(&isol,&idsol, nr, nc,a,mca,&det)	;	if(PCSDEBUG) printf(" det= \t %f\n",det);
    if(isol ==1)   {
        vmult(coeffs,a,rhs,N);
        /*		 for(k=0;k<N;k++) if(PCSDEBUG) printf("coeff %d\t %15.6g\n",k,coeffs[k]); */
    }
    else {
        for(k=0;k<N;k++) if(PCSDEBUG) printf("rhs %d %15.6g\\n",k,rhs[k]);
        for(i=0;i<N;i++) {
            coeffs[i]=0.0;
            for(j=0;j<N;j++){
                k=j+mca*i; if(PCSDEBUG) printf(" %f  ",a[k]);
            }
            if(PCSDEBUG) cout<< ">\n"<<endl;
        }
        sc->OutputToClient(" Inversion failed in PCSPLINE",1);
    }

    /* Now lets create the splinesegs   */
    if(s->segs)
        RXFREE(s->segs); // July 2000 a leak
    seg= s->segs=(PC_SPLINESEG *)CALLOC(n, sizeof(Spline_Seg));
    s->type= ONE_DIM;
    s->status=1;
    s->ns=n;
    s->chord = X[nks-1] - X[0];
    l=0;
    for(	k=0;k<n;k++,seg++) {
        g =  ( Kw[k+1] - Kw[k])/(X[k+1]-X[k]);
        seg->status=1;
        seg->order=3;
        seg->x1=X[k];
        seg->x2=X[k+1];
        seg->c[1]=coeffs[l++]*correction;
        seg->c[0]=coeffs[l++]*correction;
        seg->c[2] = (Kw[k]-g*X[k])/2.0*correction;
        seg->c[3] = g/6.0*correction;
        if(PCSDEBUG){ printf(" Coeffs %d\n", k);
            for(r=0;r<4;r++) printf( " %15.9g\t",seg->c[r]);
            cout<< " \n"<<endl;}
    }
    RXFREE (coeffs); RXFREE(a);	RXFREE(rhs);
    return iret;
}
int Print_Spline(Spline *s)  {
    int r,k;
    struct PC_SPLINESEG  *seg;
    printf(" SPLINE\nType %d\n", s->type);
    printf(" 	status %d\n", s->status);;
    printf(" 	ns=%d\n", s->ns);
    printf(" 	chord %f\n",s->chord);
    seg= s->segs;
    cout<< "seg\tstat\tord\t  x1     \t x2     \t  coeffs\n"<<endl;
    for(k=0;k<s->ns;k++,seg++) {
        printf("  %d\t",k);
        printf("  %d\t",seg->status);
        printf("  %d\t",seg->order);
        printf("   %f\t",seg->x1);
        printf("   %f\t",seg->x2) ;
        for(r=0;r<=seg->order  ;r++) printf( " %12.6g ",seg->c[r]);
        cout<< " \n"<<endl;
    }
    return 1;
}
int Sample_Enough_Spline_Points(Spline *s, VECTOR**p,int *nout,double l1, double l2, double l3){
    int iret=1;
    /* This should use a sagitta method to sample points as required - to a minimum of *nout
  The curvature is c2 + c3(x) so sagitta 	 is (ds^2/8*c)
 The angle will be about c times ds

 As a criterion lets take :
  sagitta < l1 * chord
  sagitta < l2 (absolute Y)
  angle <  l3
  also, count no greater than *nout
   Algorithm
   1) sample at start, end and middle
   2) For each segment
    add a central point  if needed

     */
    double ds,x,C,sag;
    VECTOR v;
    int k,c, Still_Going=1, count=1000;
    *p = (VECTOR*)REALLOC(*p,4*sizeof(VECTOR));
    (*p)[0].x = s->segs[0].x1;
    (*p)[1].x   = 0.5*(  s->segs[0].x1 + 	s->segs[s->ns-1].x2);
    (*p)[2].x = s->segs[s->ns-1].x2;
    c=3;
    for(k=0;k<3;k++) { (*p)[k].y = 	PC_Spline_Eval(s,(*p)[k].x);  (*p)[k].z=0.0;}

    do {
        Still_Going=0;

        for( k=0;k<c-1;k++) {
            x  =  ((*p)[k].x +  (*p)[k+1].x)/2.0;
            ds =   (*p)[k+1].x-(*p)[k].x ;
            C = 	fabs(PC_Spline_Curvature(s,x));
            sag =  ds*ds/8.0*C;
            if((sag > l1*s->chord || sag > l2 || C*ds >l3) && (ds > 0.00625*s->chord) && (c < *nout) ) {
                v.x=x;	 v.z=0.0;
                v.y = PC_Spline_Eval(s, x);
                Insert_Into_Poly(p,&c, v, k);
                Still_Going=1;
            }

        }

    } while( Still_Going && count--);

    *nout=c;
    return iret;
}

double  PC_Spline_Eval(Spline *s, double x){
    double y;
    /*
  return the value of s at x
  */
    int i, k;
    struct PC_SPLINESEG *TheSeg;

    if(s->type !=ONE_DIM) {  return 0.0;}
    i = 	s->ns-1;								  /* default to last segment */

    for(k=1;k<s->ns;k++) {
        if(x < s->segs[k].x1) { i = k-1; break;}
    }
    TheSeg =&(s->segs[i]);
    y= TheSeg->c[TheSeg->order];
    for(k=TheSeg->order;k>0;k--) {
        y 	=x *  y;
        y = y +  TheSeg->c[k-1]	;
    }
    return y;
}

double  PC_Spline_Gradient(Spline *s, double x){
    double y;
    /*   return the value of gradient x     */
    int i, k;
    double O;
    struct PC_SPLINESEG *TheSeg;

    assert(s->type ==ONE_DIM);
    i = 	s->ns-1;								  /* default to last segment */

    for(k=1;k<s->ns;k++) {
        if(x < s->segs[k].x1) { i = k-1; break;}
    }
    TheSeg =&(s->segs[i]);
    O = (double) TheSeg->order;
    y= TheSeg->c[TheSeg->order] * O;
    for(k=TheSeg->order-1;k>0;k--) {
        y 	=x *  y;
        y = y +  TheSeg->c[k]* (double) (k);
    }
    return y;
}
double  PC_Spline_Curvature(Spline *s, double x){
    double y;
    /*
  return the value of curvature at x
  */
    int i, k;
    double O;
    struct PC_SPLINESEG *TheSeg;

    assert(s->type ==ONE_DIM) ;
    i = 	s->ns-1;								  /* default to last segment */

    for(k=1;k<s->ns;k++) {
        if(x < s->segs[k].x1) { i = k-1; break;}
    }
    TheSeg =&(s->segs[i]);
    O = (double) TheSeg->order;
    y= TheSeg->c[TheSeg->order] * O *(O-1);
    for(k=TheSeg->order;k>2;k--) {
        y 	=x *  y;
        y = y +  TheSeg->c[k-1]* (double) (k-1)	* (double) (k-2)	;
    }
    return y;
}

