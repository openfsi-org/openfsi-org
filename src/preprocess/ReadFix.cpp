/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"
#include "RXSail.h"
#include "RXEntityDefault.h"
#include "stringutils.h"
#include "resolve.h"
#include "entities.h"

#include "resolve.h"


/********************************* FIXINGS ******************************/

int Resolve_Fix_Card(RXEntity_p e) // YUCH!!!!!!!!!!!!!
{    
    /* FIX card is of type
             0    1     2      3     4
    FIX:site1:fix= ! comment */

    // splice these atts into those of the node it points to
    // giving precedence to the atts already on the node.

    char *sn[20],*lp;
    RXEntity_p p1=NULL;
    int i,retVal=0; // failure
    char line[2048];

    strncpy(line,e->GetLine(),2040);

    lp = strchr(line,'!');
    if(lp) *lp = '\0';	// strip comments

    lp=strtok(line,RXENTITYSEPS);
    i=0;
    while((lp=strtok(NULL,RXENTITYSEPS))!=NULL && i<20 ) {
        sn[i]=lp;
        PC_Strip_Leading(sn[i]);
        PC_Strip_Trailing(sn[i]);
        i++;
    }
    if(i < 2)
        goto End;

    p1= e->Esail->Get_Key_With_Reporting("site,relsite",sn[0]); /* site 1 */
    if(!p1)
        goto End;

    p1->m_rxa.Splice(sn[1] ,1);

    e->PC_Finish_Entity("fix",sn[0],NULL,(long) 0,NULL,sn[1],e->GetLine());

    retVal = 1;

End:
    if(retVal)  {
        e->Needs_Resolving= 0;
        e->Needs_Finishing = 0;
        e->SetNeedsComputing(0);
    }
    return(retVal);
}

