#ifndef LINKLIST__H__
#define LINKLIST__H__

struct LINKLIST {
	void *data; 
	struct LINKLIST *next;
};  //struct LINKLIST

typedef struct LINKLIST linklist_t;
#endif

