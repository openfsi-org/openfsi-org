#pragma once
#include <string>
#include <map>
#include <vector>
using namespace std;
// CARE!!  the harwell-boeing file format is CSC
// Internally we use CSR. 
// this is why we do a Transpose on read, write and fill. 


#define  RX_NOT_POSITIVE_DEFINITE  33  // must be non-zero and unique in return values of SolveOnce 

class RXHarwellBoeingIO1
	
{
friend class RXSparseMatrix1;
public:
	RXHarwellBoeingIO1( );
	RXHarwellBoeingIO1(map<pair<int,int>,double> ,const char*p_title, const char*p_key);
	~RXHarwellBoeingIO1(void);
	int Write(const char*filename) ;
	int Read(const char*filename);
	int Init();
	void Fill(map<pair<int,int>,double> m,const char*p_title, const char*p_key ,const int base);
	int  SetRHS(vector<double> p_rhs);
    RXHarwellBoeingIO1&  operator=(const RXHarwellBoeingIO1& src);
	int Transpose();
    int CheckOrdering();
protected:
	int SetupForWrite();
	int DeepCopy(const RXHarwellBoeingIO1& src);
// dream on
    int IsSymmetric(){return 0;}
    int IsSymmetricStructure(){return 0;}
    int IsPositiveDefinite(){return 0;}
    int IsSquare(){return (this->nrow1==this->ncol1);}

public:
	double *rhsval;
	int nrow1; 
	int ncol1;
    int *colptr;  // one for each column, +1 more, Indexed from 1. IA in sparsekit notation.
    int *rowind;  // one for each value  Indexed from 1. JA in sparsekit notation
    double *values;
	int nnzero;
	int m_nRhsVs;
protected:

std::string title,key;
	char* ptrfmt;
	char* indfmt;
	char* valfmt;
	char* rhsfmt;
	char* rhstyp;
	int totcrd;
	int ptrcrd;
	int indcrd;
	int valcrd;
	int rhscrd1;
    std::string  mxtype;

	int neltvl;
	int nrhsix;

	int *rhsptr;
	int *rhsind;
	double *rhsvec;
	double *guess;
	double *exact ;

	string Cintfmt,cfltfmt;

public:
	static pair<string,string> MakeFormats(const int n,const int lw, int&nlines);
	// copy of intel pardiso_sym_c
	// default type is real and unsymmetric SLOW
	int SolveOnce(double* rhs, double* result,MKL_INT mtype = 11);
};
