#pragma once
#include "opennurbs.h"
#include "MTParser.h"
#ifdef linux
#define IDIGNORE  10101
#define IDOK	10102
#define IDABORT	10103
#define IDNO	10104
#endif

// flags for accessflags
#define RXE_PRIVATE 1
#define RXE_INPUT	2
#define RXE_OUTPUT	4
#define RXE_READONLY	8

#include "RXObject.h"
#include "RXENode.h"

class RXMTInternalVariable : public MTVariableI
{
public:
    RXMTInternalVariable();
    virtual ~RXMTInternalVariable();
    virtual const MTCHAR* getSymbol();
    virtual MTDOUBLE evaluate(unsigned int nbArgs,const MTDOUBLE *pArg);
    virtual MTVariableI* spawn()    throw(MTParserException);
    void SetExpression( 	class RXExpressionI* e)   {  m_exp =e ; }
    class RXExpressionI*  GetExpressionPtr(void)const { return  m_exp;}

     void SetMTIVName(const RXSTRING &s) {m_MTIVname =s ; }
      RXSTRING GetMTIVName(void) const {return m_MTIVname; }
 protected:
       class RXExpressionI* m_exp;
private:
    RXSTRING m_MTIVname;
};


class MyVarFactory : public MTVariableFactoryI        
{
public:
    virtual MTVariableI* create(const MTCHAR *symbol);
    virtual MTVariableFactoryI* spawn();
    MyVarFactory(class RXExpression *p);
    MyVarFactory(void);
    static int m_VarIndex;
    class RXExpression * m_rxexp;  // the expression owning THIS
    //we can get the expList from m_rxexp->GetOwner()
};

class MyStringVarFactory : public MTVariableFactoryI        
{
public:
    virtual MTVariableI* create(const MTCHAR *symbol);
    virtual MTVariableFactoryI* spawn();
    MyStringVarFactory(class RXExpression *p);
    MyStringVarFactory(void);

    class RXExpression * m_rxexp;  // the expression owning THIS
    //we can get the expList from m_rxexp->GetOwner()
};


/** @brief Base class for various RXExpression s

  This class manages the relations between objects.
  */



class  RXExpressionI:
        virtual public RXObject
{

public:
    RXExpressionI(void)
        : m_OwnerNode(0),  // the list which owns this expression
          m_accessflags(RXE_INPUT|RXE_OUTPUT),
          m_model(0)
 // for values see above. a conbination of Private, input, output)
    {

    }
    virtual ~RXExpressionI(void){}
    RXExpressionI(const RXSTRING &name,const RXSTRING &p_buf,  class RXSail *const p_sail)
        : m_OwnerNode(0),  // the list which owns this expression
          m_accessflags(RXE_INPUT|RXE_OUTPUT ), // for values see above. a conbination of Private, input, output)
          m_model(p_sail)
    {
	SetOName(name);
    }
    virtual MTDOUBLE evaluate() =0;
    virtual MTDOUBLE evaluate(const MTCHAR *expr) throw(MTParserException)=0;
    virtual bool ResolveExp()=0;
    // if it did change, resolve must return 1  and set NeedsComputing
    virtual int Change(const RXSTRING &s) =0;

    virtual MTSTRING GetExpression(void)const =0;  // returns the parser's internal text - only called for debugging.
    virtual RXSTRING GetText() const=0; // returns the file-able representation.

    virtual int Resolve()=0;
    virtual int Print(FILE *fp)const =0;
    virtual const char* What() const =0;

    class RXENode *SetOwner(class RXENode *n){class RXENode *rc=m_OwnerNode;m_OwnerNode=n;return rc; }
    class RXENode *GetOwner() const {return  m_OwnerNode; }
    int AccessFlags( void) const{  return this->m_accessflags;}
    void SetAccessFlags(const int i){ this->m_accessflags=i;}
    SAIL *Model()const{return m_model;}

protected:
    void SetModel( SAIL*m ){ m_model=m;}
    int m_accessflags;
private:
    class RXENode *m_OwnerNode;  // the list which owns this expression

    SAIL * m_model;

} ; //RXExpressionI

/** @brief RXExpressionDble is the cheapest expression: just a double. 

It is nevertheless handled fully by Enode.
  */

class RXExpressionDble  : public RXExpressionI
{

public:
    RXExpressionDble(void);
    virtual ~RXExpressionDble(void);
    RXExpressionDble(const RXSTRING &name,const RXSTRING &p_buf,  class RXSail *const p_sail);

    virtual MTDOUBLE evaluate();
    virtual MTDOUBLE evaluate(const MTCHAR *expr) throw(MTParserException);
    virtual bool ResolveExp();
    // returns 1 if it did change and sets NeedsComputing on the owning entity
    virtual int Change(const RXSTRING &s);
    MTSTRING GetExpression(void)const;
    RXSTRING GetText() const;

    virtual int Resolve();
    virtual int Print(FILE *fp) const;
    virtual const char* What()const {return "C";}
    virtual QString Descriptor()const;
    virtual int Kill();
    virtual int Compute();
    virtual int Finish();

private:
    MTDOUBLE x;
} ; //RXExpressionDble


/** @brief RXExpression is a general algebraic expression which evaluates to a double 
It should handle data dependencies correctly due to its inheritance from RXObject.
A RXEnode holds a list of RXExpressionI's so it shouldnt be necessary to have any RXExpression* in the entity classes.
It is quite expensive because it inherits from MTParser. 

sail models can contain global expressions. These get placed in the worlds list not the models.
then when we serialize the sailmodel we have to retrieve it.
 and when we delete the model we have to remove these expressions from the World.
  **/

class RXExpression :
	public MTParser , public RXExpressionI
{
    friend class RXENode;
public:
    RXExpression(void);
    ~RXExpression(void);
    RXExpression(const RXSTRING &name,const RXSTRING &p_buf, class RXSail *const p_sail);

    int CClear();

    virtual MTDOUBLE evaluate() ;
    MTDOUBLE evaluate(const MTCHAR *expr) throw(MTParserException){return MTParser::evaluate(expr);}
    int Print(FILE *fp) const;

    // CHange returns 1 if it did change and sets NeedsComputing on the owning entity
    int Change(const RXSTRING &s);
    int Fill(const RXSTRING &s);


    RXSTRING GetText() const;
    MTSTRING GetExpression(void)const{return getExpression();}

    bool ResolveExp();
    int UnResolveExp(); // the opposite of resolve. strips it back to its original m_text


    bool m_debug;

    virtual int Kill();
    virtual int Compute();
    virtual int Resolve();
    virtual int Finish();
    virtual int UnResolve();

    virtual const char* What() const {return "E";}
    virtual QString Descriptor()const;
	void SetNeedsCompiling() { m_needscompiling=true;}
protected:
    ON_wString nextword(ON_wString &line, const char delim);
    RXSTRING m_Text; // use m_Name from RXO,m_name;
    RXSTRING m_InputText;

    int m_MessageboxRetVal;
    bool m_needscompiling;
private:
    int Grandfather();

};
