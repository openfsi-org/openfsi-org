/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 Jan 2005  after each run and aftr each cycle are now in global_decs and they are from the first colon not HC__Parsed
 
  Jan 98  In the first word , underscores are treated as spaces.

 June 97 Global_Tuning_Factor. g_Gauss_Recursion
 used for debugging the size of the bounding triangle
Global Rotate Stress removed. Its never used
Also, the 'Global_' prefix no longer required in the init file
 * g_Gcurve_Count
 * several small changes (eg incl ude akmutil.h) to enable compilation on MSVC
 *   comments allowed
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */
#include "StdAfx.h"
#include <QDebug>
#include <QTextStream>
#include "global_declarations.h"
#include "RelaxWorld.h"
#ifndef WIN32
    #define _DIRSEP_	"/"
    #define TEMPLATEDIR "/code/templates"
    #define TRACEDIR "/trace"
    #define  BOATDIR "/boats"

#else
    #ifndef _DIRSEP_
    #define _DIRSEP_	"\\"
    #endif  //#ifndef _DIRSEP_
    #define TEMPLATEDIR "\\template"
    #define TRACEDIR  "\\trace"
    #define BOATDIR	 "\\boats"
#endif

#include "stringutils.h"
#include "words.h"

#include "resolve.h"
#include "offset.h"

#include "akmutil.h"
#include "akmpretty.h"
#include "griddefs.h"
#ifdef USE_PANSAIL
#include "pansail.h"
#endif
#include "panel.h"
#include "units.h"
#include "custom.h"

#ifdef WIN32
#include <direct.h>
#define getcwd _getcwd
#else
#include  <unistd.h> /* for getcwd */
#endif


#include "getglobal.h"

void getcurrentdir(char *s, int *l){ // for use from fortran
    strncpy(s,g_currentDir,*l);
}

FILE *Open_Defaults_File(char* p_defaultsFile ){
    FILE *fp;
#ifdef WIN32
    sprintf(p_defaultsFile,"%s%sRelaxII.defaults",g_workingDir,_DIRSEP_);
#else
    sprintf(p_defaultsFile,"%s/RelaxII.defaults",g_workingDir);
#endif
    fp = RXFOPEN(p_defaultsFile,"r");
    if(!fp) {
        char buf[512];
        sprintf(buf, "File 'RelaxII.defaults' not found in working directory\n'%s'\nUsing one in code directory\n'%s'\n", g_workingDir, g_CodeDir);
        g_World->OutputToClient(buf,1);

        sprintf(p_defaultsFile,"%s%sRelaxII.defaults",g_CodeDir,_DIRSEP_);
        fp = RXFOPEN(p_defaultsFile,"r");
        if(!fp) {
            sprintf(p_defaultsFile,"%s%sRelaxII.defaults",".",_DIRSEP_);
            fp = RXFOPEN(p_defaultsFile,"r");
        }

        if(!fp) {
            sprintf(buf,"file '%s' not found\n not found in CODE directory\n%s\nor local directory\n%s\nPlease check installation\n",p_defaultsFile, g_CodeDir, g_workingDir);
            g_World->OutputToClient(buf,2);
        }
    }// if fp
    if(fp)
        cout<<"\nopened file "<<p_defaultsFile <<"\n"<<endl;
    return(fp);
}
FILE *Open_INI_File(char* p_File ){
    FILE *fp;
    char buf[512];


    sprintf(p_File,"%s%sRelaxII.ini",g_CodeDir,_DIRSEP_);
    fp = RXFOPEN(p_File,"r");
    //    if(!fp) {
    //        char*tdir = getenv("RELAXII_APPLICATION_PATH");
    //        printf (" INI file '%s' not found, trying again..\n",p_File);
    //        sprintf(p_File,"%s%sRelaxII.ini",tdir,_DIRSEP_);
    //        printf (" INI file '%s' \n",p_File);
    //        fp = RXFOPEN(p_File,"r");
    //    }

    if(!fp) {
        sprintf(buf,"file '%s' not found\nin CODE directory\n%s\nPlease check installation\n",p_File, g_CodeDir);
        g_World->OutputToClient(buf,2);
    }

    return(fp);
}

int  Make_Directories(void) {/* set up default directory structures from workingDir */ 
    char buf[256];
    if(!rxIsEmpty(g_workingDir)) {
#ifdef linux
        strncpy(buf,g_workingDir,255);
        int kk = strlen(buf);    // this failed on the ASUS (!!!)
        kk=kk-1;
        if (kk>0 && g_workingDir[kk]=='/')
            g_workingDir[kk]=0;
#endif
        if(!is_directory(g_workingDir)) {
            sprintf(buf,"Creating directory %s",g_workingDir);
            g_World->OutputToClient(buf,1);
            sprintf(buf,"mkdir ""%s""",g_workingDir);
            if(system(buf))cout<<"RX cannot execute (0) "<<buf<<endl;
            if(!is_directory(g_workingDir)) {
                sprintf(buf,"FAILED to CREATE directory '%s'",g_workingDir);
                g_World->OutputToClient(buf,1);
            }
            else {
                sprintf(buf,"CREATED directory '%s'",g_workingDir);
                g_World->OutputToClient(buf,1);
            }
        }
#ifdef linux
        sprintf(buf,"cd \"%s\"",g_workingDir);
        system(buf );
        if(0 != setenv("RWD" ,g_workingDir,1))
            g_World->OutputToClient("setenv of RWD failed",2);
#else // windows
        sprintf(buf,"%s",g_workingDir); cout<<" go to chdir to '"<<g_workingDir <<"'"<<endl;
        int kkk = _chdir(g_workingDir);
        // p rintf("_chdir '%s' returned %d\n", g_workingDir,kkk);
        char*pbuf = new char[strlen(g_workingDir )+32];
        sprintf(pbuf,"RWD=%s",g_workingDir);
        if(0 != _putenv(pbuf))
            g_World->OutputToClient("setenv of RWD failed",2);
#endif

        strcpy(g_boatDir,g_workingDir);
        strcat(g_boatDir,BOATDIR);

        if(!is_directory(g_boatDir))
            strcpy(g_boatDir,g_workingDir);


        strcpy(traceDir,g_workingDir);  /* where to put rubbish */
        strcat(traceDir,TRACEDIR);

        if(!is_directory(traceDir)) {
            sprintf(buf,"Creating trace directory in %s",g_workingDir);
            g_World->OutputToClient(buf,1);
            sprintf(buf,"mkdir \"%s\"",traceDir);
            if(system(buf)) cout<<"RX cannot execute (1) "<<buf<<endl;
            if(!is_directory(traceDir)) {
                sprintf(buf,"FAILED to CREATE directory '%s' in %s",traceDir,g_workingDir);
                g_World->OutputToClient(buf,1);
                strcpy(traceDir,g_workingDir);  /* where to put rubbish */
            }
        }
    }

    sprintf( templateDir,"%s%s", g_RelaxRoot,TEMPLATEDIR);
    if(!is_directory(templateDir))
        strcpy(templateDir,g_workingDir);

    return 1;
}
#ifdef HOOPS
int PC_Define_Color_Name(const char*old,const char*erin,const char*ishin,const char*p_new){
    char er[256],ish[256], c[256];
    int found;

    HC_Begin_Color_Name_Search(); found=0;
    while (HC_Find_Color_Name(c,er,ish)) {
        if(strieq(c,old)) {

            HC_UnDefine_Color_Name(old);
            found=1; break;
        }
    }
    HC_End_Color_Name_Search();
    if(!found)  {ish[0]= er[0]=0;}
    HC_Open_Segment("/");
    if(erin&&(*erin)) strcpy(	er,erin);
    if(ishin&&(*ishin)) strcpy(	ish,ishin);
    HC_Define_Color_Name(old,er,ish,p_new);

    HC_Close_Segment();

    return 1;
}
#endif

int Append_Defaults_File(char *string) {
    FILE *fp;
    fp = Open_Defaults_File(g_defaultsFile);
    if(!fp) return 0;
    FCLOSE(fp);
    fp = RXFOPEN(g_defaultsFile,"a");
    if(!fp) return 0;

    fprintf(fp,"%s\n",string);
    FCLOSE(fp);

    return 1;
}
int Edit_Defaults_File() {
    FILE *fp;
    char buf[256];
    fp = Open_Defaults_File(g_defaultsFile);
    if(!fp) return 0;
    FCLOSE(fp);
    sprintf(buf,"$RX_EDIT %s  & \n", g_defaultsFile);
    system(buf);
    return 1;
}
void InitGlobals()
{
    g_Spline_Division = 33;
    strcpy(g_Units[OTHER_OP].kw,"no units");
    strcpy(g_Units[STRESS_OP].kw,"stress");
    strcpy(g_Units[STRAIN_OP].kw,"strain");
    strcpy(g_Units[SANGLE_OP].kw,"Solid angle");
    strcpy(g_Units[DEGREE_OP].kw,"angle");
    strcpy(g_Units[PRESS_OP].kw,"pressure");
    strcpy(g_Units[CP_OP].kw,"cp");
    strcpy(g_Units[EA_OP].kw,"stiffness");

    strcpy(g_Units[OTHER_OP].units,"");
    strcpy(g_Units[STRESS_OP].units,"N/m");
    strcpy(g_Units[STRAIN_OP].units,"strain");
    strcpy(g_Units[SANGLE_OP].units,"sterad");
    strcpy(g_Units[DEGREE_OP].units,"deg");
    strcpy(g_Units[PRESS_OP].units,"Pa");
    strcpy(g_Units[CP_OP].units,"cp");
    strcpy(g_Units[EA_OP].units,"N/m");

    g_Units[OTHER_OP].f=1.0;
    g_Units[STRESS_OP].f=1.0;
    g_Units[STRAIN_OP].f=1.0;
    g_Units[SANGLE_OP].f=1.0;
    g_Units[DEGREE_OP].f=1.0;
    g_Units[PRESS_OP].f=1.0;
    g_Units[CP_OP].f=1.0;
    g_Units[EA_OP].f=1.0;

    g_Units[OTHER_OP].flag=0;
    g_Units[STRESS_OP].flag=0;
    g_Units[STRAIN_OP].flag=0;
    g_Units[SANGLE_OP].flag=0;
    g_Units[DEGREE_OP].flag=0;
    g_Units[PRESS_OP].flag=0;
    g_Units[CP_OP].flag=0;
    g_Units[EA_OP].flag=0;




    /* start of cut */
    /* set up default directory structures */
    Make_Directories();

    //g_Tuning_Factor=500;
    g_Gauss_Recursion=1;
    g_Relaxation_Factor = 0.0;

    //} /* WAS end of if first_time until we started the INI file

    g_nSDs = 1.5;

    g_Gcurve_Count=16;
    g_Gauss_Cut_Dist = 0.05;
    g_Gauss_Parallel = 0.000025;  // square of the angle

#ifdef HOOPS

    g_Display_Mould=1;
    g_Display_Materials=0;
    g_Colour_By_Face=0;
    strcpy(g_Overlay_Text_Size,"0.016sru");
    g_Mapcount = 4;
    HC_QSet_User_Options("/","Linear_Tolerance=0.001,Angular_Tolerance=0.0002 , Cut_Distance=0.05");
#endif	  
    // careful. should match panel.c

 //   g_iterations = 1;
    g_JPEG_x = 500;
    g_JPEG_y = 500;
    g_JPEG_Quality = 100;

    strcpy(g_Export_Options,"everything");
    if(g_Preprocessor_Heuristics ) {
        RXFREE (g_Preprocessor_Heuristics);
    }
    g_Preprocessor_Heuristics = STRDUP("No duplicate nodes, no duplicate seams, (edge > 3) (sail>0)");

#ifdef HOOPS
    g_Hoops_Driver = (char*)CALLOC(256,sizeof(char));
    strcpy(g_Hoops_Driver,"?driver/X11/unix:0.0");
#endif

    /* now the analysis control options */
    //if(first_time) { comment out because of INI file

#ifdef USE_PANSAIL
    g_ControlData[0].press =  PANSAIL_PRESSURE;
    g_ControlData[1].press = PANSAIL_PRESSURE;
    g_ControlData[2].press = PANSAIL_PRESSURE;
#endif
    g_ControlData[0].value = 0.0;
    g_ControlData[0].relax_flags = 0;
    g_ControlData[0].nrelax = 0;
    g_ControlData[0].pansail_flags = 0;
    g_ControlData[0].ncycle = 3;
    g_ControlData[0].phase = 1;
    g_ControlData[0].m_ConvLimit =20;
    strcpy(g_ControlData[0].wakefile,"NONE");
    strcpy(g_ControlData[0].extra_flags_str,"/FI");


    g_ControlData[1].value = 0.0;
    g_ControlData[1].relax_flags = 0;
    g_ControlData[1].nrelax = 0;
    g_ControlData[1].pansail_flags = 0;
    g_ControlData[1].ncycle = 0;
    g_ControlData[1].phase = 2;
    g_ControlData[1].m_ConvLimit =10;
    strcpy(g_ControlData[1].wakefile,"phase1.wak");


    g_ControlData[2].value = 0.0;
    g_ControlData[2].relax_flags = 0;
    g_ControlData[2].nrelax = 0;
    g_ControlData[2].pansail_flags = 0;
    g_ControlData[2].ncycle = 1;
    g_ControlData[2].phase = 3;
    g_ControlData[2].m_ConvLimit =1;
    strcpy(g_ControlData[2].wakefile,"phase2.wak");
#ifndef USE_PANSAIL
    g_ControlData[0].press = RELAX_PRESSURE;
    g_ControlData[1].press = CUSTOM_PRESSURE;
    g_ControlData[2].press = CUSTOM_PRESSURE;
    g_ControlData[0].ncycle = 25;
    g_ControlData[1].ncycle = 0;
    g_ControlData[2].ncycle = 0;
    g_ControlData[1].relax_flags = NON_LINEAR_FLAG;
    g_ControlData[2].relax_flags = NON_LINEAR_FLAG;
    strcpy(g_ControlData[0].extra_flags_str,"/LL/FI");
    strcpy(g_ControlData[1].extra_flags_str,"/LL/FI");
    strcpy(g_ControlData[2].extra_flags_str,"/LL/FI");
#endif
#ifdef HOOPS
    HC_Open_Segment("/");
    PC_Define_Color_Name("wake_faces1","","","sky blue");
    PC_Define_Color_Name("wake_lines1","","","blue");
    PC_Define_Color_Name("wake_faces2","","","sky blue");
    PC_Define_Color_Name("wake_lines2","","","blue");
    PC_Define_Color_Name("wake_faces3","","","sky blue");
    PC_Define_Color_Name("wake_lines3","","","blue");
    PC_Define_Color_Name("waves","","","sky blue");
    PC_Define_Color_Name("sea","","","blue");

    PC_Define_Color_Name("dxfcolor0","","","black");
    PC_Define_Color_Name("dxfcolor1","","","red");
    PC_Define_Color_Name("dxfcolor2","","","yellow");
    PC_Define_Color_Name("dxfcolor3","","","green");
    PC_Define_Color_Name("dxfcolor4","","","cyan");
    PC_Define_Color_Name("dxfcolor5","","","blue");
    PC_Define_Color_Name("dxfcolor6","","","pink");
    PC_Define_Color_Name("dxfcolor7","","","white");
    PC_Define_Color_Name("dxfcolor8","","","grey");
    PC_Define_Color_Name("dxfcolor9","","","dark gray");
    HC_Close_Segment();
#endif
}// init globals

#ifndef AMGUI_2005

int RelaxWorld::GetGlobalValues(const int w)
{
    char *tdir;
    static int first_time = 1;

    if(first_time) {
        InitGlobals();
        tdir = getcwd(g_currentDir, (int)255); /* current working directory */
        strcpy(g_workingDir,g_currentDir);
        tdir = getenv("R3CODE");
        if(tdir)
            strcpy(g_CodeDir,tdir);
        else {
            strcpy(g_CodeDir,g_currentDir);		/* application code directory */
            g_World->OutputToClient("No R3CODE environment variable found.\n Using current directory.",2);
        }
    }

    FILE *fp;

    switch(w) {
    case INI_FILE :
        fp = Open_INI_File(g_defaultsFile);
        break;
    case  DEFAULTS_FILE:
        fp = Open_Defaults_File(g_defaultsFile);
        break;
    default:
        fp=NULL;       assert(0);
    }
    if(!fp) { cout<<" cant open file: "<<g_defaultsFile<<endl;   return 0;}

    int k;
    QString cline;
    {
    QTextStream in(fp);

    g_World->Clear_SumTolList();
    for(;;) {
        cline =  in.readLine();
        if(cline.isNull() ) break;
        k= cline.indexOf("!");
        if( k>=0)		/* strip comment */
            cline.remove(k,cline.length());
        if(!cline.length())
            continue;
        QStringList qwds = cline.trimmed().split(QRegExp("[:\t]") ); // keep empty parts by default
        if(qwds.size()<1)
            continue;
        qwds[0].replace("_", " ");
        this->qGetOneSettings(qwds,first_time);
    }
    }
    fflush(stdout);
    FCLOSE(fp);

    if(g_Gauss_Cut_Dist <g_SOLVE_TOLERANCE*2 )
        g_Gauss_Cut_Dist = g_SOLVE_TOLERANCE*2 ;
#ifdef HOOPS
    char *display_name;
    display_name = getenv("DISPLAY");

    if(display_name) {
        if(!strieq(display_name,":0.0") || !strieq(display_name,":0.")) {
            char * sz = stristr(g_Hoops_Driver,"unix");
            if(sz)
                strcpy(sz,display_name);
            printf("Hoops_Driver will be OPENED on '%s'\n",g_Hoops_Driver);
        }
    }
#endif
    first_time=0;
    return(1);
}
int RelaxWorld::qGetOneSettings( QStringList &ll, const int first_time)
{
    std::vector<std::string> wds;
    int i;
    if(!ll.size())
        return 0;
    for (i=0;i<ll.size();i++)
        wds.push_back(ll.value(i).toStdString());
    rxreplacestring(wds[0],"_", " ");
    return this->GetOneSettings(wds,first_time);
}

int RelaxWorld::GetOneSettings(const std::vector<std::string> sWds,const int first_time)
{
    int rc=1;
    double f;
    //    char *lp;
     std::string sline;

    const char *fw,*lw, *wd;
    int i;
    int nw = sWds.size();
    if(nw<1)
        return 0;
    fw = sWds[0].c_str();
    lw = sWds[nw-1].c_str();

     for(i=0;i<nw-1;i++)
    {
        sline +=sWds[i]; sline+= ":";
     }
     sline +=sWds[nw-1];

#ifdef HOOPS
    if(stristr(fw,"Overlay Text Size")) {
        if ((lp = strtok(line,":"))) {
            lp = strtok(NULL,"\n");
            if(lp) {
                PC_Strip_Leading(lp);
                PC_Strip_Trailing(lp);
                strcpy(g_Overlay_Text_Size,lp);
                sprintf(sstr,"size=%s",g_Overlay_Text_Size);
                printf("Overlay_Text_Font set to %s\n",sstr);
                if(HC_QShow_Existence( "/.../text_overlay" ,"self"))  HC_QSet_Text_Font("/.../text_overlay" ,sstr);
                if(HC_QShow_Existence( "?Picture","self"))  {
                    cout<< "Available fonts are...\n"<<endl;
                    HC_Begin_Font_Search("?Picture","generic,family");
                    while(HC_Find_Font(sstr))
                        printf("%s\n", sstr);
                    HC_End_Font_Search();
                }
            } }
    }

    else if(stristr(fw,"hoopsnet")) {
        strcpy( g_HNetServerIPAddress,lw);
        printf("HNetServerIPAddress set to %s\n",g_HNetServerIPAddress);
    }
    else
#endif
        if(stristr(fw,"environment")) {
            const char* s1, *s2;
            char *s3,*old  = NULL;
            int l_env;
            if(nw>=3) {
                s1 = sWds[1].c_str();
                s2 = sWds[2].c_str();
                cout<<"Env '"<<s1<<"' ";
                old = getenv(s1);
                if(old) cout<<" was '"<<old <<"'";
                s3=(char*)MALLOC(strlen(s1)+strlen(s2)+2);
                sprintf(s3,"%s=%s",s1,s2);
#ifdef WIN32    //for compatibilty with WINDOWS XP
                l_env = _putenv(s3); // DONT free it  RXFREE(s3);
#else
                l_env = putenv(s3);
#endif
                if(-1 ==l_env) g_World->OutputToClient("putenv failed",2);//FOR WINDOW DEBUG 1904
                old = getenv(s1);
                if(old) cout<<" is now '"<<old  <<"'"<<endl;
                else cout<<" didnt putenv '"<< s1  << "' ='"<< old << "'"<<endl;
            }// if nw
        }
        else if(stristr(fw,"Export Reactions")) { //
            // options are:  All (-1)
            // or none (0)
            // or flagged (the default) (-2)
            // or an integer value = 1&&2&&4&&8&&16&&32 for exporting 123456 respectively
            i = atoi(lw);
            g_Export_Reactions = i;
            cout<<"Export_Reactions  set to "<<g_Export_Reactions<<endl ;
        }
        else if(stristr(fw,"cdextra0")) {
            strcpy( g_ControlData[0].extra_flags_str ,lw);
            cout<<"g_ControlData[0].extra_flags_str: "<<g_ControlData[0].extra_flags_str<<endl;
        }
        else if(stristr(fw,"cdextra1")) {
            strcpy( g_ControlData[1].extra_flags_str ,lw);
            cout<<"g_ControlData[1].extra_flags_str: "<<g_ControlData[1].extra_flags_str<<endl;
        }
        else if(strieq(fw,"cdextra")) {
            for(int k=0;k<3 &&k<nw-1;k++){
                if(sWds[k+1].size()  )
                {
                    qDebug()<<" valgrind didnt like this , copying `"<<sWds[k+1].c_str()<<"`";
                    strncpy(g_ControlData[k].extra_flags_str,sWds[k+1].c_str(),255);}
                else
                    strcpy(g_ControlData[k].extra_flags_str  ,"");
                cout<<"extraflags("<<k<<") set to "<<g_ControlData[k].extra_flags_str<<endl;
            }
        }
        else if(strieq(fw,"cdncycle")) {
            for(int k=0;k<3 &&k<nw-1;k++){
                g_ControlData[k].ncycle = atoi(sWds[k+1].c_str () );
                cout<<"ncycle("<< k<<") set to "<< g_ControlData[k].ncycle<<endl ;
            }
        }
        else if(strieq(fw,"cdvalue")) {
            for(int k=0;k<3 &&k<nw-1;k++){
                g_ControlData[k].value =(float) atof(sWds[k+1].c_str () );
                cout<<"value("<< k<<") set to "<< g_ControlData[k].value<<endl ;
            }
        }
        else if(strieq(fw,"cdwhat")) {
            for(int k=0;k<3 &&k<nw-1;k++){
                int j =atoi(sWds[k+1].c_str () );
#ifdef USE_PANSAIL
                if(j!=RELAX_PRESSURE &&j!= PANSAIL_PRESSURE &&
                        j!=CONSTANT_PRESSURE &&j!= FILE_PRESSURE&&
                        j!=CUSTOM_PRESSURE &&j != SCRIPT_PRESSURE
                        &&j!=FIELD_PRESSURE) {
                    cout << "permissable values for what are"<<endl
                         << "    RELAX_PRESSURE" <<(1) <<endl
                         << "    PANSAIL_PRESSURE " <<(2) <<endl
                         << "    CONSTANT_PRESSURE " <<(4) <<endl
                         << "    FILE_PRESSURE " << (8) <<endl
                         << "    CUSTOM_PRESSURE " << (16)<<endl
                         << "    SCRIPT_PRESSURE " << (32)<<endl
                         << "    FIELD_PRESSURE " << (64)<<endl
                         << "    input value = " << j <<endl;
                    continue;
                }
#else
                if(j!=RELAX_PRESSURE && j!=WERNER_PRESSURE &&
                        j!=CONSTANT_PRESSURE &&j!= FILE_PRESSURE&&
                        j!=CUSTOM_PRESSURE &&j != SCRIPT_PRESSURE
                        &&j!=FIELD_PRESSURE && j!=INTERPOLATE_PRESSURE ) {
                    cout << "permissable values for what are"<<endl
                         << "    RELAX_PRESSURE" <<(1) <<endl
                         << "    WERNER_PRESSURE " <<(2) <<endl
                         << "    CONSTANT_PRESSURE " <<(4) <<endl
                         << "    FILE_PRESSURE " << (8) <<endl
                         << "    CUSTOM_PRESSURE " << (16)<<endl
                         << "    SCRIPT_PRESSURE " << (32)<<endl
                         << "    FIELD_PRESSURE " << (64)<<endl
                         << "    INTERPOLATE_PRESSURE "<<(128)<<endl
                         << "    input value = " << j <<endl;
                    continue;
                }
#endif

                g_ControlData[k].press =j;
                cout<<"what("<< k<<") set to "<< g_ControlData[k].press<<endl ;
            }
        }
        else if(strieq(fw,"cdconvergence")) { // this is order-dependent. If it comes after Convergence Limit it wins
            for(int k=0;k<3 &&k<nw-1;k++){
                g_ControlData[k].m_ConvLimit = atof (sWds[k+1].c_str () );
                cout<<"Analysis convergence Limit on phase("<< k<<") set to "<< g_ControlData[k].m_ConvLimit<<endl ;
            }
        }
        else if(stristr(fw,"Convergence Limit")) {// this is order-dependent. If it comes after 'cdconvergence' it wins
            f = atof(lw);
            g_Relax_Tolerance =  f;
            cout<<"All Analysis convergence limits set to "<<f<<" N"<<endl;

            for(int k=0;k<3;k++)	g_ControlData[k].m_ConvLimit = f;  // order dependent
        }
        else if(stristr(fw,"writeZi")) {
            g_World->PleaseWriteZIs= atoi(lw);
            cout<<"PleaseWriteZIs  set to "<<g_World->PleaseWriteZIs <<endl ;
            cout<< " NOTE:  1 = just zis, 2 = formfinding freeze for strings" <<endl;
        }
        else if(stristr(fw,"DrawingLayerHasPrecedence")) {
            g_World->m_DrawingLayerHasPrecedence= atoi(lw);
            cout<<"DrawingLayerHasPrecedence set to "<<g_World->m_DrawingLayerHasPrecedence <<endl ;
        }

        else if(stristr(fw,"threads")) {// number of OMP threads to be used.
            relaxflagcommon.NompThreads  = atoi(lw);
            cout<<"NompThreads set to "<<relaxflagcommon.NompThreads   <<endl ;
        }
        else if(strieq(fw,"ForceLinearDofoChildren")) {// 1 for linear children, 2 for linear eval
            relaxflagcommon.ForceLinearDofoChildren  = atoi(lw);
            cout<<"FLinDoChi  set to "<<relaxflagcommon.ForceLinearDofoChildren   <<endl ;
            cout<< "1 for linear children, 2 for linear eval"  <<endl;
        }
        else if(stristr(fw,"admasmin")) {// ratio of min to max eigenvalues of a node's mass matrix
            cout<<"admasmin was "<< relaxflagcommon.admasmin;
            relaxflagcommon.admasmin  = atof(lw);
            cout<<", set to "<<relaxflagcommon.admasmin<<endl ;
        }
        else if(stristr(fw,"residualcap")) {// clip the residuals passed to the implicit solver.Negative to disable
            relaxflagcommon.residualCap = atof(lw);
            cout<<"residualCap  set to "<<relaxflagcommon.residualCap <<endl ;
        }
        else if(stristr(fw,"dampingfactor")) { // feedback for the implicit solver.  ) is no feedback
            relaxflagcommon.DampingFactor = atof(lw);
            cout<<"DampingFactor set to "<<relaxflagcommon.DampingFactor <<endl ;
        }
        else if(stristr(fw,"Velocity Limit")) {   // clip the displacements in implicit solver.
            relaxflagcommon.factor2 = atof(lw);
            cout<<"Upper Velocity Limit set to "<<relaxflagcommon.factor2<<endl ;
        }
        else if(stristr(fw,"factor 1")|| stristr(fw,"acceleration limit")) {
               cout<<"factor1 ( Mass^-1.R limit in Stiff solver ) was "<<relaxflagcommon.factor1 ;
            relaxflagcommon.factor1 = atof(lw);
            cout<<", set to "<<relaxflagcommon.factor1<<endl ;
        }
        else if(strieq(fw,"UpdateInterval")) {
            relaxflagcommon.g_UpdateTime = atof(lw);
            cout<<"UpdateTime set to "<<relaxflagcommon.g_UpdateTime << " seconds (requires flag /DC)"<<endl;
        }
        else if(strieq(fw,"rkdamping")) {
            relaxflagcommon.g_RKDamping = atof(lw);
            cout<<"RKDamping set to "<<relaxflagcommon.g_RKDamping << " (requires flag /RK)"<<endl;
        }
        else if(strieq(fw,"rkEps")) {
            relaxflagcommon.g_RKEps = atof(lw);
            cout<<"RK Eps set to "<<relaxflagcommon.g_RKEps << "   (requires flag /RK)"<<endl;
        }
        else if(strieq(fw,"rkH1")) {
            relaxflagcommon.g_RK_H1 = atof(lw);
            cout<<"RK H1 set to "<<relaxflagcommon.g_RK_H1 << "   (requires flag /RK)"<<endl;
        }
        else if(strieq(fw,"lambda")) {
            g_World->m_LambdaForxyz = atof(lw);
            cout<<"LambdaForXYZ2UV set to "<<g_World->m_LambdaForxyz<<endl ; //printf("LambdaForXYZ2UV set to %f\n",g_World->m_LambdaForxyz);
        }

        else if(stristr(fw,"Janet")) {
            g_Janet = atoi(lw);
            cout<<"Janet set to "<<g_Janet<<endl ;
        }
        else if(stristr(fw,"PardisoIterations")) {
            g_NPardisoIterations = atoi(lw);
            cout<<"g_NPardisoIterations set to "<<g_NPardisoIterations<<endl ;
        }
        else if(stristr(fw,"Filament PP")) {
            g_Filament_pp = atoi(lw);
            cout<<"Filament_pp set to "<<g_Filament_pp<<endl ;
        }
        else if(stristr(fw,"ondeflected")) {
            g_Transform_On_Deflected  = atoi(lw);;
            cout<<"Transform_On_Deflected set to "<<g_Transform_On_Deflected<<endl ;
        }
        else if(stristr(fw,"ONCurve")) {
            g_Use_ONCurve = atoi(lw);
            cout<<"g_Use_ONCurve set to "<<g_Use_ONCurve<<endl ; //printf("g_Use_ONCurve set to %d\n",g_Use_ONCurve);
        }
        else if(stristr(fw,"Gcurve Count")) {
            i = atoi(lw);
            if (i>1)  {
                g_Gcurve_Count = i;
                cout<<"Gcurve_Count set to "<<g_Gcurve_Count<<endl ; //printf("Gcurve_Count set to %d\n",g_Gcurve_Count);
            }
        }

        else if(stristr(fw,"VectorPlotScale")) {
            g_VectorPlotScale = atof(lw);
            cout<<"g_VectorPlotScale set to "<<g_VectorPlotScale<<endl ; //printf("g_VectorPlotScale set to %f\n",g_VectorPlotScale);
        }

        else if(stristr(fw,"Spline_Division") || stristr(fw,"Spline Division")) {
            i = atoi(lw);  i=max(2,i); i=min(10000,i);
            if(i >= 0 && i < 10000) {
                cout<<"Spline_Division was "<<g_Spline_Division ;
                g_Spline_Division = i;
                cout<<" set to "<<g_Spline_Division<<endl ;
            }
            else
                cout<< " Spline Division: ignore unreasonable value= " <<i << "  (limit=10000) "<<endl;


        }
#ifdef USE_PANSAIL
        else if(stristr(fw,"Wake Relax")) {
            printf(" wake relaxation factor was %f..",g_relfac);
            g_relfac = atof(lw);
            printf(" now <%f>\n",g_relfac);
        }
#endif
#ifdef HOOPS
        else if(stristr(fw,"Hoops Driver")) {
            if ((lp = strtok(line,":"))) {
                lp = strtok(NULL,"\n");PC_Strip_Leading(lp);
                PC_Strip_Trailing(lp);
                strcpy(g_Hoops_Driver,lp);
                printf("Hoops Driver set to %s\n",g_Hoops_Driver);
            }
        }

        else if(stristr(fw,"nsds") ||  stristr(fw,"Contour Range")   ) {
            g_nSDs =  (float) atof(lw);
            printf("Contour Range set to %f Standard deviations\n",g_nSDs);
        }
#endif
        else if(stristr(fw,"Residual damping")) {
            f = atof(lw);
            g_Relax_R_Damp =  f;
            cout<<"Analysis R Damp set to "<<f<<endl ; //printf("Analysis R Damp set to %f\n",f);
        }
        else if(stristr(fw,"velocity damping") || stristr(fw,"gravity")  ) {
            f = atof(lw);
            g_Relax_V_Damp =  f;
            cout<<"Analysis V Damp (= gravity Z) set to "<<f<<" (but its better to use a gravity field)"<<endl ;
        }
        else if(stristr(fw,"diagonal fac")) {
            f = atof(lw);
            g_diagonalfac =  f;
            cout<<"diagonal fac set to "<<f<<endl ; //printf("diagonal fac set to %f\n",f);
        }
        else if(stristr(fw,"Gauss Linear Tol")) {
            f = atof(lw);
            PCTol_Set_Tolerances(NULL, f,PCTOL_LINEAR+ PCTOL_GLOBAL);
        }
        else if(stristr(fw,"Gauss Angle Tol")) {
            f = atof(lw);
            PCTol_Set_Tolerances(NULL,f,PCTOL_ANGULAR+ PCTOL_GLOBAL);
        }
        else if(stristr(fw,"Gauss Cut Dist")) {
            f = atof(lw);
            PCTol_Set_Tolerances(NULL, f,PCTOL_CUT+PCTOL_GLOBAL);
        }

        else if(stristr(fw,"MeshInPanelPlane")) {
            i = atoi(lw);
            g_PanelInPlane =  i;
            cout<<"MeshInPanelPlane set to "<<i<<endl ; //printf("MeshInPanelPlane set to %d\n",i);
        }
        else if(stristr(fw,"Gauss Parallel")) {
            f = atof(lw);
            g_Gauss_Parallel =  f*f;
            cout<<"Gauss_Parallel set to "<<f<<endl ; //printf("Gauss_Parallel set to %f\n",f);
        }
        else if(stristr(fw,"Gauss") && stristr(fw,"factor") ) {
            f = atof(lw);
            g_GAUSS_FACTOR  =  f;
            cout<<"Gauss Relaxation Factor set to "<<f<<endl ; //printf("Gauss Relaxation Factor set to %f\n",f);
        }

        else if(stristr(fw,"Gauss Recursion")) {
            i = atoi(lw);
            cout<<"Gauss_Recursion was "<< g_Gauss_Recursion ;
            g_Gauss_Recursion =  i;
            cout<<" now set to "<<g_Gauss_Recursion<<endl ; //printf(" now set to %d\n",g_Gauss_Recursion);
        }
        else if(stristr(fw,"Preprocessor Heuristics")) {
            char *buf;
            size_t k;
            cout<<"Preprocessor_Heuristics was '"<< g_Preprocessor_Heuristics << "'\n ";
            k =strlen(lw) +16;
            if(g_Preprocessor_Heuristics) k = strlen(g_Preprocessor_Heuristics) + k;
            buf = (char*)MALLOC(k);
            strcpy(buf,lw); strcat(buf,g_Preprocessor_Heuristics);
            g_Preprocessor_Heuristics = (char*)REALLOC(g_Preprocessor_Heuristics, strlen(buf)+8);
            strcpy(g_Preprocessor_Heuristics,buf);
            RXFREE(buf);
            cout<< " now set to "<<g_Preprocessor_Heuristics <<endl;
        }
        else if(stristr(fw,"AeroElastic Relaxation Factor")) {
            f= atof(lw);
            cout<<"AeroElastic Relaxation Factor was "<< g_Relaxation_Factor;
            g_Relaxation_Factor =  f;
            cout<<" now set to '"<<lw<<"' = " << g_Relaxation_Factor<<endl;
        }

        else if(stristr(fw,"units")) {
            int k; double x;
            /* units : stress|strain|angle|pressure : factor : unit name */
            if (nw>2) {wd=sWds[2].c_str ();
                for(k=0;k< N_UNITS;k++) {
                    if(stristr(wd,g_Units[k].kw))
                        break;
                }
                if (k< N_UNITS+1 && nw>3) {
                    wd=sWds[3].c_str(); assert(wd);
                    x=0;
                    if (sWds[3].c_str() !="%")
                        x = atof(wd);
                    g_Units[k].f=x;
                    if (nw>4)
                        strcpy(g_Units[k].units,sWds[4].c_str());
                    cout<<"units of "<<g_Units[k].kw  << " now '"<< g_Units[k].units << "' f="<<g_Units[k].f  << endl;
                }
            }
        }
#ifdef HOOPS
        else if(stristr(fw,"jpeg x")) {
            g_JPEG_x = atoi(lw);
            cout<<"g_JPEG_x =  "<<g_JPEG_x<<endl ; //printf("g_JPEG_x =  %d\n",g_JPEG_x);
        }
        else if(stristr(fw,"jpeg y")) {
            g_JPEG_y = atoi(lw);
            cout<<"g_JPEG_y =  "<<g_JPEG_y<<endl ; //printf("g_JPEG_y =  %d\n",g_JPEG_y);
        }
        else if(stristr(fw,"jpeg quality")) {
            g_JPEG_Quality = atoi(lw);
            cout<<"JPEG_Quality =  "<<g_JPEG_Quality<<endl ; //printf("JPEG_Quality =  %d\n",g_JPEG_Quality);
        }
        else if(stristr(fw,"mapcount")) {
            i = atoi(lw);
            if(i > 0  && i < (MAX_MAP_COUNT - 1)) {
                printf("Mapcount out of range(1 to %d)\n",MAX_MAP_COUNT-2);
                g_Mapcount = i;
            }
            else
                g_Mapcount = 4;
            cout<<"Mapcount set to "<<g_Mapcount<<endl ; //printf("Mapcount set to %d\n",g_Mapcount);
        }

        else if(stristr(fw,"colour by face")) {
            if(stristr(lw,"yes")) {
                g_Colour_By_Face = 1;
            }
            else	{
                g_Colour_By_Face = 0;
            }
            cout<<"Colouring by face set to "<<g_Colour_By_Face<<endl;
        }
       else if(stristr(fw,"display materials")) {
            g_Display_Materials = (!(NULL==stristr(lw,"yes")));
            cout<< "Display_Materials flag set to " << g_Display_Materials<<endl;
        }
        else if(stristr(fw,"display mould")) {
            g_Display_Mould = (!(NULL==stristr(lw,"yes")));
            cout<< "Display_Mould flag set to " << g_Display_Mould<<endl;
        }
#endif
        else if(stristr(fw ,"Export Options")) {
            strcpy(g_Export_Options,lw);
            cout<<"Export_Options set to "<<lw<<endl;
        }
        else if(stristr(fw,"After Each Run")) {
//            if (( lp=strchr(line,':'))) {  //  copy from the first colon
//                lp++;
//                g_qAfterEachRun=lp;
//                g_qAfterEachRun.replace('"',' ');
//                g_qAfterEachRun=g_qAfterEachRun.trimmed();
//            }
            g_qAfterEachRun.clear();
            if(nw>1) {
                for(i=1;i<nw;i++)
                {
                    g_qAfterEachRun.append(QString::fromStdString(sWds[i])) ;
                    if(i <nw-1) g_qAfterEachRun.append(" : ") ;
                }
                g_qAfterEachRun.replace('"',' ');
                g_qAfterEachRun=g_qAfterEachRun.trimmed();
            }
            cout<<"After_Each_Run command is:"<< qPrintable(g_qAfterEachRun ) <<endl;
        }
        else if(stristr(fw,"After Each Cycle")) {
                g_qAfter_Each_Cycle.clear();
                if(nw>1) {
                    for(i=1;i<nw;i++)
                    {
                        g_qAfter_Each_Cycle.append(QString::fromStdString(sWds[i])) ;
                        if(i <nw-1) g_qAfter_Each_Cycle.append(" : ") ;
                    }
                    g_qAfter_Each_Cycle.replace('"',' ');
                    g_qAfter_Each_Cycle=g_qAfter_Each_Cycle.trimmed();
                }
                cout<<"After_Each_Cycle command is:"<< qPrintable(g_qAfter_Each_Cycle ) <<endl;
        }
        else if(stristr(fw,"After Each Update")) {
            g_qAfter_Each_Update.clear();
            if(nw>1) {
                for(i=1;i<nw;i++)
                {
                    g_qAfter_Each_Update.append(QString::fromStdString(sWds[i])) ;
                    if(i <nw-1) g_qAfter_Each_Update.append(" : ") ;
                }
                g_qAfter_Each_Update.replace('"',' ');
                g_qAfter_Each_Update=g_qAfter_Each_Update.trimmed();
            }
            cout<<"After_Each_Update command is:"<< qPrintable(g_qAfter_Each_Update ) <<endl;



        }
        else if(stristr(fw,"tolerance")) {
            g_World->AddOneTol(sline );
        }
        else if(stristr(fw,"custom") &&stristr(fw,"button")  ) {
#ifdef _X
            Create_Custom_Button(sline );
#endif
        }
        else
        {
            rc++;
            if(sWds[0].length())
            {
                if((*fw)!='#'&&!(g_World->m_wind.Set(sline.c_str(),first_time)))
                    cout<<"Settings line not understood (keyword is <"<<fw<<">) <"<<">"<<endl;
            }
        }

    return rc;
}

#endif //#ifndef AMGUI_2005

