#pragma once
 
 /* ETYPES.H the int type efinitions 
   4/12/95 gausscurve added 
 */

EXTERN_C int INTEGER_TYPE(const char*Firstword);

#define PCE_DELETED  99
#define SEAMCURVE  128
#define CURVE 128
#define SEAM 128
#define EDGE  128

#define DXF	  101
#define PANSAIL 102
#define PCE_STRING 103
#define MATERIAL  104
#define PCE_FIXITY  105
#define FIELD 106
#define INTERPOLATION_SURFACE  107
#define STRIPE  108
#define CONNECT  109
#define PCE_NAME  110
#define PCE_CAMERA  111
#define DISPLAY  112
#define GENDP  113
#define INPUT  114
#define ANALYSIS  115
#define MESH  116
#define RENAME 117
#define PCE_ALIAS RENAME

#define PCE_SUMMARY 118
#define BOUNDARY    119
#define COMPOUND_CURVE 120
#define SITE         121
#define RELSITE      122

#define PCE_UVCURVE  123
#define TWODCURVE    124
#define BASECURVE    125
#define THREEDCURVE  126
#define PCE_SPLINE   127
#define GAUSSCURVE   147

#define PATCH   129
#define POCKET  130
#define BATTEN  131
#define FABRIC  132
#define PANEL   135
#define PCE_TPN  136
#define PCE_SETDATA  137
#define INCLUDE  138
#define PANELFAB 139
#define AEROMIK  140
#define	PCE_LAYER 141

#define PCE_PSIDE 143
#define PCE_SAIL  144

#define RLX       146

#define PCE_CUT      148
#define PCE_GOAL     149
#define PCE_ZONE     150
#define PC_QUESTION  151
#define PCE_MOULD    152
#define PCE_COORDS   153
#define PCE_WAKE     154
#define PCE_AERO      155
#define PCE_STREAMLINE 156
#define PCE_VELOCITY 157
#define PCE_NURB     158     /*debugentity*/
#define PCE_GLE      159    /*adebugentity*/


#define MSHFILE   160
#define DATFILE   161
#define ANALYSISFILE   162
#define INFILE			163
#define DO_BOAT			170
#define EDIT			171
#define EDITWORD		172
#define PCE_DELETE_CARD 173
#define PCE_IGES  		174

#define   PC_PANELDATA	175

#define PCE_3DM             177
#define PCE_PRESSURE        178
#define PCE_DOVE            179
#define PCE_CONSURF         180
#define PCE_PCFILE          181
#define PCE_SCALAR          182
#define PCE_DWGLAYER        183
#define PCE_BEAMSTRING      184
#define PCE_BEAMMATERIAL    185
#define PCE_VECTORFIELD     186
#define PCE_IMPORT          187
#define PCE_PSHELL          188
#define PCE_RIGIDBODY       189
#define PCE_NODECURVE       190
#define PCE_SUBMODEL        191




