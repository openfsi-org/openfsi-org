#ifndef __RXFIXITY__H__
#define __RXFIXITY__H__

#include "RXEntity.h"
// TYPE PCE_FIXITY type 'fixity'
class RXFixity :
        public RXEntity
{
public:
    RXFixity(void);
    RXFixity (class RXSail*p);
    ~RXFixity(void);

    virtual int Dump(FILE *fp) const;
    virtual int Compute(void); // return 1 if the object moved requiring compute of its nieces
    virtual int Resolve(void);
    virtual int Finish(void);
    virtual int ReWriteLine(void);
    virtual int CClear();

protected:
    // The dofo will be in this' sail
    int m_dofono;
};
#endif

