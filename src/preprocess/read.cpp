/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *  1/3/96 Read_CC card
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"
#include "RXSRelSite.h"
  /* this is read.c
     the read_ (entity) routines which were in entities.c abs batpatch.c
	 They are ALL simple reads. They call the resolve routine */

   #ifndef _NOT_MSW
	//#include "multipad.h"
	//#include "dummynt.h"

#endif

#include "hoopcall.h"
 
#include "entities.h"
#include "finish.h"
#include "resolve.h"
#include "read.h"

#ifdef _NOT_MSW
  #define _max max
  #define _min min
#endif
               
/* RXEntity_p Read_Compound_Curve_Card(SAIL*sail,char *line) {
   	
	RXEntity_p pe;
	char Firstword[256],name[256];
	int depth=1;
	Get_First_Words(line,Firstword,name); 
	pe = Just_Read_Card(sail,line,Firstword,name,&depth);
	Resolve_Compound_Curve_Card(pe); 	  //  sets the Needs_Flage and does a compute too
 
	return(pe);
}    */
  
  RXEntity_p Read_Batten_Card(SAIL*sail, char *line) {
   	
	RXEntity_p pe;
	char Firstword[256],name[256];
	int depth=1;
	Get_First_Words(line,Firstword,name); 
	pe = Just_Read_Card(sail, line,Firstword,name,&depth);
	Resolve_Batten_Card(pe); 	  /*  sets the Needs_Flage and does a compute too */
 
	return(pe);
}                    
RXEntity_p Read_Fabric_Card(SAIL*sail,char *line) {
   	
	RXEntity_p pe;
	char Firstword[256],name[256];
	int depth=1;
	Get_First_Words(line,Firstword,name); 
	pe = Just_Read_Card(sail,line,Firstword,name,&depth);
	Resolve_Fabric_Card(pe); 	  /*  sets the Needs_Flage and does a compute too */
 
	return(pe);
}       
        
                   
Site * Read_Site_Card(SAIL*sail,char *line) {
   	
	Site * pe;
	char Firstword[256],name[256];
	int depth=1; /* dont know anbout this - adam */
	Get_First_Words(line,Firstword,name); 
	pe =(Site*) Just_Read_Card(sail,line,Firstword,name,&depth);
	pe->Resolve(); 	  /*  sets the Needs_Flage and used to do a compute too */
 	pe->Finish();   /* does the  compute too */
	return(pe);
}
Site * Read_Relsite_Card(SAIL*sail,char *line) {
   	
	Site * pe;
	char Firstword[256],name[256];
	int depth=1;  /* as above */
	Get_First_Words(line,Firstword,name); 
	pe = (Site*) Just_Read_Card(sail,line,Firstword,name,&depth);
	pe->Resolve(); 	  /*  sets the Needs_Flage and doesnt compute  */
 /* MUST be followed immediately by a call to finish relsite */
	return(pe);
}
 RXEntity_p Read_SeamCurve_Card(SAIL*sail, char *line) {
   	
	RXEntity_p pe;
	char Firstword[256],name[256];
	int depth=1;
	Get_First_Words(line,Firstword,name); 
	pe = Just_Read_Card(sail,line,Firstword,name,&depth);
	pe->Needs_Resolving= !pe->Resolve(); 	  /*  sets the Needs_Flage and does a compute too */
 
	return(pe);
}
   RXEntity_p Read_Spline_Card(SAIL *sail, char *line) {
   	
	RXEntity_p pe;
	char Firstword[256],name[256];
	int depth=1;
	Get_First_Words(line,Firstword,name); 
	pe = Just_Read_Card(sail,line,Firstword,name,&depth);
        pe->Resolve(); 	  /*  sets the Needs_Flage and does a compute too */
 	return(pe);
}

