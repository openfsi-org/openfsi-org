#pragma once

#include <string>


#ifndef NO_RX_EXCEPTIONS
 
#define RXTHROW(a) _rxthrow (a);
#else
#define RXTHROW(a) 
#endif


class RXException
{
public:
	RXException(void);
	RXException(int a);
	RXException(char *a);

	RXException(std::string a);
 
	~RXException(void);
	int Print(FILE *fp=stdout);

protected:
	int m_i;
	std::string m_s;
 	int m_type; 
};
void _rxthrow (int a);
void _rxthrow (char *a);
void _rxthrow (std::string a);
 void _rxthrow (std::wstring a);

 
