 
  /*     SUBROUTINE Make_Nodal_Values(sli,P,H,Ncolors,nsds,cmin,cmax)
      IMPLICIT NONE
      include 'nodecom.f'
      include 'sailcom.f'
      include 'offsets.f'
c     include 'tricom.f'     
*     takes the list P of element values and returns a list H
*     of nodal values, adjusted to fit in the range (0 to nsds-1)     
*     So H is ready for passing to Hoops     
*     Now map mean  onto  cmid and (+-Extent)*SD onto cmin,cmax
          
      REAL*4 P(*)   ! values on elements
	 INTEGER ncolors	! entries in color map 
	 REAL*4 nsds		   ! no of SDs to map to the half-range
	 integer*4 imodel
!
!  returns
      REAL*4 H(*)
 REAL*4 cmax,cmin

! locals */

#ifndef TOHOOPS_16NOV04
#define TOHOOPS_16NOV04
#pragma error("tohhops.h is empty")
#endif //#ifndef TOHOOPS_16NOV04



