//RXCurve.cpp implementation of the class RXCurve
//Thomas Ricard 05 07 04
// 30 Nov 2004 Peter added some MEMCHECKS the removed them.
//  They don't work for gcc new operator
// end may 2005 return path for short non-ON curves.


#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include "RelaxWorld.h"

#include "RXEntityDefault.h"
#include "RXSail.h"
#include "RXOffset.h"
#include "rlxNearestPoint.h"
#include "rxON_Extensions.h"

#include "boxsearch.h"
#include "slowcut.h"

#include "polyline.h"

#include "drawing.h"
#include "curveintersection.h"

#include "global_declarations.h"
#ifdef DEBUG
#include <list>
#endif
#include "RXCurve.h"

RXCurve::RXCurve()
{
    Init();
}//RXCurve::RXCurve

//Copy constructor
RXCurve::RXCurve(const RXCurve & p_Obj)
{
    Init();
    this->operator =(p_Obj);
    m_e = p_Obj.m_e;

}//RXCurve::RXCurve(double p_x,double p_y,double p_z)

//Copy constructors
RXCurve::RXCurve(const ON_Curve * p_Obj)
{

    const ON_Curve * l_cr = dynamic_cast<const ON_Curve*>(p_Obj);
    assert(l_cr);
    Init();

    m_pONCurve = (ON_Curve*)l_cr;
} 

RXCurve::RXCurve(VECTOR*pxin,int cxin)
{
    Init();
    m_ONCurveBelongtoThis = 1; //we want to create an ON_Curve to store the the data

    Fill_Curve(pxin,cxin);
}//RXCurve::RXCurve(VECTOR*pxin,int cxin)

RXCurve::~RXCurve()
{
    //thomas 29 10 04 uncommented Clear(); NOT SURE ABOUT THE SIDE EFFECTS

    Clear();
}//RXCurve::~RXCurve

void RXCurve::Init()
//meme alloc + init val to 0 0 0
{
    RXTRObject::Init();
    m_pline = NULL;
    m_c = 0;
    m_C0 = 0;
    m_ds = NULL;
    m_zax = NULL;
    m_dy=m_dz=0.0;
    m_qcd = NULL;
    m_pONCurve = NULL;
    m_ONCurveBelongtoThis = 0;
    m_type = 0;
    m_e = NULL;
    m_bb.Destroy();
}//void RXCurve::Init


int RXCurve::Clear()
{
    assert(!m_type);

    //	if (m_p) {  RXFREE(m_p);  m_p=NULL;}
    //were not going to do this here because m_p very seldom owns what it is
    //pointing to.

    if (m_ds) { RXFREE(m_ds); m_ds=NULL;}

    if (m_pONCurve)
    {
        if (!m_ONCurveBelongtoThis)
        {
            //we want to disconnect the ONObject from the curve without removing the ONObject,
            m_pONCurve = NULL;
        }
        else
        {
            delete m_pONCurve;
            m_pONCurve = NULL;
        }
    }
    if(this->m_pline)
        RXFREE(this->m_pline);
    this->m_pline=0;// jan 2009  a leak. SOmetimes m_p has already been freed
    m_c = 0;
    m_C0 = 0;
    m_zax = NULL;
    m_dy=m_dz=0.0;
    if(m_qcd)  // Peter added the iff Free
        RXFREE(m_qcd);
    m_qcd = NULL;
    m_type = 0;
    m_ONCurveBelongtoThis = 0;

    RXTRObject::Clear_SSD();
    return(1);
}//int RXCurve::Clear

RXCurve& RXCurve::operator = (const RXCurve & p_Obj)
{
    //Make a copy
    Clear();
    RXTRObject::operator =(p_Obj);

    m_c = p_Obj.m_c;
    if (m_c>0)
    {
        assert(!m_pline);
        m_pline = (VECTOR *)MALLOC( (m_c) *sizeof(VECTOR));
        if (p_Obj.m_pline)
            memcpy(m_pline,p_Obj.m_pline,(m_c)*sizeof(VECTOR)); // nov 2009 peter copies c not c+1
        else
            this->Get_Ent()->OutputToClient("ERROR: in RXCurve::operator=, p_Obj.m_p == NULL",2);
        assert(!m_ds);
        m_ds = (double *)MALLOC( (m_c) *sizeof(double));
        if (p_Obj.m_ds)
            memcpy(m_ds,p_Obj.m_ds, (m_c)*sizeof(double)); // nov 2009 peter copies c not c+1
        else
            this->Get_Ent()->OutputToClient("ERROR: in RXCurve::operator=, p_Obj.m_ds == NULL",2);
    }
    m_C0 = p_Obj.m_C0;

    m_zax = p_Obj.m_zax;

    m_dy=p_Obj.m_dy;
    m_dz=p_Obj.m_dz;

    if (p_Obj.m_qcd)
        assert(!p_Obj.m_qcd);
    //	m_qcd = p_Obj.m_qcd;

    m_ONCurveBelongtoThis = p_Obj.m_ONCurveBelongtoThis;
    if (p_Obj.m_pONCurve)
    {

        //WE JUST Want to poit ot a ON_Curve not to create a new instance thomas 02 11 04
        //BEFORE 02 011 04 const ON_Curve * l_crv = dynamic_cast<ON_Curve*>(p_Obj.m_pONCurve);
        //BEFORE 02 011 04 if (!l_crv)
        //BEFORE 02 011 04 	this->Get_Ent()->OutputToClient("RXCurve::m_pONCurve is not an ON_Curve object, in RXCurve::Get_arc() ",2);
        //BEFORE 02 011 04 m_pONCurve = ON_NurbsCurve::New();
        //BEFORE 02 011 04 m_pONCurve->operator = (*l_crv);
        if (!m_ONCurveBelongtoThis)
        {
            if (dynamic_cast<ON_Curve*>(p_Obj.m_pONCurve))
                m_pONCurve = p_Obj.m_pONCurve;
            else
                m_pONCurve = NULL;
        }
        else
        {
           // assert(0);   // TRY
            m_pONCurve = ON_NurbsCurve::New();
            m_pONCurve->operator = (*(p_Obj.m_pONCurve));
        }
    }
    else
        m_pONCurve = NULL;
    m_type = p_Obj.m_type;

    return *this;
}//void RXCurve::operator =


char * RXCurve::GetClassID() const
{
    return RXCURVECLASSID;
}//char * RXCurve

int RXCurve::IsKindOf(const char * p_ClassID) const
{
    if (streq(p_ClassID,RXCURVECLASSID))
        return true;
    else
        return RXTRObject::IsKindOf(p_ClassID);
}//int RXCurve::IsKindOf

RXCurve * RXCurve::Cast( RXTRObject* p_pObj) 
{
    return(RXCurve *)Cast((const RXCurve*)p_pObj);
}//RXCurve * RXCurve::Cast

const RXCurve * RXCurve::Cast( const RXTRObject * p_pObj) 
{
    if (!p_pObj)
        return NULL;

    if (p_pObj->IsKindOf(RXCURVECLASSID))
        return (const RXCurve *)p_pObj;
    else
        return NULL;
}//const RXCurve * RXCurve::Cast


void RXCurve::SetONCurve(ON_Curve * p_obj,const char*p_text)
{
#ifdef _DEBUGNEVER // this is 75% of preproc time.
    if (p_obj)
    {
        if (CheckONCurve(p_obj,p_text))
            m_pONCurve = p_obj;
        else {
            m_pONCurve = NULL;
            this->Get_Ent()->OutputToClient( "Check ONC failed",3);
        }
    }
    else
        m_pONCurve = NULL;
#else
    m_pONCurve = p_obj;
#endif
}//void RXCurve::SetONCurve

ON_Curve * RXCurve::GetONCurve() const
{
    if (m_pONCurve)
    {
        assert(dynamic_cast<ON_Curve*>(m_pONCurve));

        return m_pONCurve;
    }
    else
        return NULL;
}//ON_Curve * RXCurve::GetONCurve

ON_3dPoint RXCurve::PointAt(const double & p_t) const 
{

    if(m_pONCurve)
        return m_pONCurve->PointAt(p_t);
    else {
        VECTOR v;
        Place_On_Poly(p_t,m_pline,m_c,&v);
        return ON_3dPoint(v.x,v.y,v.z);
    }
    assert(0);
    return ON_3dPoint(0,0,0);
}//ON_3dPoint RXCurve::PointAt

ON_3dVector RXCurve::TangentAt(const double & p_t) const 
{
    assert(m_pONCurve); //could be removed later!

    return m_pONCurve->TangentAt(p_t);
}//ON_3dVector RXCurve::TangentAt

void RXCurve::SetType(const int & p_type)
{
    m_type = p_type;
}//void RXCurve::SetType

int RXCurve::GetType() const
{
    return m_type;
}//int RXCurve::GetType


void RXCurve::Set_p(VECTOR	* p_p)
{
    m_pline = p_p;  // WHY ON EARTH??????
}//void RXCurve::Set_p

VECTOR	*RXCurve::Get_p() const
{
    return m_pline;
}//VECTOR	*RXCurve::Ge_p

void RXCurve::Set_c(const int & p_c)
{
    m_c = p_c;
}//void RXCurve::Set_c
int RXCurve::Get_c() const
{
    return m_c;
}//int RXCurve::Get_c

void RXCurve::Set_C0(const int & p_C0)
{
    m_C0 = p_C0;
}//void RXCurve::Set_C0

int RXCurve::Get_C0() const
{
    return m_C0;
}//int *RXCurve::Get_C0

void RXCurve::Set_ds(double	* p_ds)
{
    m_ds = p_ds;
}//void RXCurve::Set_ds

double *RXCurve::Get_ds() const
{
    return m_ds;
}//double *RXCurve::Get_ds

double RXCurve::Get_arc(double fractional_tolerance) const
{	
    double l_len = 0.0;
    if(!this-> IsValid()) {
        wcout<<L"Get_Arc on RXCurve which isnt ON_Valid "<< this->GetTROName()<<endl;
        if(this->Get_Ent ())
            wcout<<L"name = "<<TOSTRING(this->Get_Ent ()->name())<<endl;
        return l_len;
    }

    if(m_pONCurve)
    {
        const ON_Curve * l_crv = dynamic_cast<ON_Curve*>(m_pONCurve);
        if (!l_crv)
            this->Get_Ent()->OutputToClient("RXCurve::m_pONCurve is not an ON_Curve object, in RXCurve::Get_arc() ",2);

        l_crv->GetLength(&l_len,fractional_tolerance);	// // Sept 2006 GetLengthGetLength  CODED coded with linear method
        if(l_len ==0.0) {
            printf(" GetLength (349) gives 0.0 :RXCurve <%S>!!!!!!!!!!!\n",GetTROName().c_str());
            // go again for debug stepping
            l_crv->GetLength(&l_len,fractional_tolerance);
        }
        return l_len;
    }
    else {
        if(m_c>0)
        {
            int i;
            for (i=1;i<m_c;i++)
                l_len += PC_Dist(&(m_pline[i-1]),&(m_pline[i]));
        }
        else
            cout<< " GetLength(360) gives 0.0!!!!!!!!!!!"<<endl;
        return l_len;
    }

}//double RXCurve::Get_arc

double RXCurve::Get_chord() const
{
    double l_chord = 0.0;
    if(m_pONCurve)
    {
        const ON_Curve * l_crv = dynamic_cast<ON_Curve*>(m_pONCurve);
        if (!l_crv)
            this->Get_Ent()->OutputToClient("RXCurve::m_pONCurve is not an ON_Curve object, in RXCurve::Get_chord() ",2);

        ON_3dPoint l_ps(l_crv->PointAtStart());
        l_chord = l_ps.DistanceTo(l_crv->PointAtEnd()); // works
    }
    else
    {
        if(m_c>0)
            l_chord = PC_Dist(&(m_pline[0]) ,&(m_pline[(m_c)-1]));
        else
            l_chord = 0.0;
    }

    if(l_chord ==0.0)
        cout<< " RXCurve::GetChord returns 0!!!!!!!!!!!"<<endl;

    return l_chord;
}//double RXCurve::Get_chord

void RXCurve::Set_zax(VECTOR	*p_zax)
{
    m_zax = p_zax;
}//void RXCurve::Set_zax

VECTOR * RXCurve::Get_zax() const
{
    return m_zax;
}//VECTOR * RXCurve::Get_zax

void RXCurve::Set_dy(const double & p_dy)
{
    m_dy = p_dy;
}//void RXCurve::Set_dy

double	RXCurve::Get_dy() const
{
    return m_dy;
}//double	RXCurve::Get_dy

void RXCurve::Set_dz(const double & p_dz)
{
    m_dz = p_dz;
}//void RXCurve::Set_dz

double RXCurve::Get_dz() const
{
    return m_dz;
}//double RXCurve::Get_dz

void RXCurve::Set_qcd(struct PC_QCD *p_qcd)
{
    m_qcd = p_qcd;
}//void RXCurve::Set_qcd

struct PC_QCD * RXCurve::Get_qcd() const
{
    return m_qcd;
}//struct PC_QCD * RXCurve::Get_qcd

int RXCurve::Place_On(const double &p_offset, ON_3dPoint *p_v,double fractional_tolerance) const
{
    /* 1) find k0 which is the knot index where
  EITHER K0 = c-2 (or less if dist(c-2,c-1) is small)
  OR		K0 = 0 and offset <0
  OR 		K0+1 is the first past the offset

 2) find K1 which is first knot index >K0 where dist(k0,k1) is not small
 */
    /*
return values. 
m_pONCurve
RXCURVE_IN_DOMAIN
RXCURVE_OFF_DOMAIN_AT_END
RXCURVE_ERROR
*/
    assert(!m_type);
    int l_flag	;
    if(m_pONCurve)
    {
        ON_3dPoint l_point;
        double l_t = 0.0;
        l_flag = GetParamAt(p_offset,l_t,fractional_tolerance);

        if ((p_offset!=0)&&(l_t==0)) { //  I don't understand this.  Peter.
            int l_flag2 = GetParamAt(p_offset,l_t,sqrt(fractional_tolerance));
        }
        ON_Interval l_domain(m_pONCurve->Domain());

        if (l_flag==RXCURVE_IN_DOMAIN)
        {
            m_pONCurve->EvPoint(l_t,l_point);
            p_v->x=l_point.x; 	p_v->y=l_point.y; 	p_v->z=l_point.z;
            return l_flag;
        }
        if ((l_flag==RXCURVE_OFF_DOMAIN_AT_START)||(l_flag==RXCURVE_OFF_DOMAIN_AT_END))
        {
            ON_3dPoint  l_p(0.0,0.0,0.0);
            ON_3dVector l_t(1.0,0.0,0.0);
            if (l_flag==RXCURVE_OFF_DOMAIN_AT_START)
            {
                if(!m_pONCurve->Ev1Der(m_pONCurve->Domain().Min(), l_p, l_t))
                    return RXCURVE_ERROR;
                if (!l_t.Unitize())
                    return RXCURVE_ERROR;
                l_point = l_p - l_t*p_offset;
            }
            if (l_flag==RXCURVE_OFF_DOMAIN_AT_END)
            {
                if(!m_pONCurve->Ev1Der(m_pONCurve->Domain().Max(), l_p, l_t))
                    return RXCURVE_ERROR;
                if (!l_t.Unitize())
                    return RXCURVE_ERROR;
                double l_len=0.0;
                m_pONCurve->GetLength(&l_len,fractional_tolerance);
                double l_off = fabs(p_offset-l_len);
                l_point = l_p + l_t*l_off;
            }
            p_v->x=l_point.x; 	p_v->y=l_point.y; 	p_v->z=l_point.z;
            return l_flag;
        }
        cout<< "PlaceON flag = "<<l_flag<<endl;
        return l_flag;
    }//if(m_pONCurve)

    double l_ds;
    float l_factor,l_f2;
    int l_c,l_k, l_k0,l_k1,l_k0found,l_k1found;
    double *l_s;
    VECTOR*l_p;

    l_k0=0;

    l_s = m_ds;
    l_c = m_c;
    l_p = m_pline;
    l_k0found = 0;
    l_k = 0;
    if(l_c > 2048 || l_c <= 0)
    {
        char l_str[64];
        sprintf(l_str,"(RXCurve::Place_On) suspicious curve npts=%d", l_c);
        this->Get_Ent()->OutputToClient(l_str,2);
        return RXCURVE_ERROR;
    }
    assert(l_c >0);
    do {
        if(l_s[l_k]>p_offset || l_k>=l_c-1)	{ l_k0found = 1; l_k0 = l_k-1;}
        if(l_k0<0) l_k0=0;
        l_k++;
    } while(!l_k0found);

    /* here k0 is ok. K1 will be k0+1 unless this is too close */
    l_k1found = 0;
    l_k1 = l_k0+1;
    do {
        l_ds = l_s[l_k1]-l_s[l_k0] ;
        if (l_ds < FLOAT_TOLERANCE) {
            if(l_k1 < l_c-1) l_k1++;
            else if (l_k0>0) l_k0--;
            else { this->Get_Ent()->OutputToClient(" short polyline",1); l_k1found = 1; }
        }
        else l_k1found =1;
    } while(!l_k1found);

    if(l_ds < FLOAT_TOLERANCE)		l_factor = (float) 0.0;
    else 							l_factor = (p_offset-l_s[l_k0])/(l_s[l_k1]-l_s[l_k0]);

    l_f2 = (float) 1.0 - l_factor;
    (*p_v).x = l_p[l_k0].x*l_f2 + l_p[l_k1].x*l_factor;
    (*p_v).y = l_p[l_k0].y*l_f2 + l_p[l_k1].y*l_factor;
    (*p_v).z = l_p[l_k0].z*l_f2 + l_p[l_k1].z*l_factor;
    return(1);
}//int RXCurve::Place_On


ON_3dPoint RXCurve::GetStartPoint()  const
{
    ON_3dPoint r(0,0,0);
    if(m_pONCurve)
    {
        const ON_Curve * l_crv = dynamic_cast<const ON_Curve*>(m_pONCurve);
        if (!l_crv)
            this->Get_Ent()->OutputToClient("m_pONCurve is not a ON_Curve object, in RXCurve::GetEndPoint() ",2);

        return  l_crv->PointAtStart();
    }
    else
    {
        if( m_pline && m_c>0) {
            //return (m_pline[0]);
            PC_Vector_Copy(m_pline[0],&r); return r;
        }
        RXSTRING buf;
        if(this->m_e) buf =_T("GetStartPoint for ent < ")+TOSTRING(this->m_e->name()) + _T(">");
        else buf=_T("GetStartPoint no ent");
        buf += this->GetTROName();
        this->Get_Ent()->OutputToClient(buf,3);
    }
    return r;
}//VECTOR RXCurve::GetStartPoint

ON_3dPoint RXCurve::GetEndPoint() const
{
    ON_3dPoint r (0,0,0);
    if(m_pONCurve)
    {
        const ON_Curve * l_crv = dynamic_cast<const ON_Curve*>(m_pONCurve);
        if (!l_crv)
            this->Get_Ent()->OutputToClient("m_pONCurve is not a ON_Curve object, in RXCurve::GetEndPoint() ",2);

        return  l_crv->PointAtEnd();
    }
    else
    {
        if( m_pline && m_c>0)  {
            //return ( m_pline[m_c-1]);
            PC_Vector_Copy( m_pline[m_c-1],&r); return r;
        }

        RXSTRING buf;
        if(this->m_e) buf=_T("GetEndPoint for ent < ") + TOSTRING(this->m_e->name());
        else buf=_T("GetEndPoint no ent");
        buf += this->GetTROName();
        this->Get_Ent()->OutputToClient(buf,3);
    }
    return r;
}//VECTOR RXCurve::GetEndPoint

int RXCurve::GetParamAt(const double & p_length, double & p_t,double p_fractional_tolerance) const
//Set p_t the parameter on the curve at p_lenght for the start of the curve

// 15 June 2005 rework so that it gives correct results when we are off the end of the curve. 
// This means reworking GetLength
/*
 return RXCURVE_IN_DOMAIN if 0<=p_length<=this->Length
 return RXCURVE_OFF_DOMAIN_AT_START if p_length<0, p_t is set to Domain.Min()
 return RXCURVE_OFF_DOMAIN_AT_END if p_length>this->Length, p_t is set to Domain.Max()
        return RXCURVE_ERROR if this->Get_Ent()->OutputToClient, do not change p_t
*/
{
    //We want to find the root of f(x) = fabs(Arclenght(t) - p_length)

    double  fractional_tolerance = p_fractional_tolerance/100.;
    if (!m_pONCurve)
    {
        this->Get_Ent()->OutputToClient(" RXCurve::GetParamAt only works if RXCurve::m_pONCurve is assigned ",4);
        return RXCURVE_ERROR;
    }

    // if p_Curve is a Polyline
    ON_3dPointArray l_pline_points;
    ON_SimpleArray<double> l_pline_t;
    if (m_pONCurve->IsPolyline(&l_pline_points,&l_pline_t))
    {
        ON_PolylineCurve l_poly;
        l_poly.m_pline = l_pline_points;
        l_poly.m_t     = l_pline_t;
        return GetParamAtPolyline(&l_poly,p_length,p_t);
    }

    ON_Curve* l_curve; //=0;
    l_curve = dynamic_cast<ON_Curve*>((ON_Curve*)m_pONCurve);
    if (!l_curve)
    {
        this->Get_Ent()->OutputToClient(" RXCurve::GetParamAt only works if RXCurve::m_pONCurve is assigned to a ON_Curve Object",1);
        return RXCURVE_ERROR;
    }

    ON_Interval l_domain = l_curve->Domain();
    double l_l1,z,l_t, tstart, l_overallLength,l_error,dsdt;
    ON_3dVector d;
    ON_3dPoint r;
    int count=0,hint=0;
    tstart = l_domain.Min();
    if (p_length<0.0)
    {
        p_t = l_domain.Min();
        return RXCURVE_OFF_DOMAIN_AT_START;
    }
    if (p_length==0)
    {
        p_t = l_domain.Min();
        return RXCURVE_IN_DOMAIN;
    }


    l_curve->GetLength(&l_l1,fractional_tolerance);// ,&l_domain);
    l_overallLength = l_l1;
    if (p_length>l_l1  - 1.0e-13)  //  fractional_tolerance Peter added the delta because
    {
        p_t = l_domain.Max();
        return RXCURVE_OFF_DOMAIN_AT_END;
    }
    if (p_length==l_l1) // comparing floats.  Very unlikely to hit.
    {
        p_t = l_domain.Max();
        return RXCURVE_IN_DOMAIN;
    }
    // peters version
    //1) estimate l_t by linear interpolation.
    //
    //2) getlength(z) from curve start to l_t
    //3) this->Get_Ent()->OutputToClient =  p_length-z  in L-space
    //4) if fabs( this->Get_Ent()->OutputToClient) < (fractional_tolerance*overallLength ) set p_t = l_t retun OK
    //5) the next step in T-space  = this->Get_Ent()->OutputToClient / mod(derivativeAt(t)
    //6)  l_t +=dt
    //7) loop to (2)  if count not exceeded


    //1) estimate l_t by linear interpolation.
    l_t = l_curve->Domain().Min() * ( 1.0 - p_length/l_overallLength )
            + l_curve->Domain().Max() * (p_length/l_overallLength);

    do {
        //2)	get Z, the length in M from curve start to parameter value l_t. But if l_t is off the end we have to do a special.
        if(l_t > l_curve->Domain().Max()) {
            //2a) z is overalllength + the excess of t x dsdt
            l_curve->Ev1Der(l_curve->Domain().Max(),r,d,0,&hint);
            dsdt= d.Length();
            dsdt = max(dsdt, 1.0e-10);
            z = l_overallLength +  (l_t- l_curve->Domain().Max()) * dsdt;
        }
        else if (l_t < l_curve->Domain().Min()) {
            //2a) z is 0 - the excess of t x dsdt
            l_curve->Ev1Der(l_curve->Domain().Min(),r,d,0,&hint);
            dsdt= d.Length();
            dsdt = max(dsdt, 1.0e-10);
            z = (l_t - l_curve->Domain().Min()) * dsdt;
        }
        else {
            l_domain[1] = l_t;
            l_curve->GetLength(&z,fractional_tolerance,&l_domain);
            l_curve->Ev1Der(l_t,r,d,0,&hint);
            dsdt= d.Length();
            dsdt = max(dsdt, 1.0e-10);
        }

        //3)	this->Get_Ent()->OutputToClient =  p_length-z  in L-space
        l_error = p_length - z;

        //4)	if fabs( this->Get_Ent()->OutputToClient) < (fractional_tolerance*overallLength ) set p_t = l_t retun OK
        if(fabs(l_error) <= fractional_tolerance*l_overallLength) {
            p_t = l_t;
            return RXCURVE_IN_DOMAIN;
        }

        //5)	the next step in T-space  = this->Get_Ent()->OutputToClient / mod(derivativeAt(t)

        //6)	l_t +=dt
        l_t += l_error / dsdt;

        //7)	loop to (2)  if count not exceeded
    }while(count++<200);

    printf("GPA NOT solved in %d  err= %.12f  Length= %.12f  z=%.12f,  target= %.12f\n",count,l_error,l_overallLength,z, p_length);


    return RXCURVE_ERROR;
    // end peters version

    l_t = l_domain.Mid(); //starting point ... the middle of the curve
    ON_Interval l_Subdomain; //(l_domain.Min(),l_t);

    //double l_Dfx = 0.0; //f'(t)
    //to compute f(t)
    //double l_Epsi = 0.0; //Step length l_Epsi(n) = -f(x(n))/f'(x(n))
    //double l_step = 0.0001;
    double l_Fact=1.0;
    int l_c2, l_loop = 0;
    //double l_oldt =l_t;
    l_Subdomain = l_domain;
    l_t = l_Subdomain.Mid();
    double l_tpreMax = l_domain.Max();
    double l_tpreMin = l_domain.Min();
    double l_LpreMin,l_LpreMax;
    l_LpreMin=0.0;
    l_curve->GetLength(&l_LpreMax,fractional_tolerance,&l_Subdomain); // this call seems wasteful

    l_Subdomain[1] = l_t;

    l_curve->GetLength(&l_l1,fractional_tolerance,&l_Subdomain);
    for (l_c2 = 0; l_c2 < 8; l_c2++) {
        l_loop=0;
        while (fabs(l_l1-p_length)>1.0e-7)
        {
            if (l_l1>p_length)
            {
                l_tpreMax= l_t;
                ON_Interval l_d = ON_Interval(l_domain.Min(),l_tpreMax);
                l_curve->GetLength(&l_LpreMax,fractional_tolerance,&l_d);
            }
            if (l_l1<p_length)
            {
                l_tpreMin = l_t;
                ON_Interval l_d = ON_Interval(l_domain.Min(),l_tpreMin);
                l_curve->GetLength(&l_LpreMin,fractional_tolerance,&l_d);
            }
            //l_t = (l_tpreMin+l_tpreMax)/2.0;   //ORDER 1
            double l_Dlmin = p_length-l_LpreMin;
            double l_Dlmax = p_length-l_LpreMax;

            assert(l_tpreMin<l_tpreMax);

            if (l_Dlmin*l_Dlmax>0)
                assert(l_Dlmin*l_Dlmax<0);
            l_Dlmin=-l_Dlmin;
            if ((fabs(l_Dlmax)<1.0e-7)||(fabs(l_Dlmin)<1.0e-7))
                l_t = (l_tpreMin+l_tpreMax)/2.0;
            else
                l_t = (l_tpreMax/l_Dlmax+l_tpreMin/l_Dlmin)/((1/l_Dlmin)+(1/l_Dlmax));   //ORDER 2

            l_Subdomain[1] = l_t;
            if (!l_curve->Domain().Includes(l_Subdomain))
                assert(l_curve->Domain().Includes(l_Subdomain));
            l_curve->GetLength(&l_l1,fractional_tolerance,&l_Subdomain);

            l_loop++;
            if (l_loop>100)
            {
                // printf("GetCurveParamAt cannot convertge after %d iterations\n",l_loop);
                break;
            }
        } // end while
        if(l_loop<=100) // we have a solution
        {
            break;
        }
        l_Fact=l_Fact/2.0; // try again
    }// end for

    p_t = min(max(l_t,l_domain.Min()),l_domain.Max());

    if (l_loop>100)
        printf("RXCurve::GetParamAt cannot converge after %d iterations\n",l_loop);
    else if(l_Fact < 1.0)
        printf ("RXCurve::GetParamAt converged after %d iterations with fac= %f\n",l_loop,l_Fact);

    //	printf("END of GetCurveParamAt in %d iterations\n",l_loop);

    if (l_curve) l_curve = NULL;

    return RXCURVE_IN_DOMAIN;
}//double RXCurve::GetParamAt

// a static for debug print of old ploylines.
int RXCurve::DumpPLine(const VECTOR*pxin,const int cxin,FILE *fp){
    int k,rc=0;


    for(k=0;k<cxin;k++)
    {
        rc+=fprintf(fp," %f \t  %f \t  %f\n", pxin[k].x ,pxin[k].y ,pxin[k].z);
    }
    return rc;

}
int RXCurve::Fill_Curve(VECTOR*pxin,int cxin)
{
    if (m_pONCurve)
    {
        if (!m_ONCurveBelongtoThis)
            m_pONCurve = NULL;
        else
            delete m_pONCurve;
    }

    m_ONCurveBelongtoThis = 1;
    m_pONCurve = new rxON_PolylineCurve(); // cant use ON in a DLL.

    int k;
    assert(!m_type);

    ON_PolylineCurve * l_pPlC = ON_PolylineCurve::Cast(m_pONCurve);
    assert(l_pPlC); //if we come here m_pONCurve must point to a ON_PolylineCurve  thomas 02 11 04
    assert(m_ONCurveBelongtoThis); //the polyline from m_p allways belongs to this  thomas 02 11 04

    l_pPlC->m_pline.Empty();
    l_pPlC->m_t.Empty();

    if (m_pline)
        RXFREE(m_pline);
    if (m_ds)
        RXFREE(m_ds);

    m_pline = (VECTOR*)MALLOC((cxin+1) *sizeof(VECTOR));
    m_ds = (double*)MALLOC((cxin+1) *sizeof(double));
    m_c	= cxin;

    //C->c_alloced= C->m_c	=CALLOC	(1,sizeof(int));
    for(k=0;k<cxin;k++)
    {
        PC_Vector_Copy(	pxin[k],&(m_pline[k]));
        l_pPlC->m_pline.AppendNew() = ON_3dPoint(m_pline[k].x,m_pline[k].y,m_pline[k].z);
        l_pPlC->m_t.AppendNew() = k;
    }

    m_ds[0]=0.0;
    for (k=1;k<cxin;k++)
    {
        m_ds[k] = m_ds[k-1] + PC_Dist(&(pxin[k-1]),&(pxin[k]));
    }
    //	m_chord = PC_Dist(pxin ,&(pxin[cxin-1]));
    //	m_arc =	m_ds[(cxin-1)];
    m_zax =	NULL;

    //Clean up
    if (l_pPlC) l_pPlC = NULL;

    return(1);
}//int RXCurve::Fill_Curve


int RXCurve::Update(VECTOR*p,const int & c)
{
    /* generates the arc-lengths and the chord
 BUT keeps pointers to the polyline, not a copy*/
    int k;
    assert(!m_type);

    if(m_pONCurve) // Doesnt do anything.
    {
#ifdef LENGTH_DEBUG
        const ON_Curve * l_crv = dynamic_cast<const ON_Curve*>(m_pONCurve);
        if (!l_crv)
            this->Get_Ent()->OutputToClient("RXCurve::m_pONCurve is not an ON_Curve object, in RXCurve::Update_Curve() ",2);
        double l_len = 0.0;
        double l_fractional_tolerance = 1.0e-8; // Peter reduced this
        l_crv->Length(&l_len,l_fractional_tolerance);	// GetLength  NURBS coding works Peter but Why the call here?
        if(l_len ==0.0) {
            cout<< " GetLength(839) gives 0!!!!!!!!!!!"<<endl;
            assert(0);
        }
#endif
        //			m_chord = ps.DistanceTo(l_crv->PointAtEnd()); // works

    }
    else {
        if(c>0) {
            m_ds = (double*)REALLOC(m_ds, (c)*sizeof(double));
            m_pline = p;
            m_c = c;
            m_ds[0]=0.0;
            for (k=1;k<c;k++)
            {
                m_ds[k] = m_ds[k-1] + PC_Dist(&(p[k-1]),&(p[k]));
            }
        }
    }

    PCX_Clear_QCD((struct PC_QCD*) m_qcd);
    m_qcd=NULL;

    return(1);
}//int RXCurve::Update


int RXCurve::Find_Derivative(const double p_offset, ON_3dPoint *p_v, ON_3dVector *p_t, const double & p_delta,double fractional_tolerance) const
//!Evaluates the curve position and derivative at p_offset 
{
    ON_3dPoint l_p;
    ON_3dVector  l_d;
    double l_t = 0.0;

    int iret;
    if(m_pONCurve)
    {
        const ON_Curve * l_crv = dynamic_cast<const ON_Curve*>(m_pONCurve);
        if (!l_crv)
            this->Get_Ent()->OutputToClient("RXCurve::m_pONCurve is not a ON_Curve object, in RXCurve::Find_Tangent() ",2);


        l_t = 0.0;
        /*
 return RXCURVE_IN_DOMAIN if 0<=p_length<=this->Length
 return RXCURVE_OFF_DOMAIN_AT_START if p_length<0, p_t is set to Domain.Min()
 return RXCURVE_OFF_DOMAIN_AT_END if p_length>this->Length, p_t is set to Domain.Max()
        return RXCURVE_ERROR if this->Get_Ent()->OutputToClient, do not change p_t
*/
        if (RXCURVE_ERROR == GetParamAt(p_offset,l_t,fractional_tolerance)) //WRONG/ changed PH 14 June was -1
            return RXCURVE_ERROR;

        iret = l_crv->Ev1Der(l_t, l_p, l_d);
        p_v->x = l_p.x; 		p_v->y = l_p.y; 		p_v->z = l_p.z;
        p_t->x = l_d.x; 		p_t->y = l_d.y; 		p_t->z = l_d.z;
        //p_t->Unitize();

        l_crv=NULL;
        return iret;
    }
    // here we have the old polyline. but lets use a method like PlaceON(about line 530)
    // to interpolate the derivative between the midpoints of the facets
    // one day, that is

    ON_3dPoint l_v1;
    int lllrc = Place_On(p_offset, p_v,fractional_tolerance);
    assert(lllrc!=RXCURVE_ERROR) ; // or else we arent trapping it
    if( lllrc)
    {
        if(p_offset+p_delta >=Get_arc()) {
            if( Place_On( p_offset-p_delta, &l_v1,fractional_tolerance))
            {
                *p_t = *p_v - l_v1;
                (*p_t)  /= p_delta; 	  // p_t->Unitize ();
                return 1;
            }

        }
        else {
            if( Place_On( p_offset+p_delta, &l_v1,fractional_tolerance))
            {
                *p_t = l_v1-*p_v;
                (*p_t)  /= p_delta;  // p_t->Unitize ();
                return 1;
            }
        }
    } // if mthe first placeOn
    cout<< "RXCurve::Find_Tangent failed"<<endl;
    return(0);
}//int RXCurve::Find_Tangent

int RXCurve::Somewhere_Near(const ON_3dPoint & t,double tol) 
{// fast check
    // method. Find the bounding box for C and add tol all round. See if v is inside it.

    const ON_Curve * l_crv = dynamic_cast<const ON_Curve*>(m_pONCurve);

    struct PC_QCD *q;
    int monotonic=0;
    if(l_crv)
    {
        int l_ret = IsNear(t,NULL,tol);
        //int l_ret = bbox.IsPointIn(t);
        assert((l_ret==RXCURVE_OK)||(l_ret==RXCURVE_FALSE));
        return l_ret;
    }

    if(!m_qcd)
        m_qcd = Find_Bounding_Box(0,m_c-1,monotonic);

    q = m_qcd;
    if(!q)
        return RXCURVE_OK; // safer to say somewhere near;

    if (t.x   > q->r + tol) return 0;  //what about Z
    if (t.x < q->l - tol) return 0;

    if (t.y > q->t + tol) return 0;
    if (t.y < q->b - tol) return 0;

    //Clean up
    if (l_crv) l_crv = NULL;
    return RXCURVE_OK;
}//int RXCurve::_Somewhere_Near ON3DPoint



int RXCurve::Somewhere_NearV(VECTOR *v,double tol) 
{// fast check
    // method. Find the bounding box for C and add tol all round. See if v is inside it.

    const ON_Curve * l_crv = dynamic_cast<const ON_Curve*>(m_pONCurve);

    struct PC_QCD *q;
    int monotonic=0;
    if(l_crv)
    {
        ON_3dPoint t = ON_3dPoint(v->x,v->y,v->z);

        int l_ret = IsNear(t,NULL,tol);
        //int l_ret = bbox.IsPointIn(t);
        assert((l_ret==RXCURVE_OK)||(l_ret==RXCURVE_FALSE));
        return l_ret;
    }

    if(!m_qcd)
        m_qcd = Find_Bounding_Box(0,m_c-1,monotonic);

    q = m_qcd;
    if(!q)
        return RXCURVE_OK; // safer to say somewhere near;

    if (v->x > q->r + tol) return 0;
    if (v->x < q->l - tol) return 0;

    if (v->y > q->t + tol) return 0;
    if (v->y < q->b - tol) return 0;

    //Clean up
    if (l_crv) l_crv = NULL;
    return RXCURVE_OK;
}//int RXCurve::PC_Somewhere_Near



struct PC_QCD *RXCurve::Find_Bounding_Box(int i1,int i2,int monotonic) const
{
    //scans c between i1 and i2 INCLUSIVE,
    // fills in q
    // if monotonic just look at the end points.
    //  else look at them all
    // if it finds this section to be monotonic, it returns 1

    float x,y,xl,yl;
    int i, xup=0,yup=0,xdown=0,ydown=0;

    struct PC_QCD *q=(PC_QCD*)CALLOC(1,sizeof(struct PC_QCD));// sometimes doesnt get freed LEAK

    if (m_pONCurve)
    {
        const ON_Curve * l_crv = dynamic_cast<const ON_Curve*>(m_pONCurve);
        assert(l_crv);
        rxON_BoundingBox l_BB = l_crv->BoundingBox();
        ON_3dPoint l_ptStart = l_crv->PointAtStart();
        ON_3dPoint l_ptEnd = l_crv->PointAtEnd();
        //	q->l=q->r= l_ptStart.x;  not needed  //m_p[i1].x;
        //	q->t=q->b= l_ptStart.y; not needed//m_p[i1].y;

        if(monotonic){
            assert(0);  /// havent debugged
            x =q->r=l_ptEnd.x; //m_p[i2].x;
            y =q->b=l_ptEnd.y; //m_p[i2].y;
            if(x>=q->l)
                q->r=x;
            else
                q->l=x;

            if(y>=q->b)
                q->t=y;
            else
                q->b=y;
        }
        else  {
            q->l= l_BB.Min().x;
            q->r= l_BB.Max().x;
            q->t= l_BB.Max().y;
            q->b= l_BB.Min().y;

            /*//commeneted out by thomas 11 08 04
   xl=q->l; yl=q->b;
   for(i=i1+1;i<=i2;i++) {
    x=m_p[i].x; y=m_p[i].y;
    if(x>xl)
     xup=1;
    if(x>q->r)
     q->r=x;

    if(x < xl)
     xdown=1;
    if(x < q->l)
     q->l=x;

    if(y > yl)
     yup=1;
    if(y > q->t)
     q->t=y;

    if(y< yl)
     ydown=1;
    if(y < q->b)
     q->b=y;

    xl=x; yl=y;
   } //commeneted out by thomas 11 08 04 */
            //commeneted out by thomas 11 08 04 monotonic=1;
            //commeneted out by thomas 11 08 04 	if((xup && xdown)  || (yup && ydown))
            monotonic=0;

        }
        q->monotonic= monotonic;
        q->Curve=(RXCurve*)this;
        q->i1=i1;
        q->i2=i2;
        return q;


        //this->Get_Ent()->OutputToClient("RXCurve::Find_Bounding_Box,  this function is not design for ON_curve yet ",2);
    }//if (m_pONCurve)


    assert(i1<=i2);
    assert(i2 < m_c);

    assert(!m_pONCurve);

    q->l=q->r=m_pline[i1].x;
    q->t=q->b=m_pline[i1].y;

    if(monotonic){
        x =q->r=m_pline[i2].x;
        y =q->b=m_pline[i2].y;
        if(x>=q->l)
            q->r=x;
        else
            q->l=x;

        if(y>=q->b)
            q->t=y;
        else
            q->b=y;
    }
    else  {
        xl=q->l; yl=q->b;
        for(i=i1+1;i<=i2;i++) {
            x=m_pline[i].x; y=m_pline[i].y;
            if(x>xl)
                xup=1;
            if(x>q->r)
                q->r=x;

            if(x < xl)
                xdown=1;
            if(x < q->l)
                q->l=x;

            if(y > yl)
                yup=1;
            if(y > q->t)
                q->t=y;

            if(y< yl)
                ydown=1;
            if(y < q->b)
                q->b=y;

            xl=x; yl=y;
        }
        monotonic=1;
        if((xup && xdown)  || (yup && ydown))
            monotonic=0;

    }
    q->monotonic= monotonic;
    q->Curve=(RXCurve*)this;
    q->i1=i1;
    q->i2=i2;
    return q;
}//struct PC_QCD *RXCurve::Find_Bounding_Box



int RXCurve::Curves_Coincident(RXCurve *p_c2) const 
{ //NOT SYMMETRICAL
    /* test at up to 9 points on c1.  For each point, drop a perp to c2.
 If the distance between where we are and the dropped point is > g_SOLVE_TOLERANCE,
 the curves arent coincident.
 Else they might still be */
    ON_3dPoint v2; ON_3dVector t;
    ON_3dPoint v1;
    double s1,s2,dist=0.0, d_xy;
    assert(!m_type && ! p_c2->GetType());

    if ((m_pONCurve)&&(p_c2->GetONCurve()))
    {
        rxON_BoundingBox l_bb1 =  m_pONCurve->BoundingBox();
        rxON_BoundingBox l_bb2 =  p_c2->GetONCurve()->BoundingBox();
        rxON_BoundingBox l_temp = l_bb1;
        if (!l_temp.Intersection(l_bb2))
            return 0;
        l_temp = l_bb1;
        if (!l_temp.Includes(l_bb2))
        {
            l_temp = l_bb2;
            if (!l_temp.Includes(l_bb1))
                return 0;
        }
    }

    double l_fractional_tolerance = 1.0e-5;

    double l_arc1 = Get_arc(l_fractional_tolerance);
    double l_arc2 = p_c2->Get_arc(l_fractional_tolerance);


    for(s1=0.0; s1<l_arc1; s1 += (l_arc1/8.0)) {

        Place_On(s1,&v1);
        s2=s1;
        if( 1 != p_c2->Drop_To_Curve(v1,&v2, &t, m_e->Esail ->m_Linear_Tol ,&s2,&dist))
            return 0;
        d_xy = sqrt((v1.x-v2.x) * (v1.x-v2.x) + (v1.y-v2.y) *(v1.y-v2.y));
        if(d_xy > m_e->Esail ->m_Linear_Tol )
            return 0;
        if(s2 < (- m_e->Esail ->m_Linear_Tol ))
            return 0;
        if(s2 > l_arc2+ m_e->Esail ->m_Linear_Tol )
            return 0;
    }
    return 1;
}//int RXCurve::Curves_Coincident

int RXCurve::Curve_To_Line_Intersection(double*p_s1,
                                        ON_3dPoint *p_v1, ON_3dPoint*p_v2,
                                        ON_3dPoint *p_a,  ON_3dPoint *p_b,
                                        double*p_t2,int*p_indexout,
                                        int p_flags) const
{
    /*
 SPECIAL for Compute_Radial_Field.
  Find an intersection between C1 and the line segment from a to b
  Return PCC_IFOUND_OK if any intersection is found.
  Extensions are searched too, depending on flags
otherwise  0

  An intersection is defined as a line normal to both curves,
   between	a point v1 at s1 (arclength) on C1
   and		a point v2 at t2 (parameter) on  (a to b)


 */// options are  (EXT_1_L+EXT_1_R+EXT_2_L+EXT_2_R)

    //	int stop;
    //if (!this)
    // stop= p_flags+4;

    int fl=0, flag_S,flag_M, flag_E; //gcc
    int l_c1, rc=0;
    VECTOR *l_a1,*l_b1;
    double l_t1;
    int  i;

    flag_S=p_flags;	if(flag_S & EXT_1_R) flag_S -= EXT_1_R;
    flag_M=p_flags;	if(flag_M & EXT_1_L) flag_M -= EXT_1_L;
    if(flag_M & EXT_1_R) flag_M -= EXT_1_R;
    flag_E=p_flags;	if(flag_E & EXT_1_L) flag_E -= EXT_1_L;

    if(m_pONCurve)
    { // else if NULL carry on with the last one.
        ON_Curve * l_crv = dynamic_cast<ON_Curve*>(m_pONCurve);
        if (!l_crv)
        {
            this->Get_Ent()->OutputToClient("RXCurve::m_pONCurve is not a ON_Curve object, in RXCurve::Curve_To_Line_Intersection() ",2);
            return 0;
        }

        //We are using a generc interesection algorithm
        //To define a Curve from the line
        ON_3dPoint l_a =ON_3dPoint(p_a->x,p_a->y,p_a->z);
        ON_3dPoint l_b = ON_3dPoint(p_b->x,p_b->y,p_b->z);
        rxON_LineCurve l_line(l_a,l_b);

        double radius =0;
        l_line.GetLength (&radius);


        //		ON_ClassArray<RXCrossing2> l_Intersections;

        rc = 0;

        ON_2dPointArray results;

        double SolveTol = 0.001;
        ON_Interval d2; // the interval for the line.
        double t0,t1;
        t0 = l_line.Domain().Min();
        t1 = l_line.Domain().Max(); // ON makes this numerically= the length??
        rxON_BoundingBox bb = l_crv->BoundingBox();
        t1 = max(t1,l_a.DistanceTo (bb.FarPoint(l_a))); // UNITS!!!!!!!!!!
        d2=ON_Interval(t0,t1*2.0); // nice big interval to be sure

        rc = ONCurveIntersect(l_crv ,(ON_Curve*) (&l_line),results,t1*2.0,SolveTol,NULL, &d2); // this routine doesnt extend the curves.  We need to have already done this.
        // also we need to accept a big separation at the intersection.
        //what return value is good???????
        int is_odd =  rc && !(rc&2)	;
        if (is_odd)
        {
            int k;
            assert(results.Count());
            double dfx = results.At(0)->y ;
            // working on  l_line, Identify the first crossing  at l_line.PointAt(lowest results.y)
            for(k=1;k<results.Count(); k++) {
                if(results.At(k)->y < dfx)
                    dfx = results.At(k)->y;
            }
            dfx= l_a.DistanceTo(l_line.PointAt(dfx));
            // the ratio t = (radius) /( dist from focus to the first crossing)
            *p_t2 = radius/dfx;
        }
        return rc;

    } // if ONCurve
    else
    {
        assert(this);
        i=0;
        l_b1 = l_a1 =  Get_p();
        l_b1++;
        fl=flag_S;
        if (!Get_c())
            return 0;
        l_c1 = Get_c();

        if(l_c1==2)
            flag_E=p_flags;	// the other flag_xx dont matter

        for(;i<l_c1-1;i++,l_a1++,l_b1++) {
            if(i == l_c1-2) fl = flag_E;  // May 2003 f1 is ANDed with EXT_2D or EXT_3D
            rc = Segment_IntersectWithExtensions(l_a1,l_b1,p_a,p_b,&l_t1,p_t2,fl);
            fl = flag_M;
            switch (rc) {
            case PCC_SMALL_ANG:
            case PCC_PARALLEL :
            case PCC_OUT_OF_RANGE :
                break;

            case PCC_OK :{
                /* 	the lines arent parallel. t1 and t2 are parametric distances
     in the first segment we permit s1 to be -anything
     in the last we permit it to be + anything
     otherwise we allow it to be between 0 and 1, including 0 but not 1
     */
                *p_s1 = Get_ds()[i+1]* (l_t1)  + Get_ds()[i] * (1.0- (l_t1));
                /* convert to length units */
                p_v1->x = l_b1->x* (float)(l_t1)  + l_a1->x * (float)(1.0- (l_t1));
                p_v1->y = l_b1->y* (float)(l_t1)  + l_a1->y * (float) (1.0- (l_t1));
                p_v1->z = l_b1->z* (float)(l_t1)  + l_a1->z * (float)(1.0- (l_t1));
                p_v2->x = p_b->x  * (float)(*p_t2)  + p_a->x  * (float)(1.0- (*p_t2));
                p_v2->y = p_b->y  * (float)(*p_t2)  + p_a->y  * (float)(1.0- (*p_t2));
                p_v2->z = p_b->z  * (float)(*p_t2)  + p_a->z  * (float)(1.0- (*p_t2));
                *p_indexout=i;
                i++;
                l_a1++;
                l_b1++;
                return (rc);
            }
            default:
            {
                printf(" Segment_IntersectWithExtensions gave %d\n",rc);
                this->Get_Ent()->OutputToClient(" Segment Intersect!!!",4);
                assert(0);
                break;
            }
            } // end switch

        }
    }
    *p_indexout=-1;
    return 0;

}//int RXCurve::Curve_To_Line_Intersection

const ON_Curve * RXCurve::CheckONCurve(const ON_Curve *p_crv,char *p_text) const
{
    const ON_Curve * l_crv = dynamic_cast<const ON_Curve*>(p_crv);

    if (!l_crv)
        return NULL;

    int i;
    double l_len=0.0;
    double l_Ctol = m_e->Esail->m_Cut_Dist ;
    //Check if the curve is UNIFORM
    if (l_crv->IsPeriodic())
        this->Get_Ent()->OutputToClient("Relax Does not accept periodic curve!",3);
    if (l_crv->IsPeriodic())
        this->Get_Ent()->OutputToClient("Relax Does not accept periodic curve!",3);

    // if p_Curve is a Polyline
    ON_3dPointArray l_pline_points;
    ON_SimpleArray<double> l_pline_t;
    if (p_crv->IsPolyline(&l_pline_points,&l_pline_t))
    {
        ON_PolylineCurve l_poly;
        l_poly.m_pline = l_pline_points;
        l_poly.m_t     = l_pline_t;
        l_poly.GetLength(&l_len);
        if (l_len<2*l_Ctol*1.05) {
            char str[256];
            ON_3dPoint l_p = p_crv->PointAtStart();
            sprintf(str," (CheckONCurve) <%s>, a Polyline. Length %f <  2*g_Gauss_Cut_Dist*1.05  ",p_text,l_len);
            PC_Insert_Arrow(l_p,45.0,str);
            this->Get_Ent()->OutputToClient(str,1);
        }
        if (l_crv) l_crv = NULL;
        return p_crv;
    }

    double l_fractional_tolerance = 1.0e-8;  // Peter reduced

    //If the curve is a polycurve  we check it to make sure it's continuous
    const ON_PolyCurve * l_PolyCrv  = ON_PolyCurve::Cast(p_crv);
    //const ON_PolyCurve * l_PolyCrv = ON_PolyCurve:Cast(l_crv);
    if (l_PolyCrv)
    {
        ON_Interval l_domain = l_PolyCrv->Domain();
        double l_t = -1000;

        //All the curves needs to be longer than 2*g_Gauss_Cut_Dist*1.05

        ON_3dPoint l_PtStartPoly = l_PolyCrv->PointAtStart();
        ON_3dPoint l_PtStart = l_PolyCrv->operator [] (0)->PointAtStart();
        ON_3dPoint l_PtEnd = l_PolyCrv->operator [] (0)->PointAtEnd();

        if (l_PtStartPoly==l_PtStart)
            //int otot = 0; //l_PolyCrv->operator [](i)->Reverse();


            for (i=0;i<l_PolyCrv->Count();i++)
            {
                l_PolyCrv->operator [](i)->GetLength(&l_len,l_fractional_tolerance);
                if (l_len<2*l_Ctol*1.05){
                    char str[256];
                    ON_3dPoint l_p = 	l_PolyCrv->operator [](i)->PointAtStart();
                    sprintf(str," (CheckONCurve) '%s' One of the curves in a polycurve is shorter than 2*g_Gauss_Cut_Dist*1.05",p_text);
                    PC_Insert_Arrow(l_p,45.0,str);
                    this->Get_Ent()->OutputToClient(str,1);

                    //to flip the curve which ar ein the wrong direction
                }
            }
        if (l_PolyCrv) l_PolyCrv = NULL;

        return p_crv;
    }

    ON_NurbsCurve * l_nurbs  = l_crv->NurbsCurve();
    if (l_nurbs)
    {
        //If the curve is NOT a polycurve  we do not accept any discontunuity
        // and we perform a quick check looking at the co,ntrol vertices

        int l_CvCount = l_nurbs->CVCount();
        int l_degree = l_nurbs->Degree();
        int id;
        id = 0;
        ON_3dPoint l_prev = l_nurbs->CV(id);
        ON_3dPoint l_cv;
        int l_nCoincident = 1;
        for (i=1;i<l_CvCount;i++)
        {
            l_cv = l_nurbs->CV(i);

            if (l_cv.DistanceTo(l_prev)<m_e->Esail->m_Linear_Tol /10.0)
            {
                l_nCoincident++;
                if (l_nCoincident==l_degree)
                {
                    char str[256];
                    sprintf(str,"(CheckONCurve) ADVISORY SC '%s' NURBS curve not continuous in tangent\n Split it into 2 curves and join these curves in a Polycurve Object (%f %f %f)",p_text, l_cv.x, l_cv.y,l_cv.z );
                    PC_Insert_Arrow(l_cv,45.00,str);
                    this->Get_Ent()->OutputToClient(str,1);
                }
            }
            else
                l_nCoincident = 1;
            l_prev = l_cv;
        }
        //check if the knot vector is uniform
        int l_knotstyle = ON_KnotVectorStyle(l_nurbs->Order(),l_CvCount,l_nurbs->Knot());
        if ( l_knotstyle > 3 )
        {
            //			this->Get_Ent()->OutputToClient("The current NURBS curve is not a uniform NURBS curve, you should make it uniform",1);
        }

        l_nurbs->GetLength(&l_len,l_fractional_tolerance);
        // try GetLength
        bool l_ok = l_nurbs->GetLength(&l_len);
        if (l_len<2*l_Ctol*1.05) {

            char str[256];
            ON_3dPoint l_p = l_nurbs->PointAtStart();
            sprintf(str,"(CheckONCurve) SC '%s',a NURBS curve\n is shorter than limit %f",p_text, 2*g_Gauss_Cut_Dist*1.05  );
            PC_Insert_Arrow(l_p,45.0,str);
            this->Get_Ent()->OutputToClient(str,1);
        }
        l_nurbs->Destroy();
        delete l_nurbs;
        l_nurbs =  NULL;

        return p_crv;
    }//if (l_nubrsCrv)
    else
    {
        this->Get_Ent()->OutputToClient("Cannot cast the pointer to any ON_Curve object compatible with Relax",3);
        return NULL;
    }

}//ON_Curve * RXCurve::CheckONCurve

int RXCurve::Curve_Compare(const RXCurve & p_curve, const double & p_tol) const
{ /* return 1 if different 
 Perform a fast check on each CV if this as a ONCurve */

    double tolsq = p_tol*p_tol;
    if ((m_pONCurve)&&(!(p_curve.m_pONCurve)))
        return 1;

    if ((!m_pONCurve)&&(p_curve.m_pONCurve))
        return 1;

    if ((m_pONCurve)&&(p_curve.m_pONCurve))
    {
        const ON_Curve * l_crv1 = dynamic_cast<const ON_Curve*>(m_pONCurve);
        const ON_Curve * l_crv2 = dynamic_cast<const ON_Curve*>(p_curve.m_pONCurve);
        assert (l_crv1&&l_crv2);

        ON_NurbsCurve	l_nurbs1;
        ON_NurbsCurve	l_nurbs2;
        if (!l_crv1->GetNurbForm(l_nurbs1))
            this->Get_Ent()->OutputToClient("(1778)cannot getr the nurbs form of an ON_Curve",2);
        if (!l_crv2->GetNurbForm(l_nurbs2))
            this->Get_Ent()->OutputToClient("(1780)cannot getr the nurbs form of an ON_Curve",2);


        int l_CVnb1,l_CVnb2;
        l_CVnb1 = l_nurbs1.CVCount();
        l_CVnb2 = l_nurbs2.CVCount();

        if (l_CVnb1!=l_CVnb2){ // ??need to call Destroy()??
            l_nurbs1.Destroy();
            l_nurbs2.Destroy();
            return 1;
        }
        int i;
        ON_3dPoint l_Pt1(0.0,0.0,0.0);
        ON_3dPoint l_Pt2(0.0,0.0,0.0);
        for (i=0;i<l_CVnb1;i++)
        {
            l_nurbs1.GetCV(i,l_Pt1);
            l_nurbs2.GetCV(i,l_Pt2);

            if ((l_Pt1-l_Pt2).LengthSquared()  > tolsq)
            {
                l_nurbs1.Destroy();
                l_nurbs2.Destroy();

                return 1;
            }
        }
        l_nurbs1.Destroy();
        l_nurbs2.Destroy();
        return 0;
    }
    else
    {
        return PC_Polyline_Compare(m_pline, m_c, p_curve.m_pline, p_curve.m_c, p_tol);
    }

    //	return 0;
}//int RXCurve::Curve_Compare

int RXCurve::HasGeometry() const
{
    if (m_c>1)
    {
        return RXCURVE_GEOM_POLYLINE;
    }
    else
    {
        if (m_pONCurve)
            return RXCURVE_GEOM_ONCURVE;
        else
            return RXCURVE_FALSE;
    }
}//int RXCurve::HasGeometry

int RXCurve::Draw(RXGRAPHICSEGMENT n, const char*color) const
{
    int l_geom = HasGeometry();

    switch (l_geom) {
    case RXCURVE_GEOM_ONCURVE :
    {
#ifdef HOOPS
        if(0==ONCurveDrawHoops(m_pONCurve,"rxcurves")){
            string buf(" A curve cant be drawn - probably too many coincident CVs: ");
            buf+= this->m_e->name();
            this->Get_Ent()->OutputToClient(buf.c_str(),1);
        }
#elif defined (RXQT)
        // see if theres a SO draw NUrBS
        class ON_NurbsCurve c;
        if(this->m_pONCurve->GetNurbForm(c)){
            RX_Insert_NurbsCurve(&c,n,color) ;
            //        return 1;
        }
        //    return 0;
#endif
        break;
        int l_n;
        VECTOR * l_p = NULL;

        int i;
        // if p_Curve is a Polyline
        ON_3dPointArray l_pline_points;
        ON_SimpleArray<double> l_pline_t;
        if (m_pONCurve->IsPolyline(&l_pline_points,&l_pline_t))
        {
            ON_PolylineCurve l_poly;
            l_poly.m_pline = l_pline_points;
            l_poly.m_t     = l_pline_t;

            l_n = l_poly.m_pline.Count();  //number of point

            l_p = (VECTOR *)MALLOC( (l_n) *sizeof(VECTOR));

            for (i=0;i<l_n;i++)
            {
                l_p[i].x = l_poly.m_pline[i].x;
                l_p[i].y = l_poly.m_pline[i].y;
                l_p[i].z = l_poly.m_pline[i].z;
            }
            RX_Insert_Polyline(l_n,&(l_p->x),n,color);
            if (l_p)
            {RXFREE(l_p); l_p=NULL;}
            return RXCURVE_OK;
        }
        //for now we are inserting a polyline frfom sampling the ON_Curve
        l_n = 100;
        l_p = (VECTOR *)MALLOC( (l_n) *sizeof(VECTOR));

        ON_Interval l_domain = m_pONCurve->Domain();
        double l_u=l_domain.Min();

        double l_e = 1.0;
        l_e = l_domain.Length()/((double) (l_n -1));
        ON_3dPoint l_pt(0.0,0.0,0.0);

        for(i=0;i<l_n-1;i++)
        {
            if (m_pONCurve->EvPoint(l_u,l_pt))
            {
                //Evaluate_NURBS_By_N(n, u , v,&((*poly)[i]));
                l_p[i].x = l_pt.x; //    poly[i]->x = l_pt.x;
                l_p[i].y = l_pt.y;
                l_p[i].z = l_pt.z;
                l_u+=l_e;
            }
        }
        if (m_pONCurve->EvPoint(l_domain.Max(),l_pt))  // To prevent of numerical error
        {
            l_p[l_n-1].x = l_pt.x;
            l_p[l_n-1].y = l_pt.y;
            l_p[l_n-1].z = l_pt.z;
        }

        RX_Insert_Polyline(l_n,&(l_p->x), Get_Ent()->GNode(),color  );
        if (l_p)
        {RXFREE(l_p); l_p=NULL;}
        break;
    }
    case RXCURVE_GEOM_POLYLINE :
    {
        RX_Insert_Polyline(m_c,&(m_pline->x), Get_Ent()->GNode(),color );
        break;
    }
    case RXCURVE_ERROR :
    {
        return RXCURVE_ERROR;
    }
    }
    return RXCURVE_OK;
}//int RXCurve::Draw


int RXCurve::IsNear(const ON_3dPoint &p_pt, ON_Curve * p_crv, const double p_tol) const
{
    if (!m_pONCurve)
    {
        //	VECTOR l_v;
        //	l_v.x = p_pt.x;l_v.y = p_pt.y;l_v.z = p_pt.z;
        RXCurve l_curve(*this);
        return l_curve.Somewhere_Near(p_pt,p_tol);
    }

    ON_3dPoint l_pt1, l_pt2;
    ON_3dPoint l_ptmin, l_ptmax;
    rxON_BoundingBox l_BB;
    if (p_crv)
    {
        if (!p_crv->GetBoundingBox(l_BB))
            p_crv->GetBoundingBox(l_BB);
    }
    else
    {
        if (!m_pONCurve->GetBoundingBox(l_BB))
            m_pONCurve->GetBoundingBox(l_BB);
    }

    double l_diag = l_BB.Diagonal().Length();

    ON_3dPoint l_pttol(p_tol,p_tol,p_tol);

    if (l_diag<p_tol)
    {

        l_BB.m_min = l_BB.Min()-l_pttol;
        l_BB.m_max = l_BB.Max()+l_pttol;
        if (l_BB.IsPointIn(p_pt))
            return RXCURVE_OK;
        else
            return RXCURVE_FALSE;
    }

    l_BB.m_min = l_BB.Min()-l_pttol;
    l_BB.m_max = l_BB.Max()+l_pttol;

    double l_dist = p_pt.DistanceTo(l_BB.Max()); // this is wierd
    if (l_dist<p_tol)
        return RXCURVE_OK;
    l_dist = p_pt.DistanceTo(l_BB.Min());
    if (l_dist<p_tol)
        return RXCURVE_OK;


    if (l_BB.IsPointIn(p_pt))
    {
        //to split the curve 2
        //ON_NurbsCurve l_crv21,l_crv22;
        ON_Curve * l_pcrv1,*l_pcrv2;
        l_pcrv1 = NULL;
        l_pcrv2 = NULL;
        if (p_crv)
        {
            if ( !p_crv->Split( p_crv->Domain().Mid(), l_pcrv1,l_pcrv2) )
                return false;
        }
        else
        {
            if ( !m_pONCurve->Split( m_pONCurve->Domain().Mid(), l_pcrv1,l_pcrv2) )
                return false;
        }

        assert(l_pcrv1&&l_pcrv2);

        int l_ret = RXCURVE_FALSE;
        l_ret = IsNear(p_pt,l_pcrv1,p_tol);
        if (l_ret==RXCURVE_OK)
        {
            if (l_pcrv1) l_pcrv1 = NULL;
            if (l_pcrv2) l_pcrv2 = NULL;
            return RXCURVE_OK;
        }
        l_ret = IsNear(p_pt,l_pcrv2,p_tol);
        if (l_ret==RXCURVE_OK)
        {
            if (l_pcrv1) l_pcrv1 = NULL;
            if (l_pcrv2) l_pcrv2 = NULL;
            return RXCURVE_OK;
        }
        else
        {
            if (l_pcrv1) l_pcrv1 = NULL;
            if (l_pcrv2) l_pcrv2 = NULL;
            return RXCURVE_FALSE;
        }
    }
    else
        return RXCURVE_FALSE;
}//int RXCurve::IsInBB

int RXCurve::Find_Tangent(const double p_offset,  ON_3dPoint *p_v, ON_3dVector *p_t, const double & p_delta) const
{	int rc = Find_Derivative( p_offset, p_v, p_t,p_delta,1.0e-8); 
        p_t->Unitize ();
            return rc;
}

//int RXCurve::Find_Tangent( double p_offset, VECTOR*p_v, VECTOR *p_t) const
//{		return Find_Tangent( p_offset, p_v, p_t,.001); }//int RXCurve::Find_Tangent

int RXCurve::Place_On(const double & p_offset, ON_3dPoint* p_v) const
{
#ifdef _DEBUG
    assert(p_v);
#endif //
    return Place_On(p_offset, p_v, 1.0e-7);
}

int RXCurve::Drop_To_Curve(const ON_3dPoint p_A,ON_3dPoint *p_r, ON_3dVector*p_t,double p_tol,double*p_ss,double*p_dist,double fractional_tolerance) const
{
    // return values.  -1 error  1 converged, 0 unconverged
    /*       p_a is the point in question  (INPUT)
   p_r is the point on the curve    (OUTPUT)
   p_t tangent vestor at  where the point is dropped (unitvector)(OUTPUT)

   p_tol is used twice -
           its used as the step length along the polyline for each iteration
     its also used to stop iteration (s change is less than tol) (INPUT)

         p_ss is initially the length along the curve to start
        is set to the length along the curve where the perpendicular hits (INPUT and OUTPUT)
   p_dist is the perpendicular distance.   (OUTPUT)

   try s = s as initial length along polyline
   find tangent at s
     find location (r) of perpendicular between a and poly
   measure distance along this tangent then add this distance to s
   increment s by ((a-r) dot t)
   loop if inc > tol
   dist is (a-r) cross t
   TOL has a double purpose. Delta for tangent calc, and convergence limit
  */

    ON_3dVector ra,rat;
    double inc, relfac=1.0;
    int count, c_outer=6;

    assert(!m_type);

    if (m_pONCurve)
    {
        const ON_Curve * l_pCrv = dynamic_cast<const ON_Curve*>(m_pONCurve);
        assert (l_pCrv);

        ON_3dPoint l_pt(p_A.x,p_A.y,p_A.z);
        double l_tt = 0.0;
        int rc = GetParamAt(*p_ss,l_tt,fractional_tolerance);
#ifdef _DEBUG
        switch (rc) {
        case RXCURVE_IN_DOMAIN :
            break;
        case  RXCURVE_OFF_DOMAIN_AT_START :
            break;
        case RXCURVE_OFF_DOMAIN_AT_END :
            break;
        case RXCURVE_ERROR:
            return -1;
        default:
            assert(0);
        }
#endif
        if(rc==RXCURVE_ERROR)
            return -1;
        double maxdist=0.0;
        maxdist = p_tol;

        if(!m_pONCurve->GetClosestPoint(l_pt,&l_tt,maxdist))  // peter changed last arg from p_tol so it works even if it's far away
            return -1;
        //			now fill in the other stuff to return.
        //			*p_ss is the length along the curve from the start
        //			p_r is the point on the curve    (OUTPUT)
        //			p_t tangent vector at  where the point is dropped (unitvector)(OUTPUT)
        //			p_dist is the perpendicular distance.   (OUTPUT)

        //p_ss
        ON_Interval l_dom = ON_Interval(m_pONCurve->Domain().Min(),l_tt);
        m_pONCurve->GetLength (p_ss,fractional_tolerance,&l_dom);
        //p_r
        ON_3dPoint l_r = m_pONCurve->PointAt (l_tt);
        p_r->x = l_r.x; 		p_r->y = l_r.y; 		p_r->z = l_r.z;
        //p_t
        ON_3dVector l_vt = m_pONCurve->TangentAt(l_tt);
        p_t->x = l_vt.x; 		p_t->y = l_vt.y; 		p_t->z = l_vt.z;
        //p_dist
        *p_dist = l_r.DistanceTo (l_pt);
        return 1;

        //Setting the starting point on the curve
        double l_t = 0.0;
        rc = GetParamAt(*p_ss,l_t,fractional_tolerance);
#ifdef _DEBUG
        switch (rc) {
        case RXCURVE_IN_DOMAIN :
            break;
        case  RXCURVE_OFF_DOMAIN_AT_START :
            break;
        case RXCURVE_OFF_DOMAIN_AT_END :
            break;
        case RXCURVE_ERROR:
            return -1;
        default:
            assert(0);
        }
#endif
        if(rc==RXCURVE_ERROR)
            return -1;

        ON_Interval l_Domain = l_pCrv->Domain();

        assert(l_Domain.Includes(l_t));

        double l_d0 = l_pt.DistanceTo(l_pCrv->PointAt(l_t));
        double l_d1 = l_pt.DistanceTo(l_pCrv->PointAtStart());
        if (l_d1<l_d0)
        {	l_t = l_pCrv->Domain().Min(); l_d0=l_d1;}
        l_d1 = l_pt.DistanceTo(l_pCrv->PointAtEnd());
        if (l_d1<l_d0)
        {	l_t = l_pCrv->Domain().Max();	l_d0 = l_d1;}
        l_d1 = l_pt.DistanceTo(l_pCrv->PointAt(l_pCrv->Domain().Mid()));
        if (l_d1<l_d0)
        {	l_t = l_pCrv->Domain().Mid();	l_d0 = l_d1;}
        //END Setting the starting point on the curve to either start mid or end


        RXUPoint l_PointOUT;
        l_PointOUT.Init();
        double l_eEnd = 1.0e-3;
        //DEBUg
        const RXCurve * l_crv = RXCurve::Cast(this);
        char *l_classID = this->GetClassID();

        if (l_crv) l_crv = NULL;
        //END DEBUG
        int l_ret = DropToCurve(l_pCrv,  // Curve to project the point on
                                l_pt,  // Point to project
                                &l_t,      // Starting point
                                l_PointOUT,   //Result
                                p_tol,      // accuracy criteria
                                l_eEnd,
                                100,
                                m_e->Esail ->m_Linear_Tol   ,
                                m_e->Esail ->m_Cut_Dist   ,
                                this);

        double l_fractional_tolerance = 1.0e-8;
        double l_newt = l_PointOUT.Gett();
        assert(!_isnan(l_newt));
        l_newt = min(max(l_newt,l_Domain.Min()),l_Domain.Max());

        assert(l_newt>=l_Domain.Min()&&(l_newt<=l_Domain.Max()));

        ON_Interval l_domain_lwrcase(m_pONCurve->Domain().Min(),l_PointOUT.Gett()); // TODO often NAN
        m_pONCurve->GetLength(p_ss,l_fractional_tolerance,&l_domain_lwrcase);

        ON_3dPoint l_ptDropped = l_PointOUT.GetPos();
        ON_3dVector l_tan = l_PointOUT.GetTangent();
        l_tan.Unitize();

        p_r->x = l_ptDropped.x;p_r->y=l_ptDropped.y;p_r->z=l_ptDropped.z;
        p_t->x = l_tan.x;p_t->y=l_tan.y;p_t->z=l_tan.z;

        *p_dist = l_pt.DistanceTo(l_ptDropped);

        if (l_ret == RX_DROP_NOT_CONVERGED)
        {
            char l_buff[200];
            sprintf(l_buff,"GetNearestPoint failled to converge, curves.cpp in PC_Drop_To_Curve(), Distance %f",*p_dist);
            g_World->OutputToClient(l_buff,1);
            //int l_ret = // avoide the risk of it being optimised away.
            DropToCurve(l_pCrv,  // Curve to project the point on
                        l_pt,  // Point to project
                        &l_t,      // To transform a length in parameter crappy method!!
                        l_PointOUT,
                        p_tol,      // accuracy criteria
                        l_eEnd,
                        100,
                        m_e->Esail ->m_Linear_Tol ,
                        m_e->Esail->m_Cut_Dist ,
                        this);
            if (l_pCrv) l_pCrv = NULL;
            l_ret = 0;
        }
        else
            l_ret = RX_DROP_OK;
        return l_ret;
    }//if (m_pONCurve)

    // start here if no ONCurve
    // this bounces on polylines where the closest point is a vertex
    // two solutions:
    // 1)  do a Brent, minimising the distance rather than looking for a perpendicular.
    //		 but what do we return for tangent?
    // 2)   rewrite Find_Derivative so it is more like Place_On and interpolates the tangent between
    //		midpoints of the polyline segments.

    double LastInc=ON_DBL_MAX;
#ifdef FULLDEBUG
    std::list<double> trace;
#endif
    if(!Find_Derivative(*p_ss,p_r,p_t,.001,1.0e-8))
        assert("FCT (1315)"==0);

    ra = p_A- (*p_r);
    do{
        count=50;
#ifdef FULLDEBUG
        trace.clear();
#endif

        do {
            inc = ON_DotProduct(*p_t, ra)/ p_t->LengthSquared();
            LastInc = inc * relfac ;
            *p_ss = *p_ss + LastInc;
#ifdef FULLDEBUG
            trace.push_back (LastInc);
#endif
            if(!Find_Derivative(*p_ss,p_r,p_t,.001,1.0e-8)) assert("FCT(1337)"==0);
            ra = p_A - (*p_r);
        }
        while(fabs(inc)>p_tol&&count--);

#ifdef FULLDEBUG
        if( fabs(inc)>p_tol) {
            printf("Perpendicular out by %f last= %f tol=%f relfac=%f \n", inc,LastInc,p_tol,relfac);
            for(std::list<double>::const_iterator it=trace.begin();it!=trace.end(); it++)
                cout<< *it<<" ";
            cout<<endl;
        }
#endif
        relfac = relfac/2.0;
    } while ( fabs(inc)>p_tol&&c_outer--);

    rat = ON_CrossProduct(ra,*p_t);
    *p_dist = rat.Length();
    if(isnan(p_t->x)) { // catch an obscure bug
        cout<<"( RXCurve::Drop_To_(polyline)Curve) ISNAN. q is " <<p_A.x<< " "<< p_A.y<<" "<< p_A.z<<endl;
      //  this->DumpPLine( this->m_pline,this->m_c,stdout);  fflush (stdout);
        return 0;
    }
    if(count<=0 && c_outer<=0){
#ifdef DEBUG
        //  cout<<"Perpendicular out by "<<inc<<" last="<< LastInc<<" tol= "<<p_tol<<" relfac="<< relfac<<endl;
#endif
        return 0;
    }
    else
        return 1;

    return(count>0);
}//int RXCurve::Drop_To_Curve


int RXCurve::GetONPolyLineCurveFromRelaxPL(rxON_PolylineCurve & p_PlC) const
//returns RXCURVE_OK if can copy m_p in p_PlC
{
    //THOMAS 02 11 04  NOW We can have both m_pONCurve and m_p
    //THOMAS 02 11 04  it's temporary
    //THOMAS 02 11 04  we will soon olnly have m_pONCurve
    //THOMAS 02 11 04  if (m_pONCurve)
    //THOMAS 02 11 04  	this->Get_Ent()->OutputToClient("trying to convert a Relax Polyline to a ON_PolyLineCurve, but the object is allready descrive with a ON_Curve",2);
    //THOMAS 02 11 04  assert(!m_pONCurve);

    if (!m_pline)  {
        return 0;
    }
    assert(m_pline);

    ON_Polyline l_PL;
    l_PL.Empty();

    int i;
    ON_3dPoint l_pt(-1.0,-1.0,-1.0);
    for (i=0;i<m_c;i++)
    {
        l_pt = ON_3dPoint(m_pline[i].x,m_pline[i].y,m_pline[i].z);
        l_PL.AppendNew() = l_pt;
    }

    p_PlC = rxON_PolylineCurve(l_PL);
    l_PL.Empty();

    return RXCURVE_OK;
}//ON_PolylineCurve RXCurve::GetONPolyLineCurveFromRelaxPL

ON_3dPointArray RXCurve::Divide(RXOffset &o1, RXOffset &o2, sc_ptr sc, int side, int p_n)
{
    double s1,s2,t,t1,t2;
    int k,l_flag;
    ON_3dPointArray r;
    assert( sc == (sc_ptr ) m_e);
    s1 = o1.Evaluate(sc,side);
    s2 = o2.Evaluate(sc,side);
    if(m_pONCurve) {
        l_flag = GetParamAt(s1,t1,0.00001);
        l_flag = GetParamAt(s2,t2,0.00001);
    }
    else {
        t1=s1;
        t2=s2;
    }

    for(k=0;k<p_n+1;k++) {
        t = t1*(1.0-(float) k/(float)p_n) + t2 * (float)k/(float) p_n;
        r.AppendNew() = PointAt(t) ;
    }
    return r;
}



int RXCurve::IsPtInsideBB(const ON_3dPoint t, double tol)
{// fast check
    // method. Find the bounding box for C and add tol all round. See if v is inside it.


    struct PC_QCD *q;
    int monotonic=0;
    assert( RXCURVE_OK !=0);

    if(!m_qcd)
        m_qcd = Find_Bounding_Box(0,m_c-1,monotonic);

    q = m_qcd;
    if(!q)
        return RXCURVE_OK; // safer to say somewhere near;

    if (t.x   > q->r + tol) return 0;  //what about Z
    if (t.x < q->l - tol) return 0;

    if (t.y > q->t + tol) return 0;
    if (t.y < q->b - tol) return 0;
    assert(RXCURVE_OK !=0);
    return RXCURVE_OK;
}// int RXCurve::IsPtInsideBB(const ON_3dPoint t, double tol)




#ifndef ON_V3
// in ON V4 this calls GeteBB not GetTightBB.  GetTightBB isn't implemented in ON V3 anyway.
int RXCurve::GetTightBoundingBox(rxON_BoundingBox &bbox, bool bGrowBox, const ON_Xform *onb) 
#else
int RXCurve::GetTightBoundingBox(rxON_BoundingBox &bbox, bool bGrowBox, const ON_Plane *onb) const
#endif
{	
    int i, rc=0;
    // if it has an ONO, get its BB
    // else ge tthe BB of the polyline
    //else return 0;
    if(m_bb.IsValid ()) {
        bbox= m_bb;
        return 1;
    }
    if(m_pONCurve)
        rc= m_pONCurve->GetBoundingBox(bbox,bGrowBox); // sept 06 Peter Was tight,onb
    else {
        ON_SimpleArray<ON_3dPoint> pt;
        for(i=0;i<m_c;i++)
            pt.AppendNew() = ON_3dPoint(m_pline[i].x,m_pline[i].y, m_pline[i].z);
#ifndef ON_V3
        assert("RXCurve::BB not stepped"==0);
        bbox.Set(pt);
#else
        rc=ON_GetTightBoundingBox(  pt, bbox, bGrowBox,onb) ; // ON V3 only
#endif
        pt.Destroy();
    }
    if(rc) m_bb=bbox;
    return(rc);
}

bool RXCurve::IsValid() const {

    if( m_pONCurve)
        return m_pONCurve->IsValid ();
    return (this->m_c>1);

}
int RXCurve::IntersectRXC(const RXCurve & p_Crv2,
                          double**p_ss1, double**p_ss2, int*p_nc, //Define the intersections (OUTPUT)
                          const double & p_CutTol, const double & p_pTol) const
{
    const RXCurve & p_Crv1 = *this;
    // Find all the interesection beetween 2 RXCurves and return the result in a SlowCut Like format
    /*
returns 0 if no intersection find 
     PCC_OK if at least 1 interesection is found
  PCC_PARALLEL if the curves are aparallel  (NOT IMPLEMENTED YET)
p_Crv1, p_Crv2      //Define the 2 curve to intersect
p_ss1, p_ss2, p_nc, //Define the interesections (OUTPUT), p_ss1 position of the interesections on polyline 1, parameter 0<<1 
              , p_ss2 position of the interesections on polyline 2, parameter 0<<1
              , p_nc number of intersections
// p_CutTol	is a tolerance parallel to the curve.
// p_pTol	is a tolerance normal to the curve.

 */

    int iret = 0;
    if (p_Crv1.GetONCurve()&&p_Crv2.GetONCurve())
    {
        //Check if the curves are not Parallel
        if (p_Crv1.GetONCurve() == p_Crv2.GetONCurve()) // pointer check
            return PCC_PARALLEL;

        ON_ClassArray<RXCrossing2> l_Intersections;

        const ON_Curve * l_crv1 = dynamic_cast<ON_Curve*>(p_Crv1.GetONCurve());
        const ON_Curve * l_crv2 = dynamic_cast<ON_Curve*>(p_Crv2.GetONCurve());

        assert(l_crv1&&l_crv2);
        iret = FindCurveIntersections(l_crv1,l_crv2,l_Intersections,p_pTol, p_CutTol,&p_Crv1,&p_Crv2);

        *p_nc = l_Intersections.Count();
        //REALLOC for beeing the same as the previous version (SlowCut)
        *p_ss1 = (double *)REALLOC(*p_ss1, (*p_nc+1)*sizeof(double));
        *p_ss2 = (double *)REALLOC(*p_ss2, (*p_nc+1)*sizeof(double));

        int i;
        double l_len = 0.0;
        double l_Crvlen = 0.0;
        ON_Interval l_domain;
        ON_Interval l_Crvdomain;

        for (i=0;i<*p_nc;i++)
        {
            l_Crvdomain = l_crv1->Domain();
            l_domain = ON_Interval(l_Crvdomain.Min(),min(l_Intersections[i].Gett(0),l_Crvdomain.Max()));  //ON_Interval(l_Crvdomain.Min(),min(l_Intersections[i][0],l_Crvdomain.Max()));
            l_crv1->GetLength(&l_len,1.0e-8,&l_domain);  //l_Intersections[i][0]
            l_crv1->GetLength(&l_Crvlen,1.0e-8);
            (*p_ss1)[i] = min(l_len,l_Crvlen);

            l_Crvdomain = l_crv2->Domain();
            l_domain = ON_Interval(l_Crvdomain.Min(),min(l_Intersections[i].Gett(1),l_Crvdomain.Max()));  //			l_domain = ON_Interval(l_Crvdomain.Min(),min(l_Intersections[i][1],l_Crvdomain.Max()));  //
            l_crv2->GetLength(&l_len,1.0e-8,&l_domain);  //l_Intersections[i][1];
            l_crv2->GetLength(&l_Crvlen,1.0e-8);
            (*p_ss2)[i] = min(l_len,l_Crvlen);
        }

        //clean up
        if (l_crv1) {l_crv1= NULL;}
        if (l_crv2) {l_crv1= NULL;}

    }  //if (p_Crv1.GetONCurve()&&p_Crv2.GetONCurve())
    else
        if (p_Crv1.Get_c()&&p_Crv2.Get_c())  //to deal with the polylines
        {
            if(!p_Crv1.Get_p() || !p_Crv2.Get_p())
                this->Get_Ent()->OutputToClient("out of memory",3);

            iret = SlowCut(p_Crv1.Get_p(),p_Crv1.Get_c(),p_Crv1.Get_ds(),
                           p_Crv2.Get_p(),p_Crv2.Get_c(),p_Crv2.Get_ds(),
                           p_ss1, p_ss2, p_nc,
                           p_CutTol);
        }//if (p_Crv1.Get_c()&&p_Crv2.Get_c())
        else
        {
            this->Get_Ent()->OutputToClient("One of the 2 RXCurve has neither Polyline or ONCurve, in Intersect RXCurve ",2);
        }

    return PCC_PARALLEL; //Safer than anything else
    if (iret==0)
        return 0; //Thomas 13 07 04 PCC_PARALLEL;
    else
    {
        int l_val = PCC_OK;
        return PCC_OK;
    }
}//int IntersectRXC



