   /*  dxfout function PH 1996
  
  27/4/96 first time. 
  Contains
  DXFoutStart(f);  writes a minimal start section
  DXFoutEnd(fp) writes the end. Doesnst Close the file

	int DXFoutPoint	  (FILE *fp,VECTOR p,char*name,int*handle,char*xdata);
	int DXFoutPolyline (FILE *fp,int c,VECTOR *p,char*name,int*handle,char*xdata, float w);
	int DXFoutPolygon  (FILE *fp,int c,VECTOR *p,char*name,int*handle, char*xdata); 

	int DXFoutShell(FILE *fp, int pcount,VECTOR *pts, int Nfaces,int *flist,char *name,int*handle, char*xdata, int E);
	int DXFoutMesh(FILE *fp, int rows,int cols,VECTOR *pts, char *name, int*handle, char*xdata, int order);
	int DXFoutText (FILE *fp,VECTOR p,char*t,float h, char*name,int*handle,char*xdata);
 
 */

#include "StdAfx.h"
#include "RelaxWorld.h"
#include "polyline.h"
#include "stringutils.h"
#include "dxfout.h" 

#ifndef _CONSOLE
	#include <akmutil.h>
	#include <hoopcall.h>
#else

#define  rxerror(a,b) printf("ERROR: %s %d\n",a,b)

int  is_empty(char *s){
	if(!s) return 1;
	if(!(*s)) return 1;
	return 0;
}

int PC_Replace_Char(char *s, char a, char b){
char *lp;
int c =0;
		for(lp=s; *lp;lp++) {
			if(*lp==a) *lp=b; c++;
		}

	return c;
}
int Polygon_Centroid( VECTOR p[], int c, VECTOR *cen){

	int k;
	if (c<2) return(0);
	cen->x =	cen->y = cen->z =0.0;
	for (k=0;k<c;k++){
		cen->x += p[k].x;
		cen->y += p[k].y;
		cen->z += p[k].z;
		}

	cen->x 	 /= (float) c;
	cen->y 	 /=(float) c;
	cen->z	 /=(float) c;
	return(1);
}
#endif

int DXFoutMakeValidLayerName(char*name) { /* returns 1 if it made a change */ 
  char s[6];
   int iret=0;
  int i,n;
  int changed=0;

  /* should permit () */
  char *OKs ="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz$_-"; 
  if(rxIsEmpty(name)) return(0); // picks up NULLs
  n=strlen(name);
  if(n>31)
	  name[31]=0;
  s[1]= 0;
  for(i=0;i<n;i++) {
    s[0]=name[i];
    if(strstr(OKs,s)==NULL){ 
      name[i]='$';
      changed=1; 
    }
  }
  return(changed);
}	

int DXFoutStart(FILE *fp, int *handle){ /* writes a minimal start section */
   if(!fp){ g_World->OutputToClient(" NULL fp in  DXFoutStart ",0); return 0;}
 fprintf(fp,"  0\nSECTION\n");

 fprintf(fp,"  2\nHEADER\n");
	fprintf(fp,"  9\n$CECOLOR\n");
	fprintf(fp,"  62\n   256\n");
	fprintf(fp,"    9\n$PDMODE\n 70\n    35\n");
//	fprintf(fp,"    9\n$PDSIZE\n 40\n    0.0\n");
	fprintf(fp,"    9\n$PDSIZE\n 40\n0\n"); // Peter Feb 2005 so Rhino reads pts
	if(handle && *handle) {
		fprintf(fp,"    9\n$HANDLING\n70\n1\n");
//		fprintf(fp,"    9\n$HANDSEED\n");
//		DXFPrintHandle(fp,handle);
//		(*handle) +=1024;
	}
 fprintf(fp,"  0\nENDSEC\n");

	fprintf(fp,"  0\nSECTION\n");
	fprintf(fp,"  2\nTABLES\n");

		fprintf(fp,"  0\nTABLE\n");
			fprintf(fp,"  2\nAPPID\n");
			fprintf(fp,"  0\nAPPID\n");
			fprintf(fp,"  5\n21\n");
	//		fprintf(fp,"100\nAcDbSymbolTableRecord\n"); // TRYING TO GET rHINO TO READ
	//		fprintf(fp,"100\nAcDbRegAppTableRecord\n");
			fprintf(fp,"  2\nGAUSS\n");
			fprintf(fp," 70\n     0\n");
		  fprintf(fp,"  0\nENDTAB\n");
		fprintf(fp,"  0\nTABLE\n");
				fprintf(fp,"  2\nLAYER\n");
				fprintf(fp," 70\n     1\n");
			/* layer 0 WHITE */
				fprintf(fp,"  0\nLAYER\n");
				fprintf(fp,"  2\n0\n");
				fprintf(fp," 70\n     0\n");
				fprintf(fp," 62\n     7\n");
				fprintf(fp,"  6\nCONTINUOUS\n");
			/* layer  GAUSS_MOULD */
				fprintf(fp,"  0\nLAYER\n");
				fprintf(fp,"  2\nGAUSS_MOULD\n");
				fprintf(fp," 70\n     0\n");
				fprintf(fp," 62\n     40\n");
				fprintf(fp,"  6\nCONTINUOUS\n");
			/* layer  GAUSS_EDGES */
				fprintf(fp,"  0\nLAYER\n");
				fprintf(fp,"  2\nGAUSS_EDGES\n");
				fprintf(fp," 70\n     0\n");
				fprintf(fp," 62\n     4\n");
				fprintf(fp,"  6\nCONTINUOUS\n");

			/* layer  GAUSS_SEAMS */
				fprintf(fp,"  0\nLAYER\n");
				fprintf(fp,"  2\nGAUSS_SEAMS\n");
				fprintf(fp," 70\n     0\n");
				fprintf(fp," 62\n     5\n");
				fprintf(fp,"  6\nCONTINUOUS\n");

			/* layer  GAUSS_PATCHES */
				fprintf(fp,"  0\nLAYER\n");
				fprintf(fp,"  2\nGAUSS_PATCHES\n");
				fprintf(fp," 70\n     0\n");
				fprintf(fp," 62\n     2\n");
				fprintf(fp,"  6\nCONTINUOUS\n");

			/* layer  GAUSS_POCKETS */
				fprintf(fp,"  0\nLAYER\n");
				fprintf(fp,"  2\nGAUSS_POCKETS\n");
				fprintf(fp," 70\n     0\n");
				fprintf(fp," 62\n     6\n");
				fprintf(fp,"  6\nCONTINUOUS\n");
	  fprintf(fp,"  0\nENDTAB\n");

 fprintf(fp,"  0\nENDSEC\n");

 fprintf(fp,"  0\nSECTION\n");
 fprintf(fp,"  2\nBLOCKS\n");
 fprintf(fp,"  0\nENDSEC\n");

 fprintf(fp,"  0\nSECTION\n");
 fprintf(fp,"  2\nENTITIES\n");
 
return(1);
}
int DXFPrintHandle(FILE *fp,int*handle) {
 	if(handle && *handle) {
		fprintf(fp,"  5\n%X\n",*handle); (*handle)++;
	}
return 1;
}
 int DXFPrint(FILE *fp,char*name, int*handle, char*xdata) {
 
	DXFPrintHandle(fp,handle);
	DXFoutMakeValidLayerName(name);

	fprintf(fp,"  8\n%s\n",name);  /* layer */
	 if(xdata && *xdata) {
		PC_Replace_Char(xdata, '\n', ' ');
		fprintf(fp,"  1001\n%s\n","Gauss");	
		fprintf(fp,"  1002\n%s\n","{");	
		fprintf(fp,"  1000\n%s\n",xdata);
		fprintf(fp,"  1002\n%s\n","}");
		// We should chop xdata into 255char lines
	 }
 return 1;
	}

int DXFoutText	  (FILE *fp,VECTOR p,char*t,float h, char*name,int*handle,char*xdata){
	  if(!fp){ g_World->OutputToClient(" NULL fp in  DXFoutPoint",0); return 0;}

		fprintf(fp,"  0\nTEXT\n"); /* intro */
		fprintf(fp,"  62\n 256\n");  /*colour by layer */



		fprintf(fp,"  10\n %f\n",p.x);  
		fprintf(fp,"  20\n %f\n",p.y);  
		fprintf(fp,"  30\n %f\n",p.z); 
		fprintf(fp,"  40\n %f\n",h); 
		fprintf(fp,"  1\n %s\n",t);  
		fprintf(fp,"  50\n 0.0\n"); 
		DXFPrint (fp, name, handle, xdata);
return 1;
 }


int DXFoutPoint	  (FILE *fp,VECTOR p,char*name,int*handle,char*xdata){
		if(!fp){ g_World->OutputToClient(" NULL fp in  DXFoutPoint",0); return 0;}

		fprintf(fp,"  0\nPOINT\n"); /* intro */
		fprintf(fp,"  62\n 256\n");  /*colour by layer */

		fprintf(fp,"  10\n %f\n",p.x);  
		fprintf(fp,"  20\n %f\n",p.y);  
		fprintf(fp,"  30\n %f\n",p.z); 
		DXFPrint (fp, name, handle, xdata);

		return 1;
}

int DXFoutPolyline(FILE *fp,int c,VECTOR *p,char*name, int*handle,char*xdata,  float W){ /*writes vertices */
 int i;
  if(!fp){ g_World->OutputToClient(" NULL fp in  DXFout Polyline ",0); return 1;}
  if(!c) return 1;
  fprintf(fp,"  0\nPOLYLINE\n"); /* intro */
//  fprintf(fp,"  8\n%s\n",name);  /* layer */
 fprintf(fp,"  62\n 256\n");  /*colour by layer */
 fprintf(fp,"  40\n %f\n",W);  /*start width */
 fprintf(fp,"  41\n %f\n",W);  /*start width */

 fprintf(fp," 66\n     1\n"); /* entities follow */
 fprintf(fp," 70\n     8\n");	/* a 3D polyline */
 DXFPrint (fp, name, handle, xdata);

  for(i=0;i<c;i++) {
		fprintf(fp,"  0\nVERTEX\n");
		DXFPrintHandle(fp,handle);

		fprintf(fp,"  8\n0\n");
		fprintf(fp,"  70\n32\n");
		fprintf(fp," 10\n%f\n",p[i].x);
		fprintf(fp," 20\n%f\n",p[i].y);
		fprintf(fp," 30\n%f\n",p[i].z);
  }

  fprintf(fp,"  0\nSEQEND\n");
   DXFPrintHandle(fp,handle);// the seqend needs a handle


return(1);
}


int DXFoutPolygon  (FILE *fp,int c,VECTOR *p,char*name,int*handle, char*xdata){ /* a series of 3dfaces */
 int k,k1;

  ON_3dPoint cen;
  if(!fp){ g_World->OutputToClient(" NULL fp in  DXFout Polygon ",0); return 1;}
  if(!c) return 1;
 Polygon_Centroid( p,c, &cen); 

  for(k=1;k<c ;k++) {
		k1 = k-1;
 		fprintf(fp,"  0\n3DFACE\n"); /* intro */
 
 		DXFPrint(fp,name,NULL,NULL);

		 fprintf(fp," 70\n    0\n");	/* a closed 3D polygon mesh */
  
  		 fprintf(fp," %d\n%f\n", 10, p[k1].x);
		 fprintf(fp," %d\n%f\n", 20, p[k1].y);
		 fprintf(fp," %d\n%f\n", 30, p[k1].z);

		 fprintf(fp," %d\n%f\n", 11, p[k].x);
		 fprintf(fp," %d\n%f\n", 21, p[k].y);
		 fprintf(fp," %d\n%f\n", 31, p[k].z);

		 fprintf(fp," %d\n%f\n", 12, cen.x);
		 fprintf(fp," %d\n%f\n", 22, cen.y);
		 fprintf(fp," %d\n%f\n", 32, cen.z);

		 fprintf(fp," %d\n%f\n", 13, cen.x);
		 fprintf(fp," %d\n%f\n", 23, cen.y);
		 fprintf(fp," %d\n%f\n", 33, cen.z);
		DXFPrintHandle(fp,handle);
 	 }
return(1);
}

int DXFoutShell(FILE *fp, int pcount,VECTOR *pts, int Nfaces,int *flist,char *name, int*handle, char*xdata,int Edges_Vis){
 int i, *fi,c,k,v;

  if(!fp){ g_World->OutputToClient(" NULL fp in  DXFoutShell ",0); return 1;}

  if(!pcount || !Nfaces) { g_World->OutputToClient(" no point in writing a zero shell",2);  return 1; }
  fprintf(fp,"  0\nPOLYLINE\n"); 
	 DXFPrint (fp, name, handle, xdata);
/*  fprintf(fp,"  62\n 256\n");  colour by layer */
 
 fprintf(fp," 66\n     1\n"); /* entities follow */
 fprintf(fp," 70\n    64\n");	/* a 3D polyface mesh*/
 fprintf(fp," 71\n    %d\n",pcount);	/* no of vertices*/
 fprintf(fp," 72\n    %d\n",Nfaces);	/* no of faces*/
 


  for(i=0;i<pcount;i++) {
		 fprintf(fp,"  0\nVERTEX\n");
		  DXFPrintHandle(fp,handle);
		 fprintf(fp,"  8\n0\n");
		 fprintf(fp,"  70\n192\n");
		 fprintf(fp," 10\n%f\n",pts[i].x);
		 fprintf(fp," 20\n%f\n",pts[i].y);
		 fprintf(fp," 30\n%f\n",pts[i].z);
	  }
 fi = flist;
  for(i=0;i<Nfaces;i++) {
		 fprintf(fp,"  0\nVERTEX\n");
		DXFPrintHandle(fp,handle);
		 fprintf(fp,"  8\n0\n");
		 fprintf(fp,"  70\n128\n");
		 fprintf(fp," 10\n%f\n",0.0);
		 fprintf(fp," 20\n%f\n",0.0);
		 fprintf(fp," 30\n%f\n",0.0);
		c = *fi;  fi++;
		//if(c !=3) printf(" Nvtx = %d only allowed triangles in DXFout \n",c);
		for(k=0;k<c;k++) {
			v = (*(fi)) +1; fi++;		/*	in ACAD, start at 1 */
			if(Edges_Vis==0) v=-v;
			fprintf(fp," %d\n  %d\n",(int)(71+k), v );


		}

  }
  fprintf(fp,"  0\nSEQEND\n");
  DXFPrintHandle(fp,handle);
return 1;
}
int DXFoutMesh(FILE *fp, int rows,int cols,VECTOR *pts, char *name, int*handle, char*xdata, int order){
 int i,j,k=0, VertexFlag=64;
 
  if(!fp){ g_World->OutputToClient(" NULL fp in  DXFoutMesh ",0); return 0;}
  if(!rows || !cols)  return 1; 
  fprintf(fp,"  0\nPOLYLINE\n"); /* intro */
 
/*  fprintf(fp,"  62\n 256\n");  colour by layer */

 DXFPrintHandle(fp,handle);
 fprintf(fp," 66\n     1\n"); /* entities follow */
 fprintf(fp," 70\n    16\n");	/* a 3D polygon mesh*/

 switch (order) {
	case 2:
			fprintf(fp," 73\n    %d\n",4*rows);
			fprintf(fp," 74\n    %d\n",4*cols);
			fprintf(fp," 75\n    5\n");
			VertexFlag=80;  
			break;
	case 3:
			fprintf(fp," 73\n    %d\n",4*rows);
			fprintf(fp," 74\n    %d\n",4*cols);
			fprintf(fp," 75\n    6\n");
			VertexFlag=80; 
			break;
	 case 0:
	 case 1:
		 fprintf(fp," 75\n    0\n");	/*no smooth surface fitted */
		 VertexFlag=64; 
		 break;

	 default:
		 g_World->OutputToClient("DXFoutMesh: Order must be in range  0-3 ",2); 
		 return 0;

 }


 fprintf(fp," 71\n    %d\n",rows);
 fprintf(fp," 72\n    %d\n",cols);
	DXFPrint (fp, name, NULL, xdata);// dont print the han dle again
	k=0;
	for(i=0;i<rows;i++) {
		for(j=0;j<cols;j++,k++) {
			fprintf(fp,"  0\nVERTEX\n");

			fprintf(fp,"  8\n0\n");
			fprintf(fp,"  70\n%d\n",VertexFlag);
			fprintf(fp," 10\n%f\n",pts[k].x);
			fprintf(fp," 20\n%f\n",pts[k].y);
			fprintf(fp," 30\n%f\n",pts[k].z);
			DXFPrintHandle(fp,handle);
		}
 	}

  fprintf(fp,"  0\nSEQEND\n");
  DXFPrintHandle(fp,handle);// the seqend needs a handle
return 1;
}

int DXFoutEnd(FILE*fp){
  if(!fp){ g_World->OutputToClient(" NULL fp in  DXFoutEnd",0); return 1;}
  fprintf(fp,"  0\nENDSEC\n  0\nEOF\n");
return(1);
}
#ifdef _CONSOLE
int main(){
  FILE *fp;
  float W=0.0;
  VECTOR point = {0.,0.,0};
  int cpl=5;
  int handle = 256;
  VECTOR ppl[5] = {{0,	0,	0	},
						{1,.075,.1	},
						{2,.1,	.2	},
						{3,.075,.3	},
						{4,	0,	.4 }};
  int cpg=6;
  VECTOR ppg[6] =		{{  0.0000  ,  1.0000 ,   0.0000},
						{ -0.9511  ,  0.3090 ,   0.0000},
						{-0.5878 ,  -0.8090  ,   0.0000},
						{0.5878 , -0.8090  ,  0.0000},
						{0.9511  ,   0.3090 ,   0.0000},
						{  0.0000  ,  1.0000 ,   0.0000} };

    VECTOR ppm[12] =	
		{{	0.00	,	0.00	,	0	},
		{	-0.0928	,	0.9922	,	0	},
		{	-0.1247	,	2.0051	,	0	},
		{	0.00	,	3.00	,	0	},
		{	0.6946	,	0.00	,	0	},
		{	0.4768	,	0.9963	,	0.1331	},
		{	0.2516	,	2.0034	,	0.1103	},
		{	0.07	,	3.00	,	0	},
		{	1.5009	,	0.00	,	0	},
		{	1.3642	,	0.9977	,	0.1384	},
		{	1.1164	,	1.9844	,	0.1601	},
		{	0.2007	,	3.00	,	0	}};


  fp = fopen("relax.dxf","w");
  if(!fp) return(0);
#define  GETOUT  { fclose(fp);return 0;}
   if(! DXFoutStart(fp,&handle)) GETOUT

	handle++;
if(!DXFoutPolygon  (fp,cpg,ppg,"polygon_layer",&handle, "xdata for a polygon" )) GETOUT 

if(!DXFoutText	  (fp,point,"some text", 0.05, "textlayer",&handle," xdata for text")) GETOUT
   
if(!DXFoutPoint	  (fp,point,"pointlayer",&handle," xdata for a point")) GETOUT

if(!DXFoutPolyline (fp,cpl,ppl,"plinelayer",&handle," xdata for a polyline", 0.0)) GETOUT

//if(!DXFoutShell(fp, int pcount,VECTOR *pts, int Nfaces,int *flist,char *name,int*handle, char*xdata, int E);
if(!DXFoutMesh(fp, 3,4,ppm, "mesh_layer", &handle, "xdata for mesh ", 2)) GETOUT


	
 	if(!DXFoutEnd(fp)) { fclose(fp);return 0;}

	fclose(fp);
	cout<< "written to relax.dxf\n"<<endl;
return 1;
}
#endif
/*
A note on handles.
A handle is a unique identifier for each object in an AutoCAD drawing. 
It is a hex number.

A dxf file doesnt have to contain handles.
If it does, each entity (including the SEQENDs) has to have a unique handle 
	which is greater than $HANDSEED. 
	Note that $HANDSEED is in decimal and handles are in hexadecimal.
	Furthermore, $HANDSEED should not be too low because AutoCAD assigns 
	about 20 handles to objects on startup.  

If the dxf file doesnt use handles,
	AutoCAD will assign handles to the objects in the file.

DXF files made by AutoCAD normally contain handles.

  Relax keeps track of the handle of objects imported via DXF,
  
	This is one requirement for commutativity, 
	ie passing a model back and forwards between AutoCAD and Relax.

	At the moment (june 2000) the Relax DXFin is not quite the inverse of the
	Relax DXFout 

This sample code assigns handles sequentially, starting with 100

  If you try to import a file written by these functions to AutoCAD and
  you get a message
'Specified handle xx already in use on line xxx.'
, try a bigger initial value for the integer variable handle. 


LAYER NAMES

  For DXF entities to be translated to Relax model components, they must be in
  layers whose names begin with 'GAUSS_'
  Otherwise, they are displayed but not translated. 

XDATA

  I'll have to do you a note on what to put in the XDATA string. Its mainly used to
  specify material type and alignment, but also for battens and patches. 
  For now, work on
 
  $mat1=diax240			means on the LEFT  of this seam, the fabric is diax240	
  $mat2=diax320			means on the RIGHT of this seam, the fabric is diax320

  $patch				means the polyline represents a patch boundary
  $pocket				means there is a batten pocket along the full length of this seam. 

  ($patch and $pocket have several additional specifiers) 

For material alignment on adjacent panels

 $ref1=<a seam name>	means the panel on the left has material aligned to the given seam.
 $ref2=<a seam name>	means the panel on the right has material aligned to the given seam.

or
  $left=0.0				means on the LEFT  of this seam, the warp is aligned to the seam
  $right=0.0			means on the RIGHT of this seam, the warp is aligned to the seam


  */

