#ifndef _CHECKCONNECTS_H_
#define _CHECKCONNECTS_H_

EXTERN_C int CheckConnects(SAIL *sail);
EXTERN_C int CheckAllConnects(void);
int UpdateAllSliding(SAIL *sail);
int ConnNodesToBoat(SAIL *sail, char *curve, RXEntity_p cEntity);


#endif  //#ifndef _CHECKCONNECTS_H_


