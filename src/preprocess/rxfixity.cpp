#include "StdAfx.h"
#include "f90_to_c.h"
#include "RXSail.h"
#include "RXSRelSite.h"
#include "RXRelationDefs.h"

#include "cfromf.h"
#include "rxfixity.h"

RXFixity::RXFixity(void): 
    RXEntity(0),
    m_dofono(0)
{ assert("dont use this RXFixity constructor"==0);
}
RXFixity::RXFixity (class RXSail*p): 
    RXEntity(p),
    m_dofono(0)
{
}
RXFixity::~RXFixity(void) // unhook but leave the DOFO. Not sure this is what we want.
{
    const int sli = this->Esail->SailListIndex();
    int TheDofoNo	=this->m_dofono;
    if( TheDofoNo >0)
        cf_set_dofo_eptr (sli, TheDofoNo,(class RXObject*) 0);
}
int RXFixity::Dump(FILE *fp) const
{
    return 0;
}
int RXFixity::Compute(void)  // return 1 if the object moved requiring compute of its nieces
{
    const int sli = this->Esail->SailListIndex();
    RXSRelSite *mysite = dynamic_cast<RXSRelSite *>( this->GetOneRelativeByIndex(RXO_FIX_OF_SITE));  
    ON_3dPoint pp = mysite->GetLastKnownGood(RX_GLOBAL); // X + dx //DeflectedPos( RX_GLOBAL);// 
    double x[3]; for(int i=0;i<3;i++) x[i]=pp[i];
    int rc= cf_update_dofo_origin(sli,this->m_dofono, x )  ;
    return rc;
}
int RXFixity::Resolve(void)
{
    cout<<"dont use the 'fixity' - put the data in the site's attributes"<<endl; return 0;
}
int RXFixity::Finish(void)
{
    const int sli = this->Esail->SailListIndex();
    RXSRelSite *mysite = dynamic_cast<RXSRelSite *>( this->GetOneRelativeByIndex(RXO_FIX_OF_SITE));
    const int nn  = mysite->GetN();
    QString qbuf;
    if( ! this->AttributeGet("$fix",qbuf)) qbuf="full";
    int TheDofoNo = cf_setnodalfixing(sli,nn,qPrintable(qbuf),qbuf.length());
    if( TheDofoNo <=0) return 0;
    this->m_dofono =TheDofoNo;
    cf_set_dofo_eptr (sli, TheDofoNo,(class RXObject*) this);
    return  1;
}
int RXFixity::ReWriteLine(void)
{
    return 0;
}

int RXFixity::CClear(){
    int rc=0;
    // clear stuff belonging to this object

    // call base class CClear();
    rc+=RXEntity::CClear ();
    return rc;
}

