// layerlink.h: interface for the layerlink class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LAYERLINK_H__6D760939_85E3_417A_AE15_E1017FEF8CEA__INCLUDED_)
#define AFX_LAYERLINK_H__6D760939_85E3_417A_AE15_E1017FEF8CEA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "layerobject.h"

#include "layercontour.h"
typedef class layernode Lnode; 



class layerlink : public layerobject  
{

public:
	char * Deserialize(char *p_s);// Fills out the object from p_s. returns the ptr to the next char
	ON_String  Serialize(); // allocates and returns a character array image of the object
	int HasContourAt(const double pZ);
	double GetDivideFactor();
	int EdgeWalkWhichSideOut(const int level);
	int IsEdgeOf(const int level);
	int CrossesPlyBoundary(const int level=-1);
	double LinkError(const int level,const int pFlags, int*ierr);

	double Direction(int revFlag);
	layerlink* NextLeft(  int *revFlag,  const int level = -1);
	layerlink* NextRight(  int *revFlag,  const int level = -1);
	double DistLeft(const ON_2dPoint p,const int revFlag);
	double Length();
	HC_KEY DrawHoops(const double z1=0.0, const double z2=0.0);
	void Init();
	layernode* OtherEnd(const layernode*p_n) const;
	int Print(FILE *fp) const;

	layernode* Divide(const double fac=0.5);
	layernode* DivideDelaunay(const double fac=0.5);
	layerlink( layertst *lt);
	virtual ~layerlink(); 

	double m_w;
	Lnode  *m_n1;
	Lnode  *m_n2;

	double GetWeight()	{return m_w;}
	Lnode  *GetN1(const int revFlag=0)		{return (revFlag ? m_n2: m_n1);}
	Lnode  *GetN2(const int revFlag=0)		{return (revFlag ? m_n1: m_n2);}
	ON_SimpleArray <layercontour*> m_lc;


protected:
	double AngleGradientError(const int level, int*ierr);
	double AngleError(int level, int*err );
};

#endif // !defined(AFX_LAYERLINK_H__6D760939_85E3_417A_AE15_E1017FEF8CEA__INCLUDED_)
