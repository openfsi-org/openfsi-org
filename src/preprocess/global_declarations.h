// Relax global variables. Trying to gather them together
#ifndef _GLOBAL_DECLARATIONS_16NOV04
#define  _GLOBAL_DECLARATIONS_16NOV04

#define CONTROL_COUNT	7
#define MAX_GRAPH 100
#ifdef linux
#define _DIRSEP_	"/"
#elif defined(WIN32)
#define _DIRSEP_	"\\"
#endif   

#ifdef _X
#inc lude <X11/Intrinsic.h>
#inc lude <X11/Xproto.h>
#inc lude <X11/Shell.h>

#incl ude <Xm/Xm.h>
#incl ude <Xm/DrawingA.h>
#inc lude <Xm/CascadeBG.h>
#inc lude <Xm/CutPaste.h>
#inc lude <Xm/Form.h>
#incl ude <Xm/LabelG.h>
#inc ude <Xm/MainW.h>
#in clude <Xm/MessageB.h>
#in clude <Xm/PushBG.h>
#in clude <Xm/RowColumn.h>
#inc lude <Xm/SeparatoG.h>
#inc lude <Xm/ToggleBG.h>
EXTERN_C Widget opt1Toggle,opt2Toggle,opt3Toggle;

/* topLevel widgets & functions */
EXTERN_C Widget panelSolveButton;
EXTERN_C Widget meshButton;


#endif //#ifdef _X
#include "units.h"


#define MDIM 64

#include "optimise.h"
#include "iges.h"

#include "pansail.h"
#include "akmpretty.h"


EXTERN_C int Peter_Debug;
#ifdef __cplusplus
class RelaxWorld;
extern class RelaxWorld *g_World;
#endif
#ifndef EXTERN_C
#error( "EXTERN_C has not been defined")
#endif


//GLOBAL VARIABLES from getglobal.h
//extern int g_iterations;
extern int g_JPEG_x;
extern int g_JPEG_y;
extern int g_JPEG_Quality;


extern int g_Gauss_Recursion;
extern  struct _OUTPUT_UNITS g_Units[N_UNITS+1];
//END GLOBAL VARIABLES from getglobal.h

#ifdef NEVER
extern struct PANSAIL_INPUT_VALUES g_PansailIn; 
EXTERN_C int g_Appwind_Untransformed;	 // true for relax LL
extern int g_DO_StreamLine;


//from appwind.cpp
extern QString g_wind_name;
//end from appwind.cpp
//from drawwind.cpp
extern VECTOR g_wind_pos;
//end from drawwind.cpp

//from mast.cpp
extern int g_chosen_mast; /* index to currently chosen mast */
//end from mast.cpp
EXTERN_C int g_USE_PANSAIL;
#endif


extern int g_NON_INTERACTIVE; // turns off questions (SSD) and demotes rxerror
EXTERN_C int g_PanelInPlane;
EXTERN_C int g_Janet;    
EXTERN_C int g_Spline_Division;
EXTERN_C int g_Export_Reactions;
EXTERN_C int g_Transform_On_Deflected;

EXTERN_C double g_Relaxation_Factor;
EXTERN_C int g_NPardisoIterations;   
#ifdef HOOPS
    EXTERN_C int g_Display_Mould;
    EXTERN_C int g_Display_Materials;
    extern char g_Overlay_Text_Size[64];
    extern int g_Colour_By_Face;
    extern char *g_Hoops_Driver;
    extern int g_Mapcount;
    EXTERN_C int g_Hoops_Error_Flag;

    EXTERN_C char g_HNetServerIPAddress[256];
    extern const char *g_pbname[NROWDIM];// = {"faces","edges","lines","markers","text","orientation","Material Type","arrow", "interpolation surface","field" };
    extern const char *g_Expname[NROWDIM]; // = {"jpg","hmf","pict","eps","rad","dxf","iges","pdf","fullscreen","everything","dog" };

#endif


extern int g_Gcurve_Count;

EXTERN_C char g_RelaxRoot[256];
EXTERN_C char g_CodeDir[256];
EXTERN_C char g_workingDir[256];	
EXTERN_C char traceDir[256];
EXTERN_C char g_currentDir[256];
EXTERN_C char g_defaultsFile[256];
EXTERN_C char g_boatDir[256];
EXTERN_C char templateDir[256];


EXTERN_C QString g_qAfterEachRun;
EXTERN_C QString g_qAfter_Each_Cycle;
EXTERN_C QString g_qAfter_Each_Update;

EXTERN_C char* g_Preprocessor_Heuristics;
EXTERN_C char g_Export_Options[256];

#ifdef _X
extern Widget g_panelSolveButton;	//from sailmenu.h  /* kept to enable / disable items later */
extern Widget g_meshButton;
extern Widget g_MaterialWidget;

extern Widget g_PansailTextWindow;	//from text.cpp 
extern Widget g_RelaxTextWindow;
extern Widget g_qdialog;
extern char g_msg[512];
extern Widget g_resolveWidget;
#endif
#ifndef OKPRESS
#define OKPRESS 20
#endif //#ifndef OKPRESS
#ifndef CANCELPRESS
#define CANCELPRESS 21
#endif //#ifndef CANCELPRESS
#ifndef NROWDIM
#define NROWDIM 20
#endif //#ifndef NROWDIM

#ifdef NROWDIM
#ifdef _X
extern Widget g_visButton[NROWDIM];  //From radiomenus.cpp
#endif
#endif //#ifdef NROWDIM

EXTERN_C SAIL *g_boat;

#ifndef RXQT
extern Graphic * g_selectGraph;
extern Graphic g_graph[MAX_GRAPH];
extern int g_hoops_count;	/* to allow for 0 being boat window */
#endif

extern RXEntity_p g_CurrentIncludeFile;
extern struct LINKLIST *g_parList; //  a stack of include and similar files

//FROM menu.cpp

#ifdef _X
extern Widget g_prestressWidget;
extern Widget g_AnalysisKillWidget;
extern Widget g_AnalysisCalcWidget;

extern Widget g_Active_VPP_Widget;
extern Widget g_Passive_VPP_Widget;
#endif
/* Optimisation_Function A; */

#ifdef _X
extern int g_Select_Action;

extern Widget g_open_dialog;		/* moved 31Jan97 Adam */

#ifdef MDIM
extern Widget g_saveChildren[MDIM];
extern Widget g_loadinterpChildren[MDIM];
extern Widget g_dropChildren[MDIM];
extern SAIL *g_sailObject[MDIM]; // to go with drop_children.
#endif  //#ifdef MDIM
#endif

//extern int g_nCams;
//extern RXEntity_p g_camPlist[30];
EXTERN_C int g_ArgDebug;
/* MenuCB.c */
#ifdef _X
extern Widget g_prestressWidget; 
extern int g_TrimExists;
extern Widget g_Active_VPP_Widget;
extern Widget g_Passive_VPP_Widget;
#endif



//FROM control.cpp

#ifdef _X
extern Widget g_options_dialog;
extern Widget g_options_form;
extern Widget g_options_ref_text ;
extern Widget g_options_h_text ;

extern Widget g_wake_dialog ;
extern Widget g_wake_form ;
extern Widget g_wake_label;
extern Widget g_wake_name_text;
extern Widget g_wake_file_button ;
extern Widget g_wake_ok_button ;
extern Widget g_wake_cancel_button ;


extern Widget g_wind_dialog ;
extern Widget g_wind_form ;
extern Widget g_wind_label;
EXTERN_C Widget g_wind_name_text ;
extern Widget g_wind_file_button ;
extern Widget g_wind_ok_button ;
extern Widget g_wind_cancel_button ;

extern Widget g_control_dialog ;
extern Widget g_ncycle_label ;
extern Widget g_method_label ;
extern Widget g_oldmx_label ;
extern Widget g_const_label ;
extern Widget g_choose_label;
extern Widget g_panalysis_label ;
extern Widget g_phase_label ;
extern Widget g_phase_frame ;
extern Widget g_phase_rowcol;
extern Widget g_press_frame;


extern press_rowcol_p g_press_rowcol;  

extern Widget g_ncycle_frame;
extern Widget g_ncycle_rowcol;
extern Widget g_ncycle_text;
extern Widget g_ncycle2_text;
extern Widget g_ncycle3_text;
extern Widget g_ranalysis_label;
extern Widget g_nonlin_label;
extern Widget g_crease_label;
extern Widget g_extra_label;
extern Widget g_relax_frame;

extern relax_rowcol_p g_relax_rowcol; 
#endif

//END FROM control.cpp

//From script.cpp
extern FILE *g_SP;

extern Optimisation_Structure g_Opti; // was O_Function
//end From script.cpp

extern int g_Analysis_Flags;

#ifdef _X
extern Display 		*g_display;
extern XtAppContext	g_app_context;
#endif

#include "ch_rowt.h"
EXTERN_C ch_data_t g_CH_Run_Data;  /* holds data about current summary in file */

#include "controlmenu.h"

extern control_data_t g_ControlData[3];  /* set in getglobal.c */

#ifdef NEVER  // for prehistoric pansail interface

// ncomp now an automatic variable of calcnow. 
extern int g_N[MAXCOM],g_M[MAXCOM],g_pModel[MAXCOM]; 

extern char g_pName[MAXCOM][16];

extern float g_zmdiam[MAXCOM][MAXROW];
extern float g_diam[MAXCOM][MAXROW];
extern float g_nmastp[MAXCOM];
extern float *g_Uv;
extern float *g_Vv;
extern float *g_Xv;
extern float *g_Yv;
extern float *g_Zv;
extern float *g_Xc;
extern float *g_Yc;
extern float *g_Zc;
extern float *g_Uc;
extern float *g_Vc;

extern float *g_Dcp;
extern float *g_CpU;
extern float *g_CpL;
extern float *g_LU;
extern float *g_HL;
extern float *g_phiU;
extern float *g_phiL;
extern float *g_CfU;
extern float *g_CfL;
extern int *g_iblstu;
extern int *g_iblstl;
extern float *g_LSD;
extern float *g_BLF;	
extern float * g_Thu;
extern float * g_Thl;
extern float * g_Hu;
extern float * g_Hl;
extern float * g_Cfu;
extern float * g_Cfl;
extern float * g_Singo;
extern int g_coffset[MAXCOM];
extern int g_goffset[MAXCOM];
extern int g_noffset[MAXCOM];
extern int g_moffset[MAXCOM];
extern int g_nwakeso,g_ntwako,g_ntoto,g_ncuto;
extern int g_mwko[MAXCOM],g_nwko[MAXCOM];
extern int g_irelxo[MAXCOM][MAXROW][MAXROW];		/* these are mwk(pModel) * nwk(pModel) in size 	*/
/* BUT mwk and nwk are returned to me !! 	*/
extern float g_xwakeo[MAXCOM][MAXROW][MAXROW];
extern float g_ywakeo[MAXCOM][MAXROW][MAXROW];  		/* defined to (MAXROW,MAXROW,MAXCOM)     	*/
extern float g_zwakeo[MAXCOM][MAXROW][MAXROW];
extern char g_namwako[MAXCOM][16];
extern char g_baseName[20];
extern char g_wakefile[128];

extern int g_keepWake;
extern int g_nrelax;
extern int g_isetup;	/* always do the generate nr matrix */

extern double g_relfac ;
#endif
//FROM iges.cpp
extern int g_GlobalType[iges_NGLOBALS ];
extern int g_IGS_DTYPE[18];
//extern char g_igs_stmp[iges_MAXSTR];
//END from iges.cpp

//From text.cpp

#define MAX_TEXT_WINDOWS 2

extern const char g_PansailTextFileName[];
extern const char g_RelaxTextFileName[];
#ifdef _X
extern Widget g_TextWindows[MAX_TEXT_WINDOWS];
#endif
extern const char *g_TextFiles[MAX_TEXT_WINDOWS];
//END from text.cpp

//From question.cpp
extern struct QDATA *g_question_data;
//extern int  g_QDB;
extern int g_UNRESOLVE_ALL;
//End from question.cpp

//from vppint.cpp
extern char g_VPP_Command[256];
extern char g_VPP_Address[256];
extern char g_VPP_Transfer_File[256];
extern class RXLogFile g_VPP_Sum;
//end form vppint.cpp



//from gridmain.cpp
extern int  g_mydebug;
//end from gridmain.cpp

//from delete.cpp
extern int g_Peter_Debug;
//end from delete.cpp

//from panel.cpp
extern long int g_RCOUNT;
extern double g_SOLVE_TOLERANCE;  // careful. should match getglobal.c
extern double g_ANGLE_TOL;
extern double g_GAUSS_FACTOR;
extern double g_Gauss_Cut_Dist;
extern double g_Gauss_Parallel; // the square of the angle in rads

extern double g_U1,g_U2; 
extern int g_TRACE;
extern int g_COLOR_ERROR;
//end from panel.cpp

//from settrim.cpp
#ifdef _X
extern Widget g_control_text[CONTROL_COUNT];
// theoretically XtCallbackProc these 3 moved from Globals.h sept 2005
EXTERN_C int g_CurrentView;
EXTERN_C int g_CurrentOption;
#endif
//end from settrim.cpp

//from Check All.cpp
extern int g_Flag;  //YUCH
//end from CheckAll

extern int g_Gauss_Recursion;

extern int g_Use_ONCurve; // 1 july 2004

EXTERN_C int g_Filament_pp;

//EXTERN_C const char* g_lpat[10]; // in multigraph
//EXTERN_C const char* g_mpat[10];

EXTERN_C float g_nSDs;

//#ifdef MATHLINK
//	EXTERN_C  RX_MathSession g_MathSession;
//#endif


EXTERN_C double g_VectorPlotScale;

EXTERN_C  double g_Relax_Tolerance, g_Relax_R_Damp,g_Relax_V_Damp,g_diagonalfac ;


// finally, two function declarations
EXTERN_C int init_global_declarations(void);
EXTERN_C  void ClearGlobalValues(void);


#endif //#ifndef _GLOBAL_DECLARATIONS_16NOV04

