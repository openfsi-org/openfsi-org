#ifndef RXSUBMODEL_H
#define RXSUBMODEL_H
#include "RXEntity.h"

/****  RX submodel
Serves to define groups of Entities

Currently the only purpose is to provide some control over object snap and intersection,
for use specifying connects

like this:
The submodel contains a wildcard object-name, a wildcard layer-name.
 and ($slave|$master)=<anothersubmodel>

1)  At Finish-entity time, (but before ONC_Cut time)
   the submodel trawls the objects.
  If it finds one which matches either name or layer, it
  does a SetRelation.

  2)   when we do our cutting and snapping (eg in ONC_Cut),
     we only search amongst other objects with which we share a submodel.
    Any new objects created are given the attributes of their creator
    This means we can have coincident nodes, curves, &c in different submodels
    and they wont get coalesced.

  3)  at finish-time for all other objects,
    (unless it already has a submodel)
     we check to see if the object's name is called by any sublayers,
     ditto for its layer ($layer= xxx) i
    If it is, we do a set-relation.


At Mesh time, it generates connects between coincident objects in each submodel

NOTE.
We have ONC_Cut, Compute_Snap_Intersections, Compute_Cut_Intersections, SnapEnds

*/

class RXsubModel : public RXEntity
{
public:
    RXsubModel();
    virtual ~RXsubModel();
    RXsubModel(class RXSail *s);

    int Dump(FILE *fp) const;
    int Compute(void); // return 1 if the object moved requiring compute of its nieces
    int Resolve(void);
    int Finish(void);
    int ReWriteLine(void);
    int Mesh(void);
    static class RXsubModel* SubModel(class RXEntity *e);

protected:
    int CreateConnect(RXSRelSite*q1,RXSRelSite*q2);
    bool ConnectQ(RXSRelSite*q1 );
    int Needs_Meshing;

};
#endif // RXSUBMODEL_H
