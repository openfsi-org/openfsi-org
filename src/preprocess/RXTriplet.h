#ifndef _RXTRIPLET_HDR__
#define _RXTRIPLET_HDR__
class RXTriplet
{
public:
	RXTriplet(void);
	RXTriplet(const double x,const double y,const double z);
	RXTriplet(SAIL *sail, const RXSTRING &x,const RXSTRING &y,const RXSTRING &z);
	~RXTriplet(void);
    int Init(void);


	RXTriplet& operator-=(const RXTriplet& p);

	RXTriplet operator*(const  double d ) const;
	RXTriplet operator/(const double d ) const;
	RXTriplet operator+( const RXTriplet& p ) const;

public:
	/////////////////////////////////
// Coordinate access methods.

// to set the model-space coords.
	virtual	bool Set(const ON_3dPoint &v);
        int Set(class RXSail *sail, const int i, const RXSTRING &v);

// to retrieve the model-space coordinates
	const ON_3dPoint ToONPoint(void) const;
	 double  X(void) const {return x;}
	 double  Y(void) const {return y;}
	 double  Z(void) const {return z;}

         int  SetX(const RXSTRING &p, class RXSail *sail =0)  ;
         int  SetY(const RXSTRING &p, class RXSail *sail =0)  ;
         int  SetZ(const RXSTRING &p, class RXSail *sail =0);
private:

	double x,y,z;
public:
	int Print(FILE *fp);
	RXSTRING TellMeAbout() const;
}; //class RXTriplet

class RXExpTriplet: public RXTriplet
{
public:
    RXExpTriplet(void);
    RXExpTriplet(const double x,const double y,const double z);
    RXExpTriplet(SAIL *sail, const RXSTRING &x,const RXSTRING &y,const RXSTRING &z);
    ~RXExpTriplet(void);
    int Init(void);


    RXExpTriplet& operator-=(const RXTriplet& p);

    RXExpTriplet operator*(const  double d ) const;
    RXExpTriplet operator/(const double d ) const;
    RXExpTriplet operator+( const RXTriplet& p ) const;

public:
    /////////////////////////////////
    // Coordinate access methods.

    // to set the model-space coords.
    virtual	bool Set(const ON_3dPoint &v);
    int Set(class RXSail *sail, const int i, const RXSTRING &v);

    // to retrieve the model-space coordinates
    const ON_3dPoint ToONPoint(void) const;
    double  X(void);
    double  Y(void);
    double  Z(void);

    int  SetX(const RXSTRING &p, class RXSail *sail =0)  ;
    int  SetY(const RXSTRING &p, class RXSail *sail =0)  ;
    int  SetZ(const RXSTRING &p, class RXSail *sail =0);
private:
    //double x,y,z;
public:
    int Print(FILE *fp);
    RXSTRING TellMeAbout() const;
}; //class RXExpTriplet
#endif
