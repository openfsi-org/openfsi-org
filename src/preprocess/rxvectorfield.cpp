#include "StdAfx.h"
#include "RXExpression.h"
#include "RXSitePt.h"
#include "RXSail.h"
#include "MTParserRegistrar.h"
#include "RXQuantity.h"
#include "RXRelationDefs.h"

#include "etypes.h"
#include "rxvectorfield.h"


/* vector field definition is
 vector field 	 Name 	 (ax) 	 (ay) 	 (az)  [atts]! comment  */
RXVectorField::RXVectorField(void)
{
    assert("dont use default RXVectorField constructors"==0);
}
RXVectorField::RXVectorField( class RXSail *const s)
    :RXEntity (s)
{
    expnames[0]=L"a";  expnames[1]=L"b";    expnames[2]=L"c";
    RXENode* el = dynamic_cast<RXENode*>(s); assert(el);
    this->SetParent(el);

}

RXVectorField::~RXVectorField(void)
{	this->CClear();
       // assert(this->TYPE==PCE_DELETED);
}
int RXVectorField::Resolve(void) // if successful it places an expression named "grid" on the sail.
{
    /* vector field definition is
 vector field 	 Name 	 (u+v) 	 (u+v) 	 (u+v)  [atts]! comment */

    vector<RXSTRING> words= rxparsestring( FromUtf8(this->GetLine()),RXSTRING( RXENTITYSEPS_W));
    if(words.size()<5) return 0;
    if(words.size()>5) this->AttributeAdd(ToUtf8(words[5]).c_str());
    RXSTRING &Sx=words[2];
    RXSTRING &Sy=words[3];
    RXSTRING &Sz=words[4];
    RXExpressionI *l_A,*l_B ,*l_C ;
    l_A= this->AddExpression ( new RXQuantity(L"a",Sx,L"m",this->Esail ) );
    this->SetRelationOf(l_A,aunt,RXO_EXP_OF_ENT,RX_AUTOINCREMENT); //done
    if(! l_A->Resolve()) {
        this->CClear();  return 0; 	}
    l_B= this->AddExpression ( new RXQuantity(_T("b"),Sy,L"m",this->Esail  ) );
    this->SetRelationOf(l_B,aunt,RXO_EXP_OF_ENT,RX_AUTOINCREMENT); //done
    if(!  l_B->Resolve()) {
        this->CClear();	 return 0; 	}
    l_C= this->AddExpression ( new RXQuantity(_T("c"),Sz,L"m",this->Esail ) );
    this->SetRelationOf(l_C,aunt,RXO_EXP_OF_ENT,RX_AUTOINCREMENT); //done
    if(!  l_C->Resolve()){
        this->CClear();	return 0; 	}
    this->SetRelationOf(dynamic_cast<RXObject*>( l_A),child|niece|frog,RXO_X_OF_VF);
    this->SetRelationOf(dynamic_cast<RXObject*>( l_B),child|niece|frog,RXO_Y_OF_VF);
    this->SetRelationOf(dynamic_cast<RXObject*>( l_C),child|niece|frog,RXO_Z_OF_VF);
    this->Needs_Resolving=false;
 //   this->PrintOMap(stdout) ;
    return 1;
} 

int   RXVectorField::ValueAt(const RXSitePt&p, double*rv)
{ int rc=0;
    double u,v,x,y,z,qq;
    u = p.m_u;v=p.m_v;x=p.x;y=p.y;z=p.z;

    RXQuantity *e=0;
    for(int i=0;i<3;i++) { rv[i] =0;
        e=dynamic_cast< RXQuantity *>(this->FindExpression(expnames[i],rxexpLocal));
        if(e){
            try	{
                e->redefineVar(_T("u"),&u);
                if(!ON_IsValid(u) ) cout <<"RXVectorField::ValueAt is using an invalid U value"<<endl;
            }
            catch( MTParserException  e )
            {
               // cout<<"field define exception ";
            } // its OK for these to be undefined
            catch(...) {cout<<"...U ";}
            try	{
                e->redefineVar(_T("v"),&v);
                if(!ON_IsValid(v) ) cout <<"RXVectorField::ValueAt is using an invalid V value"<<endl;
            }  catch( MTParserException  e ) {cout<<" ";}
            catch(...) {cout<<"...V ";}
            try	{
                if(e->getRegistrar()->isVarDefined(_T("x") ))
                    e->undefineVar(_T("x"));
                e->defineVar(_T("x"),&x);
                e->SetNeedsCompiling();
            }
            catch( MTParserException  e ) {cout<<"noX ";}
            catch(...) {cout<<"..X ";}
            try	{
                if(e->getRegistrar()->isVarDefined(_T("y") ))
                    e->undefineVar(_T("y"));
                e->defineVar(_T("y"),&y);
                e->SetNeedsCompiling();
            }
            catch( MTParserException  e )  {cout<<"noY ";}
            catch(...) {cout<<"..Y ";}
            try	{
                if(e->getRegistrar()->isVarDefined(_T("z") ))
                    e->undefineVar(_T("z"));
                e->defineVar(_T("z"),&z);
                e->SetNeedsCompiling();
            }
            catch( MTParserException  e )  {cout<<"noZ ";}
            catch(...)  {cout<<"..Z";}
            qq= e->evaluate();
            rv[i]=qq ;
            rc++;
        }
    }
    return rc;
} //RXScalarProperty::ValueAt
int RXVectorField::ReWriteLine(void) {
    MTSTRING  w;
    QString s;
    QStringList NewLine;
    NewLine<<type()<<  name();

    RXQuantity *e=0;
    for(int i=0;i<3;i++) {
        e=dynamic_cast< RXQuantity *>(this->FindExpression(expnames[i],rxexpLocal));
        w=L"";
        if(e)
            w= e-> GetText();
        s = QString::fromStdWString(w);
        NewLine << s;

    }

    NewLine <<this->AttributeString( );
    NewLine <<" ! rewritten  ";
    this->SetLine(qPrintable( NewLine.join(RXENTITYSEPSTOWRITE) ));
    return 1;
}
int RXVectorField::Dump(FILE *fp) const{
    int rc=0;
    RXQuantity *e=0;
    rc+= fprintf(fp,"VectorField  :  \n" );
    return rc;
}

RXSTRING RXVectorField::TellMeAbout() const {
    return RXSTRING(L" VectorField :") ;
}

int  RXVectorField::CClear(){
    int rc=0;
    // clear stuff belonging to this object

    // call base class CClear();
    rc+=RXEntity::CClear ();
    return rc;
}

int RXVectorField::Compute(void){ return 0;}

int RXVectorField::Finish(void){  return 0;}

