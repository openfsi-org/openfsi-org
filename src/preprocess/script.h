#pragma  once
 #include <QTextStream>
#define MAXDOS		  	64
#define SPT_STREQ		8
#define SPT_FILE_READ	64
#define SPT_FILE_WRITE	128


typedef int (GraphicMethod)    (QStringList &pwords, Graphic *p_g);
// KW:filename  (g if no filename)
typedef int (kwMFG_Method)     (QStringList &pwords, Graphic *p_g );
//kw:model:filename (g i no model)
typedef int (LineMethod)       (QStringList &pwords);

typedef int (SailMethod)       (SAIL *p_sail);
typedef int (SailMethod_f)     (SAIL *p_sail,const char*file);

struct DO_STRUCTURE {
    const char     *m_s;
    SailMethod     *m_sm;
    SailMethod_f   *m_smf;
    GraphicMethod  *m_gm;
    kwMFG_Method   *m_mfg;
    LineMethod     *m_lm;
    int            m_flag;
};
EXTERN_C GraphicMethod  Do_CWD;
EXTERN_C GraphicMethod  Do_Hoist;              // KW:filename  (g if no filename)
EXTERN_C GraphicMethod  Do_Open;             // kw filename

EXTERN_C kwMFG_Method Do_Edit_Script;       // kwMFG cmd:modelname name. line wins over g
EXTERN_C kwMFG_Method Do_Load_State;       // kwMFG cmd:modelname name. line wins over g
EXTERN_C kwMFG_Method Do_SaveAs ;
EXTERN_C kwMFG_Method Do_Save_State ;


EXTERN_C LineMethod Do_Open_View;
EXTERN_C LineMethod Do_Open_Mirror;
EXTERN_C LineMethod Do_MakeLastKnownGood;     //(const char *line);
EXTERN_C LineMethod Do_ChangeExpression;    //(const char *line);
EXTERN_C LineMethod Do_UpdateExpressions;    //(const char *line);
EXTERN_C LineMethod Do_EdgeSwap;    //(const char *line);
EXTERN_C LineMethod Do_FEA_Listing;     // line isnt used
EXTERN_C LineMethod Do_RefreshSettings;    // line isnt used
EXTERN_C LineMethod Do_EditSettings;    // line isnt used
EXTERN_C LineMethod Do_PrintObject;    // line IS used
EXTERN_C LineMethod Do_Apply ;    // line IS used
EXTERN_C LineMethod Do_BringAll ;
EXTERN_C LineMethod Do_System ;
EXTERN_C LineMethod Do_LogResult ;
EXTERN_C LineMethod Do_Test ;
EXTERN_C LineMethod Do_Post_As_Blob ;
EXTERN_C LineMethod Do_Broadcast;
EXTERN_C LineMethod Do_PrestressSet;
EXTERN_C LineMethod Do_Shrink_All_Edges ;
EXTERN_C LineMethod Do_Reset_All_Edges;
EXTERN_C LineMethod Do_SetGraphicsOn;
EXTERN_C LineMethod Do_RebuildMaterials;
EXTERN_C LineMethod Do_DSM_Init;
EXTERN_C LineMethod Do_DSM_End;
EXTERN_C LineMethod Do_DSM_TransferModel;
EXTERN_C LineMethod Do_RunLoop;
EXTERN_C LineMethod Do_OneSetting;
EXTERN_C LineMethod Do_ModelList;
EXTERN_C LineMethod Do_ListDependencies;
EXTERN_C LineMethod Do_Debug;

//EXTERN_C LineMethod Do_Reset_One_Edges(SAIL *s);

//EXTERN_C int Do_Shrink_One_Edges(SAIL *s);

EXTERN_C SailMethod_f Do_Write_uvdp;//		(SAIL *p_sail,const char*filename);
EXTERN_C SailMethod_f Do_Write_vtk;//		(SAIL *p_sail,const char*filename);
EXTERN_C SailMethod_f Do_Write_Attributes;
EXTERN_C SailMethod_f Do_Write_Nastran;//	(SAIL *p_sail,const char*filename);
EXTERN_C SailMethod_f Do_Write_stl;//		(SAIL *p_sail,const char*filename );
EXTERN_C SailMethod_f Do_Write_3dm;//		(SAIL *p_sail,const char*filename );
EXTERN_C SailMethod Do_SetLastKnownGood;        //(class RXSail *p_sail);
EXTERN_C SailMethod Do_RefreshPC;               // sail*sail
EXTERN_C SailMethod Do_PrintAllEntities ;       // sail*sail
EXTERN_C SailMethod Do_DeleteModel ;            // sail*sail
EXTERN_C SailMethod Do_DrawStress ;
EXTERN_C SailMethod Do_DrawStrain ;
EXTERN_C SailMethod Do_DrawPrinStiff;
EXTERN_C SailMethod Do_DrawRefVector;
EXTERN_C SailMethod Do_DiscretizeCurvature;


extern int Do_Mesh(const char *line,Graphic *g);
EXTERN_C SailMethod Do_Mesh;                    // sail*sail

extern ON_String Do_Delete_Entity (const char*line, Graphic *g); // line wins over graphic

EXTERN_C int Do_EditWord(const char *line,Graphic *g);

EXTERN_C int Do_Zoomout(const char *line,Graphic *g);

EXTERN_C int Do_Drop_Sail(const char*line, Graphic *g);//cmd:mname //line over g
#ifdef _X
EXTERN_C int Do_Minimise_Window(Graphic *g, int i) ; //checks for g
#endif
extern int Do_Solve(const char *line, Graphic *g);
extern int Do_Solve(const char *line, SAIL *s);
//extern int Do_Mesh(const char *line,SAIL *s);

EXTERN_C int Do_Export_Pic(const char *line,Graphic *gin);//cmd:cam:f:Opts over g

EXTERN_C int Do_Resize(const char *line,Graphic *g);

EXTERN_C Graphic *Do_Set_Model(const char *linein);  // finds a Graphic corresponding to the 2nd word
EXTERN_C int Do_Env_Edit(const char *linein,Graphic *g);

EXTERN_C int Do_Post_Data(const char *line,Graphic *g);
EXTERN_C int Do_Get_Headers(const char *line,Graphic *g,QObject* client);
EXTERN_C int Do_Get_Data(const char *linein,Graphic *g);
EXTERN_C int Do_Snapshot(const char *line,Graphic *g);
EXTERN_C int Do_Constant_Pressure(const char *line,Graphic *g);
EXTERN_C int Do_Zoom(const char *p_line,Graphic *p_g);

EXTERN_C int Do_Export_IGES(const char *linein, Graphic *gin);
EXTERN_C int Do_Write_Pressure (const char*line, Graphic *g);
EXTERN_C int Print_Script_List(void);

EXTERN_C int Do_Calculate(const char *line,Graphic *g);
EXTERN_C int Do_Utils_Open_Summary(const char *line, Graphic *g);
EXTERN_C int Do_Utils_Open_In(const char *line, Graphic *g);
EXTERN_C int Do_Utils_Close_Summary(const char *line, Graphic *g);
EXTERN_C int Do_Utils_Close_SummaryIn (const char *line, Graphic *g);
EXTERN_C int Do_Utils_Choose_Line(const char *line, Graphic *g);
EXTERN_C int Do_Delete_MatWindow(Graphic *g);		//clear
#ifdef OPTIMISE
EXTERN_C int Do_Optimise(const char*line, Graphic *g); // args unused
EXTERN_C int Do_Load_Optimisation_History(const char*line, Graphic *g);//cmd:file. g just for parent

EXTERN_C int Do_Initialise_Optimisation(const char*line, Graphic *g);//cmd:f: . g unused
#endif

EXTERN_C int Do_Export_Panels(const char *line, Graphic *g);

EXTERN_C int Do_Exit(const char *line,Graphic *g) ; //line ignored.  g only
EXTERN_C int Do_Close_Script(void);			// clear
EXTERN_C int Open_ScriptLog_File(const char*name);
EXTERN_C int Do_Draw_Fanned(const char *linein, Graphic *g);

EXTERN_C int Do_Get_Expressions(const char *linein,Graphic *g, class QObject* client);


//////////////////////////////////////////////////////////////////////////////////////////////
extern QString Do_Action (QStringList &pwords, const Graphic *p_g ,struct DO_STRUCTURE p_TheDo);
EXTERN_C int Initialise_Script(void); // returns g_N_Dos
extern int Execute_Script_Line(const QString &line,const Graphic *g, class QObject * qobj=0);

EXTERN_C int Read_Script(const char *filename,const Graphic *g=0,class QObject *  qobj=0, const bool PleaseLog=true);
EXTERN_C int Process_Script( QTextStream &in,const Graphic *g=0,  class QObject *  qobj=0);

extern QString ReadScriptLine(QTextStream &in);

extern SAIL * PCS_Get_ScriptLine_Model(const QStringList line, const Graphic *g);
extern SAIL * PCS_Get_ScriptLine_Model(const QString line, const Graphic *g);
EXTERN_C Graphic *NameToGraph(const char*sailname);

