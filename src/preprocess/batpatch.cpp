/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 * Modified :
2 oct 02  minor cleaning of some formatting errors found by gcc

ALl entities created have 'made by batpatch'  in their comment.  This stops write_script from writing them
Aug 97  A 0/0 in Compute Batten when only two points
 oct 96 peterfab not fabrics
  *   16/5/96  no commas put into atts of entities generated. For the sake of Excel
 *   13/2/96 uniform patches allowed. But not checked
  *      10/7/95 etype.h
  *     12.1.95	  Put materials on all panels inside a patch
  *                 RELIES on Push_Entity NOT doing duplicates
  *    11.1.95   some speed optimisations on Compute_BP_Geometry
    16 12 94   PH intersections NOT on edges.
         last point forced to e2
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"
#include "RelaxWorld.h"
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXSRelSite.h"
#include "RX_FEPocket.h"
#include "RXLayer.h"
#include "RXRelationDefs.h"
#include "RXOffset.h"
#include "RXCompoundCurve.h"
#include "RXPside.h"
#include "rxbcurve.h"
#include "global_declarations.h"

/* file batpatch.c 13/4/94  

routines for inserting and dealing with battens and patches 
  1) When we read a batten	we generate a list of nodes on it and a list of the seams between the nodes
   The node list contains:
  1) the first node, if any
  2) all points where the batten line crosses another seamcurve (in black space) but not within
   a generous tolerance of the end of the batten or the end of a seam.

   4) The last point

  2) In Compute_Batten(called after Geo_Solve) we re-generate the ps list of the batten. This
   ps list will be used to write to analysis.dat
 The
 Also, provide a function which returns properties given s/arc

 IF its a patch, provide the material function


*/			   

#include "delete.h"

#include "hoopcall.h"
#include "polyline.h"
#include "entities.h"
#include "panel.h"
#include "read.h"


#include "batdefs.h"
#include "peterfab.h"

#include "stringutils.h" 
#include "finish.h"
#include "etypes.h"
#include "arclngth.h"

#include "RXPanel.h"
#include "batpatch.h"


//int Disp_Vector(void*ptr, char*text) {
//		   char s[64];
//		   assert(0);
//		   VECTOR *v = (VECTOR *)ptr;
//		   sprintf(s," Vectorof %s is  (%f %f %f)",text,v->x,v->y,v->z);
//		   rxerror(s,1);
//		   return(1);
//		   }
int In_List(PSIDEPTR p,PSIDEPTR **list,int *n) {
    int k;
    for(k=0;k<(*n);k++) {
        if(p==(*list)[k]) return(1);
    }
    return(0);
}
int Set_Material_Walk(PSIDEPTR ps ,PSIDEPTR **insides,int *ninsides,RXEntity_p mat){
    /* we now have to walk along the psides between the patch and the corner and push the
material onto the matlist of each. 
  METHOD
  a pside has its el[0] and el[1]  and er[0] and er[1], which are entities of other psides
  We start a search from  ps->el[side/2]  which is a pside inside the patch.
  This gets added to a list. We look at its  el[0] and el[1]  and er[0] and er[1] If they
  are not in the list, put them in the list, push the mat onto both sides,
  and walk from each of them in turn
*/	
    sc_ptr  sc;
    Panelptr  pan;
    int k;
    sc = ps->sc;
    if(sc->edge) return(0);
    if(In_List(ps,insides,ninsides)){
        return(0);
    }

    *insides = (PSIDEPTR *)REALLOC(*insides, (*ninsides+2)* sizeof(RXEntity_p ));
    (*insides)[(*ninsides)++]=ps;
    //PC_Highlight_Entity(pso);
    /*Push_Entity(mat,&(ps->m_psmats[0]) );*/    mat->SetRelationOf(ps,PS_LAYER_RELATION,RXO_LAYER,0);
    /*Push_Entity(mat,&(ps->m_psmats[2]) ); */    mat->SetRelationOf(ps,PS_LAYER_RELATION,RXO_LAYER,2);

    for(k=0;k<2;k++) {
        if(ps->leftpanel[k]) {
            pan= ps->leftpanel[k];
            mat->SetRelationOf(pan,child,RXO_LAYER);
        }
    }
    Set_Material_Walk(ps->el[0],insides,ninsides,mat);
    Set_Material_Walk(ps->el[1],insides,ninsides,mat);
    Set_Material_Walk(ps->er[0],insides,ninsides,mat);
    Set_Material_Walk(ps->er[1],insides,ninsides,mat);
    return(1);
}

int Make_Fanned_Batpatch(RXEntity_p e,char*flag,double *arc, double *a0,float chord, int *c, VECTOR **p, struct IMPOSED_ANGLE*ia,int Nin ) {

    /* INPUT
  *arc is an estimate of the arc length
  chord is the straight-line distance between the end points in m
   we are trying to find a polyline of the same chordlength
  *a0 is an estimate of the initial slope of the curve
  *c points in <p> are the fanned offsets ( X range 0 to 1, Y range in m)
  there are N imposed angles in ia
  flag may be "flat", "seam" or "curve".
  If it is "flat" we insert the points but we do not do any cranking
  If it is "seam" we insert the points and crank half the angle.
  If it is "flat" we insert the points and crank the whole angle


RETURNS
  *arc, *a0 get updated.
  in (*c,p) we put the new curve. It is the correct length but needs
  to be placed between endpoints.
  ia.i = the indices of the crank points get updated.
   */


    /* ) evaluate the angles and sort. This is necessary because the angle offsets
 may be given in m or %, and so some may overtake others as the arc-length
 changes.*/

    int YasX, k,ko, loops=0;//Yabs,
    double New_Chord;
    double nominal_Length,yf;
    int N=Nin;

    double errA, dy;
    double errX, dx;
    int *crank=NULL;
    float *x = NULL;
    double *a=NULL;

    VECTOR *pout = (VECTOR *)MALLOC((2+N+(*c)) *sizeof(VECTOR));
    if(pout == NULL ) e->OutputToClient ("out of mem in panel",3);

    nominal_Length= *arc;

    if(N) {
        crank   = (int*)MALLOC(sizeof(int)*(N+1) );
        x     = (float*)MALLOC(sizeof(float)*(N+1) );
        a     = (double*)MALLOC(sizeof(double)*(N+1) );
    }

    YasX=0;
    if (e->AttributeFind("yscaled")) {
        //	Yabs=0;
        YasX=0;
    }
    else if (e->AttributeFind( "yasx")) {
        //	Yabs= 0;
        YasX=1;
    }
    else if (e->AttributeFind("yabs")) {
        //	Yabs=1;
        YasX=0;
    }

    do              /* loop until arc-length stops changing */
    {
        double tx,ty,tz,norx,nory,norz;
        tx = cos(*a0); ty = sin(*a0); tz=0.0;
        norx = -ty;       nory = tx;        norz=0.0;
        if(YasX) yf = nominal_Length;
        else     yf = 1.0;

        for (k=0;k<*c;k++) {
            double pkx=	(*p)[k].x;
            double pky=	(*p)[k].y;
            pout[k].x = (float) (tx* pkx* nominal_Length + norx * pky * yf);
            pout[k].y = (float) (ty* pkx* nominal_Length + nory * pky * yf);
            pout[k].z = (float) 0.0; /*tz* (*p)[k].x* nominal_Length + norz * (*p)[k].y * yf;*/
        }
        *arc = (float)PC_Polyline_Length(*c,pout);
        ko = *c;
        if(N) {	/* this test is new 10/1/95 */
            Sort_IA( ia,N);

            for (k=0;k<N;k++) {
                crank[k] = !strieq(ia[k].s->type(),"relsite");
                if(strieq(ia[k].s->type(),"seamcurve")) {
                    sc_ptr  scp =(sc_ptr  )ia[k].s;
                    if(!scp->seam) crank[k] = 0;
                }
                x[k] =(float) Evaluate_Batpatch_Offset(ia[k].m_Offset,(*arc));
                if (strieq(flag,"curve") &&(ia[k].Angle)   && (ia[k].sign))
                    a[k] =  *(ia[k].Angle) *  *(ia[k].sign);
                else if (strieq(flag,"seam") && (ia[k].Angle))
                    a[k] = -0.5 * *(ia[k].Angle);
                else
                    a[k] = 0.0;
                if (strieq(flag,"flat")) a[k]=0.0;

            }

            Crank_Polyline(&pout,&ko,x,a,crank,N);  /* pout is now MALLOCed to the right size */
        }
        New_Chord = (float) PC_Dist(&(pout[0]),&(pout[ko-1]));

        if(New_Chord > FLOAT_TOLERANCE) {
            errA = (float)-atan2(pout[ko-1].y-pout[0].y,pout[ko-1].x-pout[0].x);
        }
        else errA=(float)0.0;
        errX = chord-New_Chord;

        dx = errX;
        dy = errA;

        *a0=*a0 + dy;
        nominal_Length = nominal_Length + dx;

    } while ((fabs(errX)>e->Esail->m_Linear_Tol|| fabs(New_Chord*errA)>e->Esail->m_Linear_Tol) && (100>loops++));
#ifdef 	 _PDEBUG_
    if(loops>100-1) printf("solved in %d loops dx %f dy %f  (FLOAT_TOL=%f)\n",loops,dx,dy,pso->Esail->m_Linear_Tol);
#endif		
    *arc =(float) PC_Polyline_Length(ko,pout);
    if(*c !=ko) {
        *c = ko;
        *p = (VECTOR*)REALLOC (*p,sizeof(VECTOR)*(ko+1));
    }
    for (k=0;k<ko;k++) {
        (*p)[k].x = pout[k].x;
        (*p)[k].y = pout[k].y;
        (*p)[k].z=  pout[k].z;
    }
    /*      At end, measure chord and chordangle
   Increment initial angle
   Increment ref length LOOP */

    /*      pass back the arc length and initial angle ready for use next time
  and in p the finished polyline
  maybe we store the nominal_Length value to speed up the routine next time*/

    if(N) {
        RXFREE(x); RXFREE(a); RXFREE(crank);
    }
    RXFREE(pout);
    return(1);
}
/************************************************************/


int Find_Curves_Between(RXEntity_p e1,RXEntity_p e2,sc_ptr **list,int *n) {

    /* find a curve 'connecting' p1 and p2 */
    Site  *p1,*p2;
    RXEntity_p c;
    sc_ptr  s1,s2;

    c=NULL;
    if(!e1||!e2) return(0);

    p1 = (Site *)e1; p2 = (Site *)e2;

    if( p1->CUrve[0] && (p1->CUrve[0]==p2->CUrve[0]))  c=p1->CUrve[0];
    /* combinations : p1.curve.end* = p2  or p2.curve.end* = p1	*/
    else  {
        if( p1->CUrve[0])
            s1= (sc_ptr  )p1->CUrve[0];
        else
            s1=0;
        if(p2->CUrve[0] )
            s2 = (sc_ptr  )p2->CUrve[0];
        else
            s2=0;

        if (s1 && s1->End1site == e2)
            c=p1->CUrve[0];
        else if  (s1 && s1->End2site == e2)
            c=p1->CUrve[0];
        else if  (s2 && s2->End1site == e1)
            c=p2->CUrve[0];
        else if  (s2 && s2->End2site == e1)
            c=p2->CUrve[0];
    }
    if(!c) {
        /* if any p1->pslist[k]->dataptr->owner = p1->pslist[k]->dataptr->owner  thats the curve */
        int k1,k2;
        struct SIDEREF s;
        RXEntity_p c1,c2;
        PSIDEPTR  ps1, ps2;
        for(k1=0;k1<p1->PSCount();k1++) {
            s =   p1->PSList(k1 );// p1->m_ps list[k1];
            ps1 = s.pp;
            c1=ps1->sc;
            for(k2=0;k2<p2->PSCount();k2++) {

                s = p2->PSList(k2);
                ps2 = s.pp;
                c2=ps2->sc;
                if(c1==c2) {
                    c=c1;
                    break;
                }
            }
            if(c) break;
        }
    }

    if(c) {
        *list=(sc_ptr *)REALLOC(*list, (*n+1)*sizeof(RXEntity_p *));
        (*list)[(*n)] =(sc_ptr) c; (*n)++;
    }
    else
        e1->OutputToClient ("(Find_Curves_Between)No curve found",1);


    return(0);

}
int  Compute_Patch(RXEntity_p e) {
    struct PC_PATCH *b;
    class RXCompoundCurve *   cc;
    RXEntity_p sco;
    sc_ptr  sc;
    /* creates the list of psides. A batpatch is a list of curves
 A 'basecurve zero'	batten is between nodes
 It Uses the e1sites as it consists of a partial seamcurve
  All other batpatches consist of entire seamcurves

  */
    int i,j,OK;
    char buf[128];

    if(!strieq(e->type(),"patch")) return(0);
    b = (PC_PATCH *)e->dataptr;
    if(b->cc->TYPE ==PCE_DELETED) {
        cout<< "Compute_batpatch on deleted seamcurve"<<endl;
        sprintf(buf,"Deleting Bat-patch entity : '%s'",e->name());
        e->OutputToClient (buf,1);
        e->Kill( );
        return(0);
    }
    cc=(class RXCompoundCurve *  )b->cc;
    if(cc->pslist) {RXFREE (cc->pslist);	 cc->pslist=NULL; cc->npss=0;}
    // reconstruct the cc's  pslist. Not complete if the BP is partial.
    for(i=0;i<cc->m_ccNcurves;i++) {
        sco = cc->m_ccCurves[i];
        sc=(sc_ptr  )sco;
        OK=1;
        if(b->partial) OK=0;
        for(j=0;j<sc->npss;j++) {
            PSIDEPTR ps;
            RXEntity_p node;
            ps  = (sc->pslist[j]);
            node = ps->Ep(0);
            if(b->partial && node==cc->m_ccE1sites[i] ) OK=1;
            if(OK) {
                cc->pslist=(PSIDEPTR *)REALLOC(cc->pslist,(cc->npss+1)*sizeof(RXEntity_p *));
                cc->pslist[cc->npss]=ps; cc->SetRelationOf(ps,parent,RXO_PSIDE_OF_CC,cc->npss); // not sure about 'parent'
                (cc->npss)++;
            }
            node = ps->Ep(1);
            if(b->partial && node==cc->m_ccE2sites[i]) OK=0;
        }
    }


    assert (b->m_type ==PATCH); {		   /* hard-code for radial corner */
        RXEntity_p ef =b->mat;
        QString qval; bool ok;
        class RXLayer *fabdata= (class RXLayer *)ef ;
        RXSitePt *c = dynamic_cast<RXSitePt*>(b->m_corner);
        if(e->AttributeFind("uniform")) {
            ON_3dVector chord,ylb , l_z;
            float l_clength;
            double angle = 0.0;
/* ref direction is parallel to chord, but rotated thru angle=<angle> */

            chord =b->e[1]->ToONPoint() - b->e[0]->ToONPoint() ;
            l_clength = chord.Length();

            if (l_clength==(float)0.0) {
                char s[256];
                sprintf(s," zero length chord %s in uniform patch" , e->name());
                e->OutputToClient (s,2);
                fabdata->matstruct.m_Data[0]=(float)1.0;
            }
            else {
//                if((sp =strstr(e->attributes,"angle="))) {
//                    sp+=6;
//                    angle= (float) atof(sp)/57.29577951;
//                }
                if(e->AttributeGet("$angle",qval))
                {
                    angle = qval.toDouble(&ok);
                    if(!ok)
                        angle=0;
                    else
                        angle=angle /57.29577951;
                }


                l_z = e->Normal();
                chord.Unitize ();
                ylb = ON_CrossProduct(l_z,chord); ylb.Unitize ();
                fabdata->matstruct.m_Data[0] = chord.x * cos(angle) + ylb.x*sin(angle);
                fabdata->matstruct.m_Data[1] = chord.y * cos(angle) + ylb.y*sin(angle);
                fabdata->matstruct.m_Data[2] = chord.z * cos(angle) + ylb.z*sin(angle);
            }
        }
        else	  /* radial */
        {
            // xx =  (float*) fabdata->matstruct.Data;
            fabdata->matstruct.m_Data[3]	= c->x;
            fabdata->matstruct.m_Data[4]	= c->y;
            fabdata->matstruct.m_Data[5]	= c->z;

            fabdata->matstruct.m_Data[7] = (float) sqrt(  c->DistanceTo(*((RX_FESite*)b->e[0]))
                                                          *  c->DistanceTo(*((RX_FESite*)b->e[1]) )  );

            // mem cpy(fabdata->matstruct.Data,xx,8*sizeof(float));//but arent the ptrs the same??


        }
    }
    return(1);
}



int	Compute_Next_Batpatch(SAIL *p_sail){
    int iret;
    iret=0;
    ent_citer it;
    for (it =  p_sail->MapStart(); it !=  p_sail->MapEnd(); it=p_sail->MapIncrement(it)){
        RXEntity_p e  = dynamic_cast<RXEntity_p>( it->second);

        if((e->TYPE==PATCH) && ! e->Needs_Resolving) {
            struct PC_PATCH*p=(PC_PATCH *)e->dataptr;
            if(!p->inserted){
                if(Insert_Patch(e))
                    p->inserted=1;
                iret++;
            }
            Compute_Patch(e);
            if(iret) return(iret);
        } // if patch
        else if((e->TYPE==POCKET) && ! e->Needs_Resolving) {
            class RX_FEPocket*p=(RX_FEPocket *)e;
            if(!p->inserted){
                if(p->Insert_Pocket())
                    p->inserted=1;
                iret++;
            }
            p->Compute_Pocket();
            if(iret) return(iret);
        } // if pocket

    }
    return(iret);
}
int Find_Panel_Opposite(Panelptr *pan,PSIDEPTR panps) {
    int k,k2;
    for(k=0;k<2;k++) {
        Panelptr oldpan = panps->leftpanel[k];
        if(oldpan==*pan) {
            k2 = increment(k,2);
            *pan = panps->leftpanel[k2];
            break;
        }
    }
    return(1);
}
int Find_All_CrossesForPatch(RXEntity_p e,struct PC_INTERSECTION**is,int *ni) {

    /*
  A more intelligent search routine which makes use of panel connectivity
 1)  To find the 'current' panel
  the first point of the batpatch.p->e[0]. call this e1p
  look at e1p->dataptr->pslist[0-psno]. called pse
   look at pse->dataptr->leftpanel[0-1]
   if it is not NULL, use it to seed the search.

   if the search gets going, fine.
   If it fails to find a cross on the first panel,
   try the next pside in the pslist

  2) Seed with the 'current' panel.
   search all the psides on that panel to look for a cross
   If a cross is found, change the current panel to be the one on the other side of the pside
   Keep going while the current panel is not NULL. */

    struct PC_PATCH *p = ( struct PC_PATCH *) e->dataptr;
    double s1,s2 = 0.0,s1max,s2max;
    VECTOR v;
    int found,iret,start;
    Site *e1;
    int kseed,lseed,panside;

    PSIDEPTR panps=NULL;
    Panelptr panel, panseed;

    s1=  e->Esail->m_Linear_Tol;/* not close to batten start  */
    s1max= p->m_arc;
    start=1;
    v.x=v.y=v.z=0;
    e1 = p->e[0];
    for(kseed=0;kseed<e1->PSCount();kseed++) {
        PSIDEPTR ps =  e1->PSList(kseed).pp;
        for(lseed=0;lseed<2;lseed++) {
            panseed = ps->leftpanel[lseed];
            if(panseed) {
                panel=panseed;
                do {
                    found=0;
                    for (panside=0;panside< panel->m_psidecount;panside++) {
                        panps=panel-> m_pslist[panside];

                        if(panps->sc->edge)
                            iret=0;
                        else {
                            s2 = (panps->End1off->Evaluate( panps->sc,1));
                            s2max = (panps->End2off->Evaluate( panps->sc,1));
                            if(s2max<=s2){
                                char str[256];
                                sprintf(str,"s2max (%f) < s2 (%f) in %s side %d\n",s2max,s2, panel->name(),panside);
                                e->OutputToClient (str,1);
                            }
                            iret=Find_Polyline_Intersection(p->p,panps->sc->p[1],p->c,panps->sc->c[1],&s1,&s2,&s1max,&s2max,&v);
                        }
                        /* 1 for cross, 2 for parallel 0 for none */
                        if(iret==1) {
                            if(s1< p->m_arc- e->Esail->m_Linear_Tol)  {	  /* not close to batten end */
                                found=1;
                                start=0;

                                *is = (PC_INTERSECTION *)REALLOC (*is,((*ni)+1 )*sizeof(struct PC_INTERSECTION));
                                if(!(*is)) {
                                    e->OutputToClient ("Realloc in batpatch failed ",2);
                                    continue;
                                }
                                ((*is)[*ni]).s1 = s1;
                                ((*is)[*ni]).flag =1;
                                ((*is)[*ni]).s2 = s2;
                                ((*is)[*ni]).seam = panps->sc;
                                PC_Vector_Copy(v,&(((*is)[*ni]).v));
                                (*ni)++;
                            }
                            else {

                            }
                            s1 = s1 + (float) e->Esail->m_Linear_Tol;/* not close to last one */
                            break;
                        }

                    }
                    if(found) {	/* a match found */
                        Find_Panel_Opposite(&panel,panps);
                    }
                    else {
                        if(!start)return(*ni);
                        break;	/* out of do while */
                    }
                }
                while(panel);
            }	/* end of (if pan) */
        }   /* end of (for lseed) */
    }	/* end of (for kseed) */
    return(*ni);
}


int Compute_Patch_Geometry(RXEntity_p e) {

    /*  We have two end points. They may be SITEs or offsets on curves or seams.
First we find them. Then we call up the basecurve. We rotate it to fit
between the endpoints taking the adjectives into account.
We insert a HOOPS polyline and return the key of the containing segment. 
If the attribute contains the word 'moulded' we pull the line to the mould
  */

    char flag[10];

    ON_3dVector xl,yl;
    ON_3dPoint e1,e2;
    float chord;
    int k, Xabs,Yabs,YasX,iret=1;
  //  char*att=e->attributes;
    struct PC_PATCH*p= (PC_PATCH *)e->dataptr;
    class RXBCurve * bceptr =dynamic_cast<class RXBCurve *>( p->basecurveptr) ;
    SAIL *sail = e->Esail;
    RXEntity* mould = sail->GetMould();

    switch (bceptr->TYPE) {
    case SEAMCURVE:{	// copy its curve[1] into p->p;
        sc_ptr sc = (sc_ptr  )bceptr;

        if(p->p) RXFREE(p->p);
        p->p=(VECTOR *)MALLOC(sc->c[1]*sizeof(VECTOR));
        memcpy(p->p, sc->p[1], sc->c[1]*sizeof(VECTOR)); cout<< " memcpppy!"<<endl;
        p->c = sc->c[1];
        p->end1ptr = p->e[0] = sc->End1site;
        p->end2ptr=	 p->e[1] = sc->End2site;
        break;
    }

    case TWODCURVE :
    case BASECURVE :
    case THREEDCURVE :
    {
        class RXBCurve *bcptr =  bceptr ;
        ON_3dVector l_z;
        float depth=p->d;

        int bc=(*bcptr).c;
        int err = 0;
        VECTOR* base = (VECTOR *)MALLOC(sizeof(VECTOR)*(bc+1));


        if(!base) e->OutputToClient ("out of mem in panel",3);

        HC_Open_Segment(e->type());
        e->hoopskey = HC_KOpen_Segment(e->name());
        HC_Flush_Geometry(".");

        if(!Get_Position((*p).end1ptr,(*p).End1dist,(*p).end1sign+1,&e1) )err=1;
        if(!Get_Position((*p).end2ptr,(*p).End2dist,(*p).end2sign+1,&e2) ) err=1;
        if(err) { iret= -1; goto End;}
        xl = e2-e1;
        chord=xl.Length ();

        if (chord==(float)0.0) {
            char string[256];
            const char *name1 , *name2;
            name1= (*(*p).end1ptr).name();
            name2= (*(*p).end2ptr).name();
            sprintf(string,"Zero length between %s(%S) and %s(%S)",name1,p->End1dist->GetText().c_str (),name2,p->End2dist->GetText().c_str());
            e->OutputToClient (string,2); iret= -1; goto End;;
        }
        xl.Unitize ();
        l_z = e->Normal();
        yl  = ON_CrossProduct(l_z,xl );// HC Compute_Cross_Product(&(l_z),&xl,&yl);

        Xabs=0;


        Yabs = 0; YasX=0;
        if (e->AttributeFind("$yscaled")) {
            Yabs=0;
            YasX=0;
        }
        else if (e->AttributeFind("$yasx")) {
            Yabs= 0;
            YasX=1;
        }
        else if (e->AttributeFind("$yabs")) {
            Yabs=1;
            YasX=0;
        }
        /* extract the base curve offsets */


        Scale_Base_Curve((*bcptr).p, base,bc,depth,Xabs,Yabs,YasX,(double)0.0);


        /* base is the base-curve. Y in m and x from 0 to 1 */


        strcpy(flag,"curve");

        Make_Fanned_Batpatch(e,flag,&p->m_arc,&(*p).strt_A,chord,&bc,&base,(*p).a,0 );	 /* sets arc */

        (*p).c=bc;
        p->p = (VECTOR *)REALLOC(p->p,(bc+1)*sizeof(VECTOR));
        for (k=0;k<bc;k++){
            (((*p).p)[k]).x = e1.x + xl.x*base[k].x +  yl.x*base[k].y ;
            (((*p).p)[k]).y = e1.y + xl.y*base[k].x +  yl.y*base[k].y ;
            (((*p).p)[k]).z = e1.z + xl.z*base[k].x +  yl.z*base[k].y ;
        }
        (p->p[bc-1]).x  = e2.x;
        (p->p[bc-1]).y  = e2.y;
        (p->p[bc-1]).z  = e2.z;
        (*p).end1angle=0.0;
        (*p).end2angle=0.0;
        if(e->AttributeGet("moulded")) {
            PC_Pull_Pline_To_Mould(mould,&(p->p), &bc,1,e->Esail->m_Linear_Tol);
        }
        HC_Set_Color("lines=blue");
        RX_Insert_Polyline(bc,&(p->p[0].x),e->GNode(),0);

End:	 HC_Close_Segment();
        HC_Close_Segment();

        RXFREE(base);
        break;
    }
    default:
        e->OutputToClient ("this Patch has bad basecurve type",3);

    }; // end switch

    return(iret);
}
int Bat_Sort_Fn(const void *va,const void *vb) {
    struct PC_INTERSECTION *ia = (struct PC_INTERSECTION *)va ;
    struct PC_INTERSECTION *ib = (struct PC_INTERSECTION *) vb;

    if (fabs(ia->s1 - ib->s1) <FLOAT_TOLERANCE){
        int iret;
        iret = strcmp(ia->seam->name(),ib->seam->name());
        if(!iret) {
            iret = strcmp(ia->seam->type(),ib->seam->type() );
        }
        return(iret);
    }
    if ( ia->s1 < ib->s1) return(-1);
    if ( ia->s1 > ib->s1) return( 1);
    return 0;
}

int Put_Crosses_In_Poly(struct PC_INTERSECTION*is,int ni, VECTOR **p,int*c){
    /* return in is[k].i the polyline knot index of intersection k */

    int k;

    int ka; // , N=ni;
    int kb;

    double x;
    double *s  =(double*)MALLOC( (2+ni+(*c)) * sizeof(double));
    if (s==NULL) g_World->OutputToClient ("out of memory",3);

    for(kb=0;kb<ni;kb++) {
        s[0]=0.0000;
        for (k=1;k<*c;k++) {
            s[k] = s[k-1] + PC_Dist(&((*p)[k-1]),&((*p)[k]));  /* arc lengths of the knots*/
        }

        x =  is[kb].s1;
        for (ka=0;ka<(*c)-1;ka++) {

            if (x>=s[ka]-FLOAT_TOLERANCE && x<(s[ka+1]-FLOAT_TOLERANCE)) {  /* x is between k and k+1 */

                if (FLOAT_TOLERANCE < fabs(x-s[ka])){
                    Insert_Into_Poly(p,c,is[kb].v,ka);
                    ka++;
                    is[kb].i =ka;
                }
                else{
                    if(Peter_Debug)printf(" duplicate at x=%f\n",x);
                    is[kb].i=ka;
                }
                break;
            }
        }
    }
    RXFREE(s);
    return(1);
}

int Create_Bat_BasecurveForPatch(int k,struct PC_PATCH *p,struct PC_INTERSECTION*a,int ni) {
    char bcname[64];
    int j,i1,bc;
    ON_3dVector l_z;
    RXEntity_p cco=p->cc;
    class RXBCurve*bcp = new class RXBCurve( cco->Esail);
    assert("  Create_Bat_BasecurveForPatch "==0);
    assert(cco);
    sprintf(bcname,"%s%d",  cco->name(), k);
    /* the basecurve is from knot a[k].i to knot a[k+1].i
 unless k is 0, in which case first is 0
 or k is  ni, in which case last is p->c*/
    if(ni==0) {
        i1=0;
        bc = p->c;
    }
    else if(k==ni) {
        i1=a[k-1].i;
        bc = p->c - i1;
    }
    else if(k==0) {
        i1=0;
        bc = a[0].i + 1;
    }
    else {
        i1=a[k-1].i;
        bc = a[k].i - i1 + 1;
    }
    bcp->p = (VECTOR *)MALLOC((bc+1)*sizeof(VECTOR));
    bcp->c=bc;
    for (j=0;j<bc;j++) {
        PC_Vector_Copy(p->p[j+i1],&(bcp->p[j]));
    }
    l_z =cco->Normal();
    PC_Normalise_Polyline(bcp->p,bcp->c,l_z );
    RXEntity *bco =cco->Esail->Insert_Entity("basecurve",bcname,bcp,(long)0," "," ! basecurve made by batpatch");
    cout<< "TODO: p->owner->SetRelationOf(bco,spawn,RXO_BC_OF_PATCH"<<endl;
    return(1);
}

int Insert_Patch(RXEntity_p e) {
    /* First find the curve position
 Then get the crosses
 and the parallels
 generate the seamcurves
 Build up the curve list
 set the material on the psides generated
 */

    /* First find the curve position*/
    struct PC_PATCH*p=(PC_PATCH *)e->dataptr; /*,char*att,float depth,long int *key*/
    RXEntity_p site1, site2;
    struct PC_INTERSECTION*is=NULL; int ni;
    sc_ptr  sc;
    class RXCompoundCurve *  cc = (class RXCompoundCurve *  )p->cc;
    Site *s1,*s2;

    char line[256];
    double foffset,arc;
    int k, Need_Crosses;

    /* the following section requires ps lists on nodes */

    assert(p->m_type ==PATCH);
    if (!p->basecurveptr) return(1);

    /* There is a bug in inserting batpatches in TPN designs.
    Sometimes the red-space coords can be taken from the wrong scurve
  This is the trap. It deletes offending batpatches */
    s1 = (Site *)p->e[0];
    s2 = (Site *)p->e[1];
    if(s1->PSCount()>2 ||s2->PSCount()>2  )	{
        if (e->Esail->GetFlag(FL_ISTPN)){
            char s[256];
            sprintf(s,"(TPN) cannot insert batten/patch\n %s\n It ends on a panel corner" ,e->name());
            e->OutputToClient (s,2);
            p->cc->Kill();
            e->Kill( );
            return(0);
        }
    }

    Compute_Patch_Geometry(e);  /* REALLOCs p */
    /*Then get the crosses   and the parallels	*/
    Need_Crosses = 1;
    if(p->m_type==PATCH && p->basecurveptr->TYPE == SEAMCURVE){
        sc_ptr   sc=(sc_ptr  )p->basecurveptr;
        if(!sc->sketch)
            Need_Crosses = 0;
    }


    if(Need_Crosses){ // if its NOT a PATCH SC-draw type
        ni=0;
        Find_All_CrossesForPatch(e,&is,&ni) ; // uses p->p to make (is,ni)

        for(k=0;k<ni;k++) {
            sc = (sc_ptr  )is[k].seam;
            arc = sc->GetArc(1);
            foffset = is[k].s2/arc* 100.0;
            sprintf(line,"relSite: %s%d : %s : %f%% :! made by batpatch",cc->name(), k,is[k].seam->name(),foffset);
            is[k].site=Read_Relsite_Card(e->Esail ,line);
            is[k].site->Needs_Finishing=!is[k].site ->Finish();
            e->SetRelationOf(is[k].site,child,RXO_SITE_OF_PATCH);
            is[k].seam->Break_Curve();   /* EXPERIMENT MAYBE WRONG 12/1/95 */
        }
        /* sort */

        qsort (is,ni,sizeof(struct PC_INTERSECTION),Bat_Sort_Fn);


        Put_Crosses_In_Poly(is,ni, &(p->p),&(p->c));

        /* is[k].i is now the polyline knot index of intersection k */
        /* a note on dependents. THe SCs are dependents of the sites, so they
     dont need to be pushed onto the batpatch deplist */

        p->partial=0;
        cc->m_ccCurves = (sc_ptr *)CALLOC((ni+2),sizeof(RXEntity_p ));

        cc->m_ccRevflags = (int*)CALLOC((ni+2),sizeof(int));
        site1=p->e[0];
        for(k=0;k<ni;k++) {
            site2=is[k].site;
            Create_Bat_BasecurveForPatch(k,p,is,ni);
            Create_Bat_CurveForPatch(site1,site2,k,p);
            cc->m_ccCurves[k]->Break_Curve();
            site1=site2;
        }
        site2 = p->e[1];
        Create_Bat_BasecurveForPatch(ni,p,is,ni);
        Create_Bat_CurveForPatch(site1,p->e[1],ni,p);
        cc->m_ccCurves[ni]->Break_Curve();
        cc->m_ccNcurves=ni+1;

        if(cc->m_ccNcurves==1) {
            Site*sp;
            /* 	error(" fixup for no crosses",1);   */
            sp = (Site*)site1;
            if(sp) {
                sc_ptr scp=(sc_ptr) sp->CUrve[0];
                if(scp)
                    scp->Make_Pside_Request=2;
            }
        }

        if (is)
            RXFREE(is);
    }
    /* here, if it is a patch, we set the material of the (one) pside of
     the curves to the patch material. */
    assert ( p->m_type==PATCH  ); {
        int k,*side,ninside,s2;
        PSIDEPTR  ps;
        PSIDEPTR *insides;

        Site *e0 =dynamic_cast <Site*> (p->m_corner);
        insides = (PSIDEPTR *)MALLOC((1+ cc->m_ccNcurves)*sizeof(RXEntity_p ));
        side=(int *)MALLOC((1+ cc->m_ccNcurves)*sizeof(int));
        if(Peter_Debug) printf("patch %s focus  %s at (%f %f %f)\n", e->name(),p->m_corner->name(),  e0->x,e0->y,e0->z);
        for(k=0;k<cc->m_ccNcurves;k++) {
            sc_ptr  sco = (sc_ptr  )cc->m_ccCurves[k];
            ps = sco->pslist[0];

            ON_3dPoint ep1=cc->m_ccCurves[k]->Get_End_Position(SC_START); //e1.x=ep1.x;  e1.y=ep1.y; e1.z=ep1.z;
            ON_3dPoint ep2=cc->m_ccCurves[k]->Get_End_Position(SC_END); //e2.x=ep2.x; e2.y=ep2.y; e2.z=ep2.z;

            insides[k]= ps;

            ON_3dVector l_v =cc->m_ccCurves[k]->Normal() ;
            side[k] = 1 + Compute_Sign(ep1,ep2,ep1,e0->ToONPoint(),l_v);

            /*Push_Entity(p->mat,&(ps->m_psmats[side[k]]) );*/ p->mat->SetRelationOf(ps,PS_LAYER_RELATION,RXO_LAYER,side[k]);
        }
        if(Peter_Debug) printf("WAS  %s focus  %s at (%f %f %f)\n\n", e->name(),p->m_corner->name(),  e0->x,e0->y,e0->z);
        ninside = cc->m_ccNcurves;
        /* we now have to walk 	along the psides between the patch and the corner and push the
material onto the matlist of each. Alternatively we walk from the focus
  METHOD
  a pside has its el[0] and el[1]  and er[0] and er[1], which are other psides
  We start a search from  ps->el[side/2]  which is a pside inside the patch.
  This gets added to a list.
*/			
        for(k=0;k<cc->m_ccNcurves;k++) {
            sc_ptr  sco = (sc_ptr  )cc->m_ccCurves[k];
            ps =  sco->pslist[0];
            s2 = 1-side[k]/2;
            Set_Material_Walk((ps->er[s2]),&insides,&ninside,p->mat);
            s2=1-s2;
            Set_Material_Walk((ps->el[s2]),&insides,&ninside,p->mat);
        }
        RXFREE(insides);
        RXFREE(side);
    }/* end of (if PATCH) */
    return(1);
}

double Batten_Spline(double x,double*k) {
    double f1,f2,f3,ret,k0sq,k1sq;

    k0sq=k[0]*k[0];
    k1sq=k[1]*k[1]	;
    f3 = atan(k[2])/3.14159265 + 0.5;
    if(x>0.0)
        f1 =     x  *(1.0-pow(   x  ,k0sq));
    else
        f1=0.0;
    if(x<1.0)
        f2 = (1.0-x)*(1.0-pow((1.0-x),k1sq));
    else
        f2=0.0;
    ret=(f3*f1 + (1.0-f3)*f2)*k[3];
    return(ret);
}

double Batten_Curvature(double x,double*k) {
    double f1dd,f2dd,f3,k0sq,k1sq,u,ret;
    k0sq=k[0]*k[0];
    k1sq=k[1]*k[1]	;
    f3 = atan(k[2])/3.14159265 + 0.5;
    /*if(fabs(k0sq-2.0) <FLOAT_TOLERANCE) return(0.0);
  if(fabs(k1sq-2.0) <FLOAT_TOLERANCE) return(0.0);*/

    if(x>0.0)
        f1dd = -pow(   x   ,k0sq-1.0) * k0sq * (k0sq+1.0);
    else f1dd=0.0;
    u = 1.0-x;
    if(u>0.0) f2dd =  -pow(   u   ,k1sq-1.0) * k1sq * (k1sq+1.0);
    else  f2dd=0.0;
    ret= (f3*f1dd + (1.0-f3)*f2dd)*k[3] ;
    return(ret);
}

double dBSdk(double x, double*k,int l) {
    /* the partial derivative of the batten function wrt K[l] */
    double y1,y2, dk=0.001;

    k[l]=k[l]-dk;
    y1 = Batten_Spline(x,k);

    k[l]=k[l] + 2.0* dk;
    y2 = Batten_Spline(x,k);
    k[l]=k[l]-dk;
    return((y2-y1)/(2.0*dk) );
}
int Compute_Batten(RXEntity_p e) {
    struct PC_BATTEN *b;
    /* prepares a batten (base batten) for gridding by generating  the function for EI
  The  function is interrogated by Batten_Stiffness
  
  This version uses the Powerbatten method	*/

    int i,l,n,count=0,lmax=0; //gcc
    double k[4] = {1,1,0,2};
    double err,olderror,de,dv[4],mx,bsv,relaxfactor=1.0,total,area;
    double diasq;

    if (!e->NeedsComputing())  return(1);/* aug 1996. was 0. Its OK not to need comping */
    e->SetNeedsComputing(0);

    b = (PC_BATTEN*)e->dataptr;
    double l_length,l_force,l_ea;
    RXExpressionI *q ;
    q = (e->FindExpression (L"length",rxexpLocal));
    l_length=q->evaluate () ;
    q = (e->FindExpression (L"force",rxexpLocal));
    l_force=q->evaluate () ;

    diasq = sqrt(64.0 * l_length*l_length*l_force/(3.142*3.142*3.142*41.0E9 ) );
    l_ea = (float) (41.0E9 * 3.142 * diasq / 4.0);

    n = b->c-1;	/* internal points only */
    err = 1000000000000.0;
    HC_Open_Segment(e->name());
    HC_Open_Segment("last"); HC_Close_Segment();
#ifdef HOOPS
    HC_Set_Window(-1.0,0,0.0,1.0);
    HC_Set_Camera_By_Volume("stretched",-.1,1.1,-0.2,1.3);

    HC_Open_Segment("");
    HC_Set_Color("black");
    HC_Insert_Text(0.5,1.25,0.0,"batten");
    HC_Insert_Text(0.5,1.1,0.0,e->name());
    HC_Insert_Polyline(b->c,b->p);
    HC_Close_Segment();
#endif
    if(n>1){
        do {
            olderror = err; count++;
            /* 1 error = sum across points of (yj-F(xj,k)^2 */
            err = 0.0;
            area = 0.0;
            for(i=1;i<n;i++) {
                bsv =  Batten_Spline((double)b->p[i].x,k);
                de = b->p[i].y -bsv;
                err = err + de*de;
                area = area + b->p[i].y* 0.5*(b->p[i+1].x- b->p[i-1].x);
            }

            /* get the three partial derivatives dedki = 2 sum[(F(xj,k)-yj)* df/dki  ]  */
            for(l=0;l<4;l++) {
                dv[l]=0.0;
            }
            for(i=1;i<n;i++) {
                bsv = Batten_Spline((double)b->p[i].x,k);
                de = b->p[i].y -bsv ;
                for(l=0;l<4;l++) {
                    dv[l] = dv[l] - 2.0*de*dBSdk((double)b->p[i].x, k, l);
                }
            }

            /* change the ki with the greatest partial derivative  */
            total=0.0;
            mx = dv[0]-1.0;
            for(l=0;l<4;l++){
                if(fabs(dv[l])>mx) {
                    mx=fabs(dv[l]);lmax=l;
                }
                total=total+ dv[l]*dv[l];
            }
            if(total < 0.00001) total=.00001;

            if(err>olderror) relaxfactor = relaxfactor/2.0;
            if(err>olderror) olderror=2.0*err; /* keep going */
#ifdef HOOPS			  
            HC_Open_Segment("this");
            HC_Set_Color("magenta");
            HC_Set_Line_Weight(2.0);
            HC_Restart_Ink();
            for(i=0;i<21;i++) {
                float xx,yy;
                xx = (float)i/(float) 20.0;
                yy= (float) Batten_Spline((double)xx,k);
                HC_Insert_Ink(xx,yy,yy);
            }
            HC_Set_Color("blue");
            HC_UnSet_Line_Weight();
            HC_Close_Segment();
            HC_Delete_Segment("last");
            HC_Move_Segment("this","last");
#endif

            /* update the Ks */
            k[lmax]=k[lmax] - err/dv[lmax]*relaxfactor/3.0;

        } while(count<100&& err/olderror <0.999 && err/area>0.0001 );
        HC_Update_Display();
    } /* if n >1 */

    for(l=0;l<4;l++){
        b->k[l]=k[l];
    }
    b->a=(float*)CALLOC(20,sizeof(float)); /* WRONG */


    HC_Close_Segment();
    HC_Delete_Segment(e->name());
    return(1);
}


int Create_Bat_CurveForPatch( RXEntity_p site1,RXEntity_p site2,int k,struct PC_PATCH *p) {
    char line[256];
    char bcname[64];
    class RXCompoundCurve * cc=(class RXCompoundCurve *   )p->cc;
    SAIL *sail = site1->Esail;
    sprintf(bcname,"%s%d",  p->cc->name(), k);
    if(sail->GetMould())
        sprintf(line,"curve: %s%d : %s : %s : %s :0.0 :fanned Xscaled YasX Yabs draw moulded: : : :!made by batpatch", cc->name(), k,site1->name(), site2->name(),bcname);
    else
        sprintf(line,"curve: %s%d : %s : %s : %s :0.0 :fanned Xscaled YasX Yabs draw : : : :!made by batpatch", cc->name(), k,site1->name(), site2->name(),bcname);

    cc->m_ccCurves[k]=(sc_ptr) Read_SeamCurve_Card(sail,line);
    cc->m_ccCurves[k]->Needs_Finishing=!cc->m_ccCurves[k]->Finish();
    cc->m_ccCurves[k]->generated = 30000; /* for the delete routine */
    return(1);
}








