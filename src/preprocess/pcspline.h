	/* this is pcspline.h
	
	created Feb 1997. 2D spline routines for Gauss etc */

#ifndef PCSPLINE_16NOV04
#define PCSPLINE_16NOV04

#include "vectors.h"

#define ONE_DIM	1
#define TWO_DIM	2
#define THREE_DIM	3

struct PC_SPLINESEG{
	int status;
	int order;	  /* 3 for a cubic. no of coeffs is one more*/
	double x1,x2;
	double c[10];
};
typedef struct PC_SPLINESEG Spline_Seg;

struct PC_SPLINE{
	int status;
	int type;
	int ns;	   /* no of segs */
	double chord,arclength;
	struct PC_SPLINESEG *segs;
};
typedef struct PC_SPLINE Spline;

			 /*	 Integrate to get the spline coefficients	for Y as fn of red-space S	 */
int Get_Gauss_Spline(sc_ptr sc, int nks,double *X, double*Kw,Spline *s);
int	Sample_Enough_Spline_Points(Spline *s, VECTOR **Yout,int *nout, double l1, double l2, double l3);	
double  PC_Spline_Eval(Spline *s, double X);
double  PC_Spline_Gradient(Spline *s, double x);
double   PC_Spline_Curvature(Spline *s, double X);
int Print_Spline(Spline *s);
double Simpson_Correction(int nks, double *X, double*Kw);

 
#endif //#ifndef PCSPLINE_16NOV04
