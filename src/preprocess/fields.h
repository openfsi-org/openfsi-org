 /* fields.h. prototypes for fields.c */
#ifndef _FIELDS_H
#define _FIELDS_H

#include "griddefs.h"
#include "fielddef.h"

EXTERN_C int Resolve_Field_Card(RXEntity_p e);
EXTERN_C int Set_Field_Type(struct PC_FIELD *field);
EXTERN_C int Draw_All_Fields(SAIL *sail);
EXTERN_C int  Draw_Field(RXEntity_p ep); 

#endif//#ifndef _FIELDS_H

