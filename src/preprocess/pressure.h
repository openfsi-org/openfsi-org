#ifndef _PRESSURE_H_
#define  _PRESSURE_H_

#include "griddefs.h"

struct PC_PRESSURE{
	RXEntity_p m_p0wner;
	int m_flags;
	struct PC_TABLE *m_table; // has nr,nc
}; //struct PC_PRESSURE
typedef struct PC_PRESSURE Pressure;

int Resolve_Pressure_Card (RXEntity_p e) ;

#endif  //#ifndef _PRESSURE_H_

