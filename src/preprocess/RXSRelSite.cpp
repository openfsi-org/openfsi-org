#include "StdAfx.h"
#include <QDebug>
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXSeamcurve.h"
#include "RXOffset.h"
#include "RXQuantity.h"
#include "RXCurve.h"
#include "RXPside.h"
#include "RXPanel.h"
#include "RXAttributes.h"
#include "RXRelationDefs.h"

#include "interpln.h"
#include "etypes.h"

#include "f90_to_c.h"
#include "cfromf.h"
#include "global_declarations.h"

#include "RXExpression.h"
#include "iangles.h"
#include "geodesic.h"
#include "redraw.h"
#include "cut.h"

#include "stringutils.h"

#include "arclngth.h"
#include "panel.h"

#include "polyline.h"
#include"RX_UI_types.h"

#ifndef linux
#include "..\stringtools\stringtools.h" // defines FromUtf8 and ToUtf8
#else
#include "../../stringtools/stringtools.h"
#endif

#include "rxfixity.h"
#include "RXSRelSite.h"



RXSRelSite::RXSRelSite(void)
{	assert("dont use the default RXSRelSite constructor"==0);
    Init();
}


RXSRelSite::RXSRelSite(class RXSail *p ) :
    RX_FESite(p),
    RXEntity(p) ,
    ctype(-999),
    OriginEnt(0),
    CrossAngle(0),
    m_psList(0),
    m_psno(0)
{
    assert(this->GetSail()==p);// awkward to have the sail recorded separately in RXEntity and in RX_FEObject

    CUrve[0]=0; CUrve[1]=0; /* IF both  are NULL, the node is not in the middle of a curve */
    Offsets[0]=0; Offsets[1]=0;
    this->SetParent(p);
}
RXSRelSite::~RXSRelSite(void)
{
    this->CClear ();

}
int  RXSRelSite::Init(void)
{
    assert(0);
    x=y=z=ON_UNSET_FLOAT;
    SetN(RXSITEINITIAL_N);    assert(0);
    m_u=m_v=ON_UNSET_VALUE;     // texture coordinates (0 to 1)

    ctype=-999;		/*  0 - cartesian
        see top of this file for defines
        anything else invalid
     */
    OriginEnt=0;   /* used for polar definitions */

    CrossAngle=0;

    CUrve[0]=0; CUrve[1]=0; /* IF both  are NULL, the node is not in the middle of a curve */
    Offsets[0]=0; Offsets[1]=0;

    m_psList=0;
    m_psno=0;
    m_Site_Flags=0;	// value moulded || sketch
    RX_FESite::Init();
    return 1;
}
void RXSRelSite::SetNeedsComputing(const int i) // WHY????
{
    RXEntity::SetNeedsComputing(i);
}

int RXSRelSite::Compute()
{
    /* returns 1 if the node moved*/
    ON_3dPoint v(0,0,0);
    sc_ptr sc1=NULL ,sc2=NULL;

    RXEntity_p  e_other;

    int iret_fi=0, iret=0;
    char buf[256];
    double  u,vv; int ret=0; //gcc
    double l_Ctol = this->GetSail()->m_Cut_Dist ;

    double s1s=0,s1f=0,s2s=0,s2f=0,s1=0,s2=0;
    //  RXSTRING text;
    HC_Open_Segment_By_Key(this->hoopskey);

    if  (  (!this->CUrve[0])  && (!this->CUrve[1])  ) {
        iret = this->ComputeAsSite(); // does the Set
    }

    else {
        if(this->CUrve[0] && this->CUrve[1] ) {
            ON_3dPoint l_v1,l_v2; ON_3dVector l_t1,l_t2;
            s1s=0.0 -  l_Ctol;
            s2s=0.0 -  l_Ctol ;
            sc1 = this->CUrve[0];
            sc2 = this->CUrve[1];

            s1 = (this->Offsets[0]->Evaluate(sc1,1));
            s2 = (this->Offsets[1]->Evaluate(sc2,1));
            s1f = sc1->GetArc(1) +  l_Ctol ;
            s2f = sc2->GetArc(1) +  l_Ctol;
            iret_fi= PCC_Find_Curve_Int(sc1->m_pC[1],s1s,s1f,&s1,&l_v1,&l_t1,sc2->m_pC[1],s2s,s2f,&s2,&l_v2,&l_t2);
            switch (iret_fi) {
            case PCC_IFOUND_OK:{
                RXSTRING text = TOSTRING(s1/sc1->GetArc(1) *100.0) +TOSTRING('%');
                this->Offsets[0]->Change(text);
                //sprintf(text,"%lf%%",s2/sc2->GetArc(1)*100.0);
                text = TOSTRING(s2/sc2->GetArc(1)*100.0 ) +TOSTRING('%');
                this->Offsets[1]->Change(text);
                this->CrossAngle=0.000;
                v=*(Compute_SiteWhichSnap(this,&l_v1,&l_v2));
                if((e_other=PCIA_Close_To_Another_Offset(this , this->GetSail()->m_Linear_Tol) )){
                    sc1->Needs_Cutting=sc2->Needs_Cutting=1;
                    this->CUrve[0]->SetNeedsComputing(); this->CUrve[1]->SetNeedsComputing();
                    sprintf(buf,"delete CLOSE cross-site %s\n s1 is %f on %s\ns2 is %f  on %s\nclose to\n%s", name(),  s1/sc1->GetArc(1) ,CUrve[0]->name(),  s2/sc2->GetArc(1) ,CUrve[1]->name(),e_other->name());

                    HC_Close_Segment();
                    rxerror(buf ,1);
                    this->Kill(  );
                    return 0;
                }
                break;
            }
            default :{ /* out of range!!!! */
                const char *pc0n, *pc1n;

                assert (iret_fi==PCC_OUT_OF_RANGE);
                pc0n = this->CUrve[0]->name();
                pc1n = this->CUrve[1]->name();
                PC_Highlight_Entity(this->CUrve[0]);
                PC_Highlight_Entity(this->CUrve[1]);

                sprintf(buf,"intersection node failed (%d) in relsite\n'%s'\n at (%f %f %f)\ns1= %f L1=%f on %s\n s2=%f L2= %f \non %s\nGauss_Cut_Dist=%f\n",
                        iret_fi , name(), x, y, z,s1, sc1->GetArc(1) ,pc0n,s2, sc2->GetArc(1),pc1n,l_Ctol);
                HC_Update_Display();rxerror(buf ,2);

                if(sc1->FlagQuery(CUT_FLAG))  { sc1->Needs_Cutting = 1; this->CUrve[0]->SetNeedsComputing(); }
                if(sc2->FlagQuery(CUT_FLAG))  { sc2->Needs_Cutting = 1; this->CUrve[1]->SetNeedsComputing(); }

                HC_Close_Segment();
                this->Kill( );
                return 0;
            }
            }//switch
        }
        else { // a relsite with jus one curve
            if(this->CUrve[0] ) {
                Get_Position(CUrve[0],Offsets[0],1,&v);
                sc1 = CUrve[0];
            }
            else  if(CUrve[1] ){
                Get_Position(CUrve[1],Offsets[1],1,&v);
                sc2 = (sc_ptr  )CUrve[1];
            }
        }

        if((sc1 && sc1->Geo_Or_UV ) || (sc2 && sc2->Geo_Or_UV)) { // this ASSUMES the sc is on its geodesic.  IE shapes seams or curves dont work.

            printf("HORRIBLE  in Compute_RelSite of  '%s' '%s' setting U V to (%f %f)\n",type(), name() , g_U1,g_U2);

            this->m_u =u= g_U1; // initialisation of u and v July 2001
            this->m_v =vv= g_U2; /* these are globals that GPBD made */
            // UV_from_Xsy returns 0 if the SC has depth.

            if(( sc1 &&(ret=UV_from_Sxy(sc1,this->Offsets[0]-> Evaluate(sc1,1), &u, &vv) )) ||
                    (sc2 && ( ret=UV_from_Sxy(sc2,this->Offsets[1]-> Evaluate(sc2,1), &u, &vv))))
            {
                this->m_u = u;
                this->m_v = vv;
            }
            else {
                RXEntity_p mould;
                mould =  this->GetSail()->GetMould();
                printf(" UV_from_Sxy failed on sc1 %s = %d ",sc1->name(), ret);
                if(mould) {
                    int l_err;
                    Evaluate_NURBS_By_XY(mould,( double)v.x, (double)v.y,&u,&vv,&(l_err),0);  // WOuld be better to do a find-nearest
                    printf(" in Compute_RelSite  via ENBXY GOT u=%f v=%f \n", u,vv);
                    this->m_u = u;
                    this->m_v = vv;
                } /* if mould */
            } /* else */
        } /* end of if(geo orUV) */

        if (fabs( this->x-v.x) > GetSail() ->m_Linear_Tol
                ||fabs(y-v.y) > GetSail() ->m_Linear_Tol
                || fabs(z-v.z) >  GetSail()->m_Linear_Tol)
            iret = 1;
        RXSitePt::Set(v);
    } //end of if not a simple site

    /* pick up the old text. May be renamed */
    if(!g_Janet) {
        if(!Move_Text_And_Markers(this->hoopskey, (float)this->x,(float)this->y,(float)this->z)) {
#ifdef HOOPS
            HC_Flush_Contents(".","markers,text");
            //HC_Insert_Text(this->x,this->y,this->z,this->name());
            HC_Insert_Marker( this->x,this->y,this->z);
#else
            HC_Flush_Geometry (GNode(rxexpLocal) );
            RX_Insert_Marker( this->x,this->y,this->z,GNode());
#endif
        }
#ifdef HOOPS
        if(this->TYPE==SITE)
            HC_Set_Color("text=black");
        else if( this->TYPE==RELSITE)
            HC_Set_Color("text=red");
        if(this->CUrve[0] && this->CUrve[1] )
            HC_QSet_Marker_Symbol("..","+");
        HC_QSet_Selectability("..","markers=on");
        HC_QSet_Text_Font("..", "size=10px, no transforms");
#endif
    }
    HC_Close_Segment();
    // fill in u and v provided that it it NOT a uv site.
    if(iret+1 && (this->ctype != UV_SITE)) {
        RXEntity_p mould;
        mould = this->GetSail ()->GetMould();
        if(mould&&!(this->m_Site_Flags&NOMOULD)){
            u=this->m_u; vv= this->m_v;
            int err=1;
            ON_2dPoint uv= this->GetSail ()->CoordModelXToUV(this,&err);
            if(err){
                ON_String l_buf; l_buf.Format("(compute any site ) Find Nearest didnt return OK  on %S (%f %f %f):  ",GetOName ().c_str(), this->x,this->y,this->z ) ;
                l_buf += ON_String(name() ) ;
                rxerror(l_buf,1);
            }
        } /* if mould */
    }
    if(iret || true){ // the logic here is nasty. we've already filled the fnode if its a simple site
        RX_FESite::Set(this->ToONPoint());   // GetLastKnownGood(RX_MODELSPACE)  );
        Touch_Panels();	/* Faster WRONG) = do this once at end */
    }
    if(iret)
    {
        if(CUrve[0] )
            CUrve[0]->Make_Pside_Request=1;
        if(CUrve[1] )
            CUrve[1]->Make_Pside_Request=1;
    }
    return(iret);
}

ON_3dPoint RXSRelSite::Compute_Polar_Site(const double A,const double B,const double C) {// cartesian wrt origin too
    float trigraph[3][3];
    double offset,b,c ;
    ON_3dVector  dxd;
    ON_3dVector dx;
    ON_3dPoint r; ON_3dVector t;

    switch (this->ctype) {
    case CARTESIAN:
        dx.x = A ;
        dx.y = B;
        dx.z = C;
        break;
    case CYLINDRICAL_XY:
        b = (B) * ON_DEGREES_TO_RADIANS;
        dx.x = (A * cos(b));
        dx.y = (A * sin(b));
        dx.z = C;
        break;
    case CYLINDRICAL_YZ:
        b = B * ON_DEGREES_TO_RADIANS;
        dx.y = (A * cos(b));
        dx.z = (A * sin(b));
        dx.x = C;
        break;
    case CYLINDRICAL_XZ:
        b = B * ON_DEGREES_TO_RADIANS;
        dx.z = (A *  cos(b));
        dx.x = (A *  sin(b));
        dx.y = C;
        break;
    case POLAR:
        b = (B) * ON_DEGREES_TO_RADIANS;
        c = (C) * ON_DEGREES_TO_RADIANS;
        dx.x = A * cos(b)*cos(c) ;
        dx.y = A * sin(b)*cos(c) ;
        dx.z = A * sin(c) ;
        break;

    default:
        dx.x=dx.y=dx.z=0.0;
    }
    RXEntity_p l_or= dynamic_cast<RXEntity_p>( this->GetOneRelativeByLien(RXO_ORGN_OF_SITE,aunt));
    if(l_or) {
        Site *origin = (Site *)l_or;
        ON_3dPoint vo; int kk;
        kk=Get_Position(l_or,NULL,1,  &vo);
        origin->Set(vo);// not sure
        if(!kk) cout<<" SHOULD return 0 from COmputePolarSite"<<endl;

        if(	origin->CUrve[0]) {/* WRONG assymetrical */
            sc_ptr  sc = (sc_ptr  )origin->CUrve[0];
            offset= (origin->Offsets[0]->Evaluate(sc ,1));
            sc->m_pC[1]->Find_Tangent( offset, &r,&t);
            ON_3dVector l_v = l_or->Normal();
            Compute_TrigraphXZ(t,l_v,trigraph);

            PostMult_Vect_By_Trigraph(&(trigraph[0][0]), dx , &dxd);
            dx = dxd;
        }
        dx.x += origin->x;
        dx.y += origin->y;
        dx.z += origin->z;
    }//if originent

    return dx;
}

int RXSRelSite::ComputeSimpleSite() { // sets (x,y,z) return non-zero if it moved
    /* This routine updates the position of a curve-less and non-LC site */

    RXEntity_p mould;
    int ItMoved=0;
    double A,B,C;
    ON_3dPoint Old, newpt;

    A = this->FindExpression(L"a",rxexpLocal)->evaluate();
    B = this->FindExpression(L"b",rxexpLocal)->evaluate();
    C = this->FindExpression(L"c",rxexpLocal)->evaluate(); //PC_Value_Of_Q(p->C);
    Old.z = this->z;
    Old.x =this->x;
    Old.y = this->y;
    switch (this->ctype) {
    case CARTESIAN:
    case CYLINDRICAL_XY:
    case CYLINDRICAL_YZ:
    case CYLINDRICAL_XZ:
    case POLAR:
        newpt = Compute_Polar_Site( A,B,C); // cartesian too. This permits cartesian wrt origin
        RXSitePt::Set(newpt); // just sets xyz
        assert(this->x==newpt.x&&this->y==newpt.y&&this->z==newpt.z);

        ItMoved =ItMoved ||( (Old-newpt ).LengthSquared() > pow(this->Esail->m_Linear_Tol,2));
        break;

    case UV_SITE:	{

        this->m_u=A;
        this->m_v=B;

        mould = this->Esail->GetMould();

        if(mould) {
            ON_3dPoint vp;
            Evaluate_NURBS(mould, this->m_u,this->m_v,&vp);
            this->Set(vp);
        }
        else
            rxerror(" coudnt UV a site (No Mould",2);
    }

        break;

    default:
        rxerror("unknown coordinate type in Compute_Site",2);
        break;
    }

    if(this->m_Site_Flags&MOULDED){
        mould = this->Esail->GetMould();
        if(mould)
            this->z = (float) PC_Interpolate(mould, this->x,this->y, this->z);
    }
    if(fabs(this->x - Old.x) + fabs(this->y - Old.y) + fabs(this->z -Old.z) >this->Esail->m_Linear_Tol )
        ItMoved = 1;

    return ItMoved;
}

int  RXSRelSite::ComputeAsSite() { //  does a set courtesy of ComputeSimpleSite
    /* a SITE with NULL curve may be controlled by lengths of adjacent scs
        or its coords may have been changed by an EDIT of goalseek
        This function organises this so that
                First. We apply the length-control
                Second. We apply the Edit
        */

    int iret=0,moved=0;

    if(this->NeedsComputing())
        moved=this->ComputeSimpleSite();
    iret = ComputeAsLengthControlled();


    if(!g_Janet && moved && (this->hoopskey)) {
        if(! Move_Text_And_Markers(this->hoopskey,(float)this->x,(float)this->y,(float)this->z)){
#ifdef HOOPS
            HC_Open_Segment_By_Key(this->hoopskey);
            HC_Flush_Contents(".","text,markers");
            HC_KInsert_Text(this->x,this->y,this->z,this->name());
            HC_Insert_Marker(this->x,this->y,this->z);
            HC_Close_Segment();
#else
            RX_Insert_Marker(this->x,this->y,this->z,GNode());
#endif
        }
    }

    iret = moved || iret;
    this->SetNeedsComputing(iret!=0); // gets done by the controller

    return(iret);
}

int RXSRelSite::ComputeAsLengthControlled()
{
    int i,iret=0;
    ON_3dVector dir[3];
    double move[3] ;
    sc_ptr sce[3] ;
    int ns=0;
    RXEntity_p l_or= dynamic_cast<RXEntity_p>( this->GetOneRelativeByLien(RXO_ORGN_OF_SITE,aunt));
    if(l_or)
        return iret;


    for(i=0;i<3;i++) {
        sce[i]=NULL; move[i]=0.0; dir[i].x=dir[i].y=dir[i].z=0.0;
    }
    ON_3dVector c ;
    set<RXObject*>::iterator it;

    // collect the SCs into array sce provided they have Defined_Length
    //    IE the relation is  RXO_N2_OF_SC
    set<RXObject*> theScs = FindInMap(RXO_N2_OF_SC,RXO_ANYINDEX ); // al Scs which end on this
    for(it = theScs.begin();it!=theScs.end();++it) {
        sc_ptr  sc = dynamic_cast<sc_ptr>(*it)  ;
        sce[ns] =  sc;
        if( sc->Needs_Finishing) return 0;
        RXQuantity *l_q = dynamic_cast<RXQuantity *> (sc->FindExpression(L"length",rxexpLocal));
        if(!l_q)
            continue;

        double tempL=0;
        if(l_q) tempL=l_q->evaluate();
        assert( sc->End2site==this);
        if(tempL < FLOAT_TOLERANCE)
            continue;
        HC_Close_Segment(); HC_Close_Segment();
        sce[ns]->Compute();
        HC_Open_Segment(this->type()); HC_Open_Segment_By_Key(this->hoopskey);

        move[ns] = tempL-sc->GetArc(0); /* changed adam 21 dec 95 */

        ON_3dPoint e1=sc->Get_End_Position(SC_START);
        ON_3dPoint e2=sc->Get_End_Position(SC_END  );
        c  = e2 -e1;
        if(!c.Unitize())
            continue;
        dir[ns] = c;
        ns++;

    }
    theScs = FindInMap(RXO_N1_OF_SC,RXO_ANYINDEX ); // al Scs which end on this
    for(it = theScs.begin();it!=theScs.end();++it) {
        sc_ptr  sc = dynamic_cast<sc_ptr>(*it)  ;
        sce[ns] =  sc;
        if( sc->Needs_Finishing) return 0;
        RXQuantity *l_q = dynamic_cast<RXQuantity *> (sc->FindExpression(L"length",rxexpLocal));
        if(!l_q)
            continue;

        double tempL=0;
        if(l_q) tempL=l_q->evaluate();
        assert( sc->End1site==this);
        if(tempL < FLOAT_TOLERANCE)
            continue;
        HC_Close_Segment(); HC_Close_Segment();
        sce[ns]->Compute();
        HC_Open_Segment(this->type()); HC_Open_Segment_By_Key(this->hoopskey);

        move[ns] = tempL-sc->GetArc(0); /* changed adam 21 dec 95 */

        ON_3dPoint e1=sc->Get_End_Position(SC_START);
        ON_3dPoint e2=sc->Get_End_Position(SC_END  );
        c  = e2 -e1;
        if(!c.Unitize())
            continue;
        dir[ns] = -c;
        ns++;
    }
    if(!ns)
        return 0;
    if(this->ctype != CARTESIAN) {
        rxerror("Changing type in Compute_Site",2);
        this->ctype = CARTESIAN;
    }
    RXExpressionI *e;
    if(ns == 1) {
        if (fabs(move[0]) > this->GetSail()->m_Linear_Tol ) {
            this->x = this->x + dir[0].x * move[0];
            this->y = this->y + dir[0].y * move[0];
            this->z = this->z + dir[0].z * move[0];

            e = this->FindExpression(L"a",rxexpLocal); e->Change(TOSTRING(x ));
            e = this->FindExpression(L"b",rxexpLocal) ; e->Change(TOSTRING(y ));
            e = this->FindExpression(L"c",rxexpLocal);  e->Change(TOSTRING(z ));

            iret = 1;
        }
    }
    else if(ns > 1) { /* only use first two */
        ON_3dPoint a0,a1,b0,b1,dum;
        float fdum1,fdum2;

        if (fabs(move[0])>  this->GetSail() ->m_Linear_Tol
                ||fabs(move[1])>  this->GetSail() ->m_Linear_Tol) {
            a0.x = this->x + dir[0].x * move[0];
            a0.y = this->y + dir[0].y * move[0];
            a0.z = this->z + dir[0].z * move[0];
            a1.x = this->x + dir[1].x * move[1];
            a1.y = this->y + dir[1].y * move[1];
            a1.z = this->z + dir[1].z * move[1];
            ON_3dVector l_v = this->Normal();

            dum = ON_CrossProduct(l_v,dir[0]);
            b0 = a0 + dum;
            dum = ON_CrossProduct(l_v,dir[1]  );
            b1 = a1+dum;
            if(1==PC_Intersection(a0, b0,a1,b1, dum,&fdum1,&fdum2)){
                this->x=dum.x;
                this->y=dum.y;
                this->z=dum.z;
                e = this->FindExpression(L"a",rxexpLocal); e->Change(TOSTRING(x ));
                e = this->FindExpression(L"b",rxexpLocal); e->Change(TOSTRING(y ));
                e = this->FindExpression(L"c",rxexpLocal); e->Change(TOSTRING(z ));

                iret=(fabs(move[0])>  this->GetSail() ->m_Linear_Tol
                      ||fabs(move[1])>  this->GetSail() ->m_Linear_Tol);

            } // if intersection succeeded.
        } // if mode big
    } // ns>1
    if(iret){
        sce[0]->SetNeedsComputing();   if(ns>1)sce[1]->SetNeedsComputing();
    }
    return iret;
}


int RXSRelSite::PrintShort(FILE *ff) const
{
    return( fprintf(ff,"%6f %6f %6f :%d :    %6f\n",x, y, z, GetN(),    getDs()) );
}

int RXSRelSite::Dump(FILE *fp) const
{
    int side,k,rc=0;
    VECTOR v;
    sc_ptr sc;

    rc+=fprintf(fp,"\tSail is '%s' '%S'\n", GetSail()->GetType().c_str(),
                GetSail()->GetName().c_str());

    rc+=fprintf(fp,"\t%11d\tx=%f\ty=%f\tz=%f\n", GetN(), x, y, z);
    ON_3dPoint pp= DeflectedPos( RX_GLOBAL) ;
    rc+=fprintf(fp,"\tGlobalspace\tx=%f\ty=%f\tz=%f\n",  pp.x,pp.y, pp.z);
    pp= DeflectedPos( RX_MODELSPACE) ;
    rc+=fprintf(fp,"\tModel space\tx=%f\ty=%f\tz=%f\n",  pp.x,pp.y, pp.z);

    ON_3dPoint vvv = GetLastKnownGood(RX_MODELSPACE) ; // Model space.
    rc+=fprintf(fp,"\tlastknowngd\tx=%f\ty=%f\tz=%f\n", vvv[0],vvv[1],vvv[2]);

    rc+=fprintf(fp," \t  N   \t  U   \t   V\n");
    rc+=fprintf(fp," \t%d \t%12.12g \t%12.12g\n", GetN(), m_u, m_v);
    rc+=fprintf(fp,"     crossAngle\t%f\n", CrossAngle);
    rc+=fprintf(fp,"     dS,    ,    \t%f \n",  getDs());
    rc+=fprintf(fp,"     deflections \t%f \t%f \t%f\n", Deflection().x, Deflection().y,Deflection().z);

    if (CUrve[0]) {
        sc= CUrve[0];
        rc+=fprintf(fp,"    on C1      %s\n",(*CUrve[0]).name());
        rc+=fprintf(fp,"    offset     %S\n",Offsets[0]->GetText().c_str() );
        Offsets[0]->Print(fp);
        for(side=0; side < 3; side++) {
            if(sc->m_pC[side]->HasGeometry()) {  //sc->c[side]
                if(1==Get_Position(CUrve[0],Offsets[0],side,&v))
                    rc+=fprintf(fp,"  side %d (%f %f %f)\n",side,v.x,v.y,v.z);
                else
                    rc+=fprintf(fp,"  side %d has no position\n",side);
            }
        }
    }
    if (CUrve[1]  ) {
        rc+=fprintf(fp,"    curve 2   %s\n",(* CUrve[1]).name());
        rc+=fprintf(fp,"    offset2    %S\n", Offsets[1]->GetText().c_str());
        Offsets[1]->Print(fp);
    }

    if(ctype >= 0 && ctype < 6)  {
        const char *typestr[] = {"CARTESIAN","XY CYLINDRICAL","YZ CYLINDRICAL",
                                 "XZ CYLINDRICAL","POLAR","UV"};
        rc+=fprintf(fp,"COORD type '%s' \n ",typestr[ctype]);
    }
    //    if(Origin Ent){
    //        Site *o = (Site *)OriginEnt;
    //        rc+=fprintf(fp,"origin  %s\n  %f,%f,%f)\n", OriginEnt->name(), o->x,o->y,o->z);
    //    }

    rc+=fprintf(fp," adjacent PSides\n");
    for (k=0;k<  m_psno;k++) {
        struct SIDEREF s =  m_psList[k];
        PSIDEPTR q = s.pp;
        rc+=fprintf(fp,"            %s \t%d \t%f\n",(*q).name(), s.end , s.m_srangle*57.29577951);
    }
    rc+=fprintf(fp," flags:(%d)\t",m_Site_Flags );
    if(m_Site_Flags&NOMOULD)  			rc+=fprintf(fp,"NOMould...");
    if(m_Site_Flags&MOULDED)  			rc+=fprintf(fp,"Moulded...");
    if(m_Site_Flags&PCF_SKETCH)			rc+=fprintf(fp,"sketch Site...");
    if(m_Site_Flags&PCF_EXPORT_DISP)	rc+=fprintf(fp,"Export_Displacement request...");
    if(m_Site_Flags&PCF_EXPORT_COORDS )	rc+=fprintf(fp,"Export_Coordinate request...");

    if(m_Site_Flags&PCF_ISONEDGE)		rc+=fprintf(fp,"edge node..");
    if(m_Site_Flags&PCF_ISMESHSITE )	rc+=fprintf(fp,"mesh site ..");
    if(m_Site_Flags&PCF_ISFIELDNODE )	rc+=fprintf(fp,"field node.."); else 	rc+=fprintf(fp,"NOTfieldNode..");
    rc+=fprintf(fp,  "\n\n");
    return rc;
}
int RXSRelSite::Resolve(void){
    int rc=0;
    assert(!IsInStack());
    this->SetInStack (true);
    this->SetLineLwr();
    if(TYPE==SITE)
        rc= ResolveAsSite();
    else if(TYPE==RELSITE)
        rc= ResolveAsRelsite();
    else
        assert(0);
    if(rc) {
        rc= ResolveCommon();
        if(rc) {
            this->Needs_Resolving=0;
            this->SetNeedsComputing();
            this->Needs_Finishing=1;
        }
    }
    this->SetInStack (false);
    return rc;
}
int RXSRelSite::ReWriteLine(void)
{ 
    if(this->Needs_Resolving) return 0;
    switch  (this->TYPE) {
    case SITE: {
        MTSTRING w;
        RXExpressionI *iii;
        QString s, NewLine =  this->type();
        NewLine += RXENTITYSEPSTOWRITE;
        NewLine += this->name();
        NewLine += RXENTITYSEPSTOWRITE;
        iii=this->FindExpression(L"a",rxexpLocal);
        w= iii-> GetText();
        s = QString::fromStdWString(w);
        NewLine += QString::fromStdWString( iii-> GetText() ) ;
        NewLine += RXENTITYSEPSTOWRITE;
        iii=this->FindExpression(L"b",rxexpLocal);
        s=QString::fromStdWString(iii-> GetText());
        NewLine += s;	NewLine += RXENTITYSEPSTOWRITE;
        iii=this->FindExpression(L"c",rxexpLocal);
        s= QString::fromStdWString(iii->GetText());
        NewLine +=s ;

        NewLine +=this->AttributeString( RXENTITYSEPSTOWRITE );

        this->SetLine( qPrintable(NewLine) );
        break;
    }
    case RELSITE:{
        //	  relSITE: <name> : <curve> : prop [:optional attributess[intersect=<float> ][cross=sc] !comment
        MTSTRING w;
        QString s, NewLine =  this->type();
        NewLine += RXENTITYSEPSTOWRITE;
        NewLine += this->name();
        NewLine += RXENTITYSEPSTOWRITE;

        NewLine += this->CUrve[0]->name();
        NewLine += RXENTITYSEPSTOWRITE;

        class RXOffset *o1 = this->Offsets[0];
        //  ON_String sss = ON_String(o1->GetText().c_str()  );
        NewLine += QString::fromStdWString (o1->GetText());
        NewLine += RXENTITYSEPSTOWRITE;

        NewLine +=this->AttributeString();

        class RXOffset *o2 = this->Offsets[1];
        if(o2 ) { assert( this->CUrve[1]);
            //  sss = ON_String(o2->GetText().c_str()  );
            NewLine += ",$intersect= ";
            NewLine += 	 QString::fromStdWString (o2->GetText());
            NewLine += ",$cross= ";
            NewLine += this->CUrve[1]->name();
        }
        //  NewLine += "! exp= "+ToUtf8(o1->GetExpression());

        this->SetLine (qPrintable(NewLine));
        break;
    }
    default: assert(0);

    };//switch
    return 1;
}
int RXSRelSite::Finish(void){

    if(TYPE==SITE)
        return FinishAsSite();
    if(TYPE==RELSITE)
        return FinishAsRelsite();
    assert(0);
    return 0;
}

int RXSRelSite::ResolveAsSite() 
{

    /* SITE card is of type
  SITE:   clew  :       0:  0:  0:     [atts]     ! comment */
    int retVal = 0;	/* failure */

    const wchar_t *lName=0;
    const wchar_t *pAttString=0;
    bool hasAtts=false;
    RXSTRING orname,sline =TOSTRING(this->GetLine());
    rxstriptrailing(sline);
    std::vector<RXSTRING> wds = rxparsestring(sline, RXENTITYSEPS_W,false);
    int nw = wds.size();
    if(nw<5)
        return(retVal);

    SAIL *sail = this->Esail;
    rxtolower(wds[1]);
    Make_Valid_Segname(wds[1]);  lName=wds[1].c_str ();
    RXSTRING &Sx=wds[2];
    RXSTRING &Sy=wds[3];
    RXSTRING &Sz=wds[4];
    RXExpressionI *l_A,*l_B ,*l_C ;
    if(g_Janet ) {
        l_A= this->AddExpression ( new RXExpressionDble(L"a",Sx,0 ) );
        if(! l_A->Resolve()) {
            this->CClear();  return 0; 	}
        l_B= this->AddExpression ( new RXExpressionDble(_T("b"),Sy,0 ) );
        if(!  l_B->Resolve()) {
            this->CClear();	 return 0; 	}
        l_C= this->AddExpression ( new RXExpressionDble(_T("c"),Sz,0 ) );
        if(!  l_C->Resolve()){
            this->CClear();	return 0; 	}
    }
    else
    {
        l_A= this->AddExpression ( new RXQuantity(L"a",Sx,L"m",this->GetSail() ) );      this->SetRelationOf(l_A,aunt,RXO_EXP_OF_ENT,RX_AUTOINCREMENT); //done
        if(! l_A->Resolve()) {
            this->CClear();  return 0; 	}
        l_B= this->AddExpression ( new RXQuantity(_T("b"),Sy,L"m",this->GetSail()  ) );  this->SetRelationOf(l_B,aunt,RXO_EXP_OF_ENT,RX_AUTOINCREMENT); //done
        if(!  l_B->Resolve()) {
            this->CClear();	 return 0; 	}
        l_C= this->AddExpression ( new RXQuantity(_T("c"),Sz,L"m",this->GetSail()  ) );  this->SetRelationOf(l_C,aunt,RXO_EXP_OF_ENT,RX_AUTOINCREMENT); //done
        if(!  l_C->Resolve()){
            this->CClear();	return 0; 	}
    }
    this->x = l_A->evaluate(); //  Compute_Simple_Site updates m_u,m_v to maintain parametrics.
    this->y = l_B->evaluate();
    this->z = l_C->evaluate();

    this->ctype = CARTESIAN; /* default */
    //    if (this->m_Site_Flags )
    //        cout<<"warning: setting site flags from "<< m_Site_Flags  <<" to 0 in " << this->name() <<endl;

    this->m_Site_Flags=0;

    if(nw>5) { pAttString = wds[5].c_str(); if(!wds[5].empty()) hasAtts=true;}
    if(hasAtts){ assert(pAttString); assert(*pAttString);
        RXAttributes rxa(pAttString);
        double gg;
        int l_moulded=0;
        int l_nomould= (rxfindinstring(pAttString,L"$nomold")!=NULL)  || (rxfindinstring(pAttString,L"$nomould")!=NULL);

        if(rxfindinstring(pAttString,L"moulded") || rxfindinstring(pAttString,L"uv") ) {
            if(!sail->GetMould()) /* this looks WRONG */
                if(! sail->Get_Key_With_Reporting("interpolation surface","mould"))
                    return 0;
            l_moulded= (rxfindinstring(pAttString,L"moulded")!=NULL) ;
        }

        if(rxa.Extract_Double("$grid",&gg) )
            this->m_gdp = gg; //I dont think this is used any more

        if(l_nomould)		// '$nomold" overrides 'moulded'
            this->m_Site_Flags = this->m_Site_Flags |NOMOULD;
        else if(l_moulded)
            this->m_Site_Flags = this->m_Site_Flags |MOULDED;


        if( rxa.Extract_Word(L"$origin", orname)) { //origin exists
            RXEntity_p  l_or;
            rxstripleading(orname); rxstriptrailing(orname);
            l_or = sail->Get_Key_With_Reporting( "site,relsite",ToUtf8(orname).c_str(),true);
            if(!l_or)  {
                if(g_Janet){
                    RXSTRING bb = RXSTRING(L"CRASH LIKELY (Please turn OFF Janet flag): no origin yet for " ) + TOSTRING (this->name());
                    rxerror(bb,4);
                }
                this->CClear();
                return 0;
            }
            this->OriginEnt = l_or;

            l_or->SetRelationOf(this,child|niece,RXO_ORGN_OF_SITE );
            assert(l_or== dynamic_cast<RXEntity_p>( this->GetOneRelativeByLien(RXO_ORGN_OF_SITE,aunt))) ;

            if(rxfindinstring(pAttString,L"cylindricalxy"))
                this->ctype = CYLINDRICAL_XY;
            else if(rxfindinstring(pAttString,L"cylindricalxz"))
                this->ctype = CYLINDRICAL_XZ;
            else if(rxfindinstring(pAttString,L"cylindricalyz"))
                this->ctype = CYLINDRICAL_YZ;
            else if(rxfindinstring(pAttString,L"spherical"))
                this->ctype = POLAR;
            //else cartesian
        } // origin

        else if(rxfindinstring(pAttString,L"uv")) {
            if(!sail->GetMould()) {
                cout<< " no UV site on an unmoulded sail"<<endl;
                return 0;
            }
            this->ctype = UV_SITE;
            this->m_u = l_A->evaluate (); //  Compute_Simple_Site updates m_u,m_v to maintain parametrics.
            this->m_v = l_B ->evaluate ();
        }

        this->SetIsRedSpace(false);
        this->PC_Finish_Entity("site",ToUtf8(lName).c_str(),rxa,this->GetLine());
    } // Has Atts treatment
    else{
        this->SetIsRedSpace(false);
        this->PC_Finish_Entity("site",ToUtf8(lName).c_str(),this,(long)0,NULL,0,this->GetLine());
    }
    retVal =1;	// must be OK if got here


    return(retVal);
}//RXSRelSite::ResolveAsSite()

int RXSRelSite::ResolveAsRelsite() 
{
    /* relSITE card is of type
   relSITE: <name> : <curve> : pos [:optional attributes[intersect=<float> ][cross=sc] !comment */

    char  name[256];
    class RXSeamcurve *sc;
    int retVal=0 ;
    double gg;

    std::string sline(this->GetLine() );
    rxstriptrailing(sline);
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();
    if(nw<4)
        return(retVal);

    // 'relsite'	wds[0]
    // name			wds[1]
    // SC			wds[2]
    // position		wds[3]
    // [atts]		wds[4]

    strcpy(name,wds[1].c_str());  	Make_Valid_Segname(name);
    RXAttributes  rxa; const char*att=0;
    if(nw>4){
        rxa.Add( QString::fromStdString(wds[4]));
        att = wds[4].c_str();
    }

    sc= dynamic_cast<class RXSeamcurve*> (this->Esail->Get_Key_With_Reporting("curve,seam,seamcurve,nodecurve,compound curve",wds[2].c_str(),true ));
    if(!sc )
        return(retVal);

    sc->Make_Pside_Request = 1;

    this->CUrve[0]=sc;
    if(rxa.Extract_Double ("$grid",&gg) )
        this->m_gdp = gg; //I dont think this is used any more

    this->Offsets[0]=dynamic_cast<class RXOffset* > ( this->AddExpression( new RXOffset(L"PosOnCrv",TOSTRING(wds[3].c_str()), sc,this->Esail) ));
    this->SetRelationOf( this->Offsets[0],aunt,RXO_POS_OF_RSITE); //done
    this->SetIsRedSpace(false);

    if(PC_Finish_Entity("relsite",name,this,(long)0 ,NULL,att,sline.c_str() ))
        retVal = 1;
    {
        int l_moulded= 0,l_nomould= 0 ;//,l_sk=0, l_expdef=0,l_expcoord=0;
        l_moulded=	rxa.Find("moulded" ); //(stristr(att,"moulded")!=NULL) ;
        l_nomould=  rxa.Find("$nomold")  || rxa.Find("$nomould")   ;

        if(l_nomould)	// overrides 'moulded'
            this->m_Site_Flags = this->m_Site_Flags |NOMOULD;
        else if(l_moulded)		this->m_Site_Flags = this->m_Site_Flags |MOULDED;
    }
    RXSTRING crossString;
    if(rxa.Extract_Word(TOSTRING("cross"),crossString)) {
        sc_t  *sc2;
        double f;
        this->CUrve[1]= (sc_ptr) this->Esail->Get_Key_With_Reporting(L"curve,seamcurve,cut",crossString);
        if(!this->CUrve[1]) {
            cout<< "TODO::PCO_Free_Offset_Record(this,&(this->Offsets[0]));	"<<endl;
            return 0;
        }
        sc2 = this->CUrve[1];
        crossString=L"50%"; f=0.0;
        //att,
        if(rxa.Extract_Double( "intersect", &f)) { crossString=TOSTRING(f); }
        this->Offsets[1]= dynamic_cast<class RXOffset* > (this->AddExpression( new RXOffset(L"RSO_1",TOSTRING(crossString), sc2,0)) );
        this->SetRelationOf( this->Offsets[1]  ,aunt,RXO_EXP_OF_ENT); //done

        this->CUrve[1]->SetRelationOf(this,child|niece,RXO_SC1_OF_SITE);
    } // cross

    sc->SetRelationOf(this,child|niece,RXO_SC_OF_SITE,0);

    return(retVal);
}//  RXSRelSite::ResolveAsRelsite




int RXSRelSite::ResolveCommon()
{ // return value non-zero = OK
    QString qbuf;
    int k; bool ok;

    int l_moulded= 0,l_nomould= 0 ,l_sk=0, l_expdef=0,l_expcoord=0;
    l_moulded=	AttributeFind("moulded" );
    l_nomould=  AttributeFind("$nomold")  ||AttributeFind("$nomould")   ;
    l_sk =		AttributeFind("sksite")   || AttributeFind("sketch") ;
    l_expdef  = AttributeFind("$export_def") ;
    l_expcoord =AttributeFind("$export_coords") ;

    if(l_nomould)	// overrides 'moulded'
        this->m_Site_Flags = this->m_Site_Flags |NOMOULD;
    else if(l_moulded)		this->m_Site_Flags = this->m_Site_Flags |MOULDED;
    if(l_sk)				this->m_Site_Flags = this->m_Site_Flags|PCF_SKETCH;
    if(l_expdef)			this->m_Site_Flags = this->m_Site_Flags|PCF_EXPORT_DISP;
    if(l_expcoord)			this->m_Site_Flags = this->m_Site_Flags |PCF_EXPORT_COORDS;





    if(this->AttributeFind( "$trace")) this->SetTrace(1);
    double loads[3];memset(loads,0,3*sizeof(double));
    int hasloads=0;
    if(this->AttributeGetDble( "$px",&(loads[0])) )  hasloads++; else loads[0]=0;
    if(this->AttributeGetDble( "$py",&(loads[1])) )  hasloads++; else loads[1]=0;
    if(this->AttributeGetDble( "$pz",&(loads[2])) )  hasloads++; else loads[2]=0;
    if(hasloads) this->SetLoads(loads);

    //    if(this->RXEntity::AttributeGet("$fix", qbuf)  ) {
    if( this->AttributeGet("$fix", qbuf)  ) {// inherits from any SC or PS
        /* fixity:	name:	atts*/
        qbuf.prepend("($fix = "); qbuf.append(")");
        QString line;
        RXEntity_p se=0;
        int depth = 1 + this->generated;
        line = QString("fixity:") + this->GetQName() + qbuf;
        se=Just_Read_Card(this->Esail , qPrintable(line),"fixity",qPrintable(this->GetQName()) ,&depth);
        if(se) {
            this->SetRelationOf(se ,spawn|child|niece,RXO_FIX_OF_SITE );
            se->PC_Finish_Entity("fixity",ToUtf8(this->GetOName()).c_str(),NULL,(long)0 ,NULL,qPrintable(qbuf),qPrintable(line) );
            se->Needs_Resolving=0;
            se->Needs_Finishing=1;
            se->SetNeedsComputing();
        }
    }
    // site-connect has optional ranking number (last field)
    if( this->RXEntity::AttributeGet("slave", qbuf)  ) {  // $slave=site%boat%bow  or $slave=bow;
        if(!qbuf.contains("%"))
            qbuf = QString ("site: ")+qbuf + ":" + this->Esail->GetQType();
        else
            qbuf.replace("%",":");
        /* connect	g_b_tack 	site 	b_gen_tack 	boat 	site 	tack 	genoa 	 	1*/
        QString line, qcname;
        qcname = QString(this->name()) + QString ("_c");
        RXEntity_p se=0;
        int depth = 1 + this->generated;
        line = QString("connect :") + qcname + " : site : " +this->name() + ": " + this->Esail->GetQType() +":" + qbuf ;
        if( this->RXEntity::AttributeGet("rank", qbuf)  ) {  // $rank=2
            k=qbuf.toInt(&ok);
            if(ok)
                line+= QString(" : ")+QString::number(k);
        }
        line+= QString("  ! Generated from attributes of master site");
        se=Just_Read_Card(this->Esail ,qPrintable( line) ,"connect",qPrintable( qcname ),&depth);
        if(se) {
            this->SetRelationOf(se ,spawn|child|niece,	RXO_CONNECT_CHILD );
            se->Resolve();
            se->Needs_Finishing=1;
            se->SetNeedsComputing();
        }
    } //slave
    if( this->RXEntity::AttributeGet("master", qbuf)  ) {   // $slave=site%boat%bow  or $slave=bow;
        /* connect	g_b_tack 	site 	b_gen_tack 	boat 	site:tack:genoa 	 	1*/
        if(!qbuf.contains("%"))
            qbuf = QString ("site: ")+qbuf + ":" + this->Esail->GetQType();
        else
            qbuf.replace("%",":");
        QString line, qcname;
        qcname = QString(this->name()) + QString ("_c");
        RXEntity_p se=0;
        int depth = 1 + this->generated;

        line = QString("connect :") + qcname + " : " +qbuf  +": site: " + this->name()  +": "+ this->Esail->GetQType();
        if( this->RXEntity::AttributeGet("rank", qbuf)  ) {  // $rank=2
            k=qbuf.toInt(&ok);
            if(ok)
                line+= QString(" : ")+QString::number(k);
        }
        line+= QString("  ! Generated from attributes of slave site");
        se=Just_Read_Card(this->Esail ,qPrintable( line) ,"connect",qPrintable( qcname ),&depth);
        if(se) {
            this->SetRelationOf(se ,spawn|child|niece,	RXO_CONNECT_CHILD );
            se->Resolve();
            se->Needs_Finishing=1;
            se->SetNeedsComputing();
        }
    } //master
    if( this->RXEntity::AttributeGet("rigidbody", qbuf)  ) {
        // rigidbody:name: n1,n2,n3,... %end, atts (will be something like $fix=123456 (or dof=123456)

        QString line, qcname;
        qcname = qbuf;
        RXEntity_p se=0;
        int depth = 1 + this->generated;
        // if {rb,qbuf} exists, append this->name() to its sitelist
        // else, create a rb with line 'rigidbody:qbuf:this->name() :   this->selectedAtts($fix,$dof)
        if(se=this->GetSail()->Entity_Exists(qPrintable (qcname),"rigidbody"))
        {
            line = se->GetLine();
            QStringList qsl=line.split(RXENTITYSEPS );
            qsl.insert(2,QString(this->name()));

            line=qsl.join( RXENTITYSEPS);
            if(this->RXEntity::AttributeGet( "$dof" ,qbuf))
                line+= ":$dof="; line+=qbuf;
            //  qDebug()<< "modify RB '"<<line;
            se->UnResolve();
            se->SetLine(line);
        }
        else  { // rb doesnt exist
            line = QString("rigidbody :") + qcname + " : " +this->name();
            if(this->RXEntity::AttributeGet( "$dof" ,qbuf))
                line+= ":%end:$dof="; line+=qbuf;
            // qDebug()<< " create RB '"<<line;
            se=Just_Read_Card(this->Esail ,qPrintable( line) ,"rigidbody",qPrintable( qcname ),&depth);
            this->SetRelationOf(se ,spawn|child|niece,	RXO_RB_CHILD );
        }
    } //rigid body
    return 1;
}


int RXSRelSite::FinishAsSite () {
    RXEntity_p l_or= dynamic_cast<RXEntity_p>( this->GetOneRelativeByLien(RXO_ORGN_OF_SITE,aunt));
    if(l_or)
        if(l_or->Needs_Finishing) return 0;

    if(this->m_Site_Flags&MOULDED|| (this->ctype==UV_SITE) ){
        RXEntity_p mould;
        mould = this->Esail->GetMould();
        if(mould->Needs_Finishing) return 0;
    }

    Compute();
    if (this->ctype!=UV_SITE){
        double uuvv;
        if(  GetUVAttribute(RELSITE_U ,uuvv))  // DONT FORGET:: this inherits atts from q's Curve[0]
            m_u = uuvv;
        if(  GetUVAttribute(RELSITE_V ,uuvv))  // DONT FORGET:: this inherits atts from q's Curve[0]
            m_v = uuvv;
    }
    this->Needs_Finishing =0; //  Nov 17 2001 so we can call it from SearchForCloseSites
    return 1;
}  
int RXSRelSite::FinishAsRelsite () {
    int l;
    sc_ptr sc[2];
    RXEntity_p second;
    sc_ptr cp =NULL ;

    if(this->m_Site_Flags&MOULDED){
        RXEntity_p mould = this->Esail->GetMould();
        if(mould->Needs_Finishing) return 0;
    }
    for(l=0;l<2 ; l++)  {
        cp =this->CUrve[l];
        if(cp &&cp->Needs_Finishing) return(0);
    }

    for(l=0;l<2; l++)  {
        sc[l ] = NULL;
        cp =this->CUrve[l];
        if(cp) {
            sc[l ] = (sc_ptr  )cp ;
            if(Duplicate_Relsite(sc[l ],this->Offsets[l],&second)){
                char s[256]; int depth;
                sprintf(s," (Finish) relsite %s\n coincident with %s, so deleting %s ",this->name(),second->name(),second->name());
                rxerror(s,1);
                // CAN WE put up an arrow????
                depth=0;
                //!keyword	name	type of entity	Original name (exists)	Alias (nick-name)
                sprintf(s,"rename: %s : %s : %s : %s",this->name(),this->type(), this->name(),second->name());
                second->Kill();
                Just_Read_Card(this->Esail, s,"rename",this->name(), &depth);


                /*
    We have to merge these two nodes.
    (A) 	1	Delete e, unresolving everything which depends on it
      2	generate a rename of e to second
   3	Resolve all.
    (B)	1 Go thru everything referring to e and change the reference  to second.
       (just cp)
      2 then its safe to delete e.
   (	LOOKS OK)

  */
                return(0);
            }

            if(strieq(cp->type(),"compound curve")) {
                RXOffset tmp ( *(this->Offsets[l]) );

                cp = Get_Compound_Curve_Element(cp, &tmp,&(this->Offsets[l]));

                if(!cp) {
                    char s[256];
                    sprintf(s," in %s non-existant curve",this->name());
                    rxerror(s,1);return(0);
                }
                this->CUrve[l]=cp;
            }
        }
    }


    Compute();  // Might delete it

    if( this->CUrve[0]){
        this->CUrve[0]->Insert_One_IA(this,this->Offsets[0]);
        if( sc[1]){
            //		if(sc[1]->seam) sc[0]->ia[sc[0]->n_angles-1].Angle =&(p->CrossAngle);
            //Oct 2000 maybe WRONG to remove but as we don't use crosangles it can only confuse
        }
    }
    if( this->CUrve[1]) {
        this->CUrve[1]->Insert_One_IA(this,this->Offsets[1]);
        if( sc[0]){
            //	if(sc[0]->seam)  sc[1]->ia[sc[1]->n_angles-1].Angle =&(p->CrossAngle);
        }
    }
    if (this->ctype!=UV_SITE){
        double uuvv;
        if(  GetUVAttribute(RELSITE_U ,uuvv))  // DONT FORGET:: this inherits atts from q's Curve[0]
            m_u = uuvv;
        if(  GetUVAttribute(RELSITE_V ,uuvv))  // DONT FORGET:: this inherits atts from q's Curve[0]
            m_v = uuvv;
    }
    this->Needs_Finishing=0; // Nov 2001
    return 1;
}  


int RXSRelSite::IsEdgeNode() const{
    // its an edgenode if:
    // A) one of its  CUrve s an edge
    // B) one of its m_ps list ->dataptr->sc  ->edge is true.
    int k;
    if(m_Site_Flags&PCF_ISONEDGE)
        return true;

    for(k=0;k<2;k++) {
        if(!this->CUrve[k]) continue;
        sc_ptr sc = this->CUrve[k];
        if(sc->edge) return true;
    }
    for (k=0;k<  m_psno;k++) {
        Pside  *ps  =  m_psList[k].pp;
        sc_ptr sc =   ps->sc;
        if(sc->edge) return true;
    }

    return 0;
}// IsEdgeNode

int  RXSRelSite::CClear(){
    int rc=0;
    // clear stuff belonging to this object
    /* all the functionality of old Delete Entity except that it leaves the entity intact
  Normally called from Delete Entity
    MUST be followed by a call to Finish_Entity , or by deletion of the entity*/

    /* some SCs may end on this relsite. We should delete them
   but this could be courtesy of delete_dependents
   Psides contain references to endsites as well as to endentities
   */
#ifdef NEVER
    if(this->name())
        cout<<" CClear  "<<this->name()<<endl;
#endif
    int l,k,ok;
    sc_ptr sc;
    if(this->m_psList){
        for(k=0;k<this->m_psno;) {
            struct SIDEREF side= this->m_psList[k];
            PSIDEPTR  ps = side.pp;
            sc = ps->sc;
            ps->Kill();
            if(sc){
                ok= sc->Remove_From_IA_List(this,0);
                if(ok) {
                    if(sc->FlagQuery(CUT_FLAG)) { sc->Needs_Cutting=1; sc->SetNeedsComputing();}
                }
                else{
                    RXSTRING buf = this->GetOName () + TOSTRING(" didnt find IA in list of SC ")+ TOSTRING(sc->name());
                    rxerror(buf,1);
                }
            } /* if sc */

        }

        RXFREE(m_psList); m_psList=NULL; this->m_psno=0;
    }
    for(l=0; l<2 ;l++) {
        sc = this->CUrve[l];
        if(sc){
            ok= sc->Remove_From_IA_List(this,DO_SCS_TOO);
            if(ok) {
                sc->Make_Pside_Request=1;
                if(sc->FlagQuery(CUT_FLAG)) { sc->Needs_Cutting=1; sc->SetNeedsComputing();}
                // for R3 removed this call. Needs verif //Break_Curve(sce); // maybe WRONG to put it here /* July 1998 because this can be called inside RSOLve */
            }
#ifdef DEBUG
            else{
                QString buf =" ( RXSrelSite::CClear) " + QString::fromStdWString(this->GetOName ()) + " didnt find IA in list of SC " + QString(sc->name());
                rxerror(buf,1); // THIS IS OK (I think)  IF IT OCCURS IN a deletEWHOLE.
            }
#endif
        } /* if sc */
    }
    //  lets look in its Olist for RXO_IASITE_OF_SC
    // this is horribly slow
    k=Remove_From_All_IA_Lists((RXEntity_p) this);// has trouble if p (or sthg it refers to) isnt finished
    //   if(k)
    //       cout<< "BAD SITE: RFAIAL got something in "<< this->type()<<","<< this->name() <<  endl;
    this->CUrve[0] = NULL;
    this->CUrve[1] = NULL;
    this->Offsets[0]=0; this->Offsets[1]=0;
    rc+=RXEntity::CClear();
    rc+=RX_FESite::CClear();
    return rc;
}

bool RXSRelSite::GetUVAttribute( const int UorV, double &value) const  // UorV is RELSITE_U or _V
{
    bool rc=false;

    QString what, w,s;
    switch ( UorV){
    case  RELSITE_U:
        what=  "$u";
        break;
    case  RELSITE_V  :
        what= "$v";
        break;
    default:
        assert(0);
    };
    // 1) is there a local value?
    //(2) is there a local value on its SC?
    //(3) is there a value on the sc's endpts? if there is we interpolate
    if(this->AttributeGet( what,w))
    {
        value = w.toDouble();//   wcstod( w.c_str(),&endptr);
        return true;
    }


    //(2)
    sc_ptr sc=CUrve[0];
    if(sc && sc->AttributeGet(what,w))
    {
        value = w.toDouble();//  wcstod( w.c_str(),&endptr);
        return true;
    }
    sc=CUrve[1];
    if(sc &&sc->AttributeGet(what,w))
    {
        value = w.toDouble();//  wcstod( w.c_str(),&endptr);
        return true;
    }
    //3) interpolate along any of the site's SCs
    Site * e1 , *e2; double v1,v2,off;
    for (int i=0;i<2;i++){
        sc=CUrve[i];
        if( !sc)
            continue;
        e1 = sc->End1site; e2= sc->End2site;
        if(!e1 ||!e2)
            continue;
        if(e1 &&e1->AttributeGet(what,w))// this is horrible.
            v1 = w.toDouble();//  wcstod( w.c_str(),&endptr);
        else
            continue;
        if(e2 &&e2->AttributeGet(what,w))
            v2 = w.toDouble();//  wcstod( w.c_str(),&endptr);
        else
            continue;
        off = this->Offsets[i]->Evaluate(sc,0) ;
        value =   v1 *(1.-off/sc->GetArc(0) ) + v2*( off/sc->GetArc(0) );
        return true;
    }

    return false;

    if (!AttributeGet(w,s))
        return false;
    value =s.toDouble();//  wcstod( s.c_str(),&endptr);
    return true;

    return rc;
}

bool RXSRelSite::AttributeGet(const QString what,QString &value) const{
    /*
Is used in get_contact_surface_ etc to examine the atts.  
and in get_fixing_ to find the fixings  

For a field node its the panel (itri, 211)

for resolved sites and relsites it initially the owning entity.

If its the first site on a pside
 if gets its attributes from (ps->e[0])
 and failing that from the SC
endif
If its the last site on a pside
 if gets its attributes from (ps->e[1])
 and failing that from the SC
endif
If its an intermediate node on a pside
 if gets its attributes from the SC

endif
*/
    QString w ;
    assert(dynamic_cast<const RXEntity*>(this)) ;

    if(this->RXEntity::AttributeGet( what,w))
    { value = w;return true;}

    RXEntity_p theEnt=CUrve[0];
    if(theEnt && theEnt-> AttributeGet(what,w))
    { value = w;return true;}
    theEnt=CUrve[1];
    if(theEnt && theEnt-> AttributeGet(what,w))
    { value = w;return true;}


    for (int k=0;k< this->m_psno  ;k++) {
        struct SIDEREF s = this->m_psList[k];
        PSIDEPTR  q = s.pp;
        RXSeamcurve *sc = q->sc;
        if(sc->AttributeGet(what,w))

        { value = w;return true;}
    }
    // this is nasty, but it lets Cut sites inherit the curves fixing
    // can always override with $fix=none
    if(this->RXEntity::AttributeGet( "$info",w))
    {
        w.chop(1); w.remove(0,1);
        QStringList qsl = w.split("+");
        for(int i=0;i<qsl.size();i++)
        {
            theEnt = this->GetSail()->Entity_Exists(qPrintable(qsl.at(i)),"seamcurve");
            if(theEnt)
                if(theEnt-> AttributeGet(what,w))
                { value = w;return true;}
        }
    }


    value.clear();
    return false;
}

// logic here is to find the find the tangent of the SC going away from the node, project to the XY plane
// and return the angle to the global X axis. 
// we should be working in 3D which means looking along the entity normal.  
// we'll need a reference which might be global X, 
// unless this  coincides with the entity normal in which case we'd take global Y 
// We should have a 'get entity trigraph' function and then transform the tangents to this system then take 
// the angle as 

int RXSRelSite::Get_Nodal_Angles()const{
    int k,end;
    double a;
    ON_3dVector v;
    RXOffset*offset;
    PSIDEPTR ps;
    struct SIDEREF *sp = this->m_psList;
    int n = this->m_psno;

    if(g_PanelInPlane <2) {
        for(k=0;k<n;k++) {
            ps = sp[k].pp;
            end = sp[k].end;
            if (!end) offset = ps->End1off;
            else      offset = ps->End2off;
            if (!Find_Poly_Tangent(ps->sc,offset,1,&v) )
                rxerror(" Find_Poly_Tangent failed",2);

            if (!end) {a =  atan2( (double) v.y, (double) v.x);  }
            else      {a =  atan2( (double)(-v.y), (double) (-v.x));}
            sp[k].m_srangle = a;
        }
        return 1;
    }
    else //  g_PanelInPlane is >=2
    {
        ON_3dVector l_vv;
        ON_Xform l_axes =this->Trigraph();
        for(k=0;k<n;k++) {
            ps = sp[k].pp;
            end = sp[k].end;
            if (!end) offset = ps->End1off;
            else      offset = ps->End2off;
            if (!Find_Poly_Tangent(ps->sc,offset,1,&v) ) 	rxerror(" Find_Poly_Tangent failed",2);

            l_vv=l_axes*v;

            if (!end) {a = atan2(  (double) l_vv.y, (double)  l_vv.x);  }
            else      {a = atan2( (double) -l_vv.y, (double) -l_vv.x);}
            sp[k].m_srangle = a;
        }
        return 1;
    }
    //return 0;
}//int Get_Nodal_Angles

static int NewNodal_Sort_Fn(const void *va, const void *vb) {
    
    double a0,a1;
    struct SIDEREF *ia = (struct SIDEREF *) va;
    struct SIDEREF *ib = (struct SIDEREF *) vb;

    PSIDEPTR e0 = ia->pp;
    PSIDEPTR e1 = ib->pp;
    a0 = ia->m_srangle;
    a1 = ib->m_srangle;

    //if ( a0 == a1) return(strcmp(e0->name(),e1->name()));
    //if ( a0 < a1) return(-1);
    //if ( a0 > a1) return( 1);
    //  return 0;

    if ( a0 < a1) return(-1);
    if ( a0 > a1) return( 1);
    return(strcmp(e0->name(),e1->name()));
}   

int RXSRelSite::Sort_One_Node()	{
    Site* p=this;
    int end,k,k1,k2;

    PSIDEPTR *plist;
    PSIDEPTR *elist;

    struct SIDEREF *sp = p->m_psList;
    int n = p->m_psno;
    p->Get_Nodal_Angles();

    if (sp) qsort(sp,n,sizeof(struct SIDEREF),NewNodal_Sort_Fn);

    plist = (PSIDEPTR *)MALLOC((n+1) *sizeof( PSIDEPTR ));
    elist = (PSIDEPTR  *)MALLOC((n+1) *sizeof( PSIDEPTR  ));


    for (k=0;k<n;k++) {
        struct SIDEREF s = p->m_psList[k];
        PSIDEPTR  q = s.pp;
        plist[k] = (PSIDEPTR )q;
        elist[k] = q;
    }

    for (k=0;k<n;k++) {
        struct SIDEREF s = p->m_psList[k];
        PSIDEPTR thisp;

        k1 = increment(k,n);
        k2 = decrement(k,n);
        end = s.end;

        if (end !=0 &&end!=1) {
            char errstr[256];
            sprintf(errstr," (internal) End has bad value %d\n",end);
            rxerror(errstr,99);    assert(0);
        }
        thisp = plist[k];

        (*thisp).er[end]=elist[k1];
        (*thisp).el[end]=elist[k2];
    }
    RXFREE(elist);
    RXFREE(plist);
    return(1);
}




int RXSRelSite::RemoveOnePside(const class RXPside *ps){
    int found  = 0;	int ifound = 0;	int k=0;

    for (k=0;k< this->m_psno;k++) {
        struct SIDEREF s =  this->m_psList[k];
        class RXPside * q  = s.pp;
        if (q==ps) {
            ifound=k;
            found=1;
            break;
        }
    }

    if (found) {
        (this->m_psno)--;
        for(k=ifound;k<this->m_psno;k++)
            this->m_psList[k] = this->m_psList[k+1];
        k = this->m_psno;
        this->m_psList = (struct SIDEREF *) REALLOC (this->m_psList,(k+1)*sizeof(struct SIDEREF));
        //Sort_One_Node(); //will reset elN and erN
    }
    return found;
}



int RXSRelSite::AddOnePside(const class RXPside *ps, const int e){
    int found=0;	int k=0;

    for (k=0;k< this->m_psno;k++) {
        struct SIDEREF s = this->m_psList[k];
        if (s.pp==ps) {found=1; break;}
    }
    if (!found) {
        (this->m_psno)++;
        k = this->m_psno;
        this->m_psList = (struct SIDEREF *)REALLOC (this->m_psList,(k+1)*sizeof(struct SIDEREF));
        this->m_psList[k-1].pp = (PSIDEPTR) ps;
        this->m_psList[k-1].end = e;
    }
    return !found;
}

ON_3dVector RXSRelSite::Normal(void )const{ // designed to replace getaddrZaxis

    struct PC_INTERPOLATION *p;
    RXEntity_p mould = GetSail()->GetMould();
    if(!mould)
        return GetSail()->Normal();
    if(!mould->dataptr)
        return GetSail()->Normal();

    p =  (struct PC_INTERPOLATION *) mould->dataptr;
    if(p->NurbFlag) {
        if(p->NurbData->m_pONobject){
            ON_Surface *l_s=ON_Surface::Cast( p->NurbData->m_pONobject );
            assert(l_s);
            double m0,r0,m1,r1,u,v;
            m0 = l_s->Domain(0).Min(); r0 = l_s->Domain(0).Length(); u = m_u * r0 + m0;
            m1 = l_s->Domain(1).Min(); r1 = l_s->Domain(1).Length(); v = m_v * r1 + m1;

            return l_s->NormalAt(u,v); // NormalAt doesnt seem to respond to Flip. The minus is a heuristic that works for TorusOuter04
        }
    }
    return GetSail()->Normal();
}  
int  RXSRelSite::Touch_Panels(){ 
    /* set Geometry_Has_Changed  on all adjacent panels */
    int j,k;
    Site *s = (Site *)this;
    for(k=0;k<s->PSCount();k++) {
        struct SIDEREF side= s->PSList(k);
        PSIDEPTR ps = side.pp;
        for(j=0;j<2;j++) {
            if (ps->leftpanel[j] ) {
                Panelptr   pan = ps->leftpanel[j];
                pan->Geometry_Has_Changed =1;
            }
        }
    }
    return(1);
}
