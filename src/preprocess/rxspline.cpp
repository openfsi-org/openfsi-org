#include "StdAfx.h"
#include <QDebug>
#include <QStringList>

#include "RXSail.h"
#include "RXQuantity.h"
#include "polyline.h"
#include "spline.h"

#include "rxspline.h"


//  we have
//  Resolve_ Base_Curve_Card , xyz with strange syntax, fills in (p,c)
//  Resolve_ 2D_Curve_Card   , xy with regular syntax, fills in (p,c)
//  Resolve_ 3D_Curve_Card,      , xyz with regular syntax, atts at end, fills in (p,c)
//  Resolve_ UVCurve_Card    , UVCurve : name : mould: atts :u,v,u,v,  fills in (p,c)
//  Resolve_ Spline_Card    , xyz with regular syntax, fills in  (pin)
//  Resolve_ Gauss_Curve_Card(RXEntity_p e);
RXSplineI::RXSplineI()
{
    assert("dont use default RXSplineI constructor"==0);
}

int  RXSplineI::ReWriteLine(void){

    RXSTRING  NewLine;
    cout<< "TODO: step rewrite of RXSplineI.\n" ;
    NewLine = TOSTRING(type()) + RXE_LSEPSTOWRITE +  TOSTRING(name());
    NewLine+= RXE_LSEPSTOWRITE; NewLine+=TOSTRING(this->c);

    for(int k=0;k<this->size();k++) {
        NewLine+= RXE_LSEPSTOWRITE; NewLine+=FromUtf8(this->getText(k,0));
        NewLine+= RXE_LSEPSTOWRITE; NewLine+=FromUtf8(this->getText(k,1));
        NewLine+= RXE_LSEPSTOWRITE; NewLine+=FromUtf8(this->getText(k,2));
    }
    this->SetLine(NewLine);

    return(1);
}


RXSpline::RXSpline()
{
    assert("dont use default RXSpline constructor"==0);
}
RXSpline::RXSpline(class RXSail *p ):
    RXSplineI (p)
{
    BCtype=BCSPLINE;
}
RXSpline::~RXSpline() {
    this->CClear();
}
int RXSpline::Compute(void){//see  int Compute_Basecurve(RXEntity_p p)

    assert(this->BCtype == BCSPLINE);
    VECTOR *pnew; int cnew;
    VECTOR *pold = new VECTOR[c];

      PC_Polyline_Copy(pold,this->p,this->c);
    this->m_pin.ToVectorArray( &pnew, &cnew);
    rxfcspline_(pnew, cnew,this->p, this->c );

    int change = PC_Polyline_Compare(pold,c,pnew,c, this->Esail->m_Linear_Tol);
    if(!change) qDebug()<<" rxspline didnt change ";
    delete [] pnew;
    delete[] pold;
    return(change);
}

int RXSpline::Finish(void){
    Compute();
    return 1;// see  Finish_Basecurve_Card
}

int RXSpline::CClear(){
    int rc=0;

    this->m_pin.~RXPolyline();
    return RXBCurve::CClear();
    return rc;
}

int RXSpline::Resolve() {
    int retval=0;
    /*
SPline : name : count :x :y:z:x:y:z,.....     */
    const char*sx, *sy,*sz;
    int k;
    std::string sline( this->GetLine() );
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();
    if(nw<9) return 0;

    const char *Type=wds[0].c_str();
    const char *name=wds[1].c_str();
    const char*s    =wds[2].c_str();
    if(!sscanf(s,"%d",&k))return(0);  // k is the number of interpolation pts

    this->c = k; int npts = (nw-3)/3;
    this->p = (VECTOR *)CALLOC(this->c,sizeof(VECTOR));

    k=3; cout<<"TODO:  rXSpline should be a triplet of EXPRESSIONS" <<endl;
    for(int i=0  ;i<npts;i++){
        sx=wds[k++].c_str();
        sy=wds[k++].c_str();
        sz=wds[k++].c_str();
        this->m_pin.push_back (RXTriplet(this->Esail  ,TOSTRING(sx) ,TOSTRING(sy),TOSTRING(sz)));
    }
    this->PC_Finish_Entity(Type,name,0,(long)0,NULL,Type,this->GetLine());

    this->Needs_Resolving=0;
    this->Needs_Finishing=1;
    this->SetNeedsComputing(0);
    return(1);

    return(retval);
}

int RXSpline::Dump( FILE *fp)
{
    fprintf(fp,"   A SPline curve. type=%d\n   Input Points\n",this->BCtype);
    this->m_pin.Dump(fp);
    fprintf(fp,"   Output Points\n");

    for (int k=0;k<this->c;k++){
        fprintf(fp,"\t\t\t%d \t%f\t%f\t%f\n",k, p[k].x,p[k].y,p[k].z);
    }
    return 0;
}

//int  RXSpline::ReWriteLine(void){

//    RXSTRING  NewLine;

//    cout<< "TODO: step rewrite of RXSpline.  It loses the expressions" ;
//    NewLine = TOSTRING(type()) + RXE_LSEPSTOWRITE +  TOSTRING(name());
//    NewLine+= RXE_LSEPSTOWRITE; NewLine+=TOSTRING(this->c);

//    for(int k=0;k<this->m_pin.size();k++) {
//        NewLine+= RXE_LSEPSTOWRITE; NewLine+=TOSTRING(m_pin[k].X());
//        NewLine+= RXE_LSEPSTOWRITE; NewLine+=TOSTRING(m_pin[k].Y());
//        NewLine+= RXE_LSEPSTOWRITE; NewLine+=TOSTRING(m_pin[k].Z());
//    }
//    this->SetLine(NewLine);

//    return(1);
//}
std::string RXSpline::getText(const int i, const int j)
{
    std::stringstream s;
    switch  (j) {
    case 0:
        s<<this->X(i);
        break;
    case 1:
        s<<this->Y(i);
        break;
    case 2:
        s<<this->Z(i);
        break;
    default:
        assert(0);
    };

    return s.str();
}


/////////////////////RXExpSpline/////////////////////

RXExpSpline::RXExpSpline()
{
    assert("dont use default RXExpSpline constructor"==0);
}
RXExpSpline::RXExpSpline(class RXSail *p ):
    RXSplineI (p),
    m_npts(0)
{
    BCtype=BCSPLINE;
}
RXExpSpline::~RXExpSpline() {
    this->CClear();
}

int RXExpSpline::Compute(void){//see  int Compute_Basecurve(RXEntity_p p)
    int rc;
    assert(this->BCtype == BCSPLINE);
    assert(sizeof(VECTOR)==3*sizeof(float));
    VECTOR *old = new VECTOR[this->c];
    float *pin; int cin;
    this->ToFloatArray( &pin, &cin);
    PC_Polyline_Copy(old,this->p,this->c);
    rxfcspline_((VECTOR*)pin, cin,this->p, this->c );
    rc = PC_Polyline_Compare(old,this->c,this->p,this->c, this->Esail->m_Linear_Tol );
    delete [] pin;
    delete[] old;
    return(rc);
}

int RXExpSpline::Finish(void){
    Compute();
    return 1;
}

int RXExpSpline::CClear(){
    int rc=0;

    return RXBCurve::CClear();
    return rc;
}

int RXExpSpline::Resolve() {
    int retval=0;

    RXExpressionI *exp;
    // assert("this is ok except that the exps appear twice in the object list. Once as 'a' and once as 'pas'"==0);
    /*
spline : name : count :x ,y,z,x,y,z,.....     */
     RXSTRING sx, sy,sz;
    int i, k; bool OK;
    const QString &qline =  this->GetLine();
    QStringList wds= rxParseLine(qline,RXENTITYSEPREGEXP );
    int nw = wds.size();
    if(nw<9) return 0;
    k = wds[2].toInt(&OK);
    if(!OK)return(0);  // k is the number of interpolation pts

    this->c = k;  m_npts = (nw-3)/3;
    this->p = (VECTOR *)CALLOC(this->c,sizeof(VECTOR));

    for( i=0,k=3 ;i<m_npts,k< nw;i++){
        sx=wds[k++].toStdWString() ;
        sy=wds[k++].toStdWString();
        sz=wds[k++].toStdWString();

        exp=this->AddExpression (new RXQuantity(TOSTRING(L"x")+TOSTRING(i),sx,_T("m"),this->Esail ));//done
        if( !exp->ResolveExp() )
        { this->CClear();   return(0);}
        this->SetRelationOf(exp,aunt|spawn,RXO_X_OF_SPLINE,i);//the Enode will take care of deletion

        exp=this->AddExpression (new RXQuantity(TOSTRING(L"y")+TOSTRING(i),sy,_T("m"),this->Esail ));
        if( !exp->ResolveExp() )
        { this->CClear();   return(0);}
        this->SetRelationOf(exp,aunt|spawn,RXO_Y_OF_SPLINE,i);

        exp=this->AddExpression (new RXQuantity(TOSTRING(L"z")+TOSTRING(i),sz,_T("m"),this->Esail ));
        if( !exp->ResolveExp() )
        { this->CClear();   return(0);}
        this->SetRelationOf(exp,aunt|spawn,RXO_Z_OF_SPLINE,i);
    }
    this->PC_Finish_Entity(qPrintable(wds[0]),qPrintable(wds[1]),0,(long)0,NULL,0,this->GetLine());

    this->Needs_Resolving=0;
    this->Needs_Finishing=1;
    this->SetNeedsComputing(0);
    return(1);

    return(retval);
}
double  RXExpSpline::X(const int i){
    RXExpressionI *q  = dynamic_cast<class RXExpressionI*> (this->GetOneRelativeByIndex(RXO_X_OF_SPLINE,i));assert(q);
    return q->evaluate();

}
double  RXExpSpline::Y(const int i){
    RXExpressionI *q  = dynamic_cast<class RXExpressionI*> (this->GetOneRelativeByIndex(RXO_Y_OF_SPLINE,i));assert(q);
    return q->evaluate();
}
double  RXExpSpline::Z(const int i){
    RXExpressionI *q  = dynamic_cast<class RXExpressionI*> (this->GetOneRelativeByIndex(RXO_Z_OF_SPLINE,i));assert(q);
    return q->evaluate();
}
int  RXExpSpline::SetX(const int i, const RXSTRING &text) {
    assert(i>=0 && i< this->m_npts);
    RXExpressionI *q = dynamic_cast<class RXExpressionI*> (this->GetOneRelativeByIndex(RXO_X_OF_SPLINE,i));
    assert(q);
    return q->Change(text);
}
int  RXExpSpline::SetY(const int i, const RXSTRING &text)
{
    assert(i>=0 && i< this->m_npts);
    RXExpressionI *q = dynamic_cast<class RXExpressionI*> (this->GetOneRelativeByIndex(RXO_Y_OF_SPLINE,i));
    assert(q);
    return q->Change(text);
}
int  RXExpSpline::SetZ(const int i, const RXSTRING &text)
{
    assert(i>=0 && i< this->m_npts);
    RXExpressionI *q = dynamic_cast<class RXExpressionI*> (this->GetOneRelativeByIndex(RXO_Z_OF_SPLINE,i));
    assert(q);
    return q->Change(text);
}
int  RXExpSpline::size() {
    return m_npts;
}

int RXExpSpline::Dump( FILE *fp)
{
    fprintf(fp,"   A SplineExp curve. type=%d\n   Input Points\n",this->BCtype);
    int i,k;
    for(i=0; i<this->m_npts;i++)
    {
        for(k=0;k<3;k++) {
            std::string s =  std::string( "mystring");
            cout<< getText(i,k);
            const char *lp =s.c_str();
            fprintf(fp,"  %s", lp);
        }
        fprintf(fp,"\n");
    }
    fprintf(fp,"   Output Points\n");

    for (int k=0;k<this->c;k++){
        fprintf(fp,"\t\t\t%d \t%f\t%f\t%f\n",k, p[k].x,p[k].y,p[k].z);
    }
    return 0;
}
std::string RXExpSpline::getText(const int i, const int j)
{
    assert(i>=0 && i< this->m_npts);

    RXExpressionI *q;
    switch  (j) {
    case 0:
        q = dynamic_cast<class RXExpressionI*> (this->GetOneRelativeByIndex(RXO_X_OF_SPLINE,i));
        return ToUtf8(q->GetText());
    case 1:
        q = dynamic_cast<class RXExpressionI*> (this->GetOneRelativeByIndex(RXO_Y_OF_SPLINE,i));
        return ToUtf8(q->GetText());
    case 2:
        q = dynamic_cast<class RXExpressionI*> (this->GetOneRelativeByIndex(RXO_Z_OF_SPLINE,i));
        return ToUtf8(q->GetText());
    default:
        assert(0);
        return std::string("none");
    };
    return std::string("none");
}
// we do this long-hand because it provides a check on
int RXExpSpline::ToFloatArray( float **p, int *p_c)
{

    RXExpressionI *q;
    int c= this->size();
    *p=new float[3*c];
    float *lp ; int i;
    for( i=0, lp=*p;;i++ ) {
        q = dynamic_cast<class RXExpressionI*> (this->GetOneRelativeByIndex(RXO_X_OF_SPLINE,i));if(!q) break;
        *lp = (float) q->evaluate();
        lp++;
        q = dynamic_cast<class RXExpressionI*> (this->GetOneRelativeByIndex(RXO_Y_OF_SPLINE,i)); if(!q) break;
        *lp = (float) q->evaluate();
        lp++;
        q = dynamic_cast<class RXExpressionI*> (this->GetOneRelativeByIndex(RXO_Z_OF_SPLINE,i)); if(!q) break;
        *lp = (float) q->evaluate();
        lp++;

    }
    assert(i==m_npts);

    *p_c=c;
    return c;

}
