#pragma once
#include "RXStrFuncI.h"

class RXCurveLength :
	public  RXStrFuncI
{
public:
	RXCurveLength(void); 
	RXCurveLength(class RXSail *const p, class RXObject *const o) ;
	~RXCurveLength(void);

	virtual MTDOUBLE evaluate(const MTSTRING*s) ;
	virtual const MTCHAR* getSymbol(){return _T("length"); }

	virtual const MTCHAR* getHelpString(){ return _T("length([sail$]name)"); }
	virtual const MTCHAR* getDescription(){ return _T("length(sail$name) or length(name) "); }	
	virtual int getNbArgs(){ return 1; }
	
	virtual MTFunctionI* spawn() { return new RXCurveLength(); }
};

class RXSiteX :
	public  RXStrFuncI
{
public:
	RXSiteX(void);
	RXSiteX(class RXSail *const p, class RXObject *const o) ;
	~RXSiteX(void);

	virtual MTDOUBLE evaluate(const MTSTRING*s) ;
	virtual const MTCHAR* getSymbol(){return _T("x"); }

	virtual const MTCHAR* getHelpString(){ return _T("x([sail$]name)"); }
	virtual const MTCHAR* getDescription(){ return _T("look up the x of a point  ([sail$]name)"); }	
	virtual int getNbArgs(){ return 1; }
	
	virtual MTFunctionI* spawn() { return new RXSiteX(); }
};
class RXSiteY :
	public  RXStrFuncI
{
public:
	RXSiteY(void);
	RXSiteY(class RXSail *const p, class RXObject *const o);
	~RXSiteY(void);

	virtual MTDOUBLE evaluate(const MTSTRING*s) ;
	virtual const MTCHAR* getSymbol(){return _T("y"); }

	virtual const MTCHAR* getHelpString(){ return _T("y([sail$]name)"); }
	virtual const MTCHAR* getDescription(){ return _T("look up the y of a point ([sail$]name)"); }	
	virtual int getNbArgs(){ return 1; }
	
	virtual MTFunctionI* spawn() { return new RXSiteY(); }
};
class RXSiteZ :
	public  RXStrFuncI
{
public:
	RXSiteZ(void);
	RXSiteZ(class RXSail *const p, class RXObject *const o) ;
	~RXSiteZ(void);

	virtual MTDOUBLE evaluate(const MTSTRING*s) ;
	virtual const MTCHAR* getSymbol(){return _T("z"); }

	virtual const MTCHAR* getHelpString(){ return _T("z([sail$]name)"); }
	virtual const MTCHAR* getDescription(){ return _T("look up the z of a point ([sail$]name)"); }	
	virtual int getNbArgs(){ return 1; }
	
	virtual MTFunctionI* spawn() { return new RXSiteZ(); }
};
