// RLXDblMatrix.h: interface for the RLXDblMatrix class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RLXDBLMATRIX_H__A79AC444_8FEC_11D6_8648_C68AB2091277__INCLUDED_)
#define AFX_RLXDBLMATRIX_H__A79AC444_8FEC_11D6_8648_C68AB2091277__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "DoubleArray.h"

class RLXDblMatrix  
{
public:
	double * GetArray();
	RLXDblMatrix();
	RLXDblMatrix(const int & p_ColNb, 
				  const int & p_RowNb);
	RLXDblMatrix(const RLXDblMatrix & p_Matrix);
	RLXDblMatrix(const double * p_pMatrix,
				  const int   &   p_ColNb, 
				  const int   &   p_RowNb);
	RLXDblMatrix(double ** p_pMatrix,
				  const int   &   p_ColNb, 
				  const int   &   p_RowNb);
	virtual ~RLXDblMatrix();
	void Initialise(const int & p_ColNb, 
			  const int & p_RowNb);
	void Clear();
	RLXDblMatrix& operator = (const RLXDblMatrix & p_Matrix);
	double& operator() (const int & p_Col, const int & p_Row);
	double  operator() (const int & p_Col, const int & p_Row) const;
	RLXDblMatrix operator + (const double &p_Val);
	RLXDblMatrix operator - (const double &p_Val);
	RLXDblMatrix operator * (const double &p_Val);
	RLXDblMatrix operator / (const double &p_Val);
	void GetSize(int &p_colNb, int &p_rowNb) const;
	int GetColNb() const;
	int GetRowNb() const;
	void JoinCol(const RLXDblMatrix & p_Matrix);
	void JoinRow(const RLXDblMatrix  & p_Matrix);
	RLXDblMatrix GetRange(const int & p_ColInf, 
						  const int & p_ColSup, 
						  const int & p_RowInf, 
						  const int & p_RowSup) const;
	void Transpose();
	RLXDblMatrix T() const ;
	RLXDblMatrix GaussSolve(const RLXDblMatrix & p_Vector);
	RLXDblMatrix SolveLeastSquare(const RLXDblMatrix & p_P) const;
	double ** GetArray2D();
	BOOL IsSquare() const;
	void PrintToFile(FILE * fp) const;
	void ReadFromFile(FILE *fp);

		//!DGESV computes the solution to a real system of linear equations
	/*!
		 A * X = B,
		where A is an N-by-N matrix and X and B are N-by-NRHS matrices.
	*/
	RLXDblMatrix Solve_dgesv_(RLXDblMatrix & p_B);

	friend RLXDblMatrix operator+ (const RLXDblMatrix & p_Matrix1,
							        const RLXDblMatrix & p_Matrix2);
	friend RLXDblMatrix operator- (const RLXDblMatrix & p_Matrix1,
							        const RLXDblMatrix & p_Matrix2);
	friend RLXDblMatrix operator* (const RLXDblMatrix & p_Matrix1,
   							        const RLXDblMatrix & p_Matrix2);
	friend RLXDblMatrix operator+ (const double     & p_Val, 
							        const RLXDblMatrix & p_Matrix);
	friend RLXDblMatrix operator- (const double     & p_Val, 
							        const RLXDblMatrix & p_Matrix);
	friend RLXDblMatrix operator* (const double     & p_Val, 
							        const RLXDblMatrix & p_Matrix);
protected:
	ON_SimpleArray<double> m_pArray;
	int      m_RowNb;
	int      m_ColNb;
	void UpDateSize(const int & p_ColNb, const int & p_RowNb);
};
//FRIEND Prototypes
extern inline RLXDblMatrix operator+ (const RLXDblMatrix & p_Matrix1,
									const RLXDblMatrix & p_Matrix2);
extern inline RLXDblMatrix operator- (const RLXDblMatrix & p_Matrix1,
									const RLXDblMatrix & p_Matrix2);
extern inline RLXDblMatrix operator* (const RLXDblMatrix & p_Matrix1,
							        const RLXDblMatrix & p_Matrix2);
extern inline RLXDblMatrix operator+ (const double & p_Val,
								       const RLXDblMatrix & p_Matrix);
extern inline RLXDblMatrix operator- (const double & p_Val,
								       const RLXDblMatrix & p_Matrix);
extern inline RLXDblMatrix operator* (const double & p_Val,
								    const RLXDblMatrix & p_Matrix);
#endif // !defined(AFX_RLXDBLMATRIX_H__A79AC444_8FEC_11D6_8648_C68AB2091277__INCLUDED_)
