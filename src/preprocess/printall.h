	/* printall.h */
#ifndef _PRINTALL_HDR_
#define _PRINTALL_HDR_ 
extern double  TotalGaussArea;
EXTERN_C int Print_BroadSeam(sc_ptr sc, FILE *fp);
EXTERN_C int Print_All_Entities(SAIL *const sail, const char*a);

EXTERN_C int To_Panellist(const char*fname,float space);
EXTERN_C int write_stress_by_triangle(SAIL *p_sail,const char*file);
EXTERN_C int write_D_by_triangle(SAIL *p_sail,const char*file);
EXTERN_C int Print_End(RXEntity_p ep,const char *text,FILE *fp);
EXTERN_C int Print_Table(FILE *fp, struct PC_TABLE *t);
#endif  //#ifndef _PRINTALL_H_

