#include "StdAfx.h"

#include "RXOptimisation.h"
#ifdef HOOPS  // was NOT  RHINO_PLUGIN
  #include "AMGTapingZone.h" 
#endif
#include <assert.h>
RXOptimisation::RXOptimisation(void)
{
	m_nfuncCalls=0;
	m_goal_function=rxo_testfunc;
	m_goal_function_user_data=0;
	m_caller=0;
	m_nmax=23;
	m_tol=0.001;
}
RXOptimisation::RXOptimisation(void*p)
{
 	m_nfuncCalls=0;
	m_goal_function=rxo_testfunc;
	m_goal_function_user_data= p;
	m_caller=0;
	m_nmax=29;
	m_tol=0.002;
}

RXOptimisation::~RXOptimisation(void)
{
	if(m_caller) m_caller->SetOpt(0);
}
void RXOptimisation::SetCaller(class RXOptimisationCaller *p)
	{ m_caller=p;
	if(p && p->GetOpt()!=this)
		p->SetOpt(this);
	}


RX_DP RXOptimisation::func(ON_SimpleArray<double> p_x)
		{
			m_nfuncCalls++;
			printf("\tnvars=%d\t",p_x.Count()); //assert( p_x.Count());
			for(int i=0; i<p_x.Count();i++) printf(" %13.7g \t",p_x[i]); fflush(stdout);
            RX_DP rv = (m_goal_function)(this, m_goal_function_user_data, p_x);
			printf("\t  func= \t%18.10g\n",rv);
		 	if(m_caller&&m_caller->GetStopFlag())
		 		m_nfuncCalls = this->m_nmax +10;
			return rv;
}
int RXOptimisation::Print(FILE *fp)
{
	int i,j, k =fprintf(fp," RXOptimisation::Print nfuncCalls=%d/%d\t tol=%f\n",m_nfuncCalls, m_nmax,m_tol);

	for(i=0;i<m_x.Count();i++) 
		k+= fprintf(fp,"  %d\t %14.6g\t %14.6g\t %14.6g\n", i, m_x[i],m_x0[i],m_scale[i]);
	fflush(stdout);
return k;
}

 int RXOptimisation:: SetValues(RXOptVarList *p_OptVarList,ON_SimpleArray<double> &p_x) //Fills in the value membersof p_OptVarList by copying from the solver�s m_X array. 

	{
int i,c=0;
	for(i=0;i<p_OptVarList->Count ();i++) {
		if((*p_OptVarList)[i].m_index<0)
			continue;
		c = (*p_OptVarList)[i].m_index; assert(c>=0);
 		(*p_OptVarList)[i]. m_value=p_x[c];
	}
	return c;
}// RXOptimisation::ExtractResults



int RXOptimisation::ExtractResults(RXOptVarList *p_OptVarList) //Fills in the value membersof p_OptVarList by copying from the solver�s m_X array. 

	{
int i,c=0;
double dbg;
	for(i=0;i<p_OptVarList->Count ();i++) {
		RXOptVariable *vv = (*p_OptVarList).At(i);
		if((*p_OptVarList)[i].m_index<0)
			continue;
		c = (*p_OptVarList)[i].m_index; assert(c>=0);
 		(*p_OptVarList)[i]. m_value=m_x[c];
		(*p_OptVarList)[i]. m_x0=m_x0[c];
		dbg = m_scale[c];
		(*p_OptVarList)[i]. m_steplengthscaled=dbg; //plutot scaled??
	}


	return c;
}// RXOptimisation::ExtractResults



bool RXOptimisation::FindMinimum()  { // just walks around the space 

	static int d;
double a1,da,x0,x1;
static RX_DP best=1e50;
static ON_SimpleArray<double> bestvector;
if(!d) {
	//print the header
}
#ifdef WIN32 
 {    
    MSG Msg;    HWND hWnd=0 ;
	UINT wMsgFilterMin=WM_KEYFIRST;
	UINT wMsgFilterMax=WM_KEYLAST;
	UINT wRemoveMsg =PM_NOREMOVE;

	if(PeekMessage(&Msg,hWnd,wMsgFilterMin,wMsgFilterMax,wRemoveMsg) )
	{
//	 cout<< "a message"<<endl;  
		
	}
}
#endif
if(d<m_x.Count()) {
//	da = 4.*m_scale[d]/((double)m_nmax);
//	for( a1 = m_x0[d]-2.*m_scale[d]; a1<=m_x0[d] +2.*m_scale[d]; a1+=da ) {
	da = m_scale[d];
	x0 = m_x0[d]- da*(double)m_nmax/2; 
	x1 = m_x0[d]+ da*(double)m_nmax/2; 
	for( a1 = x0; a1<= x1; a1+=da ) {
		m_x[d]=a1;
		d++;
		if(!FindMinimum()) return false;
		d--;
	}
}
else {
	int i;
	//printf("depth = %d ",d);
	for(i=0;i<m_x.Count();i++) printf("\t%f",m_x[i]); 
	fflush(stdout);
    RX_DP result = func(m_x);
//	printf("\t  func=  \t %15.10g\n",result);
	if(result<best) {
		 best=result;
		 bestvector.Empty();
		bestvector=m_x;  
	}
	fflush(stdout);
#if not defined( RHINO_PLUGIN)  &&not defined(RXQT)
	HC_Update_Display();
#endif
}
if(!d) {
 	m_x.Empty(); m_x=bestvector;
}
return true;
} //  RXOptimisation::FindMinimum() JUST CALLS func on a grid 

int RXOptimisation::SetStart(RXOptVarList *p_OptVarList) 
// fills in x0,scales and indices of OptVars from the OptVarList.
	{
	// 1) count the active members of the list and allocate the arrays.
	int i,c=0;
	for(i=0;i<p_OptVarList->Count ();i++) {
		if((*p_OptVarList)[i].m_active)
			c++;
	}
		m_x.Empty();    	m_x.SetCapacity (c);	m_x.SetCount (c);
		m_x0.Empty();   	m_x0.SetCapacity(c);	m_x0.SetCount (c);
		m_scale.Empty();	m_scale.SetCapacity (c);m_scale.SetCount (c);

	// 2) for each active member, fill in its index and copy its members values to the optimisation arrays
	c=0;
	for(i=0;i<p_OptVarList->Count ();i++) {
		if((*p_OptVarList)[i].m_active) {
			m_x[c]=(*p_OptVarList)[i]. m_value;
			m_x0[c]=(*p_OptVarList)[i]. m_x0;
			m_scale[c]=(*p_OptVarList)[i]. m_steplengthscaled;
			assert(ON_IsValid(m_scale[c]));
			(*p_OptVarList)[i].m_index=c;
			c++;
		}
		else 
			(*p_OptVarList)[i].m_index=-1;
	}
	return c;
} /// RXOptimisation::SetStart


RX_DP  rxo_testfunc(class RXOptimisation  *p_opt, void *p_user_data, ON_SimpleArray<double> &p_x){

	RXOptimisationCaller *c= p_opt->GetCaller(); // gives access to its m_vars. We should see what we can cast it to.
	return 1.0;
}
void RXOptimisation::SetTolerance(const RX_DP p_tol)
{
	m_tol =p_tol;
}
void RXOptimisation::SetMaxCycles(const int p_n)
{
m_nmax=p_n;
}
