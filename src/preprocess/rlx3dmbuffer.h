#ifndef _RLX3DMBUFFER_H_
#define _RLX3DMBUFFER_H_

#include "griddefs.h"
#include "nurbs.h"

//EXTERN_C  int Resolve_3DM_Card(RXEntity_p e);
EXTERN_C  double EvaluateONNurbsSurface(struct PC_NURB* p_pNurbs,const double u,const double v,ON_3dPoint*r);
EXTERN_C  int EvaluateONNurbsDerivatives(struct PC_NURB* p_pNurbs,const double u,const double v,
										 ON_3dPoint*r,
										 ON_3dVector*r10, ON_3dVector*r01,
										 ON_3dVector*r11, ON_3dVector*r20, ON_3dVector*r02);

#endif  //#ifndef _RLX3DMBUFFER_H_


