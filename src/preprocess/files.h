/* header files for 
 * directory structory.
 *
 * NOTE: all of them end WITHOUT trailing backslash 
 */

#ifndef _HDR_FILES_
#define _HDR_FILES_
#include <QtCore>

#ifdef _X
	#include <X11/Intrinsic.h>
	EXTERN_C char *getfilename(Widget parent,const char *pattern,const char *prompt,const char *dir,const int readwrite);
#else
extern QString getfilename(QObject *client,const QString pattern,const QString prompt,const QString dir,const int readwrite);
#endif   

	#define PC_FILE_READ 1
	#define PC_FILE_WRITE 0
        #define PC_FILE_DIR 2

#endif //#ifndef _HDR_FILES_
