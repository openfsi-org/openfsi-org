/*  debug.h for Relax. the tope-level definitions.
ALL source code should include this file
EG it redefines the free function to do some checks */
#ifdef AMGTAPER
#error("dont use debug.h in AMGTaper")
#endif
#ifndef _DEBUG_HDR_
#define _DEBUG_HDR_
#ifdef __cplusplus

// this block MUST match RXGraphicCalls.h
#ifdef HOOPS
    #include "hc.h"
    #define RXGRAPHICOBJECT HC_KEY
    #define RXGRAPHICSEGMENT HC_KEY
    #define RX_Insert_Polyline(a,b,c,d )   HC_Insert_Polyline(a,b)
    #define RX_Insert_Line(cx,cy,cz,dx,dy,dz,node)   HC_Insert_Line(cx,cy,cz,dx,dy,dz)
    #define HC_KInsert_Polygon(c,p,n )  HC_Insert_Polygon(c,p)
    #define RX_Insert_Marker(x,y,z,n)  HC_Insert_Marker(x,y,z)
#elif defined (USE_GLC)
    typedef  class GLC_Geometry* RXGRAPHICOBJECT;
    typedef  class GLC_StructOccurence* RXGRAPHICSEGMENT;
    #include <GLC_Global>
    #define RXGR_KEY  GLC_uint
    #define HC_KEY  RXGR_KEY

#elif defined(COIN3D)
    #include <Inventor/nodes/SoGroup.h>
    #include <Inventor/nodes/SoNode.h>
    typedef class SoNode* RXGRAPHICOBJECT ;
    typedef class SoGroup* RXGRAPHICSEGMENT;
    #define HC_KEY RXGRAPHICSEGMENT
#else
    typedef void* RXGRAPHICOBJECT ;
    typedef void* RXGRAPHICSEGMENT;
    #define HC_KEY RXGRAPHICSEGMENT
#endif



	typedef class RXSail  SAIL;  
/* A note on the various typedefs for differnt kinds of Site.
	The meshing code used for interpolation grids 

*/

typedef double MatValType;


	#define EPS            1.e-5
	  /* Adam has 1e-7 for years. */
	#define EPSSQ		(double) 1.e-10
/* tol2sq is used in samesiterough */
	#define TOL2SQ		((double) (1.0e-8))  /*Peter Test */
	#define TOL2  	((double) (1.0e-4)) 
/*			((double) (1.0e-12))  Adam*/


	#define FLOAT_TOLERANCE  ((double) 0.000001)
	#define BLACKCURVE 1
	#define REDCURVE 0

	typedef  class RXEntity* RXEntity_p ; 
// RXSiteBase is a virtual class
	typedef class RXSRelSite  Site;  
 	typedef class RXSitePt MeshSite;  
	typedef  class RXSeamcurve* sc_ptr; 
	typedef  class RXSeamcurve  sc_t; 
	typedef class layernodestack layernodestack;

	typedef class RXPside	Pside;                 /* for adam */
	typedef class RXPside*   PSIDEPTR;
	typedef class RX_FEedge FortranEdge;
	typedef class RX_FETri3 FortranTriangle;

#else
	#define SAIL void
	#define Site void
#endif

typedef class RXQuantity   Quantity;
typedef class RXQuantity*  Quantity_p;
#ifdef RXQT
typedef class RXSubWindowHelper  Graphic ;
#else
typedef struct GRAPHIC_STRUCT  Graphic ;
#endif
#ifdef _X
	typedef struct press_rowcol_s  press_rowcol_t, *press_rowcol_p;
	typedef struct relax_rowcol_s  relax_rowcol_t, *relax_rowcol_p;
	typedef struct control_data_s  control_data_t;
#endif

#ifdef UNICODE
	#include <string>
	#define RXSTRING		std::wstring
	#define TOSTRING(a)		to_wstring(a)
//	#define TOCSTRING(a)	to_string(a) // use ToUtf8
#else
	#error (" are you sure you want to build without UNICODE")
	#include <string>
	#define RXSTRING		std::string
	#define TOSTRING(a)		to_string(a)
//	#define TOCSTRING(a)	to_string(a)//ToUtf8
#endif
		
	//We'd like to be able to use wchar_t throughout but
// this won't interface with some 3rd party code or with fortran
// so just use narrow-char for now.
	#define RXCHAR			char 



#ifdef __cplusplus
// lets 's use the hoops prototype for streq, strieg
    #include "opennurbs.h"
    #define ON_V4
    #ifdef linux
        #ifdef ON_V4
                    #include "opennurbs_staticlib_linking_pragmas.h"
        #else
                    #include "examples_linking_pragmas.h"  // opennurs Version 5
        #endif
    #endif
    #define EXTERN_C extern "C"
    #undef BOOL
#else
    #pragma message("debug.h:  EXTERN_C defined for C")
    #define EXTERN_C extern
    #undef BOOL  //added by thomas 09 11 04
#endif  //#ifdef __cplusplus

        #ifndef WIN32
		#define _NOT_MSW
	#endif  //#ifdef WIN32


	#if defined (linux) || defined(_GNU_SOURCE) 
		#define _NOT_MSW
		#define _stricmp strcasecmp
		#define strnicmp strncasecmp
		#define stricmp strcasecmp
		#define PC_Post_Message(msg)   rxerror(msg,2)
		#define _isnan  isnan
		#include "unixver.h"
	#else
		#ifdef WIN32 
			#define isnan _isnan
			//#define _stricmp strcasecmp
		#define strnicmp strncasecmp
		//#define stricmp strcasecmp
		#endif  //#ifdef WIN32
	#endif  //#ifdef linux

	#include <stdio.h>
	#include <string.h>	  /* changed order to suppress a MSVC compilation mess */
	#include <stdlib.h>
	#include <math.h>
	#include <assert.h>

#if defined (AMGCONTROLPLOTER) || defined(AMGUI_2005)
	#include "AMGMacro.h"
#endif

#define DBG_FULLPRINT	1
#define DBG_SILENT	0
#define DBG_MARK	2
#define DBG_ALL 	4

#define DB_MATERIALS 4
#define DB_TRIS	     8 
#define RX_MAX_TYPES	200  // all ETYPES shall be less than this.

#define DEFAULT_REPORTING 1  /* used in reporting */
#if(defined _RXMALLOC) 
	void* MALLOC(const size_t a);// {void*p=malloc(a); if(!p) _rxthrow("malloc fails"); return p;}
	void* CALLOC(size_t a,size_t b);// {void*p=calloc(a,b); if(!p)_rxthrow("calloc fails"); return p;}
	void* REALLOC(void *a,size_t b); //{void*p=realloc(a,b); if(!p)_rxthrow("realloc fails"); return p;}
	#define RXFREE(a)  free(a) 
	#define STRDUP(a) strcpy((char *) MALLOC(1+strlen(a)),a) 
#else
	#define MALLOC malloc
	#define CALLOC calloc
	#define REALLOC realloc
	#define RXFREE  free 
	#ifndef linux
	//	#define STRDUP(a) strcpy((char *) malloc(1+strlen(a)),a)  // Intel doesnt like this
	#define STRDUP(a) _strdup(a)
	#else
		EXTERN_C char * STRDUP(const char *);
	#endif
#endif
	


		EXTERN_C int Set_MALLOC_DATA(const char*c);
		EXTERN_C int Set_Previous_MALLOC_DATA(void);
#ifdef __cplusplus
		EXTERN_C int Check_All_Heap(int i = DBG_SILENT);
#else
		EXTERN_C int Check_All_Heap(int i);	
#endif
		EXTERN_C int Print_One_Heap(void *p);
		EXTERN_C int MEMCHECK(void*p);
		EXTERN_C long  NEXTBLANKMEM(void);
		EXTERN_C long RENUMBER(void*p, long newindex);
		EXTERN_C int Repack_Heap(int) ;
 
                EXTERN_C   FILE *RXFOPEN ( const char *filename, const char *type);
                EXTERN_C   FILE *FOPEN_S ( const std::string &filename, const char *type);
		EXTERN_C  int FCLOSE (FILE *stream);
		EXTERN_C int List_C_Files(void);
		EXTERN_C int Close_C_Files(void);

		// replaces certain environment variables

int filename_copy_local(char *out, const int lout, const char *in);

		#ifdef MY_XTMALLOC
			#define XtMalloc MALLOC
			#define XtCalloc CALLOC
			#define XtRealloc REALLOC
			#define XtFree RXFREE
		#endif  //#ifdef MY_XTMALLOC
		#ifdef _WRAP
			EXTERN_C void  __wrap_free(void*);
			EXTERN_C void* __wrap_malloc(long int);
			EXTERN_C void* __wrap_calloc(long int, long int);
			EXTERN_C void* __wrap_realloc(void*, long int);
		#endif  //#ifdef _WRAP
#ifdef _MSC_VER
	#pragma warning ( disable : 4244 4100 4127 4189 4706)
	#pragma warning (error: 4701)
	#pragma warning ( disable : 4211)
#endif

#ifndef RLX_DEFAULT_UNITS // for TR's AMG projects we dont scale to meters.
		#define RLX_DEFAULT_UNITS  ON::meters
#endif


#ifdef rxerror  // peter 18 may 2007
	#undef rxerror
	#error("why is rxerror already defined????")
#endif

	#define rxerror(a,b)  this->OutputToClient(a,b) 


#endif //#ifndef _DEBUG_HDR_


