/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
Nov 2002   Resolve_String now places into seg ./string/<name> and includes into
?Inclu de_ historic/designs/strings/sail_<type>_<ppp>/<name>
  *	Oct 97 cplus comment
 *   	May 1997. A problem with interactive working
 Because the 'resolve' creates the fedges, these should be destroyed
 by a future UnR-solve - or maybe by a 'Clear-Entity'

 *	apr 97  cn c all oced even on NODE-type strings. Because cn[0] is deferenced elsewhere

 *	apr 97 set selectability ON for strings. For curiosity only
 *	aug 96  insert as Ink not Line
 *           june 1996 working on pars/deps
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"

#include "RXQuantity.h"
#include "RX_FEString.h"
#include "RXExpression.h" 
#include "akmutil.h"
#include "entities.h"
#include "resolve.h"

#include "words.h"

#include "readstring.h"

#define PCS_NEW 4
#define PCS_OLD 8
RXEntity_p PCS_Create_Curve_From_Ends(RXEntity_p p1,RXEntity_p p2,char *atts, const int pDepth){
    const char *n1, *n2; char *buf,*cname;
    RXEntity_p sce;
    size_t  la=0;
    int depth=pDepth+1;
    if(!p1) return NULL;
    if(!p2) return NULL;
    n1 = p1->name(); n2 = p2->name();
    cname = (char *)CALLOC(strlen(n1) + strlen(n2) +16,1);
    sprintf(cname,"%s_%s",n1,n2);
    if(atts) la = strlen(atts);
    buf = (char *)CALLOC(strlen(n1) + strlen(n2) + strlen(cname) + la +128,1);
    sprintf(buf,"curve:%s : %s : %s : straight:0.0: edge,sketch,xscaled,yscaled,flat",cname,n1,n2);
    if(atts) strcat(buf,atts);
    sce = Just_Read_Card( p1->Esail,buf,"curve",cname,&depth);
    if(sce){
        if(! sce->Resolve()) {
            sce->Kill(); sce=NULL;}
    }
    RXFREE(buf); RXFREE(cname);
    return sce;
}
int PCS_Parse_String_Line(const char *line, char **l, double *EA, double *Ti, double *L0, QString &qatts){ // atts SB a QString.
    //l will be the new-format line up to just before EA including one trailing sep.
    // an old style has word 2 'node' or 'curve'
    // a new style has word 2 '%node' or '%curve' or '%endlist'
    // returns PCS_OLD or PCS_NEW
    // and returns the attstring in char**atts.

    char **words, **wd;
    int k,nw,style;
    size_t n = strlen(line) + 256;
    char *lc = STRDUP(line);

    if(*l) RXFREE(*l);
    *l=(char *)MALLOC(n); **l=0;


    nw = make_into_words(lc,&words,":");
    wd = &(words[2]);
    if(strchr(*wd,'%') )
        style= PCS_NEW;
    else
        style=PCS_OLD;

    if(style==PCS_OLD) {	// all words up to 3 before the end
        strcat(*l,"%");
        for(k= 2; k<nw-3;k++, wd++) {
            if(isOnlyNumber(TOSTRING(*wd) )){
                break;
            }
            strcat(*l,*wd);
            strcat(*l,":");
        }
        *EA=atof(*wd);
        wd++;
        if(*wd && **wd){
            *L0=atof(*wd);
            wd++;
            if(*wd && **wd)
                *Ti=atof(*wd);
        }
        qatts.clear();
    }
    else {  // all words from 2 to '%endlist'
        for(k= 2; k<nw  ;k++, wd++) {
            if(!(*wd)) {
                wd--; break;}
            if(strieq(*wd,"%endlist")) {
                break;
            }
            strcat(*l,*wd);
            strcat(*l,":");
        }
        if(*wd)
            wd++;
        if(*wd && **wd) {
            *EA=atof(*wd);
            wd++;  if(*wd && **wd) {
                *L0=atof(*wd);
                wd++;  if(*wd && **wd) {
                    *Ti=atof(*wd);
                    wd++;
                    if(*wd && **wd) {
                        qatts=QString(*wd);
                    }
                }
            }
        }

    }
    RXFREE(words);
    RXFREE(lc);
    return style;
}

int PCS_Rewrite_String_Line(const char*type, const char*name, char **line, char *l1, char *l2, double EA, double Ti, double L0, const QString &qatts){
    size_t n =0;
    char *buf, buffer[256];
    if(l1)
        n += strlen(l1);
    if(l2)
        n += strlen(l2);
    n += strlen(type);
    n += strlen(name);

        n += qatts.length();

    n +=128;
    buf=(char *)MALLOC(n); *buf=0;

    strcat(buf,type); strcat(buf," : ");
    strcat(buf,name); strcat(buf," : ");
    if(l1)
        strcat(buf,l1);
    if(l2)
        strcat(buf,l2);
    strcat(buf," %endlist : ");
    sprintf(buffer," %f : %f : %f",EA,L0,Ti);
    strcat(buf,buffer);
    if(qatts.length()) {
        strcat(buf," : ");
        strcat(buf,qPrintable(qatts));
    }

    if(*line) RXFREE(*line);
    *line=STRDUP(buf);
    RXFREE(buf);

    return 1;
}
int PCS_Combine_String_Lines(RXEntity_p old,char *line){
    /* Old string card is of type
         0    1          2      3                4      5  6       7
string:name:node:site1:site2(:site3.....):EA:Li (Zi): Ti   ! comment 
string:name:curve:curve1:EA:Li (Zi): Ti   ! comment 

  Current string card (nov 2000) is

string	headstay	%curve	leech	%node	headfwd	ipoint	%endlist	10E4	0	0	noslide
*/
    char *l1 = NULL, *l2=NULL; QString atts;
    double EA,Ti, L0;
    PCS_Parse_String_Line(line,&l2,&EA,&Ti,&L0,atts);
    PCS_Parse_String_Line(old->GetLine(),&l1,&EA,&Ti,&L0,atts);
    PCS_Rewrite_String_Line(old->type(), old->name(),&old->line, l1,l2,EA,Ti,L0,atts);

    if(l1)	RXFREE(l1);
    if(l2) RXFREE(l2);

    return 1;
}
int PCS_Convert_String_Line(char **line,const char *type,const char *name){ //CAREFUL.  Is line long enough??
    char *l2 = NULL, *l=NULL;
    QString atts;
    double EA,Ti, L0;
    if(PCS_NEW !=PCS_Parse_String_Line(*line,&l2,&EA,&Ti,&L0,atts)){
        PCS_Rewrite_String_Line(type, name,&l, NULL,l2,EA,Ti,L0,atts);
        if(strlen(*line) > strlen(l))
            cout<< " found BUG in Convert_Xstring\n"<<endl;
        //strcpy(*line,l);
        RXFREE(*line); *line=STRDUP(l);
    }
    if(l) RXFREE(l);
    if(l2) RXFREE(l2);
    return  1;
}

