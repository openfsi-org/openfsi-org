/* include file with some general structures in for
 * looking after the windows etc. 
 */

#ifndef _HDR_GRAPHICSTRUCT_
#define _HDR_GRAPHICSTRUCT_ 1
#ifdef RXQT
#error(" graphicstruct.h is SSD")
#include "rxsubwindowhelper.h"

#else
#ifndef WIN32
		typedef int             boolean;
    #ifdef _X
		#include <Xm/Xm.h>
    #endif
#endif

	#define FABS(a)         ((a) < 0.0 ? -(a) : (a))
	#define local           static





#ifndef linux // this causes probs. Need to phase out
	#ifndef HC_POINT	
		#define	HC_POINT	Point
	#endif
#endif
#ifdef __cplusplus
	#include "griddefs.h"


// HOOPS 13 widget
#ifdef _X
	#include <X11/IntrinsicP.h> // nov 2008 a guess
	#include "hoopsw.h"
	#define m_ModelSeg m_Phoops_widget->hoops.m_pHBaseModel->GetModelKey()
	#define m_ViewSeg m_Phoops_widget->hoops.m_pHView->GetSceneKey()
	#define m_OverlaySeg m_Phoops_widget->hoops.m_pHView-> GetProgressBarKey()    //GetStaticGeometryKey()
	#define HBV m_Phoops_widget->hoops.m_pHView
	#define HBM m_Phoops_widget->hoops.m_pHBaseModel
#else
        HC_KEY m_ModelSeg;
        HC_KEY m_ViewSeg;
#endif

#endif
struct GRAPHIC_STRUCT { 
#ifdef WIN32
	HWND h;
	#define Widget void * // ifdef windows
#endif
     char *hoops_driver; // synonym for m_ViewSeg
     char *basename; // something like 'sailbase'. Better to use WT_TYPE instead 
    // Widget 

     int ZoomFlag;
#ifndef _X
     char *topLevel;
#else
     Widget topLevel;

     Widget mainWindow;
     Widget workArea ;
     Widget menuBar;
     Widget msgform ;
     Widget msgboxLHS ;
     Widget msgboxRHS ;
#endif
     char filename[200];	/* eg. a sail filename  */     

     int EMPTY;
     HC_KEY m_SelectedGraph;
     RXEntity_p entity_selected;
     HC_KEY highlight;

     HC_KEY axisSeg;		/* key to sibling axis system window */
     HC_KEY keySeg;       
     HC_KEY m_ExportSeg;       	
     
     SAIL *GSail; 
 
/* only for views !!! */
     int defaultShell;
 #ifdef _X
     Widget quantityButton;
     Widget attributeButton;
     Widget saveStateSubmenu;
     Widget NewSailSubmenu;
     Widget dropSailSubmenu;
     Widget New_Menu;
     Widget Open_Menu;
     Widget Hoist_SailMenu;
	Widget options_menu;  

	Widget loadInterpStateSubmenu;
#endif

	Point last_point;
	void  * m_pc_channel;
	void * m_fd;
 

        long GType;   /* eg. WT_VIEWWINDOW Peter changed from Type for DB  */
    /* its used in  Close_All_Options (doview.c)
        It is set in nextGraphic (OpenBuildFile.c)
   */

#ifdef linux
    #ifdef __cplusplus 
		#ifdef _X
			 HT_Widget m_Phoops_widget;
		#endif
     #else
		void *m_Phoops_widget;    
     #endif
#else
		void *m_Phoops_widget;   
#endif

};
typedef struct GRAPHIC_STRUCT  Graphic ;
// the values for GType are:
#define WT_DESIGNWINDOW 1
#define WT_BOATWINDOW	2
#define WT_BUILDFILE	3
#define WT_BAGGEDFILE	4
#define WT_BOATFILE		5
#define WT_VIEWWINDOW	6
#define WT_MATWINDOW	7

#define WT_DESIGN		10
#define WT_STATE		11
#define WT_TPN			12
#define WT_SOCKET_TYPE 16 // so a socket can own a degenerate GS pointing to it.

// values for ZoomFlag

#define EH_ROTATE		1
#define EH_ZOOM_AREA	2
#define EH_PAN			4
#define EH_ZOOM_DYNAMIC	8




EXTERN_C Graphic *selectGraph;

#endif
#endif  //#ifndef _HDR_GRAPHICSTRUCT_

