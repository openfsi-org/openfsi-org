#pragma once
#include "RXPolyline.h"
#include "rxbcurve.h"
#include <string>

class RXSplineI : public  RXBCurve
{
public:
     RXSplineI();
     RXSplineI(class RXSail *p ): RXBCurve(p){ this->BCtype=333; }
//    virtual ~RXSpline();
    virtual int CClear()=0;
    virtual int Dump(FILE *fp) =0;
    virtual int Compute(void)=0;// Compute will generate a polyline of c points along the spline
    virtual int Resolve(void)=0;
    virtual int ReWriteLine(void);
    virtual int Finish(void)=0;

    virtual class RXSplineI * DataPtr() {return this;}

    virtual std::string getText(const int i, const int j) =0;
    virtual double X(const int i)=0;
    virtual double Y(const int i)=0;
    virtual double Z(const int i)=0;
    virtual int SetX(const int i, const RXSTRING &text) =0;
    virtual int SetY(const int i, const RXSTRING &text) =0;
    virtual int SetZ(const int i, const RXSTRING &text) =0;
    virtual int size() =0;
};


class RXSpline : public  RXSplineI
{
public:
    RXSpline();
    RXSpline(class RXSail *p );
    ~RXSpline();
    virtual int CClear();
    virtual int Dump(FILE *fp) ;
    virtual int Compute(void);// Compute will generate a polyline of c points along the spline
    virtual int Resolve(void);
//    virtual int ReWriteLine(void);
    virtual int Finish(void);

    std::string getText(const int i, const int j)  ;
    virtual double X(const int i){ return this->m_pin[i].X();}
    virtual double Y(const int i){ return this->m_pin[i].Y();}
    virtual double Z(const int i){ return this->m_pin[i].Z();}
    virtual int SetX(const int i, const RXSTRING &text) {return  this->m_pin[i].SetX(text);}
    virtual int SetY(const int i, const RXSTRING &text) {return  this->m_pin[i].SetY(text);}
    virtual int SetZ(const int i, const RXSTRING &text) {return  this->m_pin[i].SetZ(text);}
    virtual int size() {return this->m_pin.size();}
private:
    class RXPolyline m_pin;
};
class RXExpSpline : public  RXSplineI
{
public:
    RXExpSpline();
    RXExpSpline(class RXSail *p );
    ~RXExpSpline();
    virtual int CClear();
    virtual int Dump(FILE *fp) ;
    virtual int Compute(void);
    virtual int Resolve(void);
 //   virtual int ReWriteLine(void);
    virtual int Finish(void);
     std::string getText(const int i, const int j) ;
    double X(const int i);
    double Y(const int i);
    double Z(const int i);
    int SetX(const int i, const RXSTRING &text) ;
    int SetY(const int i, const RXSTRING &text) ;
    int SetZ(const int i, const RXSTRING &text) ;
    int size() ;

private:
    int ToFloatArray( float **pp,int *cin );
    int m_npts;

};
