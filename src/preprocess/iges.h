/* header file for igesin function PH 2000*/
#ifndef _IGES_H_
#define _IGES_H_

#include "vectors.h"
#include "nurbs.h"
/* section definitions for file pointer
 position */

#define iges_STDSTR 64
#define iges_MAXSTR 255
#define iges_NGLOBALS 25
#define iges_blksize	8
#define IGS_BUFFERSIZE	256 // sb 1024 at least

#define IGI 1
#define IGS 2
//With the exception of the fields numbered 10, 16, 17, 18, and 20, 
//entries in all fields in this section will be either integer constants
 //or pointer constants.
/*

|1||     8 9|||   16 17|||  24 |25||   32|33||   40 41|||  48 |49||   56|57||   64 65|||  72 |73||   80|||
|__________|_________|_________|_________|__________|_________|_________|__________|_________|_________|_
|          |         |         |         |          |         |         |          |         |         |
|   (1)    |  (2)    |   (3)   |   (4)   |   (5)    |  (6)    |   (7)   |   (8)    |  (9)    |  (10)   |
| Entity   | Para-   |Structure|  Line   |  Level   | View    |Transfor-|  Label   | Status  |Sequence |
|  Type    | meter   |         |         |          |         |         | Display  |         |         |
|          |         |         |  Font   |          |         | mation  |          |Number   |Number   |
|Number    | Data    |         |Pattern  |          |         | Matrix  | Assoc.   |         |         |
|   #      |   )     | #; )    |  #; )   |  #; )    |  0; )   |  0; )   |  0; )    |   #     |  D #    |
|          |         |         |         |          |         |         |          |         |         |
|__________|_________|_________|_________|__________|_________|_________|__________|_________|_________|_
|          |         |         |         |          |         |         |          |         |         |
|  (11)    |  (12)   |  (13)   |  (14)   |   (15)   |  (16)   |  (17)   |   (18)   |  (19)   |  (20)   |
| Entity   |         |         |         |          |         |         | Entity   | Entity  |Sequence |
|          | Line    | Color   |  Para-  |  Form    |Reserved |Reserved |          |         |         |
|  Type    |Weight   |Number   | meter   |Number    |         |         |  Label   |Subscript|Number   |
|Number    |Number   |         |  Line   |          |         |         |          |Number   |         |
|          |         |         | Count   |          |         |         |          |         |         |
|          |         |         |         |          |         |         |          |         |         |
|   #      |   #     | #; )    |   #     |    #     |         |         |          |   #     |D # + 1  |
|__________|_________|_________|_________|__________|_________|_________|__________|_________|_________|_
  */
// definitions for the Directory Section

#define IGS_ETYPE	0
#define IGS_PDATA	1
#define IGS_STRUC	2
#define IGS_LFONT	3
#define IGS_LEVEL	4
#define IGS_VIEW	5
#define IGS_T_MAT	6
#define IGS_LDISP	7
#define IGS_STATS	8	
// #define IGS_SEQNO	9
#define IGS_ETYPE2	10-1
#define IGS_LWGHT	11-1
#define IGS_COLOR	12-1
#define IGS_PCONT	13-1
#define IGS_FORMN	14-1
#define IGS_RESV1	15-1
#define IGS_RESV2	16-1
#define IGS_ELABL	17-1	//ie name
#define IGS_SUBSC	18-1
#define IGS_SEQNO2	19-1
// so eg ie->Dwords[IGS_FORMN] is the form no and we interpret it by IGESTYPE

#define IGS_INT		1
#define IGS_REAL	2
#define IGS_STRING	4
#define IGS_DATE IGS_STRING	
#define IGS_POINTER	8
#define IGS_LANG	16
#define IGS_LOGICAL	32 

 
//definitions for the Global Section
// Unlike the Standard, we start at 0 not 1
#define	IGS_PDLM	0	//	Parameter delimiter character (default is comma)
#define	IGS_RDLM	1	//	Record delimiter character (default is semicolon)
#define	IGS_PID_S	2	//	Product identification from sending system
#define	IGS_FNAME	3	//	File name
#define	IGS_SID 	4	//	System ID
#define	IGS_PPV 	5	//	Preprocessor version
#define	IGS_NBBI	6	//	Number of binary bits for integer representation
#define	IGS_MPTF	7	//	Maximum power of ten representable in a single precision floating point number on the sending system
#define	IGS_NSFF	8	//	Number of significant digits in a single precision floating point number on the sending system
#define	IGS_NPTD	9	//	Maximum power of ten representable in a double precision floating point number on the sending system
#define	IGS_NSFD	10	//	Number of significant digits in a double precision floating point number on the sending system
#define	IGS_PID_R	11	//	Product identification for the receiving system
#define	IGS_SCALE	12	//	Model space scale (example:  .125 indicates a ratio of 1 unit model space to 8 units real world)
#define	IGS_UFLAG	13	//	Unit flag
#define	IGS_UTXT	14	//	Units.
#define	IGS_NLWT	15	//	Maximum number of line weight gradations (1-32768).  Refer to the Directory Entry Parameter 12 (see Section 2.2.4.3.12) for use of this parameter. (Default = 1)   
#define	IGS_LWGT	16	//	Width of maximum line weight in units. Refer to the Directory Entry Parameter 12 (see Section 2.2.4.3.12) for use of this parameter.
#define	IGS_DGEN	17	//	"Date & time of exchange file generation  13HYYMMDD.HHNNSS  where:  
						//YY is last 2 digits of year  HH is hour (00-23)
						//MM is month (01-12)          NN is minute (00-59)
						//DD is day (01-31)            SS is second (00-59)    

#define	IGS_TOL 	18	//	Minimum user-intended resolution or granularity of the model expressed in units defined by Parameter 15 (example .0001)
#define	IGS_XMAX	19	//	Approximate maximum coordinate value occurring in the model expressed in units defined by Parameter 15.  (Example:  1000.0 means for all coordinates |X|; |Y |; |Z|  1000:)
#define	IGS_AUTH	20	//	Name of author
#define	IGS_ORG 	21	//	Author's organization
#define	IGS_VERS	22	//	Integer value corresponding to the version of the Specification used to create this file.
#define	IGS_DST 	23	//	Drafting standard in compliance to which the data encoded in this file was generated.
#define	IGS_DCRE	24	//	Date and time the model was created or last modified, whichever occurred last, 13HYYMMDD.HHNNSS (see index 18)

// return flags for iges_get_next_char
#define IGS_OK 0
#define IGS_EOF 101
#define IGS_EOR 102
#define IGS_NAME_SEPARATOR  "()/$[]{},:;"

 struct PC_iges{
	 FILE *fp;
  	 char *filename;
	 char *segname;
	 char lastline[96];
	 char prm_dlmtr[4];
	 char rcrd_dlmtr[4];
	 char *globals[iges_NGLOBALS]; // [96];
	 int n_globals;
	 struct IGES_ENTITY *elist;
	 int ne, nea;
	 VECTOR origin;
	 int Use_Origin;
	 char *buffer;
	 char *lp;	//lp is  pointer into lastline . These three are used in iges_get_next_word
	 char *lpend; 
	 int limit,JustFoundAnEOR;
	 RXEntity_p owner;
};
struct IGES_ENTITY {
	char Dline[164];
	int flags; 
	RXEntity_p e;
	char Dwords[20][9];
	int DData[20];
	int *asses, na;
	int *props, np;
	char **Pword;  int nPw;// this is for the words of a 406 entity
	char *name, *layer;
};

int  Resolve_iges_Card( RXEntity_p e);
HC_KEY igesin(const char *filename, struct PC_iges*p,SAIL *sail);

EXTERN_C int iges_start_section( struct PC_iges*p);
EXTERN_C int iges_directory_section( struct PC_iges*p);
EXTERN_C int iges_parameter_section(struct PC_iges*p);
EXTERN_C int iges_global_section(struct PC_iges*p);
EXTERN_C int iges_terminate_section( struct PC_iges*p);
EXTERN_C int iges_print(FILE *fp, struct PC_iges *p);
EXTERN_C int iges_create_entity(char *line,struct PC_iges *p);
EXTERN_C int iges_find_parameter_line(struct IGES_ENTITY *ie,struct PC_iges *p);
EXTERN_C int iges_read_116(struct IGES_ENTITY *ie, struct PC_iges*p);
EXTERN_C int iges_read_128(struct IGES_ENTITY *ie, struct PC_iges*p);
EXTERN_C int iges_read_126(struct IGES_ENTITY *ie, struct PC_iges*p);
EXTERN_C int iges_read_406(struct IGES_ENTITY *ie, struct PC_iges*p);
EXTERN_C int iges_read_N(struct IGES_ENTITY *ie, struct PC_iges*p);
EXTERN_C char *iges_get_next_parameter(struct IGES_ENTITY *ie, struct PC_iges*p ,int*linecount);
EXTERN_C int iges_write_line(FILE *fp,const char *buf,const char w,int iret);
EXTERN_C int iges_write_multiline(FILE *fp,const std::string &text,const char w,int iret);
EXTERN_C int iges_write_header(FILE *fp,const char *fname);
EXTERN_C int iges_write_global(FILE *fp,const char *fname);
EXTERN_C int iges_write_Hollerith(char *buf,const char *s);
EXTERN_C int iges_write_128(FILE *fp,FILE *tfp, const char *name, struct PC_NURB *n,int debackptr, int iret);
EXTERN_C int iges_out_128 (const char*fname,const char *mname, struct PC_NURB *n);
EXTERN_C int iges_free( struct PC_iges *p);
EXTERN_C int iges_read_properties(struct PC_iges *p,struct IGES_ENTITY *ie,int *lc);
EXTERN_C int iges_apply_properties(struct PC_iges *p,struct IGES_ENTITY *ie );
EXTERN_C char *iges_Create_Attributes(struct IGES_ENTITY *ent);
EXTERN_C char *iges_append_line(struct PC_iges *p, char sect,int *lc);
EXTERN_C char *iges_get_next_word(struct PC_iges*p,int*lc,char sect,int type,int*No_More);
EXTERN_C char *iges_fgets(char*string, int nmax, FILE *fp,char*seps);
EXTERN_C int iges_is_hollerith(char*lp) ;
EXTERN_C char *iges_read_hollerith(char *lp);
EXTERN_C char iges_read_one_char(struct PC_iges *p,int *linecount, const int limit,int*e);
#endif  //#ifndef _IGES_H_





