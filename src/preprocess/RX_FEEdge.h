#ifndef _RXFORTRANEDGE_HDR_
#define   _RXFORTRANEDGE_HDR_
#include "RXGraphicCalls.h"
#include "RX_FEObject.h"
#define RXFEE_NOSHELL  0
#define RXFEE_ISEDGE 2      // means that this edge is a Triangle Element Edge.
#define RXFEE_ISMODELEDGE 4
class RXSail;
class RX_FESite;
// the allocation block size 
//#define EDGEBLOCK  64

class RX_FEedge: 
        public RX_FEObject

{
    friend class RXSail;
public:
    RX_FEedge(void);// deprecated
#ifdef NEVER
    RX_FEedge(SAIL *p_sail,const int p_N,
              Site *n1, Site *n2,
              double p_L);
#else
    RX_FEedge(class RXSail *p);
#endif
    ~RX_FEedge(void);
    void Init(SAIL *s=0);

protected:
    RX_FESite *m_node[2];
    double m_Linit;
    double m_initialCorrection;

public:
    int m_Flags;
    RXGRAPHICOBJECT DrawDeflected(HC_KEY p_k);
    bool IsOnModelEdge();
    bool IsOnShell();
    int SetLength(const double p_zi);

    double GetLength(void)const;
    RX_FESite * GetNode(const int k)const;
    void SetNodes(RX_FESite *s1,RX_FESite *s2 );
    int EdgePrint(FILE *ff)const;
    //int AddFEdge(SAIL *s);
    //int ClearFEdge(SAIL *s);
    RXSTRING TellMeAbout(void) const;
};
#endif
