//curveintersection.h header for curveintersection.cpp
//Thomas 19 07 04

#ifndef RXCURVEINTERSECTIONS_H
#define RXCURVEINTERSECTIONS_H

#include "RXCrossing2.h"

//!Finds all the intersections between the 2 curves which verify p_Tolerance and p_CutDistance
/*!
	and intersection is found if the distance beetween the 2 curves is < p_Tolerence
	if we are around the end the tolerance is p_CutDistance
	if (p_MergeToEnds) we are performing a test and we set the intersection point to the end of the curves if 
	they are close enought the the ends 
*/
int FindCurveIntersections(const ON_Curve * p_Crv1,
						   const ON_Curve * p_Crv2,
					 	   ON_ClassArray<RXCrossing2> & p_Intersections, 
						   const double     & p_Tolerence,
						   const double     & p_CutDistance,
						   const int        & p_MergeToEnds,
							const RXTRObject *  p_pPersistentCrv1=NULL,//persitant curve from which the p_Crv1 is extracted 
							const RXTRObject *  p_pPersistentCrv2=NULL);//persitant curve from which the p_Crv1 is extracted 

//!Finds all the intersections between the 2 curves which verify p_Tolerance and p_CutDistance
/*!
	and intersection is found if the distance beetween the 2 curves is < p_Tolerence
	if we are around the end the tolerance is p_CutDistance
	by default p_MergeToEnds is set to TRUE
	p_Intersections[i].m_Point1 is a point on the curve p_Crv1
	p_Intersections[i].m_Point2 is a point on the curve p_Crv2
*/
int FindCurveIntersections(const ON_Curve * p_Crv1,
						  const ON_Curve * p_Crv2,
						  ON_ClassArray<RXCrossing2> & p_Intersections, 
						  const double     & p_Tolerence,
						  const double     & p_CutDistance,
							const RXTRObject *  p_pPersistentCrv1=NULL,//persitant curve from which the p_Crv1 is extracted 
							const RXTRObject *  p_pPersistentCrv2=NULL);//persitant curve from which the p_Crv1 is extracted 

/* First written for use in filament intersections
  We recursively subdivide the curve until the sub-BBs are smaller than p_b;
  if any of these small BBs intersect with the input BB return true.
*/

int CrvBBInter(const ON_Curve * p_Crv1,
			   const rxON_BoundingBox &p_b,
			   ON_Interval &foundInterval
			   );


//!Find ALL the intersections of Two curves with a bounding box method 
/*!
	Start with a bounding box mehtod to find area of all the intersection and \n
	then uses a projection method to accuratly set the intersections  \n
	p_Crv1 and p_Crv1 are the refences of the complet curves  \n
	p_crv1 p_crv2 are pointer on parts of the curves (needed because of recursive call)  
	p_Intersections store the parameter of the intersection poin t on the 2 curves  \n
	p_EpsMinD criteria to set the minimum distance beetween 2 intersections
	p_EpsBB Criteria for stopping the Bounding search,  \n
	p_EpsDot crit�ria for stopping the projected method \n
*/
int CrvCrvInter(const ON_Curve * p_Crv1,
				const ON_Curve * p_Crv2,
				const ON_Curve * p_crv1,
				const ON_Curve * p_crv2, 
				ON_ClassArray<RXCrossing2> & p_Intersections, 
				const double & p_EpsMinD  = 0.1, 
				const double & p_EpsBB  = 0.01,
				const double & p_EpsDot = 1.0e-5,
			    const RXTRObject *  p_pPersistentCrv1=NULL,//persitant curve from which the p_Crv1 is extracted 
			    const RXTRObject *  p_pPersistentCrv2=NULL);//persitant curve from which the p_Crv1 is extracted 

int CrvCrvInter(const ON_Curve * p_Crv1,
				const ON_Curve * p_Crv2,
				const ON_NurbsCurve * p_crv1,
				const ON_NurbsCurve * p_crv2, 
				ON_ClassArray<RXCrossing2> & p_Intersections, 
				const double & p_EpsMinD  = 0.1, 
				const double & p_EpsBB  = 0.01,
				const double & p_EpsDot = 1.0e-5,
			    const RXTRObject *  p_pPersistentCrv1=NULL,//persitant curve from which the p_Crv1 is extracted 
			    const RXTRObject *  p_pPersistentCrv2=NULL);//persitant curve from which the p_Crv1 is extracted 



//! False if the 2 bounding boxes are not near to each other
/*!
	returns 1 if their an intersection beetween p_Bb1 and p_Bb2 \n
	returns 2 if the distance is smaller than p_EpsBB \n
	Otherwise return 0\n
*/
int IsNear(const rxON_BoundingBox & p_Bb1,
		   const rxON_BoundingBox & p_Bb2,
		   const double         & p_EpsBB);


//!Return 1 if an item of p_Intersections is closer than p_Epsilon from p_point else 0
/*!
	p_EpsMinD criteria to set the minimum distance beetween 2 intersections
	p_EpsBB Criteria for stopping the Bounding search,  \n
*/
int AlreadyFound(ON_ClassArray<RXCrossing2> & p_Intersections, 
				  const RXCrossing2 & p_point,
				  const double p_EpsMinD, 
				  const double & p_EpsBB);

//!Find a point on each curve such like the line beetween these 2 points is perpendicular to both curves
/*!
	p_pcrv1 and p_pcrv2 are the 2 curve2 where we are looking for an intersection. 
	p_t1 and p_t2 starting points on the curves 1 and 2
	if p_t1Fixed ==1 t1 cannot move along crv1
	if p_t0Fixed ==1 t2 cannot move along crv2
	p_EpsDot crit�ria for stopping the projected method default (1.0e-5) \n
	returns 1 is success, 0 is not 
*/
int CrvCrvPerpendicular(const ON_Curve * p_crv1,
						const ON_Curve * p_crv2,
						RXCrossing2 & p_Inter,
						const int & p_t1Fixed,
						const int & p_t2Fixed,
						const double & p_EpsDot = 1.0e-5,
						const RXTRObject *  p_pPersistentCrv1=NULL,//persitant curve from which the p_Crv1 is extracted 
						const RXTRObject *  p_pPersistentCrv2=NULL);//persitant curve from which the p_Crv1 is extracted 

int CrvCrvInter(const rxON_PolylineCurve * p_Crv1,
				const rxON_PolylineCurve * p_Crv2,
				ON_ClassArray<RXCrossing2> & p_Intersections, 
				const double & p_EpsMinD, 
				const double & p_EpsBB,
				const double & p_EpsDot,
			    const RXTRObject *  p_pPersistentCrv1=NULL,//persitant curve from which the p_Crv1 is extracted 
			    const RXTRObject *  p_pPersistentCrv2=NULL);//persitant curve from which the p_Crv1 is extracted 

int CrvCrvInter(const rxON_LineCurve * p_Crv1,
				const rxON_LineCurve * p_Crv2,
				RXCrossing2 & p_Intersection, 
				const double & p_EpsMinD, 
				const double & p_EpsBB,
				const double & p_EpsDot,
			    const RXTRObject *  p_pPersistentCrv1=NULL,//persitant curve from which the p_Crv1 is extracted 
			    const RXTRObject *  p_pPersistentCrv2=NULL);//persitant curve from which the p_Crv1 is extracted 

//!Sort the crossing point by parameter value on the curve defined by p_ID
/*!
	if p_ID == 0 sort the crossing point along the curve 1
	if p_ID == 1 sort the crossing point along the curve 2
*/
int SortCrossingOnCurve(ON_ClassArray<RXCrossing2> & p_Intersections, 
						const int & p_ID);
#endif  //#ifndef RXCURVEINTERSECTIONS_H
