#ifndef _RESOLVE_H_
#define  _RESOLVE_H_

#include "reportform.h"  

#define PCTOL_LINEAR	1	// for use in setting and retrieving the 3 tolerances.
#define PCTOL_ANGULAR	2
#define PCTOL_CUT	4
#define PCTOL_GLOBAL	8
#define PCTOL_LOCAL	16

int Report_Init(SAIL*sail,struct REPORT_FORM*r);
int Report_Sort_Fn(struct REPORT_ITEM *a,struct REPORT_ITEM *b);
int Report_Clear(struct REPORT_FORM*r); 
char *Report_To_String(struct REPORT_FORM*r,int c =-1 ) ;

EXTERN_C int Resolve_Fix_Card(RXEntity_p e); 

int Resolve_Batten_Card (RXEntity_p e);

int Resolve_Fabric_Card (RXEntity_p e) ;
int Resolve_Mould_Card (RXEntity_p e);

int Resolve_Pansail_Card(RXEntity_p e) ;
int Resolve_Patch_Card (RXEntity_p e);
int Resolve_Rename_Card(RXEntity_p e);
int Resolve_Set_Card (RXEntity_p e);
int Resolve_Summary_Card(RXEntity_p e);

int PCTol_Update_Tolerances(SAIL *sail) ;  // from UOs to globals
EXTERN_C int PCTol_Set_Tolerances(SAIL *sail, double v,int type) ; // from double to UOs and globals

int DBG_Create_Wanted_Entities(SAIL *sail,struct REPORT_FORM*r);

EXTERN_C int Parse_BP_Line(SAIL*const sail,const char*line,char*type,char*name,char*nname,char*n1,char*n2,char*mat,float*thickness,char*basecurve,float*depth,char*att) ;

#endif  // #ifndef _RESOLVE_H_


