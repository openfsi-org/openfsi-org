// RXCrossing2.h: interface for the RXCrossing2 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RXCROSSING2_H__34B6764A_ABBD_4907_B5C3_4BDEF35DCD64__INCLUDED_)
#define AFX_RXCROSSING2_H__34B6764A_ABBD_4907_B5C3_4BDEF35DCD64__INCLUDED_



#include "RXUPoint.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


//! to define an intersection between 2 RXUPoints to same RXCrossing
// should remain in the furture
//! Thomas 29 10 04
class RXCrossing2  
{
public:
	double GetCrvlength(const int &p_ID,double fractional_tolerance = 1.0e-8) const;
	ON_3dPoint EvalPt(const int & p_ID) const;
	double Gett(const int &p_ID) const;
	//!Constructor
	RXCrossing2();
	//!Constructor by copy
	RXCrossing2(const RXCrossing2 & p_Obj);

	//!Destructor
	virtual ~RXCrossing2();
	void Init();
	int Clear();

	RXCrossing2& operator = (const RXCrossing2 & p_Obj);

	int SetPoint1(const RXUPoint & p_pt);
	int SetPoint2(const RXUPoint & p_pt);

	RXUPoint * GetPoint1();
	RXUPoint * GetPoint2();
	RXUPoint * GetPoint(const int & p_ID);

	int SetJ1(const int j);
	int SetJ2(const int j);
//	int GetJ1(void) const;
//	int GetJ2(void) const;

	//Returns the distance between the 2 points
	double Distance() const;
	int Print(FILE*fp) const;
protected:
	//!point on the first curve
	RXUPoint m_Point1;
	//!point on the second curve
	RXUPoint m_Point2;
	int m_j1, m_j2;  // indices that may be useful 
};

#endif // !defined(AFX_RXCROSSING2_H__34B6764A_ABBD_4907_B5C3_4BDEF35DCD64__INCLUDED_)
