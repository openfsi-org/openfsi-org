/* header file for :
	rdpfile.h

 	written by A K Molyneaux
		28.3.95
 */

#ifndef _HDR_RDPFILE_
#define _HDR_RDPFILE_ 1
#error(" do we need rdpfile.h")
EXTERN_C  int next_number(char **data,double *x);
EXTERN_C int next_3_numbers(char **data,double *u, double *v, double *d);
EXTERN_C int Do_Read_Triplet_File(const char *fname,SAIL *s,RXEntity_p aeroEnt,RXEntity_p panEnt,RXSitePt ***slist,int *pn,int guess_at_v);
EXTERN_C int Do_Read_Nlet_File(const char *fname,RXSitePt ***slist,int *pn,int N);
EXTERN_C int read_nlet_from_text(char *lp,RXSitePt ***slist,int *pn,int N);

#endif  //#ifndef _HDR_RDPFILE_
