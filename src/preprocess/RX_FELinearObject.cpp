#include "StdAfx.h"
#include "RXSail.h"
#include "RXEntityDefault.h"
#include "RX_FESite.h"
#include "RXPside.h"

#include "RX_FELinearObject.h"


RX_FELinearObject::RX_FELinearObject(void)
{
}

RX_FELinearObject::~RX_FELinearObject(void)
{
    this->CClear();
}
int RX_FELinearObject::CClear() {
    int rc=0;
    this->m_elist.clear();


    rc+=RX_FEObject::CClear();
    return rc;
}
int RX_FELinearObject::ClearElist(){

    this->m_elist.clear();
    return 1;
}
int RX_FELinearObject::CountPsideEdges(double*p_length, PSIDEPTR *pslist,int npss) 
{  // the elist isnt necessarily empty.
    FEEdgeRef er;
    Pside *ps;
    int c=0;
    for(int j=0;j<npss;j++) {
        ps= pslist[j];
        for(int k=0; k<ps->m_eList.size();k++) {
            (*p_length) += GetSail()->Get_Init_Edge_Length((ps->m_eList[k])->GetN());
            er.e=ps->m_eList[k]; er.rev=0;
            m_elist.push_back( er ); c++;
        }
    }
    return c ;
}// RX_FEPocket::CountPsideEdges


// now sort the pside edges so they chain together. 
// there are separate sort eoutines for whether or not there is a seed site

int RX_FELinearObject::SortPsideEdgesWithSeed(RXEntity_p seedent) 
{
    // the list of edges is not necessarily contiguous, nor are they all pointed the right way.
    // this fn builds a list of edges, starting with seedent. It ASSUMES that seedent
    // is at one end of the list.

    vector<FEEdgeRef>::iterator i;
    vector<FEEdgeRef> newlist;
    FEEdgeRef r;
    int nhi;
    assert(seedent) ;
    RX_FESite *seedsite = dynamic_cast<RX_FESite *> (seedent);


    bool found = false;
    nhi = seedsite->GetN();

    newlist.clear();

    // search for a member of elist with first node == nhi and not rev OR (last node == nhi  && rev)
    //	If we find one, remove it from elist and push-back it onto newlist. Set the rev flag and update nhi.
    //		and try again while we found one
    do{
        found = false;
        for(i=m_elist.begin();i!= m_elist.end(); i++) {
            r = *i;
            if(r.e->GetNode(0) ->GetN()==nhi && !r.rev){
                found = true;
                newlist.push_back(r);
                m_elist.erase(i);
                nhi = r.e->GetNode(1)->GetN();
                break;
            }
            else if(r.e->GetNode(1)->GetN()==nhi && r.rev){
                found = true; r.rev=true;
                newlist.push_back(r);
                m_elist.erase(i);
                nhi = r.e->GetNode(0)->GetN();
                break;
            }
        }
    }while(found);
    assert(!m_elist.size());
    m_elist = newlist;

    return m_elist.size();


}//RX_FEPocket::SortPsideEdgesWithSeed


int RX_FELinearObject::SortPsideEdges(void) 
{
    // the list of edges is not necessarily contiguous, nor are they all pointed the right way.
    // this method is ony good when we dont have a seed for the start end.

    vector<FEEdgeRef>::iterator i,pivot;
    vector<FEEdgeRef> newlist;
    FEEdgeRef piv,r;
    int nhi, nlo;

    bool found = false;
    // cout<< "SortPsideEdges initial list"<<endl; printelist(m_elist);
    if(! m_elist.size()) return 0;
    pivot = m_elist.begin();
    piv = *pivot; //cout<< "pivot"<<endl; printr(piv);
    newlist.push_back(piv);
    m_elist.erase(pivot  );

    if(piv.rev) { nhi = piv.e->GetNode(0)->GetN(); nlo = piv.e->GetNode(1)->GetN(); }
    else		{ nhi = piv.e->GetNode(1)->GetN(); nlo = piv.e->GetNode(0)->GetN(); }

    // search for a member of elist with first node == nhi OR last node == nhi
    //	If we find one, remove it from elist,Set its rev flag, push-back it onto newlist and update nhi.
    //  In the same search we look for one we can tack onto the start.
    // search for a member of elist with last node == nlo OR (irst node == nlo
    //	If we find one, remove it from elist, set its rev flag, insert at the beginning of newlist and update nlo.
    //		and try again while we found one

    do{
        found = false; 	//printf(" nhi = %d, nlo= %d\n",nhi,nlo);
        for(i=m_elist.begin();i!= m_elist.end(); i++) {
            r = *i;
            if(r.e->GetNode(0) ->GetN()==nhi ){
                found = true; r.rev= false;
                newlist.push_back(r);
                m_elist.erase(i);
                nhi = r.e->GetNode(1)->GetN();
                break;
            }
            else if(r.e->GetNode(1)->GetN()==nhi){
                found = true; r.rev=true;
                newlist.push_back(r);
                m_elist.erase(i);
                nhi = r.e->GetNode(0)->GetN();
                break;
            }
            if(r.e->GetNode(1)->GetN()==nlo){
                found = true;
                r.rev=false;
                newlist.insert(newlist.begin(),r);
                m_elist.erase(i);
                nlo = r.e->GetNode(0)->GetN();
                break;
            }
            else if(r.e->GetNode(0)->GetN()==nlo){
                found = true; r.rev=true;
                newlist.insert(newlist.begin(),r);
                m_elist.erase(i);
                nlo = r.e->GetNode(1)->GetN();
                break;
            }
        }
    }while(found);
    m_elist = newlist;
    // printf(" after  copynew list has size  %d\n", m_elist.size());

    return m_elist.size();
}



int RX_FELinearObject::printr (FEEdgeRef &r) {

    return printf(" %d  %d from %p  %d to %p  %d \n", r.e->GetN(),r.rev, r.e->GetNode(0), r.e->GetNode(0)->GetN(),
                  r.e->GetNode(1), r.e->GetNode(1)->GetN());
}
int RX_FELinearObject::printelist(vector<FEEdgeRef> &p_elist) {

    vector<FEEdgeRef>::iterator i; int c=0;
    FEEdgeRef r;
    printf(" size = %d, capacity= %d empty = %d\n", (int)p_elist.size(), (int)p_elist.capacity (), p_elist.empty());
    for(i=p_elist.begin();i!= p_elist.end(); i++) {
        r = *i;
        printf("  %d   ",c++); printr(r);
    }
    return c;
}

std::vector<int> RX_FELinearObject::GetNodeList()
{
    std::vector<int>rv;
    vector<FEEdgeRef>::iterator i;
    FEEdgeRef r;
    bool starting = true;
    for(i=m_elist.begin();i!= m_elist.end(); i++) {
        r = *i;
        if(starting) {
            if(r.rev)
                rv.push_back( r.e->GetNode(1)->GetN() );
            else
                rv.push_back( r.e->GetNode(0)->GetN() );
            starting=false;
        }
        if(r.rev)
            rv.push_back( r.e->GetNode(0)->GetN() );
        else
            rv.push_back( r.e->GetNode(1)->GetN() );
    }
    return rv;
}
