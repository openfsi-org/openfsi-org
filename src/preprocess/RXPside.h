#ifndef _RXPSIDE_HDR__
#define  _RXPSIDE_HDR__

#include "RXEntity.h"
#include "griddefs.h"
class RXOffset;
#define PS_LAYER_RELATION notype

class RXPside :
        public RXEntity
{
public:
    RXPside(void);
    RXPside(SAIL *sail);
    ~RXPside(void);
    int CClear();
    int Init(void);
    int Compute(void);
    int Resolve(void);
    int Finish(void);
    int ReWriteLine(void);
public:
    Site *Ep(const int k)const { return m_ep[k];}
    void SetEp(const int k, Site*s) { m_ep[k]=s ;}
    int DrawDeflected(HC_KEY p_k);
    vector<RXSitePt> psGetCurveCoords(const int whin,const bool ReverseOrder) const;
    vector<int > GetEdgeNumbers(const bool ReverseOrder) const;
    int Mesh(void);
    int ReMesh();
    int Add_To_EndNode_Lists(  ) const;

    int Dump(FILE *fp ) const;
    void SetpsFlag(const int i) { m_psFlag=i;}
    int GetpsFlag(void) const {return  m_psFlag;}
    int IsSpecial ( RXEntity_p EndSCO[2] ) const ;
    int IsDisordered(void) const;
    int Find_Special_Side(  RXEntity_p sco2,RXEntity_p se) const;

private:
    int m_psFlag;

protected:
    int UpdateEdges( ); // ONLY call from ReMesh
    int MakeEdges( ); // ONLY call from MeshOnePside
    vector<double> GetOffsets() const; // work in REDSPACE

    int ClearSlist(SAIL *sail );
    int ClearElist(SAIL *sail);

    int Remove_From_Node_Lists();
    int Get_Position(RXOffset *const offin,int side,ON_3dPoint &v)const;

public:

    std::vector< std::pair<class RX_FESite*,double> > m_sList;
    std::vector<class RX_FEedge*> m_eList;
    double LengthUnstressed() const;

    Panelptr leftpanel[2];
    Panelptr  leftZone[2];	/* a ZONE is a closed list of psides,
          like a panel. It is used by
          Compute_Zone and by Get_SC_Endwidths */

    // these are  early candidates for elimination via enode
private:
    Site* m_ep[2];  /* SITES or a RELSITE */
public:
    PSIDEPTR  el[2];
    PSIDEPTR  er[2];  /* a PSIDE  */

    sc_ptr sc;        /* the owning seamcurve */

    class RXOffset *End1off;
    class RXOffset *End2off;

    RXEntity_p  Defined_Material[3];	 /* if user-defined */
    struct SIDE_ANGLE Mat_Angle[3];

    int Backwards; /* set by Compute_Compund curve, used in CheckConnects */
} ;  //class  RXPside


#endif

