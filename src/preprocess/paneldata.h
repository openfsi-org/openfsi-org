#pragma once
#include "griddefs.h"

int PCE_Reverse_List(RXEntity_p *l, int c);
int PCE_Cyclic_Permutation(RXEntity_p *l1,int c1,RXEntity_p *l2,int c2);
int Resolve_RegionData_Card(RXEntity_p e);

int Create_PanelData_Entity(Panelptr pan, RXEntity_p mat, RXEntity_p ref);

