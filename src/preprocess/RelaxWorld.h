// RelaxWorld.h: interface for the RelaxWorld class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RELAXWORLD_H__8CD9BEFF_7920_4842_8FF5_9EA8EF26E075__INCLUDED_)
#define AFX_RELAXWORLD_H__8CD9BEFF_7920_4842_8FF5_9EA8EF26E075__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include <QString>
#include <set>
#include <vector>
#include <queue>
#ifdef HOOPS
#include "HBaseModel.h"
#endif
#include <QMutex>
#include "UtfConverter.h"
#include "RXENode.h"
#include "rxwindgeometry.h"
#define MIRRORPTR  class RXMirrorDBI*
//peter
//  windows messages

//peters
#if defined(MICROSOFT) && !defined(_CONSOLE)
#define WM_RXMESSAGE WM_USER+0x123

//With with wParam='my hwnd' and LParam 99  this is our 'hello' message
const UINT    wm_RXMessage =
        RegisterWindowMessage(_T("CC667211-7CE9-40c5-809A-1DA48E4014C4"));
const UINT     RXWIN_IHEARYOU =
        RegisterWindowMessage(_T("B9A8BE59-D7C3-499f-8E2C-6F959FDB5432"));
const UINT      RXWIN_IHAVERUN=
        RegisterWindowMessage(_T("D7F30103-223C-438a-A981-07659B8CB8FA"));
const UINT      RXWIN_PLEASERUN=
        RegisterWindowMessage(_T("96ABA4BA-AB16-48e4-BE08-41801E246B6F"));
const UINT      RXWIN_PLEASEREFRESH=
        RegisterWindowMessage(_T("FE2E85A9-5132-478a-AA2B-0EBF1569A792"));


#define RXWIN_COUCOU		99

#endif


#define RX_TOPOLOGYCHANGE 0 
#define RX_VALUECHANGE 1 

#ifdef RXEDEV
#define  control_data_t void
#else
#ifndef _CONSOLE
//#include "controlmenu.h"//  but its bad style
#include "global_declarations.h"
#endif
#endif

#ifndef _CONSOLE
#ifndef linux
#include "stringtools.h" // defines FromUtf8 and ToUtf8
#else
#include "stringtools.h"
#include <pthread.h>
#endif
#endif

// forward declarations
class RXSail;
class RXDataBaseLogI;
class RXMirrorDBI;

class RelaxWorld : public  RXENode
{
public:

    RelaxWorld();
    virtual ~RelaxWorld();
#ifdef HOOPS 	
    HBaseModel* GetHBaseModel();
    HDB* GetHDB() {return m_HDB;};
#endif
    std::vector<RXSail*> ListModels(const int p_flags); // only accepts flag non-zero meaning 'ishoisted;
    int PrintModelList(FILE *fp);
    class RXSail *FindModel(const char *name, const int p_hoisted=1);
    class RXSail *FindModel(const std::string  &name, const int p_hoisted=1);
    class RXSail *FindModel(const RXSTRING &name, const int p_hoisted=1) {return FindModel(UtfConverter::ToUtf8(name),p_hoisted);}
    class RXSail *FindModel(const QString &name, const int p_hoisted=1) {return FindModel(qPrintable(name),p_hoisted);}

    int AddSail(class RXSail *p_s);
    int RemoveSail(class RXSail *p_s);

    int SetHeelAngle(const double g_a_degrees);
    double HeelAngle() const { return m_HeelAngle;}
    int MakeLastKnownGood();
    int ListAllExpressions(FILE *fp);
    int ExportAllExpressions(wostream &s);
    int ExportAllDependencies(wostream &s, const int what);
    int ExportDependencies(wostream &s,const wstring &head, const int recur, const int what ) const ;

    int PackLoadingIndices();  // renumber the loading indices, starting from 1. called on a Drop.
    int SetPressuresOnActive(struct control_data_s *data);
    int CalcNow(void){  if(m_CalcIsRunning) return 0;m_CalcIsRunning=1; int rc=__CalcNow();   m_CalcIsRunning=0;  return rc;}
    void SetAutoCalc(const int i=1){ m_AutoCalc=i;}
    int IsAutoCalc(){return this->m_AutoCalc;}
    int AbsoluteFileName( QString &qfname );
    int  run_Werner();
    int GetGlobalValues(const int w) ;
    int qGetOneSettings(QStringList &ll, const int first_time);

 private:
    int GetOneSettings(const std::vector<std::string> wds, const int first_time);
    int __CalcNow(void);
    int run_wind(control_data_t *data,	std::map <int,struct PCN_PANSAIL *> panList);
    int run_stress(control_data_t *data);
    int  PostSomeResults() ;
    int m_AmInBringAll;
    int m_GraphicsEnabled;
public:
    int SetGraphicsEnabled( const int i)
    {
        int rc = (i!= m_GraphicsEnabled);  m_GraphicsEnabled=i; return rc;
    }
    int GraphicsEnabledQ() const {return m_GraphicsEnabled;} // see also the global Fn
    int Set_Output_Flags(void);
    int Bring_All_Models_To_Date();
    void SetDBin ( RXDataBaseLogI* a )	{ m_DBin=a;}
    void SetDBout ( RXDataBaseLogI* a )	{ m_DBOut=a;}
    void SetDBMirror(MIRRORPTR  a);
    RXDataBaseLogI* DBin()const	{return m_DBin;}
    MIRRORPTR  DBMirror()const	{return m_DBmirror;}
    RXDataBaseLogI* DBout()const	{return m_DBOut ;}

    //	int  m_Last_Summary_Line;
    //	QString  m_qLast_Summary_File;
    int m_StateFile; // 1 means apply nodes even if not editable. Value could come from sd->SailSum

    int PostAllDBInputs(); // call this after opening a logfile or log DB

    void ProcessIncoming(); // a platform-dependent treatment of  actions from DB, sockets, etc.
    void ExecuteDelayed(const std::string &s);
    /// typically called from the app's OnIdle method or main event loop.
private:  
    QMutex m_cs;
#ifdef MICROSOFT
    class CWnd* m_cwnd;
public:
    std::set<pair<HWND,HWND> > m_callers;  // first is caller, 2nd is interested Relax m_hWnd
    void SetCWnd(class CWnd*p ) {m_cwnd =p;  }
    class CWnd* GetCWnd() {return m_cwnd;  }

#endif
private:
#ifdef HOOPS
    HBaseModel * m_hbm;
    HDB* m_HDB;
    int SetDriverType(void);
    int DefineAllCallbacks(void);
#endif
    void InitActionMap();
    std::vector<RXSail*> m_sails;
    double m_HeelAngle; // DEGREES

    class RXDataBaseLogI *m_DBin;
    class RXDataBaseLogI *m_DBOut;
    MIRRORPTR m_DBmirror;
    //	RXDatabaseI *m_DBcurrent;
    std::set<class RXSummaryTolerance* > m_tol_list;
    std::queue<std::string> m_commandQueue;
    std::map<pair<int,int>,int> m_ActionOnEdit;
    int m_CalcIsRunning;
public:
    int Check_Convergence(void); // the principle convergence test.
    int AddOneTol(const std::string &s);
    int Clear_SumTolList(void);
    int FlushTolHistory();
    int EditAction(const int type,const int column);
    int PostProcessAllSails(int wh);
    int SetUpAllTransforms(int regardless);
    // determines the weight given to lengths and angles in Meyer flattening
    // low value(0) prioritizes length and makes a small head wobbly.
    // high value (1) makes the head clean but bunches the isoparms high up .
    //so use the lowest value which makes the head clean.
    double m_LambdaForxyz;
    int SendMessageToCallers(int lparam);
    int DatabaseValue(const QString head, double&value) ;
    int Post_Wind_Details( );
    int Post_VMC();
    class RXWindGeometry m_wind;

private:
    char m_defaultRunName[128],  m_Run_Name[128], m_LastRunName[128];
    std::set<class RXSubWindowHelper*> m_subwindows;
    int m_AutoCalc;
    int MakeRunNameUnique();
#ifdef LINK_H5FDDSM
    class rxh5fddsmlink *m_dsmlink;
#endif
public:
    int DSM_Init(int pUseDsm);
    int DSM_End();
    int DSM_TransferModels(const QString s);


    int IncrementRunName(void) ;
    char *GetRunName(){return m_Run_Name;}
    int  ModifyRunName(const char*p_new);
    int PleaseWriteZIs;
    int m_DrawingLayerHasPrecedence;
};

extern RelaxWorld *g_World;

EXTERN_C void Start_RelaxWorld();
EXTERN_C void End_RelaxWorld();
EXTERN_C int RXGraphicsEnabledQ() ; // see also the method

#ifdef HOOPS
#define THE_HDB g_World->GetHDB()
#endif

#endif // !defined(AFX_RELAXWORLD_H__8CD9BEFF_7920_4842_8FF5_9EA8EF26E075__INCLUDED_)
