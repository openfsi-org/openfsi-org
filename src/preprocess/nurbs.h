#ifndef _NURBS_HDR_
#define _NURBS_HDR_

#include "RXON_Surface.h" // dec 2007

#include "vectors.h"

#include "griddefs.h"

//#define NURBMAX 128
#define  NURB_U  1
#define  NURB_V  2
#define  NURBS_NORM  1
#define  NURBS_NONORM  0

/* the different types of knot vector */

#define UNKNOWN		-1
#define FROMATTS	4
#define FROMFILE	8
#define NUNIFORM	16
#define ARCLENGTH	32
#define ALGORITHM   64
#define SSD			128
#define PCN_IGES	256
#define PCN_ONE_D	512
#define PCN_ON_FLAG	1024	// non-zero if the entity maintains the Opennurbs object.
#define BADNURBS  (-100000.0)


struct PC_NURB {
	double tol; /* only used in XYtoUV iteration */
	int LimitU,LimitV, KValU,KValV,head,tail, KoffU,KoffV;
	int knotN,knotK, Limit, uv;
//	float Wu[NURBMAX ],Wv[NURBMAX ]; // Sept 2002 for HOOPS compatibility
//	float KnotVectorU[NURBMAX ];	// Sept 2002 for HOOPS compatibility
//	float KnotVectorV[NURBMAX ];	// Sept 2002 for HOOPS compatibility
//	double u[NURBMAX ],  v[NURBMAX ];

	float **m_W; // Sept 2003 for 3DM compatibility
	float  *m_KnotVectorU;	
	float  *m_KnotVectorV;	
	double *m_u,  *m_v;		// u and v

	int Knot_Type, Interpolatory;
	VECTOR**iP, **cP;
	int flags;
#ifdef __cplusplus
	ON_Object * m_pONobject;
#else
	void* m_pONobject;
#endif
	};

EXTERN_C int PCN_Nurbs_Alloc(struct PC_NURB *n) ; // requires LU,LV, Ku,Kv to be filled in.
int PCN_Create_Interpolated_Nurb(struct PC_NURB *n);
EXTERN_C int PCN_Free_Nurb(struct PC_NURB *n);
int Draw_Control_Graph(struct PC_NURB *n); 
int Draw_Control_Graph_Mesh(struct PC_NURB *n);
int Draw_Nurb_Labels(RXEntity_p e, int nu,int nv);

int Draw_Surface_Trigraphs(RXEntity_p e);

int Compute_NURBS(RXEntity_p e); /* prepares a NURBS by reading the line */
int  AddHeadRows(struct PC_NURB *nurb ,int n);
int  AddTailRows(struct PC_NURB *nurb, int n);

int Evaluate_NURBS_By_XYZ(RXEntity_p e,const double xx,const double yy,const double zz, double*u,double*v);  //{u,v} are normalized

double Evaluate_NURBS_By_XY(RXEntity_p e,const double xx,const double yy,double*u,double*v, int*err ,const ON_Xform *tr ); 
/* returns z  at (xx,yy); */

int Evaluate_NURBS_Derivatives(RXEntity_p e,const double u,const double v,
							   ON_3dPoint*r, 
							   ON_3dVector*r10, ON_3dVector*r01,
							   ON_3dVector*r11, ON_3dVector*r20, ON_3dVector*r02);

//extern double Evaluate_NURBS(RXEntity_p e, double u, double v,VECTOR*res);
extern double Evaluate_NURBS(RXEntity_p e, double u, double v,ON_3dPoint *res);

/* returns vector at (u,v);  and Z as function value*/
double 	Evaluate_NURBS_By_N(struct PC_NURB *n,double u, double v,ON_3dPoint*res );

 
int PCN_Evaluate_NURBS_Tangents(RXEntity_p e,const double u,const double v,ON_3dPoint *r,
								ON_3dVector*tu, ON_3dVector*tv);

/* returns vectors dfbydu and dFbyDv at (u,v); 
Doesnt normalise the vectors
*/
int PCN_Slow_Evaluate_U_Tangent(RXEntity_p e, double u, double v,VECTOR*tu);
int PCN_Slow_Evaluate_V_Tangent(RXEntity_p e, double u, double v,VECTOR*tv);

int Evaluate_U_Tangent(RXEntity_p e, double u, double v,VECTOR*tu);
int Evaluate_V_Tangent(RXEntity_p e, double u, double v,VECTOR*tv);


int Set_Nurb_Globals(struct PC_NURB *nu, int k,int i,int n,int uvflag) ;


double  PCN_Calc_Bval_nw(struct PC_NURB *nurb, int k,int i,int n,double u,int uv);
double  Calc_Bval_1(struct PC_NURB *nurb, int k,int i,int n,double u,int uv );
double  Calc_Bval_2(struct PC_NURB *nurb, int k,int i,int n,double u ,int uv);


double Bspline(struct PC_NURB *n, int i,int k, double u ,int im) ;
double DBspline(struct PC_NURB *n, int i,int k, double u ,int im,int k0) ;
double DDBspline(struct PC_NURB *n, int i,int k, double u ,int im) ;

float Knot(struct PC_NURB *n,int i) ;


int PCN_Print_Nurb_Knots ( struct PC_NURB *n,FILE*fp);
int PCN_Print_Nurb_Table(struct PC_NURB *nurb,FILE*fp); 
int Print_Basis_Functions( RXEntity_p e,double u);

EXTERN_C int PCN_Set_Weights(struct PC_NURB *n) ;
int DxDy_to_DuDv (RXEntity_p e,double dx, double dy, double*u, double*v);
int Set_Knot_Vectors(RXEntity_p e, struct PC_NURB *n);
int PCN_Create_One_Knot_Vector(struct PC_NURB *n, int Lim,int  head,int tail,int order,float*KV);
EXTERN_C int PCN_Print_Nurbs_Samples(const RXEntity_p e, FILE *fp);
EXTERN_C int PCN_OneD_NURBS_To_Poly(struct PC_NURB *n,int *c,VECTOR **poly);

#endif  //#ifndef _NURBS_HDR_

