 
 /* pansin.h 


 created  28/11/94 PH. headers from pansin.c */

#ifndef PANSIN_16NOV04
#define PANSIN_16NOV04


//EXTERN_C int AKM_Pansail_UVtoXY_Only(SAIL*sail,const double  u,const double v,double*x,double*y);
//EXTERN_C int AKM_Pansail_UVtoXYZ(SAIL*sail,const double u,const double v,double *x,double *y,double *z);

EXTERN_C int interp_(int *const model,float *const u,float *const v,float *x,float *y,float *z);

//EXTERN_C  int Deflected_To_Iges(SAIL*sail, char*fname,int nu,int nv) ;

#endif //#ifndef PANSIN_16NOV04
