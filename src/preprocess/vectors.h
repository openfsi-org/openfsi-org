/* FILE: VECTORS.H  header file for vectors.C
date  Thursday 2 April 1992

3 Oct 2006 touch the file to assure a rebuild after losing the MSVC copy
*/

#ifndef _HDR_VECTORS_
#define _HDR_VECTORS_	1
#ifdef __cplusplus
#include "opennurbs.h"
#endif
typedef struct  {
	float x,y,z;
} VECTOR;

typedef struct  {
	double x,y,z;
} DVECTOR;

//EXTERN_C double distToArea(double a,double b,double c);
//EXTERN_C double doubleCompute_Cross_Product(DVECTOR *a,DVECTOR *b,DVECTOR *v);
//EXTERN_C double double_triarea_by_xy(	double x1,double y1,
//				double x2,double y2,
//				double x3,double y3);
//extern   VECTOR *   PreMult_Vect_By_Trigraph (float *d, VECTOR*v,VECTOR *vd); 
//extern   VECTOR *   PreMult_Vect_By_Trigraph (float *d, float v[3],VECTOR *vd); 


//extern   ON_3dVector *PreMult_Vect_By_Trigraph (float *, float *, ON_3dVector *);
//extern   ON_3dVector  PreMult_Vect_By_Trigraph (float *d, const ON_3dVector&v, ON_3dVector&vd); 

extern   ON_3dVector*  PostMult_Vect_By_Trigraph(float *d, const ON_3dVector v,ON_3dVector *vd);

//EXTERN_C int VectorPrint(FILE *ff,VECTOR *v);
EXTERN_C double dists2D(const VECTOR *s,const VECTOR *t);
EXTERN_C double distV(VECTOR *s,VECTOR *t); 
EXTERN_C double distV_sq(VECTOR *s,VECTOR *t);

//extern int PC_Compute_Perp_Transform(VECTOR*,double d[3][3]);
extern int PC_Compute_Perp_Transform(ON_3dVector *,double d[3][3]);

EXTERN_C int PC_Compute_3Matrix_Product(double a[3][3], double b[3][3], double r[3][3]);
extern int Compute_TrigraphXY(ON_3dVector x,ON_3dVector y,float d[3][3]);
//extern int Compute_TrigraphXY(VECTOR*,VECTOR *,float d[3][3]);
EXTERN_C int Compute_TrigraphYZ(VECTOR*,VECTOR *,float d[3][3]);

extern int Compute_TrigraphXZ(const ON_3dVector &x,const ON_3dVector&z,float d[3][3]);
extern int Compute_TrigraphXZ(VECTOR*,VECTOR *,float d[3][3]);

//double PC_Apparent_Angle(VECTOR a, VECTOR b, VECTOR view);
double PC_Apparent_Angle(const ON_3dVector a, const ON_3dVector b, const ON_3dVector view);

//double PC_True_Angle(VECTOR *a, VECTOR *b, int*err);
double PC_True_Angle(const ON_3dVector a, const ON_3dVector b, int*err);

double PC_Signed_True_Angle(VECTOR a, VECTOR b, ON_3dVector view,int *err);

//EXTERN_C void PC_Vector_Op(VECTOR ,VECTOR ,const char *, VECTOR *);

extern void PC_Vector_Copy(const VECTOR src, VECTOR *dest) ;
extern void PC_Vector_Copy(const ON_3dPoint src, VECTOR *dest) ;
extern void PC_Vector_Copy(const VECTOR src, ON_3dPoint *dest);

//EXTERN_C int    PC_Vector_Compare(VECTOR , VECTOR, double ) ;

#ifdef __cplusplus
	extern void PC_Vector_Difference(const VECTOR * ,const VECTOR * , VECTOR *);
	#ifdef Site

		extern void PC_Vector_Difference(const Site * ,const VECTOR * , VECTOR *);
		extern ON_3dVector  PC_Vector_Difference(const Site * ,const Site * );
		extern double PC_Dist(const Site* ,const VECTOR* );
		extern double PC_Dist_sq(const Site* ,const VECTOR* );
	#endif
	extern double PC_Dist(const VECTOR* ,const VECTOR* );
	extern double PC_Dist_sq(const VECTOR* ,const VECTOR* );

#endif
EXTERN_C double PC_Polyline_Length(int,VECTOR []);
//EXTERN_C void   vprint(const char *,VECTOR [],int) ;
//EXTERN_C double  Polygon_Area(int cin,VECTOR *p,const ON_3dVector normal);
EXTERN_C int PC_Vector_Rotate(ON_3dVector &x,const double angle,const ON_3dVector Zaxis);

#endif  //#ifndef _HDR_VECTORS_

