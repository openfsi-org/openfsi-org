#ifndef _STRINGUTILS_H_
#define _STRINGUTILS_H_


#ifdef __cplusplus
		#define EXTERN_C extern "C"
#else
		#define EXTERN_C extern
#endif  //#ifdef __cplusplus

#include <stdio.h>
#include <string.h>	
#include <stdlib.h>
EXTERN_C bool str_islower( const char *s) ;
EXTERN_C char *stripcomments( char*p_lp); // 
EXTERN_C char *compresstok(char *s,char *tok);
EXTERN_C char *unbracket(char *s,const char *tok);
EXTERN_C char *strclend(char *s) ;
EXTERN_C char *EOL_To_Blank(char *s) ;
EXTERN_C int rxIsEmpty(const char *s); // was is_empty but clashes with cpp11
EXTERN_C int is_clean(const char *s,const int length);
EXTERN_C char *streos( char *s1, char *s2);
EXTERN_C const char *strieos( char *s1, char *s2);
EXTERN_C char *strrep(char *s1,char a,char b);
EXTERN_C char *strtolower(char *s);

EXTERN_C char *strtoupper(char *s);
EXTERN_C char *stristr(const char *s1,const char *s2);
EXTERN_C char *striistr(const char *s1,const char *s2);

EXTERN_C int PC_Strip_Leading(char *);
EXTERN_C int PC_Strip_Leading_Chars(char *, const char *);
EXTERN_C int PC_Strip_Trailing(char *s);
EXTERN_C int PC_Strip_Trailing_Chars(char *, const char *);
EXTERN_C int PC_Replace_Char(char*s, char a, char b);
EXTERN_C int Separators_to_Colons(char *line);
#endif

