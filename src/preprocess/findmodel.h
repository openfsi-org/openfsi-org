#ifndef _FINDMODEL_H_
#define _FINDMODEL_H_

#error(" dont use  findmodel.h")

SAIL   *ffst(const int flag ); 
SAIL   *fnxt(void);
int		fend(void);

#define MODELSEARCH(f,s) for( s=ffst(f);s; s=fnxt())
#define ENDMODELSEARCH fend()

#endif

