#ifndef _GLE_H_
#define _GLE_H_

extern "C"  void cf_delete_gle(void **p_i);
extern "C"  void  cf_printallgle(int *u);
extern "C"  void  cf_gle_export_forces(void);

EXTERN_C int Resolve_Gle_Card(RXEntity_p e);

int Resolve_BIG_Gle_Card(RXEntity_p e, FILE *p_fp);

#endif  //#ifndef _GLE_H_


