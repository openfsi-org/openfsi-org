/* Rela5xII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *  Ayg 2006 for 64bit
 *
  28 march 2007  bigger buffers on Resolve_SeamCurve
 * Modified :
May 2003 in Create_Patch_From_SC   I have no idea why we didnt permit space or dot in the focus name. This must have been due to a particular reading problem.  Possibly with SailMaker. 
I suggest using MakeValidSegname instead 
Make_Valid_Segname(focus);
May 2003  pockets didnt like names beginning with a number, which is the Rhino IGES default

   */

#include "StdAfx.h"
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXExpression.h"
#include "RelaxWorld.h"
#include "RXEntityDefault.h"
#include "RXSRelSite.h"

#include "RXLayer.h"

#include "RXSeamcurve.h"
#include "RXRelationDefs.h"
#include "RXAttributes.h"
#include "rxsubwindowhelper.h"
#ifndef linux
#include "..\stringtools\stringtools.h" // defines FromUtf8 and ToUtf8

#else
#include "../../stringtools/stringtools.h"
#endif
#include "MTTools.h"
#include "akmutil.h"

#include "peterfab.h"

#include "stringutils.h"
#include "polyline.h"
#include "entities.h"
#include "panel.h"
#include "tpn.h"

#include "batdefs.h"
#include "rxbcurve.h"
#include "fields.h"

#include "global_declarations.h"

#include "rsummary.h"
#include "uselect.h"
#include "alias.h"
#include "interpln.h"
#include "etypes.h"
#include "finish.h"

#ifdef WIN32
#define _max max
#define _min min
#endif

#include "aero.h"
#include "aeromik.h"
#include "boundary.h"
#include "camera.h"
#include "delete.h"
#include "dxf.h"

#include "files.h"
#include "gcurve.h"
#include "material.h"
#include "ReadBagged.h"
#include "ReadGenDp.h"
#include "ReadStripe.h"
#include "targets.h"

#include "arclngth.h"
#include "question.h"
#include "iangles.h"

#ifdef USE_PANSAIL
#include "velocity.h"
#endif

#include "paneldata.h"
#include "iges.h"
#include "gle.h"

#include "ReadName.h"

#include "rlx3dmbuffer.h"
#include "pressure.h"

#include "RXdovefile.h"
#include "RXContactSurface.h"
#include "RX_FEString.h"
#include "RXQuantity.h"
#include "RXException.h"

#include "resolve.h"

#ifdef _DEBUG
int Global_Warnings = 99; /* should be defined in get glo bal */
#else
int Global_Warnings = -1; 
#endif



int Resolve_Mould_Card (RXEntity_p e) {
    /* card is of type
     mould: <name> : <interpolation surface> <(uv|xy)>: [atts]
 OBJECTIVE
 To define the ISurface as the sail->Mould.
 if an <XY> type, the Isurface is a scalar  interpolation surface  of z as f(x,y)
  if a <uv> type, the Isurface is a vector interpolation surface  of {x,y,z} as f(u,v)
 using a generalisation of the Read_NDD method

      */

    char text[256], atts[256];
    const char*name,* surface;

    RXEntity *ep;
    const char *line=e->GetLine();
    int retval=0;   e->Needs_Resolving=1;
    std::string sline(e->GetLine());
    rxstriptrailing(sline);
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();
    if(nw<3) return 0;

    name= wds[1].c_str();
    surface= wds[2].c_str();


    if(nw>3) strcpy(text,wds[3].c_str()); else  strcpy(text,"XY");
    if(nw>4) strcpy(atts,wds[4].c_str()); else strcpy(atts,"  ");

    if(strieq(text,"XY")) {
        ep = e->Esail->Get_Key_With_Reporting( "interpolation surface",surface);
        if (!ep)
            return 0;
    }
    else if(strieq(text,"UV")) {
        e->OutputToClient (" UV surfaces not currently supported",2);
        ep = e->Esail->Get_Key_With_Reporting("vector field",surface);
        if(!ep) {  return(0); }
        ep->SetRelationOf(e,child|niece,RXO_VECFLD_OF_MOULD);
    }
    else {
        e->OutputToClient ("only XY or UV surfaces currently supported",2);
        return(0);
    }

    e->Esail->SetMould(ep);

    if(e->PC_Finish_Entity("mould",name,NULL,(long)0,NULL,atts,line)) {
        e->SetNeedsComputing(0);
        e->Needs_Finishing=0;
        retval=1;
    }
    else
        retval=0;

    e->Needs_Resolving=(!retval);
    return(retval);
} 


int Resolve_Summary_Card(RXEntity_p e) {
    /* Summary card is of type
   ToExcel :  segspec : optionspec
   Causes PC_Get_Key to return the key of <type,n1>
   when called with <type,n2>
   */
    int retval = 0;
    struct PC_SUMMARY *p; // this is ToExcel
    const char *segspec,*optionspec;

    //   const char*line = e->GetLine();
    std::string sline(e->GetLine());
    rxstriptrailing(sline);
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();
    if(nw<3) return 0;

    segspec  = wds[1].c_str();
    optionspec=wds[2].c_str();

    p = (PC_SUMMARY *)CALLOC(1,sizeof(struct PC_SUMMARY)); // this is ToExcel
    if (p==NULL) {  return(0);} // we never get here becuase we fire an exception on an alloc failure.
    p->segspec=STRDUP(segspec);
    p->optionspec=STRDUP(optionspec);

    e->SetDataPtr(p);
    e->SetNeedsComputing(0);
    e->Needs_Resolving=0;
    retval = 1;

    return(retval);
}  



int RXSail::Parse_Seam_Position(const char*name, char *seamname,char *offset) {
    /* returns 1 if the name can be parsed as (luff@50%)
     where the brackets may be ({[]}) or nothing and the delimiter is @
     NOTE: can't use + or $ because + used in naming of generated relsite
     and $ used in generating index key in tree.
     */

    char s[256], inside[256],*s1;
    strcpy(s,name);

    s1 = strtok(s,"({[]})\t");
    if(!s1) return(-1);
    strcpy(inside,s1);

    s1= strtok(inside,"@");
    if(!s1||!*s1) return(0);
    strcpy(seamname,s1);

    s1=strtok(NULL,"]})");
    if(!s1||!*s1) return(0);
    strcpy(offset,s1);
    if(strieq(offset,"0.00%")) {
        char buf[256];
        strcpy(offset,"0.5%");
        sprintf(buf," %s converted to {%s@%s}", name,  seamname,offset);
        rxerror(buf,2);
    }
    if(strieq(offset,"100.00%")) {
        char buf[256];
        strcpy(offset,"99.5%");
        sprintf(buf," %s converted to {%s@%s}", name,  seamname,offset);
        rxerror(buf,2);
    }



    PC_Strip_Trailing(seamname);
    return(1);
}

int Report_Init(SAIL * p_sail, struct REPORT_FORM*r) {
    r->z=NULL;
    r->N=0;
    r->m_owner=NULL;
    r->m_Sail=p_sail;
    return(1);
}	

int Report_Clear(struct REPORT_FORM*r) {
    int k;

    for(k=0;k<r->N;k++) {
        if(r->z[k].otype  )
            RXFREE(r->z[k].otype);
        if(r->z[k].oname  )
            RXFREE(r->z[k].oname);
        if(r->z[k].ctype  )
            RXFREE(r->z[k].ctype);
        if( r->z[k].cname )
            RXFREE(r->z[k].cname);
    }
    if(r->z)
        RXFREE(r->z);
    r->z=NULL;
    r->N=0;
    r->m_owner=NULL;
    return(1);
}
int Report_Sort_Fn(const void*ia, const void*ib){
    struct REPORT_ITEM *a=  (struct REPORT_ITEM *) ia;
    struct REPORT_ITEM *b = (struct REPORT_ITEM *) ib;
    if(a->value>b->value) return(-1);
    if(a->value<b->value) return(1);

    if(strieq(a->otype,b->ctype) && strieq(a->oname,b->cname))
        return(1);
    if(strieq(b->otype,a->ctype) && strieq(b->oname,a->cname))
        return(-1);
    return(0); /*strcmp(a->cname,b->cname));	*/
}

//int Report_Sort2_Fn(const void *va,const void *vb){ // from  report.cpp
//  MissingData *mda,*mdb;
//  struct tnode **a, **b;
//  int cmp;

//  a =  (struct tnode **) va;
//  b = (struct tnode **) vb;

//  mda = (MissingData *) (*a)->data;
//  mdb = (MissingData *) (*b)->data;

//  if(mda->needs_count > mdb->needs_count) return(1);
//  if(mda->needs_count < mdb->needs_count) return(-1);
//  if(mda->neededBy_count < mdb->neededBy_count) return(1);
//  if(mda->neededBy_count > mdb->neededBy_count) return(-1);

//  cmp =stricmp(mdb->type,mda->type);

//  return(cmp);	/* same on both counts */

//}

void RXSail::SortReportForm() {
    qsort( this->m_ReportForm.z, this->m_ReportForm.N, sizeof(struct REPORT_ITEM),&Report_Sort_Fn);
}

#ifdef WIN32				
int DBG_Create_Wanted_Entities(SAIL *sail,struct REPORT_FORM*r) {
    /* the idea: If the model lacks battens or materials
 we create renames to any one that does exist*/
    int k, depth=1,done =0, count=0;
    char string[512] ;
    for(k=0;k<r->N;k++) {
        depth=1; done=0;
        if(strieq(r->z[k].otype,"regiondata")) {
            if(RXEntity* zzz= sail->GetKeyWithAlias(r->z[k].otype,r->z[k].oname))//
                zzz->Kill( );
            count++;
            done=1;
        }
        else
            if(strieq(r->z[k].ctype,"batten")) {
                sprintf(string,"batten:%s:14:1:0:0:100:0",r->z[k].cname);
                Just_Read_Card(sail,string,"batten",r->z[k].cname,&depth);
                count++;
                done=1;
            }
            else if( stristr(r->z[k].ctype,"material") ||  stristr(r->z[k].ctype,"fabric")){
                PC_Strip_Leading(r->z[k].cname);
                if(!sail->Entity_Exists(r->z[k].cname,"material")){
                    sprintf(string,"material:%s:yellow:wrinkle:0.3:50mm:0.40%%:table:ext(%%)\t0.00%\t1.50%\nwarp\t0\t1500\nfill\t0\t397\nbias\t0\t217\n!end",r->z[k].cname);
                    Just_Read_Card(sail,string,"material",r->z[k].cname,&depth);
                    count++;
                }
                done=1;
            }
            else if(strieq(r->z[k].otype,"field") && strieq(r->z[k].ctype,"site,relsite")){
                //WRONG but we assume there exists a site whose name is the first word of cname
                char *c, n[256];
                // the focus name is name up to the first blank or .
                strcpy(n, r->z[k].cname);
                PC_Strip_Leading(n);
                if ((c = strpbrk(n," .")))
                    *c=0;

                depth=0;
                if(sail->GetKeyWithAlias("site",n)){
                    sprintf(string,"rename: %s : %s : %s : %s",r->z[k].cname,"site" ,n, r->z[k].cname);
                    Just_Read_Card(sail,string,"rename",r->z[k].cname, &depth);
                    count++;
                    done=1;
                }
                else
                    done=0;
                //rename	Rename1	fabric	20000 Denier	topfab

            }

        if(!done) { /* make a rename to the first that we find */
            const char *another=NULL ;
            ent_citer it;
            for (it = sail->MapStart(); it != sail->MapEnd(); it=sail->MapIncrement(it)) {
                RXEntity_p p  = dynamic_cast<RXEntity_p> (it->second);

                if (stristr(r->z[k].ctype,p->type() ) && !p->Needs_Resolving){
                    another = p->name();
                    break;
                }
            }
            if(another) {
                depth=0;
                sprintf(string,"rename: %s : %s : %s : %s",r->z[k].cname,r->z[k].ctype, another,r->z[k].cname);
                Just_Read_Card(sail, string,"rename",r->z[k].cname, &depth); // type was r->z[k].ctype
                count++;
            }
            else {
                sprintf(string," Cannot fudge an alias for %s  %s", r->z[k].cname,r->z[k].ctype);
                g_World->OutputToClient(string,3);	HC_Exit_Program();
            }
        }
    }
    return count;
}//int DBG_Create_Wanted_Entities
#endif  //#ifdef WIN32	

char *Report_To_String(struct REPORT_FORM*r,int c) {
    size_t l;
    int k;
    char *s;
    l=32;
    if(c<0) c=r->N;
    else c = min(c,r->N);
    for(k=0;k<c;k++) {
        if(r->z[k].otype ) l += strlen(r->z[k].otype);
        if(r->z[k].oname ) l += strlen(r->z[k].oname);
        if(r->z[k].ctype ) l += strlen(r->z[k].ctype);
        if(r->z[k].cname ) if(r->z[k].cname) l += strlen(r->z[k].cname);
        l+=64;
    }
    s = (char *)MALLOC(l);
    if(!s) return NULL;
    *s= 0;
    strcat(s,"(Report_To_String)Unidentified entities\n");
    for(k=0;k<c;k++) {
        strcat(s,"O>");
        if(r->z[k].otype ) strcat(s, r->z[k].otype);
        strcat(s," , ");
        if(r->z[k].oname )strcat(s,r->z[k].oname);
        strcat(s," needs(eventually) ");
        if(r->z[k].ctype )strcat(s,r->z[k].ctype);
        strcat(s," , ");
        if(r->z[k].cname )strcat(s,r->z[k].cname);
        strcat(s,"\n");
    }
    assert(strlen(s)<l);
    return(s);
}		


int Parse_BP_Line(SAIL*const sail,const char*line,char*type,char*name,char*nname,char*n1,char*n2,char*mat,float*pthickness,char*basecurve,float*pdepth,char*att) {
    // type :name
    // if type is 'patch'
    // type :name:nname:

    //else type isnt patch
    // type :name: n1:n2:mat :s5(either basecurve or thiskness):
    char s5[256];
    int k,iret;
    iret=0;
    std::string sline(line );
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();
    rxgetword(wds,0,type ,256); //Parse_BP_Line

    if ( rxgetword(wds,1,name ,256)){
        if(strieq(type,"patch")) {
            rxgetword(wds,2,nname  ,256);
            k=3;
        }
        else k=2;
        if (rxgetword(wds,k++,n1 ,256) ){
            if (rxgetword(wds,k++,n2 ,256)){
                if (rxgetword(wds,k++,mat ,256)){
                    /* 5 is either basecurve or thickness */
                    iret=1; /* basic minimum */

                    if (rxgetword(wds,k++,s5 ,256)) {
                        if (isOnlyNumber(TOSTRING(s5)) && (1==sscanf(s5,"%f",pthickness))) {
                            if (!rxgetword(wds,k++,basecurve ,256))strcpy(basecurve,"straight");
                        }
                        else {
                            *pthickness=(float)1.0;
                            strcpy(basecurve,s5);
                            k=6;
                        }
                    }
                    else  { // no 5, use defaults
                        strcpy(basecurve,"straight");
                        *pthickness=(float)1.0;
                    }

                    if (rxgetword(wds,k++,att ,256)){
                        if (isOnlyNumber(TOSTRING(s5) ) && (1==sscanf(att,"%f",pdepth))){
                            *pdepth=(float)Evaluate_Quantity(sail,att,"length");
                            if(!rxgetword(wds,k++,att ,256))
                                strcpy(att,"flat Xscaled Yscaled drawmoulded");
                        }
                        else
                            *pdepth=(float)0.0;
                    }
                    else {
                        *pdepth=(float)0.0;
                        strcpy(att,"flat Xscaled Yscaled draw moulded");
                    }
                }
            }
        }
    } // if name
    return(iret);
}


int Resolve_Patch_Card (RXEntity_p e) {

    /*
   patch card is of type
   patch:  name  :  SITE1 : SITE2 : material :[thickness]: basecurve :[depth:[atts]
  if SITE1 and SITE2 are empty, the basecurve field must apply to a seamcurve

  The focus is taken to  have the same name as the patch.
 */

    char name[256],nname[256],n1[256],n2[256],basecurve[256];
    char att[256],type[64],material[256];
    float depth,thickness;
    struct PC_PATCH *p;
    RXEntity *p3=NULL;
    Site * p1, *p2;
    sc_ptr  ccd;
    class RXLayer *fabdata;
    HC_KEY key=0;
    int k,A_SC_Draw_Type=0;
    const char*line=e->GetLine();
    int retval=0;


    k = Parse_BP_Line(e->Esail,line,type,name,nname,n1,n2,material,&thickness,basecurve,&depth,att);
    if(!k)
        return retval;

    p = (PC_PATCH *)CALLOC(1,sizeof(struct PC_PATCH));
    p->m_type = PATCH;

    if(rxIsEmpty(n1) && rxIsEmpty(n2)) {
        sc_ptr sc;
        p3 = e->Esail->Get_Key_With_Reporting("curve,seamcurve,compound curve",basecurve);
        if(!p3){RXFREE(p); return(0);	}
        p3->SetRelationOf(e,child,RXO_CC_OF_PATCH);;

        ccd = (sc_ptr  )p3;

        p->end1ptr=p->e[0] = p1 =  ccd->End1site;
        p->end2ptr=p->e[1] = p2=   ccd->End2site;
        p->basecurveptr = dynamic_cast<RXBCurve *>(p3) ;
        sc = (sc_ptr  )p3;
        if(!sc->sketch)A_SC_Draw_Type=1;
    }
    else 			   /* n1,n2 are non-zero */
    {

        p1= (Site*) e->Esail->Get_Key_With_Reporting("site,relsite",n1);
        p2= (Site*) e->Esail->Get_Key_With_Reporting("site,relsite",n2);

        if (p1==NULL ||p2==NULL )  {RXFREE(p); return(0);}

        p1->SetRelationOf(e,child,RXO_N1_OF_PATCH);
        p2->SetRelationOf(e,child, RXO_N2_OF_PATCH);


        (*p).e[0] = p1; (*p).e[1] = p2;
        (*p).end1ptr=p1;(*p).end2ptr=p2;
        (*p).d=depth;


        p3= e->Esail->Get_Key_With_Reporting("basecurve",basecurve);
        if (p3==NULL ) 	{
            RXFREE(p);
            return(0);
        }
        p3->SetRelationOf(e,child,RXO_BC_OF_PATCH );
        p->basecurveptr=p3;
    }   // end of if n1,n2 non-blank

    p->m_corner= e->Esail->Get_Key_With_Reporting("site,relsite",nname);
    if (p->m_corner==NULL ) 	{RXFREE(p);	return(0);}
    p->m_corner->SetRelationOf(e,child,RXO_CORNER_OF_PATCH);

    p->mat= e->Esail->Get_Key_With_Reporting("fabric",material);
    if (!p->mat) 	{
        RXFREE(p);
        return(0);
    }
    p->mat->SetRelationOf(e,child,RXO_MAT_OF_PATCH);
    p->mat->SetNeedsComputing();



    p->mat = e->Esail->Insert_Entity("layer",name,0,(long)0, " "," belongs to batpatch" );
    fabdata= (class RXLayer*) p->mat;
    fabdata->matptr=p->mat;
    e->SetRelationOf( p->mat,spawn,RXO_LAYER_OF_PATCH);

    if(stristr( att,"uniform")) {
        fabdata->matstruct.function = Get_Material_Function_Key("uniform");
        fabdata->matstruct.m_sizeofdata=3;
        fabdata->matstruct.m_Data.resize(3);
        fabdata->Use_Black_Space=1;
        fabdata->Use_Me_As_Ref=0;
    }
    else {
        fabdata->matstruct.function = Get_Material_Function_Key("radial");
        fabdata->matstruct.m_sizeofdata=8;
        fabdata->matstruct.m_Data.resize(8);// =(float*)CALLOC(8,sizeof(float));
        fabdata->Use_Black_Space=1;
        fabdata->Use_Me_As_Ref=0;
        fabdata->matstruct.m_Data[6] = thickness;
    }
    e->SetRelationOf(p->mat,child,RXO_MAT_OF_PATCH );
    if(A_SC_Draw_Type){
        sprintf(basecurve,"compound curve : %s : %s " ,name,p3->name());
        k = e->generated;
        p->cc= Just_Read_Card(e->Esail,basecurve,"compound curve",name,&k);
    }
    else{
        assert("(resolvePatch) SC!!"==0); ccd=0;//(sc_ptr  )CALLOC(1,sizeof(struct PC_SEAMCURVE));
        if(!ccd) return(0);
        p->cc=e->Esail->Insert_Entity("compound curve",name,ccd,(long) 0,NULL,"generated from BATPATCH"); e->SetRelationOf (p->cc,spawn,RXO_CC_OF_PATCH);
        assert(0); //ccd->owner=p->cc;
        p->cc->PC_Finish_Entity("compound curve",name,ccd,(long)0,NULL,NULL,NULL);
        p->cc->Needs_Resolving=0;
        p->cc->Needs_Finishing=0;
    }
    e->SetRelationOf(p->cc,child,RXO_CC_OF_PATCH );

    if(e->PC_Finish_Entity("patch",name,p,key,NULL,att,line)) {
        e->Needs_Resolving=0;
        e->Needs_Finishing=1;
        retval = 1;
    }


    return(retval);
}

int Resolve_Batten_Card (RXEntity_p e) {
    /* this is the base batten.

   batten : name : loadKN :  lengthmm : x ,y,x,y,.....   : [optional $atts]    */

    int k,OK,retval=0;
    RXSTRING atts ;
    std::wstring sline( TOSTRING(e->GetLine()) );
    std::vector<std::wstring> wds = rxparsestring(sline,RXENTITYSEPS_W,false);
    int nw = wds.size();
    if(nw<4) return 0; // 4 means uniform.
    struct PC_BATTEN *p = (PC_BATTEN *)CALLOC(1,sizeof(struct PC_BATTEN));
    if (! p) { return(0);}
    RXExpressionI * q;

    //  s =wds[2].c_str(); /*  load N */ p->force =Evaluate_Quantity(e->Esail,s,"force");
    q= e->AddExpression ( new RXQuantity(L"force",wds[2],L"N",e->Esail) );
    e->SetRelationOf(q,aunt|spawn,RXO_EXP_OF_ENT); //done
    if(! q->Resolve()) {
        e->CClear();
        return 0;
    }

    //   s  =wds[3].c_str();   /* length */  //    p->length  =  Evaluate_Quantity(e->Esail,s,"length");
    q= e->AddExpression ( new RXQuantity(L"length",wds[3],L"m",e->Esail)  );
    e->SetRelationOf(q,aunt|spawn,RXO_EXP_OF_ENT); //done
    if(! q->Resolve()) {
        e->CClear();
        return 0;
    }

    if(nw&1){ // we have atts
        atts = wds[nw-1];
        nw--;
    }
    p->c = nw/2 -2;
    if(p->c<2) {
        p->p = (VECTOR *)MALLOC(2*sizeof(VECTOR));
        p->p[0].x = (float) 0.0;
        p->p[0].y = (float) 0.0;
        p->p[0].z = (float) 0.0;
        p->p[0].x = (float) 100.0;
        p->p[0].y = (float) 0.0;
        p->p[0].z = (float) 0.0;
        p->c=2;
    }
    else{
        p->p = (VECTOR *)MALLOC((p->c)*sizeof(VECTOR));
        k = 4;
        for(int i =0;i<p->c;i++) {
            //    sx = wds[k++].c_str();
            //   sy = wds[k++].c_str();
            p->p[i].x = (float) Evaluate_Quantity(e->Esail,wds[k++],L"length");
            p->p[i].y = (float) Evaluate_Quantity(e->Esail, wds[k++],L"length");
            p->p[i].z = (float) 0.0;

        }
    }
    ON_3dVector l_z = e->Normal();
    PC_Normalise_Polyline(p->p,p->c,l_z );

    if(e->PC_Finish_Entity("batten",e->name(),p,(long)0,NULL,ToUtf8(atts).c_str(),e->GetLine())) {
        e->Needs_Resolving= 0;
        e->Needs_Finishing = 1;
        e->SetNeedsComputing(0);
        if(!e->generated)  e->SetNeedsComputing();	/* So we see all battens in library */
        retval=1;
    }
    return(retval);
}
int Resolve_Fabric_Card (RXEntity_p e) {
    /* fabric card is of type

   fabric: <name> :  <color> : (9 matrix elements) keyword 'wrinkle'  */
    char*dummy;

    const char*name, *text;
    char att[256]; *att=0;
    HC_KEY key=0;
    int i,j,k;
    string s;
    std::string sline(e->GetLine() );
    rxstriptrailing(sline);
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();
    if(nw<1)
        return 0;
    int retval=0;
    if (nw>1){
        name=wds[1].c_str ();
        if (nw>2){
            text=wds[2].c_str ();
            struct PC_MATERIAL *ptr = (PC_MATERIAL *)CALLOC(1,sizeof(struct PC_MATERIAL));

            if (ptr==NULL) {return(0);}
            k=3;
            for(i=0;i<3;i++) {
                for(j=0;j<3;j++) {
                    if(k<nw)
                        ptr->d[i][j]=strtod(wds[k].c_str(),&dummy);
                    else {
                        s="cannot parse "+ string( e->GetLine());
                        e->OutputToClient(s.c_str(),1); ptr->d[i][k]=0.0; }
                    k++;
                }
            }
              if(nw>k){
                s = wds[k];
                strcpy(att,s.c_str());
            }
            else
                s.clear();
              if(!rxIsEmpty(att))
                  ptr->creaseable= (NULL != stristr(att,"wrinkle") )  && !(NULL != stristr(att,"nowrinkle"));

            ptr->linear = 1;
            ptr->text=STRDUP(text);

#ifdef HOOPS
            int f=1;
            char s3[256];
            f = Valid_Hoops_Color(text);

            HC_Open_Segment("fabrics");
            key = HC_KOpen_Segment(name);
            if(!f) {
                sprintf(s3,"Fabric <%s> : Colour %s not defined" ,name,text);
                e->OutputToClient(s3,2);
                HC_Set_Color("faces=periwinkle");
            }
            else {
                sprintf(s3,"faces=%s",text);
                HC_Set_Color(s3);
            }
            HC_Close_Segment();
            HC_Close_Segment();
#endif

            if(e->PC_Finish_Entity("fabric",name,ptr,key,NULL,att,e->GetLine())) {
                e->SetNeedsComputing(0);
                e->Needs_Resolving=0;
                retval=1;
            }
            else
                cout<<"fab didnt finish"<<endl;

          //  ptr->creaseable=e->AttributeFind("wrinkle") &&!e->AttributeFind("nowrinkle");
        }
    }
    e->Needs_Resolving=(!retval);
    return(retval);
}

int PCTol_Set_Tolerances(SAIL *p_sail, double f,int flag) {
#ifdef HOOPS
    char str[256] ;
    char  seg[256];
#endif

    int type, where;
    HC_KEY key=0;

    type  = flag & (PCTOL_LINEAR + PCTOL_ANGULAR + PCTOL_CUT);
    where = flag & (PCTOL_GLOBAL + PCTOL_LOCAL);

    switch(where) { // there's a bug in the local method
    case PCTOL_GLOBAL:
    case PCTOL_LOCAL:
#ifdef HOOPS
        strcpy(seg,"/");
#endif
        break;
    case PCTOL_LOCAL*64:

        if(p_sail->GetGraphic()){
            key = p_sail->GetGraphic()->m_ModelSeg;
        }
#ifdef HOOPS
        strcpy(seg,".");

        if(key) {
            HC_Show_Key_Type(key,seg);
            if(strieq(seg,"segment"))
                HC_Show_Segment(key,seg);
        }
#endif
    }

    switch (type) {
    case PCTOL_LINEAR:
        g_SOLVE_TOLERANCE =  f;
        cout<< "Linear Tolerance  set to "<<f <<endl;
#ifdef HOOPS
        sprintf(str, "Linear_Tolerance=%f",f);
        HC_QSet_User_Options(seg,str);
#endif
        return PCTOL_LINEAR;
    case PCTOL_ANGULAR:
        g_ANGLE_TOL =  f;
        cout<< "Angle Tolerance  set to "<<f<<endl;
#ifdef HOOPS
        sprintf(str, "Angular_Tolerance=%f",f);
        HC_QSet_User_Options(seg,str);
#endif
        return PCTOL_ANGULAR;
    case PCTOL_CUT:
        g_Gauss_Cut_Dist =  f;
        cout<< "g_Gauss_Cut_Dist set to "<<f<<endl;
#ifdef HOOPS
        sprintf(str, "Cut_Distance=%f",f);
        HC_QSet_User_Options(seg,str);
#endif
        return PCTOL_CUT;
    default:
        assert(0);
    }
    if(p_sail) {
        p_sail->m_Linear_Tol= g_SOLVE_TOLERANCE;
        p_sail->m_Angle_Tol = g_ANGLE_TOL;
        p_sail->m_Cut_Dist = g_Gauss_Cut_Dist;
    }
    return type;
}

int PCTol_Update_Tolerances(SAIL *sail) { // from net UOs to globals
    char buf[256];  *buf=0;
    RXEntity *zzz=0;
    HC_Show_One_Net_User_Option( "Linear_Tolerance", buf);
    if(!rxIsEmpty(buf)) {
        while(zzz=sail->Entity_Exists("Linear_Tolerance","set"))
            zzz->Kill();
    }
    HC_Show_One_Net_User_Option( "Angular_Tolerance", buf);
    if(!rxIsEmpty(buf)) {
        while(zzz=sail->Entity_Exists("Angular_Tolerance","set"))
            zzz->Kill();
    }
    HC_Show_One_Net_User_Option( "Cut_Distance", buf);
    if(!rxIsEmpty(buf)) {

        while(zzz= sail->Entity_Exists("Cut_Distance","set"))
            zzz->Kill();
    }
    return 1;
}
int Resolve_Set_Card (RXEntity_p e) {
    /* Set card is of type
   set:  variable : value [: <nodal address>]       ! address can be ?sail(or '.'), global (or '/'). default is '.'
   set: varname : value : type:name
*/

    RXExpressionI *le;
    RXENode * thelist=0;
    double x;
    QString  address;
    e->SetLineLwr();
    QString qline = e->GetLine();
    QStringList wds= rxParseLine(qline,RXENTITYSEPREGEXP );

    int nw = wds.size();
    if(nw<3)
        return 0;

    QString &name =wds[1];
    QString v =wds[2];
    if (nw>3)
        address =wds[3];
    else
        address=".";
    {
        if(address=="?sail")  		thelist = e->Esail;
        else if(address=="/") 		thelist = g_World;
        else if(address=="global") 		thelist = g_World;
        else if(address==".") 		thelist =  e->Esail;
        else if (nw>4 && INTEGER_TYPE(qPrintable (address)))
        {
            RXEntity_p ee = e->Esail->Get_Key_With_Reporting(qPrintable (address), qPrintable (wds[4]),true );
            if(!ee){
                e->CClear();
                return 0;
            }
            thelist=ee;
        }
        else {
            printf("in SET <%s> dont know about address <%s>\n", e->GetLine(),qPrintable (address));
            thelist=0;
        }
        // if this already has a spawn expression, just change it
        // else insert it

        if(thelist) {
            RXObject*old =e->GetOneRelativeByIndex(RXO_EXP_OF_SET);
            le = dynamic_cast<RXExpressionI *>(old);
            if(le){
                le->Change(v.toStdWString());
                x=le->evaluate ();
                cout<<"now evaluates to "<<x<<endl;
            }
            else{
                le = thelist ->AddExpression(name,v,e->Esail);
                e->SetRelationOf(dynamic_cast<RXObject*>(le),child|niece|spawn,RXO_EXP_OF_SET);
                // feb 2014 changing the exp didnt propagate   across models.
                if(le->ResolveExp ())
                    x=le->evaluate ();
                else {
                    cout<< " cant resolve this expression"<<endl;
                    delete le;
                    return 0;
                    //TODO add 'set','name' to the unknowns
                }
            }
            e->SetDataPtr(le);
        } // thelist
    }//else
    e->Needs_Resolving= 0;
    return(1);
}

int Resolve_Pansail_Card(RXEntity_p p_e) {
    /* Pansail card is of type

   Pansail : leading eDGe : trailing eDGe: basename : [atts]
   NOTE : the word 'reversed' is reserved.

    4/8/95
    To enable scaling wakes, we change the syntax as follows

  Pansail :[namepansail (verbatim) :] leading eDGe : trailing eDGe: basename [:atts [: <edgfile entity>]]

    The only name allowed is "namepansail" MAY 2003 relaxed this requirement.
    The last 2 fields are optional
    the atts may contain (vmin= vmax= umin=,umax=)

    IF it doesnt exist, the file <basename.edg> must exist and is used verbatim
    If the last field does exist, it must refer to an entity of type 'wake'
     and then the file  <basename.edg> will be over-written.

    Either way, the file <basename>.str must exist if streamlines are required.

    The wake entity contains references to SCs, and to the AWA
    SO it should be resolved at Resolve time
    AND it should be computed each time PANSAIL is run.


   */
    class RXEntityDefault*e = dynamic_cast<class RXEntityDefault*> (p_e);
    int retval=0;
    char  lflag[256],tflag[256];
    char buf[512];
    size_t a,lp;
    int has_name;
    struct PCN_PANSAIL *p = (PCN_PANSAIL *)CALLOC(1,sizeof(struct PCN_PANSAIL));
    string n4;
    const char*atts="none";// *name,
    std::string sline(e->GetLine() );
    rxstriptrailing(sline);
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();
    if(nw<1)
        return 0;

    if ( nw>1 ){ string &n1=wds[1] ;
        if (  nw>  2 ){string &n2=wds[2] ;
            if ( nw>  3 ){string &n3=wds[3] ;
                if( nw>  4 ){
                    n4=wds[4] ;
                    has_name = 1;
                }
                else
                    has_name = 0;
                if(nw>5) atts=wds[5].c_str();

                if(!has_name) {
                    n4=n3 ;
                    n3=n2 ;
                    n2=n1 ;
                    cout<<" TODO step no HASNAME"<<endl;
                }

                if (p==NULL) {e->OutputToClient("out of memory",2); return(0);}

                a=n2.find("reversed");
                strcpy(lflag,"");
                strcpy(tflag,"");

                if(a !=string::npos) {
                    n2.erase(a);
                    rxstriptrailing(n2);
                    p->lrev=1;
                    strcpy(lflag,"reversed");
                }

                a=n3.find("reversed");
                if(a!=string::npos) {
                    n3.erase(a);
                    rxstriptrailing(n3);
                    p->trev=1;
                    strcpy(tflag,"reversed");
                }

                lp=n4.find(".");
                if(lp!=string::npos && (lp>n4.length()-5) )
                    n4.erase(lp);
                n4+=".edg";

                p->leading = e->Esail->Get_Key_With_Reporting("seamcurve,compound curve",n2.c_str ());
                p->trailing= e->Esail->Get_Key_With_Reporting("seamcurve,compound curve",n3.c_str ());
                if(!p->leading || !p->trailing) { RXFREE(p); return(0);}
#ifdef USE_NIECES
                Push_Entity(e,&p->leading->NIECES);
                Push_Entity(p->leading,&e->aunts);
                Push_Entity(e,&p->trailing->NIECES);
                Push_Entity(p->trailing,&e->aunts);

#endif
                p->leading->SetRelationOf(e,child|niece,RXO_LDG_OF_AERO);


                p->trailing->SetRelationOf(e,child|niece, RXO_TLG_OF_AERO);

                char n4c[512]; strcpy(n4c,n4.c_str());
                if(!VerifyFileName (n4c," Please select an edg file","*.edg"))	 {   // wont work on MSW because of the colon
                    RXFREE(p);
                    return(0);
                }
                n4=n4c;
                /*strip the .edg from n4	 */
                lp=n4.find(".edg");
                if(lp !=string::npos){
                    n4.erase(lp);
                }
                if(p->basename) RXFREE(p->basename);
                p->basename = STRDUP(n4.c_str());
                e->SetDataPtr( p);
                RXAttributes rxa(atts);
                if(!rxa.Extract_Double("vmin",&(p->Lower_V))) p->Lower_V=0.0;
                if(!rxa.Extract_Double("vmax",&(p->Upper_V))) p->Upper_V=1.0;
                if(!rxa.Extract_Double("umin",&(p->Lower_U))) p->Lower_U=0.0;
                if(!rxa.Extract_Double("umax",&(p->Upper_U))) p->Upper_U=1.0;
                /* re-construct the input line */

                sprintf(buf,"pansail:%s :%s%s :%s%s :%s :%s",e->name(),p->leading->name(),lflag,p->trailing->name(),tflag,p->basename,atts);

                if(!strieq(buf,e->GetLine())) {
                    //	printf(" pansail object line\nwas<%s>\n is<%s>\n", e->line, buf);
                    e->SetLine(buf);
                    e->edited=1;
                }
                e->AttributeDestroy();
                e->AttributeAdd(atts);
                e->SetNeedsComputing();
                e->Needs_Resolving= 0;
                retval = 1;
            }
        }
    }
    if(retval) e->Esail ->m_Selected_Entity[PANSAIL]=e;
    return(retval);
}  

int Resolve_Rename_Card(RXEntity_p e) {
    /* rename card is of type
   rename : <name> : type : name1: name2:
   Causes PC_Get_Key to return the key of <type,n1>
   when called with <type,n2>

   */
    std::string sline( e->GetLine() );
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    //int nw = wds.size();
    int retval = 0;
    char name[256],type[256],n1[256],n2[256];
    struct PC_ALIAS *p;
    RXEntity_p ep;
    if(!e)  {     e->OutputToClient(" resolve rename on NULl entity",5); }
    //const char*line = e->GetLine();
    
    if (rxgetword(wds,1,name ,256)){
        Make_Valid_Segname(name);
        if (rxgetword(wds,2,type ,256)){
            if (rxgetword(wds,3,n1 ,256)){
                if (rxgetword(wds,4,n2 ,256)){
                    ep=e->Esail->GetKeyWithAlias( type,n1);
                    if(!ep)
                        return(0);

                    p = (PC_ALIAS *)CALLOC(1,sizeof(struct PC_ALIAS));

                    p->type=STRDUP(type);
                    p->p=ep;
                    p->name=STRDUP(n2);

                    if(! g_Janet && (strieq(type,"relsite") ||strieq(type,"site"))) {
                        if( ep->hoopskey) {
                            char htype[256];
                            Site*s= (Site *)ep;
#ifdef HOOPS
                            HC_Show_Key_Type(ep->hoopskey,htype);
                            if(strieq(htype,"segment")) {
                                HC_Open_Segment_By_Key(ep->hoopskey);
                                HC_Flush_Contents(".","text");
                                ON_3dPoint d = s->GetLastKnownGood ( RX_MODELSPACE);
                                HC_Insert_Text(d.x,d.y,d.z,n2);
                                HC_Close_Segment();
                            }
                            else
                                e->OutputToClient(htype,1);
#endif
                        }
                    }

                    /* akm mod 6 sept 95 */
                    // this changes the name of the alias card to a standard one

                    sprintf(name,"%s(%s)",p->name,p->type); /* force a particular format of name eg. tack(site) */
                    e->Rename(name,"alias");

                    e->SetDataPtr( p);
                    e->SetNeedsComputing(0);
                    e->Needs_Resolving=0;
                    retval = 1;

                }
            }
        }
    }
    return(retval);
}



