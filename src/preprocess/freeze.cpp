// freeze.cpp  the commands to manage frozen post-processing.

// begun 3 Jan 2005  PH

/* need to think thru the logic

  the command to RXFREEZE a sail is executive. 
	  It applies to all viewports because that's easier.
	  It records a copy of the tri stresses in SAIL->m_StressResults,
	  first freeing any previous list. 

	  It sets the FL_ISFROZEN flag on the SAIL
	  It sets all the freeze tickboxes referring to the sail.

  The command to UNRXFREEZE clears the flag, keeping the old data.

  Then GetShell (or whatever does the update view) has to:
	1) Whenever it gets Tri Results and FL_ISFROZEN, it subtracts the stored results.
	2) It adds some text somewhere saying (sd->isail->GetType().c_str()) frozen at run (runName)

  In terms of UI.
  Setting the Freeze tickbox sets it for all freeze tickboxes referring to that sail,
  courtesy of PCP_Freeze


  */
#include "StdAfx.h"
#include "RXSail.h"
#include "script.h"
#include "global_declarations.h"
//#include "getshell.h"

#include "freeze.h"

// see asgraph_value_changed 
#ifdef _X
XtCallbackProc freeze_value_changed_CB	(Widget p_w,XtPointer client_data, XmToggleButtonCallbackStruct* call_data) { 

struct FRAMEDATA *l_fd;
int i;
SAIL * sail=NULL;
int  state; 

l_fd = (struct FRAMEDATA*) client_data;

state = call_data->set;

	for(i=0;i<l_fd->m_nframes;i++) {
		if(l_fd->m_frames[i]->m_freeze == p_w) { /* its this graph button */
			sail = l_fd->m_frames[i]->sail;
			break;
		}
	}

	cout << sail->GetType() <<endl;
	if(state) Execute_Script_Line("Freeze :", sail->Graphic() );
	else Execute_Script_Line("UnFreeze :" ,sail->Graphic());

return 0; // nov 2008 icc
}

#endif

EXTERN_C int PCP_Freeze(SAIL* p_sail) {

    {
        if( !p_sail)
            return 0;
        int flags=0;
         return  p_sail-> FreezeStrings(flags);
    }

// make a record of the triStressResults into p_sail->m_StressResults

	if(p_sail->m_StressResults)
		RXFREE(p_sail->m_StressResults);
	p_sail->m_StressResults=NULL;
	assert(!p_sail->GetFlag(FL_ISFROZEN));	
#ifdef _X
	assert(" RXFREEZE"==0); // 	Get_Triangle_Results(p_sail,&(p_sail->m_StressResults));
#endif	
	
p_sail->SetFlag(FL_ISFROZEN,true);
	
// set all of this sail's 'Freeze' checkboxes.
Set_All_FreezeButtons(p_sail, 1);



return 1;
}
EXTERN_C int PCP_UnFreeze(SAIL* p_sail) {
// clear the sail's FROZEN flag.  but leave it's stressdata

	p_sail->SetFlag(FL_ISFROZEN,false);
// Clear all of this sail's 'Freeze' checkboxes.
	Set_All_FreezeButtons(p_sail, 0);
	
return 1;
}
int Set_All_FreezeButtons(const SAIL* p_sail, const int p_stat) {
#ifdef _X
	int i,j;
	Graphic *g;
	FrameData * fd;

	Arg al[64]; int l_ac=0;
	for(j=0;j<g_hoops_count;j++)  {
		g = &g_graph[j]; 
		if(g->GType == WT_VIEWWINDOW && g->options_menu && !(g->EMPTY) && g->m_fd) {
			fd = (FrameData *) g->m_fd;
			for(i=0;i<fd->m_nframes;i++) {
			
				Frame *l_f = fd->m_frames[i];
				if(l_f->sail== p_sail) {
					Widget theButton = l_f->m_freeze;
					l_ac=0; XtSetArg(al[l_ac], XmNset, p_stat); l_ac++;
					XtSetValues (theButton,al, l_ac );
				}
			}
			
					
		}		
	}

#endif
	return 1;
}
