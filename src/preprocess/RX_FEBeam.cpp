#include "StdAfx.h"
#include <QDebug>
#include "RXSail.h"
#include "RXRelationDefs.h"
#include "RXExpression.h"
#include "words.h"
#include "pctable.h"
#include "etypes.h"
#include "summary.h"
#include "stringutils.h"
#include "f90_to_c.h"
#include "nlm.h"
#include "RX_FESite.h"
#include "RXAttributes.h"
#include "RX_FEBeam.h"

///		keyword "beamstring"    TYPE	PCE_BEAMSTRING
///		keyword "beammaterial" 	 TYPE	PCE_BEAMMATERIAL
RX_FEBeam::RX_FEBeam(void)
{
    assert(0); // dont use this constructor
}

RX_FEBeam::RX_FEBeam(SAIL *s)
    :RX_FEString(s)
{

}
RX_FEBeam::~RX_FEBeam(void)
{
    int err=0, sli,n; sli=this->m_sail->SailListIndex ();
    std::vector<int>::const_iterator it;
    for(it= m_ElementNos.begin();it!=this->m_ElementNos .end(); ++it)  {
        n=*it ;
        cf_removebeamelement(sli,n,&err);
    }
}
int RX_FEBeam ::CClear(){
    int rc=0;  // shouldnt we do a ReWriteLine();// so the string Trim is preserved on a re-resolve
    // clear stuff belonging to this object
    ClearFEA(); // this destroys the fortran object
    // call base class CClear();

    rc+=RX_FEString::CClear ();
    return rc;
}

// removes the string from the FEA database
int RX_FEBeam::ClearFEA(void)
{
    int err=0, sli,n; sli=this->m_sail->SailListIndex ();
    if(IsInDatabase()) {

        std::vector<int>::const_iterator it;
        for(it= m_ElementNos.begin();it!=this->m_ElementNos .end(); ++it)  {
            n=*it ;
            cf_removebeamelement(sli,n,&err);

            if(err)
                cout<<"(RX_FEBeam::ClearFEA) "<<name()<<"  ("<< GetN()<<") err = "<<err<<endl;
        }
        SetIsInDatabase(false);
        m_FPse=0; // pointer to the fortran array holding SE
        m_FPrev=0; // pointer to the fortran array
        m_FPtq=0;
    }
    return(!err);
}

int RX_FEBeam::Resolve( RXEntity_p sce)
{
    char buf[256];RXEntity_p ee ;
    int rc = RX_FEString::Resolve(sce);
    this->Rename (this->name(), "beamstring");
    this->TYPE =PCE_BEAMSTRING;
    if(! rc) return 0;
    if( AttributeGet("$beam",buf) ){
        ee = this->Esail ->Get_Key_With_Reporting("beammaterial",buf,true);
        if(ee){
            this->SetRelationOf(ee, parent|aunt,RXO_MAT_OF_BEAM );
            ee->SetNeedsComputing();
        }
        else {
            this->Needs_Resolving=1; // WHY NOT do a CCLear()????????????
            return 0;
        }
    }

    return 1;
}
int RX_FEBeam::Dump(FILE *fp) const
{
    fprintf(fp,"FE Beam \n");
    std::vector<int>::const_iterator it;
    for(it= m_ElementNos.begin();it!=this->m_ElementNos .end(); ++it)  {
        fprintf(fp, "  elno  %d\n", *it);
    }

    return 1;//
}
int RX_FEBeam::Compute(void)
{
    int rc= RX_FEString::Compute();
    if(rc)
        this->SetNeedsMeshUpdate();
    return rc;
}
int RX_FEBeam::Finish(void)
{
    int rc=RX_FEString::Finish();
    this->SetNeedsComputing();
    return rc;
}
int RX_FEBeam::ReWriteLine(void)
{
    cout<<" RX_FEBeam::ReWriteLine  not implemented "<<endl;
    return 0;
}
// collects the pside list and the attri butes into the P structure. Its OK to free and reallocate if we follow by an AddFString
int RX_FEBeam::PostMesh(void)
{	
    // insert a beam element for each FEdge and ensure that all the nodes are Six-Nodes.
    // Get the properties by interpolating the beamMaterial.

    d.Ne   = 0;
    d.m_Slide = 0;		d.m_CanBuckle3=RXS_NEVERBUCKLE;		d.m_TConst  = 0;

    m_elist.clear(); m_L_FromEdges=0;
    d.Ne=CountPsideEdges(&m_L_FromEdges );

    int count = SortPsideEdges();
    this->MakeSixNodes();
    if(d.Ne !=count) {printf(" Sort removed some %d %d\n", d.Ne,count); d.Ne =count;}

    if(!m_Text)
        m_Text=(char*)calloc(256,1);
    memset(m_Text,' ',256);
    strcpy(m_Text,"my own text");
    sprintf( m_Text,"%s,%s",GetSail()->GetType().c_str(),this->name() ); 			PC_Strip_Trailing(m_Text);
    AddFEA(d.Ne); // ought to be safe multiple times. Generates the eles
    d.m_zi_LessTrim = m_L_FromEdges;
    d.m_Scptr=this;

    return d.Ne;
}

int RX_FEBeam::MakeSixNodes(void) 
{
    // the list of edges is not necessarily contiguous, nor are they all pointed the right way.
    // this method is ony good when we dont have a seed for the start end.

    vector<FEEdgeRef>::iterator i;
    FEEdgeRef r;
    int nn, c=0;
    if(! m_elist.size()) return 0;

    for(i=m_elist.begin();i!= m_elist.end(); i++) {
        r = *i;
        c+=r.e->GetNode(0)->MakeSixNode();
        c+=r.e->GetNode(1)->MakeSixNode();
    }

    return c;
}

int RX_FEBeam::PostToMirror() // this is really heavy on the Database. We have 16 entries per instance
{
    int rc=0;
    QString header,val;

    if(!this->AttributeFind("post"))
        return 0;
    int k, c=0,start;
    int sli=this->Esail->SailListIndex();

    double f[10],ftot[10], fsq[10];
    double fmax[10],fmin[10];
    memset(ftot,0,10*sizeof(double));
    memset( fsq,0,10*sizeof(double));
    memset(fmax,0,10*sizeof(double));
    memset(fmin,0,10*sizeof(double));
    std::vector<int>  ee = this->Elements() ;
    std::vector<int>::const_iterator ii;
    start=1;
    for(ii=ee.begin(); ii !=ee.end(); ++ii) {
        if( cf_one_beamelementresult(sli, *ii, f)  )
        {
            f[0]-= f[1];f[0]/=2.;  f[1]=0; // f[0] now mean M3 Minus because the moments have opposite signs
            f[2]-= f[3];f[2]/=2.;  f[3]=0; // f[2] now mean M2
            for(k=0;k<10;k++)
            {
                ftot[k] += f[k];
                fsq[k] += f[k]*f[k];
                if(start) {
                    fmax[k]=fmin[k]=f[k];
                }
                else
                {
                    fmax[k]=max(fmax[k],f[k]);
                    fmin[k]=min(fmin[k],f[k]);
                }
            }
            c=c+1;
            start=0;
        }
    }
    if(c<1)
        return 0;
    if(c>1) {
    // SD is 1/(N- 1)  * ( sumofsquares - (squareOfSum/N)
    QStringList w; w<<"$M3$"<<""<<"$M2$"<<""<<"$Torque$"<<"$Axial$";
    double sd, nn=(double) c;
    for(k=0;k<6;k++)
    {
        if(w.at(k).isEmpty())
            continue;
        header=QString("beam$")+this->name()+w.at(k)+QString("_Max");
        val=QString("%1").arg(fmax[k]);
        Post_Summary_By_Sail(this->Esail,qPrintable (header)  ,qPrintable(val));

        header=QString("beam$")+this->name()+w.at(k)+QString("_Min");
        val=QString("%1").arg(fmin[k]);
        Post_Summary_By_Sail(this->Esail,qPrintable (header)  ,qPrintable(val));

        header=QString("beam$")+this->name()+w.at(k)+QString("_Mean");
        val=QString("%1").arg(ftot[k]/nn);
        Post_Summary_By_Sail(this->Esail,qPrintable (header)  ,qPrintable(val));
        if(c<2)
            continue;
        header=QString("beam$")+this->name()+w.at(k)+QString("_SD");
        sd = ( fsq[k] - pow(ftot[k],2)/nn)/(nn-1.); sd=sqrt(sd);
        val=QString("%1").arg(sd);
        Post_Summary_By_Sail(this->Esail,qPrintable (header)  ,qPrintable(val));

        rc++;
    }
}

    else if(c==1)
    {
        QStringList w; w<<"$M3"<<""<<"$M2"<<""<<"$Torque"<<"$Axial";
        for(k=0;k<6;k++)
        {
            if(w.at(k).isEmpty())
                continue;
            header=QString("beam$")+this->name()+w.at(k);
            val=QString("%1").arg(fmax[k]);
            Post_Summary_By_Sail(this->Esail,qPrintable (header)  ,qPrintable(val));
            rc++;
        }
    }

    return rc;
}





// places the elements into the FEA database. Ensures that all beam end nodes are six-nodes
int RX_FEBeam::AddFEA(const int n_edges)
{
    int err=0, NN=-1;
    FEEdgeRef r;
    int n1, n2,is_WHATONEARTH;
    double f,t, beta, zi,ti, mass,grad;
    double emp[5]; //ea, ei2,ei3,gj,ti
    int axLoadFlag =0;
 //!axload >0 = tension only. <0 = compression only
//    noCompForce"
//    noTensileForce"


    char name[64];
    double s[3],e[3]; ON_3dPoint p; int kk;
    class RXBeamMaterial *m = dynamic_cast<class RXBeamMaterial *>(this->GetOneRelativeByIndex(RXO_MAT_OF_BEAM));
    if(!m) return 0;
    struct PC_TABLE*tbl = m->m_btable;
    int ncols = tbl->nc-1;
    SAIL * sail = GetSail();
    mass = this->d.m_Mass;
    strcpy(name,this->m_Text );
    if(this->AttributeFind("$nocompforce")) axLoadFlag = 1;
     if(this->AttributeFind("$notensileforce")) axLoadFlag = -1;

    ti=0.0;  emp[0]= 10000; emp[01]= 1000; emp[2]= 500; emp[3]= 400; emp[4]= ti;
    // for each edge
#ifdef FORTRANLINKED 
    if(!IsInDatabase()) { //here we add the fortran beam elements
        int j, nels = this->d.Ne;
        if(nels>0){
            t=0.;
            for(int i=0;i<nels;i++)  {
                r = this->m_elist [i];
                zi =  r.e->GetLength();
                t = t+ zi/2.0;
                f = t/this->m_L_FromEdges;
                Polyline_Interp(m->m_bvals [RXB_s ],m->m_bvals [RXB_eixx],&ncols,&is_WHATONEARTH,&f ,&(emp[1]) ,&grad,&err);
                Polyline_Interp(m->m_bvals [RXB_s ],m->m_bvals [RXB_eiyy],&ncols,&is_WHATONEARTH,&f ,&(emp[2]) ,&grad,&err);
                Polyline_Interp(m->m_bvals [RXB_s ],m->m_bvals [RXB_gj  ],&ncols,&is_WHATONEARTH,&f ,&(emp[3]) ,&grad,&err);
                Polyline_Interp(m->m_bvals [RXB_s ],m->m_bvals [RXB_ea  ],&ncols,&is_WHATONEARTH,&f ,&(emp[0]) ,&grad,&err);
                Polyline_Interp(m->m_bvals [RXB_s ],m->m_bvals [RXB_beta],&ncols,&is_WHATONEARTH,&f , &beta    ,&grad,&err);
                if(m->m_bvals.size()>RXB_ti)
                    Polyline_Interp(m->m_bvals[RXB_s],m->m_bvals[RXB_ti],&ncols,&is_WHATONEARTH,&f ,&(emp[4]),&grad,&err);
                ti = emp[4];

                n1= r.e->GetNode(0) ->GetN(); n2= r.e->GetNode(1) ->GetN();

                p = r.e->GetNode(0)->ToONPoint(); for(kk=0;kk<3;kk++) s[kk]=p[kk];
                p = r.e->GetNode(1)->ToONPoint(); for(kk=0;kk<3;kk++) e[kk]=p[kk];
                j= cf_createbeamelement( sail->SailListIndex(), n1, n2,
                                         beta, zi,ti, mass,
                                         emp, axLoadFlag,
                                         name, 64,
                                         &err); // returns the index (ie from nextfree
                if(err) { cout<< " Create beam error"<<endl; _rxthrow( " Create beam error");}
                kk = cf_one_initial_beamaxismatrix(sail->SailListIndex(),j,s,e);
                this->m_ElementNos.push_back(j);
                t = t+ zi/2.0;
            }
        }
        SetIsInDatabase();
    }
    else  // modify it
    {
        int j, nels = this->d.Ne;
        if(nels>0){
            t=0.;
            for(int i=0;i<nels;i++)  {
                r = this->m_elist [i];
                zi =  r.e->GetLength();
                t = t+ zi/2.0;
                f = t/this->m_L_FromEdges;
                Polyline_Interp(m->m_bvals [RXB_s ],m->m_bvals [RXB_eixx],&ncols,&is_WHATONEARTH,&f ,&(emp[1]) ,&grad,&err);
                Polyline_Interp(m->m_bvals [RXB_s ],m->m_bvals [RXB_eiyy],&ncols,&is_WHATONEARTH,&f ,&(emp[2]) ,&grad,&err);
                Polyline_Interp(m->m_bvals [RXB_s ],m->m_bvals [RXB_gj  ],&ncols,&is_WHATONEARTH,&f ,&(emp[3]) ,&grad,&err);
                Polyline_Interp(m->m_bvals [RXB_s ],m->m_bvals [RXB_ea  ],&ncols,&is_WHATONEARTH,&f ,&(emp[0]) ,&grad,&err);
                Polyline_Interp(m->m_bvals [RXB_s ],m->m_bvals [RXB_beta],&ncols,&is_WHATONEARTH,&f , &beta    ,&grad,&err);
                if(m->m_bvals.size()>RXB_ti)
                    Polyline_Interp(m->m_bvals[RXB_s],m->m_bvals[RXB_ti],&ncols,&is_WHATONEARTH,&f ,&(emp[4]),&grad,&err);
                ti = emp[4];

                j =this->m_ElementNos [i];
                j= cf_changebeamelement( sail->SailListIndex(),j,
                                         beta, zi,ti, mass,
                                         emp, axLoadFlag,
                                         name, 64,
                                         &err); // returns the index (ie from nextfree
                if(err) { cout<< " Change beam error"<<endl; _rxthrow( " Create beam error");}

                t = t+ zi/2.0;
            }
        }
    }
#else
    assert(0);
#endif
    return NN;
}







RXBeamMaterial::RXBeamMaterial(void)
    : m_btable(NULL)
    , m_whichway(0)
{
}
RXBeamMaterial::RXBeamMaterial(SAIL *s)
    :RXEntity(s) ,
      m_btable(NULL)
{
}

RXBeamMaterial::~RXBeamMaterial(void)
{
    if(m_btable)
        PCT_Free_Table( m_btable);
    for(std::vector<double*>::iterator it=  m_bvals.begin(); it!=m_bvals.end();++it ){
        double*p = *it;
        if(p) RXFREE(p);
    }
    m_bvals.clear();
}
int RXBeamMaterial::Dump(FILE *fp) const
{
    int rc=0;

    if(! this->m_btable ) return rc;
    PCT_Print_Table(fp,this->m_btable);
    fprintf(fp," mvals size= %d\n",(int) m_bvals.size());

    for(int r=0; r<this->m_bvals.size(); r++)
    {
        //  double*p = this->m_vals[r];
        //for(int c=0; c<min(this->m_table->nc,this->m_table->nr ); c++,p++){
        //	rc+=fprintf(fp,"\t%f", *p);
        //}
        // this may not give a full print but its safe.
        rc+=fprintf(fp,"\n");
    }
    return rc;
}
int RXBeamMaterial::Evaluate(const double fin, double emp[6])
{

    int rc=0;
    double f=fin;
    struct PC_TABLE*tbl = this->m_btable;
    int is,err=0, ncols = tbl->nc-1;
    double grad;
    Polyline_Interp(m_bvals [RXB_s ],m_bvals [RXB_eixx],&ncols,&is,&f ,&(emp[1]) ,&grad,&err);
    Polyline_Interp(m_bvals [RXB_s ],m_bvals [RXB_eiyy],&ncols,&is,&f ,&(emp[2]) ,&grad,&err);
    Polyline_Interp(m_bvals [RXB_s ],m_bvals [RXB_gj  ],&ncols,&is,&f ,&(emp[3]) ,&grad,&err);
    Polyline_Interp(m_bvals [RXB_s ],m_bvals [RXB_ea  ],&ncols,&is,&f ,&(emp[0]) ,&grad,&err);
    Polyline_Interp(m_bvals [RXB_s ],m_bvals [RXB_beta],&ncols,&is,&f ,&(emp[5]) ,&grad,&err);
    if(m_bvals.size()>RXB_ti)
        Polyline_Interp(m_bvals[RXB_s],m_bvals[RXB_ti],&ncols,&is,&f ,&(emp[4]),&grad,&err);
    else
        emp[4]=0;

    return rc;
}

int RXBeamMaterial::Compute(void) {

    int rc=0;
    if(!this->NeedsComputing()) return(1);
    rc = this->Translate();

    this->SetNeedsComputing(0); // looks WRONG
    return rc;
}
int  RXBeamMaterial::Translate(void) // return 1 if OK.
{
    int i,ok,rc=1;
    char *Str;
    char**List=NULL;
    int N ;
    const char*headers[]={"s","eixx","eiyy","gj","ea","beta","ti"};
    int nh=7;
    const char *t; char *tabletext;

    t = strstr(this->GetLine(),"$table");
    if(t) t+=6;
    else	{  return(0);}
    tabletext=(char *)MALLOC((strlen(this->GetLine())+1) *sizeof(char));

    strcpy(tabletext,t);
    if(!PCT_Read_Table_From_Text(tabletext,&(this->m_btable))) {
        size_t n = strlen(this->name()) + strlen(tabletext) +64;
        Str = (char *)MALLOC( n);
        sprintf(Str,"in %s\n cant convert table \n<%s>   ",this->name(),tabletext);
        rxerror(Str,2);
        RXFREE(Str);
    }
    else {
        m_whichway=PCT_Which_Way(this->m_btable,headers,nh,this); // 0 gives trouble
        if(!m_whichway)
        {
            qDebug()<<" BAD table in "<<this->name();
        }
        this->m_bvals.resize(nh);
        // we fill in the six

        for(i=0;i<nh;i++) {
            if(PCT_Extract_Series(this->m_btable,m_whichway,headers[i],&N,&List)){ /* MALLOCs s */
                if (N ==1){  /* special for degenerate lists */
                    List = (char **)REALLOC(List,2*sizeof(char*));
                    List[1]=STRDUP(List[0]); 	N = 2;
                }
                ok= PCT_ListToDoubleByQ (this->Esail , &List,N,&(m_bvals[i]),L""); // frees List !!// returns 0 if bad
                //  ok= 1;  PCT_List_To_Double (this->Esail , &List,N,&(m_bvals[i]),""); // frees List !!
                if(!ok)
                    rc=0;
            }
        }
    }
    if (tabletext) RXFREE(tabletext);

    return(rc);
}
int RXBeamMaterial::Resolve(void)
{
    /*
     beammaterial: <name> :  atts : keyword '$table'
     [ a table] */
    const char*name,*atts,*s;
    const char*lp=0;

    int ok, retval=0;
    std::string sline(this->GetLine() );
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();
    if(nw>3) {
        name=wds[1].c_str();
        atts=wds[2].c_str();
        s   =wds[3].c_str();

        lp=strstr(s,"$table");
        if(lp) {
            lp+=6;
            this->m_TableText = TOSTRING(lp);
        }
        else
            cout<<"no '$table keyword in <"<<this->GetLine()<<">"<<endl;

        ok= Translate() ;// return 1 if OK.
        if(!ok)
        {
            this->CClear();
            return 0;

        }
        if(this->PC_Finish_Entity("beammaterial",name,NULL,0,NULL,atts,this->GetLine())) {
            this->SetNeedsComputing(0);	/* sb 0 if its set on GKwR */
            this->Needs_Finishing=1;
            retval=1;
        }
    }
    this->Needs_Resolving=(!retval);
    return(retval);
} 
int RXBeamMaterial::ReWriteLine(void)
{
    QStringList newline;
    newline<<type()<<  name();

    newline <<this->AttributeString();
    newline <<"$table ";
    newline <<QString::fromStdWString(this->m_TableText) ;

   this->SetLine(qPrintable( newline.join(RXENTITYSEPSTOWRITE) ));
    return 1;
}

int RXBeamMaterial::Finish(void)
{
    this->Needs_Finishing=0;
    this->SetNeedsComputing();
    return 1;
}
