// RXPointOnCurve.h: interface for the RXPointOnCurve class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RXPOINTONCURVE_H__34B6764A_ABBD_4907_B5C3_4BDEF35DCD64__INCLUDED_)
#define AFX_RXPOINTONCURVE_H__34B6764A_ABBD_4907_B5C3_4BDEF35DCD64__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define RXPOINTONCURVE_OK 1
#define RXPOINTONCURVE_FAILLED 0
#include "rxON_Extensions.h"

class RXPointOnCurve  
{
public:
	int IsValid() const;
	int Set(const double &p_t, const ON_Curve * p_crv);
	double GetLength(double fractional_tolerance = 1.0e-8) const;
	ON_Interval Domain() const;
	//!Constructor
	RXPointOnCurve();
	//!Constructor by copy
	RXPointOnCurve(const RXPointOnCurve & p_Obj);

	//!Destructor
	virtual ~RXPointOnCurve();
	void Init();
	int Clear();

	RXPointOnCurve& operator = (const RXPointOnCurve & p_Obj);

	double Gett() const;
	
	ON_3dPoint GetPos() const;
	ON_3dVector GetTangent() const;
	
	int SetToMin();
	int SetToMid();
	int SetToMax();
	int Print(FILE *fp) const;
protected:
	//!curve where the point is located on Peter WHY a NURBS curve
//	ON_NurbsCurve * m_curve;
	ON_NurbsCurve m_Ncurve;

// Peter Nov 27  shouldnt this be a reference to an RX_Curve???
	// or to an ON_Curve  
//		ON_Curve&  m_rONcurve;

	//!position on the curve  PETER  Is this a parameter or a length in metres?????
	double m_t;

};

#endif // !defined(AFX_RXPOINTONCURVE_H__34B6764A_ABBD_4907_B5C3_4BDEF35DCD64__INCLUDED_)
