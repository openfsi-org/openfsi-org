
#include "StdAfx.h"
#include <QDebug>
#include <QString>
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RX_FESite.h"
#include "RX_FEEdge.h"
#include "RXSeamcurve.h"
#include "RXPside.h"
#include "RXAttributes.h"
#include "RXSRelSite.h"
#include "RXRelationDefs.h"
#include "RXQuantity.h"
#include "RXPside.h" 
#include "summary.h"
#include "words.h"
#include "etypes.h"
#include "stringutils.h"

#include "entities.h"
#include "readstring.h"
#include "f90_to_c.h"
#include "global_declarations.h"  // for g_Janet
#ifdef _X
#inc lude "trim menu.h"
#inc lude "RXExc eption.h"
#endif
#include "RX_FEString.h"


#define SDB (0)
/*
 A NOTE ON TRIMMENU AND THE SUMMARY LOG
=========================================
The trimmenu points directly at members  d.TIS ,d.m_trim, even though they ought to be private.

Then the function apply_string_trims uses this pointer to post the edits.
This is HORRIBLE. (thanks Adam) We should write these via a Change method which does the post too. 
*/
int RX_FEString::Compute(void){
    int rc=0;
    rc+=this->EvaluateAllReadOnly();
    this->SetNeedsComputing(rc!=0); // Feb  2014 to turn it off
 //   if(rc)
        this->m_Needs_Meshing=1;
    return rc;
}
int RX_FEString::Finish(void) {
//make it a niece of all relsites in its SC. A project to deal properly with edited relsites

    sc_ptr sc = dynamic_cast<sc_ptr>  ( this->GetOneRelativeByIndex(RXO_SC_OF_STRING));
    if(sc)
    {
        set<RXObject*> s = sc->FindInMap(RXO_SC_OF_SITE,RXO_ANYTYPE);
        set<RXObject*> ::const_iterator it,ite=s.end() ;
        for(it=s.begin(); it!=ite;++it)
        {
            RXObject*o =*it;
            o->SetRelationOf(this,niece,RXO_SITE_OF_STRING);
        }
    }
   return 1;
 }
int RX_FEString ::CClear(){
    int rc=0;  // shouldnt we do a ReWriteLine();// so the string Trim is preserved on a re-resolve
    // clear stuff belonging to this object
    ClearFEA(); // this destroys the fortran object
    ClearElist();
    if(m_Text) free(m_Text); m_Text=0;

    Free_Entity_List(&(m_scList));
    d.Ne=0; d.m_zi_LessTrim=0;d.EAS =0;
    // call base class CClear();
    rc+=RX_FELinearObject::CClear();
    rc+=RXEntity::CClear ();
    return rc;
}


RX_FEString::RX_FEString(SAIL *s)
    :RXEntity(s)
{
    m_sail=s;
    RX_FEString::Init(s);
    RXENode::SetParent(s);

}

RX_FEString::RX_FEString(void)
{

    assert("NEVER call the default constructor for RX_FEString"==0);

    Init(0);

}

RX_FEString::~RX_FEString(void)
{
    // July 2001 now frees the stringdata too.

#ifdef _X
    delete_trim_shell ( g_trim_shell );  g_trim_shell =NULL;
#endif
    this->CClear();
    ClearFEA(); // this destroys the fortran object

    if(m_Text) free(m_Text);
    Free_Entity_List(&(m_scList));
}

int RX_FEString::Dump(FILE *fp) const {
    int nnn,k=0;
    fprintf(fp,"Name= %s\n scList\n",Name().Array());

    //    Print_Entity_List(m_scList ,fp);
    fprintf(fp," test whether getonerelative can replace m_scList \n");
    fprintf(fp,"  index  relative \t    m_scList \n");
    struct LINKLIST *p= m_scList;
    int index=0;class RXObject *oo;
    while (oo= GetOneRelativeByIndex(RXO_SC_OF_STRING,index))
    {
        fprintf(fp,"  %d   %S \t",index++,   oo->GetOName().c_str() ) ;
        if(p) {
            RXEntity_p ep = (RXEntity_p )p->data;  assert(ep);
            fprintf(fp,"  %s   %s ",  ep->type(),ep->name()) ;
            p=p->next;
        }
        fprintf(fp,"\n");
    }


    fprintf(fp,"GetN  = 12%d\n", GetN()   );
    fprintf(fp,"LMeas = %12.7g\n",m_L_FromEdges);
    fprintf(fp," The D-structure\n");

    fprintf(fp,"m_zi_LessTrim= %12.7g\n",d. m_zi_LessTrim );// initial L from edges or card. Without Trim
    fprintf(fp,"EAS          = %12.7g\n",d.EAS);
    fprintf(fp,"tis          = %12.7g\n",d.TIS );
    fprintf(fp,"trim         = %12.7g\n",d.m_trim);
    fprintf(fp,"mass         = %12.7g\n",d.m_Mass);
    fprintf(fp,"zt_s         = %12.7g\n",d. m_zt_s)	;
    fprintf(fp,"m_TConst     = %12d\n", d. m_TConst);
    fprintf(fp,"m_Slide      = %12d\n", d.m_Slide);
    fprintf(fp,"m_CanBuckle3 = %12d\n", d.m_CanBuckle3);	// never, always or flag-controlled
    fprintf(fp,"m_Scptr      = %12p\n", d.m_Scptr);
    fprintf(fp,"ExpCptr      = %12p\n", d.m_ExpCptr);
    fprintf(fp,"Nedges       = %12d\n", d.Ne);
    //  fprintf(fp,"m_tqOut      = %12.7g\n",d.m_tqOut); ;

    nnn = GetN();
#ifdef FORTRANLINKED
    double t;
    int sli = this->Esail->SailListIndex();
    if(cf_get_one_string_tension(&sli,&nnn, &t))
        fprintf(fp,"T(from F)= %12.7f\n",t);
//    char buf[2048]; *buf=0;
//    if(cf_get_one_string_listing(sli,nnn, buf))
//        fprintf(fp,"(from F)\n %s\n",buf);
    //    QString bufdbg(buf);
    //    qDebug()<<bufdbg;
#endif
    fflush(fp);
    return k;
}
int RX_FEString::Resolve( RXEntity_p sce){ 

    /*  string card (nov 2000) is

string	headstay	%curve	leech	%node	headfwd	ipoint	%endlist	10E4	0	0	noslide*/

    char *l_name=0;
    char *l_line;
    char *words[200],*lp,**wd;
    HC_KEY key = 0;
    RXEntity_p p1,p2 ;
    std::list< RXEntity_p>  ChildList;
    int k, i;

    this->SetLineLwr(); QString qline(this->GetLine());

    l_line = STRDUP(this->GetLine());    assert(!strstr(l_line,"!"));
    memset(words,0,200);
    wd=words;

    lp=strtok(l_line,RXENTITYSEPS);
    i=0;
    while(((lp=strtok(NULL,RXENTITYSEPS))!=NULL)&& i < 200) {
        words[i]=lp;
        PC_Strip_Leading(words[i]);	PC_Strip_Trailing(words[i]);
        i++;
    }
    if(i < 2) {
        RXFREE(l_line); return -1;
    }
    l_name = *wd;


#ifdef RXQT
    HC_Open_Segment_By_Key(this->Esail->PostProcNonExclusive());
#else
    HC_Open_Segment_By_Key(this->Esail->Graphic()->m_ModelSeg);
#endif
#ifdef HOOPS
    this->Esail->Hoops_Strings = HC_KOpen_Segment("strings");
#else
    HC_KOpen_Segment("strings");
#endif
    HC_Close_Segment();
    HC_Close_Segment();

    wd++;
    int count=0;
    while(*wd && !strieq(*wd,"%endlist")) {
        if(strieq(*wd,"%node")) { // There will be at least TWO nodes, we expect.
            wd++;
            p1 = this->Esail->Get_Key_With_Reporting("site,relsite",*wd);
            wd++;
            while ( **wd != '%') {
                p2 = this->Esail->Get_Key_With_Reporting("site,relsite",*wd);
                if(p1&&p2)
                { // create a straight SC
                    if(!(sce = RXSeamcurve::Find_Seamcurve_By_Ends(p1,p2))) // there is an order-dependence here
                        sce= PCS_Create_Curve_From_Ends(p1,p2,NULL,this->generated);
                    if(!(sce ))
                        return 0;
                    sce->SetRelationOf(this,frog|child|niece,RXO_SC_OF_STRING,count++);
                    ChildList.push_back(sce);
                }
                else 				// the sites not existing OR not resolved
                    return 0;
                p1 = p2;
                wd++;
            }
            wd--;

        }
        if(strieq(*wd,"%curve")) {
            wd++;
            if(!(sce&& strieq(sce->name(),*wd)))
                sce = this->Esail->Get_Key_With_Reporting("seam,curve,seamcurve,compound curve,nodecurve",*wd);
            if(!(sce))
                return 0;

            ChildList.push_back(sce);
            sce->SetRelationOf(this,spawn|child|niece,RXO_SC_OF_STRING,count++);
        } //if wd is curve
        wd++;
    }
    // If we are here at all, we have a list of SCs and wd points to '%endlist'
    wd+=4; //rxa.Add(*wd);

    this->PC_Finish_Entity("string",l_name,this,key,NULL,*wd,this->GetLine());
    this->Needs_Resolving= 0;
    this->Needs_Finishing = 1; // peter, was 0, jan 2014 so it can pick up relsites as aunts
    this->SetNeedsComputing(0);
    if ( this->Set_EA_Etc_On_StringData(l_name) ) { // fill in EA Ti, L0,trim
        std::list< RXEntity_p> ::iterator it,ite = ChildList.end();
        k=1;
        for(it = ChildList.begin(); it!=ite;++it) {
            sce = *it;
            Push_Entity(sce,&(this->m_scList));
        }

#ifdef HOOPS
        HC_Open_Segment("string");
        HC_Set_Visibility("off");
        HC_Set_Color("line=blue");
        HC_Set_Selectability("lines=on");
        HC_Close_Segment();
        if(!g_Janet) {
            HC_Open_Segment_By_Key(this->Esail->Hoops_Strings);
            HC_Include_Segment_By_Key(this->hoopskey);
            HC_Close_Segment();
        }
#endif
        RXFREE(l_line);
        return(1);
    }
    else
        return 0;
}
int RX_FEString::Freeze(const int flags)
{
    if(this->Needs_Resolving) return 0;
    // we assume that the string has ti,tq,zi,zt,ea, but
    //       these are not necessarily consistent because we may
    //       just have changed ea or something.
    // we  set TI = tq , zi = zt, trim=0;
    set<RXObject*>objs;
    RXExpressionI* expTi, *expTq, *expZi, *expZt,*expTrim;//,*expEA
    RXObject*o;
    double tq,zt;

    objs.clear(); objs= FindInMap(RXO_STRING_TI, 1);    assert(objs.size()==1);
    o =*(  objs.begin() );            expTi = dynamic_cast <RXExpressionI*>(o);
    objs.clear(); objs= FindInMap(RXO_STRING_TRIM, 1);    assert(objs.size()==1);
    o =*(  objs.begin() );            expTrim = dynamic_cast <RXExpressionI*>(o);
    objs.clear(); objs= FindInMap(RXO_STRING_ZI, 1);    assert(objs.size()==1);
    o =*(  objs.begin() );            expZi = dynamic_cast <RXExpressionI*>(o);
    objs.clear(); objs= FindInMap(RXO_STRING_ZT, 1);    assert(objs.size()==1);
    o =*(  objs.begin() );            expZt= dynamic_cast <RXExpressionI*>(o);
    objs.clear(); objs= FindInMap(RXO_STRING_TQ, 1);    assert(objs.size()==1);
    o =*(  objs.begin() );            expTq = dynamic_cast <RXExpressionI*>(o);


    tq=expTq->evaluate();  zt = expZt->evaluate();

    // we  set TI = tq , zi = zt, trim=0;
    expTi->Change(TOSTRING(tq));expTi->evaluate();
    expZi->Change(TOSTRING(zt));expZi->evaluate();
    expTrim->Change(L"(0*0)");expTrim->evaluate();
    this->ReWriteLine();
    return 1;
}

int RX_FEString::ReWriteLine(void){

    //string	headstay	%curve	leech	%node	headfwd	ipoint	%endlist	10E4	0	0	noslide
    RXEntity_p sce;
    struct LINKLIST *ll ;
    this->EvaluateAllReadOnly();
    RXSTRING buf(L"string : "); buf+= TOSTRING(this->name()); buf+= L" :";
    ll = this->m_scList;
    while(ll){
        sce = (RXEntity_p )ll->data;
        buf += L" %curve :";
        buf+= TOSTRING(sce->name());
        buf+= L" : ";
        ll = ll->next;
    }
    buf+= L" %endlist : ";
    buf+=TOSTRING(this->d.EAS ) + L" :  : " + TOSTRING(  this->d.TIS) + L" : ";

    //   RXAttributes att(this->attributes);

    AttributeSet(L"$trim",TOSTRING(this->d.m_trim));
    AttributeSet(L"$ea",TOSTRING( this->d.EAS ));
    AttributeSet(L"$ea", this->GetExpressionText(RXO_STRING_EA, 1 ) );
    AttributeSet(L"$ti",TOSTRING(this->d.TIS));
    AttributeSet(L"$mass",TOSTRING(this->d.m_Mass));
    AttributeSet(L"$zi",TOSTRING(this->d.m_zi_LessTrim));
#ifndef NEVER
    set<RXObject*>objs;  RXExpressionI* exp;RXObject*o;

    objs.clear();
    objs= FindInMap(RXO_STRING_EA, 1);   // unless there's a bug objs will always have size 1 and
    assert(objs.size()==1);
    if(objs.size())
    {   o =*(  objs.begin() );
        exp = dynamic_cast <RXExpressionI*>(o);
        if(exp)
            AttributeSet(L"$ea", exp->GetText());
    }
    objs.clear(); objs= FindInMap(RXO_STRING_TI, 1);    assert(objs.size()==1);
    if(objs.size())
    {
        o =*(  objs.begin() );
        exp = dynamic_cast<RXExpressionI*>(o);
        if(exp)  AttributeSet(L"$ti", exp->GetText());
    }
    objs.clear(); objs= FindInMap(RXO_STRING_TRIM, 1);    assert(objs.size()==1);
    if(objs.size())
    {
        o =*(  objs.begin() );
        exp = dynamic_cast<RXExpressionI*>(o);
        if(exp) AttributeSet(L"$trim", exp->GetText());
    }
#endif
    //  QString  attstring( AttributeString());
    buf+=AttributeString().toStdWString();

    this->SetLine( ToUtf8(buf.c_str()).c_str());
    return 0;
}
int RX_FEString::Set_EA_Etc_On_StringData(char*p_nnname){ // fill in EA Ti, L0, in all the bd
    static int dbgcount;
    char *line2=NULL;
    double EA,Ti, L0;
    QString qatts;
    dbgcount++;
    PCS_Parse_String_Line(this->GetLine(),&line2,&EA,&Ti,&L0,qatts ); // no strstrs YUCH!
    if(line2) RXFREE(line2);

    this->m_rxa.Add(qatts);

    d.EAS=EA;
    d.TIS =  Ti;


    std::string  sbuf;
    RXExpressionI  *q=0, *q1=0,*q2=0,*q3=0, *q4=0;

    sbuf = to_string(d.m_tqOut);
    q=this->AddExpression (new RXQuantityReadOnlyDble(L"tq",TOSTRING(sbuf.c_str()),L"N",this->Esail, &(d.m_tqOut)));
    this->SetRelationOf(q,spawn,RXO_STRING_TQ,1);//the Enode will take care of deletion
    if(! q->Resolve()) { this->CClear();  return 0; }
    sbuf = to_string(d.m_zt_s);
    q=this->AddExpression (new RXQuantityReadOnlyDble(L"zt",TOSTRING(sbuf.c_str()),L"N",this->Esail, &(d.m_zt_s)));
    this->SetRelationOf(q,spawn,RXO_STRING_ZT,1);
    if(! q->Resolve()) { this->CClear();  return 0; }

    QString qbuf;
    if( !AttributeGet("$zi",qbuf))
    {
        qbuf =  "-1" ;
        this->AttributeRemove("$flength");
    }
    if(!qbuf.length()){
        qbuf =  "-1";
        this->AttributeRemove("$flength");
    }
    q=this->AddExpression (new RXQuantityWithPtr(L"zi", qbuf.toStdWString(),L"m",this->Esail, &(d.m_zi_LessTrim)));
    this->SetRelationOf(q,parent|aunt|spawn,RXO_STRING_ZI,1);
    if(! q->Resolve()) { this->CClear();  return 0; }
    if(q)q->evaluate();

    if(! AttributeGet("$mass",qbuf)) qbuf="0";
    if(!qbuf.length()) qbuf="0";
    q4=this->AddExpression (new RXQuantityWithPtr(   L"mass",qbuf.toStdWString(),L"Kg/m",this->Esail, &(d.m_Mass ) ));
    this->SetRelationOf(q4,parent|aunt|spawn,RXO_STRING_MASS,1);//the Enode will take care of deletion
    if(! q4->Resolve()) { this->CClear();  return 0; }


    if(!AttributeGet("$ea",qbuf))        qbuf="0";
    if(!qbuf.length()) qbuf="0";
    q1=this->AddExpression (new RXQuantityWithPtr(   L"ea",qbuf.toStdWString(),L"N",this->Esail, &(d.EAS ) ));
    this->SetRelationOf(q1,parent|aunt|spawn,RXO_STRING_EA,1);//the Enode will take care of deletion

    if(!AttributeGet("$ti",qbuf))  qbuf="0";
    if(!qbuf.length()) qbuf="0";
    q2=this->AddExpression (new RXQuantityWithPtr(   L"ti",qbuf.toStdWString(),L"N",this->Esail, &(d.TIS ) ));
    this->SetRelationOf(q2,parent|aunt|spawn,RXO_STRING_TI,1);//the Enode will take care of deletion

    if(!AttributeGet("$trim",qbuf)) qbuf="0" ;
    if(!qbuf.length()) qbuf="0";
    q3=this->AddExpression (new RXQuantityWithPtr(L"trim",qbuf.toStdWString(),L"m",this->Esail, &(d.m_trim)));
    this->SetRelationOf(q3,parent|aunt|spawn,RXO_STRING_TRIM,1);//the Enode will take care of deletion

    if(q1)
        if(! q1->Resolve()) {
            this->CClear();  return 0; }
    if(q2)
        if(! q2->Resolve()) { this->CClear();  return 0; }
    if(q3)
        if(! q3->Resolve()) { this->CClear();  return 0; }
    if(q1)q1->evaluate();
    if(q2)q2->evaluate();
    if(q3)q3->evaluate();
    if(q4)q4->evaluate();


    return 1;
}
int RX_FEString::EdgeSwap()
{
    // 1) find the first and last psides.
    // from them, find the strings e1l and e2r for this seam
    // e1l gets changed from end1 to end2
    //e2R gets changed from end2 to end1
    // if they are NOT spawn of their SCs, rewrite their lines, unresolve then resolve
    // else if they are spawn, rewrite the SC's line, unresolve/reresolve the SC.
    QString skip;
    vector<PSIDEPTR> pss;
    sc_ptr sc;
    struct LINKLIST  *sclist = m_scList;
    while(sclist) {
        sc = (sc_ptr  ) sclist->data;
        for(int i=0;i< sc->npss;i++)
            pss.push_back(sc->pslist[i] );
        sclist = sclist->next;
    }
    if(!pss.size() )
        return 0;
    if(!( pss.front()->er[0]  ))
        return 0;
    if(! ( pss.back()->er[1]))
        return 0;

    RXEntity_p e1l, e2r;
    class RXSRelSite* n1, *n2;
    e1l = pss.front()->er[0]->sc;   n1 = pss.front()->Ep(0);
    e2r = pss.back() ->er[1]->sc;   n2 = pss.back() ->Ep(1);
    if((!n1 )|| (!n2))
        return 0;
    QString n1Name(n1->name()  ), n2Name(n2->name());

    // e1l, e2r are SCs which may have spawn strings.

    e1l->AttributeRemove( "xyzabs" );
    e1l->AttributeAdd("xscaled,yscaled" );
    e2r->AttributeRemove( "xyzabs" );
    e2r->AttributeAdd("xscaled,yscaled" );

    e1l->ReWriteLine(); e1l->edited++; // writes the string's zi,trim
    e2r->ReWriteLine(); e2r->edited++;
  //  Replace_String(&(e1l->line), "xyzabs","xscaled,yscaled");
  //  Replace_String(&(e2r->line), "xyzabs","xscaled,yscaled");

    RXEntity_p FileEnt =0;
    // if the SC is spawn of a 3dm, detach it.
    set<RXObject*> ss = e1l->FindInMap(RXO_ANYTYPE,RXO_ANYINDEX,frog);//RXO_ENT_OF_INCFILE
    set<RXObject*>::iterator its;
    for(its=ss.begin ();its!=ss.end(); ++its){
        FileEnt = dynamic_cast<RXEntity_p> (*its);
        if(FileEnt){
            e1l->UnHookCompletely( FileEnt);
            skip = "$skip=" + QString(e1l->name());
            FileEnt->AttributeAdd(skip);
            FileEnt->Needs_Resolving=0;
            break;
        }
    }
    ss = e2r->FindInMap(RXO_ANYTYPE,RXO_ANYINDEX,frog);
    for(its=ss.begin ();its!=ss.end(); ++its){
        FileEnt = dynamic_cast<RXEntity_p> (*its);
        if(FileEnt){
            e2r->UnHookCompletely( FileEnt);
            skip = "$skip=" + QString(e2r->name());
            FileEnt->AttributeAdd(skip);
            FileEnt->Needs_Resolving=0;
            break;
        }
    }
    e1l->UnResolve();
    e2r->UnResolve();
    if(FileEnt) FileEnt->Needs_Resolving=0;

    QString l(e1l->GetLine());
    l.replace(n1Name,n2Name, Qt::CaseInsensitive);
    if(FileEnt) {
        QStringList ll = l.split (RXENTITYSEPS);
        ll[4]= "straight";
        l=ll.join(RXENTITYSEPS );
        e1l->SetLine(qPrintable(l));

        l=QString(e2r->GetLine());
        l.replace(n2Name,n1Name, Qt::CaseInsensitive);
        ll = l.split (RXENTITYSEPS);
        ll[4]= "straight";
        l=ll.join(RXENTITYSEPS );
         e2r->SetLine(qPrintable(l));


        RXEntity_p bce = dynamic_cast<RXEntity_p>(this->Esail->Entity_Exists("straight","basecurve"));
        int depth = 0;
        if(!bce) {
            bce=Just_Read_Card(this->Esail ,"2dcurve:straight:0:0:1:0","2dcurve","straight",&depth,true);
            if(bce ) {
                if(bce->Resolve( ))
                    bce ->Needs_Finishing=!bce->Finish();
            }
        }
    }
    return 1;
}

// Mesh just creates the fortran edges for this string.
// need to follow it by something to construct the strings P structure.
int RX_FEString::Mesh(void)
{
    int j,k;
    RXEntity_p p1;
    sc_ptr cd1;
    Pside *ps;
    struct LINKLIST  *SClink;
    double z;
    if(!m_Needs_Meshing) return 0;
    m_Needs_Meshing=false;

    SClink =  m_scList;
    while(SClink) {
        p1 = (RXEntity_p )SClink->data;

        if(!p1 || p1->Needs_Resolving || p1->Needs_Finishing) {
            char buf[256];
            sprintf(buf," Invalid string curve reference by %s (%s) \n", this->name(),Name().Array());
            rxerror(buf,1);
            SClink = SClink->next;
            return 0;//  continue
        }

        cd1 = (sc_ptr ) p1;
        if(SDB && !cd1->npss)
            printf(" in MeshStrings %s %s has Npss=%d, so mesh it\n", p1->type(), p1->name(), cd1->npss);
        if(!cd1->npss ){
            j = cd1->sketch; cd1->sketch=0; cd1->Make_Pside_Request =1; // WRONG lashup
            cd1->Break_Curve();
            cd1->sketch =j;

        }
        for(j=0;j<cd1->npss;j++) {
            ps=  cd1->pslist[j];

            if(!ps->m_eList.size()){ // normally a sketch SC
                ps->Mesh();   // this works in blackspace.
            }
            else if(!ps->leftpanel [0] && !ps->leftpanel [1]) { // normally a Draw SC with no panels
                for(k=0;k<ps->m_eList.size();k++) {
                    class RX_FEedge *ed = ps->m_eList[k];
                    z =ed->GetNode(0)->DistanceTo (*(ed->GetNode(1)) );// NB This places the string length into Black Space and can stuff the pattern
                    ed->SetLength(z);
                    int ne = ed->GetN();
                    RX_FEedge * fe = this->Esail->m_Fedges[ne];
                    assert(ed==fe);

                    fe->SetLength(z);
                }
            }
        }/* for j */

        SClink = SClink->next;
    } /* end while strlink */
    return 0;
}
int RX_FEString::PostToMirror()  // just some inputs
{
    int rc=0;
    RXExpressionI *exp;
    string header;
    QString qbuf,lp;
    if ( !strchr(this->name(),'&') && !this->AttributeGet("$notrim" , lp))       //   && !stristr(this->attributes,"$notrim"))
    {
        header=string("string$")+this->name()+string("$trim");
        double*x= this->GetTrimPtr();
        exp=this->FindExpression(L"trim",rxexpLocal);
        if(exp)
            qbuf = QString (ToUtf8(exp->GetText()).c_str());
        else
            qbuf = QString("%1").arg(*x);
        Post_Summary_By_Sail(this->Esail,header.c_str(),qPrintable(qbuf));

        header=string("string$")+this->name()+string("$prestress");
        x= this->GetPrestressPtr();
        exp=this->FindExpression(L"ti",rxexpLocal);
        if(exp)
            qbuf = QString (ToUtf8(exp->GetText()).c_str());
        else
            qbuf =  QString("%1").arg(*x);
        Post_Summary_By_Sail(this->Esail,header.c_str(),qPrintable(qbuf));
        rc++;
    }
    return rc;
}

// places the string into the FEA database
// The FEA database shares  memory allocated in C
// prerequisite is that the P structure is already prepared. 
int RX_FEString::AddFEA(const int n_edges)
{
    int err=0, N=-1;
    double*f_tqptr=0;  int *f_septr=0,*f_revptr=0 ;
    SAIL * sail = GetSail();
    // first translate the attributes as for PrintStrings
#ifdef FORTRANLINKED 
    if(!IsInDatabase()) { //here we add a fortran STRING to stringlist
        N = cf_createfortranstring(sail->SailListIndex(), &err); if(err) { cout<< " Create string error"<<endl; _rxthrow( " Create string error");}

        SetIsInDatabase();

        SetN(N);

        int tl =  256;
        // hook up  the fortran data
        cf_Hook_String_Memory2(sail->SailListIndex(),N,&(d),d.Ne, &f_septr ,&f_revptr,&f_tqptr,&tl,m_Text);
        m_FPse = f_septr;
        m_FPtq = f_tqptr;
        m_FPrev = f_revptr;

    }
#else
    assert(0);
#endif
    return N;
}

// removes the string from the FEA database
int RX_FEString::ClearFEA(void)
{
    int err=0;
    if(IsInDatabase()) {
#ifdef FORTRANLINKED
        cf_removefortranstring(GetSail()->SailListIndex(),GetN(),&err);
        if(err)
            cout<<"(RX_FEString::ClearFEA) "<<name()<<"  ("<< GetN()<<") err = "<<err<<endl;
#else
        assert(0);
#endif
        SetIsInDatabase(false);
        m_FPse=0; // pointer to the fortran array holding SE
        m_FPrev=0; // pointer to the fortran array
        m_FPtq=0;
    }
    return(!err);
}

// collects the pside list and the attributes into the P structure. Its OK to free and reallocate if we follow by an AddFString
int RX_FEString::PostMesh(void)
{	
    /* format is:
 string no.	no of edges	Ti	EA 	Zi edge1  edge2 ...	*/
    /*
1) never buckles	$nobuckle	Ibuckle = -1
2) always buckles	$buckle 	Ibuckle = 1
3) depends on the /CC setting	Ibuckle = 0
*/	
    //	if(g_Janet && !FLength && !started) error(" Janet string without Flength",4); this must have been BAD

    int  noslide;

    if(SDB) printf("Post Mesh string %s\n",this->name());
    d.Ne   = 0;
    noslide=this->AttributeGet((const char*)"$noslide" );
    d.m_Slide =!noslide;
    d.m_CanBuckle3=RXS_BUCKLEDEPENDS;
    if ( this->AttributeGet((const char*)"$buckle" ) )
        d.m_CanBuckle3=RXS_ALWAYSBUCKLE;
    if ( this->AttributeGet((const char*)"$nobuckle" ) )
        d.m_CanBuckle3=RXS_NEVERBUCKLE;

    d.m_TConst  = (this->AttributeGet("$force_control"));

    //	which =   LENGTH or TENSION */
    m_elist.clear(); m_L_FromEdges=0;
    d.Ne=CountPsideEdges(&m_L_FromEdges );
    if(SDB) cout<<"After CountPsideEdges, m_L_FromEdges= "<<this->m_L_FromEdges <<endl;
    int count = SortPsideEdges();

    if(d.Ne !=count) {printf(" Sort removed some %d %d\n", d.Ne,count); d.Ne =count;}
    if(d.Ne ==1 )
        d.m_Slide=0;  // a work-around for the element assembler
    if(!m_Text)
        m_Text=(char*)calloc(256,1);
    memset(m_Text,' ',256);
    strcpy(m_Text,"my own text");
    sprintf( m_Text,"%s,%s",GetSail()->GetType().c_str(),this->name() ); 			PC_Strip_Trailing(m_Text);
    AddFEA(d.Ne); // its safe multiple times. This allocates the se,rev,tq array in fortran and passes their addresses to C

    RXExpressionI *q;
    q = this->FindExpression(L"zi",rxexpLocal);
    if (!this->AttributeGet("$flength") || q->evaluate()<=0.0)
    {
        q->Change(TOSTRING(m_L_FromEdges  )); // provokes a 'NEEDS_GAUSSING'
    }
    // Ibuckle = 0 means cannot buckle.  Iconst = 0 is normal.  Other Iconst untested.
    if(SDB) printf(" \t%3d \t%f \t%f \t%f \t%d \t%d \t%d \t%f  \t%f\n",d.Ne ,
                   d.TIS,d.EAS,d.m_trim  ,d.m_CanBuckle3 , noslide, d.m_TConst ,d.m_zi_LessTrim , d.m_Mass );
    d.m_Scptr=this;
    // populate the arrays  from m_elist


    vector<FEEdgeRef>::iterator ii;
    int kkk;
    for(ii=	m_elist.begin(),kkk=0 ; ii!= m_elist.end();ii++,kkk++) {
        FEEdgeRef r = *ii;
        FortranEdge *e= r.e;
        m_FPse[kkk] = e->GetN();
        m_FPrev[kkk]=r.rev;
        m_FPtq[kkk] =0.0;
    }
    return d.Ne;
}

void RX_FEString::SetTrim(const double x){
    this->d.m_trim=x;  // this member is shared by fortran.
}

int RX_FEString::CountPsideEdges (double*p_z)
{
    sc_ptr sc;
    struct LINKLIST  *sclist;
    int  c=0;
    sclist = m_scList;

    while(sclist) {
        sc = (sc_ptr  ) sclist->data;
        c+= RX_FELinearObject::CountPsideEdges(p_z,sc->pslist,sc->npss);
        sclist = sclist->next;
    }
    return c;
}// RX_FEString::CountPsideEdges


void RX_FEString::Init(SAIL * p_sail)

{
    m_scList=0; // list of edges - may be generated or not
    m_Needs_Meshing=true;
    m_which=0;	// LENGTH or TENSION
    flags=0;
    m_FPse=0;
    m_FPrev=0;
    m_FPtq=0;
    m_Text=0;

    m_L_FromEdges=ON_UNSET_VALUE;	// measured from the edges
    d.m_zi_LessTrim =ON_UNSET_VALUE;// initial length for analysis. a copy of one of tehe above. Without Trim

    d.EAS=ON_UNSET_VALUE; ;			// EA
    d.TIS=ON_UNSET_VALUE; d.m_trim=ON_UNSET_VALUE; d.m_Mass= ON_UNSET_VALUE;
    d.m_zt_s= 0.0;
    d.m_TConst=0;		// true if ignore extension as if EA is zero
    d.m_CanBuckle3=RXS_BUCKLEDEPENDS;
    d.m_Slide= true;	// means intermediate nodes slide freely on the string
    d.m_tqOut=0.0;
    d.Ne=-99;
    d.m_Scptr=0;
    d.m_ExpCptr=0;
}

int RX_FEString::SetNeedsMeshUpdate(){
    int rc =0;
    RXEntity_p sce;
    sc_ptr sc;
    PSIDEPTR pp;
    struct LINKLIST  *sclist;
    sclist = m_scList;
    while(sclist) {
        sce = (RXEntity_p )sclist->data;
        sc = (sc_ptr  )sce;
        for (int k=0;k<sc->npss;k++) {
            pp = sc->pslist[k];
            pp->SetpsFlag(1);
            rc++;
        }
        sclist = sclist->next;
    }
    this->m_Needs_Meshing=true;
    return rc;
}
double RX_FEString::MeasureLengthFromEdges(void)
{
    m_L_FromEdges =0.0;

    RXEntity_p sce;
    sc_ptr sc;
    PSIDEPTR pp;
    struct LINKLIST  *sclist;

    sclist = m_scList;
    while(sclist) {
        sce = (RXEntity_p )sclist->data;
        sc = (sc_ptr  )sce;
        for (int k=0;k<sc->npss;k++) {
            pp = sc->pslist[k];
            for(int l=0;l<pp->m_eList.size();l++){
                FortranEdge *e = pp->m_eList[l];
                m_L_FromEdges+= e->GetLength();
            }
        }
        sclist = sclist->next;
    }
    return m_L_FromEdges;
}

HC_KEY RX_FEString::DrawDeflected(HC_KEY p_k,const double t1, const double t2,const bool Skip_Zero_Tension){
    RXEntity_p sce;
    sc_ptr sc;
    PSIDEPTR pp;
    struct LINKLIST  *sclist;
    double t,hue=200;
    if(this->d.Ne<1) return 0;
    if(Skip_Zero_Tension && m_FPtq[0]<=0.0)
        return 0;

    if(fabs(t2-t1) < 1e-8)
        hue = 225;
    else {
        t = (m_FPtq[0] -t1)/(t2-t1);
        hue = 270.0 - t*270.0;  // 270 =cold, descending to 0 =hot
        hue = max(hue,0.0); hue = min(hue,360.0);
    }
    int ihue = (int) hue;
    char buf[256],s[256];
    int has_seg =   AttributeGet("$seg",s)&&!rxIsEmpty(s);
    if(has_seg) HC_Open_Segment(s);

    sprintf(buf,"hue_%3.3d",ihue);
    HC_Open_Segment(buf);
    HC_Set_Color_By_Value("lines", "HSV",(float)hue,(float)1.0,(float)1.0);
    sclist = m_scList;
    while(sclist) {
        sce = (RXEntity_p )sclist->data;
        sc = (sc_ptr  )sce;
        for (int k=0;k<sc->npss;k++) {
            pp = sc->pslist[k];
            for(int l=0;l<pp->m_eList.size();l++){
                FortranEdge *e = pp->m_eList[l];
                e->DrawDeflected( p_k);
            }
        }
        sclist = sclist->next;
    }
    HC_Close_Segment();
    if(has_seg) HC_Close_Segment();
    return 0;
}


