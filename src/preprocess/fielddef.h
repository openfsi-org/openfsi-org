#pragma once

	/* fielddef.h  structures used by fields */

	  struct PC_FIELD {
		 RXEntity_p sc1, sc2, mat, focus;
		 char* m_TyPe;
	 	int flag;
		int Local;
	  };
/* the following definitions are for the flag */

#define UNIFORM 		1 
#define TRIANGLE		2
#define RAMP			4
#define COSINE  		8
#define COSSQ			16
#define EVERYWHERE		32
#define INVERSE 		64
#define RADIAL  		128
#define REVERSED		256
#define FILAMENT_FIELD	512
#define CURVE_FIELD		1024
#define NURBS_ALPHA		2048
#define DOVE_FIELD		4096

