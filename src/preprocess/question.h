#pragma once

EXTERN_C int Finish_Question ( RXEntity_p e ) ;
EXTERN_C int Ask_All_Questions(Graphic *g,int regardless );

EXTERN_C int Finish_All_Questions(class RXSail *p_sail, const int regardless);
EXTERN_C int Env_Edit(Graphic *gin,const char *filter,const char *n);
EXTERN_C int XQuestion(char**p,char**d,char**t,char**a,int c,const Graphic *g);

