
#ifndef READ_16NOV04
#define READ_16NOV04

RXEntity_p Read_Batten_Card(SAIL*sail,char *line);
RXEntity_p Read_Fabric_Card(SAIL*sail,char *line);
Site * Read_Site_Card(SAIL*sail,char *line);
Site * Read_Relsite_Card(SAIL*sail,char *line);
RXEntity_p Read_SeamCurve_Card(SAIL*sail,char *line);
RXEntity_p Read_Spline_Card(SAIL*sail, char *line); 
   	
#endif //#ifndef READ_16NOV04
