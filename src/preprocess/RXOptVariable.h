#pragma once
#include "layerobject.h"
#include "opennurbs.h"

#define RX_DP double
template<class T>
inline void SWAP(T &p, T &q)
    {T dum=p; p=q; q=dum;}
#define RXOPT_UNDEFINED -1


 enum rxoptType   {  undefined,   u,   v,  dir,  length};// put back in for gcc

class RXOptVariable;
typedef  ON_ClassArray<RXOptVariable> RXOptVarList;

class RXOptimisationCaller 
{
//	friend RXOptimisation;
public:
	RXOptimisationCaller();
	~RXOptimisationCaller();
	RXOptVarList * GetVarList(){ return &m_vars;}
    RXOptVariable* CreateVariable ( int p_type,RX_DP p_value, RX_DP p_scale, ON_String p_label); //
	RXOptVariable *GetVarByType( int p_type);
	void SetOpt( class RXOptimisation *p_opt) {m_opt= p_opt;};
	class RXOptimisation *GetOpt() { return m_opt;}
	int VarPrint(FILE *fp);

	bool GetStopFlag() { return m_STOP;}
	void SetStopFlag(bool p) { m_STOP=p;}
private:
		bool m_STOP;
public:

protected:
RXOptVarList m_vars;
class RXOptimisation *m_opt;

public:
	int ClearVariables(void);
}; // class RXOptimisationCaller




class RXOptVariable :
	public layerobject 

{
	friend class RXOptimisation;
	friend class RX_NelderMead;
public:
	RXOptVariable(void);
	RXOptVariable (enum rxoptType p_type);
    RXOptVariable (int p_type,RX_DP x0,RX_DP scale, ON_String p_label);
public:
	~RXOptVariable(void);

	void SetOn(bool b=true) {m_active=b ;}
	void SetOff() {m_active=false ;}
	int GetType(){return m_type;}
    void SetValue(const RX_DP u);
    RX_DP GetValue(){ return m_value;};
    void SetInputStep(const RX_DP s  ) {m_steplengthinput=s; };
    RX_DP   GetInputStep( )const {return m_steplengthinput; };
    void SetScaledStep(const RX_DP s  ) {m_steplengthscaled=s; };
	int Print(FILE *fp);

protected:
	int m_index;  // index into the array of optimisation parameters.
    RX_DP m_value; //
    RX_DP m_steplengthinput;
    RX_DP m_x0;
    RX_DP m_steplengthscaled;
	bool m_active;
	int m_type ; // u,v,dir,l, etc
	ON_String m_label;

}; //class RXOptVariable


