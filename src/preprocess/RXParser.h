#pragma once
//RXParser is a wrapper for MTParser. 

#include "opennurbs.h"
#include <iostream>

#include "MTParser.h"

ON_String ReplaceWhitespace(ON_String line) ;

class RXParser
{
public:
	RXParser(void);
	RXParser(const char*buf);
public:
	~RXParser(void);
	int Print(FILE *fp);
	ON_wString Report(const char*head);
	ON_String Serialize() const;
	char *Deserialize(char*lp);

	bool redefineVar(const MTCHAR *symbol, MTDOUBLE *pVal);
	MTDOUBLE evaluate();
	bool HasErrors() { return ( m_MessageboxRetVal!=IDIGNORE); }

	static ON_wString nextword(ON_wString &line, const char delim) ;
protected:
	MTParser m_eqn;
	ON_wString m_text; // the text of the expression
	ON_wString m_originalText; // the whole input text. If you edit the expression you must rewrite this.
	double *m_pVars;	// dummy variables initially associated with the variables referenced in the expression and macros
	int m_MessageboxRetVal;
}; // class RXParser
