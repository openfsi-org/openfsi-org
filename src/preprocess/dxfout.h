 /* dxfout.h */
#ifndef _DXFOUT_H_
#define _DXFOUT_H_


#ifdef _CONSOLE
 	struct vector{
 	float x,y,z;
 	 };
// 	typedef struct vector VECTOR;
#else
	#include "vectors.h"
#endif


int PC_Show_Net_DXF_Color(char *type, int*dxfcol); /* gets ACAD color of current seg */
int DXFoutStart(FILE *fp,int *handle); /* writes a minimal start section */
int DXFoutEnd(FILE*fp);
int DXFout(char *fname);

int DXFoutPoint	  (FILE *fp,VECTOR p,char*name,int*handle,char*xdata);
int DXFoutPolyline (FILE *fp,int c,VECTOR *p,char*name,int*handle,char*xdata, float w);
int DXFoutPolygon  (FILE *fp,int c,VECTOR *p,char*name,int*handle, char*xdata); 

int DXFoutShell(FILE *fp, int pcount,VECTOR *pts, int Nfaces,int *flist,char *name,int*handle, char*xdata, int E);
int DXFoutMesh(FILE *fp, int rows,int cols,VECTOR *pts, char *name, int*handle, char*xdata, int order);
int DXFoutText	  (FILE *fp,VECTOR p,char*t,float h, char*name,int*handle,char*xdata);
int DXFPrintHandle(FILE *fp,int*handle) ;

#endif  //#ifndef _DXFOUT_H_
