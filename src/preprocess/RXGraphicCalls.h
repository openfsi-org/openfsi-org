#ifndef _RXGRAPHIC_CALLS_HDR__
#define _RXGRAPHIC_CALLS_HDR__
#ifndef EXTERN_C
#define EXTERN_C extern "C"
//#define EXTERN_C Q_DECL_EXPORT
#define RXEntity_p  void *
#endif
#include <iostream>
#include <stdio.h>
using namespace std;

#ifdef NO_RXGRAPHICS
#define DISABLE_GRAPHICS (1)
#else
extern "C" int RXGraphicsEnabledQ();
#define DISABLE_GRAPHICS (!RXGraphicsEnabledQ())
#endif

#ifdef HOOPS // MUST MATCH debug.h
    #include "hc.h"
    #define RXGRAPHICOBJECT HC_KEY
    #define RXGRAPHICSEGMENT HC_KEY
	#define RX_Insert_Polyline(a,b,c,d )   HC_Insert_Polyline(a,b) 
	#define RX_Insert_Line(cx,cy,cz,dx,dy,dz,node)   HC_Insert_Line(cx,cy,cz,dx,dy,dz) 
	#define HC_KInsert_Polygon(c,p,n )  HC_Insert_Polygon(c,p)
        #define RX_Insert_Marker(x,y,z,n)  HC_Insert_Marker(x,y,z)
#elif defined (USE_GLC)
    typedef  class GLC_Geometry* RXGRAPHICOBJECT;
    typedef  class GLC_StructOccurence* RXGRAPHICSEGMENT;
    #include <GLC_Global>
    #define RXGR_KEY  GLC_uint
    #define HC_KEY  RXGR_KEY

#elif  defined (COIN3D)
    #include <Inventor/nodes/SoGroup.h>
    #include <Inventor/nodes/SoNode.h>

//    #define RXGR_OBJ_CLASS SoNode
//    #define RXGR_SEG_CLASS SoGroup
    typedef class SoNode* RXGRAPHICOBJECT ;
    typedef class SoGroup* RXGRAPHICSEGMENT;

    #define HC_KEY RXGRAPHICSEGMENT
#else
    typedef void* RXGRAPHICOBJECT ;
    typedef void* RXGRAPHICSEGMENT;
    #define HC_KEY RXGRAPHICSEGMENT
#endif


#ifdef USE_GLC
EXTERN_C RXGRAPHICSEGMENT RXNewWindow(const std::string caption, void*ptr);
EXTERN_C int RXDeleteWindow( void*ptr); // called with a ptr to RXViewFrame
EXTERN_C RXGRAPHICSEGMENT RXNewGNode(const char*caption,class GLC_StructOccurence* pParent);

EXTERN_C class GLC_Geometry* RX_Insert_Polyline(const int count,const void *poly,RXGRAPHICSEGMENT  o, const char*color=0)  ;
EXTERN_C class GLC_Geometry* HC_KInsert_Polygon(const int count,const void *poly,RXGRAPHICSEGMENT  o, const char*color=0)  ;
EXTERN_C class GLC_Geometry* RX_Insert_Line( const float a, const float b, const float c, const float d, const float e, const float f,RXGRAPHICSEGMENT  o) ;
EXTERN_C class GLC_Geometry* RX_Insert_Marker ( const float a, const float b, const float c ,RXGRAPHICSEGMENT  o) ;

extern void HC_Flush_Segment  (RXGRAPHICSEGMENT n);
extern void HC_Flush_Geometry (RXGRAPHICSEGMENT n );

//#elif  defined (COIN3D) NO lets always have these
#else
EXTERN_C int RXGNodeWrite(const char*filename,RXGRAPHICSEGMENT root);
EXTERN_C RXGRAPHICSEGMENT RXNewWindow(const std::string caption,void*ptr);
EXTERN_C int RXDeleteWindow( void*ptr); // called with a ptr to RXViewFrame
EXTERN_C RXGRAPHICSEGMENT RXNewGNode(const char*caption,RXGRAPHICSEGMENT  pParent);
EXTERN_C RXGRAPHICSEGMENT RXGNodeAddChild(RXGRAPHICSEGMENT pParent,RXGRAPHICSEGMENT pChild);
EXTERN_C RXGRAPHICSEGMENT RXGNodeRemoveChild(RXGRAPHICSEGMENT  pParent, RXGRAPHICSEGMENT  pChild);

EXTERN_C RXGRAPHICOBJECT RX_Insert_NurbsCurve(const class ON_NurbsCurve *c,RXGRAPHICSEGMENT  o, const char*color=0)  ;
EXTERN_C RXGRAPHICOBJECT RX_Insert_Polyline(const int count,const float*poly,RXGRAPHICSEGMENT  o, const char*color=0)  ;
EXTERN_C RXGRAPHICOBJECT HC_KInsert_Polygon(const int count,const float*poly,RXGRAPHICSEGMENT  o, const char*color=0)  ;
EXTERN_C RXGRAPHICOBJECT RX_Insert_Line( const float a, const float b, const float c, const float d, const float e, const float f,RXGRAPHICSEGMENT o, const char*color=0) ;
EXTERN_C RXGRAPHICOBJECT RX_Insert_Marker ( const float a, const float b, const float c ,RXGRAPHICSEGMENT  o) ;

//SoCoordinate3
EXTERN_C RXGRAPHICOBJECT RX_InsertVertices(const float*xyz, const int n ,RXGRAPHICSEGMENT  o, const char*color=0);

// PER_VERTEX -bound materials
EXTERN_C RXGRAPHICOBJECT RX_VertexColor(const float*rgb, const int n ,RXGRAPHICSEGMENT  o );

//SoIndexedFaceSet
EXTERN_C RXGRAPHICOBJECT RX_InsertFaces(const int *abc, const int n ,RXGRAPHICSEGMENT  o, RXGRAPHICOBJECT coords );
// the last two do a flush first.

EXTERN_C RXGRAPHICOBJECT RX_InsertPolyhedron(const float* xyz,const int cp, const int*faceSet, const int cf,RXGRAPHICSEGMENT  o);


extern void HC_Flush_Segment  (RXGRAPHICSEGMENT n);
extern void HC_Flush_Geometry (RXGRAPHICSEGMENT n );


#endif

#ifndef HOOPS
extern void HC_Flush_Segment (const char*s);
extern void HC_Flush_Geometry (const char*s );

EXTERN_C void HC_Flush_Contents(const char *a, const char *b);



EXTERN_C int List_Open_Segments(void);

//EXTERN_C void HC_Begin_Segment_Search(const char *s);
//EXTERN_C int  HC_Find_Segment( char*buf) ;
//EXTERN_C void HC_End_Segment_Search();
//EXTERN_C void HC_Begin_Contents_Search(const char *s, const char*t);
//EXTERN_C int  HC_Find_Contents(const char *type, HC_KEY *k) ;
//EXTERN_C void HC_End_Contents_Search();

EXTERN_C void HC_Exit_Program() ;

EXTERN_C void HC_Open_Segment(const char *s);

EXTERN_C void HC_Include_Segment(const char *s);
EXTERN_C void HC_Delete_Segment(const char *s);
//EXTERN_C void HC_Style_Segment(const char *s);
//EXTERN_C void HC_Set_User_Options (const char *s);
EXTERN_C void HC_Open_Segment_By_Key( const HC_KEY  k);
EXTERN_C void HC_Set_Selectability (const char *s);
EXTERN_C HC_KEY HC_KOpen_Segment(const char *s);
EXTERN_C void HC_Include_Segment_By_Key(const HC_KEY k) ;

//EXTERN_C void  HC_Style_Segment_By_Key(const HC_KEY k) ;
EXTERN_C void HC_Show_Segment(const HC_KEY k, char*segname);
EXTERN_C void HC_Close_Segment();
EXTERN_C void HC_Update_Display();



EXTERN_C void HC_Set_User_Index(const int i, const void *v) ;
EXTERN_C void HC_Show_One_Net_User_Index(const int i, RXEntity_p *v) ;
EXTERN_C void HC_Show_One_User_Index(const int i, RXEntity_p *v) ;

EXTERN_C void  HC_Show_Key_Status(const HC_KEY k, char*segname);
EXTERN_C void HC_Show_Include_Segment(const HC_KEY k, char*segname);

//EXTERN_C void HC_MSet_Vertex_Colors_By_FIndex(const HC_KEY k,const char*what ,const int i,const int j,float*vv);
//EXTERN_C void  HC_Open_Geometry(const HC_KEY k );
//EXTERN_C void  HC_Close_Geometry();
EXTERN_C void HC_Delete_By_Key(const HC_KEY k);



EXTERN_C int HC_QShow_Existence(const char *a, const char *b);

EXTERN_C void HC_Show_Pathname_Expansion(const char *a,  char *b);
EXTERN_C void HC_QSet_Color(const char *a, const char *b);
EXTERN_C void  HC_Set_Color_By_Index(const char *a, const int i);
//EXTERN_C void  HC_QSet_Selectability(const char *a, const char *b);
EXTERN_C void HC_QShow_Color(const char *a, char *b);
EXTERN_C void HC_Set_Color(const char *a);
//EXTERN_C void  HC_Set_Text_Alignment(const char *a);

EXTERN_C void HC_Set_Visibility(const char *a);
//EXTERN_C void HC_Set_Line_Pattern(const char *a);
//EXTERN_C void HC_Set_Rendering_Options(const char *a);
//EXTERN_C void HC_Set_Text_Font(const char *a);
//EXTERN_C void HC_QSet_Text_Font(const char *a, const char*b);
//EXTERN_C void HC_Set_Text_Path(float a, float b, float c) ;
//EXTERN_C void HC_QSet_Marker_Symbol(const char *a, const char*b);

//EXTERN_C void HC_QUnSet_Rendering_Options (const char *a);
EXTERN_C void HC_Define_System_Options(const char *a);

EXTERN_C void HC_Set_Color_By_Value(const char *a,const char *b,  float p, float q, float r) ;
EXTERN_C void HC_QShow_One_Color(const char *a, const char *b, char *c); 
EXTERN_C void HC_QSet_User_Options(const char *a, const char *b);
EXTERN_C void HC_Show_One_Net_User_Option(const char *a, char *b);
EXTERN_C void HC_QShow_User_Options(const char *a, char *b); 
EXTERN_C void HC_Show_User_Options(char *b);


EXTERN_C void HC_Set_Modelling_Matrix(float *x) ;
//EXTERN_C int  HC_Compute_Matrix_Inverse(float* a,float* b) ;

EXTERN_C void 	HC_Set_Line_Weight(const double a)   ; 


EXTERN_C void 	HC_Insert_Ink ( const float a, const float b, const float c ) ;
EXTERN_C void 	HC_Insert_Text   ( const float a, const float b, const float c, const char*s ) ;
EXTERN_C void 	HC_Move_Text(const HC_KEY k, const float a, const float b, const float c ) ;	
EXTERN_C void 	HC_Set_Marker_Symbol(const char *a);
EXTERN_C void HC_Restart_Ink();

//EXTERN_C void  HC_QInsert_Line (const char*s, const float a, const float b, const float c, const float d, const float e, const float f) ;
//EXTERN_C void  HC_Rotate_Object( const float a, const float b, const float c ) ;
EXTERN_C void  HC_Translate_Object( const float a, const float b, const float c ) ;
#endif
#endif
