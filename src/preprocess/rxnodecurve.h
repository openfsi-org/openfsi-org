#ifndef RXNODECURVE_H
#define RXNODECURVE_H
#include "RXSeamcurve.h"

class RXNodeCurve : public RXSeamcurve
{
public:
    RXNodeCurve();
    RXNodeCurve(class RXSail *s);
    int CClear();
    virtual int Dump(FILE *fp) const;

    virtual int Compute(void);
    virtual int Resolve(void);
    virtual int ReWriteLine(void);
    virtual int Finish(void);

    virtual~RXNodeCurve();

};

#endif // RXNODECURVE_H
