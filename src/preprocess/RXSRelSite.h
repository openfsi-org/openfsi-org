#pragma once
#include "griddefs.h" // for sideref
#include "RX_FESite.h"
#include "RXEntity.h"

// values for RXSRelSite::ctype
#define CYLINDRICAL_XY   1
#define CYLINDRICAL_YZ   2
#define CYLINDRICAL_XZ   3
#define CARTESIAN        0
#define POLAR            4
#define UV_SITE          5
#define UV_RELSITE       7

#define MOULDED		     8 // MUST MATCH griddefs.h 
#define NOMOULD		     16

#define  RELSITE_U   1
#define  RELSITE_V  2
class RXExpressionI;
class RXSail;


class RXSRelSite :
        public RX_FESite, public RXEntity
{
public:
    RXSRelSite(void);
    RXSRelSite(class RXSail *p );
    virtual ~RXSRelSite(void);
    int Init(void);
    int CClear();
public: 

    int ctype;		/*  0 - cartesian
                    see top of this file for defines
                    anything else invalid
                 */

    RXEntity_p OriginEnt;   /* used for polar definitions */

    double CrossAngle;

    sc_ptr CUrve[2];  // IF both  are NULL, the position isnt determined by disance along a curve

    class RXOffset* Offsets[2];
private:
    struct SIDEREF *m_psList;
    int m_psno;

public:
    bool GetUVAttribute( const int what,  double &value)const; // what is RELSITE_U or _V
    bool AttributeGet(const QString what,QString &value) const; // implements a complex inheritance for fixings and contacts
    int IsEdgeNode()const;
    int PrintShort(FILE *ff) const;
    int Dump(FILE *fp) const;
    RXSTRING TellMeAbout() const;

    int ResolveCommon();
    int Resolve(void);
    int ReWriteLine(void);
    int Compute(void);
    int Finish(void);

    int Get_Nodal_Angles()const ;
    int Sort_One_Node();
    int RemoveOnePside(const class RXPside* ps);
    int AddOnePside(const RXPside *ps,const int e);
    struct SIDEREF PSList(const int k)const { assert(k>=0); assert(k < this->m_psno); return this->m_psList[k];}
    int PSCount() const {return m_psno;}
    ON_3dVector Normal(void ) const;

    void SetNeedsComputing(const int i=1);
private:
    int ResolveAsSite();
    int ResolveAsRelsite();

    int FinishAsSite();
    int FinishAsRelsite();
    int ComputeAsSite();
    int ComputeAsLengthControlled();
    int ComputeSimpleSite(); //returns  nonzero if it moved
    ON_3dPoint Compute_Polar_Site(const double A,const double B,const double C);
    int Touch_Panels();

};
