// RXUPoint.h: interface for the RXUPoint class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RXUPOINT_H__34B6764A_ABBD_4907_B5C3_4BDEF35DCD64__INCLUDED_)
#define AFX_RXUPOINT_H__34B6764A_ABBD_4907_B5C3_4BDEF35DCD64__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define RXUPOINT_OK 1
#define RXUPOINT_FAILLED 0

#include "RXTRObject.h"

//! Thomas 29 10 04 to define a Point on curve without copying the curce
//! the RXUpoint has pointer to an ON_Curve instead of a copy for the case of the RXPointOnCurve
//! RXUPoint must be better and should remain in the furture. I am keeping both clase to do no t damge the current code 
class RXUPoint  
{
public:
//!Constructor
	RXUPoint();
	//!Constructor by copy
	RXUPoint(const RXUPoint & p_Obj);

	//!Destructor
	virtual ~RXUPoint();
	void Init();
	int Clear();

	RXUPoint& operator = (const RXUPoint & p_Obj);

	int Set(const double &p_t, const RXTRObject * p_pCurve,const int sense =0);

	int IsValid() const;
	
	double Gett() const;
	
	ON_3dPoint GetPos() const;
	ON_3dVector GetTangent() const;
	
	double GetLength(double fractional_tolerance = 1.0e-8) const;
	ON_Interval Domain() const;
	
	int SetToMin();
	int SetToMid();
	int SetToMax();
	int Print(FILE *fp) const;
protected:
	//!pointer on the curve where the point is located 
	//!(stored as an integer, the user can try giving anything. RXUPoint will take care of the consistency of the pointer
	const RXTRObject * m_pCurve;
// how about  	const RXTRObject& m_rCurve;
	//!position on the curve
	double m_t;

private:
	int m_sense;
};

#endif // !defined(AFX_RXUPoint_H__34B6764A_ABBD_4907_B5C3_4BDEF35DCD64__INCLUDED_)
