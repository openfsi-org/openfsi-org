/*    SUBROUTINE fspline(Pin,ng,p,npo)
      IMPLICIT NONE
      REAL*4 Pin(3,*)
      INTEGER ng
        INTEGER npo

      REAL*4 p(3,*) */

#ifndef SPLINE_16NOV04
#define SPLINE_16NOV04

EXTERN_C void rxfcspline_(const VECTOR* pin,const int cin,VECTOR* p,const int co);

#endif //#ifndef SPLINE_16NOV04


