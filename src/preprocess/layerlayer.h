// layerlayer.h: interface for the layerlayer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LAYERLAYER_H__A5E7E9E3_B8FD_46DC_9B58_9E58B0AE1EFC__INCLUDED_)
#define AFX_LAYERLAYER_H__A5E7E9E3_B8FD_46DC_9B58_9E58B0AE1EFC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "layerobject.h"
#include "layervertex.h"
class layerply : public layerobject  
{
public:
	int RemoveEdgePoints(ON_SimpleArray<layernode*> *pts);
	int FindBoundaries();
	int m_level;
	layerply();
	layerply( layertst*p_lt);
	virtual ~layerply();

	ON_ClassArray<layervertex> m_vertices;	

private:
	int 	OldFindBoundarySeed(layerlink **p_seedLink);
	int FindBoundarySeed(layerlink **ll);
};

#endif // !defined(AFX_LAYERLAYER_H__A5E7E9E3_B8FD_46DC_9B58_9E58B0AE1EFC__INCLUDED_)
