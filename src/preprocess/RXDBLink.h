#pragma once


//class RXDBLinkMySQL  is superceded by class RXDBlinkQt. We originally retained it to provide some DB functionality
// with a simplified build on Windows, but it doesnt really simplify things.
// TO use this class you need to have installed libmysql manually:
// to use the Qt mysql driver you have to do the same and then re-configure Qt to compile it,


#include "RXDatabaseI.h"
#include <QMutex>
#include <string>
#ifdef linux
#include <pthread.h>
#else
#include <process.h>
#include <winsock2.h>
#endif
#ifndef NOMYSQL
#include <mysql.h>
#endif


class RXDBLinkMySQL :public RXDataBaseLogI,public RXMirrorDBI,public RXDatabaseCommon
{
public:
    RXDBLinkMySQL (void);
    virtual ~RXDBLinkMySQL (void);

public:
    int Open(const char*filename,const char*what);
    int Close (); // the delete

    int FlushMirrorTable (); // partial delete

    int QQuery(const std::string &q, DBRESULT &r,unsigned int*err); // failure ->return NEGATIVE

    // low-level work-horse insert and/or update the value of a DB item
    virtual int post_summary_value(const char*label,const char*value);
    virtual int post_summary_value(const std::string &label,const std::string &value);
    virtual int PostFile( const QString &label,const QString &fname);

    // commits the in-core contents of the RXDBI to file, called from Calc-Now.
    virtual int Write_Line  (MIRRORPTR mirrorSource);

    //searches in the DB for 'label'. returns value.
    virtual int Extract_One_Text(const char *label,char*value,const int buflength);
    virtual std::string Extract_Headers(const std::string &what); //is 'input', 'output' or 'all'

    virtual int Extract_Column(const char*fname,const char*head,char***clist,int*Nn);
    virtual int Create_Filtered_Summary(MIRRORPTR snew,char *filter);
    virtual int read_and_post_one_row(const int row, const QString runName,MIRRORPTR dest);

    int  Apply_Summary_Values() ;
    int Replace_Words_From_Summary(char **strptr, const int append);
    int RemoveAllWithPrefix(const std::string & prefix);
    virtual std::string GetFileName()const;
    virtual std::string ScriptCommand();
    void SetLast(const QString h, const QString v);
    int print_result(DBRESULT &rq);
    std::map<std::string,std::string> m_lastbuffer;
    // transfer to script format
    virtual int Write_Script (FILE *fp);
    int MakeUnique(const QString &header, QString &value) {return 0;}

    bool m_PleaseEndPump; // in RXDBLinkMySQL
private:  
    std::string GetBlobData(const std::string &label);
#ifdef RXQT
    QMutex m_cs;
    class RXDBLinkThreadLibMySQL*m_PumpThrd;
    static void *RXDBLinkThreadProcess( void* pParams );
#else
#ifndef linux
    CRITICAL_SECTION m_ cs; // a windows idea
    uintptr_t  m_Pump Thrd;
#else
    pthread_mutex_t m_ cs;
    class RXDBLinkThread LibMySQL*m_PumpThrd;
#endif
    static void  RXDBLinkThread Process( void* pParams );
#endif
#ifndef NOMYSQL
    MYSQL *conn;
#endif
    std::string m_server;// = "localhost";// "mysql-server.ucl.ac.uk";
    std::string m_user ;// = "root";//"ucabwww";
    std::string m_password;//  = "dominique";// "secret";
    std::string m_database;//  = "relaxtest";
    std::string m_table;//
    int dbgPrint ;

};

