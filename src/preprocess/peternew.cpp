/*
16 Mar 2003  some error recovery for bad materials
15/3/96 offset management now all via arclngth.c

15/10/94  Put+PS M S now picks up leftfab,rightfab from the SC 
 first time through. IE only when no psides exist */
/* 31/12/94 debug.h routines added */

#include "StdAfx.h" 
#include "RXSeamcurve.h"
#include "RXPside.h"
#include "RXRelationDefs.h"


#include "peternew.h"

RXPosOnCurve::RXPosOnCurve(const class RXOffset &o1,const class RXOffset &o2,const RXEntity_p p, const int si):
    m_off1 (o1),
    m_off2 (o2),
    ptr	(p),
    side (si)
{}
RXPosOnCurve::RXPosOnCurve():
    ptr (0),
    side (0)
{}

int Put_Pside_Mats_Safe(sc_ptr sc,std::list <class RXPosOnCurve> &mats) {
    struct LINKLIST*next;
    int i,k;
    mats.clear ();
    set<RXObject*> mylist;
    set<RXObject*>::iterator it;
    if(sc->npss){
        for(k=0;k<3;k+=2) {  // k is side
            for(i=0;i<sc->npss;i++) {
                PSIDEPTR pss = sc->pslist[i];

                mylist= pss->FindInMap(RXO_LAYER, RXO_ANYINDEX,k);// NEEDS CHECKING
                for(it=mylist.begin();it!=mylist.end();++it)
                    mats.push_back ( RXPosOnCurve(*(pss->End1off),*(pss->End2off), dynamic_cast<RXEntity_p>(*it),k ));

            }
        }
    }
    else {			  /* see if the sc has any materials attached */
        for(k=0;k<3;k+=2) {
            next = sc->mats[k];
            for(;next;) {
                cout<< "this only happens with cut-style patches (if then)"<<endl;
                mats.push_back ( RXPosOnCurve ());
                class RXPosOnCurve &p = mats.back();
                p.ptr = (RXEntity_p )next->data;
                p.m_off1.Change(L"0.00");
                p.m_off2.Change(L"100%");
                p.side = k;
                next=next->next;
            }
        }
    }
    return(mats.size());
}
int Restore_Pside_Mats(sc_ptr sc,std::list <class RXPosOnCurve> &mats) {
    int k,n;
    float ofp1,ofp2 ;

    std::list <class RXPosOnCurve> ::iterator it;

    int nold = mats.size();
    if(!nold) return(1);

    /* purpose ; np->pslist  are np->npss psides
     They may be old, and so have materials already, which is why we
  delete the old material lists before pushing the new ones.
    For each side, for each one, we examine mats to see if this
    pside falls within its offsets
  If it does, we push the material from mats onto the pside matlist */

    for(n=0;n<sc->npss;n++) {
        PSIDEPTR ps = sc->pslist[n];
        assert(ps);
        ofp1=(float)(ps->End1off->Evaluate(sc,1));
        ofp2=(float)(ps->End2off->Evaluate(sc,1));
        for(it=mats.begin ();it!=mats.end(); it++) {
            double ofm1 = it->m_off1.Evaluate(sc,1);
            double ofm2 = it->m_off2.Evaluate(sc,1);
            if(ofp1 >=ofm1 -FLOAT_TOLERANCE &&ofp2<=ofm2 +FLOAT_TOLERANCE) {
                int mside = it->side;
                it->ptr->SetRelationOf(ps,PS_LAYER_RELATION,RXO_LAYER,mside);
            }
        }
    } // forN

    return(1);
}


