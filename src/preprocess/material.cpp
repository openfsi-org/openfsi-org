/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 25 May 2003 removed the first row of the mesh ditto.  No change.
 21 May  remove line weights in the hope that it makes hoops more stable. DIDNT
    13 may 2003 display IF Global Display Materials OR the material is in the top-level script.

    4/97 . The mat window has become dangerous. I dont know why.
 So it is not displayed unless extern int g_Display_Materials;is defined
   11/3/97 include material.h moved to end of inc list. for HPUX (??)
   * 8/96 DOnt call define system options. It affects the trimmenu
  *   30/7/96 display now in its own XWindo
 *   7/3/96 now reads mass optionally in field 7
 * 15/12/95 prin tfs removed
 20/10/95 PH Valid_Hoops_Color called from the fabric card. So the colors may be set
    in any HOOPS form
   DANGER Might Not be top copy

  28/5/95 Just speed display for DELL. ignore
               29.12.94  major overhaul, including graphical display
               involves changes to (and bugs found in) nonlin.f and nlstuf.f a new file

 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 *   17.11.94 PH   linear D is based on secant stiffness at Reference Strain
 */

#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#ifndef FORTRANLINKED
#define fortpol yline_interp_ Polyline _Interp  //                     should not be usefull anymore thomas 14 06 04
#define fc_prepare_nl_mat Prepare_NL_Mat
#define uni_str ess_test_ Uni_Stre ss_Test
#endif

#include "RXSail.h" 
#include "RXQuantity.h"
#include "RXRelationDefs.h"

#include "stringutils.h"
#include "pctable.h"
#include "words.h"

#include "nlstuf.h"
#include "nlm.h"
#include "nonlin.h"

#include "global_declarations.h"

#ifdef _X
#include "OpenMat.h"
#endif

#include "material.h"


#ifndef FORTRANLINKED
void Prepare_NL_Mat(double *ex,double*fx,double*fy,double*f45,int*N,double *ref,double*nu,double*zk,double*SW,double*en,double*fn,double*es,double*fs,int*err)/* this is a dummy WRONG */ 
{
    int k;
    //cout<< "dummy fc_prepare_nl_mat "<<endl;
    for(k=0;k<*N;k++) {
        en[k]=ex[k];
        es[k]=ex[k];
        *zk=0.2;
        fn[k]=fx[k]/  *SW;
        fs[k]=f45[k]/ *SW;
    }
    return;
}

void Uni_Stress_Test(double *theta,double *etheta,int* N,double*stheta,int *retval)  {
    cout<< "dummy uni_stress_test_"<<endl;
    *stheta = (*etheta) *12.5 * (1.0+tan((*theta)/10.0));/*	 WRONG DUMMY */
    *retval=1;
    return;
}
#endif

int Draw_Cube(float X1,float Y1,float Z1,float x2,float y2,float z2)

{
#ifdef HOOPS
    HC_Open_Segment("cube");
    HC_Insert_Line(X1,Y1,Z1,X1,y2,Z1);
    HC_Insert_Line(X1,y2,Z1,x2,y2,Z1);
    HC_Insert_Line(x2,y2,Z1,x2,Y1,Z1);
    HC_Insert_Line(x2,Y1,Z1,X1,Y1,Z1);

    HC_Insert_Line(X1,Y1,Z1,X1,Y1 ,z2);
    HC_Insert_Line(X1,y2,Z1, X1,y2,z2);
    HC_Insert_Line(x2,y2,Z1,x2,y2,z2);
    HC_Insert_Line(x2,Y1,Z1,x2,Y1,z2);

    HC_Insert_Line(X1,Y1,z2,X1,y2,z2);
    HC_Insert_Line(X1,y2,z2,x2,y2,z2);
    HC_Insert_Line(x2,y2,z2,x2,Y1,z2);
    HC_Insert_Line(x2,Y1,z2,X1,Y1,z2);
    HC_Close_Segment();
#else
    cout<<"TODO:Draw_Cube"<<endl;
#endif
    return 1;
}

int Create_Mat_Graph(const char*name, double*ext, double*f,int N, float width){
    int k;
    float emin,emax,x,y,xsc,ysc;
    char buf[64];
#ifdef HOOPS

    xsc = 1000.00;  /* 7.5 / ext[N-1]; */
    ysc = 1.0 / width/1000.0 ;
    return 0;
    HC_Open_Segment(name);

    HC_Set_Text_Alignment("v");
    HC_Flush_Segment(".");
    HC_Set_Color("black");
    /*HC_Set_Text_Font("size=0.015sru, rotation=follow path, transforms=character position only");   */
    HC_Set_Text_Font("size=0.015sru");

    /* to sort out the text size correctly it will be necessary to insert it AFTER the Z scaling */

    emin = ext[0]*xsc;
    emax = ext[N-1]*xsc;
    HC_Insert_Line(emin,0.0,0.0, emax,0.0,0.0);
    HC_Insert_Text((float) ((emin+emax)/2.0),0.0,0.0,name);
    for(k=0;k<N;k++){
        x = ext[k]*xsc;
        y = f[k]*ysc;
        HC_Open_Segment("graph");
        HC_Insert_Ink(x,y,0.0);
        HC_Close_Segment();
        HC_Open_Segment("verts");
        HC_Insert_Line(x,0.0,0.0,x,y,0.0);
        HC_Close_Segment();
        HC_Open_Segment("Xlabels");
        sprintf(buf, "%3.2f", ext[k]*100.0);
        HC_Set_Text_Alignment("^");
        HC_Insert_Text(x,0.0,0.0, buf);
        HC_Close_Segment();
        HC_Open_Segment("Ylabels");
        sprintf(buf, "%3.2f", f[k]);
        HC_Set_Text_Alignment(">");
        HC_Insert_Text((float) (ext[N-1]*xsc),y,0.0, buf);
        HC_Close_Segment();
    }

    HC_Close_Segment();

    return 1;
#else
    return 0;
#endif
}

#ifdef HOOPS
int Display_Material(RXEntity_p e){ 

    /* a  Hoops shell based on uni-directional stress tests
REQUIRES nlcom to be current */
    HC_KEY shellkey;
    int N1,N2,I,J,c,mapcount;
    float emin, emax ,STEP ,smin,smax, zero=(float)0.0, zfact;
    double x,y,theta,strain,stheta;
    VECTOR*points;
    float realsmax;
    int ONE = 1;
    int Uretval,err = 0;
    char buf[128];
    struct PC_MATERIAL*ptr = (PC_MATERIAL *)e->dataptr;
    if(!ptr->npn) return(0);

    emax =(float)(ptr->en[ptr->npn-1] ); /* * 0.75); */

    emin = 0.0;
    STEP = (emax-emin)/(float)20.0;


    HC_Open_Segment_By_Key(e->hoopskey);
    HC_Flush_Contents(".","geometry,subsegments");
    HC_Open_Segment("material_display");


    N1 =  (int)(emin / STEP);
    N2 =  (int)(emax / STEP);
    c = (N2-N1+1)*(N2-N1+1);
    points=(VECTOR *)MALLOC( c*sizeof(VECTOR)); // BIG leak
    
    x = emin;
    smax=(float)0.00001;
    smin= (float) 0.0;
    c=0;
    strain=0.0 ;
    for(I=N1;I<=N2;I++) {
        y = emin;
        theta = 0.0;
        for(J=N1;J<=N2;J++) {
            x = cos(theta) *strain;
            y = sin(theta) * strain;
#ifdef FORTRANLINKED		
            uni_stress_test_(&theta,&strain,&ONE,&stheta,&Uretval); /* uses nlcom */
#else
            assert(0);
#endif

            if(Uretval==0) err=1;
            points[c].x = (float)(y*1000.0);
            points[c].y = (float)(x*1000.0);
            points[c++].z = (float)stheta;
            if(stheta > smax) smax=(float) stheta;
            if(stheta < smin) smin=(float) stheta;
            y = y + STEP;
            theta  = theta + ON_PI /2.0/20.0;
        }
        x=x + STEP;
        strain = strain + STEP;
    }
    realsmax = smax;
    HC_Show_Net_Color_Map_Count(&mapcount);
    c = N2 - N1 + 1;
    zfact = (float) 1000.0 * (emax-emin)/smax;
    for(I=0;I<c*c;I++) {
        points[I].z =points[I].z *zfact;
    }
    /* heights now from (emax-emin)/smax*smin to(emax-emin) */

    smin = 	(emax-emin)/smax*smin; smax = (emax-emin);

    shellkey=HC_KInsert_Mesh( c,c,points);
    HC_Open_Geometry(shellkey);
    for(I=0;I<c*c;I++) {
        float col;
        col = (points[I].z/(float)1000.0 - smin)/ (smax-smin)*(float)(mapcount-1);
        if(col<-0.5) col=-0.5;
        assert(col<(65000.));
        HC_Open_Vertex(I);
        HC_Set_Color_By_FIndex("faces",col);
        HC_Close_Vertex();
    }
    HC_Close_Geometry();
    RXFREE(points); points=NULL;
    HC_Set_Rendering_Options("color index interpolation=on,color interpolation=off");

    emin=emin*1000; emax=emax*1000;  smax=smax*1000;
    Draw_Cube(emin,emin,zero, emax,emax,smax);

    HC_Rotate_Object(-90.0,0.0,0.0);
    /*	 sprintf(buf,"origin (%3.2f,%3.2f,0)",emin,emin);
    HC_Insert_Text(emin,emin,0.0,buf);
   sprintf(buf,"(%3.2f,%3.2f,%f",emax,emax,realsmax);
    HC_Insert_Text(emax,emax,smax,buf); */

    HC_Set_Text_Font("size=8px");

#ifdef _DRAWINGTHEGRAPHS // ie never
    HC_Open_Segment("graphs");

    sprintf(buf,"?Include Library/material_graphs/%s/Warp",e->name);
    HC_Copy_Segment(buf,"warp");
    HC_Open_Segment("warp");
    HC_Rotate_Object(90.0,0.0,0.0);
    HC_Rotate_Object(0.0,0.0,90.0);
    HC_Close_Segment();

    sprintf(buf,"?Include Library/material_graphs/%s/Fill",e->name);
    HC_Copy_Segment(buf,"Fill");
    HC_Open_Segment("Fill");
    HC_Rotate_Object(90.0,0.0,0.0);
    HC_Close_Segment();

    sprintf(buf,"?Include Library/material_graphs/%s/Bias",e->name);
    HC_Copy_Segment(buf,"bias");
    HC_Open_Segment("Bias");
    HC_Rotate_Object(90.0,0.0,0.0);
    HC_Rotate_Object(0.0,0.0,45.0);
    HC_Close_Segment();

    HC_Scale_Object(1.0,1.0,(float) (zfact*1000.0 ));
    HC_Close_Segment(); //graphs
#endif

    HC_Close_Segment();

    HC_Close_Segment();
    if(g_Display_Materials || !(e->generated)) {
#ifdef WIN32
        //	Mat_MSW_Disp( emin, emax,smin, smax, e);
#else
        {
            char colour[2560], colstring[2560];
            Graphic *g ;
            HC_Open_Segment_By_Key(e->hoopskey);
            HC_Show_One_Net_Color("faces",colour);
            HC_Close_Segment();
            g = OpenMatWindow(e->name());
            HC_Open_Segment_By_Key(g->m_ViewSeg);

            //HC_Set_Visibility("lines=off") ; //to try and get some stability
            /* the following are to (try to) make the Attributes menu work */
            HC_Open_Segment("dummy/d2/field");
            HC_Close_Segment();
            HC_Open_Segment("dummy/d2/orientation");
            HC_Close_Segment();
            HC_Open_Segment("dummy/d2/Material Type");
            HC_Close_Segment();
            HC_Open_Segment("dummy/d2/arrow");
            HC_Close_Segment();
            HC_Open_Segment("dummy/d2/interpolation surface");
            HC_Close_Segment();


            HC_Include_Segment_By_Key(e->hoopskey);
            HC_Set_Camera_By_Volume("ortho",-10000.0,10000.0,-10000.0,10000.0);
            sprintf(colstring,"windows=%s",colour);
            HC_QSet_Color ("^/*",colstring);

            HC_Set_Color("windows=white");
            HC_Close_Segment();
            Do_Resize(NULL,g);


            ptr->g = g;
        }
#endif
    }

    if(err) {
        sprintf(buf,"Trouble with material table values on\n%s", e->name());
        e->OutputToClient(buf,2);
    }


    return(1);
}

#endif
int PCM_Make_Linear_D(double d[3][3],double ex,double ey,double e45,double nuxy){

    double nuyx,facnu;
    //  printf("inside PCM_Make_Linear_D %f %f %f %f \t",ex,ey,e45,nuxy);
    if(ey <=0.0 && nuxy > 0.0) return 0;
    if(ex <=0.0) return 0;

    nuyx=nuxy*ex/ey;
    facnu=1.0-nuxy*nuyx;
    if(facnu <= 0.0) return 0;
    /* old formulation
  d[2][2]=1.0/(4.0/e45-(1.0-nuxy)/ex-(1.0-nuyx)/ey);
  d[0][0]=ex/facnu;
  d[1][0]=ey*nuyx/facnu;
  d[1][1]=ey/facnu;
  d[0][1]=ex*nuxy/facnu;
  d[0][2] = 0.0;
  d[2][0] = 0.0;
  d[1][2] = 0.0;
  d[2][1] = 0.0;
  */
    // new formulation
#define Power(a,b) pow(a,b)
    double d11,d22,d33,  nsq = nuxy * nuxy;
    d11 = (ex*ey)/(ey - ex*Power(nuxy,2));
    d22 = Power(ey,2)/(ey - ex*Power(nuxy,2));
    d33 = (e45*ex*ey)/(4.*ex*ey - e45*(ex + ey) + 2.*e45*ex*nuxy);
    d[2][2]= d33;
    d[0][0]= d11;
    d[1][0]=nuxy*d11;
    d[1][1]=d22 ;
    d[0][1]=nuxy*d11;
    d[0][2] = 0.0;
    d[2][0] = 0.0;
    d[1][2] = 0.0;
    d[2][1] = 0.0;

#ifdef NEVER
    printf("  Make_Linear_D ex=%f ey=%f  e45 =%f  nuxy=%f  \n",ex,ey,e45,nuxy);
    printf("  %f  \t  %f  \t  %f  \n",	d[0][0],d[0][1],d[0][2]);
    printf("  %f  \t  %f  \t  %f  \n",	d[1][0],d[1][1],d[1][2]);
    printf("  %f  \t  %f  \t  %f  \n\n",	d[2][0],d[2][1],d[2][2]);
    cout<< " 2008 formulation see QA/material matrix.nb \n LOOKS LIKE e45 is wrong.\n"<<endl;

    printf("  %f  \t  %f  \t  %f  \n",	d11,nuxy*d11,0.);
    printf("  %f  \t  %f  \t  %f  \n",	nuxy*d11,d22,0.);
    printf("  %f  \t  %f  \t  %f  \n\n",	0.,0.,d33);
#endif

    return(1);
}

int Compute_Material_Card(RXEntity_p e) {
    int i;
    char *Str, buf[256];
    const char *what[3] = {"$psxx","$psyy","$psxy"};
    char**List=NULL;
    int N,whichway,err;
    const char*headers[]={"extension","warp","fill","bias" };
    int nh=4;
    double  ex=0.0,ey=0.0, e45=0.0,grad =0.0,nu=0.0;

    const char *t;
    struct PC_MATERIAL *ptr =  ( struct PC_MATERIAL *) e->dataptr;

    if(!e->NeedsComputing()) return(1);

    if (!ptr->linear)  {
        char *table=(char *)MALLOC((strlen(e->GetLine())+1) *sizeof(char));

        t = strrchr(e->GetLine(),':');
        if(t) t++;
        else	return(0);
        strcpy(table,t);
        if(!PCT_Read_Table_From_Text(table,&(ptr->table))) {
            size_t n = strlen(e->name()) + strlen(table) +64;
            if(g_CurrentIncludeFile) n+= strlen( g_CurrentIncludeFile->name()) + 16;
            Str = (char *)MALLOC( n);
            sprintf(Str,"in %s\n cant convert table \n<%s>   ",e->name(),table);
            if(g_CurrentIncludeFile){
                strcat(Str," \n in file  ");
                strcat(Str, g_CurrentIncludeFile->name());
            }
            e->OutputToClient(Str,2);
            RXFREE(Str);
        }
        else { //linear


            whichway=PCT_Which_Way(ptr->table,headers,nh,e);

            if(PCT_Extract_Series(ptr->table,whichway,"extension",&N,&List)){ /* MALLOCs s */
                if (N ==1){  /* special for degenerate lists */
                    List = (char **)REALLOC(List,2*sizeof(char*));
                    List[1]=List[0]; 	List[0]=STRDUP("0.0"); 	N = 2;
                }
                if(N){
                    double *ext=NULL,*fx=NULL,*fy=NULL,*f45=NULL;
                    PCT_List_To_Double (e->Esail, &List,N,&ext,"extension");	/* frees LIST */

                    if( PCT_Extract_Series(ptr->table,whichway,"warp",&N,&List)) { /* REALLOCs s */
                        if (N ==1){  /* special for degenerate lists */
                            List = (char **)REALLOC(List,2*sizeof(char*));
                            List[1]=List[0]; 	List[0]=STRDUP("0.0"); 	N = 2;
                        }
                        PCT_List_To_Double (e->Esail,&List,N,&fx,"force");

                        if(  PCT_Extract_Series(ptr->table,whichway,"fill",&N,&List)){ /* REALLOCs s */
                            if (N ==1){  /* special for degenerate lists */
                                List = (char**)REALLOC(List,2*sizeof(char*));
                                List[1]=List[0]; 	List[0]=STRDUP("0.0"); 	N = 2;
                            }
                            PCT_List_To_Double (e->Esail  ,&List,N,&fy,"force");

                            if(PCT_Extract_Series(ptr->table,whichway,"bias",&N,&List)) { /* MALLOCs s */
                                if (N ==1){  /* special for degenerate lists */
                                    List = (char **)REALLOC(List,2*sizeof(char*));
                                    List[1]=List[0]; 	List[0]=STRDUP("0.0"); 	N = 2;
                                }
                                PCT_List_To_Double (e->Esail,&List,N,&f45,"force");

                                ptr->es=(double*)CALLOC(N+1,sizeof(double));
                                ptr->en=(double*)CALLOC(N+1,sizeof(double));
                                ptr->fs=(double*)CALLOC(N+1,sizeof(double));
                                ptr->fn=(double*)CALLOC(N+1,sizeof(double));
                                ptr->npn=N;


                                if(N > 2)
                                    ptr->linear=0;
                                else
                                    ptr->linear = 1;   /* temp. fix NOT TRUE WRONG*/
                                double Reference_Strain = e->FindExpression (L"refstrain",rxexpLocal)->evaluate();
                                double Strip_Width = e->FindExpression (L"width",rxexpLocal)->evaluate();
                                Polyline_Interp(ext,fx ,&N,&(ptr->seedx),&(Reference_Strain),&ex,&grad ,&err);

                                if(err){ sprintf(buf,"BAD MATERIAL  %s",e->name()); e->OutputToClient(buf,2);}

                                Polyline_Interp(ext,fy ,&N,&(ptr->seedx),&(Reference_Strain),&ey,&grad,&err);
                                if(err){ sprintf(buf,"BAD MATERIAL  %s",e->name()); e->OutputToClient(buf,2);}
                                Polyline_Interp(ext,f45,&N,&(ptr->seedx),&(Reference_Strain),&e45,&grad,&err);
                                if(err){ sprintf(buf,"BAD MATERIAL %s",e->name()); e->OutputToClient(buf,2);}
                                ex  = ex /Strip_Width /(Reference_Strain);
                                ey  = ey /Strip_Width /(Reference_Strain);
                                e45 = e45/Strip_Width /(Reference_Strain);

                                nu =  e->FindExpression (L"nu",rxexpLocal)->evaluate();
                                //printf("before PCM_Make_Linear_D %f %f %f %f \n",ex,ey,e45,nu);

                                i=PCM_Make_Linear_D(ptr->d,ex,ey,e45,nu);
                                if( !i) {
                                    sprintf(buf, "material %s has impossible properties\n ex=%f ey=%f nu=%f\n THIS MATERIAL WILL BE IGNORED",e->name(),ex,ey,nu);
                                    e->OutputToClient(buf,3);
                                }
                                for(i=0;i<3;i++) {
                                    char buff[256];
                                    if(e->AttributeGet( what[i],buff)){
                                        if(!ptr->m_qprestress.Set(e->Esail ,   i,TOSTRING(buff)))
                                            e->OutputToClient(buff,2);
                                    }
                                }

                                HC_Open_Segment("?Include Library/material_graphs");
                                HC_Open_Segment(e->name());
                                HC_Flush_Segment(".");
                                Create_Mat_Graph("Warp", ext, fx,N, (float) Strip_Width );
                                Create_Mat_Graph("Fill", ext, fy,N, (float) Strip_Width );
                                Create_Mat_Graph("Bias", ext, f45,N, (float) Strip_Width );
                                HC_Close_Segment();
                                HC_Close_Segment();
#ifdef FORTRANLINKED
                                fc_prepare_nl_mat(ext,fx,fy,f45,&N,&(Reference_Strain),
                                                  &(nu),&(ptr->zk),&(Strip_Width),ptr->en,ptr->fn,ptr->es,ptr->fs,&err);

#else
                                assert(0);
#endif

                                if(err) {
                                    sprintf(buf," BAD shear test on %s", e->name());
                                    e->OutputToClient(buf,2);
                                }
                                e->SetNeedsComputing(0);
                            }
                        }
                    }
                    if(ext) RXFREE(ext);
                    if(fx) RXFREE(fx);
                    if(fy) RXFREE(fy);
                    if(f45) RXFREE(f45);
                } // if N

            }
            else {
                printf("BAD data <%s>\n",e->GetLine());
                sprintf(buf," bad material test data\n%s ", e->name());
                e->OutputToClient(buf,2);
            }

        }
        if (table) RXFREE(table);

#ifdef HOOPS
        Display_Material(e); /* requires nlcom to be current */
#endif
        e->SetNeedsComputing(0);
    } /* if linear */
    else
        e->SetNeedsComputing(0);

    return(1);
}
int Resolve_Material_Card (RXEntity_p e) {
    /* material card is of type

     material: <name> :  <color> :[keyword 'wrinkle':[poisson:[width[ref strain[mass[atts]]]]]
     [ a table] */
    /* char*dummy;	*/
    char s[2024],  name[256],s3[256],s4[256],text[256],buffer[256],atts[256];

    RXExpressionI * q;
    HC_KEY key=0;
    int f; double xx;
    const char *line=e->GetLine();
    int retval=0;
    *atts=0; *s4=0;
    std::string sline( e->GetLine() );
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();
    if ( rxgetword(wds,1,name  ,256) ){
        if ( rxgetword(wds,2,text ,256)){
            struct PC_MATERIAL *ptr = (PC_MATERIAL *)CALLOC(1,sizeof(struct PC_MATERIAL));

            if( rxgetword(wds,3,s4   ,256)&&!strieq(s4,"table"))	{
                ptr->creaseable= (NULL != stristr(s4,"wrinkle") );		 if(NULL != stristr(s4,"nowrinkle") ) ptr->creaseable=0;

                if(rxgetword(wds,4,s   ,2024)&&!strieq(s,"table") ) {
                    q = new RXExpression(L"nu",TOSTRING(s), 0);

                    e->AddExpression (q);
                    e->SetRelationOf(q,aunt|spawn,RXO_EXP_OF_ENT);
                    if(! q->Resolve()) {
                        e->CClear();  return 0;}
                    if(rxgetword(wds,5,s ,2024) &&!strieq(s,"table")){
                        q = new RXQuantity(L"width",TOSTRING(s),L"m",0);
                        e->AddExpression (q);   e->SetRelationOf(q,aunt|spawn,RXO_EXP_OF_ENT);
                        if(! q->Resolve())
                        {
                            e->CClear();
                            return 0;
                        }

                        if(rxgetword(wds,6,s,2024)&&!strieq(s,"table")) {
                          //  q = new RXExpression(L"refstrain",TOSTRING(s), 0);
                            q = new RXQuantity(L"refstrain",TOSTRING(s),L"%", 0);
                            e->AddExpression (q);
                            e->SetRelationOf(q,aunt|spawn,RXO_EXP_OF_ENT);
                            if(! q->Resolve())
                            {e->CClear();  return 0;}

                            if(rxgetword(wds,7,s   ,2024)&&!strieq(s,"table")) {
                                if(rxIsEmpty(s)) strcpy(s,"0.0");
                                q = new RXQuantity(L"mass",TOSTRING(s),L"Kg/m",0);
                                e->AddExpression (q);
                                e->SetRelationOf(q,aunt|spawn,RXO_EXP_OF_ENT);
                                if(! q->Resolve())
                                {e->CClear();  return 0;}
                                if(rxgetword(wds,8,s   ,2024)&&!strieq(s,"table")) {
                                    strcpy(atts,s);
                                }
                            }
                        }
                    }
                    else  {
                        q = new RXQuantity(L"width",L"0.05",L"m",0);
                        e->AddExpression (q);
                        e->SetRelationOf(q,aunt|spawn,RXO_EXP_OF_ENT);
                        if(! q->Resolve())
                        {e->CClear();  return 0;}
                    }
                }
                else
                {
                    q = new RXQuantity(L"nu",L"0.3",L"",0);
                    e->AddExpression (q);e->SetRelationOf(q,aunt|spawn,RXO_EXP_OF_ENT);
                    if(! q->Resolve())
                    {e->CClear();  return 0;}
                }
            }
            else
                ptr->creaseable=1;

            // vrify refstrain value
            q = dynamic_cast<RXExpression*> (e->FindExpression (L"refstrain",rxexpLocal));
            if(!q) {
                q = new RXExpression(L"refstrain",L"0.005",0);
                e->AddExpression (q);
            }
            xx=q->evaluate () ;
            if(xx< FLOAT_TOLERANCE) {
                wcout<<L"material"<<e->name();
                wcout<<L" small refstrain ="<<xx<<L" so setting to 0.005" <<endl;
                q->Change (L"0.005");
            }
            // verify width value
            q = dynamic_cast<RXExpression*> (e->FindExpression (L"width",rxexpLocal));
            if(!q) {
                q = new RXQuantity(L"width",L"0.05",L"",0);
                e->AddExpression (q);
            }
            xx=q->evaluate () ;
            if(xx< FLOAT_TOLERANCE) {
                wcout<<L"material"<<e->name();
                wcout<<L" small width ="<<xx<<L" so setting to "<< FLOAT_TOLERANCE <<endl;
                q->Change (TOSTRING(FLOAT_TOLERANCE ));
            }
            ptr->text=STRDUP(text);
            f=1;
#ifdef HOOPS
            f = Valid_Hoops_Color(text);
#endif
            HC_Open_Segment("/materials");

            if (e->Esail){
                sprintf(buffer,"%s_%p", e->Esail->GetType().c_str (),e->Esail);
            }
            else
                sprintf(buffer,"%s_%p", "unknownType",e->Esail);
            HC_Open_Segment(buffer);
            key = HC_KOpen_Segment(name);
            HC_Flush_Segment(".");
            if(!f) {
                sprintf(s3,"material <%s> : Colour %s not defined" ,name,text);
                e->OutputToClient(s3,2);
                HC_Set_Color("faces=periwinkle");
            }
            else {
                sprintf(s3,"faces=%s",text);
                HC_Set_Color(s3);
            }
            HC_Close_Segment();
            HC_Close_Segment();
            HC_Close_Segment();
            QString qatt;
            if(!rxIsEmpty(s4)) {
                qatt += QString(s4);
                qatt += QString(",");
            }
            if(!rxIsEmpty(atts))
                qatt+=QString (atts);

            if(e->PC_Finish_Entity("fabric",name,ptr,key,NULL,atts,line)) {
                e->hoopskey=key;    /* we lose the shells but keep the colour */
                e->SetNeedsComputing(0);	/* sb 0 if its set on GKwR */
                e->Needs_Finishing=1;
                if(!e->generated) { e->SetNeedsComputing();}	/* So we see all mats in library */
                retval=1;
            }
            else
                cout<<"mat didnt resolve"<<endl;

        //    ptr->creaseable=e->AttributeFind("wrinkle") &&!e->AttributeFind("nowrinkle");
        }
    }
    e->Needs_Resolving=(!retval);
    return(retval);
} 

#if defined( WIN32) && defined(HOOPS)
int Mat_MSW_Disp(float emin,float emax,float smin,float smax, RXEntity_p e){
    float x;

    smax = (smax-smin); /* to fool the perspective cameras */
    smin = -smax;
    x = emax;
    emax = emax*(float) 1.5 - emin*(float) 0.5;
    emin = emin*(float) 1.5 -  (float) ( x* 0.5);
    HC_Open_Segment("material_Disp");
    HC_Set_Window(-1.0,-.1,-1.0,1.0);
    HC_Set_Color("lines=black,edges=grey,text=black, windows=yellow");
    HC_Set_Visibility("lines=on,faces=on,markers=off,edges=mesh quads only");
    HC_Open_Segment("text");
    HC_Set_Camera_By_Volume("stretched",-1.0,1.0,-1.0,1.0);
    HC_Insert_Text(0.0,0.95,0.0,e->name());
    HC_Close_Segment();
    HC_Open_Segment("one");
    HC_Set_Window(-1.0,0.0 ,-1.0 ,0.0);
    HC_Set_Camera_By_Volume("ortho",emin,emax,(smin*0.6+smax*0.4),smax);
    HC_Include_Segment_By_Key(e->hoopskey);
    HC_Close_Segment();
    HC_Open_Segment("two");
    HC_Set_Window(0.0,1.0, -1.0 ,0.0);
    HC_Set_Camera_By_Volume("perspective",emin,emax*1.8,smin,smax*1.4);
    HC_Include_Segment_By_Key(e->hoopskey);
    HC_Orbit_Camera((float)-150.0,(float)30.0);
    HC_Close_Segment();
    HC_Open_Segment("three");
    HC_Set_Window(-1.0,0.00, 0.0 ,0.9);
    HC_Set_Camera_By_Volume("ortho",emin,emax,(emin*0.5 + emax*0.5),emax);
    HC_Include_Segment_By_Key(e->hoopskey);
    HC_Orbit_Camera((float)0.0,(float)-90.0);
    HC_Close_Segment();
    HC_Open_Segment("four");
    HC_Set_Window(0.0,1.00, 0.0 ,0.9);
    HC_Set_Camera_By_Volume("ortho",emin,emax,(emin*0.5 + emax*0.5),emax);
    HC_Include_Segment_By_Key(e->hoopskey);
    HC_Orbit_Camera((float)90.0,(float)-90.0);
    HC_Close_Segment();
    HC_Close_Segment();

    HC_Update_Display();
    //HC_Delete_Segment("material_Disp");

    return 1;
}
#endif






