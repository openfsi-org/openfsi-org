#include "StdAfx.h"

#include "RX_FEObject.h"

RX_FEObject::RX_FEObject(void)
: m_No(RXOBJ_INITIAL_N), m_sail(NULL),m_RXFEexists(false)
{
#ifdef NO_RXFEA
	assert(0);
#error  (" no FEA allowed in this project")
#endif
#ifndef NO_FEO_UUID
	ON_CreateUuid( m_uuid );
#endif
}

RX_FEObject::RX_FEObject(class RXSail *p )
: m_No(RXOBJ_INITIAL_N), m_sail(p),m_RXFEexists(false)
{
#ifndef NO_FEO_UUID
	ON_CreateUuid( m_uuid );
#endif
}
RX_FEObject::~RX_FEObject(void)
{
 CClear();
}
int RX_FEObject::CClear(){
	return 0;
}

int RX_FEObject::GetN(void) const
{
	return  m_No;
}

bool RX_FEObject::SetN(const int n)
{
	m_No=n;
	return true;
}
void RX_FEObject::SetIsInDatabase(const bool p)
{
    m_RXFEexists=p;
}
bool  RX_FEObject::IsInDatabase(void) const
{
    return m_RXFEexists;
};
class RXSail* RX_FEObject::GetSail(void) const 

{	assert(m_sail);
	return m_sail;
}
RXSTRING RX_FEObject::TellMeAbout(void) const
{
	RXSTRING rc(_T("RXFE_Object:")); rc+= TOSTRING(GetN())+TOSTRING(L" ");
return rc;
}



