 #pragma once
 
#ifndef linux

// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//
#ifdef _MSC_VER
    #define MICROSOFT
#endif

#if _MSC_VER >= 1300
#if defined _M_IX86
// peter removed for qmake-generated MSVC project 
//#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#endif
#ifdef _MSC_VER
         #define WINVER          0x0600
        #define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
        #include <afxwin.h>         // MFC core and standard components
        #include <afxext.h>         // MFC extensions

        #ifndef _AFX_NO_AFXCMN_SUPPORT
            #include <afxcmn.h>			// MFC support for Windows Common Controls
        #endif // _AFX_NO_AFXCMN_SUPPORT

        #define WM_NEW_STREAM_FILE_BUFFER WM_USER+0x100
        #define WM_READING_THREAD_COMPLETED WM_USER+0x101
        #define WM_READING_THREAD_COMPLETED_LIGHT WM_USER+0x102
#elif defined( __GNUC__)
    #define max(x,y)  ((x) > (y) ? (x) : (y))
    #define min(x,y)  ((x) < (y) ? (x) : (y))
    #include <windows.h> //gnu version
    #ifdef UNICODE
        #define _T(x) L ## x
    #else
        #define _T(a)  a
    #endif
#endif

    // standard includes
    #include <assert.h>
    #include <shlobj.h>

    #define bool2BOOL(trueOrfalse)	\
        ( trueOrfalse == true ) ? TRUE : FALSE

    #define BOOL2bool(TRUE_Or_FALSE)	\
        (( TRUE_Or_FALSE == TRUE ) ? true : false)
    #define streq(a,b)   ( a && b &&  (0==strcmp(a,b)))
    #define strieq(a,b) (a && b &&  ( 0==_stricmp(a,b)))
    #ifndef RXEDEV //  nasty
        #include "debug.h"
    #endif
	#include "RXException.h"
	#ifndef linux
        #include "..\stringtools\stringtools.h" // defines FromUtf8 and ToUtf8
	#else
        #include "../../stringtools/stringtools.h"
	#endif

typedef enum {RX_GLOBAL, RX_MODELSPACE, RX_REDSPACE,RX_BLACKSPACE} RX_COORDSPACE;

 #else
#ifdef _MSC_VER
    #include <afx.h>
    //If you do not use the '#define _CRTDBG_MAP_ALLOC' statement, defined in 'crtdbg.h', included in 'afx.h', which is by default included in your 'stdafx.h', the memory leak dump would look like this
#endif

#ifdef HOOPS
    #include "hc.h"
    #include "HTools.h"
#endif
#include <assert.h> 
#include <iostream>
#include "debug.h"
#include "RXException.h"
#include "stringtools.h"

typedef enum {RX_GLOBAL, RX_MODELSPACE, RX_REDSPACE,RX_BLACKSPACE} RX_COORDSPACE;

#ifdef UNICODE
	#include "mtlinux.h"
#endif

#define striistr(a,b) stristr(a,b)
#define _finite(x) ( !isnan(x) && !isinf(x) )
#if !defined (_X) && !defined(RXQT)
    #define Widget void*
#endif

#endif
#ifndef strieq
#define	 strieq(a,b)  (_stricmp(a,b)==0)
#endif
#ifndef streq
#define	 streq(a,b)  (strcmp(a,b)==0)
#endif




