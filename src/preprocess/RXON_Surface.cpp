// RX_ONSurface.cpp PHA overrides for the ON NURBS surface


 
#include "StdAfx.h"
#include <iostream>
#include <sstream> 
#include "rxON_Extensions.h"

typedef	struct {float x, y, z;}	MyPoint;
#define	MYHC_POINT MyPoint
#ifndef max
#define max(x,y)  ((x) > (y) ? (x) : (y))
#define min(x,y)  ((x) < (y) ? (x) : (y))
#endif
#include "opennurbs.h"

#include "RXON_3dPoint.h"
#include "RXON_Surface.h"
#include "RXException.h"


RXON_NurbsSurface::RXON_NurbsSurface(void)
{
m_im.Empty(); 
m_jm.Empty(); 
m_is.Empty();
m_js.Empty();

}
RXON_NurbsSurface::RXON_NurbsSurface(const ON_NurbsSurface &src)
: ON_NurbsSurface(src)
{
m_im.Empty(); 
m_jm.Empty(); 
m_is.Empty();
m_js.Empty();

}


RXON_NurbsSurface::~RXON_NurbsSurface(void)
{
}



ON_BOOL32 RXON_NurbsSurface::GetLocalClosestPoint( const ON_3dPoint& q, // test_point
        double p_u0,double p_v0,     // seed_parameters
        double* p_pu,double* p_pv,   // parameters of local closest point returned here
        const ON_Interval* p_d1, // first parameter sub_domain
        const ON_Interval* p_d2 ,  // second parameter sub_domain
        const double p_tol_uv	// is this in cartesian or parameter space??
        ) const
{



// 1 find the closest CVs to  q
//2 try to see if the CV list is continuous or clustered.
// if it is clustered we need to test each cluster.
//3 ) for each cluster, do a geometric search for the perpendicular pt
// 4) return the point which is closest to q.
	RXON_NurbsSurface::GuessStartReturnType gsrv;
	ON_Interval d1,d2;
	gsrv=ok;
	if(p_d1)  d1= *p_d1;
	else d1=Domain(0);

	if(p_d2)  d2=*p_d2;
	else d2=Domain(1);

	if(!ON_IsValid(p_u0) || !ON_IsValid(p_v0))
		gsrv =Guess_Start(this,q,&p_u0,&p_v0);
	if(gsrv==hitcorner)
		{*p_pu = p_u0; *p_pv = p_v0; 		return true;}

	if(p_u0 < d1.Min()) p_u0= d1.Mid();
	else if(p_u0 > d1.Max()) p_u0= d1.Mid();
	if(p_v0 < d2.Min()) p_v0= d2.Mid();
	else if(p_v0 > d2.Max()) p_v0= d2.Mid();

	double u = p_u0;
	double v=p_v0;

	ON_3dPoint p;
	ON_3dVector tu,tv,dx,tuu,tuv,tvv;
	ON_2dVector lhs,rhs,errRhs,errLhs;
	ON_Matrix a=ON_Matrix(2,2);
	int i;
	double u_last= u, v_last= v;
	double limdu,limdv,du=0,dv=0,damp,efac,ldu,fac,ltu,ltv;
	static int hints[2]= {0,0	};

	int side, closed[2];
	closed[0] = IsClosed(0);
	closed[1] = IsClosed(1);
 
// limits on step in u and v.
	limdu = d1.Length()*0.05;
	limdv = d2.Length()*0.05;

// calcualting the side;	
                       //         0 = default
                       //         1 = from NE quadrant
                       //         2 = from NW quadrant
                       //         3 = from SW quadrant
                       //         4 = from SE quadrant


	if(p_u0 < d1.Mid()){ // west
		if(p_v0 < d2.Mid()) // south
			side = 3;
		else			
			side = 2;
	}
	else {					//east
		if(p_v0 < d2.Mid()) // south
			side = 4;
		else			
			side = 1;
	}

	for(damp=1.;damp>=1./16.; damp=damp/2.){
		u = p_u0;
		v = p_v0; du=0; dv=0;
		int ilim = (int)(256./damp);
		for (i=0;i<ilim;i++) {
		//	Ev1Der(u,v,p,tu,tv,0,hints);
			if(!Ev2Der(u,v,p,tu,tv,tuu,tuv,tvv,side,hints))
				{ printf(" (GLCP) ev2der fails on uv= %f %f\n",u,v);  return 0; }
			dx=q-p;
			ltu = tu.LengthSquared();
			ltv = tv.LengthSquared();
			int NeedToGoAgain ;
			int count=1;
			do{
			NeedToGoAgain=0;

			a[0][0]				= ltu; // ON_DotProduct(tu,tu);
			a[1][1]				= ltv;// ON_DotProduct(tv,tv);
			a[1][0] = a[0][1]	= ON_DotProduct(tu,tv);	
			int PleaseInvert=1;
			if(ltu < RXON_DEFAULT_PARAMETER_TOLERANCE*RXON_DEFAULT_PARAMETER_TOLERANCE) { // short tu so solve for dv.
					if(ltv < RXON_DEFAULT_PARAMETER_TOLERANCE*RXON_DEFAULT_PARAMETER_TOLERANCE)
						{ 
							//printf("(GLCP) jacobian is singular dx= %g %g %g\n",dx.x,dx.y,dx.z);
							if(dx.LengthSquared() < 1.e-10){ // WRONG. hard-coded. 
								*p_pu= u;
								*p_pv =v;
								return true;
							}
							return false;}// 	;
					rhs.x	= 0.;		rhs.y	= ON_DotProduct(dx,tv);
					lhs.x	= 0.;		lhs.y =  rhs.y/ltv;
					a[1][1]	=1./a[1][1]; a[1][0] = a[0][1]	= a[0][0]=0;
					tuu.x=tuu.y=tuu.z=tuv.x=tuv.y=tuv.z=0;	PleaseInvert=0;
			} 
			if(ltv < RXON_DEFAULT_PARAMETER_TOLERANCE*RXON_DEFAULT_PARAMETER_TOLERANCE) { // short tv so solve for du.
					rhs.x	= ON_DotProduct(dx,tu);		rhs.y	= 0;
					lhs.x	= rhs.x /ltu ;				lhs.y = 0.0 ;
					a[0][0]	=1./a[0][0]; a[1][0] = a[0][1]	= a[1][1]=0.;
					tvv.x=tvv.y=tvv.z=tuv.x=tuv.y=tuv.z=0;	PleaseInvert=0;
			}
			if(	PleaseInvert) {
					rhs.x				= ON_DotProduct(dx,tu);
					rhs.y				= ON_DotProduct(dx,tv);
					if(!a.Invert(RXON_DEFAULT_PARAMETER_TOLERANCE* RXON_DEFAULT_PARAMETER_TOLERANCE)){
						cout<< "(GLCP) inversion failed"<<endl; return false;}
					lhs.x = a[0][0] * rhs.x  +  a[0][1] * rhs.y;
					lhs.y = a[1][0] * rhs.x  +  a[1][1] * rhs.y;
			}
			lhs=lhs *damp;
// error in rhs
			errRhs.x	= ON_DotProduct(tu,tuu) * lhs.x * lhs.x/2.
						+ ON_DotProduct(tu,tuv) * lhs.x *lhs.y
						+ ON_DotProduct(tu,tvv) * lhs.y *lhs.y/2.;
			errRhs.y	= ON_DotProduct(tv,tuu) * lhs.x * lhs.x/2.
						+ ON_DotProduct(tv,tuv) * lhs.x *lhs.y
						+ ON_DotProduct(tv,tvv) * lhs.y *lhs.y/2.;
			errLhs.x = a[0][0] * errRhs.x  +  a[0][1] * errRhs.y;
			errLhs.y = a[1][0] * errRhs.x  +  a[1][1] * errRhs.y;

			ldu = lhs.LengthSquared();
			if(ldu > RXON_DEFAULT_PARAMETER_TOLERANCE * RXON_DEFAULT_PARAMETER_TOLERANCE) {
				efac =  sqrt(errLhs.LengthSquared()/ldu); //more aggressive without the sqrt
#define D	 0.5 // factor when efac=1 Lower value = safer;
#define A	D/(1.-D)
				fac = A/(A + efac);
				lhs =lhs *fac;
			}
			lhs.x = max(-limdu,min(limdu,lhs.x)); 	lhs.y = max(-limdv,min(limdv,lhs.y)); 
			du=lhs.x; 	dv=lhs.y;
#ifdef GOODOLDCODE
			if(v+dv<= d2.Min()-ON_EPSILON ||v+dv>= d2.Max()+ON_EPSILON )
				{ltv= tv.x=tv.y=tv.z=0; NeedToGoAgain=1;}
			if(u+du<= d1.Min()-ON_EPSILON ||u+du>= d1.Max()+ON_EPSILON )
				{ltu= tu.x=tu.y=tu.z=0; NeedToGoAgain=1;}
#else
			if(v+dv<= d2.Min()-ON_EPSILON  )
				{ltv= tv.x=tv.y=tv.z=0; NeedToGoAgain=1; v =d2.Min(); }
			if(v+dv>= d2.Max()+ON_EPSILON )
				{ltv= tv.x=tv.y=tv.z=0; NeedToGoAgain=1; v = d2.Max(); }
			if(u+du<= d1.Min()-ON_EPSILON  )
				{ltu= tu.x=tu.y=tu.z=0; NeedToGoAgain=1; u = d1.Min(); }
			if(u+du>= d1.Max()+ON_EPSILON )
				{ltu= tu.x=tu.y=tu.z=0; NeedToGoAgain=1;u = d1.Max();  }

#endif

			}while(NeedToGoAgain &&count--);

#undef _CYCLICOK
#ifdef _CYCLICOK
			u = ONPH_IncrementParm(closed[0], u,du*damp,&d1,&du);
			v = ONPH_IncrementParm(closed[1], v,dv*damp,&d2,&dv);
			if(fabs(du)<  p_tol && fabs(dv)<  p_tol){
#else
			u=u+du;
			v=v+dv;
			if(u < d1.Min()) {
				u= d1.Min();
			}
			else if(u > d1.Max()) {
				u= d1.Max();
			}
			if(v < d2.Min()) {
				v= d2.Min();
			}
			else if(v > d2.Max()) {
				v= d2.Max();
			}
			if( fabs(u-u_last)<  p_tol_uv && fabs(v-v_last)<  p_tol_uv){
#endif
				*p_pu= u;
				*p_pv =v;
				return true;
			}
			u_last=u; v_last=v;
			} // I loop
	//printf("find nearest on surface reducing damp %f\n",damp);
		} // damp loop
		wstringstream  buf;
		buf << "find nearest RXON surface not converged du = ("<<du<<" "
			<<dv<<") at q= "<<q.x<<","<<q.y<<","<<q.z<<" (tol="<<p_tol_uv;
		RXTHROW(buf.str()   );

  return false;
}

/*
enum GuessStartReturnType {  
   ok,
   error,           
   hitcorner
};
*/

RXON_NurbsSurface::GuessStartReturnType RXON_NurbsSurface::Guess_Start(const ON_Surface*t,const ON_3dPoint &q, double*p_u0,double *p_v0) const
{ // simple fn to find the nearest CV and return the greville abscissae
	ON_NurbsSurface l_n ;
	RXON_NurbsSurface::GuessStartReturnType rc=ok;
	if(! t->GetNurbForm (l_n))
		return error;
	int c0= l_n.CVCount(0);
	int c1= l_n.CVCount(1);
	int i,j,iout=0,jout=0;
	double distsq,x;
	ON_3dPoint vp;
	l_n.GetCV(0,0,vp);
		ON_3dVector d = q - vp;
		distsq=d.LengthSquared();
	for(i=0;i<c0;i++) {
		for(j=0;j<c1;j++) {
		//		l_n.GetCV(i,j,vp);
			double *v = l_n.CV(i,j);
		//	ON_3dVector d = q- vp;
		//	x=d.LengthSquared();
			x = (pow((*v)-q.x,2) + pow((*(v+1))-q.y,2) + pow((*(v+2))-q.z,2));
			if(x<distsq) {
				distsq=x;
				iout=i;
				jout=j;
			}
		}
	}
	if(distsq < 1.e-9) {
		if(iout==0 &&jout==0) 
			{ *p_u0 = Domain(0).Min(); *p_v0 = Domain(1).Min();	return hitcorner;}
		if(iout==0 &&jout==c1-1)
			{ *p_u0 = Domain(0).Min(); *p_v0 = Domain(1).Max();	return hitcorner;}
		if(iout==c0-1 &&jout==0)
			{ *p_u0 = Domain(0).Max(); *p_v0 = Domain(1).Min();	return hitcorner;}
		if(iout==c0-1 &&jout==c1-1) 
			{ *p_u0 = Domain(0).Max(); *p_v0 = Domain(1).Max();	return hitcorner;}

	}
	double *g=(double*)onmalloc((l_n.CVCount(0)+l_n.Order(0))*sizeof(double));
	if(l_n.GetGrevilleAbcissae(0,g)) {
		*p_u0=g[iout];
	}
	else
		rc=error;
	onfree(g);
	g= (double*)onmalloc((l_n.CVCount(1))*sizeof(double));
	if(l_n.GetGrevilleAbcissae(1,g)) {
		*p_v0=g[jout];
	}
	else
		rc=error;
	onfree(g);

// shimmer a bit away from the ends;
	*p_u0 = *p_u0 *0.99 + l_n.Domain(0).Mid() *0.01;
	*p_v0 = *p_v0 *0.99 + l_n.Domain(1).Mid() *0.01;
	return rc;
}


int RXON_NurbsSurface::Flow(const ON_Curve * p_c1, const ON_Curve * p_c2)
{
	int rc=0;
	int i,j;
	RXON_3dPoint p;
	for(i=0;i<CVCount(0);i++) {
		for(j=0;j<CVCount(1);j++) {
			GetCV(i,j,p); //printf(" CV %2d %2d ) \t %f\t%f\t%f",i,j,p.x,p.y,p.z);
			p = p.Flow(p_c1,p_c2);
			SetCV(i,j,p);  //printf(" \t becomes\t %f\t%f\t%f\n",p.x,p.y,p.z);
			rc=true;
		}		
	}
	return rc;
}

HC_KEY RXON_NurbsSurface::DrawHoops(const char *seg, const char *atts) const
{
	HC_KEY key=0;
#ifdef HOOPS
	int i,j,k;
	int u_degree	= Degree(0);
	int v_degree	= Degree(1);  
	int  u_cnt		= CVCount(0);
	int  v_cnt		= CVCount(1);
	int u_kc_h		=u_cnt +Order(0);
	int v_kc_h		=v_cnt +Order(1);
	int u_kc_on		=KnotCount(0);
	int v_kc_on		=KnotCount(1);

	MYHC_POINT *points	= new MYHC_POINT[ u_cnt * v_cnt ];
	float *  weights	= new  float [ u_cnt * v_cnt ];
	float *  u_knots	= new float [ max(u_kc_h ,u_kc_on )];  
	float *  v_knots	= new float [ max(v_kc_h ,v_kc_on) ];   

	for(k=0,i=0;i<u_cnt;i++) {
		for(j=0;j<v_cnt;j++,k++) {
			ON_3dPoint l_p = CV(i,j);
			points[k].x=(float)l_p.x; points[k].y=(float)l_p.y; points[k].z=(float)l_p.z;
			weights[k]=(float)Weight(i,j);
	}
	}
// U KNots
#define L_OFFSET ((int) 1)
	for(i=0,k=0;i<L_OFFSET;i++,k++){
		u_knots[k]=(float)(Knot(0,0)-Knot(0,0));
	}
	for(i=0;i<u_kc_on;i++,k++) {
		u_knots[k]=(float)(Knot(0,i)-Knot(0,0));
	}
	for(;k<u_kc_h;k++){
		u_knots[k]=(float)(Knot(0,u_kc_on-1)-Knot(0, 0));
	}
// try normalizing the knots
	u_knots[0]=0.0;
	for(i=1;i<u_kc_h;i++) {
		if(u_knots[i]<u_knots[i-1])
			u_knots[i]=u_knots[i-1];
	}
// V KNots
	for(i=0,k=0;i<L_OFFSET;i++,k++){
		v_knots[k]=(float)( Knot(1,0)-Knot(1,0));
	}
	for(i=0;i<v_kc_on;i++,k++) {
		v_knots[k]=(float)(Knot(1,i)-Knot(1,0));
	}
	for(;k<v_kc_h;k++){
		v_knots[k]=(float) (Knot(1,v_kc_on-1)-Knot(1, 0));
	}
// try normalizing the knots
	v_knots[0]=(float)0.0;
	for(i=1;i<v_kc_h;i++) {
		if(v_knots[i]<v_knots[i-1])
			v_knots[i]=v_knots[i-1];
	}
 
	if(seg) HC_Open_Segment(seg);
	key=HC_KInsert_NURBS_Surface(u_degree,v_degree,u_cnt,v_cnt,(const MyPoint *)points,weights, u_knots,v_knots) ; 
	if(seg) HC_Close_Segment();
	delete []points; 
	delete [] weights ;delete []u_knots ; delete [] v_knots ;
#endif
	return(key);
}
HC_KEY rxON_Mesh::DrawHoops(const char *seg, const char *atts) const
{
	HC_KEY key=0;
#ifdef HOOPS
	MYHC_POINT *coords=NULL;
    int *flist=NULL, pcount=0, ntri=0;
	int i,j,k;

	flist = new int[5*m_F.Count()+1];
	coords = new MYHC_POINT [this->m_V.Count()+1 ];
	for(i=0;i<this->m_V.Count() ;i++) {
		const ON_3fPoint &p = this->m_V[i];
		coords[i].x=p.x; coords[i].y=p.y;coords[i].z=p.z; pcount++;
	}
	k=0;
	for(i=0;i<m_F.Count();i++)  {
		const ON_MeshFace&f = m_F[i];int vi[4]; // vertex index - vi[2]==vi[3] for tirangles
		//if(!f.IsValid ()) continue;
		if(f.IsTriangle()){
			flist[k++]=3;
			for(j=0;j<3;j++){
				flist[k++]=f.vi[j];
			}
		}
		else if (f.IsQuad ()){
			flist[k++]=4;
			for(j=0;j<4;j++)
				flist[k++]=f.vi[j];
		}
  
   } // for i
	if(seg) HC_Open_Segment(seg);
		key = HC_KInsert_Shell(pcount,coords,k,flist);
//	key=HC_KInsert_NURBS_Surface(u_degree,v_degree,u_cnt,v_cnt,(const MyPoint *)points,weights, u_knots,v_knots) ; 
	if(seg) HC_Close_Segment();
	delete[] coords; 
	delete[] flist;
#endif
	return(key);
}

HC_KEY RXON_NurbsSurface::DrawNormals(const char *seg, const char *atts) const
{
	HC_KEY key=0;
#ifdef HOOPS
	int  nu=7,nv=7;
	double u,v,du,dv;
	du = Domain(0).Length()/(double) (nu-1);
	dv = Domain(1).Length()/(double) (nv -1);
	ON_3dPoint p; ON_3dVector n;
	if(seg) key=HC_KOpen_Segment(seg);
		HC_Flush_Contents(".","lines");
		for(u  = Domain(0).Min();u<=Domain(0).Max();u+=du) {
			for(v = Domain(1).Min();v<=Domain(1).Max();v+=dv) {
				p = PointAt(u,v);
				n=NormalAt(u,v)*0.1;
				ON_Line l = ON_Line(p,p+n);
				HC_Insert_Line(l.from.x, l.from.y,l.from.z,l.to.x,l.to.y,l.to.z );
			}
		}
	if(seg) HC_Close_Segment();
#endif
	return(key);
}//RXON_NurbsSurface::DrawNormals



int RXON_NurbsSurface::Intersect(const ON_Line & p_line, 
		double & p_t, 
		double & p_u1, double & p_u2, 
		const double p_Tol)
const
{
	/*
The method:
 1) do an intersection of the line and this' BB. 
 If it returns false, then there is no intersection 
 If it returns true, there MAY be some intersections between t0 and t1. 
 If t0 <0, set it to 0. If t1>1, set it to 1.
 WE ARE ONLY INTERESTED IN THE FIRST ONE.
	so we:
	1) Do a find-nearest on t0. Note which side it is. 
	2) from the normal vector, calculate the updated t0.
	3) repeat until the perp vector is very short. 
		if resulting t0 is between 0 and 1, we've got an intersection, which is probably the first one.

	*/
	double t0,t1, u0=ON_UNSET_VALUE,u1=ON_UNSET_VALUE ;
	double dl,dt;
	ON_3dPoint p0,p1; // points on the line
	ON_3dPoint f0;		//footprints of p0 on the surf.
	ON_3dVector n0;		// normal.  
	ON_3dVector linedir = p_line.Tangent();  //  normalized
	int rc=1;
	double linelength = p_line.Length();
	double tol=p_Tol /linelength;
	int count=100;
	double b1[3],b2[3];
	if(!GetBBox(b1,b2)) 
		return 0;
	rxON_BoundingBox theBB = rxON_BoundingBox(ON_3dPoint(b1),ON_3dPoint(b2));
	if(!theBB.Intersection(p_line,&t0,&t1))
		return 0;
	t0 = max(0,t0); t1 = min(1.,t1);

	do {
		p0 = p_line.PointAt(t0);
		if(!GetLocalClosestPoint(p0,u0,u1,&u0,&u1))
			return 0;
		f0=PointAt(u0,u1);
		n0 = NormalAt(u0,u1);
		dl = ON_DotProduct(linedir,f0-p0); 
		dt = dl/linelength;
		t0 += dt;
	}while((fabs(dt) > tol) && count--);
	p_t = t0; p_u1=u0; p_u2 = u1;
	rc = (count>0);
	if(u0 > Domain(0).Max()-0.01) rc=0;
	else if(u0 < Domain(0).Min()+0.01) rc=0;
	else if(u1 > Domain(1).Max()-0.01) rc=0;
	else if(u1 < Domain(1).Min()+0.01) rc=0;
	return (rc);
}


// if a CV is coincident in X and Y with a previous one, we set its slave marker 
// and we append it the master's list of slaves.
int  RXON_NurbsSurface::SetCVRelations(const double p_tol)
{
	int i1,i2,j1,j2, rc=0;
	ON_4dPoint  p1,p2;
	m_im.Empty();
	m_jm.Empty();
	m_is.Empty();
	m_js.Empty();

	for(i1=0;i1<CVCount(0); i1++) {
	for(j1=0;j1<CVCount(1); j1++) {
		ON_NurbsSurface::GetCV(i1,j1,p1);
		for(i2=0;i2<=i1; i2++) {
		for(j2=0;j2<=j1; j2++) {
			if(i1==i2 && j1==j2)
				continue;
			ON_NurbsSurface::GetCV(i2,j2,p2);
			if(fabs(p1.x-p2.x) > p_tol)
				continue;
			if(fabs(p1.y-p2.y) > p_tol)
				continue;
// make p2 a slave of p1;
			rc +=SetIsSlave(i2,j2,i1,j1);
				i2 +=i1; j2+=j1;
		}
		}
	}
	}
	//PrintSlaves(stdout);
return rc;
}

// i1 j1 is master i2 j2 is slave
int  RXON_NurbsSurface::SetIsSlave(const int p_i1,const int p_j1,const int p_i2,const int p_j2 ){

//	int i1,j1,i2,j2; // indices in the arrays;
	//	i1 = m_im.Search(p_i1);
	//	j1 = m_jm.Search(p_j1);
	//	i2 = m_is.Search(p_i2);
	//	j2 = m_js.Search(p_j2);
//		assert(i1<0 && i2<0 && j1<0 &&j2<0);

		m_im.AppendNew()=(p_i1);
		m_jm.AppendNew()=(p_j1);
		m_is.AppendNew()=(p_i2);
		m_js.AppendNew()=(p_j2);

return 1;
}

int  RXON_NurbsSurface::IsMaster(const int p_i,const int p_j ){

	int i; 
		for(i=0;i<m_im.Count();i++) {
		if( m_jm[i]==p_j && m_im[i]==p_i)
			 return 1;
		}
		return 0;
}

int  RXON_NurbsSurface::IsSlave(const int p_i,const int p_j ){

	
#ifdef DOVEEDITOR_TR
	return 0; 
#endif

	int i; 
		for(i=0;i<m_is.Count();i++) {
		if( m_js[i]==p_j && m_is[i]==p_i)
			 return 1;
		}
		return 0;
}

int RXON_NurbsSurface::FindSlave(const int i1,const int j1,int *i2,int *j2, int &count) {

#ifdef DOVEEDITOR_TR
	return 0; 
#endif

// find an index which gives m_im[i] == i1 and m_j1[i]==j1;
	int i,c=0,found=0; 
	for(i=0;i<m_im.Count();i++) {
		if(m_im[i] == i1 && m_jm[i]==j1 )
			c++;
		if(c>count) {
			found=1; count++;	break;
		}
	}
	if(!found) return 0;
	*i2 = m_is[i];
	*j2=m_js[i];
	assert(" we have counting trouble. How to get i and j from K"==0);
	return 1;

}
int RXON_NurbsSurface::PrintSlaves(FILE *fp) {
	int i,c=0; 
	ON_TextLog l_tl(fp);

	if(! m_im.Count() ) return 0;
	c+= fprintf(fp,"%s\t%s\t%s\t%s\t%s\n", " n","m_im[i]", "m_jm[i]","m_is[i]","m_js[i]");
	for(i=0;i<m_im.Count();i++) {
		c+= fprintf(fp,"%d\t%d\t%d\t%d\t%d\n",i, m_im[i], m_jm[i],m_is[i],m_js[i]);
	}

	Dump(l_tl);
	fprintf(fp," that was RXON_NurbsSurface::PrintSlaves\n\n\n");
	return c;
}
int  RXON_NurbsSurface::SetRXCVs(const int p_k, const double x, const double y, const double z,HC_KEY surfacekey){
	MyPoint bp; 
	int rc=1,k=p_k;
	// first get i and j from K.  Then do a SetCV on it. 
	// then, find all its slaves and set their CVs too. 
	// don't forget to do the same thing for the hoops surface.
	int u_count = CVCount(1);
	div_t d = div(k,u_count);
	int i1,j1=-1,i2,j2=-1;
	ON_3dPoint cv;
	i1 = d.quot; j1 = d.rem; i2=i1; j2=j1;
	GetCV(i2,j2,cv); 
	cv.z=z;
	SetCV(i2,j2,cv); 
	bp.x=(float) cv.x; bp.y=(float)cv.y; bp.z=(float)z;
#ifdef HOOPS
	HC_Edit_NURBS_Surface_Points(surfacekey, k,  1, &bp);
#endif
	assert(k==j2+i2*CVCount(1));
// now for the slaves.
	int count=0;
	while( FindSlave(i1,j1,&i2,&j2,count)) {
		rc++;
		k=j2+i2*CVCount(0);
		GetCV(i2,j2,cv); cv.z=z;
		SetCV(i2,j2,cv); 
		bp.x=(float)cv.x; bp.y=(float)cv.y; bp.z=(float)z;
#ifdef HOOPS
		HC_Edit_NURBS_Surface_Points(surfacekey, k,  1, &bp); 
#endif
	}
	return rc;
}

int RXON_NurbsSurface::FitZ(const ON_3dPointArray &pts)
{
/*
Find values for CV Z to give a least squares fit to the points (which are (u,v,Z)
Construct an array A of dimension (np, ncv).  Evaluate a(i,j) as
fj(ui,vi)
Where Fj is a surface with all Z zero except CV J which has Z value 1.0
and (ui,vi) are the UV coordinates of point I
Construct an array Z consisting of the point Z values.

  Then RHS (dimension ncv) is -2 Transpose(A) . Z
  and matrix M (dimension ncv,ncv) is - 2 Transpose[A] . A  (we can drop the -2 )

  and the CV Z values are Inverse[M] . RHS
  */
	int i,k,np, i1,j1,i2,j2, n1,n2, ncv,rc=0;
	np = pts.Count(); ncv = CVCount(); 
	n1 = CVCount(0); n2=CVCount(1);
	ncv = n1*n2;
	double z;
	ON_4dPoint r;
 	ON_Matrix l_A	=ON_Matrix(np,ncv);
	ON_Matrix l_Z	=ON_Matrix(np,1);
	ON_Matrix l_RHS	=ON_Matrix(ncv,1);
	ON_Matrix l_M	=ON_Matrix(ncv,ncv);
	for(i=0;i<np;i++) {
		z= pts[i].z;
		l_Z[i][0] =z ;
	}
	ON_NurbsSurface l_s;
	GetNurbForm(l_s);

	i=0;
	for(i1=0;i1<n1;i1++) {	for(j1=0;j1<n2;j1++) {
	
		for(i2=0;i2<n1;i2++) {	for(j2=0;j2<n2;j2++) {// zero the Zs
			l_s.GetCV(i2,j2,r);

			r.z=0.0;  l_s.SetCV(i2,j2,r);
		} }

		l_s.GetCV(i1,j1,r);
		r.z=1.0;  l_s.SetCV(i1,j1,r);// now l_s is a surface with one bump.

		for(k=0;k<np;k++)  { 
			r =l_s.PointAt(pts[k].x,pts[k].y);
			l_A[k][i]=r.z;
		 } 
		i++;
	}}

// now we have A and Z.
	ON_Matrix at = ON_Matrix(l_A); at.Transpose();
	l_RHS.Multiply(at,l_Z);

	l_M.Multiply(  at, l_A);

	i=l_M.Invert (1e-12); //ON_DEFAULT_PARAMETER_TOLERANCE* ON_DEFAULT_PARAMETER_TOLERANCE);
	if(!i) { 
		RXTHROW(" matrix is singular");
		return 0;
	}
	l_Z.Multiply(l_M,l_RHS);

// now fill in the CV Zz

	i=0;
	for(i1=0;i1<n1;i1++) {	for(j1=0;j1<n2;j1++) {
		 GetCV(i1,j1,r);  
		 r.z=l_Z[i][0];
		 SetCV(i1,j1,r);
		rc++;
		i++;
	}}
	return rc;
}
RXON_NurbsSurface* RXON_NurbsSurface::MapTo(const ON_NurbsSurface*p_s){ // p_s has the knots,order and XY we want. 'this' has the Z we want
/*
make a copy of p_s;
sample plenty of points on 'this' (u,v,Z)
Do a FitZ of the copy to these points.
Return the copy.
  */
	RXON_NurbsSurface *rs=new RXON_NurbsSurface();
	p_s->GetNurbForm(*rs);

	ON_3dPointArray pts;
	ON_Interval l_d1 = rs->Domain (0); 
	ON_Interval l_d2 = rs->Domain (1); 
	double i,j,di,dj; ON_3dPoint p; 
	int nn;
	nn = max(max(max(CVCount(0),CVCount(1)),rs->CVCount (0)),rs->CVCount (1));
	
	nn = nn * 4.0;
	//TR 9 may 07 to do the modulus map which have a lot of points
	if (nn>=100)
		nn = 100;

	di = l_d1.Length ()/nn; dj = l_d2.Length() /nn;

	for(i=l_d1.Min();i<=l_d1.Max()+di/2.; i+=di){
		for(j=l_d2.Min();j<=l_d2.Max()+dj/2.0; j+=dj){
			p=PointAt(i,j);
			p.x =i;p.y= j;
			pts.AppendNew()=p;
	}}

	rs->FitZ (pts);

	pts.Empty();
	return rs;
}


int RXON_NurbsSurface::CountSlaves() const
{
	return m_is.Count();
}
