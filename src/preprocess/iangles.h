#ifndef _IANGLES_H_
#define _IANGLES_H_

#include "griddefs.h"

#define DO_SCS_TOO 1

EXTERN_C RXEntity_p PCIA_Close_To_Another_Offset(RXEntity_p pe,double tol);
EXTERN_C double Show_Offset_By_Entity(sc_ptr sc,RXEntity_p e,int *err);
EXTERN_C int Insert_Angle_References(RXEntity_p p1,RXEntity_p p2,sc_ptr pe);
EXTERN_C int Duplicate_Relsite(const sc_ptr sc, RXOffset *offset,RXEntity_p *e2,const double tol=0.001) ;

EXTERN_C int Remove_From_All_IA_Lists(RXEntity_p e);
int Compare_Angles( const void *va,const void *vb);
int Sort_IA( struct IMPOSED_ANGLE*ia,int N) ;

 
#endif  //#ifndef _IANGLES_H_
