#ifndef _GETGLOBAL_H_
#define  _GETGLOBAL_H_

int  Make_Directories(void);

//EXTERN_C int  GetGlobalValues(int w); Now in relaxWorld
EXTERN_C void InitGlobals();
int  Edit_Defaults_File(void) ;

EXTERN_C int Append_Defaults_File(char *string);
EXTERN_C void getcurrentdir(char *s, int *l); // for calling from fortran
EXTERN_C FILE *Open_Defaults_File(char* p_defaultsFile );
EXTERN_C FILE *Open_INI_File(char* p_File );
#ifdef HOOPS
EXTERN_C int PC_Define_Color_Name(const char*old,const char*erin,const char*ishin,const char*p_new);
#endif

#define INI_FILE  1
#define DEFAULTS_FILE  2

#endif  //#ifndef _GETGLOBAL_H_


