#pragma once

#include "opennurbs.h"
#include "opennurbs_extensions.h"

#define PCF_UNCONVERGED		0
#define PCF_OK				1
#define PCF_OFF_START		-4
#define PCF_OFF_END			-8
#define PCF_FARAWAY			-16

#define RXONC_OFFEND1	PCF_OFF_START	
#define RXONC_OFFEND2	PCF_OFF_END	
#define RXONC_ERR		-32
#define RXONC_PARALLEL	-64

bool ON_WildCardMatchNoCase(const char* s, const char* pattern); // because ON doesnt put it in a header.


class rxON_BoundingBox :
	public ON_BoundingBox
{
public:
	rxON_BoundingBox(void);
	rxON_BoundingBox(const ON_BoundingBox a);
	rxON_BoundingBox(const ON_3dPoint a, const ON_3dPoint b);
	~rxON_BoundingBox(void);
	bool  GrowAllRound(const double tol);
};

class rxON_Mesh:
	public ON_Mesh
{
public:
	rxON_Mesh(void);
	~rxON_Mesh(void);
 
	HC_KEY DrawHoops(const char*seg=0, const char *atts=0)const;
 
};


class rxON_LineCurve:
	public ON_LineCurve
{
public:
	rxON_LineCurve(void);
	rxON_LineCurve (const ON_3dPoint a,const ON_3dPoint b) ;
	rxON_LineCurve (const ON_LineCurve &n) ;
	~rxON_LineCurve(void);
	HC_KEY DrawHoops(const char*seg=0, const char *atts=0)const;
	bool Intersect( const ON_Curve &c, 
		 ON_2dPointArray &results,
		 double p_Max_Sep = 0.001, 
		 double p_SolveTol=1.0e-7,
		 const ON_Interval *p_d1= NULL,
		 const ON_Interval *p_d2= NULL) const;
 // use ONv5 getlength
	bool Offset(const double d,  rxON_LineCurve&offetcrv);
}; 

class rxON_PolyCurve:
	public ON_PolyCurve
{
public:
	rxON_PolyCurve(void);
	~rxON_PolyCurve(void);
	HC_KEY DrawHoops(const char*seg=0, const char *atts=0)const;
}; 
 
class rxON_NurbsCurve:
	public ON_NurbsCurve
{
public:
	static rxON_NurbsCurve* New();
	rxON_NurbsCurve(void);
	rxON_NurbsCurve(const ON_NurbsCurve &n);
	~rxON_NurbsCurve(void);
	HC_KEY DrawHoops(const char*seg=0, const char *atts=0)const;
	ON_Xform TrigraphAt(const double p_t) const;
// we need the following to manage m_seglengths
	  void Destroy();
	  void Initialize(void);
	  bool ReserveKnotCapacity(int desired_capacity); // safety on the saved lengths.
//	  void DestroyCurveTree(); we had this inONV4 but its an override of non-virtaul ON_Curve fn
	  ON_BOOL32 SetCV( int i, ON::point_style style, const double* Point );
	  ON_BOOL32 SetCV( int i, const ON_3dPoint& point );
	  ON_BOOL32 SetCV( int i, const ON_4dPoint& point );
	  bool Extend( const ON_Interval& domain ) ;
  ON_BOOL32 GetLength(
          double* length,
          double fractional_tolerance = 1.0e-8,
          const ON_Interval* sub_domain = NULL
          ) const;

		bool MapToSurface(const ON_Surface * p_From, 
								const ON_Surface * p_To,
								ON_Curve *       p_crv) const;

	bool GetClosestPoint(  // need peters
          const ON_3dPoint&, // test_point
          double* t,       // parameter of local closest point returned here
          double maximum_distance = 0.0,  // maximum_distance
          const ON_Interval* sub_domain = NULL // sub_domain
          ) const;
	int PC_Find_Perpendicular( double t0, double t1,
							 const ON_3dPoint& q, 
							 double *tperp, double *p_dist, double p_tol) const;
	bool Intersect(const ON_Plane & p_p,
			ON_3dPointArray &p_results,
			double p_SolveTol = 1e-7, 
			const ON_Interval *p_d = NULL,
			double * p_pseed = NULL,
			int p_even = 1)const;
	int IntersectRecursive(const ON_Plane &p_p, 
		const int k0,
		const int k1,
		const int strict_below,
		ON_3dPointArray& p_Results,
		double p_SolveTol = 1e-6) const;
	    bool Reset_Seglengths(int i=-1);
protected:
		double* m_segLengths; // vector of lengths between knots. 
							//member (0) is between knot 0  and knot 1.
							// allocate the same as m_knot
		static double PC_LocalGetLength(const ON_NurbsCurve& p_c,double t1, double t2, double tol);
		static double SecondOrderDt(const rxON_NurbsCurve &p_c,double p_t,const ON_3dPoint&p_q, int p_Degree, int p_side, int*hint);


}; //rxON_NurbsCurve
 

class rxON_PolylineCurve:
	public ON_PolylineCurve
{
public:
	static rxON_PolylineCurve* New();
	rxON_PolylineCurve(void);
	~rxON_PolylineCurve(void);
	rxON_PolylineCurve(ON_Polyline l_PL);
	// peters
		bool Intersect( const ON_LineCurve &c, 
				 ON_2dPointArray &results,
				 double p_Max_Sep = 0.001, 
				 double p_SolveTol=1.0e-7,
				 const ON_Interval *p_d1= NULL,
				 const ON_Interval *p_d2= NULL) const;
		int IsInside(const ON_3dPoint q);
		ON_Curve * Interpolate() const;
		HC_KEY DrawHoops(const char*seg=NULL, const char*atts=NULL) const;
		int FirstIntersect(const ON_Line &theline, double *tOnLine, double *tOnP);
};


class rxON_Object:
	public ON_Object
{
public:
	rxON_Object(void);
	~rxON_Object(void);
};

class rxON_String:
	public ON_String
{
public:
	rxON_String(void);
	rxON_String(const char *s);
	rxON_String& operator=(const ON_String&);
	~rxON_String(void);
	int Replace_String(const ON_String a, const ON_String b);
};

class rxONX_Model:
	public ONX_Model
{
public:
	rxONX_Model(void);
	~rxONX_Model(void);

	bool ExtractNumericPostfix(const ONX_Model_Object &o, double*x1) ;
	ON_String ExtractName(const ON_String  & p_Line);
	int CountTrailingNumerics(const wchar_t*s);
	int IncrementName(ONX_Model_Object *p_pObject,const double dx);
	int MakeNamesUnique(void);
	ON_Object * FindByName( const ON_String p_theName) const;
	ON_Object * FindByLayer(const ON_String p_theName) const;
	ON_Object * FindFirstByName( const ON_wString p_pattern) ;
	ON_Object * FindNextByName( const ON_wString p_pattern) ;
	int SortEntities( int (*compar)(const ONX_Model_Object*,const ONX_Model_Object*)  ) ;

	static int comparebyname(const ONX_Model_Object*a,const ONX_Model_Object*b);
	static int comparebytype(const ONX_Model_Object*a,const ONX_Model_Object*b);
	int m_index_findbyname;
};

 EXTERN_C  bool ONX_IsValidNameFirstChar( const wchar_t c );


// these ONCurvexxxx functions have to call specific methods for each different rxON  type.

EXTERN_C HC_KEY ONCurveDrawHoops(const ON_Curve *c, const char*seg=0, const char*atts=0); // return -1 for baddraw

extern bool ONCurveIntersect(const ON_Curve *c1,const  ON_Curve *c2, //const ON_Curve &c,  
						 ON_2dPointArray &results,
						 const double p_Max_Sep,	//used twice. Once for growing BB once to test distance off
						 const double p_SolveTol,
						 const ON_Interval * p_d1=0, 
						 const ON_Interval *p_d2=0); 
// ONCurveIntersectByGeometry returns
//  1 if an intersection found,
//  0 if no intersections found
// -1 if it thinks this interval needs subdividing

int ONCurveIntersectByGeometry(const ON_Curve *pc1,const  ON_Curve *pc2,
                      ON_2dPointArray &results,
                      const double p_Max_Sep,	//used twice. Once for growing BB once to test distance off
                      const double p_SolveTol,
                      const ON_Interval p_d1,
                      const ON_Interval p_d2);

extern bool  ONCurveIntersect(const ON_Curve *c, const ON_Plane & p_p,
						ON_3dPointArray & p_results,  // SB 3d for u,v,t
						const double p_SolveTol,
						const ON_Interval * p_d=0,// interval on p_p
						double * p_pseed=0, // default NULL
						int p_even=1 )  ;// default 1 (=even)


EXTERN_C int ONCurveIntersectByGeometry(const ON_Curve *c,const ON_Plane & p,
		ON_3dPointArray &results,
		double Solve_Tol=1e-7, 
		const ON_Interval *d1 =NULL,
		double * p_pseed=NULL,
		int p_even =1); 


// rxONConvertObject returns its argument but in the meantime it has converted its 
//member ONX_Model_Object * m_pObj;  to a rxON_xxx (if it can)
class ONX_Model_Object* rxONConvertObject( class ONX_Model_Object *p_p3dmEnt);
// peter added 
ON_DECL
int ON_SolveCubicEquation(
       double qqq, double r, double s, double t,
       double* r1, double* r2, double* r3
       );

ON_DECL
int ONPC_SolveCubicEquation(
       double qqq, double r, double s, double t,
       double* r1, double* r2, double* r3
       );
// end peter added
