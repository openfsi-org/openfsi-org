/* this is iges.c started 25/01/01

  MODS
 2009. write_multiline for long strings
  12 sept 2007 fixed the hollerith length of the word Relax in the global section,
  and fixed (worse) the 128 entities last K knot values being zero.

  BEWARE. This is the old C file,still used for Relax iges in and out.
  The RX_IgesFile class is a separate line of development
  and needs to be kept up with these fixes (and vice versa)

  A iges entity is a graphical object from a file. The file has extension .igs

  A simple iges just has a filename and a segname.
  The geometry is placed in a child of the include library
  and that child is HC_Included in the boat.
April 2003.  Can accept 1st & 3rd order surfaces.  Doesnt do weights on surfaces
because IGES sets them on control vertices and we set them on isoparms.



iges:segname:filename : */
#include "StdAfx.h"
#include <QDebug>
#include "RXEntityDefault.h"
#include "RXSail.h"
#define IGS_DEBUG (0)
#include "RXAttributes.h"
#include "entities.h"

#include "resolve.h"
#include <string>
#include "akmutil.h"

#include "stringutils.h"
#include "hoopcall.h"
#include "bend.h"
#include "nurbs.h"
#include "interpln.h"

#include "words.h"
#include "script.h"
#include "dxfin.h"

#include "finish.h"
#include "RXGraphicCalls.h"
#include "global_declarations.h"

#include "rxqtdialogs.h"

#include "iges.h"

#define IGS_INT		1
#define IGS_REAL	2
#define IGS_STRING	4
#define IGS_POINTER	8
#define IGS_LANG	16
#define IGS_LOGICAL	32 

static FILE *iges_one, *iges_two;
static int sCount,gCount,pCount;

double igesin_atof(char *lp){
    char *p;
    if((p = strchr(lp,'D'))) *p='e';
    else if((p = strchr(lp,'d'))) *p='e';
    return (atof(lp));

}

int iges_out_start() {

    char tmpname[256];
    iges_one = iges_two=NULL;
    /* in MSVC this is
errno_t _mktemp_s
  */
    strcpy(tmpname,"RLX_IGESONE_XXXXXX");
#ifndef WIN32
    if(-1 ==(int)mkstemp(tmpname))
        return 0;
#endif
    iges_one = RXFOPEN(tmpname,"w");
    strcpy(tmpname,"RLX_IGESTWO_XXXXXX");
#ifndef WIN32
    if(-1 == (int)mkstemp(tmpname))
        return 0;
#endif
    iges_two = RXFOPEN(tmpname,"w");

    sCount = iges_write_header(iges_one,"fnameH");
    gCount = iges_write_global(iges_two,"fnameG");
    return 1;
}

int iges_out_end(char *fname){
    // write iges_one and iges_two to fname then add the iges tail.
    FILE *fp = fopen(fname,"w");
    char c;
    char buf[128];
    if(!fp) return 0;
    if(!iges_one) return 0;
    if(!iges_two) return 0;
    fseek (iges_one, 0,SEEK_SET);
    fseek (iges_two, 0,SEEK_SET);
    while ((c = fgetc(iges_one)) != EOF)
        fputc(c,fp);
    while ((c = fgetc(iges_two)) != EOF)
        fputc(c,fp);
    fclose(iges_one); fclose(iges_two);

    sprintf(buf,"S%7dG%7dD%7dP%7d",sCount-1,gCount-1,2,pCount-1);
    iges_write_line(fp,buf,'T',1);
    fclose(fp);
    return 1;
}

int iges_out_128 (const char*p_Fname,const char *mname,  struct PC_NURB *n) {

    char l_fname[1024];
    char buf[96];
    int s,g,d,p=1;

    filename_copy_local(l_fname,1024,p_Fname);
    FILE *tfp=0;
    FILE *fp = RXFOPEN(l_fname,"w");

    char t[1024]; strcpy(t,"rlxIgs_XXXXXX");
#ifndef WIN32
    mkstemp(t);
#else
    qDebug()<<" temportay filenames in MSW !!";
#endif
    if(!fp) {
        cout<<"(iges write) cant open file '"<<l_fname<<"'"<<endl;
        return 0;
    }
    tfp = fopen(t,"w");
    if(!tfp) {
        cout<<"(iges write) cant open temporary file '"<<t<<"'"<<endl;
        FCLOSE(fp); return 0;}



    s = iges_write_header(fp,l_fname);
    g = iges_write_global(fp,l_fname);
    d=2;
    p = iges_write_128(fp,tfp, mname, n,1,p);
    fclose(tfp);
    tfp = fopen(t,"r");
    while(fgets(buf,82,tfp))
        fputs(buf,fp);
    fclose(tfp);
#ifdef WIN32
    sprintf(buf ,"del %s\n",t);
#else
    sprintf(buf ,"rm -f %s\n",t);
#endif
    system(buf);

    sprintf(buf,"S%7.7dG%7.7dD%7.7dP%7.7d",s-1,g-1,2,p-1);
    iges_write_line(fp,buf,'T',1);
    FCLOSE(fp);
    return 1;
}
int iges_write_line_debackptr(FILE *fp,char *bufin,char w,int ptr,int iret){
    int l;
    char buf[96];
    strcpy(buf,bufin);

    for(;;){
        l=strlen(buf);
        if(l>65) break;
        strcat(buf," ");
    }
    buf[64]=0;
    fprintf(fp,"%64s %7d%c%7.7d\n",buf,ptr,w,iret++);
    return iret;
}
int iges_write_line(FILE *fp,const char *bufin,const char w,int iret){
    int l;
    char buf[96];
    strcpy(buf,bufin);

    for(;;){
        l=strlen(buf);
        if(l>73) break;
        strcat(buf," ");
    }
    buf[72]=0;
    fprintf(fp,"%72s%c%7.7d\n",buf,w,iret++);
    return iret;
}
int iges_write_multiline(FILE *fp,const string &text,const char w,int iret){
    int k;
    string buf;
    bool stillgoing;

    stillgoing=true; k=0;
    while(stillgoing) {
        buf=text.substr(k,72); k+=72;
        if(buf.size() < 72) {
            stillgoing=false;
            buf.resize(72,' ');
        }
        fprintf(fp,"%72s%c%7.7d\n",buf.c_str(),w,iret++);
    }
    return iret;
}

int iges_write_header(FILE *fp,const char *fname){
    int iret=1;
    char buf[96];
    sprintf(buf,"File: %s",fname);
    iret = iges_write_line(fp,buf,'S',iret);
    iret = iges_write_line(fp,"RELAX output",'S',iret);
    iret = iges_write_line(fp,"",'S',iret);
    return iret;
}
int iges_write_global(FILE *fp,const char *p_fname){
    int iret=1;
    char buf[1024]; const char *lp;
    size_t flen = strlen(p_fname);
    if(flen > 48)
        lp = p_fname + flen-48;
    else
        lp= p_fname;
    sprintf(buf,"File: %s",lp);
    //	iret = iges_write_line(fp,",,2Hg3,",'G',iret);
    iret = iges_write_line(fp,"1H,,1H;,,",'G',iret);
    iges_write_Hollerith(buf,lp);
    strcat(buf,",");
    iret = iges_write_line(fp,buf,'G',iret);
    iret = iges_write_line(fp,"5Hrelax,16HRelease 04/01/98,16,6,15,13,15,   ",'G',iret);
    iret = iges_write_line(fp,"10HENTITY#128,1.,6,1HM,8,.016,15H20010815.120000  ,.0001,0.,",'G',iret);
    iret = iges_write_line(fp,"6HpeterH,12HpeterHeppel ,10,0,15H20010815.120000;       ",'G',iret);
    return iret;
}
int iges_write_Hollerith(char *buf,const char *s){
    sprintf(buf,"%dH%s",(int)strlen(s),s);
    return 1;
}
int iges_write_128(FILE *fp,FILE *f, const char *name, struct PC_NURB*n,int debackptr, int iret){/* fp, the dir, f the ent */
    int iretp=1,lu,lv,k,i,j;
    char buf[96];

    iretp = iges_write_line(fp,"     128       1       0       0       0       0       0       000000000",'D',iretp);
    if(n){
        //128, 18, 18,  3,  3,  0,  0,  0,  0,  0,                               1P0000001
        lu = n->LimitU -1;
        lv = n->LimitV -1;
        sprintf(buf,"128,%3d,%3d,%3d,%3d,%3d,%3d,%3d,%3d,%3d,%s",lu,lv,n->KValU,n->KValV,0,0,0,0,0,"                               1");
        iret = iges_write_line_debackptr(f,buf,'P',debackptr,iret);
        n->uv=NURB_U;

        double kval=Knot(n,0);
        for(k=-1;k<lu+2*n->KValU-1-1;k++) {
            kval = max((float) kval, Knot(n,k));  // August 2007 Peter this makes sure they are strictly non-reducing and catches a bug
            sprintf(buf,"    %f,",kval);

            iret = iges_write_line_debackptr(f,buf,'P',debackptr,iret);
        }
        n->uv=NURB_V;
        kval=Knot(n,0);
        for(k=-1;k<lv+2*n->KValV-1-1;k++) {
            sprintf(buf,"    %f,",Knot(n,k));
            kval = max((float) kval, Knot(n,k));
            sprintf(buf,"    %f,",kval);
            iret = iges_write_line_debackptr(f,buf,'P',debackptr,iret);
        }
        for(i=0;i<lu+1;i++){
            for(j=0;j<lv+1;j++){
                sprintf(buf,"%f,",1.00);
                iret = iges_write_line_debackptr(f,buf,'P',debackptr,iret);
            }
        }
        //	error("Switched I J in write_128",2);
        for(j=0;j<lv+1;j++){
            for(i=0;i<lu+1;i++){

                sprintf(buf,"%15.6e, %15.6e, %15.6e,",n->iP[i][j].x ,n->iP[i][j].y,n->iP[i][j].z);
                iret = iges_write_line_debackptr(f,buf,'P',debackptr,iret);
            }
        }
        iret = iges_write_line_debackptr(f,"0.000000,1.000000,0.000000,1.000000; " ,'P',debackptr,iret);
    }
    else{
        iret = iges_write_line_debackptr(f,"debug1" ,'P',debackptr,iret);
        iret = iges_write_line_debackptr(f,"debug2" ,'P',debackptr,iret);
    }
    sprintf(buf,"     128       0       0%8d       0                 %8s       0",iret-1,name);
    sprintf(buf,"%8d%8d%8d%8d%8d%8d%8d%8s%8d",128 , 0,0,iret-1,0 ,0,0,name,0);


    iretp = iges_write_line(fp,buf,'D',iretp);
    return iret;
}

HC_KEY iges_Read(const char *name,const char *filename,struct PC_iges *p,SAIL *sail) {
    int NOpen_Start,NOpen_End;
    char  ext[256], seg1[256],seg2[256];
    HC_KEY dkey;
    if (IGS_DEBUG) cout<< " Enter Read_iges\n"<<endl;
    NOpen_Start = List_Open_Segments();
    {
        char  base[256],path[256];
        rxfileparse(filename ,path,base, ext,256);
        if(rxIsEmpty(ext))	 strcpy(ext,".igs");
        cout<<"iges filename "<<path<<base<<ext<<endl;
    }
    if (strieq(ext,".igs")) {
        HC_KEY key;
        HC_Show_Pathname_Expansion(".", seg2);
        if(IGS_DEBUG) {List_Open_Segments(); cout<< " that was before igesin"<<endl;}
        key = dkey = 	igesin(filename,p,sail);
        if(IGS_DEBUG) {List_Open_Segments(); cout<< " that was after igesin"<<endl;	}
        if(key){
            HC_Open_Segment_By_Key(key);
            HC_Set_Selectability("off");
            HC_Close_Segment();

            HC_Show_Segment(key,seg1);
            if(IGS_DEBUG) printf(" %s was read into %s\n", filename,seg1);

            HC_Open_Segment(name);
            if(IGS_DEBUG) {
                HC_Show_Pathname_Expansion(".", seg2);
                printf("including %s \ninto %s\n", seg1,seg2);
            }
            HC_Include_Segment_By_Key(key);
            HC_Close_Segment();


        }
        if (IGS_DEBUG) cout<< " Leave Read_iges\n"<<endl;
        do{
            NOpen_End = List_Open_Segments(); if (IGS_DEBUG) printf(" at end there are %d open\n", NOpen_End);
            if (NOpen_End > NOpen_Start) {
                HC_Close_Segment();
                cout<< "CLOSING THE TOP ONE\n"<<endl;}
        } while(NOpen_End > NOpen_Start) ;


        return dkey;
    }
    else  {
        sail->OutputToClient(" igesin requires file extension .igs",2);
        return(0);
    }
}

int Resolve_iges_Card(RXEntity_p e) {
    // iges:name:filename[:atts[:command]]
    struct PC_iges *p=0;

    QString qline = e->GetLine();
    QStringList wds= rxParseLine(qline,RXENTITYSEPREGEXP );
    if(wds.size()<3)  wds<<"*.igs";

    QString & name=  wds[1];
    QString &fname = wds[2];
    QString atts =  wds.value(3);
    QString command = wds.value(4);

    fname = e->Esail->AbsolutePathName(fname);

    if(!VerifyFileName(fname,name,"*.igs"))
        return 0;
    fname = e->Esail->RelativeFileName(fname);
    qline = wds.join(RXENTITYSEPS);
    e->edited = !(qline ==e->GetLine());
    if(e->edited){
        e->SetLine( qPrintable(qline));
    }


    p = (PC_iges*)CALLOC( 1,sizeof(struct PC_iges));
    p->owner = e;						// Sept 2003
    p->filename = STRDUP(qPrintable(fname) );
    p->segname = STRDUP(qPrintable(name) );
    if(strstr(p->filename,"already done") == NULL) {
#ifdef HOOPS
        HC_Open_Segment("?Include Library");
        if(HC_QShow_Existence(name,"self"))
            HC_Delete_Segment(name);
        HC_Close_Segment();
#endif
        if(!command.isEmpty()   ) {
            QString buf = command + " " + fname;

            printf(" preprocess iges with '%s'\n",qPrintable(buf));
            system(qPrintable(buf));
        }
        if(!atts.isEmpty()) {
            RXAttributes rxa(qPrintable(atts)); QString dum;
            if (rxa.Extract_Word("$origin",dum)) {
                if(3==sscanf(qPrintable(dum) ," %f %f %f",&(p->origin.x),&(p->origin.y),&(p->origin.z)))
                {
                    p->Use_Origin=1;
                }
            }
        }
        e->hoopskey = iges_Read( p->segname,p->filename ,p,e->Esail);
    }
    e->SetDataPtr( p); p->owner = e;
    e->Needs_Resolving=0;
    e->Needs_Finishing=0;
    e->SetNeedsComputing();
    return(1);
}  	

int Compute_iges(RXEntity_p e) {

    HC_KEY key;
    char segname[256];
    struct PC_iges *p = (PC_iges*)e->dataptr;
    if(!e->NeedsComputing()) return(0);
    HC_Open_Segment("?Include Library");
    key = HC_KOpen_Segment(p->segname);
    HC_Close_Segment();
    HC_Close_Segment();
    HC_Show_Segment(key,segname);

    HC_Include_Segment_By_Key(key);

    return(1);
}		
HC_KEY igesin(const char *p_filename, struct PC_iges*p, SAIL *sail){
    HC_KEY rKey=0;
    p->fp=RXFOPEN(p_filename,"r");
    if(!p->fp) return 0;

    char filename[512];	char ext[512];

    // dir includes its trailing sep.  ext includes its leading dot
    rxfileparse(p_filename ,0,filename,ext, 512);
    strcat(filename,ext);

#ifndef linux
    assert(0);
#else
    HC_Open_Segment_By_Key(sail->PostProcNonExclusive());
#endif
    rKey=HC_KOpen_Segment("iges");

    HC_Flush_Contents(".","everything");

    p->lastline[80]= p->lastline[0] = 0;

    if(iges_start_section(p)) {
        if(iges_global_section(p)) { assert(p->n_globals ==25);
            if(iges_directory_section(p)) {
                if(iges_parameter_section(p)) {
                    if(!iges_terminate_section(p))
                        sail->OutputToClient(" bad IGS file ",2);
                }
            }
        }
    }
    HC_Close_Segment();
    HC_Close_Segment();
    FCLOSE(p->fp);
    return(rKey);
}


int iges_start_section( struct PC_iges*p) {

    p->prm_dlmtr[0]=p->rcrd_dlmtr[0]=0;
    strcpy(&(p->prm_dlmtr[1]),"\r\n");
    strcpy(&(p->rcrd_dlmtr[1]),"\r\n");

    while (iges_fgets( p->lastline, 82,p->fp,"\n\r")){
        if (p->lastline[72] !='S') return 1;
        if(IGS_DEBUG) printf(" %s\n",p->lastline);
    }

    return 0;

}
char *iges_deHollerith(char *lp) {
    char *out,*end ;

    // lp is presumed a hollerith. We read up to an H ,
    //convert to a int n and then read N chars past the H
    int i = (int)strtol( lp, &end, 10);
    if(*end == 'H' || *end == 'h') {
        end++;
        out = (char*)CALLOC(i+1,sizeof(char));
        strncpy(out,end,i);
        return(out);
    }

    return STRDUP(lp);
}
char * iges_parse_with_hollerith(char **lp, char delim,int c){
    //extract the c'th hollerith string from s
    // 	the strings are delimited by 'delim'
    // beware. That character might occur in a hollerith string.
    // *lp is the ptr to the next unread char on return
    int i, InHol=0, count = 0;
    char *lps=NULL;
    static char wd[256];
    *wd=0;
    do{
        // **lp might be delim
        // **lp might be white space
        // **lp might be thestartof a hollerith string.
        // **lp might be 0


        if(**lp == delim){
            count++;
            if(count > c) {
                (*lp)++;
                return wd;
            }
        }
        else
            switch (**lp){
            case 0:
                count++;
                if(count > c) {
                    return wd;
                }
                return NULL;
                break;
            case ' ':
            case '\n':
            case '\t':
            case '\r':
                break;
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case '0':
                if(!InHol) {
                    lps=*lp;
                    InHol=1;
                }
                break;
            case 'H':
                if(InHol){
                    i = atoi(lps);
                    strncpy(wd,*lp+1,i);
                    wd[i]=0;
                    (*lp)+=i;
                    InHol=0;
                    lps= NULL;

                }
                break;
            }


    } while(*((*lp)++));

    return NULL;
}
int iges_global_section(struct PC_iges*p){ 
    int i, c=-1,No_More=0;
    char  *w;
    double dum;
    p->prm_dlmtr[0]=',';
    p->rcrd_dlmtr[0]=';';
    // c = - iges_clobal_count(p);
    p->n_globals =iges_NGLOBALS;
    for(i=0;i<p->n_globals &&!No_More;i++) {
        w =iges_get_next_word(p,&c,'G',g_GlobalType[i],&No_More);

        if(i==0 && w && *w)
            p->prm_dlmtr[0]=*w;
        else if(i==1 && w && *w)
            p->rcrd_dlmtr[0]=*w;

        p->globals[i]=STRDUP(w);
    }


    if (!rxIsEmpty(p->globals[IGS_TOL])) {
        dum = igesin_atof( p->globals[IGS_TOL]);
        if((dum>0.0000001))
            PCTol_Set_Tolerances(p->owner->Esail, dum,    PCTOL_LINEAR +PCTOL_LOCAL);
        PCTol_Set_Tolerances(p->owner->Esail, dum*2.0,PCTOL_CUT    +PCTOL_LOCAL);
    }

    return 1;
}
int oldiges_global_section(struct PC_iges*p){ 
    int i, c=-1;
    char *lp,*lp2, *w;
    p->prm_dlmtr[0]=',';
    p->rcrd_dlmtr[0]=',';
    do {
        lp=p->lastline;
        if (lp[72] !='G') return 1;
        lp[72]=0;
        PC_Strip_Trailing(lp);
        if(c<0) {
            w = iges_parse_with_hollerith(&lp,',',0);
            if(w && *w)
                p->prm_dlmtr[0]=*w;
            w = iges_parse_with_hollerith(&lp,p->prm_dlmtr[0],0);  //0 because lp is incremented
            if(w && *w)
                p->rcrd_dlmtr[0]=*w;
            c=3 ;
            //lp++;
        }
        while((lp2=strchr(lp,*(p->prm_dlmtr)))){
            *lp2=0;
            strcpy(p->globals[c],lp);
            if(c<iges_NGLOBALS-1)c++;
            lp=lp2;
            lp++;
        }

        p->n_globals=c;
    } while (iges_fgets( p->lastline, 82,p->fp,"\n\r"));
    if(p->n_globals !=25) {
        for(i=0;i<p->n_globals;i++) {
            printf(" %d+1  \t%s  \t%d \n", i,p->globals[i],g_GlobalType[i]);
        }

    }

    return 1;}
int iges_directory_section(struct PC_iges*p){ 

    // consists of pairs of lines. Each one creates an entity and fills in its flags
    //parsing is fixed-format.
    int flip =-1;
    char *lp;
    char line[256];
    while(p->lastline[72] !='D')
        if(!iges_fgets( p->lastline, 82,p->fp,"\n\r")) return 0;
    do {
        lp=p->lastline;
        if (lp[72] !='D') return 1;
        lp[72]=0;
        if(flip < 0)
            strcpy(line,lp);
        else {
            strcat(line,lp);
            iges_create_entity(line,p);
        }



        flip=-flip;
    } while (iges_fgets( p->lastline, 82,p->fp,"\n\r"));
    return 1;}


int iges_parameter_section(struct PC_iges*p){
    // fill in the entities one by one.

    int i,type;
    struct IGES_ENTITY *ie;
    char *what, buf[256];

    for(i=0;i<p->ne;i++) {
#ifdef linux
        if(i>0){
            div_t  howfar = div((i ),(p->ne)/20);		// progress report
            if(howfar.rem==0) {
                sprintf(buf,"iges import %d%%",(100*(i))/p->ne);
                SetMsgText( buf, p->owner->Esail->Client());
            }
        }
#endif
        ie = &(p->elist[i]);
        iges_find_parameter_line(ie,p); // lastline is the first of the entity
        type = ie->DData[IGS_ETYPE];
        if(!strncmp(ie->Dwords[IGS_STATS],"01",2))// its invisible
            continue;
        p->JustFoundAnEOR = 0; // a flag in iges_get_next_paramtr
        switch(type){
        case 116:
            iges_read_116(ie,p);
            break;
        case 126: {
            iges_read_126(ie,p);
            break;
        }
        case 128: {
            iges_read_128(ie,p);
            break;
        }
        case 406: {
            iges_read_406(ie,p);
            break;
        }
        default:
            switch(type) {
            case 314:
                what = "Color";
                break;
            default :
                what = "unknown";
            }
            sprintf(buf," skip entity type %d, (%s)\n",type,what);
            p->owner->OutputToClient(buf,1);
            iges_read_N(ie,p);
        }
        // here we could read any associations. We may not yet be at the record delimiter.
        // But it is probably easier to call this at the end of each individual entity read.
    }
    return 1;
}
char *iges_create_attributes(struct IGES_ENTITY *ent) {// July 2003 now alters the name and splits it into atts
    char *a, *n=NULL,*l,*e, *lp, *en=NULL;
    int L;

    e = getenv(strtoupper(ent->layer));

    if(!e) {
        char buff[256];
        //	sprintf(buff," No environment set for\n '%s'\n", ent->layer);
        //	error(buff,1);
        //	sprintf(buff,"%s= blank ", ent->layer);
        //   	putenv( strdup(buff)); // linux didnt want the underscore
        sprintf(buff," envEdit:GAUSS: %s" ,strtoupper(ent->layer));
        Execute_Script_Line(buff,NULL);
        e = getenv(strtoupper(ent->layer));
        printf(" env now <%s>\n",e);
    }
    if(ent->name)
        n = STRDUP(ent->name);
    l = ent->layer;

    L=64;
    if(e) L+=strlen(e);
    if(n) L+=strlen(n);
    if(l) L+=strlen(l);
    a =(char*)MALLOC(L);

    if((lp = strpbrk(n,IGS_NAME_SEPARATOR))){ // to just before the first $. () OK for entity name but NOT Hoops name
        en = lp; en++;
        *lp=0;
    }


    strcpy(a,"$iges ");
    if(n) {strcat(a,"($name="); 	strcat(a,n); 	strcat(a,"),");}
    if(l) { strcat(a,"($layer="); 	strcat(a,l); 	strcat(a,"),");}
    if(en) { strcat(a,"N>> "); 	strcat(a,en); 	}
    if(e) {strcat(a," , E> "); 	strcat(a,e);}

    assert(strlen(a) < (size_t) (L-1));
    if(n) RXFREE(n);

    if((lp = strpbrk(ent->name,IGS_NAME_SEPARATOR))){ // to just before the first $. () OK for entity name but NOT Hoops name
        *lp=0;
    }

    return a;
}
int iges_find_parameter_line(struct IGES_ENTITY *ie,struct PC_iges *p){
    int i,pass;
    char *lp;
    // we are looking for the line ending 'P0000ie->start'
    // lastline is the first to check
    // return 1 if we found it, with the file pointer in the right place and
    //the first line of this entities parameters in lastline.

    for(pass=0;pass<2;pass++) {
        do {
            lp = &(p->lastline[72]);
            if(*lp =='P') {
                lp++;
                if(0>=sscanf(lp,"%d",&i)) i=0;
                if(i==ie->DData[IGS_PDATA])
                    return 1;
            }
        } while (iges_fgets( p->lastline, 82,p->fp,"\n\r"));
        rewind(p->fp);
        if(!iges_fgets( p->lastline, 82,p->fp,"\n\r")) return 0;
    }
    return 0;
}
int iges_read_128(struct IGES_ENTITY *ie, struct PC_iges*p){// template entity reader
    // lastline is the first of this entity
    // We will read a further ie->c lines
    // returning one parameter at a time
#define GETOUT {PCN_Free_Nurb(n); return 0;}
    struct PC_NURB *n = (PC_NURB *)CALLOC(1,sizeof(struct PC_NURB ));
    char line[256];
    RXEntity_p e;
    char *lp;
    VECTOR d_o;
    double kmin,kmax, dum;
    int i,j, prop[6], lc= - ie->DData[IGS_PCONT]; // c=1
    n->tol = 0.001;
    if (!rxIsEmpty(p->globals[IGS_TOL])) {
        dum = igesin_atof( p->globals[IGS_TOL]);
        if((dum>0.0000001))
            n->tol=dum;
    }

    d_o.x=d_o.y=d_o.z=0.0;  // found by MSWVC
    n->flags = PCN_IGES;
    lp = iges_get_next_parameter(ie, p,&lc);
    if(!lp)
        GETOUT
                i = atoi(lp); if(i!=128) GETOUT
            lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
            n->LimitU= 1 + atoi(lp);
    lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
            n->LimitV= 1 + atoi(lp);
    lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
            n->KValU  = atoi(lp);
    lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
            n->KValV =  atoi(lp);
    n->KoffU = 1; // was n->KValU-2; till april 2003 // a guess
    n->KoffV= 1; // was n->KValV-2; till april 2003
    n->Knot_Type=PCN_IGES;

    PCN_Nurbs_Alloc(n) ; // creates all the arrays
    for(i=0;i<5;i++){
        lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
                prop[i]=atoi(lp);
        if(prop[i]&&IGS_DEBUG)
            printf("prop %d is %d NOT coded\n",i,prop[i]);	// these variations arent coded
    }

    // knots in U
    //	i = n->LimitU-2+n->KValU*2;
    i = n->LimitU+n->KValU + 1;//  April 2003 for K not 3

    for(j=0;j<i;j++) { float l_f;
        lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
                l_f = 	igesin_atof(lp);
        n->m_KnotVectorU[j]= l_f;
    }
    // knots in V
    //	i = n->LimitV-2+n->KValV*2;
    i = n->LimitV + n->KValV + 1;//  April 2003 for K not 3

    for(j=0;j<i;j++) {
        lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
                n->m_KnotVectorV[j]=igesin_atof(lp);

    }

    for(i=0;i<n->LimitU;i++) {
        for(j=0;j<n->LimitV;j++) {
            lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
                    n->m_W[i][j]=igesin_atof(lp);
        }
    }
    for(j=0;j<n->LimitV;j++) {
        for(i=0;i<n->LimitU;i++) {
            lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
                    n->iP[i][j].x=(float)igesin_atof(lp);
            lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
                    n->iP[i][j].y=(float)igesin_atof(lp);
            lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
                    n->iP[i][j].z=(float)igesin_atof(lp);

            if(p->Use_Origin){

                if(!i && !j) {
                    d_o.x = p->origin.x - n->iP[i][j].x;
                    d_o.y = p->origin.y - n->iP[i][j].y;
                    d_o.z = p->origin.z - n->iP[i][j].z;
                }
                n->iP[i][j].x += d_o.x;
                n->iP[i][j].y += d_o.y;
                n->iP[i][j].z += d_o.z;
            }
        }
    }
    // now the last 4, should be the U&V limits
    // knots in U
    if((lp= iges_get_next_parameter(ie, p,&lc))){//if(!lp) GETOUT
        kmin = igesin_atof(lp);
        if((lp= iges_get_next_parameter(ie, p,&lc))){// if(!lp) GETOUT
            kmax = igesin_atof(lp);
            i = n->LimitU-2+n->KValU*2;
            for(j=0;j<i;j++) {
                n->m_KnotVectorU[j]=(n->m_KnotVectorU[j]-kmin)/(kmax-kmin);
                if(n->m_KnotVectorU[j]>1.0) n->m_KnotVectorU[j]=1.0;
                if(n->m_KnotVectorU[j]<0.0) n->m_KnotVectorU[j]=0.0;
            }
        }
    }
    // knots in V
    if((lp= iges_get_next_parameter(ie, p,&lc))){ // if(!lp) GETOUT
        kmin = igesin_atof(lp);
        if((lp= iges_get_next_parameter(ie, p,&lc))){  // if(!lp) GETOUT
            kmax = igesin_atof(lp);
            i = n->LimitV-2+n->KValV*2;
            for(j=0;j<i;j++) {
                n->m_KnotVectorV[j]=(n->m_KnotVectorV[j]-kmin)/(kmax-kmin);
                if(n->m_KnotVectorV[j]>1.0) n->m_KnotVectorV[j]=1.0;
                if(n->m_KnotVectorV[j]<0.0) n->m_KnotVectorV[j]=0.0;
            }
        }

    }
    iges_read_properties(p,ie,&lc);
    // now we have a NURBS structure


    sprintf(line,"interpolation surface: %s : NURBS: NULL: 1 : 1 : From IGES \n",ie->Dwords[IGS_ELABL]);
    i=1;
    ie->e = e = Just_Read_Card( p->owner->Esail,  line,"interpolation surface",ie->Dwords[IGS_ELABL],&i); //p->owner->Esail,
    if(e){
        if(  Resolve_Interpolation_Card (e)){
            struct PC_INTERPOLATION *pi = (struct PC_INTERPOLATION *)e->dataptr;
            e->SetNeedsComputing(0);
            pi->NurbData=n;
        }
    }

    // write a debug copy of this entity
    //	strcpy(line, ie->name);
    //	PC_Strip_Leading(line); PC_Strip_Trailing(line);
    //	strcat(line,".igs");
    //iges_out_128 (line,ie->name, n);
#undef GETOUT
    return 1;
}
int iges_read_126(struct IGES_ENTITY *ie, struct PC_iges*p){
    // lastline is the first of this entity
    // We will read a further ie->c lines
    // returning one parameter at a time
#define GETOUT {PCN_Free_Nurb(n); return 0;}
    struct PC_NURB *n = (PC_NURB *)CALLOC(1,sizeof(struct PC_NURB ));
    char *lp;
    VECTOR norm, *poly; //
    VECTOR *vlist;
    double kmin,kmax,dum;
    int i,j, prop[6], c=1,lc= - ie->DData[IGS_PCONT]; //     nrec;

    n->tol = 0.001;
    if (!rxIsEmpty(p->globals[IGS_TOL])) {
        dum = igesin_atof( p->globals[IGS_TOL]);
        if((dum>0.0000001))
            n->tol=dum;
    }


    n->flags = PCN_ONE_D;
    lp = iges_get_next_parameter(ie, p,&lc);
    if(!lp)
        GETOUT
                i = atoi(lp); if(i!=126) GETOUT
            lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
            n->LimitU= 1 + atoi(lp);
    n->LimitV= 1 ;
    lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
            n->KValU  = atoi(lp);
    n->KValV =  0;
    n->KoffU =  1; //  DONT CHANGE
    n->KoffV =  1; // DONT CHANGE
    n->Knot_Type=PCN_ONE_D;
    /*
Tested KoffU results
 Order
  1	 n->KValU - 0	1
  2	 n->KValU - 1	1
  3	 n->KValU - 2	1
  4	???     */

    if(!PCN_Nurbs_Alloc(n)) GETOUT

            for(i=0;i<4;i++){
        lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
                prop[i]=atoi(lp);
        if(prop[i] && IGS_DEBUG)
            printf("prop%d is %d NOT coded\n",i+1,prop[i]);	// these variations arent coded PROP2=1 means weights equal
    }

    // knots in U
    i = n->LimitU+n->KValU + 1;//  Sept 2002

    for(j=0;j<i;j++) {
        lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
                n->m_KnotVectorU[j]=igesin_atof(lp);
    }

    //	n->iP = n->cP = CALLOC( (n->LimitU),sizeof(void *));
    //	for(i=0;i<  n->LimitU;i++)
    //		n->iP[i]= n->cP[i]= CALLOC((n->LimitV),sizeof(VECTOR));

    vlist =  (VECTOR *)CALLOC( (n->LimitU+8),sizeof(VECTOR));

    PCN_Set_Weights(n); // not needed. Just sets soem defaults


    for(i=0;i<n->LimitU;i++) {
        lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
                n->m_W[i][0] =  igesin_atof(lp);
    }
    for(i=0;i<n->LimitU;i++) {
        lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
                vlist[i].x = n->iP[i][0].x=(float)igesin_atof(lp);
        lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
                vlist[i].y = n->iP[i][0].y=(float)igesin_atof(lp);
        lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
                vlist[i].z = n->iP[i][0].z=(float)igesin_atof(lp);
    }

    // now the last 4, should be the U&V limits
    // knots in U
    if((lp= iges_get_next_parameter(ie, p,&lc))){	// for RElax MUST normalise the knotpositions
        kmin = igesin_atof(lp);
        if((lp= iges_get_next_parameter(ie, p,&lc))){
            kmax = igesin_atof(lp);
            i = n->LimitU+n->KValU + 1;//  Sept 2002
            for(j=0;j<i;j++) {
                n->m_KnotVectorU[j]=(n->m_KnotVectorU[j]-kmin)/(kmax-kmin);
                if(j && (n->m_KnotVectorU[j] < n->m_KnotVectorU[j-1]))
                    n->m_KnotVectorU[j] = n->m_KnotVectorU[j-1];
                if(n->m_KnotVectorU[j]>1.0) n->m_KnotVectorU[j]=1.0;
                if(n->m_KnotVectorU[j]<0.0) n->m_KnotVectorU[j]=0.0;

            }
        }
    }
    if((lp= iges_get_next_parameter(ie, p,&lc)))
        norm.x = (float)igesin_atof(lp);
    if((lp= iges_get_next_parameter(ie, p,&lc)))
        norm.y = (float)igesin_atof(lp);
    if((lp= iges_get_next_parameter(ie, p,&lc)))
        norm.z = (float) igesin_atof(lp);

    // now we have a NURBS structure
    iges_read_properties(p,ie,&lc); // lat IGEs read for this entity

    for(i=0;i <= n->KValU;i++) { // DODGY insurance.  still doesnt fix HOOPS
        if(	n->m_KnotVectorU[i] >0.0)
            n->m_KnotVectorU[i] = 0.0;
    }


    //	HC_Insert_NURBS_Curve ( n->KValU,n->LimitU, vlist,n->Wu ,n->KnotVectorU,(float) 0.0, (float) 1.0) ;
    RXFREE(vlist);

    if( ie->layer && (!strncasecmp( ie->layer  , "GAUSS",5 ))) {
        char *atts = iges_create_attributes(ie);
        PCN_OneD_NURBS_To_Poly(n,&c,&poly); // now c pts in poly
        struct TwinEntity  theEnts;
        theEnts= Generate_Curve_From_Graphic(p->owner->Esail,atts, ie->Dwords[IGS_PDATA],ie->layer,ie->name,c,poly);
        ie->e = theEnts.e1;
        // 1 july ie->e is now the bce NOT the sce
        RXFREE(poly);
        if(atts) RXFREE(atts);
    }
    // need to free the nurb N or else use it in a basecurve
    if(n) 	PCN_Free_Nurb(n);
#undef GETOUT
    return 1;
}
int iges_read_406(struct IGES_ENTITY *ie, struct PC_iges*p){// not debugged
    // lastline is the first of this entity
    // We will read a further ie->c lines
    // returning one parameter at a time
#define GETOUT { return 0;}
    /* typically
     406      11       0       0       0       0       0       000010300D     21
     406       0       0       1      15       0       0 NAMETXT       0D     22
*/
    char *lp,buf[256];
    int i,  lc= - ie->DData[IGS_PCONT];
    lp = iges_get_next_parameter(ie, p,&lc);
    if(!lp)
        GETOUT
                i = atoi(lp); if(i!=406) GETOUT
            lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
            ie->nPw = atoi(lp);
    ie->Pword=(char **)CALLOC(ie->nPw,sizeof(char*));
    for(i=0;i<ie->nPw;i++){
        lp = iges_get_next_parameter(ie, p,&lc); if(!lp) GETOUT
                ie->Pword[i] = iges_deHollerith(lp);// should de-Hollerith and STRDUP it
        if(IGS_DEBUG) printf(" entity 406 word %d is <%s>\n",i,lp);
    }

    iges_read_properties(p,ie,&lc);

    switch (ie->DData[IGS_FORMN]) {
    case 15: // a name
    case 3: // a layer

        break;

    default:
        sprintf(buf," type %d property form %d not coded",ie->DData[IGS_ETYPE], ie->DData[IGS_FORMN]);
        p->owner->OutputToClient(buf,1);
    }

#undef GETOUT
    return 1;
}

struct IGES_ENTITY *iges_find_entity(struct PC_iges *p, int type,int lyno, int form){
    int i;
    struct IGES_ENTITY * ie;
    for (i=0,ie =p->elist  ;i<p->ne;i++,ie++) {
        if(type && ie->DData[IGS_ETYPE] != type) continue;
        if(form && ie->DData[IGS_FORMN] != form) continue;
        if(lyno && ie->DData[IGS_LEVEL] != lyno) continue;
        return ie;
    }
    return NULL;
}
int iges_apply_properties(struct PC_iges *p,struct IGES_ENTITY *ie ){
    int i,k,layno;
    struct IGES_ENTITY *pe, *le;
    //The layer  is
    layno = ie->DData[IGS_LEVEL];
    // so we search for a 406 with the same layno and formnumber 3

    le = iges_find_entity(p, 406,layno, 3);
    if(le) {
        assert(le->Pword);
        ie->layer = STRDUP(le->Pword[1]);
    }

    for(i=0;i<ie->np; i++) {
        k = ie->props[i]; // find the ie with D=p; trace its parameters. Act depending on its form_number
        k = (k-1)/2;
        pe = &(p->elist[k]); // pe is the entity whose property we want to extract

        switch(pe->DData[IGS_ETYPE]){
        case 406:
            switch(pe->DData[IGS_FORMN]){
            case 15: // name
                if(ie->name) RXFREE(ie->name);
                ie->name = STRDUP(pe->Pword[0]);
                break;
            case 3: // level
                if(ie->layer) RXFREE(ie->layer);
                ie->layer = STRDUP(pe->Pword[1]);
                break;

            default:
                break;
            }

            break;
        default:

            break;
        }
    }
    if(!ie->name) {
        char buf[256],b2[256];;
        strcpy(buf, ie->Dwords[IGS_ELABL]); PC_Strip_Trailing(buf);
        strcat(buf,"_");
        strcpy(b2, ie->Dwords[IGS_PDATA]); PC_Strip_Leading(b2);
        strcat(buf,b2);
        ie->name = STRDUP(buf);
    }
    return 0;
}
int iges_read_properties(struct PC_iges *p,struct IGES_ENTITY *ie,int *lc){
    // read until we get a eor marker.

#define GETOUT { return 0;}
    char *lp;
    int  i;
    ie->na = 0;
    if((lp= iges_get_next_parameter(ie, p,lc))){
        ie->na = (int) igesin_atof(lp);

        if(ie->na) ie->asses = (int *)CALLOC(ie->na, sizeof(int));
        for(i=0;i<ie->na;i++) {
            if((lp= iges_get_next_parameter(ie, p,lc))){
                printf(" associativity %d is %s\n",i,lp);
                ie->asses[i] = (int) igesin_atof(lp);
            }
        }
        if((lp= iges_get_next_parameter(ie, p,lc))){
            ie->np = (int) igesin_atof(lp);
            if(ie->np) ie->props = (int *)CALLOC(ie->np, sizeof(int));

            for(i=0;i<ie->np;i++) {
                if((lp= iges_get_next_parameter(ie, p,lc))){
                    ie->props[i] = (int) igesin_atof(lp);
                    if(IGS_DEBUG) printf(" property %d is %s (%d) \n",i,lp,ie->props[i]);}
            }
        }
    }
    iges_apply_properties(p,ie);

    return 1;
}
int iges_read_116(struct IGES_ENTITY *ie, struct PC_iges*p){// template entity reader
    // lastline is the first of this entity
    // We will read a further ie->DData[IGS_PCONT] lines
    // returning one parameter at a time
    char *lp;
    char *atts,line[512];
    int lc= - ie->DData[IGS_PCONT]; // c=1
    float x,y,z;
    int i;

    lp= iges_get_next_parameter(ie, p,&lc); if(!lp) return 0;
    i = (int) igesin_atof(lp);
    assert(i==116);
    lp= iges_get_next_parameter(ie, p,&lc);if(!lp) return 0;
    x = (float) igesin_atof(lp);
    lp= iges_get_next_parameter(ie, p,&lc);if(!lp) return 0;
    y =  (float) igesin_atof(lp);
    lp= iges_get_next_parameter(ie, p,&lc);if(!lp) return 0;
    z =  (float) igesin_atof(lp);
    lp= iges_get_next_parameter(ie, p,&lc);if(!lp) return 0;
    i = (int) igesin_atof(lp);//Pointer to the DE of the Subfigure Definition Entity

    iges_read_properties(p,ie,&lc); // sets name and layer

    HC_Set_Marker_Symbol("(.)"  );
    RX_Insert_Marker(x,y,z,ie->e->GNode());

    if(strncasecmp(ie->layer,"GAUSS",5))  return 1;

    atts= iges_create_attributes(ie);
    sprintf(line,"site : %s: %f: %f: %f: %s",ie->name,x,y,z,atts);
    RXFREE(atts);
    i=1;
    if(!( p->owner->Esail->Entity_Exists(ie->name,"site")  )){ //p->owner->Esail,
        ie->e = Just_Read_Card(p->owner->Esail,line,"site",ie->name,&i);
        if(ie->e->Resolve())
            ie->e->Needs_Finishing=!ie->e->Finish();
    }
    return 1;
}
int iges_read_N(struct IGES_ENTITY *ie, struct PC_iges*p){// template entity reader
    // lastline is the first of this entity
    // We will read a further ie->c lines
    // returning one parameter at a time
    char *lp;
    int c=1,lc= - ie->DData[IGS_PCONT];
    if(IGS_DEBUG) printf(" start reading <%s> with %d lines\n", ie->Dwords[IGS_ELABL],ie->DData[IGS_PCONT]);
    while ((lp= iges_get_next_parameter(ie, p,&lc))){
        if(IGS_DEBUG) printf (" %d\t %s\n",c,lp);
        c++;

    }
    return 1;
}
char *iges_Newget_next_parameter(struct IGES_ENTITY *ie, struct PC_iges*p,int*linecount){
    //  we have lastline and ie->nrec-1 further lines.
    // we return the next parameter,
    static int limit;
    static char *lp,*lpout, *lpnext;
    char seps[3];
    seps[0] = *p->prm_dlmtr;
    seps[1] = *p->rcrd_dlmtr;
    seps[2] = 0;

    if(*linecount<0) {
        limit=- (*linecount);
        *linecount=1;
        lp=p->lastline;
        if (lp[72] !='P') return NULL;
        lp[64]=0;
        PC_Strip_Trailing(lp);
    }
    for(;;){
        // if *lp is the rcrd_dlmtr, we have already read the last parameter so return NULL;
        // Otherwise, increment lp and then read from lp up to the next prm_dlmtr or rcrd_dlmtr.
        // recognise a hollerith and read past its end, to the first delimiter after it.
        // ignore any delimiters in it.
        //	Leave  lp pointing to the delimiter

        if(lp && !(*lp)) return NULL;
        lp++;
        lpnext = strstr(lp, seps);
        if(!lpnext) { // got to EOL without finding anything)
        }
        else { // who knows???????????

            if(*lp ==  *(p->prm_dlmtr) ) {

            }
            else if (*lp == *(p->rcrd_dlmtr)){

            }
            else
                assert(0);
        }


        // tok out the next parameter. seps were p->prm_dlmtr until sept 9 2002
        if((lpout=strtok(lp,seps))){
            return lpout;
        }
        // here if we've ended the line

        if(!iges_fgets( p->lastline, 82,p->fp,"\n\r")) return NULL;
        (*linecount)++;
        if(*linecount > limit)	// so we don't corrupt the next read
            return NULL;
        lp=p->lastline;
        if (lp[72] !='P') return NULL;
        lp[64]=0;
        PC_Strip_Trailing(lp);
    }
    return NULL;
}

char *iges_get_next_word(struct PC_iges*p,int*lc,char sect,int type,int*No_More){

    // Reader for IGES RXFREE FORMAT. Gets the next parameter
    //           2.2.2   Constants.   This Specification defines six types of constants: integer (or fixed point), real
    //           (or floating point), string, pointer, language statement, and logical.



    // sect is either 'P' or 'G'
    // type is IGS_INT or IGS_REAL orn IGS_STRING or IGS_POINTER or IGS_LANG or IGS_LOGICAL
    //											defaults are ' ' and "0"
    // according to the standard
    /* 	an empty or blank record is interpreted as the default value.
 If we reach the record delimiter, we assign the remaining records their
 default values.
 The EOL is not a delimiter.
 Numerics will not cross the EOL
 Holleriths MAY cross the EOL

  terminate reading when
 A) linecount is reached
 B) we reach the record delimiter
 C) a read error.
The last usable column on lines in the Parameter Data Section is Column 64; 
on lines in all other sections it is Column 72. 
*/

    // LOGIC
    // lp is the pointer to the next character to read
    // if its prm_dlm (or the start), increment it to the next delimiter and return default
    // if its rcrd_dlm return default without incrementing
    //	when we read a word, we leave lp pointing to the delimiter just after the word.
    //	if type = string, we read as a hollerith
    // for all other types, read to the next delimiter
    // METHOD
    //	lets maintain a buffer which may be several lines long.
    // when *lc is negative, we concat ALL the entity's lines into a buffer.
    // after this read, p->lastline is set to the NEXT line in the file.
    // we keep lp as the current marker into the buffer.


    // A rolling buffer.
    // if this is the first read of this section, strdup lastline into buffer and set lp = buf
    // set lpend to point to the last character in lp. (ie 63 or 71)

    // To read a non-string.
    // (A) read up to the next delimiter
    // if we find the delimiter, copy out the word and return.
    // if we dont, we{
    //		1) read another line and append it to buf,
    //		2) strdup buf to a spare.
    //		3) free buf and strdup it from lp.
    //		} and loop to A. lp should be pointing to a delimiter.

    // to read a string
    // read up to the next delimiter or H
    // while we don't find a delimiter or an H, concat successive lines.
    // interpret the text before the H as an integer L
    // while strlen (buf) < L, read lines and concat them.
    // copy out L characters from the H into a return buffer.
    // set lp to the next occurrence of a delimiter.

    // p has buffer and lp
    int L,d,dl;
    char *a,*b , *Seps, *ret;
    char StrSeps[4];
    StrSeps[0] = 'H';
    StrSeps[1] = *p->prm_dlmtr;
    StrSeps[2] = *p->rcrd_dlmtr;
    StrSeps[3] = 0;


    Seps = StrSeps; Seps++;

    if(*lc<0) {  // starting a new section.
        p->limit=99999; // - (*lc);
        *lc=1;
        p->lp = p->lastline;
        if (p->lp[72] !=sect) return NULL;
        if(sect=='P')  // parameter section
            p->lp[64]=0;
        else
            p->lp[72]=0;
        if(p->buffer) RXFREE(p->buffer);
        p->buffer = STRDUP(p->lp); p->lp = p->buffer;
        p->lpend = p->lp; (p->lpend)+=(strlen(p->lp)-1);
    }
    switch(type) {
    case IGS_STRING:
        while (!(a = strpbrk((p->lp),"H"))) {
            if(!iges_append_line(p,sect, lc) )
                return NULL;
        }
        // a is now one of StrSeps.
        // it may be that a delimiter comes before 	a
        // in which case we increment lp and return " "
        b = strpbrk(p->lp,Seps);
        if(b && b<a) {
            p->lp = ++b;
            return " ";
        }

        ret = p->lp;
        L = (int) strtol( p->lp, &a, 10); d = a-p->lp;
        dl = strlen(p->lp+d);
        while(dl < L) {
            if(!iges_append_line(p,sect, lc) )
                return NULL;
            dl = strlen(p->lp+d) ;
        }
        ret = p->lp;
        p->lp = p->lp+d; p->lp+=(L+1);
        *No_More =(*(p->lp) == p->rcrd_dlmtr[0]);
        *(p->lp)=0; (p->lp)++;
        return (iges_deHollerith(ret));
        break;
    case IGS_LANG:
        p->owner->OutputToClient(" can't read IGES language data",3);
        return NULL;
    default: // all the rest. Numeric and logical data
        // To read a non-string.
        // (A) read up to the next delimiter
        // if we find the delimiter, copy out the word and return.
        // if we dont, we{
        //		1) read another line and append it to buf,
        //		2) strdup buf to a spare.
        //		3) free buf and strdup it from lp.
        //		} and loop to A. lp should be pointing to a delimiter.
        while (!(a = strpbrk((p->lp),Seps))) {
            if(!iges_append_line(p,sect, lc) )
                return NULL;
        }
        ret = p->lp;
        *No_More = (*a == p->rcrd_dlmtr[0]);
        *a=0; p->lp = a+1;
        return ret;
        break;
    }


    return NULL;
}

char * iges_append_line(struct PC_iges *p, char sect, int *lc){
    // read one line and append to buffer, resetting lp and lpend

    char *a;
    if(!iges_fgets( p->lastline, 82,p->fp,"\n\r")) return NULL;
    (*lc)++;
    if(*lc > p->limit)	// so we don't corrupt the next read
        return NULL;

    if (p->lastline[72] !=sect) return NULL;
    if(sect=='P') { // parameter section
        p->lastline[64]=0;
    }
    else {
        p->lastline[72]=0;
    }
    if(p->buffer) {
        a = (char*)MALLOC(strlen(p->lp) + 80); *a=0;
        strcpy(a,p->lp);
        RXFREE(p->buffer);
        p->buffer=a;
    }
    else {
        a = (char*)MALLOC(80); *a=0;
    }

    strcat(a,p->lastline);
    p->lp = a;


    return p->lp;
}
int iges_is_hollerith(char*lp) {// returns count INCLUDiNG the hollerith header. 0 if no hollerith
    // a hollerith is an integer followed by 'H' or 'h'
    char *end;
    int c,hc=0;
    if(!lp) return 0;
    c = strtol(lp,&end,10);
    if(*end == 'H' || *end=='h')
        hc = (end-lp) + c + 1;
    return hc;

}

char iges_read_one_char(struct PC_iges *p,int *linecount,const int limit,int *err){// exception returns look  wrong
    char cout;
    *err = IGS_OK;   // IGES_OK or IGES_EOF or IGES_EOR
    cout = *(p->lp); (p->lp)++; // ready for the next read

    if(0==(*(p->lp))) { // we've reached the end of lastline
        if(!iges_fgets( p->lastline, 82,p->fp,"\n\r")) {
            *err=IGS_EOF; // here we return the char
            return cout;
        }
        (*linecount)++;
        if(*linecount > limit)	{// so we don't corrupt the next read
            *err = IGS_EOR;
            return cout;
        }
        p->lp=p->lastline;
        if (p->lp[72] !='P') return 0;
        p->lp[64]=0;
    } // end if EOL
    return cout;
}
char *iges_get_next_parameter(struct IGES_ENTITY *ie, struct PC_iges*p,int*linecount){
    //  we have lastline and ie->nrec-1 further lines.
    // we return the next parameter,  as pointer to the start of a buffer
    static int limit;
    // p->lp is the pointer into lastline of the next character to read.
    static char buffer[IGS_BUFFERSIZE];// its static so we can return its contents
    char *b;
    int hc,err;
    char seps[3];
    seps[0] = *p->prm_dlmtr;
    seps[1] = *p->rcrd_dlmtr;
    seps[2] = 0;
#ifdef _DEBUG
    memset(buffer,0,IGS_BUFFERSIZE);
#endif

    if(p->JustFoundAnEOR) { // this is set after reading the last parameter of a record.
        p->JustFoundAnEOR = 0;
        return NULL; // so we return a NULL lp to stop this sequence
    }

    if(*linecount<0) { // starting a new line
        limit=- (*linecount);
        *linecount=1;
        p->lp=p->lastline;
        if (p->lp[72] !='P') return NULL;
        p->lp[64]=0;
        //	PC_Strip_Trailing(p->lp);
    }
    b = buffer;
    for(;;){
        //  start copying characters one by one from lp into buffer.
        //  if lp points to a hollerith, read the required number of characters, appending new lastlines as needed
        // if we are NOT in a hollerith and we come across a prm_dlmtr or rdrd_dlmtr, that's the end
        // At the end, we position lp JUST PAST the delimiter

        hc=iges_is_hollerith(p->lp); // returns count INCLUDiNG the hollerith header. 0 if no hollerith

        for(;hc>0;hc--,b++)
            *b = iges_read_one_char(p,linecount,limit,&err);

        *b = iges_read_one_char(p,linecount,limit,&err);// Keep this char.  ERR means the NEXT read should return NULL
        // err may be OK, EOR or EOF

        if(strchr(seps, *b)){  // we have just read a sep
            if(*b == *p->rcrd_dlmtr)
                p->JustFoundAnEOR=1;
            *b=0; assert(strlen(buffer) < IGS_BUFFERSIZE);
            return buffer;
        }
        //	if(err !=IGS_OK) { *b=0; StatErr = 1; return buffer; } //  IGE_OK or IGS_EOF or IGS_EOR
        b++;


    } // end for
    return NULL;
}
#ifdef NEVER
char *iges_get_next_parameter_SSD(struct IGES_ENTITY *ie, struct PC_iges*p,int*linecount){
    //  we have lastline and ie->nrec-1 further lines.
    // we return the next parameter.
    //This routine is good except for holleriths and multi-line records.
    static int limit;
    static char *lp,*lpout;
    char seps[3];
    seps[0] = *p->prm_dlmtr;
    seps[1] = *p->rcrd_dlmtr;
    seps[2] = 0;
    lp=NULL;
    if(*linecount<0) { // starting a new line
        limit=- (*linecount);
        *linecount=1;
        lp=p->lastline;
        if (lp[72] !='P') return NULL;
        lp[64]=0;
        PC_Strip_Trailing(lp);
    }
    for(;;){
        // tok out the next parameter. seps were p->prm_dlmtr until sept 9 2002

        if((lpout=strtok(lp,seps))){
            return lpout;
        }
        // here if we've ended the line

        if(!iges_fgets( p->lastline, 82,p->fp,"\n\r")) return NULL;
        (*linecount)++;
        if(*linecount > limit)	// so we don't corrupt the next read
            return NULL;
        lp=p->lastline;
        if (lp[72] !='P') return NULL;
        lp[64]=0;
        PC_Strip_Trailing(lp);
    } // end for
    return NULL;
}
#endif
int iges_terminate_section(struct PC_iges*pi){ 
    if(IGS_DEBUG) iges_print(stdout,pi);
    return 1;
}

int iges_create_entity(char *line,struct PC_iges *p){
    // creates space for another entity and returns its index;
    struct IGES_ENTITY *ie;
    char *lp;
    int i;
    if(p->nea <= p->ne) {
        p->nea += iges_blksize;
        p->elist=(IGES_ENTITY *)REALLOC(p->elist,p->nea*sizeof(struct IGES_ENTITY ));
    }
    ie = &(p->elist[p->ne]); p->ne++;
    memset(ie, 0, sizeof(struct IGES_ENTITY));

    strcpy(ie->Dline,line);

    for(i=0,lp=ie->Dline;i<18;i++, lp +=8){ // not 20 because Dline doesnt have the last tail
        strncpy((ie->Dwords[i]),lp,8);
        if(IGI==g_IGS_DTYPE[i]) ie->DData[i]= atoi(&(ie->Dwords[i][0]));
    }

    i = ie->DData[IGS_ETYPE];//	0
    i = ie->DData[IGS_PDATA];//	1
    i = ie->DData[IGS_STRUC];//	2
    i = ie->DData[IGS_LFONT];//	3
    i = ie->DData[IGS_LEVEL];//	4
    i = ie->DData[IGS_VIEW];//	5
    i = ie->DData[IGS_T_MAT];//	6
    i = ie->DData[IGS_LDISP];//	7
    i = ie->DData[IGS_STATS];//	8
    //	i = ie->DData[IGS_SEQNO] = -99;//	9  // TEXT. Doesnt get set
    i = ie->DData[IGS_ETYPE2];//	10
    i = ie->DData[IGS_LWGHT];//	11
    i = ie->DData[IGS_COLOR];//	12
    i = ie->DData[IGS_PCONT];//	13
    i = ie->DData[IGS_FORMN];//	14
    i = ie->DData[IGS_RESV1];//	15
    i = ie->DData[IGS_RESV2];//	16
    i = ie->DData[IGS_ELABL];//	17	//ie name
    i = ie->DData[IGS_SUBSC];//	18
    i = ie->DData[IGS_SEQNO2];//	19

    //	ie->type=atoi(ie->Dline);
    //	ie->start=atoi(&ie->Dline[8]);
    //	ie->nrec = atoi(&ie->Dline[99]);
    //	ie->form_number=ie->DData[IGS_FORMN];

    return 0;
}
int iges_print(FILE *fp, struct PC_iges *pi){
    int c;
    fprintf(fp,"IGES entity prm delim=[%c] rcrd delim=[%c]\n",*(pi->prm_dlmtr),*(pi->rcrd_dlmtr));
    fprintf(fp," %s\n",pi->filename);
    fprintf(fp," %s\n",pi->segname);
    for (c=1;c<pi->n_globals;c++)
        fprintf(fp,"%d \t<%s>\n",c,pi->globals[c]);

    fprintf(fp,"there are %d entities\n",pi->ne);
    for(c=0;c<pi->ne;c++)
        fprintf(fp,"%d\t%s\t%s\n",c,pi->elist[c].Dwords[IGS_ELABL], pi->elist[c].Dline);
    fflush(stdout);
    return 1;
}
int iges_free_ie( struct IGES_ENTITY  *pi){
    int i;
    if(!pi) return 0;
    //	int *asses, na;
    //	int *props, np;
    //	char **Pword;  int nPw;// this is for the words of a 406 entity
    if(pi->Pword) {
        for(i=0;i<pi->nPw;i++) {
            if(pi->Pword[i] ) RXFREE ( pi->Pword[i] );
        }
        RXFREE(pi->Pword);
    }
    if(pi->asses) RXFREE(pi->asses);
    if(pi->props) RXFREE(pi->props);
    if(pi->name)  RXFREE(pi->name);
    if(pi->layer) RXFREE(pi->layer);
    return 1;

}
int iges_free( struct PC_iges *pi){
    int i;
    if(!pi) return 0;
    if ( pi->filename)  RXFREE(pi->filename);
    if ( pi->segname)  RXFREE(pi->segname);
    for(i=0;i<pi->n_globals ;i++) {
        if(pi->globals[i]) RXFREE(pi->globals [i]);
    }
    if (pi->elist ) {
        for(i=0;i<pi->ne;i++) {
            iges_free_ie(&(pi->elist [i]));
        }
        RXFREE(pi->elist);
    }
    RXFREE(pi);
    return 1;
}

char *iges_fgets(char*string, int nmax, FILE *fp,char*seps) {
    /* slightly different from fgets
it reads up to nmax chars into <string> and stops at the first char which is in seps
it terminates the string with a NULL
return values
 if it encounters EOF before reading anything, it returns NULL
if a read error occurs it returns NULL
else it returns a pointer to <string>
*/
    int n;
#ifdef WIN32
    int  k = 0;
    char *lp;
    lp  = string;
    *lp=0;
    memset(lp,0,nmax);
    do {
        n = fscanf(fp, "%c",lp);
        if(0==n) {
            *lp=0;
            return(NULL);}
        //	if(*lp == '"') //gave trouble excel introduced this character
        //	continue;
        if (EOF==n){
            if(!k)
                return(NULL);
            else
                break;
        }
        if (strchr(seps,*lp))
            break;
        lp++; k++;

    }while (k < (nmax-1));
    *lp=0;
#else
    char c;
    if(!fgets(string, nmax, fp)) return NULL;
    if(fscanf(fp,"%c",&c) && c !=10 && c!=13)
        ungetc(c,fp);

    n = strlen(string)-1;
    while (n>=0) {
        if(string[n]== 10  || string[n]== 13 )
            string[n]=0;
        else
            break;
        n = strlen(string)-1;
    }

    //printf(" iges_fgets got <%s>\n", string);
#endif

    return (string);
}
