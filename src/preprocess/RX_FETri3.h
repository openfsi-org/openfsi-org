#ifndef _RXFORTRANTri3_HDR_
#define   _RXFORTRANTri3_HDR_

#include "RX_FEObject.h"
#include "offset.h" // tristressstruct
#include "RX_SimpleTri.h"

 class RXSail;

struct TRIANGLE_LAMINATE_DEFINITION {
  double	Angle;		// 0.0 for the ref layer.
  double	Thickness;
  class		RXLayer  *Layptr; // careful.  The destructor of RXLayer doesnt clear these pointers. 
  double	*dd; //MUST be the ame as MatValType
  double	*ps;
  int		m_flags;
};//struct TRIANGLE_LAMINATE_DEFINITION 

#define TLD_ALLOCED		1
#define TLD_PRESTRESS	2
#define TLD_PSALLOCED	4
#define TLD_SPARE		8
 
typedef struct TRIANGLE_LAMINATE_DEFINITION TriLamDefn;

class RX_FETri3: 
	 public RX_TriBase,public RX_FEObject
{
	friend class RXSail;
public:
	RX_FETri3(void);
	RX_FETri3(SAIL *p_sail);
	~RX_FETri3(void);

	void Init(SAIL *s=0);  
	void ReInit(SAIL *s=0);  

	// cannot point into Triangles because they get realloced
private:
	int node[3];
	double m_RefAngle; // the angle of the element base in its material axis system. In Radians
public:
	int Node(const int k) const{ return node[k];}
	int m_iEd[3];
    double m_TriArea;
	MatValType mx[9];
	bool SetRefAngle(const double a){ m_RefAngle=a; return true;}
	bool SetRefAngle(const ON_3dVector &matrefdir);
	ON_Xform MaterialTrigraph();
	double RefAngle(void) const { return m_RefAngle;}

	int creaseable;
	TriLamDefn *m_laminae;    /* array of TriLamDefn's one for each layer in the element */
	int laycnt;
	char *m_colour;          /* set to current colour we want visible on screen */
	double prestress[3];
	struct PC_FILAMENT_ON_TRI *m_fils;
	// for Get Next Material Stress
	int m_CurrentLayer;  
	class RXPanel *m_panel;
public:
	int IsValid();
	class RX_FESite* GetNode(const int k)const;
	void SetNode(const int k, class RX_FESite *const s);
        virtual int Vertices(const RXSitePt *v[4])const ;

	int Tri3Print(FILE *ff)const;

        int SetProperties(void);// copy this object's props  to the FEA database
	int AllFilamentStress(double e[3],double s[3],bool KillNegative);
	int SetPressure(const double p);
	double GetPressure() const;
	int IsUsed() const;
	int GetStressResults( TriStressStruct *val) const;
        int WriteStressTensor(ofstream &dst);
        int WriteStrainTensor(ofstream &dst);
        int WriteStiffnessTensor(ofstream &dst );
        int WriteLocalAxis (ofstream &dst );

        int GetStressTensor(double*x);
        int GetStrainTensor(double*x);
        int GetStiffnessTensor(double*x );
        int GetLocalAxis (double*s);

	RXSTRING TellMeAbout() const;


};
#endif
