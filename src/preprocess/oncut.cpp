// oncut.cpp
//started Feb 2005.   A set of routines to find intersections between curves all at once
// TODO reparameterize the curve after moving the end points.
// June 25 2005.  Now works (partially) for mixed script/3dm models

#include "StdAfx.h"
#include <QDebug>
#include "RXSail.h"
#include "RXSRelSite.h"
#include "RXSeamcurve.h"
#include "RXOffset.h"
#include "rxsubmodel.h"
#include "entities.h"
#include "iangles.h"
#include "etypes.h"
#include "printall.h"

#ifndef linux
#include "..\stringtools\stringtools.h" // defines FromUtf8 and ToUtf8

#else
#include "../../stringtools/stringtools.h"
#endif

#include "oncut.h"

using namespace UtfConverter;
//#define ONC_DEBUG

#define FORALL(j,l,p) 	for(j=0;p = l.At(j),j<l.Count();j++)  
namespace oncut{

int ONC_Find_3dm_Cuts(SAIL *sail){
    ON_ClassArray<RX_ONC_Cpoint> PointList;
    ON_ClassArray<RX_ONC_Cpoint> CurveList;
    ON_TextLog l_dump_tostdout;
    int changing;
    if(! sail->GetFlag(FL_ONFLAG))
        return 0;

    /*
this needs re-working to cope with re-builds*/

    PointList.Empty();
    CurveList.Empty();

    // June 2005  there was a bug we were calling finish_site card on relsites.
    //Fixed this but it doesnt do much good because relsites usually don't finish at this stage
    // because their curves wont be finished.  And if we finish their curves, then they wont be caught here.
    // The problem is models with mixed cut and script seams and relsites,
    // in the case where a relsite has been defined exactly where another curve cuts.
    // the solution will be to recover this later
    //int_4 is where headaft is

    // 1) get lists of all the sites and relsites and SC's which needed finishing
    // 2) once we have this list, we finish as many as we can - with Needs-cutting OFF
    // 3) delete from the lists anything we were not able to finish
    // 4) Now we can try to hook themn up.


    ONC_Create_CurveList(sail,CurveList);  // also deletes short ones and (june 2005) FInishes the curve after noting it.
    ONC_Create_PointList(sail,PointList, sail->m_Cut_Dist);  // ignores the second close one; doesnt delete it (tried ad it caused trouble)
    //The  pointlist is a mix of finished and unfinished. ALl are resolved.
    // lets finish the sites we can and then remove those we cant from this list

#ifdef ONC_DEBUG
    l_dump_tostdout.Print("PointListstarting\n");
    ONC_Print_Clist(PointList,l_dump_tostdout);
    l_dump_tostdout.Print("ONC_CurveList starting\n");
    ONC_Print_Clist(CurveList,l_dump_tostdout);
#endif

    do{
        changing = ONC_Finish_CurveList(sail,CurveList);
        changing+= ONC_Finish_PointList(sail,PointList);
    }
    while (changing);
    // now delete from the lists anything that needs finishing
    ONC_Remove_UnFinished(CurveList);
    ONC_Remove_UnFinished(PointList);
    ONC_Tidy_PointList(CurveList) ;
    ONC_Tidy_PointList( PointList ) ;
#ifdef ONC_DEBUG
    l_dump_tostdout.Print(" PointList after RemoveUnfinished\n");
    ONC_Print_Clist(PointList,l_dump_tostdout);
    l_dump_tostdout.Print(" ONC_CurveList after RemoveUnfinished\n");
    ONC_Print_Clist(CurveList,l_dump_tostdout);
#endif
    ONC_Delete_Coincident_Curves(CurveList, sail->m_Cut_Dist);// not coded
    ONC_Extend_All_Curves(CurveList, sail->m_Cut_Dist); // CHECKED OK
    ONC_Tidy_PointList(CurveList) ;
    ONC_Tidy_PointList( PointList ) ;

    ONC_Find_All_Nearest(PointList,CurveList, sail->m_Linear_Tol);
    ONC_Tidy_PointList(CurveList) ;
    ONC_Tidy_PointList( PointList ) ;
#ifdef ONC_DEBUG
    l_dump_tostdout.Print(" PointList before SnapEnds\n");
    ONC_Print_Clist(PointList,l_dump_tostdout);
    l_dump_tostdout.Print(" ONC_CurveList before SnapEnds\n");
    ONC_Print_Clist(CurveList,l_dump_tostdout);
#endif
    ONC_SnapEnds(PointList,2.0* sail->m_Cut_Dist ,sail->m_Linear_Tol );
    ONC_Tidy_PointList(CurveList) ;
    ONC_Tidy_PointList( PointList ) ;
#ifdef ONC_DEBUG
    l_dump_tostdout.Print("\nONC_Find_All_Intersections\n");
#endif

    ONC_Find_All_Intersections(CurveList,PointList,sail->m_Linear_Tol); // UNSTABLE
    ONC_Tidy_PointList(CurveList) ; // just removed thos marked 'PCE_DELETE'
    ONC_Tidy_PointList( PointList ) ;
#ifdef ONC_DEBUG
    l_dump_tostdout.Print(" ONC_PointList after Intersect\n");//exit(1);
    ONC_Print_Clist(PointList,l_dump_tostdout);
    l_dump_tostdout.Print(" ONC_CurveList after Intersect\n");
    ONC_Print_Clist(CurveList,l_dump_tostdout);
#endif

    //	l_dump_tostdout.Print("\nONC_Coalesce\n");
    ONC_Coalesce(PointList, sail->m_Cut_Dist );
    ONC_Tidy_PointList(CurveList) ;
    ONC_Tidy_PointList( PointList ) ;
    //	l_dump_tostdout.Print("\nONC_CreateSitesFromClist\n");
    ONC_CreateSitesFromClist(sail,PointList);
    ONC_CompleteCurveList(PointList,CurveList);
    ONC_Tidy_PointList(CurveList) ;
    ONC_Tidy_PointList( PointList ) ;

    ONC_ClistSnapToPoints(CurveList,PointList,sail->m_Cut_Dist ); // we (no longer) get perp convergence failures here
    ONC_Tidy_PointList(CurveList) ;
    ONC_Tidy_PointList( PointList ) ;
#ifdef ONC_DEBUG
    l_dump_tostdout.Print(" ONC_PointList after Snap\n");//exit(1);
    ONC_Print_Clist(PointList,l_dump_tostdout);
    l_dump_tostdout.Print(" ONC_CurveList after Snap\n");
    ONC_Print_Clist(CurveList,l_dump_tostdout);
#endif

    ONC_CreateAllIALists(CurveList); // turns off needs_cutting (cut?) flag too.
    // also fills in End1site,End2site on the SC;
#ifdef ONC_DEBUG
    l_dump_tostdout.Print("ONC_PointList Final\n");
    ONC_Print_Clist(PointList,l_dump_tostdout);
    l_dump_tostdout.Print("ONC_ CurveList Final\n");
    ONC_Print_Clist(CurveList,l_dump_tostdout);
#endif
    PointList.Destroy();
    CurveList.Destroy();

    return 1;
}
// jan 2009 its wierd to skip the finished ones.  We want them too
// let's skip the needs_resolving ones. 
int ONC_Create_PointList(SAIL *sail,ON_ClassArray<RX_ONC_Cpoint> &clist,double p_tol ) {
    RXEntity_p p;
    ent_citer it;
    for (it = sail->MapStart(); it != sail->MapEnd();it=sail->MapIncrement(it)) {
        p  = dynamic_cast<RXEntity_p>( it->second);

        if(p->TYPE != SITE && p->TYPE != RELSITE ) 	continue;

        if(p->Needs_Resolving) continue; // jan 09if(!p->Needs_Finishing) continue;

        RX_ONC_Cpoint l_cp;
        l_cp.m_pe = p;
        if(p->m_pONMO)
            l_cp.m_ono = p->m_pONMO->m_object;

        Site *sq = dynamic_cast<Site *>( it->second);
        if((sq->m_Site_Flags&PCF_SKETCH)) // nov 2013
            continue;
        l_cp.m_p=ON_3dPoint(sq->x,sq->y,sq->z);
        l_cp.m_sm=RXsubModel::SubModel(sq);
        clist.AppendNew()= l_cp;
    }
    return 1;
}

int ONC_Finish_PointList(SAIL *sail,ON_ClassArray<RX_ONC_Cpoint> &p_PointList) {
    // returns the number of entities in the list which went from Needs_Finishing to !Needs_Finishing
    int rc=0;
    int i;
    RX_ONC_Cpoint *theP;
    RXEntity_p p;
    FORALL(i,p_PointList,theP) {

        p = theP->m_pe;
        if(p->Needs_Finishing) {
            if(p->TYPE == SITE)
                p->Finish();
            else if(p->TYPE == RELSITE )
                p->Finish();

            if(p->Needs_Finishing)
                continue;
            rc++;
        }

        if(p->m_pONMO)
            theP->m_ono = p->m_pONMO->m_object; // is it already set?
        class RXSRelSite *sq =dynamic_cast<Site*> (p) ;
        theP->m_p=ON_3dPoint(sq->x,sq->y,sq->z);
    }

    return rc;

} //  ONC_Finish_PointList

int ONC_Create_CurveList(SAIL *sail,ON_ClassArray<RX_ONC_Cpoint> &clist) {

    // Pick the SCs with a basecurve with m_pONMO

    ent_citer it;
    for (it = sail->MapStart(); it != sail->MapEnd(); it=sail->MapIncrement(it)){
        RXEntity_p p  = dynamic_cast<RXEntity_p>( it->second);
        if(p->TYPE != SEAMCURVE) 	continue;
        sc_ptr sc = (sc_ptr) p;
        if(p->Needs_Resolving) continue;
        RXEntity_p bc = sc->BorGcurveptr;
        if(! bc || !bc->m_pONMO )
            continue;

        if(!p->Needs_Finishing) continue;
        if(! sc->FlagQuery(CUT_FLAG))
            continue;

        ON_Curve * theCurve =  (ON_Curve*) dynamic_cast<const ON_Curve*> (bc->m_pONMO->m_object);
        if(!theCurve) continue;
        double len=0.0;

        theCurve->GetLength (&len);
        if(len < 2.*sail->m_Cut_Dist) {
            printf("ONCut deleting short '%s' '%s', len=%f\n", p->type(),p->name(),len);
            p->Kill();
        }
        else {
            RX_ONC_Cpoint l_cp;
            l_cp.m_pe = p;
            l_cp.m_sm=RXsubModel::SubModel(p);
            l_cp.m_ono = bc->m_pONMO->m_object;
            clist.AppendNew()= l_cp;
        }
    }
    return 1;
} // Create_CurveList

int ONC_Finish_CurveList(SAIL *sail,ON_ClassArray<RX_ONC_Cpoint> &p_CurveList){ 
    // returns the number of entities in the list which went from Needs_Finishing to !Needs_Finishing
    int rc=0;
    int j,l_nc;
    RX_ONC_Cpoint *theP;
    sc_ptr   sc;  //RXEntity_p p;
    FORALL(j,p_CurveList,theP) {
        //  p = theP->m_pe;
        sc = (sc_ptr)theP->m_pe;
        //ON_Curve *c =  (ON_Curve*) dynamic_cast<const ON_Curve*> (theP->m_ono);

        if(!sc->Needs_Finishing ) continue;
        l_nc=sc->Needs_Cutting; sc->Needs_Cutting=0; // try just getting the geometry
        sc->Finish(); // can try to Finish all the Sites
        sc->Needs_Cutting=	l_nc;
        if(!sc->Needs_Finishing ) rc++;
    }
    return rc;
} //ONC_Finish_CurveList

int ONC_Remove_UnFinished(ON_ClassArray<RX_ONC_Cpoint> &p_List){

    RX_ONC_Cpoint *theC;
    int k,rc =0, changing;
    do {
        changing=0;
        for(k=0;k<p_List.Count();k++)  {
            theC = p_List.At(k);
            if( theC->m_pe->TYPE==PCE_DELETED || theC->m_pe->Needs_Finishing) {
#ifdef ONC_DEBUG
                printf("(ONC_Remove_UnFinished) \t '%s'\t'%s'\t %d\n", theC->m_pe->type(),theC->m_pe->name(),theC->m_pe->Needs_Finishing);
#endif
                p_List.Remove(k);
                changing++;
                rc++;
            }
        }
    } while(changing);
    return rc;
}
int ONC_Tidy_PointList(ON_ClassArray<RX_ONC_Cpoint> &clist) {

    int nr=0;
    int k,j;

    RX_ONC_Cpoint *theP;
    RX_OncIntersect *l_i ;
    class RXsubModel *m1, *m2;

    FORALL(j,clist,theP) {
        //ON_3dPoint &q = theP->m_p;
        RXEntity_p pp = theP->m_pe;
        m1 = theP->m_sm; if(pp) assert(RXsubModel::SubModel(pp )==theP->m_sm);
        if(pp && pp->TYPE==PCE_DELETED) {
            cout<< "(ONC_Tidy) should delete ";
            if(pp) printf(" '%s' \t '%s' \t", pp->type(),pp->name());
            else  printf(" '%s' \t '%s' \t", "no type ","no name");//lp = "No Entity";
            printf(" %f\t%f\t%f\n", theP->m_p.x, theP->m_p.y, theP->m_p.z ); nr++;
        }

        FORALL(k,theP->m_Clist,l_i) {
            RXEntity_p  p = l_i->m_ce ;
            m2 = RXsubModel::SubModel(p ); assert(p);
            assert(m1==m2);
            if(p && p->TYPE==PCE_DELETED) {
                cout<< "(theP->CList) should delete"<<endl; nr++;
            }

        } //theP->m_Clist
    }// clist

    return nr;
}
int ONC_Delete_Coincident_Curves(ON_ClassArray<RX_ONC_Cpoint> &CurveList, double tol){
    // if any two curves are coincident, delete the shorter one.
    // The curves are those basecurves with m_ONxxx

    // TODO  ONC_Delete_Coincident_Curves
    int j;
    //RX_ONC_Cpoint *p;
    return  0;
    //FORALL(j,CurveList,p) {

    //
    //}
    //return  0;
}
int ONC_Extend_All_Curves(ON_ClassArray<RX_ONC_Cpoint> &CurveList, double dist )
// find all the RXCurves that describe XYZAbs seamcurves and extend each end by <dist>
{  
    int j,rc;
    RX_ONC_Cpoint *theP;

    FORALL(j,CurveList,theP) {
        //ON_3dPoint &q = theP->m_p;
        //RXEntity_p pp = theP->m_pe;
        ON_Curve *c =  (ON_Curve*) dynamic_cast<const ON_Curve*> (theP->m_ono);
        // we extend the domain by dist/|derivative|
        // end1
        ON_3dPoint p; ON_3dVector vt;
        double dL1,dL2,tl;
        ON_TextLog l_dump_tostdout;
        if(c->IsClosed())
            continue;

        c->Ev1Der(c->Domain().Min(),p,vt,1);
        tl = vt.Length();
        dL1 = dist/tl;
        c->Ev1Der(c->Domain().Max(),p,vt,1);
        tl = vt.Length();
        dL2 = dist/tl;

        ON_Interval d   = ON_Interval(c->Domain().Min() -dL1,c->Domain().Max()+dL2) ;
#undef  _ONDEBUG
        //debug
#ifdef _ONDEBUG
        l_dump_tostdout.Print ("extending  %s\n",theP->m_pe->name );
        c->GetLength(&dbg_len,1.0e-8,&c->Domain ());
        l_dump_tostdout.Print ("length before extend %f\n\ndump of c\n ",dbg_len);
        c->Dump (l_dump_tostdout );
        ON_NurbsCurve l_nc1,l_nc2;
        ON_3dPoint p11,p12,p21,p22;
        c->NurbsCurve( &l_nc1);
        p11 = c->PointAtStart();   p12 = c->PointAtEnd();
#endif
        rc= c->Extend(d);
        // this is horrible but necessary
        assert(rc) ; // ON code at least exists for NURBS CURVES Does this stuff the polyline list?
        if(!rc)
            cout<< " Extend returned 0"<<endl;


#ifdef _ONDEBUG
        c->GetLength(&dbg_len,1.0e-8,&c->Domain ());
        l_dump_tostdout.Print ("length After extend %f\n",dbg_len);


        l_dump_tostdout.Print (" after extend  dump of c\n ");
        c->Dump (l_dump_tostdout );
        l_dump_tostdout.Print ("NC1\n ");
        l_nc1.Dump (l_dump_tostdout );
        c->NurbsCurve(&l_nc2);
        l_dump_tostdout.Print ("NC2\n ");
        l_nc2.Dump (l_dump_tostdout );
        p21 = c->PointAtStart();   p22 = c->PointAtEnd();
        assert(c->GetLength(&dbg_len,1.0e-8,&d));
#endif	

    } // forall

    return 0;
}

int ONC_Find_All_Nearest(
        ON_ClassArray<RX_ONC_Cpoint> &PointList,
        ON_ClassArray<RX_ONC_Cpoint> &CurveList,
        double p_Tol)
// create the Clist from all the SITEs and relsites in the model.
// for each member of the clist, test whether and of the Curves in the model are within tol of it.  
// If so, add them to the members' m_clist
{ 
    int i,j;

    RXsubModel *m1, *m2;
    RX_ONC_Cpoint *theP,*theC;
    FORALL(j,PointList,theP) {
        ON_3dPoint &q = theP->m_p;
        m1=RXsubModel::SubModel(theP->m_pe);

        FORALL(i,CurveList,theC) {
            m2=RXsubModel::SubModel(theC->m_pe);
            if(m1 != m2)
                continue;
            ON_Curve *c =  (ON_Curve*) dynamic_cast<const ON_Curve*> (theC->m_ono);
            double t =0.0;
#ifdef ONC_DEBUG

            if(strstr(theP->m_pe->name(),"debug") && strstr(theC->m_pe->name(),"debug")) {
                printf("Testing point %s on curve %s",theP->m_pe->name() , theC->m_pe->name());
            }
#endif
            if(	c->GetClosestPoint(q,&t,p_Tol)){

                RX_OncIntersect l_i;
                l_i.m_ce=theC->m_pe; l_i.m_t = t;
                l_i.m_ono = c;
                //      double dist = q.DistanceTo (c->PointAt (t));
                //	printf("site\tcurve \t %.12f \t%s\t%s\n", dist,pp->name ,theC->m_pe->name);
                theP->m_Clist.AppendNew() = l_i;
                // Works for cleanly converted
                // works for NURBS but we need MUCH better error control
                // works for arcs too
                // works for polycurves.
            }
            //	else
            //		printf("\tMISS\n",theP->m_pe->name , theC->m_pe->name );

        }
    }
    return 0;
}

int ONC_SnapEnds(ON_ClassArray<RX_ONC_Cpoint> &clist,double par_tol ,double per_Tol )
// for each point member of clist, see if any of it's curves hit closer to the end than partol.
// if it does, then move that end point to the member point.
// remember that at this stage the curves are Extended.  This is why we call with 2x Partol

{  
    int j,k, nchanged=0;

    RX_OncIntersect *l_i;
    RX_ONC_Cpoint *theP;
    FORALL(j,clist,theP) {
        //	for(j=0;j<clist.Count();j++) {

        ON_3dPoint &q = theP->m_p;
        //Entity *pp = theP->m_pe; // for debug

        //	printf(" %d  %s\n", j,pp->name);
        FORALL(k,theP->m_Clist,l_i) {
            ON_Curve *c =  (ON_Curve*) dynamic_cast<const ON_Curve*> (l_i->m_ono);
            ON_Interval l_d = c->Domain();
            //	printf("\t %d \t %s \tt= %f \tL=%f \tdom = (%f %f )\n",k,l_i->m_ce->name , l_i->m_t,length ,  l_d.Min() , l_d.Max() );
            if( (l_i->m_t - l_d.Min() ) < par_tol) { 		//	pSnap the start
                if(!c->SetStartPoint(q)) {
                    RXSTRING buf = RXSTRING(L"cant move start point on curve ") + TOSTRING(l_i->m_ce->name ());
                    theP->m_pe->OutputToClient(buf,2);
                }
                l_i->m_t = c->Domain().Min();
            }
            else if( ( l_d.Max()  - l_i->m_t ) < par_tol) { //	Snap the End
                if(!c->SetEndPoint (q)) {
                    RXSTRING buf = RXSTRING(L"cant move end point on curve ") + TOSTRING(l_i->m_ce->name ());
                    theP->m_pe->OutputToClient(buf,2);
                }
                l_i->m_t = c->Domain().Max();

            }
        }

    }
    return nchanged;
}
int ONC_Print_Clist(ON_ClassArray<RX_ONC_Cpoint> &clist,ON_TextLog &p_dump) {
    int k,j;

    RX_ONC_Cpoint *theP;
    RX_OncIntersect *l_i ;
    //#ifndef ONC_DEBUG
    //	return 0;
    //#endif

    FORALL(j,clist,theP) {
        printf("%d   \t", j);
        RXEntity_p pp = theP->m_pe;
        if(pp) printf("'%s' \t'%s'\t%d \t", pp->type(),pp->name(),pp->Needs_Finishing);
        else  printf("'%s'\t '%s'\t    \t", "no type ","no name");
        printf(" %.5g\t%.5g\t%.5g\n", theP->m_p.x, theP->m_p.y, theP->m_p.z );


        FORALL(k,theP->m_Clist,l_i) {
            //	for(k=0;k<theP->m_Clist.Count() ; k++) {
            //		RX_OncIntersect *l_i =  theP->m_Clist.At(k);
            //Entity * p = l_i->m_ce ;
            ON_Curve *c =  (ON_Curve*) dynamic_cast<const ON_Curve*> (l_i->m_ono);
            double length =0.0;
            if(c) {
                ON_Interval l_d = c->Domain();
                c->GetLength (&length,1e-8,&l_d);
                ON_3dPoint p = c->PointAt(l_i->m_t);
                printf("\t%d\t(%.5g\t%.5g\t%.5g)\t%s %s\tt=%.4g L=%.5g\tdom=(%.3g %.3g)\n",k,p.x,p.y,p.z, l_i->m_ce->type(),l_i->m_ce->name(), l_i->m_t,length ,l_d.Min() , l_d.Max() );
                //	c->Dump(p_dump);

            }
            else
                printf("\t %d (No C) \t %s %s \tt= %.4g\n", k, l_i->m_ce->type(), l_i->m_ce->name(), l_i->m_t );
        }

    }

    return j-1;
}

int ONC_Find_All_Intersections(ON_ClassArray<RX_ONC_Cpoint> &CurveList,
                               ON_ClassArray<RX_ONC_Cpoint> &PointList,
                               double p_Tol) // tol is lineartol
// look for intersections between all the RXCurves in the model. Don't look past the end.
// for each intersection found, add a member to clist. 
{  RXsubModel *m1=0, *m2=0;
    int i,j,k;
    RX_ONC_Cpoint *C1,*C2;
    FORALL(i,CurveList, C1){
        ON_Curve *c1 =  (ON_Curve*) dynamic_cast<const ON_Curve*> (C1->m_ono);
        m1=RXsubModel::SubModel( C1->m_pe);
        if(0) {
            double t = c1->Domain().Min();
            double te = c1->Domain().Max();
            double dt = (te-t)/1024.;
            for(;t<=te; t+=dt){
                ON_3dPoint p = c1->PointAt(t);
                qDebug("Point %19.15f,%19.15f,%19.15f",p.x,p.y,p.z);
            }
        }

        for(j=i+1; C2 = CurveList.At(j),j<CurveList.Count();j++) {
            m2 = RXsubModel::SubModel(C2->m_pe);
            if(m1 !=m2)
                continue;
            ON_Curve *c2 =  (ON_Curve*) dynamic_cast<const ON_Curve*> (C2->m_ono);
            ON_ClassArray<RX_OncIntersect> list1,list2;

            if(! ONC_Find_One_Intersections(*c1,*c2,p_Tol,&list1,&list2))
            {
                //            if(ON_LineCurve::Cast(c1) &&  !ON_LineCurve::Cast(c2) )
                //                    cout <<"no intersection between linecurve "<< C1->m_pe->name() <<" and notlinecurve "<<C2->m_pe->name()<<endl;
            }

            // the result has the params along c1 in list1, the params along c2 in list2
            for(k=0;k<list1.Count(); k++) {
                list1.At(k)->m_ce = C2->m_pe;
                list2.At(k)->m_ce = C1->m_pe;
                RX_ONC_Cpoint theP;
                ON_3dPoint q1,q2,q;
                q1=c1->PointAt(list1.At(k)->m_t);
                q2=c2->PointAt(list2.At(k)->m_t);
                if(0) {
                    qDebug()<< "find intersection at   ";
                    qDebug( "  q1 is %19.15f  %19.15f  %19.15f  ",q1.x,q1.y,q1.z);
                    qDebug( "  q2 is %19.15f  %19.15f  %19.15f  ",q2.x,q2.y,q2.z);
                    qDebug( "diff is %19.15f  %19.15f  %19.15f  ",q2.x-q1.x,q2.y-q1.y,q2.z-q1.z);
                }
                q = (q1 + q2) /2.0;
                theP.m_pe=NULL;
                theP.m_p= q;
                theP.m_sm=m1;
                RX_OncIntersect l_i;
                l_i.m_ce = C1->m_pe;  l_i.m_ono = c1;
                l_i.m_t= list1.At(k)->m_t;
                theP.m_Clist.AppendNew()= l_i;

                l_i.m_ce = C2->m_pe;  l_i.m_ono = c2;
                l_i.m_t= list2.At(k)->m_t;
                theP.m_Clist.AppendNew()= l_i;
                PointList.AppendNew() = theP;
            }
            // each list now has t and the entity it crosses.

            list1.Destroy(); list2.Destroy();
        } // for j
    } // for i

    return 0;
}

int ONC_Coalesce(ON_ClassArray<RX_ONC_Cpoint> &pList, double tol )
// look at the plist ( the global PointList) and merge any points closer than <tol>
// priority to the ones which have a value for the entity member
{  
    int i,j,k,l;
    RX_ONC_Cpoint *p1=0,*p2=0;
    RXsubModel *m1=0, *m2=0;
    for(i=0; i<pList.Count(); i++) {
        p1 = pList.At(i);  m1 = p1->m_sm;// RXsubModel::SubModel(p1->m_pe);
        for(j=i+1; j<pList.Count(); j++) {
            p2 = pList.At(j);
            m2 = p2->m_sm; //RXsubModel::SubModel(p2->m_pe);
            if(m1 !=m2)
                continue;
            if(p1->m_p.DistanceTo(p2->m_p) <=tol) {	//close AND they share a submodel
                // if any curve in p2's Clist isnt already in the p1's Clist, append it
                RX_OncIntersect *I_1, *I_2;
                FORALL(l,p1->m_Clist,I_1) {
                    RXEntity_p e1 = I_1->m_ce; // each curve in p1;

                    FORALL(k,p2->m_Clist,I_2) {// each curve in p2;
                        if( e1 == I_2->m_ce) { // duplicate so remove from p2
                            p2->m_Clist.Remove(k);
                        }
                    }
                } // for L
                p1->m_Clist.Append(p2->m_Clist.Count(),p2->m_Clist.Array()); // P1 now has an entry for all P2's curves
                if(p2->m_pe) {
                    qDebug()<< "(ONCut) killing "<<p2->m_pe->name();
                    p2->m_pe->Kill();
                }
                // deletes p2.
                pList.Remove(j);
                j--;
            } // end if close
        }
    }

    return 1;
}

int ONC_CreateSitesFromClist(SAIL*sail,ON_ClassArray<RX_ONC_Cpoint> &pointList)
// insert and finish sites at the clist points. Fill in the m_pe member;
{  
    class RXsubModel *m;
    int i,depth=1;;
    RX_ONC_Cpoint *p1;
    char line[256],name[256];
    RXEntity_p e;
    QString  qAtts,qLayer;
    FORALL(i,pointList,p1) {
        m=0;
        if(p1->m_pe) continue;
        if(! p1->m_Clist.Count() )  {
            //	printf(" skip point  %d. It has no curves\n", i);
            continue;
        }
        sprintf(name,"int_%d",i);

        // the 'info' att is used by the site' AttributeGet to inherit from the curves
        qAtts = "$info=<";
        for(int k=0;k<p1->m_Clist.Count();k++) {
            e = p1->m_Clist[k].m_ce;
            if(!m)
                m=RXsubModel::SubModel(e);
            qAtts +=   e->name();
            qAtts+= "+";
            if(!qLayer.isEmpty())
                e->AttributeGet("layer",qLayer);
        }
        if(qAtts.endsWith("+"))qAtts.chop(1);
        qAtts+= ">";
        if(!qLayer.isEmpty())
            qAtts += ",($layer="+qLayer+")";

        sprintf(line,"site : %s : %.12f : %.12f : %.12f : %s",name, p1->m_p.x,p1->m_p.y,p1->m_p.z ,qPrintable(qAtts) );
        e = Just_Read_Card(sail,line,"site",name,&depth);
        // can we make it a relsite - or at least
        if(e->Resolve())
            e->Needs_Finishing=!e->Finish();
        if(m)
        { m->SetRelationOf(e,niece,RXO_SUBMODEL); m->SetNeedsComputing();}
        pointList.At(i)->m_pe =e;
    }
    return 0;
}
int ONC_CompleteCurveList(ON_ClassArray<RX_ONC_Cpoint> &PointList,
                          ON_ClassArray<RX_ONC_Cpoint> &CurveList)
{
    // trawl thru the Pointlist.
    // for each curve in a point, see if it exists in CurveList.
    // if it doesnt exist in CurveList, add it to CurveList, filling in
    //		Entity *m_pe;	// the Curve entity.
    //		double m_t; for the T value.
    // then add the Point to the Curve's m_Clist;
    int i,j,k;
    RX_ONC_Cpoint *theP, *theC;
    RX_OncIntersect *theI;
    FORALL(i,PointList,theP) {
        FORALL(k,theP->m_Clist,theI) {
            //Entity * ei = theI->m_ce; // debug
            FORALL(j,CurveList,theC)  {
                //Entity * ej = theC->m_pe;
                if(theI->m_ce == theC->m_pe) { // theC needs theP adding
                    RX_OncIntersect l_i;
                    l_i.m_ce=theP->m_pe;
                    l_i.m_t = theI->m_t;
                    l_i.m_ono =theP->m_ono;
                    theC->m_Clist.AppendNew() = l_i;
                }
            } // for curvelist.  The curvelist now contains the Curve - as if it needed it.
            //	theI is a curve. We should add the point to the curve's list

        }
    }

    return 0;
}
int ONC_Remove(ON_ClassArray<RX_ONC_Cpoint> &List, RXEntity_p p_e1, RXEntity_p p_e2){
    // Search for the member of the list containing p_e1
    // if we find it, we search for the member of its Clist containing p_e2
    // and delete that member of its Clist
    RX_ONC_Cpoint *theC;
    RX_OncIntersect *theI;
    int i,k;
    FORALL(i,List, theC){
        if(theC->m_pe != p_e1)
            continue;
        FORALL(k,theC->m_Clist ,theI) {
            if(theI->m_ce == p_e2) {
                theC->m_Clist.Remove(k);
                return true;
            }
        }
    }
    return false;
}
int ONC_ClistSnapToPoints(ON_ClassArray<RX_ONC_Cpoint> &CurveList,
                          ON_ClassArray<RX_ONC_Cpoint> &PointList,
                          double p_Tol)
// change the curves so they start, end and pass thru the point.
{  
    //) For each curve:
    // 1)	If it has only one point, delete the curve and its entity.
    // 2)	Do a Find-Nearest on each pt in its m_Clist, to get an update on T.
    // 3)	Trim it to its first and last T's - then snap its ends.
    // 4)	For each intermediate point, split the curve at the T.  Move the cut ends to Q and re-join.

    int i,k;
    double tmin , tmax;
    RX_ONC_Cpoint *theC;
    RX_OncIntersect *theI;
    FORALL(i,CurveList, theC){
        int kmin=-1,kmax=-1;
        if(theC->m_Clist.Count() < 2) {
            cout<< " remove because count small :"<<theC->m_pe->name() <<endl;
            static_cast<RXSeamcurve*> (theC->m_pe)->Demote_To_Sketch();
            CurveList.Remove(i); i--; continue;

        }
        const ON_Object *ono =  theC->m_ono;
        assert(ono->IsValid());
        const ON_Curve *cconst =  ON_Curve::Cast( theC->m_ono);
        ON_Curve *c =   (ON_Curve*)( cconst ) ;
        assert(cconst);
        tmin = cconst->Domain ().Max()+ cconst->Domain().Length ();
        tmax = cconst->Domain().Min() - cconst->Domain().Length ();

        FORALL(k,theC->m_Clist ,theI) {
            RXEntity_p ee = theI->m_ce;
            //	printf("  \t  %s\n", e->name);
            assert(ee->TYPE==SITE || ee->TYPE==RELSITE);
            Site *qs =  dynamic_cast<Site*> (ee) ;
            ON_3dPoint q = ON_3dPoint(qs->x,qs->y,qs->z);

            if(!cconst->GetClosestPoint(q,&(theI->m_t),p_Tol)){
                //	printf(" Remove point '%s' from Curve '%s' \n",e->name,theC->m_pe->name);
                // Now remove the Curve from the Points Clist in PointList
                ONC_Remove(PointList, ee, theC->m_pe);
                theC->m_Clist.Remove(k); k--;
                continue;
            }
            if(theI->m_t < tmin) { tmin = theI->m_t; kmin=k;}
            if(theI->m_t > tmax) { tmax = theI->m_t; kmax=k;}
        }
        //Trim and set the end points
        if(			cconst->Domain ().Includes(tmin)
                    &&		cconst->Domain ().Includes(tmax)) {
            ON_Interval l_d = ON_Interval(tmin,tmax);
            if(c->Trim(l_d)) {
                rxON_NurbsCurve*rnc;
                if (rnc= dynamic_cast<rxON_NurbsCurve*>(c))
                    rnc->Reset_Seglengths();
            }
        }
        //		else
        //			printf(" tmin = %f, tmax=%f outside domain\n", tmin,tmax);

        RXEntity_p eee;
        Site *l_qs;
        if(kmin >=0) {
            eee = theC->m_Clist.At(kmin)->m_ce;
            l_qs = dynamic_cast<Site*> (eee) ;
            c->SetStartPoint(ON_3dPoint(l_qs->x,l_qs->y,l_qs->z));
        }
        if(kmax >=0) {
            eee = theC->m_Clist.At(kmax)->m_ce;
            l_qs =  dynamic_cast<Site*> (eee) ;
            c->SetEndPoint(ON_3dPoint(l_qs->x,l_qs->y,l_qs->z));
        }
    } // forall curvelist;
    return 0;
}

int ONC_CreateAllIALists(ON_ClassArray<RX_ONC_Cpoint> &CurveList) 
// Add each m_pe to the ialist of each curve
// fill in End1site,End2site values on the SC;
// turns off needs_cutting (cut?) flag too. 
// step thru Finish_SC and decide if its OK to turn off Needs_Finishing too.
// Remember NOT to seed a Compute_Cut-Intersections on a curve with an ON_Object.
// set the 		if(!SeamCurve_FlagQuery(sc, SC_3DM_CUT)) { flag

{  
    int i,k;
    class RXsubModel *m1, *m2;
    RX_ONC_Cpoint *theC;
    RX_OncIntersect *theI;
    int iacount=0;
    FORALL(i,CurveList, theC){
        sc_ptr sc = (sc_ptr) theC->m_pe;
        const ON_Curve *c = dynamic_cast<const ON_Curve*>(theC->m_ono);
        m1 =  RXsubModel::SubModel (theC->m_pe);
        FORALL(k,theC->m_Clist ,theI) {
            RXOffset *dummyo;
            RXSTRING buf;
            double len;

            double  s0,s2;
            s0 = c->Domain ().Min();
            s2 = theI->m_t;
            m2 =  RXsubModel::SubModel (theI->m_ce);
            if(m1 !=m2)
                continue;

            const ON_Interval dom = ON_Interval(s0 ,s2 );
            if(dom.Length () > 1e-10) {
                c->GetLength( &len, 1E-8,&dom );
                buf += TOSTRING(len);
            }
            else {
                len=0.0;
                buf = L"0.0";
            }

            RXSTRING ianame(L"iadum_"); ianame+=TOSTRING(iacount++);
            dummyo= dynamic_cast<RXOffset *> (theC->m_pe->AddExpression(new RXOffset(ianame,buf,sc,sc->Esail)));
            theC->m_pe->SetRelationOf(dummyo,aunt,RXO_EXP_OF_ENT,RX_AUTOINCREMENT);
            dummyo->SetAccessFlags(RXE_PRIVATE);
            dummyo->Evaluate(sc ,1);
            sc_ptr  dddd = dynamic_cast<sc_ptr> (theC->m_pe );
            dddd->Insert_One_IA(theI->m_ce,dummyo); // ps is a relsite. p is the seamcurve it lies on
        }
        // fill in End1site,End2site values on the SC;

        if(sc->n_angles >=2) {
            sc->End1site = dynamic_cast<Site*> (sc->ia[0].s);               sc->End1site->SetRelationOf(sc,child|niece,RXO_N1_OF_SC);
            sc->End2site =  dynamic_cast<Site*> (sc->ia[sc->n_angles-1].s); sc->End2site->SetRelationOf(sc,child|niece,RXO_N2_OF_SC);
        }
        else  {
            printf(" less than 2 crosses (%d) in %s so demote to sketch\n",sc->n_angles ,theC->m_pe->name());
#ifdef ONC_DEBUG
            theC->m_pe->Print_One_Entity(stdout);
#endif
            static_cast<RXSeamcurve*> (theC->m_pe)->Demote_To_Sketch();
        }
        sc->Needs_Cutting=0;
        sc->FlagSet( SC_3DM_CUT);
        // turns off needs_cutting (cut?) flag too.

    }

    return 0;
}
bool ONC_Print(ON_NurbsCurve *p) {
    int k,m; 		double s0;
    ON_3dPoint l_p;

    cout<< " PC_Print \t";

    printf(" Kc = %d \t CC = %d,order = %d\n", p->KnotCount(), p->CVCount(),p->Order());
    for(k=0;k<p->KnotCount(); k++) {
        s0 = p->Knot(k);
        m = p->KnotMultiplicity(k);
        p->EvPoint(s0,l_p);
        printf("K\t%d \t%f\t %d\t (\t %f \t%f \t%f\t)\n",k,s0, m,l_p.x,l_p.y,l_p.z);
    }

    for(k=0;k<p->CVCount();k++) {
        p->GetCV(k,l_p);
        printf("CV\t%d \t     \t(\t %f \t%f \t%f\t)\n", k, l_p.x,l_p.y,l_p.z);
    }
    cout<<endl;
    return true;
}
bool ONC_GetLocalBB(const ON_NurbsCurve *c, rxON_BoundingBox *bb, int n, double tol )  {
    // segment between KNot(n) and Knot(n+1) will be inside the convex hull of CVs (n-order+1) to (n+1)
    // we create the BB of these CVs
    int k,order;
    ON_3dPoint p0,   *m, *x;
    double *q;

    m = &(bb->m_min);
    x =  &(bb->m_max);
    assert(c->m_cv_stride>=3);
    order = c->Order();

    q = c->m_cv + ( n-order+2) *c->m_cv_stride;
    *m = ON_3dPoint(q);
    *x=*m;

    // int     m_cv_stride;      // The pointer to start of "CV[i]" is
    //   m_cv + i*m_cv_stride.
    q+=c->m_cv_stride;
    for(k=1;k<order;k++) {

        m->x=min(m->x,*q); 		x->x=max(x->x,*q); q++;
        m->y=min(m->y,*q);		x->y=max(x->y,*q); q++;
        m->z=min(m->z,*q);		x->z=max(x->z,*q); q++;
        q+=(c->m_cv_stride-3);
    }

    ONC_Grow_BoundingBox(*bb, tol);
    return true;
}
/* this ONC_GLBB is really slow. SSD by the fn just above
 BOOL ONC_GetLocalBBSlow(const ON_NurbsCurve *c, rxON_BoundingBox *bb, int n, double tol )  {
// segment between KNot(n) and Knot(n+1) will be inside the convex hull of CVs (n-order+1) to (n+1)
// we create the BB of these CVs
 int k,order,np;
 ON_3dPointArray l_pts;
 rxON_BoundingBox lbb;
 ONC_GetLocalBBFast ( c, &lbb,  n, tol ) ;

 order = c->Order();

 for(k=0;k<order;k++) {
   np = n-order+2+k;	l_pts.AppendNew()= c->CV(np);
 }
 *bb = l_pts.BoundingBox();
 ONC_Grow_BoundingBox(*bb, tol);
 assert(bb->m_min == lbb.m_min);
 assert(bb->m_max == lbb.m_max );
return true;
} 
*/



bool ONC_Grow_BoundingBox(rxON_BoundingBox &bb, double dx) {
    return bb.GrowAllRound(dx);
}


int ONC_Find_One_Intersections(ON_Curve &c1,
                               ON_Curve &c2,
                               double p_MaxSep,
                               ON_ClassArray<RX_OncIntersect> *list1,
                               ON_ClassArray<RX_OncIntersect> *list2){

    int k, count = 0;

    ON_2dPointArray results;
   // const double tol = 0.0000001; //NOTE july 2014 this was too low. Its square is below machine precision
    const double tol = 0.00001; //NOTE july 2014

    results.Empty(); // on polycurves(at least) theIntersect method has a leak(NurbsFOrm)
    if (ONCurveIntersect(&c1, &c2,results,p_MaxSep,tol)) { //what return value is good???????
        for(k=0;k<results.Count(); k++) {
            RX_OncIntersect i1,i2;
            i1.m_ce=NULL;
            i2.m_ce=NULL;
            i1.m_t=results.At(k)->x;
            i2.m_t=results.At(k)->y;
            list1->AppendNew()=i1;
            list2->AppendNew()=i2;
            count++;
        }

    }
    results.Destroy();


    return count;
}
}// end namespace oncut 
