/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 25 May 2003 protected against recursion
 15/11/98 Check Model has various options
 6/5/96 Draw_All_Int_Surfaces(); added
  *  	 20/3/96 if any gaussian seams spotted, make psides before R_Solve
  this is because the Gauss Endwidths routines need Psides
    NOTE. The Needs_Finishing flag is used to stop an attempt at Gauss_Endwidths
   in the compute called from Finish_SC

 *  in WIndows ONly, a lash-up for incomplete resolve
               14.2 95  No HC_Update_Display in compute_Batp loop = faster
              15.12.94     geo solve now up to 50 times - for kites
 *             26.11.94  PH  HC update display if any new panels
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */
//#define PPDB 1

#include "StdAfx.h"
#include "RelaxWorld.h"
#include <QDebug>
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXSeamcurve.h"
#include "rxsubwindowhelper.h"
#include "global_declarations.h"
#include "rxqtdialogs.h"

/* preproc.c 5/8/94 */
#include "targets.h"

#include "RXException.h"
#include "stringutils.h"
#include "entities.h"
#include "resolve.h"

#include "akmutil.h"
#include "material.h"
#include "panel.h"
#include "etypes.h"

#include "printall.h"
#include "peternew.h"
#include "batpatch.h"

#include "rescb.h"

 #include "compute.h"
#include "interpln.h"
#include "finish.h"

#include "f90_to_c.h"

#include "fields.h"
#include "pansin.h"
#include "RXPanel.h"
#include "script.h"
#include "rxqtdialogs.h"

#include "RXSail.h"
/*
 *  function RXSail::Pre_Process
 * optionally reads a script file , then tries to build the model ready for analysis
 * return values:
 * normal return value is iret
 *  iret is initialized to 1
 *  iret is then incremented when paneller returns non-zero
 *
 * return 0 if startmacro returns 0
 * return 0 on message The following items didnt resolve/cant load this model
 * return 0 on message `Cannot complete build:`
 *  return 0 if it fails heuristic test
 *  return 0 if Finish_All return 0
 *
 * Callers interpret return value as follows:
 *  ReadScript returns 0 if Pre_Process returns non-positive
 * Do_Solve sets Needs_Gaussing and returns 0  if Pre_Process returns non-zero
 * ReadBoat returns 0 if Pre_Process returns non-positive
 * */

int RXSail::Pre_Process(  const char*fname) {
    int iret = 1,rcount=0;
    int depth = 0;
    int retry = 1;
    int i,j,k,NLeft;

    RXEntity_p theEnt;
    char buf[256];
    this->Tidy();

    int DELETED = 0;
    int RE_PANEL_FLAG=0;

#ifdef HOOPS 
    HC_Begin_Alias_Search();
    while(HC_Find_Alias(buf)){
        if(strieq("?sail",buf)) {
            HC_UnDefine_Alias("?sail");
            break;
        }
    }
    HC_End_Alias_Search();

    i = List_Open_Segments();
    if(i<2){
        printf(" N Open = %d ",i);
        rxerror("Preproc with few segs open",2);
    }

    HC_Show_Pathname_Expansion(".", buf);
    HC_Define_Alias("?sail",buf);
    HC_QSet_Visibility(".","text=off,faces=off");

    // touch the postproc segments

    this->PostProcExclusive();
    this->PostProcNonExclusive();
    this->PostProcTransformSeg();
#endif
    struct REPORT_FORM *rf = this->GetReportForm();

    if (fname){
        if(startmacro(fname,&depth)) {
            sprintf(buf,"Failed to open/read  file = '%s'",fname);
            rxerror(buf,2);
            return(0); /* couldn't read file */
        }
    }
    /* the following are to make the Atributes menu work */
#ifdef HOOPS
    HC_Open_Segment("dummy/d2/field");
    HC_Close_Segment();
    HC_Open_Segment("dummy/d2/orientation");
    HC_Close_Segment();
    HC_Open_Segment("dummy/d2/Material Type");
    HC_Close_Segment();
    HC_Open_Segment("dummy/d2/arrow");
    HC_Close_Segment();
    HC_Open_Segment("dummy/d2/interpolation surface");
    HC_Close_Segment();

    HC_Open_Segment("seamcurve");
    HC_Set_Selectability(" lines=on");
    HC_Close_Segment();
#endif


   setStatusBar (QString("Resolve : ") + QString(this->GetType().c_str()),this->Client());

    do {
        NLeft = this->Resolve_All_Entities();
        rcount++;
        for(i=0;i<rf->N;i++)
            qDebug()<<"Entity("<<i<<") '"<< rf->z[i].otype <<", "<<  rf->z[i].oname <<" needed "<< rf->z[i].ctype <<", "<< rf->z[i].cname <<"'";


        if(NLeft>0) {
            if(this->GetReportForm()->N) {
                this->SortReportForm();
                for(i=0;i<rf->N;i++)
                    qDebug()<<"after sort \n Entity("<<i<<") `"<< rf->z[i].otype <<"`,`"<<  rf->z[i].oname <<"` needed `"<< rf->z[i].ctype <<"`, `"<< rf->z[i].cname <<"`" ;


                retry = do_resolve_dialog(this->GetGraphic()); /* interactive dialog etc. */

                this->SetFlag(FL_NEEDS_SAVING);

                //                char*rs = Report_To_String(this->GetReportForm());
                //                if(rs) {
                //                   // puts(rs); //     i=rxerror(rs,2);
                //                    RXFREE(rs);
                //                }
#ifdef  NEVER //WIN32 // July 3 2004
                if(i != IDYES) {
                    i= MessageBox(NULL, L"Stop Build??", L"Question", MB_YESNO|MB_ICONSTOP);
                    if(i==IDYES) return 0;
                }

                if(DBG_Create_Wanted_Entities(this,this->GetReportForm()))
                    retry=1;
                else
                    retry=0;
#endif


                if(!retry) {
                    for(i=0;i<rf->N;i++) {
                        printf("Entry (%d) '%s, %s needed %s, %s'\n",i,rf->z[i].otype,rf->z[i].oname,rf->z[i].ctype,rf->z[i].cname);
                    }
                }
            }
            else {

                DELETED = 0;
                for (ent_citer  it = this->MapStart(); it != this->MapEnd(); it=this->MapIncrement(it)) { /* remove unwanted and unresolved RENAMES */
                    theEnt  = dynamic_cast<RXEntity_p>( it->second);

                    if(theEnt->Needs_Resolving && stristr("rename",theEnt->type())) {
                        printf("Killing unwanted rename '%s'\n",theEnt->name());
                        theEnt->Kill();
                        DELETED++;
                    }
                }
                if(!DELETED) {
                    rxerror(" The following items didnt resolve",2);

                    for (ent_citer it = this->MapStart(); it != this->MapEnd(); ++it) {// list unresolved ents. WHY NOT MapIncrement ??
                        RXEntity_p p  = dynamic_cast<RXEntity_p>( it->second);

                        if(p->Needs_Resolving)
                            cout<< p->name()<< "  " <<p->type() <<  "needs resolving"<<endl;
                    }
                    rxerror(" cant load this model",2);
                    return(0);
                }
            }
            Report_Clear(this->GetReportForm());

        } //NLeft

    } // resolve DO
    while(NLeft>0 && retry);
    if(!retry || NLeft<0 ) {
        QString qbuf("Cannot complete build: "); qbuf.append(this->GetQType());
        int rcode = rxerror(qbuf,2);
        qDebug()<<" rxerror returned code "<<rcode;
        return(0);
    }
    setStatusBar (QString("Resolve Done: ") + QString(this->GetType().c_str()),this->Client());
    Check_All_Heap();
    this->SetFlag(FL_HASGEOMETRY,1);

    /*WRONG shouldn't this be before RESOLVE */
    PCTol_Update_Tolerances(this) ; // now this just deletes one 'set' 'tol'

    vector<RXEntity_p >thelist;
    this->MapExtractList(FABRIC,  thelist) ; this->MapExtractList(MATERIAL,  thelist) ;
    vector<RXEntity_p >::iterator itt;
    for (itt = thelist.begin (); itt != thelist.end(); itt++)  {
        RXEntity_p ep  =*itt;
  //  for (it = this->MapStart(); it != this->MapEnd(); it=this->MapIncrement(it)) {
    //    RXEntity_p ep  =  it->second ;
        if(ep->NeedsComputing() && ( FABRIC==ep->TYPE || MATERIAL==ep->TYPE )) {
            Compute_Material_Card(ep); // strange flow.
        }
    }
    if(!this->IsOK( g_Preprocessor_Heuristics)){ 	//rxerror("Model NOT COMPLETE",2);
        return(0);
    }

    SetMsgText("Second Pass",this->Client());

    if(!Finish_All_Entities(this))  {
        return(0);
    }

    SetMsgText("Second Pass done",this->Client());
    /* if there are gaussian seams,  we have to get the endwidth refs
  This calc needs panels, but a special set of panels (Zones), made from
  gaussian and edge SCs and non-gaussian multiple-pside seams only

  This is generated from Trace_Zone from Get_Gaussian_Seam  if a change in
  topology is spotted.
  but it needs breaked SCs. So here we break all if any gauss seams exist

 */
     ent_citer it;
    for (it = this->MapStart(); it != this->MapEnd(); it=this->MapIncrement(it))   {
        theEnt  =  it->second ;
        if(theEnt->TYPE==SEAMCURVE) {
            sc_ptr sc = (sc_ptr  )theEnt;
            if(sc->gauss) {
                Make_All_Psides(this);
                break;
            }
        }
    }
    Check_All_Heap();
    RE_PANEL_FLAG = 0;

    setStatusBar (QString("Solve Geometry : ") + QString(this->GetType().c_str()),this->Client());

    HC_Update_Display();

    do{
        for(i=0;i<2000;i++) {
            k = R_Solve();
            if(k) {
                RE_PANEL_FLAG = 1;
            }
            Make_All_Psides(this); /* WHY SLOW? */
            if(!k) break;
        }

        if(i> 1998)
            rxerror(" solve may not be converged",2);
    }while(!this->GoalSeek( &RE_PANEL_FLAG));

    Check_All_Heap();

    RE_PANEL_FLAG = 0;
    Sort_Nodes(this);// 2009 moved this from inside paneller as a speed opt

       setStatusBar (QString("panel : ") + QString(this->GetType().c_str()),this->Client());
    do {
        Make_All_Psides(this);
        j= Paneller(this);
        if(j) {
            RE_PANEL_FLAG = 1;
        }
        Compute_All_Compound_Curves(this);
    }
    while(Compute_Next_Batpatch(this));

    if(RE_PANEL_FLAG)
    {   iret++;
         setStatusBar (QString("CheckAndDisplayPanels : ") + QString(this->GetType().c_str()),this->Client());
        // the check always returns TRUE and we dont want to waste time displaying them in soqt
        this-> CheckAndDisplayPanels();
        this->SetFlag(FL_HASNEWTOPOLOGY );
    }
    if(g_World->GraphicsEnabledQ()){
         setStatusBar (QString("Draw : ") + QString(this->GetType().c_str()),this->Client());
        DrawDesignWindow( );
  #ifdef HOOPS
        if(g_Display_Mould)
        {
            Draw_All_Int_Surfaces(this);
            Draw_All_Fields(this);
        }
   #endif
        setStatusBar (QString("Done Draw : ") + QString(this->GetType().c_str()),this->Client());
    }
    this->SetFlag(FL_HASPANELS,1);

    for (it = this->MapStart(); it != this->MapEnd(); it=this->MapIncrement(it))
    {
        RXEntity_p ep  =   it->second ;
        if(ep->NeedsComputing() && FABRIC ==ep->TYPE) {
            Compute_Material_Card(ep);
        }
    }
     setStatusBar (QString("Done preprocessing : ") + QString(this->GetType().c_str()),this->Client());
    return(iret);
}


