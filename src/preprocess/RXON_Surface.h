#ifndef _RXON_SURFACE_H_
#define  _RXON_SURFACE_H_

#include "rxON_Extensions.h"


#define RXON_DEFAULT_PARAMETER_TOLERANCE 1.0e-10

#ifdef AMGUI_2005
	#define GLCP_CONVERGENCE_TOLERANCE  1.0E-4 
#else
	#define GLCP_CONVERGENCE_TOLERANCE  RXON_DEFAULT_PARAMETER_TOLERANCE 
#endif

class RXON_NurbsSurface :
	public ON_NurbsSurface
{
public:
	RXON_NurbsSurface(void);
	RXON_NurbsSurface(const ON_NurbsSurface &src);
public:
	int CountSlaves() const;

	~RXON_NurbsSurface(void);

		int FitZ( const ON_3dPointArray &p); // CAREFUL  p is {u,v,Z}
		RXON_NurbsSurface* MapTo(const ON_NurbsSurface*p_s); // p_s has the knots and XY we want. 'this' has the Z we want
		ON_BOOL32 GetLocalClosestPoint( const ON_3dPoint& q, // test_point
			double p_u0,double p_v0,     // seed_parameters
			double* p_pu,double* p_pv,   // parameters of local closest point returned here
			const ON_Interval* p_d1=0, // first parameter sub_domain
			const ON_Interval* p_d2=0,
			const double p_tol = GLCP_CONVERGENCE_TOLERANCE 
        )
		const;  // second parameter sub_domain

		typedef enum {ok,error,hitcorner} GuessStartReturnType;
		 GuessStartReturnType Guess_Start(const ON_Surface*t,const ON_3dPoint &q, double*p_u0,double *p_v0) const;  

                int Flow(const ON_Curve * p_c1, const ON_Curve * p_c2);
 
		HC_KEY DrawHoops(const char *seg=0, const char *atts=0) const;
		HC_KEY DrawNormals(const char *seg=0, const char *atts=0) const;
 
		int Intersect(const ON_Line & p_line, 
				double & p_t, 
				double & p_u1, double & p_u2, 
				const double p_tol)
		const;
// if a CV is coincident in X and Y with a previous one, we set its slave marker 
// and we append it the master's list of slaves.
		int SetCVRelations(const double p_tol=0.01);
		int  IsSlave(const int i1,const int j1 );
		int  IsMaster(const int i1,const int j1 ); 
		int SetRXCVs(const int i, const double x, const double y, const double z,HC_KEY surfacekey);
protected:
		// i1 j1 is master i2 j2 is slave
		int SetIsSlave(const int i1,const int j1,const int i2,const int j2 );
		int FindSlave(const int i1,const int j1,int *i2,int *j2, int &count);
		int PrintSlaves(FILE *fp);

		ON_SimpleArray<int> m_im, m_jm, m_is,m_js;
};

#endif

