// RXCurveIntersection.h: interface for the RXCurveIntersection class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RXCURVEINTERSECTION_H__34B6764A_ABBD_4907_B5C3_4BDEF35DCD64__INCLUDED_)
#define AFX_RXCURVEINTERSECTION_H__34B6764A_ABBD_4907_B5C3_4BDEF35DCD64__INCLUDED_


#include "opennurbs.h"

#include "RXPointOnCurve.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class RXCurveIntersection  
{
public:
	double GetCrvlength(const int &p_ID,double fractional_tolerance = 1.0e-8) const;
	ON_3dPoint EvalPt(const int & p_ID) const;
	double Gett(const int &p_ID) const;
	//!Constructor
	RXCurveIntersection();
	//!Constructor by copy
	RXCurveIntersection(const RXCurveIntersection & p_Obj);

	//!Destructor
	virtual ~RXCurveIntersection();

	void Init();

	int Clear();

	RXCurveIntersection& operator = (const RXCurveIntersection & p_Obj);

	int SetPoint1(const RXPointOnCurve & p_pt);
	int SetPoint2(const RXPointOnCurve & p_pt);

	RXPointOnCurve * GetPoint1();
	RXPointOnCurve * GetPoint2();

	//Returns the distance between the 2 points
	double Distance() const;
protected:
	//!point on the first curve
	RXPointOnCurve m_Point1;
	//!point on the second curve
	RXPointOnCurve m_Point2;


};

#endif // !defined(AFX_RXCURVEINTERSECTION_H__34B6764A_ABBD_4907_B5C3_4BDEF35DCD64__INCLUDED_)
