/* global.h 30/1/94  */

/* standard includes */

#pragma once


#ifdef _X
	#include <X11/Intrinsic.h>
	#include <X11/Xproto.h> 
	#include <X11/Shell.h>

	#include <Xm/Xm.h>
	#include <Xm/DrawingA.h>
	#include <Xm/CascadeBG.h>
	#include <Xm/CutPaste.h>
	#include <Xm/Form.h>
	#include <Xm/LabelG.h>
	#include <Xm/MainW.h>
	#include <Xm/MessageB.h>
	#include <Xm/PushBG.h>
	#include <Xm/RowColumn.h>
	#include <Xm/SeparatoG.h> 
	#include <Xm/ToggleBG.h>
	EXTERN_C Widget opt1Toggle,opt2Toggle,opt3Toggle;

/* topLevel widgets & functions */
	EXTERN_C Widget panelSolveButton;
	EXTERN_C Widget meshButton;


#endif //#ifdef _X

 



