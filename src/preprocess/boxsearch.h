#ifndef _BOXSEARCH_H_
#define _BOXSEARCH_H_

#include "griddefs.h"
//#include "filament.h"

 struct PC_QCD {
	float			l,r,b,t;   //Assumption: should be Left right bottom top, Thomas 07 07 04
	int				monotonic,flags;
	RXCurve *Curve;
	sc_t * sc;
	int				i1,i2;
	struct LINKLIST *insideList;
	struct PC_QCD	*lowerhalf, *upperhalf;
	SAIL *m_sail;
	};  //struct PC_QCD

#define	PCX_DONT_KNOW		0
#define PCX_MONOTONIC		2
#define	PCX_ALL_INSIDE		4
#define PCX_ALL_OUTSIDE		8
#define	PCX_INTERSECTING	16
#define PCX_SURROUNDS		32
#define	PCX_TRIANGLE		64
 
 // the routines
EXTERN_C int PCX_IsPointInside(struct PC_QCD *q1, ON_3dPoint p_t,double tol);
//EXTERN_C int PCX_Set_Dont_Search(sc_ptr sc);
EXTERN_C struct PC_QCD* PCX_Begin_Box_Search(SAIL *sail, class FILAMENT_LOCAL_DATA *fld); // ALLOCS a Pc_QCD,clears SC flags.

EXTERN_C int PCX_Compare_Boxes(struct PC_QCD *q1,struct PC_QCD *q2);

EXTERN_C int PCX_End_Box_Search(struct PC_QCD *q); // FREES q,clears SC 'dont_search'flags. Leaves their inside lists.
EXTERN_C int PCX_Clear_QCD( struct PC_QCD* q); // FREES q. clears all inside_lists.
// call from Update_Curve
#endif  //#ifndef _BOXSEARCH_H_

