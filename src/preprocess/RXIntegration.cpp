// RXIntegration.cpp: implementation of the RXIntegration class.
//
//////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
 
#include "RXIntegration.h"
/* typical useage:
1) instantiate
2)  	integral = Integrate(const double x0,const double x1, const double tol, double*perr);
3) destroy.
TO override, youll usually want to override the constructor and method func.
*/
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

RXIntegration::RXIntegration()
{
m_ncalls=0;
m_p=0;
}

RXIntegration::~RXIntegration()
{

}

double RXIntegration::Integrate( const double x0,const double x1, const double tol, double*perr){
double y0,y1,ymid,err,rv;
int depth=0;
	m_ncalls=0;
	y0=func(x0);
	y1 = func(x1);
	ymid = (y0+y1)/2.0;
	rv =  RIntegrate( x0, x1, y0, y1, ymid,tol,depth, &err);
	if(perr) *perr=err;
	return  rv;
}

double RXIntegration::func(double p_x) {
	m_ncalls++;
	return 2*p_x+1;
}

double  RXIntegration::RIntegrate(
	const double x0,
	const double x1, 
	const double y0,
	const double y1, 
	const double ymid,  // quadratic estimate of the mid=point evaluation
	const double tol, 
	 int depth,
	double*err )
{
	double rv=0, x3,y3,dx,y03, y31,err1=0,err2=0;
	depth++;
	x3 = (x0 + x1)/2.;
	y3 = func(x3);
	dx = (x1-x0);  // ATT double of the classical Simpsons notation
	rv = (y0 + y1 + 4. * y3) * dx/6.0 ; //WANT TO DIVIDE BY sdashed but where do we sample it"
	*err = (y3-ymid) * dx * 2./3.;
	if(depth >12) return rv;
	if(fabs(*err) < tol) return rv;
	y03 = (3.*y0 - y1 + 6.*y3)/8.;
	y31 = (-y0 + 3.*y1 + 6.*y3)/8.;
	rv   =  RIntegrate(x0, x3, y0,y3, y03,tol, depth, &err1);
	rv  +=  RIntegrate(x3, x1, y3,y1, y31,tol, depth, &err2);
	*err=err1 + err2;
return rv;
}

int IntegrationTest(){
	double total, err=0;
 RXIntegration theI =  RXIntegration();
 total = theI.Integrate(0.0,1.0,1e-6,&err);

 printf(" the integral gave %f\t with error = %f\n",total,err);
 printf(" the exact result is %f\n", 2.0);

  total = theI.Integrate(0.0,2.0,1e-6,&err);

 printf(" the integral gave %f\t with error = %f\n",total,err);
 printf(" the exact result is %f\n", 6.0);
 return 1;

}

int RXIntegration::GetNCalls()
{
return m_ncalls;
}
