#include "StdAfx.h"
#include <QDebug>
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXQuantity.h"
#include "RXRelationDefs.h"
#include "RXON_3dPoint.h"
#include "global_declarations.h"
#include "etypes.h"
#include "panel.h"
#include "spline.h"
#include "stringutils.h"
#include "uvcurve.h"
#include "entities.h" // for MAXLINELENGTH
#include "words.h"
#include "gcurve.h"
#include "rxbcurve.h"

//we have

//  Resolve_ 2D_Curve_Card   , xy with regular syntax, fills in (p,c)                      RXCurve2D
//  Resolve_3 D_Curve_Card,      , xyz with regular syntax, atts at end, fills in (p,c)    RXCurve3D
//  Resolve_ UVCurve_Card    , UVCurve : name : mould: atts :u,v,u,v,  fills in (p,c)      RXCurveUV
//  Resolve_ Spline_Card    , xyz with regular syntax, fills in  (pin)		        RX Spline
//  Resolve_ Gauss_Curve_Card(RXEntity_p e);			                        RXGaussCurve
//  Resolve_ Base_Curve_Card , xyz with strange syntax, is SUPERCEDED


RXBCurve::RXBCurve()
{
    assert("dont use default SC constructor"==0);
}
RXBCurve::RXBCurve(class RXSail *p ):
    RXEntity (p),
    p(0),
    c(0),
    BCtype(-999 )
{
    this->SetParent(p);
}
RXBCurve::~RXBCurve() {
    this->CClear();
}
class RXBCurve* RXBCurve::DataPtr() // nasty transition from BASE_CURV
{
    return this;
}

RXCurve2D::RXCurve2D(class RXSail *p ):
    RXBCurve (p)
{
    BCtype=0;
}
RXCurve2D::RXCurve2D( ) { assert(0);}
RXCurve2D::~RXCurve2D( ) {}

RXCurve3D::RXCurve3D(class RXSail *p ):
    RXBCurve (p)
{
    BCtype=0;
}
RXCurve3D::RXCurve3D( ) { assert(0);}
RXCurve3D::~RXCurve3D( ) {}

RXCurveUV::RXCurveUV(class RXSail *p ):
    RXBCurve (p)
{
    BCtype = BCUVCURVE;
}
RXCurveUV::RXCurveUV( ) { assert(0);}
RXCurveUV::~RXCurveUV( ) {}

RXGaussCurve::RXGaussCurve(class RXSail *p ):
    RXBCurve (p)
{
    BCtype=0;
}
RXGaussCurve::RXGaussCurve( ) { assert(0);}
RXGaussCurve::~RXGaussCurve( ) {}


int RXBCurve::Compute(void){//see  int Compute_Basecurve(RXEntity_p p)

    RXEntity_p mould = this->Esail->GetMould();

    if(this->BCtype ==BCUVCURVE) {
        return(Compute_UV_Curve( this, mould,this->Esail->m_Linear_Tol) );
    }
    if(this->AttributeGet("$moulded")){
        if(!mould) { cout<< " Basecurve cant find the mould"<<endl; return 0;}
        if(mould->NeedsComputing()) { cout<< "mould->Needs _Computing\n"<<endl; return 0;}
        printf("Pull '%s'  to mould\n", this->name());
        PC_Pull_Pline_To_Mould(mould,&(this->p),&(this->c),1,this->Esail->m_Linear_Tol/10.0);
        this->SetNeedsComputing(0);
        this->Needs_Finishing=0;
    }
    return(0);
}


int RXBCurve::Finish(void){
    Compute();
    return 1;// see  Finish_Basecurve_Card
}

int RXBCurve::CClear(){
    int rc=0;

    if(this->p) RXFREE(this->p); this->p=0; this->c=0;
    return rc;
}

int RXBCurve::Resolve() {
    /* basecurve card is of type

      basecurve: <name> : (x ,y,z)(x,y,z).....      */

    int retval = 0;
    std::string sline( this->GetLine() );
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();
    if(nw<2) return 0;

    const char *name=wds[1].c_str();
    const char *s=wds[2].c_str();

    if (!String_To_Polyline(s,&(this->p),&(this->c)) ) {
        printf(" this is %s %s with line\n%s\n", type(),name,GetLine());
        OutputToClient("String_To_Polyline failed",2);
        CClear();
        return(0);
    }
    if(PC_Finish_Entity(type(),name,this,(long)0,NULL,0,GetLine())) {
        SetNeedsComputing(0);
        Needs_Resolving=0;
        retval=1;
    }
    return(retval);
}



int RXBCurve::String_To_Polyline(const char *s,VECTOR **p, int *c){

    /* a string is of form < x,y,z /x,y,z/...*/
    char s2[128];
    float x,y,z;
    int  n;
    printf("TODO: rewrite STRING TO POLYLINE on  \n%s\n",s);
#ifndef HOOPS
    assert(0);
    return 0;
#else
    (*p)=NULL;
    (*c)=0;
    for (;;)    {
        if (!HC_Parse_String(s,"/",*c,s2)  ) // TODO: rewrite STRING TO POLYLINE
            break;

        n=sscanf(s2," %f %f %f",&x,&y,&z);
        if (n==3) {
            *p = (VECTOR*) REALLOC (*p,(2+(*c))*sizeof(VECTOR));
            assert((*p));
            (*p)[(*c)].x=x;
            (*p)[(*c)].y=y;
            (*p)[(*c)].z=z;
            (*c)++;
        }
        else {
            printf(" unable to read BaseCurve line \n%s\n",s);
            g_World->OutputToClient("QUIT - bad basecurve format",3);
            return 0;
        }
    }
    return(1);
#endif
}

int RXBCurve::Dump( FILE *fp) const
{
    int rc =  fprintf(fp,"   Output Points\n");
    for (int k=0;k<this->c;k++)
        rc+=fprintf(fp,"\t\t\t%d \t%f\t%f\t%f\n",k, p[k].x,p[k].y,p[k].z);
    return 0;
}
int RXCurve2D::Dump( FILE *fp) const
{
    fprintf(fp,"   A 2D curve. type=%d\n",this->BCtype);
    return RXBCurve::Dump(fp);
}
int RXCurve3D::Dump( FILE *fp) const
{
    fprintf(fp,"   A 3D curve. type=%d\n",this->BCtype);
    return RXBCurve::Dump(fp);
}
int RXCurveUV::Dump( FILE *fp) const
{
    fprintf(fp,"   A UV curve. type=%d\n",this->BCtype);
    return RXBCurve::Dump(fp);
}
int RXGaussCurve::Dump( FILE *fp) const
{
    fprintf(fp,"   A Gausscurve. type=%d\n",this->BCtype);
    this->m_pin.Dump(fp);
    fprintf(fp," Output\n");
    return RXBCurve::Dump(fp);
}

int RXGaussCurve::ReWriteLine(void){
    int k;
    RXSTRING NewLine;
    // char new_line[MAXLINELENGTH];
    //  char str[1024];
    assert("TODO: step rewrite of gausscurve"==0);
    // sprintf(new_line,"%s%s%s",type(),RXENTITYSEPSTOWRITE,this->name());
    NewLine = TOSTRING(type()) + TOSTRING(RXENTITYSEPSTOWRITE)+TOSTRING(this->name());
    for(k=0;k<this->m_pin.size();k++) {
        RXTriplet tt = this->m_pin[k];
        ON_3dPoint q = tt.ToONPoint();
        //   sprintf(str,"%s%f%s%f",RXENTITYSEPSTOWRITE,q.x,RXENTITYSEPSTOWRITE, q.y);
        //   strcat(new_line,str);
        NewLine+= TOSTRING(RXENTITYSEPSTOWRITE); NewLine+=TOSTRING(q.x);
        NewLine+= TOSTRING(RXENTITYSEPSTOWRITE); NewLine+=TOSTRING(q.y);
    }
    this->SetLine(NewLine);
    return 1;
}
int  RXCurve2D::ReWriteLine(void){

    char new_line[MAXLINELENGTH];
    RXSTRING NewLine;
    ON_String l_line;
    RXObject*old ;
    RXExpressionI *le ;
    char str[128];
    int k;
    sprintf(new_line,"%s%s%s",this->type(),RXENTITYSEPSTOWRITE,this->name());
    for(k=0;k<this->c;k++) {
        sprintf(str,"%s%f%s%f",RXENTITYSEPSTOWRITE,this->p[k].x,RXENTITYSEPSTOWRITE,p[k].y);
        strcat(new_line,str);
    }
    this->SetLine(new_line);
    return(1);
}
int  RXBCurve::ReWriteLine(void){

    RXEntity_p e	 =this;
    char new_line[MAXLINELENGTH];
    std::string NewLine;
    ON_String l_line;
    RXObject*old ;
    RXExpressionI *le ;
    char str[128];
    int k;
    sprintf(new_line,"%s%s%s",e->type(),RXENTITYSEPSTOWRITE,e->name());
    for(k=0;k<this->c;k++) {
        sprintf(str,"%s%f%s%f%s%f",RXENTITYSEPSTOWRITE,p[k].x,RXENTITYSEPSTOWRITE,p[k].y,RXENTITYSEPSTOWRITE,p[k].z);
        strcat(new_line,str);
    }
    this->SetLine(new_line);
    return(1);
}

int RXCurve2D::Resolve() {
    /*  A simple 2D polyline
         2Dcurve : name : x :y:x:y,.....      */
    int k, retval = 0;
    Separators_to_Colons(this->line); // commas and fwd slashes
    std::string sline( GetLine() );
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();
    if(nw<6) return 0;
    const char *name = wds[1].c_str();
    const char *sx,*sy;


    this->p=NULL; 	k = 2;

    for(k=2;k<nw-1;k+=2) {
        sx = wds[k].c_str();
        sy=wds[k+1].c_str();
        (this->c)++;
        this->p = (VECTOR *)REALLOC(this->p,(this->c)*sizeof(VECTOR));
        this->p[(this->c)-1].x = (float)Evaluate_Quantity(Esail, sx,"length"); //WRONG use onetime eval
        this->p[(this->c)-1].y = (float)Evaluate_Quantity(Esail, sy,"length");
        this->p[(this->c)-1].z = (float) 0.0;
    }
    if(PC_Finish_Entity(type(),name,this,(long)0,NULL,"2DCurve",GetLine())){
        retval = 1;
        Needs_Resolving= 0;
        SetNeedsComputing(0);
    }
    return(retval);
}
int  RXCurve3D::Resolve() {
    /* 	 3Dcurve : name : x:y:z: x:y:z,..... attributes    . minimum 8  */


    Separators_to_Colons(this->line);  // commas and fwd slashes
    int k, retval=0,err=0;

    const char*line=GetLine();
    std::string sline( GetLine() );
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();
bool mallocked;
double tolsq = pow(this->Esail->m_Linear_Tol,2);
    const char*atts=wds.back().c_str();
    const char *name=wds[1].c_str();
    //   if(this->m_pONMO)  we could skip reading the coordinates from the text
    // something like
         const ON_Curve *onc = 0;

         const ON_PolylineCurve *pc=0;
     if(this->m_pONMO)
          onc= ON_Curve::Cast(this->m_pONMO->m_object);
     if(onc)
     {
        pc = ON_PolylineCurve::Cast(this->m_pONMO->m_object); mallocked=false;

        if(!pc)
        {
            pc=CurveToPolyline(onc,  tolsq, err) ;
            mallocked=true;
        }


        if(pc) {
            const int np = pc->m_pline.Count(); this->c=np;
            this->p = (VECTOR *)MALLOC(np*sizeof(VECTOR));
            for(k=0;k<np;k++) {
                this->p[k].x = pc->m_pline.At(k)->x;
                this->p[k].y = pc->m_pline.At(k)->y;
                this->p[k].z = pc->m_pline.At(k)->z;
            }
            if(mallocked) delete pc;
            if(PC_Finish_Entity(type(),name,this,(long)0,NULL,atts,line)){
                Needs_Resolving=0;
                if(this->AttributeFind("moulded" ))
                    Needs_Finishing=1;
                SetNeedsComputing(0);
                retval=1;
            }
            return(retval);
        }
    }

    // else we read the line
    if(nw<8) return 0;
    this->p=NULL; 	k = 2;
    for(k=2;k<nw-2;k+=3) {
        RXExpressionI *rxe = AddExpression ( new RXQuantity(L"dum",TOSTRING(wds[k].c_str () ),L"m", Esail ) );
        this->SetRelationOf(rxe,aunt,RXO_EXP_OF_ENT); //done
        if(! rxe->ResolveExp())
            break;
        double X=rxe->evaluate(); delete rxe;
        (this->c)++;
        this->p = (VECTOR *)REALLOC(this->p,(this->c)*sizeof(VECTOR));
        this->p[(this->c)-1].x =(float) X; //(float)Evaluate_Quantity(Esail, sx,"length");
        this->p[(this->c)-1].y = (float)Evaluate_Quantity(Esail, wds[k+1].c_str(),"length");
        this->p[(this->c)-1].z = (float)Evaluate_Quantity(Esail,  wds[k+2].c_str(),"length");
    }
    k = 2 + this->c*3;  if(k<nw-1) atts = wds[nw-1].c_str();
    if(PC_Finish_Entity(type(),name,this,(long)0,NULL,atts,line)){
        Needs_Resolving=0;
        if(stristr(atts,"moulded"))
            Needs_Finishing=1;
        SetNeedsComputing(0);
        retval=1;
    }
    return(retval);
}

int RXGaussCurve::Resolve() {
    /* takes a card of the form
       gausscurve : name : x ,y,x,y,.....      */

    int retVal = 0;
    Separators_to_Colons(this->line);  // commas and fwd slashes
    int k ;

    std::string sline( GetLine() );
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();
    int npts = (nw-2)/2;
    if(npts<2) return 0;
    this->p=NULL;  this->c=0;
    for(k=2  ;k<nw-1; ){
        RXSTRING sx=TOSTRING( wds[k++] .c_str());
        RXSTRING sy=TOSTRING( wds[k++].c_str());
        this->m_pin.push_back (RXTriplet(this->Esail  ,sx ,sy,L"0.0"));
    }
    if(PC_Finish_Entity(type(),wds[1].c_str(),0,(long)0,NULL,"GaussPts",GetLine())) {
        retVal = 1;
        Needs_Resolving= 0;
        SetNeedsComputing();
        Needs_Finishing = 0;
        this->m_pin.ToVectorArray( &p, &c);
    }

    return(retVal);
}


int  RXCurveUV ::Resolve(){
    Separators_to_Colons(this->line);// commas and fwd slashes
    /*
         UVCurve : name : mould: atts :u,v,u,v.......
          return 0 if cant alloc or cant read or no mould entity. */

    const char *sx,*sy;
//    RXCurveUV*this =this ;
    int k=0 ;
    std::string sline( this->GetLine() );
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();
    if(nw<8) return 0;
    int np = (nw-4)/2;  assert(" TODO: step UV curve resolve"==0);
    const char *name=wds[1].c_str();
    const char*mould=wds[2].c_str();
    const char*atts =wds[3].c_str();
    SAIL *sail =Esail;
    if(!sail->GetMould()) /* this looks WRONG */
        if(! Esail->Get_Key_With_Reporting("interpolation surface","mould"))
            return 0;

    BCtype=BCUVCURVE; this->c=np;
    this->p = (VECTOR *)MALLOC(np*sizeof(VECTOR));
    if(!this->p) {CClear();  return(0);	}
    k =4;
    for(int i=0;i<np;i++) {
        sx = wds[k++].c_str();
        sy = wds[k++].c_str();
        this->p[i].x=(float) atof(sx);
        this->p[i].y=(float) atof(sy);
        this->p[i].z=0;
    }

    PC_Finish_Entity(type(),name,this,(long)0,NULL,"3dcurve",GetLine());
    this->AttributeAdd(atts);
    Needs_Resolving=0;
    Needs_Finishing=1;
    SetNeedsComputing(0);
    return(1);
}

int RXGaussCurve::Group(const int nmax, const double tol)
{
    return this->m_pin.Group(nmax,tol);
}

/***************************************************************/
int RXGaussCurve:: Compute_GaussCurve(float Chord,int Seam,double p_Linear_Tol) {
    /* The input X values are assumed normalised on the SC length.
         The imposed-angles are grouped into a reasonable number. Say 16
         and then generates a cranked curve of the right arc-length*/



    /* INPUT
      the pin of the gausscurve
       chord is the straight-line distance between the end points in m
       we are trying to find a polyline of the same chordlength

       RETURNS

       in (*c,p) of the gausscurve curve. It is the correct length but needs
       to be placed between endpoints.

       */
    int N,k,ko, loops=0;
    double arc,New_Chord;
    double nominal_Length;
    double a0=0.0;
    double errA, dy;
    double errX, dx;
    int *crank;
    float *x;
    double *a;
    VECTOR *pout;
    if(g_TRACE) printf("RXGaussCurve:: Compute_GaussCurve \n");
    if(g_Gcurve_Count < 3) {
        this->OutputToClient  ("g_Gcurve_Count=128",2);
        g_Gcurve_Count=128;
    }
    N=0;
    if ( this->Group(g_Gcurve_Count,p_Linear_Tol ))
        // if(Group_Polyline( gc->p ,&gc->c ,g_Gcurve_Count,p_Linear_Tol))
        printf( " Grouped %s\n",this->name());
    N = this->m_pin.size() ;

    crank = (int*)MALLOC(sizeof(int)*(N+1) );
    x     = (float *)MALLOC(sizeof(float)*(N+1) );
    a     = (double *)MALLOC(sizeof(double)*(N+1) );
    pout = (VECTOR *)CALLOC(3+N, sizeof(VECTOR));
    if(!pout) 	this->OutputToClient ("out of mem in panel",3);

    for (k=0;k<N;k++) {
        crank[k] = 1;
        x[k] =  m_pin[k].X()*Chord;
        a[k] =  -m_pin[k].Y();
        if (Seam)
            a[k] = 0.5 * a[k];
    }

    nominal_Length= Chord;
    do              /* loop until arc-length stops changing */
    {
        double tx,ty;//,ltz,lnorx,lnory,lnorz;
        tx = cos(a0); ty = sin(a0); //ltz=0.0;
        // lnorx = -ty;       lnory = tx;       lnorz=0.0;
        pout[1].x = (float) (tx* nominal_Length);
        pout[1].y = (float) (ty* nominal_Length);
        ko = 2;
        //       assert("RXPOLYline"==0);
        for (k=0;k<N;k++) {
            x[k] =m_pin[k].X()* (float)nominal_Length;
        }

        Crank_Polyline(&pout,&ko,x,a,crank,N);  /* pout is now MALLOCed to the right size */

        New_Chord = (float) PC_Dist(&(pout[0]),&(pout[ko-1]));
        arc =  PC_Polyline_Length(ko,pout);

        if(New_Chord > FLOAT_TOLERANCE) {
            errA = (float)-atan2(pout[ko-1].y-pout[0].y,pout[ko-1].x-pout[0].x);
        }
        else
            errA=(float)0.0;
        errX = Chord-New_Chord;

        dx = errX * arc/New_Chord;
        dy = errA;

        a0=a0 + dy;
        nominal_Length = nominal_Length + dx;

    } while ((fabs(errX)>p_Linear_Tol
              || fabs(New_Chord*errA)>p_Linear_Tol )
             && (100>loops++));

    this->c = ko;
    this->p = (VECTOR *)REALLOC (this->p,sizeof(VECTOR)*(ko+1));
    for (k=0;k<ko;k++) {
        this->p[k].x = pout[k].x;
        this->p[k].y = pout[k].y;
        this->p[k].z=  pout[k].z;
    }
    /*      At end, measure chord and chordangle
       Increment initial angle
       Increment ref length LOOP */
    RXFREE(x); RXFREE(a); RXFREE(crank); RXFREE(pout);

    return(1);
}
