#ifndef _READSTRING_H_
#define  _READSTRING_H_


//EXTERN_C int Resolve_String_Card(RXEntity_p e);
EXTERN_C int PCS_Convert_String_Line(char **line,const char *type,const char *name);
EXTERN_C int PCS_Combine_String_Lines(RXEntity_p old,char *line);
EXTERN_C int PCS_Rewrite_String_Line(const char*type,const char*name, char **line, char *l1,char *l2,double EA,double Ti,double L0,const QString &qatts );
//EXTERN_C int PCS_Parse_String_Line(const char *line,char **l,double *EA,double *Ti,double *L0,char **atts);
extern int PCS_Parse_String_Line(const char *line,char **l,double *EA,double *Ti,double *L0,QString &qatts);


EXTERN_C RXEntity_p  PCS_Create_Curve_From_Ends(RXEntity_p p1,RXEntity_p p2,char *atts, const int pDepth);
#endif  //#ifndef _READSTRING_H_



