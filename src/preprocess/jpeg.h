#ifndef _PJPEG_H_
#define _PJPEG_H_

#define  HAVE_BOOLEAN
#ifdef linux
#include <jpeglib.h>

int my_write_jpeg_file(JSAMPLE *data,int width,int height,char *fname,int quality);
#endif
#endif
