/* data constants */

#ifndef PANSAIL_16NOV04
#define PANSAIL_16NOV04

#define MAXPAN  (1024 but it doesnt matter)
#define MAXCOM  24	// was 24 for athena
#define MAXROW  36	//	was 36 for athena


/*
EXTERN_C int PansailDone;
EXTERN_C int N[MAXCOM];
EXTERN_C int M[MAXCOM];
EXTERN_C int pModel[MAXCOM];
EXTERN_C char pName[MAXCOM][16];
EXTERN_C float *Uv;
EXTERN_C float *Vv;
EXTERN_C float *Xv;
EXTERN_C float *Yv;
EXTERN_C float *Zv;
EXTERN_C float *Xcol;  // was Xc, Yc, Zc but we want to see where they are used 
EXTERN_C float *Ycol;
EXTERN_C float *Zcol;
EXTERN_C float *Uc;
EXTERN_C float *Vc;
EXTERN_C float *Dcp;
EXTERN_C float *CpU;
EXTERN_C float *CpL;
EXTERN_C float *LU;
EXTERN_C float *HL;
EXTERN_C float *phiU;
EXTERN_C float *phiL;
EXTERN_C float *CfU;
EXTERN_C float *CfL;
EXTERN_C int *iblstu;
EXTERN_C int *iblstl;
EXTERN_C float *LSD;
EXTERN_C float *BLF;	
EXTERN_C float * Thu;
EXTERN_C float * Thl;
EXTERN_C float * Hu;
EXTERN_C float * Hl;
EXTERN_C float * Cfu;
EXTERN_C float * Cfl;
EXTERN_C float * Singo;
EXTERN_C int coffset[MAXCOM];
EXTERN_C int goffset[MAXCOM];
EXTERN_C int noffset[MAXCOM];
EXTERN_C int moffset[MAXCOM];
EXTERN_C int nwakeso;
EXTERN_C int g_ntwako;
EXTERN_C int ntoto;
EXTERN_C int ncuto;
EXTERN_C int mwko[MAXCOM];
EXTERN_C int nwko[MAXCOM];
EXTERN_C int irelxo[MAXCOM][MAXROW][MAXROW];		 these are mwk(pModel) * nwk(pModel) in size 	
						 BUT mwk and nwk are returned to me !! 	
EXTERN_C float xwakeo[MAXCOM][MAXROW][MAXROW];
EXTERN_C float ywakeo[MAXCOM][MAXROW][MAXROW];  	 defined to (MAXROW,MAXROW,MAXCOM)     	
EXTERN_C float zwakeo[MAXCOM][MAXROW][MAXROW];
EXTERN_C char namwako[MAXCOM][16];
EXTERN_C char baseName[20];
*/

//  EXTERN_C float mlength; SSD
//  EXTERN_C float Vwind;
//  EXTERN_C float Heading;
//  EXTERN_C float Leeway;
//  EXTERN_C float Vboat;

// EXTERN_C float ReflectHeight;	/* displacement of reflection plane from bottom of sail ? */
// EXTERN_C int keepWake;
// EXTERN_C int nrelax;
// EXTERN_C int xyplane;
// EXTERN_C double relfac;

EXTERN_C void strlin_(int*answer);
EXTERN_C void velpt_(float*x,float*y,float*z,float*u,float*v,float*w);

/*
      subroutine pansail(baseo,mo,no,ncompo,nmodelo,cnames,nxypln,nr,
     &           reflen,zrefl,dgheel,xvert,yvert,zvert,
     &           xcol,ycol,zcol,uvo,vvo,uco,vco,cpu,cpl,dcp,
     &           thu,thl,hu,hl,cfu,cfl,singo,
     &           nwakso,g_ntwako,ntoto,ncuto,mwko,nwko,
     &           xwakeo,ywakeo,zwakeo,irelxo,namwak,ians1,ians2,ierror,
     &           isetup,nmastp,zmdiam,diam,relfac)
      integer MAXCOM,MAXROW
      parameter( MAXCOM = something in fortran, MAXROW = something )
      integer ncompo
      integer mo(ncompo),no(ncompo),nmodelo(ncompo)
      integer nwakso,g_ntwako,ntoto,ncuto
      integer mwko(MAXCOM),nwko(MAXCOM)
      integer irelxo(MAXROW,MAXROW,MAXCOM)
      integer ians1,ians2,isetup

      real*4 xwakeo(MAXROW,MAXROW,MAXCOM),ywakeo(MAXROW,MAXROW,MAXCOM)
      real*4 zwakeo(MAXROW,MAXROW,MAXCOM)
      real*4 reflen,dgheel
      real*4 xvert(*),yvert(*),zvert(*),dcp(*)
      real*4 xcol(*),ycol(*),zcol(*)
      real*4 uvo(*),vvo(*),uco(*),vco(*)
      real*4 cpu(*),cpl(*)
      real*4 thu(*),thl(*),hu(*),hl(*),cfu(*),cfl(*)
      real*4 singo(*)
      real*4 nmastp(MAXCOM)
      real*4 zmdiam(MAXROW,MAXCOM),diam(MAXROW,MAXCOM)
      character*20 baseo
      character*16 cnames(MAXCOM),namwak(MAXCOM) */

#ifdef NEVER
//#ifndef linux
EXTERN_C void pansail_(char baseo[20], 				
	int *mo,int *no,int *ncompo,int *nmodelo, 	
	char cnames [MAXCOM][16]			, 
	int *nxypln,int *nr				, 		
	float *reflen, float *zrefl,float *dgheel	,	
   	float *xvert, float *yvert, float *zvert	,	
	float *xcol,float *ycol,float *zcol		,			
	float *uvo,float *vvo,float *uco,float *vco	,	
	float *cpu,float *cpl,float *dcp		,		
	float *thu,float *thl,float *hu,float *hl	,	
	float *cfu,float *cfl,float *singo		,		
	int *nwakso,int *g_ntwako,int *ntoto,int *ncuto	,	
	int *mwko,int *nwko				,				
	float xwakeo[MAXCOM][MAXROW][MAXROW]		,
	float ywakeo[MAXCOM][MAXROW][MAXROW]		,
	float zwakeo[MAXCOM][MAXROW][MAXROW]		,	
	int irelxo[MAXCOM][MAXROW][MAXROW]		,
	char namwak[MAXCOM][16]				,		
	int *ians1,int *ians2,int *ierror,int *isetup,	
	float nmastp[MAXCOM],
	float zmdiam[MAXCOM][MAXROW],float diam[MAXCOM][MAXROW] ,double *relfac);

#else
EXTERN_C void pansail_(char baseo[20], 				
	int *mo,int *no,int *ncompo,int *nmodelo, 	
	char cnames [MAXCOM][16]			, 
	int *nxypln,int *nr				, 		
	float *Reflen, float *Zrefl,float *Dgheel	,	
   	float *Xvert, float *Yvert, float *Zvert	,	
	float *xcol,float *ycol,float *zcol		,	
	float *uvo,float *vvo,float *uco,float *vco	,	
	float *cpu,float *cpl,float *dcp		,		
	float *thu,float *thl,float *hu,float *hl	,	
	float *cfu,float *cfl,float *singo		,		
	int *nwakso,int *Ntwako,int *Ntoto,int *Ncuto	,	
	int *Mwko,int *Nwko				,	
	float Xwakeo[MAXCOM][MAXROW][MAXROW]		,
	float Ywakeo[MAXCOM][MAXROW][MAXROW]		,
	float Zwakeo[MAXCOM][MAXROW][MAXROW]		,	
	int   Irelxo[MAXCOM][MAXROW][MAXROW]		,
	char namwak[MAXCOM][16]				,		
	int *ians1,int *ians2,int *ierror,int *isetup,	
	float nmastp[MAXCOM],
	float zmdiam[MAXCOM][MAXROW],float diam[MAXCOM][MAXROW],double *Relfac,
	long c1, long c2, long c3);

#endif
 

#endif //#ifndef PANSAIL_16NOV04

