#ifndef _RX_FE_EDGE_H__
#define  _RX_FE_EDGE_H__
// #incl ude "op ennu rbs.h"


#ifndef RXFEOBJECTCLASSID  
	#define RXFEOBJECTCLASSID "C701E5F4-0001-4435-B440-8BEEA68423AC"
#endif

#define RXOBJ_INITIAL_N -199

class RXSail;

class RX_FEObject
{
public:
	RX_FEObject(void);
	RX_FEObject(class RXSail *p );
	virtual ~RX_FEObject(void);
	RXSTRING TellMeAbout(void) const;
	int CClear();
        // if it does exist we can reference it with N
private:
	int m_No;//  the unique(for each subclass) ID number.  Indexes into RXSail::m_Fxxx_lists and into fortran model's entity lists
	bool m_RXFEexists;
protected:


	class RXSail *m_sail;
#define NO_FEO_UUID

public:
	class RXSail* GetSail(void) const;
    bool SetN(const int n);
	int GetN(void) const;
    void SetIsInDatabase(const bool p=true);
    bool  IsInDatabase(void) const;
};
#endif
