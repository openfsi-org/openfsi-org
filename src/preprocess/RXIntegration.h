// RXIntegration.h: interface for the RXIntegration class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RXINTEGRATION_H__2591D43E_F687_47B9_8707_F445EFF1F076__INCLUDED_)
#define AFX_RXINTEGRATION_H__2591D43E_F687_47B9_8707_F445EFF1F076__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class RXIntegration  
{
public:
	int GetNCalls();
	RXIntegration();
	virtual ~RXIntegration();

	double Integrate(const double x0,const double x1, const double tol, double*perr);

protected:
	int m_ncalls;
	void *m_p; // a pointer to useful data.
	virtual double func(double p_x);
private:
	double  RIntegrate(
	const double x0,
	const double x1, 
	const double y0,
	const double y1, 
	const double ymid,  // quadratic estimate of the mid=point evaluation
	const double tol, 
	 int depth,
	double*err );

};

EXTERN_C int IntegrationTest();

#endif // !defined(AFX_RXINTEGRATION_H__2591D43E_F687_47B9_8707_F445EFF1F076__INCLUDED_)
