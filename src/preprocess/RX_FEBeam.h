#pragma once
#include "RX_FEString.h"
#include "RXEntity.h"
//		keyword "beamstring"  TYPE		PCE_BEAMSTRING
#define RXB_s   	0
#define RXB_eixx	1
#define RXB_eiyy	2
#define RXB_gj  	3
#define RXB_ea		4
#define RXB_beta	5
#define RXB_ti  	6
class RX_FEBeam :
	public RX_FEString
{
public:
	RX_FEBeam(void);
	RX_FEBeam(SAIL *s);
    virtual ~RX_FEBeam(void);
    virtual int CClear();
 	virtual int Dump(FILE *fp) const;
	int Compute(void);
	int Resolve( RXEntity_p sce);
	int Finish(void);
	int ReWriteLine(void);
    int PostMesh(void);
    int PostToMirror();

    // removes the string from the FEA database
    virtual int ClearFEA(void);
    std::vector<int>  Elements() { return m_ElementNos;}
protected:
	// put the elements into the FEA database
	int AddFEA(const int n_edges);

	int MakeSixNodes();
	std::vector<int> m_ElementNos;
};

class RXBeamMaterial :
	public RXEntity
{
	friend class RX_FEBeam;
public:
	RXBeamMaterial(void);
	RXBeamMaterial(SAIL *s);
	~RXBeamMaterial(void);
	virtual int Dump(FILE *fp) const;
	int Compute(void);
	int Resolve(void);
	int Finish(void);
	int ReWriteLine(void);
    int Evaluate(const double f, double emp[6]);
protected:
 // evaluate the table as  RXQuantities.
    int Translate(); // return 1 if OK.
	RXSTRING m_TableText;
    struct PC_TABLE*m_btable;
    std::vector<double*> m_bvals;
public:
	int m_whichway;
};
