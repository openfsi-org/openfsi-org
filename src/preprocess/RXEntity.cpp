#include "StdAfx.h"
#include <QDebug>
#include <QMessageBox>
#include "RXSail.h"
#include "rxsubwindowhelper.h"
#include "RXGraphicCalls.h"
#include "RXAttributes.h"
#include "global_declarations.h"
#include <iostream>
using namespace std;
#include "entities.h"
#include "stringutils.h"
#include "traceback.h"
#include "etypes.h"
#include "panel.h"
#include "words.h"
#include "script.h"
#include "RX_UI_types.h"

#include "RXEntity.h"

RXEntity::RXEntity(void)
    :	m_type(0),
      m_name(0),
      //    attributes(0),
      line(0),
      Needs_Finishing(0),
      Needs_Resolving(0),
      m_CHANGED(0),//*   used  in UpdateActive, probably boolean but not confirmed*/
      edited(0),
      generated(0),
      TYPE(0),
      hoopskey(0),
      Esail(0),
      m_pONMO(0)
{

}
RXEntity::RXEntity(class RXSail*p)
    :	m_type(0),
      m_name(0),
      //     attributes(0),
      line(0),
      Needs_Finishing(0),
      Needs_Resolving(0),
      m_CHANGED(0),//*   used  in UpdateActive, probably boolean but not confirmed*/
      edited(0),
      generated(0),
      TYPE(0),
      hoopskey(0),
      Esail(p),
      m_pONMO(0)
{
    this->SetParent(p);
}
void RXEntity::SetLineLwr()
{
    strtolower(this->line);
}
const char* RXEntity::name()const
{	
    return m_name;
    //std::string ss;
    //if(m_name) return m_name;
    //if(this->GetOName().length()>0) {
    //	ss = ToUtf8(this->GetOName());
    //	return ss.c_str();
    //}
    //return 0;
}
QString RXEntity::Descriptor()const
{
    QString q(this->type());
    q+="$"+this->GetQName();


    if(this->Esail)
        q.prepend(this->Esail->GetQType()+"$");
    return q;
}

void RXEntity::SetLine(const std::string &s){
    if(this->line)
        RXFREE(this->line);
    this->line=(char*)MALLOC(s.length()+1);
    strcpy( this->line,s.c_str());
}
void RXEntity::SetLine(const RXSTRING &s){
    SetLine(ToUtf8(s));
}

/*   RXExpression::OnValueChange needs to be called recursively to ensure that the entities that depend on it get their NC flags set
 *   that is because the Recursive_Solve seeds on entities not on expressions.
 *   as a speed optimisation, RXEntity::SetNeedsComputing is non-recursive. That's OK because R-Solve will recur into any niece Expressions.
 *March 2014 that was WRONG
 **/
void  RXEntity::SetNeedsComputing(const int i){
  //  this->m_NeedsComputing =i; // we dont need to recur
    if(i)
        this->Esail->SetFlag(FL_NEEDS_GAUSSING );
    this->RXObject::SetNeedsComputing(i);
}
int RXEntity::Recursive_Solve(const int depth, const class RXSail *cSail) { //returns 1 if the entity moved, else 0
    if(cSail != this->Esail)
        return (0);
    if(!this->NeedsComputing() )
        return(0);
    return RXObject::Recursive_Solve(depth,cSail);
}

RXEntity::~RXEntity(void)
{
    this->CClear();
    if(this->name()&&type())
        Esail->MapRemoveObject(this) ;

    if(this->type()) RXFREE(m_type);   m_type=0;
    if(name()) RXFREE(m_name);	m_name=0;
    AttributeDestroy();
    if(line) RXFREE(line);	line=0;

    // if it's in the garbage list, pop it
    std::set<RXEntity*>::iterator it;
    while((it = this->Esail ->m_garbage.find(this))!=this->Esail ->m_garbage.end())
        this->Esail->m_garbage.erase(it);

}
int RXEntity::Kill() {
    dbgc++;
    {
        if(RXODBG){
            RXTAB(dbgc);cout<<"RXEntity:Kill  "<<this<<" ";
            if(this->name())cout<<":"<<this->name();
            if(this->type())cout<<","<<this->type();
            cout<<"(eNd)"<< endl;
        }
    }

    if(this->IsBeingKilled()){
        if(RXODBG){cout<<"Skip kill of "<<this->name()<<","<<this->type()<<endl;}
        return 0;
    }
    int rc =this->RXObject::Kill();

    this->Esail->m_garbage.insert(this);
    if(name()&&type()  ) {
        this->Esail->MapRemoveObject(this) ;
        RXFREE(m_name); m_name=0;
        RXFREE(m_type); m_type=0;
        TYPE=PCE_DELETED;
    }
    //	else
    //		wcout <<L" I hope this is a pside's internal site: "<< this->GetOName() <<endl;


    if(RXODBG){RXTAB(dbgc);cout<<"end RXEntity:Kill "<<this<<" "<<endl;}
    dbgc--;
    return rc;
}



int RXEntity::CClear(){		// is supposed to strip an entity back to its line
    int rc=0;

    SAIL *const sail = this->Esail; // insulation in case P gets killed while we still need sail

    if(sail) {
        sail->ZeroSpecialLists(this); //  CAREFUL we use the select list for the PC_File
    }

    this->Esail->SetFlag(FL_NEEDS_GAUSSING );		// June 2003

    AttributeDestroy();

    sail->Some_Deleted++;

    //	if(this->m_pONMO ) this->m_pONMO->m_bDeleteObject=true;
    this->m_pONMO=NULL;
    this->Needs_Resolving=1;
    this->Needs_Finishing=0;
    this->SetNeedsComputing(0);

    if (this->hoopskey) {
#ifndef HOOPS
        char status[256];
        HC_Show_Key_Status(this->hoopskey,status);
        if(strieq(status,"valid")) {
            if(this->TYPE==PCE_CAMERA ) { /* so we can swoop using editword */

            }
#endif
            if(!g_Janet) HC_Delete_By_Key(this->hoopskey);
#ifndef HOOPS
        }
#endif
        this->hoopskey=0;
    }


    // call base class CClear();
    rc+=RXObject::CClear();
    rc+=RXENode::CClear();

    return rc;

}
// newAtt is expected to be a string of type att,att,(att=val),...

bool RXEntity::AttributeAdd(const QString &newAtt){
    this->m_rxa.Splice(newAtt,0);
    return true;
}


bool RXEntity::AttributeRemove(const QString &AttToKill)
{
    bool rc=false; ;
    if(rc=  this->m_rxa.Remove(AttToKill ))
    {
        this->edited++;
    }
    return rc;
}
int RXEntity::AttributeDestroy( )
{
    int rc=0;
    this->m_rxa.RemoveAll();
    return rc;
}

bool RXEntity::AttributeGet(const RXSTRING what,RXSTRING &value) const{
    // We might consider searching in the parent but that is hard to manage.
    return  this->m_rxa.Extract_Word(what,value);

}

bool RXEntity::AttributeGet(const QString what, QString &value) const
{
    return  this->m_rxa.Extract_Word(what,value);
}
bool RXEntity::AttributeGetDble(const QString & what, double*value) const
{
    *value=0;
    return  this->m_rxa.Extract_Double(qPrintable (what),value);
}
bool RXEntity::AttributeGetInt(const QString & what, int*value) const
{
    *value=0;
    return  this->m_rxa.Extract_Integer(qPrintable (what),value);
}


bool RXEntity::AttributeGet(const char* what, char*pvalue) const{
    // We might consider searching in the parent but that is hard to manage.
    int rc;
    QString value;
    rc= this->m_rxa.Extract_Word(what,value);

    if(pvalue&&rc){
        strcpy(pvalue, qPrintable(value));
    }

    return rc;
}
int RXEntity::AttributeSet(const QString &what, const QString &val)
{
    int rc=0;
    if( rc=  this->m_rxa.ChangeValue  ( what,val) )
    {
        this->edited++;}
    return rc;
}

int RXEntity::AttributeSet(const char*what, const char*val){
    int rc=0;
    if( rc=  this->m_rxa.ChangeValue  ( what,val) )
    {
        this->edited++;}
    return rc;
}
int RXEntity::AttributeSet(const RXSTRING what, const RXSTRING val){
    int rc=0;
    if (rc=  this->m_rxa.ChangeValue  (what,val ))
    {
        this->edited++;}
    return rc;
}
bool RXEntity::AttributeFind(const QString &key )const
{
    return  this->m_rxa.Find(key);
}
QString RXEntity::AttributesPrettyPrint() const
{
    return  this->m_rxa.PrettyPrint();
}
QString RXEntity::AttributeString(const char* prefix ) const
{
    QString q = this->m_rxa.ToQString();
    if(prefix)
        return q.prepend(prefix);
    return q;
}

// we'll need a reference which might be global X, 
// unless this  coincides with the entity normal in which case we'd take global Y 

ON_Xform RXEntity::Trigraph(void)const
{
    ON_3dVector l_Eltnormal;
    ON_Xform l_xf;
    ON_3dVector l_xg,l_x,l_y;

    l_Eltnormal = this->Normal();
    l_Eltnormal.Unitize ();
    if(l_Eltnormal.LengthSquared()<1e-10){
        l_Eltnormal=this->Esail->Normal();
        cout<<" short normal on entity "<<this->name()<<endl;
    }
    if(l_Eltnormal.IsParallelTo(ON_3dVector(1,0,0)))
        l_xg = ON_3dVector(0,1,0);
    else
        l_xg = ON_3dVector(1,0,0);


    l_y = ON_CrossProduct(l_Eltnormal,l_xg);
    l_y.Unitize();

    l_x = ON_CrossProduct(l_y,l_Eltnormal);
    l_x.Unitize();

    l_xf = ON_Xform(ON_3dPoint(0,0,0),l_x,l_y,l_Eltnormal);
    l_xf.Transpose();
    return l_xf;

}


int RXEntity::Print_One_Entity( FILE *fp)const  {
    int k;
    const char *lp;

    fprintf(fp,"\n\nENTITY ");
    fprintf(fp,"    type()       '%s'",type());
    fprintf(fp,"    NAME          '%s'\n\n",name());

    fprintf(fp,"NC=%d \t",NeedsComputing());
    fprintf(fp,"NF=%d \t",Needs_Finishing);
    fprintf(fp,"NR=%d \t",Needs_Resolving);
    fprintf(fp,"cc=%d \t",m_ComputeCount );
    fprintf(fp,"IS=%d \t",IsInStack() );
    fprintf(fp,"ED=%d \t",edited);
    fprintf(fp,"GE=%d \t",generated);
    fprintf(fp,"TY=%d \t",TYPE);

    fprintf(fp,"GN=%p \t",this->GNode(rxexpLocal));
    fprintf(fp,"ONO=%p\n",m_pONMO);

    fprintf(fp,"    attributes\n%s\n",qPrintable(AttributesPrettyPrint()));
    if(GetLine()){
        fprintf(fp,"    Original data    \n<");
        lp = GetLine();
        for(k=0; *lp; lp++, k++) {
            if(*lp=='\n' ) k=0;
            fprintf(fp,"%c",*lp);
            if(k>64&& ((*lp=='\t' ) || (*lp==':')))  {
                fprintf(fp,"\n ");  k=0;
            }
        }
        fprintf(fp,">\n");
    } // if line

    ON_3dVector l_n = this->Normal();
    fprintf(fp,"normal (\t%f \t%f \t%f\t)\n",l_n.x,l_n.y,l_n.z);
    this->PrintOMap(fp);
    if(this->GetMapSize()) {
        fprintf(fp," its expression list\n");
        RXENode::List(fp," ",0,0);
    }
    if(!this->Needs_Resolving)
        this->Dump(fp);

    // the tail
#ifdef HOOPS
    if (hoopskey) {
        char ht[256],buf[2048],seg[2048];
        HC_Show_Key_Type(hoopskey,ht);
        fprintf(fp,"    HOOPS KEY  %s\n",ht);
        if(strieq(ht,"segment")) {
            HC_Show_Segment(hoopskey,seg);
            fprintf(fp,"    segment  %s\n",seg);
            if(HC_QShow_Existence(seg,"user options"))	{
                HC_Define_System_Options("C string length=2048");
                HC_QShow_User_Options(seg,buf);
                fprintf(fp,"    UOs <%s>\n",buf);
                HC_Define_System_Options("C string length=255");
            }
            HC_Open_Segment_By_Key(hoopskey);
            RXEntity_p ip=0;
            HC_Show_One_Net_User_Index(RXCLASS_ENTITY, &ip);

            if(! (!ip ||ip==this  ) ) {
                fprintf(fp,"DANGER User Index Mismatch Net UI =%p , sb %p",ip,this);
                printf("DANGER User Index Mismatch Net UI =%p , sb %p",ip,this);

                HC_Show_One_User_Index(RXCLASS_ENTITY, &ip);
                fprintf(fp,"\tlocal  UI =%p\n",ip);
                printf("\tlocal  UI =%p\t type()=%s\n",ip,type());
            }
            RXEntity_p l_p = (RXEntity_p  )ip;
            if(l_p)
                fprintf(fp,"from LOCAL UI \t%s \t %s\n",l_p->type(),l_p->name());
            HC_Close_Segment();
        }
    }
    else 	fprintf(fp,"    HOOPS KEY  zero\n");
#endif
    
    fprintf(fp,"that was %s  %s\n",type(),name());
    fflush(fp);
    return 1;

}

int RXEntity::Rename(const char*namein,const char*typein){ // should change the hoops segment too

    char *l_name;

    if(!(str_islower(typein)) ){

        printf(" UPC type %s\n", typein); }

    if(!(str_islower(this->type())) ){

        printf(" UPC type %s\n", this->type()); }
    if(this->name() && strieq(this->name(),namein)&& this->type() && strieq(this->type(),typein))
        return 0;

    l_name=STRDUP(namein); /* slow but insulating */

    do{
        if(this->Esail->GetKeyWithAlias(typein,l_name)) {
            l_name = (char*)REALLOC(l_name,strlen(l_name)+4);
            strcat(l_name,"A");
        }
        else break;
    }while(1);

    this->TYPE = INTEGER_TYPE(typein);

    this->Esail->MapRemoveObject(this);

    RXFREE(this->m_name);
    this->m_name=l_name;
    RXFREE(this->m_type);
    this->m_type=STRDUP(typein);
    this->SetOName (TOSTRING(l_name));

    this->Esail->MapInsertObject(this);

    return 1;
}// rename entity
int RXEntity::SetTypeName(const char*p_type, const char*p_name) {
    int rc=0;
    if(m_name) RXFREE(m_name);
    if(m_type) RXFREE(m_type);
    m_name=STRDUP(p_name);
    m_type=STRDUP(p_type);
    return rc;
}
int RXEntity::PC_Finish_Entity(const char *typein,
                               const char *namein,
                               const RXAttributes &pRxa,
                               const char *linein)
{
    /* Similar  to Insert Entity, this one fills out the entity p instead of generating another */

    char *name;
    char *type;
    char *lline=NULL;

    name =STRDUP(namein);
    type =STRDUP(typein);
    if(linein)  lline= STRDUP(linein);

    PC_Strip_Trailing(type); /* a bug because type is often  a constant */
    PC_Strip_Trailing(name);
    Make_Valid_Segname(name);

    //cout<< "Not sure we want to clear In_Stack in PC_Fin_Ent"<<endl;
    SetInStack (0);	     /* A dbg item. The number of times RecursiveSolve has operated on it*/

    Rename(name,type);
    if(this->line)
        RXFREE(this->line);
    this->line = lline;

    AttributeDestroy( );
    this->m_rxa=pRxa;
    QString qs; QStringList arg;
    if(this->m_rxa.HeuristicCheck(qs))
    {
        QString pp; int kkk;
        pp= QString("Are these attributes OK? \n") ;
        pp+=QString ("model : ")+this->Esail->GetQType() + "\nfile  : ";
        pp+=this->Esail->ScriptFile() +QString("\nObject: ");
        pp+= QString(this->type())+QString(",") +this->GetQName() +QString("\n");

        qs.prepend(pp);
        kkk=  this->OutputToClient(qs,3)  ;
            switch (kkk) {
          case QMessageBox::Yes:
              // Save was clicked
              break;
          case QMessageBox::No:
              // Don't Save was clicked
                arg <<"editscript";
                arg<<this->Esail->GetQType();
                 Do_Edit_Script(arg, 0);
               break;
          case QMessageBox::Cancel:
              // Cancel was clicked
              break;
          default:
              // should never be reached
              break;
        }
    }

    hoopskey = 0;
    if(!g_Janet){
        HC_Open_Segment_By_Key(Esail->GetGraphic()->m_ModelSeg);
        HC_Open_Segment("data");
        HC_Open_Segment(this->type());
        hoopskey=HC_KOpen_Segment(this->name());// the first time for most entities
        HC_Set_User_Index(RXCLASS_ENTITY, this);
        HC_Close_Segment();
        HC_Close_Segment();
        HC_Close_Segment();
        HC_Close_Segment();
    }
    else
        hoopskey=Esail->GetGraphic()->m_ModelSeg;

    TYPE = INTEGER_TYPE(type);
    if (name) RXFREE(name);
    if (type) RXFREE(type);
    return(1);
}
int RXEntity::PC_Finish_Entity(const char *typein,
                               const char *namein,void *dp,HC_KEY hk,
                               struct LINKLIST *plist,const char *attin,
                               const char *linein)
{

    /* Similar  to Insert Entity, this one fills out the entity p instead of generating another */

    char *name;
    char *type;
    char *lline=NULL;
    SAIL *sail = this->Esail;
    Graphic *g = sail->GetGraphic() ;


    name =STRDUP(namein);
    type =STRDUP(typein);
    if(linein)  lline= STRDUP(linein);



    PC_Strip_Trailing(type); /* a bug because type is often  a constant */
    PC_Strip_Trailing(name);
    Make_Valid_Segname(name);

    //cout<< "Not sure we want to clear In_Stack in PC_Fin_Ent"<<endl;
    SetInStack (0);


    Rename(name,type);
    if(this->line)
        RXFREE(this->line);
    this->line = lline;

    AttributeDestroy( );
    AttributeAdd(attin );
    QString qs; QStringList arg;
    if(this->m_rxa.HeuristicCheck(qs))
    {
        QString pp; int kkk;
        pp= QString("Are these attributes OK? \n") ;
        pp+=QString ("model : ")+this->Esail->GetQType() + "\nfile  : ";
        pp+=this->Esail->ScriptFile() +QString("\nObject: ");
        pp+= QString(this->type())+QString(",") +this->GetQName() +QString("\n");

        qs.prepend(pp);
        kkk=  this->OutputToClient(qs,3)  ;
            switch (kkk) {
          case QMessageBox::Yes:
              // Save was clicked
              break;
          case QMessageBox::No:
              // Don't Save was clicked
                arg <<"editscript";
                arg<<this->Esail->GetQType();
                 Do_Edit_Script(arg, 0);
               break;
          case QMessageBox::Cancel:
              // Cancel was clicked
              break;
          default:
              // should never be reached
              break;
        }
    }

    assert(!plist);

    hoopskey = hk;
    if(hk) {
        HC_Open_Segment_By_Key(hk);
        HC_Set_User_Index(RXCLASS_ENTITY, this);
        HC_Close_Segment();
    }
    else if(!g_Janet){
        HC_Open_Segment_By_Key(g->m_ModelSeg);
        HC_Open_Segment("data");
        HC_Open_Segment(this->type());
        hoopskey=HC_KOpen_Segment(this->name());// the first time for most entities
        HC_Set_User_Index(RXCLASS_ENTITY, this);
        HC_Close_Segment();
        HC_Close_Segment();
        HC_Close_Segment();
        HC_Close_Segment();
    }
    else
        hoopskey=g->m_ModelSeg;

    TYPE = INTEGER_TYPE(type);
    if (name) RXFREE(name);
    if (type) RXFREE(type);
    return(1);
}
