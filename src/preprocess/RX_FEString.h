#ifndef _RXFESTRING_HDR_B6889B9D8DA1486382737A4A5847B3E4
#define _RXFESTRING_HDR_B6889B9D8DA1486382737A4A5847B3E4
#include "RX_FELinearObject.h"
#include "RXEntity.h"

#define PCS_LENGTHFROMFILE	(1)	// must match stringlist.f 
#define PCS_NOTRIM		(2)	// must match stringlist.f 
#define RXS_NEVERBUCKLE (-2)
#define RXS_ALWAYSBUCKLE  (1)
#define RXS_BUCKLEDEPENDS  (0) // MUST MATCH ftypes.f90

struct StringContents { // must match  TYPE, bind(c) :: mystringdata. See ftypes.f90
    double  m_zi_LessTrim ;// initial L from edges or card. Without Trim
    double  EAS;			// EA
    double  TIS ,m_trim, m_Mass	,m_zt_s	;
    int     m_TConst,m_Slide;
    int	    m_CanBuckle3;	// never, always or flag-controlled
    void   *m_Scptr; // ! RX_FEString or RX_FEBeam
    void   *m_ExpCptr;  // not used
    int     Ne ;
    double  m_tqOut;
}; 
// the m_xxx3  flags are 3-valued: RXS_NEVER, RXS_ALWAYS or RXS_FLAG

class RX_FEString :
        public RXEntity,
        public RX_FELinearObject
{
public:
    RX_FEString(SAIL *s);
    RX_FEString(void);
    virtual ~RX_FEString(void);
    virtual int CClear();
    int Compute(void);
    int Finish(void);

    virtual int Dump(FILE *fp) const;
    int Resolve(void) {
        return Resolve(0);
    }
    int Resolve(RXEntity_p sce=0);
    int ReWriteLine(void);
    HC_KEY DrawDeflected(HC_KEY p_k,const double t1=0, const double t2=0,const bool Skip_Zero_Tension=true);
    int SetNeedsMeshUpdate();
    int PostToMirror();
protected:
    int Set_EA_Etc_On_StringData(char*name);



    struct LINKLIST *m_scList; // list of edges - may be generated or not
    const ON_String Name()const {return ON_String( this->name());}
 public:
    struct StringContents d;
    int m_which;	// 0 means length-controlled (t = ext*eA + ti), 1 means tension-controlled, but not implemented.
    int *m_FPse; // pointer to the fortran array holding SE
    int *m_FPrev; // pointer to the fortran array
    double *m_FPtq; // pointer to the fortran array holding tq

protected:
    int flags;

    char  *m_Text;				// calloc'd to 256
    double  m_L_FromEdges;		// measured from the edges
    //		RXExpression *m_pzi, *m_ptrim, *m_pea; // before we use these we need a way of getting them to update d.ea, etc.
public:
    bool	m_Needs_Meshing;
    /// generate the psides
    int Mesh(void);
    // put the string into the FEA database
    int AddFEA(const int n_edges);
    // removes the string from the FEA database
    virtual int ClearFEA(void);
    // collects the pside list and the attributes into the P structure
    int PostMesh(void);
    double* GetTrimPtr(){ return &(d.m_trim) ;}
    double* GetPrestressPtr(){ return &(d.TIS) ;}
    void SetTrim(const double x);
    int EdgeSwap();
    int Freeze(const int flags);

protected:
    int CountPsideEdges(double*p_length);
private:
    void Init(SAIL * p_sail);
protected:
    double MeasureLengthFromEdges(void);
};
#endif
