#include "StdAfx.h"
#include <QDebug>
#include <iostream>
#include <fstream>

#ifdef _CONSOLE  // for sparsetest framework
#define g_diagonalfac 0.0001
#define RXFOPEN(a,b) fopen(a,b)
#define FCLOSE(a ) fclose(a)
#else
#include "RelaxWorld.h"

#include "global_declarations.h"
#include "cfromf.h"
#include "offset.h"
#endif 
#include "f90_to_c.h"
using namespace std;
#include "mkl_lapack.h"
#include "mkl_spblas.h"
#include "RXSparseMatrix1.h"
#define RXSMALL	1.0e-15
#define RXLARGE	1.0e15

#define SPTIMING (false)
// if  (SPTIMING) 

/* IMPORTANT. RESTRICTIONS
 this variant RXSparseMatrix1  is for matrices where all the representations are counted from 1 not from zero.
*/
/*

A RX Sparse Matrix may be stored either in map form or CSC form
 we provide method SetType which copies it from one format to the other.
 (but temporarily uses double the memory)

It is normally easier to create and operate on the matrix in map form
 a MM file is naturally mapstyle and a harwell-boeing file is naturally CSC.


Use  the C style interface for calling from fortran
extern "C" class RXSparseMatrix1 *NewSparseMatrix(){ return new RXSparseMatrix1();}

extern "C" void AddToSparseMatrix(RXSparseMatrix1 *m,const int i,const int j, const double v);

Once you've built it, you  can Write it or Solve it. 

We should also code up MKL matrix multiplication, addition and preconditioning.
And submatrix extraction and insertion

and finally if we want to destroy it from C or fortran
extern "C" void DestroySparseMatrix( class RXSparseMatrix1 *m) {delete m;}
*/
/* managing the matrix type

We can ask User to choose the matrix type( sym, posDef, etc) at creation time.
Then we can use that to set the file write and solver parameters.
Specifically
Is its symmetric we only store the upper diagonal. It's an error to set an element
with row>col.
If its symmetric 
*/
/*
 matrixType is defined in the HBIO standard
the second letter of line 3 first word (eg RUA) 

MatrixMarket format, in the 5th word of the header
%%MatrixMarket matrix coordinate real general
%%MatrixMarket matrix coordinate real general
MM is Lower diagonal only for symmetric types
In addition, Pardiso accepts: (forgetting complex
 RSS=1,RSPD=2,RSI=-2,RU=11,MTNONE=0
Symmetric
  real
   Indef or posdef
  hermitian
   indef or posdef

REAL TYPES
          HBIO		    MM	              pardiso
 S	     symmetric	symmetric,	  1,2,-2	"1 real and structurally symmetric
               2  real sym pos def
           -2  real and symmetric indefinite "
 U 	   Unsymmetric	  general,	       11	    real and unsymmetric matrix
 Z	skew symmetric	skew-symmetric,	  11
 R	rectangular	       general,	      N/A

COMPLEX types				
                                     3	complex and structurally symmetric
 H	 herrmitian	 hermitian	                  4	complex and Hermitian positive definite
                                    -4	complex and Hermitian indefinite matrix
                                     6	complex and symmetric matrix
                                     13	complex and unsymmetric matrix

So we can use (SUZRH) with 
an additonal flag to say if a symmetric is
just symmetric
Positive definite
indefinite

*/
double rxmax(double*v ,int n)
{
    if(n<1) return 0;
    int i;
    double*p, rv = *v;
    for(p=v, i=0;i<n;i++,p++)
        rv=max(rv,*p);
    return rv;
}

double rxsum(double*v ,int n)
{
    if(n<1) return 0;
    int i;
    double*p, rv =0.0;
    for(p=v, i=0;i<n;i++,p++)
        rv+= *p;
    return rv;
}
double  rxmean(double*v ,int n)
{
    if(n<1) return 0;
    return rxsum(v,n)/((double)n);
}
double  rxmeanabs(double*v ,int n)
{
    if(n<1) return 0;
    int i;
    double*p, rv =0.0;
    for(p=v, i=0;i<n;i++,p++)
        rv+= fabs(*p);
    return rv/((double)n);
}
int RXSparseMatrix1::SetMatrixType(const int t) {// call with (0) and it gives the m_flags
    int rc = m_flags;
    m_hbio.mxtype ="R A";
    if(t&RXM_SYMMETRIC)             m_hbio.mxtype[1]='S';
    else	if(t&RXM_RECTANGLE)		m_hbio.mxtype[1]='R';
    else	if(t&RXM_SKEW_SYMM)		m_hbio.mxtype[1]='Z';
    else	if(t&RXM_HERMITIAN)		m_hbio.mxtype[1]='H';
    else 							m_hbio.mxtype[1]='U' ;

#ifdef DEBUG
    if(m_flags && m_flags !=t)
        cout<< "MODIFYING flags in Sparse Matrix "<<rc<<" to " << t<<endl;
#endif
    m_flags= t;
    return rc;
}
int RXSparseMatrix1::GetPardisoFlag()const{
    // dont allow case for structurall sym but not tru sym.

    if(!(m_flags&RXM_COMPLEX)){
        if( m_flags&(RXM_POS_DEF+RXM_SYMMETRIC )) return 2;
        if( m_flags&(RXM_INDEFINITE+RXM_SYMMETRIC) ) return -2;
        if( m_flags& RXM_SYMMETRIC) return 1;
        if( m_flags& RXM_STRUCTSYM) return 1;
        else
            return 11;
    }
    // here we are complex
    if( m_flags& (RXM_HERMITIAN+RXM_POS_DEF) )		return 4;
    if( m_flags& (RXM_HERMITIAN+RXM_INDEFINITE ))	return -4;
    if( m_flags&  RXM_SYMMETRIC)					return 6;
    return 13;

}
const char*RXSparseMatrix1::GetMMartString()const{
    if( m_hbio.mxtype[1]=='S') return "symmetric      ";
    if( m_hbio.mxtype[1]=='R')	return "general       ";
    if( m_hbio.mxtype[1]=='Z')	return "skew-symmetric";
    if( m_hbio.mxtype[1]=='H')	return "hermitian     ";
    if( m_hbio.mxtype[1]=='U' )	return "general       ";
    return "(unknowntype) ";
}
RXSparseMatrix1::RXSparseMatrix1()
    : m_nRhsVecs(0)
    ,m_StorageType(CSC)
    ,m_flags(0)
    ,m_b(1)
    ,m_dsshandle (0)
    , m_dssopt  ( MKL_DSS_DEFAULTS)
    , m_dsssym   (MKL_DSS_NON_SYMMETRIC)
    , m_dsstype  (MKL_DSS_INDEFINITE)

{
    SetMatrixType(RXM_RECTANGLE);
}

RXSparseMatrix1::RXSparseMatrix1(const int t)
    : m_nRhsVecs(0)
    ,m_StorageType(MAPSTYLE)
    ,m_flags(t)
    ,m_b(1)
    ,m_dsshandle (0)
    , m_dssopt  ( MKL_DSS_DEFAULTS)
    , m_dsssym   (MKL_DSS_NON_SYMMETRIC)
    , m_dsstype  (MKL_DSS_INDEFINITE)

{
    SetMatrixType(t);
}

RXSparseMatrix1::RXSparseMatrix1(const map<pair<int,int>,double> m,const int t)
    :  m_nRhsVecs(0)
    ,m_StorageType(MAPSTYLE)
    ,m_flags(t)
    ,m_b(1)
    ,m_dsshandle (0)
    , m_dssopt  ( MKL_DSS_DEFAULTS)
    , m_dsssym   (MKL_DSS_NON_SYMMETRIC)
    , m_dsstype  (MKL_DSS_INDEFINITE)
{
    Init(MAPSTYLE );

    int r,c; // do it one at a time so we can tot up nr and nc
    map<pair<int,int>,double>::const_iterator it;
    for(it=m.begin() ;it!=m.end();++it) {
        r = it->first.first; m_hbio.nrow1=max(Nr(),r);
        c = it->first.second;m_hbio.ncol1=max(c,Nc());
        this->m_map[pair<int,int>(r,c) ]=it->second;
    }
}


RXSparseMatrix1::~RXSparseMatrix1(void)
{
    Init(MAPSTYLE);

}

int RXSparseMatrix1::Init(const enum smtype  t)
{
    if (m_dsshandle )
        dss_end() ;
    m_dsshandle=0;
    this->m_hbio.Init();
    this->m_map.clear();
    rHs.clear();
    this->m_hbio.ncol1=0;
    this->m_hbio.nrow1=0;
    this->m_StorageType=t;

    return 0;
}
int RXSparseMatrix1::DeepCopy(const RXSparseMatrix1& src){

    this->m_b= src.m_b;
    this->SetMatrixType( src.m_flags);
    this->SetStorageType( src.m_StorageType );
    this->m_hbio.DeepCopy(src.m_hbio);
    this->m_map.clear(); m_map.insert( src.m_map.begin(),src.m_map.end());
    this->m_nRhsVecs= src.m_nRhsVecs;

    this->rHs.clear(); this->rHs.insert (this->rHs.begin(),src.rHs.begin(),src.rHs.end());
    return 1;
}
RXSparseMatrix1&  RXSparseMatrix1::operator=(const RXSparseMatrix1& src){

    this->m_b= src.m_b;
    this->SetStorageType( src.m_StorageType );
    this->SetMatrixType (src.m_flags);
    this->m_hbio = src.m_hbio;
    this->m_map.clear(); m_map.insert( src.m_map.begin(),src.m_map.end());
    this->m_nRhsVecs= src.m_nRhsVecs;

    this->rHs.clear(); this->rHs.insert (this->rHs.begin(),src.rHs.begin(),src.rHs.end());
    return *this;
}
// Tranpose followed by a nr<=>nc swap works to switch between CSR and CSC

int RXSparseMatrix1::Multiply(RXSparseMatrix1 &yy, RXSparseMatrix1 &rv)
{
    if  (SPTIMING) cout<<"op..";
    RXSparseMatrix1* b = this;
    RXSparseMatrix1* a = &yy;
    if(a->m_StorageType !=CSC) return -1;
    if(b->m_StorageType !=CSC) return -1;

    rv.SetStorageType(CSC);
    rv.SetMatrixType( RXM_RECTANGLE); // safest max(a->m_flags,y.m_flags) );

    if(this->Nc() !=  yy.m_hbio.nrow1)
    {
        cout<<"Matrix multiply: non conform"<<endl;
        cout<<"("<<this->Nr()<< ","<<this->Nc();
        cout<<") times  ( "<<yy.Nr() << ","<<yy.Nc()<<")"<< endl;
        return -2;
    }
    int request = 1; // measure memory.
    int sort=0;	// asserting that the matrices are already sorted
    int nzmax=0; int info=0;
    int mm= a->Nc();
    int n = a->Nr();
    int kk = b->Nr();
    double *cvals=0; int *jc=0;
    int *ic =new int[ max(mm,n) +1];
    memset(ic,0,sizeof(int)*( max(mm,n) +1));
    if  (SPTIMING) cout<< "mkl1..";
    mkl_dcsrmultcsr("N", &request, &sort, &mm, &n, &kk,
                    a->m_hbio.values, a->m_hbio.rowind, a->m_hbio.colptr,
                    b->m_hbio.values, b->m_hbio.rowind,b->m_hbio.colptr,
                    cvals, jc, ic, &nzmax, &info);
    int temp= ic[mm]-1;

    cvals=new double[temp];
    jc=new int[temp];
    request=0; nzmax=temp;
    if  (SPTIMING) cout<< "mkl2..";
    mkl_dcsrmultcsr("N", &request, &sort, &mm, &n, &kk,
                    a->m_hbio.values, a->m_hbio.rowind, a->m_hbio.colptr,
                    b->m_hbio.values, b->m_hbio.rowind, b->m_hbio.colptr,
                    cvals, jc, ic, &nzmax, &info);
    if  (SPTIMING) cout<< "FillIn..";
    // now fill in rv
    rv.m_hbio.values=cvals;
    rv.m_hbio.colptr=ic;
    rv.m_hbio.rowind=jc;
    rv.m_hbio.nrow1= b->Nr();
    rv.m_hbio.ncol1= a->Nc();
    rv.m_hbio.nnzero=temp;
    if  (SPTIMING) cout<< "Done..";
    return info;
}//Multiply

RXSparseMatrix1  RXSparseMatrix1::operator*( RXSparseMatrix1& y){
    RXSparseMatrix1 rv(0 ); rv.SetStorageType(CSC);
    assert("Need deep copy"==0);
    this->Multiply(y, rv);
    return rv;
}

int RXSparseMatrix1:: BinaryAdd(const RXSparseMatrix1& ma,const RXSparseMatrix1& mb )
{
int rc=0;
    //    if(ma.m_flags !=mb.m_flags )
    //    {
    //        qDebug()<< "matadd - inconsistent flags - does it matter??";
    //    }
 
    SetMatrixType( this->m_flags|RXM_RECTANGLE );

    if(ma.m_hbio.nrow1 != mb.m_hbio.nrow1) { qDebug()<< "matadd - inconsistent NR"; return 0;}
    if(ma.Nc() != mb.Nc()) { qDebug()<< "matadd - inconsistent Nc"; return 0;}
    if(ma.m_StorageType ==MAPSTYLE) {
        if(mb.m_StorageType !=MAPSTYLE) { qDebug()<< "matadd - Y not mapstyle"; return 0;}
        int r,c;
        SetStorageType( MAPSTYLE );
        if(ma.m_map.size() != mb.m_map.size()){ qDebug()<< "matadd - inconsistent mapsize"; return 0;}
        m_map.clear();
        map<pair<int,int>,double>::const_iterator it1,it2;
        for(it1=ma.m_map.begin(),it2=mb.m_map.begin();
            it1!=ma.m_map.end()&&it2!=mb.m_map.end();
            ++it1,++it2)
        {
            r= it1->first .first ; c=it1->first.second;
            if(r!= it2->first.first|| c!=it2->first.second ) {
                m_map.clear(); return 0;
            }
            m_map[it1->first]=it1->second+it2->second;
        }
        return 1;
    }
    else if(ma.m_StorageType==CSC)
    {
        if(mb.m_StorageType!=CSC)  {
            qDebug()<< "matadd - Y not CSC"; return 0;
        }
        SetStorageType(CSC);
        const RXHarwellBoeingIO1 &ha = ma.m_hbio;
        const RXHarwellBoeingIO1 &hb = mb.m_hbio;
        RXHarwellBoeingIO1 &hc = m_hbio;
        int nrow = ma.Nr(); // ICPC 13 doesnt like const
        int ncol = ma.Nc();
        const int job = 1 ; // treat values as well as structure
        int nzmax;
        nzmax = ha.nnzero+hb.nnzero+2*nrow+2*ncol+2;
        assert(!hc.colptr );
        hc.colptr= new int[ ncol+1 ];
        hc.rowind= new int[nzmax];
        hc.values = new double[nzmax];
        for(int j=0; j<nzmax;j++) { hc.rowind[j]=-1; hc.values[j]=0;}
        hc.nrow1 = ha.nrow1;
        hc.ncol1 = ha.ncol1;

        int  ierr=0;  // this call   fails
//        cf_aplb1(nrow, ncol,job,
//                 ha.nnzero, ha.values, ha.rowind, ha.colptr,
//                 hb.nnzero, hb.values, hb.rowind, hb.colptr,
//                 hc.values, hc.rowind, hc.colptr,
//                 nzmax, &ierr);

 // see https://software.intel.com/en-us/node/468638#BE0F701A-72F5-4435-A63C-50D522D0784A
        double beta=1.0;
        int request=0, sort=3;
        mkl_dcsradd ("N",&request, &sort, &nrow, &ncol, ha.values, ha.rowind, ha.colptr,
                    &beta, hb.values, hb.rowind, hb.colptr,
                    hc.values, hc.rowind, hc.colptr, &nzmax,&ierr);


        if(ierr)
            qDebug()<< "mkl_dcsradd err="<<ierr;
        else
        {
            for(int j=0; j<nzmax;j++) {
                if(hc.rowind[j]==-1){nzmax=j; break;}
            }
            hc.nnzero= nzmax;
            rc=1;
        }

    }
    return 1;
}

const RXSparseMatrix1  RXSparseMatrix1::operator*=(const double& f){
    SetStorageType(MAPSTYLE);
    map<pair<int,int>,double> ::iterator it;
    for(it=this->m_map.begin();it!=this->m_map.end();++it)
    {
        it->second*=f;
    }
    return *this;
}
const RXSparseMatrix1 RXSparseMatrix1::operator*(const double& f){
    RXSparseMatrix1 rv(this->m_flags );
    if(this->m_StorageType==MAPSTYLE)
    {
        rv.SetStorageType(MAPSTYLE);
        rv= *this; rv.m_map.clear();
        map<pair<int,int>,double> ::iterator it;
        for(it=this->m_map.begin();it!=this->m_map.end();++it)
        {
            rv.m_map[it->first]=it->second*f;
        }
    }
    else if(this->m_StorageType==CSC)
    {
        rv.SetStorageType(CSC);
        rv= *this;
        int i;
        for(i=0;i<this->m_hbio.nnzero;i++)
        {
            rv.m_hbio.values[i]= this->m_hbio.values[i]*f;
        }
    }


    return rv;
}
int RXSparseMatrix1::Transpose(){
    if(this->IsFlaggedSymmetric())
       return 1;
    map<pair<int,int>,double> t;
    map<pair<int,int>,double> ::iterator it;
    switch (this->m_StorageType ) {
    case MAPSTYLE:// easy method: full copy.
        for(it=this->m_map.begin();it!=this->m_map.end();++it)	{
            t[pair<int,int>( it->first.second  ,it->first.first   )]=it->second;
        }
        m_map.clear(); m_map = t;
        Swapcr();
        return 1;
    case CSC:
    {
        if(true)
        {
            this->SetStorageType( MAPSTYLE);
            int rc=this->Transpose ();
            this->SetStorageType( CSC);
            return rc;
        }
        RXHarwellBoeingIO1 &h=this->m_hbio;
        int ierr=0;
        int* iwk = new int[h.nnzero]; // better to use csrcsc
        cf_transp (&( h.nrow1) , &(h.ncol1 ),h.values,h.rowind,h.colptr,iwk,&ierr) ;
        qDebug()<<  "cf_transp returned " <<ierr; assert(!ierr);
        delete[] iwk;
        Swapcr();
        return (!ierr);

        //  return this->m_hbio.Transpose (); // this uses sparsepack and isnt checked out
    }
    default:
        cout<<" unrecognised storage type in RXSparseMatrix1::Transpose"<<endl;
    };
    return 0;

}
int RXSparseMatrix1::Swapcr()
{
    int b =  Nc();
    m_hbio.ncol1=m_hbio.nrow1;
    m_hbio.nrow1= b;
    return 0;
}
int RXSparseMatrix1::ZeroRow(const int i){
    // the comparison takes r first then c
    // so row[i] is between
    int rc=0;
    map<pair<int,int>,double> ::iterator it,l,h;
    l = m_map.lower_bound(pair<int,int>(i-1,Nc()));
    h = m_map.upper_bound(pair<int,int>(i,Nc()));

    for(it=l;it!=h;++it){
        it->second =0.; rc++;}
    return rc;
}
int RXSparseMatrix1::GetColumn(const int j,double *p){

    SetStorageType( MAPSTYLE );

    int  r, rc=0;
    map<pair<int,int>,double> ::iterator it,l,h;
    l = m_map.begin();
    h = m_map.end();

    for(it=l;it!=h;++it){
        if(it->first.second ==j+m_b){
            r = it->first.first; assert(r-m_b>=0);
            p[r-m_b] = it->second; rc++;
        }
    }
    return rc;
}
int RXSparseMatrix1::ZeroColumn(const int j){
    // the comparison takes r first then c so the column isnt contiguous
    // we  just do a trawl
    // one way to speed this up would be if we knew the bandwidth
    SetStorageType( MAPSTYLE );
    int rc=0;
    map<pair<int,int>,double> ::iterator it,l,h;
    l = m_map.begin();
    h = m_map.end();

    for(it=l;it!=h;++it){
        if(it->first.second ==j){
            it->second =0.; rc++;
        }
    }
    return rc;
}

int  RXSparseMatrix1::ExtractDiagonal( map<int,double> &d) const{
    d.clear();
    map<pair<int,int>,double> ::const_iterator it;
    for(it=m_map.begin(); it!=m_map.end();it++)
    {
        if(it->first.second != it->first.first)
            continue;
        d[it->first.first]=it->second ;
    }
    return d.size();
}


/* ATTENTION  this is very crude use of pardiso
 and the solver parameters are those from the example.
*/
int RXSparseMatrix1::SolveOnce(double*result){
    int rc=0;

    SetStorageType(CSC);

    assert(0); rc=this->m_hbio.SolveOnce (this->m_hbio.rhsval,result);
    if(0){
        printf(" %s\n"," i, m_hbio.rhsvAl[i],result[i]");
        for(int i=1;i<=this->m_hbio.nrow1;i++)
            printf(" %d  %f  %f\n", i, m_hbio.rhsval[i],result[i]);
    }

    return rc;
}

int RXSparseMatrix1::SolveIterative(double*result,const double tol){
    int i,j, rc=0;
    //double *resultU has length
    double *resid = new double[Nr()*m_nRhsVecs];
    SetStorageType(CSC);
    assert(0); rc=this->m_hbio.SolveOnce (this->m_hbio.rhsval,result);
    // we want resid = rhs - A.result. We re-solve for resid
    // and we add the new solution to result
    if(m_nRhsVecs!=1)
    { delete [] resid; return rc;}

    char transa='N';

    for(j=0;j<1;j++) {
        mkl_dcsrgemv(&transa, &(this->m_hbio.nrow1),m_hbio.values,m_hbio.colptr,
                     m_hbio.rowind , result, resid); // resid now Ax
        double err = 0.0;
        double*rd, *b;
        for(i=0,b=m_hbio.rhsval ,rd=resid;i<=m_hbio.nrow1;i++,b++,rd++){
            *rd = *b-*rd;
            err+=((*rd) * (*rd));
        }
    }
    delete []resid;
    return rc;
}


enum  smtype RXSparseMatrix1::SetStorageType(const enum  smtype tt)
{
    enum  smtype rc = this->m_StorageType;
    switch (tt){
    case NONE:
        switch (rc){
        case NONE:
        case MAPSTYLE:
        case CSC:
            Init(NONE);
            return rc;
        };//rc
        break;
    case MAPSTYLE: //new
        switch (rc){ //old
        case NONE:
            Init(MAPSTYLE);
            return rc;
        case MAPSTYLE:
            return rc;
        case CSC:
            this->ConvCSCToMap();
            return rc;
        };//rc
        break;
    case CSC:  //new
        switch (rc){ //old
        case NONE:
            Init(CSC);
            return rc;
        case MAPSTYLE:
            this->ConvMapToCSC ();
            return rc;
        case CSC:
            return rc;
        };//rc
        break;
    };///tt

    return rc;
}


int RXSparseMatrix1::SetDimensions(const int nr, const int nc)
{
    int rc=0;
    assert(m_StorageType==MAPSTYLE );
    if(m_StorageType!=MAPSTYLE)
        return 0 ;

    map<pair<int,int>,double>::iterator itend;
    pair<int,int> theEnd (nr,nc);

    itend=m_map.lower_bound(theEnd); itend++;
    if(itend==m_map.end())
        return 0;
    map<pair<int,int>,double>::iterator it ;

    m_map.erase(itend,m_map.end()); rc++;
    this->m_hbio .ncol1=nc;
    this->m_hbio .nrow1=nr;



    return rc;
}
int RXSparseMatrix1::Polish(void) // various finishing of the structural matrices.
// Polish cannot shrink a matrix
{
    int rc=0;
    if(m_b !=1) cout<<"Polish sets Base to 1"<<endl;
    m_b=1;
    int r,c ,nmin=99; // do it one at a time so we can tot up nr and nc
    if(!m_StorageType!=MAPSTYLE)
    {
        // cout<<"cant polish not mapstyle. storage type is"<< m_StorageType<<endl;
        return 0 ;
    }
    map<pair<int,int>,double>::const_iterator it;
    for(it=m_map.begin() ;it!=m_map.end();++it) {
        r = it->first.first; m_hbio.nrow1=max(m_hbio.nrow1,r);
        c = it->first.second; m_hbio.nrow1=max(c,Nc());
        nmin=min(nmin,min(r,c));
        if(nmin<m_b) {
            cout<<" there are some elements below the base "<<nmin<<endl; rc++;}
    }
    if( m_hbio.nnzero != m_map.size()){
        rc++;
        m_hbio.nnzero = m_map.size();
    }
    m_hbio.title="generated by openfsi.org Polish";
    m_hbio.key="openfsi";
    cout<<  "Polish sets nr = " <<m_hbio.nrow1<<" nc= "<<Nc() << " nnzeros="<<m_hbio.nnzero  << endl;
    return rc;
}

pair<double,double> RXSparseMatrix1:: DiagonalStats() {

    double v,  maxval=0,minval=0;
    int start; start=1;
    enum smtype oldmaptype = SetStorageType(MAPSTYLE);
    map<pair<int,int>,double>::iterator it;
    pair<int,int>ij;
    for(int i=1;i<=this->Nc();i++){
        ij=pair<int,int> (i,i);
        it = m_map.find(ij);
        if( it!= m_map.end ())
            v=it->second;
        else
            v=0;
        if(start){
            start=0;
            maxval=v;
            minval=v;
        }
        else
        {
            maxval=max(maxval  ,v);
            minval=min(minval  ,v);
        }
    }

    SetStorageType(oldmaptype);

    return make_pair(maxval,minval);
}
int RXSparseMatrix1::LimitToDiagonal(const double &xmin, const double&xmax)
{
    int rc=0;
    enum smtype oldmaptype = SetStorageType(MAPSTYLE);
    map<pair<int,int>,double>::iterator it;
    pair<int,int>ij;
    for(int i=1;i<=this->Nc();i++){
        ij=pair<int,int> (i,i);
        it = m_map.find(ij);
        if( it!= m_map.end ()){
            if(it->second < xmin)
            { it->second =xmin; rc++;}
            else if(it->second >xmax)
            { it->second =xmax; rc++;}
        }
        else
            m_map[ij]=xmin;
    }
    SetStorageType(oldmaptype);
    return rc;

}
int RXSparseMatrix1::AddToDiagonal(const double &x, const double&threshold){
    int rc=0;
    enum smtype oldmaptype = SetStorageType(MAPSTYLE);
    map<pair<int,int>,double>::iterator it;
    pair<int,int>ij;
    for(int i=1;i<=this->Nc();i++){
        ij=pair<int,int> (i,i);
        it = m_map.find(ij);
        if( it!= m_map.end ()){
            if(it->second < threshold)
            { it->second +=x; rc++;}
        }
        else
            m_map[ij]=x;
    }
    SetStorageType(oldmaptype);
    return rc;
}

int RXSparseMatrix1::ConvMapToCSC(void) {
    int rc=0;
    this->m_hbio.Fill( this->m_map ,"OpenFSI Sparse Matrix - Base One", "CSCFmt" ,this->m_b  );
    this->m_StorageType= CSC ;
    this->m_map.clear();
    this->m_hbio.SetRHS (rHs);
    return rc;
}
int RXSparseMatrix1::ConvCSCToMap(void){
    int rc=0;
    int r,c,j;
    double v;
    pair<int,int> ij;
    this->m_map.clear ();
    for(c=0;c<Nc();c++) {
        for(j=m_hbio.colptr[c]; j<m_hbio.colptr [c+1];j++) {
            r = m_hbio.rowind[j-1];
            ij=pair<int,int> (r,c+1);
            v = m_hbio.values [j-1];
            this->m_map[ij]=v;
        }
    }
    delete [] m_hbio.colptr;  m_hbio.colptr=0;
    delete [] m_hbio.rowind;  m_hbio.rowind=0;
    delete [] m_hbio.values;  m_hbio.values=0;

    this->m_StorageType= MAPSTYLE;
    this->SetRHS(this->m_hbio.nrow1,this->m_hbio.rhsval);
    //	this->m_hbio.Init();
    return rc;
}
double SparseMatrixElement (RXSparseMatrix1 *m,const int r,const int c) {
    double x ;
    if(!m->SlowGetElement (r+1,c+1,x) ) x=0;
    return x;

}
// rin, cin indexed from 1; 
bool RXSparseMatrix1::SlowGetElement(const int r, const int c, double &v) const{
    int j,j1,k;
    pair<int,int> ij;
    map<pair<int,int>,double>::const_iterator it;
    switch( this->m_StorageType){
    case CSC:
        if(r<m_b) return false;
        if(c<m_b) return false;
        if(r>m_hbio.nrow1) return false;
        if(c>Nc()) return false;
        // now follow the  HBIO rules.
        j = m_hbio.colptr[c-1]-1; j1=m_hbio.colptr[c]-1; // colptr is indexed from zero.
        if(j1==j) // this column is empty
            return false;
        // if there is a rowind between j and j1-1 with value r we've found it.
        for(k = j;k<j1;k++) {
            if(m_hbio.rowind[k]==r) {
                v = m_hbio.values[k];
                return true;
            }
        }
        return false;
    case MAPSTYLE:

        ij=pair<int,int> (r,c);
        it = m_map.find(ij);
        if( it!= m_map.end ()){
            v = it->second ;//m_map[ij];
            return true;
        }
        return false;
    default:
        assert(0);
    };//switch
    return false;
}
// addRows(1,1) adds one row before 1
// (2,5) adds 5 rows between rows 1 and 2
// AddRows (nr+1, k) appends k rows.
int RXSparseMatrix1::AddRows(const int where, const int howmany)
{
    //To Add an empty Row at N, we increment NR and we increment all members of RowInd that are >=N
    int rc=0;
    class RXHarwellBoeingIO1 &h = this->m_hbio;
    if(howmany <=0) return -1;
    if(where <1)return -1;
    if(where > this->Nr()+1) return -1;
    int i; int*lp;
    for(i=0,lp=h.rowind;i<h.nnzero;i++,lp++)
    {
        if(*lp >=where)
            (*lp)+=howmany;
    }
    this->m_hbio.nrow1 +=howmany;
    return rc;
}
// AddCols (1,4) inserts 4 columns before column 1. (ie at start
// AddColss (nc+1, k) appends k columns.
int RXSparseMatrix1::AddCols(const int where, const int howmany)  // where counts from 1
{
    //To add an empty column just before N, we increment NC and we duplicate the ColInd entry N
    int rc=0;
    class RXHarwellBoeingIO1 &h = this->m_hbio;
    if(howmany <=0) return -1;
    if(where <1)return -1;
    if(where > this->Nc()+1) return -1;
    SetStorageType(CSC);
    int nc = this->Nc();
    int*buf=new int[nc+1];
    assert(h.colptr ); memcpy(buf,h.colptr,(nc+1)*sizeof(int));
    delete [] h.colptr;
    h.colptr= new int [ nc+howmany+1 ] ; if(!h.colptr) return -1;
    memcpy(h.colptr,buf,(nc+1)*sizeof(int));
    delete[]buf;
    int* src = h.colptr; src+=(where-1);
    int* dest = src; dest+=howmany;
    size_t nn = ( nc-where+2) *sizeof(int);
    memmove(dest,src,nn);
    int*lp;
    for(lp=src; lp!=dest;++lp) *lp=*src;
    this->m_hbio.ncol1+=howmany;

    return rc;
}
int RXSparseMatrix1::IdentityMatrix(const int n)
{
    class RXHarwellBoeingIO1 &h = this->m_hbio;
    if(n <=0) return -1;
    Init(CSC);
    SetStorageType(CSC);
    h.colptr= new int [ n+1];
    h.rowind= new int [n];
    h.values= new double[n];
    for(int i=0;i<n;i++)
    {
        h.colptr[i]=i+1;
        h.rowind[i]=i+1;
        h.values[i]=1.0;
    }
    h.colptr[n] =n+1;
    h.nnzero=h.ncol1=h.nrow1=n;


    return 1;
}

// RSPD is the type of a finite element matrix
class RXSparseMatrix1 *NewSparseMatrix(const int type)
{ 
    //cout<<" New Sparse Matrix with type = "<<type <<endl;
    return new RXSparseMatrix1(type);
}

void DestroySparseMatrix( class RXSparseMatrix1 *m) {delete m;}

void  AddToSparseMatrix(RXSparseMatrix1 *m, const int r,const int c, const double v) {
    if(m->m_StorageType!=MAPSTYLE)
        m->SetStorageType(MAPSTYLE);

    if(r>c && m->IsFlaggedSymmetric() ){
        cout << " skIP setting element "<< r <<" , "<<c<<" on symmetric matrix"<<endl;
        return;
    }
    pair<int,int> ij(r,c);// cout<<"at ("<<r<<" , "<<c<<" ) " ;
    m->m_hbio.ncol1=max(m->Nc(),c); m->m_hbio.nrow1=max(m->m_hbio.nrow1,r);
    map<pair<int,int>,double> ::iterator it;

    it = m->m_map.find(ij);
    if(it!=m->m_map.end()) {
        m->m_map[ij]+=v;// cout<<"  add "<<v<<" -> "<<m->m_map[ij]<<endl;
        return;
    }
    m->m_map[ij]=v; // cout<<" new element " <<v<< endl;
}

void WriteSparseMatrix(RXSparseMatrix1 *m,const char*filename)
{
    // m->PrettyPrint (cout);
    m->HB_Write(filename);
}
#ifdef NEVER
int SparseSolveLinearInline( RXSparseMatrix1 *m, RXSparseMatrix1 *jay,RXSparseMatrix1 *rhs,double*result,int *flags)
// construct a sparse matrix JT . m . J and solve it for RHS.
// if the solution fails return a negative number.
// else a non-negative. 
{
    //    int multrc =0;
    int rc=0;  if  (SPTIMING) cout<<"(SparseSolveLinearInline)pOlishing...";
    class RXSparseMatrix1 a(CSC),c(CSC);
    m->Polish(); jay->Polish(); rhs->Polish();

    m->SetStorageType(MAPSTYLE);			jay->SetStorageType(MAPSTYLE);
    if  (SPTIMING) cout<<"mult..";
    //	rc= m->Multiply (*jay,a);

    //int RXSparseMatrix1::Multiply(RXSparseMatrix1 &y, RXSparseMatrix1 &rv)
    // this is m.  y is jay.   rv is a
    if  (SPTIMING) cout<<"prep..";
    a.SetStorageType(CSC);

    a.SetMatrixType( RXM_RECTANGLE); // safest max(this->m_flags,y.m_flags) );

    if(m->Nc() !=  jay->m_hbio.nrow1)
    {
        cout<<"(SparseSolveLinearInline) Matrix multiply: non conform"<<endl;
        cout<<"("<<m->Nr()<< ","<<m->Nc();
        cout<<") times  ( "<<jay->Nr() << ","<<jay->Nc()<<")"<< endl;
        return -2;
    }

    m->Transpose();
    jay->Transpose ();
    m->Swapcr();
    jay->Swapcr();

    if  (SPTIMING) cout<<"op..";
    int request = 1; // measure memory.
    int sort=0;	// asserting that the matrices are already sorted
    int nzmax=0; int info1=0;
    int mm = m->Nr();
    int n = m->Nc();
    int kk = jay->Nc();
    double *cvals1=0; int *jc1=0;
    int *ic1 =new int[ max(mm,n) +1]; for(int q=0;q<max(mm,n) +1;q++) ic1[q]=0;

    mkl_dcsrmultcsr("N", &request, &sort, &mm, &n, &kk,
                    m->m_hbio.values, m->m_hbio.rowind, m->m_hbio.colptr,
                    jay->m_hbio.values, jay->m_hbio.rowind, jay->m_hbio.colptr,
                    cvals1, jc1, ic1, &nzmax, &info1);
    int ttemp= ic1[mm]-1;
    cvals1=new double[ttemp];
    jc1=new int[ttemp];
    request=0; nzmax=ttemp;
    mkl_dcsrmultcsr("N", &request, &sort, &mm, &n, &kk,
                    m->m_hbio.values, m->m_hbio.rowind, m->m_hbio.colptr,
                    jay->m_hbio.values, jay->m_hbio.rowind, jay->m_hbio.colptr,
                    cvals1, jc1, ic1, &nzmax, &info1);

    // now fill in rv
    a.m_hbio.values=cvals1;
    a.m_hbio.colptr=ic1;
    a.m_hbio.rowind=jc1;
    a.m_hbio.nrow1= jay->Nc();
    a.m_hbio.ncol1= m->m_hbio.nrow1;
    a.m_hbio.nnzero=ttemp;
    if  (SPTIMING) cout<<"post..";
    a.Transpose();

    m->Swapcr();		m->Transpose ();
    jay->Swapcr();
    if  (SPTIMING) cout<<"....";
    // end m->multiply  Jay,a)
    if(info1 !=0) 	cout<< " first multiply failed"<<endl;

    int count=0;

    /////************ start DO ************************/////
    // come back here if it isnt SPD We have to redo the mult because Solve alters the matrix
    do{
        if  (SPTIMING) cout<<"2nd mult.."<<endl;
        jay->SetStorageType(CSC); a.SetStorageType(CSC);
        //		rc=jay->Multiply (a,c) ;
        //	int RXSparseMatrix1::Multiply(RXSparseMatrix1 &y, RXSparseMatrix1 &rv)
        //{  this is jay.  y is  a   c  is rv;
        if  (SPTIMING) cout<<"prep..";
        c.SetStorageType(CSC);
        c.SetMatrixType( RXM_RECTANGLE); // safest max(this->m_flags,y.m_flags) );

        jay->Transpose();
        a.Transpose ();
        jay->Swapcr();
        a.Swapcr();

        if  (SPTIMING) cout<<"op..";
        request = 1; // measure memory.
        sort=0;	// asserting that the matrices are already sorted
        nzmax=0; int info=0;
        mm= jay->Nr();
        n = jay->Nc();
        kk = a.Nc();
        double *cvals=0; int *jc=0;
        int *ic =new int[ max(mm,n) +1]; for(int q=0;q<max(mm,n) +1;q++) ic[q]=0;

        mkl_dcsrmultcsr("N", &request, &sort, &mm, &n, &kk,
                        jay->m_hbio.values, jay->m_hbio.rowind, jay->m_hbio.colptr,
                        a.m_hbio.values, a.m_hbio.rowind, a.m_hbio.colptr,
                        cvals, jc, ic, &nzmax, &info);
        int temp= ic[mm]-1;

        cvals=new double[temp];
        jc=new int[temp];
        request=0; nzmax=temp;
        mkl_dcsrmultcsr("N", &request, &sort, &mm, &n, &kk,
                        jay->m_hbio.values, jay->m_hbio.rowind, jay->m_hbio.colptr,
                        a.m_hbio.values, a.m_hbio.rowind, a.m_hbio.colptr,
                        cvals, jc, ic, &nzmax, &info);

        // now fill in rv
        c.m_hbio.values=cvals;
        c.m_hbio.colptr=ic;
        c.m_hbio.rowind=jc;
        c.m_hbio.nrow1= a.Nc();
        c.m_hbio.ncol1= jay->m_hbio.nrow1;
        c.m_hbio.nnzero=temp;
        if  (SPTIMING) cout<<"post..";
        c.Transpose();

        jay->Swapcr();		jay->Transpose ();
        a.Swapcr();			a.Transpose ();
        if  (SPTIMING) cout<<"....";

        pair<double,double>mystats =c.DiagonalStats();
        if  (SPTIMING) cout<<" diagonal maxmin(1) is "<<mystats.first <<" , "<<mystats.second <<endl;

        // end multiply
        if(info !=0) {cout<< " second multiply failed"; 	return -5;	}
        if(count){
#ifndef _CONSOLE  // for sparsetest framework
            double myfac=1;
            double delta =rxmeanabs(c.m_hbio.values,c.m_hbio.nnzero);
            cout<<"mean= "<<delta<<" fac="<<g_diagonalfac<<" X "<<myfac <<endl;

            c.AddToDiagonal(delta*g_diagonalfac*myfac,-RXLARGE); myfac *=2.;
#else
            assert(0);
#endif
        }
        count++;
        c.SetMatrixType( RXM_STRUCTSYM ) ; // wasRXM_RECTANGLE

        double *p, *r;
        int i, nr = rhs->m_hbio.nrow1;
        if(nr  != c.m_hbio.nrow1 || rhs->Nc()<1 )
        {
            cout<< "RHS doesnt conform. C has nr= "<< c.m_hbio.nrow1 << " nc="<<c.Nc()<<endl;
            cout<< "rhs has nrow (should match C)= "<< nr  << "ncols(sb>0) = "<<rhs->Nc()  <<endl;
            return 0;
        }
        int rlen = nr* rhs->Nc();
        p=r=new double[ rlen];
        memset(r,0,rlen*sizeof(double)); // may be sparse.
        for(i=0;i<rhs->Nc();i++) {
            rhs->GetColumn(i,p);
            p+= c.m_hbio.nrow1;
        }
        c.m_hbio.rhsval=r;
        c.m_nRhsVecs= c.m_hbio.m_nRhsVs= rhs->Nc();
        c.SetStorageType(CSC);

        int mtype = c.GetPardisoFlag();





        rc=c.m_hbio.SolveOnce (c.m_hbio.rhsval,result, mtype );
        delete [] r;
    }while(rc==RX_NOT_POSITIVE_DEFINITE );

    c.m_hbio.rhsval=0;
    a.Init (CSC);
    return rc;
} //SparseSolveLinearInline
#endif
#ifdef NEVER
int SparseSolveLinearD( RXSparseMatrix1 *m, RXSparseMatrix1 *jay,RXSparseMatrix1 *rhs,double*result,int *pflags)// pflags isnt used
// construct a sparse matrix JT . m . J and solve it for RHS.
// if the solution fails return a negative number.
// else a non-negative. 
//  based on SparseSolveLinearC (using sparskit manipulations)

// a test-bed for various solvers;
// mkl_?csrsm are convenient because we already have the right storage type.



{
    //OK  compared with sparsematrixmult.nb  this solves for the transpose matrix = WRONG
    int rc=0;  if  (SPTIMING) cout<<"(SparseSolveLinear D";
    class RXSparseMatrix1 a(RXM_RECTANGLE),c(m->GetFlags());
    int bad=m->Polish()+100* jay->Polish()+ 10000 *rhs->Polish();
    if(bad) cout<<" polish gave "<<bad<<endl;
    if  (SPTIMING) cout<<"storagetype..";
    m->SetStorageType(CSC);			jay->SetStorageType(CSC);
    if  (SPTIMING) cout<<"mult..";
    rc= m->Multiply (*jay,a);
    if(rc !=0) {
        cout<< " first multiply failed: code="<<rc<<endl; return -11;
    }
    //if  (SPTIMING) cout<<endl<<" Jay..";
    //jay->Pretty Print(cout);
    if  (SPTIMING) cout<<" transpose..";
    jay->SetStorageType(MAPSTYLE);
    jay->Transpose();
    jay->SetStorageType(CSC);
    //	jay->Prett yPrint(  cout);
    int count=0;
    double myfac=1.0;
    do{  // come back here if it isnt SPD We have to redo the mult because Solve alters the matrix
        if  (SPTIMING)cout<<"setstoragetype.."<<endl;
        jay->SetStorageType(CSC); a.SetStorageType(CSC);
        if  (SPTIMING)cout<<"2nd mult..";
        rc=jay->Multiply (a,c) ;
        if(rc !=0) {
            cout<< "SparseSolveLinearD second multiply failed"; 	return -5;
        }
        if  (SPTIMING) cout<<"mult done..";
        //c.HB_Write("cmatrix.rra"); //c.Pretty Print (cout);
        pair<double,double>mystats =c.DiagonalStats(); // max and min
        double avg =rxmeanabs(c.m_hbio.values,c.m_hbio.nnzero);  // the mean of the whole matrix
        if(count ||(mystats.second<avg *g_diagonalfac)) {
            if  (SPTIMING)cout<<"diag max & min "<<mystats.first <<", "<<mystats.second <<" avg= "<<avg<<", ";
#ifndef _CONSOLE
            if(fabs(g_diagonalfac  )< 1e-8){
                g_World->OutputToClient(" need non-zero <diagonal fac> in INI file",3);
                return 1;
            }
#endif
            double dmin = avg *g_diagonalfac*myfac ;
            if  (SPTIMING)cout<<" Limit diagonal "<<dmin <<" ("<<g_diagonalfac<<" X "<<myfac <<")"<<endl;
            c.LimitToDiagonal(dmin,mystats.first+1.);
            myfac *=2.;
#ifndef _CONSOLE
            int kkk=2; kkk=check_messages(&kkk);
            if (relaxflagcommon.Stop_Running != 0){
                cout<<" user brEak";
                break;
            }
#endif
        }

        count++;
        c.SetMatrixType(m->GetFlags()) ; //if symmetric, should make upper triangular

        double *p, *r;
        int i, nr = rhs->m_hbio.nrow1;
        if(nr  != c.m_hbio.nrow1 || rhs->Nc()<1 )
        { 	cout<< "RHS doesnt conform. C has nr= "<< c.m_hbio.nrow1 << " nc="<<c.Nc()<<endl;
            cout<< "rhs has nrow (should match C)= "<< nr  << "ncols(sb>0) = "<<rhs->Nc()  <<endl;	return -20;}
        int rlen = nr* rhs->Nc();
        p=r=new double[ rlen];
        memset(r,0,rlen*sizeof(double)); // may be sparse.
        for(i=0;i<rhs->Nc();i++) {
            rhs->GetColumn(i,p);
            p+= c.m_hbio.nrow1;
        }
        c.m_hbio.rhsval=r;
        c.m_nRhsVecs= c.m_hbio.m_nRhsVs= rhs->Nc();
        if(c.IsSymmetric ())
            if(! c.MakeUpperTriangular()) cout<<"MUT FAILED !"<<endl;
        c.SetStorageType(CSC);
        c.Transpose();
        if  (SPTIMING) cout<<" solve..";
        int mtype = c.GetPardisoFlag();
        //       rc=c.m_hbio.SolveOnce (c.m_hbio.rhsval,result, mtype );	if  (SPTIMING) cout<<" rv="<<rc<<" ";

        // WORKFACE:  mkl direct solver
        //Input Parameters

        //        Parameter descriptions are common for all implemented interfaces with the exception of data types that refer here to the FORTRAN 77 standard types. Data types specific to the different interfaces are described in the section "Interfaces" below.

        char *transa ="T"  ;

        //            CHARACTER*1. Specifies the system of linear equations.
        //            If transa = 'N' or 'n', then C := alpha*inv(A)*B
        //            If transa = 'T' or 't' or 'C' or 'c', then C := alpha*inv(A')*B,

        int  m =-32;   //            INTEGER. Number of columns of the matrix A.
        int  n =1;//            INTEGER. Number of columns of the matrix C.

        double alpha=1.0e00;  //   Specifies the scalar alpha.

        char  matdescra[6];//    CHARACTER. Array of six elements, specifies properties of the matrix used for operation. Only first four array elements are used, their possible values are given in Table “Possible Values of the Parameter matdescra (descra)”. Possible combinations of element values of this parameter are given in Table “Possible Combinations of Element Values of the Parameter matdescra”.

        double *val =c.m_hbio.values ;//            DOUBLE PRECISION for mkl_dcsrsm.
        //            Array containing non-zero elements of the matrix A.
        //            For one-based indexing its length is pntre(m) - pntrb(1).
        //            For zero-based indexing its length is pntre(m-1) - pntrb(0).
        //            Refer to values array description in CSR Format for more details.
        //            Note iconNote
        //            The non-zero elements of the given row of the matrix must be stored in the same order as they appear in the row (from left to right).
        //            No diagonal element can be omitted from a sparse storage if the solver is called with the non-unit indicator.

        int *indx; //   INTEGER. Array containing the column indices for each non-zero element of the matrix A. Its length is equal to length of the val array.

        //            Refer to columns array description in CSR Format for more details.
        //            Note iconNote
        //            Column indices must be sorted in increasing order for each row.

        int * pntrb; //  INTEGER. Array of length m.
        //            For one-based indexing this array contains row indices, such that pntrb(i) - pntrb(1)+1 is the first index of row i in the arrays val and indx.
        //            For zero-based indexing this array contains row indices, such that pntrb(i) - pntrb(0) is the first index of row i in the arrays val and indx. Refer to pointerb array description in CSR Format for more details.

        int *pntre;//    INTEGER. Array of length m.
        //            For one-based indexing this array contains row indices, such that pntre(i) - pntrb(1) is the last index of row i in the arrays val and indx.
        //            For zero-based indexing this array contains row indices, such that pntre(i) - pntrb(0)-1 is the last index of row i in the arrays val and indx.Refer to pointerE array description in CSR Format for more details.

        double * b;//            DOUBLE PRECISION for mkl_dcsrsm.
        //            Array, DIMENSION (ldb, n)for one-based indexing, and (m, ldb) for zero-based indexing.
        //            On entry the leading m-by-n part of the array b must contain the matrix B.

        int ldb; //   INTEGER. Specifies the first dimension of b for one-based indexing, and the second dimension of b for zero-based indexing, as declared in the calling (sub)program.

        int ldc;//    INTEGER. Specifies the first dimension of c for one-based indexing, and the second dimension of c for zero-based indexing, as declared in the calling (sub)program.

        //        Output Parameters

        double *c;//   REAL*8.
        //            Array, DIMENSION (ldc, n) for one-based indexing, and (m, ldc) for zero-based indexing.

        //            The leading m-by-n part of the array c contains the output matrix C.

        mkl_dcsrsm( transa, &m, &n, &alpha,
                    matdescra, val, indx, pntrb,
                    pntre, b, &ldb, c, &ldc);

        delete [] r;
    }while(rc==RX_NOT_POSITIVE_DEFINITE );

    c.m_hbio.rhsval=0;
    a.Init (CSC);
    return rc;
} //SparseSolveLinearD
#endif
//we need to recover from
//op.. solve..np= 8solving with mtype=1
//ERROR during  solution: -4
// zero pivot at row 0
//
// rv=3   drop out of IM due to failed solver
int SparseSolveLinearC( RXSparseMatrix1 *m, RXSparseMatrix1 *jay,RXSparseMatrix1 *rhs,double*result,int *pflags)// pflags isnt used
// construct a sparse matrix JT . m . J and solve it for RHS.
// if the solution fails return a negative number.
// else a non-negative.
// this version uses sparskit for the transpose and for changing storage type.
// because they account for over 50% of runtime.
// it is VERIFIED 02 april 2011. Let's re-write the InLine solver to see if we can reduce the operations.
//
{
    //OK  compared with sparsematrixmult.nb  this solves for the transpose matrix = WRONG
    int rc=0;  if  (SPTIMING) cout<<"(SparseSolveLinearC";
    class RXSparseMatrix1 a(RXM_RECTANGLE),c(m->GetFlags());
    int bad=m->Polish()+100* jay->Polish()+ 10000 *rhs->Polish();
    if(bad) cout<<" polish gave "<<bad<<endl;
    if  (SPTIMING) cout<<"storagetype..";
    m->SetStorageType(CSC);			jay->SetStorageType(CSC);
    if  (SPTIMING) cout<<"mult..";
    rc= m->Multiply (*jay,a);
    if(rc !=0) {
        cout<< " first multiply failed: code="<<rc<<endl; return -11;
    }

    if  (SPTIMING) cout<<" transpose..";
    jay->SetStorageType(MAPSTYLE);
    jay->Transpose();
    jay->SetStorageType(CSC);

    int count=0;
    double myfac=1.0;
    do{  // come back here if it isnt SPD We have to redo the mult because Solve alters the matrix
        if  (SPTIMING)cout<<"setstoragetype.."<<endl;
        jay->SetStorageType(CSC);
        a.SetStorageType(CSC);
        if  (SPTIMING)cout<<"2nd mult..";
        rc=jay->Multiply (a,c) ;
        if(rc !=0) {
            cout<< " SparseSolveLinearC second multiply failed"<<rc;
            return -5;	}
        if  (SPTIMING) cout<<"mult done..";
        //c.HB_Write("cmatrix.rra"); //c.Pretty Print (cout);
        pair<double,double>mystats =c.DiagonalStats(); // max and min
        double avg =rxmeanabs(c.m_hbio.values,c.m_hbio.nnzero);  // the mean of the whole matrix
        if(count ||(mystats.second<avg *g_diagonalfac)) {
            if  (SPTIMING)cout<<"diag max & min "<<mystats.first <<", "<<mystats.second <<" avg= "<<avg<<", ";
#ifndef _CONSOLE
            if(fabs(g_diagonalfac  )< 1e-8){
                g_World->OutputToClient(" need non-zero <diagonal fac> in INI file",3);
                return 1;
            }
#endif
            double dmin = avg *g_diagonalfac*myfac ;
            if  (SPTIMING)cout<<" Limit diagonal "<<dmin <<" ("<<g_diagonalfac<<" X "<<myfac <<")"<<endl;
            c.LimitToDiagonal(dmin,mystats.first+1.);
            myfac *=2.;
#ifndef _CONSOLE
            int kkk=2; kkk=check_messages(&kkk);
            if (relaxflagcommon.Stop_Running != 0){
                cout<<" user brEak";
                break;
            }
#endif
        }

        count++;
        c.SetMatrixType(m->GetFlags()) ; //if symmetric, should make upper triangular

        double *p, *r;
        int i, nr = rhs->m_hbio.nrow1;
        if(nr  != c.m_hbio.nrow1 || rhs->Nc()<1 )
        { 	cout<< "RHS doesnt conform. C has nr= "<< c.m_hbio.nrow1 << " nc="<<c.Nc()<<endl;
            cout<< "rhs has nrow (should match C)= "<< nr  << "ncols(sb>0) = "<<rhs->Nc()  <<endl;	return -20;}
        int rlen = nr* rhs->Nc();
        p=r=new double[ rlen];
        memset(r,0,rlen*sizeof(double)); // may be sparse.
        for(i=0;i<rhs->Nc();i++) {
            rhs->GetColumn(i,p);
            p+= c.m_hbio.nrow1;
        }

        c.m_hbio.rhsval=r;
        c.m_nRhsVecs= c.m_hbio.m_nRhsVs= rhs->Nc();
        if(c.IsFlaggedSymmetric ())
            if(! c.MakeUpperTriangular()) cout<<"MUT FAILED !"<<endl;
        c.SetStorageType(CSC);
        c.Transpose();
        if  (SPTIMING) cout<<" solve..";
        int mtype = c.GetPardisoFlag();

        rc=c.m_hbio.SolveOnce (c.m_hbio.rhsval,result, mtype );	if  (SPTIMING) cout<<" rv="<<rc<<" ";

        if  (SPTIMING) cout<<" rv="<<rc<<" ";

        delete [] r;
    }while( rc==RX_NOT_POSITIVE_DEFINITE );

    c.m_hbio.rhsval=0;
    a.Init (CSC);
    return rc;
} //SparseSolveLinearC

int SparseSolveLinear( RXSparseMatrix1 *m, RXSparseMatrix1 *jay,RXSparseMatrix1 *rhs,double*result,int *flags)
// construct a sparse matrix JT . m . J and solve it for RHS.
// if the solution fails return a negative number.
// else a non-negative. 
{
    //#ifdef _DEBUG
    if(true) return SparseSolveLinearC(m,jay,rhs,result,flags);
    //#endif
    int rc=0;  if  (SPTIMING) cout<<"(SparseSolveLinear  ) Polishing...";
    class RXSparseMatrix1 a(RXM_RECTANGLE),c(m->GetFlags());
    m->Polish(); jay->Polish(); rhs->Polish();

    m->SetStorageType(CSC);			jay->SetStorageType(CSC);
    if  (SPTIMING) cout<<"mult..";
    rc= m->Multiply (*jay,a);
    if(rc !=0) {
        cout<< " first multiply failed"<<endl; return -11;
    }
    if  (SPTIMING) cout<<" transpose..";
    jay->Transpose();
    int count=0;
    double myfac=1.0;
    do{  // come back here if it isnt SPD We have to redo the mult because Solve alters the matrix
        if  (SPTIMING) cout<<"setstoragetype.."<<endl;
        jay->SetStorageType(CSC); a.SetStorageType(CSC);
        if  (SPTIMING) cout<<"2nd mult..";
        rc=jay->Multiply (a,c) ;	if(rc !=0) {cout<< " SparseSolveLinear (dead) second multiply failed"; 	return -5;	}
        if  (SPTIMING) cout<<"mult done..";
        pair<double,double>mystats =c.DiagonalStats(); // max and min
        double avg =rxmeanabs(c.m_hbio.values,c.m_hbio.nnzero);  // the mean of the whole matrix
        if(count ||(mystats.second<avg *g_diagonalfac)) {
            cout<<"diag max & min "<<mystats.first <<", "<<mystats.second <<" avg= "<<avg<<", ";
            if(fabs(g_diagonalfac  )< 1e-8){
#ifndef _CONSOLE
                g_World->OutputToClient(" need non-zero <diagonal fac> in INI file",3);
#endif
                return 1;
            }

            double dmin = avg *g_diagonalfac*myfac ;
            cout<<" Limit diagonal "<<dmin <<" ("<<g_diagonalfac<<" X "<<myfac <<")"<<endl;
            c.LimitToDiagonal(dmin,mystats.first+1.);
            myfac *=2.;
#ifndef _CONSOLE
            int kkk=2; kkk=check_messages(&kkk);
            if (relaxflagcommon.Stop_Running != 0){
                cout<<" user break";
                break;
            }
#endif
        }

        count++;
        c.SetMatrixType(m->GetFlags()) ; //should make upper triangular

        double *p, *r;
        int i, nr = rhs->m_hbio.nrow1;
        if(nr  != c.m_hbio.nrow1 || rhs->Nc()<1 )
        { 	cout<< "RHS doesnt conform. C has nr= "<< c.m_hbio.nrow1 << " nc="<<c.Nc()<<endl;
            cout<< "rhs has nrow (should match C)= "<< nr  << "ncols(sb>0) = "<<rhs->Nc()  <<endl;	return -20;}
        int rlen = nr* rhs->Nc();
        p=r=new double[ rlen];
        memset(r,0,rlen*sizeof(double)); // may be sparse.
        for(i=0;i<rhs->Nc();i++) {
            rhs->GetColumn(i,p);
            p+= c.m_hbio.nrow1;
        }
        c.m_hbio.rhsval=r;
        c.m_nRhsVecs= c.m_hbio.m_nRhsVs= rhs->Nc();
        // cant do this  unless we know we are symmetric
        //	if(! c.MakeUpperTriangular())  cout<<"MUT FAILED !"<<endl;
        c.SetStorageType(CSC);
        //c.m_hbio.Write("matrixsenttosolve.rsa");
        if  (SPTIMING) cout<<" solve..";
        int mtype = c.GetPardisoFlag();
        rc=c.m_hbio.SolveOnce (c.m_hbio.rhsval,result, mtype );	if  (SPTIMING) cout<<" rv="<<rc<<" ";
        delete [] r;
    }while(rc==RX_NOT_POSITIVE_DEFINITE );

    c.m_hbio.rhsval=0;
    a.Init (CSC);
    return rc;
} //SparseSolveLinear
//we need to recover from
//op.. solve..np= 8solving with mtype=1
//ERROR during  solution: -4
// zero pivot at row 0
//
// rv=3   drop out of IM due to failed solver


#ifndef _CONSOLE  // for sparsetest framework
int  SparseSolveSVD  ( RXSparseMatrix1 *pm, RXSparseMatrix1 *jay,RXSparseMatrix1 *rhs,double*result,int *flags)
// if the solution fails return a negative number.
// else a non-negative. 

// construct a sparse matrix JT . m . J and solve it for RHS assuming that it is singular
// we'd normally only be here if we have first failed SparseSolveLinear,
//often because the matrix is singular or not PD
//  so its silly to re-compute Jt S J we should use the old one.
{
    int rc=0;
    class RXSparseMatrix1 l_a(CSC),c(CSC);
    pm->Polish();  rhs->Polish();
    jay->Transpose (); // because sPARSEsOLVElINEAR LEft it transposed.

    pm->SetStorageType(CSC);			jay->SetStorageType(CSC);
    rc= pm->Multiply (*jay,l_a);
    if(rc !=0) {
        cout<< " first multiply failed"<<endl;
        pm->PrettyPrint (cout); // multiply fail
        jay->PrettyPrint (cout);
    }
    jay->Transpose();
    rc=jay->Multiply (l_a,c) ;
    if(rc !=0) {
        cout<< "SparseSolveSVD(dead) second multiply failed"<<endl;
        return -5;
    }
    l_a.Init (CSC); // save some memory
    c.SetMatrixType(RXM_RECTANGLE) ; //should make upper triangular

    double *p ;
    int i, nr = rhs->m_hbio.nrow1;
    int nc = c.m_hbio .ncol1;
    if(nr  != c.m_hbio.nrow1 || rhs->Nc()<1 )
    {
        cout<< "RHS doesnt conform. C has nr= "<< c.m_hbio.nrow1 << " nc="<<c.Nc()<<endl;
        cout<< "rhs has nrow (should match C)= "<< nr  << "ncols(sb>0) = "<<rhs->Nc()  <<endl;
        return 0;}

    int rlen = nr* rhs->Nc();
    p=result;
    memset(result,0,rlen*sizeof(double)); // may be sparse.
    for(i=0;i<rhs->Nc();i++) {
        rhs->GetColumn(i,p);
        p+= c.m_hbio.nrow1;
    }

    if(! c.MakeUpperTriangular()) cout<<" MakeUpperTriangular  FAILED !"<<endl;
    c.SetStorageType(CSC);
    c.m_hbio.Write("matrixsenttoSVDsolve.rsa");

    double *a =c.FullMatrix();
    if(!a)
        return -99;
    cout<<" start SVDsolve.. " ;
    rc=-99;
    rc= cf_SolveSVD(a,nr,nc,result );

    cout<<" end SVDsolve with rv="<<rc<<endl;

    delete [] a;

    return rc;
} //SparseSolveSVD

#endif

RXSparseMatrix1 * RXSparseMatrix1::PreConditioner(const int type )
{
    RXSparseMatrix1 * rc=0;
    map<int,double> diag;
    map<int,double> ::iterator it;
    map<pair<int,int>,double> pvalues;
    switch( type)
    {
    case RXMP_DIAGONAL:
        ExtractDiagonal( diag);
        for(it=diag.begin(); it!=diag.end();++it){
            if(it->second > 1e-12 )
                it->second =1.0/it->second ;
            else
                cout<<" negative on diag at "<< it->first<<endl;
            pvalues[ make_pair(it->first,it->first)] = it->second;
        }
        rc=new RXSparseMatrix1(pvalues,RXM_SYMMETRIC);
        break;
    default:
        assert( strlen(" invalid preconditioner type") ==0);
    };
    return rc;
}

int RXSparseMatrix1::SetRHS(const int p_nrhs, double* p_rhs)
{
    int i;
    if(!p_rhs ) return 0;
    double*p;
    switch(this->m_StorageType ) {
    case MAPSTYLE:
        this->rHs.resize(m_hbio.nrow1+1);
        for(i=0,p=p_rhs ; i<p_nrhs ; i++,p++){
            //cout <<*p<<endl;
            rHs[i]= *p;
        }
        return p_nrhs;
    case CSC:
        return this->m_hbio.SetRHS(rHs);
    default:
        assert(0);

    };//switch
    return 0;
}
//%%MatrixMarket matrix array real general
//%Created by Wolfram Mathematica 7.0 : www.wolfram.com
//2 2
//  1.1000000000000000E+001
//  2.1000000000000000E+001
//  1.2000000000000000E+001
//  2.2000000000000000E+001

//%%MatrixMarket matrix coordinate real general
//%Created by Wolfram Mathematica 7.0 : www.wolfram.com
//3 3 5
//1 1   1.0000000000000000E+000
//1 3   4.0000000000000000E+000
//2 3   2.0000000000000000E+000
//3 2  -1.0000000000000000E+000
//3 3   3.0000000000000000E+000


int RXSparseMatrix1::MTXRead(const char*fname) //  the MAP uses indexing starting at zero
{
    int r,c,nl=0 ; double v;
    int contents=0; // 1=array, 2 = coordinates
    char buf[1024];
    filename_copy_local(buf,2048,fname );	ifstream f; f.open (buf);
    if(!f) return 0;
    string s;

    this->Init (MAPSTYLE );

    f.getline (buf,1023); s=string(buf); //%%MatrixMarket matrix array real general
    f.getline (buf,1023); 	 //%Created by Wolfram Mathematica 7.0 : www.wolfram.com
    f>>m_hbio.nrow1>>m_hbio.ncol1;
    if(s.find(" hermitian") !=string::npos) 	this->SetMatrixType (RXM_HERMITIAN);
    else if(s.find("skew-symmetric") !=string::npos) 	this->SetMatrixType (RXM_SKEW_SYMM);
    else if(s.find(" symmetric") !=string::npos) 	this->SetMatrixType (RXM_SYMMETRIC);
    else if(s.find(" general") !=string::npos) 	{
        if( Nr()==Nc())  this->SetMatrixType (0);
        else							 this->SetMatrixType (RXM_RECTANGLE);
    }

    if(s.find(" array") !=string::npos) 			contents=1;
    else if(s.find(" coordinate") !=string::npos)	contents=2;

    if(s.find(" matrix") ==string::npos) 	 {
        cout<<"no keyword <matrix> in header. this type not coded here"<<endl;
        f.close(); return 0;
    }

    cout<< fname<<endl;
    cout<<s<<endl<<buf<<endl;


    if(contents==1) {
        cout<<"Array MTXread. by columns not rows"<<endl;
        for(c=1;c<=Nc();c++) {
            for(r=1;r<=Nr();r++) {
                f>> v; if(f.bad()) break;
                m_map[pair<int,int>(r,c)]=v;
            }
        }
    }
    else 	if(contents==2) {
        f>>m_hbio.nnzero;
        do{
            f>>r>>c>>v; if(!f.good()) break;
            nl++;
            m_map[pair<int,int>(r,c)]=v;
            if (this->IsFlaggedSymmetric())
                m_map[pair<int,int>(c,r)]=v;

        }while( f.good());
    }

    f.close();
    return nl;

}

int RXSparseMatrix1::MTXWrite(const char*fname) //  the MAP uses indexing starting at zero
{
    string f(fname);
    size_t p = f.find_last_of('.');
    if(p==string::npos) f=f+string(".MTX");
    else	f=f.substr(0,p)+string(".MTX");
    FILE *fp=RXFOPEN(f.c_str(),"w");

    if(!fp) return 0;
    SetStorageType(MAPSTYLE);
    fprintf(fp,"%%%%MatrixMarket matrix coordinate real %s\n%%Created by RX Mesh www.openfsi.org\n",GetMMartString());

    int nel=0;// ni=0,nj=0

    map<pair<int,int>,double>::const_iterator it;
    for(it=m_map.begin(); it!=m_map.end();++it) {
        nel++;
        //	ni=max(ni,it->first.first);
        //	nj=max(nj,it->first.second);
    }
    fprintf(fp,"%%  nr   nc  nnzero\n");
    fprintf(fp,"%d %d %d\n", this->Nr(),Nc() ,nel);
     fprintf(fp,"%%  the elements\n%% row   col  value\n");
    for(it=m_map.begin() ;it!=m_map.end();++it) {
        fprintf(fp,"%d %d %.9g\n",it->first.first,it->first.second,it->second );
    }

    FCLOSE(fp);

    return nel;

}
int RXSparseMatrix1::HB_Read(const char*fname)
{	
    this->Init (CSC);
    int rc=0;

    rc = this->m_hbio.Read (fname); this->SetStorageType(CSC);

    return rc;
}


int RXSparseMatrix1::HB_Write(const char*fname) 
{
    int rc;
    enum  smtype t =this->m_StorageType;
    //if(this->m_StorageType !=CSC)
    //{ cout<< "cant write "<<fname<<" storage type not CSC "<<endl;
    //return 0;}
    SetStorageType(CSC);
    if(this->m_hbio.title.empty()) m_hbio.title="generated by openfsi.org";
    if(this->m_hbio.key.empty()) m_hbio.key="openfsi" ;
    rc = this->m_hbio.Write(fname);
    SetStorageType(t);
    return rc;
}
int RXSparseMatrix1::TestForSymmetric(const map<pair<int,int>,double> &m,const double &tol ){
    // for each (i,j) there must exist a (j,i) with the same value

    int r,c;
    double a;
    map<pair<int,int>,double>::const_iterator it,cr;
    for(it=m.begin();it!=m.end();++it)
    {
        r = it->first.first;
        c = it->first.second;
        if(r==c) continue;
        a = it->second;
        cr = m.find(pair<int,int>(c,r));
        if(cr==m.end())
            return 0;
        if(fabs(a - cr->second) > tol)
            return 0;
    }
    return 1;

}
int RXSparseMatrix1::MakeUpperTriangular(  ){
    // remove any elements with c<r
    this->SetStorageType( MAPSTYLE );
    int r,nr,nc;
    map<pair<int,int>,double>::iterator it1,it2;

    map<pair<int,int>,double>::reverse_iterator e = m_map.rbegin ();
    nr  = e->first.first+1;
    nc = e->first.second+1;
    if(1) { // original code
        for(r=1;r<nr;r++) {
            it2 = m_map.find(pair<int,int>(r,r));
            if(it2==m_map.end()) {
                cout <<"(MakeTriangular)  no diagonal at row "<<r<<endl;
                return 0;}
            it1 = m_map.lower_bound(pair<int,int>(r-1,nc+1));
            if(it2!=m_map.end()){
                m_map.erase(it1,it2);
            }
        }
    }
    else //new code
    {
        for(r=1;r<nr;r++) {
            it1 = m_map.lower_bound(pair<int,int>(r,0));// returns the first element of row r
            if(it1==m_map.end())
                continue;
            it2 = m_map.lower_bound(pair<int,int>(r,r));// returns (r,r) if it exists or else the next one after.
            m_map.erase(it1,it2);// erase takes it1 and stops just short of it2
        }
    }
    this->SetMatrixType(RXM_SYMMETRIC);
    return 1;
}

double * RXSparseMatrix1::FullMatrix() 
{
    int  nr = Nr();
    int  nc= Nc();

    if(this->IsFlaggedSymmetric()){
        assert(nr==nc);
        double * a = new double[nr*nc];memset(a,0,nr*nc*sizeof(double));
        if(!a) { cout<<" cant allocate for  RXSparseMatrix1::FullMatrix "<<endl; return NULL;}
        double v;
        int r,c;
        for(r=1;r<=Nr();r++) {
            for(c=1;c<=r;c++) {
                if( SlowGetElement( r,c,v))
                {
                    a[(r-1)*nr+(c-1)] =v;
                    a[(c-1)*nr+(r-1)] =v;
                }
            }}
        return a;
    }
    if(nr!=nc){
        cout<<" Fullmatrix on rectangular NOT CODED"<<endl;
        return 0;
    }
    double * a = new double[nr*nc];memset(a,0,nr*nc*sizeof(double));
    if(!a) { cout<<" cant allocate for  RXSparseMatrix1::FullMatrix "<<endl; return NULL;}
    double v;
    int r,c;

    for(r=1;r<=Nr();r++) {
        for(c=1;c<=Nc();c++) {
            if( SlowGetElement( r,c,v))
                a[(r-1)*nr+(c-1)] =v;
        }}
    if(false){
        this->HB_Write("fullmatrix.RRA");
        FILE *fp = fopen("fullmatrix.txt","w");
        for(r=0;r<Nr();r++)
        {
            for(c=0;c<Nc()-1;c++)   fprintf(fp,"%20.14g\t",  a[r*nr+c] );
            fprintf(fp,"%20.14g\n", a[r*nr+c]);
        }
        fclose(fp);
    }
    return a;
}
int RXSparseMatrix1::PrettyPrint(std::ostream &s) const 
{
    int k=0,r,c;
    pair<int,int> rc;
    map<pair<int,int>,double>::const_iterator it;
    double v;
    const class RXHarwellBoeingIO1 *hb= &( m_hbio);


    s<<endl<<"RX Sparse Matrix BASE ONE "<<endl;
    if(this->m_flags &RXM_STRUCTSYM	)	s<< "RXM_STRUCTSYM"<<endl;
    if(this->m_flags &RXM_SYMMETRIC	)	s<< "RXM_SYMMETRIC"<<endl;
    if(this->m_flags &RXM_RECTANGLE)	s<< "RXM_RECTANGLE"<<endl;
    if(this->m_flags &RXM_SKEW_SYMM	)	s<< "RXM_SKEW_SYMM"<<endl;
    if(this->m_flags &RXM_POS_DEF	)	s<< "RXM_POS_DEF"<<endl;
    if(this->m_flags &RXM_INDEFINITE)	s<< "RXM_INDEFINITE"<<endl;
    if(this->m_flags &RXM_COMPLEX)		s<< "RXM_COMPLEX"<<endl;
    if(this->m_flags &RXM_HERMITIAN)	s<< "RXM_HERMITIAN"<<endl;

    s <<" nr = "<<Nr()<<" nc= "<<Nc()<<" nnzeros= "<< m_hbio.nnzero<<endl<<" storagetype "<<this->m_StorageType <<endl;
    s<<" mx type <"<<m_hbio.mxtype <<"> Mattype "<<this->m_flags <<endl;
    if(m_map.size())
        s<< "listing from map"<<endl;
    else s<<" No map is defined"<<endl;

    if(hb->rowind && hb->colptr &&hb->values){
        s<< "listing of CSR storage"<<endl;
        s<<"colptr..."<<endl;		for(k=0;k<hb->ncol1+1;k++)
            s<<" "<<hb->colptr[k];  s<<endl;
        s<<"rowind "<<endl; 		for(r=0;r<m_hbio.nnzero;r++)
            s<<" "<<hb->rowind[r]; s<<endl;
        s<<"values  "<<endl; 		for(r=0;r<m_hbio.nnzero;r++)
            s<<" "<<hb->values[r]; s<<endl;
    }
    else s<< "NO  CSR representation"<<endl;
    int w=7;
    s<<"\n Matrix Formatted \n";
    s.width(w); s<<"row";
    for(c=1;c<=Nc();c++)
    {s.width(w);s<<c;}
    s<<endl;
    for(r=1;r<=Nr();r++) {
        s.width(w);s<<r;
        for(c=1;c<=Nc();c++) {
            if( SlowGetElement( r,c,v))
            {s<<" "; s.width(w-1);s<<v;}
            else
            {s.width(w);s<<" .";}
        }
        s<<endl;
    }
    s<<endl<<" RHS nrhsVecs = "<<this->m_nRhsVecs << " rhs length = "<<this->rHs .size()<<endl;
    if(this->rHs.size()) {
        ; k=0;
        for(vector<double>::const_iterator it2 = rHs.begin();it2!=rHs.end ();it2++,k++) {
            s<< k<<"  "<<*it2<<endl;
        }
    }
    return 1;
}
#ifdef NEVER
RXSparseMatrixRow::RXSparseMatrixRow()
    : m_mat(0)
    , n(0)
    , k(-1)
{
    assert(" dont use this construcTor"==0);
}
RXSparseMatrixRow::RXSparseMatrixRow( class RXSparseMatrix1 *m  , const int nn )
    : m_mat(m)
    , n(nn)
    ,k(-1)
{
}
RXSparseMatrixRow::~RXSparseMatrixRow(){
    m_l.clear();
}
int RXSparseMatrixRow::Init(void)	// Just  clears the list. maintains ref into matrix
{	int rc = this->m_l.size();
    this->m_l.clear ();
    this->k=-2;
    return rc;
}

// this fn is very specific to the needs of PrePostMult. It will be incorrect unless 'this' is a dogleg
size_t RXSparseMatrixRow::operator+=(pair<list<pair< pair<int,int>,double> > ,  int > arg ){
    // its ok  for the two to be different sizes and for members of src not to exist in this
    //  algorithm:
    // before the break, the second indices in the pair<r,c> must be equal,
    // otherwise we insert an element in this' matrix
    // after the break, the first indices should align.
    // so we walk through the SRC. the element of Src is <<iSrc,jSrc>,val>
    // up to and including the break of src, the target index is jSrc.
    // afterwards it is iSrc.
    // up to, and including, the break of this, the targetrow is this->n
    // after the break  of this, the target col is this->n;
    // the target always has  r >= c

    list<pair< pair<int,int>,double> > &src = arg.first ;
    const int &srcK =arg.second;

    map<pair<int,int>,double> ::iterator it2;
    int kSrc ,count=0;// iSrc,jSrc,
    double val;
    pair<int,int> target;
    list<pair< pair<int,int>,double> > ::const_iterator itSrc;
    for ( itSrc=src.begin(),count=0 ; itSrc != src.end(); itSrc++,count++ ){
        //iSrc = itSrc->first.first ;
        //	jSrc =  itSrc->first .second ;
        if(count<=srcK)
            kSrc =itSrc->first .second ;
        else
            kSrc =itSrc->first.first ;
        target.first = max(this->n,kSrc);
        target.second = min(this->n,kSrc);
        val =  itSrc->second;

        it2 = this->m_mat->m_map.find (target);
        if(it2 != this->m_mat->m_map.end()){
            //cout<<" element ("<<target.first<<" "<<target.second  <<") was "<< it2->second;
            it2->second += val ;
            //cout<<" becomes "<< it2->second<<endl;
        }
        else{
            this->m_mat->m_map[target ]= val ;
            cout<<" new element ("<<target.first<<" "<<target.second  <<") = "<< val<<endl;
        }
    }
    return  m_l.size();
}

size_t  RXSparseMatrixRow::operator+=(const RXSparseMatrixRow& src) 
{

    if(this->n != src.n) {assert(0); return 0;}

    list<pair<int,int> >::const_iterator it1;
    map<pair<int,int>,double> ::iterator it2;
    for ( it1=src.m_l.begin() ; it1 != src.m_l.end(); it1++ ){
        // *it1 is <pair<int,int>
        it2 = this->m_mat->m_map.find (*it1);
        if(it2 != this->m_mat->m_map.end())
            it2->second +=src.m_mat ->m_map[*it1];
        else
            this->m_mat->m_map[(*it1) ]= src.m_mat ->m_map[*it1];
    }
    return  m_l.size();
}


size_t  RXSparseMatrixRow::operator*=(const double&f)
{
    list<pair<int,int> >::const_iterator it;
    for ( it=m_l.begin() ; it != m_l.end(); it++ )
        m_mat->m_map[*it ] *=f;

    return  m_l.size();
}
int RXSparseMatrixRow::PrettyPrint(std::ostream &s) const
{
    cout<<"RXSparseMatrixRow n="<<this->n<<" k="<<this->k<<endl;
    int rc=0;
    list<pair<int,int> >::const_iterator it;
    for ( it=m_l.begin() ; it != m_l.end(); it++ )
        cout<<" ( "<<it->first<<" , "<<it->second <<"  "<< m_mat->m_map[*it ]<<endl;

    return rc;
}
list<pair< pair<int,int>,double> > RXSparseMatrixRow::Extract(const double x)  const
{
    list<pair< pair<int,int>,double> > rc;

    list<pair<int,int> >::const_iterator it;
    //	 pair <string,double> product1 ("tomatoes",3.25);

    for ( it=m_l.begin() ; it != m_l.end(); it++ ){
        pair< pair<int,int>,double>  val ((*it)  ,  x*( m_mat->m_map[*it ]) );
        rc.push_back(val);
    }
    return rc;
}
#endif
void SparseMatrixDimensions(RXSparseMatrix1 *m, int* nr, int *nc)
{ *nr=m->Nr(); *nc = m->m_hbio.ncol1 ;
}

//static _MKL_DSS_HANDLE_t m_dsshandle;
//static MKL_INT m_dssopt;// = MKL_DSS_DEFAULTS ;
//static MKL_INT m_dsssym;// = MKL_DSS_NON_SYMMETRIC;
//static MKL_INT m_dsstype;// = MKL_DSS_INDEFINITE;
int RXSparseMatrix1::dss_start()  // see mkl example dss_unsym_c.c
{
    int error;
   //   qDebug()<<this->Statistics();
    m_dssopt = MKL_DSS_DEFAULTS;
    m_dsssym = MKL_DSS_NON_SYMMETRIC;
    m_dsstype = MKL_DSS_INDEFINITE;//MKL_DSS_POSITIVE_DEFINITE;//
    if(this->IsFlaggedSymmetric())
     m_dsssym = MKL_DSS_SYMMETRIC;


    /* --------------------- */
   // qDebug()<< " Initialize the solver  */";
    /* --------------------- */
    assert(0==MKL_DSS_SUCCESS);
    assert(sizeof(int)==sizeof(MKL_INT ));

    error = dss_create (m_dsshandle, m_dssopt);
    if (error != MKL_DSS_SUCCESS)
        return error;
    /* ------------------------------------------- */
   // qDebug()<< "Define the non-zero structure of the matrix */";
    /* ------------------------------------------- */
    int nr=Nr(); int nc = Nc(); int nnz=this->m_hbio.nnzero ;

    //    qDebug()<<" nr= "<<nr<<" nc= "<<nc<<" nnz= "<<nnz<<"\n rowindex=";
    //    for(i=0;i<=nr;i++) qDebug()<<this->m_hbio.colptr [i];
    //    qDebug()<<"colptr=";
    //    for(i=0;i<nnz;i++) qDebug()<<this->m_hbio.rowind[i] << "\t"<< this->m_hbio.values[i];

 //   this->HB_Write("dssInput");
    error = dss_define_structure (m_dsshandle, m_dsssym, this->m_hbio.colptr ,
                                  nr, nc,
                                  this->m_hbio.rowind, nnz  );

    if (error != MKL_DSS_SUCCESS)
          {qDebug()<<"dss_define_structure   err= "<<error;  return error;  }

    /* ------------------ */
   // qDebug()<<"Reorder the matrix */";
    /* ------------------ */
    int opts = m_dssopt;//|  MKL_DSS_AUTO_ORDER;
    error = dss_reorder (m_dsshandle, opts, 0);
    if (error != MKL_DSS_SUCCESS)
    {qDebug()<<" dss_reorder  err= "<<error;  return error;  }

    return error;
}

int RXSparseMatrix1::dss_factor(double*pvalues) // see mkl example dss_unsym_c.c
{
    int error;
    /* ------------------ */
    //  cout<< "Factor the matrix  */"<<endl;
    /* ------------------ */
    error = dss_factor_real (m_dsshandle, m_dsstype,pvalues);
    //  cout <<" factor done. err= "<<error<<endl;
    if (error != MKL_DSS_SUCCESS)
        return error;
    return error;
}

int RXSparseMatrix1::dss_solve(double*pSolValues)  // returns the solution in rhs
{
    int j, error;
    _CHARACTER_t *uplo = "non-transposed";
    _DOUBLE_PRECISION_t *rhs =new _DOUBLE_PRECISION_t[this->Nc() ];
    /* ------------------------ */
    // qDebug()<< "Get the solution vector for Ax=b */";
    /* ------------------------ */
    for (j = 0; j < Nc(); j++)
    {   rhs[j] = pSolValues[j];
        //     qDebug()<< pSolValues[j] ;
    }


    int nRhs=1;
    error = dss_solve_real (m_dsshandle, m_dssopt, rhs, nRhs, pSolValues);
    //  qDebug()<< "dss_solve_real done err= "<<error ;
    if (error != MKL_DSS_SUCCESS)
    {
        for (j = 0; j < Nc(); j++)
            pSolValues[j] = 0.0;
    }
    delete [] rhs;
    return error;
}
int RXSparseMatrix1::dss_end() // see mkl example dss_unsym_c.c

{
    int error;
        if (!m_dsshandle ) return 0;
    /* -------------------------- */
   // qDebug()<< "Deallocate solver storage  */";
    /* -------------------------- */
    error = dss_delete (m_dsshandle, m_dssopt);
    m_dsshandle=0;
    return error;
}
int RXSparseMatrix1::StructureIsSameQ(class RXSparseMatrix1*other )
{
    int   IsSame =  0;
    this->SetStorageType(CSC);
    other->SetStorageType(CSC);
    if(this->Nr ()!= other->Nr()) {qDebug()<< " NR changed"; return 0;}
    if(this->Nc ()!= other->Nc())  {qDebug()<< " NC changed"; return 0;}
    if(this->m_hbio.nnzero != other->m_hbio.nnzero ) {qDebug()<< "nnzero changed"; return 0;}

    if (0 !=memcmp(this->m_hbio.colptr, other->m_hbio.colptr,(this->m_hbio.ncol1+1)* sizeof(int )))
    {qDebug()<< "colptr changed"; return 0;}
    if (0 !=memcmp(this->m_hbio.rowind, other->m_hbio.rowind,(this->m_hbio.nnzero)* sizeof(int )))
    {qDebug()<< "rowind changed"; return 0;}


    IsSame=1;
    return IsSame;
}
QString  RXSparseMatrix1::Statistics(  ) const
{
    QString s;
    map<pair<int,int>,double> ::const_iterator it,l,h;
    int i,j,r,c;
    double tot=0,momsq =0,totsq=0;
    s=QString("matrix:base= %0 nr=%1 nc=%2\n" ).arg(m_b).arg(Nr()).arg(Nc());
    s+=QString( "flags= %1, mxtype=%2 \n").arg(this->m_flags).arg(this->m_hbio.mxtype.c_str());
    if(this->m_flags& RXM_SYMMETRIC) s+= "\t Symmetric \n";
    if(this->m_flags& RXM_RECTANGLE) s+= "\t RECTANGLE \n";
    if(this->m_flags& RXM_SKEW_SYMM) s+= "\t SKEW_SYMM \n";
    if(this->m_flags& RXM_POS_DEF )  s+= "\t POS_DEF \n";
    if(this->m_flags& RXM_INDEFINITE) s+= "\t INDEFINITE \n";
    if(this->m_flags& RXM_COMPLEX)   s+= "\t COMPLEX \n";
    if(this->m_flags& RXM_HERMITIAN) s+= "\t HERMITIAN \n";
    if(this->m_flags& RXM_STRUCTSYM) s+= "\t STRUCTSYM \n";


    if(this->IsFlaggedSymmetric()) s+="\tIsFlaggedSymmetric gives True\n";
    else s+="\tIsFlaggedSymmetric gives False \n";

    switch (this->m_StorageType)
    {
    case CSC:

        s+=QString("\nStorage Type: CSC\n nnzero = %1\n").arg(this->m_hbio.nnzero);
        break;
    case MAPSTYLE:
        s+=QString("\nStorage Type: mapstyle\n nnzero = %1\n").arg(this->m_map.size());
        if (TestForSymmetric(m_map,0.00001)) s+="TestForSymmetric(0.00001) gives True\n";
        else s+="TestForSymmetric(0.00001)  gives False \n";
        l = m_map.begin();
        h = m_map.end();
        for(it=l;it!=h;++it){
            r = it->first.first;
            c = it->first.second;
            momsq += it->second * it->second * (double) (c-r);
            totsq += it->second *it->second;
            tot += it->second;
        }
        s+=QString("sum=%1 , sumsq=%2 , momentsq = %3").arg(tot).arg(totsq).arg(momsq);
        break;

    default:
        s+= "Storage type not defined";
    };

    return s+"\n";
}
