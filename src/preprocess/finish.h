  /* finish.h */
#ifndef _FINISH_H_
#define _FINISH_H_

#include "griddefs.h"

EXTERN_C int Finish_All_Entities(SAIL *p_s);

EXTERN_C int Finish_Batten_Card (RXEntity_p e);
EXTERN_C int Finish_Patch_Card(RXEntity_p e);
EXTERN_C int Finish_Pocket_Card(RXEntity_p e);
//EXTERN_C int Finish_Basecurve_Card(RXEntity_p e);
EXTERN_C int Finish_Compound_Curve_Card (RXEntity_p e);

EXTERN_C int Draw_Cylinder(SAIL *sail,RXCurve *C, double dia);
EXTERN_C int Finish_ISurface_Card (RXEntity_p e) ;

#endif  //#ifndef _FINISH_H_


