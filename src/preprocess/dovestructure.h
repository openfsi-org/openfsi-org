// dovestructure.h: interface for the dovestructure class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGDOVE_H__667429D9_669F_42F3_B0BA_4FBE2B19BF72__INCLUDED_)
#define AFX_AMGDOVE_H__667429D9_669F_42F3_B0BA_4FBE2B19BF72__INCLUDED_


#include "RXGraphicCalls.h"
#include "rxON_Extensions.h"

#include "RXmaterialMatrix.h"
#include "layertst.h"	// Added by ClassView
#include "dovecheckerboard.h"
#include "RXMathlink.h"
#ifdef DOVETEST
#include "AMGTape.h"	// Added by ClassView
#else
class AMGTape ;
#endif
#include "AMGLevel.h"

class  AMGTapingZone;
class AMGZoneEdge;
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "layerobject.h"
#include "opennurbs_string.h"	// Added by ClassView
#include "rlx3dmEntities.h"
#include "dovtemptest.h" 

class AMGZoneCycle;
class rxONX_Model;	
class  RXON_NurbsSurface;

//AlphaFromRefDir returns
#define RX_DOVE_NO_PLY  		0  // must BE ZERO 	
#define RX_DOVE_OFF_EDGE		2
#define RX_DOVE_OK      		1 // musT BE NON-ZERO
#define RX_DOVE_TOBOUNDARY 		4 // musT BE NON-ZERO
#define RX_DOVE_IS_BEINGTAPED 	8 // musT BE NON-ZERO

#define NOTRIMTOEDGE			16
#define TRIMTOEDGE				32

#define RX_DOVE_THK				64
#define RX_DOVE_NU				128
#define RX_DOVE_KE				256
#define RX_DOVE_KG				512
#define RX_DOVE_SA				1024
#define RX_DOVE_CA				2048
#define RX_DOVE_ANGLE			(RX_DOVE_SA|RX_DOVE_CA)
#define RX_DOVE_ALL			(RX_DOVE_THK|RX_DOVE_NU	|RX_DOVE_KE	|RX_DOVE_KG	|RX_DOVE_ANGLE)

// return values for Rrkodeint
#define RK_OK				0
#define RK_NO_DERIVS		1 
#define RK_NO_RKQS			2
#define RK_SMALLSTEP		4
#define RK_TOOMANYSTEPS		8

//  control flags
#define RX_DOVE_USE_BLUE_MATRIX 		4096
//									8192
#define RX_DOVE_GLOBAL_X_REF		16384
#define RX_DOVE_Y_IS_DV     		32768
#ifndef TODO
#define TODO assert(0);
#endif
// for navigating the surfaces eg in ShouldDrawCV
#define RX_DOVE_N				1
#define RX_DOVE_S				2
#define RX_DOVE_E				4
#define RX_DOVE_W				8
#define RX_DOVE_NE				16
#define RX_DOVE_NW				32
#define RX_DOVE_SE				64
#define RX_DOVE_SW				128

class AMGSolidDoveView;
class CDoveHoopsView;
class CAmgui3dView;
class CAmguiDoc;


class dovestructure  : public layerobject 
{
#ifndef linux
	friend AMGSolidDoveView;
	friend CDoveHoopsView;
	friend CAmgui3dView;
	friend CAmguiDoc;
#endif
public:
virtual	int PlyStrains(const double p_u, const double p_v,
							 double p_eps[3],
							 double &plyminX,double &plymaxX,
							 double &plyminY,double &plymaxY,
							 double &plymaxXY);
virtual int PrintLayerStrain(FILE *fp, const double p_u, const double p_v, double p_eps[3]);

	int NoOfOnAxisPlies(const double thk);
	int DrawDirection(const int p_nbU,const int p_nbV);
	int RedrawFromON(void);
	int ImportAsTable(const char *fname);
	int SaveAsTable(const char *fname);
	int ShouldDrawCV(const int p_flags,const ON_2dPoint &p_uv,const ON_3dPoint &p_xyz  );
	int GetStackType();
	

	
	int GetEditableAngleHoopsSurfs(HC_KEY *p_pSinSurface,HC_KEY *p_pCosSurface);
	int GetEditableAngleONSurfs(RXON_NurbsSurface **p_pSinSurface,RXON_NurbsSurface **p_pCosSurface);
	virtual double GetAlpha(const double u, const double v);
	dovestructure* ReParameterize(const int nr, const int nc, const ON_Surface *p_newmould);
	int Set_SurfacePtrs();
	void SetStackType(const int type);
	virtual double LayerThickness(const AMGLevel  level, const ON_2dPoint &ptUV);

	//TR 4 may 07 // to be able to directly set the CV of the thickness map
	// p_i : CV index along u
	// p_j : CV index along v
	// p_Z : Height of the CV
	// return 1 if success else 0
	int SetThkCV(int p_i, int p_j, double p_Z);

	int SetAngleCV(int p_i, double a);

	int GradThickness (double u,double v, ON_3dVector &p_g);

	
	void SetDovefile (	ON_String p) {m_dovefile=p ;}
	ON_String GetDovefileType() const; 
		
        int SetRootKey(RXGRAPHICSEGMENT p_key) {m_rootkey = p_key; return 1;}

	int IsCloseToAPole(ON_3dPoint q, const AMGLevel level); // in world coordinates.
	int IsCloseToAPole(ON_Line a, const AMGLevel level);
	int IsCloseToAPole(ON_Curve *a, const AMGLevel level);
	int TrimToPanel(const ON_Surface *s=0);  // the result in normalized UV space. It's 3d so we can use it for a plinecurve) ;
	void SetPanel(AMGZoneCycle *);
	HC_KEY TraceAllLayers(const int fast=0, 
						  const double & p_tol = 1.0E-6,
						  const int & p_nr = 37, 
						  const int & p_nc = 10);

	rxONX_Model *Get_ONX_Model() { if(m_p3dmM) return m_p3dmM->m_pONXModel ; if(g_Last3dmModel )return g_Last3dmModel->m_pONXModel; return 0; }// The model

	ON_SimpleArray<double> * GetRKXResult() { return &(m_rkxp); }
	ON_3dPointArray * GetRKUVResult() { return &(m_rkyp);}

// traceLayer returns a ptr to a new ON_CurveOnSurface.
	virtual ON_Curve *TraceLayer(const double u0,const double v0,  // initial coords.  In normalized UV space
		const AMGLevel level, 
		const double maxlenXYZ,			// trace will stop at this length.
		const ON_3dVector &p_startDirXYZ ,// the trace will start approximately in this direction.
		const double eps,			// an accurate trace needs about 1e-9 =5 tapes/sec
		const int p_TraceFlag 

		);

virtual		AMGTape *GetOneTapeGeometry(
				ON_Curve *p_ptheC,
				double t,
				bool LorRonCurve,
				double alphaerror,
				double MaxLateralOverlap,
				double minLength,
				double maxlength,
				double tapewidth,
				const AMGLevel level,
				AMGZoneEdge *p_e);

		AMGTape *GetNextTapeGeometry(
				ON_Curve *p_ptheCurve,
				const double alphaerror,
				const double MaxLateralOverlap,
				const double minLength,
				const double maxlength,
				const double tapewidth,
				const AMGLevel level,
				AMGTape *p_pLastTape,
				const bool LorRonTape,
				const bool p_TapeIsReversed,
				AMGZoneEdge *p_e=0);

		AMGTape *complicatedGetNextTapeGeometry(
				ON_Curve *p_ptheCurve,
				double alphaerror,
				double MaxLateralOverlap,
				double minLength,
				double maxlength,
				double tapewidth,
				int level,
				AMGTape *p_pLastTape,
				const bool LorRonTape);

AMGTape * GetNextTapeGeometryByBrent(
				ON_Curve *p_ptheCurve,
				const double alphaerror,
				const double MaxLateralOverlap,
				const double p_MinLength,
				const double p_MaxLengthXYZ,
				const double tapewidth,
				const AMGLevel level,
				AMGTape *p_pLastTape,
				const bool IsRightOnTape,
				const bool p_TapeIsReversed,
				AMGZoneEdge *p_e);

AMGTape *GetOneTapeGeometry(
				ON_3dPoint	&p_startXYZ,
				ON_3dVector &p_startVectorXYZ,
				double alphaerror,
				double MaxLateralOverlap,
				double p_MinLength,
				double p_MaxlengthXYZ,
				double tapewidth,
				const AMGLevel level);

	virtual int Print(FILE *fp) ;
	virtual	int FibreTangentXYspace(const double u, const double v, const AMGLevel level, ON_3dVector &p_vec);
	virtual	int FibreTangentXYspace(const ON_3dPoint &p_global, const AMGLevel level, ON_3dVector &p_vec);
	ON_Curve *StartingCurve(const ON_3dPoint p0,const int level);
	virtual int HowManyLayers(const int flag);
	ON_3dPointArray *TraceAlpha(double u0, double v0 , const AMGLevel level,const int GoRight=0,const int maxSteps=1000);
	ON_3dPointArray * TracePerpendicularToAlpha(double u0, double v0, const AMGLevel level,const int GoRight=0,const int maxSteps=1000);

	int Save(const char*p_filename=0);
	HC_KEY GetRootKey();
	const ON_Surface *GetMould() { return m_mould;}
	virtual void SetMould(const ON_Surface * p); // Now sets the domain to (0-1)
	int TranslateSixSurfacesToON();
	int Empty();
	double PrincDir(const double u, const double v);
	RXmaterialMatrix * GetPlyMatrix();
	void SetPlyMatrix(RXmaterialMatrix m);
	dovestructure();
	dovestructure(const char *p_dovefile);
	virtual ~dovestructure();

	ON_2dVector GetReferenceDirection2D(const double u, const double v) ;
	ON_3dVector GetReferenceDirection3D(const double u, const double v ,const ON_Surface *p_mould =0);
	layertst * Get_LT();
	int Initialize(const char *lp =0);	// lp is the path to the mathematica executable.
	int Calculate();
	int EditParameters();
	int AbortCalculation();

/*  
 calls AlphaFromRefDir(u, v, level, &a,0) on each layer. 
It sums  m_plyMatrix, rotated by alpha, into the result.
so the reference direction is the same as for AlphaFromRefDir.

*/
virtual	int DD_From_PliesWRTRefDir(const double u, const double v, 
		ON_Matrix&p_m,
		ON_2dVector *pref2d=0, // the reference vector in U,V space.
		ON_3dVector *pref3d=0);


virtual	int AlphaFromRefDir(
		const double u, // in domain 0 to 1
		const double v, // in domain 0 to 1
		const AMGLevel level,// lowest is level 0; If the level doesnt exist, the fn returns 0;
		double *a,double *wfactor,	// the angle of this ply in radians from the ref vector.
		ON_2dVector *pref2d=0, // the reference vector in U,V space.
		ON_3dVector *pref3d=0// the reference vector in cartesian space, calculated from ON_Surface *m_mould
);
virtual int GradAlpha(
		const double u, // in domain 0 to 1
		const double v, // in domain 0 to 1
		const AMGLevel level,// lowest is level 0; If the level doesnt exist, the fn returns 0;
		ON_2dVector *gradAlpha,// the directional derivative {da/du,da/dv}
		int p_DALPHADUV = 0  // set to true to take into account the dimensions of the ALPHA surface
		);

	virtual int Stack(const double pu, const double pv, 
				ON_ClassArray<layernodestack> &p_TheStack, double *alpha0 =0);

	int StackCheck(const double p_i, const double p_j );
	HC_KEY VerifyStack(const char *filename);

//	static int Stack(const double alpha,const double thk, const double ks,const double ky,
//				  ON_ClassArray<layernodestack> &p_TheStack,
//				  int &p_OffsetInStack);

	RXmaterialMatrix DDFromDoveSurfaces(const double pu, const double pv);

	int GetContours(ON_ClassArray<ON_Curve> & p_p);
	virtual	ON_String Serialize();
	virtual	char * Deserialize(char*lp);

	dovestructure *amgdoveFromONSurfaces( 
						const RXON_NurbsSurface *p_sa,  // angle sine
						const RXON_NurbsSurface *p_ca,  // angle cosine
						const RXON_NurbsSurface *p_thk,	//thickness in number of plies
						const RXON_NurbsSurface *p_ke,	// eyy by exx 
						const RXON_NurbsSurface *p_kg,	// gxy by exx
						const RXON_NurbsSurface *p_nu,	// nu
						const HC_KEY rootkey , 	// default value 0. If non-null we draw into it	
						const int UseMathematica ,
						const RXON_NurbsSurface *p_uvsurf
						);


	const RXON_NurbsSurface * Getthk() {return m_thk;};
	const RXON_NurbsSurface * Getnu() {return m_nu;};
	const RXON_NurbsSurface * Getke() {return m_ke;};
	const RXON_NurbsSurface * Getkg() {return m_kg;};
	const RXON_NurbsSurface * Getuv() {return  m_uv;};

	void Setthk(const RXON_NurbsSurface *p_s ) { m_thk=p_s;};
	void Setnu (const RXON_NurbsSurface *p_s ) { m_nu =p_s;};
	void Setke (const RXON_NurbsSurface *p_s ) { m_ke =p_s;};
	void Setkg (const RXON_NurbsSurface *p_s ) { m_kg =p_s;};
	
	//TR 4June 07
	void SetCosine(RXON_NurbsSurface *p_s ) { m_aca.Empty(); m_aca.AppendNew()  =p_s;};
	void SetSine  (RXON_NurbsSurface *p_s ) { m_asa.Empty();  m_asa.AppendNew() =p_s;};
	//end TR 4 June 07
	void Setuv (const RXON_NurbsSurface *p_s ) { m_uv =p_s;};
private:

	dovecheckerboard * m_checker;	// an ephemeral object used for displaying the fibre curves.

	int m_nrhs;   // counts function evaluations in Runge Kutta method

	int Initialize_Mathematica(const char *p_mathpath);

layertst m_LayerStruct; // this is in layerobject.h

protected:

	virtual double TransverseAngleDerivative(const double pu,const double pv);
	virtual double GetTapeWidth(); 
	virtual int CalcBlueOffAxisProps( double pu,double pv, double &thk, double &nu, double &ks,double &ky);
	virtual int CalcBlueOffAxisMatrix( double pu,double pv,double thk,ON_Matrix&p_m);
	int WalkTillTurnedThrough(const ON_Curve *p_c, double &p_angle, double &p_t, double &p_l);
	double TypicalDimension();
	int m_StackType;
	enum StackDSubtype m_doveStacksubtype;
	const RXON_NurbsSurface * m_thk,*m_nu,*m_ke,*m_kg, *m_uv; //, *m_sa,*m_ca;
	
	ON_ClassArray<RXON_NurbsSurface *> m_aca, m_asa;

        RXGRAPHICOBJECT  m_Kthk,m_Knu,m_Kke,m_Kkg,m_Ksa,m_Kca,m_Kuv;

	virtual int DAlphaByDx(const ON_3dPoint &q,  // use Q if the dove type allows, else uv
		const ON_2dPoint &uv,
		const AMGLevel level, ON_3dVector *dadx);
	int TrimUVCurveToMould();

	HC_KEY TraceOneLayer(const AMGLevel level,const int NR=37,const int NC=10,const double TOL=2.5e-6);

	struct PC_3DM_Model * m_p3dmM;

	int TapeLengthLimits(const ON_3dVector GradA, 
							const ON_3dVector FibreVec, 
							const double anglelimit,  // in radians!!!!
							const double tapewidth,  
							const double dw,  // same units as tapewidth.
							double*p_bow,
							double*p_div)const ;

	int SetTapeLength(const double L0,
						const double L1,
						const double L2,
						const double L3,
						const double L4,
						double*result) const;


	int GetMouldOrthoTrigraph(const double u,const double v, ON_Xform *p_xf,ON_2dVector *p_uv=0);
	HC_KEY MapToMould(HC_KEY surfacekey);
	const ON_Surface *m_mould;
	ON_String SerializeHoopsTree();
	char* DeserializeHoopsTree(char*lp);

        RXGRAPHICSEGMENT m_rootkey;
	ON_String m_dovefile;
	RXmaterialMatrix m_plyMatrix;
	ON_String m_descriptivetext;

	int rkodeint(ON_2dPoint &ystart, const double x1, const double x2, const double eps,
	const double h1, const double hmin, int &nok, int &nbad	, const AMGLevel level,const ON_3dVector &p_startDirXYZ,
	const int p_TraceFlag);


	int rkCk(ON_2dVector &y, ON_2dVector &dydx, const double x,
		const double h, ON_2dVector &yout, ON_2dVector &yerr, const AMGLevel level, ON_3dVector &startDirXYZ);

	int rkQs(ON_2dVector &y, ON_2dVector &dydx, double &x, const double htry,
		const double eps, ON_2dVector &yscal, double &hdid, double &hnext,const AMGLevel level, ON_3dVector &p_startDirXYZ);

	int rkDerivs(const double x, ON_2dVector &y, ON_2dVector &dydx,const AMGLevel level,ON_3dVector &p_startDirXYZ);

// Runge Kutta results	
		ON_SimpleArray<double> m_rkxp;
        ON_Polyline m_rkyp;  // the result in normalized UV space.
		double m_DxSaved;  // defining declarations
 
		AMGZoneCycle *m_zc;

		char *DeSerializeAttributes(char*p_lp);
		ON_String SerializeAttributes(void);

public:
	int ZeroAngles();
	// Returns the flags, stacktype, etc formatted 
	ON_String InfoString(void);
};

#ifndef AMGUI_2005
	EXTERN_C int amgdoveDeserializeTest (const char*p_filename );
	EXTERN_C int amgdoveTapingTest (const char*p_filename );
	EXTERN_C int amgdoveSurfacetest(const char*p_file );
#endif
//extern dovestructure *g_theDove;

#endif // !defined(AFX_AMGDOVE_H__667429D9_669F_42F3_B0BA_4FBE2B19BF72__INCLUDED_)
