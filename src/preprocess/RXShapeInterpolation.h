#pragma once
#include "RXInterpolationI.h"
class rxON_Mesh;
class RX_FESite;
class RX_FEedge;
/*

Here we define subclasses of RXInterpolationI .
RXShapeInterpolation works from a vector of pointers-to-points and is designed for use with Relax
so it overrides the point definition of RXInterpolationI

RXShapeInterpolationG works from the model's finite element triangulation. It doesnt use Shewchuk at all and so it is
independent of 3d orientation.
Its purpose is to create the UV (ie texture) coordinates


RXShapeInterpolationP works from a vector of points and is free-standing
it uses the point vector definition inherited from RXInterpolationI
*/
// note  just has constructors, destructor, ValueAt and Dump
class RXShapeInterpolationI :
        public RXInterpolationI
{
public:
        RXShapeInterpolationI(class RXSail *s, vector<RX_FESite*> &p,const vector<RX_FEedge*> &e );
        RXShapeInterpolationI(class RXSail *s);
        virtual ~RXShapeInterpolationI(void);

        virtual rxON_Mesh* MakeONMeshXYZ()=0;
        virtual rxON_Mesh* MakeONMeshUV()=0;
        virtual int Prepare(const double lambda, const int flag)=0;  // lambda was .75, flag was 0
        virtual ON_3dPoint CoordsAt(const ON_2dPoint& p,const RX_COORDSPACE space)=0;

// the Pt override is needed because the base class has vector<RXSitePt> pts;
// but here we use (and I dont know why) RX_FESite instead.

        virtual class RX_FESite& Pt(const int n)=0;

//        RXEntity_p m_unitSquareToQuad; // now retired
//        RXEntity_p m_QuadToUnitSquare; // used in ToUnitSquare only

};
 
class RXShapeInterpolation :
        public RXShapeInterpolationI
{

public:
        RXShapeInterpolation(class RXSail *s, vector<RX_FESite*> &p,const vector<RX_FEedge*> &e );
        virtual ~RXShapeInterpolation(void);
        int Prepare(const double lambda, const int flag); // lambda was .75, flag was 0
        ON_3dPoint CoordsAt(const ON_2dPoint& p,const RX_COORDSPACE space);

 protected:
        int Triangulate(const int flag=0);

	int Flatten(const double lambda=0.75);
    class RX_FESite& Pt(const int n);
	vector<RX_FESite*>& Points(){ return pts;}
	const vector<RX_FEedge*>& Edges(){return Fedges;}
	rxON_Mesh* MakeONMeshXYZ(){return msh.MakeONMeshXYZ(pts);}
	rxON_Mesh* MakeONMeshUV(){return msh.MakeONMeshUV(pts);}

	int DrawHoops(rxONX_Model*mod, const int n);
	class RXShapeInterpolation *Invert();
        int Dump(const char* fname);

protected:
        double _ValueAt(const ON_2dPoint& p);
        int _ValueAt(const ON_2dPoint& p, double*rv) ;
        vector<RX_FESite*> &pts;
        const vector<RX_FEedge*> &Fedges;
        int ToUnitSquare();
};
 
class RXShapeInterpolationP :  // used in read_nd_state
        public RXInterpolationI
{

public:
        RXShapeInterpolationP(class RXSail *s);
	int Read(const RXSTRING &filename, const int skiplines, const int ndata);

	virtual ~RXShapeInterpolationP(void);
        int Dump(const char* fname);
        int Prepare(const double lambda, const int flag); // lambda was .75, flag was 0

        int Triangulate(const int flag=0);
        double _ValueAt(const ON_2dPoint& p);
	vector<RXSitePtWithData*>& Points(){ return pts;}
	int NPoints(){ return pts.size();}
        rxON_Mesh* MakeONMeshXYZ(){assert(0); return 0;}
        rxON_Mesh* MakeONMeshUV(){assert(0); return 0;}
        int dataSize()const{return m_ndata;}

protected:
        int _ValueAt(const ON_2dPoint& p, double*r);
private:
	vector<RXSitePtWithData*> pts;
	int m_ndata;
};
