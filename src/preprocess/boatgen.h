#ifndef _HDR_BOATGEN_
  #define _HDR_BOATGEN_ 1

struct BOATGEN {
	double I,J,EM,P,Rake_Ipt,BAD,Mast_Freeboard;
	double  Mast_Size_Shear,Mast_Size_Ipt,Overlap,Sheeting_Base;
	double Bow_Freeboard, HLength;
	double BG,BH, profile_length,profile_width;

	double Track,Clew_Height,Main_Clew_Height,Tube_Length;
	double LOA,Beam,EA,MClew_X, EA_Cun;
	int   Port_Tack, Custom_Jibsheet;
};

EXTERN_C int Process_Boat_Card(SAIL *sail, char *line, struct BOATGEN *bg);
EXTERN_C int Create_Boat_Script(SAIL *const sail,const char **names ,const char **data ,const char *fileroot, struct BOATGEN *bg);
EXTERN_C int Generate_Boat_Script(SAIL *const sail,char *data[] , FILE *f, struct BOATGEN *bg);
EXTERN_C int PBS_Write_Boat_Script(FILE *f, struct BOATGEN *bg);

#endif  //#ifndef _HDR_BOATGEN_


