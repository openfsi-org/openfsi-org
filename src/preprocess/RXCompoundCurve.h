#ifndef _RXCOMPOUNDCRV_HDR_
#define   _RXCOMPOUNDCRV_HDR_

#include "RXSeamcurve.h"



class RXCompoundCurve : public  RXSeamcurve
{
public:

	RXCompoundCurve();
	RXCompoundCurve(class RXSail *p );
		int CClear();
		virtual int Dump(FILE *fp) const;
		virtual int Compute(void);
		virtual int Resolve(void);
		virtual int ReWriteLine(void);
		virtual int Finish(void);


	~RXCompoundCurve();
	int Init();
	int Resolve (const RXEntity_p p_bce=0,const RXEntity_p p_p1=0,const RXEntity_p p_p2=0) ;

	int Delete_Seamcurve(void ); // really a Clear
	int Compute_Compound_Curve();
	double GetArc(const int &k,const double fractional_tolerance=1.0e-8) const; // p->GetArc(0)
protected:
    int FlagSet(const char *att); // as SC_FlagSet(p, att);



public:
	sc_ptr *m_ccCurves;	   /*  	  curves owned */
	int *m_ccRevflags;					/* flag for sense of curve */
	RXEntity_p *m_ccE1sites;	 /* end1,end2 of each curve */
	RXEntity_p *m_ccE2sites;
	int m_ccNcurves;
};
#endif
