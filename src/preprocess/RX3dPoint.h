// RX3dPoint.h: interface for the RXObject class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RX3DPOINT_H__ABB3CEE2_47AF_454F_BA84_FE3393E3C12A__INCLUDED_)
#define AFX_RX3DPOINT_H__ABB3CEE2_47AF_454F_BA84_FE3393E3C12A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define RX3DPOINT_OK 1

#include "RXObject.h"
 /////////////////////////////////////////////////////////////////
  //
  // Any object derived from ON_Object should have a
  //   ON_OBJECT_DECLARE(ON_...);
  // as the last line of its class definition and a
  //   ON_OBJECT_IMPLEMENT( ON_..., ON_baseclass );
  // in a .cpp file.
  //
  // These macros declare and implement public members
  //
  // static ON_ClassId m_ON_Object;
  // static cls * Cast( ON_Object* );
  // static const cls * Cast( const ON_Object* );
  // virtual const ON_ClassId* ClassId() const;

#ifdef __cplusplus

#ifndef RX3DPOINTCLASSID //17B1D057 A3BF 43CF 849B 6C0477A154E9
#define RX3DPOINTCLASSID "17B1D057-A3BF-43CF-849B-6C0477A154E9"
#endif

class RX3dPoint //: public RXObject 
{
public:
	RX3dPoint();
	RX3dPoint(const RX3dPoint& p_Obj);
	RX3dPoint(double * p_x,double * p_y,double * p_z);

	virtual ~	RX3dPoint();
	void Init();
	void Clear();
	RX3dPoint& operator = (const RX3dPoint& p_Obj);
	
	virtual char * GetClassID() const; 
	virtual int IsKindOf(const char * p_ClassID) const;

	static RX3dPoint* Cast( RX3dPoint* p_pObj); 
	static const RX3dPoint* Cast( const RX3dPoint* p_pObj);

	const double * operator[](int i) const;
	int Set(int i, double * p_V);
	int Set(const ON_3dPoint & p_pt);
	
	ON_3dPoint operator*( double d ) const;
	ON_3dPoint operator/( double d ) const;
	ON_3dPoint operator+( const ON_3dPoint& p ) const;
	ON_3dPoint operator+( const ON_3dVector& v ) const;
	ON_3dVector operator-( const ON_3dPoint& p ) const;
	ON_3dPoint operator-( const ON_3dVector& v ) const;
	
	ON_3dPoint  operator-( const RX3dPoint& v ) const;

	double operator*(const ON_3dPoint& h) const;
	double operator*(const ON_3dVector& h) const;
	double operator*(const ON_4dPoint& h) const;
	bool operator==( const ON_3dPoint& p ) const;
	bool operator!=( const ON_3dPoint& p ) const;

protected:
	const double * m_x;  
	const double * m_y;
	const double * m_z;
};

#endif //#ifdef __cplusplus

#endif // !defined(AFX_RXOBJECT_H__ABB3CEE2_47AF_454F_BA84_FE3393E3C12A__INCLUDED_)
