/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 august 1996. Only compute batten if Needs flag is set
 april 1996 DELL zerooff, hundredoff defined
 march 1996  allow cross relsites
 *   5/12/95  Finish SC BorGCurveptr
 *    26/7/95		Finish_Seamcurve can cope with NULL p3 (ie RLX curve)
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"
#include <QString>
#include "rxsubwindowhelper.h"
#include "RXSail.h"
#include "RXEntityDefault.h"
#include "RXSRelSite.h"
#include "RXSeamcurve.h"
#include "RXCompoundCurve.h"
#include "RX_FEPocket.h"
#include "RXExpression.h"
#include "RXOffset.h"
#include "RXRelationDefs.h"
#include "rxbcurve.h"
#include "rxqtdialogs.h"
#include "global_declarations.h"
#include "batdefs.h"
#include "batpatch.h"
#include "panel.h"
#include "etypes.h"
#include "interpln.h"
#include "oncut.h"
using namespace oncut;

#include "finish.h"


int Finish_ISurface_Card (RXEntity_p e) {	
    e->SetNeedsComputing();

    return( Compute_Interpolation_Card(e));
}	


int Finish_All_Entities(SAIL *sail) {
    int iret;
    int retval;
    int nc;	  /* number of changes this loop */
    int Changing;
    RXEntity_p e=0;
    nc = 0;

    /* the effect of the 'do while changing' loop is to allow order-independence
    but because the Finish routines require their p-rents to be Finished already,
  this will NOT Finish circularly-defined models
  To do this it would be necessary to force items to FINISH regardless of the
  state of their p-rents.
  */

    try{ // return 0 if we throw an exception.
// Finish all submodels
        vector<RXEntity* > smlist;
        vector<RXEntity* >::iterator it;
        sail->MapExtractList(PCE_SUBMODEL, smlist,PCE_ISRESOLVED) ;
        for (it = smlist.begin (); it != smlist.end(); it++)  {
            (*it)->Finish();  (*it)->Needs_Finishing=0;
         }

//  find all CUT
        if(!g_Janet)  {
           setStatusBar (QString("Intersections: ") + sail->GetQType(),sail->Client());
            ONC_Find_3dm_Cuts(sail);
            setStatusBar (QString("2nd pass: ") + sail->GetQType(),sail->Client());
        }
        do{
            retval=1;
            iret=1;
            Changing=0;
            // first finish all Isurfs, then the rest
            ent_citer it;
            for (it = sail->MapStart(); it != sail->MapEnd(); it=sail->MapIncrement(it))  {
                e  =   it->second;
                if (e->Needs_Finishing) {
                    if(e->TYPE==INTERPOLATION_SURFACE) {
                        iret=Finish_ISurface_Card(e);

                        if(iret==0) {
                            retval=0;  /* some failed */
                            e->Needs_Finishing=1;
                        }
                        else {
                            e->Needs_Finishing=0;
                            nc++;Changing=1;
                        }
                    }
                }
            }
            // now the basecurves.  Some may be moulded

            for (it = sail->MapStart(); it != sail->MapEnd();it=sail->MapIncrement(it)) {
                e  = dynamic_cast<RXEntity_p>( it->second);
                if (e->Needs_Finishing &&e->TYPE ==BASECURVE ) {
                    iret= e->Finish();
                    if(iret==0) {
                        retval=0;  /* some failed */
                        e->Needs_Finishing=1;
                    }
                    else {
                        e->Needs_Finishing=0;
                        nc++;Changing=1;
                    }
                }
            }

            // now the rest

            for (it = sail->MapStart(); it != sail->MapEnd(); it=sail->MapIncrement(it))  {
                e  = dynamic_cast<RXEntity_p>( it->second);

                if (e->Needs_Finishing) {
                    switch (e->TYPE) {
                    case SEAMCURVE:
                    case RELSITE:
                    case SITE:
                    case PCE_BEAMMATERIAL:
                    case PCE_FIXITY:
                    case TWODCURVE:
                    case BASECURVE :
                    case THREEDCURVE:
                    case PCE_SPLINE:
                    case GAUSSCURVE:
                    case PCE_NODECURVE:
                    case PCE_SUBMODEL:
                    case PCE_STRING:
                    case PCE_BEAMSTRING:
                    {
                        iret=e->Finish();// dont forget - SC may be PCE_DELETED
                        break;
                    }
                    case BATTEN : {
                        iret=Finish_Batten_Card(e);
                        break;
                    }

                    case PATCH: {
                        iret=Finish_Patch_Card(e);
                        break;
                    }
                    case POCKET: {
                        iret=Finish_Pocket_Card(e);
                        break;
                    }
                    case COMPOUND_CURVE: {
                        iret=Finish_Compound_Curve_Card(e);
                        break;
                    }
                    default:
                        ;
                    } //switch
                    if(iret==0) {
                        retval=0;  /* some failed */
                        e->Needs_Finishing=1;
                    }
                    else {
                        e->Needs_Finishing=0;
                        nc++;Changing=1;
                    }
                } // is needs it
            }
        }
        while(Changing);

    } //end try
    catch (RXException &ex){
        ex.Print(stdout);
        return 0;
    }
    catch( char * str ) {
        printf( "Exception raised:  <%s>\n",str);
        return 0;
    }
    catch( unsigned int ex ) {
        printf( "Exception raised: no= %d\n",(int)ex);
        return 0;
    }
#ifdef MICROSOFT
    catch(CException *ex) {
        wchar_t bufff[1024];
        ex->GetErrorMessage(bufff,1024);
        cout<< "Cexception in Finish " << bufff << endl;
        return 0;
    }
#endif
    catch( MTException &ex)
    {

        MTSTRING msg = _T("(FinishAll)  Parser error: ") + e->GetOName() +_T("\n");
        msg += ex.m_description.c_str();
        cout <<ToUtf8(msg ).c_str()<<endl;
        return 0;
    }
    catch(...) {
        cout<< "(FinishAll) otherexception "<<endl;
        return 0;
    }

    return(retval);
}

int Finish_Patch_Card(RXEntity_p e)	{
    struct PC_PATCH *p = (PC_PATCH *)e->dataptr;

    RXOffset*dummy=NULL;
    if (p->basecurveptr) {	/* will beed geometry-compute */

        ON_3dPoint e1,e2;
        if(!Get_Position(p->e[0],dummy,1,&e1))return(0);
        if(!Get_Position(p->e[1],dummy,1,&e2))return(0);

        p->m_arc =  e1.DistanceTo (e2);
        if(-1==Compute_Patch_Geometry(e)) {
            return(0);	 /* means 	e->Needs_Finishing=1; */
        }

        p->basecurveptr->SetRelationOf(e,child,RXO_BC_OF_PATCH);; // are they not already there?
        p->e[0]->SetRelationOf(e,child,RXO_N1_OF_PATCH);
        p->e[1]->SetRelationOf(e,child,RXO_N2_OF_PATCH);;

    }

    return(1);
}


int Finish_Pocket_Card(RXEntity_p e)	{
    class RX_FEPocket *p = (class RX_FEPocket *)e; assert(p==dynamic_cast<class RX_FEPocket *>(e)  );

    RXOffset*dummy=NULL;
    if (p->basecurveptr) {	/* will beed geometry-compute */

        ON_3dPoint e1,e2;
        if(!Get_Position(p->e[0],dummy,1,&e1))return(0);
        if(!Get_Position(p->e[1],dummy,1,&e2))return(0);

        p->m_arc = e1.DistanceTo (e2);
        if(-1== p->Compute_Pocket_Geometry()) {
            return(0);	 /* means 	e->Needs_Finishing=1; */
        }

        p->basecurveptr->SetRelationOf(e,child,RXO_BC_OF_POCKET);  // are they not already there?
        p->e[0]->SetRelationOf(e,child,RXO_N1_OF_POCKET );
        p->e[1]->SetRelationOf(e,child,RXO_N2_OF_POCKET );

    }

    return(1);
}

int Finish_Batten_Card (RXEntity_p e) {	
    return(Compute_Batten(e));
}	

int Finish_Compound_Curve_Card (RXEntity_p e) {

    /* Given curve list gets arc length      */
    class RXCompoundCurve *  p;
    int i;

    p = (class RXCompoundCurve *)e; assert(p==dynamic_cast<class RXCompoundCurve * >(e));
    p->m_arcs[1] =(float) 0.0;

    for (i=0;i<p->m_ccNcurves;i++) {
        sc_ptr  sc;
        RXEntity_p cc = p->m_ccCurves[i];
        if(cc->Needs_Finishing)
            return(0);
        sc = (sc_ptr  )cc;
        p->m_arcs[1] = p->GetArc(1) + sc->GetArc(1);
    }

    return(1);

}


//int Finish_Basecurve_Card(RXEntity_p e) {

//  if(!Compute_Basecurve(e)) return(1);	  /* Compute Basecurve returns 0 if OK 	 */
//  return(1);
//  }
