#pragma once
#include <string>
#include <map>
#include <vector>

 
using namespace std;

// values for m_MatrixType, 
enum  MatrixType{RSS=1,RSPD=2,RSI=-2,RU=11,MTNONE=0 };
class RXHarwellBoeingIO
{
    friend class RXSparseMatrix;
public:
	RXHarwellBoeingIO( );
	RXHarwellBoeingIO(map<pair<int,int>,double> ,const char*p_title, const char*p_key,const MatrixType t);
	~RXHarwellBoeingIO(void);
	int Write(const char*filename);
	int Read(const char*filename);
	int Init();
	void Fill(map<pair<int,int>,double> m,const char*p_title, const char*p_key );
	int  SetRHS(vector<double> p_rhs);
	void SetMatrixType(const MatrixType t){ m_MatType =t;}
    int SolveSym (double* rhs, double* result);	// copy of intel pardiso_sym_c
    int solveunsym(double* result);  // copy of  pardiso_unsym_c.cpp
protected:
	int SetupForWrite();
// dream on
    int IsSymmetric(){return 0;}
    int IsSymmetricStructure(){return 0;}
    int IsPositiveDefinite(){return 0;}
    int IsSquare(){return (this->nrow==this->ncol);}
public:
	double *rhsval;
	int nrow; 
	int ncol;
	int *colptr;
	int *rowind;
	double *values;

protected:
	enum  MatrixType m_MatType;
	int m_nRhsVs;
    std::string title;
    std::string  key;
	char* ptrfmt;
	char* indfmt;
	char* valfmt;
	char* rhsfmt;
	char* rhstyp;
	int totcrd;
	int ptrcrd;
	int indcrd;
	int valcrd;
	int rhscrd;
    std::string  mxtype;

	int nnzero;
	int neltvl;
	int nrhsix;

	int *rhsptr;
	int *rhsind;
	double *rhsvec;
	double *guess;
	double *exact ;

	string Cintfmt,cfltfmt;

public:
	static pair<string,string> MakeFormats(const int n,const int lw, int&nlines);
    static  int Test(const char*filename);
};
