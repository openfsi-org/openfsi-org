#ifndef _GETFILENAME_H_
#define  _GETFILENAME_H_

#include "files.h"  // for getfilename

#ifdef _X
//static    monarch doesnt like static
void  okCB(Widget w,	
				 XtPointer client_data,
				 XtPointer call_data);
//static 
void cancelCB(Widget w,		/*  widget id		*/
					 XtPointer client_data,	/*  data from application   */
					 XtPointer call_data);	/*  data from widget class  */
#endif //#ifdef _X

#endif//#ifndef _GETFILENAME_H_

