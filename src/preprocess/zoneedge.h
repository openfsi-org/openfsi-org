// zoneedge.h: interface for the zoneedge class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ZONECYCLE_H__1A1B81FF_552F_4FEA_B939_3FE09113E0CB__INCLUDED_)
#define AFX_ZONECYCLE_H__1A1B81FF_552F_4FEA_B939_3FE09113E0CB__INCLUDED_

#include "opennurbs.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class zoneedge 
{
public:
	zoneedge();
	virtual ~zoneedge();

	static ON_SimpleArray<zoneedge*> s_zonelist; // ptrs to the starts of all the zones.
	ON_Curve * m_c;
	int m_IsOnRight;
	ON_Interval m_dom;
	zoneedge *m_prev;
	zoneedge *m_next; 

};

#endif // !defined(AFX_ZONECYCLE_H__1A1B81FF_552F_4FEA_B939_3FE09113E0CB__INCLUDED_)
