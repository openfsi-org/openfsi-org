
#ifndef _EDIT_HDR_
#define _EDIT_HDR_ (1)

typedef struct {
  char *string; // BUG needs to be const but its not worked thru
  int  field;
  int set;
  char *value;
} FieldRec;

struct EDIT_DATA {
	SAIL *sail;
#ifdef _X
	Widget fab_label;
	Widget shell;
#endif

};

typedef struct EDIT_DATA Edit_Data;
#ifdef _X
EXTERN_C int do_entity_edit(Widget parent,SAIL *const sail,const char *filter);
#endif
#endif//#ifndef _EDIT_HDR_

