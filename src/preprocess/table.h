/* file : table.h
date:  21 April 1993
 contains declarations for table.C */
#ifndef TABLE_15NOV04
#define TABLE_15NOV04

#include "pctable.h"



EXTERN_C int Edit_Slide_Table(char *seg, float uplim, float downlim, int NP);
EXTERN_C int PC_Create_Slide_Table(float xmin,float ymax,int Nv,int Nw,int NP,char *newt[][4],HC_KEY ownerkeys[]);
EXTERN_C int PC_Create_Slide_Table_TwoCol(float xmin,float ymax,int Nv,int Nw,int NP,char *newt[][2]);
EXTERN_C int  PCT_Free_Table(struct PC_TABLE *t);

#endif //#ifndef TABLE_15NOV04
