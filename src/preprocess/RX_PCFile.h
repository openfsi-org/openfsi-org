#pragma once
struct ltint
{
  bool operator()(const int s1, const int s2) const
  {
    return (s1< s2);
  }
};
#include <map>
using namespace std;
#include "TensylProp.h"
#include "TensylNode.h"
#include "TensylLink.h"
#ifdef RXRHINOPLUGIN   
#ifndef RX_XML2PC
#include "c:\Program Files\Rhino 4.0 SDK\inc\rhinoSdkDoc.h"
#endif
#endif

#define FORCE_UNIT_FACTOR 4.44822     // Pound force to Newton
#define LINEAR_MASS_FACTOR 17.858 // 1 Lb/inch is 17.858 Kg/m

#define RX_PCFile_LINK_LENGTHS 2 
#define RX_PCFile_LINK_PROPS 4 
#define RX_PCFile_COORDS 8 

class RX_PCFile
{
public:
	RX_PCFile(void);
friend class CTensylLink;
	~RX_PCFile(void);
#ifndef RXRHINOPLUGIN   
	int Read(const double tol);
#else
#ifndef RX_XML2PC
	size_t ReadRhino(const double minlength, CRhinoDoc& doc, const int flags);
#endif
#endif
	int Refresh(const char*filename, const int flags=RX_PCFile_LINK_LENGTHS);
	int Print(FILE *fp=stdout,const double p_scale=1.0);
	void AddNode(const int i, CTensylNode n) {m_nodes[i]=n; }
	void AddLink(const int i, CTensylLink n) {m_links[i]=n; }
	void AddProp(const int i, CTensylProp n) {m_props[i]=n; }
	ON_3dPoint GetNodePoint(const int i);
	CTensylNode& GetTNode(const int i);
	CTensylProp& GetTProp(const int i);
	int Init(void);
static	int findline(FILE ** fp, ON_String s);

protected:
	int RefreshNodes(FILE *fp, const int nn); // returns change count
	int RefreshProps(FILE *fp, const int nn); // returns change count
	int RefreshLinks(FILE *fp, const int nn, const int flags, const int PropsHaveChanged); // returns change count

#if !defined(RXRHINOPLUGIN) && !defined(PHYSX) 
protected:
#else
public:
#endif
	int MasterNodeNo(const int i);
//	int findline(FILE ** fp, ON_String s);
	int CreateModel();
	int ShimmerDuplicates(const double tol);
	int DeleteShortLinks(const double tol);


	map<int, CTensylProp > m_props;
	map<int, CTensylNode > m_nodes;
	map<int,CTensylLink > m_links;
	// fixings decoder
	map<int, char * > m_restraints;
	map<int, int > m_masters;
	static int comparebypos(const CTensylNode*a,const CTensylNode*b);
	ON_String m_titlestring; 
public:
#if !defined(RXRHINOPLUGIN) && !defined(RXPHYSX) 
 	RXEntity_p m_owner;
#else
	void *m_owner;
#endif
#ifdef RXQT
        QString m_filename;
#else
        ON_wString m_filename;
#endif

public:
int SpecialAdjustZI();


ON_String PrintSummary(FILE * fp, const double p_scale);
double m_HangerFac;
};
#if !defined(RXRHINOPLUGIN) && !defined(RXPHYSX) 
EXTERN_C int  Resolve_PCFile_Card( RXEntity_p e);

#endif
