// RelaxWorld.cpp: implementation of the RelaxWorld class.
//
//////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include "RXEntity.h"
#include <QDir>
#include <QDebug>
#include "RXLogFile.h"
#include "RXSitePt.h"
#include "rxsubwindowhelper.h"
#ifdef LINK_H5FDDSM
#pragma  message ("compiling "__FILE__  "for h5fddsm ")
#include "rxh5fddsmlink.h"
#endif
#ifdef HOOPS
#include "HBaseModel.h"
#include "HBaseView.h"
#endif

#include "summary.h"
#ifdef _X
#include "hoopsw.h"
#include "pp_callbacks.h"
#include "RX_UI_types.h"
#include <X11/Xatom.h>
#include <X11/Intrinsic.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#include <Xm/PushB.h>
#include <Xm/TextF.h>
#include <Xm/Label.h> /* 15/3/97 */
#endif
#include "menuCB.h"
#ifndef linux
#include "..\stringtools\stringtools.h" // defines FromUtf8 and ToUtf8
#else
#include "../../stringtools/stringtools.h"

#endif
#include "RX_UI_types.h"
#ifdef HOOPS
#include "HDB.h"
#include "HBaseView.h"
#include "HBaseModel.h"
#include "hoopcall.h"
#include "finishview.h"
#endif
#include "cfromf.h"
#include "f90_to_c.h"
#include "RXSail.h"
#include "RXExpression.h"
#include "script.h"

#include "rxmirrorsimple.h"
#include "etypes.h"
#include "Gflags.h"
#include "global_declarations.h"
#include "akmutil.h"
#include "stringutils.h"
#include "WernerWorld.h"
#include "WernerSail.h"

#include "RelaxWorld.h"
int RXGraphicsEnabledQ()  // see also the method
{return g_World->GraphicsEnabledQ();}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

RelaxWorld *g_World;

void Start_RelaxWorld()
{ 
    g_World=new RelaxWorld();
}
void End_RelaxWorld()
{
    delete g_World;
}

RelaxWorld::RelaxWorld():
    m_AmInBringAll(0)
  ,m_StateFile(0)
  , m_HeelAngle(0.0)
  ,m_DBin(0)
  ,m_DBOut(0)
  ,m_DBmirror(0)
  ,m_CalcIsRunning(0)
  , m_LambdaForxyz(0.1) // low value prioritizes lengths, hi prioritizes angles. Generall use the smallest poss
  ,m_AutoCalc(0)
  ,m_GraphicsEnabled(1)
  ,PleaseWriteZIs(0)
  ,m_DrawingLayerHasPrecedence(0)



{
#if defined( _DEBUG) && defined (WIN32)
    char *lp = (char*) malloc(12*sizeof(char));
    memset(lp,0,11);  // this should blitz the heap.
    _CrtCheckMemory();
    free(lp);
#pragma message("_debug  Build - microsoft memory check enabled")
#endif
    init_global_declarations();

    strcpy(m_defaultRunName,"");
    strcpy(m_Run_Name,"un_named");
    strcpy(m_LastRunName,"un_named");
#ifdef HOOPS
    m_HDB = new HDB();  /* initialize MVO's one global class */
    m_HDB->Init();
    //	m_HDB->SetCleanInclude(false);  Hoops15 VC8 doesnt have this
    if( !SetDriverType() ) cout<< " CANT set driver\n"<<endl;

    m_hbm=new HBaseModel();
    m_hbm->Init();
    HC_QSet_User_Index("/",RXCLASS_SAIL,0);
    HC_QSet_User_Index("/",RXCLASS_GRAPHICSTRUCT,0);
    DefineAllCallbacks();
#endif


    m_sails.clear();
    RXExpressionI *eee = this->AddExpression (L"_a_a_",L"1.0",NULL );
    eee->SetAccessFlags(RXE_PRIVATE); // nov 2011
    m_DBin=0;
    m_DBmirror=new RXMirrorSimple() ;
    m_DBOut=0;

    InitActionMap();
}

RelaxWorld::~RelaxWorld()
{
    this->DSM_End();
    while (m_sails.size())
        delete m_sails.back();

    Clear_SumTolList();

    ClearGlobalValues(); // release the memory allocated in GetGlobalValues
    cf_close_all_fortran_files();
    if(DBMirror()) DBMirror()->FlushMirrorTable () ;

    if(this->m_DBin) delete this->m_DBin ; this->m_DBin=0;
    if(this->m_DBmirror) delete this->m_DBmirror ;this->m_DBmirror=0;
    if(this->m_DBOut ) delete this-> m_DBOut ;this-> m_DBOut=0;

    if (this== g_World)
        g_World=0;
#ifndef RXQT
    DeleteCriticalSection(&m_cs);
#endif

}

void RelaxWorld::ExecuteDelayed(const std::string &s){
#ifdef RXQT
    // see    connect(g_rxmw,SIGNAL(RXCommand(QObject *, const QString &)), this, SLOT(rxExecute_w(QObject *, const QString &)));

#else
    QMutexLocker l(&m_cs);
    m_commandQueue.push(s);;
#endif

}
int  RelaxWorld::PostAllDBInputs()  // call this after opening a logfile or log DB
{
    int rc=0;
    for(int i=0;i<this->m_sails.size(); i++) {
        RXSail *s =this->m_sails[i];
        if(!s->IsHoisted ())
            continue;
        this->DBMirror()->StartTransaction();
        rc+= s->AddToSummary();
        this->DBMirror()->Commit();
    }
    Post_Wind_Details( );

    return rc;
}
int  RelaxWorld::PostSomeResults()  // call this after analysis to copy some results to the mirror
{
    int rc=0;
    for(int i=0;i<this->m_sails.size(); i++) {
        RXSail *s =this->m_sails[i];
        if(!s->IsHoisted ())
            continue;
        this->DBMirror()->StartTransaction();
        rc+= s->PostSomeResults();
        this->DBMirror()->Commit();
    }
    Post_Wind_Details( );

    return rc;
}

void RelaxWorld::SetDBMirror( RXMirrorDBI *a)
{
    m_DBmirror=a;
    this->FlushTolHistory();

}
#ifdef HOOPS
HBaseModel* RelaxWorld::GetHBaseModel()
{
    return m_hbm;
}

int RelaxWorld::SetDriverType(void)
{
    const char		*hoops_picture;

    hoops_picture = getenv( "HOOPS_PICTURE" );
    printf(" HOOPS_PICTURE is %s\n",  hoops_picture);
    
    if( !hoops_picture ||
            strstr(hoops_picture,  "x11") ) {
#ifdef _X
        m_HDB->SetDriverType("x11");
#endif
    }
    else if(strstr(hoops_picture,"opengl") ) {
        m_HDB->SetDriverType("opengl");
    }
    else{
        printf( "invalid value in HOOPS_PICTURE: %s (must be lower case)", hoops_picture );
        return 0;
    }
    return 1;
}

int RelaxWorld::DefineAllCallbacks(void){
    //	HC_Define_Callback_Name ("cb1", HIM_FUNCTION(cb1));
    //	assert("should define some callbacks"==0);
    //	HC_Define_Callback_Name ("CB_Camera_DrawTree", HIM_FUNCTION(CB_Camera_DrawTree));
    return 0;
}
#endif
int RelaxWorld::AddSail(SAIL *p_s)
{
    m_sails.push_back(p_s);
    return m_sails.size();
}

int RelaxWorld::RemoveSail(SAIL *p_s)
{
    int k;
    vector<RXSail*>::iterator  it;
    for(it=m_sails.begin();it!=m_sails.end();++it) {
        if(*it==p_s){
            m_sails.erase(it);
            break;
        }
    }
    return m_sails.size();
}


int RelaxWorld::ListAllExpressions(FILE *fp) {

    int i,rc=0;
    rc+= fprintf(fp, "Global Expressions \n");

    //rc+= this->RXENode::List(fp,"global",0,1);
    for(i=0;i<this->m_sails.size(); i++) {
        RXSail *s =this->m_sails[i];
        rc+= s->ListAllExpressions(fp);
    }
    rc+= fprintf(fp, "\nNow via children  \n");
    //	this->RXENode::ListAndChildren(fp);

    return rc;
}

int RelaxWorld::ExportAllExpressions(wostream &s) {

    int i,rc=0; s<<"##ExpList##";
    rc+= RXENode::Export(s,L"world$global", 0 ) ;
    for(i=0;i<this->m_sails.size(); i++) {
        RXSail *ss =this->m_sails[i];
        rc+= ss->ExportAllExpressions(s);
    }
    return rc;
}

int RelaxWorld::ExportAllDependencies (wostream &s,const int what) {

   // DANGER TODO: A cyclic dependency will make this recur indefinitely.
    int  rc=0; s<<"myThickness=0.004;\n  objectgraph:= Graph[ DeleteDuplicates[{  "<<endl;
    // objects are found in:
    //  world/enode ::m_xmap
    //  sail/enode::xmap
    //  sail::emap = entities
    // entity/Object:: omap
    // entity/ENode/xmap
    // entity::

    /*  SO
   *1) do  each member of the worlds xmap
   *2) for each sail
   *        - do  each member of  its xmap
   *        - do  each member of its emap
   *        - for each entity
   *            - do  each member of its xmap
   *            - do  each member of its emap
   *
   **/

    RXEXMAP::const_iterator itm;
    for(itm=m_xmap.begin();itm!=m_xmap.end(); itm++){
        RXExpressionI* p=itm->second ;
        rc+= p->PrintOMapAsGraph(s,what,false);
    }

    for(int i=0;i<this->m_sails.size(); i++) {
        RXSail *ss =this->m_sails[i];
        if(!ss->IsHoisted ())
            continue;
        for(itm=ss->m_xmap.begin();itm!=ss->m_xmap.end(); itm++){
            RXExpressionI* p=itm->second ;
            rc+= p->PrintOMapAsGraph(s,what,false);
        }

        for (ent_citer it = ss->MapStart(); it !=ss->MapEnd(); ++it){
            RXEntity* p  = it->second;
            if(p) {
                rc+= p->PrintOMapAsGraph(s,what,true);
            }
        }
        for (ent_citer it = ss->MapStart(); it !=ss->MapEnd(); ++it){
            RXEntity* p  = it->second;
            if(p) {
                for(itm=p->m_xmap.begin();itm!=p->m_xmap.end(); itm++){
                    RXExpressionI* pp=itm->second ;
                    rc+= pp->PrintOMapAsGraph(s,what,true);
                }

            }
        }
    }// for sail
    long pos = s.tellp();
    s.seekp (pos-2);
    QString tail(" }], VertexLabels->\"Name\" ,ImagePadding->60 ];" );

    RXSTRING tt =  tail.toStdWString();
    s<< tt <<endl;

    tail = QString(
                "v = VertexList[objectgraph] ;\n"
                "  ee = EdgeList[objectgraph];\n"
                "  vs = Map[ToString, v ];\n"
                "  vlabels := MapThread[Tooltip, {v, vs}]  ; \n"
                "  objectgraph:= Graph[vlabels, ee ]\n " );
            tt =  tail.toStdWString();
    s<< tt <<endl;
    return rc;
}

// resets the loading indices so all hoisted models have contiguous loading indixe
int RelaxWorld::PackLoadingIndices(){  // called on a Drop.


    int rc=1;
    for(int i=0;i<this->m_sails.size(); i++) {
        RXSail *s =this->m_sails[i];
        if(!s->IsHoisted ()) {s->SetLoadingIndex(-1);  continue;}
        if (s->m_Selected_Entity[PANSAIL]==0 )
        {s->SetLoadingIndex(-1);  continue;}
        rc+= s->SetLoadingIndex(rc); rc++;
    }

    return rc;
}
vector<RXSail*> RelaxWorld::ListModels(const int p_Flag){ // only accepts flag  RXSAIL_ALL, RXSAIL_ISBOAT, RXSAIL_ISHOISTED
    // Feb 9 2007
    // we've verified that
    // to get hoisted sails(noboat)	rc = ListModels(RXSAIL_ISHOISTED);
    // to get the boat go 	 	rc = ListModels(RXSAIL_ISBOAT+ RXSAIL_ISHOISTED);
    //to get unhoisted sails go 	rc = ListModels( RXSAIL_NOTBOAT_NORHOISTED );
    // to get everything go     	rc = ListModels(RXSAIL_ALL);
    // to get hoisted and the boat 	rc = ListModels(RXSAIL_ALLHOISTED);

    vector<RXSail*> rc;
    RXSail *s;
    rc.clear();

    bool	  bFlag = (0!=(p_Flag&RXSAIL_ISBOAT)); //printf("\n(RelaxWorld::ListModels) Flag = %d \t Isboat = %d \t",p_Flag,bFlag);
    bool	  hFlag = (0!=(p_Flag&RXSAIL_ISHOISTED)); //printf(" IsHoisted = %d \n",hFlag);
    bool hs,bs;
    int i;
    for (i=0;i<m_sails.size(); i++) {
        s = m_sails[i];
        hs = s->IsHoisted();
        bs = s->IsBoat();

        if((p_Flag == RXSAIL_ALL)) {
        }
        else if((p_Flag == RXSAIL_ALLHOISTED)) {
            if(!hs) continue;
        }
        else
        {
            if((hs != hFlag  ) || ( bs != bFlag))
                continue;
        }
        rc.push_back(m_sails[i]);
    }
    return rc;
}

int RelaxWorld::PrintModelList(FILE *fp){  // Feb 12 2007 this checks out OK\// remember that the boat is hoisted,


    vector<RXSail*> rc;
    RXSail *s;
    fprintf(fp,"\n***************************\nGlobal Model List\n RXSAIL_ALL \n");
    rc = ListModels(RXSAIL_ALL);
    int i;

    for (i=0;i<rc.size(); i++) {
        s = rc[i];
        s->PrintModelSummary(fp);
    }
    fprintf(fp,"\n***************************\nGlobal Model List\nISBOAT and HOISTED \n");
    rc = ListModels(RXSAIL_ISBOAT+ RXSAIL_ISHOISTED);

    for (i=0;i<rc.size(); i++) {
        s = rc[i];
        s->PrintModelSummary(fp);
    }
    fprintf(fp,"\n***************************\nGlobal Model List\n RXSAIL_ISHOISTED (not boat)\n");
    rc = ListModels(RXSAIL_ISHOISTED);
    for (i=0;i<rc.size(); i++) {
        s = rc[i];
        s->PrintModelSummary(fp);
    }
    fprintf(fp,"\n***************************\nGlobal Model List\n  RXSAIL_ISBOAT (unhoisted boat))\n");
    rc = ListModels( RXSAIL_ISBOAT );
    for (i=0;i<rc.size(); i++) {
        s = rc[i];
        s->PrintModelSummary(fp);
    }
    fprintf(fp,"\n***************************\nGlobal Model List\n  neither boat nor hoisted \n");
    rc = ListModels( RXSAIL_NOTBOAT_NORHOISTED );
    for (i=0;i<rc.size(); i++) {
        s = rc[i];
        s->PrintModelSummary(fp);
    }
    fprintf(fp,"\n***************************\nGlobal Model List\n all hoisted (sb boat too) \n");
    rc = ListModels( RXSAIL_ALLHOISTED );
    for (i=0;i<rc.size(); i++) {
        s = rc[i];
        s->PrintModelSummary(fp);
    }

    return 1;
}
SAIL *RelaxWorld::FindModel(const std::string &p_name, const int p_hoisted){
    SAIL *s =0;
    bool hs;
    int i;
    for (i=0;i<m_sails.size(); i++) {
        s = m_sails[i];
        hs = s->IsHoisted();
        if(p_hoisted && (!hs) ) continue;
        if(p_name!=s->GetType())
            continue;
        return s;
    }
    return 0;
}
SAIL *RelaxWorld::FindModel(const char*name, const int p_hoisted){
    SAIL *s =0;
    bool hs;

    int i;
    for (i=0;i<m_sails.size(); i++) {
        s = m_sails[i];
        hs = s->IsHoisted();
        if(p_hoisted && (!hs) ) continue;
        if(!strieq(name,s->GetType().c_str ()))
            continue;
        return s;
    }
    return 0;
}

int RelaxWorld::SetHeelAngle(const double g_a_degrees)
{
    if(fabs(this->m_HeelAngle - g_a_degrees) < ((double) 1e-5) )
        return 0;
    this->m_HeelAngle = g_a_degrees;

    mkGFlag(GLOBAL_DO_TRANSFORMS);
    mkFlag(g_Analysis_Flags,WIND_CHANGE);
    if(g_boat) g_boat->SetFlag(FL_NEEDS_TRANSFORM);
    return 1;
}
int RelaxWorld::MakeLastKnownGood() {
    SAIL *s;
    int i;
    for (i=0;i<m_sails.size(); i++) {
        s = m_sails[i];
        if( !s->IsHoisted() ) continue;
        s->MakeLastKnownGoodDxDyDz ();
    }
    return 0;
}

int RelaxWorld::PostProcessAllSails(int p_Wh) // 1 means aero, + 2 for structure too
{
    int i ;
    int count=1;
    if (!DISABLE_GRAPHICS  ) {
        count=0;
        vector<RXSail*> ll = this->ListModels(RXSAIL_ALLHOISTED); // boat too
        for(i=0;i<ll.size();i++)  {
            SAIL *theSail = ll[i];

            if(theSail->GetFlag(FL_NEEDS_POSTP)) {
                theSail->DrawDeflected( theSail->PostProcNonExclusive() ); // for the string
                theSail->PostProcessOneModel(p_Wh);
                theSail->ClearFlag(FL_NEEDS_POSTP);
                //            if(!count++)   cout<<" postprocess." ;
                //            cout<<".";
                count++;
            }
        }
#ifdef HOOPS
        Change_Hoops_Strings("/.../text_overlay","user options,text", m_LastRunName,m_Run_Name,Summout());
        Finish_All_Views();
#endif

    }
    if(count) {
        if(g_qAfter_Each_Update.length() ) { //
            char *buff = STRDUP(qPrintable(g_qAfter_Each_Update) );
            if(this->DBMirror() )
                while(this->DBMirror()->Replace_Words_From_Summary(&buff,0) ) ;

            Execute_Script_Line(buff,0);//RXSubWindowHelper::rootgraphic);
            RXFREE(buff);
        }
    }
    return(0);
}
int RelaxWorld::Bring_All_Models_To_Date(){
    int i;
    if (m_AmInBringAll){
//        qDebug()<< "am already in BringAll";
        return 0;
    }
    m_AmInBringAll=1;
    int SomethingNeedsRunning
            =rxFlag(g_Analysis_Flags,WIND_CHANGE)
            + rxFlag(g_Analysis_Flags,INITIAL_RUN);

    int  retVal = 0;
    vector<RXSail*> ll = ListModels(RXSAIL_ALL); // boat tooRXSAIL_ALLHOISTED
    //repeat this loop while any s->GetFlag(FL_NEEDS_GAUSSING)
    int StillGoing,SolveOK;
    do {
        StillGoing=0;
        for(i=0;i<ll.size();i++)  {
            SAIL *s = ll[i];
            if(s->GetFlag(FL_NEEDS_GAUSSING)) {
                SolveOK= Do_Solve(NULL,s);
                if(!SolveOK) {
                    StillGoing=0;
                    s->ClearFlag(FL_NEEDS_RUNNING);
                    break;
                }
                Do_Mesh(s);
                StillGoing++;
                retVal++;
            }
            retVal+=(0!=s->GetFlag(FL_NEEDS_TRANSFORM));
            if(s->IsHoisted() && s->GetFlag(FL_NEEDS_RUNNING))
                SomethingNeedsRunning++;
        } // for i
    } while(StillGoing);

    if(retVal ||isGFlag(GLOBAL_DO_TRANSFORMS) )  {
        SAIL *s;
        this->SetUpAllTransforms(1); // in BAMTD 1 means regardless of flags
        clGFlag(GLOBAL_DO_TRANSFORMS);
        for(i=0;i<ll.size();i++)  {
            s = ll[i];
            s->LoadStateIfNecessary();
            retVal+= s->GetFlag(FL_NEEDS_POSTP);
        }
        cf_analysis_prep(); // so connects are re-made after an edit.
    }
    if(retVal)
        retVal += this->PostProcessAllSails(3);
#ifdef _DEBUG
    if(retVal||SomethingNeedsRunning )
        OutputToClient(QString("Done model update  %1 %2").arg(retVal).arg( SomethingNeedsRunning),1);
#endif
    if(this->m_AutoCalc && SomethingNeedsRunning)
        Do_Calculate(NULL,0);// RXSubWindowHelper::rootgraphic);
    m_AmInBringAll=0;
    return(retVal);
} // Bring_All_Models_To_Date

void RelaxWorld::InitActionMap() // fill the words we know are safe for a RX_VALUECHANGE 
// else leave blank or set to RX_TOPOLOGYCHANGE;
{
    m_ActionOnEdit[make_pair(SEAMCURVE  ,	5)] = RX_VALUECHANGE;
    m_ActionOnEdit[make_pair(SITE	,	2)] = RX_VALUECHANGE;
    m_ActionOnEdit[make_pair(SITE	,	3)] = RX_VALUECHANGE;
    m_ActionOnEdit[make_pair(SITE	,	4)] = RX_VALUECHANGE;
    m_ActionOnEdit[make_pair(RELSITE	,	3)] = RX_VALUECHANGE;
    m_ActionOnEdit[make_pair(PCE_SETDATA,	2)] = RX_VALUECHANGE; // what about   PCE_SPLINE ?
}
int RelaxWorld::EditAction(const int type,const int column)
{// if in doubt, return RX_TOPOLOGYCHANGE
    int rc = RX_TOPOLOGYCHANGE;
    if( m_ActionOnEdit.find(make_pair(type,column)) !=m_ActionOnEdit.end())
        rc = m_ActionOnEdit[make_pair(type,column)];
    return rc;
}

int fc_OnReload(void){ // draw all deflected
    class RXSail * theSail;
    int i,rc=0;
    vector<RXSail*> lsl  = g_World->ListModels(RXSAIL_ALLHOISTED); // boat too
    for (i=0;i<lsl.size();i++) {
        theSail = lsl[i];
        theSail->DrawDeflected( theSail->PostProcNonExclusive() );rc++;
    }
    return rc;
}

#ifdef MICROSOFT
int RelaxWorld::SendMessageToCallers(int i) // eg RXWIN_IHAVERUN
{	int rc=0;

    std::set<pair<HWND,HWND> > ::iterator it;
    int errval=0;
    for(it=m_callers.begin();it!=m_callers.end();++it){
        HWND caller = it->first ;
        HWND contact = it->second ;
        if(! ::PostMessage(caller,
                           i,
                           (WPARAM) contact,
                           0))
        {
            errval = GetLastError();
            cout<<"failed to post a message. errcode= "<<errval<<endl;
        }

    }

    return rc;
}
#endif
// for the header

int  RelaxWorld::IncrementRunName(void) {
    /* designed to be called ONCE after each analysis */

    strcpy( m_LastRunName, m_Run_Name);

    if(strieq(m_defaultRunName,m_Run_Name)) {
        //        char *lp;
        //        char buf[256];
        //        int ival;

        //        lp = strchr(m_defaultRunName,'+');
        //        if(lp) {
        //            ival = atoi(lp);
        //            *lp = '\0';
        //        }
        //        else
        //            ival=0;

        //        ival++;

        //        sprintf(buf,"%s+%d",m_defaultRunName,ival);
        QString qs(m_Run_Name);
        Increment_String (qs);
        ModifyRunName(qPrintable(qs) );
    }
    return 1;
}
int  RelaxWorld::MakeRunNameUnique() // called once before each analysis
{
    QString qs(m_Run_Name);
    if  (this->DBout())
    {
        if(this->DBout()->MakeUnique("Run_Name",qs))
        {
            strcpy(m_Run_Name,qPrintable(qs));
            Post_Summary_By_Sail(NULL,"Run_Name",g_World->GetRunName());
            return 1;
        }
    }
    return 0;
}

int  RelaxWorld::ModifyRunName(const char*p_new)/* may be called many times */
{
    if(!p_new) return 0;
    strcpy(m_Run_Name,p_new);
    this->MakeRunNameUnique();
    strcpy(m_defaultRunName,m_Run_Name);

    Post_Summary_By_Sail(NULL,"Run_Name",m_Run_Name);
    return 1;
}
int RelaxWorld::AbsoluteFileName( QString &qfname )
{
    FILE *fp;
    assert (OK_TO_READ   ==  1);    assert (OK_TO_WRITE  ==  0);    assert (BAD_FILENAME == -1);

    QRegExp  rx( "[?*@]");
    if(qfname.isEmpty() || qfname.contains(rx))
        return(BAD_FILENAME);

    if(!qfname.startsWith("$R") ) {
        QDir base(g_currentDir);
        qfname= base.absoluteFilePath ( qfname ) ;
        qfname=  QDir::cleanPath ( qfname  ) ;
        QFileInfo  fi(qfname);
        bool isdir = fi.isDir();
        bool exists = fi.exists();
        if(!isdir) {
            if(exists) return OK_TO_READ;
            return OK_TO_WRITE;
        }
        else {
            if(exists) return OK_TO_WRITE;
            return BAD_FILENAME;
        }
    }
    if((fp = RXFOPEN(qPrintable(qfname),"r"))) {
        FCLOSE(fp);
        return(OK_TO_READ);
    }
    if((fp = RXFOPEN(qPrintable(qfname),"w"))) { // strangely, linux permits a '*' in a 'w' filename

        FCLOSE(fp);
        remove(qPrintable(qfname));
        return(OK_TO_WRITE);
    }
    return(BAD_FILENAME);
}

int RelaxWorld::Post_Wind_Details()
{
    return m_wind.Post_Wind_Details();
}

int RelaxWorld::DSM_Init(int pUseDsm)
{
#ifdef LINK_H5FDDSM
    if(this->m_dsmlink)
        return 0;

    this->m_dsmlink=new rxh5fddsmlink(pUseDsm,0);
    this->m_dsmlink->Connect();
    return this->m_dsmlink->IsOK();
#endif
    return 0;
}

int RelaxWorld:: DSM_End()
{
#ifdef LINK_H5FDDSM
    if(!this->m_dsmlink)
        return 0;
    delete  this->m_dsmlink;
    this->m_dsmlink=0;
    return 1;
#endif
    return 0;
}

int  RelaxWorld::DSM_TransferModels(const QString s)
{
#ifdef LINK_H5FDDSM
    if(!this->m_dsmlink)
    {
        // cout<<" DSM link not open"<<endl;
        return 0;
    }
    if(!this->m_dsmlink->StartTransfer())
    {
        return DSM_End();
    }
    std::vector<RXSail*>::iterator it;
    std::vector<RXSail*>ll=  ListModels( RXSAIL_ALLHOISTED);
    for(it=ll.begin();it!=ll.end();++it)  {
        SAIL *theSail = *it;
        this->m_dsmlink->TransferOneModel(theSail ) ;
        theSail->ClearFlag(FL_NEEDS_POSTP); //
    }
    this->m_dsmlink->FinishTransfer();
    return 1;
#endif
    return 0;
}
//#define RXSAIL_ISHOISTED	4 	// by default, not the boat
//#define RXSAIL_ALLHOISTED	8	// meant to include the boat
#ifndef NEW_WERNER

int   RelaxWorld::run_Werner( )
{
    int rc=0;
    SAIL *rjib=0, *rmain=0;
    WernerWorld ww;
    double pinc=25.0;
    int N= m_wind.SailppN();
    int flags = m_wind.SailppFlags();
    int pdist=1;
    int pNWLS=20;
    int pNWLSF=20;
    int pihwl=20;
    double aws;

    pinc = - this->m_wind.Get(RXWindGeometry::AWA) * 180.0/ON_PI;
    aws = this->m_wind.Get(RXWindGeometry::AWS);
    cout<<"Sail++ with incidence="<<pinc<<" AWS= "<<aws<<" flags="<<flags<<" ( U cosine=2, V Cosine=4)"<<endl;
    ww.SetParameters (  pinc, N, pdist, pNWLS, pNWLSF, pihwl,aws);

    std::vector<RXSail*>sls=  ListModels( RXSAIL_ISHOISTED);
    if(sls.size()<2){
        cout<<" Sorry - Sail++ needs two sails"<<endl;
        return 0;
    }
    //    for( std::vector<RXSail*>::iterator it = sls.begin(); it!= sls.end();++it)
    //        qDebug()<< "rsail"<< (*it)->GetType().c_str();

    rjib  = *(sls. begin());    class WernerSail *wjib  =  ww.AddSail( rjib->GetType().c_str());
    rmain = *(sls.rbegin());    class WernerSail *wmain =  ww.AddSail(rmain->GetType().c_str());

    std::vector<WernerPoint>vv ,cps;
    std::vector<RXSitePt> ::iterator it;
    std::vector<RXSitePt>  rxcpsj ,rxvvj ;

    rjib-> WernerPoints(N, rxvvj ,rxcpsj,flags );

    for(it= rxvvj.begin(); it!=rxvvj.end(); ++it)
        vv.push_back(WernerPoint( it->x,it->z,it->y));
    for(it= rxcpsj.begin(); it!=rxcpsj.end(); ++it)
        cps.push_back(WernerPoint( it->x,it->z,it->y));

    wjib->SetCPs(cps);
    wjib->SetVVs(vv);
    std::vector<RXSitePt> rxcpsm,rxvvm ;
    rmain->WernerPoints(N, rxvvm ,rxcpsm, flags );// on unit square.
    vv.clear(); cps.clear();
    for(it= rxvvm.begin(); it!=rxvvm.end(); ++it)
        vv.push_back(WernerPoint( it->x,it->z,it->y));
    for(it= rxcpsm.begin(); it!=rxcpsm.end(); ++it)
        cps.push_back(WernerPoint( it->x,it->z,it->y));

    wmain->SetCPs( cps);
    wmain->SetVVs(vv);
    QString  textout = ww.justRun();
    qDebug()<<textout;
    QString mname(rmain->GetType().c_str() );
    QString jname(rjib->GetType().c_str() );


    QStringList wds = textout.split(QRegExp("[\t\n\r: ]"), QString::SkipEmptyParts );
    QString what("Cl")  ;
    // typically  "jib", " Cl","1.90075", "jib",.....
    for(int i=0; i<wds.size()-2;i++){
        if(wds[i]== mname && wds[i+1]==what)
            Post_Summary_By_Sail(rmain,qPrintable(wds[i+1]),qPrintable(wds[i+2]));
    }
    for(int i=0; i<wds.size()-2;i++){
        if(wds[i]== jname && wds[i+1]==what)
            Post_Summary_By_Sail(rjib,qPrintable(wds[i+1]),qPrintable(wds[i+2]));
    }

    std::vector<double> dp;

    //    qDebug()<<"\nmain pressures";
    dp = wmain->GetPressures();
    rmain-> SetUpWernerPressures(   rxcpsm,  dp);
    //   std::vector<double> ::iterator it2;
    //    for(it2=dp.begin(); it2!=dp.end();++it2)
    //        qDebug()<<*it2;
    //    qDebug()<<"jib pressures";
    dp = wjib->GetPressures();
    rjib-> SetUpWernerPressures(   rxcpsj,  dp );
    //    for(it2=dp.begin(); it2!=dp.end();++it2)
    //        qDebug()<<*it2;

    delete wjib;
    delete wmain;

    return rc;
}
#else


int   RelaxWorld::run_Werner( )
{
    int rc=0;
    SAIL *rjib=0;
    WernerWorld ww;
    double pinc=25.0;
    int N= g_PansailIn.PANSAIL_N_DEFAULT;
    int flags = g_PansailIn.PANSAIL_M_DEFAULT;
    int pdist=1;
    int pNWLS=20;
    int pNWLSF=20;
    int pihwl=20;
    double awa,aws;

    pinc = - this->m_wind.Get(RXWindGeometry::AWA) * 180.0/ON_PI;
    aws = this->m_wind.Get(RXWindGeometry::AWS);

    cout<<"Sail++ with incidence="<<pinc<<" AWS= "<<aws<<" flags="<<flags<<" ( U cosine=2, V Cosine=4)"<<endl;
    ww.SetParameters (  pinc, N, pdist, pNWLS, pNWLSF, pihwl,aws);

    std::vector<RXSail*>sls=  ListModels( RXSAIL_ISHOISTED);
    std::vector<class WernerSail*>wss;
    std::vector<RXSail*>::iterator its;
    std::vector<class WernerSail*>::iterator itw;
    std::vector< std::vector<RXSitePt>  > TheCps;
    std::vector< std::vector<RXSitePt>  >::iterator itcp;
    if(sls.size()<1){
        return 0;
    }
    for( its = sls.begin(); its!= sls.end();++its)
    {
        // qDebug()<< "rsail"<< (*its)->GetType().c_str();
        rjib  = *its;
        class WernerSail *wjib  =  ww.AddSail( rjib->GetType().c_str());
        wss.push_back(wjib);
        std::vector<WernerPoint>vv ,cps;
        std::vector<RXSitePt> ::iterator it;
        std::vector<RXSitePt>  rxcpsj ,rxvvj ;

        rjib-> WernerPoints(N, rxvvj ,rxcpsj,flags );
        for(it= rxvvj.begin(); it!=rxvvj.end(); ++it)
            vv.push_back(WernerPoint( it->x,it->z,it->y));
        for(it= rxcpsj.begin(); it!=rxcpsj.end(); ++it)
            cps.push_back(WernerPoint( it->x,it->z,it->y));

        wjib->SetCPs(cps);
        wjib->SetVVs(vv);
        TheCps.push_back(rxcpsj);
    } // iterate over sails

    ////////////////////////////////////////////////////
    QString  textout = ww.justRun();
    qDebug()<<textout;
    QStringList wds = textout.split(QRegExp("[\t\n\r: ]"), QString::SkipEmptyParts );
    QString what("Cl")  ;
    // typically  "jib", " Cl","1.90075", "jib",.....

    ////////////////////////////////////////////////////


    for(its = sls.begin(),itw=wss.begin(),itcp=TheCps.begin(); its!= sls.end();++its,++itw,++itcp)
    {
        //   qDebug()<< "rsail"<< (*its)->GetType().c_str();
        rjib = *its;
        class WernerSail *wjib = *itw;
        std::vector<RXSitePt>  &rxcpsj  = *itcp;

        QString jname(rjib->GetType().c_str() );
        for(int i=0; i<wds.size()-2;i++){
            if(wds[i]== jname && wds[i+1]==what)
                Post_Summary_By_Sail(rjib,qPrintable(wds[i+1]),qPrintable(wds[i+2]));
        }
        std::vector<double> dp;

        dp = wjib->GetPressures();
        rjib-> SetUpWernerPressures(   rxcpsj,  dp );
        delete wjib;

    } // iterate over sails



    return rc;
}


#endif
