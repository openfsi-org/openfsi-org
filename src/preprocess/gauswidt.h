   /* gauswidt.h header for gausedg.c */

#ifndef GAUSWIDT_16NOV04
#define GAUSWIDT_16NOV04


struct GAUSSEDGE {
	VECTOR e1,e2;
	PSIDEPTR *ps;
	int sensitive;
	int side;
};
 struct GAUSSBISECTOR {
	VECTOR start,v;
};
struct GAUSSINTERSECT {
	VECTOR v;
	double s1,s2;
	int k1,k2;
	int live,right;

};

#endif //#ifndef GAUSWIDT_16NOV04
