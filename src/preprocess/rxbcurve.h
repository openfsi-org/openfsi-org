#pragma once
#include "RXPolyline.h"
#include "vectors.h"
#include "opennurbs.h"
#include "RXEntity.h"

#define BCSPLINE 1
#define BCUVCURVE 2

// the old basecurve functionality,
// the parent class for 2D curve, 3D curve, UV Curve, Spline, Gauss Curve
// RXCurve2D
// RXCurve3D
// RXCurveUV
// RXGaussCurve


class RXBCurve : public  RXEntity
{
public:

    RXBCurve();
    RXBCurve(class RXSail *p );
    int CClear();
    virtual int Dump(FILE *fp) const;
    virtual int Compute(void);
    virtual int Resolve(void);
    virtual int ReWriteLine(void);
    virtual int Finish(void);

    ~RXBCurve();
    class RXBCurve * DataPtr();
    int Init();

    //protected:
    VECTOR *p;
    int c;
    /* if its a spline, the following define
        the spline. Compute BC will generate a
        polyline of c points along the spline */
    int BCtype;	 /* BCSPLINE  if its a spline , BCUVCURVE if its a UVcurve*/
protected:

    int String_To_Polyline(const char *s,VECTOR**p ,int *c);
public:
};

class RXCurve2D : public  RXBCurve
{
public:
    RXCurve2D();
    RXCurve2D(class RXSail *p );
    ~RXCurve2D();
    int Resolve();
    int ReWriteLine(void);
    int Dump(FILE *fp) const;
};


class  RXCurve3D : public  RXBCurve
{
public:

    RXCurve3D();
    RXCurve3D(class RXSail *p );
    ~RXCurve3D();
    int Resolve();
    int Dump(FILE *fp) const;
};


class  RXCurveUV : public  RXBCurve
{
public:

    RXCurveUV();
    RXCurveUV(class RXSail *p );
    ~RXCurveUV();
    int Resolve();
    int Dump(FILE *fp) const;
};


class    RXGaussCurve : public    RXBCurve
{
protected:
    class RXPolyline m_pin;
public:

    RXGaussCurve();
    RXGaussCurve(class RXSail *p );
    ~RXGaussCurve();
    int Resolve();
    int Dump(FILE *fp) const;
    int ReWriteLine(void);
    int Group(const int nmax, const double tol);
    int Compute_GaussCurve(float Chord,int IsSeam,double p_Linear_Tol) ;
};



