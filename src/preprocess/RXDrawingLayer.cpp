#include "StdAfx.h"
#include "RXSail.h"
#include "RXDrawingLayer.h"

// february 2013  made Resolve tolerant of incomplete line

RXDrawingLayer::RXDrawingLayer(void)
{
}

RXDrawingLayer::RXDrawingLayer(class RXSail *s ):
	RXEntity(s)
{

}


RXDrawingLayer::~RXDrawingLayer(void)
{
}

int RXDrawingLayer::Dump(FILE *fp) const{
	return 0;
}
int RXDrawingLayer::Compute(void) {this->SetNeedsComputing(0); return 0;}
int RXDrawingLayer::Resolve(void){
/* drawinglayer card is of type
	drawinglayer 	 name 	 atts ! comment */
	vector<string> words= rxparsestring(this->GetLine(),RXENTITYSEPS);

        if(words.size()>2)
            AttributeAdd(words[2].c_str());


        if(PC_Finish_Entity(this->type(),name(),0,(long)0,NULL,qPrintable (this->AttributeString()),GetLine())) {
		 this->SetNeedsComputing(0);
		 this->Needs_Resolving=0;
		 return 1;
   }
	return 0;

}
int RXDrawingLayer::Finish(void){this->Needs_Finishing=0; return 0;}
int RXDrawingLayer::ReWriteLine(void){
    QStringList newline;
    newline<<type()<<  name();
    newline <<this->AttributeString();
    this->SetLine(qPrintable( newline.join(RXENTITYSEPSTOWRITE) ));
	return 0;}
