// layertst.h: interface for the layertst class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LAYERTST_H__28292935_DFF3_4587_B5AE_FD45A8D9512D__INCLUDED_)
#define AFX_LAYERTST_H__28292935_DFF3_4587_B5AE_FD45A8D9512D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "RXMathlink.h"
class dovestructure;  // nov 2008 for gcc
#include "layerobject.h"
#include "layernode.h"
#include "layerlink.h"
#include "layerlayer.h"
#include "layercontour.h"

#include "opennurbs.h"	// Added by ClassView
#include "RXmaterialMatrix.h"	// Added by ClassView

#ifdef linux
//	#define max(x,y)  ((x) > (y) ? (x) : (y))
//	#define min(x,y)  ((x) < (y) ? (x) : (y))
#endif


class RXmaterialMatrix;

class layerint
{
public:
	layerint();
	virtual ~layerint();


int m_i1,m_i2;
};

class layertst  : public layerobject 
{
friend class layerobject;
friend class layerlink;
friend class layernode;
friend class dovestructure;
friend class doveforstack;
protected:
	int Sample_All_Nodes();
	int IsMathematicaRunning();
	int Start_Mathematica(const char*p_mathpath);
	int Empty();

	int CreateNodes(const int nr,const int nc);
	int AW_Recursive_Improve(layernode*, layerlink*);
	layernode * FindNode(const int p_f);
	int FinishDataStructure();
	int AddLinks();
	int Read(const char*f);

	int m_nc;
	int m_nr;
	int m_penaltyPower;
	ON_SimpleArray<layernode*> m_node;
	ON_ClassArray<layerply> m_plies;	

public:
	int HowManyLayers();
	int SetPoles();
	void SetDoveStructure(dovestructure* p);
	dovestructure* GetDoveStructure();
	RXmaterialMatrix DDFromSurfaces(const double u,const double v);
	double GetPlyTrace();
	int GetOneGradient(const double u,const double v, const int level, ON_2dVector *gradAlpha);
	ON_String GetDovefile();
	int SetDoveFile(const char*p_s);
	int Read_Control_Data(const char *f);

	layernode* AW_Adaptive_Improvement();
	int Create_Contours(double z0,double pStep);
	ON_2dVector GetRefVector();
	char * Deserialize(char *p_s);// Fills out the object from p_s. returns the ptr to the next char
	ON_String  Serialize(); // returns a ON_String image of the object  Also renumbers the nodes.

	int RemoveDuplicates(ON_SimpleArray<layernode *> & p_pl, const double p_tol=1e-6);
	int TraceFacet(layerlink *e, int revFlag, int go_right, ON_2dPoint &p, ON_SimpleArray<layernode *> *ptList ,const int level=-1);
	int PrintPlies(FILE *fp) const;
	int DrawAllInterpolatedAngles(const int nr, const int nv  );
	int DrawAllInterpolatedGrads(const int nr, const int nv  );
	int DrawPrincAngle(const int nr, const int nv  );
	int PrintInterpolatedAngles(ON_2dPoint &p,ON_SimpleArray<layernode*> ptList,ON_SimpleArray<double>theWeights );
	int NearestNeighbour(ON_2dPoint &p,ON_SimpleArray<layernode *> *ptList,ON_SimpleArray<double> *theWeights ) ;
	int CreatePlies();
	int DeleteOneNode( const layernode * n);
	layernode* InsertOneNode(const double u,const double v,
		const int r,const int c, 
		const ON_ClassArray<layernodestack>  stack,
		double StackError,
		double *Trace =0,
		double *npliesExact=0
		);
	int DeleteOneLink(const layerlink*);
	layerlink* InsertOneLink(  layernode*n1,  layernode*n2,const double w,const int flag);
	ON_ClassArray<layerint> Delaunay(const ON_SimpleArray<layernode*> );
	HC_KEY DrawHoops();
	// we'll call the recursive walk routine AW_xxx so they come at the top of the method list.
	int AW_Walk(const int p_adaptive=0);
	layernode* AW_FindStart();
	int ClearAllLinkFlags(const int p_f);
	int ClearAllNodeFlags(const int p_f);
	int SetAllNodeFlags(const int p_f);
	int SetAllLinkFlags(const int p_f);

	int Print(const char*f, const int p_flag) const;  //0 for all, else for input format
	int Print_Control_Data(const char *f)const ;

	layertst();
	virtual ~layertst();

	int Do_Layer_Test(const char*f);
	double SetPlyTrace(const char*s);
	double SetPlyTrace(void);
	int RefineBoundaries( const double pTol);
	RXmaterialMatrix GetPlyMatrix(const char*s);

	int m_drawing;
	ON_String m_MathText;
	int m_dbgint;		// the time limit
	ON_String m_ltDoveFile;
	double m_lengthTol;
	ON_SimpleArray<layerlink*> m_links;
	ON_SimpleArray <layercontour *> m_lc;
#ifdef MATHLINK
	RX_MathSession m_mathsession; 
#else
//	int m_mathsession;  horrible
#endif

private:
	int Alpha(const double u,const double v,const int level, double*a);
	int SetDefaultParameters();
	dovestructure * m_dovestructure;
	double m_plyTrace;
	double m_angleTol;
	layerlink* m_seedEdge;
	int m_seedrevFlag;

};

EXTERN_C int RXL_Dove_Test(char *p_file,int flag) ;

EXTERN_C int RXL_Layer_Test(char *p_file,int flag) ;
#endif // !defined(AFX_LAYERTST_H__28292935_DFF3_4587_B5AE_FD45A8D9512D__INCLUDED_)
