// layerangle.h: interface for the layerangle class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LAYERANGLE_H__8B6C4403_0D1F_4CB9_AAEE_62D948369B3D__INCLUDED_)
#define AFX_LAYERANGLE_H__8B6C4403_0D1F_4CB9_AAEE_62D948369B3D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "layerobject.h"

class layerangle : public layerobject  
{
public:
	double GetCosTwoA();
	double GetSinTwoA();
	void SetAngle(const double a);
	double GetSin()const;
	double GetCos()const;
	double GetAngleInRadians()const;
	double GetAngleInDegrees() const;
	double Difference(layerangle* p_a);
	layerangle(double *p_pa);
	layerangle(double p_a);
	layerangle();
	virtual ~layerangle();

private:
	double m_ca;
	double m_sa;
	double m_a;
protected:
	layernode* m_node;
};

#endif // !defined(AFX_LAYERANGLE_H__8B6C4403_0D1F_4CB9_AAEE_62D948369B3D__INCLUDED_)
