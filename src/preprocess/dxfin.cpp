/*
may 2008 added $n1=, $n2= functionality to generateCurveFromGraphic

dxfin function JIB 1995
  Nov 97  arcs look funny. Is this because of the angle PVs or is this
  the transformation matrix. Should we append this not set it?
   Polylines should treat 2Ds and 3Ds exactly the same. (post V11)

 Oct 97 some changes to deal with spline polylines. WE skip spline vertices.

  July 97 remember the FCLOSE
  19/3/96 PH ported to MSVC. One functional
  change. Use  the fact that the last line is "EOF" to terminate
  searches.

 */
#include "StdAfx.h"
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXSeamcurve.h"
#include "RelaxWorld.h"
#include "RXAttributes.h"
#include "iges.h"
#include "stringutils.h"
#include "vectors.h"
#include "panel.h"
#include "words.h"
#include "cut.h"
#include "script.h"
#include "polyline.h"
#include "resolve.h"
#include "entutils.h"
#include "hoopcall.h"
#include "akmutil.h"
#include "entities.h"
#include "interpln.h"
//#include "add_hoops.h"
#include "resolve.h"
#include "finish.h"
#include "printall.h"

#include "global_declarations.h"

#include "dxfin.h"
//#include "hcdb.h" /* to mobilize, global change HC_ to HCDB_ */

//#define DEBUG_DXF

FILE *filein;
int G_group,G_section,G_is_oriented;

char s_table[DXF_MINSTR];

char segment[DXF_MAXSTR],dxfsegment[DXF_MAXSTR],dxflibrary[DXF_MAXSTR];

const char *dxfcolorList[256];

float G_thickness;
int wierd=0;
struct header *first_header, *cur_header;

/*#ifdef MAINPROG
  int  main (ArgC,ArgV) int ArgC; char *ArgV[]; {
     HC_KEY key;
   char filename[256], *lp=0;
   if( !ArgC)	  return 1;
   strcpy(  filename, ArgV[0]);
     key = 	dxfin(filename);
      if(key){
    lp = strrchr(filename	,'.');
    if(lp) *lp=0;
    HC_Open_Segment("?Picture");
        HC_Open_Segment(filename);
        HC_Include_Segment_By_Key(key);
      HC_Close_Segment();
      HC_Open_Segment_By_Key(key);

      HC_Close_Segment();
      HC_Pause();
    }

 #endif*/
/*
Color number (present if not BYLAYER); 
zero indicates the BYBLOCK (floating) color; 
256 indicates BYLAYER; 
a negative value indicates that the layer is turned off (optional).
  */

const char *DXFcolor(int c){ // WRONG but at least it wont overflow bounds
    if(c<0)
        c=-c;
    if(c>=256)
        return dxfcolorList[255];
    return dxfcolorList[c];
}

char *Create_DXF_Attributes(struct s_dxfentity *ent) { // allocates. Need to free the rv later.
    char *a;
    char *h;
    char *l;
    QString qe;
    char *x=NULL;
    int L;
    /* First, xdata group 1000
strictly, should be ALL group 1000s between 1001 RELAX and the next group 0 

We first look for a DrawingLayer entity.
If we find one we append its atts.
If we don't find one we look in the environment.
If we dont find one there we put up a menu to define it.
and if we find it (ie user didnt cancel) we create a DrawingLayer entity.
Then getenv from the group name
*/

    class RXSail *sail = ent->m_sail;
    RXEntity_p atte =sail->Entity_Exists(ent->group[8].value,"drawinglayer");
    if(atte) {
        qe =  atte->AttributeString();
    }
    else {
        qe = getenv(strtoupper(ent->group[8].value));

        if(qe.isEmpty()) {
            char buff[256];
            sprintf(buff," envEdit:GAUSS: %s" ,strtoupper(ent->group[8].value));
            Execute_Script_Line((char*)buff,(Graphic *)NULL);
            qe = getenv(strtoupper(ent->group[8].value));
        }
        if( ! qe.isEmpty()) {
            char buff[1256]; int depth=0;
            sprintf(buff,"drawinglayer:%s:%s" ,ent->group[8].value, qPrintable (qe) );
            atte = Just_Read_Card(sail,buff,"drawinglayer",ent->group[8].value ,&depth );
            atte->Resolve();
        }
    } // no atte

    h = ent->group[5].value; //printf(" h = '%s'\n",h);
    l = ent->group[8].value; //printf(" L = '%s'\n",l);
    x=STRDUP(ent->xtend[0].value);//printf(" x = '%s'\n",x);
    L=64;
    L+=qe.length();
    if(h) L+=strlen(h);
    if(l) L+=strlen(l);
    if(x) L+=strlen(x);
    a =(char*)MALLOC(L);
    assert(a);
    strcpy(a,"dxf");
    if(h){
        strcat(a,"(handle="); 	strcat(a,h); 	strcat(a,")");
    }
    if(l){
        strcat(a,"(layer="); 	strcat(a,l); 	strcat(a,")");
    }
    if(x){
        strcat(a," X>"); 	strcat(a,x);
    }
    if(qe.length()){
        strcat(a," E>"); 	strcat(a,qPrintable(qe));
    }
    if(rxIsEmpty(a))
        strcpy(a, "dxf flat XYZabs draw cut"  );
    assert(strlen(a) < (size_t) (L-1));
    RXFREE(x);
    strtolower(a);
    return a;
}
struct TwinEntity Generate_Curve_From_Graphic(SAIL *p_sail,const char*attsin,
                                              const char*handle,char*layer,const char *namein, int c, VECTOR  *p, void*p_ono){

    // p_ono is ONX_Model_Object*
    //May 2003  Finishes it too
    // Sept 2002.  Modified for IGES input.  In this case, the Handle is the entity number.  This is NOT persistent!
    // June 2001.  the keyword '$basecurve' means just generate a 3dcurve
    // the curve is shifted to start at the origin
    // its name doesnt have 'c' appended

    // Feb 2001  the curve is given depth=1 so it is NOT put in the script

    // so edits will be lost.
    // but this is a work-around for a partial-atts bug
    // We tried again with depth 0 and no transfer of atts. IE rely on the partial-atts
    // system.  A test

    /* Generate a 3dcurve entity and a cut curve
3dcurve	x1c  -1	1	0  6	4	0
curve	  x1	 	 	x1c 	0 	flat XYZabs draw cut
The Curve is made with depth=0 so it will be put in the script file for editing.
The 3DCurve is not.
Hence the 3DCurve may be edited in ACAD and picked up again.
The CURVE may be edited in the scriptfile and will not be overwritten.

*/

    int depth=1 ;
    int SC_Too=1;
    unsigned int l_k;
    RXSTRING name,n1,n2,extra;
    char buf[256];


    RXAttributes atts;
    RXSTRING stype;
    RXEntity_p  bce=NULL;
    class RXSeamcurve *sce=NULL;
    char Bcurve_Append = ' ';
    VECTOR o = {0,0,0};
    int qq;
    struct TwinEntity  rc = {NULL,NULL};
    if(!p) {
        cout << namein<<" has null polyline - cannot use "<<endl;
        return rc;
    }
    //	printf(" Generate a Curve  in layer %s with %d points\n ",ent->group[8].value, c);
    l_k=32;
    if(attsin) l_k += strlen(attsin); // already contains the tail of the 3dm object nam
    if(namein) l_k += strlen(namein); // still has a tail

    if(attsin) atts.Add(attsin);

    if(atts.Find("edge"))
        stype = L"edge";
    else
        stype = L"curve";

    if(atts.Find("$basecurve"))
        SC_Too=0;
    if(namein) {
        qq = strlen(namein);
        name=TOSTRING(namein) ;
    }
    else if(!atts.Extract_Word(TOSTRING("name"),name))
        name= TOSTRING(layer)+TOSTRING("_")+ TOSTRING(handle);// ;sprintf(name,"%s_%s",  layer, handle);

    int ncount=0;
    if(atts.Extract_Word(L"$n1",n1)&&!n1.empty())
        ncount++;
    if(atts.Extract_Word(L"$n2",n2)&&!n2.empty())
        ncount++;
    if(ncount==2)
        extra=L"$nocut,$nosnap,";
    qq = name.length ();
    size_t  q = name.find_first_of(TOSTRING(IGS_NAME_SEPARATOR) );
    if(q != string::npos) name.erase(q);


    if(extra.empty ()) extra=L" ";
#ifdef HOOPS
    Make_Valid_Segname(name);
#endif
    if( atts.Find("$iges")) //||(stristr(atts,"$3dm")))
        Make_Name_Unique(p_sail,stype ,name);
    char * cline =0;
    string nnn = ToUtf8(name);
    if(SC_Too){
        Bcurve_Append = 'c';

        cline =(char*)CALLOC(144+atts.Length () + 2*name.length() + 3*stype.length () + n1.length() +  n2.length() + extra.length()+12, 1);

        sprintf(cline,"%S :%S : %S : %S :%Sc :0 :$%S%S %S ",L"seamcurve",name.c_str(),n1.c_str(),n2.c_str(),name.c_str(),stype.c_str(),extra.c_str(),atts.GetAll().c_str () );
        RXSTRING sline;

        sline =L"seamcurve"; sline +=RXE_LSEPSTOWRITE;
        sline += name;
        sline +=RXE_LSEPSTOWRITE+ n1
                +RXE_LSEPSTOWRITE+ n2
                +RXE_LSEPSTOWRITE+ name+L"c"
                +RXE_LSEPSTOWRITE+L"0"
                +RXE_LSEPSTOWRITE +L"$"
                +stype  + L" " + extra  + L" " +atts.GetAll();

        sprintf(buf,"seamcurve");

        if(  !(p_sail->Entity_Exists(nnn.c_str(),to_string(stype.c_str()).c_str())|| p_sail->Entity_Exists(nnn.c_str(),"seamcurve")   ))
            sce = dynamic_cast<class RXSeamcurve* >(Just_Read_Card(p_sail,cline,"seamcurve",nnn.c_str(),&depth));  // was type
        else
            cout<< stype.c_str()<<", '"<< nnn.c_str() <<"' already exists \n";

        if(p_ono  && sce) sce->m_pONMO =(ONX_Model_Object *)p_ono; // dxf read usually has p_ono null.
    }
    else
        PC_Vector_Copy(p[0],&o);
    // the basecurve

        int oldc=c;
        if(!p_ono)
            if(PCP_Polyline_Remove_Close_Points(p, &c, p_sail->m_Linear_Tol)){
                cout << "Try rebuilding curve: '"<<nnn.c_str()<< "' npts was " <<oldc <<" is "<<c<< " l= "<< PC_Polyline_Length(c,p)  << endl;
            }
        cline = (char*)REALLOC (cline, (128 + 48*c + atts.Length())*sizeof(char));
        sprintf(cline,"3dcurve : %S%c : ", name.c_str(),Bcurve_Append);
        if(p_ono){
            for(l_k=0;l_k<(unsigned int) c;l_k++) {
                sprintf(buf," %f : %f : %f :", p[l_k].x-o.x,p[l_k].y-o.y,p[l_k].z-o.z);
                strcat(cline,buf);
            }
        }
        if(atts.Find("base_mould"))
            strcat(cline,"  $moulded");

        sprintf(buf,"%S%c",  name.c_str(),Bcurve_Append);
        assert(strlen(buf) < 255);
        depth=1;
        // May 20 2008 try inserting it directly as a 'basecurve' to avoid the rename was "3dcurve"
        // slow to call twice
        if( !(p_sail->Entity_Exists(buf, "3dcurve") || p_sail->Entity_Exists(buf, "basecurve"))  )
        {
            bce = Just_Read_Card(p_sail,cline,"3dcurve",buf,&depth);
            //thomas 01.03.04
            bce->m_pONMO = sce->m_pONMO; //peter put it back in - Jan 2014

        }
        else
            cout<<" 3dcurve "<<buf<<" already exists "<< name.c_str()<<endl;


        if(bce && bce->Resolve() )
            bce->Needs_Finishing=!bce->Finish();


    // It would be nice to finish here so the ent becomes visible and selectable,
    // but this brings an order-dependency. Esp. when we want to snap to sites
    // which are further down this file.  We cant do it.
    // so instead we just resolve it...
    // It would be much faster if instead of resolving we pushed a task to a TBB parallel_do
    //

    if(sce) sce->Needs_Resolving= !sce->Resolve(bce);


    RXFREE(cline);
    rc.e1 =bce; rc.e2=sce;
    return rc; // was sce till July 1 2004
} 


HC_KEY dxfin(SAIL *sail, const char *pfilename) 
{
    HC_KEY key;
    char namebuf[256],stmp[256],ext[256],path[256];

    if (!checkdxffile(pfilename)) {
        sprintf(stmp,"<%s> does not look like a DXF file", pfilename);
        sail->OutputToClient(stmp,2);
        if(filein)
            FCLOSE(filein);
        return(0);
    }
    dxfinit();// filename);
    cout<<" TODO: step this"<<endl;
    rxfileparse(pfilename ,path,stmp,ext, 256);
    strcat(stmp,ext);

    strcpy(namebuf,stmp);
    Make_Valid_Segname(namebuf);

    strcpy(dxfsegment,"/DXF/");
    char l_filename[512];
    strncpy(l_filename,stmp,510);
    strcat(dxfsegment,namebuf);

    strcpy(dxflibrary,"?Inclu de_hi storic"); /* Peter Feb 1997 maybe WRONG */
    strcat(dxflibrary,"/DXF/");
    strcat(dxflibrary,namebuf);
    sprintf(stmp,"_%p",sail);
    strcat(dxflibrary,stmp);

#ifndef PETERSMSW
    HC_Open_Segment("?sail");
#else
    HC_Open_Segment_By_Key(PostProcNonExclusive(sail));
#endif
    HC_Open_Segment("dxf");
    HC_Show_Pathname_Expansion(".",dxflibrary);
    printf("dxflibrary is %s\n",dxflibrary);
    HC_Close_Segment();
    HC_Close_Segment();


    HC_Open_Segment(dxflibrary);
    HC_Flush_Contents(dxflibrary,"everything");
    HC_Close_Segment();
    key=HC_KOpen_Segment(dxfsegment);

    HC_Flush_Contents(dxfsegment,"everything");

    G_section=DXF_FALSE;
    getdxfheader();
    getdxflayers();
    // getdxfblocks(sail);
    getdxfentities(sail);

    HC_Close_Segment();
#ifdef DEBUG_DXF
    cout<< " (DXF) that should have been level  0\n"<<endl;
#endif
    FCLOSE(filein);
    return(key);
}

void dxferror(const char *stmp)	/* to link to RELAX rxerror */
{
#ifdef _NOT_MSW
    // fprintf(stderr,"Warning: %s\n",stmp);
#else
    printf("DXFin Warning: %s\n",stmp);
#endif

}

int checkdxffile(const char *filename) /* return 1 if it looks like a dxf file, else return 0 */
{

    char stmp[256];

    filein=RXFOPEN(filename,"r");
    if(!filein) return 0;
    if (fscanf(filein,"%d",&G_group)!=1) return(0);
    if (G_group!=0) return(0);
    if (fscanf(filein,"%s",stmp)!=1) return(0);
    //printf("\t %d '%s'\n", G_group,stmp);
    if (strcmp(stmp,"SECTION")!=0) return(0);

    if (fscanf(filein,"%d",&G_group)!=1) return(0);
    if (G_group!=2) return(0);
    if (fscanf(filein,"%s",stmp)!=1) return(0);
    //printf("\t %d '%s'\n", G_group,stmp);
    if (strcmp(stmp,"HEADER")!=0) return(0);

    //printf("checkdxffile OK on '%s' (%p)\n", filename, filein);
    rewind(filein);
    return(1);			/* it looks like a dxf file */

}

void getdxfheader()
{
    cout<< "getdxfheader"<<endl;
    rewind(filein);
    if (!findsection("HEADER")) {  g_World->OutputToClient("NO header section",2); return; }
    dxferror("in header section\n");
}

void getdxflayers(void)
{
    char stmp[256];
    struct layer *first_layer, *cur_layer,*last_layer,*new_layer;

    if (!findsection("TABLES")) { g_World->OutputToClient("no TABLES section",1); return;}
    strcpy(s_table,"LAYER");
    if (!findtable(s_table)) return;
    HC_Open_Segment(dxfsegment); printf(" getdxflayers opening %s\n", dxfsegment);
    dxferror("in layer table\n");
    fscanf(filein,"%d",&G_group);
    fscanf(filein,"%s",stmp);
    fscanf(filein,"%d",&G_group);
    fscanf(filein,"%s",stmp);
    first_layer=(struct layer *) malloc(sizeof * first_layer);
    last_layer=(struct layer *) malloc(sizeof * last_layer);
    first_layer->next=last_layer;
    last_layer->next=last_layer;
    cur_layer=first_layer;
    while (strcmp(stmp,"LAYER")==0) {
        /* new layer >> new segment */
        fscanf(filein,"%d",&G_group);
        while (G_group!=0) {
            /*   printf(" WHILE group = %d\n",group); */
            fscanf(filein,"%s",stmp); /* printf(" string is %s\n",stmp); */
            switch(G_group){
            case 2: sscanf(stmp,"%s",cur_layer->name); break; /* layer name */
            case 70:  break;  		/* flags */
            case 62: sscanf(stmp,"%d",&cur_layer->color); /* color if <0 layer off */
                /*  printf(" RAW READ OF COLOR %d\n",cur_layer->color);  */

                if (cur_layer->color<0) {
                    cur_layer->color=-cur_layer->color;
                    cur_layer->visible=DXF_FALSE;
                }
                else cur_layer->visible=DXF_TRUE;
                break;
            case 6:sscanf(stmp,"%d",&cur_layer->linetype); break; /* linetype */
            }
            fscanf(filein,"%d",&G_group);
        }
        fscanf(filein,"%s",stmp);   /*  printf(" throw away <%s> on leaving\n",stmp); */

#ifdef DEBUG_DXF
        printf("creating layer and OPENING %s\n",cur_layer->name);
        printf(" COlor %d  %s \n",cur_layer->color,  DXFcolor(cur_layer->color));

#endif 
        Make_Valid_Segname(cur_layer->name);
        HC_Open_Segment(cur_layer->name);
        assert(cur_layer->color> -1 && cur_layer->color<256);
        HC_Set_Color(DXFcolor(cur_layer->color));
        HC_Close_Segment();
        new_layer=(struct layer *) malloc(sizeof * cur_layer);
        new_layer->next=cur_layer->next;
        cur_layer->next=new_layer;
        cur_layer=new_layer;
    }
    last_layer=cur_layer;
    last_layer->next=last_layer;
#ifdef DEBUG_DXF
    cout<< "list of layers:\n"<<endl;
    cur_layer=first_layer;
    while (cur_layer->next!=cur_layer)  {
        printf("layer %s\n",cur_layer->name);
        cur_layer=cur_layer->next;
    }
#endif
    HC_Close_Segment();
    return;
}

void getdxfblocks(SAIL *sail) {  
    int nr;
    struct s_dxfentity curblock,nextentity;
    char blockname[DXF_MAXSTR];
    if (G_section>DXF_BLOCKS) rewind(filein);
    if (!findsection("BLOCKS")) { sail->OutputToClient("no BLOCKS",1); return;}
    dxferror("in block section\n");

    dxfclear(&nextentity);
    dxfclear(&curblock);
    fscanf(filein,"%d",&G_group);
    fscanf(filein,"%s",curblock.group[G_group].value);
    fscanf(filein,"%d",&G_group);
    while (G_group!=0) {
        fscanf(filein,"%s",curblock.group[G_group].value);
        if(!fscanf(filein,"%d",&G_group)) break;
    }


    while (strcmp(curblock.group[0].value,"ENDSEC")!=0) {
        getdxf_one_entity(&curblock,G_group);

        HC_Open_Segment(dxflibrary);
        Make_Valid_Segname(curblock.group[2].value);
        HC_Open_Segment(curblock.group[2].value);
        HC_Show_Pathname_Expansion(".",blockname);
        HC_Close_Segment();
        HC_Close_Segment();
        if(! fscanf(filein,"%s",nextentity.group[0].value)) {
            cout<< " couldnt read file\n"<<endl;	   break;
        }

        /* Peter  I dont know why, but we can get an ENDSEC here. I guess we should break */
        if(strcmp( nextentity.group[0].value,"ENDSEC" )==0) { cout<< " Peters ENDSEC break\n"<<endl; break;}
        while (strcmp(nextentity.group[0].value,"ENDBLK")!=0) {
            if(EOF== fscanf(filein,"%d",&G_group)) break;
            getdxf_one_entity(&nextentity,G_group);
            nextentity.m_sail = sail;
            strcpy(segment,blockname);
            if(!creategeom(&nextentity)) break;
            dxfclear(&nextentity);
            if(EOF==fscanf(filein,"%s",nextentity.group[0].value)) break;
        }
        dxfclear(&curblock);
        while (G_group!=0) {
            fscanf(filein,"%d",&G_group);
            fscanf(filein,"%s",curblock.group[G_group].value);
        }
        nr = fscanf(filein,"%d",&G_group);
        if(!nr || (nr==EOF)) break;


    }

#ifdef DEBUG_DXF
    cout<< "Leave getdxfblocks \n"<<endl;
#endif 
    return;
}
int dxf_place_group(struct s_dxfentity *dxfentity,int group,char *buf){
    if (group < 80)
        strcpy(dxfentity->group[group].value,buf);
    else if (group < 240 )
        strcpy(dxfentity->plane[group-200].value,buf);
    else if (group==999 )
        strcpy(dxfentity->comment,buf);
    else if (group > 1080) {
        char str[512];
        sprintf(str,"Ignore DXF entity group %d\n %s", group,buf);
        dxfentity->m_sail->OutputToClient(str,1);
    }
    else if(group >=1000) {
        if(strlen(buf)>0 && ( buf[strlen(buf)-1]=='\n' || buf[strlen(buf)-1]==13) )
            buf[strlen(buf)-1]=0;

        PC_Strip_Leading(buf); PC_Strip_Trailing(buf);
        if(strlen(dxfentity->xtend[group-1000].value) + strlen(buf) < DXF_MAXSTR)
            strcat(dxfentity->xtend[group-1000].value,buf);
        else
            dxfentity->m_sail->OutputToClient("truncating xdata",2);

    }
    else {
        ON_String str;
        str.Format(" don't know about DXF group %d",group);
        dxfentity->m_sail->OutputToClient(str,2);
    }

    return 1;
}
int dxf_read_group(struct s_dxfentity *dxfentity,int group){

    char  buf[256];
    if(group >=1000 && group < 1080)
        PC_fgets( buf, 256, filein,"\r\n" );
    else
        fscanf(filein,"%s",buf);

    return(dxf_place_group(dxfentity, group,buf));

    if (group < 80) fscanf(filein,"%s",dxfentity->group[group].value);
    else if (group < 240 )
        fscanf(filein,"%s",dxfentity->plane[group-200].value);
    else if (group==999 ) fscanf(filein,"%s",dxfentity->comment);
    else if (group > 1080) {
        char str[512];
        fscanf(filein,"%s",buf);
        sprintf(str,"Ignore DXF entity group %d\n %s", group,buf);
        dxfentity->m_sail->OutputToClient(str,1);

    }
    else {
        if(PC_fgets( buf, 256, filein,"\r\n" )){
            if(strlen(buf)>0 && ( buf[strlen(buf)-1]=='\n' || buf[strlen(buf)-1]==13) ){

                buf[strlen(buf)-1]=0;
            }
            PC_Strip_Leading(buf); PC_Strip_Trailing(buf);
            if(strlen(dxfentity->xtend[group-1000].value) + strlen(buf) < DXF_MAXSTR)
                strcat(dxfentity->xtend[group-1000].value,buf);
            else
                dxfentity->m_sail->OutputToClient("truncating xdata",2);
        }
    }

    return 1;
}
int getdxf_one_entity(struct s_dxfentity *dxfentity,int group){ 

    int ItsASpline = strieq( dxfentity->group[0].value ,"SPLINE");
    while (group!=0) {
        dxf_read_group(dxfentity,group);
        fscanf(filein,"%d",&group);
        if((group==40 || group == 10 ) && ItsASpline)  // nasty heuristic for RHino splines. Order-dependent
            return group;
    }
    return group;
}

void getdxfentities(SAIL *sail) {
    struct s_dxfentity curentity;
    int iret;
    dxfclear(&curentity);
    // if (section>DXF_ENTITIES)
    rewind(filein);
    if (!findsection("ENTITIES")) {sail->OutputToClient("no ENTITIES section",1); return;}

    dxferror("in entities section\n");

    fscanf(filein,"%d",&G_group);
    fscanf(filein,"%s",curentity.group[0].value);
    while (strcmp(curentity.group[0].value,"ENDSEC")!=0) {

        fscanf(filein,"%d",&G_group);

        iret = getdxf_one_entity(&curentity,G_group);
        if( strieq( curentity.group[0].value ,"SPLINE"))
            G_group = iret;

        /* this is horrible logic.  SPline entities contain multiple groups.
    All other R12 entities have single groups (except xdata)
    returning into 'group' stuffs up most other entities
    */
        strcpy(segment,dxfsegment);
        curentity.m_sail = sail;
        if(!creategeom(&curentity))break;
        dxfclear(&curentity);
        fscanf(filein,"%s",curentity.group[0].value);
#ifdef DEBUG_DXF
        printf("\nnext word <%s>\n",curentity.group[0].value);
#endif
        if(!strcmp(curentity.group[0].value,"EOF")) { cout<< " break for EOF\n"<<endl; break;}
    }
}

int creategeom(struct s_dxfentity *dxfentity)
{
#ifdef DEBUG_DXF
    printf(" creategeom with %s\n", dxfentity->group[0].value);
#endif
    if (strcmp(dxfentity->group[0].value,"LINE")==0) newline(dxfentity);
    else if (strcmp(dxfentity->group[0].value,"POLYLINE")==0) newpolyline(dxfentity);
    else if (strcmp(dxfentity->group[0].value,"SPLINE")==0) newdxfspline(dxfentity);
    else if (strcmp(dxfentity->group[0].value,"VERTEX")==0) newvertex(dxfentity);
    else if (strcmp(dxfentity->group[0].value,"POINT")==0) newpoint(dxfentity);
    else if (strcmp(dxfentity->group[0].value,"3DFACE")==0) new3dface(dxfentity);
    else if (strcmp(dxfentity->group[0].value,"CIRCLE")==0) newcircle(dxfentity);
    else if (strcmp(dxfentity->group[0].value,"ARC")==0) newarc(dxfentity);
    else if (strcmp(dxfentity->group[0].value,"INSERT")==0) newinsert(dxfentity);
    else {
        //printf(" entity unknown yet. <%s>\n",dxfentity->group[0].value);
        /*    if(is_empty( dxfentity->group[0].value  ))
         return 0; */
    }
    return 1;
}

void dxFOPENsegment(char *segm,struct s_dxfentity *ent)
{
    int color;

    strcat(segm,"/");
    Make_Valid_Segname(ent->group[8].value);
    strcat(segm,ent->group[8].value);
    strcat(segm,"/");
    Make_Valid_Segname(ent->group[62].value);
    strcat(segm,ent->group[62].value);
    HC_Open_Segment(segm);
#ifdef DEBUG_DXF
    printf("dxFOPENsegment. %s \n",segm);
    if(wierd) List_Open_Segments();
#endif

    if (sscanf(ent->group[62].value,"%d",&color)>0){
        HC_Set_Color(DXFcolor(color));

#ifdef DEBUG_DXF
        printf(" Color  set to  %d %s on seg\n",color, DXFcolor(color));
#endif
    }
}
//void hoopsface(struct s_point p1, struct s_point p2,float thickness)
//{
//    struct s_point p[4];
//    p[0]=p[3]=p1;
//    p[1]=p[2]=p2;
//    p[2].z=p[2].z+thickness;
//    p[3].z=p[3].z+thickness;

//    HC_Insert_Polygon(4,p);
//#ifdef DEBUG_DXF
//    cout<< "hoopsface\n"<<endl;
//#endif
//}
void newline(struct s_dxfentity *dxfentity)
{
    struct s_line line;
    float thickness;

#ifdef DEBUG_DXF
    cout<< "new line.\n"<<endl;
#endif

    sscanf(dxfentity->group[10].value,"%f",&line.p1.x);
    sscanf(dxfentity->group[20].value,"%f",&line.p1.y);
    sscanf(dxfentity->group[30].value,"%f",&line.p1.z);
    sscanf(dxfentity->group[11].value,"%f",&line.p2.x);
    sscanf(dxfentity->group[21].value,"%f",&line.p2.y);
    sscanf(dxfentity->group[31].value,"%f",&line.p2.z);
    sscanf(dxfentity->group[39].value,"%f",&thickness);

    dxFOPENsegment(segment,dxfentity);
    /*  is_oriented=dxforientation(dxfentity);	*/

    RX_Insert_Line(line.p1.x,line.p1.y,line.p1.z,line.p2.x,line.p2.y,line.p2.z,dxfentity->m_sail->GNode()  );

    /* if (is_oriented)  HC_Close_Segment();	   */
    HC_Close_Segment();

#ifdef DEBUG_DXF
    dxftrace(dxfentity);
#endif
}



void newpoint(struct s_dxfentity *dxfentity)
{
    struct s_point p;
    char  *handle,*layer,line[512], *x,*y,*z;
    RXAttributes atts;
    const char *type;
    RXSTRING name;
    int depth=1;
#ifdef DEBUG_DXF
    cout<< "new point.\n"<<endl;
#endif

    sscanf(dxfentity->group[10].value,"%f",&p.x);
    sscanf(dxfentity->group[20].value,"%f",&p.y);
    sscanf(dxfentity->group[30].value,"%f",&p.z);
    handle = dxfentity->group[5].value;
    layer = dxfentity->group[8].value;
    if(strncmp(dxfentity->group[8].value,"GAUSS",5)==0 ){

        atts.Add (Create_DXF_Attributes(dxfentity));

        x = dxfentity->group[10].value;
        y = dxfentity->group[20].value;
        z = dxfentity->group[30].value;
        type="site";
        if(!atts.Extract_Word(L"name",name))
            name=TOSTRING(layer)+TOSTRING("_")+TOSTRING(handle); //sprintf(name,"%s_%s",  layer, handle);
        sprintf(line,"site : %S : %s  : %s :%s :%S ", name.c_str(), x,y,z ,atts.GetAll().c_str());

        if(!(dxfentity->m_sail-> Entity_Exists(  ToUtf8(name).c_str(),type)  ))
            Just_Read_Card(dxfentity->m_sail,line,type,ToUtf8(name).c_str(),&depth);
        //else
        //	printf(" %s %s already exists \n", type,name);
    } // if GAUSS

    dxFOPENsegment(segment,dxfentity);
    RX_Insert_Marker(p.x,p.y,p.z, dxfentity->m_sail->GNode() );
    HC_Close_Segment();

#ifdef DEBUG_DXF
    dxftrace(dxfentity);
#endif
}

void new3dface(struct s_dxfentity *dxfentity)
{
    struct s_point p[4];

#ifdef DEBUG_DXF
    cout<< "new 3dface.\n"<<endl;
#endif

    sscanf(dxfentity->group[10].value,"%f",&p[0].x);
    sscanf(dxfentity->group[20].value,"%f",&p[0].y);
    sscanf(dxfentity->group[30].value,"%f",&p[0].z);
    sscanf(dxfentity->group[11].value,"%f",&p[1].x);
    sscanf(dxfentity->group[21].value,"%f",&p[1].y);
    sscanf(dxfentity->group[31].value,"%f",&p[1].z);
    sscanf(dxfentity->group[12].value,"%f",&p[2].x);
    sscanf(dxfentity->group[22].value,"%f",&p[2].y);
    sscanf(dxfentity->group[32].value,"%f",&p[2].z);
    sscanf(dxfentity->group[13].value,"%f",&p[3].x);
    sscanf(dxfentity->group[23].value,"%f",&p[3].y);
    sscanf(dxfentity->group[33].value,"%f",&p[3].z);

    dxFOPENsegment(segment,dxfentity);
    HC_KInsert_Polygon(4,&(p[0].x),dxfentity->m_sail->GNode());
    HC_Close_Segment();

#ifdef DEBUG_DXF
    dxftrace(dxfentity);
#endif
}

void newcircle(struct s_dxfentity *dxfentity) /* add a circle */
/* missing plane orientation flags 210 220 230 */
/* circle are in an horizontal plane*/
{
    struct s_point cp,bp,mp,ep;	/* Center Point, Begin Point, Mid Point, End Point, Normal vector; */
    float radius;			/* radius ; */

#ifdef DEBUG_DXF
    cout<< "new circle.\n"<<endl;
#endif

    sscanf(dxfentity->group[10].value,"%f",&cp.x);
    sscanf(dxfentity->group[20].value,"%f",&cp.y);
    sscanf(dxfentity->group[30].value,"%f",&cp.z);
    sscanf(dxfentity->group[40].value,"%f",&radius);

    bp.x=cp.x+radius;bp.y=cp.y;bp.z=-cp.z;/* WRONG fixup for pontevedra
*/
    mp.x=cp.x;mp.y=cp.y+radius;mp.z=-cp.z;
    ep.x=cp.x-radius;ep.y=cp.y;ep.z=-cp.z;

    dxFOPENsegment(segment,dxfentity);
    G_is_oriented=dxforientation(dxfentity);
#ifdef HOOPS
    HC_Insert_Circle(&bp,&mp,&ep);
#endif
    if (G_is_oriented)     HC_Close_Segment();
    HC_Close_Segment();

#ifdef DEBUG_DXF
    dxftrace(dxfentity);
#endif
}

int dxforientation(struct s_dxfentity *dxfent)
{

    ON_3dVector norm,Ax,Ay,Az,Wy,Wz,Ax1,Ay1;
    float mymatrix[4][4];
    float Imatrix[4][4];
    int i,j,k;

    /* WRONG Wx, Wy are undefined */
    //Wx.x=1.0; Wx.y=0.0; Wx.z=0.0;
    Wy.x=0.0; Wy.y=1.0; Wy.z=0.0;
    Wz.x=0.0; Wz.y=0.0; Wz.z=1.0;


    i=sscanf(dxfent->plane[10].value,"%lf",&norm.x);
    j=sscanf(dxfent->plane[20].value,"%lf",&norm.y);
    k=sscanf(dxfent->plane[30].value,"%lf",&norm.z);
    if(i!=1 || j!=1 || k !=1) {
        cout<< " (DXFin) failed to read normal vector\n"<<endl;
        return(0);
    }
    Az=norm; if(!Az.Unitize ()) return 0;
    // if (HC Compute_Normalized_Vector(&norm,&Az)==0) return(0);

    if ((fabs( Az.x)<1.0/64.0)&&(fabs( Az.y)<1.0/64.0)){

        Ax = ON_CrossProduct(Wy,norm);  //HC Compute_Cross_Product(&Wy,&norm,&Ax);
        if( fabs( Az.z) < 1.0/64.0) {
            dxfent->m_sail->OutputToClient (" SHORT REF VECTOR",2);
            return(0);
        }
    }
    else
    {Ax=ON_CrossProduct(Wz,norm); }// HC Compute_Cross_Product(&Wz,&norm,&Ax); }

    Ax1 =Ax ; Ax1.Unitize (); // ; HC Compute_Normalized_Vector(&Ax,&Ax1);
    Ay=ON_CrossProduct(norm,Ax1); // HC Compute_Cross_Product(&norm,&Ax1,&Ay);
    Ay1 = Ay; Ay1.Unitize (); // HC Compute_Normalized_Vector(&Ay,&Ay1);

    mymatrix[0][0]=Ax1.x;
    mymatrix[1][0]=Ax1.y;
    mymatrix[2][0]=Ax1.z;
    mymatrix[3][0]=0.0;
    mymatrix[0][1]=Ay1.x;
    mymatrix[1][1]=Ay1.y;
    mymatrix[2][1]=Ay1.z;
    mymatrix[3][1]=0.0;
    mymatrix[0][2]=Az.x;
    mymatrix[1][2]=Az.y;
    mymatrix[2][2]=Az.z;
    mymatrix[3][2]=0.0;
    mymatrix[0][3]=0.0;
    mymatrix[1][3]=0.0;
    mymatrix[2][3]=0.0;
    mymatrix[3][3]=1.0;


    HC_Open_Segment("");
#ifdef HOOPS
    HC_Compute_Matrix_Inverse(&(mymatrix[0][0]),&(Imatrix[0][0]));
    HC_Set_Modelling_Matrix(&(Imatrix[0][0]));
#endif

    return(1);

}

void newarc(struct s_dxfentity *dxfentity) /* missing plane orientation flags 210 220 230 */
/* arcs are in an horizontal plane*/

{
    struct s_point cp, bp, mp, ep;	/* Center Point, Begin Point, Mid Point, End Point; */
    float radius,start,end,da;		/* radius , start angle, end angle; */
#ifdef DEBUG_DXF
    cout<< "new arc.\n"<<endl;
#endif

    sscanf(dxfentity->group[10].value,"%f",&cp.x);
    sscanf(dxfentity->group[20].value,"%f",&cp.y);
    sscanf(dxfentity->group[30].value,"%f",&cp.z);
    sscanf(dxfentity->group[40].value,"%f",&radius);
    sscanf(dxfentity->group[50].value,"%f",&start);
    sscanf(dxfentity->group[51].value,"%f",&end);

    if(end < start) end =end+(float) 360.0;
    start = (start )* (float) (atan(1.0) /45.0);
    end =   (end   )* (float) (atan(1.0) /45.0);

    da = (end-start)/(float)2.0;

    bp.x=cp.x+radius*(float) cos(start);
    bp.y=cp.y+radius*(float)sin(start);
    ep.x=cp.x+radius*(float)cos(end);
    ep.y=cp.y+radius*(float)sin(end);
    mp.x=cp.x+radius*(float)cos(start+da);
    mp.y=cp.y+radius*(float)sin(start+da);
    bp.z=ep.z=mp.z=cp.z;


    dxFOPENsegment(segment,dxfentity);
    G_is_oriented=dxforientation(dxfentity);
#ifdef HOOPS
    HC_Insert_Circular_Arc(&bp,&mp,&ep);
#endif
    if (G_is_oriented)  HC_Close_Segment();
    HC_Close_Segment();

#ifdef DEBUG_DXF
    dxftrace(dxfentity);
#endif

}

int read_next_attr(char*tag, char*value) {
    /* return 0 if a SEQEND attained */

    int group=0;
    char item[256];

    strcpy(value,"undefined");
    strcpy(tag,"tag");

    while(1){
        fscanf(filein,"%s",item);
#ifdef DEBUG_DXF
        printf(" \tnext attrlist %d  %s\n", group,item);
#endif
        if(group==1) strcpy(value,item);
        if(group==2) strcpy(tag,item);
        if(strcmp(item,"SEQEND")==0)  return 0;
        fscanf(filein,"%d",&group);
        if(!group) return 1;
    }

    return (2);
}

void newinsert(struct s_dxfentity *dxfentity) 
{
    struct s_point p;
    float  rz, sx, sy, sz;
    int aff;
    char tag[256], value[256],string[512],stmp[256];


    sscanf(dxfentity->group[10].value,"%f",&p.x);
    sscanf(dxfentity->group[20].value,"%f",&p.y);
    sscanf(dxfentity->group[30].value,"%f",&p.z);
    if (sscanf(dxfentity->group[41].value,"%f",&sx)==0) sx=1.0;
    if (sscanf(dxfentity->group[42].value,"%f",&sy)==0) sy=1.0;
    if (sscanf(dxfentity->group[43].value,"%f",&sz)==0) sz=1.0;
    if (sscanf(dxfentity->group[50].value,"%f",&rz)==0) rz=0.0;
    if (sscanf(dxfentity->group[66].value,"%d",&aff)==0) aff=0;

    dxFOPENsegment(segment,dxfentity); /* open a segment so don't forget to close it */

    strcpy(stmp,dxflibrary);
    strcat(stmp,"/");
    Make_Valid_Segname(dxfentity->group[2].value);
    strcat(stmp,dxfentity->group[2].value);

    HC_Open_Segment("");
    G_is_oriented=dxforientation(dxfentity);
#ifdef HOOPS
    HC_Scale_Object(sx,sy,sz);
    HC_Rotate_Object(0.0,0.0,rz);
    HC_Translate_Object(p.x,p.y,p.z);

    if(HC_QShow_Existence(stmp,"self")) {
#ifdef DEBUG_DXF
        printf("insert <%s>\n",stmp);
#endif
        HC_Include_Segment(stmp);

        if(aff !=0) {
            printf(" Start attr list read on layer '%s'\n", dxfentity->group[8].value);
            string[0]=0;
            fscanf(filein,"%s", string);
            printf(" before attlist dumping '%s'\n",  string);

            while(read_next_attr(tag,value)) {
                Make_Valid_Segname(tag);
                printf(" type %s   %s=%s\n", dxfentity->group[2].value, tag,value);
                HC_Open_Segment(dxfentity->group[2].value);
                sprintf(string,"%s=%s",tag,value);
                HC_Set_User_Options(string);
                HC_Close_Segment();
            }
            cout<< " that should have been SEQEND\nFinished attr list read\n"<<endl;
            /* there may be a tail to the 'insert' entity. Clean it up */
            while(1== fscanf(filein," %d",&G_group)) {
                if(!G_group) break;
                fscanf(filein,"%s", string);
                printf(" dumping %d '%s'\n", G_group, string);
            }

        }
    }
    else
        printf(" INSERT wants to include nonexistent %s\n", stmp);
#endif
    if (G_is_oriented)  HC_Close_Segment();
    HC_Close_Segment();

    HC_Close_Segment();		/* close of dxFOPENsegment */




}
int dxf_append_to_list(char *s, double **x,int *mx,int *nx,int *npts){
    double v;
    char *lp;
    v = strtod(s, &lp);
    if(*mx <*nx) *x = (double*)REALLOC(*x,*mx*sizeof(double));
    (*x)[*nx]=v; (*nx)++;
    if(*nx > *npts) g_World->OutputToClient(" too many pts in dxf list",3);

    return 1;
}


int newdxfspline(struct s_dxfentity *dxfentity) 
{
    VECTOR  st,et ,  *poly=NULL;
    int flag,degree,nkts,ncp,nfp,c=0,c1,c2;
    float ktol,cptol,fptol;
    double *x=NULL,*y=NULL,*z=NULL, *k=NULL; //FEb 2003  MSW detailed warnings
    char v[256], *atts;
    int mx=0,my=0,mz=0,mk=0;
    int nx=0,ny=0,nz=0,nk=0;
    struct PC_NURB n;

    /*
210,220, 230	 Normal vector (omitted if the spline is nonplanar)
70
 Spline flag (bit coded):
1 = Closed spline
2 = Periodic spline
4 = Rational spline
8 = Planar
16 = Linear (planar bit is also set) 
71	 Degree of the spline curve
72	 Number of knots
73	 Number of control points
74	 Number of fit points (if any)

42	 Knot tolerance (default = 0.0000001)
43	 Control-point tolerance (default = 0.0000001)
44	 Fit tolerance (default = 0.0000000001)

12 ,22, 32	 Start tangent--may be omitted (in WCS)
13,23, 33	 End tangent--may be omitted (in WCS).

40	 Knot value (one entry per knot)
41	 Weight (if not 1); with multiple group pairs, are present if all are not 1

10	 Control points (in WCS) one entry per control point.
20, 30	 DXF: Y and Z values of control points (in WCS) (one entry per control point)

11	 Fit points (in WCS) one entry per fit point.
21, 31	 DXF: Y and Z values of fit points (in WCS) (one entry per fit point)

  */
    sscanf(dxfentity->group[70 ].value,"%d",&flag  );
    sscanf(dxfentity->group[71 ].value,"%d",&degree  );
    sscanf(dxfentity->group[72 ].value,"%d",&nkts  );
    sscanf(dxfentity->group[73 ].value,"%d",&ncp  );
    sscanf(dxfentity->group[74 ].value,"%d",&nfp  );
    sscanf(dxfentity->group[42 ].value,"%f",&ktol  );
    sscanf(dxfentity->group[43 ].value,"%f",&cptol  );
    sscanf(dxfentity->group[44 ].value,"%f",&fptol  );
    sscanf(dxfentity->group[12 ].value,"%f",&(st.x)  );
    sscanf(dxfentity->group[22 ].value,"%f",&(st.y)   );
    sscanf(dxfentity->group[32 ].value,"%f",&(st.z)   );
    sscanf(dxfentity->group[13 ].value,"%f",&(et.x)   );
    sscanf(dxfentity->group[23 ].value,"%f",&(et.y)   );
    sscanf(dxfentity->group[33 ].value,"%f",&(et.z)   );

    assert(flag!=1 && flag !=2);
    // fscanf(filein,"%d",&group);

    if(nfp > 0) return 0;

    // now groups 10,20,30,40,41
    if(ncp) {
        x = (double*)MALLOC(ncp*sizeof(double)); mx=my=mz=ncp;
        y = (double*)MALLOC(ncp*sizeof(double));
        z = (double*)MALLOC(ncp*sizeof(double));
    }
    if(nkts) {
        k = (double*)MALLOC(nkts*sizeof(float)); mk=nkts;
    }


    while (G_group) {
        fscanf(filein,"%s",v);
        switch(G_group) {
        case 10: //x
            dxf_append_to_list(v,&x,&mx,&nx,&ncp);
            break;
        case 20://y
            dxf_append_to_list(v,&y,&my,&ny,&ncp);
            break;
        case 30: //z
            dxf_append_to_list(v,&z,&mz,&nz,&ncp);
            break;
        case 40: //k
            dxf_append_to_list(v,&k,&mk,&nk,&nkts);
            break;
        case 41: //w
            break;
        default:
            dxf_place_group(dxfentity,G_group,v);
        }
        fscanf(filein,"%d",&G_group);
    }

    dxFOPENsegment(segment,dxfentity);
    c1 = List_Open_Segments();
    if (G_thickness==0)  {
        if(ncp >= 2) {
            if(!strncasecmp(dxfentity->group[8].value,"gauss",5) ){
                n.cP=NULL;
                Generate_NURBS_From_Dxf_Spline(dxfentity,x,y,z,k,ncp,nkts,degree,&n);

                PCN_OneD_NURBS_To_Poly(&n,&c,&poly);
                if(n.cP){
                    RXFREE(n.cP[0]);
                    RXFREE(n.cP);
                }
                atts = Create_DXF_Attributes(dxfentity);
                Generate_Curve_From_Graphic(dxfentity->m_sail,atts,dxfentity->group[5].value,dxfentity->group[8].value ,NULL, c, poly);
                RXFREE(poly);
                if(atts) RXFREE(atts);
            }

        }
        else
            printf(" SHORT SPLINE SKIPPED (only %d pts\n", ncp);
    }
    c2 = List_Open_Segments();

    if(c2 != c1)
        dxfentity->m_sail->OutputToClient(" seg error",3);
    if(c2==5)
        HC_Close_Segment();
    else {
        dxfentity->m_sail->OutputToClient(" whats happening",3); fflush(stdout);
        HC_Exit_Program();
    }
    if(x) RXFREE(x); if(y) RXFREE(y);if(z) RXFREE(z); if(k) RXFREE(k);
    return 1;
}
int newpolyline(struct s_dxfentity *dxfentity) 
{
    VECTOR  v;
    VECTOR  *poly=NULL;
    char stmp[256];
    int Edges_Visible=0, fcount=0, *faces=NULL, NurbsEntity =0;
    int F_Alloc=0, V_Alloc=0;
    struct s_dxfentity nextvertex;
    int vertexfollows,nbpts,flag,meshtype=0,isExtruded=0;
    float mymatrix[4][4];
#ifdef DEBUG_DXF
    char curseg[512];
    HC_Show_Pathname_Expansion(".",curseg);
    printf("newpolyline at  CurSeg= %s\n",curseg);
    if(wierd) List_Open_Segments();
#endif 
    sscanf(dxfentity->group[66].value,"%d",&vertexfollows);

    if (!vertexfollows) return 0;
    sscanf(dxfentity->group[70].value,"%d",&flag);
    sscanf(dxfentity->group[39].value,"%f",&G_thickness);
    nbpts=0;
    dxfclear(&nextvertex);
    if(flag&16)
        sscanf(dxfentity->group[75].value,"%d",&meshtype);
#ifdef _DEBUG_DXF
    printf(" flag is %d\n ",flag);
    if(flag&1) rxerror("this is a closed Polyline or a polygon mesh closed in M direction",1);
    if(flag&2) rxerror("curve-fit vertices have been added",1);
    if(flag&4) rxerror("Spline-fit vertices have been added",1);
    if(flag&8) rxerror("This is a 3D polyline",1);
    else {
        rxerror("This is a 2D polyline",1);
    }


    if(flag&16) {
        if(meshtype==0) rxerror("this is a 3D polygon mesh. No smooth surface",1);
        if(meshtype==5) rxerror("this is a 3D polygon mesh. Quadratic bspline surface",1);
        if(meshtype==6) rxerror("this is a 3D polygon mesh. cubic B-spline surface",1);
        if(meshtype==8) rxerror("this is a 3D polygon mesh. Bezier surface",1);
    }
    if(flag&32) rxerror("this is a polygon mesh closed in the N direction",1);
    if(flag&64) rxerror("this is a polyface mesh",1);
#endif 
    if(!(flag&8)) {
        if(dxforientation(dxfentity)){
            isExtruded=1;
#ifdef HOOPS
            HC_Show_Modelling_Matrix(&(mymatrix[0][0]));
#endif
            HC_Close_Segment();
        }
    }
    fscanf(filein,"%d",&G_group);

    if(flag&64) {  /* polyface mesh */
        int vflag,fc=0,i1,i2,i3,i4, nedg;

        sscanf(dxfentity->group[71].value,"%d",&V_Alloc);
        sscanf(dxfentity->group[72].value,"%d",&F_Alloc);
        //printf(" polyface mesh %d vertices, %d faces\n", V_Alloc, F_Alloc);


        while (strcmp(nextvertex.group[0].value,"SEQEND")!=0) {
            getdxf_one_entity(&nextvertex,G_group);
            sscanf(nextvertex.group[70].value,"%d",&vflag);
            if(vflag&64) {
                sscanf(nextvertex.group[10].value,"%f",&v.x);
                sscanf(nextvertex.group[20].value,"%f",&v.y);
                sscanf(nextvertex.group[30].value,"%f",&v.z);
                /*    		    printf("vertex %d  (%f %f %f\n",vc,v.x,v.y,v.z); */
                Append_To_Poly(&poly,&nbpts,&v,1);
            }
            if((vflag&128) &&!  (vflag&64) ) {
                sscanf(nextvertex.group[71].value,"%d",&i1);
                sscanf(nextvertex.group[72].value,"%d",&i2);
                sscanf(nextvertex.group[73].value,"%d",&i3);
                sscanf(nextvertex.group[74].value,"%d",&i4);
                nedg = (i1 !=0) +  (i2 !=0) +  (i3 !=0) +  (i4 !=0);
                Edges_Visible=Edges_Visible || ((i1 >0) ||  (i2 >0) || (i3 > 0) || (i4 >0));
                faces=(int*)REALLOC(faces, (fcount+nedg+1)*sizeof(int));
                faces[fcount++] = nedg;
                if(i1!=0) faces[fcount++] = abs(i1)-1;
                if(i2!=0) faces[fcount++] = abs(i2)-1;
                if(i3!=0) faces[fcount++] = abs(i3)-1;
                if(i4!=0) faces[fcount++] = abs(i4)-1;
                fc++;
            }


            strcpy(dxfentity->group[62].value,nextvertex.group[62].value);

            dxfclear(&nextvertex);
            fscanf(filein,"%s",nextvertex.group[0].value);
            fscanf(filein,"%d",&G_group);
        }
        // printf("Finished reading a polyface mesh with %d vertices, %d  faces Edge Vis=%d\n", nbpts, fc,Edges_Visible );

    }
    else if(flag&16) {  /* polygon mesh */
        int vflag, vc=0,fc=0,i1,i2,i3,i4;
        int SplineFitVerticesExist = (flag&4);
        int TakeIt;
        /* meshtype (g75 was read if flag&16)
  Curves and smooth surface type (optional; default = 0);
  integer codes, not bit-coded:
0 = No smooth surface fitted
5 = Quadratic B-spline surface
6 = Cubic B-spline surface
8 = Bezier surface
I don't think this affects the contents: just what you do with them. 
*/
        /*
Vertex flags:
1 = Extra vertex created by curve-fitting
2 = Curve-fit tangent defined for this vertex. 
  A curve-fit tangent direction of 0 may be omitted from DXF output
  but is significant if this bit is set.
4 = Not used
8 = Spline vertex created by spline-fitting
16 = Spline frame control point
32 = 3D polyline vertex
64 = 3D polygon mesh
128 = Polyface mesh vertex
  */
        /*
  We only take vertices with 64 set.
  furthermore, if SplineFitVerticesExist we
  separate out
  Vertices with 16 are control-graph points	(g71 x g72 off)
  Vertices with 8 are surface points			(g73 x g74 off)
  Otherwise, if SplineFitVertices dont Exist we take them all
  */
        sscanf(dxfentity->group[71].value,"%d",&V_Alloc);
        sscanf(dxfentity->group[72].value,"%d",&F_Alloc);

        poly = (VECTOR *)MALLOC((V_Alloc+2) *( F_Alloc+2) * sizeof(VECTOR));

        while (strcmp(nextvertex.group[0].value,"SEQEND")!=0) {
            getdxf_one_entity(&nextvertex,G_group);
            sscanf(nextvertex.group[70].value,"%d",&vflag);
            TakeIt =(( !SplineFitVerticesExist && (vflag&64)) || (SplineFitVerticesExist && (vflag&16)));
            if(TakeIt) {
                sscanf(nextvertex.group[10].value,"%f",&(poly[vc].x));
                sscanf(nextvertex.group[20].value,"%f",&(poly[vc].y));
                sscanf(nextvertex.group[30].value,"%f",&(poly[vc].z));
                if(vc <= (V_Alloc * F_Alloc))	vc++;
                else dxfentity->m_sail->OutputToClient(" inconsistent vertex count in   polygon mesh   ",2);
            }
            else if(vflag&128 ) {
                sscanf(nextvertex.group[71].value,"%d",&i1);
                sscanf(nextvertex.group[72].value,"%d",&i2);
                sscanf(nextvertex.group[73].value,"%d",&i3);
                sscanf(nextvertex.group[74].value,"%d",&i4);
                fc++;
            }
            strcpy(dxfentity->group[62].value,nextvertex.group[62].value);

            dxfclear(&nextvertex);
            fscanf(filein,"%s",nextvertex.group[0].value);
            fscanf(filein,"%d",&G_group);
        }
        if(strncmp(dxfentity->group[8].value,"GAUSS",5)==0 ){
            Create_Nurbs_From_Dxf(dxfentity,vc, poly);
            NurbsEntity=1;
        }
    }
    else if(flag&8 || (flag==4) ||(!flag)) {
        int vflag;
        while (strcmp(nextvertex.group[0].value,"SEQEND")!=0) {
            getdxf_one_entity(&nextvertex,G_group);
            if(!sscanf(nextvertex.group[70].value,"%d",&vflag)) vflag=0;
            sscanf(nextvertex.group[10].value,"%f",&v.x);
            sscanf(nextvertex.group[20].value,"%f",&v.y);
            sscanf(nextvertex.group[30].value,"%f",&v.z);
            if(	isExtruded){
#ifdef HOOPS
                HC_Compute_Transformed_Points (1, &v, &(mymatrix[0][0]), &v);
#endif
            }
            if(!(vflag&16) )
                Append_To_Poly(&poly,&nbpts,&v,1);
            //else printf(" skip vertex %f %f %f because vflag %d\n", v.x,v.y,v.z,vflag);
            strcpy(dxfentity->group[62].value,nextvertex.group[62].value);

            dxfclear(&nextvertex);
            fscanf(filein,"%s",nextvertex.group[0].value);
            fscanf(filein,"%d",&G_group);
        }
    }
    //else
    // printf(" Polyline with flag %d not coded \n",flag);

    while (G_group!=0) {
        fscanf(filein,"%s",stmp);
#ifdef _DEBUG_DXF
        printf("(newpolyline) throw away group %d string %s\n",G_group, stmp);
#endif
        fscanf(filein,"%d",&G_group);
    }

    if(flag&64) {
        if(poly && faces) {
            dxFOPENsegment(segment,dxfentity);
            G_is_oriented=dxforientation(dxfentity);
#ifdef HOOPS
            HC_Insert_Shell( nbpts,poly,fcount,faces);
#endif
            if(!Edges_Visible) HC_Set_Visibility("edges=off");
            if( G_is_oriented)  HC_Close_Segment();
            HC_Close_Segment();
#ifdef DEBUG_DXF
            printf("flag 64 Insert Mesh with %d %d\n", nbpts, fcount);
#endif
            RXFREE(faces);
        }
    }
    else
        if(flag&16) {
            if(poly) {
                dxFOPENsegment(segment,dxfentity);
                G_is_oriented=dxforientation(dxfentity);
#ifdef HOOPS
                if(!NurbsEntity)
                    HC_Insert_Mesh( V_Alloc, F_Alloc,poly);
#endif
                if( G_is_oriented)  HC_Close_Segment();
                HC_Close_Segment();
#ifdef _DEBUG_DXF
                printf("flag 16 Insert Mesh with %d %d\n", V_Alloc, F_Alloc);
                cout<< " Close\n"<<endl;
                List_Open_Segments();
                wierd=1;
#endif
            }
        }
        else if (( flag&8) || flag==4 || flag==0) {
            dxFOPENsegment(segment,dxfentity);
            G_is_oriented=0; // dxforientation(dxfentity);  Now we transform the vertices instead
            if (G_thickness==0)  {
                if(nbpts >= 2) {  /*  peter changed. 2 points is OK */
                    if(strncasecmp(dxfentity->group[8].value,"gauss",5)==0 ) {
                        char *atts = Create_DXF_Attributes(dxfentity);
                        Generate_Curve_From_Graphic(dxfentity->m_sail, atts, dxfentity->group[5].value,dxfentity->group[8].value,NULL, nbpts,poly); // DANGER. Trashes poly
                        if(atts) RXFREE(atts);
                    }
                    else
                        RX_Insert_Polyline(nbpts,&(poly->x), dxfentity->m_sail->GNode() ,0 );

                }
                else
                    printf(" SHORT POLY SKIPPED (only %d pts\n", nbpts);
            }
            else {
                cout<< " thick lines not supported\n"<<endl;
                /*  for (i=0;i<nbpts-1;i++) {
      hoopsface(poly[i],poly[i+1],thickness);
          }
        hoopsface(poly[i],poly[0],thickness); */
            }
            if (G_is_oriented) HC_Close_Segment();
            HC_Close_Segment();

        }
        else {char buf[256];  sprintf(buf, " Polyline with Flag=%d  not coded",flag);  dxfentity->m_sail->OutputToClient(buf,1);}
    if(poly) 	  RXFREE(poly);
    if(wierd) { cout<< "Wierd Leaving newpolyline\n"<<endl; List_Open_Segments();}
    return (!wierd);
}

void newvertex(struct s_dxfentity *dxfentity)
{
#ifdef DEBUG_DXF
    cout<< "newvertex\n"<<endl;
#endif  

}

void dxftrace(struct s_dxfentity *dxfentity)
{
    printf("dxftrace\nsegment: <%s> ", segment);
    printf("color: <%s> ", dxfentity->group[62].value);
    printf("dxf x: <%s>\n", dxfentity->group[10].value);
}

int findsection(const char *sectionname)

{
    int ig,is; char buf[256];
#ifdef DEBUG_DXF
    printf("find sectionname %s (%p)\n",sectionname,filein);
#endif
    buf[0]=0; G_group=0;
    if(!filein) { g_World->OutputToClient("NO file open in DXF read",2); return 0;}

    ig=fscanf(filein,"%d",&G_group);
    is=fscanf(filein,"%s",buf);
    printf("next group(0)(%d %d) %d is '%s' SB '%s'\n",ig,is,G_group,buf,sectionname);

    while ( !feof(filein) && (G_group!=0||strcmp(buf,"SECTION")!=0)) {
        ig = fscanf(filein,"%d",&G_group);
        if(!ig || ig==EOF) cout<< "cant read group\n"<<endl;
        is = fscanf(filein,"%s",buf);
        if(!is || is==EOF) cout<< "cant read buf\n"<<endl;

        if(ig==EOF) { cout<< "EOF\n"<<endl; break;}
    }

    fscanf(filein,"%d",&G_group);
    fscanf(filein,"%s",buf);


    if (G_group!=2) dxferror("oops (findsection. group not 2)");
    while ((strcmp(buf,sectionname)!=0)&&!feof(filein)) {	/* PH added 19/3/96 */

        fscanf(filein,"%d",&G_group);
        fscanf(filein,"%s",buf);
        while ( !feof(filein) && (G_group!=0||strcmp(buf,"SECTION")!=0)) {
            fscanf(filein,"%d",&G_group);
            fscanf(filein,"%s",buf);
#ifdef DEBUG_DXF
            printf("discard (3) %d '%s'\n",G_group, buf);
#endif
        }
        fscanf(filein,"%d",&G_group);
        fscanf(filein,"%s",buf);
    }
    if (strcmp(buf,sectionname)==0) {
#ifdef DEBUG_DXF
        dxferror("we find it");
#endif
        return(1);
    }
    else {
#ifdef DEBUG_DXF
        dxferror("we did not find it");
#endif
        return(0);
    }
}

int findtable(char *tablename)
{
    char stmp[256];
    fscanf(filein,"%d",&G_group);
    fscanf(filein,"%s",stmp);
    while ((G_group!=0||strcmp(stmp,"TABLE")!=0 ) &&!feof(filein)) {
        fscanf(filein,"%d",&G_group);
        fscanf(filein,"%s",stmp);
    }
    fscanf(filein,"%d",&G_group);
    fscanf(filein,"%s",stmp);
    if (G_group!=2) dxferror("oops(findtable. group not 2)");
    while (strcmp(stmp,tablename)!=0) {
#ifdef DEBUG_DXF
        cout<< "find table name\n"<<endl;
#endif
        fscanf(filein,"%d",&G_group);
        fscanf(filein,"%s",stmp);
        while ((G_group!=0||strcmp(stmp,"TABLE")!=0)&&!feof(filein)) {
            fscanf(filein,"%d",&G_group);
            fscanf(filein,"%s",stmp);
        }
        fscanf(filein,"%d",&G_group);
        fscanf(filein,"%s",stmp);
    }
    if (strcmp(stmp,tablename)==0) {
#ifdef DEBUG_DXF
        dxferror("we find table \n");
#endif
        return(1);
    }
    else {
#ifdef DEBUG_DXF
        dxferror("we did not find it");
#endif
        return(0);
    }
}
//
//void setattributes()
//{
//}


void dxfclear(struct s_dxfentity *what)
{
    int i;

    for (i=0;i<80;i++) what->group[i].value[0]=what->xtend[i].value[0]=0;
    for (i=0;i<40;i++) strcpy(what->plane[i].value,"0.0");
    what->comment[0]=0;
    for (i=40;i<49;i++) strcpy(what->group[i].value,"1");
    for (i=50;i<59;i++) strcpy(what->group[i].value,"0");
    strcpy(what->group[39].value,"0");
    strcpy(what->group[70].value,"0");
    strcpy(what->group[62].value,"BYLAYER");
}

void dxfinit() //char*fname)
{

    int i;/*
  dxfcolors[0]="black";
  dxfcolors[1]="red";
  dxfcolors[2]="yellow";
  dxfcolors[3]="green";
  dxfcolors[4]="cyan";
  dxfcolors[5]="blue";
  dxfcolors[6]="pink";
  dxfcolors[7]= "white" ;  "R=1.0 G=0.97 B=0.97";
  dxfcolors[8]="grey";
  dxfcolors[9]="grey";
  */
    dxfcolorList[0]="dxfcolor0";
    dxfcolorList[1]="dxfcolor1";
    dxfcolorList[2]="dxfcolor2";
    dxfcolorList[3]="dxfcolor3";
    dxfcolorList[4]="dxfcolor4";
    dxfcolorList[5]="dxfcolor5";
    dxfcolorList[6]="dxfcolor6";
    dxfcolorList[7]="dxfcolor7";
    dxfcolorList[8]="dxfcolor8";
    dxfcolorList[9]="dxfcolor9";

    for (i=10;i<250;i++)  {
        char buf[64]; int j;
        float H,S,V;
        //float percent = (float) ((i-249)/7.0);

        H = ((float)(i-10)) *(float) 1.5;
        if (i&1) S=0.5;
        else S = 1.0;

        j = i;
        for( j=i; j >= 10; j=j-10){

        }
        V = (float)(1.0- ((float)j)/10.0);
        V = S*(1-V) + V;
        sprintf(buf,"H=%f S=%f V=%f", H,S,V);

        dxfcolorList[i]=strdup(buf);
    }

    for (i=250;i<256;i++)  {
        char buf[64];
        float percent = (float) ((i-249)/7.0);
        sprintf(buf,"R=%f G=%f B=%f", percent,percent,percent);
        dxfcolorList[i]=strdup(buf);
    }


}

int Create_Nurbs_From_Dxf(struct s_dxfentity *ent,int vc, VECTOR  *p){
    int depth = 1;
    /* if layer starts with GAUSS
    nu is g(72)
    nv is g(71)
    if g70&4 its an interpolatory mesh, so look at G75 0 = no smooth 5 quad 6 cube 8 cant do */
    RXEntity_p e;
    char*line;
    char buf[256];
    RXSTRING(name);
    int i,k, flag, meshtype,nu,nv, c,c0;
    char  *handle,*layer;
    RXAttributes atts(Create_DXF_Attributes(ent));
    //sprintf(buff," atts '%s' in env.. are \n'%s'", ent->group[8].value ,atts);

    handle = ent->group[5].value;
    layer = ent->group[8].value;
    if(!atts.Extract_Word(L"name",name))
        name=TOSTRING(layer)+TOSTRING("_")+TOSTRING(handle);
    printf(" Create_Nurbs_From_Dxf %s np = %d\n",layer,vc );

    sscanf(ent->group[70].value,"%d",&flag);

    if(flag&16)
        sscanf(ent->group[75].value,"%d",&meshtype);
    else
        return 0;
    //	sscanf(ent->group[71].value,"%d",&nv);
    //	sscanf(ent->group[72].value,"%d",&nu);
    // a guess 2/8/00
    sscanf(ent->group[71].value,"%d",&nu);
    sscanf(ent->group[72].value,"%d",&nv);

    if(flag&4) {
        switch (meshtype) {
        case 0 :
            k = 1;
            break;
        case 5 :
            k=2;
            break;
        case 6:
            k=3;
            break;
        default :
            ent->m_sail->OutputToClient(" can't translate this mesh",2);
            return 0;
        }
    }
    else
        k=1;

    //	sprintf(name,"%s_%s",layer,handle); ent->sail,
    if(ent->m_sail -> Entity_Exists( ToUtf8( name).c_str(),"interpolation surface")) {
        ent->m_sail->OutputToClient("Skip NURBS read. already exists",2);
        return 0;
    }
    c0 = 164 + 32*vc+ atts.Length();
    line = (char*)MALLOC(c0);
    assert(line);
    sprintf(line,"interpolation surface: %S : NURBS: NULL: 1 : 1 : %S nu=%d nv=%d ku=%d kv=%d\n",name.c_str(),atts.GetAll().c_str(),nu,nv,k,k);
    strcat(line,":table: ");
    c=strlen(line);
    for(i=0;i<vc;i++) {
        sprintf(buf,"%f \t%f \t%f\n", p[i].x, p[i].y,p[i].z);
        c += strlen(buf );
        assert(c<c0);
        strcat(line,buf);
    }

    e = Just_Read_Card(ent->m_sail , line,"interpolation surface",ToUtf8( name).c_str(),&depth);
    RXFREE(line);

    if(e){
        if(  Resolve_Interpolation_Card (e)){
            if(!Compute_Interpolation_Card(e) )
                ent->m_sail->OutputToClient(" Compute_Interpolation_Card failed",2);
        }
        else
            ent->m_sail->OutputToClient(" Resolve_Interpolation_Card failed",2);
    }

    return 1;
}
int Generate_NURBS_From_Dxf_Spline(struct s_dxfentity *ent,double *x,double *y,double *z,double *k,int ncp,int nkts,int order,struct PC_NURB *n){
    double kmin,kmax;
    VECTOR *vlist ;
    int i,j;
    float *W = (float*)MALLOC ((ncp+2)*sizeof(float));
    memset(n,0,sizeof(struct PC_NURB));
    n->tol = 0.001;
    n->LimitU = ncp;
    n->KValU = order;

    vlist = (VECTOR *)MALLOC(ncp*sizeof(VECTOR));
    n->m_KnotVectorU=(float *)MALLOC(n->LimitU  *sizeof(float));
    n->flags = PCN_ONE_D;
    n->LimitV= 1 ;
    n->KValV =  0;
    n->KoffU = 1; //n->KValU-2; // a guess
    n->KoffV= n->KValV-2;
    n->Knot_Type=PCN_ONE_D;

    kmin = kmax = k[0];
    // knots in U
    for(j=0;j<nkts;j++) {
        n->m_KnotVectorU[j]=(float) k[j];
        kmin = min(kmin,k[j]);
        kmax = max(kmax,k[j]);
    }

    n->iP = n->cP = (VECTOR **)CALLOC( (n->LimitU),sizeof(void *));
    for(i=0;i<  n->LimitU;i++) {
        n->iP[i]= n->cP[i]= (VECTOR *)CALLOC((n->LimitV),sizeof(VECTOR));
        W[i]=1.0;
    }

    PCN_Set_Weights(n); // sets them all to unity
    // skip weights
    for(j=0;j<n->LimitV;j++) {
        for(i=0;i<n->LimitU;i++) {
            vlist[i].x = n->iP[i][j].x=(float)x[i];
            vlist[i].y = n->iP[i][j].y=(float)y[i];
            vlist[i].z = n->iP[i][j].z=(float)z[i];
        }
    }

    // knots in U

    for(j=0;j<nkts;j++) {
        n->m_KnotVectorU[j]= (n->m_KnotVectorU[j]-kmin)/(kmax-kmin);
        if(n->m_KnotVectorU[j]>1.0) n->m_KnotVectorU[j]=1.0;
        if(n->m_KnotVectorU[j]<0.0) n->m_KnotVectorU[j]=0.0;
    }


    // now we have a NURBS structure
#ifdef HOOPS
    HC_Insert_NURBS_Curve ( n->KValU,n->LimitU, vlist,W ,n->m_KnotVectorU,(float) 0.0, (float) 1.0) ;
#endif

    RXFREE(vlist); RXFREE(W);
    return 1;
}
