
#ifndef _BOUNDINGBOX_H_
#define  _BOUNDINGBOX_H_
#include "opennurbs.h"
class  rxON_BoundingBox;
int	RXON_GetLocalBB(const rxON_NurbsCurve *pc, rxON_BoundingBox *bb, const int n, const double maximum_distance );// a BB around the relevant CVs, grown by max_dist

#endif
