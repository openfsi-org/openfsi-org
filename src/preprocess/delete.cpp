/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 Jan 2005 panel elist and unfreeze
jul 2001  Delete String doesnt free edges on curve strings.  They belong to the psides.
Jan 98	Delete_IA_Record sets Needs _Computing
 *	27/3/96  Delete a RLX seam which may not have endNptrs
 *  29/2/96 dealing with CUT IAs. dropsail ifdeffed out
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */
/*  The Delete routines MUST NOT RXFREE the dataptr; 
   This is done in Delete Entity.  */

#include "StdAfx.h"
#include "RXEntityDefault.h"
#include "RXSail.h"
#include "RXCompoundCurve.h"
#include "RXPside.h"
#include "batdefs.h"

#include "delete.h"

int Delete_Patch(RXEntity_p p){
	int k;
	 struct PC_PATCH*b = (PC_PATCH *)p->dataptr;
	class RXCompoundCurve *  cc = (class RXCompoundCurve *  )b->cc;

	if(cc) {
	for (k=0;k<cc->npss;k++) {
		PSIDEPTR e = cc->pslist[k];
		e->Kill(  );
	}
		for (k=0;k<cc->m_ccNcurves;k++) {
	 	    RXEntity_p e = cc->m_ccCurves[k];
		     if(e->generated==30000) e->Kill( ); /* dangerous.  Only if its a 'cut' batpatch*/
		}
	}
	 if(b->p) RXFREE(b->p);  b->p=NULL;

	 return 1;
}

int Resolve_Delete_Card(RXEntity_p e) {
	const char* name,*type;
	int  retVal = 0;
	RXEntity_p  p;
    std::string sline(e->GetLine() );
	std::vector<std::string> wds = rxparsestring(sline,",",false);
	int nw = wds.size();
	if(nw>2){
		type=wds[1].c_str();
		name=wds[2].c_str();
		p=e->Esail ->GetKeyWithAlias(type,name); cout <<"TODO: Step Resolve_Delete"<<endl;
		if(p){
			retVal = p->Kill();

			e->Needs_Finishing = 0;	/* because MSVC version cannot retrieve un-resolved	Es*/
			e->SetNeedsComputing(0);
			e->Needs_Resolving= 0;
			return(1);
		}
   }  
return(retVal);
}

 int Free_Gcurve_List(sc_ptr sc){   
	   linklist_t *lln, *ll = (LINKLIST *)sc->gauss_pts;

		while(ll) {
				if(ll->data) RXFREE(ll->data);
				lln = ll->next;
				RXFREE(ll);
				ll=lln;
		}
	sc->gauss_pts=NULL;
	return 1;
}



