#include "StdAfx.h"
#include <QDebug>
#include <fstream>
#include "RXMesh.h"
#include "rxvectorfield.h"
#include "RXPressureInterpolation.h"
// pressure interpolation objects have the pressures defined on the unit square in UV space.
// unlike some other interpolation objects which may have the data defined on some other quadrilateral.

RXPressureInterpolationP::RXPressureInterpolationP(class RXSail *s)
    :RXInterpolationI(s)
{
}

RXPressureInterpolationP::~RXPressureInterpolationP(void)
{
}
int RXPressureInterpolationP::Dump(const char* fname)
{
    return 0;
}

int RXPressureInterpolationP::Read(const std::wstring& fname)
{
    vector<int> edgeNos;
    char c;			std::string comment;
    char buf [2048];
    filename_copy_local(buf,2048,ToUtf8(fname).c_str() ); 	ifstream input(buf);
    if(!input) {cout <<" couldnt open file <"<<buf<<">"<<endl; return 0;}
    double u,v,dp; int cnt=0;
    do{
        input>>u>>v>>dp>>skipws; if(! input.good()) break;
        // soak up some whitespace
        do{
            c=input.peek();
            if(c==' '||c=='	') { input.get(c);  }
            else break;
        }while(true);
        //  now c can be '!' or '#'
        if(c=='!'||c=='#'){
            cout<<"Pressure file comment :<";

            getline(input,comment);
            cout<<comment.c_str()<<">"<<endl;
        }

        //cout <<u <<" "<<v<<" "<<dp<<endl;
        Points().push_back( RXSitePt( u,v,dp,ON_UNSET_VALUE,ON_UNSET_VALUE,cnt++));
    } while(true);
    input.close ();
    wcout<<L"pressure file "<<fname<<L" has "<< cnt<<" pts"<<endl;
    if(cnt<3){
        wcout <<L"you need at least 3 points - cant use this pressure file "<<endl;
        return 0;
    }
    if(!msh.Triangulate(Points(),edgeNos,"pcenzQ" ))
    {
        cout<<"triangulation failed"<<endl;
        return 0;
    }
    return cnt;
}

double RXPressureInterpolationP::ValueAt(const ON_2dPoint& pin) // pressure interpolation objects have the pressures defined on the unit square.
{
    return  _ValueAt(pin );
}
int RXPressureInterpolationP::ValueAt(const ON_2dPoint& p, double*rv)
{
    return _ValueAt(p,rv);
}

double RXPressureInterpolationP::_ValueAt(const ON_2dPoint& p)
{
    double result=0.0;
    int ii[3]; double ww[3];
    msh.Locate(p,ii,ww);
    for(int k=0;k<3;k++) {
        if(ii[k]<=0) continue;
        result	+=  this->Pt(ii[k]).z * ww[k];
    }
    return result;
}


int RXPressureInterpolationP::_ValueAt(const ON_2dPoint& p, double*rv)
{
    double result;
    int ii[3]; double ww[3];
    msh.Locate(p,ii,ww);   assert(0);
    // as a check
    result	=  this->Pt(ii[0]).z * ww[0] \
            +  this->Pt(ii[1]).z  * ww[1]	\
            +  this->Pt(ii[2]).z  * ww[2];
    assert(0);
    *rv=result;
    return 1;
}




