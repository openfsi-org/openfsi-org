// RXmaterialMatrix.h: interface for the RXmaterialMatrix class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RXMATERIALMATRIX_H__8AAB56D9_8969_4685_9037_3AA9DD41C75D__INCLUDED_)
#define AFX_RXMATERIALMATRIX_H__8AAB56D9_8969_4685_9037_3AA9DD41C75D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AMGLevel.h"
#include "layernode.h"

#define STACKTYPE_A	1
#define STACKTYPE_B	2
#define STACKTYPE_C	3
#define STACKTYPE_D	4
#define STACKTYPE_E	5
#define STACKTYPE_F	6

#define STACK_FAILED		0
#define STACK_NO_SUBSTRATE	2
#define STACK_ADD_SUBSTRATE	4


enum StackDSubtype {  
   subtype_undefined,
   nt1pt7nt2pt33,           
   nt1is1nt2is1 ,
   nt1is1nt2pt5
};

static const char *StackSubTypeString[4]={"a","b","c","d"};
class RXmaterialMatrix    
{
public:
	int LayerStrain(double p_eps[3],double &emin, double&emax);
	int  PrintLayerStrain(FILE *fp, const double p_u, const double p_v, double p_eps[3]);
	void SetStackType(const int StackType);
	void SetStackSubType(const enum StackDSubtype p);
//static 	int LevelToStackIndex(int StackCount, int OffsetInStack, const AMGLevel p_level);
static int LevelToStackIndex(ON_ClassArray<layernodestack> &p_TheStack, const AMGLevel p_level);

	int NoOfOnAxisPlies( const double t);

	int Stack(const double alpha,const double thk, const double nu,const double p_ks,const double p_ky,
						 ON_ClassArray<layernodestack> &p_TheStack,
						 const double & p_Lambda = 0, 
						const double & p_Ne = 0, 
						const double & p_K1 = 0, 
						const double & p_K2 = 0, 
						const int  & p_ptrEngine = 0 );
	int Stack1(const double alpha,const double thk, const double nu,const double p_ks,const double p_ky,
						 ON_ClassArray<layernodestack> &p_TheStack );
	int Stack2(const double alpha,const double thk, const double nu,const double p_ks,const double p_ky,
						 ON_ClassArray<layernodestack> &p_TheStack );
	int Stack3(const double alpha,const double thk, const double nu,const double p_ks,const double p_ky,
						 ON_ClassArray<layernodestack> &p_TheStack );
	int Stack4(const double alpha,const double thk, const double nu,const double p_ks,const double p_ky,
						 ON_ClassArray<layernodestack> &p_TheStack   );
	int Stack5(const double alpha,const double thk, const double nu,const double p_ks,const double p_ky,
						 ON_ClassArray<layernodestack> &p_TheStack );

	int StackLambdaNe(const double & p_alpha, 
					const double & p_thk, 
					const int & p_ptrEngine, 
					const double & p_Lambda,
					const double & p_Ne,
					const double & p_K1,
					const double & p_K2,
					ON_ClassArray<layernodestack> &p_TheStack, 
					const double & p_nudebug, 
					const double & p_ksdebug, 
					const double & p_kydebug);

int StackCheck(const double alpha,const double thk, const double nu,const double p_ks,const double p_ky);
static int StackCheck1(const double alpha,const double thk, const double nu,const double p_ks,const double p_ky);
static int StackCheck2(const double alpha,const double thk, const double nu,const double p_ks,const double p_ky);
static int StackCheck3(const double alpha,const double thk, const double nu,const double p_ks,const double p_ky);
static int StackCheck4(const double alpha,const double thk, const double nu,const double p_ks,const double p_ky);

	HC_KEY DrawHoops(const char *seg, const ON_3dPoint p0, const ON_2dVector & refdir);
	int ExtractDoveRatios(double &thk,double&ks,double&ky,double&nu);
	int Symmetrify();
	int Print(FILE *fp);
	double PrincipleAngle();
	ON_3dVector PrincipleVector();
	int ToParameters(RXmaterialMatrix qply, double*thk,double*ke,double*kg,double*nu);
	RXmaterialMatrix(const double*a);
//	ON_ClassArray<layernodestack> Stack(const RXmaterialMatrix &qply);
//	ON_ClassArray<layernodestack> Stack(const double thk, const double ky, const double ks);
	double TensorTrace() const;
	int DDash(const double a);
	int Rotate(const double a);
	
	ON_Matrix Rotated(const double a);
	ON_Matrix J(const double a);
	ON_Matrix GetMatrix() const; 

	ON_String Serialize();
	char *Deserialize(char *lp);

	RXmaterialMatrix(ON_ClassArray<layernodestack> &TheStack,RXmaterialMatrix &qply);
	RXmaterialMatrix();
	RXmaterialMatrix(const int type,const enum StackDSubtype);
	RXmaterialMatrix(ON_Matrix &m);
	RXmaterialMatrix(	const double thk,
						const double ke,
						const double kg,
						const double nu
						);

		RXmaterialMatrix(const double thk, // OK for symmetric laminates only
			const double ke,
			const double kg,
			const double nu,
			RXmaterialMatrix *qply
			);
	virtual ~RXmaterialMatrix();
	static ON_3dVector PrincipleStress(ON_3dVector &p_stress);

	RXmaterialMatrix  operator*(const double) const;
	RXmaterialMatrix  operator+(const RXmaterialMatrix&) const;

	RXmaterialMatrix& operator=(const RXmaterialMatrix&);

private:
	ON_Matrix m_m;
	int m_StackType;
	enum StackDSubtype m_stacksubtype;

protected:


};


#endif // !defined(AFX_RXMATERIALMATRIX_H__8AAB56D9_8969_4685_9037_3AA9DD41C75D__INCLUDED_)
