/* file : hoopcall.h 
  header file for HOOPCALL.LIB
date:  Wednesday 4 March 1992
*/

#ifndef HOOPCALL_15NOV04 //#ifndef  HOOPCALL_15NOV04
#define HOOPCALL_15NOV04 

class RXLogFile;
#include "unixver.h" // for strcasecmp
//#include "pcwin.h"

EXTERN_C int Change_Hoops_Strings(const char*seg,const char*what, char*old,char*n,class RXDatabaseI *sum);
EXTERN_C int Valid_Hoops_Color(const char*string);

EXTERN_C int PC_Hard_Copy(char *, char *,char *) ;
EXTERN_C char *Compute_Relative_Filename (char *root, char *fname, char *word);
EXTERN_C int Compute_Relative_Pathname (char *,char *,char *);
EXTERN_C int PC_Box_Size(const char *,const char *,const char *,float*,float*,float*,float*,float*,float*);
EXTERN_C int Add_Colours(char *s, double* thick, char**List, int c);


#ifdef STROONA
#include "vectors.h"


EXTERN_C int  PC_Clear_Post(void);
EXTERN_C int  PC_Silent_Post(void);

EXTERN_C int  dump(char * ,char *) ;
EXTERN_C void PC_Banner(char *, char *);
EXTERN_C int  PC_Label(char *) ;
EXTERN_C int  PC_Memory(void) ;
EXTERN_C int  PC_Vmenu(float, float, int, char * [] );

EXTERN_C int  PC_Box_Selection(char * ,char *,float,float,float,float,long [],int *);
EXTERN_C int  PC_Zoom_Box(float*,float*,float*,float*,float*,float*,float*,float*);
EXTERN_C int  PC_ReCamera_Segment( const char *);
EXTERN_C int  PC_Set_Camera_To_Geometry( char *,	char *); 
EXTERN_C int  PC_Update_Camera_To_Geometry( char *,	char *);

//EXTERN_C int message( char *);
EXTERN_C int startup(char *);
EXTERN_C int add_button (float ,float ,float ,float ,char *);
EXTERN_C int yesno(char *);
EXTERN_C long First_Text_Key(void);
EXTERN_C int PC_Edit_String(void) ;
EXTERN_C int PC_Get_String(char *, char *);



void HC_CDECL Color_Report_Error(int cat,int spec,int sev,int msgc,char *msgv[],int stackc,char *stackv[]);
 //HC_ANY_ARGS); //
#include "hoopserv.h"


//#include "evaluate.h"
#include "pchoops.h"
#endif
#endif  //#ifndef  HOOPCALL_15NOV04
