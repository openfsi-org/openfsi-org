// RX_OncIntersect.cpp: implementation of the RX_OncIntersect class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "RX_OncIntersect.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

RX_OncIntersect::RX_OncIntersect()
{
Init();
}

RX_OncIntersect::~RX_OncIntersect()
{
Clear();
}

void RX_OncIntersect::Init()
{
m_ce=NULL;
m_t=0;
m_ono=NULL;

}
void RX_OncIntersect::Clear()
{
m_ce=NULL;
m_t=0;
}

RX_ONC_Cpoint::RX_ONC_Cpoint()
{
Init();
}

RX_ONC_Cpoint::~RX_ONC_Cpoint()
{
Clear();
}


void RX_ONC_Cpoint::Init()
{
	m_p=ON_3dPoint(0,0,0);
	m_pe=0;
    m_sm=0;
	m_ono=NULL;
	m_Clist.Empty();
}

void RX_ONC_Cpoint::Clear()
{
	m_p=ON_3dPoint(0,0,0);
	m_pe=NULL;
	m_ono=NULL;
    m_sm=0;
	m_Clist.Destroy();
}
