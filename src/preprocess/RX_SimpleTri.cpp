#include "StdAfx.h"
#include "RX_SimpleTri.h"

/*RX_TriBase::RX_TriBase(void)
{
}

RX_TriBase::~RX_TriBase(void)
{
} */


RXSitePt *RX_TriBase::Centroid(RXSitePt *q) const{
 const RXSitePt *v[4] ;
 Vertices(v);
*q = *(v[0])+*(v[1])+*(v[2]);
*q=(*q)/3.;
return q;
}


double RX_TriBase::Area(void) const{
ON_3dVector va,vb,vn;
double a;                                                                                                                   

 const RXSitePt *v[4] ;
 Vertices(v);

va = *(v[2]) - *(v[0]);
vb = *(v[1]) - *(v[0]);
 vn=ON_CrossProduct(va,vb);
a = vn.Length()/2.0; 

return(a);
}

ON_3dVector RX_TriBase::baseVector(void)const{ //NOT normalized.
ON_3dVector rc;
 const RXSitePt *v[4] ;
 Vertices(v);
 
rc = *(v[1]) - *(v[0]);
return rc;
}
ON_3dVector  RX_TriBase::adjVector(void)const{ //NOT normalized.
ON_3dVector rc;
 const RXSitePt *v[4] ;
 Vertices(v);
 
rc = *(v[2]) - *(v[0]);
 
return rc;
}

ON_Xform RX_TriBase::Trigraph(const double theta)
{
	ON_3dVector base, adj,y,z;
	const RXSitePt *v[4] ;
	Vertices(v);
	base =*(v[1]) - *(v[0]);
	adj =  *(v[2]) - *(v[0]);

	z = ON_CrossProduct(base,adj);
	z.Unitize ();
	base.Unitize();
	y = ON_CrossProduct(	z,base);
        ON_Xform l_xf = ON_Xform(v[0]->ToONPoint (),base,y,z);
	l_xf.Transpose();

	if (fabs(theta) < ON_EPSILON) return l_xf;
        assert("I dont remember ever testing the case where theta is no-zero." ==0) ;
	ON_3dVector zz = ON_3dVector(0,0,1) * l_xf;
	ON_Xform rx; rx.Rotation ( theta,zz,ON_3dPoint(0,0,0));// then trig=trig*rx
	l_xf=l_xf * rx;

return l_xf;

}


RX_SimpleTri::RX_SimpleTri(void)
{
}

RX_SimpleTri::~RX_SimpleTri(void)
{
}
int RX_SimpleTri::Vertices(const RXSitePt * v[4]) const{

	v[0]		= &(pts[0]);
	v[1]		= &(pts[1]);
	v[2]		= &(pts[2]);
	v[3]		= &(pts[0]);
return 1;
}

RXSTRING RX_SimpleTri::TellMeAbout() const
	{
	return RXSTRING(L"TODO Simple Tri");
	}
