/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by P Heppel  February 1998
 * Nov 98  added a TOL to segment_intersect. May give trouble 

 * 15.7/98  first trial
 *	status at 3/3/96
  *
 */
 


#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXOffset.h"
#include "RXSeamcurve.h"
#include "global_declarations.h"
#define CUTDB (0) 

#ifdef _DEBUG
	#include "printall.h"
#endif
#include "etypes.h"
#include "arclngth.h"
#include "iangles.h"
#include "panel.h"
#include "RXCurve.h"
#include "cut.h"


#include "slowcut.h"

double PCC_Angle_Approx_SQ(const VECTOR *a1,const VECTOR*b1,const VECTOR *a2,const VECTOR*b2) {
	float v1x,v1y,v2x,v2y;
	double cross,lv1sq,lv2sq;
//	PC_Vector_Difference(b1,a1,&v1);
//	PC_Vector_Difference(b2,a2,&v2);
	v1x = b1->x - a1->x; 
	v1y = b1->y - a1->y; 
	v2x = b2->x - a2->x; 
	v2y = b2->y - a2->y; 

	cross = v1x*v2y - v2x*v1y;
	lv1sq = v1x*v1x + v1y*v1y;
	lv2sq = v2x*v2x + v2y*v2y;
	return(cross*cross/(lv1sq*lv2sq));
}
double PCC_Angle_Approx_SQ(const VECTOR *a1,const VECTOR*b1,const ON_3dPoint *a2,const ON_3dPoint*b2) {
	float v1x,v1y,v2x,v2y;
	double cross,lv1sq,lv2sq;
//	PC_Vector_Difference(b1,a1,&v1);
//	PC_Vector_Difference(b2,a2,&v2);
	v1x = b1->x - a1->x; 
	v1y = b1->y - a1->y; 
	v2x = b2->x - a2->x; 
	v2y = b2->y - a2->y; 

	cross = v1x*v2y - v2x*v1y;
	lv1sq = v1x*v1x + v1y*v1y;
	lv2sq = v2x*v2x + v2y*v2y;
	return(cross*cross/(lv1sq*lv2sq));
}

//double SSD_PCC_Angle_Between_Accurate(VECTOR *a1,VECTOR*b1, VECTOR *a2,VECTOR*b2,int*err) {
//	VECTOR v1,v2;
//	PC_Vector_Difference(b1,a1,&v1);
//	PC_Vector_Difference(b2,a2,&v2);
//	return(PC_True_Angle(&v1,&v2,err));
//}
int  PCC_Box_Intersection(const VECTOR *a1,const VECTOR*b1,const ON_3dPoint *a2,const ON_3dPoint*b2)
{
// two boxes: one from a1 to b1. The other from a2 to b2
// if they dont overlap, return 0;
	if(max(a1->x,b1->x) < min(a2->x,b2->x))
		return 0;
	if(max(a1->y,b1->y) < min(a2->y,b2->y))
		return 0;

	if(max(a2->x,b2->x) < min(a1->x,b1->x))
		return 0;
	if(max(a2->y,b2->y) < min(a1->y,b1->y))
		return 0;
	return 1;
}
int  PCC_Box_Intersection(const VECTOR *a1,const VECTOR*b1,const VECTOR *a2,const VECTOR*b2)
{
// two boxes: one from a1 to b1. The other from a2 to b2
// if they dont overlap, return 0;
	if(max(a1->x,b1->x) < min(a2->x,b2->x))
		return 0;
	if(max(a1->y,b1->y) < min(a2->y,b2->y))
		return 0;

	if(max(a2->x,b2->x) < min(a1->x,b1->x))
		return 0;
	if(max(a2->y,b2->y) < min(a1->y,b1->y))
		return 0;
	return 1;
}

int  Segment_Intersect(VECTOR *a1,VECTOR*b1, VECTOR *a2,VECTOR*b2,double*s1,double*s2, double tol) {

/* checks whether the line segment from a1 to b1 intersects the  line segment a2->b2 within their
lengths (plus a tol)  in view along Z axis*/

double denom;
/* 
if (! Box_Intersection(a1,b1,a2,b2)) return 0; */

denom = b2->x*(a1->y - b1->y) + a2->x*(-a1->y + b1->y) + (a1->x - b1->x)*(a2->y - b2->y); 

if(denom ==0.0) return 0;

*s1 = (b2->x*(a1->y - a2->y) + a1->x*(a2->y - b2->y) + a2->x*(-a1->y + b2->y))/denom;
if(*s1<0.0 - tol) return 0;  /* WRONG we dont need TOL here */
if(*s1>1.0 + tol) return 0;
*s2 = (b1->x*(a1->y - a2->y) + a1->x*(a2->y - b1->y) + a2->x*(-a1->y + b1->y))/denom;

if(*s2<0.0 - tol) return 0;
if(*s2>1.0 + tol) return 0;

return 1;
} 


int  Segment_IntersectWithExtensions(const VECTOR *a1,const VECTOR*b1,
									 const  ON_3dPoint *a2,const ON_3dPoint*b2,
									double*s1,double*s2,int flagIn)
{ 

//int  Segment_IntersectWithExtensions(VECTOR *a1,VECTOR*b1, VECTOR *a2,VECTOR*b2,double*s1,double*s2, int flagIn) {

/* checks where the line  thru a1 to b1 intersects the  line thru a2->b2 
	return 0 if they are parallel
	s1 and s2 are the parametric lengths along each line. 
	ie 0 at end1 and 1 at end2
	Flag is an AND of (EXT_1_L+EXT_1_R+EXT_2_L+EXT_2_R)
	*/

double denom,asq; 
int flag = (flagIn | EXT_3D)   -  EXT_3D;	//May 2003 to enable 3D NOT USED
	flag =  (flag  | EXT_2D)  -	EXT_2D;  

	
if(!flag && !PCC_Box_Intersection(a1,b1,a2,b2))
	return PCC_OUT_OF_RANGE;

denom = (b2->x - a2->x)*(a1->y - b1->y) + (a1->x - b1->x)*(a2->y - b2->y); 
if(fabs(denom) <0.0001) 
	return PCC_PARALLEL;

*s1 = (b2->x*(a1->y - a2->y) + a1->x*(a2->y - b2->y) + a2->x*(-a1->y + b2->y))/denom;
*s2=-100.;

if((*s1<0.)		&& !(flag & EXT_1_L)) 
		return PCC_OUT_OF_RANGE;
if((*s1 > 1.0 )	&& !(flag & EXT_1_R)) 
		return PCC_OUT_OF_RANGE;

*s2 = (b1->x*(a1->y - a2->y) + a1->x*(a2->y - b1->y) + a2->x*(-a1->y + b1->y))/denom;

if((*s2  <0.)	&& !(flag & EXT_2_L)) 
		return PCC_OUT_OF_RANGE;
if((*s2 > 1.0)	&& !(flag & EXT_2_R)) 
		return PCC_OUT_OF_RANGE;

// after profiling, Feb 2001 moved this test to the end.  
// changes the logic  - maybe bad - but 
asq = PCC_Angle_Approx_SQ(a1,b1,a2,b2);// done formally,this was very slow.
if(asq < g_Gauss_Parallel)			// about .25 degree is good
	return PCC_SMALL_ANG;  
return PCC_OK;
} 


int  Segment_IntersectWithExtensions(const VECTOR *a1,const VECTOR*b1,
									 const  VECTOR *a2,const VECTOR*b2,
									double*s1,double*s2,int flagIn)
{ 
/* checks where the line  thru a1 to b1 intersects the  line thru a2->b2 
	return 0 if they are parallel
	s1 and s2 are the parametric lengths along each line. 
	ie 0 at end1 and 1 at end2
	Flag is an AND of (EXT_1_L+EXT_1_R+EXT_2_L+EXT_2_R)
	*/

double denom,asq; 
int flag = (flagIn | EXT_3D)   -  EXT_3D;	//May 2003 to enable 3D NOT USED
	flag =  (flag  | EXT_2D)  -	EXT_2D;  

	
if(!flag && !PCC_Box_Intersection(a1,b1,a2,b2))
	return PCC_OUT_OF_RANGE;

denom = (b2->x - a2->x)*(a1->y - b1->y) + (a1->x - b1->x)*(a2->y - b2->y); 
if(fabs(denom) <0.0001) 
	return PCC_PARALLEL;

*s1 = (b2->x*(a1->y - a2->y) + a1->x*(a2->y - b2->y) + a2->x*(-a1->y + b2->y))/denom;
*s2=-100.;

if((*s1<0.)		&& !(flag & EXT_1_L)) 
		return PCC_OUT_OF_RANGE;
if((*s1 > 1.0 )	&& !(flag & EXT_1_R)) 
		return PCC_OUT_OF_RANGE;

*s2 = (b1->x*(a1->y - a2->y) + a1->x*(a2->y - b1->y) + a2->x*(-a1->y + b1->y))/denom;

if((*s2  <0.)	&& !(flag & EXT_2_L)) 
		return PCC_OUT_OF_RANGE;
if((*s2 > 1.0)	&& !(flag & EXT_2_R)) 
		return PCC_OUT_OF_RANGE;

// after profiling, Feb 2001 moved this test to the end.  
// changes the logic  - maybe bad - but 
asq = PCC_Angle_Approx_SQ(a1,b1,a2,b2);// done formally,this was very slow.
if(asq < g_Gauss_Parallel)			// about .25 degree is good
	return PCC_SMALL_ANG;  
return PCC_OK;
} 




int SlowCut(
			const VECTOR*p_p1, int p_c1, const double *p_ds1, // Define the first polyline (INPUT)
			const VECTOR*p_p2, int p_c2, const double *p_ds2, //INPUT Define the Second polyline (INPUT)
			double**p_ss1, double**p_ss2, int*p_nc, //Define the intersections (OUTPUT)
			double p_tol) 
//Find all the inetersection beween 2 polylines.
/*
p_e,  //NOT USED
p_p1, p_c1, p_ds1, // Define the first polyline (INPUT), p_p1 arry of vector, p_c1 nb of points,  p_ds1 slope (I guess Thomas 07 07 04) 
p_p2, p_c2, p_ds2, // Define the 2nd   polyline (INPUT), p_p2 arry of vector, p_c2 nb of points,  p_ds2 slope (I guess Thomas 07 07 04) 
p_ss1, p_ss2, p_nc, //Define the interesections (OUTPUT), p_ss1 position of the interesections on polyline 1, parameter 0<<1 
														, p_ss2 position of the interesections on polyline 2, parameter 0<<1	
														, p_nc number of intersections
*/
{
int i,j, iret, parallel_count = 0;
VECTOR *a1,*a2,*b1,*b2;
double s1,s2, t1,t2,q1,q2;

*p_nc=0;
assert("SlowCut hasnt been verified for a long time"==0 );
	b1 = a1 =  (VECTOR*)p_p1; b1++;

	for(i=0;i<p_c1-1;i++,a1++,b1++) { 
		assert(0);b2 = a2 =  (VECTOR*)p_p2; b2++;
		for(j=0;j<p_c2-1;j++,a2++,b2++) {  // May 2003 f1 is ANDed with EXT_2D or EXT_3D
			//0 ,PCC_PARALLEL, PCC_OK
			iret = Segment_IntersectWithExtensions(a1,b1,a2,b2,&t1,&t2,EXT_ALL);

			switch (iret) {
			case PCC_SMALL_ANG:
			//	break;  ??MAYBE
			case PCC_PARALLEL :
				parallel_count++;
				break;
	
			case PCC_OK :{ 
			/* 	the lines arent parallel. t1 and t2 are parametric distances
				in the first segment we permit s1 to be -tol/ds
				in the last we permit it to be 1+tol /ds
				otherwise we allow it to be between 0 and 1, including 0 but not 1
				*/
				q1 = p_ds1[i+1] - p_ds1[i];
				q2 = p_ds2[j+1] - p_ds2[j];
				if(i==0		&&	t1 < -(p_tol/q1))			continue;
				if(i!=0		&&	t1 < 0.0)				continue;			
				if(i==p_c1-2	&&	t1 >= 1.0 + p_tol/q1)		continue;
				if(i!=p_c1-2	&&	t1 >= 1.0)				continue;

				if(j==0		&&	t2 < -(p_tol/q2))			continue;
				if(j!=0		&&	t2 < 0.0)				continue;			
				if(j==p_c2-2	&&	t2 >= 1.0 + p_tol/q2)		continue;
				if(j!=p_c2-2	&&	t2 >= 1.0)				continue;

				s1 = p_ds1[i] *(1.0-t1) + p_ds1[i+1] * t1;		/* convert to length units */
				s2 = p_ds2[j] *(1.0-t2) + p_ds2[j+1] * t2; 
				(*p_nc) = (*p_nc) + 1; 
				*p_ss1 = (double *)REALLOC(*p_ss1, (*p_nc+1)*sizeof(double)); 
				*p_ss2 = (double *)REALLOC(*p_ss2, (*p_nc+1)*sizeof(double)); 
				assert(*p_ss1 && *p_ss2);

				(*p_ss1)[*p_nc-1] = s1;
				(*p_ss2)[*p_nc-1] = s2;
				break;
			}
			default:
				assert(0);
				break;
			}
		}
	}
// here  *nc is the number of crosses.
	if(parallel_count) {

		return PCC_PARALLEL;
	}
return PCC_OK;
}//int SlowCut



int Box_Intersection(VECTOR *a1,VECTOR*b1, VECTOR *a2,VECTOR*b2 ){

if(a1->x < a2->x && b1->x < a2->x  && a1->y < a2->y && b1->y < a2->y) return 0;

if(a1->x > a2->x && b1->x > a2->x  && a1->y > a2->y && b1->y > a2->y) return 0;
return 1;
}

int Cross_Already(RXEntity_p e, double s1, RXEntity_p p, double s2, RXEntity_p *e1,RXEntity_p *e2,    double tol ) {
/* Is there already a relsite in the IALISTS of e and p, within TOL of s1 and s2?
15/9/98  SITE also 

27/11/98 returns 	1 if there is a (rel)site in e close to s1
		AND	2 if there is a (rel)site in p close to s2
		AND	4 if there is a (rel)site that is both.
		AND 8 if there are two different relsites.
		AND 16 if the two different relsites are found in both lists.

Rewrite  Sept 2000.   We need to find 
*/
sc_ptr sc1, sc2;
 RXEntity_p commonPt;
double o1,o2;
int k,j,retval=0;
*e1=NULL; 
*e2=NULL; 
sc1 = (sc_ptr  )e; sc2 = (sc_ptr  )p;
    commonPt = Crossed(sc1,sc2,&o1,&o2);						// a topological test
	if(commonPt && ((fabs(o1-s1) > tol) || (fabs(o2-s2) > tol))) // found one but its somewhere else
		commonPt=NULL;

	*e1 = *e2 = commonPt;

if(commonPt)
	retval=19;
else  {
	for (k=0;k<sc1->n_angles ;k++) { 
		if( sc1->ia[k].s->TYPE==RELSITE || sc1->ia[k].s->TYPE==SITE) {
            o1 = ((sc1->ia[k]).m_Offset->Evaluate(NULL,1));
			if(fabs(o1-s1) < tol) {
				*e1=sc1->ia[k].s; 
				if(CUTDB){
						printf(" o1 match o1=%f s1=%f e1='%s' ", o1,s1,(*e1)->name());
						printf("  retval was %d", retval); 
					}
				retval = retval + ((1-(retval&1)) );

				if(CUTDB)printf("  ..becomes  %d\n", retval); 
				break; 
			}
		}
	}

	for (j=0;j<sc2->n_angles ;j++) {
		if (sc2->ia[j].s->TYPE==RELSITE|| sc2->ia[j].s->TYPE==SITE ) {
            o2 = ((sc2->ia[j]).m_Offset->Evaluate(NULL,1));
 			if( fabs(o2-s2) < tol ) {
				if(CUTDB){
					cout<< " o2 match  "<<endl;
					printf("o2=%f s2=%f e2='%s' ", o2,s2,sc2->ia[j].s->name());
					printf("  retval was %d", retval); 
				}
				*e2=sc2->ia[j].s; 
				retval = retval + ((2-(retval&2)) );
				if(CUTDB)printf("  ..becomes  %d\n", retval); 
				break;
			}
		}
	}
}
	if( ((*e1) !=NULL) &&((*e2) !=NULL) && ((*e1) !=(*e2)) ) {

// *e2 is in sc2 and *e1 is in sc1
// if *e2 is also in sc1 and *e1 is also in sc2, its OK to return 3
        if(sc1->In_IA_List(*e2) && sc2->In_IA_List(*e1)) {
// Lets let it go. 
			retval =retval&16;
		}
		else {

#ifdef _DEBUG
		// we get here if e1 and e2 arent the same
		//IE there are crosses between sc1 and a 3rd and between sc2 and a 4th
		// both of which are close to the intersection point we have just found
		// between sc1 and sc2.  
		char buf[512];
		cout<< " CASE 8  "<<endl;
		printf(" e1 is %s e2 is %s retval was %d\n", (*e1)->name(), (*e2)->name(), retval);		
		e->Print_One_Entity(stdout);
		p->Print_One_Entity(stdout);
		(*e1)->Print_One_Entity(stdout);
		(*e2)->Print_One_Entity(stdout);
		sprintf(buf,"Cross Already error on %s\nand\n%s\n found %s\n and\n%s",
			e->name(),p->name(), (*e1)->name(), (*e2)->name());
		e->OutputToClient(buf,2);

#endif

		retval = retval + ((8-(retval&8)) );
		}
	}

return retval;
}

					
