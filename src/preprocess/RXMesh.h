#pragma once
#include <vector>
#include <map>

#include "triangle.h"
#include "RXSitePt.h"
class rxONX_Model;
class RX_FESite;
class rxON_Mesh;
/// ONLY_EDGEEDGES makes the UV triangulation ignore internal segments.
/// It creates a new delaunay triangulation within the model edges,
/// using all the nodes but ignoring internal seams. This is a much better
/// mesh for the UV interpolation

 #define ONLY_EDGEEDGES false

#define vertexmark(vx)  ((int *) (vx))[m_m->vertexmarkindex]

typedef map<pair<int,int>,double>  spamat;

class RXMesh
{
    friend class RXShapeInterpolation;
    enum meshspace{ XYZ,UV,NOMESHSPACE};
public:
    RXMesh(void);
    virtual ~RXMesh(void);
    void Init();
    void SetONModel(rxONX_Model *p){m_onm=p;}
    int Triangulate(vector<RXSitePt> inpts, vector<int> enos,const char *buf,const int flag=0);
    int Triangulate(vector<RXSitePt> inpts, vector<pair<int,int> > &edges ,const char *buf,const int flag=0);
    int TriangulateForUV(SAIL *sail, vector<RX_FESite*> &pts,const vector<RX_FEedge*> &Fedges,const char *buf,const int flag=0);
    int Triangulate(vector<RXSitePtWithData*> &pts,const char *buf);


    void LocateInitialize(); // sets m_searchtri
    //int L ocate(const ON_Point *op,int*ii,double*ww); //seeds on m_searchtri
    int Locate(const ON_2dPoint p2,int*ii,double*ww); //seeds on m_searchtri
    void report(FILE *fp,struct triangulateio *io,
                int markers,int reporttriangles,int reportneighbors,int reportsegments,
                int reportedges, int reportnorms);

    int WriteAsPoly(struct triangulateio *io, const char *text) const;// the Shewchuk poly file format
    bool HasMesh(){return (m_m!=0);}
    int FlattenSP(vector<RXSitePt> &pts3dd );
    int Flatten(vector<RX_FESite*> &pts3d );
    void SetLambda(const double x){m_lambda=x;}
    int DrawHoops(rxONX_Model*mod,const int n) ;

protected:

    int MakeMeyerMatrices(vector<RX_FESite* > &pts3d, map<pair<int,int>,double> &ma,map<pair<int,int>,double> &machi );
    int MakeMeyerMatricesSP(vector<RXSitePt> &pts3d, map<pair<int,int>,double> &ma,map<pair<int,int>,double> &machi );


    int WritePoints(vector<RXSitePt> pts3d,const char*fname);
    int WriteLabels(vector<RXSitePt> pts3d,const char*fname); // a special for mathematica benchtesting


public:
    struct triangulateio *m_t; // the result of the triangulation. will be consistent with m_m and m_b;


private:	
    // Shewchuk data structures

    struct mesh *m_m;
    struct behavior *m_b;

    struct otri m_searchtri;
    rxONX_Model *m_onm;
    double m_lambda;
    map<int,int> ShewkIndex; // was fwdind
    map<int,int> NodeNo;  // was revind
protected:
    enum meshspace m_MeshSpace;
public:
    rxON_Mesh * MakeONMeshUV(vector<RXSitePt> &pts3d);
    rxON_Mesh * MakeONMeshXYZ(vector<RXSitePt> &pts3d);
    rxON_Mesh * MakeONMeshUV(vector<RX_FESite*> &pts3d);
    rxON_Mesh * MakeONMeshXYZ(vector<RX_FESite*> &pts3d);
};
