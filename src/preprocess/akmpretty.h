#ifndef _HDR_RPRETTY_H
#define _HDR_RPRETTY_H 1

#define VMAX 			50
#define MAX_MAP_COUNT 		36

#define N_RELAX_SHELLS 		26
#define N_RELAX_SHELLS_USED	17
#define MAX_TOGGLES 128 // changed Peter for V53

#define LR_DEFAULT_STATE 0
#define RELAX_OUT_STRING "RELAX"
#define LEFT_EDGE ((float) -2.3)
#define LEFT_BOX_EDGE ((float) -4.5)
#define RIGHT_BOX_EDGE ((float) -2.3)


EXTERN_C const char *g_relaxShell[N_RELAX_SHELLS];
#ifdef USE_PANSAIL
	#define N_PANSAIL_SHELLS 	4 
	EXTERN_C const char *g_pansailShell[N_PANSAIL_SHELLS];
	EXTERN_C int g_pnslU[];
#endif

EXTERN_C int g_rlxU[] ;



#endif  //#ifndef _HDR_RPRETTY_H

