
// curveintersection.cpp: 
//
//////////////////////////////////////////////////////////////////////


#include "StdAfx.h"



#include "rlxNearestPoint.h"
#include "slowcut.h"

#include "curveintersection.h"

int FindCurveIntersections(const ON_Curve* p_Crv1,
						   const ON_Curve* p_Crv2,
					 	   ON_ClassArray<RXCrossing2> & p_Intersections, 
						   const double     & p_Tolerence,
						   const double     & p_CutDistance,
						   const RXTRObject* p_pPersistentCrv1,
						   const RXTRObject* p_pPersistentCrv2)
{
	assert(dynamic_cast<const ON_Curve*>(p_Crv1)&&dynamic_cast<const ON_Curve*>(p_Crv2));
	
	int l_DoMergeEnds = 1; //By default we do a check to merge the intersection points at the end of the curves
	return FindCurveIntersections(p_Crv1,p_Crv2,p_Intersections,p_Tolerence,p_CutDistance,l_DoMergeEnds,p_pPersistentCrv1,p_pPersistentCrv2); 
}//int FindCurveIntersections

int FindCurveIntersections(const ON_Curve* p_Crv1,
						   const ON_Curve* p_Crv2,
					 	   ON_ClassArray<RXCrossing2> & p_Intersections, 
						   const double     & p_Tolerence,
						   const double     & p_CutDistance,
						   const int        & p_MergeToEnds,
						   const RXTRObject* p_pPersistentCrv1,
						   const RXTRObject* p_pPersistentCrv2)
{
	assert(dynamic_cast<const ON_Curve*>(p_Crv1)&&dynamic_cast<const ON_Curve*>(p_Crv2));
	int i;
	int l_res = 0;
	
	double l_EpsMinD = p_CutDistance; 
	double l_EpsBB = p_CutDistance;
	double l_EpsDot = 1.e-5;

	ON_3dPointArray l_pline_points1;
	ON_SimpleArray<double> l_pline_t1;
	ON_3dPointArray l_pline_points2;
	ON_SimpleArray<double> l_pline_t2;

	if ((p_Crv1->IsPolyline(&l_pline_points1,&l_pline_t1))&&(p_Crv2->IsPolyline(&l_pline_points2,&l_pline_t2)))
	{//The 2 curves can be seen as Polylines, NOTE: A line is seen as a polyline with only 2 points.
		// peter safer to cast as ON_POLYLINECurve
		rxON_PolylineCurve l_PL1; 
		rxON_PolylineCurve l_PL2;
		l_PL1.m_pline = l_pline_points1; 
		l_PL1.m_t = l_pline_t1; 
		
		l_PL2.m_pline = l_pline_points2; 
		l_PL2.m_t = l_pline_t2;

		l_res =  CrvCrvInter(&l_PL1,&l_PL2, p_Intersections, l_EpsMinD,l_EpsBB,l_EpsDot,p_pPersistentCrv1,p_pPersistentCrv2);
	}
	else
	{
		const ON_PolyCurve * l_Pcrv1 = ON_PolyCurve::Cast(p_Crv1); 
		const ON_PolyCurve * l_Pcrv2 = ON_PolyCurve::Cast(p_Crv2);

		if ((l_Pcrv1||l_Pcrv2)&&(!p_Crv1->IsPolyline())&&(!p_Crv2->IsPolyline()))
		{//if at least 1 of the curves is a polycurve
			ON_NurbsCurve l_nurbs;
			int l_nbcrv = 0;
			
			if (l_Pcrv1)
			{	
				l_nbcrv= l_Pcrv1->Count();
				for (i=0;i<l_nbcrv;i++)
				{
					l_Pcrv1->operator[](i)->NurbsCurve(&l_nurbs);
					l_res+=FindCurveIntersections(&l_nurbs,p_Crv2,p_Intersections, p_Tolerence,p_CutDistance,p_pPersistentCrv1,p_pPersistentCrv2);
				}
			}//if (l_Pcrv1)
			if (l_Pcrv2)
			{		
				l_nbcrv= l_Pcrv2->Count();
				for (i=0;i<l_nbcrv;i++)
				{
					l_Pcrv2->operator[](i)->NurbsCurve(&l_nurbs);
					l_res+=FindCurveIntersections(p_Crv1,&l_nurbs,p_Intersections, p_Tolerence,p_CutDistance,p_pPersistentCrv1,p_pPersistentCrv2);
				}
			}//if (l_Pcrv2)
		}//	if ((l_Pcrv1||l_Pcrv2)&&(!p_Crv1->IsPolyline())&&(!p_Crv2->IsPolyline()))
		else
		{
			if ((p_Crv1->IsPolyline(&l_pline_points1,&l_pline_t1))&&(!p_Crv2->IsPolyline()))
			{//ONLY the curve 1 is a polyline
				double l_nbPoints= l_pline_points1.Count();
				 ON_LineCurve l_line;
				for (i=0;i<l_nbPoints-1;i++)
				{
					l_line.m_line.from = l_pline_points1[i];
					l_line.m_line.to   = l_pline_points1[i+1];
					l_line.SetDomain(l_pline_t1[i],l_pline_t1[i+1]);
					ON_Curve * l_tempcrv1 = l_line.NurbsCurve();
					ON_Curve * l_tempcrv2 = p_Crv2->NurbsCurve();
					l_res += CrvCrvInter(&l_line,p_Crv2,l_tempcrv1,l_tempcrv2,p_Intersections, l_EpsMinD, l_EpsBB , l_EpsDot,p_pPersistentCrv1,p_pPersistentCrv2);
			//		l_res+=FindCurveIntersections(l_line,p_Crv2,p_Intersections, p_Tolerence,p_CutDistance);
					if (l_tempcrv1) l_tempcrv1 =NULL; // LEAK can it be a ON_NC?
					if (l_tempcrv2) l_tempcrv2 = NULL;// LEAK;
				}
			}//if ((p_Crv1->IsPolyline(&l_pline_points1,&l_pline_t1))&&(!p_Crv2->IsPolyline()))
			else
			{
				if ((!p_Crv1->IsPolyline())&&(p_Crv2->IsPolyline(&l_pline_points2,&l_pline_t2)))
				{//ONLY THE CURVE 2 is a polyline
					double l_nbPoints= l_pline_points2.Count();
					ON_LineCurve l_line;
					for (i=0;i<l_nbPoints-1;i++)
					{
						l_line.m_line.from = l_pline_points2[i];
						l_line.m_line.to   = l_pline_points2[i+1];
						l_line.SetDomain(l_pline_t2[i],l_pline_t2[i+1]);
						ON_Curve * l_tempcrv1(p_Crv1->NurbsCurve());
						ON_Curve * l_tempcrv2 ( l_line.NurbsCurve());
						l_res += CrvCrvInter(p_Crv1,&l_line,l_tempcrv1,l_tempcrv2,p_Intersections, l_EpsMinD, l_EpsBB , l_EpsDot,p_pPersistentCrv1,p_pPersistentCrv2);
				//		l_res+=FindCurveIntersections(p_Crv1,l_line,p_Intersections, p_Tolerence,p_CutDistance);
						if (l_tempcrv1) delete l_tempcrv1;l_tempcrv1 = NULL; //LEAK
						if (l_tempcrv2) delete l_tempcrv2;l_tempcrv2 =  NULL;
					}
				}//if ((!p_Crv1->IsPolyline())&&(p_Crv2->IsPolyline(&l_pline_points2,&l_pline_t2)))
				else
				{  // FROM HERE WE SHOULD NOT HAVE ANY POLYLINE
					assert((!p_Crv1->IsPolyline())&&(!p_Crv2->IsPolyline()));
					
					ON_Curve * l_tempcrv1( p_Crv1->NurbsCurve());
					ON_Curve * l_tempcrv2(  p_Crv2->NurbsCurve());
					l_res = CrvCrvInter(p_Crv1,p_Crv2,l_tempcrv1,l_tempcrv2,p_Intersections, l_EpsMinD, l_EpsBB , l_EpsDot,p_pPersistentCrv1,p_pPersistentCrv2);
					if (l_tempcrv1) l_tempcrv1 =  NULL;//LEAK
					if (l_tempcrv2) l_tempcrv2 =  NULL;
				}//else
			}
		}
		if (l_Pcrv1) l_Pcrv1=NULL;
		if (l_Pcrv2) l_Pcrv2=NULL;
	}
		
	if (!p_MergeToEnds)
		return l_res;

	//Intersection post processing 
	ON_3dPoint l_start1 = p_Crv1->PointAtStart();
	ON_3dPoint l_end1 = p_Crv1->PointAtEnd();
	ON_3dPoint l_start2 = p_Crv2->PointAtStart();
	ON_3dPoint l_end2 = p_Crv2->PointAtEnd();

	ON_3dPoint l_pt1,l_pt2;
	double l_dist = 0.0;
	int l_case = 0;
	
	for (i=0;i<l_res;i++)
	{
		l_case = 0;
		int l_ID = p_Intersections.Count()-l_res+i;

		l_pt1 = p_Intersections[l_ID].EvalPt(0); //Ptp_Crv1->PointAt(p_Intersections[i][0]);
		l_pt2 = p_Intersections[l_ID].EvalPt(1); //p_Crv2->PointAt(p_Intersections[i][1]);
		l_dist = l_pt1.DistanceTo(l_pt2);
		if (l_dist<=p_Tolerence)
			l_case = 1;
	
		if (l_pt1.DistanceTo(l_start1)<=p_CutDistance)
		{
			l_case = 2;
			p_Intersections[l_ID].GetPoint1()->SetToMin(); //p_Intersections[i][0] = p_Crv1->Domain().Min();
		}
		if (l_pt1.DistanceTo(l_end1)<=p_CutDistance)
		{
			l_case = 3;
			p_Intersections[l_ID].GetPoint1()->SetToMax(); ; //p_Intersections[i][0] = p_Crv1->Domain().Max();
		}
		if (l_pt2.DistanceTo(l_start2)<=p_CutDistance)
		{
			l_case = 4;
			p_Intersections[l_ID].GetPoint2()->SetToMin();  //p_Intersections[i][1] = p_Crv2->Domain().Min();
		}	
		if (l_pt2.DistanceTo(l_end2)<=p_CutDistance)
		{
			l_case = 5;
			p_Intersections[l_ID].GetPoint2()->SetToMax();  //p_Intersections[i][1] = p_Crv2->Domain().Max();
		}
		
		//if we arrive here it means the intersection doesn't match the tolerences ... We remove it
		if (l_case == 0)
		{
			p_Intersections.Remove(l_ID);
			l_res--;
			i--;
		}

	}
	return l_res; 
}//int FindCurveIntersection

/* First written for use in filament intersections
  We recursively subdivide the curve until the sub-BBs are smaller than p_b;
  if any of these small BBs intersect with the input BB return true.
*/

int CrvBBInter(const ON_Curve * p_Crv,
			   const rxON_BoundingBox &p_b,
			   ON_Interval &foundInterval 

			   ) 
{
	assert(dynamic_cast<const ON_Curve*>(p_Crv));
	
	rxON_BoundingBox l_Bb(p_Crv->BoundingBox());
	bool doesInt = l_Bb.Intersection (p_b ); //  Intersect this with other_bbox and save intersection in this.
	if(!doesInt )
			return 0;
//l_Bb is now the intersection of the curves BB and p_b
	if(l_Bb.Diagonal().LengthSquared() < p_b.Diagonal().LengthSquared()) { // its small
		return doesInt;
	}
		ON_Interval l_domain(p_Crv->Domain());
		double l_t = l_domain.Mid();//    .Min()+l_domain.Length()/2.0;

		//To split the curve 1

		ON_Curve * l_pcrv1 = NULL;
		ON_Curve * l_pcrv2 = NULL;
		ON_Interval i;

		if ( !p_Crv->Split( l_t, l_pcrv1,l_pcrv2) )
			return false;

		if(CrvBBInter(l_pcrv1,p_b,i )) {
			foundInterval=i; delete l_pcrv1; delete l_pcrv2;
			return 1;
		}
		if(CrvBBInter(l_pcrv2,p_b,i )) {
			foundInterval=i;delete l_pcrv1; delete l_pcrv2;
			return 1;
		}
	if(l_pcrv1 ) delete l_pcrv1;
	if (l_pcrv2 ) delete l_pcrv2;
return 0;
}//int CrvBBInter
 int CrvCrvInter(const ON_Curve * p_Crv1,
				const ON_Curve * p_Crv2,
				const ON_NurbsCurve * p_pcrv1,
				const ON_NurbsCurve * p_pcrv2, 
				ON_ClassArray<RXCrossing2> & p_Intersections, 
				const double & p_EpsMinD, 
				const double & p_EpsBB,
				const double & p_EpsDot,
			   const RXTRObject* p_pPersistentCrv1,
			   const RXTRObject* p_pPersistentCrv2)
{
	assert(dynamic_cast<const ON_Curve*>(p_Crv1)&&dynamic_cast<const ON_Curve*>(p_Crv2));
	assert(dynamic_cast<const ON_Curve*>(p_pcrv1)&&dynamic_cast<const ON_Curve*>(p_pcrv2));
	
	rxON_BoundingBox l_Bb1(p_pcrv1->BoundingBox());
	rxON_BoundingBox l_Bb2(p_pcrv2->BoundingBox());
	int l_n = 0;
	if (IsNear(l_Bb1,l_Bb2,p_EpsBB))
	{
		//CONVERGENCE TEST 
		if ((l_Bb1.Diagonal().Length()<=powf(float(p_EpsBB),1))||(l_Bb2.Diagonal().Length()<=powf(float(p_EpsBB),1)))
		{
			if ((l_Bb1.Area()<=powf(float(p_EpsBB),2))||(l_Bb2.Area()<=powf(float(p_EpsBB),2)))
			{
				int l_t1Fixed,l_t2Fixed;
				l_t1Fixed=l_t2Fixed=1;
				//double l_t1,l_t2;
				
				RXUPoint l_Ptcrv1;
				RXUPoint l_Ptcrv2;
				l_Ptcrv1.Init();
				l_Ptcrv2.Init();
			
				if ((p_pcrv1->Domain().Min()==p_Crv1->Domain().Min())||(p_pcrv1->Domain().Min()==p_Crv1->Domain().Max()))
					if (p_pPersistentCrv1)
						l_Ptcrv1.Set(p_pcrv1->Domain().Min(),p_pPersistentCrv1);  //Set to the temporary curve
					else
						assert(0); //DEBUG FORW NOW 08 11 04  l_Ptcrv1.Set(p_pcrv1->Domain().Min(),p_Crv1);  //Set to the temporary curve
				else
					if ((p_pcrv1->Domain().Max()==p_Crv1->Domain().Min())||(p_pcrv1->Domain().Max()==p_Crv1->Domain().Max()))
						if (p_pPersistentCrv1)
							l_Ptcrv1.Set(p_pcrv1->Domain().Max(),p_pPersistentCrv1);
						else
							assert(0); //DEBUG FORW NOW 08 11 04   l_Ptcrv1.Set(p_pcrv1->Domain().Max(),p_Crv1);
					else
					{
						if (p_pPersistentCrv1)
							l_Ptcrv1.Set(p_pcrv1->Domain().Mid(),p_pPersistentCrv1);
						else
							assert(0); //DEBUG FORW NOW 08 11 04   l_Ptcrv1.Set(p_pcrv1->Domain().Mid(),p_Crv1);
						l_t1Fixed=0;
					}
				
				if ((p_pcrv2->Domain().Min()==p_Crv2->Domain().Min())
					||(p_pcrv2->Domain().Min()==p_Crv2->Domain().Max())) {
						RXCurve l_cc(p_Crv2);
						l_Ptcrv2.Set(p_pcrv2->Domain().Min(),&l_cc); assert(0);
				}
//:266: warning: taking address of temporary

				else
					if ((p_pcrv2->Domain().Max()==p_Crv2->Domain().Min())||(p_pcrv2->Domain().Max()==p_Crv2->Domain().Max()))
						if (p_pPersistentCrv2)
							l_Ptcrv2.Set(p_pcrv2->Domain().Max(),p_pPersistentCrv2);
						else
							assert(0); //DEBUG FORW NOW 08 11 04   l_Ptcrv2.Set(p_pcrv2->Domain().Max(),&RXCurve(p_Crv2));
					else
					{
						if (p_pPersistentCrv2)
							l_Ptcrv2.Set(p_pcrv2->Domain().Mid(),p_pPersistentCrv2);
						else
							assert(0); //DEBUG FORW NOW 08 11 04   l_Ptcrv2.Set(p_pcrv2->Domain().Mid(),&RXCurve(p_Crv2));
						l_t2Fixed=0;
					}
				
				RXCrossing2 l_Intersection;
				l_Intersection.SetPoint1(l_Ptcrv1);
				l_Intersection.SetPoint2(l_Ptcrv2);

				//find t1 and t2 such like the line beetween Pt1 and Pt2 is perpendicular to both curves
				if (!AlreadyFound(p_Intersections, l_Intersection,p_EpsMinD,p_EpsBB))
				{
					//int l_res = CrvCrvPerpendicular(p_Crv1,p_Crv2,l_t1,l_t2,l_t1Fixed,l_t2Fixed,p_EpsDot);
				   int l_res = CrvCrvPerpendicular(p_Crv1,p_Crv2,l_Intersection,0,0,p_EpsDot,p_pPersistentCrv1,p_pPersistentCrv2);
				   //int l_res = CrvCrvPerpendicular(*p_pcrv1,*p_pcrv2,l_Intersection,0,0,p_EpsDot);

					if (l_Intersection.Distance()<=p_EpsBB)//p_Crv1->PointAt(l_t1).DistanceTo(p_Crv2->PointAt(l_t2))<=p_EpsBB)
					{
						if (!AlreadyFound(p_Intersections, l_Intersection,p_EpsMinD,p_EpsBB))
						{
							l_n++;
							p_Intersections.AppendNew() = l_Intersection;//	p_Intersections.Append(ON_2dPoint(l_t1,l_t2));
							return l_n;
						}
					}
				}
				return l_n;
			}//if ((l_Bb1.Diagonal().Length()<=powf(float(p_EpsBB),1))||(l_Bb1.Diagonal().Length()<=powf(float(p_EpsBB),1)))
		}//if ((l_Bb1.Diagonal().Length()<=powf(float(p_EpsBB),1))||(l_Bb1.Diagonal().Length()<=powf(float(p_EpsBB),1)))

		ON_Interval l_domain1(p_pcrv1->Domain());
		ON_Interval l_domain2(p_pcrv2->Domain());

		double l_t1 = l_domain1.Min()+l_domain1.Length()/2.0;
		double l_t2 = l_domain2.Min()+l_domain2.Length()/2.0;

		//To split the curve 1
		//ON_NurbsCurve l_crv11,l_crv12;
		ON_Curve * l_pcrv11 = NULL;
		ON_Curve * l_pcrv12 = NULL;
		
		ON_Interval l_Int1 = p_pcrv1->Domain();
		ON_Interval l_Int2 = p_pcrv2->Domain();
		if ( !p_pcrv1->Split( l_t1, l_pcrv11,l_pcrv12) )
			return false;

		//to split the curve 2
		//ON_NurbsCurve l_crv21,l_crv22;
		ON_Curve * l_pcrv21 = NULL; 
		ON_Curve * l_pcrv22 = NULL; 
		
		if ( !p_pcrv2->Split( l_t2, l_pcrv21,l_pcrv22) )
			return false;

		//WHY DO we do l_pcrv11->NurbsCurve(),l_pcrv21->NurbsCurve() and not l_pcrv11, l_pcrv21  in CrvCrvInetr?? thomas 28 10 04
		l_n+= CrvCrvInter(p_Crv1,p_Crv2,l_pcrv11->NurbsCurve(),l_pcrv21->NurbsCurve(),p_Intersections,p_EpsMinD,p_EpsBB,p_EpsDot,p_pPersistentCrv1,p_pPersistentCrv2);
		l_n+= CrvCrvInter(p_Crv1,p_Crv2,l_pcrv11->NurbsCurve(),l_pcrv22->NurbsCurve(),p_Intersections,p_EpsMinD,p_EpsBB,p_EpsDot,p_pPersistentCrv1,p_pPersistentCrv2);		
		l_n+= CrvCrvInter(p_Crv1,p_Crv2,l_pcrv12->NurbsCurve(),l_pcrv21->NurbsCurve(),p_Intersections,p_EpsMinD,p_EpsBB,p_EpsDot,p_pPersistentCrv1,p_pPersistentCrv2);
		l_n+= CrvCrvInter(p_Crv1,p_Crv2,l_pcrv12->NurbsCurve(),l_pcrv22->NurbsCurve(),p_Intersections,p_EpsMinD,p_EpsBB,p_EpsDot,p_pPersistentCrv1,p_pPersistentCrv2);

		l_pcrv11=l_pcrv12=NULL;
		l_pcrv21=l_pcrv22=NULL;
	}
	return l_n;
}//int CrvCrvInter


 int CrvCrvInter(const ON_Curve * p_Crv1,
				const ON_Curve * p_Crv2,
				const ON_Curve * p_pcrv1,
				const ON_Curve * p_pcrv2, 
				ON_ClassArray<RXCrossing2> & p_Intersections, 
				const double & p_EpsMinD, 
				const double & p_EpsBB,
				const double & p_EpsDot,
			   const RXTRObject* p_pPersistentCrv1,
			   const RXTRObject* p_pPersistentCrv2)
{
	assert(dynamic_cast<const ON_Curve*>(p_Crv1)&&dynamic_cast<const ON_Curve*>(p_Crv2));
	assert(dynamic_cast<const ON_Curve*>(p_pcrv1)&&dynamic_cast<const ON_Curve*>(p_pcrv2));
	
	rxON_BoundingBox l_Bb1(p_pcrv1->BoundingBox());
	rxON_BoundingBox l_Bb2(p_pcrv2->BoundingBox());
	int l_n = 0;
	if (IsNear(l_Bb1,l_Bb2,p_EpsBB))
	{
		//CONVERGENCE TEST 
		if ((l_Bb1.Diagonal().Length()<=powf(float(p_EpsBB),1))||(l_Bb2.Diagonal().Length()<=powf(float(p_EpsBB),1)))
		{
			if ((l_Bb1.Area()<=powf(float(p_EpsBB),2))||(l_Bb2.Area()<=powf(float(p_EpsBB),2)))
			{
				int l_t1Fixed,l_t2Fixed;
				l_t1Fixed=l_t2Fixed=1;
				//double l_t1,l_t2;
				
				RXUPoint l_Ptcrv1;
				RXUPoint l_Ptcrv2;
				l_Ptcrv1.Init();
				l_Ptcrv2.Init();
			
				if ((p_pcrv1->Domain().Min()==p_Crv1->Domain().Min())||(p_pcrv1->Domain().Min()==p_Crv1->Domain().Max()))
					if (p_pPersistentCrv1)
						l_Ptcrv1.Set(p_pcrv1->Domain().Min(),p_pPersistentCrv1);  //Set to the temporary curve
					else
						assert(0); //DEBUG FORW NOW 08 11 04  l_Ptcrv1.Set(p_pcrv1->Domain().Min(),p_Crv1);  //Set to the temporary curve
				else
					if ((p_pcrv1->Domain().Max()==p_Crv1->Domain().Min())||(p_pcrv1->Domain().Max()==p_Crv1->Domain().Max()))
						if (p_pPersistentCrv1)
							l_Ptcrv1.Set(p_pcrv1->Domain().Max(),p_pPersistentCrv1);
						else
							assert(0); //DEBUG FORW NOW 08 11 04   l_Ptcrv1.Set(p_pcrv1->Domain().Max(),p_Crv1);
					else
					{
						if (p_pPersistentCrv1)
							l_Ptcrv1.Set(p_pcrv1->Domain().Mid(),p_pPersistentCrv1);
						else
							assert(0); //DEBUG FORW NOW 08 11 04   l_Ptcrv1.Set(p_pcrv1->Domain().Mid(),p_Crv1);
						l_t1Fixed=0;
					}
				
				if ((p_pcrv2->Domain().Min()==p_Crv2->Domain().Min())
					||(p_pcrv2->Domain().Min()==p_Crv2->Domain().Max())) {
						RXCurve l_cc(p_Crv2);
						l_Ptcrv2.Set(p_pcrv2->Domain().Min(),&l_cc); assert(0);
				}
//:266: warning: taking address of temporary

				else
					if ((p_pcrv2->Domain().Max()==p_Crv2->Domain().Min())||(p_pcrv2->Domain().Max()==p_Crv2->Domain().Max()))
						if (p_pPersistentCrv2)
							l_Ptcrv2.Set(p_pcrv2->Domain().Max(),p_pPersistentCrv2);
						else
							assert(0); //DEBUG FORW NOW 08 11 04   l_Ptcrv2.Set(p_pcrv2->Domain().Max(),&RXCurve(p_Crv2));
					else
					{
						if (p_pPersistentCrv2)
							l_Ptcrv2.Set(p_pcrv2->Domain().Mid(),p_pPersistentCrv2);
						else
							assert(0); //DEBUG FORW NOW 08 11 04   l_Ptcrv2.Set(p_pcrv2->Domain().Mid(),&RXCurve(p_Crv2));
						l_t2Fixed=0;
					}
				
				RXCrossing2 l_Intersection;
				l_Intersection.SetPoint1(l_Ptcrv1);
				l_Intersection.SetPoint2(l_Ptcrv2);

				//find t1 and t2 such like the line beetween Pt1 and Pt2 is perpendicular to both curves
				if (!AlreadyFound(p_Intersections, l_Intersection,p_EpsMinD,p_EpsBB))
				{
					//int l_res = CrvCrvPerpendicular(p_Crv1,p_Crv2,l_t1,l_t2,l_t1Fixed,l_t2Fixed,p_EpsDot);
				   int l_res = CrvCrvPerpendicular(p_Crv1,p_Crv2,l_Intersection,0,0,p_EpsDot,p_pPersistentCrv1,p_pPersistentCrv2);
				   //int l_res = CrvCrvPerpendicular(*p_pcrv1,*p_pcrv2,l_Intersection,0,0,p_EpsDot);

					if (l_Intersection.Distance()<=p_EpsBB)//p_Crv1->PointAt(l_t1).DistanceTo(p_Crv2->PointAt(l_t2))<=p_EpsBB)
					{
						if (!AlreadyFound(p_Intersections, l_Intersection,p_EpsMinD,p_EpsBB))
						{
							l_n++;
							p_Intersections.AppendNew() = l_Intersection;//	p_Intersections.Append(ON_2dPoint(l_t1,l_t2));
							return l_n;
						}
					}
				}
				return l_n;
			}//if ((l_Bb1.Diagonal().Length()<=powf(float(p_EpsBB),1))||(l_Bb1.Diagonal().Length()<=powf(float(p_EpsBB),1)))
		}//if ((l_Bb1.Diagonal().Length()<=powf(float(p_EpsBB),1))||(l_Bb1.Diagonal().Length()<=powf(float(p_EpsBB),1)))

		ON_Interval l_domain1(p_pcrv1->Domain());
		ON_Interval l_domain2(p_pcrv2->Domain());

		double l_t1 = l_domain1.Min()+l_domain1.Length()/2.0;
		double l_t2 = l_domain2.Min()+l_domain2.Length()/2.0;

		//To split the curve 1
		//ON_NurbsCurve l_crv11,l_crv12;
		ON_Curve * l_pcrv11 = NULL;
		ON_Curve * l_pcrv12 = NULL;
		
		ON_Interval l_Int1 = p_pcrv1->Domain();
		ON_Interval l_Int2 = p_pcrv2->Domain();
		if ( !p_pcrv1->Split( l_t1, l_pcrv11,l_pcrv12) )
			return false;

		//to split the curve 2
		//ON_NurbsCurve l_crv21,l_crv22;
		ON_Curve * l_pcrv21 = NULL; 
		ON_Curve * l_pcrv22 = NULL; 
		
		if ( !p_pcrv2->Split( l_t2, l_pcrv21,l_pcrv22) )
			return false;

		//WHY DO we do l_pcrv11->NurbsCurve(),l_pcrv21->NurbsCurve() and not l_pcrv11, l_pcrv21  in CrvCrvInetr?? thomas 28 10 04
		l_n+= CrvCrvInter(p_Crv1,p_Crv2,l_pcrv11->NurbsCurve(),l_pcrv21->NurbsCurve(),p_Intersections,p_EpsMinD,p_EpsBB,p_EpsDot,p_pPersistentCrv1,p_pPersistentCrv2);
		l_n+= CrvCrvInter(p_Crv1,p_Crv2,l_pcrv11->NurbsCurve(),l_pcrv22->NurbsCurve(),p_Intersections,p_EpsMinD,p_EpsBB,p_EpsDot,p_pPersistentCrv1,p_pPersistentCrv2);		
		l_n+= CrvCrvInter(p_Crv1,p_Crv2,l_pcrv12->NurbsCurve(),l_pcrv21->NurbsCurve(),p_Intersections,p_EpsMinD,p_EpsBB,p_EpsDot,p_pPersistentCrv1,p_pPersistentCrv2);
		l_n+= CrvCrvInter(p_Crv1,p_Crv2,l_pcrv12->NurbsCurve(),l_pcrv22->NurbsCurve(),p_Intersections,p_EpsMinD,p_EpsBB,p_EpsDot,p_pPersistentCrv1,p_pPersistentCrv2);

		l_pcrv11=l_pcrv12=NULL;
		l_pcrv21=l_pcrv22=NULL;
	}
	return l_n;
}//int CrvCrvInter

int IsNear(const rxON_BoundingBox & p_Bb1,
		   const rxON_BoundingBox & p_Bb2,
		   const double         & p_EpsBB)
{
	rxON_BoundingBox l_BB;
	int l_res = l_BB.Intersection(p_Bb1,p_Bb2);
	
	if (l_res)
		return 1;
	else
	{
		ON_3dPoint l_ptEpsBB(p_EpsBB,p_EpsBB,p_EpsBB);
		l_BB = rxON_BoundingBox(p_Bb1.Min()-l_ptEpsBB,p_Bb1.Max()+l_ptEpsBB);
		l_res = l_BB.Intersection(l_BB,p_Bb2);
		if (l_res)
			return 1; 
		else
		{
			l_BB = rxON_BoundingBox(p_Bb2.Min()-l_ptEpsBB,p_Bb2.Max()+l_ptEpsBB);
			l_res = l_BB.Intersection(l_BB,p_Bb1);
			if (l_res)
				return 1;
			else
				return 0;
		}
	}
}//int IsNear

int AlreadyFound(ON_ClassArray<RXCrossing2> & p_Intersections, 
				  const RXCrossing2 & p_point,
				  const double p_EpsMinD, 
				  const double & p_EpsBB)
{
	//It's allready found if one of the coordinates is found in p_Intersections 
	//or if the distance beetween p_point and p_Intersections[i]  < p_EpsMinD
	int i;
	int l_n = p_Intersections.Count();

	ON_3dPoint l_ptcrv1 = p_point.EvalPt(0); //p_Crv1->PointAt(p_point[0]);
	ON_3dPoint l_ptcrv2 = p_point.EvalPt(1); //p_Crv2->PointAt(p_point[1]);

	for (i=0;i<l_n;i++)
	{	
		if (l_ptcrv1.DistanceTo(p_Intersections[i].EvalPt(0))<p_EpsMinD)
			return 1;
		if (l_ptcrv2.DistanceTo(p_Intersections[i].EvalPt(1))<p_EpsMinD)
			return 1;
	}
	return 0;	
}//int AllreadyFound
int CrvCrvPerpendicular(const ON_Curve * p_crv1,
						const ON_Curve * p_crv2,
						RXCrossing2 & p_Inter,
						const int & p_t1Fixed,
						const int & p_t2Fixed,
						const double & p_EpsDot,
					   const RXTRObject* p_pPersistentCrv1,
					   const RXTRObject* p_pPersistentCrv2)
{
	assert(dynamic_cast<const ON_Curve*>(p_crv1)&&dynamic_cast<const ON_Curve*>(p_crv2));

	ON_Interval l_domain1 = p_Inter.GetPoint1()->Domain(); //p_Crv1->Domain();
	ON_Interval l_domain2 = p_Inter.GetPoint2()->Domain(); //p_Crv2->Domain();
	
	//This method works well if the curves are not paralleles
	//we are performing a test if the curve are parallele we fixing one of the points setting the intersection 
	ON_3dVector l_tangent1,l_tangent2;

	if ((p_t1Fixed)&&(p_t2Fixed))
		return 0;

	ON_3dPoint l_Pt1 = p_Inter.EvalPt(0); //(p_Crv1->PointAt(p_t1));
	ON_3dPoint l_Pt2 = p_Inter.EvalPt(1); //(p_Crv2->PointAt(p_t2));

	double l_t2 = 0;
	double l_t1 = 0;

	l_t1 = p_Inter.GetPoint1()->Gett();
	l_t2 = p_Inter.GetPoint2()->Gett();

	if (p_t1Fixed)
	{
		int l_res1 = GetNearestPoint(p_crv2,l_Pt1,&l_t2,*(p_Inter.GetPoint2()),p_EpsDot,1.0e-3,100,p_pPersistentCrv2);
		return 1;
	}
	if (p_t2Fixed)
	{
		int l_res1 = GetNearestPoint(p_crv1,l_Pt2,&l_t1,*(p_Inter.GetPoint1()),p_EpsDot,1.0e-3,100,p_pPersistentCrv1);
		return 1;
	}

	ON_3dVector l_line;
	double l_dot1,l_dot2;

	int l_loop = 0;
	double l_length = 0.0;
	double l_Alpha1, l_Alpha2;

	int l_Pt1Moves =1;
	int l_Pt2Moves =1;
	ON_3dPoint l_new_Pt1; 
	ON_3dPoint l_new_Pt2;

	while (1)
	{
		if (GetNearestPoint(p_crv2,l_Pt1,&l_t2,*(p_Inter.GetPoint2()),p_EpsDot,1.0e-3,100,p_pPersistentCrv2))  //if (GetNearestPoint(p_crv2,l_Pt1,p_t2,p_EpsDot,2.0,5,30,l_crvOUT2))
			if (GetNearestPoint(p_crv1,l_Pt2,&l_t1,*(p_Inter.GetPoint1()),p_EpsDot,1.0e-3,100,p_pPersistentCrv1))
			{
				l_new_Pt2 = p_Inter.EvalPt(1); 
				if (l_new_Pt2!=l_Pt2) 
					l_Pt2 = l_new_Pt2;
				else
					l_Pt2Moves = 0;

				l_new_Pt1 = p_Inter.EvalPt(0); 
				if (l_new_Pt1!=l_Pt1) 
					l_Pt1 = l_new_Pt1;
				else
					l_Pt1Moves = 0;
				if ((!l_Pt1Moves)&&(!l_Pt2Moves))
					break;
			}
			else
				return 0;
		else
			return 0;

		l_tangent1 = p_Inter.GetPoint1()->GetTangent(); //p_Crv1->TangentAt(p_t1);
		l_tangent2 = p_Inter.GetPoint2()->GetTangent(); //p_Crv2->PointAt(p_t1);
		
		l_line = (l_Pt2-l_Pt1);
		l_length = l_line.Length();
		if (l_length<p_EpsDot)
			break;
		
		l_dot1 = ON_DotProduct(l_line,l_tangent1);
		l_dot2 = ON_DotProduct(l_line,l_tangent2);

		l_Alpha1 = acos(l_dot1/l_length);
		l_Alpha2 = acos(l_dot2/l_length);
		if ((fabs(l_Alpha1)<p_EpsDot)&&(fabs(l_Alpha2)<p_EpsDot))
			break;

		l_loop++;
		if (l_loop>100)
			break;
	}

	return 1;
}//int CrvCrvPerpendicular


int CrvCrvInter(const rxON_PolylineCurve * p_Crv1,
			    const rxON_PolylineCurve * p_Crv2,
				ON_ClassArray<RXCrossing2> & p_Intersections, 
				const double & p_EpsMinD, 
				const double & p_EpsBB,
				const double & p_EpsDot,
			   const RXTRObject* p_pPersistentCrv1,
			   const RXTRObject* p_pPersistentCrv2)
{
	assert(ON_PolylineCurve::Cast(p_Crv1)&&ON_PolylineCurve::Cast(p_Crv2));

	if (!IsNear(p_Crv1->BoundingBox(),p_Crv2->BoundingBox(),p_EpsBB))
		return 0;

	int i,j;
	int l_nbline1 = p_Crv1->m_pline.Count();  
	int l_nbline2 = p_Crv2->m_pline.Count();  

	int l_n=0;

	rxON_LineCurve l_line1;
	rxON_LineCurve l_line2;
	ON_3dPoint l_temp; 
	
	ON_3dPoint l_pt1,l_pt2;
	RXCrossing2 l_Intersection;
	for (i=0;i<l_nbline1-1;i++)
	{			
		l_line1.SetDomain(p_Crv1->m_t[i],p_Crv1->m_t[i+1]);
		l_line1.m_line.from = p_Crv1->m_pline[i];
		l_line1.m_line.to = p_Crv1->m_pline[i+1];
		assert(l_line1.IsValid());
		for (j=0;j<l_nbline2-1;j++)
		{
			l_line2.SetDomain(p_Crv2->m_t[j],p_Crv2->m_t[j+1]);
			l_pt1 = p_Crv2->m_pline[j];
			l_pt2 = p_Crv2->m_pline[j+1];
			l_line2.m_line.from = l_pt1;
			l_line2.m_line.to = l_pt2;
			assert(l_line2.IsValid());
			if (CrvCrvInter(&l_line1,&l_line2,l_Intersection,p_EpsMinD,p_EpsBB,p_EpsDot,p_pPersistentCrv1,p_pPersistentCrv2))
				if (!AlreadyFound(p_Intersections, l_Intersection,p_EpsMinD,p_EpsBB))
				{
					p_Intersections.AppendNew() = l_Intersection;
					l_n++;
				}
				else
				{
					int j;
					int l_IsNear = 0;
					for (j=0;j<p_Intersections.Count();j++)
					{
						if (l_Intersection.EvalPt(0).DistanceTo(p_Intersections[j].EvalPt(0))<p_EpsBB)
							l_IsNear = 1;
						if (l_Intersection.EvalPt(1).DistanceTo(p_Intersections[j].EvalPt(1))<p_EpsBB)
							l_IsNear = 1;
						if (l_IsNear)
							if (l_Intersection.Distance()<p_Intersections[j].Distance())
								p_Intersections[j] = l_Intersection;
					}
				}
		}
	}//for (i=0;i<l_nbline1;i++)
		
	return l_n;
}//int CrvCrvInter

int CrvCrvInter(const rxON_LineCurve * p_Crv1,
				const rxON_LineCurve * p_Crv2,
				RXCrossing2 & p_Intersection, 
				const double & p_EpsMinD, 
				const double & p_EpsBB,
				const double & p_EpsDot,
			   const RXTRObject* p_pPersistentCrv1,
			   const RXTRObject* p_pPersistentCrv2)
{
	assert(ON_LineCurve::Cast(p_Crv1)&&ON_LineCurve::Cast(p_Crv2));

	int l_res = IsNear(p_Crv1->BoundingBox(),p_Crv2->BoundingBox(),p_EpsBB);

	if (!l_res)
		return 0;

	ON_3dPoint A1 = p_Crv1->PointAtStart();
	ON_3dPoint A2 = p_Crv2->PointAtStart();
	ON_3dVector V1 = p_Crv1->PointAtEnd()-A1;
	ON_3dVector V2 = p_Crv2->PointAtEnd()-A2;
	V1.Unitize();
	V2.Unitize();

	double l_crossprod = ON_CrossProduct(V1,V2).Length();
	if (l_crossprod<1.0e-6)
	{
		char l_buff[256];
		sprintf(l_buff, "Trying to find the intersection of 2 parallele lines, 0 intersection is returned, A1(%f,%f,%f) V1(%f,%f,%f) A2(%f,%f,%f) V2(%f,%f,%f)",A1[0],A1[1],A1[2],V1[0],V1[1],V1[2],A2[0],A2[1],A2[2],V2[0],V2[1],V2[2]);
#ifdef _GLOBAL_DECLARATIONS_H_
		error(l_buff,2);
#endif //#ifdef _GLOBAL_DECLARATIONS_H_
		return 0;
	}
	
	double k1 = -(V2[0]*V1[0]*V2[1]*A2[1]-powf(V2[1],2)*V1[0]*A2[0]+V2[1]*V1[1]*V2[2]*A2[2]+powf(V2[0],2)*V1[2]*A1[2]-powf(V2[0],2)*V1[2]*A2[2]-V2[0]*V1[0]*V2[2]*A1[2]+powf(V2[0],2)*V1[1]*A1[1]-powf(V2[0],2)*V1[1]*A2[1]+powf(V2[1],2)*V1[2]*A1[2]+powf(V2[1],2)*V1[0]*A1[0]-powf(V2[2],2)*V1[0]*A2[0]+V2[2]*V1[2]*V2[1]*A2[1]+powf(V2[2],2)*V1[1]*A1[1]-powf(V2[1],2)*V1[2]*A2[2]-V2[0]*V1[0]*V2[1]*A1[1]+powf(V2[2],2)*V1[0]*A1[0]+V2[0]*V1[0]*V2[2]*A2[2]-powf(V2[2],2)*V1[1]*A2[1]+V2[2]*V1[2]*V2[0]*A2[0]-V2[2]*V1[2]*V2[1]*A1[1]-V2[2]*V1[2]*V2[0]*A1[0]+V2[1]*V1[1]*V2[0]*A2[0]-V2[1]*V1[1]*V2[2]*A1[2]-V2[1]*V1[1]*V2[0]*A1[0])/(powf(V2[0],2)*powf(V1[2],2)+powf(V2[0],2)*powf(V1[1],2)+powf(V2[1],2)*powf(V1[0],2)+powf(V2[1],2)*powf(V1[2],2)+powf(V2[2],2)*powf(V1[1],2)+powf(V2[2],2)*powf(V1[0],2)-2*V2[1]*V1[1]*V2[2]*V1[2]-2*V2[0]*V1[0]*V2[2]*V1[2]-2*V2[0]*V1[0]*V2[1]*V1[1]);
	double k2 = -(V2[0]*A2[0]*powf(V1[2],2)+V2[0]*A2[0]*powf(V1[1],2)-V2[0]*A1[0]*powf(V1[2],2)-V2[0]*V1[0]*V1[1]*A2[1]+V2[0]*V1[0]*V1[1]*A1[1]+V2[0]*V1[0]*V1[2]*A1[2]-V2[0]*V1[0]*V1[2]*A2[2]-V2[0]*A1[0]*powf(V1[1],2)+V2[2]*A2[2]*powf(V1[1],2)+powf(V1[0],2)*V2[1]*A2[1]-V1[0]*V2[1]*V1[1]*A2[0]+V2[1]*A2[1]*powf(V1[2],2)+V2[1]*V1[1]*V1[2]*A1[2]-V1[0]*V2[2]*V1[2]*A2[0]-V2[1]*V1[1]*V1[2]*A2[2]+V1[0]*V2[1]*V1[1]*A1[0]-V2[2]*V1[2]*V1[1]*A2[1]+V2[2]*V1[2]*V1[1]*A1[1]+V1[0]*V2[2]*V1[2]*A1[0]-V2[1]*A1[1]*powf(V1[2],2)-powf(V1[0],2)*V2[2]*A1[2]+powf(V1[0],2)*V2[2]*A2[2]-powf(V1[0],2)*V2[1]*A1[1]-V2[2]*A1[2]*powf(V1[1],2))/(powf(V2[0],2)*powf(V1[2],2)+powf(V2[0],2)*powf(V1[1],2)+powf(V2[1],2)*powf(V1[0],2)+powf(V2[1],2)*powf(V1[2],2)+powf(V2[2],2)*powf(V1[1],2)+powf(V2[2],2)*powf(V1[0],2)-2*V2[1]*V1[1]*V2[2]*V1[2]-2*V2[0]*V1[0]*V2[2]*V1[2]-2*V2[0]*V1[0]*V2[1]*V1[1]);

	double l_len1,l_len2;
	p_Crv1->GetLength(&l_len1);
	p_Crv2->GetLength(&l_len2);

	k1 = max(-p_EpsBB,min(k1,l_len1+p_EpsBB));
	k2 = max(-p_EpsBB,min(k2,l_len2+p_EpsBB));

	ON_3dPoint l_pt1 = A1+k1*V1;
	ON_3dPoint l_pt2 = A2+k2*V2;
	if (l_pt1.DistanceTo(l_pt2)>p_EpsBB)
		return 0;
	else
	{
		double l_t1 = p_Crv1->Domain().Min()+(k1/l_len1)*p_Crv1->Domain().Length(); 
		double l_t2 = p_Crv2->Domain().Min()+(k2/l_len2)*p_Crv2->Domain().Length();
		RXUPoint l_Point1,l_Point2; 
		if (p_pPersistentCrv1)
			l_Point1.Set(l_t1,p_pPersistentCrv1);
		else
			assert(0); //DEBUG FOW NOW 08 11 04 l_Point1.Set(l_t1,&RXCurve(p_Crv1));
		
		if (p_pPersistentCrv2)
			l_Point2.Set(l_t2,p_pPersistentCrv2);
		else
			assert(0); //DEBUG FOW NOW 08 11 04 l_Point2.Set(l_t2,&RXCurve(p_Crv2));

		p_Intersection.SetPoint1(l_Point1);
		p_Intersection.SetPoint2(l_Point2);
		return 1;
	}
	return 0;//we never get here
}//int CrvCrvInter


int SortCrossingOnCurve(ON_ClassArray<RXCrossing2> & p_Intersections, 
						const int & p_ID)						
//Sort the crossing point by parameter value on the curve defined by p_ID
/*
	if p_ID == 0 sort the crossing point along the curve 1
	if p_ID == 1 sort the crossing point along the curve 2
*/
{	
	ON_ClassArray<RXCrossing2> l_temp; 
	int l_n = p_Intersections.Count();
	int l_nINIT = l_n;
	int i,j;
	double l_tj = 0.0;
	double l_ti = 0.0;
	double l_tmin = 0.0;
	int l_minID = 0;
	for (i=0;i<l_n;i++)
	{
		l_ti = p_Intersections[i].GetPoint(p_ID)->Gett();
		l_tmin= l_ti;
		l_minID = i;
		for (j=i+1;j<l_n;j++)
		{
			l_tj = p_Intersections[j].GetPoint(p_ID)->Gett();
			if (l_tj<l_tmin)
			{
				l_tmin=l_tj; 
				l_minID = j;
			}
		}
		l_temp.AppendNew() = p_Intersections[l_minID];
		p_Intersections.Remove(l_minID);
		i--;
		l_n--;
	}	

	p_Intersections = l_temp;

	return 1;
}//int SortCrossingOnCurve
