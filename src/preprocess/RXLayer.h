#pragma once

// entity type  PCE_LAYER

#include "RXEntity.h"

#include "RXSitePt.h"
#define MXLENGTH 9
typedef vector<double> MatDataType;
typedef MatValType * (*MatFuncPtr)(double *thickness,MatValType *,const class RXSitePt *,class RX_FETri3 *,class RXLayer * ,MatDataType &xv,double[3]);

struct MATERIAL_FIELD {
        MatFuncPtr function;
        int m_sizeofdata;		// ssd by m_Data.size()
        MatDataType m_Data;    // data to call it with
        RXEntity_p ref_sc;
        struct LINKLIST *ll;
};//struct MATERIAL_FIELD
class RXLayer: // usually owned by a panel 
	public RXEntity
{
public:
	RXLayer(void);
	virtual ~RXLayer(void); 
	RXLayer (SAIL *sail);
	int CClear();
	int Init(void);
	int Compute(void);
	int Resolve(void);
	int Finish(void);
	int ReWriteLine(void);
	int Dump( FILE *fp) const;

public:
        ON_3dVector Get_Mat_Vector(const class RXSitePt q);
	char *text;
	int Use_Me_As_Ref;
	int Use_Black_Space;
	struct MATERIAL_FIELD matstruct;
	RXEntity_p matptr;
};	 //class RXLayer



