#include "StdAfx.h"
#include<QDebug>
#include "RXGraphicCalls.h"

#ifdef _OPENMP
#include <omp.h>
#endif

#include "RXEntityDefault.h"
#include "RXSeamcurve.h"

#include "RXSail.h"
#include "RXSRelSite.h"
#include "RXLayer.h"
#include "RXOffset.h"
#include "RXPside.h"
#include "RXRelationDefs.h"

#include "uselect.h"

// a few headers needed by materials
#include "global_declarations.h"
#include "etypes.h"
#include "fielddef.h"
#include "hoopcall.h"
#include "RX_SimpleTri.h"
#include "RXTriangle.h"
// end extras for materials
#include "stringutils.h"
#include "RX_FESite.h"
#include "RX_FEEdge.h"
#include "RX_FETri3.h"
#include "entities.h"
#include "panel.h"
#include "polyline.h"
#include "arclngth.h"
#include "peternew.h"
#include "freeze.h"
#include "RXSitePt.h"
#include "RXPside.h"
#include "griddefs.h"
#include "RXCurve.h"
#include "RXSeamcurve.h"

#include "RX_UI_types.h"
#include "printall.h"
#include "paneldata.h"
#include "peterfab.h"
#include "drawing.h"
#include "filament.h"
#include "curveintersection.h"
#include "RXPanel.h"

#ifdef DEBUG
int RXPPRINT =99;
#else
int RXPPRINT =0;
#endif

/*
  RXPanel  is a region of surface bounded by a closed list of psides.
  It keeps its Shewchuk data  structure and its red and black boundary lists
  so that it can find  coordinates in blackspace or in its own redspace.

  Note that there are several ways of dealing with a mould.
  For instance, the redspace might be a developable surface bounded by the
  panel boundary, or it might be a patch of the mould, or it might be in a
  different world altogether like a cutting table.

  In the 2008 implementation, we only distinguish between the cases 'mould' and 'no-mould'.
  In the no-mould case, the redspace is taken to be a delaunay triangulation between
  the boundaries. Delaunay is lousy for interpolation so if the red-space isn't
  somewhere in the X-Y plane we get a very unfair surface.
  In practice this means  that red-space must be an X-Y cutting table.

  In the 'mould' case, we project the boundaries onto the X-Y plane for the delaunay triangulation
  and afterwards project the points back to the mould .  This is slow and inaccurate because
  both the projection to the mould and the finding of (u,v) coordinates are iterative processes.
  VERY IMPORTANT. This assumes that the panel's red-space is in the same coordinate system as
  its blackspace.  IE the model is of a surface on a mould, with some shaped seams introducing
  additional gaussian curvature.
  ALSO VIP.  The mould must have a unique and non-degenerate projection to the XY plane.

Clearly more generality is needed. 
 For instance
  * we might triangulate in (UV) space using geometry tests in moulded space.
  * each panel might have its own red-space surface, other than XY

The way to deal with this is going to be to sub-class a RXPanel, overriding the Shewchuk calls. 


*/


RXPanel::RXPanel	() /// this constructor is used in gauss.cpp to make a zone
    : m_m(NULL)
    , m_b(NULL)
{	assert("dont use this constructor"==0);
    Init();
}
RXPanel::RXPanel(SAIL *s) // this constructor used in TPN and Paneller

    : RXEntity(s)
    , m_m(NULL)
    , m_b(NULL)
    , m_t(0)
    , m_PanelData(0)
    , Geometry_Has_Changed(0)
    , Needs_ReGridding(1)
    , NeedsMaterials(0)
    , m_psidecount(0)
    , m_IsInBlackSpace(0)
    , m_HasRedspace(0)
    , reversed(0)
    , m_pslist(0)
    , allocsize(0)

{
    RXPanel::Init();
}

int RXPanel::Fillin(const int sidecount) // this was the constructor used in TPN and Paneller
{
    m_psidecount=sidecount;
    reversed=(int*)CALLOC(sidecount,sizeof(int));
    m_pslist= (PSIDEPTR *)CALLOC(sidecount,sizeof(PSIDEPTR));
    Needs_ReGridding = 1;
    return 1;
}
void RXPanel::AddTri3(RX_FETri3* t)
{
    m_FEtris.push_back (t);
    t->m_panel=this;
}
RXPanel::~RXPanel() 
{									
    this->CClear ();
}
int  RXPanel::CClear() // cclear just pushes the nodes onto tgarbage-patch. We must delete them before referring to fnodes again.
{
    int rc=0;
    // clear stuff belonging to this object
    if(m_t)
        trifreeIOstruct( *m_t );
    m_t=0;
    if(m_m&&m_b)
        triangledeinit(m_m, m_b);
    if(m_m)
        delete m_m;
    if(m_b)
        delete m_b;
    m_m=0; m_b=0;
    if(!Needs_ReGridding)
        Needs_ReGridding = 1; // this is strange but you never know if its needed


    for(int i=0;m_pslist&& (i< m_psidecount);i++) {
        PSIDEPTR pss = m_pslist[i];
        for(int m=0;m<2;m++) {
            if(pss->leftpanel[m]== this) pss->leftpanel[m]=NULL;
        }
    }
    if(m_pslist){
        RXFREE(m_pslist); m_pslist=NULL;
        this->Esail->panel_count--; // not definitive but with usual usage it works out
        m_psidecount=0;
    }

    if(reversed){ RXFREE(reversed); reversed=NULL; }

    if(m_PanelData)
        if(m_PanelData ->owner==this) // VG complains
            RXFREE( m_PanelData );

    m_PanelData=0;
    PCP_UnFreeze(this->Esail); 	// make sure we don't overwrite
    if(Esail->m_StressResults)
        RXFREE(Esail->m_StressResults);
    Esail->m_StressResults=NULL;

    for(size_t i=0;i<m_FEtris.size();i++){
        RX_FETri3*ft = m_FEtris[i];
        delete ft;
    }
    m_FEtris.clear();

    for(size_t i=0;i<m_FEedges.size();i++){
        RX_FEedge*ft = m_FEedges[i];
        delete ft;
    }
    m_FEedges.clear();
    redpts.clear();
    blackpts.clear();
    // call base class CClear();
    rc+=RXEntity::CClear ();
    return rc;
}
int RXPanel::Init(){
    int rc=0;
    m_psidecount = 0;
    reversed = 0;
    m_pslist = 0;
    allocsize = 0;	  /* for the two above */

    m_PanelData = 0;

    m_IsInBlackSpace = 0;
    m_HasRedspace=0;
    Needs_ReGridding = 1;
    this->NeedsMaterials=0;
    Geometry_Has_Changed = 0;
    m_t=0; // the Shewchuk TriangleIO structure
    m_m=0;
    m_b=0;
    return rc;
}

int RXPanel::Compute(void){
    this->Needs_ReGridding=1;
    return 0;}
int RXPanel::Resolve(void){assert(0); return 0;}
int RXPanel::Finish(void){assert(0); return 0;}
int RXPanel::ReWriteLine(void){  return 0;}




// this is modified from the Shewchuk original.

int triunsuitable(vertex triorg, vertex tridest, vertex triapex, REAL area, struct behavior *b)
//vertex triorg;                              /* The triangle's origin vertex. */
//vertex tridest;                        /* The triangle's destination vertex. */
//vertex triapex;                               /* The triangle's apex vertex. */
//REAL area;                                      /* The area of the triangle. */


{
    REAL dxoa, dxda, dxod;
    REAL dyoa, dyda, dyod;
    REAL oalen, dalen, odlen;
    REAL maxlen;

    // peters decs
    REAL cx,cy;
    REAL sx, sy;
    REAL px ,py, fac,grid;

    dxoa = triorg[0] - triapex[0];
    dyoa = triorg[1] - triapex[1];
    dxda = tridest[0] - triapex[0];
    dyda = tridest[1] - triapex[1];
    dxod = triorg[0] - tridest[0];
    dyod = triorg[1] - tridest[1];
    /* Find the squares of the lengths of the triangle's three edges. */
    oalen = dxoa * dxoa + dyoa * dyoa;
    dalen = dxda * dxda + dyda * dyda;
    odlen = dxod * dxod + dyod * dyod;
    /* Find the square of the length of the longest edge. */
    maxlen = (dalen > oalen) ? dalen : oalen;
    maxlen = (odlen > maxlen) ? odlen : maxlen;

    // peter get the centroid. this is in cartesian space,
    //   May be in Model or Panel coords.
    //   g_PanelInPlane    zero = Model coords.
    //   g_PanelInPlane Nonzero = Panel coords.

    cx = ( triorg[0] +  tridest[0] + triapex[0])/3.;
    cy =  ( tridest[1] + triapex[1] + triorg[1] )/3.;

    class RXPanel *t =(class RXPanel * ) b->userdata;
    class RXSail *s = t->Esail ;
    assert(0); cout<<cy<<cx;
    // now convert the centroid into model coords is necessary.

    class RXSitePt pp;

    // now find the acceptable gridsize (grid) at this position
    grid = s->MeshSize(pp);
    grid =  grid*grid;

    if(maxlen > grid) return 1;
    return 0;

} //triunsuitable



int RXPanel::Mesh(){
    int rc=1;
    /*
 For each panel {
  find out if redspace and blackspace are different (rbDiff)
  extract red-space boundary.
  construct a ShewchukTriangle input object
  constrained Delaunay.
  (dbg) Draw in redspace
  if(rbDiff)  map internal pts to blackspace. (dbg) draw in blackspace
  Create RXSiteRelSites from the internal points . Get their UV coordinates
  Create the FEEdges and FETriangles.  Set materials on the triangles (in the right space)
 }
 for timing purposes it would be good to separate out the xyz->uv stage and the build materials stage.

*/
    vector<int> edgeNos;
    if(!this->Needs_ReGridding) return 0;
    if(!this->m_psidecount) // usually a NAS pshell, but it may  be mal-formed panel.
    { 	this->NeedsMaterials=1 ;return 0;}

    if (RXPPRINT>0) printf("  RXPanel::Mesh  %s\n" , this->name () );

    if (g_PanelInPlane ) {
        m_tr =this->Trigraph();
        m_tinv=m_tr.Inverse();
    }
    else{
        m_tr.Identity(); m_tinv.Identity ();
    }

    if(this->m_t){
        if(! this->m_HasRedspace)
        {
            this->Needs_ReGridding=0;
            //   QString qbuf("Skip remesh of panel `"); qbuf+= this->GetQName() ; qbuf+=("` (no redspace)");
            //   rxerror(qbuf,1);
            return 0;
        }
        // if it has redspace we do something like:
        //       1  re-interpolate the red-space coords of the internal nodes from the Shewchuk user-data
        //       2  set the internal nodes to that place without changing their deflections.
        //       3  set the lengths of all m_FEedges from these red-space positions

        this->blackpts = GetBoundaryPoints(RX_BLACKSPACE);
        this->redpts   = GetBoundaryPoints(RX_REDSPACE);
        this->ReMakePtsEdgesTris();
        this-> RegenerateIntPts(RX_REDSPACE );
        this->Needs_ReGridding=0;
        return 1;
    }// if remeshing

    m_HasRedspace = 1;
    if (!this->Esail->GetFlag(FL_ISTPN)) // horrible work-around for prehistoric TPN style
        m_HasRedspace = !(Edges_Are_All_Curves());

    blackpts = GetBoundaryPoints(RX_BLACKSPACE);
    edgeNos = GetEdgeNumbers();
    int k=0;
    if (RXPPRINT>2){
        cout<< " blackspace \n"<<endl;
        HC_Open_Segment("black"); HC_Set_Color("lines=black"); HC_Restart_Ink();
        for(vector<RXSitePt>::iterator it= blackpts.begin(); it!=blackpts.end(); ++it)  {
            if (RXPPRINT>2)printf("%d   %f %f %f ( %8.8g %8.8g )\n", k++, (*it).x,it->y,it->z,it->m_u,it->m_v );
            HC_Insert_Ink( it->x,it->y,it->z ); // a dead call
        }
        HC_Insert_Ink( blackpts.begin()->x,blackpts.begin()->y,blackpts.begin()->z );
        HC_Close_Segment();
    }
    if(m_t)
        trifreeIOstruct(*m_t );
    else
        m_t = new struct triangulateio; // exists but contains garbage while we dont have a constructor

    if( m_HasRedspace) {
        if (RXPPRINT>0) cout<< "starting redspace mesh  "<< endl;

        redpts = GetBoundaryPoints(RX_REDSPACE); // evaluation of the curves on the edges of the panel

        Triangulate(redpts,edgeNos,*m_t,PLEASEMAKEREGEN);
        // now the point markers are set to zero for the interior pts
        // and hold their values for the bdy pts.
        //the  edges have marker 1 or 0 (ShewchukTriangle seems to use this)

        // we can make the edges, interior points and triangles from t
        //  the triangles don't know their bounding edges so we have to do that/
        // we then regenerate the interior points to set their coords.

        if (RXPPRINT>1) report(stdout,m_t, 1, 1, 1, 1, 1, 1);
        MakePtsEdgesTris();
        /// RegenerateIntPts sets the internal points to black.
        RegenerateIntPts(RX_BLACKSPACE );// RX_REDSPACE,RX_BLACKSPACE
        // oct 2012 its been black for a while - try red
        if (RXPPRINT>6)DrawTriangulation(m_t  );

        m_t->regionlist=0; // it was just a copy

    } // if( HasRedspace)
    else { // just mesh in black

        if(!Triangulate(blackpts,edgeNos,*m_t))
        {cout<<"Triangulation failed"<<endl;  cout.flush();}

        try{
            MakePtsEdgesTris();
        }//end try
        catch (RXException e) {
            e.Print(stdout);
            rc=0;
        }
        catch( char * str ) {
            printf( "Exception raised:  <%s>\n",str);
            //WriteAsPoly(&in);
            rc=0;
        }
        catch( unsigned int ex ) {
            printf( "Exception raised: no= %u\n",ex);
            //WriteAsPoly(&in);
            rc=0;
        }
#ifdef MICROSOFT
        catch(CException *ex) {
            wchar_t buf[1024];
            ex->GetErrorMessage(buf,1024);
            cout<< buf << endl;
            cout<< "(MakePtsEdgesTris) Cexception " ;
            //WriteAsPoly(&in);
            rc=0;
        }
#endif
        catch(...) { // usually get here
            cout<< "(MakePtsEdgesTris) otherexception "<<endl;
            //WriteAsPoly(&in);
            rc=0;
        }
        m_t->regionlist=0; // it was just a copy
    } // no HasRedspace)
    if (RXPPRINT>0) cout<< " done this RXPanel::Mesh"<<endl;
    this->Needs_ReGridding=0; 	this->NeedsMaterials=1;
    return rc;
}
int RXPanel::ReMakePtsEdgesTris( ){// actually just the edges
    int i, rcc=0;

//    TriPt p;
    int  N1,N2 ;
    class RX_FEedge *fe;

    for (i = 0; i < m_t->numberofedges; i++) {
        N1=m_t->edgelist[i * 2  ]; N2 = m_t->edgelist[i * 2 + 1];
        fe = this->Esail->m_Fedges[   m_t->edgemarkerlist[i] ];
        assert(m_t->edgemarkerlist[i]==fe->GetN() ) ;
        RXSitePt p1 = this->RedPosition(N1);
        RXSitePt p2 = this->RedPosition(N2);
        double oldL=fe->GetLength();
        fe->SetLength( p1.DistanceTo (p2)   );
        double newL=fe->GetLength();
        if(fabs(oldL-newL)>1e-7)  {
            //  qDebug()<<"(ReMakePtsEdgesTris) edge "<<fe->GetN()<< " was "<<oldL<<" is "<<newL<<endl;
            rcc++;
        }
        // else cout<<"edge "<<fe->GetN()<< " unchanged d= " <<newL-oldL <<endl;

    } // edges
    return rcc;
}
int RXPanel::MakePtsEdgesTris( ){
    int rcc=0;
    struct triangulateio *io = m_t;
    // create a vector of TriPoints, one for each Point
    //1 create a new RelSite for all points without a Marker (zero).
    /*
 BUT Note shewchuk's notes on boundary markers.

   The boundary marker associated with each vertex in an output .node file
  is chosen as follows:
    - If a vertex is assigned a nonzero boundary marker in the input file,
      then it is assigned the same marker in the output .node file.
    - Otherwise, if the vertex lies on a PSLG segment (even if it is an
      endpoint of the segment) with a nonzero boundary marker, then the
      vertex is assigned the same marker.  If the vertex lies on several
      such segments, one of the markers is chosen arbitrarily.
    - Otherwise, if the vertex occurs on a boundary of the triangulation,
      then the vertex is assigned the marker one (1).
    - Otherwise, the vertex is assigned the marker zero (0).

 */
    // we deal with the mould by projecting the Tri pt along Z
    // Later, it would be neater to triangulate in UV space using
    // a 3D criterion for the triangles.
    //2 ) For each Edge
    //		if  marker Zero create an analysis edge and set its length.
    //
    // push its no onto each of its nodes' Emaps
    //3) For each Tri
    //		recover its edge numbers from the emaps of its nodes.
    //		Create an analysis tri.
    int i,j,N1,N2,j1,j2,eno;
    vector<TriPt> pts; // automatic
    TriPt p;
    RX_FESite *p1,*p2;
    class RX_FEedge *fe;
    class RX_FETri3 * t;
    int * n13;
    int e[3];
    // a little aside: If we did the meshing in the panel's average plane we have to map back to global

    if (g_PanelInPlane ) {
        m_tr =this->Trigraph();
        m_tinv=m_tr.Inverse();
    }
    else{
        m_tr.Identity(); m_tinv.Identity ();
    }
    int nbdypts = max(this->redpts.size() ,  this->blackpts .size()) ;

    // create an array of TriPts from the Triangle results, pulling to mould if necessary
    for (i = 0; i < io->numberofpoints; i++) {
        p.m_Sptr =0;
        p.m_u =-99;// icpc V10 doesnt like the ON2dpt;
        p.m_v =-99;// ON_UNSET_VALUE;
        p.m_marker=io->pointmarkerlist[i];

        if(i < nbdypts){
            p.m_Sptr = this->Esail->GetSiteByNumber( p.m_marker );
            p.m_Sptr->m_Site_Flags |= PCF_ISFIELDNODE;
            if(this->m_HasRedspace)
            { p.x = this->redpts[i].x ;p.y = this->redpts[i].y ;p.z = this->redpts[i].z; p.m_u= this->redpts[i].m_u;p.m_v= this->redpts[i].m_v;}
            else
            { p.x = this->blackpts[i].x ;p.y = this->blackpts[i].y ;p.z = this->blackpts[i].z; p.m_u= this->blackpts[i].m_u;p.m_v= this->blackpts[i].m_v;}
        }
        else	{  // its a field point IE a new one made by Triangle.
            p.m_Sptr =0;
            p.m_marker=0;
            // aa
            if (!g_PanelInPlane ) {
                p.x = io->pointlist[i * 2]; p.y= io->pointlist[i * 2 + 1]; p.z=0;
                if(this->Esail ->GetMould() )
                    p.PullToMould ( this->Esail ->GetMould() ,0 ); //
            }
//bb
            else {
                p.x = io->pointlist[i * 2]; p.y= io->pointlist[i * 2 + 1]; p.z=0;
                if(this->Esail ->GetMould() )
                    p.PullToMould ( this->Esail ->GetMould() ,&m_tr ); //project to the surface along tr.Z
                ON_3dPoint qqq = this->m_tinv * p;
                p.x=qqq.x; p.y=qqq.y; p.z=qqq.z;
            }
//cc
            p.m_e.clear ();
            if(!p.m_marker) {
                RXSRelSite * pp = new RXSRelSite(this->Esail);
                pp->SetOName(TOSTRING("IntPt_Pan_")+TOSTRING(i));
                this->SetRelationOf(pp,spawn,RXO_SITE_OF_PANEL ,i);
                pp->AttributeAdd (this->AttributeString() );   // to transmit things like fixings.
                pp-> RXSitePt::Set(p);
                pp->Set(p);
                pp->m_Site_Flags |= PCF_ISFIELDNODE;
                pp->m_u= p.m_u; pp->m_v = p.m_v;
                io->pointmarkerlist[i] =p.m_marker=pp->GetN(); p.m_Sptr =pp;
            }
            else
            {
                RX_FESite* pp =this->Esail->GetSiteByNumber(p.m_marker);
                ON_3dVector  dx = *pp-p;
                if(dx.Length()>0.00001)
                    qDebug()<<"(RXPanel MakePtsEdgesTris) Moved a marker pt"<<p.m_marker<<dx.x<<dx.y<<dx.z;
                pp-> RXSitePt::Set(p);
                pp->Set(p);
                // shouldnt we just set p
            }
        } // its a field point
        pts.push_back(p);
    } //for i ... pts
    // here all the PTS are correctly placed on the mould
    m_FEedges.clear();
    for (i = 0; i < io->numberofedges; i++) {
        N1=io->edgelist[i * 2  ]; N2 = io->edgelist[i * 2 + 1];
        pts[N1].m_e[N2]=i;  pts[N2].m_e[N1]=i;
        if (! io->edgemarkerlist[i]){ 			// insert an FEedge
            fe = new RX_FEedge(this->Esail);

            fe->SetLength( pts[N1].DistanceTo (pts[N2])   ); // need redspace to set length
            N1=pts[N1].m_marker; N2=pts[N2].m_marker;
            p1 = this->Esail->GetSiteByNumber(N1);
            p2 = this->Esail->GetSiteByNumber(N2);

            fe->SetNodes(p1,p2);
            m_FEedges.push_back(fe);

            io->edgemarkerlist[i]=fe->GetN();
        }
    } // edges
    m_FEtris.clear();
    for (i = 0; i < io->numberoftriangles; i++) {
        if(RXPPRINT>3) printf("Generate FE Triangle %4d points:", i);

        t = new RX_FETri3(this->Esail);
        t->m_panel =this;

        m_FEtris.push_back (t);
        n13 = &(io->trianglelist[i * io->numberofcorners]);
        for (j = 0; j < io->numberofcorners; j++) {
            if(RXPPRINT>3) printf("  %4d", n13[ j]);
            p1 = this->Esail->GetSiteByNumber  (pts[n13[ j]].m_marker);
            t->SetNode(j,p1);
            j2 = j+1; if(j2==3) j2=0;
            j1 = n13[j]; j2 =n13[j2];
            eno = pts[j1].m_e[j2];
            eno = io->edgemarkerlist[eno];
            t->m_iEd[j]=  e[j] = eno;

        }
        if(RXPPRINT>3)  printf("\t edges (model numbering) are  %d %d %d\n", e[0],e[1],e[2]);
    }  // tris

    return rcc;
}
int RXPanel::RegenerateIntPts(const RX_COORDSPACE space){ // just a set-to-black of the internal points
    int rc=0;

    /*
the easy way is for the panel to keep its struct triangulateio,
this is expensive because it has a square array, dimension (N bndy pts)
A better way would be to use a sparse array structure.
We keep the influence coeffs (as well as the two coord sets) because then it is simpler
to re-build a panel after a red-space curve change .
The topology is preserved. This gives a fast analysis restart after changing the shape.
 */
    int i;
    SAIL *sail = this->Esail ;
    if(space==RX_BLACKSPACE ) {//RX_REDSPACE,


        if (m_t->numberofpointattributes <= 0)  {
            cout<< "no need to regenerate. this panel has no redspace"<<endl; return 0;
        }
        if(this->Esail->GetMould() ){
            cout<< " experimental regenerate on moulded"<<endl;
        }

        for (i = m_t->numberofpointattributes; i < m_t->numberofpoints; i++) {
            int nodeno = m_t->pointmarkerlist[i];
#ifdef BEFOREJUNE2012
            RXSitePt*s = sail->GetSiteByNumber(nodeno);
            RXSitePt q=this->BlackPos (i);
            s-> Set(ON_3dPoint(q));
#else
            RX_FESite*s = sail->GetSiteByNumber(nodeno);
            RXSitePt q=this->BlackPos (i);
            s->RXSitePt::Set(ON_3dPoint(q));
            s->RX_FESite::Set(ON_3dPoint(q));

#endif
            rc++;
        }
    }
    else if(space== RX_REDSPACE ) {
        for (i = m_t->numberofpointattributes; i < m_t->numberofpoints; i++) {
            int nodeno = m_t->pointmarkerlist[i];
            RX_FESite*s = sail->GetSiteByNumber(nodeno);
            RXSitePt q=this->RedPosition (i);
            s->RXSitePt::Set(ON_3dPoint(q));
            s->RX_FESite::Set(ON_3dPoint(q)); //  leaves deflections unchanged
            rc++;
        }
    }
    else assert(0);
    return rc;
}
RXSitePt RXPanel::BlackPos(const int i) const{
    RXSitePt p, q;
    q.Set(ON_3dPoint(0,0,0)); q.m_u=0; q.m_v=0;
    if( this->m_HasRedspace ) {
        for (int j = 0; j < m_t->numberofpointattributes; j++) {
            double f = m_t->pointattributelist[i * m_t->numberofpointattributes + j];
            p= static_cast<RXSitePt>(this->blackpts[j]);
            q = q +  p  *f;
        }
        {
            if(this->Esail ->GetMould() ) {
                int err;
                this->Esail->CoordModelXToUV(&q,&err);
            }
        }
        return q;
    } // if has redspace

    // no redspace so we simply extract the pt from m_t;
    q.x = m_t->pointlist[i * 2]; q.y= m_t->pointlist[i * 2 + 1]; q.z=0;

    {
        if(this->Esail ->GetMould() ) {
            int err;
            //q.PullToMould ( owner->Esail ->mould  );
            this->Esail->CoordModelXToUV(&q,&err);
        }
    }
    return q;
}
RXSitePt RXPanel::RedPosition(const int i) const{
    assert(this->m_HasRedspace );
    RXSitePt q ;
    q.Set(ON_3dPoint(0,0,0)); q.m_u=0; q.m_v=0;
    for (int j = 0; j < m_t->numberofpointattributes; j++) {
        double f = m_t->pointattributelist[i * m_t->numberofpointattributes + j];
        q = q +  static_cast<RXSitePt>(this->redpts[j] )*f;
    }
    //#pragma omp critical(accessmould)
    {
        if(this->Esail ->GetMould() ) {
            int err;
            this->Esail->CoordModelXToUV(&q,&err);
        }
    }
    return q;
}
// WARNING. this is only safe if called AFTER the panel is meshed.
// if space is RX_GLOBAL we use deflected coords, mapped to modelspace
class RX_SimpleTri RXPanel::BlackTri(const int i,const RX_COORDSPACE space)
{
    class RX_SimpleTri rc;
    int j;
    RXSitePt sp;
    assert( m_t->numberofcorners ==3);
    // lets just copy the FETRI
    class RX_FETri3* fet = this->m_FEtris[i];
    for(j=0;j<3;j++) {
        int nn = fet->Node(j);
        RX_FESite *s1 = this->Esail->GetSiteByNumber(nn);
        sp = *s1;
        if(space ==RX_GLOBAL )
            sp = s1->DeflectedPos(RX_MODELSPACE) ; //  gets U&V from above
        rc.SetPt(j,sp );
    }
    return rc;
}
class RX_SimpleTri RXPanel::RedTri(const int i) 
{
    class RX_SimpleTri rc;
    int j,n;

    for (j = 0; j < m_t->numberofcorners; j++) {
        n=m_t->trianglelist[i * m_t->numberofcorners + j] ;
        rc.SetPt(j,RedPosition(n));
    }
    return rc;
}
// enos is only used to populate the segment marker list.
int RXPanel::Triangulate(vector<RXSitePt> inpts, vector<int> enos, struct triangulateio &mid, const int flag){

    int rc=1;
    struct triangulateio in;
    ON_Xform l_axes = m_tr;
    ON_3dPoint xl;
    RXTRI_REAL *lp;
    int i, *ilp,*ime;
    assert(sizeof(RXTRI_REAL)==sizeof(double));

    if(!m_m) m_m = new struct mesh;
    if(!m_b) m_b = new struct behavior;  m_b->verbose =0; m_b->userdata=this;

    RXSitePt centroid; centroid.Set(ON_3dPoint(0.,0.,0.));
    assert(inpts.size()==enos.size());
    /* Define input points. */
    memset(&in,0,sizeof(struct triangulateio  ));
    memset(&mid,0,sizeof(struct triangulateio  ));
    in.numberofpoints = inpts.size();
    if(flag&PLEASEMAKEREGEN)
        in.numberofpointattributes = in.numberofpoints;
    else
        in.numberofpointattributes = 1;
    in.pointlist = (RXTRI_REAL *) malloc(in.numberofpoints * 2 * sizeof(REAL));

    lp=in.pointlist;

    for(vector<RXSitePt>::iterator it= inpts.begin(); it!=inpts.end(); ++it)  {
        if(g_PanelInPlane) { // turned on by "MeshInPanelPlane: <non-zero>"
            xl = l_axes * (*it);
            *lp = xl.x; lp++; *lp = xl.y; lp++;
        }
        else {
            *lp = it->x; lp++; *lp = it->y; lp++;
        }
        centroid += (*it);
    }
    centroid /= ((double) ( inpts.size() ) );

    // generate the segments: we assume that the Points form a closed cycle. That's silly
    ilp = in.segmentlist = (int*) malloc(in.numberofpoints*2* sizeof(int));
    ime = in.segmentmarkerlist = (int*) malloc(in.numberofpoints * sizeof(int));
    for(i=0;i<in.numberofpoints;i++) {
        *ilp=i; ilp++; *ilp=i+1; ilp++;
        *ime = enos[i]; ime++;
    }
    ilp--; *ilp=0; // close the cycle
    in.numberofsegments=in.numberofpoints;



    in.pointattributelist = (RXTRI_REAL *) calloc(in.numberofpoints *
                                                  in.numberofpointattributes ,
                                                  sizeof(REAL));
    if(flag&PLEASEMAKEREGEN){ // the attributes are the identity matrix
        lp = in.pointattributelist;
        for(vector<RXSitePt>::iterator it= inpts.begin(); it!=inpts.end(); ++it)  {
            *lp = 1.0; lp+=(in.numberofpoints +1);
        }
    }
    else { //just attribute of 1 to mark the boundary
        lp = in.pointattributelist;
        for(vector<RXSitePt>::iterator it= inpts.begin(); it!=inpts.end(); ++it)  {
            *lp = 1.0; lp++;
        }

    }// no regenpts



    in.pointmarkerlist = (int *) calloc(in.numberofpoints , sizeof(int));
    ilp = in.pointmarkerlist;

    for(vector<RXSitePt>::iterator it= inpts.begin(); it!=inpts.end(); ++it)  {
        *ilp =  it->RXSite_N();  assert ( RXSITEINITIAL_N != *ilp );
        assert ( RXOBJ_INITIAL_N != *ilp );
        ilp++; // Site_N or GetN?();
    }

    in.numberofholes = 0;
    if(0) { // this was copied from the distro 'tricall.c'
        in.numberofregions = 2;
        in.regionlist = (REAL *) malloc(in.numberofregions * 4 * sizeof(REAL));
        in.regionlist[0] = 10.;
        in.regionlist[1] = 1.0;
        in.regionlist[2] = 7.0;            /* Regional attribute (for whole mesh). */
        in.regionlist[3] = 0.1;          /* Area constraint that will not be used. */
        in.regionlist[4] = 12.0;
        in.regionlist[5] = 1.0;
        in.regionlist[6] = 7.0;            /* Regional attribute (for whole mesh). */
        in.regionlist[7] = 0.01;          /* Area constraint that will not be used. */
    }
    else {
        in.numberofregions = 0;
        in.regionlist =0;
    }
    if(RXPPRINT>1){
        cout<< "Input point set:\n\n"<<endl;
        report(stdout,&in, 1, 0, 1, 1, 1, 1);
    }
    /* Make necessary initializations so that ShewchukTriangle can return a */
    /*   triangulation in `mid' and a voronoi diagram in `vorout'.  */

    mid.pointlist = (REAL *) NULL;            /* Not needed if -N switch used. */
    /* Not needed if -N switch used or number of point attributes is zero: */
    mid.pointattributelist = (REAL *) NULL;
    mid.pointmarkerlist = (int *) NULL; /* Not needed if -N or -B switch used. */
    mid.trianglelist = (int *) NULL;          /* Not needed if -E switch used. */
    /* Not needed if -E switch used or number of triangle attributes is zero: */
    mid.triangleattributelist = (REAL *) NULL;
    mid.neighborlist = (int *) NULL;         /* Needed only if -n switch used. */
    /* Needed only if segments are output (-p or -c) and -P not used: */
    mid.segmentlist = (int *) NULL;
    /* Needed only if segments are output (-p or -c) and -P and -B not used: */
    mid.segmentmarkerlist = (int *) NULL;
    mid.edgelist = (int *) NULL;             /* Needed only if -e switch used. */
    mid.edgemarkerlist = (int *) NULL;   /* Needed if -e used and -B not used. */

    /* Triangulate the points.  Switches are chosen to read and write a  */
    /*   PSLG (p),  number everything from  */
    /*   zero (z), assign a regional attribute to each element (A), and  */
    /*   produce an edge list (e), a Voronoi diagram (v), and a triangle */
    /*   neighbor list (n).
  that gives <"pczAevn"  >*/

    double grid = this->MeshSize(centroid);

    ON_String t, sbuf=ON_String("penzY");;
    if(RXPPRINT>3) sbuf=ON_String("VpenzY");
    else if(RXPPRINT<2) sbuf=ON_String("QpenzY"); // the 'a' a test for a fine grid at (10,9)
    //  'u' turns on 'triunsuitable - that tests out fine.
    t.Format("a%f", (  grid*grid*0.433013));///1.21) );
    sbuf+=t;

    if(RXPPRINT>3)// to see if a vanilla Triangle app will mesh it.
        WriteAsPoly(&in);

    try{
        ///////////////////////////////////////////////////////////////////
        triangulate(sbuf.Array(), &in, &mid, NULL , m_m,m_b);
        ////////////////////////////////////////////////////////////////////
        // if it fails we could try -i
    }//end try
    catch (RXException e) {
        e.Print(stdout);
        WriteAsPoly(&in);rc=0;
    }
    catch( char * str ) {
        printf( "Exception raised:  <%s>\n",str);
        WriteAsPoly(&in);rc=0;
    }
    catch( unsigned int ex ) {
        printf( "Exception raised: no= %u\n",ex);
        WriteAsPoly(&in);rc=0;
    }
#ifdef MICROSOFT
    catch(CException *ex) {
        wchar_t buf[1024];
        ex->GetErrorMessage(buf,1024);
        cout<< buf << endl;
        cout<< "(Triangle) Cexception " ;
        WriteAsPoly(&in); rc=0;
    }
#endif
    catch(...) { // usually get here
        cout<< "(Triangle) otherexception (5) "<<this->name()<<endl;
        WriteAsPoly(&in); rc=0;
    }
    /* Free all allocated arrays, including those allocated by ShewchukTriangle. */

    free(in.pointlist);
    free(in.pointattributelist);
    free(in.pointmarkerlist);
    free(in.segmentmarkerlist);
    if(in.regionlist )free(in.regionlist);
    free(in.segmentlist );
    return rc;
}
// we'd like the meshsize
double RXPanel::MeshSize(const RXSitePt centroid) const
{
    double rv ;
    // we might get the (?geometric?) mean (or min or something) of the lengths of the edge segments
    //   and then take the geometric mean of that and the models meshsize function
    //       eg walk the psides m_eList
    rv = this->Esail->MeshSize(centroid);
    return rv;
}

void RXPanel::report(FILE *fp, struct triangulateio *io,
                     int markers,int reporttriangles,int reportneighbors,int reportsegments,
                     int reportedges, int reportnorms)
{
    int i, j;
    if (RXPPRINT<=0){ fprintf(fp," tri detail report turned off\n"); return;}

    for (i = 0; i < io->numberofpoints; i++) {
        fprintf(fp,"Point %4d:", i);
        for (j = 0; j < 2; j++) {
            fprintf(fp,"  %.6g", io->pointlist[i * 2 + j]);
        }
        if (io->numberofpointattributes > 0) {
            fprintf(fp,"   attributes");
        }
        for (j = 0; j < io->numberofpointattributes; j++) {
            fprintf(fp,"  %.6g",
                    io->pointattributelist[i * io->numberofpointattributes + j]);
        }
        if (markers && io->pointmarkerlist) {
            fprintf(fp,"   marker %d\n", io->pointmarkerlist[i]);
        } else {
            fprintf(fp,"\n");
        }
    }
    fprintf(fp,"\n");

    if (reporttriangles || reportneighbors) {
        for (i = 0; i < io->numberoftriangles; i++) {
            if (reporttriangles) {
                fprintf(fp,"Tri angle %4d points:", i);
                for (j = 0; j < io->numberofcorners; j++) {
                    fprintf(fp,"  %4d", io->trianglelist[i * io->numberofcorners + j]);
                }
                if (io->numberoftriangleattributes > 0) {
                    fprintf(fp,"   attributes");
                }
                for (j = 0; j < io->numberoftriangleattributes; j++) {
                    fprintf(fp,"  %.6g", io->triangleattributelist[i *
                            io->numberoftriangleattributes + j]);
                }
                fprintf(fp,"\n");
            }
            if (reportneighbors) {
                fprintf(fp,"Tri angle %4d neighbors:", i);
                for (j = 0; j < 3; j++) {
                    fprintf(fp,"  %4d", io->neighborlist[i * 3 + j]);
                }
                fprintf(fp,"\n");
            }
        }
        fprintf(fp,"\n");
    }

    if (reportsegments) {
        for (i = 0; i < io->numberofsegments; i++) {
            fprintf(fp,"Segment %4d points:", i);
            for (j = 0; j < 2; j++) {
                fprintf(fp,"  %4d", io->segmentlist[i * 2 + j]);
            }
            if (markers && io->segmentmarkerlist) {
                fprintf(fp,"   marker %d\n", io->segmentmarkerlist[i]);
            } else {
                fprintf(fp,"\n");
            }
        }
        fprintf(fp,"\n");
    }

    if (reportedges) {
        for (i = 0; i < io->numberofedges; i++) {
            fprintf(fp,"Edge %4d points:", i);
            for (j = 0; j < 2; j++) {
                fprintf(fp,"  %4d", io->edgelist[i * 2 + j]);
            }
            if (reportnorms && (io->edgelist[i * 2 + 1] == -1)) {
                for (j = 0; j < 2; j++) {
                    fprintf(fp,"  %.6g", io->normlist[i * 2 + j]);
                }
            }
            if (markers &&io->edgemarkerlist ) {
                fprintf(fp,"   marker %d\n", io->edgemarkerlist[i]);
            } else {
                fprintf(fp,"\n");
            }
        }
        fprintf(fp,"\n");
    }
} // report

void RXPanel::DrawTriangulation( struct triangulateio *io ) 
{
    int i, j,n;
    HC_Open_Segment("shewchuk tris"); HC_Set_Visibility("edges=on,faces=on");
    VECTOR poly[10];
    for (i = 0; i < io->numberoftriangles; i++) {

        for (j = 0; j < io->numberofcorners; j++) {
            n= io->trianglelist[i * io->numberofcorners + j];
            if(j>9) break;
            poly[j].x=(float)io->pointlist[n * 2 + 0];
            poly[j].y=(float)io->pointlist[n * 2 + 1];
            poly[j].z=(float)0.0 ;

        }
        Shrink_Poly(3,poly,0.9);
        HC_KInsert_Polygon(3,&(poly->x),this->GNode());

    } // for triangles
    HC_Close_Segment();


} // Draw


vector<int> RXPanel::GetEdgeNumbers(void) const {
    int  i;
    PSIDEPTR ps;
    vector<int> rc, l;

    for(i=0;i<this->m_psidecount; i++) {
        ps = this->m_pslist[i];
        if(this->reversed[i])
            l=ps->GetEdgeNumbers(true);
        else
            l=ps->GetEdgeNumbers(false);
        rc.insert(rc.end(),l.begin (),l.end ());
    } // for i
    return  rc;
}

// GetBoundaryPoints evaluates the curves on the edges of the panel and returns SitePt objects holding those
// coordinates.

vector<RXSitePt> RXPanel::GetBoundaryPoints(const RX_COORDSPACE space){
    // see global ResetCoords
    // theCurve is either BLACKCURVE (1)  or REDCURVE 0

    int i;
    const int rev[3] = {2,1,0};
    PSIDEPTR ps,  pso;
    vector<RXSitePt> rc, l;
    int WhichCurve=99;
    if(space==RX_BLACKSPACE) WhichCurve=1;
    else if(space==RX_REDSPACE) WhichCurve=0;
    else assert(0);
    int tempdbg=0;

    pso = this->m_pslist[this->m_psidecount-1];
    for(i=0;i<this->m_psidecount; i++) {
        ps = this->m_pslist[i];
        if(ps==pso) {
            wcout <<L"out and back will crash the mesher. on panel "
                 << this->GetOName().c_str()
                 << L" pside is "<<pso->GetOName()
                 <<endl;
            ON_3dPoint pt =  pso->Ep(0) ->ToONPoint();
            wcout <<L"somewhere near " << pt.x<<"," << pt.y<<","<< pt.z<<","<<endl;
        }

        if(this->reversed[i]) {
            l=ps->psGetCurveCoords(WhichCurve,true);
        }
        else {
            l=ps->psGetCurveCoords(rev[WhichCurve],false);  /* set up coords line */
        }
        l.pop_back(); //the last point duplicates the first of the next side
        if(tempdbg) {
            cout <<"Pside "<< ps->name() <<endl;
            vector<RXSitePt>::iterator it;
            for(it=l.begin (); it!= l.end();++it) {
                cout << it->x<< ","<< it->y<< ","<< it->z<< endl;
            }
        }
        vector<RXSitePt>::iterator it;
        for(it=l.begin (); it!= l.end();++it) {
            it->m_Site_Flags |= PCF_ISFIELDNODE;  // careful.   These are temporary pts, not the real ones.
        }
        rc.insert(rc.end(),l.begin (),l.end ());
        pso=ps;
    } // for i
    return  rc;
}// GetBoundaryPoints

// a better way takes advantage of the fact that Triangle doesnt require the segments to be ordered
// It does care if there are duplicate points or duplicate edges.


void RXPanel::PanelTriangleListsPrint(FILE *ff)
{



#ifndef NOTNOW
    cout<< " TODO  PanelTriangleListsPrint"<<endl;
#else
    int i;
    class RXSitePt **pt;
    Panelptr  pan =this;
    OldTriangle_t *tr;
    pt= pan->BasePts;
    if(pt) {
        for(i=0;i<pan->nBpts;i++)
        {
            fprintf(ff,"%S\n", (*pt)->TellMeAbout ().c_str());
            pt++;
        }

        fprintf(ff,"EndNode\n");
    }

    tr = pan->BaseTri;
    if(tr) {
        for(i=0;i<pan->nBtri;i++)
        {
            // TrianglePrint(ff,tr);
            tr++;
        }

        fprintf(ff,"EndTriangles\n");
    }

    pt=pan->BasePts;
    if(pt) {
        for(i=0;i<pan->nFpts;i++)
        {
            fprintf(ff,"%S\n", (*pt)->TellMeAbout ().c_str());
            pt++;
        }

        fprintf(ff,"EndNode\n");
    }

    tr=pan->FullTri;
    if(tr) {
        for(i=0;i<pan->nFtri;i++)
        {
            // TrianglePrint(ff,tr);
            tr++;
        }
        fprintf(ff,"EndTriangles\n");
    }
#endif
}

int RXPanel::Edges_Are_All_Curves(){ //see petern2.cpp l 585 
    // return 1 if no seam borders the panel.
    int i;
    PSIDEPTR ps;
    for(i=0;i<this->m_psidecount; i++) {
        ps = this->m_pslist[i];
        if(ps->sc->seam) return 0;
    }
    if(this->Esail->GetFlag(FL_ISTPN))
        return 0;
    return 1;
}


HC_KEY RXPanel::DrawHoops(	) {

    int j,k;
    double s1,s2,s;
    VECTOR v;

    int ns;
    int side;

    int TPN = (this->Esail->GetFlag(FL_ISTPN)) ;

    if(!this->hoopskey) {
        VECTOR *poly=NULL;
        int count=0;
        Panelptr  np =this;

        ns=this->m_psidecount;
        HC_Open_Segment(this->type());
        this->hoopskey =  HC_KOpen_Segment(this->name());
        HC_Set_User_Index(RXCLASS_ENTITY, this);
        HC_Flush_Contents(".","subsegments");
        set<RXObject*> mylist= this->FindInMap(RXO_LAYER, RXO_ANYINDEX);

        if(mylist.size()) {
            class RXLayer*layer = dynamic_cast<class RXLayer*> (*(mylist.begin()));
            RXEntity_p mat = layer->matptr;
#ifdef HOOPS
            HC_KEY fabkey=mat->hoopskey;
            if(fabkey){
                char type[64],seg[512],color[512]; // type is a shadow
                HC_Show_Key_Type(fabkey,type);
                if(strieq(type,"segment")) {
                    HC_Show_Segment(fabkey,seg);
                    if (HC_QShow_Existence(seg,"color")){

                        HC_QShow_Color(seg,color);
                        HC_Set_Color(color);
                    }
                }
                else rxerror(" fab key not of a segment",1);
            }
#endif
        }

        /* the red-space outline */
        if(!TPN) {
            HC_Open_Segment("redspace");
            for (j=0;j<ns;j++) {
                PSIDEPTR pp = m_pslist[j];
                RXEntity_p sco = pp->sc;
                sc_ptr sc = (sc_ptr  )sco;
                side = 2*(1- reversed[j]);
                RXCurve * theCurve = sc->m_pC[side];
                ON_3dPointArray pts;
                pts.Empty();
                if (0!=side) { /* going forwards*/
                    pts = theCurve->Divide(*pp->End1off,*pp->End2off,sc,side,5);
                }
                else {
                    pts = theCurve->Divide(*pp->End2off,*pp->End1off,sc,side,5);
                }
                for(k=0;k<6;k++) {
                    ON_3dPoint *ppp = pts.At(k);
                    v.x=(float)ppp->x; v.y=(float)ppp->y; v.z=(float)ppp->z;
                    Append_To_Poly(&poly,&count,&v,1);
                }
                pts.Empty();
            }
            if(poly) {
                Shrink_Poly(count,poly,0.90);

                if(!Draw_Panel_Materials(count,poly)) { // bad fabric
                    if(poly) RXFREE(poly);
                    return 0;
                }

                HC_Set_Color("edges=red");
                PCP_Poly_Planar(count,poly);
                HC_KInsert_Polygon(count,&(poly->x),this->GNode());


                RXFREE(poly);
            }
            HC_Close_Segment();
        }
        else { /* a TPN */
            /* black space */
            HC_Open_Segment("blackspace");
            count=0;
            for (j=0;j<ns;j++) {
                PSIDEPTR pp = m_pslist[j];
                RXEntity_p sco = pp->sc;
                sc_ptr sc = (sc_ptr  )sco;
                //double refl;
                side = 2*(1- (*np).reversed[j]);
                //refl =
                sc->GetArc(1);
                if (0!=side) { /* going forwards*/

                    s1 = (pp->End1off->Evaluate(sc,1));
                    s2 = (pp->End2off->Evaluate(sc,1));
                }
                else {
                    s1 = (pp->End2off->Evaluate(sc,1));
                    s2 = (pp->End1off->Evaluate(sc,1));
                }
                for(k=0;k<5;k++) {
                    s = s1*(1.0-(float) k/5.0) + s2 * (float)k/5.0;

                    Get_Position_By_Double( sco,s,1,&v);
                    Append_To_Poly(&poly,&count,&v,1);
                }
            }
            if(poly) {
                Shrink_Poly(count,poly, 0.9);
                HC_KInsert_Polygon(count,&(poly->x),this->GNode());
                RXFREE(poly);
            }
            HC_Close_Segment();
        }
        HC_Close_Segment();
        HC_Close_Segment();
    }
    cout<<"RXPanel Draw no longer returns 1"<<endl;
    return 0;
}

int RXPanel::List(FILE *fp)  {
    int k,side;
    VECTOR v,vo,vs;
    //   Panelptr  this =  this;
    RXEntity_p o;
    sc_ptr sc;
    double err;
    char str[512];
    vo.x=vo.y=vo.z=999.;
    v.x=v.y=v.z=99999.;
    vs.x=vs.y=vs.z=9999.;
    fprintf(fp,"owning ent %s  %s \n",this->type(), this->name());

    set<RXObject*> z= this->FindInMap(RXO_LAYER,RXO_ANYINDEX);
    fprintf(fp,"    MList from Map \n");
    for(set<RXObject*>::iterator it=z.begin();it!=z.end();++it){
        class RXLayer* l= dynamic_cast<class RXLayer*>(*it);
        RXEntity_p se, me = l->matptr;
        struct MATERIAL_FIELD *ms =&(l->matstruct);
        struct LINKLIST *lll = ms->ll;
        fprintf(fp,"  Layer '%s' ",l->name());
        fprintf(fp,"\t mat '%s' ",me->name());

        while (lll) {
            se = (RXEntity_p )lll->data;
            if(se) fprintf(fp," \t SC=%s ",se->name());
            lll=lll->next;
        }

        fprintf(fp,"\n");

    }

    fprintf(fp,"    N_Sides    %d\n",this->m_psidecount);
    fprintf(fp,"    Geom_has_Changed %d\n",this->Geometry_Has_Changed);
    for (k=0;k<this->m_psidecount;k++) {
        PSIDEPTR pp = this->m_pslist[k];
        if(pp) {
            fprintf(fp,"    %2d  %25.25s    %d\t",k,pp->name() ,this->reversed[k]);

            o = pp->sc;
            side = 2*(1-(this->reversed[k]));
            sc = (sc_ptr  )o;
            if(sc->m_pC[side]->HasGeometry()){ //sc->c[side]
                if(!this->reversed[k]) {
                    Get_Position(pp,pp->End1off,side,&v);
                    fprintf(fp,"end1 %15S   side%1d    \t%f \t%f \t%f\n",pp->End1off->GetText().c_str(),side,
                            v.x,v.y,v.z);
                    if(!k) PC_Vector_Copy(v,&vs);//	mem cpy (&vs,&v,sizeof(VECTOR));
                    if (k && (err=distV(&v,&vo)) > this->Esail ->m_Linear_Tol ) {
                        fprintf(fp,"  ERROR IN PANELLING (1) %f\n", err );
                        printf("  ERROR IN PANELLING (1) %f\n", err );
#ifdef _DEBUG
                        sprintf(str,"ERROR IN PANELLING '%s' = %f",this->name() ,err);
                        rxerror(str,1);
#endif
                        HC_Open_Segment("");
                        HC_Set_Line_Weight(4.0);
                        HC_Set_Color("lines=magenta");
                        RX_Insert_Line(v.x,v.y,v.z,vo.x,vo.y,vo.z,this->GNode());
                        HC_Close_Segment();
                        PC_Insert_Arrow(&v,(float )45,"panel rxerror");
                    }
                    Get_Position(pp,pp->End2off,side,&v);
                    fprintf(fp,"                                \t");
                    fprintf(fp,"end2 %15S   side%1d    \t%f \t%f \t%f\n",pp->End2off->GetText().c_str(),side,
                            v.x,v.y,v.z);
                    PC_Vector_Copy(v,&vo); //mem cpy (&vo,&v,sizeof(VECTOR));
                }
                else
                {
                    Get_Position(pp,pp->End2off,side,&v);
                    fprintf(fp,"end2 %15S   side%1d    \t%f \t%f \t%f\n",pp->End2off->GetText().c_str(),side,
                            v.x,v.y,v.z);
                    if(!k)  PC_Vector_Copy(v,&vs);//	mem cpy (&vs,&v,sizeof(VECTOR));
                    if (k &&  (err=distV(&v,&vo)) > this->Esail->m_Linear_Tol) {
                        fprintf(fp,"  ERROR IN PANELLING (2) %f\n", err);
                        printf("  ERROR IN PANELLING  (2) %f\n", err);
                        HC_Open_Segment("");
                        HC_Set_Line_Weight(4.0);
                        HC_Set_Color("lines=magenta");
                        RX_Insert_Line(v.x,v.y,v.z,vo.x,vo.y,vo.z,this->GNode());
                        HC_Close_Segment();
                        sprintf(str,"Panelling rxerror (2) of %f on '%s'",err, this->name());
#ifdef _DEBUG
                        rxerror(str,2);
#endif
                        PC_Insert_Arrow(&v,(float )45,str);
                    }

                    Get_Position(pp,pp->End1off,side,&v);
                    fprintf(fp,"                                \t");
                    fprintf(fp,"end1 %15S   side%1d    \t%f \t%f \t%f\n",pp->End1off->GetText().c_str(),side,
                            v.x,v.y,v.z);
                    PC_Vector_Copy(v,&vo); //  mem cpy (&vo,&v,sizeof(VECTOR));
                }
            }  else fprintf(fp," C NULL in %s %s\n",this->type(),this->name());
        }
        else  fprintf(fp,"PP NULL");
    }
    if (m_psidecount && (err=distV(&v,&vs)) > 0.001) {
        fprintf(fp,"  ERROR IN PANELLING  AT END %f\n", err );
        sprintf(str,"  ERROR IN PANELLING  AT END %f\n", err);
        PC_Insert_Arrow(&v,(float )45,str );
    }

    Print_Panel_Triangles(fp);
    Print_Panel_DMatrices(fp);

    if(m_t && (	RXPPRINT>1)) report(fp,m_t, 1, 1, 1, 1, 1, 1);

    return 1;
}
int RXPanel::ReBuildTriangles(const RX_COORDSPACE space){
    int rc=1;
    unsigned int i;
    FortranTriangle *te;
    SAIL *sail = this->Esail;
    for(i=0;i<this->m_FEtris.size() ;i++) {
        te = this->m_FEtris[i]; if(!te) continue;
        //   te->area = te->Area(); // this is based on blackspace - I think its wrong
        double checkangle = BuildOneTriMaterial(i,space);
        //  if(fabs(checkangle-te->RefAngle()) > ON_EPSILON)  print a warning

        te->SetProperties(); //DANGER - sets area to original
    }

    if(!(this->m_IsInBlackSpace)) {  /* panel in red space */
        cout<< " TODO ResetCoords(pan,BLACKCURVE);"<<endl;
        cout<< "TODO  RegeneratePanel(pan,mould); "<<endl;
    }
    return rc;
}


int RXPanel::BuildTriMaterials(){
    int rc=0;
    class RX_FETri3* te;
    int fils=1;
    if(AttributeFind("$nofilament" ))
        fils = 0;
#pragma omp critical (getfillist)
    {
        if(fils)
            this->GetFilamentList();
    }

    for(unsigned int i=0;i<m_FEtris.size();i++) {
        te = m_FEtris[i];

        te->m_TriArea = te->Area();   // this is based on blackspace - I think its wrong
        double checkangle = BuildOneTriMaterial(i,RX_MODELSPACE);
        if(fabs(checkangle-te->RefAngle()) > ON_SQRT_EPSILON ){
            cout<<"trielement angle check imprecision:  "<<checkangle <<" isnt "<<te->RefAngle()<<endl;
            cout <<"Probable cause : panel '"<<this->name()<<"' has no materials defined"<<endl;
        }
        assert( te->IsInDatabase()); // so we dont need the AddFtri3
        te->SetProperties();
    }

    return rc;
}

double RXPanel::BuildOneTriMaterial(const int index, const RX_COORDSPACE space) { //  UGLY but it works!
    /*
the ugly thing about this is that the element itslef is passed to the Mfunc. 
However most of the Mfuncs ignore it.
Compute_Filament_Field  reads its  area,  m_refangle and vertices via Tri_Vertices.
and  WRITEs the filaments to its linklist 'fils'

*/
    class RX_SimpleTri  l_t;//  a container for the nodal positions - which change according to the coordinate space.
    class RX_FETri3 *p_fte = m_FEtris[index ];  // the material properties are collected in it.

    set<RXObject*>::iterator it;
    set<RXObject*> lyrs= this->FindInMap(RXO_LAYER, RXO_ANYINDEX);

    class RXSitePt l_qq;
    MatValType mx[MXLENGTH];
    double this_mat_to_matref_angle,matref_to_base_angle, this_mat_to_Element_Base;

    ON_3dVector  matrefVector,vdash  ;
    MatDataType xv; xv.resize(20,0); //float xv[20]; // Our trigraph stuff is inaccurate. Use double precision ON_XForm
    RXEntity_p ep, matEnt;
    class RXLayer *fab, *ref_fab;

    struct PC_MATERIAL *ref_material, *a_material;
    double thick; /* thickness of current layer */
    int ref_count,mcount;  /* no of layers in the triagle */
    TriLamDefn *tld,*lam_list ;
    char buf[256], colourstring[256];
    char** l_ColourList;
    double*colour_thk;
    double prestress[3];
    struct PC_FIELD *l_field ;
    int  cc; /* colour_Count */
    void  *fieldOK, *refOK;
    this_mat_to_matref_angle=0;matref_to_base_angle=0;

    strcpy(colourstring,"white");

    p_fte->creaseable = 1; /* start off creasable */

    memset(p_fte->mx,0,sizeof(MatValType)*9); /* set the matrix attached to Fortran tri to 0's */
    memset(p_fte->prestress,0,sizeof(double)*3); /* set the matrix attached to Fortran tri to 0's */
    mcount = 0; cc=0;
    ref_count = 0; thick=0;
    fab=NULL;ref_fab=NULL;  ref_material=NULL;a_material=NULL;
    if(!lyrs.size())
        return 0;
    for(it=lyrs.begin();it!=lyrs.end();++it) {
        ep = dynamic_cast<RXEntity_p>(*it);
        if(!ep) {
            rxerror("Fabric list undefined !",2);
            return(0);
        }
        assert(ep && ep->TYPE == PCE_LAYER );

        fab = (class RXLayer*)ep;
        if(!ref_fab && fab->Use_Me_As_Ref) {
            ref_fab = fab;
            ref_count = mcount;      /* set ref_count to the no. of the ref_material */
        }
        if(!fab->matptr) {
            rxerror("Null Fabric",2);
            return(0);
        }

        mcount++;
    }

    l_ColourList = (char **)CALLOC( (mcount+2), sizeof(char*));
    colour_thk = (double*)CALLOC( (mcount+2), sizeof(double));

    tld = (TriLamDefn *) CALLOC(mcount,sizeof(TriLamDefn));//LEAK!!
    lam_list = tld;

    if(!ref_fab) {
        rxerror("NO REFERENCE MATERIAL FOUND \nDefaulting to first material found",2);
        it = lyrs.begin() ;
        ep = dynamic_cast<RXEntity_p>(*it );
        ref_fab = (class RXLayer*)ep ;
    }

    /* do everything on the refernce material first */
    /* NOTE: the site q is not set yet and is currently untouched by SetTo... */

    if(ref_fab->Use_Black_Space)
        l_t=BlackTri(index,space);
    else
        l_t=RedTri(index);
    l_t.Centroid(&l_qq); /* get centroid of triangle  in this space*/

    assert( ref_fab->matstruct.function );
    matEnt = ref_fab->matptr;

    if(matEnt->TYPE==FABRIC)
        ref_material  = (struct PC_MATERIAL *) matEnt->dataptr;
    else {
        struct PC_FIELD *field = (PC_FIELD *)matEnt->dataptr;
        assert(!(field->flag&DOVE_FIELD));
        ref_material  = (struct PC_MATERIAL *) field->mat->dataptr;
    }
    memset(prestress,0,sizeof(double)*3);
    //refOK=
    (ref_fab->matstruct.function)(&thick,mx,&l_qq,p_fte,ref_fab,xv,prestress);			  /* get Vmref */
    //assert(refOK);
    if(!ref_material->creaseable) {
        if(thick >0.0) {
            p_fte->creaseable = 0;	/* if any part NOT creaseable turn whole triangle  OFF */
        }
        else
            cout<< " dont turn off creaseable because Ref Mat thickness low\n"<<endl;
    }

    for(int ii=0;ii<9;ii++)   p_fte->mx[ii] += mx[ii]*thick;  // Peter added thick Feb 2003. Was a BUG in /FI method
    for(int ii=0;ii<3;ii++)  p_fte->prestress[ii] += prestress[ii]*thick;  // Peter added thick Feb 2003. Was a BUG in /FI method

    /* Get the angle between the triangle base and this layer material x-axis*/

    matrefVector.x = xv[0];  // in redspace or blackspace coordinates
    matrefVector.y = xv[1];
    matrefVector.z = xv[2];
    assert(  (fabs(matrefVector.x ) + fabs(matrefVector.y ) + fabs(matrefVector.z )  ) >0.);

    ON_Xform trigraph1= l_t.Trigraph ();
    assert(trigraph1.IsValid());

    vdash = trigraph1*matrefVector;   // matrefvector is in  red/black space, same as l_t;

    if(  (fabs(vdash.x ) + fabs(vdash.y ) + fabs(vdash.z )  ) <=0.0000001)
        qDebug()<< " Warning:: short VDash ";
    // assert(  (fabs(vdash.x ) + fabs(vdash.y ) + fabs(vdash.z )  ) >0.);

    matref_to_base_angle =  -(atan2(vdash.y,vdash.x));	/* angle from reference material to triangle base*/
    p_fte->SetRefAngle ( matref_to_base_angle );

    // vdash is the laminate ref vector in element coordinates.

    tld[ref_count].Angle = 0.0;
    tld[ref_count].Thickness = thick;
    tld[ref_count].Layptr = ref_fab;
    tld[ref_count].dd = (double *)ref_material->d; //linux insists
    tld[ref_count].ps=NULL ;  // WASTEFUL AS THEese are alreadyNULL
    tld[ref_count].m_flags=0;
    if(p_fte->m_colour) RXFREE (p_fte->m_colour);
    p_fte->m_colour = STRDUP(ref_material->text);
    strcpy(colourstring, ref_material->text);
    l_ColourList[cc] =ref_material->text;
    colour_thk[cc++] = thick;

    for(it=lyrs.begin();it!=lyrs.end();++it) {// Now we have ref fab, loop thru the other layers
        ep = dynamic_cast<RXEntity_p>(*it);
        fab = (class RXLayer*)ep ;

        if(fab == ref_fab) {	/* dont process twice */
            tld++;
            continue;
        }
        matEnt = fab->matptr;
        switch ( matEnt->TYPE) {
        case FABRIC:
            a_material = (struct PC_MATERIAL *) matEnt->dataptr;
            break;
        case FIELD:
            l_field = (PC_FIELD *)matEnt->dataptr;
            if(!( l_field->flag & DOVE_FIELD) && !( l_field->flag & FILAMENT_FIELD) )
                a_material = (struct PC_MATERIAL *) l_field->mat->dataptr;
            else  {
                a_material=NULL; // we won't get color but until we have inheritance its a pain otherwise
            }

            if(( l_field->flag & FILAMENT_FIELD))
            {
                assert( fab->Use_Black_Space  );
                xv[0]=  matrefVector.x ;  //  blackspace coordinates
                xv[1]=  matrefVector.y ;
                xv[2] = matrefVector.z ;
            }
            break;
        case PCE_DOVE: // never happens
            assert(0);
            break;
        default:
            assert(0);
        };

#ifdef USE_LAST_COLOR
        te->colour = a_material->text;
#endif
        l_qq.x=l_qq.y=l_qq.z=0;
        if(fab->Use_Black_Space)
            l_t=BlackTri(index,space);
        else
            l_t=RedTri(index);
        l_t.Centroid(&l_qq);// coord space may have changed

        memset(prestress,0,sizeof(double)*3);
        fieldOK=  (fab->matstruct.function)(&thick,mx,&l_qq,p_fte,fab,xv,prestress);		// mx is Dmatrix at centroid. its ref dir (redorblack) is xv.

        if(!fieldOK)
            thick=0;
        if(a_material &&(!a_material->creaseable)) {		// this lets a field be non-creasable eg for a corner patch
            if(thick > 0.0)
                p_fte->creaseable = 0;			// cout<< " if any part NOT creaseable turn whole triangle  OFF \n"<<endl;
        }

        ON_Xform trigraph= l_t.Trigraph ();
        vdash = trigraph*ON_3dVector(xv[0],xv[1],xv[2]); // in the same space as the fab.

        this_mat_to_Element_Base = (-(atan2(vdash.y,vdash.x)));	// angle from this material to element base.
        this_mat_to_matref_angle = (matref_to_base_angle - this_mat_to_Element_Base);// angle from this material to ref mat

        rotate_material(mx,-this_mat_to_matref_angle);

        for(int ii=0;ii<9;ii++)    p_fte->mx[ii] += mx[ii]*thick;

        rotate_stress(prestress,-this_mat_to_matref_angle);

        for(int ii=0;ii<3;ii++) p_fte->prestress[ii] += prestress[ii]*thick;


        tld->Angle = this_mat_to_matref_angle; // tested 2003 . the  sign is OK
        tld->Thickness = thick;
        tld->Layptr = fab;
        if(fab->matptr->TYPE==FIELD ) {//  As mx is transformed,  shouldnt Angle be 0
            tld->dd = (double*) MALLOC(9*sizeof(MatValType)); //LEAK
            memcpy(tld->dd, mx, 9*sizeof(MatValType)); // this is in material red direction
            tld->m_flags = tld->m_flags  | TLD_ALLOCED;
        }
        else
            tld->dd = (double*) a_material->d; // unless its a filament field, in which case we 'strdup' mx.
        tld->ps=NULL ;  // WASTEFUL AS THEese are alreadyNULL


        if(a_material) { // NULL for doves which is bad because they dont give color, not can they be used as ref
            l_ColourList[cc] =a_material->text;
            colour_thk[cc++] = thick;
        }

        tld++;
    }

    p_fte->m_laminae = lam_list;
    assert(fabs(p_fte->RefAngle() - matref_to_base_angle)< ON_EPSILON); // was a redundant assignment
    p_fte->laycnt = mcount;
#ifdef HOOPS
    if(Add_Colours(colourstring,colour_thk,l_ColourList ,cc)) {
        if(p_fte->m_colour) RXFREE (p_fte->m_colour);
        p_fte->m_colour = STRDUP(colourstring);
    }
#endif
    if(!p_fte->m_colour ) {
        p_fte->m_colour = STRDUP("magenta");
        sprintf(buf,"Tri %d has NO colour on reference material",p_fte->GetN());
        rxerror(buf,1);
    }

    if (l_ColourList ) RXFREE(l_ColourList );
    if ( colour_thk)	RXFREE( colour_thk);
    return(matref_to_base_angle);
} // buildonetriMaterial

int RXPanel::Print_Panel_Triangles( FILE *fp)const{
#ifdef NEVER
    int k;
    // lets print the tri stress data
    {
        int tdn;
        TriStressStruct *l_td = NULL;
        OldTriangle_t *tp = np->FullTri; double totArea =0.0,totP=0, Weight;
        totArea =0.0; totP=0.0; Weight=0.0;
        Get_Triangle_Results(sail, &l_td);
        fprintf(fp," \ttriFTno \tarea \t u \t v \t x \t y \t z \t UPstress \tLPstress\t ang(rad)\t");
        fprintf(fp,"  sx  \t  sy  \t  txy  \t");
        fprintf(fp,"  ex  \t  ey  \t  gxy  \t crease \n");

        // I think the stress stuff is wrong. The index should be offset

        for(k=0;k<np->nFtri ; k++) {
            FortranTriangle *ft = tp->m_ft;
            site cs(NULL);
            //	assert(ft->m_tp == tp);
            Tri_Centroid(tp->m_eEd[0],&cs);
            totArea += ft->area;
            fprintf(fp," \t%d \t%f\t ", ft->n, ft->area);
            tdn = ft->n-1;
            fprintf(fp, " %g \t%g\t ", cs.m_u,cs.m_v);
            fprintf(fp, " %f \t%f\t%f\t ", cs.x,cs.y,cs.z);
            if(l_td) {
                fprintf(fp, " %f\t%f\t%f\t", l_td[tdn].princ[0],  l_td[tdn].princ[1],  l_td[tdn].princ[2]);
                fprintf(fp, " %f\t%f\t%f\t", l_td[tdn].stress[0], l_td[tdn].stress[1], l_td[tdn].stress[2]);
                fprintf(fp, " %g10.5\t%g10.5\t%g10.5\t", l_td[tdn].eps[0],  l_td[tdn].eps[1], l_td[tdn].eps[2]);
                fprintf(fp, " %f\n", l_td[tdn].cre);
                totP += l_td[tdn].princ[2]  *( ft->area * l_td[tdn].princ[0]) ;
                Weight +=  ft->area * l_td[tdn].princ[0];
            }
            else
                fprintf(fp, "\t NO stress defined\n");

            PCF_PrintTriFilamentList(ft, fp);
            tp++;
        }
        fprintf(fp,"PanelArea = %f ", totArea);
        if((Weight) > 0.0001)
            fprintf(fp," ,Weighted mean princAngle = %f deg\n", totP/Weight*57.29578);
        else
            fprintf(fp," (zero)\n");
        RXFREE(l_td);

    }
#else	// just print some geometric info
#ifndef NEVER
    double totArea =0.0;
    RXSitePt cs;
    fprintf(fp,"TRIANGLES\nTriftno \t     area \t      u \t      v \t     cx \tcy     \tcz \t n1 \t n2 \t n3\n");

    // I think the stress stuff is wrong. The index should be offset
    for(unsigned int i=0;i<m_FEtris.size() ;i++)	{
        RX_FETri3 *ft = m_FEtris[i];
        ft->Centroid(&cs);

        totArea += ft->m_TriArea;
        fprintf(fp," \t%5d \t%f\t ", ft->GetN(), ft->m_TriArea);

        fprintf(fp,"%g \t%g \t%g \t%g \t%g  ", cs.m_u,cs.m_v,cs.x,cs.y,cs.z     );
        for(int j=0;j<3;j++) fprintf(fp,"\t%d ",   ft->GetNode(j) ->GetN());
        fprintf(fp,"\n");
        PCF_PrintTriFilamentList  (ft, fp);
    }
    fprintf(fp," Panel Area = %f \n", totArea);


#endif
#endif
    return 1;
}



int RXPanel::Print_Panel_DMatrices( FILE *fp)const{

    unsigned int j;
    const char*l_what;

    // we want to present the D matrix in the same reference frame for all elements.
    // suggest referring it to the iso-V through the centroid.
    // we Rotate the matrix by theta to get it into the element axis system.

    // then we get the triangle axis system
    // then we find the angle (atan(y/x) of the U derivative of the surface in the triangle axis system.
    // then rotate the matrix again by this angle
    // and print it.

    fprintf(fp," \ttriftNo \tarea \t u \t v \t d x 9\t");
    fprintf(fp,"  \n");

    for(j=0;j<m_FEtris.size(); j++) {
        FortranTriangle *ft = m_FEtris[j];
        ON_Xform l_Element_Trigraph;
        RXSitePt cs;
        ON_3dPoint r; ON_3dVector r10,  r01,  r11,r20,  r02;
        double UDir_To_BaseAngle;
        RXTriangle l_t = RXTriangle(*ft);
        UDir_To_BaseAngle=0;
        ft->Centroid (&cs);
        l_Element_Trigraph=l_t.Trigraph();

        MatValType l_Dmatrix[9];
        memcpy(l_Dmatrix, ft->mx,9*sizeof(MatValType));
        if(Esail->GetMould() &&
                Evaluate_NURBS_Derivatives(Esail->GetMould(), cs.m_u,cs.m_v,&r, &r10, &r01, &r11, &r20,&r02)) {
            //r10 is the derivative WRT u. IE the chord direction

            // The following rotates the material to the tri axis system.
            // which is pointless.
            ON_3dVector l_Udirection = ON_3dVector(r10.x,r10.y,r10.z); // global
            ON_3dVector l_UDirInElAxes = l_Element_Trigraph*l_Udirection;
            UDir_To_BaseAngle=atan2(l_UDirInElAxes.y,l_UDirInElAxes.x);
            rotate_material(l_Dmatrix,UDir_To_BaseAngle); // or negative
            // this is wrong.  the D matrix is already in the material axis system, which may me anything.
            // we might want to rotate it to the u axis or we might just want to record its X direction.



            l_what="to iso-V";
        }
        else
            l_what="in local material axes";

        fprintf(fp," \t%d \t%f \t", ft->GetN(), ft->Area());

        fprintf(fp, "%9.5g \t%9.5g", cs.m_u,cs.m_v);
        for(int i=0;i<9;i++)
            fprintf(fp, " \t%f",l_Dmatrix[i] );
        fprintf(fp, " \t! %f %s\n",UDir_To_BaseAngle *180/ON_PI, l_what);
    }

    return 1;
}



int RXPanel::WriteAsPoly(struct triangulateio *io){
    // write   as a poly
    int i, j;
    char fname[512]; sprintf(fname,"%s_%d.poly",this->name(),io->numberofpoints  );
    FILE *fp = RXFOPEN(fname,"w");
    cout<<"write panel ' " <<  this->name() <<  "' as poly"<<endl;
    if(!fp) return 0;
    fprintf(fp,"#  debug poly file written by OpenFSI.org\n");
    fprintf(fp,"# <# of vertices> <dimension (must be 2)> <# of attributes><# of boundary markers (0 or 1)>\n");
    fprintf(fp,"%d %d %d %d\n", io->numberofpoints,2,io->numberofpointattributes,0);
    for (i = 0; i < io->numberofpoints; i++) {
        fprintf(fp,"%d", i);
        for (j = 0; j < 2; j++) {
            fprintf(fp,"  %.6g", io->pointlist[i * 2 + j]);
        }
        for (j = 0; j < io->numberofpointattributes; j++) {
            fprintf(fp,"  %.6g",
                    io->pointattributelist[i * io->numberofpointattributes + j]);
        }
        fprintf(fp,"\n");
    }
    fprintf(fp,"\n# the segments\n#  <# of segments> <# of boundary markers (0 or 1)>\n");
    fprintf(fp,"%d %d\n", io->numberofsegments,0);

    for (i = 0; i < io->numberofsegments; i++) {
        fprintf(fp,"%4d", i);
        for (j = 0; j < 2; j++) {
            fprintf(fp,"  %4d", io->segmentlist[i * 2 + j]);
        }
        fprintf(fp,"\n");
    }
    fprintf(fp,"\n# no holes\n 0 \n# skipping any regions and regional atts\n");

    FCLOSE(fp);
    return 1;
}// RXPanel::WriteAsPoly
// shift x to the nearest place on the mould, using pp->(u,v) as a seed
// fills in the trpts' UV coordinates.

TRIANGLE_POINT::TRIANGLE_POINT() 
{
    m_Sptr=0;
    m_marker=-99;
    m_u=-1;
    m_v=-1; // was N_UNSET_VALUE;
    m_e.clear();
}
TRIANGLE_POINT::~TRIANGLE_POINT() 
{
}

bool TRIANGLE_POINT::PullToMould (RXEntity_p mould,const ON_Xform *axes ){ // we need to make this work in a general coordinate system. 
    int err;
    if(this->m_Sptr)
    { m_u = this->m_Sptr->m_u; m_v= this->m_Sptr->m_v;  }
    else
    {	 m_u=  0.5;  m_v=0.5;}
    assert(mould);


    z=	Evaluate_NURBS_By_XY(mould,x,y ,&(m_u),&(m_v),&err,axes);    // should use  CoordModelXToUV
    // we now have u and v correct but if axes are non-null we need to evalaute the transformed mould at (u,v)

    return (err==0);
}
MatValType * RXPanel::rotate_material(MatValType *Q,double theta){

    double m,n,m2,n2;
    MatValType J[MXLENGTH],Jt[MXLENGTH];
    /* int i,j,k;	*/

    m=cos(theta); n = sin(theta);
    m2 = m*m; n2=n*n;

    J[0] = m2;
    J[1] = n2;
    J[2] = 2.0*m*n;
    J[3] = n2;                   /* set up rotation MX */
    J[4] = m2;
    J[5] = -2.0*m*n;
    J[6] = -m*n;
    J[7] = m*n;
    J[8] = m2 - n2;

    Jt[0] = m2;
    Jt[1] = n2;
    Jt[2] = -m*n;
    Jt[3] = n2;                   /* set up rotation MX */
    Jt[4] = m2;
    Jt[5] = m*n;
    Jt[6] = 2.0*m*n;
    Jt[7] = -2.0*m*n;
    Jt[8] = m2 - n2;


    MatMult33(J,Q,Q);
    MatMult33(Q,Jt,Q);

    return(Q);
}

int RXPanel::rotate_stress(double Q[3],double theta){
    double m,n,m2,n2,mn;
    MatValType J[MXLENGTH];

    m=cos(theta); n = sin(theta);
    m2 = m*m; n2=n*n;
    mn = m*n;
    J[0] = m2;
    J[1] = n2;
    J[2] = 2.0E00*mn;
    J[3] = n2;                   /* set up rotation MX */
    J[4] = m2;
    J[5] = -J[2]  ;//-2.0*mn;
    J[6] = -mn;
    J[7] = mn;
    J[8] = m2 - n2;

    VMult33(J,Q,Q);

    return(1);
}

MatValType *RXPanel::MatMult33(MatValType *a,MatValType *b,MatValType *q)
{
    int i,j,k;
#define IND (3)
    MatValType dummy[9];

    for(i=0;i<9;i++) dummy[i] = 0;


    for(j=0;j<3;j++)  {
        for(k=0;k<3;k++)  {
            for(i=0;i<3;i++) {
                dummy[k+j*IND] += a[i+j*IND] * b[k + i*IND];
            }
        }
    }

    memcpy(q,dummy,9*sizeof(MatValType));
    return(q);
}
MatValType *RXPanel::VMult33(MatValType *a,MatValType *b,MatValType *q)
{
    int i,j;

    MatValType dummy[3];

    memset(dummy,0, 3*sizeof(MatValType));

    for(j=0;j<3;j++)  {
        for(i=0;i<3;i++) {
            dummy[j] += a[i+j*IND] * b[i];
        }
    }

    memcpy(q,dummy,3*sizeof(MatValType));
    return(q);
}
int RXPanel::GetFilamentList()
{
    int rc=0, filtot=0;
    RXEntity_p  l_pSco;
    sc_ptr l_pSc;
    rxON_BoundingBox l_bbt,l_bbc;
    ON_Interval foundInterval;
    this->m_fils.clear();
    class RXSail *l_sail = this->Esail;
    this->GetTightBoundingBox(l_bbt); // this is really a GetVanillaBB not tight
    l_bbt.GrowAllRound (l_sail-> m_Linear_Tol );
    for (ent_citer it = l_sail->MapStart(); it != l_sail->MapEnd(); ++it) {
        l_pSco  = dynamic_cast<RXEntity_p>( it->second);
        if(l_pSco->TYPE != SEAMCURVE) continue;
        l_pSc = (sc_ptr  )l_pSco;
        if(!(l_pSc->FlagQuery(FILAMENT))) continue;
        if( (l_pSc->Needs_Resolving)) continue;
        if( (l_pSc->Needs_Finishing)) continue;
        filtot++;
        ON_Curve *onc = l_pSc->m_pC[1]->GetONCurve();
        if(!onc) {
            if(l_pSc->m_pC[1]->GetTightBoundingBox(l_bbc,0,0)) {
                rxON_BoundingBox l_temp = l_bbt;
                if(!l_temp.Intersection(l_bbc))
                    continue;
                //foundInterval= l_pSc->m_pC[1]->Domain();
            }
        }
        else {
            /* CrvBBInter does a binary BB search by splitting the ONcurve.  This split is really slow, but even so this gives about 25% speedup.
 TO make it better, we need a alternative to the split. IE the lower BB is of the CVs below( what exactly does 'below' mean??) and the split pt.
 TO make it better still we'd pass the interval down, so the tris don't have to look outside the panel's BB.
*/
            if(! CrvBBInter(onc,l_bbt,foundInterval) )
                continue;
        }
        this->m_fils.insert(l_pSco); // should pass down the interval
        rc++;

    }//for  it

    return rc;
}
int RXPanel::GetTightBoundingBox(rxON_BoundingBox &bbox, bool bGrowBox, const ON_Plane *onb) const
{
    int i;
    ON_SimpleArray<ON_3dPoint> pt;

    int nc = this->blackpts.size();
    for(i=0;i<nc;i++)
        pt.AppendNew() = blackpts[i].ToONPoint();

    if(!nc)  // sometimes a panel is just a collection of elements.
    {
        pt.AppendNew() = ON_3dPoint(-100000,-100000,-100000); // ugly to hard-code this
        pt.AppendNew() = ON_3dPoint(100000,100000,100000);
    }
#ifndef ON_V3
    i=bbox.Set(pt,true); //?? return cvalue
    assert(i); // because we havent stepped it

#else
    i=ON_GetTightBoundingBox(  pt, bbox, bGrowBox,onb) ; // ON V3 only
#endif
    pt.Destroy();
    return i;
}





int RXPanel::Supply_Material(const char*matname) {  
    RXEntity_p matkey;
    class RXLayer *thelayer=0;
    int TheEdge,iret=0;
    /*  first look for a 'paneldata' entity which matches this panel.
     if none found, look for a fabric defined on any pside,
    or failing that, on any owning SC
    or failing that, on an owning zone
 or failing that, ask the user.
    or failing that, (test code) use the material named in the argument.
      (production code) return 0;
     Oct 1996. A variation
  Moulded sails  require the ref vector to vary along the seam
  So it its Moulded, we use the Parallel mat func
  and we set data to the owning entity of the ref SC

    */	char str[256];

    struct LINKLIST *ll;
    //  SAIL *sail = this->Esail;
    matkey = this->Get_PanelData_Material();
    if(!matkey )
        this->Check_For_Edge_Material(&matkey);

    if(!matkey){
        sprintf(str,"Material for panel: %s",this->name());
        this->Display_One_Panel(0);
#ifdef HOOPS
        HC_Open_Segment_By_Key(this->hoopskey);
        HC_Set_Color("everything=magenta");		HC_Set_Visibility("everything=on");
        HC_Set_Line_Pattern("- -");				HC_Set_Line_Weight(3.0);
        HC_Set_Rendering_Options("attribute lock = (color,line pattern,line weight)");
        HC_Close_Segment();
#endif
#ifdef linux
        printf("working on panel '%s' atts '%s'\n",this->name(),qPrintable(this->AttributeString()));
        matkey = UserSelection("fabric","",str,(this->Esail->GetGraphic() ));  //UserSelection from uselect.h does nothing
#endif
        HC_Delete_By_Key(this->hoopskey);
        this->hoopskey=0;

        if(matkey) {
            Record_SC_Edit("mat",matkey); // mputs the $ref on the SCs
            // if it cannot, it makes a paneldata card.
        }
    }
    if(!matkey){
#ifdef linux
        return 0;
#endif
        matkey=this->Esail->GetKeyWithAlias( "fabric",matname);
        Record_SC_Edit("mat",matkey);
        iret=1;
        if(!matkey){
            rxerror("You must define a default fabric",1);
            return(0);
        }
    } //!matkey
    if(!matkey->NeedsComputing()) // peter added the condition 3 march 2013 because of vtune
        matkey->SetNeedsComputing(); // this call is extremely slow

    ll= this->Get_Panel_Ref_SC(); // try it here
    sprintf(str,"%s_l",this->name());
    thelayer =dynamic_cast<class RXLayer*>(this->Esail->Insert_Entity("layer",str,0,(long)0," ","generated"));

    thelayer->matptr=matkey;

    thelayer->SetNeedsComputing(0);
    thelayer->Use_Black_Space=0;
    thelayer->Use_Me_As_Ref=1;
    if(this->Edges_Are_All_Curves())// a speed optimisation. esp. for fields
        thelayer->Use_Black_Space=1;

    // was here		ll= this->Get_Panel_Ref_SC();
    if(ll) {
        thelayer->matstruct.function = Get_Material_Function_Key("curve");
        thelayer->matstruct.ll= ll;
        thelayer->matstruct.m_sizeofdata=0;
    }
    else {
        thelayer->matstruct.m_sizeofdata=3;
        thelayer->matstruct.m_Data.resize(3);
        thelayer->matstruct.function = Get_Material_Function_Key("uniform");
        ON_3dVector vv;
        this->Get_Panel_Ref_Vector(vv);  /* MUST be in red-space */
        for(int k=0; k<3;k++) thelayer->matstruct.m_Data[k]=vv[k];
    }
    matkey->SetRelationOf(thelayer,child|niece,RXO_MAT_OF_LYR); 	// June 2003 for DeleteAlias
    this->SetRelationOf(thelayer,spawn|parent,RXO_LAYER);

    if(this->hoopskey) {
        HC_Delete_By_Key(this->hoopskey);
        this->hoopskey=0;
    }
    for (TheEdge=0;TheEdge<this->m_psidecount;TheEdge++) {
        int mside = 2*(1- this->reversed[TheEdge]);
        PSIDEPTR pss = this->m_pslist[TheEdge];
        thelayer->SetRelationOf(pss,PS_LAYER_RELATION,RXO_LAYER,mside);
    }
    CollectFieldLayers();
    return iret;
}
bool RXPanel::FieldIsLocal(const RXEntity_p fieldEnt )
///if any pside's SC may have '$field=name1,name2,name3
{
    bool rc = false;
    int j,ns;
    QString val;
    QStringList flist;
    PSIDEPTR pp;
    sc_ptr sc;
    ns= m_psidecount;

    for (j=0;j<ns;j++) {
        pp = m_pslist[j]; sc = pp->sc;
        if( sc->AttributeGet("$field" ,val))
        {
            flist=val.split(QRegExp ("[\(\)\,]") );
            if(flist.contains(fieldEnt->name() ,Qt::CaseInsensitive  ))
                return true;
        }

    }


    return false;
}

int RXPanel::CollectFieldLayers() {
    int TheEdge,iret=0;
    bool doit=true;

    /* Search the model for fields onto this panel
NOTE each field is owned by a PC LAYER of the same name
and refers to a PC MATERIAL
 */
    SAIL *sail = this->Esail;
    ent_citer it;
    //  sail->FindInMap( );
    for (it = sail->MapStart(); it != sail->MapEnd(); ++it) {
        RXEntity_p fieldEnt  = ( RXEntity_p) it->second;
        if (fieldEnt->TYPE!=FIELD) continue;
        RXEntity_p layerEnt;
        struct PC_FIELD *f = (PC_FIELD*)fieldEnt->dataptr;
        if(!f->Local)
            doit=true;
        else  // its a local field. Does the panel know about it?
            doit= this->FieldIsLocal(fieldEnt );
        if(doit) {
            layerEnt= sail->GetKeyWithAlias( "layer",fieldEnt->name());
            if(!layerEnt)
            {
                QString mess=QString ("(Panel '%0') field '%1' has no layer named '%2'").arg(this->name()).arg(fieldEnt->name()).arg(fieldEnt->name());
                rxerror(mess,3);
            }
            else {
                iret++;
                layerEnt->SetRelationOf(this,child,RXO_LAYER);
                for (TheEdge=0;TheEdge<this->m_psidecount;TheEdge++) {
                    int mside = 2*(1- this->reversed[TheEdge]);
                    PSIDEPTR pss = this->m_pslist[TheEdge];
                    layerEnt->SetRelationOf(pss,PS_LAYER_RELATION,RXO_LAYER,mside);
                }
            }
        }
    }//for it
    return(iret);
}


int RXPanel::Check_For_Edge_Material( RXEntity_p *matkey) {

    /* look for a fabric defined on any pside,
    or failing that, on any owning SC
    or failing that, on an owning zone
    If none found, use default */
    int j,ns,side;
    PSIDEPTR pp;
    sc_ptr sc;
    ns= m_psidecount;

    for (j=0;j<ns;j++) {
        pp = m_pslist[j]; sc = pp->sc;
        side = 2*(1- this->reversed[j]);

        if(pp->Defined_Material[side]) {
            *matkey = pp->Defined_Material[side];
            //cout<< " from a pside"<<endl;
            return(1);
        }
    }
    for (j=0;j<ns;j++) {
        pp = m_pslist[j]; sc = pp->sc;
        side = 2*(1- (*this).reversed[j]);
        // look here for partial materials

        if(sc->Defined_Material[side]) {
            *matkey = sc->Defined_Material[side];
            return(1);
        }
    }
    for (j=0;j<ns;j++) {
        pp = m_pslist[j]; sc = pp->sc;
        /*	if(sc->zone){
   struct PC_ZONE*z = sc->zone->dat aptr;
   if(z->mat) {
    *matkey = z->mat;
    return(1);
   }
  }	*/
    }

    return(0);
}

RXEntity_p RXPanel:: Get_Panel_Ref_Vector( ON_3dVector &x)  {
    PSIDEPTR pp;
    RXEntity_p sco ;
    sc_ptr sc;
    Panelptr  np = this;
    RXCurve *l_theCurve;
    int ns,j,k,side,nk;

    double s,s1,s2,refl, angle = 0.0;
    const char*matname;
    int pside_To_Use = -1;
    VECTOR  *poly=NULL;
    ON_3dPoint p1,p2,l_p;
    ON_3dVector t, ch, l_onz =this->Normal(); //??

    int count=0;
    ns = this->m_psidecount;
    /* first look on psides */

    for (j=0;j<ns;j++) {
        pp = m_pslist[j]; sco = pp->sc;  sc = (sc_ptr)sco;
        side = 2*(1- this->reversed[j]);
        if(pp->Mat_Angle[side].flag &1) {
            pside_To_Use=j;
            angle = pp->Mat_Angle[side].angle;
            break;
        }
    }
    if (pside_To_Use==-1) {
        for (j=0;j<ns;j++) {
            pp = m_pslist[j]; sco = pp->sc; sc = (sc_ptr)sco;
            side = 2*(1- (*np).reversed[j]);
            if(pp->sc->Mat_Angle[side].flag&1) {
                pside_To_Use=j;
                angle = sc->Mat_Angle[side].angle;
                break;
            }
        }
    }

    if(pside_To_Use!=-1) {
        j= 	pside_To_Use;
        pp = m_pslist[j];
        sco = pp->sc;
        sc = (sc_ptr  )sco;

        side = 2*(1- (*np).reversed[j]);
        refl = pp->sc->GetArc(side);
        if (0!=side) { /* going forwards*/
            s1 = (pp->End1off->Evaluate(pp->sc,side));
            s2 = (pp->End2off->Evaluate(pp->sc,side));
        }
        else {
            s1 = (pp->End2off->Evaluate(pp->sc,side));
            s2 = (pp->End1off->Evaluate(pp->sc,side));
        }

        Get_Position_By_Double( sco, s1,side,p1);
        Get_Position_By_Double( sco,s2,side,p2);
        ch = p2-p1;
        refl = ch.Length ();
        if(refl<FLOAT_TOLERANCE) refl=FLOAT_TOLERANCE;

        PC_Vector_Rotate( ch,angle/57.29577, l_onz); //SB panel mean normal

        x[0] = 	ch.x/(float)refl;
        x[1] = 	ch.y/(float)refl;
        x[2] = 	ch.z/(float)refl;


        return(sco);
    }
    // Here there is no material reference.
    // get the User to select a ref SC and push it onto any nearby SC's atts
    // return the sco
    sco = User_Selects_Ref_SC();
    if(sco) {
        // Here we copy the SC chord into xv WRONG. We should carry the curve ref forwards.
        sc = (sc_ptr  )sco;
        l_theCurve = sc->m_pC[1];
        l_theCurve->Find_Tangent( sc->m_arcs[1]/2.0,&p1, &t);
        //			theCurve->Find_Tangent( theCurve->Get_arc()/2.0,&v1, &v);
        assert(fabs(t.LengthSquared()-1.0)<0.000001);  // so dont unitize
        //t.Unitize();
        x[0]=t.x;
        x[1]=t.y;
        x[2]=t.z;
        return sco;
    }
    for (j=0;j<ns;j++) {
        pp = m_pslist[j];
        sco = pp->sc;
        sc = (sc_ptr  )sco;
        side = 2*(1- (*np).reversed[j]);
        refl = sc->GetArc(side);
        if (0!=side) { /* going forwards*/

            s1 = (float)(pp->End1off->Evaluate(sc,side));
            s2 = (float)(pp->End2off->Evaluate(sc,side));
        }
        else {
            s1 = (float)(pp->End2off->Evaluate(sc,side));
            s2 = (float)(pp->End1off->Evaluate(sc,side));
        }
        nk = 4* (int) fabs(s2-s1); // one every 1/4m
        nk = max(2,nk);
        for(k=0;k<nk;k++) {
            s = s1*((float)1.0-(float) k/(float)nk) + s2 * (float)k/(float)nk;
            Get_Position_By_Double( sco,s,side,l_p);
            VECTOR v; v.x=(float)l_p.x; v.y=(float)l_p.y; v.z=(float)l_p.z;
            Append_To_Poly(&poly,&count,&v,1);
        }
    }
    if(poly) {
        VECTOR v;
        PC_Compute_Polyline_Mean(poly,count,&v);
        x[0]=v.x;
        x[1]=v.y;
        x[2]=v.z;
        /*	 ("Ref vector from mean %f %f %f\n",x[0],x[1],x[2]); */
        RXFREE(poly);
    }
    else {
        x[0]=(float)0.0;x[1]=(float)1.0;x[2]=(float)0.0;
        sco=NULL;
    }
    return(sco);
}
int Ptr_Sort_Fn( const void *ia,const void *ib) { // used by Tidy_Panel_Materials
    if (ia>ib) return(1);
    if (ia<ib) return(-1);
    return(0);
}

/* Tidy_Panel_Materials: Collect a list of all the materials in each pside->mats[side]
    remove duplicates (as a full material list is stored in each pside )
   Push onto the material lists of the panel and of each pside  */
int  RXPanel::Tidy_Panel_Materials()	{ /* called in paneller, in PANEL.c */
    int l;
    RXEntity_p last;
    static int l_s_def = 0; // OK its static but its just to suppress a warning
    set<RXObject*> z;
    set<RXObject*>::iterator it;

    ;
#ifdef NEVER
    int mc=0;
    RXEntity_p *elist=NULL;

    for (l=0;l<this->n;l++) {//n has become m_psidecount
        PSIDEPTR pss;
        struct LINKLIST*next=NULL;
        int mside = 2*(1- this->reversed[l]);
        pss = this->m_plist[l];

        next = pss->m_psmats[mside];// in Tidy__
        for(;next!=NULL;) {
            mc++;
            elist=(RXEntity_p *)REALLOC( elist,mc*sizeof(RXEntity_p ));
            elist[mc-1]=(RXEntity_p )next->data;
            next=next->next;

        }
        Free_Entity_List(&(pss->m_psmats[mside]));// this is Tidy_..
    }

    /* elist is now all the Layers associated with this panel
  push each element onto the panel, and onto each pside,  removing any duplication */

    qsort(elist,mc,sizeof(RXEntity_p ),Ptr_Sort_Fn);

    Free_Entity_List(&(this->m_mlist));// Tidy
    last= NULL;
    for(l=0;l<mc;l++) { // walk elist
        int e; /* ps number */
        if(elist[l]&& elist[l]!=last) {
            last=elist[l];
            Push_Entity(last,&(this->m_mlist) );last->Set RelationOf(this,child,RXO_LAYER);

            for (e=0;e<this->n;e++) {
                int mside = 2*(1- this->reversed[e]);
                PSIDEPTR pss = this->m_plist[e];
                Push_Entity(last,&(pss->m_psmats[mside]));  last->Set RelationOf(pss,child,RXO_LAYER,mside);
            }
        }
    }
    if(elist) RXFREE(elist);
    if(!this->m_mlist) { //ie if panel->FindInMap(RXO_LAYER,RXO_ANYINDEX) returns empty
        if(this->Supply_Material("default")){	  /* 0 if a defined mat used */
            if(!l_s_def) rxerror("Some panels have default material",1);
            l_s_def = 1;
        }
    }	/* panel->mlist now has ONE entry. Push onto psides */



    if(this->m_PanelData){ // maybe from an entity or from an edit
        struct PANELDATA *pd = this->m_PanelData;
        RXEntity_p mat = pd->mat;
        RXEntity_p ref = pd->refSC;
        assert(pd->owner);
        // append pd owner atts to panel owner's atts
        ON_String buf;
        buf=ON_String(this ->attributes )+ON_String(pd->owner->attributes );
        RXFREE(this ->attributes);
        this->attributes=STRDUP(buf.Array ());
        if(pd->owner) {
            if(pd->owner!=this)
                pd->owner->Kill( );
            //else
            //cout<< " DONT delete this paneldata - we own it\n"<<endl;
        }
        Create_PanelData_Entity(this, mat, ref);
        if(mat ) 				//May 2003
            mat->SetNeedsComputing();
    }
#else
    set<RXEntity_p >::iterator itt;
    set<RXEntity_p > elist;
    for (l=0;l<this->m_psidecount;l++) {
        PSIDEPTR pss;
        int mside = 2*(1- this->reversed[l]);
        pss = this->m_pslist[l];
        z= pss->FindInMap(RXO_LAYER, RXO_ANYINDEX,mside);

        for(it=z.begin();it!=z.end ();++it)
            elist.insert(dynamic_cast<RXEntity_p >(*it) );
    }


    /* elist is now all the Layers associated with this panel
  push each element onto the panel, and onto each pside,  removing any duplication */

    for(itt=elist.begin();itt!=elist.end();++itt)	{
        last=dynamic_cast<RXEntity_p >(*itt);
        last->SetRelationOf(this,child,RXO_LAYER);

        for (int e=0;e<this->m_psidecount;e++) {
            int mside = 2*(1- this->reversed[e]);
            PSIDEPTR pss = this->m_pslist[e];
            last->SetRelationOf(pss,PS_LAYER_RELATION,RXO_LAYER,mside);
        }
    }


    if(!elist.size()) { //ie if panel->FindInMap(RXO_LAYER,RXO_ANYINDEX) returns empty
        if(this->Supply_Material("default")){	  /* 0 if a defined mat used */
            if(!l_s_def) rxerror("Some panels have default material",1);
            l_s_def = 1;
        }
    }	/* panel->mlist now has ONE entry. Push onto psides */



    if(this->m_PanelData){ // maybe from an entity or from an edit
        struct PANELDATA *pd = this->m_PanelData;
        RXEntity_p mat = pd->mat;
        RXEntity_p ref = pd->refSC;
        assert(pd->owner);
        // append pd owner atts to panel's atts

        this->AttributeAdd(pd->owner->AttributeString() );
        if(pd->owner) {
            if(pd->owner!=this) {
                pd->owner->Kill( );
            }

        }
        Create_PanelData_Entity(this, mat, ref);
        if(mat ) 				//May 2003
            mat->SetNeedsComputing();
    }



#endif
    return(1);
}

struct LINKLIST* RXPanel::Get_Panel_Ref_SC()  {
    PSIDEPTR pp;
    RXEntity_p sco;
    sc_ptr sc;
    int ns,j,k,side,pside_To_Use = -1;
    struct LINKLIST *ll = NULL;
    RXEntity_p e;
    ns = m_psidecount;

    if((e=this->Get_PanelData_Ref_SC())){ // only finds e if a previous call to
        Push_Entity(e,&ll);
        return ll;
    }

    /* first look on psides */

    for (j=0;j<ns;j++) {
        pp = m_pslist[j]; sco = pp->sc;  sc = (sc_ptr  )sco;
        side = 2*(1-  reversed[j]);
        if(pp->Mat_Angle[side].flag &4) { // OR a partial says so
            // parse out pp->Mat_Angle[side].str and push the entities onto a linklist.
            std::vector<std::string> wds = rxparsestring(pp->Mat_Angle[side].str,"+",false);
            //while( HC_Parse _String(pp->Mat_Angle[side].str,"+",k,str)){
            for(k=0;k<wds.size();k++) {
                const char*str = wds[k].c_str();
                if((e = this->Esail->GetKeyWithAlias("seamcurve,edge,curve,seam",str))){
                    Push_Entity(this,&ll);
                    pside_To_Use=j;
                }
            }
            break;
        }
    }

    // now look at the SC

    if (pside_To_Use==-1) {
        for (j=0;j<ns;j++) {
            pp = m_pslist[j]; sco = pp->sc;	sc = (sc_ptr  )sco;
            side = 2*(1-  reversed[j]);
            if(sc->Mat_Angle[side].flag &4) {
                std::vector<std::string> wds = rxparsestring(sc->Mat_Angle[side].str,"+",false);
                // while( HC_ Parse_String(sc->Mat_Angle[side].str,"+",k,str)){
                for(k=0;k<wds.size();k++) {
                    const char*str = wds[k].c_str();
                    if((e = this->Esail->GetKeyWithAlias("seamcurve,edge,curve,seam",str))){
                        Push_Entity(e,&ll);
                        pside_To_Use=j;
                    }
                }
                break;
            }
        }
    }

    return(ll);
}
int RXPanel::Dump( FILE *fp)const 
{
    Panelptr np= (Panelptr) this; // WRONG to cast away the const
    np->List(fp);
    return 1;
}



int RXPanel::Draw_Panel_Materials(const int c,VECTOR *poly) {

    ON_3dPoint l_o;
    class RXSitePt q;
    return 1; // this is WRONG for moulded sails
    // struct LINKLIST *mlist = this->m_m list;


    /*
 Polygon_Centroid(poly,c,&l_o);
 r = (float) (PC_Dist(&o,poly)/ 8.0);
    while(mlist) {
     RXEntity_p fab = (RXEntity_p )mlist->data;
     if(fab){
      class RXLayer *layer = (class RXLayer *)fab ;
      HC_Open_Segment(fab->name);
      HC_Set_Color("lines=red");
      switch (layer->matstruct.sizeofdata){
      case 3:{

       float *x = layer->matstruct.Data;
       a.x= r * x[0]; a.y= r * x[1]; a.z= r * x[2];
       PC_Vector_Op(o,a,"+", &b);
       PC_Vector_Op(o,a,"-", &d);
       HC_Insert_Line(d.x,d.y,d.z,b.x,b.y,b.z);


       }
      default:{
       q.x=o.x; q.y=o.y;q.z=o.z;
       matVec=layer->Get_Mat_Vector(q );
       a.x= r * matVec.x; a.y= r * matVec.y; a.z= r * matVec.z;
       PC_Vector_Op(o,a,"+", &b);
       PC_Vector_Op(o,a,"-", &d);
       HC_Insert_Line(d.x,d.y,d.z,b.x,b.y,b.z);
       HC_Set_Color("lines=purple");
       if(layer->matstruct.sizeofdata==0) {// a ref SC
        struct LINKLIST *ll;
//identify the curve and drop a perp from q to it. 
         ll = layer->matstruct.ll;
        while (ll) {
         RXEntity_p e = (RXEntity_p )ll->data;
         sc_ptr sc;
         double dist,s,tol;
         ON_3dPoint  p1; ON_3dVector t1;
         if(!e) continue;
         sc = (sc_ptr  )e;
         if(!sc) continue;
        //	xv will be the tangent to sc->curves[1] at the nearest point to  q
         dist=-1.0;
         s = 0.5;
         tol = sc->GetArc(1) *.01;
         if (0 < sc->C[1]->Drop_To_Curve(o,&p1,&t1, tol,&s,&dist)){
         HC_Open_Segment("perp");
         HC_Set_Color("lines=red");
         HC_Insert_Line(o.x,o.y,o.z,p1.x,p1.y,p1.z);
         HC_Close_Segment();
           }
         ll=ll->next;
         }
       }
       break;
        }
      }
      HC_Close_Segment();

      }
     mlist = mlist->next;
     }
return 1;
*/
}
ON_3dVector RXPanel::Normal()const {

    int j ,j1,j2;
    Site** ptlist;

    ON_3dVector v1,v2,v3;
    ON_3dVector l_v = ON_3dVector(0,0,0);

    if(true || this->Esail ->GetMould() )
    {
        for (j=0;j<this->m_psidecount;j++) {
            PSIDEPTR pp = this->m_pslist[j];
            Site* l_endsite;
            l_endsite = pp->Ep( 1-this->reversed[j]);
            l_v= l_v + l_endsite->Normal();//   PC_Site_Normal( p->Esail , );
        }
        l_v.Unitize();
        return l_v;
    }
    // else no mould
    ptlist = new Site*[this->m_psidecount];
    for (j=0;j<this->m_psidecount;j++) {
        PSIDEPTR pp = this->m_pslist[j];
        Site* l_endsite;
        l_endsite = pp->Ep( 1-this->reversed[j]);
        ptlist[j]= l_endsite;
    }
    for (j=0;j<this->m_psidecount;j++) {
        j1 = increment(j,this->m_psidecount );
        j2 = decrement(j,this->m_psidecount );
        v1 = ptlist[j1]->ToONPoint()   -ptlist[j]->ToONPoint() ;
        v2 = ptlist[j2]->ToONPoint() -ptlist[j]->ToONPoint() ;
        v3 = ON_CrossProduct(v1,v2);
        if(v3.Unitize())
            l_v+=v3;
    }
    if(!l_v.Unitize())
        l_v = this->Esail->Normal();
    delete[] ptlist;
    return l_v;
}

int RXPanel::Display_One_Panel(HC_KEY  seg) {
    int j,k;
    double s1,s2,s;
    VECTOR v;
    int ns;
    int side;
    // return 1; // try to see if its faster
    int TPN = (this->Esail->GetFlag(FL_ISTPN)) ;
    VECTOR *poly=NULL;
    int count=0;
    if(!this->hoopskey) {
        ns=m_psidecount;
        HC_Open_Segment(this->type());
        this->hoopskey =  HC_KOpen_Segment(this->name());
        HC_Set_User_Index(RXCLASS_ENTITY,(RXEntity_p)this);
        HC_Flush_Contents(".","subsegments");

#ifdef HOOPS
        set<RXObject*> mylist= this->FindInMap(RXO_LAYER, RXO_ANYINDEX);
#error (" CAREFUL!  The open/close may be broken here ")
        if(mylist.size()) {
            class RXLayer*layer = dynamic_cast<class RXLayer*> (*(mylist.begin()));

            RXEntity_p mat = layer->matptr;
            HC_KEY fabkey=mat->hoopskey;
            if(fabkey){
                char type[64],seg[512],color[512]; // type is a shadow
                HC_Show_Key_Type(fabkey,type);
                if(strieq(type,"segment")) {
                    HC_Show_Segment(fabkey,seg);
                    if (HC_QShow_Existence(seg,"color")){
                        HC_QShow_Color(seg,color);
                        HC_Set_Color(color);
                    }
                }
                else rxerror(" fab key not of a segment",1);
            }
#endif
            HC_Close_Segment();        HC_Close_Segment();
        }
        else
            HC_Flush_Segment (this->hoopskey);

        HC_Open_Segment_By_Key(this->hoopskey );

        /* the red-space outline */
        if(!TPN) {
            HC_Open_Segment("redspace");
            for (j=0;j<ns;j++) {
                PSIDEPTR pp = m_pslist[j];
                side = 2*(1- this->reversed[j]);
                RXCurve * theCurve = pp->sc->m_pC[side];
                ON_3dPointArray pts;
                pts.Empty();
                if (0!=side) { /* going forwards*/
                    pts = theCurve->Divide(*pp->End1off,*pp->End2off,pp->sc,side,5);
                }
                else {
                    pts = theCurve->Divide(*pp->End2off,*pp->End1off,pp->sc,side,5);
                }
                for(k=0;k<6;k++) {
                    ON_3dPoint *ppp = pts.At(k);
                    v.x=(float)ppp->x; v.y=(float)ppp->y; v.z=(float)ppp->z;
                    Append_To_Poly(&poly,&count,&v,1);
                }
                pts.Empty();
            }
            if(poly) {
                Shrink_Poly(count,poly,0.90);

                if(!Draw_Panel_Materials(count,poly)) { // bad fabric
                    if(poly) RXFREE(poly);
                    return 0;
                }

                HC_Set_Color("edges=red");
                PCP_Poly_Planar(count,poly);
                HC_KInsert_Polygon(count,&(poly->x),this->GNode());


                RXFREE(poly);
            }
            HC_Close_Segment();
        }
        else { /* a TPN */
            /* black space */
            HC_Open_Segment("blackspace");
            count=0;
            for (j=0;j<ns;j++) {
                PSIDEPTR pp = m_pslist[j];
                side = 2*(1- this->reversed[j]);

                /*lrefl = */ pp->sc->GetArc(1); // there may be a side-effect we need (yuch)
                if (0!=side) { /* going forwards*/

                    s1 = (pp->End1off->Evaluate(pp->sc,1));
                    s2 = (pp->End2off->Evaluate(pp->sc,1));
                }
                else {
                    s1 = (pp->End2off->Evaluate(pp->sc,1));
                    s2 = (pp->End1off->Evaluate(pp->sc,1));
                }
                for(k=0;k<5;k++) {
                    s = s1*(1.0-(float) k/5.0) + s2 * (float)k/5.0;

                    Get_Position_By_Double( pp->sc,s,1,&v);
                    Append_To_Poly(&poly,&count,&v,1);
                }
            }
            if(poly) {
                Shrink_Poly(count,poly, 0.9);
                HC_KInsert_Polygon(count,&(poly->x),this->GNode());
                RXFREE(poly);
            }
            HC_Close_Segment();
        }
        HC_Close_Segment();
        //       HC_Close_Segment();
        return 1;
    }


    RXEntity_p RXPanel::User_Selects_Ref_SC(){
        // Here there is no material reference.
        // get the User to select a ref SC and push it onto any nearby SC's atts
        // return the sco
        char str[256];
        RXEntity_p sco;
        sprintf(str,"Pick a Reference Curve for panel %s ", this->name());
        this->Display_One_Panel(0);
#ifdef HOOPS
        HC_Open_Segment_By_Key(this->hoopskey);
        HC_Set_Color("everything=magenta");
        HC_Set_Visibility("everything=on");
        HC_Set_Line_Pattern("- -");
        HC_Set_Line_Weight(3.0);
        HC_Set_Rendering_Options("attribute lock = (color,line pattern,line weight)");
        HC_Close_Segment();
#endif
        qDebug()<<"working on panel '" << this->name()  <<"  ' atts= ' " <<this->AttributeString() ;
        sco = UserSelection("curve,seamcurve,seam","",str,this->Esail->GetGraphic());

        if(!sco) { sco=this->m_pslist[0]->sc;	cout<<"ForWindows pick a Reference Curve at random"<<endl;}

        HC_Delete_By_Key(this->hoopskey);
        this->hoopskey=0;
        if(sco) {
            Record_SC_Edit("REF",sco);
        }
        return sco;
    }


    int RXPanel::Entirely_On_Panel(sc_ptr sc,int side){

        // returns 1 if ALL the psides of sc have pan in their leftpanel.
        int i;
        for (i=0;i<sc->npss; i++) {
            PSIDEPTR ps  =  sc->pslist[i];
            if(ps->leftpanel[side]!=this)
                return 0;
        }

        return 1;
    }

    int RXPanel::Record_SC_Edit(const char *kw,RXEntity_p matkey){
        // find a SC entirely bordering pan
        //figure out which side
        // generate an editword entity that fills in word 8 or 9
        int i,rev,done=0;//Side,
        PSIDEPTR ps;
        RXEntity_p sce;
        sc_ptr sc;
        if(!matkey) {
            this->Print_One_Entity(stdout);
            wcout <<L" Record_SC_Edit with NULL mat on panel " << this->GetOName() <<endl;
            return 0;
        }
        for(i=0;i<this->m_psidecount; i++) {
            ps = this->m_pslist[i];
            rev = this->reversed[i]; // 0 or 1;
            sce = ps->sc;
            sc = (sc_ptr  )sce;
            if(this->Entirely_On_Panel(sc, rev)) {
                done++;
            }
        }
        // the	LEAK on PanelData is because sometimes it is calloced here and sometimes we point into a PC_PANELDATA entity
        if(!this->m_PanelData)
            this->m_PanelData=(PANELDATA *)CALLOC(1,sizeof(struct PANELDATA));
        this->m_PanelData->owner = this;						// Nov 2005 chasing the leak
        if(strieq(kw,"mat")) this->m_PanelData->mat=matkey;
        else if(strieq(kw,"ref")) this->m_PanelData->refSC=matkey;
        else {
            printf("Record_SC_Edit with invalid keyword %s\n",kw);
        }
        //	}
        return done;
    }


