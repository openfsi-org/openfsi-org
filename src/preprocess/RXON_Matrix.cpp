
#include "StdAfx.h"
//#include <assert.h>
//#include "opennurbs.h"
#include "RXON_Matrix.h" 

RXON_Matrix::RXON_Matrix(void)
{
	Create(0,0);
}
/*
RXON_Matrix::RXON_Matrix(const ON_Matrix p_m ){
#ifdef linux
cout<< " gcc doesnt like this  RXON_Matrix::RXON_Matrix"<<endl;
#endif
	int nr = p_m.RowCount();
	int nc = p_m.ColCount();
	Create(nr,nc);
}*/

RXON_Matrix::RXON_Matrix(const int i, const int j){
 //this->ON_Matrix::ON_Matrix(i,j);
	Create(i,j);

}
RXON_Matrix::~RXON_Matrix(void)
{
}
/*
char * RXON_Matrix::Deserialize(char*lp){
	int nc,k;
	ON_Matrix m_m=ON_Matrix(3,3);
		k = sscanf(lp,"{ {%lf ,%lf ,%lf } ,\n{ %lf ,%lf , %lf } ,\n{%lf ,%lf ,%lf }} %n\n",
			&(m_m[0][0]),	&(m_m[0][1]),	&(m_m[0][2]),
			&(m_m[1][0]),	&(m_m[1][1]),	&(m_m[1][2]),
			&(m_m[2][0]),	&(m_m[2][1]),	&(m_m[2][2])	
		,&nc);


	if(k!=9) { //		cout<< "try UNbracketed form"<<endl; 
		k = sscanf(lp," %lf %lf %lf\n%lf %lf %lf\n%lf %lf %lf %n\n",
				&m_m[0][0],	&m_m[0][1],	&m_m[0][2],
				&m_m[1][0],	&m_m[1][1],	&m_m[1][2],
				&m_m[2][0],	&m_m[2][1],	&m_m[2][2]	
				,&nc);

		if(k!=9) {
			cout<< " cant deserialize material matrix"<<endl; 
			return lp;
		}	
	}
 	*this=RXON_Matrix(m_m);
 	lp+=nc;
return lp;
} */
char * RXON_Matrix::Deserialize(char*lp){
	int nc,k;
	Create(3,3);
		k = sscanf(lp,"{ {%lf ,%lf ,%lf } ,\n{ %lf ,%lf , %lf } ,\n{%lf ,%lf ,%lf }} %n\n",
			&(m[0][0]),	&(m[0][1]),	&(m[0][2]),
			&(m[1][0]),	&(m[1][1]),	&(m[1][2]),
			&(m[2][0]),	&(m[2][1]),	&(m[2][2])	
		,&nc);


	if(k!=9) { //		cout<< "try UNbracketed form"<<endl; 
		k = sscanf(lp," %lf %lf %lf\n%lf %lf %lf\n%lf %lf %lf %n\n",
				&m[0][0],	&m[0][1],	&m[0][2],
				&m[1][0],	&m[1][1],	&m[1][2],
				&m[2][0],	&m[2][1],	&m[2][2]	
				,&nc);

		if(k!=9) {
			cout<< " cant deserialize material matrix"<<endl; 
			return lp;
		}	
	}
 	lp+=nc;
return lp;
}

int RXON_Matrix::Print(FILE *fp)
{	int i,j,k=0;
	double** this_m =m;//  m_row.Array();
		for ( i = 0; i < RowCount(); i++ ){
		  for ( j = 0; j < ColCount(); j++ ) {
			  k+= fprintf(fp, " %lf\t", this_m[i][j]) ;
	  }
	  k+=fprintf(fp, "\n");
	}
	  return k;
}

ON_String RXON_Matrix::Serialize()
{
	ON_String s, rs; 
	int i,j;
	double** this_m = m;//m_row.Array();
		rs=ON_String("{");
		for ( i = 0; i < RowCount(); i++ ){
			rs+=ON_String("{ ");
			for ( j = 0; j < ColCount(); j++ ) {
				s.Format(" %lf ",this_m[i][j]) ;
				rs += s;
				if(j<ColCount()-1)
					rs+=ON_String(",");
			}
			rs+=ON_String("}");
			if(i<RowCount()-1)
				rs+=ON_String(" , ");
		}
		rs+=ON_String("}");
	  return rs;

}
