
/* Mods
April 00   Error return from Calc GKnotAngles


March 13, 1999   We put the non-linear (sin rather than alpha) calc-Y back in

end Feb 99.  Takes the (U,v) of the ends from the end sites. 
  the Sxy to Suv routines are in
  BUT linear interpolation across an interval isnt at all accurate.



  5.5.99  Default to model's z axis if no normal

*/



/* this is geodesic c a routine to find geodesics between two points. 
The line is defined in UV space. It is recursively subdivided (equally in UV space) until
 it is 'smooth enough'
The iteration seeks to preserve the even UV spacing and reduces the World-space geodesic angles.
The routine has trouble if an end point isnt on the surface. It has some - but less- trouble if the
end pt is in the surface but with erroneous UV values. 
This frequently happens because  The geodesic knots do not reflect the relsites.
So the relsites wouldnt be more than (saglimit) away from the surface.  Thats  OK for now

If the relsites had corresponding GKnots, (ie if this routine could achieve target S_World) we'd be sure of keeping
the relsites on the surface. 

A NOTE ON END POINTS. 
During the geodesic calc, we allow the end points to be pulled onto the surface 
Then at the end, we snap them back to the given end positions. 
This overcomes the rounding errors associated with end points being off the surface, 
or outside the bounds. 


SOME THINGS STILL WRONG in geodesic c

A) The U and V values for the end points will be approximate, because they are usually derived from
the iterative XY to UV routine. 
A fixup would be to ignore (u,v) in calcing the position of the end points. But there would still be an
rxerror in the normal at the end point because it is calculated on (u,v).   This would mean that however
many points we added next to the end point, the angle between normals would be excessive. 
We  lash that up by refusing to insert points too close to the end

B) Inserting a point at the middle in UV space is not ideal when dubydx is changing fast. So what?

C) WRONG Not verified in corners where dxbydu go to 0;

D) WRONG  The criterion to remove knots is not the formal inverse of the insertion criterion

E) In Compute_Seamcurve, we call this routine twice. Once in redspace and once in black
 As well as being slow, this may muddle Find Geod because it wont know that
 the end points are different from when it was last called.



With NURB moulds it is much smoother  than with Delauney moulds, 
This routine uses the primary definition of a curve in (u,v) space.  So NURBS ONLY

We start with a 2 end points curve, (Better, provide a heuristic for the number of starting points )
do{ 
 do{
  for all internal points{
    calc a Delta(u,v) from the geodesic angle.
   Modify the (UV) and hence derive new XYZ of the point.
   recalculate everything about any points that move.
  }
  } while dist from geodesic too big
   Traverse the curve.{
  Remove any internal nodes whose absAngle is very very small.

  For each segment,
   test the joint absAngles at both ends. and the angle between their normals

    If one or both is excessive,{  ( the normal angle test takes care of starting)

    insert a point.    It neednt be in the middle. It could be weighted
   towards the end with the greater angle.
   Geodify it immediately (locally)and
   UPDATE the geodesic and knot angles either side of it

   break out of traverse
   }



 } end traverse

} while we are still inserting points. 

*/

#include "StdAfx.h"
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXSRelSite.h"
#include "RXSeamcurve.h"

#include <time.h>
#include "entities.h"
#include "RXOffset.h"

#include "panel.h"
#include "polyline.h"
#include "vectors.h"

#include "matinvd.h"
#include "RXExpression.h"

#include "interpln.h" 
#include "nurbs.h" 

#include "global_declarations.h"

#include "geodesic.h"

#define veg carrot

int GEO_DB =0;

int Find_Geodesic(sc_ptr sc, VECTOR**p,int*c,float*chord){
    /* e1 and e2 are the start and end points.
 *p is allocateable
 *c is the number of points we end up with
 returns 2 if it moved, 1 if it didnt,  else 0 for rxerror
*/
    SAIL *sail;
    RXEntity_p mould=NULL;
    int iret=1, inserting=0,count=1000, l_err; time_t starttime ;
    struct GPoint *g, *g2=NULL,*gb;
    double  Atol = sc->Esail->m_Angle_Tol ;
    double Ltol = sc->Esail->m_Linear_Tol *16.0;/* Beware. The 16 is from the sagitta calc */
    //double minsep = Ltol;

    struct PC_INTERPOLATION *ip ;
    char str[256];
    Site *e1, *e2;


    starttime =time(0);
    sail = sc->Esail;
    if(sail)
        mould = sail->GetMould();
    if(!mould) return 0;

    if(GEO_DB)printf("\nGeodesic on %s...\n", sc->name());
    if(!sc->End1site ||!sc->End2site) return 0;
    e1 = (Site *)sc->End1site;
    e2 = (Site *)sc->End2site;

    if( sc->Geo_Or_UV ) {
        if( e1->CUrve[0] || !ON_IsValid(e1->m_u) || !ON_IsValid(e1->m_v) || e1->m_u<0 || e1->m_v<0 ) { // this to force evbyXY on relsites

            if(e1->m_u<0|| !ON_IsValid(e1->m_u))
                e1->m_u =.4;
            if(e1->m_v < 0 || !ON_IsValid(e1->m_v))
                e1->m_v =.4;
            sprintf(str," '%s' '%s' (%f %f %f )  doesnt have UV set at end 1 of %s", e1->type(), e1->name(),e1->x,e1->y,e1->z,sc->name());
            sc->OutputToClient(str,1);
            Evaluate_NURBS_By_XY(mould, (double) e1->x, (double) e1->y,&(e1->m_u),&(e1->m_v),&l_err,0);
        }
        if(e2->CUrve[0] || !ON_IsValid(e1->m_u) || !ON_IsValid(e1->m_v) || e2->m_u<0.0 ||e2->m_v < 0.0) {
            if(e2->m_u<0 || !ON_IsValid(e1->m_u))
                e2->m_u =.6;
            if(e2->m_v < 0 || !ON_IsValid(e1->m_v))
                e2->m_v =.6;

            sprintf(str," '%s' '%s'  (%f %f %f ) doesnt have UV set at end 2 of %s", e2->type(), e2->name(),e2->x,e2->y, e2->z, sc->name());
            sc->OutputToClient(str,1);

            Evaluate_NURBS_By_XY(mould, (double) e2->x, (double) e2->y,&(e2->m_u),&(e2->m_v),&l_err,0);
        }
        if(!sc->Geo_Or_UV) return 0;
    }

    *p=(VECTOR *)REALLOC(*p, 2*sizeof(VECTOR));
    // (*p)[0] = e1->To VectorType();
    // (*p)[1] = e2->To VectorType();
    (*p)[0].x=e1->x;	(*p)[0].y=e1->y;	(*p)[0].z=e1->z;
    (*p)[1].x=e2->x;	(*p)[1].y=e2->y;	(*p)[1].z=e2->z;
    *c=2;
    *chord = PC_Dist(&((*p)[0]),&((*p)[1]));

    if(!sc->Geo_Or_UV) return 2;   /* only get here if its NOT a geo */


    ip = (PC_INTERPOLATION *)mould->dataptr;
    if(!ip || !ip->NurbFlag) {
        sc->Geo_Or_UV=0; sc->m_scMoulded=1;
        printf(" Mould isnt a NURBS so Changing %s from geodesic to moulded\n",sc->name());
        return 2;
    }


    HC_Open_Segment("geodesic"); HC_Flush_Contents(".","everything");
    Delete_Geodesic_List(sc->GeodesicList);sc->GeodesicList=NULL;//July 2001
    if(!sc->GeodesicList) {
        if(! InitGlist(sc,e1,e2,mould) ) {
            HC_Close_Segment(); return 0;
        }
    }
    else GlistFirstAndLast(sc, e1,e2,mould);

    do{
        inserting=0;
        //	if(time(0) > starttime+300) { cout<< "timed out\n"<<endl; count=0; continue;}

        if(GEO_DB) printf("\n*****************\n FindGeodesic LOOP %d on %s\n",count, sc->name());
        if(sc->Geodesic )  iret = (1+ GeodifyList(sc,mould)) || iret;
        else Get_Normalised_Geodesic_S(sc);
        for(g =sc->GeodesicList->next; g; g=g->next) {
            if(g->end) continue;
            if(!g->next) continue;
            gb = g->before;
            if(Geo_Test_Remove(g)) {
                if(GEO_DB) printf(" remove knot at %f ...", g->t);
                inserting=1;
                Remove_Gknot(&g,mould); /* g is now g before */
                sc->SetNeedsComputing();
            }
        }

        for(g =sc->GeodesicList; g; g=g->next) {
            if(GEO_DB) printf("test %f.. ",g->t);
            if(Geo_Test_Insert(g)  ) {
                if(GEO_DB)printf(" Insert_Gknot_After t=%f atoNN=%f(%f)  AbsA=%f(%f),  sag=%f ref%f \n", g->t, g->AngleToNextNormal, Atol, g->AbsAngle,Atol ,(g->AbsAngle + g->next->AbsAngle) * g->SegLen,Ltol );
                Insert_Gknot_After(&g,mould);
                if(sc->Geodesic ) GeodifyKnot(g,mould); /*  updates gangles, here, before and after */
                iret=2;
                inserting=1;
                sc->SetNeedsComputing();
            } else if(GEO_DB) cout<< " No insert\n"<<endl;
        }

    } while(inserting && ((count--) > 0) );


    g = sc->GeodesicList;
    if(fabs(e1->z- g->z) > 0.0001)  if(GEO_DB)  printf(" Snap e1 leaving Find Geo by %f\n", e1->z-g->z);
    g->x=e1->x; 	g->y=e1->y; 	g->z=e1->z;

    for(g=sc->GeodesicList;g; g=g->next)  g2=g;
    if(fabs(e2->z- g2->z) > 0.0001)   if(GEO_DB) printf(" Snap e2 leaving Find Geo by %f\n", e2->z-g2->z);
    g2->x=e2->x;	g2->y=e2->y;	g2->z=	e2->z;

    Get_Normalised_Geodesic_S(sc); /* I don't know if this call is needed but its safe */

    CopyGlistToPline(sc,p,c);

    /* 	HC_Insert_Polyline(*c,*p);
 HC_Set_Color(" lines=green"); */
    HC_Close_Segment();
    if(GEO_DB) Print_GList(sc->GeodesicList,stdout);
    cout<< " a fix-up because Sort_IA in make_fanned needs the arc length 0 WRONG "<<endl;
    sc->m_arcs[0] = PC_Polyline_Length(*c,*p);

    return(iret);
}
int Geo_Test_Remove( struct GPoint *g) {
    /* return 1 to remove the gpoint */
    double OC,  Atol = g->m_e->Esail ->m_Angle_Tol ;
    double Ltol =  g->m_e->Esail ->m_Linear_Tol*16.0;/* Beware. The 16 is from the sagitta calc */
    //double minsep = Ltol;
    if(!g) return 0;
    if(!(g->before) ) return 0;
    if (((g->AngleToNextNormal + g->before->AngleToNextNormal)< Atol/40.) &&(( g->AbsAngle + g->before->AbsAngle) < Atol/40.0) ) { /* a candidate */
        OC =  g->AbsAngle * g->SegLen * g->before->SegLen/ (  g->SegLen + g->before->SegLen);
        if(GEO_DB) printf("  OC=%f\n",OC);
        if(OC < Ltol/40.0) return 1;
    }
    return 0;
}
int Geo_Test_Insert(struct GPoint *g ) {
    /* return 1 to insert a  gpoint just after */
    double Atol =  g->m_e->Esail ->m_Angle_Tol ;
    double Ltol =  g->m_e->Esail ->m_Linear_Tol*16.0;/* Beware. The 16 is from the sagitta calc */


    double minsep = Ltol;
    if(!g) return 0;
    if(!g->next) return 0;
    if( g->SegLen  < minsep)  return 0;
    if (g->AngleToNextNormal> Atol) {  if(GEO_DB)  printf(" Insert because ofAngleToNextNormal =%f\n", g->AngleToNextNormal);  return 1;}
    if( g->AbsAngle > Atol ) {   if(GEO_DB) printf(" Insert because g->AbsAngle =%f Atol  \n", g->AbsAngle) ; return 1; }
    if( (g->AbsAngle + g->next->AbsAngle) * g->SegLen >Ltol) {
        if(GEO_DB) printf(" IGNORE saggitta est gave %f / 16  cf %f / 16\n", (g->AbsAngle + g->next->AbsAngle) * g->SegLen, Ltol);
        return 0;
    }
    return 0;
}


int  Print_Gknot(struct GPoint *g, FILE *fp) {
    fprintf(fp,"   Knot  %p\n", g);
    fprintf(fp,"      u          v      (x         y         z)  GAngle  AbsAngle  Seglen  GeodesicY    t     AToNextN\n");
    fprintf(fp," %f %f(%f %f %f) %f %f  %f %f %f  %f\n",g->u,g->v,g->x,g->y,g->z, g->GAngle, g->AbsAngle,g->SegLen, g->GeodesicY, g->t, g->AngleToNextNormal);
    fprintf(fp," SegVec		(%f %f %f)\n", g->SegVec.x, g->SegVec.y,g->SegVec.z);
    fprintf(fp," du			(%f %f %f)\n", g->du.x, g->du.y,g->du.z);
    fprintf(fp," dv	    	(%f %f %f)\n", g->dv.x, g->dv.y,g->dv.z);
    fprintf(fp," normal		(%f %f %f)\n", g->normal.x, g->normal.y,g->normal.z);
    fprintf(fp," endflag	%d  g %p before %p next %p\n\n", g->end, g, g->before, g->next);
    fprintf(fp," entity		%s %s\n", g->m_e->type(),g->m_e->name());

    return 1;
}
int  Print_GList(struct GPoint *head, FILE *fp){
    struct GPoint *g;
    if (!head) return 0;
    fprintf(fp,"    Geodesic data\n   u     v    (    x       y       z  )  (      normal       )    t    S_uv    GAngle  AbsAng  Seglen  Sworld GeodesY AToNextN	entity\n");

    for(g =head; g ; g=g->next) {
        fprintf(fp," %5.3f %5.3f  (%7.4f %7.4f %7.4f)  (%5.3f %5.3f %5.3f)" ,g->u,g->v,g->x,g->y,g->z, g->normal.x, g->normal.y,g->normal.z);
        fprintf(fp," %6.4f %6.4f  %7.4f  %6.4f  %6.4f  %7.4f  %7.4f  %6.4f", g->t, g->Suv,  g->GAngle, g->AbsAngle,g->SegLen, g->Sw, g->GeodesicY, g->AngleToNextNormal);
        fprintf(fp,"\t%s,%s\n", g->m_e->type(),g->m_e->name());
    }
    if(GEO_DB) printf(" arc length is %f\n", Geodesic_Arc(head));
    return 1;
}


/*****************/
int Calc_GKnotAngles(struct GPoint *g,RXEntity_p mould){
    /* get all its angles and lengths that depend on its neighbours too
  GIVEN coords and normal of itself and its neighbours*/
    ON_3dVector vb;
    struct GPoint *g2 = g->next, *gb = g->before;
    int err=0;

    if(g2) {
        g->SegVec.x = g2->x-g->x;
        g->SegVec.y = g2->y-g->y;
        g->SegVec.z = g2->z-g->z;
        g->SegLen = sqrt(g->SegVec.x*g->SegVec.x + g->SegVec.y*g->SegVec.y + g->SegVec.z*g->SegVec.z);
        g->UVLen = sqrt( (g2->u-g->u) * (g2->u-g->u)  +  (g2->v-g->v) * (g2->v- g->v) );

        if(g->SegLen  < 0.0001 || g->UVLen < 0.000001) {
            printf("Calc_GKnotAngles g->SegLen =%f g->UVLen=%f   \n", g->SegLen , g->UVLen);
            Print_Gknot(g, stdout) ;
            mould->OutputToClient("short seglen in Calc_GKnotAngles",1);
            g->AngleToNextNormal =0.0;
            g->AbsAngle =0.0;
            g->GAngle = 0.0;
            return 0;
        }
        g->AngleToNextNormal = PC_True_Angle(g->normal,g2->normal,&err);

        if(gb) {
            vb.x = g->x-gb->x;
            vb.y = g->y-gb->y;
            vb.z = g->z-gb->z;

            g->AbsAngle =  PC_True_Angle(g->SegVec,vb,&err);
            g->GAngle = PC_Apparent_Angle(g->SegVec, vb, g->normal);
        }
        else { /* no gb */
            g->GAngle = 0.0;
            g->AbsAngle = 0.0;
        }
    }
    else {/* no g2 */
        g->SegLen =0.;
        g->AngleToNextNormal=0.0;
        g->GAngle = 0.0;
        g->AbsAngle = 0.0;
    }
    assert(!err); // need to step this.
    return (!err);
}


int Get_Normalised_Geodesic_S(sc_ptr sc){
    /* Sum Y's according to Gangle, t  into the structure element 'spare' */
    struct GPoint *g, *h=NULL;
    double arc;
    sc->GeodesicList->Sw = 0.0;
    sc->GeodesicList->Suv = 0.0;
    for(g =sc->GeodesicList->next; g; g=g->next) {
        g->Sw = g->before->Sw + g->before->SegLen;
        g->Suv = g->before->Suv +  g->before->UVLen;
        h=g;
    }
    arc = h->Sw;
    for(g =sc->GeodesicList->next; g; g=g->next) {
        g->Sw = g->Sw/arc;
        if(h->Suv>0.) g->Suv = g->Suv /h->Suv ;
    }




    return 1;
}
int Calculate_Y(sc_ptr sc,RXEntity_p mould){
    /* Sum Y's according to Gangle, t  into the structure element 'spare' */
    struct GPoint *g, *h;
    double Arc, a1,sumY=0.0, biggestA=0.0;

    Get_Normalised_Geodesic_S(sc);		/* non-dimensionalises Sw */

    Arc = Geodesic_Arc(sc->GeodesicList); 	/* in length units */
    for(g =sc->GeodesicList; g; g=g->next)  {
        g->GeodesicY=0.0;
        if(g->GAngle >   0.25) g->GAngle =   0.25;
        else if(g->GAngle < -0.25) g->GAngle = -0.25;

        /* 	g->spare = g->GAngle *  g->Sw * (1.- g->Sw ) *  Arc;  */ /* approximates sin(a)/a->1*/

        a1 = atan2((1.-g->Sw )*sin(g->GAngle), ( g->Sw  + (1.- g->Sw )*cos(g->GAngle)  )); /* assumes sw is reasonable hypotenuse */
        g->spare = g->Sw  * sin(a1) * Arc;

        sumY += 	g->spare;
        if(fabs(biggestA) <fabs(g->GAngle))  biggestA = g->GAngle;
    }

    if(fabs(biggestA ) < sc->Esail->m_Angle_Tol && fabs( sumY) < sc->Esail->m_Linear_Tol ) return 0;
    /* Y (spare) is now the height of this knot if it were the only one  */

    for(g =sc->GeodesicList; g; g=g->next) {
        if(g->end) continue;

        for(h =sc->GeodesicList; h!=g; h=h->next)
            h->GeodesicY += g->spare * h->Sw/ g->Sw ;
        for(h =g; h; h=h->next)
            h->GeodesicY += g->spare *(1.- h->Sw)/ (1.- g->Sw );

    }
    return 1;
}

int  GeodifyList (sc_ptr sc,RXEntity_p mould){
    /* Should move the curve in UV space if any geodesic angles non-zero
    Doesnt calc geodesic angles at the beginning, but recalcs them after a move
     tot up the desired movements 'GeodesicY'  (aka OC) in each knot.
      move its u and v by OC*Ufac, OC*Vfac
 Then recalc its coords and angles
 Consider  tuning Ufac, Vfac according to distance travelled.
 Strangely, resetting Ufac Vfac each loop
 (ie CalcGeodesicScale called from Calc_Gknot_Position)
 made it unstable
*/
    struct GPoint *g;
    double tol = 0.01, biggestY=0.0, biggestA=0.0, biggestL=0.0, relfac =0.5;
    int count, c2=5;
    int NotOK;
    double  l_Atol = sc->Esail->m_Angle_Tol ;

    do {
        count=50;
        do {
            biggestL = biggestY = biggestA=0.0;
            if(!Calculate_Y(sc,mould)) { /* converged OK */
                if(count==50 ) {
                    if(GEO_DB) cout<< " already OK\n"<<endl;
                    return 1;
                }
            }

            for(g =sc->GeodesicList; g; g=g->next) {
                if(g->end) continue;
                if(fabs(biggestY) <fabs(g->GeodesicY))  biggestY = g->GeodesicY;
                if(fabs(biggestA) <fabs(g->GAngle))  biggestA = g->GAngle;
                if(fabs(biggestL) <fabs(g->spare))  biggestL = g->spare;
                g->u += g->GeodesicY * g->UFac*relfac;
                g->v += g->GeodesicY *g->VFac*relfac;

                /*
This is nice, but it causes instability on some curves, especially when the
end point isnt on the surface 
   g->u += (g->t -g->Suv) * g->ChordU.x*relfac;
   g->v += (g->t -g->Suv) * g->ChordU.y*relfac;*/

                if(g->u>1.0) g->u=1.0;
                else if (g->u<0.0) g->u=0.0;
                if(g->v>1.0) g->v=1.0;
                else if (g->v<0.0) g->v=0.0;
                Calc_Gknot_Position(g,mould);
            }
            for(g =sc->GeodesicList; g; g=g->next)
                if(!Calc_GKnotAngles(g,mould)) {
                    if(g == sc->GeodesicList) g=g->next;
                    printf(" Calc_GKnotAngles failed .Remove inGeodifyList, knot at %f\n", g->t);
                    Print_Gknot(g,stdout);
                    Remove_Gknot(&g,mould);
                }
            NotOK = (fabs(biggestY) > tol || fabs(biggestA) > l_Atol ) ;

        }while ( NotOK && count--);
        relfac=relfac/2.0; if(NotOK) { printf(" relfac now %f in %s\n", relfac, sc->name());  HC_Update_Display();}
    }while ( NotOK && c2--);
    if(NotOK) {Print_GList(sc->GeodesicList,stdout); }
    if(GEO_DB) printf(" End GeodifyList after %d\n",count);
    return (count > 0);
} 
int VerifyScale(struct GPoint *g, double zfac){
    /* The world vector corresponding to the UV space chord vector
  is [du,dv,normal] * ChordU
The world vector corresponding to the UV space perp vector
  is [du,dv,normal] *[g->UFac,  g->VFac, zfac]

This routine test whether they are perpendicular */

    float dot,l1,l2;
    VECTOR R,P;

    R.x = g->du.x*g->ChordU.x +  g->dv.x*g->ChordU.y + g->normal.x*g->ChordU.z;
    R.y = g->du.y*g->ChordU.x +  g->dv.y*g->ChordU.y + g->normal.y*g->ChordU.z;
    R.z = g->du.z*g->ChordU.x +  g->dv.z*g->ChordU.y + g->normal.z *g->ChordU.z;

    P.x = g->du.x*g->UFac +  g->dv.x*g->VFac +  g->normal.x*zfac;
    P.y = g->du.y*g->UFac +  g->dv.y*g->VFac + g->normal.y*zfac;
    P.z = g->du.z*g->UFac +   g->dv.z*g->VFac + g->normal.z *zfac;

    dot = P.x*R.x + P.y*R.y + P.z*R.z;
    l1 =   P.x*P.x + P.y*P.y + P.z*P.z;
    l2   = R.x*R.x + R.y*R.y + R.z*R.z;

    if(fabs(dot) < 10E-10) { cout<< " VerifyScale OK"<<endl; /* return 1;*/}
    printf(" Uchord vector %f %f %f\n", g->ChordU.x, g->ChordU.y, g->ChordU.z);
    printf(" Ufac Vfac Zfac %f %f %f\n", g->UFac, g->VFac, zfac);
    printf(" World chord (%f %f %f)", R.x,R.y,R.z);
    printf(" Perp (%f %f %f)", P.x,P.y,P.z);
    printf(" DOT=%f (sb 0)\n", dot);
    return 0;
}
int printmatrix(const char*s, double L[3][3]) {
    printf("%s\n",s);
    printf(" %f \t%f \t%f\n",L[0][0] ,L[0][1], L[0][2]);
    printf(" %f \t%f \t%f\n",L[1][0] ,L[1][1], L[1][2]);
    printf(" %f \t%f \t%f\n",L[2][0] ,L[2][1], L[2][2]);
    return 1;
}
int CalcGeodesicScale(struct GPoint *g){
    double zfac;
    int isol, idsol;double det,scale;
    int DB=0;
    double L[3][3], Pn[3][3], Ldash[3][3],spare[3][3],T[3][3];
    VECTOR R;

    L[0][0]= g->du.x;    L[0][1]= g->dv.x;  	L[0][2]= g->normal.x;
    L[1][0]= g->du.y;	L[1][1]=  g->dv.y; 	L[1][2]= g->normal.y;
    L[2][0]= g->du.z ;	L[2][1]=  g->dv.z;	L[2][2]= g->normal.z;

    if(DB) printmatrix("L",L);
    memcpy(Ldash,L, 9*sizeof(double));
    matinvd(&isol,&idsol, 3, 3,(double*) Ldash,3,&det);//linux liked cast
    if(DB) printmatrix("Ldash",Ldash);

    PC_Compute_Perp_Transform(&(g->normal), Pn);
    if(DB) printmatrix("perp matrix",Pn);
    PC_Compute_3Matrix_Product(Pn,L,spare);

    PC_Compute_3Matrix_Product(Ldash,spare,T);
    if(DB)printmatrix("Ldash * Pn x L", T );
    g->UFac = T[0][0]*g->ChordU.x + T[0][1]*g->ChordU.y; /* ChordU.z is zero */
    g->VFac = T[1][0]*g->ChordU.x + T[1][1]*g->ChordU.y;
    zfac =        T[2][0]*g->ChordU.x + T[2][1]*g->ChordU.y;

    R.x = g->du.x* g->UFac + g->dv.x*g->VFac + g->normal.x*zfac ;
    R.y = g->du.y*g->UFac  + g->dv.y*g->VFac + g->normal.y*zfac ;
    R.z =  g->du.z*g->UFac  + g->dv.z*g->VFac  + g->normal.z*zfac;

    scale = sqrt(R.x*R.x + R.y*R.y + R.z*R.z);
    if(scale < 0.00001) scale=1.0;

    g->UFac =g->UFac/scale ; g->VFac =g->VFac/scale ; zfac =zfac/scale ;

    if(DB)	VerifyScale(g,zfac);
    return 1;
};

int  GeodifyKnot (struct GPoint *g,RXEntity_p mould){
    /* moves g so that it is on the geodesic between before and after
  updates the angles of itself, and of knots before and after */
    struct GPoint *gb, *gn;
    double OC;
    int iret;
#ifdef _GEO_DBAGAIN
    VECTOR old;
#endif
    gb = g->before; gn = g->next;
    if(!gb || !gn) return 0;
    CalcGeodesicScale(g);
    /*  lateral dist to move in world coords */
#ifdef _GEO_DBAGAIN
    old.x = g->x; old.y=g->y; old.z=g->z;
    if(GEO_DB) printf(" geodify one t=%f  GAngle=%f segbef=%f seg=%f uv=(%f %f)...", g->t, g->GAngle,gb->SegLen, g->SegLen , g->u ,g->v  );
    if(GEO_DB) printf("old %f %f %f\n",g->x,g->y,g->z);
#endif
    OC =   g->GAngle * g->SegLen * gb->SegLen/ (  g->SegLen + gb->SegLen);
    if(GEO_DB) printf("OC = %f\n", OC);

    g->u += OC *g->UFac;
    g->v += OC *g->VFac;
    if(g->u>1.0) g->u=1.0;
    else if (g->u<0.0) g->u=0.0;
    if(g->v>1.0) g->v=1.0;
    else if (g->v<0.0) g->v=0.0;

    Calc_Gknot_Position(g,mould);
    iret = 1;
    if(! Calc_GKnotAngles(gb,mould))   iret =0;
    if(! Calc_GKnotAngles(g,mould))   iret =0;
    if(! Calc_GKnotAngles(gn,mould))   iret =0;
    if(GEO_DB) printf(" after      t=%f  GAngle=%f segbef=%f seg=%f uv=(%f %f) ", g->t, g->GAngle,gb->SegLen, g->SegLen , g->u ,g->v  );
    if(GEO_DB) printf("new %f %f %f\n",g->x,g->y,g->z);
    if(!iret)     mould->OutputToClient( "GeodifyKnot BUG",2);
    return iret;
}

int Remove_Gknot(struct GPoint **g,RXEntity_p mould){
    struct GPoint *gb, *gn;
    gb = (*g)->before; gn = (*g)->next;
    RXFREE(*g);
    gb->next=gn;  gn->before=gb;
    if(!Calc_GKnotAngles(gb,mould))     mould->OutputToClient(" Remove_Gknot bug 1",2);
    if(!Calc_GKnotAngles(gn,mould))   mould->OutputToClient(" Remove_Gknot bug 2",2);
    *g = gb;
    return 1;
}

int Calc_Gknot_Position(struct GPoint *g,RXEntity_p mould){
    /* given u and v, fills in x,y,z, normal */
    ON_3dVector l_z; ON_3dPoint q, r;
    int flag;
    if(!g->end) {
        Evaluate_NURBS(mould, g->u, g->v,&q);
        g->x=q.x; g->y=q.y; g->z=q.z;
    } else if(GEO_DB) printf(" skip EvNu on t=%f \n", g->t);
    flag = PCN_Evaluate_NURBS_Tangents(mould,g->u,g->v,&r,&g->du,&g->dv);
    /*	if(flag !=3) { char str[64]; sprintf(str,"Calc_Gknot_Position tangent result=%d at (%f %f)", flag, g->u,g->v); rxerror(str,1); }
*/	

    g->normal =ON_CrossProduct(g->dv,g->du);
    if(!g->normal.Unitize ()   ) {
        l_z =mould->Normal();
        g->normal.x = l_z.x; g->normal.y = l_z.y ;   g->normal.z = l_z.z;
    }

    /* 	CalcGeodesicScale(g); */
    return 1;
}



int Insert_Gknot_After(struct GPoint **gb,RXEntity_p mould){
    struct GPoint *gn;
    struct GPoint *g = (GPoint *)CALLOC(1, sizeof(struct GPoint));

    g->m_e=(*gb)->m_e;

    gn = (*gb)->next;
    (*gb)->next=gn ->before = g;
    g->before=(*gb); g->next=gn;
    /* find its coords, get normal, etc */

    g->u = (gn->u + (*gb)->u)/2.0;
    g->v = (gn->v + (*gb)->v)/2.0;
    g->t = (gn->t + (*gb)->t)/2.0;
    g->ChordU.x  = (*gb)->ChordU.x ;
    g->ChordU.y  = (*gb)->ChordU.y;
    g->ChordU.z  = (*gb)->ChordU.z ;
    if(GEO_DB) printf(" insert t=%f Uchord= %f Vchord=%f\n", g->t , g->ChordU.x , g->ChordU.y);

    Calc_Gknot_Position(g,mould);

    if(!Calc_GKnotAngles(*gb,mould))   mould->OutputToClient("  Insert_Gknot_After bug 1",1);
    if(!Calc_GKnotAngles(gn,mould) )   mould->OutputToClient("  Insert_Gknot_After bug 2",1);
    if(!Calc_GKnotAngles(g,mould))   mould->OutputToClient("  Insert_Gknot_After bug 3",1);

    (*gb)=g;
    return 1;
}

int GlistFirstAndLast(sc_ptr sc,Site *e1, Site *e2,RXEntity_p mould) {
    struct GPoint *g2=NULL, *g = sc->GeodesicList; //gcc

    g->x=e1->x; 	g->y=e1->y; 	g->z=e1->z;
    /* 	g->z = Evaluate_ NURBS _By_ XY(mould, (double)e1->x, (double)e1->y,&g->u,&g->v);  */
    g->u= e1->m_u ; g->v= e1->m_v;

    for(g=sc->GeodesicList;g; g=g->next)  g2=g;

    /* 	g2->z= Evaluate_ NURBS_ By _XY(mould, (double)e2->x, (double)e2->y,&g2->u,&g2->v);  */
    g2->x=e2->x;	g2->y=e2->y; g2->z= e2->z;
    g2->u=e2->m_u;	g2->v=e2->m_v;
    // July 2001 added this to cope with ends having moved.
    for(g =sc->GeodesicList; g; g=g->next)
        if(!Calc_GKnotAngles(g,mould)) {
            if(g == sc->GeodesicList) g=g->next;
            printf(" Calc_GKnotAngles failed .Remove inGeodifyList, knot at %f\n", g->t);
            Print_Gknot(g,stdout);
            Remove_Gknot(&g,mould);
        }
    return 1;
}

int  InitGlist(sc_ptr sc,Site *e1, Site*e2,RXEntity_p mould){
    /* make a linklist of the first and last points
and fills in all their values */

    struct GPoint *g1, *g2 ;
    int iret=1;

    g1 = (GPoint *)CALLOC(1, sizeof(struct GPoint)); g1->m_e=sc ;
    g2 = (GPoint *)CALLOC(1, sizeof(struct GPoint)); g2->m_e=sc ;

    /* 	g1->z = Evaluate_  NURBS_By_  XY(mould, (double)e1->x, (double)e1->y,&g1->u,&g1->v);  */
    g1->x=e1->x; g1->y=e1->y; 	g1->z = e1->z;
    g1->u= e1->m_u ; g1->v= e1->m_v;
    g1->end=1;

    /* 	g2->z = Evaluate  _NURBS_  By_  XY(mould, (double)e2->x, (double)e2->y,&g2->u,&g2->v); */
    g2->x=e2->x;	g2->y=e2->y;	g2->z =e2->z;
    g2->u=e2->m_u;	g2->v=e2->m_v;
    g2->end=1;
    g2->t=1.0;

    g1->next=g2;
    g2->before=g1;
    sc->GeodesicList=g1;

    g2->ChordU.x = g1->ChordU.x =  g2->u - g1->u;
    g2->ChordU.y = g1->ChordU.y =  g2->v - g1->v;

    Calc_Gknot_Position(g1,mould);
    Calc_Gknot_Position(g2,mould);
    if(!Calc_GKnotAngles(g1,mould)){    mould->OutputToClient("  InitGlist bug 1",2); iret=0;}
    if(!Calc_GKnotAngles(g2,mould)) {   mould->OutputToClient("  InitGlist bug 1",2); iret=0;}

    /* 	g2->ChordU.x = g1->ChordU.x =  g2->u - g1->u;
 g2->ChordU.y = g1->ChordU.y =  g2->v - g1->v; */

    return iret;
}
int CopyGlistToPline(sc_ptr sc,VECTOR**p,int*c){
    struct GPoint *g ;
    VECTOR*v;
    *c=0;
    for(g =sc->GeodesicList; g; g=g->next, (*c)++) {
        *p = (VECTOR *)REALLOC(*p, ((*c)+1)*sizeof(VECTOR));
        v = &((*p)[(*c)]);
        v->x=(float) g->x; v->y=(float)g->y; v->z=(float)g->z;
    }
    return 1;
}
double Geodesic_Arc(struct GPoint *head) {
    double arc = 0.0;
    struct GPoint *g;

    for(g =head; g ; g=g->next)
        arc += g->SegLen;


    return arc;
}
double Suv_from_Sxy( sc_ptr sc, double sxy, int*flag){
    /* Sxy  is an offset along the seam in world coords.
      we want to find the equivalent in UV units.
  Lets try linear interpolation
 g->Suv is in uv space (0-1)  g->Sw is world space ( but scaled to (0-1)*/

    struct GPoint *head = sc->GeodesicList;
    struct GPoint *g, *gl=NULL;
    double f,d,arc;
    int found=0;
    *flag=0;
    if (!head) return 0;
    arc = Geodesic_Arc(head);
    sxy = sxy/ arc;

    for(g =head; g ; g=g->next) {
        if(g->Sw >= sxy) {
            gl = g->before;
            found=1;
            break;
        }
    }
    if(!found) {
        printf(" Suv_from_Sxy on %s not found sxy=%f\n", sc->name(), sxy);
        sc->OutputToClient(" Suv_from_Sxy not found",2);
        return 0.5;}
    if(!found) return 0.5;
    if(!gl) { *flag=1; return 0.0;} /* begin ning of list */
    d = g->Sw-gl->Sw;
    if(d<=0.0) return 0.6;
    *flag=1;
    f = (sxy - gl->Sw)/ d;
    return (g->Suv * f + gl->Suv *(1.-f));
}

double Sxy_from_Suv( sc_ptr sc, double suv, int*flag) {
    /* Sxy  is an offset along the seam in world coords.
      we want to find the equivalent in UV units.
  Lets try linear interpolation
 g->Suv is uv space S g->Sw is world space S*/

    struct GPoint *head = sc->GeodesicList;
    struct GPoint *g, *gl=NULL;
    double f,d;
    int found=0;
    *flag=0;
    if (!head) return 0;

    for(g =head; g ; g=g->next) {
        if(g->Suv >= suv) {
            gl = g->before;
            found=1;
            break;
        }
    }
    if(!found) {
        printf(" in Sxy_from_Suv with suv = %f not found\n", suv);
        sc->OutputToClient("Sxy_from_Suv not Found",2);
        return 0.5;
    }
    if(!gl) { *flag=0; return 0.0;} /* start of list */
    d = g->Suv-gl->Suv;
    if(d<=0.0) return 0.6;
    *flag=1;
    f = (suv - gl->Suv)/ d;
    return ( (g->Sw * f + gl->Sw *(1.-f)) * Geodesic_Arc(head) ) ;
}

int UV_from_Sxy( sc_ptr sc, double sxy, double*u, double*v) {
    /* Sxy  is an offset along the seam in world coords.
      we want to find the equivalent in UV units.
  Lets try linear interpolation
 g->Suv is uv space (0-1)  g->Sw is world space  ( scaled 0-1)
 This routine must FAIL if there is any chance that the SC doent lie along its geodesic.


 */

    struct GPoint *head = sc->GeodesicList;
    struct GPoint *g, *gl=NULL, *p=NULL;
    double f,d;
    int found=0;

    if (!head) {
        if(GEO_DB) printf(" UV_from_Sx no head in %s  \n", sc->name());
        return 0;
    }
    double depth  = sc->FindExpression(L"depth",rxexpLocal)->evaluate();
    if(fabs(depth) > 0.00) //  July 2001  This technique fails on non-geodesics
        return 0;




    if(GEO_DB) printf(" UV from Sxy sxy= %f arc %f \n", sxy, Geodesic_Arc(head));
    sxy = sxy/ Geodesic_Arc(head);

    for(g =head; g ; g=g->next) {

        if(g->Sw >= sxy) {
            gl = g->before;
            found=1;
            break;
        }
        p=g;
    }
    if(!gl) {
        if(!found) {
            /* 	printf("found=0  probably off end of list Sxy=%f p->Sw=%f\n",  sxy, p->Sw );   */
            *u=p->u; *v=p->v;
        }
        else {
            /* 	printf("found=1  probably beginning of list Sxy=%f g->Sw=%f\n",  sxy, g->Sw );   */
            *u=g->u; *v=g->v;
        }
        return 2;
    }
    if(GEO_DB)  printf(" found  g->Sw =%f and Gbefore->Sw=%f  \n", g->Sw,gl->Sw) ;
    d = g->Sw-gl->Sw ;
    if(d<=0.0)  { printf(" in  UV from Sxy  d=%f\n", d); return 0;  }

    f = (sxy - gl->Sw)/ d;
    if(GEO_DB) printf(" giving d=%f ,  f=%f ",d,f);
    *u =  g->u * f + gl->u *(1.-f);
    *v =  g->v * f + gl->v *(1.-f);
    return  1;
}

int Place_On_Geo(sc_ptr sc,double Sxy, double*x,double*y,double*z,double*u,double*v){
    ON_3dPoint q;

    SAIL *sail;
    RXEntity_p mould=NULL;
    if(GEO_DB)	printf(" PlaceonGeo '%s' Sxy=%f metres\n", sc->name(), Sxy);
    sail = sc->Esail;
    if(sail)
        mould = sail->GetMould();
    if(!mould)
        return 0;
    if( !UV_from_Sxy(sc, Sxy, u, v) ) {   sc->OutputToClient("UV from Sxy failed in Place_On_Geo",2);  return 0;  }

    Evaluate_NURBS(mould, *u, *v,&q);
    *x=q.x; *y=q.y; *z=q.z;
    if(GEO_DB) printf(" Place_On_Geo with Sxy=%f gave (%f %f) (%f %f %f)\n", Sxy, *u,*v,*x,*y,*z);

    return 1;
}


int Create_Displaced_Polyline(sc_ptr sc,RXEntity_p mould,int which,int c,double*x,double*y,double*z, VECTOR *base){

    /* create a polyline (in sc->p[which], sc->c[which])
consisting of the geodesic knots (normalised spacing Sworld) moved in directions Y and Z according to linear interpolation
from the C points (x,y,z) */ 

    /* prelim. If c<2 or y and Z all small, just copy the knots into the pline and return */

    //  A MORE ACCURATE METHOD WOULD BE TO WORK IN UV SPACE. using UFac, etc


    struct GPoint *g ;
    VECTOR*v, **p, vbase ;
    ON_3dVector Yaxis;
    int cg;
    double basechord = base[c-1].x;
    p = &(sc->p[which] );

    cg=0;
    for(g =sc->GeodesicList; g; g=g->next, cg++) {
        *p = (VECTOR *)REALLOC(*p, (cg+1)*sizeof(VECTOR));
        v = &((*p)[(cg)]);

        // as basecurve is normalised in X, we can interp to it with g->Sw (also normalised)
        assert (g->Sw <=1.0);
        Place_On_Poly_By_X((float)(g->Sw*(float)basechord),base,c,&vbase);


        // vbase already scaled in Y,Z according to sc's scaling rules.


        // get the surface Y axis  - approx segvec cross normal. NOT EXACT but close, if gpts are a geo

        Yaxis=ON_CrossProduct(g->normal,g->SegVec);  Yaxis.Unitize ();

        if(which==2||(!sc->seam)) {
            v->x = (float)g->x + vbase.y*Yaxis.x + vbase.z * g->normal.x;
            v->y = (float)g->y + vbase.y*Yaxis.y + vbase.z * g->normal.y;
            v->z = (float)g->z + vbase.y*Yaxis.z + vbase.z * g->normal.z;
        }
        else {
            v->x = (float)g->x -( vbase.y*Yaxis.x + vbase.z * g->normal.x);
            v->y = (float)g->y -( vbase.y*Yaxis.y + vbase.z * g->normal.y);
            v->z = (float)g->z -( vbase.y*Yaxis.z + vbase.z * g->normal.z);
        }

    }
    sc->c[which] = cg;
    PC_Pull_Pline_To_Mould(mould,&(sc->p[which]),&(sc->c[which]),0, sc->Esail ->m_Linear_Tol);

    return 1;
} 
int Delete_Geodesic_List(struct GPoint *g){
    if (!g) return 0;
    Delete_Geodesic_List(g->next);
    RXFREE(g);
    g=NULL;
    return 1;
}

