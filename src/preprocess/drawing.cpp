/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by P Heppel  Oct 1997
drawing .c contains some minor drawing routines 
STATUS OCT 97. First written. 
? mat color the correct side? YES
? Mat vector the correct side? YES
? MAt vector attached to the correct sidepoly? YES


*/
#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXSeamcurve.h"

#include "RXSitePt.h"
#include "RXCurve.h"

#include "panel.h"
#include "stringutils.h"

#include "drawing.h"

HC_KEY PC_Insert_Arrow(const ON_3dPoint q,const float ang, const char * text){
	HC_KEY key=0;
	VECTOR p[3];
#define  ARROW_LENGTH (float)5.0
#define ARROW_WIDTH (float)0.5
#ifndef HOOPS
    return 0;
#else
	if(fabs(q.x) > 100000. ||   fabs(q.y) > 100000. || fabs(q.z) > 100000. ) { 
		printf(" bad arrow %s\n",text); 
		return 0;
	}
	HC_Open_Segment("?sail/arrows");
		key= HC_KOpen_Segment("");
			p[0].x = (float)0;
			p[0].y = (float)0;
			p[0].z =(float) 0.0;
			p[1].x = p[0].x + ARROW_LENGTH; 
			p[1].y = p[0].y + ARROW_WIDTH/2;
			p[1].z = p[0].z;  
			p[2].x = p[0].x + ARROW_LENGTH;
			p[2].y = p[0].y - ARROW_WIDTH/2;
			p[2].z = p[0].z;
			HC_Insert_Polygon(3,p);
			HC_Rotate_Object((float)ang,(float) ang,0.0);
			printf("arrow %f    %f    %f   %s\n", q.x,q.y,q.z,text);
			HC_Translate_Object(q.x,q.y,q.z);
			HC_Set_Color("faces=magenta,edges=green");
			HC_Set_Visibility("everything=on");
			HC_Set_Selectability("everything=on");
			if(!is_empty(text)) {
				char *s =(char*) MALLOC(strlen(text)+64);
				sprintf(s,"popuptext=""%s""",text);
				HC_Set_User_Options(s);
				RXFREE(s);
			}
		HC_Close_Segment();
	HC_Close_Segment();
	return key;
#endif
}

HC_KEY PC_Insert_Arrow(const RXSitePt *q,const float ang, const char * text){
	return PC_Insert_Arrow(q->ToONPoint(), ang, text);
}

HC_KEY PC_Insert_Arrow(const VECTOR *q,const float ang, const char *text){
	return PC_Insert_Arrow(ON_3dPoint(q->x,q->y,q->z), ang, text);
}


int Draw_Ruler(float p_x0,float p_y0,float p_z0,int p_ax,float p_ymax,float p_ymin){
printf(" Draw Ruler at (%f %f %f) with ax=%d, (%f %f)", p_x0,p_y0,p_z0,p_ax,p_ymax,p_ymin);

return 1;
}
int Draw_Orientation_Vector(RXEntity_p e , const ON_3dPoint e1,const ON_3dPoint e2,const ON_3dVector xl,const ON_3dVector yl,const ON_3dVector zl,const float Chord){
sc_ptr sc = (sc_ptr ) e;
/* If the atts contain left or right, we draw  a 'T' onto the appropriate curve  pointing the
appropriate way. There is a 25% chance of getting it right first time. 
The 'T' is parallel to the chord. 
*/
ON_3dVector p; 
ON_3dVector l_z;
ON_3dPoint l_a,b,c,d;
int k,side, ok;
float offin, dy,dx,sign;
offin =  (Chord)/2.;
dy = (Chord)/40.0;
if(dy > 0.1) dy=float(0.1);
dx = dy*2.0;
l_z =e->Normal();
	for(k=0;k<3;k+=2) {
		RXCurve * l_c = sc->m_pC[k];
	side=k;  sign = (float)(side-1);
		if(sc->Mat_Angle[k].flag &1  )   {
			l_a = l_c->PointAt(offin); 
			b.x = l_a.x + sign*dy*yl.x; 
			b.y = l_a.y + sign*dy*yl.y; 
			b.z = l_a.z + sign*dy*yl.z;  
			p.x=xl.x;  p.y=xl.y;  p.z=xl.z; 
			PC_Vector_Rotate(p, sc->Mat_Angle[k].angle / 57.29577, l_z);
			c.x = b.x + dx* p.x; 
			c.y = b.y + dx* p.y; 
			c.z = b.z + dx* p.z; 

			d.x = b.x - dx* p.x; 
			d.y = b.y - dx* p.y; 
			d.z = b.z - dx* p.z;  
			ok=1;
#ifdef DEBUG
			if(l_a.DistanceTo (ON_3dPoint(0,0,0)) > 1000)
			{printf(" L_A is %g %g %g\n", l_a.x,l_a.y,l_a.z); ok=0;}
			else if(b.DistanceTo(ON_3dPoint(0,0,0)) > 1000)
			{printf(" b  is %g %g %g\n", b.x,b.y,b.z); ok=0;}
			else if(c.DistanceTo(ON_3dPoint(0,0,0)) > 1000)
			{printf(" c is %g %g %g\n",c.x,c.y,c.z); ok=0;}
			else if(d.DistanceTo(ON_3dPoint(0,0,0)) > 1000)
			{printf(" d  is %g %g %g\n", d.x,d.y,d.z); ok=0;}
#endif
			if(ok) {
                            RX_Insert_Line((float)l_a.x,(float)l_a.y,(float)l_a.z,b.x,b.y,b.z,e->GNode());
                            RX_Insert_Line(c.x,c.y,c.z,d.x,d.y,d.z,e->GNode());
			}
		}
	}
HC_Set_Color("lines=black");

return 1;
}
int Draw_Material_Ref(RXEntity_p e, const ON_3dPoint  e1b,const ON_3dPoint e2b,const ON_3dVector xlb,const ON_3dVector yl,const ON_3dVector zlb ) {

/* IF the leftmat is defined, draw a thick (4 pixel) line in the right colour, offset 2 pixels to the correct
side of the appropriate curve. */
//VECTOR a; 
int k; 
HC_KEY key;

float sign,dy;
sc_ptr sc = (sc_ptr ) e;
char color[256],seg[512];
float One_Pixel = float(0.05);
if(One_Pixel <=0.0){ // slow but more accurate 
 //a.x=a.y=a.z=0.0; 

// a.x=1.0;

 One_Pixel = float(0.030); //distV(&b,&c); 
}
for(k=0;k<3;k+=2) {  // k is the side
		if(sc->Defined_Material[k] ) { 
			//side=k;  
			sign = (float)(k-1);
			dy = 2.0*One_Pixel*sign; // half the weight plus 1
			key = sc->Defined_Material[k] ->hoopskey;
			HC_Show_Segment(key,seg); 
			HC_QShow_One_Color(seg,"faces",color);  

			HC_Set_Line_Weight(4.0); //nice but slow
			HC_Open_Segment("");
				HC_Set_Color(color);
				sc->m_pC[k]->Draw();
				HC_Translate_Object(yl.x*dy, yl.y*dy,yl.z*dy);
			HC_Close_Segment();
		}
	}


return 1;
}

int Draw_SC_Arrow(RXEntity_p e, const ON_3dPoint e1,const ON_3dPoint e2,const ON_3dVector xl,const ON_3dVector yl,const ON_3dVector zl ) {

/* draw a thick (4 pixel) arrow in the right colour, 
pointing in the direction of the curve. 
Colour magenta else if sketch, grey. */

int k,side; 
double s;
float sign, One_Pixel,dx,dy;
sc_ptr sc = (sc_ptr ) e;
ON_3dPoint p1,p2, a;

return 0; // for some reason the RXCurve Evaluate gives rubbish. 
a.x=a.y=a.z=0.0; 


a.x=1.0;

One_Pixel = float(0.030); // distV(&b,&c); 

/* get middle position p1 and position dx below it p2
lines from p1 to (p2+yl*dy)
*/
	if(sc->sketch) 	
		One_Pixel = One_Pixel/2.0;
	dx = 6.0*One_Pixel; 
	RXCurve * l_c = sc->m_pC[1];
	s = sc->m_arcs[1]/2.0 +dx;
	p1 = l_c->PointAt (s);
	s = sc->m_arcs[1]/2.0 - dx;
	p2 = l_c->PointAt (s);
	for(k=0;k<3;k+=2) { 
			side=k;  sign = (float)(side-1);
			dy = 3.0*One_Pixel*sign; 
	
			HC_Set_Line_Weight(4.0);

			HC_Set_Color("magenta");
			if(sc->sketch) 
					HC_Set_Color("grey");
			a.x = p2.x + yl.x*dy;
			a.y = p2.y + yl.y*dy;
			a.z = p2.z + yl.z*dy;

                        RX_Insert_Line((float) p1.x,(float)p1.y,(float)p1.z,(float)a.x,(float)a.y,(float)a.z,e->GNode());
		
	}


return 1;
}
