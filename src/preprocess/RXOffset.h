#ifndef RX_OFFSET_HH
#define RX_OFFSET_HH

 
/*!  an offset is an expression defining a position on a curve. 
	if the expression evaluates to a number, then the evaluation
	just returns that number.
	if the expression string contains a '%' the evaluation is only meaningful
	given a curve '%' expands to "the curve's length divided by 100".
*/
#include "RXExpression.h"

class RXOffset  :
	public RXExpression
{
public:
	//!Constructors
	RXOffset(RXSTRING name,RXSTRING exp, sc_ptr sc, class RXSail *const p_sail);
	RXOffset();
	//!Constructor by copy
	RXOffset(const RXOffset & p_Obj);

	//!Destructor
	virtual ~RXOffset();
	void Init();

	RXOffset& operator = (const RXOffset & p_Obj);

	int Print(FILE *fp = stdout);
	int Change(const RXSTRING &sin);
	int Fill(const RXSTRING &sin);
	double evaluate()  ;
	double Evaluate(sc_ptr scin,int which)  ;
	double EvaluateBySpace(sc_ptr scin,const RX_COORDSPACE space);

	int Set_Offstring(char * p_str);
	int Set_Offstring(const RXSTRING &p_str);
	
	RXSTRING GetText() const;

	sc_ptr  GetSc()const { return m_sc;} 
	virtual const char* What() {return "O";}
private: //Methods

	void Set_rxoFlag(const int & p_flag);

	void Set_Percent(const int & p_percent);

	int ClearO();	//!Clear the attributes
	int Is_Parameter();
	int Is_Percent()const;

private: // data

	int m_PerCent;	// RXO_IS_PERCENT if we are to multiply by *arc 
					// RXO_IS_PARAM if X is a parameter value
					// RXO_NONE if X is a length value
	int m_O_Flag;	// status. RXO_NONE=sscanf  and set sce
					// RXO_COMPLETE we don't have to parse the string.

	sc_ptr m_sc;
};  //class RXOffset 

#define RXO_NONE		0
#define RXO_COMPLETE	1

#define RXO_IS_PERCENT	4
#define RXO_IS_PARAM	8

EXTERN_C int PCO_Free_Offset_Record(void* whoami,RXOffset **o);

#endif //#ifndef RX_OFFSET_HH


