#pragma once
#include "RXEntity.h"

class RXVectorField :
        public RXEntity
{
public:
    RXVectorField(void);

    RXVectorField( class RXSail *const p_model);
    virtual ~RXVectorField(void);
    int CClear();
    int Kill(){ return RXEntity::Kill();}
    int Resolve(void);
    int Compute(void);
    int Finish(void);
    int ReWriteLine(void);
    int ValueAt(const RXSitePt&p, double*rv) ;

    int Dump(FILE *fp) const;
    RXSTRING TellMeAbout() const;
    virtual const char* What()const {return "VF";}
protected:
    RXSTRING expnames[3];
};
