#pragma once
#include <map>
#include <vector>
#include <list>
#include <QString>
#include "mkl_dss.h"
#include "mkl_types.h"

using namespace std;

#include "RXHarwellBoeingIO1.h"
// RXSparseMatrix1 is the class used for the finite element stiffness matrix. I think that's because it is indexed from 1 - for pardiso
//conversely, plain old RXSparseMatrix is used for texture mapping
// CARE!!  the harwell-boeing file format is CSC
// Internally we use CSR.
// this is why we do a Transpose on read, write and fill.

// values for m_StorageType, 
#ifndef RXM_SMTYPE_DEF
enum  smtype{NONE,MAPSTYLE,CSC };
#define  RXM_SMTYPE_DEF
#endif
// values for m_MatrixType,   this must match the definition in fortran module solveimplicit_f

#define		RXM_SYMMETRIC	1
#define		RXM_RECTANGLE  	2
#define		RXM_SKEW_SYMM	4
#define		RXM_POS_DEF		8
#define		RXM_INDEFINITE	16
#define		RXM_COMPLEX		32
#define		RXM_HERMITIAN	(64+RXM_COMPLEX)
#define		RXM_STRUCTSYM	128
#define RXROWTYPE class RXSparseMatrixRow

// preconditioner types
#define		RXMP_DIAGONAL	0


// for symmetric matrices which we store in lower diagona lform,
//RXSparseMatrixRow holds a 'dog-leg'  First the row N - up to the diagonal
// then the tail of the column N, from the diagonal onward.
#ifdef NEVER
class RXSparseMatrixRow // a holder for a pointer into the row. Beware of side-effects! 
{
    friend 	class RXSparseMatrix1;
public:

    RXSparseMatrixRow();
    RXSparseMatrixRow(class RXSparseMatrix1 *mat  , const int n );
    ~RXSparseMatrixRow();

    size_t operator*=(const double&);
    size_t operator+=(const RXSparseMatrixRow&);
    size_t operator+=(pair<list<pair< pair<int,int>,double> >,  int > );
    // Just  clears the list. maintains ref into matrix
    int Init(void);

protected:
    class RXSparseMatrix1 * m_mat;
    list<pair<int,int> > m_l;
    int n; // the number - whether row or col.
    int k; // the break ;  ie the element in this row (counting from 0) which is on the diagonal.
    int PrettyPrint(std::ostream &s) const;
    list<pair< pair<int,int>,double> > Extract(const double x=1.0)  const;
};// row class 
#endif
class RXSparseMatrix1

{
public:
    RXSparseMatrix1();
    RXSparseMatrix1(const int t);
    RXSparseMatrix1(const map<pair<int,int>,double> m,const int t);
    virtual ~RXSparseMatrix1(void);
    int Init(const enum smtype  t);
    int Nr()const{ return this->m_hbio.nrow1;}
    int Nc()const{ return this->m_hbio.ncol1;}

    // careful. These operators return a matrix of type NONE
    // if the matrices don't conform
    int DeepCopy(const RXSparseMatrix1& src);
    RXSparseMatrix1&  operator=(const RXSparseMatrix1& src);
//    RXSparseMatrix1& operator+(const RXSparseMatrix1&) ;
    int BinaryAdd(const RXSparseMatrix1& ma,const RXSparseMatrix1& mb );
    RXSparseMatrix1 operator*( RXSparseMatrix1&) ;
    const RXSparseMatrix1 operator*(const double&);
    const RXSparseMatrix1 operator*=(const double&);

    int Multiply(RXSparseMatrix1 &right, RXSparseMatrix1 &result);
    int Transpose();
    int GetColumn(const int n,double *p);
    QString  Statistics() const;

    int ZeroRow(const int i);
    int ZeroColumn(const int i);
    int SolveOnce(double*result); // return value 0 means OK. this is not our usual convention
    int SolveIterative( double*result,const double tol);
    int Polish(void);
    int SetRHS(const int nrhs, double* p_rhs);
    int MTXWrite(const char*fname);
    int MTXRead(const char*fname);
    int HB_Write(const char*fname) ;
    int HB_Read(const char*fname);

    enum  smtype SetStorageType(const enum  smtype t);
    int SetMatrixType(const int t); // returns oldtype
    int GetPardisoFlag()const;
    int GetFlags(void) const{return m_flags;}

    bool IsFlaggedSymmetric( void)const{ return(m_flags&RXM_SYMMETRIC);}
    void FlagSet  (const int p) {
        m_flags = m_flags| p;
    }

    void FlagClear(const int p) {
        int j;
        j=m_flags&p;
        m_flags = m_flags - j;
    }



    bool SlowGetElement(const int r, const int c, double &v) const;

    int AddToDiagonal(const double &x, const double&threshold);
    int LimitToDiagonal(const double &xmin, const double&xmax);
    pair<double,double> DiagonalStats();
    int ExtractDiagonal( map<int,double> &d) const;
    int Swapcr(); // very dangerous.
    int StructureIsSameQ(class RXSparseMatrix1*other );
    int AddRows(const int where, const int howmany);
    int AddCols(const int where, const int howmany);
    int IdentityMatrix(const int n);
    const char*GetMMartString()const;
    // these methods follow the mkl example dss_unsym_c.c
    int dss_start();
    int dss_end();
    int dss_factor(double *values);
    int dss_solve(double*pSolValues); // returns the solution in rhs
private: // variables for dss solver

    _MKL_DSS_HANDLE_t m_dsshandle;
    MKL_INT m_dssopt;// = MKL_DSS_DEFAULTS ;
    MKL_INT m_dsssym;// = MKL_DSS_NON_SYMMETRIC;
    MKL_INT m_dsstype;// = MKL_DSS_INDEFINITE;

protected:
    int ConvMapToCSC(void);
    int ConvCSCToMap(void);

    int SetDimensions(const int nr, const int nc);
    RXSparseMatrix1 *PreConditioner(const int type = RXMP_DIAGONAL); // experimental

public:
    map<pair<int,int>,double> m_map;
    vector<double> rHs;// may be a multiple of nr.
    int m_nRhsVecs;
    enum  smtype m_StorageType;

    class RXHarwellBoeingIO1 m_hbio;
protected:
    int m_flags;
    int m_b ; // index base 0 or 1;
    // pardiso system uses base one
    // the structural solver system uses base 1.

    //MM general doesnt care
    //	mm coordinate is base 1
    //	HBIO asumes base 1

public:
    static int TestForSymmetric(const map<pair<int,int>,double> &m,const double &tol );
    int MakeUpperTriangular( ); // map<pair<int,int>,double> &m  );
    double * FullMatrix();
    int PrettyPrint(std::ostream &s) const;
};

// C style interface for calling from C or fortran See cfromf_declarations.f90
extern "C" class RXSparseMatrix1 *NewSparseMatrix(const int type);
extern "C" void DestroySparseMatrix( class RXSparseMatrix1 *m);

extern "C" void AddToSparseMatrix(RXSparseMatrix1 *m,const int r,const int c, const double v);
extern "C" double SparseMatrixElement (RXSparseMatrix1 *m,const int r,const int c);


extern "C" int  SparseSolveLinear( RXSparseMatrix1 *m, RXSparseMatrix1 *jay,RXSparseMatrix1 *rhs,double*r ,int *myflags);
//SparseSolveLinearInline is SSD.
//extern   int  SparseSolveLinearInline( RXSparseMatrix1 *m, RXSparseMatrix1 *jay,RXSparseMatrix1 *rhs,double*r);
extern "C" int  SparseSolveLinearB( RXSparseMatrix1 *m, RXSparseMatrix1 *jay,RXSparseMatrix1 *rhs,double*r);

// SparseSolveLinearC is forked from  SparseSolveLinear March 2011. It uses  sparskit so we can stay in CSR form.
extern "C" int  SparseSolveLinearC( RXSparseMatrix1 *m, RXSparseMatrix1 *jay,RXSparseMatrix1 *rhs,double*r ,int *myflags);

// SparseSolveLinearD is forked from  SparseSolveLinearC March 2012. It is for testing other solvers than pardiso
// which has difficulties with the big condition numbers that come with fine meshes.

//extern "C" int  SparseSolveLinearD( RXSparseMatrix1 *m, RXSparseMatrix1 *jay,RXSparseMatrix1 *rhs,double*r ,int *myflags);




// SparseSolveSVD was a bad idea for dealing with singular stiffness matrices. The idea was to ignore the free modes
// which means you never get started. 
extern "C" int  SparseSolveSVD   ( RXSparseMatrix1 *m, RXSparseMatrix1 *jay,RXSparseMatrix1 *rhs,double*r ,int *myflags);

extern "C" void SparseMatrixDimensions(RXSparseMatrix1 *m, int* nr, int *nc);
extern "C" void WriteSparseMatrix(RXSparseMatrix1 *m,const char*filename);
EXTERN_C double rxmeanabs(double*v ,int n);
EXTERN_C double rxmax(double*v ,int n);
EXTERN_C double rxsum(double*v ,int n);
EXTERN_C double rxmean(double*v ,int n);
//extern  int SparseSolveLinearInline( RXSparseMatrix1 *m, RXSparseMatrix1 *jay,RXSparseMatrix1 *rhs,double*result,int *flags);
