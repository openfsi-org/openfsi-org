#pragma once
#include "griddefs.h"
class RXOffset;
#include "RXEntity.h"
#include "vectors.h"
class  RXAttributes ;
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
THESE ARE THE GLOBAL FUNCTIONS WHICH ARE CANDIDATES TO BE METHODS OF THIS CLASS or maybe organised into namespaces.
THIS LIST DOESNT INCLUDE THOSE WHICH HAVE AN RXENTITY ARG RATHER THAN A PC_SEAMCURVE ARG.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

cut.h(30):EXTERN_C int PCC_Check_Cut_Parameters(sc_ptr sc);
cut.h(31):EXTERN_C int PCCut_Set_Cut_Limits(sc_ptr sc,double *upper, double *lower);

cut.h(39):EXTERN_C int SearchForCloseSites(sc_ptr sc,double tol);
cut.h(40):EXTERN_C int PCC_Create_EndSites_If_Required(sc_ptr sc,double tol);
cut.h(49):EXTERN_C int Make_End_Ptrs(sc_ptr  sc);
delete.h(11):EXTERN_C int Delete_IA_Record(sc_ptr np,int i);
delete.h(20):EXTERN_C int Free_Gcurve_List(sc_ptr sc);

gauss.h(11):int Get_Gaussian_Seam(sc_ptr p,VECTOR**base,int*bc,VECTOR e1b,VECTOR e2b,VECTOR e1r,VECTOR e2r,float Red_Chord,double*e1a,double*e2a);
gcurve.h(46):EXTERN_C int addOneGaussPt_oncurve( PSIDEPTR ps,sc_ptr sc,int space,int what);
gcurve.h(51):EXTERN_C int Print_SC_GaussPoints(FILE *fp, sc_ptr np);
geodesic.h(7):EXTERN_C int Find_Geodesic(sc_ptr sc, VECTOR**p,int*c, float*chord);
geodesic.h(9):EXTERN_C int Create_Displaced_Polyline(sc_ptr sc,RXEntity_p mould,int which,int c,double*x,double*y,double*z,VECTOR *base);
geodesic.h(10):EXTERN_C int  InitGlist(sc_ptr sc,Site *e1, Site*e2,RXEntity_p mould);
geodesic.h(11):EXTERN_C int  CopyGlistToPline(sc_ptr sc,VECTOR**p,int*c);
geodesic.h(12):EXTERN_C int  GeodifyList (sc_ptr sc,RXEntity_p mould);
geodesic.h(17):EXTERN_C  int Get_Normalised_Geodesic_S(sc_ptr sc);
geodesic.h(18):EXTERN_C  int Calculate_Y(sc_ptr sc,RXEntity_p mould)
geodesic.h(25):EXTERN_C int UV_from_Sxy( sc_ptr sc, double sxy, double*u, double*v);
geodesic.h(33):EXTERN_C double Suv_from_Sxy( sc_ptr sc, double sxy, int*flag); 
geodesic.h(34):EXTERN_C double Sxy_from_Suv( sc_ptr sc, double suv, int*flag); 
geodesic.h(35):EXTERN_C int UV_from_Suv( sc_ptr sc, double suv, double*u, double*v) ;
geodesic.h(36):EXTERN_C int Place_On_Geo(sc_ptr sc,double Suv, double*v1,double*v2,double*v3,double*U1,double*U2); 
geodesic.h(40):EXTERN_C int GlistFirstAndLast(sc_ptr sc,Site *e1, Site *e2,RXEntity_p mould);

panel.h(27):EXTERN_C int Update_Xabs_Offset(sc_ptr p);
panel.h(28):EXTERN_C int Measure_End_Angles(sc_ptr  p,int seamflag,int Use_Gauss_EA ,double e1a,double e2a);
panel.h(40):int Make_Fanned_(sc_ptr  sc,int flag,float *arc, double *a0,float chord, int *c, VECTOR **p);
partialatts.h(12):int Create_Partial_Atts(sc_ptr sc) ;
partialatts.h(13):int Append_Partial_Atts(sc_ptr sc,RXEntity_p sco,char *o1, char *o2,char *atts, int rev);
partialatts.h(16):char *Search_Partial_Atts(sc_ptr sc,double o1,double o2,double tol,const char*kw1,const char*kw2,RXEntity_p *sco, int*rev);
peternew.h(7):EXTERN_C int Put_Pside_Mats_Safe(sc_ptr np,struct POSONCURVE **mats,int*nmats);
peternew.h(9):EXTERN_C int Restore_Pside_Mats(sc_ptr np,struct POSONCURVE *mats,int nold);
printall.h(5):EXTERN_C int Print_BroadSeam(sc_ptr sc, FILE *fp);

rxcurve.h(41):		ON_3dPointArray Divide(RXOffset  &o1, RXOffset &o2, sc_ptr sc,int side, int p_n);
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
#define		SC_START 55
#define		SC_END 56
#define 	SC_SUB_SEGMENT	2

#define FLATFLAG		 99
#define FANNEDFLAG	 100
#define SEAMFLAG 	101
#define CURVEFLAG	 103
#define UNKNOWNFLAG	 104

#define SCTYPE_UNDEFINED	200
#define SCTYPE_XYZABS_ONLY	201
#define SCTYPE_XYZABS_CUT	202
#define SCTYPE_XYZABS_CUT_GEO 203
#define SCTYPE_SCALED_NON_GEO 204
#define SCTYPE_SCALED_GEO	205
#define SCTYPE_RLX      	206
#define SCTYPE_SEAM_G_M 	207
#define SCTYPE_SCALED_GEO_MOULD_CURVE 208
#define SCTYPE_TPN			 209
#define SCTYPE_XYZABSSKFIL	210

class RXSeamcurve : public RXEntity
{
    friend class RX_SeamcurveHelper;
public:

    RXSeamcurve();
    RXSeamcurve(class RXSail *p );
    int CClear();
    virtual int Dump(FILE *fp) const;

    virtual int Compute(void);
    virtual int Resolve(void);
    virtual int ReWriteLine(void);
    virtual int Finish(void);

    ~RXSeamcurve();

    int Resolve (const RXEntity_p p_bce=0,const RXEntity_p p_p1=0,const RXEntity_p p_p2=0) ;

    int FlagSet(const int what);
    int FlagClear(const int what);
    int FlagQuery(const int what) const;

    //!Return the arc length of one of the curves associated to the seamcurve structure
    double GetArc(const int &k,const double fractional_tolerance=1.0e-8) const; // p->GetArc(0)
    int Insert_One_IA(RXEntity_p ps, RXOffset*offset) ;
    int Remove_From_IA_List(RXEntity_p e,const int DoScsToo);
    int In_IA_List(const  RXEntity_p e) const;
protected:

    int Delete_One_IA(const int i);
    int ResolveCommon(const RXAttributes &att  );  // for things which we want derived classes to do too.
    int FlagSet(const RXAttributes &att); // as SC_FlagSet(p, att);
    int Init();
private:
    static int Func_Compare_Pside( const void *va,const void *vb);
    int Delete_Seamcurve(void ); // really a Clear

    RXEntity_p Create_Pocket_From_SC();
    RXEntity_p Create_Patch_From_SC();
    RXEntity_p Create_String_From_SC();
    RXEntity_p Create_Beam_From_SC();

    int Rename_Pside_List();
    int Sort_Pside_List();
    int Compare_Pside_Lists(Site **ts,Site **ls,
                            int newcount,int*Keep_Mark,int*Make_Mark);

    int Update_IAOffsets();
 //   int Ask_String(const char*s,const char*yes,const char*no);

public:
    int SeamCurve_Type( ) const ;
    int SnapEnds();
    int Break_Curve();
    int Demote_To_Sketch(void);
    int Append_One_Pside( Site**This_Node,Site**Last_Node,RXOffset *e1o,RXOffset *e2o);
    double MeshSize(const RXSitePt &p) const;
    ON_3dPoint Get_End_Position(const int flag);
    int SeamCurve_Init();
    HC_KEY Draw_Deflected_SC(FILE *fp=0); // put it in an invisible segment under p->hoopskey;
    int Remove_Distant_Sites(const double tol);
    int Compute_Seamcurve_Trigraph(int side1,int side2,
                                   RXSitePt *e1b, RXSitePt *e2b,
                                   ON_3dVector*xlb,ON_3dVector*ylb,ON_3dVector*zlb,
                                   float *Chord,const ON_3dVector Zax);
public:
    RXEntity_p BorGcurveptr;
    class RXSRelSite * End1site;
    class RXSRelSite* End2site;  /* ptr to RXEntity of end2 {SITE|seam|curve} */

    struct LINKLIST *mats[3];
    RXEntity_p Defined_Material[3];

    RXEntity_p m_spare;  // used in PC file
    RXEntity_p gauss;
    RXEntity_p g1p[3],g2p[3];

    PSIDEPTR *pslist;	   /* Psides owned */
    int npss;

    VECTOR *p[3];  /* must be malloced*/
    int c[3];
public:
    double m_arcs[3];      /* current arclengths of the 3 polylines */

    int n_angles;
    struct IMPOSED_ANGLE *ia;	/* end<n>type will be <(*end<n>ptr).type> */        /* site, seam,curve */


    class RXOffset *End1dist;
    class RXOffset *End2dist;

    int end1sign;
    int end2sign;

    double strt_A;   /* an estimate of the initial slope of the IA curve */

    int sketch; /* 1 if paneller is to ignore */
    int edge;   /* 1 if this is an edge panel */
    double end1angle, old_end1angle; /* ready for export */
    double end2angle, old_end2angle;
private:  
    int Flags;  // see top of this file for definitiona
public:
    int Make_Pside_Request;
    int m_isFlat;
    int Xabs;
    int Yabs;
    int YasX;
    int  XYZabs;
    int Needs_Cutting;
    int seam;  /* 1 for seam, 0 for curve*/
    int m_scMoulded;   // means just pull to the mould. Dodgy . SSD by FlagQuery(MOULDED)
    int Geo_Or_UV;    /* means fit adaptively to the mould */
    int Geodesic;    /* In addition to the above, make into a geodesic */
    struct GPoint*GeodesicList;

    /* for fabrics */
    struct SIDE_ANGLE Mat_Angle[3];	/* 0=ignore 1 = ref psidechord 2 ref SC chord */

    double G1Fac[3],G2Fac[3];
    double e1w,e2w;	  /* derived  from g<n>p and G<n>Fac	*/
    double GaussArea;	/*  dbg . The integration area */

    class RXCurve *m_pC[3];  //pointer because cannot have RXCurve instance because PC_SEAMCURVE is created using CALLOC
    //WE SHOULD ALWAYS CALL new for each C[i] if Seamcurve_Init and delete

    /* akm added 28 Nov 95 for gauss stuff */
    void *gauss_pts;		/* not yet sure what it will be */
    int gauss_npts;		/* this will say how many of the above there are */
    int moved_relsites_flag;	/* means re-copy the offsets */
    int Defined_Length;
    struct LINKLIST *Inside_List;

    static  RXSeamcurve *  Find_Seamcurve_By_Ends(RXEntity_p p1,RXEntity_p p2);// used by string resolve
};
