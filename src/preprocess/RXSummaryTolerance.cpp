#include "StdAfx.h"
#include <QDebug>
#include "global_declarations.h"
#include "RXDatabaseI.h"
#include "stringutils.h"
#include "RelaxWorld.h"
#include "RXSummaryTolerance.h"

//  static
QString  RXSummaryTolerance::m_logtext;

RXSummaryTolerance::RXSummaryTolerance(const string &a, const double &b, const string &c) 
{
    int i;
    Tol_Flag=0;
    this->m_header = a; m_logtext+=QString(a.c_str()) + " \t ";
    this->tol = b;

    if( c[0] =='%')
        this->Tol_Flag = SCT_PERCENT;
    else {
        i = atoi(c.c_str());
        switch (i) {
        case 0:
            this->Tol_Flag = SCT_DISABLED;
            break;
        case 1:
            this->Tol_Flag = SCT_UNITS;
            break;
        case 2:
        case '%':
            this->Tol_Flag = SCT_PERCENT;
            break;
        default:
            cout<<" tolerance word '"<< c<<"'not understood. setting to 'units'\n";
            this->Tol_Flag = SCT_UNITS;
        }
    }
    cout<<"added '"<<a<<"' with tol = "<<b<<"("<<c<<")"<<endl;
}

RXSummaryTolerance::~RXSummaryTolerance(void)
{
    qDebug()<<" deleting RXSummaryTolerance "<<this->m_header.c_str()<<endl;
    qDebug()<<" its old-value list is :"<<endl;
    std::list<double>::iterator it;
    for(it=this->m_oldvals.begin(); it!=m_oldvals.end(); ++it)
        qDebug()<< *it;


    qDebug()<<"\n FSI convergence log \n relaxation factor = "<< g_Relaxation_Factor;
    qDebug()<< this->m_logtext;
}
int RelaxWorld::AddOneTol(const std::string &linein)
{
    /* reads default pref values for the summary tolerances */

    class RXSummaryTolerance *sum_tol;
    vector<string> wds;
    int retVal = 0;
    int i;

    string   line = rxstripcomment(linein );
    wds = rxparsestring (line,":\t\n");
    i = wds.size(); // make_into_words(line,&words,":\t\n");

    if(i != 4) {
        stringstream ss; ss<<"corrupt preferences file - Tolerance setting"<<endl;
        for(int k=0;k<i;k++)
            ss<<k<<" '"<<wds[k]<<"'"<<endl;
        rxerror(ss.str().c_str ()   ,2);
        retVal = 1;
        return(retVal);
    }

    sum_tol = new  RXSummaryTolerance(wds[1],atof(wds[2].c_str()), wds[3]);
    this->m_tol_list.insert(sum_tol);

    return(retVal);
}

int  RelaxWorld::Clear_SumTolList(void){ // these statics will eventually be methods of class RXSummary

    class RXSummaryTolerance *sum_tol;
    for(	set<RXSummaryTolerance* >::iterator it=  g_World->m_tol_list.begin ();it!= g_World->m_tol_list.end();++it)
    {
        sum_tol = *it;
        delete sum_tol;
    }
    g_World->m_tol_list.clear();
    return 0;
}


int  RXSummaryTolerance::FlushHistory(){
    m_oldvals.clear ();
    return 1;
}
int RelaxWorld::FlushTolHistory(){

    class RXSummaryTolerance *t;
    set<RXSummaryTolerance* >::iterator it;
    for(it=  this->m_tol_list.begin ();it!= this->m_tol_list.end();++it)
    {
        t = *it;
        t->FlushHistory();
    }
    return 1;
}

int RXSummaryTolerance::HasNotChanged(const char*buf){
    /* return 1 if inside
    0 if outside
     -1 if no opinion
 */
    char str[8];
    double err ;
    str[0]=0;
    int rc=-1;
    char*lp;
    double oldv=0;
    this->value = strtod(buf,&lp);
    if(!rxIsEmpty(lp))
        cout<<"(warning) Variable "<< this->m_header<<" is '"<<buf<<"'' evaluates to "<< this->value<<" It isnt just a number\n";
    if(m_oldvals.size()){
        oldv=m_oldvals.back();
        err= (oldv - this->value);
        if(Tol_Flag==SCT_PERCENT ) { // percent
            strcpy(str,"%");
            if(fabs(value)> 0.0000001)
                err = err/value * 100.0;
        }
        cout.width(24);        cout<< this->m_header;        cout.width(0);

        if( fabs(err) > tol* (1.0 - g_Relaxation_Factor)) {
            cout<<" is out of tolerance ("<< value <<" was "<<oldv  <<". D="<< -err <<str<<" > " << tol<<"*(1-"<< g_Relaxation_Factor<<" ))"<<endl;
            rc= 0;
        }
        else{
            cout<<" is inside tolerance ("<< -err<< str<<"  < "<<  tol<<"*(1-"<< g_Relaxation_Factor<<" ))" ;
            cout<<" (is "<<  value<<" was "<<   oldv<<")"<<endl;
            rc=1;
        }
    } // if there are old vals
    m_oldvals.push_back(this->value);


    return rc;
}
int RelaxWorld::Check_Convergence(void) {// the principle convergence test.
    /*
   return 1 if all criteria are met
    return 0 if some criteria are not met  */
    int retVal=0;
    int i,iret=0;
    char buffer[256];
    int PleaseReturnZero = 0;

    class RXMirrorDBI *sum = this->DBMirror(); if(!sum) return -1;
    RXSummaryTolerance::m_logtext+= "\n";


    class RXSummaryTolerance *t;
    set<RXSummaryTolerance* >::iterator it;
    for(it=  this->m_tol_list.begin ();it!= this->m_tol_list.end();++it)
    {
        t = *it;

        if(sum->Extract_One_Text(t->GetHeader().c_str(),buffer,255)) {
            t->m_logtext+=QString(buffer) + " \t ";
            iret = t->HasNotChanged (buffer); // return 1 if inside,  0 if outside, -1 if no opinion
            if(iret == 0) PleaseReturnZero = 1; // it changed.
            if(iret > 0) retVal = 1; // it didnt change.
            if(iret < 0) PleaseReturnZero = 0; // first time through.
        }
    }
    if (PleaseReturnZero)
        return 0;
    if(retVal>0)
        qDebug()<<RXSummaryTolerance::m_logtext<<endl;
    return (retVal);
}

int RelaxWorld::DatabaseValue(const QString head, double&value) {// the principle convergence test.

    char buffer[256];
    class RXMirrorDBI *sum = this->DBMirror(); if(!sum) return 0;
    bool ok;

    if(sum->Extract_One_Text(qPrintable(head),buffer,255)) {
        QString result(buffer);
        value = result.toDouble(&ok);
        if(ok) return 1;
    }
    return (0);
}

