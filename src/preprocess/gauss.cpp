/* gauss.c

 16.7.97 functional change
 This wasnt dealing with mitre seams properly so we relaxed Find Relevant a bit
 The logic was too preoccupied with edges

 FIRST CHOICE	the first site in a gauss SC which isnt the seed
 SECOND CHOICE	the first site in any SC which is neither the seed nor
   the first SC after the seed

 and a note in the Gfac. It really should be
  0.5 if the opposing SC is gaussian
   else 1.0 , regardless of whether the opposing SC is an edge or not.
  AND we halve Both gnps if they point to the same site.
  This deals with triangular panels.
  ( a cleverer trick would be to set the Gnp to 0 that was furthest from its end)


  22/5/96 FUNCTIONAL CHANGE. set factor to 0.5 even on edges
 or else mainsails end up too frisbeed
  17/5/96 zone has Needs _Computing set 0 so R Solve doesnt pick it up
  16/4/96 dont try and trace if an SC has no psides
   NB this arose during the Golf Ellipse project and I dont understand
 how it happened.  Why wasnt the sc broken?
 ? use of RLX model

  20/3/96  endwidths automated.
  After the first pass, it is OK. A bit repetitive
  It finds the zones OK
  Get_Gauss_Endwidths  checks for zones existing on its first & last psides
   whenever it tries to calc a gauss seam.
   If the zones don't exist it tries to trace them and fill in G<n>p
   This could cause a conflict if zones are used for other purposes, ie
   for Flood fill.
   It would be safer to rely on the existence of G<n>P
    (maybe -1 if we've tried and failed to find them)
   instead of the existence of zones.
   Then the zones would only have life for the duration of the tracing
   But this would require a call to Delete_Pside to
   nullifying G<n>P on its parent SC.
   A future compute on that SC would trace a new zone and update G<n>P on all
   other SCs touched by the zone change.

   The function Find Relevant finds the PS whose end will be used
   for G<n>p. It searches
      First  	A) A different SC(gauss). 	   				return 1
    Second 	B) On the SECOND edge found in the cycle 	return 2
    Third 	C) On any edge found in the cycle.	   		return 3

    This works for normal fully-shaped designs, including at edges
  It over-estimates the integration width at the head of a Xcut, for example
  This might be overcome by calculating G<n>Fac on the basis of the distance
  bewteen G1p[0] and g2p[0] instead of assuming a trapezoidalarea
  But that needs thinking about

  The important test is whether the analysis Solid Angle calc agrees.

  Note that the routine to find GnP is redundant.
  For the current Zone, it does both ends of ALL scs touched by the zone
  several times. THis is harmless but a bit slow.



 28/5/95  some malloc checking
    also BUG FIX flipflop on yold interp was wrong
 12/5/95 ds now set on Red_Chord so gaussed curve is returned the correct length

 started 14/3/95 Routines for Gaussian seamcurves

*/

#include "StdAfx.h"

#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXSRelSite.h"
#include "RXSeamcurve.h"
#include "RXRelationDefs.h"
#include "RXPside.h"
#include "RXPanel.h"
#include "griddefs.h"
#include "RXCurve.h"

#define DEBUG1
#include "global_declarations.h"

#include "entities.h"
#include "panel.h"
#include "polyline.h"
#include "interpln.h"
#include "pcspline.h"
#include "panel.h"
#include "etypes.h"
#include "gauss.h" 

#define ABOVE 1
#define BELOW 2

double simpson(int k, int c) {
    if(! ( c&1)) cout<< " you cant simpson on even \n"<<endl;
    if(k&1)   return 4.0;
    if(k==0) return 1.0;
    if(k==c-1) return 1.0;

    return 2.0;
}
int  RXPanel::Clear_Zone_Flags() {

    Panelptr  Zone=this;
    PSIDEPTR np;
    int k,c;
    c= (*Zone).m_psidecount;

    for (k=0;k<c;k++) {
        np = ((*Zone).m_pslist)[k];
        if ((*Zone).reversed[k] ) {
            np->leftZone[1]=NULL;
        }
        else {
            np->leftZone[0]=NULL;
        }
    }
    return(1);
}

PSIDEPTR PCZ_Next_Eligible(Site *s,PSIDEPTR np,const char*t) {
    /* Gauss Zone search
 - provided t is NULL
  Look at the pslist on s. keep on decrementing until we find a ps in the list
  which is an edge or gaussian. or not gaussian and multi-pside (a joining seam)
  return a ptr to this pside.
  If none are found its probably because the site is on a SC which needs breaking
  so Make All Psides
 - if t non-NULL, we are tracing a zone formed by SCs which have the string 't' in
 their atts.

   */
    struct SIDEREF side;
    QString t_debug(t);
    PSIDEPTR ps;
    PSIDEPTR  l_enext=NULL;
    sc_ptr sc;
    int j,k ,hopeless=  -1;
    do{
        hopeless++;
        /* which in the pslist is np? */
        for(k=0;k< s->PSCount();k++) {
            side = s->PSList(k);
            ps = side.pp;
            if(ps==np) break;
        }
        if(k== s->PSCount()) return(NULL);

        /* decrement k until we find a good one, unless its np */
        j = k;
        do {
            j = decrement(j, s->PSCount());
            side = s->PSList(j);
            ps = side.pp;
            sc = ps->sc;
            if(!t){
                if(sc->gauss || sc->edge || (!sc->gauss && sc->npss>1 )) return(ps);
            }
            else if(sc->AttributeFind(t)) return (ps);




        }while (j!=k);


        if(l_enext) return(l_enext);

        s->OutputToClient("gaussing",3);

    }	while(hopeless==0);

    return(NULL);
}
int PCZ_Grow_Zone(RXEntity_p pivot,PSIDEPTR p, Panelptr  Zone,RXEntity_p estart,const char*test) {

    /* grows a Zone from the starting pside by tracing around
     to the left until the end site equals the start site
     REQUIRES the search to be seeded on an internal pside
     */
    int OK=1;

    int found=0,k;

    int count=1;
    Site *s;

    RXEntity_p next_site;
    PSIDEPTR np, enext=NULL;

    count=1;
    next_site=pivot;
    np = (PSIDEPTR )p;
    do{
        s = (Site *)next_site;
        enext = PCZ_Next_Eligible(s,np,test);

        if (!enext){ OK=count; break;}

        np = enext;   /* current pside */

        for (k=0;k<2;k++ ) {
            found=0;

            /* find which end of enext is next_site,
    append the list (if its not the first)
  update next_site
  loop
  */
            if(np->Ep(k)==next_site) {
                found=1;
                if (np->leftZone[k]!=NULL)  return(0);
                if(count>=Zone->allocsize) {
                    Zone->allocsize= Zone->allocsize*2;
                    Zone->reversed=(int*)REALLOC(Zone->reversed ,Zone->allocsize*sizeof(int));
                    Zone->m_pslist   =(PSIDEPTR *)REALLOC(Zone->m_pslist,Zone->allocsize*sizeof(PSIDEPTR ));
                }
                Zone->reversed[count]=k;
                Zone->m_pslist[count]=np;
                count++;
                Zone->m_psidecount = count;

                next_site=np->Ep(1-k);

                break;
            }
        }
        if (!found){
            OK=0;
            break;
        }
        OK=1;
    }  while (next_site!=estart);

    return(OK);
}

int Find_First_Relevant_Pside( Panelptr  Zone,PSIDEPTR pp,PSIDEPTR *pout,int whichway, int*trev){
    /*  pp is the seed ps
   pout is the pside we find
 whichway is either ABOVE or BELOW
  trev is the reverse flag of the pside we find, in this list

   The old function found the first PS in the zone below this one which belongs to
     A) A different SC(gauss). 	   		return 1
   B) On the SECOND edge found in the cycle 	return 2
   C) On any edge found in the cycle.	   	return 3

The revised function (July 97) tries to deal better with mitre seams
The Logic is
 A) The first end which is in a different SC which is Gaussian
 This deals well with normal panel seams, whether the panels are tri or quad (or penta)
 B) The first site that is in a pside which is not in the same SC as the first pside.
  ie its SC is neither scoSeed nor scoSecond
   */
    int k,j;
    PSIDEPTR seed, p;
    RXEntity_p scoSeed,scoSecond,sco;
    sc_ptr sc;

    /* find the index of pp */
    for(k=0;k<Zone->m_psidecount;k++) {
        seed = Zone->m_pslist[k];
        if(seed == pp) break;
    }
    if(k==Zone->m_psidecount) return 0;
    scoSeed = pp->sc;
    /* search for a Gausscurve */
    j=k;
    do {
        if (whichway==ABOVE)		j = rxincrement(j,Zone->m_psidecount);
        else if(whichway==BELOW)  	j = rxdecrement(j,Zone->m_psidecount);
        else assert("WHICHWAY"==0);
        p = Zone->m_pslist[j];
        sco = p->sc;
        sc = (sc_ptr  )sco;
        if(sco!=scoSeed) {
            *trev = Zone->reversed[j];
            *pout = p;
            if(sc->gauss ) {	/* || sc->edge */
                return 1;
            }
        }
    }while (j!=k);

    /* start again. Find the first pside which is not on scoseed or on the sc immediately
 following */

    j=k;

    /*  get past the first */
    scoSecond=NULL;
    do {
        if (whichway==ABOVE)	j = rxincrement(j,Zone->m_psidecount);
        else if(whichway==BELOW)j = rxdecrement(j,Zone->m_psidecount);
        p = Zone->m_pslist[j];
        sco = p->sc;
        if(sco!=scoSeed) {
            scoSecond = sco;
            break;
        }
    }while (j!=k);

    do {
        if (whichway==ABOVE)	j = rxincrement(j,Zone->m_psidecount);
        else if(whichway==BELOW)j = rxdecrement(j,Zone->m_psidecount);
        p = Zone->m_pslist[j];
        sco = p->sc;
        if((sco!= scoSecond) && (sco!= scoSeed) ) {
            *trev = Zone->reversed[j];
            *pout = p;
            return 2;
        }
    }while (j!=k);
    /* search for the any edge */
    printf("Got to stage 3 in %s\n", scoSeed->name() );

    if(scoSecond) printf(" second is %s\n", scoSecond->name() );
    else cout<< " second is NULL\n"<<endl;
    j=k;
    do {
        if	(whichway==ABOVE)		j = rxincrement(j,Zone->m_psidecount);
        else if(whichway==BELOW)  	j = rxdecrement(j,Zone->m_psidecount);
        p = Zone->m_pslist[j];
        sco = p->sc;
        sc = (sc_ptr  )sco;
        if(sco!=scoSeed) {
            *trev = Zone->reversed[j];
            *pout = p;
            if(sc->edge ) {
#ifdef DEBUG
                if	(whichway==ABOVE) printf("any edge ABOVE for %s is %s\n",scoSeed->name(),sco->name());
                else printf("any edge BELOW for %s is %s\n",scoSeed->name(),sco->name());
#endif
                return 3;
            }
        }
    }while (j!=k);



    return 0;
}
int InZone(PSIDEPTR ps, Panelptr  Zone) { 
    int k;
    for(k=0;k<Zone->m_psidecount;k++) {
        PSIDEPTR pp = Zone->m_pslist[k];
        if(pp==ps) return 1 ;
    }
    return 0 ;
}			
int Fix_Up_Integration_For_Triangles(sc_ptr sc) {
    /*A triangular integration area will have g1k 0= g2k
  In this case, we find the end furthest to g1k and we set its gfact  to zero */
    int k;
    double d1,d2;
    Site *g;
    //VECTOR s;

    for(k=0;k<3; k+=2) {

        if(sc->g1p[k] &&(sc->g1p[k] == sc->g2p[k])){
            ON_3dPoint st =sc->Get_End_Position( SC_START);
            g = (Site*)sc->g1p[k];
            d1 =  g->DistanceTo(ON_3dPoint(st));

            st = sc->Get_End_Position( SC_END);
            g = (Site*)sc->g2p[k];
            d2 =  g->DistanceTo(ON_3dPoint(st));
            if(d1>=d2) {
                sc->G1Fac[k]=0.0;
                /*printf(" setting g1fact[%d] to 0 in %s\n", k, sc->owner->name());*/
            }
            else  {
                sc->G2Fac[k]=0.0;
                /*printf(" setting g2fact[%d] to 0 in %s\n", k, sc->owner->name());*/
            }

        }

    }
    return 1;
}
int Fill_In_Gnps( Panelptr  Zone){
    /* given a zone, set g1p[0,2], g2p[0,2], G1Fac[3],G2Fac[3] */

    int j,k,rev,K,trev,revold;
    PSIDEPTR pout;
    RXEntity_p sco,scold;
    sc_ptr sc,sct;
    revold=99;
    scold=NULL;

    for(k=0;k<Zone->m_psidecount;k++) {
        PSIDEPTR pp = Zone->m_pslist[k];
        rev = Zone->reversed[k];
        sco = pp->sc;
        sc = (sc_ptr  )sco;

        if(rev !=revold && sco !=scold && sc->gauss) {
            K = 2-2*rev;
#ifdef DEBUG
            printf(" working on %s K=%d\n",sco->name(),K);
#endif
            revold=rev;
            scold=sco;
            j=0; /* start of this sc */
            pp = sc->pslist[j];
            if(InZone(pp,Zone)) {
                if(!rev) {
                    /* find the first PS in the zone below this one
       IF that PS is REV Its first end is sc->g1p[K]
     else its second end is sc->g1p[K]
     */
                    if(Find_First_Relevant_Pside(Zone,pp,&pout,BELOW, &trev)) {
                        sc->g1p[K] = pout->Ep(1-trev);
#ifdef DEBUG				 	  
                        //	 	  printf(" G1P[%d]=%s\n",K,sc->g1p[K]->name());
#endif
                        sct = (sc_ptr  )pout->sc;
                        if(!sct->gauss) sc->G1Fac[K]=1.0;
                        else
                            sc->G1Fac[K]=0.5;
                    }
                    else {
                        sc->g1p[K] = NULL;
                        /*#ifdef DEBUG */
                        printf("(A) '%s' rev=%d G1P[%d]=NULL\n",sco->name(),rev, K );
                        /* #endif */
                    }
                }
                else {
                    /* find the first PS in the zone above this one
       IF that PS is REV Its second end is sc->g1p[K]
     else its first end is sc->g1p[K]
     */
                    if(Find_First_Relevant_Pside(Zone,pp,&pout,ABOVE, &trev)) {
                        sc->g1p[K] = pout->Ep(trev)  ;
#ifdef DEBUG
                        //	  printf(" G1P[%d]=%s\n",K,sc->g1p[K]->name());
#endif
                        sct = (sc_ptr  )pout->sc;
                        if(!sct->gauss) sc->G1Fac[K]=1.0;
                        else
                            sc->G1Fac[K]=0.5;
                    }
                    else {
                        sc->g1p[K] = NULL;
#ifdef DEBUG					  	
                        //		  printf(" G1P[%d]=NULL\n",K );
#endif
                        /*#ifdef DEBUG */
                        printf("(B) '%s' rev=%d G1P[%d]=NULL\n",sco->name(),rev, K );
                        /* #endif */
                    }
                }
            }

            j=sc->npss-1; /* end of this sc */
            pp = sc->pslist[j];
            if(InZone(pp,Zone)) {
                if(!rev) {
                    /* find the first PS in the zone ABOVE this one
       IF that PS is REV Its 2nd end is sc->g2p[K]
     else its first end is sc->g2p[K]
     */
                    if(Find_First_Relevant_Pside(Zone,pp,&pout,ABOVE, &trev)){
                        sc->g2p[K] = pout->Ep(trev);
#ifdef DEBUG				  	   
                        //  	 printf(" G2P[%d]=%s\n",K,sc->g2p[K]->name());
#endif
                        sct = (sc_ptr  )pout->sc;
                        if(!sct->gauss) sc->G2Fac[K]=1.0;
                        else
                            sc->G2Fac[K]=0.5;
                    }
                    else  {
                        sc->g2p[K] = NULL;
                        printf("(C) '%s' rev=%d G2P[%d]=NULL\n",sco->name(),rev, K );
                    }
                }
                else {
                    /* find the first PS in the zone BELOW this one
       IF that PS is REV Its first end is sc->g2p[K]
     else its second end is sc->g2p[K]
     */
                    if(Find_First_Relevant_Pside(Zone,pp,&pout,BELOW, &trev)){
                        sc->g2p[K] = pout->Ep(1-trev);
#ifdef DEBUG				  	
                        //	  	 printf(" G2P[%d]=%s\n",K,sc->g2p[K]->name());
#endif
                        sct = (sc_ptr  )pout->sc;
                        if(!sct->gauss) sc->G2Fac[K]=1.0;
                        else
                            sc->G2Fac[K]=0.5;
                    }
                    else  {
                        sc->g2p[K] = NULL;
                        printf("(D) '%s' rev=%d G2P[%d]=NULL\n",sco->name(),rev, K );
                    }
                }
            }
        }
        Fix_Up_Integration_For_Triangles(sc);
    }
    return 1;
}
int Set_WidthPtrs( RXEntity_p e,int K){
    /*	If the Ksidezone now exists, we fill in the SCs	g<K>p and G<K>fac
 The zone tracer might as well fill in g<n>p and G<n>Fac on all the gaussian SCs it touches
 */
    sc_ptr sc;

    int psn;
    PSIDEPTR np;
    Panelptr  Zone;
    sc = (sc_ptr)e;
    for(psn=0;psn<sc->npss;psn++ ) {
        np = sc->pslist[psn];
        if(np->leftZone[K]){
            Zone = np->leftZone[K];
#ifdef DEBUG
            printf(" SIDE K=%d\n",K);
#endif 
            Fill_In_Gnps(Zone);
        }
    }

    return 1;
}

int PCZ_Trace_Zone(sc_ptr sc,int k,char*test){ /* generates g<n>p on all the SCs it touches */
    /*	attempt to trace a ZONE  seeded on side k of sc e;
     In the process, we may meet SCs which need finishing or need psides
     IF they need finishing, we stop this trace and return
     If they need psides, we break them

     The ZONE tracer is like Paneller except that it ignores seams that are not Gaussian OR edges
      so it cant use

  if test is null, it uses the old gauss zone tests.
  otherwise it accepts scs whose atts contain the string
      */
///GOAT  why do we get 100 zones on this  WRONG
#define Zone_BLOCK 10
    int OK=1;
    int psn;

    static int count=0;
    PSIDEPTR np;
    RXEntity_p estart,pivot;
    Panelptr  Zone;

    char Zone_Name[256];

    if(!test && !sc->gauss) return 0;
    if(test && !sc->AttributeFind(test)) return 0;

    for(psn=0;psn<sc->npss;psn++) {

        np = sc->pslist[psn];
        /* try to start a Zone firstly by going forwards on
  the left edge (K=0), or going backwards on right edge (ifK=1)
  */

        if( (np->leftZone[k] ==NULL) ) {
            sprintf(Zone_Name,"ZONE%d",count++);
           // sc->OutputToClient("zones are under reconstruction\n",3);
            char buf[512]; sprintf(buf," seed is<%s> ",np->name());
            RXEntity_p epp= sc->Esail->Insert_Entity( "zone",Zone_Name,NULL,(long)0,buf,"!Zone generated");
            Zone  = dynamic_cast< Panelptr> (epp);

            (*Zone).m_psidecount=1;
            (*Zone).reversed=(int*)MALLOC(Zone_BLOCK*sizeof(int));
            (*Zone).m_pslist= (PSIDEPTR *)MALLOC(Zone_BLOCK*sizeof(PSIDEPTR ));
            (*Zone).reversed[0]=k;
            ((*Zone).m_pslist)[0]=np;
            Zone->allocsize=Zone_BLOCK;

            estart=np->Ep(k);	/* starting node */
            pivot = np->Ep(1-k);   /* first pivot */

            OK= PCZ_Grow_Zone(pivot,np,Zone,estart,test);

            /* returns 0 if it cant close a Zone. otherwise the count */

            if (!OK) {
                printf(" failed to trace zone on %s\n", sc->name());
                Zone->Clear_Zone_Flags();
                delete Zone;
            }
            else {  /* insert the entity and set leftZone on its psides */

                RXEntity_p ep =Zone;
                int side,l;

                sc->SetRelationOf(ep,spawn,RXO_ZONE_OF_SC,Zone->reversed [0]); // NOT SURE!!!
                Zone->Needs_ReGridding = 1;
                Zone->Geometry_Has_Changed = 1;
                Zone->SetNeedsComputing(0);
                for (l=0;l<Zone->m_psidecount;l++) {
                    PSIDEPTR pss;
                    side = Zone->reversed[l];
                    pss = Zone->m_pslist[l];
                    pss->leftZone[side] = Zone;
                }
            }
        }

    }

    return 1;
}

int Get_Gauss_Endwidths(RXEntity_p e){
    sc_ptr sc;
    PSIDEPTR ps=NULL;

    int k,OK=0;
    double s=0.0,dist=0.0;
    double e1w=0.0,e2w=0.0;
    Site *p=NULL;
    ON_3dPoint r; ON_3dVector t;
    sc = (sc_ptr  )e;


    /*A) if e isnt broken, break it  */
    if(e->Needs_Finishing) return(0);	 /* if called from finish_Entity */
    sc->Break_Curve();
    /*
 B) For each side K
    Look at the Kside zone  of the first and last psides
    If either is NULL
    attempt to trace a ZONE
     In the process, we may meet SCs which need finishing or need psides
     IF they need finishing, we stop this trace and try the other side
     If they need psides, we break them

    If the Ksidezone now exists, we fill in the SCs	g<K>p and G<K>fac
    The zone tracer might as well fill in g<n>p on all the gaussian SCs it touches
       The ZONE tracer is like Paneller except that it ignores seams that are not Gaussian OR edges
      NOTE that Delete_Pside deletes any ZONES on it and remove any reference to them in
       other psides
   */

    for(k=0;k<2;k++) {
        OK=0;
        if(sc->npss) {
            ps = sc->pslist[0];
            if(ps->leftZone[k]) OK=1;
            ps = sc->pslist[sc->npss-1];
            if(ps->leftZone[k]) OK=1;
            if(!OK)	{
                PCZ_Trace_Zone(sc,k,NULL);
                Set_WidthPtrs(e,k); /* generates g<n>p on all the SCs it touches */
            }
        }
    }
    /*
 C)
   if  g1p exists, recalculate e1w by dropping a perp from g1p[0],g1p[2] and multiplying
        by g1fac
 else leave unchanged
   if g2p exists recalculate e2w by dropping a perp from g2p[0],g2p[2] and multiplying
        by g2fac
    else leave unchanged
 */

    e1w=0.0; e2w=0.0;
    OK=1;
    /* the red-space method.
 We get GnP(k) via get_Position using endNsign+1
 Then we average the perp distances to curves 0 and 2

   for(k=0;k<3;k+=2){
 s=0.0;
   if(sc->g1p[k]) {
    Get_Position(sc->g1p[k],NULL, sc->end1sign+1,&v);
     if(!PC_Drop_ Perp endicular(&v, sc->p[0],sc->c[0],(float)0.001,&s,&dist,0)) OK=0;
   else
    e1w += dist *sc->G1Fac[k] ;
     if(!PC_Drop_ Perp endicular(&v, sc->p[2],sc->c[2],(float)0.001,&s,&dist,0)) OK=0;
   else
     e1w += dist *sc->G1Fac[k] ;
    }
  }
  if(OK) sc->e1w=e1w/2.0;
          else  { printf("Bad e1w in %s\n",e->name()); sc->e1w=0.0; }

  OK=1;
  s = sc->arcs[0];
   for(k=0;k<3;k+=2){
   if(sc->g2p[k]) {
    Get_Position(sc->g2p[k],NULL,sc->end2sign+1,&v);
     if(!PC_Drop_ Perp endicular(&v, sc->p[0],sc->c[0],(float)0.001,&s,&dist,0))OK=0;
   else
     e2w += dist *sc->G2Fac[k] ;
     if(!PC_Drop_ Perp endicular(&v, sc->p[2],sc->c[2],(float)0.001,&s,&dist,0))OK=0;
   else
     e2w += dist *sc->G2Fac[k] ;
    }
  }
  if(OK) sc->e2w=e2w/2.0;
else  { printf("Bad red e2w in %s\n",e->name()); sc->e2w=0.0; }
*/
    /*  the black-space method */

    e1w=0.0; e2w=0.0;
    OK=0;

    for(k=0;k<3;k+=2){
        if(sc->g1p[k]) {
            p =(Site*)sc->g1p[k];  s=0.0;
            if(0< sc->m_pC[1]->Drop_To_Curve( p->ToONPoint() , &r,&t,(double)0.001,&s,&dist ) ){
                OK++;
                e1w += dist *sc->G1Fac[k] ;}
        }
    }
    if(OK)
        sc->e1w=e1w;
    else  {
        printf("Bad black e1w in %s\n",e->name());
        if(sc->g1p[0]) printf(" g1p[0]=%s ", sc->g1p[0]->name());
        if(sc->g1p[2]) printf(" g1p[2]=%s ", sc->g1p[2]->name());
        printf("\n");
        sc->e1w=0.0;
    }
    OK=0;
    for(k=0;k<3;k+=2){
        if(sc->g2p[k]) {
            p =(Site*)sc->g2p[k];
            s = sc->GetArc(1);
            if(0< sc->m_pC[1]->Drop_To_Curve( p->ToONPoint(), &r,&t,(double)0.001,&s,&dist )) {
                OK++;
                e2w += dist *sc->G2Fac[k] ;
            }
        }
    }
    if(OK)
        sc->e2w=e2w;
    else  {
        printf("Bad black e2w in %s",e->name());
        if(sc->g2p[0]) printf(" g2p[0]=%s ", sc->g2p[0]->name());
        if(sc->g2p[2]) printf(" g2p[2]=%s ", sc->g2p[2]->name());
        printf("\n");
        sc->e2w=0.0;
    }




    return(1);

}

int Dprint( char*a, double*p,int c) {
    int k;
    //if(!TRACE) return 0;
    printf("%s",a);
    for(k=0;k<c;k++) printf("%d\t%15.8f\n",k, p[k]);
    return 1;
}

int Remove_Duplicate_Points(VECTOR*r,int *c) {
    int i,j;
    for(i=0;i<(*c-1);i++) {
        if(fabs(r[i].x - r[i+1].x)< FLOAT_TOLERANCE) {
            for(j=i;j<(*c-2);j++) {
                PC_Vector_Copy(r[j+1], &(r[j]));
            }
            i--;
            (*c)--;

        }
    }
    return 1;
}
static int Compare_Points( const void*va,const void*vb) { // for qsort

    VECTOR*ia = (VECTOR*) va;
    VECTOR*ib = (VECTOR*) vb;

    if (ia->x <ib->x)  return(-1);
    if (ia->x >ib->x)  return( 1);
    return 0;
}

int PC_Add_Polylines(VECTOR **base, int*bc,VECTOR**gcurve,int *gc){
    /*  base and gcurve are two polylines
 We want to generate a third polyline where the y ordinates are the sum of the interpolated
values for both the others 
METHOD
  Start building a new polyline <r>.
  For each point in p1, interpolate a value from p2 and add.
   Append the result to <r>
   Now do the same for each point in p2

  Now sort <r> by x
      Now remove duplicate points from <r>
       Finally realloc (*base) and copy <r> into it
    */

    int i,j;
    VECTOR  p, *p1,*p2, *r=NULL;
    int c1,c2,c=0;
    double x,f;

    if(*gc==0) return 0;

    p2= *gcurve; p1=*base; c2=*gc, c1=*bc;
    PC_Polyline_Length( c1, p1);
    PC_Polyline_Length( c2, p2);

    for(i=0;i<c1;i++) {
        x = p.x= p1[i].x;
        p.y =p1[i].y ;
        p.z =p1[i].z ;
        for(j=0;j<c2-1;j++) {
            if(p2[j].x < x &&  p2[j+1].x >= x ) {
                f = (x-	p2[j].x)/( p2[j+1].x  - p2[j].x);
                p.y =p.y + (1.0-f)* p2[j].y + f *  p2[j+1].y;
                p.z =p.z + (1.0-f)* p2[j].z + f *  p2[j+1].z;
                break;
            }
        }
        Append_To_Poly(&r, &c,&p,1);
    }
    for(i=0;i<c2;i++) {
        x = p.x= p2[i].x;
        p.y =p2[i].y ;
        p.z =p2[i].z ;
        for(j=0;j<c1-1;j++) {
            if(p1[j].x < x &&  p1[j+1].x >= x ) {
                f = (x-p1[j].x)/( p1[j+1].x  - p1[j].x);
                p.y =p.y + (1.0-f)* p1[j].y + f *  p1[j+1].y;
                p.z =p.z + (1.0-f)* p1[j].z + f *  p1[j+1].z;
                break;
            }
        }
        Append_To_Poly(&r, &c,&p,1);
    }

    qsort(r,c,sizeof(VECTOR),Compare_Points);
    Remove_Duplicate_Points(r,&c);

    *base=(VECTOR *)REALLOC (*base, c*sizeof(VECTOR));
    memcpy(*base, r, c*sizeof(VECTOR));
    RXFREE(r);
    *bc=c;


    return 1;

}

int Get_Starting_Widths( sc_ptr p, int np,  double *s,  double *Y,double *Ref_Width){
    /* gets some widths from linear interpolation from the SCs endwidths */
    double f,f1;
    int k;
    *Ref_Width=0.01;	/* a nominal value to stop it running for ever */

    for(k=0;k<np;k++) {
        f = s[k] /s[np-1];
        f1 = 1.0-f;
        Y[k] = p->e1w * f1 + p->e2w * f;
        if(Y[k] > *Ref_Width )  *Ref_Width= Y[k];
    }

    return 1;
}
int Draw_Integration_Polyline(const ON_3dPoint e1r,const ON_3dPoint e2r,double *X,double *s1w,double *Yk,int nks){

#ifdef HOOPS
    int k;
    float c,dx,dy;
    VECTOR *pts ;

    return 0;
    pts = (VECTOR *)MALLOC((2*nks+2)*sizeof(VECTOR));

    HC_Open_Segment("gp");
    HC_Flush_Contents(".","everything");
    for(k=0;k<nks;k++){
        pts[k].x =  pts[2*nks - k-1].x = X[k];
        pts[k].z =  pts[2*nks - k-1].z = 0.5;
        pts[k].y =  (s1w[k]+Yk[k])/2.0;
        pts[2*nks - k-1].y = - pts[k].y;
    }
    HC_Insert_Polygon(2*nks,pts);
    HC_Set_Color("edges=magenta,faces=periwinkle");
    dy = e2r.y - e1r.y;
    dx = e2r.x - e1r.x;
    c = atan2(dy,dx ) *180.0/ (4.0*atan(1.0));
    HC_Rotate_Object(0.0,0.0,c);
    HC_Translate_Object(e1r.x,e1r.y,e1r.z);
    ;


    HC_Close_Segment();

    RXFREE(pts);
    return 1;
#else
     return 0;
#endif
}
int Get_Gaussian_Seam(sc_ptr p,VECTOR**base,int*bc,
                      const RXSitePt &e1b,const RXSitePt &e2b,
                      const RXSitePt &e1r,const RXSitePt &e2r,
                      float Red_Chord,double*e1a,double*e2a)
{	 

    int iret=1;
    double l_Atol = p->Esail ->m_Angle_Tol ;
    /* Operation
  1) Sample 'enough' K values	along the black-space line e1b to e2b
    2) Approximate the integration widths by linear interpolation from e1w, e2w
   3) Map the black-space S's onto the red-space -
   Integrate to get the spline coefficients	for Y as fn of red-space S
     4) Evaluate Y at the K sample points - or more if Y varies a lot with S
   Check for changes from previous widths
     5)  If changes Loop to 3 repeat the integration using the new widths
    6) Sample 'enough' Y values from the spline and return in 'base,bc'

  Variables are
  Ks		   The K   values at knots. all oced in 	Sample Enough Surface Values
  Sb		   The arc values at knots. all oced in 	Sample Enough Surface Values
  slw			straight-line-width
  Yk			offsets at the knots
  X		   red-space arc-lengths from start
  Kw			Gauss curvature times width, at knots
      */
    int k, nks, Still_Going, count=100;

    double *Ks=NULL, *Sb=NULL,*slw=NULL,*Yk=NULL,  *X=NULL, *Kw=NULL;
    struct PC_SPLINE spline;
    double Ref_Width, Ymax, yn, tol=0.0001;/* WAS .005 Sept 97 PH */  /* Keep looping if width changed by a factor > this */

    if(!p->gauss) {*bc=0; return 0;}
    if(! Get_Gauss_Endwidths(p)) {
        *bc=0;
        return(0);
    }
    memset(&spline,0, sizeof( struct PC_SPLINE) );

    /*  Sample 'enough' K values along the black-space line e1b to e2b
      really we should start with nks=3 and fill in the gaps by error analysis
       so this is really just a test	*/
    nks=9;
    if(!ON_IsValid(e1b.m_u)) {


    }
    if(!ON_IsValid(e1b.m_v)) {


    }
    Sample_Enough_Surface_Values(e1b,e2b,p->e1w,p->e2w,p->gauss,&Ks,&Sb,&nks);
    for(k=0;k<nks;k++) {
        if(	fabs(Ks[k] ) > 1E5) {
            printf(	 "  %d %15.6g\n", k,	Ks[k] );
            printf(	 "  e1b  (%f %f %f)   \n", 	e1b.x,e1b.y,e1b.z);
            printf(	 "  e2b  (%f %f %f)   \n", 	e2b.x,e2b.y,e2b.z);
            p->OutputToClient("unreasonable K value",1);  Ks[k]=0.0;
        }
    }
    /* Approximate the integration widths by linear interpolation from e1w, e2w */
    slw = (double*)CALLOC((nks+1), sizeof(double));
    X = (double*)CALLOC((nks+1),sizeof(double));
    Yk = (double*)CALLOC((nks+1),sizeof(double));
    Kw = (double*)CALLOC((nks+1),sizeof(double));
    Get_Starting_Widths( p, nks, Sb, slw,&Ref_Width);		 /* KW will be K x (slw + y) */

    /*  Map the black-space S's onto the red-space - */
    /* 			Arc_Corr = (p->arcs[2]/p->arcs[0])  ; Arc_Corr =Arc_Corr * Arc_Corr; */
    /* printf(" redarc correction (cube= %f) 0/2=%f\n", ((p->arcs[2]/Red_Chord)) *((p->arcs[2]/Red_Chord)) *((p->arcs[2]/Red_Chord)) ,p->arcs[0]/p->arcs[2]);
 arcs[2] wont be the same as arcs[0] */
    for(k=1;k<nks;k++)	{  X[k]  =  Sb[k]/Sb[nks-1]*Red_Chord;		Yk[k] =  0.0;}

    do{		 p->GaussArea = 0.0;
        for(k=0;k<nks;k++) {

            Kw[k]= - Ks[k]*(slw[k] + Yk[k]) ;
            p->GaussArea += simpson(k,nks)* (slw[k] + Yk[k]) *X[1] /3.0;

            if(	fabs(	Kw[k] ) > 1E5) {
                printf(	 "  %d  Kw=%15.6g Ks=%15.6g slw[k]=%15.6g Yk[k]=%15.6g\n", k,	Kw[k] ,Ks[k],slw[k] ,Yk[k]);
                printf(	 "  e1b  (%f %f %f)   \n", 	e1b.x,e1b.y,e1b.z);
                printf(	 "  e2b  (%f %f %f)   \n", 	e2b.x,e2b.y,e2b.z);
                p->OutputToClient("unreasonable K value(2)",1);Kw[k]=Ks[k]=0.0;
            }
        }

        /*	 Integrate to get the spline coefficients	for Y as fn of red-space S	 */
        Get_Gauss_Spline(p, nks,X, Kw,&spline); // callocs spline->segs
        /*Print_Spline(&spline); */
        /*  Verify that the spline has the right curvatures
      cout<< "( k,X,Kw,Curvbelow above) \n"<<endl;
   for(k=0;k<nks;k++) {
       double z1,z2;
        printf(" %d %f  \n", k, X[k]);
       z1= PC_Spline_Curvature(&spline , X[k]-.001);
       z2= PC_Spline_Curvature(&spline , X[k]+.001);
       printf(" %f  %f   %f\n",  Kw[k] , z1,z2);
   } */
        /* Eva luate Y at the K sample points - or more if Y varies a lot with S
       Check for changes from previous widths									 */
        Still_Going=0; Ymax=0.0;
        for(k=0;k<nks;k++) {
            yn = PC_Spline_Eval(&spline , X[k]);
            if(fabs(yn) > Ymax) Ymax=fabs(yn);
            if(fabs(yn-Yk[k]) > tol*Ref_Width )  Still_Going=1;
            Yk[k] =yn;
        }

        /*  If changes Loop to 3 repeat the  integration using the new widths   */
    } while((count--) && Still_Going);


    Draw_Integration_Polyline(e1r.ToONPoint(),e2r.ToONPoint(),X,slw,Yk,nks);


    /*  Sample 'enough' Y values from the spline and return in 'base,bc'
   The tolerances are 1) fraction of chord	. 2) absolute value of sagitta, 3) max angle  (rads)
 */
    if(Ymax < 0.001) Ymax=0.001;
    *bc=255;

    Sample_Enough_Spline_Points(&spline, base,bc, 0.01,(0.01*Ymax),(l_Atol*100.0));
    /* here the Y's will be double of the Y's needed for seaming. Lets half it now */
    for(k=0;k<  *bc; k++) (*base)[k].y = (*base)[k].y /2.0;

    *e1a = PC_Spline_Gradient(&spline,  0.0);
    *e2a =  - PC_Spline_Gradient(&spline,   spline.segs[spline.ns-1].x2);


    //	if(spline.status==1)
    if(spline.segs)
    {RXFREE(spline.segs); spline.segs=NULL;}
    if(Ks) RXFREE(Ks);
    if(Sb) RXFREE(Sb);
    if(slw) RXFREE( slw);
    if(Yk ) RXFREE( Yk );
    if( X   ) RXFREE(	X   );
    if(Kw  ) RXFREE( Kw  ) ;
    return iret;
}
#ifdef NEVER
int OldGet_Gaussian_Seam(sc_ptr p,  VECTOR**base,int*bc, VECTOR e1b,VECTOR e2b,float Red_Chord){

    double *y=NULL, *w=NULL, *Kw=NULL, *yold=NULL,*K=NULL;
    double l_ds,x,xx,yy,zz,g,glast,endy,err,f,f1;
    int k,np,flip,count=10,c2,changing,k2,k21;	 /* 21/3/96 count was 100 but that allows np=1E30 */

    if(!(p->gauss)) return(0);

    if(! Get_Gauss_Endwidths(p))
        return(0);

    y = (double *)CALLOC(10,sizeof(double));
    yold = (double *)CALLOC(10,sizeof(double));

    /* c =PC_Dist(e1b,e2b); */
    np = 4;
    do {
        err = 0.0;
        np=np*2;
        y = (double *)REALLOC(y,(np+1)*sizeof(double));
        yold = (double *)REALLOC(yold,(np+1)*sizeof(double));
        w = (double *)REALLOC(w,(np+1)*sizeof(double));
        w[0] = 0.0;
        Kw = (double *)REALLOC(Kw,(np+1)*sizeof(double));
        K = (double *)REALLOC(K,(np+1)*sizeof(double));
        if(!K) rxerror("out of memory",3);
        l_ds = Red_Chord /(double)np;
        c2 = 100;
        flip = -1;	/* BUG 28/5/95 WAS +1 */
        for(k=0;k<np+1;k++) {
            k2 = k/2; k21 = k/2 + 1;
            if(flip ==1)
                yold[k] = (y[k2] + y[k21])/2.0;
            else
                yold[k] = y[k2];
            flip=-flip;
        }
        /* get K,w,Kw at node points in Black Space*/
        for(k=0;k<=np;k++) {
            f =  (double)k   /(double)np;
            f1 = 1.0-f;
            xx = e1b.x*f1 + e2b.x*f;
            yy = e1b.y*f1 + e2b.y*f;
            zz = e1b.z*f1 + e2b.z*f;
            K[k] = PC_Interpolate(p->gauss,xx,yy,zz);
            w[k] = p->e1w * f1 + p->e2w * f;
        }

        do { /* in this loop we solve for Y with np constant.  measure error (sagitta) */
            err=0.0;
            for(k=0;k<=np;k++) {
                Kw[k] = K[k] * (w[k] + yold[k]);
                if (fabs(l_ds*l_ds/8.0*Kw[k]) > err) err = fabs(l_ds*l_ds/8.0*Kw[k]);
            }
            g = 0.0;  glast=0.0;

            /* trapezium rule integration in red space courtesy of ds */
            for(k=1;k<=np;k++) {
                g = g - (Kw[k] + Kw[k-1])/2.0 * l_ds;	/* gradient at full-point */
                y[k] = y[k-1]  + (g+glast)/2.0 * l_ds;
                glast = g;

            }
            endy = y[np];
            changing = 0;

            for(k=1;k<=np;k++) {
                y[k] = y[k] - (double)k/(double)np * endy;
                if(fabs(y[k]-yold[k]) > p->Esail ->m_Linear_Tol) changing=1;
                yold[k] = y[k];
            }
        }while (changing &&c2--);

        /*	printf(" done inner loop in %d ",99-c2);
 printf(" np %d and sagitta err = %f\n ",np, err);  */
    } while (err > endy/800.0 && err > p->Esail ->m_Linear_Tol &&count--);
    /* here we should have Y values corresponding to  the GAUSS field */
    if(count <=0) {
        printf(" %s NOT CONVERGED err= %f endy=%f np=%d \n",p->name(),err,endy,np);
    }


    (*base) = (VECTOR *)REALLOC((*base),(np+2)*sizeof(VECTOR));
    x = 0.0;
    for (k=0;k<=np;k++) {
        (*base)[k].x = (float) x;
        (*base)[k].y = (float) (y[k]/2.0);  /* halve because the curve will be applied each side */
        (*base)[k].z = (float)0.0;
        x = x + l_ds;
    }

    *bc = np + 1;

    RXFREE(y); RXFREE(w); RXFREE(Kw);


    return(1);
}

#endif
