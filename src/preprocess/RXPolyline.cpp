#include "StdAfx.h"

#include "RXPolyline.h"

RXPolyline::RXPolyline(void)

{
m_p.clear(); 
}

RXPolyline::RXPolyline( ON_3dPointArray &p)
{
 assert(0);
}
RXPolyline::~RXPolyline(void)
{
 this->m_p.clear();

}

RXPolyline& RXPolyline::operator=(const RXPolyline& src)
{  
  if ( this != &src ) {
		this->m_p.clear ();
 
	for (vector<RXTriplet>::const_iterator it= src.m_p.begin(); it!=src.m_p.end();++it) {
		RXTriplet t = *it;
		this->m_p.push_back(t);
	}

  }
  return *this;
}

RXTriplet& RXPolyline::operator[]( int i ){
RXTriplet& p = this->m_p[i];
return p;
}

int RXPolyline::ToVectorArray( VECTOR **p, int*p_c) {
	int c= size();   
	*p=new VECTOR[c];int k=0; // LEAK need delete[] 
	for (vector<RXTriplet>::iterator it= m_p.begin(); it!=m_p.end();++it,++k) {
		RXTriplet t = *it;
		ON_3dPoint q = t.ToONPoint();
		(*p)[k].x = q.x  ;(*p)[k].y =  q.y ;(*p)[k].z = q.z  ;
	}

   *p_c=c;
	return c;
}
RXSTRING RXPolyline::TellMeAbout(void) const
{
RXSTRING rc(_T("RXPolyline  ")); rc+= TOSTRING(size());
return rc;
}
int RXPolyline::Dump(FILE *fp) const	{
	int rc=0;
	for (int k=0;k<size() ;k++){
		ON_3dPoint v= this->m_p[k].ToONPoint();
		rc+=fprintf(fp,"\t\t\t%d %f\t%f\t%f\n",k, v.x,v.y,v.z);
	} 
	 rc+= fprintf(fp,"\n");  
	return 0;
}
   int  RXPolyline::Group(const int nmax, const double tol)
   {
       int rc=0;
       if(this->size()<=nmax)
           return 0;

       assert("TODO : group RXpolyline"==0);
       //
       return rc;
   }
// see Group_Poyline
//   int Group_Polyline( VECTOR *p, int *c, int n, double p_tol){

//       /* if there are more than n members of p, cluster them into n points
//      at 1/n,2/n, 3/n, etc
   // else (there are less) maYBE we want to bin any close points together.
//      STRIP values at 0.0 or 1.0
//         */
//       float xmax,xmin,dx,x,xl,xh;
//       float y,z,*Y,*Z;
//       int j,k;

//       if(*c <= n) return(0);
//       printf(" grouping from %d to %d\n", *c,n);
//       Y = (float *)MALLOC(n*sizeof(float));
//       Z = (float *)MALLOC(n*sizeof(float));
//       xmax = (float) 1.0;
//       xmin = (float) 0.0;
//       dx = (xmax - xmin)/(float)(n+1);

//       x = (float) 0.0;
//       xl = (float) p_tol;
//       for(k=0;k<n;k++) {
//           x = x + dx;
//           xh = x + dx/(float) 2.0;
//           if(k==n-1) xh=xmax-(float) p_tol;
//           y=z=(float) 0.0;
//           for(j=0;j<*c;j++) {
//               if(p[j].x >= xl && p[j].x < xh) {
//                   y=y+p[j].y;
//                   z=z+p[j].z;
//                   /*	printf(" %d %f %f into %d (%f)\n",j,p[j].x,p[j].y, k,x); */
//               }
//           }
//           Y[k] = y; Z[k]=z;
//           xl = xh;
//       }
//       *c=n;
//       x = dx;
//       for(k=0;k<*c;k++) {
//           p[k].x = x;
//           p[k].y= Y[k];
//           p[k].z= Z[k];
//           /*	   printf("grouped %d %f %f %f\n",k,x,Y[k],Z[k]); */
//           x = x + dx;
//       }
//       RXFREE(Y); RXFREE(Z);
//       return(1);
//   }

