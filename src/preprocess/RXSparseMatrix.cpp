#include "StdAfx.h"
#include <QDebug>
#include "mkl_spblas.h"
// for DSS
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include "mkl_dss.h"
#include "mkl_types.h"
#include "RXSparseMatrix.h"
/* IMPORTANT. RESTRICTIONS
the first implementation enables a matrix to be assembled in Map form , copied to CSC form and
solved using Pardiso.
We configure Pardiso for a  full solution from cold on a symmetric matrix. 
Therefore it is necessary that the matrix is symmetric.
Pardiso requires the matric in UpperTriangular form (col >= row), using fortran indexing.
our map form is (r,c,val) which matches fortran ordering. 

*/
/*

A RX Sparse Matrix may be stored either in map form or CSC form
 we provide method SetType which copies it from one format to the other.
 (but temporarily uses double the memory)

It is normally easier to create and operate on the matrix in map form
 a MM file is naturally mapstyle and a harwell-boeing file is naturally CSC.


Use  the C style interface for calling from fortran
extern "C" class RXSparse Matrix *NewSparse Matrix(){ return new RXSparse Matrix();}

extern "C" void AddToSparse Matrix(RXSparse Matrix *m,const int i,const int j, const double v);

Once you've built it, you  can Write it or Solve it. 

We should also code up MKL matrix multiplication, addition and preconditioning.
And submatrix extraction and insertion

and finally if we want to destroy it from C or fortran
extern "C" void DestroySparseMatrix( class RXSparseMatrix *m) {delete m;}
*/
/* managing the matrix type

We can ask User to choose the matrix type( sym, posDef, etc) at creation time.
Then we can use that to set the file write and solver parameters.
Specifically
Is its symmetric we only store the upper diagonal. It's an error to set an element
with row>col.
If its symmetric 


*/


RXSparseMatrix::RXSparseMatrix(const MatrixType t)
:m_nRhsVecs(0)
,m_StorageType(MAPSTYLE)
,m_MatType(t)
 
{
m_hbio.SetMatrixType(t);
}

RXSparseMatrix::RXSparseMatrix(const map<pair<int,int>,double> m,const MatrixType t)
:m_nRhsVecs(0)
,m_StorageType(MAPSTYLE)
,m_MatType(t)
{
Init();
SetType(MAPSTYLE); int nr=-1,nc=-1;
int r,c; // do it one at a time so we can tot up nr and nc
	map<pair<int,int>,double>::const_iterator it;
	for(it=m.begin() ;it!=m.end();++it) {
		r = it->first.first; nr=max(nr,r+1);
		c = it->first.second; nc=max(c+1,nc);
		this->m_map[pair<int,int>(r,c) ]=it->second;
	}
    SetNr(nr); SetNc(nc);
}

RXSparseMatrix::~RXSparseMatrix(void)
{
Init();
}

int RXSparseMatrix::Init(void)
{
 	this->m_hbio.Init();
	this->m_map.clear();
	rHs.clear();
    SetNr(0); SetNc(0);
	m_StorageType=MAPSTYLE;
	return 0;
}

const RXSparseMatrix RXSparseMatrix::operator+(const RXSparseMatrix& y)const{
		RXSparseMatrix rv(MTNONE ); rv.SetType(NONE);
		if(this->m_MatType !=y.m_MatType ) return rv;
		if(this->m_StorageType !=MAPSTYLE) return rv;		 
		if(y.m_StorageType !=MAPSTYLE) return rv;
		rv.SetMatrixType( this->m_MatType );
		if(this->m_map.size() != y.m_map.size()) return rv;
        if(this->Nr() != y.Nr()) return rv;
        if(this->Nc() != y.Nc()) return rv;
		int r,c;

		rv= *this; rv.m_map.clear();

		map<pair<int,int>,double>::const_iterator it1,it2;
		for(it1=this->m_map.begin(),it2=y.m_map.begin();
			it1!=this->m_map.end()&&it2!=y.m_map.end();
			++it1,++it2)
			{
				r= it1->first .first ; c=it1->first.second;
				if(r!= it2->first.first|| c!=it2->first.second ) { 
					rv.m_map.clear(); return rv;
				}
				rv.m_map[it1->first]=it1->second+it2->second;
		}
		return rv;
	}

const RXSparseMatrix& RXSparseMatrix::operator*=(const double& f){
		SetType(MAPSTYLE);
 		map<pair<int,int>,double> ::iterator it;
		for(it=this->m_map.begin();it!=this->m_map.end();++it)
			{
				it->second*=f;
		}
		 return *this;
	}
const RXSparseMatrix RXSparseMatrix::operator*(const double& f){
		RXSparseMatrix rv(this->m_MatType );
		SetType(MAPSTYLE);
		rv= *this; rv.m_map.clear();
		map<pair<int,int>,double> ::iterator it;
		for(it=this->m_map.begin();it!=this->m_map.end();++it)
		{
				rv.m_map[it->first]=it->second*f;
		}
		rv.SetType(MAPSTYLE);
		return rv;
	}

int RXSparseMatrix::ZeroRow(const int i){
// the comparison takes r first then c
	// so row[i] is between 
	int rc=0;
	map<pair<int,int>,double> ::iterator it,l,h;
    l = m_map.lower_bound(pair<int,int>(i-1,Nc()+1));
    h = m_map.upper_bound(pair<int,int>(i,Nc()+1));
	for(it=l;it!=h;++it){
            it->second =0.; rc++; assert(it->first.first==i);}
	return rc;
}

int RXSparseMatrix::ZeroColumn(const int j){
// the comparison takes r first then c so the column isnt contiguous
// we  just do a trawl
// one way to speed this up would be if we knew the bandwidth

	int rc=0;
	map<pair<int,int>,double> ::iterator it,l,h;
	l = m_map.begin();
	h = m_map.end();

	for(it=l;it!=h;++it){
		if(it->first.second ==j){
			it->second =0.; rc++;
		}
	}
	return rc;
}
/* ATTENTION  this is very crude use of pardiso
 ans the solver parameters are those from the example.
*/
int RXSparseMatrix::SolveDSS(double*presult){ // for the moment, just the Intel example
    /*
    ********************************************************************************
    *   Copyright(C) 2004-2015 Intel Corporation. All Rights Reserved.
    *   The source code contained  or  described herein and all documents related to
    *   the source code ("Material") are owned by Intel Corporation or its suppliers
    *   or licensors.  Title to the  Material remains with  Intel Corporation or its
    *   suppliers and licensors. The Material contains trade secrets and proprietary
    *   and  confidential  information of  Intel or its suppliers and licensors. The
    *   Material  is  protected  by  worldwide  copyright  and trade secret laws and
    *   treaty  provisions. No part of the Material may be used, copied, reproduced,
    *   modified, published, uploaded, posted, transmitted, distributed or disclosed
    *   in any way without Intel's prior express written permission.
    *   No license  under any  patent, copyright, trade secret or other intellectual
    *   property right is granted to or conferred upon you by disclosure or delivery
    *   of the Materials,  either expressly, by implication, inducement, estoppel or
    *   otherwise.  Any  license  under  such  intellectual property  rights must be
    *   express and approved by Intel in writing.
    *
    ********************************************************************************
    *   Content : MKL DSS C example
    *
    ********************************************************************************
    */
    /*
    **
    ** Example program to solve real unsymmetric system of equations.
    ** The example also demonstrates how to solve transposed system ATx=b.
    **
    */
//    #include<stdio.h>
//    #include<stdlib.h>
//    #include<math.h>
//    #include "mkl_dss.h"
//    #include "mkl_types.h"
    /*
    ** Define the array and rhs vectors
    */
    #define NROWS       5
    #define NCOLS       5
    #define NNONZEROS   9
    #define NRHS        1
    static  MKL_INT nRows = NROWS;  // icpc 13 doesnt like const
    static const MKL_INT nCols = NCOLS;
    static const MKL_INT nNonZeros = NNONZEROS;
    static const MKL_INT nRhs = NRHS;
    static MKL_INT rowIndex[NROWS + 1] = { 1, 3, 5, 7, 9, 10 };
    static MKL_INT columns[NNONZEROS] = { 1, 2, 1, 2, 3, 4, 3, 4, 5 };
    static double values[NNONZEROS] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    static _DOUBLE_PRECISION_t rhs[NCOLS * 2];
    static _DOUBLE_PRECISION_t solValues[NROWS] = { 0, 1, 2, 3, 4 };

//    MKL_INT
//    main ()
    {
      MKL_INT i, j;
      /* Allocate storage for the solver handle and the right-hand side. */
      _MKL_DSS_HANDLE_t handle;
      _INTEGER_t error;
      _CHARACTER_t statIn[] = "determinant", *uplo;
      _DOUBLE_PRECISION_t statOut[5], eps = 1e-6;
      MKL_INT opt = MKL_DSS_DEFAULTS, opt1;
      MKL_INT sym = MKL_DSS_NON_SYMMETRIC;
      MKL_INT type = MKL_DSS_INDEFINITE;
    /* --------------------- */
    /* Initialize the solver */
    /* --------------------- */
qDebug()<<" enter SolveDSS";
      error = dss_create (handle, opt);
      if (error != MKL_DSS_SUCCESS)
        goto printError;
    /* ------------------------------------------- */
    /* Define the non-zero structure of the matrix */
    /* ------------------------------------------- */
      error = dss_define_structure (handle, sym, rowIndex, nRows, nCols, columns, nNonZeros);
      if (error != MKL_DSS_SUCCESS)
        goto printError;
    /* ------------------ */
    /* Reorder the matrix */ qDebug()<<" reorder....";
    /* ------------------ */
      error = dss_reorder (handle, opt, 0);
      if (error != MKL_DSS_SUCCESS)
        goto printError;
    /* ------------------ */
    /* Factor the matrix  */ qDebug()<<"factor...";
    /* ------------------ */
      error = dss_factor_real (handle, type, values);
      if (error != MKL_DSS_SUCCESS)
        goto printError;
    /* ------------------------ */
    /* Get the solution vector for Ax=b and ATx=b and check correctness */
    /* ------------------------ */
      for (i = 0; i < 3; i++)
        {
          if (i == 0)
            {
              uplo = "non-transposed";
              opt1 = MKL_DSS_DEFAULTS;
            }
          else if (i == 1)
            {
              uplo = "transposed";
              opt1 = MKL_DSS_TRANSPOSE_SOLVE;
            }
          else
    // Conjugate transposed == transposed for real matrices
          if (i == 2)
            {
              uplo = "conjugate transposed";
              opt1 = MKL_DSS_CONJUGATE_SOLVE;
            }

          printf ("\nSolving %s system...\n", uplo);

    // Compute rhs respectively to uplo to have solution solValue
          mkl_dcsrgemv (uplo, &nRows, values, rowIndex, columns, solValues, rhs);

    // Nullify solution on entry (for sure)
          for (j = 0; j < nCols; j++)
            solValues[j] = 0.0;

    // Apply trans or non-trans option, solve system
          opt |= opt1; qDebug()<<" solve_REal...";
          error = dss_solve_real (handle, opt, rhs, nRhs, solValues);
          if (error != MKL_DSS_SUCCESS)
            goto printError;
          opt &= ~opt1;

    // Check solution vector: should be {0,1,2,3,4}
          for (j = 0; j < nCols; j++)
            {
              if ((solValues[j] > j + eps) || (solValues[j] < j - eps))
                {
                  printf ("Incorrect solution\n");
                  error = 1000 + i;
                  goto printError;
                }
            }
          printf ("Print solution array: ");
          for (j = 0; j < nCols; j++)
            printf (" %g", solValues[j]);

          printf ("\n");
        }
    /* -------------------------- */
    /* Deallocate solver storage  */
    /* -------------------------- */
      error = dss_delete (handle, opt);
      if (error != MKL_DSS_SUCCESS)
        goto printError;
    /* ---------------------- */
    /* Print solution vector  */
    /* ---------------------- */
     fflush(stdout);
        qDebug()<<"\nExample successfully PASSED!\n" ;
      return (0);
    printError:
         fflush(stdout);
       qDebug()<<"Solver returned error code %d\n"<< error;
      exit (1);
    }
}
int RXSparseMatrix::SolveOnce(double*result){
	int rc=0;

	SetType(CSC);

 rc=this->m_hbio.solveunsym(result);

    if(0){
        printf(" %s\n"," i, m_hbio.rhsVal[i],result[i]");
        for(int i=0;i<this->Nr();i++)
			printf(" %d  %f  %f\n", i, m_hbio.rhsval[i],result[i]);
	}

return rc;
}

int RXSparseMatrix::SolveIterative(double*result,const double tol){
    int i,j, rc=0;


    SetType(CSC);
//    rc=this->m_hbio.SolveSym (this->m_hbio.rhsval,result);
    rc=this->m_hbio.solveunsym(result);
    // we want resid = rhs - A.result. We re-solve for resid
    // and we add the new solution to result

  // WIERD because  this test usually shows the first line and some other lines NAN.

    if(m_nRhsVecs!=1)  {  return rc;}
    if(true)  { return rc;} // disable this test
    char transa='N';
    double *resid = new double[(2+Nr())*m_nRhsVecs];
    int nr = this->Nr();
    for(j=0;j<1;j++) {
        cout<<"RXSparseMatrix::SolveIterative refinement "<<j<<endl;
        mkl_dcsrgemv(&transa, &(nr),m_hbio.values,m_hbio.colptr,
                     m_hbio.rowind , result, resid); // resid now Ax

        double e, err = 0.0;
       // double*rd, *b;
        cout<<" I  rhs   pardisoResult  resid  "<<endl;
  //      for(i=0,b=m_hbio.rhsval ,rd=resid;i<nr;i++,b++,rd++){
      for(i=0  ;i<nr;i++ ){
            cout <<i<< "    "<< this->m_hbio.rhsval[i]<<"     "<< result[i]<<"    " << resid[i] <<endl;
            e = m_hbio.rhsval[i]-resid[i];
            if(!(isnan(resid[i])) )err+=(e*e);
        }
        cout<< "error fOr iteration "<< j <<" is "<<sqrt(err)<<endl;
    }
    delete [] resid;
    return rc;
}


enum  smtype RXSparseMatrix::SetType(const enum  smtype tt)
{
    enum  smtype rc = this->m_StorageType;
    switch (tt){
        case NONE:
        switch (rc){
            case NONE:
            case MAPSTYLE:
            case CSC:
            Init();
            return rc;
        };//rc
        break;
        case MAPSTYLE: //new
        switch (rc){ //old
                     case NONE:
                     Init();
                     return rc;
                     case MAPSTYLE:
                     return rc;
                     case CSC:
                     //cout<<"CSCtoMap start"<<endl;
                     this->CSCToMap();
                     //cout<<"CSCtoMap done"<<endl;
                     return rc;
        };//rc
        break;
        case CSC:  //new
        switch (rc){ //old
                     case NONE:
                     Init();
                     return rc;
                     case MAPSTYLE:
                     //cout<<"MaptoCSC start"<<endl;
                     this->MapToCSC ();
                     //cout<<"MaptoCSC done"<<endl;
                     return rc;
                     case CSC:
                     return rc;
        };//rc
        break;
    };///tt

    return rc;
}
int RXSparseMatrix::MapToCSC(void) {
int rc=0;
	this->m_hbio.Init();
	this->m_hbio.Fill( this->m_map ,"OpenFSI Sparse Matrix", "CSCFmt");
	this->m_StorageType = CSC;
	this->m_map.clear();
	this->m_hbio.SetRHS (rHs);

return rc;
}
int RXSparseMatrix::CSCToMap(void){
int rc=0;
int r,c,j,nr,nc;
pair<int,int> ij;
	nc = m_hbio.ncol; nr = this->m_hbio.nrow ;
	this->m_map.clear ();
	for(c=0;c<nc;c++) {
		for(j=m_hbio.colptr[c]; j<m_hbio.colptr [c+1];j++) {
			r = m_hbio.rowind[j];
			ij=pair<int,int> (r,c);
			this->m_map[ij]=m_hbio.values [j];
		}
	}
	this->m_StorageType= MAPSTYLE;
	this->SetRHS(this->m_hbio.nrow,this->m_hbio.rhsval);
	this->m_hbio.Init();
return rc;
}

// RSPD is the type of a finite element matrix

//void DestroySparseMatrix( class RXSparseMatrix *m) {delete m;}

//void  AddToSparseMatrix(RXSparseMatrix *m, const int r,const int c, const double v) {
//	if(m->m_StorageType!=MAPSTYLE)
//		m->SetType(MAPSTYLE);
// cout<<"are you sure this is indexed from Zero???"<<endl;

// if(r>c && m->m_MatType==RSPD || m->m_MatType ==RSI){
//	 cout << " skip setting element "<< r <<" , "<<c<<" on symmetric matrix"<<endl;
//	 return;
// }
//	pair<int,int> ij(r,c);
//	m->nc=max(m->nc,c+1); m->nr=max(m->nr,r+1);
//	map<pair<int,int>,double> ::iterator it;

//	it = m->m_map.find(ij);
//	if(it!=m->m_map.end()) {
//			m->m_map[ij]+=v;
//			return;
//	}
//	m->m_map[ij]=v;
//}

int RXSparseMatrix::SetRHS(const int p_nrhs, double* p_rhs)
{
	int i;
        double*p;
	switch(this->m_StorageType ) {
		case MAPSTYLE:
        this->rHs.resize( this->Nr(),0);
			for(i=0,p=p_rhs ; i<p_nrhs ; i++,p++){
				cout <<*p<<endl;  rHs[i]= *p;
			}
			return p_nrhs;
		case CSC:
			return this->m_hbio.SetRHS(rHs);
		default:
			assert(0);

	};//switch
	return 0;
}


int RXSparseMatrix::MTXWrite(const char*fname) //  the MAP uses indexing starting at zero
{


	FILE *fp=RXFOPEN(fname,"w");
	if(!fp) return 0;
	SetType(MAPSTYLE);
	fprintf(fp,"%%%%MatrixMarket matrix coordinate real general\n%%Created by RX Mesh www.openfsi.org\n");
	int ni=0,nj=0,nel=0;

	map<pair<int,int>,double>::const_iterator it;
	for(it=m_map.begin(); it!=m_map.end();++it) {
		nel++;
		ni=max(ni,it->first.first);
		nj=max(nj,it->first.second);
	}
	fprintf(fp,"%d %d %d\n", ni+1,nj+1,nel);
	for(it=m_map.begin() ;it!=m_map.end();++it) {
		fprintf(fp,"%d %d %.9g\n",it->first.first+1,it->first.second+1,it->second );
	}

	FCLOSE(fp);
	return nel;

}

int RXSparseMatrix::HB_Read(const char*fname)
{
int rc=0;
SetType(CSC);
rc = this->m_hbio.Read(fname);
return rc;
}

int RXSparseMatrix::HB_Write(const char*fname)
{
int rc=0;
SetType(CSC);
rc = this->m_hbio.Write(fname);
return rc;
}
int RXSparseMatrix::IsSymmetric(const map<pair<int,int>,double> &m,const double &tol ){
 // for each (i,j) there must exist a (j,i) with the same value

	int r,c;
	double a;
	map<pair<int,int>,double>::const_iterator it,cr;
	for(it=m.begin();it!=m.end();++it)
			{
				r = it->first.first;
				c = it->first.second;
				if(r==c) continue;
				a = it->second;
				cr = m.find(pair<int,int>(c,r));
				if(cr==m.end())
					return 0;
				if(fabs(a - cr->second) > tol)
					return 0;
		}
return 1;

}
int RXSparseMatrix::MakeUpperTriangular( map<pair<int,int>,double> &m  ){
// remove any elements with c<r

		int r,nr,nc;

	map<pair<int,int>,double>::iterator it1,it2;

	map<pair<int,int>,double>::reverse_iterator e = m.rbegin (); 
	nr  = e->first.first+1;
	nc = e->first.second+1;

	for(r=0;r<nr;r++) {
	it2 = m.find(pair<int,int>(r,r));
	if(it2==m.end()) { cout <<"malformed Sparse Matrix"<<endl; return 0;}
	it1 = m.lower_bound(pair<int,int>(r-1,nc+1));
	if(it2!=m.end()){
			m.erase(it1,it2);
	}

}

return 1;

}
