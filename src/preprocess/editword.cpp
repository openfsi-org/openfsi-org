
#include "StdAfx.h"
#include "RXSail.h"
#include "RXEntity.h"
#include "entities.h"

#include "words.h"
#include "editword.h"
//NOTE.  We need a syntax to change an attribute

// the editword command is unusual in that it can be executed either
// by the command interpreter  (see Do_EditWord )
// or as an entity in a model script. (see Resolve_EditWord_Card )
// the format is identical

// the command-form implementation is much more efficient.
//
//  an edit command is
// editword:genoa:seamcurve:lojo:<insertionPoint>:<newvalue>[newvalue[newvalue]]
// If the entity doesnt exist it is created.
//  - but note that in this case you need the insertion point to be 2
//  eg to make
//      'relsite 	gooseneck	track	BAS	'
//  you would go
//      editword: boat:relsite:gooseneck:2:track:BAS

// Ifthe entity [model/type/name]  already exists, it is modified.
// starting from the word at index <insertionPoint> it replaces consecutive words until the list of newvalues is exhausted.


int Resolve_EditWord_Card(RXEntity_p ein) {


    /* an editword command is
editword$genoa$seamcurve$lojo$5$<newvalue>[newvalue[newvalue]]
If in a script, word[1] may just be a name
Else (in a macro or in summary file) it must be a modelname
 */

    RXEntity_p e;
    int  kk,  nw,  i, insertion_point;
    char **words,*s;
    //printf("Resolve_EditWord_Card with <%s>\n", ein->line);
    s = STRDUP(ein->GetLine());
    nw = make_into_words(s,&words,"$:");
    if(nw>4) {
        e = ein->Esail->GetKeyWithAlias( words[2],words[3]);
        insertion_point = i = atoi(words[4]);
        if(e) {
            e->CClear();
            for(kk=5;kk<nw;kk++,i++){
                Replace_Word(&(e->line),i, words[kk],":");
            }
            ein->CClear( ); // wierd. Why not for a new ent too???
            printf("(Resolve_EditWord_Card ) after UnR type=<%s> name=<%s>\n line=<%s>\n", e->type(),e->name(),e->GetLine());

            e->Needs_Resolving= 1;
            e->SetNeedsComputing();
            e->edited=1;
        }
        else {
            char  *newent;
            int k, depth=1; // changed from 0 Nov 29 2003

            //	sprintf(buf," Couldnt find %s %s in sail %s",words[2],words[3],words[1]);
            //	 error(buf,1);
            // so create one type - words[2] name words[3] then fill in up to 2 with blanks
            //			 then add the other words
            //				 then resolve.
            newent = (char*)MALLOC(strlen( ein->GetLine()) + 8*(nw+insertion_point) );
            sprintf(newent,"%s:%s", words[2],words[3]);
            for (k=2; k< insertion_point;k++)
                strcat(newent,":  ");
            for(k=5;k<nw;k++){
                strcat(newent,":");
                strcat(newent,words[k]);
            }
            Just_Read_Card(ein->Esail ,newent, words[2],words[3],&depth);

            RXFREE(newent);
        }
        ein->Needs_Resolving= 0;
        ein->Needs_Finishing = 0;
        ein->SetNeedsComputing(0);
    }
    RXFREE(words);
    RXFREE(s);
    cout<< "leave Res Editword"<<endl;
    return 1;
}

