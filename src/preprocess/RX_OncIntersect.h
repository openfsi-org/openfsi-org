// RX_OncIntersect.h: interface for the RX_OncIntersect class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RX_ONCINTERSECT_H__DC87D145_22FE_4E09_BDB2_012891A1BFF2__INCLUDED_)
#define AFX_RX_ONCINTERSECT_H__DC87D145_22FE_4E09_BDB2_012891A1BFF2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class RX_OncIntersect //: public RXObject  
{
public:
	RX_OncIntersect();
	virtual ~RX_OncIntersect();

	void Clear();
	void Init();

	RXEntity_p  m_ce;
	const ON_Object *m_ono;
	double m_t;

};

class  RX_ONC_Cpoint //: public RXObject 
{

public:
	RX_ONC_Cpoint();
	virtual ~RX_ONC_Cpoint();
	void Init();
	void Clear();
	RXEntity_p m_pe;	// the point entity if it exists.
	ON_3dPoint m_p;
	const ON_Object *m_ono;
     class RXsubModel *m_sm;
	ON_ClassArray<RX_OncIntersect> m_Clist;  
};


#endif // !defined(AFX_RX_ONCINTERSECT_H__DC87D145_22FE_4E09_BDB2_012891A1BFF2__INCLUDED_)
