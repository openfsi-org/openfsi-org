#pragma once
// RXLogfile is a historic 'database connection' which connects to a CSV file
// and functions as input or as output
// and in addition as a mirror (with luck)

#include "RXDatabaseI.h"
#include "RelaxWorld.h"
#include <list>

#define STRING_LENGTH_TOL ((double) 1.0e-6)
#define MAST_TRIM_TOL ((double) 1.0e-6)
#define STRING_TENSION_TOL ((double) 1.0e-3)
#define MAX_LINE_LENGTH  ((long) 128000)




class RXLogFile :  public RXMirrorDBI
        ,public RXDatabaseCommon, public RXDataBaseLogI//
{
public:
    RXLogFile(void);
    virtual ~RXLogFile(void);

public:
    int Open(const char*filename,const char*what);
    int Close();
    int StartTransaction ();
    int Commit();
    int RowCount();
    int Print(FILE *fp);

    // low-level insert and/or set the value of a DB item
    int post_summary_value(const char*label,const char*value);
    virtual int post_summary_value(const std::string &label,const std::string &value);
    virtual int PostFile( const QString &label,const QString &fname);
    // commits the in-core contents of the RXDBI to file.
    // for CSV files its complicated because they must be square.
    int Write_Line (MIRRORPTR mirrorSource );

    int FlushMirrorTable (); // partial delete

  //  int QQuery(const std::string&q, DBRESULT &r,unsigned int*err)  {assert(0); return -1;} // failure ->return NEGATIVE
    //searches in the DB for 'label'. returns value.
    double Extract_One_Value(const char *label,double*value);
    int Extract_One_Text(const char *label,char*value,const int buflength);
    std::string Extract_Headers(const  string &what); //is 'input', 'output' or 'all'
    int Extract_Column(const char*fname,const char*head,char***clist,int*Nn);
    int Create_Filtered_Summary(MIRRORPTR snew,char *filter);
    int read_and_post_one_row(const int row, const QString runName,MIRRORPTR dest) ;
    int  Apply_Summary_Values() ;
    int Replace_Words_From_Summary(char **strptr, const int append);
public:
    class SUMMARY_ITEM * add_new_item(char *label,char *value);
    int Write_MirrorDB_As_CSV(char *filename) ; // only used for the statefile header
    int RemoveAllWithPrefix(const std::string & prefix);
public: // for now - eventually private
    int GetNCols() const{ return N_Items;}
    class SUMMARY_ITEM * GetListHead() const{ return list;}
    std::string GetFileName()const{ return std::string(this->filename);}
    char * SetFileName(const char*filename);
    std::string ScriptCommand()const;
    void SetLast(const QString h, const QString v);
    // transfer to script format
        virtual int Write_Script (FILE *fp);
   virtual int MakeUnique(const QString &header, QString &value);
private:  
    char *filename;
    FILE *fp;
    char* Fopen_Flags;  /* "r" or "w" */
    int Run_To_End;	/* if set, run all the lines to the end of the file. Else just run this line */

    int List_Has_Changed;
    char sep;
    int type; // ie Summary file, state file, other.
    int  Last_Summary_Line;  // currently its own global. Could be confusing between different summaries.

    int N_Items;
public:
    class SUMMARY_ITEM * list;

private:
    int clean_summary_file(const char *fname);
    int PCS_Strip_Trailing(char *s); // a special (!!!)
    int post_summary_value_from_file(char*label,char*value);
protected:
    int Read_Next_Table_Line(FILE *f,char***wlist,int *d,char sep); //low level - for RXLogFile
    int Update_Summary_File ();
    int OutputToClient(const RXSTRING &s, const int level)const { return g_World->OutputToClient(s, level);}
    int OutputToClient(const char *s, const int level) const { return g_World-> OutputToClient(s, level);}
    int OutputToClient(const std::string &s, const int level)const { return g_World-> OutputToClient(s, level);}

};
