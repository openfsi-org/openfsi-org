#ifndef _RXCONTACTSURFACE_H_
#define _RXCONTACTSURFACE_H_
//#in clude "opennurbs.h"
#include "RXON_Surface.h"
//#inc lude "griddefs.h" // for RXEntity

#define C_GOING_UP    1
#define C_GOING_DOWN -1  // MUST agree with coordinates.f

class RXContactSurface
{
public:
	RXContactSurface(void);
public:
	~RXContactSurface(void);
public:
	int TestIsBelow( double proposedpt[3], double newptuvw[3]); // return 1 if proposed point is UNDER and 

	/* return 0 if the line between oldpt and proposedpt doesnt intersects the surface
, else C_GOING_UP or C_GOING_DOWN
K is the parameter along the path line from oldpt to newpt. */
	int TestPathIntersects( double oldpt[3], double proposedpt[3], double newptxyz[3], double uvw[3], double *k); 

	int Jacobian(double u, double v, double j[9]);
	int Compute(void);	//  flows the surf from c1 to c2
	int Evaluate(const double u1, const double u2, double pt[3]);
	int TestPassEdge(const double u1, const double v1, const double u2, const double v2, double *k);
	static int Resolve(RXEntity_p e);
protected:
	// a pointer to an object in the ONXModel.  DO NOT DELETE.
	const ON_Surface *m_s1;
	// placed in this model's design coordinate space
	RXON_NurbsSurface m_s2;

	RXEntity_p m_c1;
	RXEntity_p m_c2;
	RXEntity_p m_owner;
public:
	int Print(FILE *p_fp);
};
// globals
#endif
