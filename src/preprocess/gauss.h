/* header for the gaussian routines in gauss.c  */


#ifndef GAUSS_16NOV04
#define GAUSS_16NOV04

#include "griddefs.h"

double simpson(int k, int c);
int PCZ_Trace_Zone(sc_ptr e,int k,char*test);
int Get_Gaussian_Seam(sc_ptr p,VECTOR**base,int*bc,
					  const RXSitePt &e1b,const RXSitePt &e2b,
					  const RXSitePt &e1r,const RXSitePt &e2r,
					  float Red_Chord,double*e1a,double*e2a);

int Draw_Integration_Polyline(const ON_3dPoint e1r,const ON_3dPoint e2r,double *X,double *s1w,double *Yk,int nks);

int PC_Add_Polylines(VECTOR **base, int*bc,VECTOR**gcurve,int *gc);
int PCZ_Grow_Zone(RXEntity_p pivot,PSIDEPTR np, Panelptr  Zone,RXEntity_p estart,const char*test);
#endif //#ifndef GAUSS_16NOV04

