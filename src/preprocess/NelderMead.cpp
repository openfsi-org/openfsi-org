// NelderMead.cpp: implementation of the NelderMead class.
//
//////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include <iostream>
using namespace std;

#include "NelderMead.h"

/* This method finds the minimum of the function (x-1)^2 (y-2)^2
Typical useage
1) Construct
2) Specify start point and length scale of each variable
3) run
4) collect results
*/
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ON_SimpleArray<double> NelderMead::GetResult(void ) // returns a vector N+1 with the func value last.
{
ON_SimpleArray<double> rv= ON_SimpleArray<double>(m_MP); rv.SetCount(m_MP);
int i,j;
double q;
	rv[m_MP-1]=0;
	for (j=0;j<m_NP;j++)  {
		rv[j]=0.;  
		m_x[j]=0.0;
		for (i=0;i<m_MP;i++) {
			rv[j] += m_p[i][j];
			q=rv[j];
		}
		rv[j]/=((double)m_MP);
		q=m_x[j]=rv[j];
        }
	rv[m_MP-1]=func(m_x);

return rv;
}
int NelderMead::SetStart( ON_SimpleArray<double> & x ,ON_SimpleArray<double> &scales) // CAREDUL need setFunc first
{
	int n = x.Count();
	m_NP=n; m_MP=n+1;
	m_p.Destroy ();
	m_p = ON_Matrix(m_MP,m_NP);
	m_x.SetCapacity(m_NP);	m_x.SetCount(m_NP);
	m_y.SetCapacity(m_MP); 	m_y.SetCount(m_MP);
//	set the zeroth P to X.
//	set the other Ps to x + diagonalmatrix(scales);
	double q;//,v;
 int i,j;
        for (i=0;i<m_MP;i++) {
			for (j=0;j<m_NP;j++) {
				q=m_p[i][j]=x[j] + (i == (j+1) ? scales[j] : 0.0);
				m_x[j]=m_p[i][j];
			}
          m_y[i]=func(m_x);
        }
//Print(stdout); fflush(stdout);
return 1;
}



NelderMead::NelderMead()
{
   m_ftol=1.0e-10;

   m_nmax=5000;
	m_MP=2;
	m_NP=1;
    m_x.SetCapacity(m_NP);    m_x.SetCapacity(m_NP);
	m_y.SetCapacity(m_MP);	m_y.SetCount(m_MP);

	m_p = ON_Matrix(m_MP,m_NP);
        int i,j;
        for (i=0;i<m_MP;i++) {
			for (j=0;j<m_NP;j++) {
				m_p[i][j]=(i == (j+1) ? 1.0 : 0.0);
				m_x[j]=m_p[i][j];
			}
          m_y[i]=func(m_x);
        }
}

NelderMead::~NelderMead()
{
	m_x.Destroy();
	m_y.Destroy();
	m_p.Destroy ();
}
double NelderMead::func(ON_SimpleArray<double> &x)
{
        return 0.6;
}

bool NelderMead::FindMinimum()
{

	const double TINY=1.0e-10;
	int i,ihi,ilo,inhi,j;
	double rtol,ysave,ytry;

	int mpts=m_p.RowCount();
	int ndim=m_p.ColCount();
	ON_SimpleArray<double> psum(ndim); psum.SetCount(ndim);
	 m_nfuncCalls=0;
	get_psum(m_p,psum);
	for (;;) {
		ilo=0;
		ihi = m_y[0]>m_y[1] ? (inhi=1,0) : (inhi=0,1);
		for (i=0;i<mpts;i++) {
			if (m_y[i] <= m_y[ilo]) ilo=i;
			if (m_y[i] > m_y[ihi]) {
				inhi=ihi;
				ihi=i;
			} else if (m_y[i] > m_y[inhi] && i != ihi) inhi=i;
		}
		rtol=2.0*fabs(m_y[ihi]-m_y[ilo])/(fabs(m_y[ihi])+fabs(m_y[ilo])+TINY);
		if (rtol < m_ftol) {
			SWAP(m_y[0],m_y[ilo]);
			for (i=0;i<ndim;i++) SWAP(m_p[0][i],m_p[ilo][i]);
			break;
		}
		if (m_nfuncCalls >= m_nmax) 
			cout<< "NMAX exceeded"<<endl;
		m_nfuncCalls += 2;
		ytry=amotry(m_p,m_y,psum,ihi,-1.0);
		if (ytry <= m_y[ilo])
			ytry=amotry(m_p,m_y,psum,ihi,2.0);
		else if (ytry >= m_y[inhi]) {
			ysave=m_y[ihi];
			ytry=amotry(m_p,m_y,psum,ihi,0.5);
			if (ytry >= ysave) {
				for (i=0;i<mpts;i++) {
					if (i != ilo) {
						for (j=0;j<ndim;j++)
							m_p[i][j]=psum[j]=0.5*(m_p[i][j]+m_p[ilo][j]);
						m_y[i]=func(psum);
					}
				}
				m_nfuncCalls += ndim;
				get_psum(m_p,psum);
			}
		} else --m_nfuncCalls;
	}
	return (!m_caller->GetStopFlag());
}

double NelderMead::amotry(ON_Matrix &p, ON_SimpleArray<double>&y, ON_SimpleArray<double> &psum, 
	const int ihi, const double fac)
{
	int j;
	double fac1,fac2,ytry;

	int ndim=p.ColCount();
	ON_SimpleArray<double> ptry(ndim); ptry.SetCount(ndim);
	fac1=(1.0-fac)/ndim;
	fac2=fac1-fac;
	for (j=0;j<ndim;j++)
		ptry[j]=psum[j]*fac1-p[ihi][j]*fac2;
	ytry=func(ptry);
	if (ytry < y[ihi]) {
		y[ihi]=ytry;
		for (j=0;j<ndim;j++) {
			psum[j] += ptry[j]-p[ihi][j];
			p[ihi][j]=ptry[j];
		}
	}
	return ytry;
}

	inline void NelderMead::get_psum(ON_Matrix &p,  ON_SimpleArray<double> &psum)
	{
		int i,j;
		double sum;

		int mpts=p.RowCount();
		int ndim=p.ColCount();
		for (j=0;j<ndim;j++) {
			for (sum=0.0,i=0;i<mpts;i++)
				sum += p[i][j];
			psum[j]=sum;
		}
	} //getpsum


	int NelderMeadtest(void)
{
//        const int MP=4,NP=3;
 
 //       int i,j;
     //   Vec_DP x(NP),y(MP);
     //   Mat_DP p(MP,NP);

		NelderMead t = NelderMead();
    //    for (i=0;i<MP;i++) {
  //        for (j=0;j<NP;j++)
  //          x[j]=p[i][j]=(i == (j+1) ? 1.0 : 0.0);
  //        y[i]=t.func(x);
  //      }

		cout << "Vertices of initial 3-d simplex and" << endl;

		t.Print(stdout);
        t.FindMinimum();
   
		t.Print(stdout);

        cout << endl << "True minimum is at (0.5,0.6,0.7)" << endl;
        return 0;
}


int NelderMead::Print(FILE *fp)
{
int i,j, k=0;
     cout << endl << "Number of function evaluations: " << GetNCalls() << endl;
        cout << "Vertices of simplex and" << endl;
        cout << "function values at the vertices:" << endl << endl;
			printf(" \t%s\t%s\t%s\t%s\t%s\n", "i" , "x[i]","y[i]"    , "z[i]" , "function"  );
 
        cout << fixed ;
        for (i=0;i<m_MP;i++) {

			k+=printf("\t%d ",i);
			for (j=0;j<m_NP;j++) 
				k+=printf(" \t %15.6g ", m_p[i][j]);
			k+=printf(" \t%15.6g\n", m_y[i] );
        }

return k;
}

