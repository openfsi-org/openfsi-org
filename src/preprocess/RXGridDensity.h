#pragma once
#include "RXEntity.h"

class RXGridDensity :
	public RXEntity
{
public:
		RXGridDensity(void);
		RXGridDensity(SAIL *s);
		~RXGridDensity(void);
		int CClear();
		int Compute(void);
		int Resolve(void);
		int Finish(void);
		int ReWriteLine(void);


		int Dump(FILE *fp) const;
		RXSTRING TellMeAbout() const;

};
