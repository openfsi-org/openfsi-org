
/* RelaxII source code
 * Copyright Peter Heppel Associates 2000
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 
 */
// these are the interface functions to be called from fortran

#include "StdAfx.h"
#include <QDebug>
#undef BOOL // Peter nov 2004 
#include "RX_FESite.h"
#include "RXEntity.h"
#include "RXEntityDefault.h"
#include "RXSail.h"
#include "rxON_Extensions.h"
#include "RXON_Matrix.h"
#include "RXSRelSite.h"
#include "RX_FEPocket.h"
#include "RX_FEString.h"
#include "entities.h"
#include "words.h"
#include "RX_FESite.h"
#include "RXContactSurface.h"
#include "delete.h"
#include "rxvectorfield.h"
#include "nlm.h"

#ifdef _X
#include "akmupdis.h"
#endif

#ifdef RXQT
#include "rxqtdialogs.h"
#endif

#include "printall.h"
#include "r2af.h"

#include "script.h" 
#include  "akmutil.h"
#include "TensylLink.h"
#include "global_declarations.h"
#include "RXRelationDefs.h"
#ifdef ODETESTING
#include "rxrungekutta.h"
#include "rxstiffsolver.h"
#include "rxstiffsolve_invmass.h"
#endif
#include "cfromf.h"


//BUFFER FUNCTIONS TO BE CALLED FROM FORTRAN, 

//MUST be consistent with the fortran interface. cfromf_declarations.f90
// definitions  must agree with rximplicitsolution.f90
#define STIFFSOLVE  1
#define RUNGEKUTTA  2

int fc_runtopeak(const int method, int *kit)  //returns iexit
{
    int iexit=0;
    static double h1 = 0.25;
#ifdef ODETESTING
    if(false){
//        class Nr rk mytest;
//        mytest.m_kit = *kit;
//        qDebug()<< " run to peak with stiffsolve ";
//        //    iexit = mytest.rksolve();
//        iexit = mytest.stiffsolve();
//        *kit  =  mytest.m_kit;
    }
    else if(method==RUNGEKUTTA ){
        class RXRungeKutta mytest;
        mytest.SetKit ( *kit);
       cout<< " run to peak with class RXRungeKutta"<<endl;
        iexit = mytest.runtopeak();
        *kit  =  mytest.Kit();
    }
    else if(method==STIFFSOLVE){
        class RXStiffSolver mytest; //RXStiffSolve_InvMass
        mytest.SetKit ( *kit);
        mytest.setH1(h1);
        mytest.SetDamping(relaxflagcommon.g_RKDamping);
        mytest.SetEps(relaxflagcommon.g_RKEps);
        cout<< " run to peak with StiffSolver"<<endl;
        iexit = mytest.runtopeak();
        *kit  =  mytest.Kit();
        h1 = mytest.GetH1(); qDebug()<<" after peak H1 = "<<h1;
    }
#endif
return iexit;
}


int cf_gravity_field ( double*x,  double* acc)
{
    acc[0]=acc[1]=acc[2]=0.0;

    RXVectorField* e=0;
    e=dynamic_cast<RXVectorField*>(PC_Get_Key_Across_Models("vector field","gravity"));
    if(!e) return 0;
    RXSitePt p(x[0],x[1],x[2],ON_UNSET_VALUE,ON_UNSET_VALUE,-1);

    int rc = e->ValueAt(p,acc);
    return rc;
}
int cf_SetParentRelation (RX_FESite*  fes,  RXEntityDefault* p2)
{
    RXSRelSite * p1 = dynamic_cast<RXSRelSite *> (fes) ;
    if(!p1)
        return 0;
    return p1->SetRelationOf(p2,  child|niece,RXO_CONNECT_OF_SITE); // should this just be 'niece'?

} //TODO WRONG: we should only remove RXO_CONNECT_OF_SITE links between p1 and p2;
int cf_UnSetParentRelation (RX_FESite*  fes,  RXEntityDefault* p2)
{
    RXSRelSite * p1 = dynamic_cast<RXSRelSite *> (fes) ;
    if(! p1)
        return 0;
    int rc= p1->UnHook(p2,RXO_CONNECT_OF_SITE);  // need to get rid of the niece relations too
    return rc;

}
int cf_SetParentRelationStr (RX_FEString*  fes,  RXEntityDefault* p2)
{
    RXEntity * p1 = dynamic_cast<RXEntity *> (fes) ;
    if(!p1)
        return 0;
    return p1->SetRelationOf(p2,  child|niece,RXO_CONNECT_OF_STRG); // should this just be 'niece'?

}
int cf_UnSetParentRelationStr (RX_FEString*  fes,  RXEntityDefault* p2)
{
    RXEntity * p1 = dynamic_cast<RXEntity *> (fes) ;
    if( !p1)
        return 0;
    int rc= p1->UnHook (p2,RXO_CONNECT_OF_STRG);
    return rc;
    //April 2014  only remove RXO_CONNECT_OF_STRG links between p1 and p2, not ALL links;

}
int cf_postprocess(){
    Execute_Script_Line("Update: ",NULL);
    return 1;
}
int  cf_IsEdgeNode(const void *p_s,const int n){
    class RXSail *s = (class RXSail * ) p_s;
    class RX_FESite*p =s->GetSiteByNumber(n) ;
    assert(p);
    int rc1 = p->IsEdgeNode();
    if(rc1) return 1;
    int rc2=0;
    class RXSRelSite*rs = dynamic_cast<class RXSRelSite*>(p) ;//(s->GetSiteByNumber(n));
    if(rs)
        rc2 = rs->IsEdgeNode();
    return  rc2;
}

int cf_getTensyllinkPropNo (const class CTensylLink *p ){
    if(!p) return 0;
    return p->GetPropNo();
}
int ClearBatten(RX_FEPocket *p) {
    // DONT call ClearFEA because we only call this from
    //cf_removefortranbatten which is called from ClearFEA
    p->SetIsInDatabase(false);
    return  1;
}
int check_messages(int *k) {


#if defined( RXQT) ||defined(linux)
    return akm_update_display_(k);

#else
    MSG msg;
    while ( ::PeekMessage( &msg, NULL, 0, 0, PM_NOREMOVE ) )
    {
        if (!AfxGetApp()->PumpMessage())
        {
            ::PostQuitMessage(0);
            return *k;
        }
    }
    if(0){ //!g_Janet){
        // let MFC do its idle processing
        LONG lIdle = 0;
        while ( AfxGetApp()->OnIdle(lIdle++ ) ){
            // Perform some background processing here
            // using another call to OnIdle
        }
    }
#endif
    return *k;
}

#ifdef linux
int fc_isnan_d_(double *a){
    return (isnan(*a));
}

int r_to_af_(double *r, double *tmax)
{
    return R_To_Af(r, tmax);
}
#endif


int getfortrannodeno(char *model, char *nname) 
{ 
    return RXSail::GetFortranNoDeno(model, nname);
}//int getfortrannodeno_


double get_vector_plot_scale()
{
    return g_VectorPlotScale;
}
// note that some of the slidecurve fns cast to rxON_NurbsCurve. Others just cast to ON_Curve.
// so don't be surprised if a non- NurbsCurve crashes
// also note that slidecurves requre the ONFlag to be ON

int slidecurve_update_param(RXEntity_p *cptr,double *p_t,double *dt){ // return OK
    sc_ptr l_sc = (sc_ptr  ) (*cptr);
    SAIL *l_sail = (*cptr)->Esail;

    const ON_Object *l_ono = (*cptr)->m_pONMO->m_object;
    const ON_Curve *l_onc = dynamic_cast<const ON_Curve*>(l_ono);

    double t = *p_t + (*dt);
    if(!l_onc->IsClosed()) {
        *p_t=t;
        return 1;
    }
    while( t > l_onc->Domain().Max())
        t -= l_onc->Domain().Length();
    while( t < l_onc->Domain().Min())
        t += l_onc->Domain().Length();
    *p_t=t;
    return 1;
} //slidecurve_update_param_

void slidecurve_lengthto(const RXEntity_p *cptr,const double *p_t,double *length){
    sc_ptr l_sc = (sc_ptr  ) (*cptr);
    double z;
    const ON_Object *l_ono = (*cptr)->m_pONMO->m_object;
    const ON_Curve *l_onc = dynamic_cast<const ON_Curve*>(l_ono);

    ON_Interval i ( l_onc->Domain().Min(),*p_t);
    if(l_onc->GetLength(&z,0.0001,&i))
        *length = z;
    else
        *length = -1;
    return ;
}

// note that some of the slidecurve fns cast to rxON_NurbsCurve. Others just cast to ON_Curve.
// so don't be surprised if a non- NurbsCurve crashes
// also note that slidecurves requre the ONFlag to be ON
void slidecurve_evaluate(const RXEntity_p *cptr,const double *p_t,double p[3]){ 
    sc_ptr l_sc = (sc_ptr  ) (*cptr);
    const ON_Object *l_ono = (*cptr)->m_pONMO->m_object;
    const ON_Curve *l_onc = dynamic_cast<const ON_Curve*>(l_ono);
    ON_3dPoint pp = l_onc->PointAt(*p_t);
    pp = (*cptr)->Esail->XToGlobal(pp);
    for(int k=0;k<3;k++) p[k]=pp[k];
    return ;
} //slidecurve_evaluate
// note that some of the slidecurve fns cast to rxON_NurbsCurve. Others just cast to ON_Curve.
// so don't be surprised if a non- NurbsCurve crashes
// also note that slidecurves requre the ONFlag to be ON
int slidecurve_find_jacobian(RXEntity_p *cptr, double *t,double p_j[9])  // return OK

{
    sc_ptr l_sc = (sc_ptr  ) (*cptr);
    assert(l_sc);
    SAIL *sail = (*cptr)->Esail;
    int i,k,kk;
    double *lp, j[9];
    //	cout<< "need to look at contact_find_jaco to ensure it does the same as this"<<endl;
    const ON_Object *l_ono = (*cptr)->m_pONMO->m_object;
    const rxON_NurbsCurve *l_onc = dynamic_cast<const rxON_NurbsCurve*>(l_ono);

    ON_Xform xf = l_onc->TrigraphAt(*t); // shouldnt be normalised
    ON_Matrix m = ON_Matrix ( xf.m_xform);

    ON_3dVector tt = l_onc->TangentAt (*t);
    ON_3dPoint p;
    l_onc->Ev1Der(*t,p,tt);
    double l_scale = tt.Length();
    kk=0;
    for(i=0;i<3;i++){
        for(k=0;k<3;k++)	{//CAREFUL !! TRANSPOSE
            j[kk]=m[k][i]*l_scale;
            kk++;
        }
    }
    //	rxerror(" STEP transform to global space ",1);

    lp = j;
    tt = sail->VToGlobal(lp); *lp++=tt.x;*lp++=tt.y;*lp++=tt.z;
    tt = sail->VToGlobal(lp); *lp++=tt.x;*lp++=tt.y;*lp++=tt.z;
    tt = sail->VToGlobal(lp); *lp++=tt.x;*lp++=tt.y;*lp++=tt.z;

    for(i=0;i<9;i++) p_j[i]=j[i];
    return 1;
}

// note that some of the slidecurve fns cast to rxON_NurbsCurve. Others just cast to ON_Curve.
// so don't be surprised if a non- NurbsCurve crashes
// also note that slidecurves requre the ONFlag to be ON


int slidecurve_find_intersection(RXEntity_p *cptr,
                                 double p_oldpt[3],
                                 double p_newptxyz[3],double *p_newptu)
{
    if(! (*cptr)) {  cout<< " zero cptr"<<endl; } // crash is certain
    sc_ptr l_cs = (sc_ptr ) (*cptr);
    assert(l_cs);
    // 	transform the pts to cptr's model space
    SAIL *l_sail = (*cptr)->Esail;

    double oldpt[3];
    double proposedpt[3];
    double newptxyz[3]; ON_3dPoint newp;

    int rc;
#ifdef NOTlinux
    cout<<" slidecurve_find_intersec TODO:"<<endl;
    assert(0); //untransform_coords_(&m,p_oldpt  ,oldpt  );
#else
    memcpy(oldpt,p_oldpt,3*sizeof(double));
#endif
    const ON_Object *l_ono = (*cptr)->m_pONMO->m_object;
    const rxON_NurbsCurve *l_onc = dynamic_cast<const rxON_NurbsCurve*>(l_ono);
    double t = l_onc->Domain ().Mid();

    rc =l_onc->GetClosestPoint(ON_3dPoint(oldpt), &t,999999.0);
    if(!rc) return 0;
    newp= l_onc->PointAt(t);   newptxyz[0]=newp.x;  newptxyz[1]=newp.x;  newptxyz[2]=newp.x;
    *p_newptu=t;
#ifdef NOTlinux  // transform newpt to global space
    assert(0); //transform_coords_(&m,newptxyz  ,p_newptxyz);
#else
    memcpy(newptxyz  ,p_newptxyz,3*sizeof(double));
#endif

    return rc;
}
// functions introduced OCt 2006
/* returns 0 if no intersection was found, else C_GOING_UP or C_GOING_DOWN
  *k is the parameter on the line from oldpt to newpt
*/
int contact_find_intersection(RXEntity_p *cptr,
                              double p_oldpt[3], double p_proposedpt[3],
                              double p_newptxyz[3],double p_newptuv[3],double*k)
{
    if(! (*cptr)) {  cout<< " zero cptr"<<endl; } // crash is certain
    RXContactSurface *l_cs = (RXContactSurface*) (*cptr)->dataptr;
    assert(l_cs);
    // 	transform the pts to cptr's model space
    SAIL *l_sail = (*cptr)->Esail;

    double oldpt[3];
    double proposedpt[3];
    double newptxyz[3];

    int rc;
#ifdef linux
    assert("contact_find_intersection needs stepping"==0);
    //printf(" (contact_find_intersection_) poldpt=%f %f %f\n prop=%f %f %f\n",
    // p_oldpt[0],p_oldpt[1],p_oldpt[2], p_proposedpt[0],p_proposedpt[1],p_proposedpt[2]);

    assert(0); //untransform_coords_(&m,p_oldpt  ,oldpt  );
    assert(0); //untransform_coords_(&m,p_proposedpt  ,proposedpt  );
#else
    memcpy(oldpt,p_oldpt,3*sizeof(double));
    memcpy(proposedpt  ,p_proposedpt,3*sizeof(double)); assert(0);
#endif
    rc = l_cs->TestPathIntersects(oldpt,proposedpt,newptxyz,p_newptuv,k);
    if(!rc) return 0;

#ifdef linux  // transform newpt to global space
    assert(0); //transform_coords_(&m,newptxyz  ,p_newptxyz);
#else
    memcpy(newptxyz  ,p_newptxyz,3*sizeof(double));
    assert("transform not linked"==0);
#endif

    return rc;
}

int contact_find_jacobian(RXEntity_p *cptr, double uv[2],double p_j[9])  // return OK
{
    RXContactSurface *l_cs = (RXContactSurface*) (*cptr)->dataptr;
    assert(l_cs);
    SAIL *l_sail = (*cptr)->Esail;

    double j[9];
    int rc = l_cs->Jacobian(uv[0],uv[1],j ); //   TestIsBelow(oldpt,proposedpt,newpt);
    if(!rc) return 0;
    // transform newpt to global space
    cout<<" contact jacobian transformation not coded"<<endl;
#ifdef linux
    assert(0); //transform_vector_(&m, j  ,p_j);
    assert(0); //transform_vector_(&m, &j[3]  ,&p_j[3]);
    assert(0); //transform_vector_(&m, &j[6]  ,&p_j[6]);
#else
    assert(0);
#endif

    return 1;
}

void contact_evaluate(const RXEntity_p *cptr,const double uv[2],double p_p[3])  // return OK
{
    RXContactSurface *l_cs = (RXContactSurface*) (*cptr)->dataptr;
    assert(l_cs);
    SAIL *l_sail = (*cptr)->Esail;
    double j[3];
    int rc = l_cs-> Evaluate (uv[0],uv[1],j );
    if(!rc) {
        printf(" cs Evaluate failed on uv= %f %f\n", uv[0],uv[1]);
        return ;
    }
    // transform newpt to global space
#ifdef linux
    assert(0); //transform_coords_(&m, j  ,p_p);
    //	printf("(contact _ evaluate_ )on uv= %f %f  returns %f %f %f\n", uv[0],uv[1], p_p[0],p_p[1],p_p[2]);
#else
    assert(0);
#endif

    return ;

}

int contact_test_passedge(RXEntity_p *cptr,  // return non-zero if uv2 is past the edge
                          const double uv1[2],
                          const double uv2[2],
                          double *k){
    /*  on the line in uvspace from {u1,v1} to {u2,v2}
is (u2,v2) outside the domain and if so, by what proportion.
return in *k the proportion from {u1,v1} to {u2,v2} where we first hit the edge
*/
    //	assert("needs stepping"==0);
    RXContactSurface *l_cs = (RXContactSurface*) (*cptr)->dataptr; assert(l_cs);
    return ( l_cs->TestPassEdge( uv1[0],uv1[1],  uv2[0], uv2[1], k));
}



int contact_test_isbelow (RXEntity_p *cptr, // the contact surface entity
                          double p_proposedpt[3],// the points are in global coordinates
                          double p_newptuvw[3])
{
    int rc=0;
    /*  This function does a find-nearest on proposedpt, determines if closestpt  is on the edge.
If its not, and proposedPt is on the negative side, we set newpt to closestpt and return 1.
Otherwise it sets newpt ro proposedpt and return 0. 
The function works in the model space of the model which owns the contact surface.  
Thus before operating it shifts the arguments to the model?s coordinate system 
after operating it shifts the result back to global. 
*/
    if(! (*cptr)) {  cout<< " zero cptr"<<endl; } // crash is certain

    // printf(" (contact_test_isbelow_ ) %f %f %f\n",p_proposedpt[0],p_proposedpt[1],p_proposedpt[2]);

    RXContactSurface *l_cs = (RXContactSurface*) (*cptr)->dataptr;
    assert(l_cs);
    // transform the pts to cptr's model space
    SAIL *l_sail = (*cptr)->Esail;
    double proposedpt[3];
#ifdef linux
    assert(0); //untransform_coords_(&m, p_proposedpt , proposedpt );
#else
    memcpy(proposedpt,p_proposedpt,3*sizeof(double));
#endif
    rc = l_cs->TestIsBelow(proposedpt,p_newptuvw);

    return rc;

}



RXEntity_p  get_slidecurve(Site *p) 
{
    RXEntity_p l_cs=0;
    int kk;
    QString bufu ;
    kk= p->AttributeGet( "$slidecurve" ,bufu);
    if(!kk) return 0;
    SAIL *s = p->GetSail();
    l_cs=s->GetKeyWithAlias("seamcurve",qPrintable (bufu));// first look in the currentmodel
    if(l_cs) return l_cs;
    l_cs=PC_Get_Key_Across_Models("seamcurve",qPrintable(bufu));

    return l_cs;
} //  get_slidecurve_
RXEntity_p get_contact_surface(Site *p) 
{
    RXEntity_p l_cs=0;
    int kk;
    QString  bufu ;
    kk= p->AttributeGet( "$contactsurface",bufu);
    if(!kk)
        kk= p->AttributeGet("$contact surface",bufu);
    if(!kk) return 0;
    l_cs=PC_Get_Key_Across_Models("contact surface",qPrintable (bufu));

    return l_cs;
}
int fc_dump_snode(Site *const snode) {
    if(snode) return snode->Dump (stdout);
    return 0;
}
int fc_SetSiteAttribute(Site *s,const char* what,const char*val )
{
    RXEntity_p p = dynamic_cast<RXEntity_p>(s);
    if(!p)   return 0;
    return p->AttributeSet(what,val);
}
int fc_getsitename ( Site *p, char *output) 
{
    RXEntity_p e = dynamic_cast<RXEntity_p>(p);
    if(!e) {
        strcpy(output,"NoSite"); return 0;
    }
    if(!e->name()  ) {
        strcpy(output,"NoName"); return 0;
    }
    strncpy(output, e->name(),255);
    output[255]=0;
    return 1;

    if(!e->name() && !e->GetOName().length() ) {
        strcpy(output,"NoName"); return 0;
    }
    if(e->name()){
        strncpy(output,e->name(),255);
        output[255]=0; return 1;
    }
    string ss = ToUtf8(e->GetOName());

    strncpy(output,ss.c_str (),255);
    output[255]=0;
    return 1;
}


int fc_getentityname (RXEntity_p p,char*output, const int olength)  
{
    if(!p) { strncpy(output,"NoEnt",olength); return 0;}
    if(!p->name()) { strncpy(output,"NoName",olength); return 0;}
    strncpy(output,p->name(),olength);output[olength]=0;
    return 1;
}
int fc_delete_object(class RXObject *ptr)
{

    if(ptr) {
        ptr->Kill(); // places it on garbage stack.
        return 1;
    }
    return 0;
}

int cf_get_nodal_fixing (Site *p, char *output)
{
    assert(strlen("cf_get_nodal_fixing ")==0);
    QString buf;
    bool kk;

    kk = p->AttributeGet("$fix",buf);
    if(!kk) return 0;
    int n =buf.length(); n=min(n, 511); // see FillFNode.
    //   string temp = ToUtf8(buf);
    strncpy(output,qPrintable(buf),n);output[n]=0;
    return 1;
}



//END BUFFER FUNCTIONS TO BE CALLED FROM FORTRAN, 



