/* this is dxf.c started 10/1/95
// Dec 2005 we check the metafile exists before reading it.
 *  6/6/96  Unset Metafile if wrapped
  3/3/96 aunt push added

 16/10/95 debugging added
 14.1/95   Needs Computing flags adjusted.

  A DXF entity is a graphical object from a file. The file might be .hmf or .dxf
  (1/1/95) only HMF is supported
  A simple DXF just has a filename and a segname.
  The geometry is placed in a child of the include library
  and that child is HC_Included in the boat.

  A complex DXF is BENT around a curve by Compute_DXF which takes the include seg, copies it
  to the model segment and then modifies its coordinates.



dxf:segname:filename :[sc] */
#include "StdAfx.h"
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXSeamcurve.h"
#include "RXSitePt.h"
#include "RXRelationDefs.h"

#define DXF_DEBUG (1)

#include "entities.h"
#include "stringutils.h" 
#include "akmutil.h"
#include "resolve.h"

#include "panel.h"

#include "bend.h"
#include "dxfin.h"

#include "dxf.h"

HC_KEY Read_DXF(SAIL *sail, const char *name,const char *filename) {

    char base[256], ext[256],buf[256],seg1[256],Seg2[256],path[256];
    HC_KEY dkey;
    if (DXF_DEBUG) cout<< " Enter Read_DXF\n"<<endl;

    rxfileparse(filename ,path,base, ext,256);
    if(rxIsEmpty(ext))	 strcpy(ext,".dxf");
    cout<<"dxf filename "<<path<<base<<ext<<endl;
    sprintf(buf,"%s%s%s",path,base,ext);
#ifdef HOOPS
    if(strieq(ext,".hmf")) {
        cout<<" .HMF no longer supported"<<endl;
        //  sprintf(buf,"'%s%s%s'",path,base,ext);
        //  FILE *fp=FOPEN(buf,"w");
        //  if(fp)
        //   FCLOSE(fp);
        //  else
        //return 0;

        //HC_Read_Metafile(buf,name," ");
        //return(1);
    }
    else
#endif	   
        if (strieq(ext,"dxf")) {
            HC_KEY key;
            HC_Show_Pathname_Expansion(".", Seg2);
            key = dkey = 	dxfin(sail, filename);


            if(key){
                HC_Open_Segment_By_Key(key);
                HC_Set_Selectability("off");
                HC_Close_Segment();

                HC_Open_Segment(Seg2);
                HC_Open_Segment(name);
                HC_Show_Segment(key,seg1);
                HC_Show_Pathname_Expansion(".", Seg2);
                if(DXF_DEBUG) printf(" %s was read into %s\n", filename,seg1);
                if(DXF_DEBUG) printf("including %s \ninto %s\n", seg1,Seg2);
                HC_Include_Segment_By_Key(key);
                HC_Close_Segment();
                HC_Close_Segment();

            }
            if (DXF_DEBUG) cout<< " Leave Read_DXF\n"<<endl;

            return dkey;
        }
        else  {
            sail->OutputToClient(" DXFin requires file extensions .hmf or .dxf",2);
            return(0);
        }
}

int Resolve_Dxf_Card(RXEntity_p e) {

    struct PC_DXF *p;

    int retval = 0;
    cout<<" TODO step dxf reader "<<endl;
    QString qline = e->GetLine();
    QStringList wds= rxParseLine(qline,RXENTITYSEPREGEXP );
    if(wds.size()<3)  wds<<"*.dxf";
    if(wds.size()<4)  wds<<"";
    int nw =wds.size();
    QString & name=wds[1] ;
    QString &fname = wds[2];
    QString sc = wds.value(3); // value is optional
    QString command = wds.value(4);
    fname=  e->Esail->AbsolutePathName(fname);

    if(!VerifyFileName(fname,name,"*.dxf"))
        return 0;
    fname = e->Esail->RelativeFileName(fname);
    qline = wds.join(RXENTITYSEPS);
    e->edited = !(qline ==e->GetLine());
    if(e->edited){
        e->SetLine( qPrintable(qline));
    }

    p = (PC_DXF*)CALLOC( 1,sizeof(struct PC_DXF));
    if( !sc.isEmpty()) {
        p->sc = e->Esail->Get_Key_With_Reporting("curve,seam,seamcurve",qPrintable(sc));
        if(!p->sc) {
            RXFREE(p);
            return(retval);
        }
        p->sc->SetRelationOf(e,child|niece,RXO_SC_OF_DXF);;

    }
    p->filename = STRDUP(qPrintable(fname));
    p->segname = STRDUP(qPrintable(name));
    if(strstr(p->filename,"already done") == NULL) {
#ifdef HOOPS
        HC_Open_Segment("?Include Library");
        if(HC_QShow_Existence(name,"self"))
            HC_Delete_Segment(name);
#endif
        if(command.isEmpty()) {
            QString buf = command + " "+ fname;
            printf(" preprocess DXF with '%s'\n",qPrintable(buf));
            system(qPrintable(buf));
        }
        e->hoopskey = Read_DXF(e->Esail, qPrintable(name),qPrintable(fname));
#ifdef HOOPS
        HC_Close_Segment();
#endif
    }
    e->SetDataPtr( p);
    e->Needs_Resolving=0;
    e->Needs_Finishing=0;
    e->SetNeedsComputing();

    retval = 1;
    return(retval);
}  	

int Compute_DXF(RXEntity_p e) {

    HC_KEY key;
    char segname[256];
    struct PC_DXF *p = (PC_DXF*)e->dataptr;
    if(!e->NeedsComputing()) return(0);
    HC_Open_Segment("?Include Library");
    key = HC_KOpen_Segment(p->segname);
    HC_Close_Segment();
    HC_Close_Segment();
    HC_Show_Segment(key,segname);
    if(p->sc) {
        RXSitePt e1,e2;
        ON_3dVector py[2],pz[2], xl; ON_3dVector l_z;
        float Black_Chord;

        sc_ptr  sc = (sc_ptr  )p->sc;
        l_z =p->sc ->Normal();
        /*sample call
 copy {mast->p[1], mast[>c[1]] } to {pxin,cxin}
 call Compute Seamcurve Trigraph 	 on mast
 copy {e2, 2 } to {pxin,cxin}
 copy {e3, 2 } to {pxin,cxin}	  */
        py[0].x= py[0].y= py[0].z=(float)0.0;
        pz[0].x= pz[0].y= pz[0].z=(float)0.0;

        if(1== sc->Compute_Seamcurve_Trigraph(1, 1,&e1,&e2,&xl,&(py[1]),&pz[1],&Black_Chord,l_z)) return(-1);
        VECTOR pyv[2],pzv[2];
        pyv[0].x= pyv[0].y= pyv[0].z=(float)0.0;
        pzv[0].x= pzv[0].y= pzv[0].z=(float)0.0;
        pyv[1].x= py[1].x;	pyv[1].y= py[1].y;	pyv[1].z= py[1].z;
        pzv[1].x= pz[1].x;	pzv[1].y= pz[1].y;	pzv[1].z= pz[1].z;
#ifdef HOOPS
        PC_Bend_Segment(e,segname,"geometry,include",p->segname,sc->p[1],sc->c[1],pyv,2,pzv,2);
#endif
    }
    else
        HC_Include_Segment_By_Key(key);

    return(1);
}		
