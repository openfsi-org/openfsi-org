#ifndef _TEXT_H_
#define _TEXT_H_

char *writedata(char *p_filename,
				char *p_data);
EXTERN_C char * readdata(const char *p_filename);
//char * readdata(const char *p_filename);
int updateAllTextWindows(void);
int updateOneTextWindow(int which);

//TR  15 JAN 05 EXTERN_C int initTextWindows(void);
int initTextWindows(void);

#endif  //#ifndef _TEXT_H_


