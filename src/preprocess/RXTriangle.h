//RXTriangle.h header file for RXTriangle.cpp
///Thomass Ricard 12 08 04

#if !defined(AFX_RXTriangle_H__34B6764A_ABBD_4907_B5C3_4BDEF35DCD64__INCLUDED_)
#define AFX_RXTriangle_H__34B6764A_ABBD_4907_B5C3_4BDEF35DCD64__INCLUDED_



#include "RXTRObject.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "RXCrossing2.h"

#include "griddefs.h"

#define RXTRIANGLEOK 1
#define RXTRIANGLEFALSE 0

#define RX_FILELT_INTER_MORETHAN_2  100//--> their is more than 2 intersection betweent he element and the filament
#define RX_FILELT_INTER_NONE     101// --> no intersection beetween the filament and the element
#define RX_FILELT_INTER_VERTEX    102//--> the element does not see the filament
#define RX_FILELT_INTER_EDGE      103//--> the element does not see the filament
#define RX_FILELT_INTER_VERTEX_EDGE  104 //--> the element does see the filament
#define RX_FILELT_INTER_EDGE_VERTEX   105//--> the element does see the filament
#define RX_FILELT_INTER_VERTEX_VERTEX  106//--> the element does see the filament (the filement is on an edge)
#define RX_FILELT_INTER_EDGE_EDGE	107 //--> usual crossing type. no vertices

#define RX_MESH_XYPLANE 2
#define RX_MESH_3D 3

#define RX_ELT_SEES_FILAMENT			201   //RX_ELT_SEES_FILAMENT if the element can see the filament
#define RX_ELT_CANNOT_SEES_FILAMENT		0     //RX_ELT_CANNOT_SEES_FILAMENT if the element CANNOT see the filament


#ifndef RXTRIANGLECLASSID //8541C3F4 715E 4B70 ADD5 9CE821EC6652
#define RXTRIANGLECLASSID "8541C3F4-715E-4B70-ADD5-9CE821EC6652"
#endif

class rxON_BoundingBox;

class RXTriangle : public RXTRObject 
{
//	RXOBJECT_DECLARE(RXTriangle);  //do not put anything after this ON_OBJECT_DECLARE statement !!!
public:

	ON_Xform Trigraph();
	int GetTightBoundingBox(rxON_BoundingBox& bbox, bool bGrowBox=0,const ON_Plane* onb=NULL) const;
	ON_3dVector Yaxis() const;
	int CircumCircle( ON_3dPoint &p_centre, double*p_r) ;
	int SomewhereNear(const RXCurve & p_c, const double p_tol);
	int SetAllVertices( class FILAMENT_LOCAL_DATA * p_fld);
	int IsOnaVertex(const RXUPoint & p_pt, int & p_VIDn, const double & p_EpsDot) const;
	//!Constructor
	RXTriangle();
	//!Constructor by copy
	RXTriangle(const RXTriangle & p_Obj);
	RXTriangle( class RX_FETri3 &p_t);
	//!Constructor by copy
	
	//!Destructor
	virtual ~RXTriangle();

	void Init();

	int Clear();

	RXTriangle& operator = (const RXTriangle & p_Obj);

	virtual char * GetClassID() const; 
	virtual int IsKindOf(const char * p_ClassID) const;
	static RXTriangle * Cast( RXTRObject* p_pObj); 
	static const RXTriangle * Cast( const RXTRObject* p_pObj);

	ON_3dPoint operator[](const int & p_ID) const;
//	ON_3dPoint & operator[](const int & p_ID);
	int SetV(const int & p_ID,const ON_3dPoint & p_pt);

	const ON_3dPoint GetV(const int & p_ID) const;

	ON_3dPoint PointAt(const double & p_t) const; 
	ON_3dVector TangentAt(const double & p_t) const; 

        //!set the triangle form a p_fld
	/*!compute the intrestion with the curve and do some other stuffs to the field
	returns 
		RX_ELT_SEES_FILAMENT : if the element can see the filament
	returns
		RX_ELT_CANNOT_SEES_FILAMENT		if the element CANNOT see the filament
	*/
	int Create_Filament_Matrix(sc_ptr p_sc,
								class FILAMENT_LOCAL_DATA * p_fld,
							   const double & p_Tolerence,
							   const double & p_CutDistance, 
							   const double & p_EpsDot,
							   const int & p_IsIn3d = RX_MESH_3D); 



	//!returns the number of intersection between the triangle and the curve 
	//! sets p_Intersections with these interstions
	//!the intersections cannot be off the edges of the triangle of of the curve
	int FindIntersections(const RXCurve * p_crv, 
						  ON_ClassArray<RXCrossing2> & p_Intersections, 
						  const double     & p_Tolerence,
						  const double     & p_CutDistance,
						  const double     & p_EpsDot) ;

	//!To do all the operation about intersection between filamaent and element
	/*
		Does some post processing to deal witht the diffenret case of intersection and to return the right flags to Relax
	*/
	int FindFilamentIntersection(const RXCurve * p_crv,
								 ON_ClassArray<RXCrossing2> & p_Intersections, 
								 const double     & p_Tolerence,
								 const double     & p_CutDistance,
								 const double	  & p_EpsDot);

	int GetUnitNormal(ON_3dVector&) const; // peter sept 15 for ON V4
	//FOR NOW we are defining the triangle in static (with no pointer)
	//in the future it would be nice to do it as the struct 'Triangle' is with pointer onto edge. 

protected:
	int ClearCC();
private: 
	ON_3dPoint m_centre; 
	double m_radius; // of the CircumCircle
 	ON_3dPoint m_v[3]; 

};

#endif // !defined(AFX_RXTRIANGLE_H__34B6764A_ABBD_4907_B5C3_4BDEF35DCD64__INCLUDED_)

