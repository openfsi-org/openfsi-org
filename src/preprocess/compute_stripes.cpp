/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
	November 2002 stripes are drawn into baseseg/data/type/name and included
    October 2000 get z from find_dx_at_xy not  AKM_Pansail_UVtoXY
 *	better error return for failed finddxatxy
 * 	14/3/97 a BUG for single-point stripes fixed 
     	27/3/96    file separator changed to TAB
 *	28/11/94  PH stripe	 type "between" = Between {u1,v1},{u2,v2} defined
 
 *	14.11.94
 
 Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXSeamcurve.h"
#include <ctype.h>

#include "RX_FESite.h"
#include "RXSail.h"

#include "etypes.h"
#include "pansin.h"

#include "global_declarations.h"

#include "elements.h"
#include "stripes.h"

//#define STRIPE_DEBUG


int Compute_All_Stripes(class RXSail  *s,FILE *fp) 
{
  /* m - model no. */
  /* look at all stripe cards in a model
   * produce an output curve for each entry in each curve
   * draw the curve into the currently open segment
   */

        int cnt=0; HC_KEY key;
	Graphic *g;
	char buf[256];
	assert(s);
#ifdef STRIPE_DEBUG
	 HC_Show_Pathname_Expansion(".",buf);
	 printf(" putting stripes for %s  in seg <%s>\n",s->GetType().c_str(), buf);
#endif 
 #ifdef HOOPS
	 HC_Flush_Contents(".","include");

	g = s->Graphic() ;

	
	HC_Open_Segment_By_Key(g->m_ModelSeg);
	 HC_Open_Segment("data/stripe"); 
	 HC_Set_Visibility("off");
	 HC_Show_Pathname_Expansion(".",buf);
 		 cnt = 0;
		ent_citer it;
		for (it = s->MapStart(); it != s->MapEnd(); ++it){
		RXEntity_p p  = dynamic_cast<RXEntity_p>( it->second);
   			if (p->TYPE==STRIPE)  {
    				if(p->hoopskey) {
					HC_Delete_By_Key(p->hoopskey); assert(!g_Hoops_Error_Flag);  }  
     				p->hoopskey = HC_KOpen_Segment(p->name()); 
					HC_Flush_Geometry(".");
    		 			Compute_Stripe(p,0,fp); /* always geometry only */
    		 			fflush(fp);
     				HC_Close_Segment();
     				cnt++;
    			}
			if(p->TYPE==SEAMCURVE && ((sc_ptr) p)->FlagQuery(SC_STRIPE)){
			key = ((sc_ptr) p)->Draw_Deflected_SC(fp);  
			if(key) {
				HC_Open_Segment(p->name());
					HC_Flush_Segment(".");
					HC_Include_Segment_By_Key(key);
				HC_Close_Segment();
			}
			}
 		 }
	 HC_Close_Segment();
	HC_Close_Segment();	    /* close the base window segment */

	strcat(buf,"/*");
	if(HC_QShow_Existence(buf,"self"))
		HC_Include_Segment(buf); 
#else
         cout<<"TODO QT compute stripes"<<endl;
#endif
    	return(cnt);
}

/* step size for polyline */

/* type = 0 -> deflection
 * type = 1 -> pressure
 * shown on deformed sail shape
 */
int Compute_Stripe(RXEntity_p p,int type, FILE *fp) 
{
  
  StripeData *stripe;
  int i,j;
  double *val;
  double sval, fstep;
  char colorString[200];
  ON_3dPoint srot;
 
  int  sno=0;
  SAIL *theSail;
  float wt;
  char *lp,*colStr;
  
 #ifdef STRIPE_DEBUG
 	printf(" compute stripe on %s %s\n", p->type,p->name);
 #endif
 	theSail = p->Esail;

  if(type ==1) {
     p->OutputToClient(" Cant do this kind of stripe in V2.00-18 and above",2) ; return 0;
    }
 

  stripe = (StripeData *) p->dataptr;
  fprintf(fp,"! New Stripe '%s' type '%s' \n",p->name(),stripe->type);
  
  
  colStr = STRDUP(stripe->color);
  lp = colStr;
  while(*lp && !isdigit(*lp) ) {
    lp++;
  }
  if(*lp) {
    *(lp-1) = '\0';	/* fake end of string */
    wt = (float) atof(lp);
    if( wt < 1 || wt > 10)
      wt = (float) 1.0;
    HC_Set_Line_Weight(wt);
  }
  
  sprintf(colorString,"lines=%s",colStr);
  HC_Set_Color(colorString);
  
  RXFREE(colStr);
  
  fstep = (float) ((float)1)/((float) stripe->step);

  if(strieq(stripe->type,"between")) {  /* means ONE STRIPE BETWEEN u1,v1, u2,v2*/
    double u,v,u1,u2,v1,v2;
    double du,dv;
    double length=0.0;
	ON_3dPoint l_old;
	ON_3dVector d;
    char text[64];
    int start=1;
	l_old.x=l_old.y=l_old.z=0.0;
        HC_Open_Segment("between"); {
     HC_Open_Segment(stripe->name);
      u1=stripe->u1; 
      v1=stripe->v1;
      u2=stripe->u2;
      v2=stripe->v2;
if(stripe->step>1) {
      du = (u2-u1)/((float) (stripe->step-1));
      dv = (v2-v1)/((float) (stripe->step-1));
     	 fprintf(fp,"3dcurve \t%s_b_%d ",p->name(),type);
}
else{
     	fprintf(fp,"site  \t %s \t %s ","site", p->name());
	du=dv=0.0;
}
      
   
      HC_Restart_Ink();
      
      for(j=0;j<stripe->step;j++) {
				u = u1 + du * ((float) j);
				v = v1 + dv * ((float) j); 
				srot = theSail->CoordsAt(ON_2dPoint(u,v),RX_GLOBAL);	
				//if(!Get_Global_Coords(u,v,theSail , srot)) continue;
				
				HC_Insert_Ink(srot.x,srot.y,srot.z);
				if(!start)	{
				  d  = l_old -srot ; 
				  length+=  d.Length ();
				}
				start=0;
				l_old = srot;
				fprintf(fp,"\t%f \t%f \t%f ",srot.x,srot.y,srot.z);
      }
	  if(stripe->step) {		//otherwise srot uninitialised
		    sprintf(text,"%10.2f",length);
			HC_Insert_Text(srot.x,srot.y,srot.z,text);
	  }
      fprintf(fp,"! from(%f,%f) to (%f,%f) length %f \n",u1,v1,u2,v2,length);
      HC_Close_Segment();
    }HC_Close_Segment();
  }
  else if(strieq(stripe->type,"u")) {  /* means at constant v, range of u's */
    HC_Open_Segment("u"); 
      for(i=0,val = stripe->val ;i<stripe->n;i++,val++) {
	sval = 0.0000;		
	sno++;
if(stripe->step > 1)
	fprintf(fp,"3dcurve \t %s_u_%d",p->name(), sno);
else 
	fprintf(fp,"site \t %s",p->name());
	HC_Restart_Ink();
	
	for(j=0;j<stripe->step + 1;j++) {
	  sval += fstep;  if( j== stripe->step ) sval=1.0;
	  srot = theSail->CoordsAt(ON_2dPoint(*val,sval),RX_GLOBAL);
	 // if(!Get_Global_Coords(*val,sval,theSail , srot)) continue;
	  HC_Insert_Ink(srot.x,srot.y,srot.z);
	  fprintf(fp,"\t%f \t%f \t%f ",srot.x,srot.y,srot.z);	  
	}
	fprintf(fp," \n");
      }
    HC_Close_Segment();
  }
  else if(strieq(stripe->type,"v")) {  /* means at constant v, range of u's */
    HC_Open_Segment("v"); {
      for(i=0,val = stripe->val ;i<stripe->n;i++,val++) {
	sno++;	
if(stripe->step > 1)
	fprintf(fp,"3dcurve \t %s_v_%d",p->name(),sno);
else 
	fprintf(fp,"site \t %s",p->name());

	HC_Restart_Ink();
	sval = - fstep;
	for(j=0;j<stripe->step+1;j++) {
	  	sval += fstep; if(j == stripe->step ) sval=1.0;
		srot = theSail->CoordsAt(ON_2dPoint(sval,*val),RX_GLOBAL);
	 	// if(!Get_Global_Coords(sval,*val,theSail , srot)) continue;
	 	 HC_Insert_Ink(srot.x,srot.y,srot.z);
	 	 fprintf(fp,"\t %f \t %f \t %f",srot.x,srot.y,srot.z);	  
		}
	 fprintf(fp," \n");
     	 }
    }HC_Close_Segment();
  }
  HC_Restart_Ink();
  
  return(1);
}






