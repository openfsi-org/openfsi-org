/* the declarations for Compute_Stripes.c */

#ifndef STRIPES_16NOV04
#define STRIPES_16NOV04
//int Get_Global_Coords( const double u,const double  v,class RXSail *sail , ON_3dPoint &srot);

int Compute_All_Stripes(class RXSail *s,FILE *fp) ;
int Compute_Stripe(RXEntity_p p, int type, FILE *fp) ;

#endif //#ifndef STRIPES_16NOV04
