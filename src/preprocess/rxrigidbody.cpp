#include "StdAfx.h"
#include "QtCore"
#include "QDebug"
#include "RXSail.h"
#include "RXRelationDefs.h"
#include "RXSRelSite.h"
#include "f90_to_c.h"
#include "stringutils.h"
#include "cfromf.h"
#include "rxrigidbody.h"

/*
  A RXRigidBody declares that the nodes in its list are to have their displacements linked for analysis
  The attribute keyword $dofs=123456 is ignored.
  If any of the nodes has 6 degrees of freedom (ie is connected to a beam element) then a 6 dof rigid body
  is created:  the group of nodes translates and rotates rigidly.
  Otherwise, a 3 DOF rigid body is created: the group of nodes share the same translations and the group does not rotate.
  This is a bit wierd: it would be smarter to put this under user control via the attributes. Then we could replace
  the stiffest parts of the model (like corners) with rigid-bodies, which would help convergence a lot.

  If a node is common to several rigid bodies they are coalesced.
  This is done at resolve time because that is more efficient than merging the dofos each analysis cycle.
  HOWEVER (july 2012) there is a big questionmark over whether non-linear dofos (including RBs) have their origins correctly
  updated when merging and unmerging.
  */
/*
 *  June 2013 If we want to allow strings to be defined as rigid bodies its more complicated because
 * we don't know the string's nodes at resolve-time.  We get to know it at mesh-time.
 * So the rigid-body must hold its list of strings and extract their node-lists at Mesh time.
 * When a string gets re-meshed, we need to unmesh (?) any rigid-bodies that look at it.
 **/

RXRigidBody::RXRigidBody()
{
    assert("dont use this constructor"==0);
}
RXRigidBody::RXRigidBody( class RXSail *s):
    RXEntity(s),
    m_dofono(0),
    m_Needs_Meshing(0),
    m_IsDead(0)
{
}
RXRigidBody::~RXRigidBody()
{
    const int sli = this->Esail->SailListIndex();
    int TheDofoNo  =this->m_dofono;
    if( TheDofoNo >0){
        cf_set_dofo_eptr (sli, TheDofoNo,(class RXObject*) 0); // so the dofo destroy doesnt recur back here
        cf_delete_dofo(sli, TheDofoNo);
    }

}
int RXRigidBody::Dump(FILE *fp) const
{
    int rc=0;
    rc+=fprintf(fp,"\n\tNeedsMesh= %d, DofoNo=%d, IsDead=%d\n" , m_Needs_Meshing, m_dofono, m_IsDead);
    return rc;
}
int RXRigidBody::Compute(void)  // return 1 if the object moved requiring compute of its nieces
{
    int rc = (!this->m_Needs_Meshing);
    this->m_Needs_Meshing=1;
    return rc;
}
int RXRigidBody::Mesh(void)     //     count up the nodenos and go createrigidbodydofo
{
     m_Needs_Meshing=0;
    int rc=0;
    if(this->m_dofono>0)
    {
        rc= cf_delete_dofo (this->Esail->SailListIndex(),this->m_dofono); // new mar 18 2014
 //       return 0; // old mar 18 2014
    }
    if(this->m_IsDead)
        return 0;
    int index=0;class RXObject *oo;
    class RXSRelSite *s;
    std::vector<int> n;
    QString  att; (this->AttributeString());
    if(!this->AttributeGet("dof",att)) // expect $dof=123 or $dof=123456
        this->AttributeGet("fix",att); // grandfathering

    while (oo= GetOneRelativeByIndex(RXO_RIGIDBODY_NODE,index))
    {
        if(s= dynamic_cast<RXSRelSite *>(oo))
            n.push_back(s->GetN() );
        index++;
    }
    int nn=n.size();
    int *thenodes =new int[nn ];
    for(int k=0; k<nn;k++)
        thenodes[k] = n[k];

    this->m_dofono= cf_create_dofo_rigidbody (this->Esail->SailListIndex()  , nn, thenodes,(class RXObject*)  this,qPrintable(att),att.length());

    delete [] thenodes;
    m_Needs_Meshing=0;
    return 1;
}

int RXRigidBody::Resolve(void)
{
    int rc=0;
    // rigidbody:name: n1,n2,n3,... %end, atts (will be something like $fix=123456
    QString qline = this->GetLine();
    QStringList wds= rxParseLine(qline,RXENTITYSEPREGEXP );
    RXEntity_p s;

    int i, count=0;
    for(i=2;i<wds.size();i++)
    {
        if(wds[i].startsWith("%")  )
            break;
        QString &nn = wds[i];
        s = this->Esail->Get_Key_With_Reporting("site,relsite",qPrintable(nn));
        if(!s)
        {
            this->CClear();
            return 0;
        }
        s->SetRelationOf(this,child|niece,RXO_RIGIDBODY_NODE,count++);

    } // for i

    if(PC_Finish_Entity(this->type(),this->name(),this,0,0,qPrintable(wds.last()),qPrintable(qline))) {
        rc=1;
        this->Needs_Resolving= 0;
        this->Needs_Finishing = 1;
        this->SetNeedsComputing();
        this->TestAndMerge();
    }

    return rc;
}
int RXRigidBody::IsSixDOF() const
{
//  if(stristr(this->Attri butes(),"123456"))
//    return 1;
//  return 0;
  QString v;
  if(!this->AttributeGet("dof" ,v ))
  {
      if(!this->AttributeGet("fix" ,v )) //grandfather
          return 0;
  }
  return (v=="123456");
}
/*
  Logic for merging rigid bodies:
    Mark 'this' as dying.
   for each site in this rigid body {
    get a list of the site's RBs
    for each of these RBs (that aren't dead or dying)
        extract its nodes
         mark as dead
         recur
   }
    copy all that RB's nodes to here
    mark the other as dead


    August 2013 an extension
    We only collect nodes from other RBs if both 'this' and the other RB are '123456' types
  */
int  RXRigidBody::TestAndMerge()
{
    int  count=0,rc=0;
    if(this->Needs_Resolving)
        return 0;
    assert (!this->m_IsDead);
    std::set<RXObject*> siteset;
    this->m_IsDead=-1; // its in work;

 // find how many in Omap are RXO_RIGIDBODY_NODE
      std::set<RXObject*> ss = this->FindInMap(RXO_RIGIDBODY_NODE,RXO_ANYTYPE);
      count = ss.size();
      ss.clear();

    this->ExtractSites( siteset);
    std::set<RXObject*>::iterator i;

    for(i=siteset.begin(); i!=siteset.end();++i){ // *i is a site
        if(this->FindInMap( *i, aunt))
            continue;
        (*i)->SetRelationOf(this,child|niece,RXO_RIGIDBODY_NODE,count++);
    }
    this->m_IsDead=0;
    return rc;

}
int RXRigidBody::ExtractSites(    std::set<RXObject*> &siteset )   {
    class RXRigidBody *rb=0;
    std::set<RXObject*> ss = this->FindInMap(RXO_RIGIDBODY_NODE,RXO_ANYTYPE);
    std::set<RXObject*>::iterator i,j;

    for(i=ss.begin(); i!=ss.end();++i){ // *i is a site
        siteset.insert(*i);
        if(this->IsSixDOF() )
        {
          std::set<RXObject*> srb = (*i)->FindInMap(RXO_RIGIDBODY_NODE,RXO_ANYTYPE); // srb is a set of RBs
          for(j=srb.begin(); j!=srb.end();++j){ // *j is a rigidbody
            rb = dynamic_cast<class RXRigidBody *>(*j);
            assert(rb);
            if(rb->m_IsDead)
              continue;
            if(!rb->IsSixDOF())
              continue;
            rb->m_IsDead=-1;
            rb->ExtractSites( siteset); rb->m_IsDead=1;
          } // for j

        }
    }
}

int RXRigidBody::Finish(void)
{
    int rc=0;
    return rc;
}
int RXRigidBody::ReWriteLine(void)
{
    //rigidbody:name: n1,n2,n3,... %endlist, atts (will have something like $fix=123456
    int rc=0;
    QString newline;
    newline +=this->type();
    newline += RXENTITYSEPS;
    newline +=QString::fromStdWString(this->GetOName());
    newline +=RXENTITYSEPS;
    // all    RXO_RIGIDBODY_NODE childs
    // then  %end, atts
    std::set<RXObject*> s = this->  FindInMap(RXO_RIGIDBODY_NODE,RXO_ANYTYPE);
    std::set<RXObject*>::iterator i;
    for(i=s.begin(); i!=s.end();++i){
        newline +=RX_WSTRINGTOQSTRING( (*i)->GetOName() );
        newline += RXENTITYSEPS;

    }
    newline += "%endlist";
    newline +=RXENTITYSEPS ;
    newline += this->AttributeString();
    if(this->m_IsDead) newline +=",$dead";
    this->SetLine( qPrintable(newline) );
    return rc;
}
