


/* arclngth.c
   The ONLY routines to access offset objects
MODS
3.5.96  diagnostics for empty offin 


	Create	Just fills in the string. Sets Flag to 0

	Evaluate
		if Flag 0	calls Modify_By_Text

		THEN returns X*arcs{which] if Percent
			or X					if Percent=0;

	Modify By Text
		Calls  Transcribe
		 Sets X to the return value
		if "%" in string, sets X to X/arc[1]
								Percent=1

		Sets Flag to 1

	 Modify_By_Double
	 	sprints X into the string, sets Percent 0
		Sets Flag to 1

	Transcribe
	  	parses the string as <number> <units>
		Case A) Just a number. 
		CASE B) units are percent
					return N/100*arc[1]
		CASE C) Other units.
					Convert to Metres and return


 */
 
  
#include "StdAfx.h"
#include "RXSRelSite.h"
#include "RXSeamcurve.h"
#include "RXOffset.h"

//#include "seamcurve.h" // for GetArc
#include "panel.h"

#include "arclngth.h"


 int PC_Same_Offsets( RXOffset *o1 , RXOffset *o2,const sc_ptr  sc,const int which,const double tol) 
 {
	double x1,x2;
	x1 = o1->Evaluate(sc,which);
	x2 = o2->Evaluate(sc,which);

	if(fabs(x2-x1) < tol)
	{
		ON_String s1,s2("(No SC defined)\n");
		s1.Format( "(BUG)\nTwo offsets very similar but different\n Try smaller IGES Tolerance  %f %f \n",x1,x2);
		if(sc)  s2.Format(" on entity  '%s' \n", sc->name()); 
		sc->OutputToClient(s1+s2 ,3);
		//just for e debug test
		x1 = o1->Evaluate(sc,which);
		x2 = o2->Evaluate(sc,which);
		return(1);
	}
	return(0);
} 

 double Evaluate_Batpatch_Offset(RXOffset*offin,float arc) {

/*    offin  is in form "3.2%" . The trailing string can be one of
		% m mm ft in cm
*/
const char *units[6] = {"%","cm" , "in" , "ft" ,"mm" ,"m"}; 
const double f[6]     = { 0.01,0.01 , 0.0254, 0.3048, 0.001, 1.0 };

double x;
int k,n;
char s[32];
assert("please step though  Evaluate_Batpatch_Offset"==0);
strcpy(s," ");
char l_buff[50];
n=sscanf(l_buff,"%lf%s",&x,s); 	  /* WRONG. Could use flags */
offin->Set_Offstring(l_buff);

if (n==0) {
	char string[256];
         printf( "(Evaluate) bad offset&%S",offin->GetText().c_str());
	 
	return((float)0.0);
	}
	else if(n==1) {
		return(x);
	  }
for (k=0;k<6;k++) {
	if (strieq(s,units[k])) {
		x = x * f[k];
		if (k==0) x=x*arc;
		return(x);
		}
	}
return(x); /* default m */
}

