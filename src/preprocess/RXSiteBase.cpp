#include "StdAfx.h"

#include "RXSiteBase.h"

RXSiteBase::RXSiteBase(void):
	m_RXSite_N (RXSITEINITIAL_N) 
{
//	cout<< " virtual RXSiteBase constructor"<<endl;
    this->x=y=z=-999;
}

RXSiteBase::~RXSiteBase(void)
{
	 
}


RXSTRING RXSiteBase::TellMeAbout() const{
	RXSTRING rc(_T(" RXSiteBase::RXS_N= "));
#ifndef RXMESH
		rc+= TOSTRING( m_RXSite_N) 
		+ _T(" ") + TOSTRING(x )
		+ _T(" ") + TOSTRING(y )
		+ _T(" ") + TOSTRING(z )+ _T(" ");
#endif
	return rc;
}

