#pragma once
#include "RXInterpolationI.h"

class RXPressureInterpolationP :
        public RXInterpolationI
{
public:
    RXPressureInterpolationP(class RXSail *s);
    virtual ~RXPressureInterpolationP(void);
    int Read(const std::wstring& fname);

    int FromExternalArrays(SAIL *sail,std::vector<RXSitePt> cps, std::vector<double> dp);
    double  ValueAt(const ON_2dPoint& p);
    virtual int ValueAt(const ON_2dPoint& p, double*rv);
    RXSitePt& Pt(const int n){ return pts[n];}
    vector<RXSitePt>& Points(){ return pts;}
    int NPoints(){ return pts.size();}
    int Dump(const char* fname);
protected:
    double _ValueAt(const ON_2dPoint& p);
    virtual int _ValueAt(const ON_2dPoint& p, double*rv);
private:
    std::vector<RXSitePt> pts;

};
