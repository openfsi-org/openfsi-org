
#include<StdAfx.h>
#include <QDebug>
#include <QRegExp>
#include "RXSail.h"
#include "RXSRelSite.h"
#include "entities.h"
#include "etypes.h"

#include "rxsubmodel.h"
//    else if (strieq(Firstword,"submodel"))
//return(PCE_SUBMODEL);

RXsubModel::RXsubModel()
{
    assert("dont use this constructor"==0);
}
RXsubModel::RXsubModel( class RXSail *s):
    RXEntity(s),
    Needs_Meshing(0)
{
}
RXsubModel::~RXsubModel()
{

}
int RXsubModel::Dump(FILE *fp) const
{
    int rc=0;
    rc+=fprintf(fp,"\n");
    return rc;
}
int RXsubModel::Resolve(void)
{
    int rc=0;
    //submodel:name: atts (will be something like ($layernames=.+), ($objnames= nod.+)
    QString qline = this->GetLine();
    QStringList wds= rxParseLine(qline,RXENTITYSEPREGEXP );

    if(PC_Finish_Entity(this->type(),this->name(),this,0,0,qPrintable(wds.last()),qPrintable(qline))) {
        rc=1;
        this->Needs_Resolving= 0;
        this->Needs_Finishing = 1;
        this->SetNeedsComputing();
    }

    return rc;
}
// static
class RXsubModel* RXsubModel::SubModel(class RXEntity *e) // Test whether any submodel owns 'q'
{
    class RXsubModel* m=0;
    RXObject* m1, *m2=0,*m3=0;
    m1=  ( e->GetOneRelativeByIndex(RXO_SUBMODEL , 0) ) ;//fastest

    m=dynamic_cast <class RXsubModel*>(m1);
 //   if(m)
        return m;
//    set<RXObject*>s= e->FindInMap(RXO_SUBMODEL,RXO_ANYTYPE);//slow
//    if(s.size() )
//        m2= *(s.begin());
//    m=dynamic_cast <class RXsubModel*>(m2);
//    if(m)
//        return m;

//    m3=   e->GetOneRelativeByLien(RXO_SUBMODEL ,aunt) ;
//    m=dynamic_cast <class RXsubModel*>(m3);
//    assert(!m);
//    return m;
}

bool RXsubModel::ConnectQ(RXSRelSite*q )// Test whether this submodel connects 'q'
{
    // q may have multiple RXO_SM_CONNECTs
   //  return true if q contains a RXO_SM_CONNECT to a connect whose other RXO_SM_CONNECT is to an object
   //  whose submodel is this.
    RXSRelSite* q2;
   set<RXObject*>s1 = q->FindInMap(RXO_SM_CONNECT,RXO_ANYINDEX) ;
   set<RXObject*>::iterator it1, it2;
   for(it1=s1.begin(); it1 !=s1.end(); ++it1)
   {
       RXObject* c = *it1; // a connect
       set<RXObject*>s2 = c->FindInMap(RXO_SM_CONNECT,RXO_ANYINDEX) ;
       for(it2=s2.begin(); it2 !=s2.end(); ++it2)
       {
           q2 = dynamic_cast<RXSRelSite*>  (*it2);
           if(q2==q)
               continue;
           if( SubModel(q2)==this)
               return true;
       }
   }
    return false;
}

int RXsubModel::CreateConnect(RXSRelSite*q1,RXSRelSite*q2)
{
    // make a RXO_SM_CONNECT relation between this and the two sites
    /* connect	q1_c_q2 	site 	q1 	boat 	site 	q2 	genoa 	 */
    QString line, qcname;
    qcname = QString(q1->name()) + QString ("_c_")+ QString(q2->name()) ;
    RXEntity_p se=0;
    int depth = 1 + this->generated;
    line = QString("connect :") + qcname + " : site : " +q1->name() + ": " + q1->Esail->GetQType()
            + " : site : " +q2->name() + ": " + q2->Esail->GetQType()  ;
  // qDebug()<< " create submodel connect '"<<line;
    se=Just_Read_Card(this->Esail ,qPrintable( line) ,"connect",qPrintable( qcname ),&depth);
    if(se) {
        q1->SetRelationOf(se ,spawn|child|niece,	RXO_SM_CONNECT,0 );
        q2->SetRelationOf(se ,spawn|child|niece,	RXO_SM_CONNECT,1 );
        se->Resolve();
        se->Needs_Finishing=1;
        se->SetNeedsComputing();
    }
 return 0;
}



int RXsubModel::Finish(void)
{
    //    the submodel trawls the objects.
    //   If it finds one which matches either name or layer, it
    //   does a SetRelation.

    int rc=0;
    RXEntity_p e;
    QString ln;
    SAIL * theSail = this->Esail;
    int DoIt, UseNames=0, UseLayers=0;
    QRegExp wlayer;
    QRegExp  wname;
    this->SetNeedsComputing(0);
    if( !this->Needs_Finishing)  return 0;
    if (this->AttributeGet("layernames",ln)){
        UseLayers++; wlayer.setPattern(ln);
    }
    if (this->AttributeGet("objnames",ln)){
        UseNames++; wname.setPattern(ln);
    }

    for (ent_citer  it = theSail->MapStart(); it != theSail->MapEnd(); it=theSail->MapIncrement(it)) {
        e  = dynamic_cast<RXEntity_p>( it->second);
        if(e==this)
            continue;
        DoIt=0;
        if(UseNames)
            if(wname.exactMatch(QString(e->name())))
                DoIt++;

        if(UseLayers){
            if(e->AttributeGet("layer",ln)  ){
                if(wlayer.exactMatch(ln))  DoIt++;
            }
        }
        if(DoIt)
        {
            this->SetRelationOf(e,niece,RXO_SUBMODEL); this->SetNeedsComputing(); rc++;
        }
    }
    this->Needs_Finishing=0;
    return rc;
}
int RXsubModel::ReWriteLine(void)
{
    //submodel:name: atts (will be something like ($layernames=.+), ($objnames= nod.+)

    QString newline (this->type());
    newline += RXENTITYSEPS;
    newline +=QString::fromStdWString(this->GetOName());
    newline +=RXENTITYSEPS;
    newline += this->AttributeString();

    this->SetLine( qPrintable(newline) );
    return 1;
}
int RXsubModel::Compute(void)
{
    int rc=0;
    this->Needs_Meshing=1;
    return rc;
}

int RXsubModel::Mesh(void)
{
    int rc=0;
    if(!this->Needs_Meshing)
        return 0;
    // declarations
    double distSq = this->Esail->m_Linear_Tol; distSq=distSq*distSq;
    set<RXObject*>::iterator it1;
    set<RXSRelSite*> mySites,otherSites;
     set<RXSRelSite*> ::iterator   it1s,it2s;
    vector<RXEntity* >others;
    vector<RXEntity* >::iterator itv;
    RXSRelSite*q1,*q2;
    RXEntity_p p ;
    // all sites in this model
    set<RXObject*>  s1 = this->FindInMap(RXO_SUBMODEL,RXO_ANYINDEX) ;

    for(it1=s1.begin();it1!=s1.end(); ++it1) {
        q1 = dynamic_cast<RXSRelSite*>(*it1);
        if(!q1)
            continue;
         mySites.insert(q1);
    }
    // all sites in other models

    this->Esail->MapExtractList(RELSITE,others ,PCE_ISRESOLVED) ;
    this->Esail->MapExtractList(SITE,others ,PCE_ISRESOLVED);
    for (itv = others.begin (); itv != others.end(); itv++)  {
        q2  = (Site*) *itv;
        if (RXsubModel::SubModel (q2) !=this)
            if (! this->ConnectQ(q2) )
                otherSites.insert(q2);
    } // for itv
    others.clear();
    // meshing means :
    // for all sites in this submodel (in mySites)
    //  search for a close site in another submodel (or in NO submodel)
    //   If I find one, and it isnt already subject of a connect  to this's node
    //      generate a connect object.
    //      mark
    //    endif
    // end for
    // NOTE   Connects may exist but not (yet) be resolved
    // we dont want to create a second connect in the case where we are here
    // before the connects have been resolved. so we cannot use RXO_CONNECT_OF_SITE
    //   because its relations are only set when a connect is Resolved

    // It's not enough to flag the site as 'connected'
    // because one site may be connected to multiple other sites, each in a different submodel (or in none)


    for ( it1s=mySites.begin(); it1s!=mySites.end(); ++it1s)   // all in this submodel
    {
        q1 = *it1s;
    for ( it2s=otherSites.begin(); it2s!=otherSites.end(); ++it2s)  // only those with NO connection via this submodel
        {
        q2=*it2s;
        if(!SubModel(q2)) // dont connect to nodes without submodel
            continue;
            if((*q2-*q1).LengthSquared() > distSq)
               continue;

         this->CreateConnect(q1,q2);
        }//for each othersites
    } //for each mySites
    this->Needs_Meshing=0;
    return rc;
}























