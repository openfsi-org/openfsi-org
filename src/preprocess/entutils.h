#ifndef _ENTUTILS_H_
#define _ENTUTILS_H_

#include "griddefs.h"
int  PCS_IsNotAmbiguous(RXEntity_p p);
//char* All_But_First_Two_Words(char * line, char *seps);

extern  int Make_Name_Unique(SAIL*const p_sail,const char*type, char *name);
extern int Make_Name_Unique(SAIL*const p_sail,const RXSTRING type, RXSTRING &name);
#endif  //#ifndef _ENTUTILS_H_


