#ifndef _RXMATHLINK_H__
#define  _RXMATHLINK_H__
#ifdef MATHLINK
#include "mathlink.h"



	#ifdef __cplusplus
		class RXmaterialMatrix;
		#include "opennurbs.h"
		#define EXTERN_C extern "C"
		#undef BOOL// for ON
	#else
		#define EXTERN_C extern
		#undef BOOL  //added by thomas 09 11 04
	#endif  //#ifdef __cplusplus

/* This is the Aldus Placeable metafile header, used by everyone that
   manipulates Windows metafiles.  The pragmas adjust packing so that
   byte alignment is identical to the struct compiled under a 16-bit
   app, since this is a native 16-bit data type */
#pragma pack(push)
#pragma pack(2)
typedef struct
    {
    DWORD      dwKey;
    WORD       hmf;
    SMALL_RECT bbox;
    WORD       wInch;
    DWORD      dwReserved;
    WORD       wCheckSum;
    } APMHEADER;
#pragma pack(pop)

/*********************************************************************
   VCFE.H -- Visual C++ Mathematica Front End
 *********************************************************************/

#define DEFAULT_PROTOCOL     ""
#define DEFAULT_MODE         "Launch"

#define   LINK_NAME          "-linkname"
#define   LINK_PROTOCOL      "-linkprotocol"
#define   LINK_MODE          "-linkmode"
#define   LINK_MATHLINK      "-mathlink"

#define   EMPTY_STRING       ""
#define   SPACE_STRING       " "
#define   SINGLEQUOTE_STRING "'"
#define   DOUBLEQUOTE_STRING "\""
#define   END_STRING         '\0'
#define   NULL_STRING        NULL

#define UPPER(c)             (( 'a' <= (c) && (c) <= 'z' ) ? ( (c) - 'a' + 'A' ) : (c))

#define SMALL_BUFFER         32
#define LARGE_BUFFER         2048

#define IDC_EVALUATE         10
#define IDC_INTERRUPT        11
#define IDC_ABORT            12
#define IDC_CONNECT          13
#define IDC_QUIT             14

#define IDC_EDITINPUT        20
#define IDC_EDITOUTPUT       21
#define IDC_EDITMESSAGES     22
#define IDC_INPUT            23
#define IDC_INPROMPT         24
#define IDC_OUTPUT           25
#define IDC_OUTPROMPT        26
#define IDC_MESSAGES         27
#define IDC_GRAPHICS         28
#define IDC_GRAPHICWINDOW    29

#define IDC_LDEDIT           30
#define IDC_LDLINKPROTOCOL   31
#define IDC_EDITLINKPROTOCOL 32
#define IDC_STATICLINKMODE   33
#define IDC_EDITLINKMODE     34
#define IDC_LDSEARCH         35
#define IDC_LDLAUNCH         36
#define IDC_LDSTATIC         37

#ifdef __cplusplus
	struct RXM_Return_Type_t {
		int m_what;

		ON_String m_IDC_EDITINPUT ;        
		ON_String m_MathOutput ;       
		ON_String m_IDC_EDITMESSAGES ;     
		ON_String m_IDC_INPUT   ;          
		ON_String m_IDC_INPROMPT  ;        
		ON_String m_IDC_OUTPUT   ;         
		ON_String m_IDC_OUTPROMPT  ;       
		ON_String m_IDC_MESSAGES  ;        
		ON_String m_IDC_GRAPHICS  ;       
		ON_String m_IDC_GRAPHICWINDOW ;  
		HENHMETAFILE m_hemf;
	};
	typedef struct RXM_Return_Type_t RXM_Return_Type;
#endif

/*
       " -linkname '""C:\\Program Files\\Wolfram Research\\Mathematica\\5.2\\MathKernel.exe"" -mathlink'
	   -linkmode Launch"
  */
#define REMOTE_NAME   "-linkname 6000@WRKST1_WIN  -linkprotocol TCP -linkconnect"

#define MATH_AS_SLAVE  " -linkname '""C:\\Program Files\\Wolfram Research\\Mathematica\\5.2\\MathKernel.exe"" -mathlink' -linkmode Launch"
#define MATH_ON_WRKST1  "  -LinkProtocol TCPIP -LinkHost 'WRKST1_WIN' -linkname '""C:\\Program Files\\Wolfram Research\\Mathematica\\5.2\\MathKernel.exe"" -mathlink' -linkmode Launch "

extern const char initstring[];
#define RXM_NO_MATHSESSION	64
#define RXM_SERIOUSERROR	32
#define RXM_READERROR		16
#define RXM_OK				1

class layernodestack;

class RX_MathSession 
{
public:

	MLINK Get_MLink();

		RX_MathSession();
		~RX_MathSession();
		int  RXM_PrintEvaluation(const char*inputstring);
		RXM_Return_Type RXM_EvaluateBlocking(const char*s);

		ON_Matrix RXM_EvaluateMatrixUV(const char *p_f,double p_u, double p_v);
		int RXM_EvaluateMatrix(const char *p_f,ON_Matrix &p_m);
		ON_4dPointArray RXM_EigenSolution(ON_Matrix &theM, int *err);
		ON_Matrix RXM_EvaluateDoveMatrix(char *p_f,double p_u, double p_v,ON_3dVector& p_x,int*err);
		int RXM_peterstest(FILE *fp);
		ON_ClassArray<layernodestack>  RXM_GetLayerStack( RXmaterialMatrix &pm, RXmaterialMatrix &qply,  double *pError);
		ON_ClassArray<layernodestack>  RXM_GetLayerStack(const double u,const double v,double *pError,double*pTrace);

		int RXM_InstallMe();
		BOOL RXM_Open_Mathematica_Session(const char*p_path,const char*p_init);
		int RXM_PrintWhatsThere( ) ;
		BOOL RXM_Close_Mathematica_Session();


	 int RXM_Fan(
			long int p_id, 
			ON_2dPointArray &p_dom, //.containing the edge of the domain as a closed loop of (u,v) pairs.
			ON_ClassArray<int> &p_c ,//indicating the indices of the vertices at the corners
			ON_CurveArray &p_twodeeout, //to contain the output 2Deed shape.
			ON_CurveArray *p_fanout, //(which may be NULL)  to contain the output fanned shape.
			int control_flag);

			BOOL               Quit;
	private:
		char *RXM_ReplaceSlashZeroOneTwo(char *string);
		char *RXM_ReplaceSlashZeroOneOne(char *string);
		HENHMETAFILE  RXM_OpenWMFFile(const char *filestring);

			MLENV              mlenvp;
			MLINK              mlinkp;
			WNDPROC            wpOriginalGraphicProc;
			HENHMETAFILE       hemfGlobal;
			HINSTANCE          hInstanceGlobal;
			HACCEL             hAccel;

			int					m_Flag;
	}; // end of the class
#ifdef WIN32
		EXTERN_C int RXM_DisplayGraphic(HWND hwnd, int GraphicItem, HENHMETAFILE hemf, HDC hdcGraphic);
#endif
	EXTERN_C int RXM_CheckError( MLINK *link);
//	EXTERN_C int MLInstall( MLINK p_mlp); // from mprepped source
//	EXTERN_C int MLDoCallPacket( MLINK p_mlp); // from mprepped source

	EXTERN_C WORD RXM_CalculateAPMCheckSum(const APMHEADER * apmfh);

#endif // no MATHLINK defined
#endif //  _RXMATHLINK_H__




