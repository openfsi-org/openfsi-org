/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
  Aug 2003 some asserts for buffer lenghts
 *  23.9.97 Big change. No longer takes sc->e[0] and e[1] as first and last points
  Rather it only looks in the IALIST
 *    26/4/96 uses zero_off, hundred_off from SC rather that create its own
   = remove a memory leak and reduce the chance ofdoubel-free
 *	  debug print to trace why gaussian golf ellipse fell over
 *   30/3/96 some trouble re-gridding of TPNS. seems to be fixed
 *		bt Compare_Pside_Lists returnins 1 always, provided that
 *		rename_Pside doesnt try to rename to the same name
 * 	 march 96  modify to use the PCN_Offset
 *  27/2/96 Dont break CUT curves
 *  6/1/95			Sort the nodes referenced by the new psides
      to save sorting them all afterwards
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */




/* this is the second break_Curve routine. It compares the proposed new PS list with the
  existing one before it deletes/re-inserts any psides.
  THis is an important optimisation, for inserting batpatches etc.
  15/10/94  mats offsets were not being RXFREEd. Fixed */

#include "StdAfx.h"
#include <QDebug>
#include "RXEntityDefault.h"
#define VeJo  hope-it-doenst-exist
#include "RXSRelSite.h"
#include "RXOffset.h"
#include "RXSeamcurve.h"
#include "RXPside.h"
#include "etypes.h"
#include "entities.h"
#include "panel.h"
#include "peternew.h"



int RXSeamcurve::Func_Compare_Pside( const void *va,const void *vb) {
    /* evaluate end1off and end2off of each pside
     if end1B >=end2A(+TOL)   A is before
  if end1A >=end2B(+TOL)   B is before
   */
    PSIDEPTR * pa, *pb;
    double x1a,x1b,x2a,x2b;
    sc_ptr sc;

    pa = (PSIDEPTR *) va;
    pb = (PSIDEPTR *) vb;

    sc= (*pa)->sc;

    x1b = ((*pb)->End1off->Evaluate(sc,1));
    x2a = ((*pa)->End2off->Evaluate(sc,1));
    if(x1b>= x2a-FLOAT_TOLERANCE) return(-1);

    x1a = ((*pa)->End1off->Evaluate( sc,1));
    x2b = ((*pb)->End2off->Evaluate( sc,1));
    if(x1a>= x2b-FLOAT_TOLERANCE) return(1);

    return(0);
}	
int RXSeamcurve::Sort_Pside_List()
{
    int N;
    N = this->npss;
    qsort(this->pslist,N,sizeof(PSIDEPTR *),Func_Compare_Pside);
    return(1);
}
int RXSeamcurve::Rename_Pside_List()
{
    sc_ptr np=this;
    int j;
    char name[256],type[256];
    RXEntity_p sco = dynamic_cast< RXEntity_p >(np);
    for (j=0;j< np->npss;j++) {
        RXEntity_p pso = np->pslist[j];
        sprintf(name,"%s(%d)",sco->name(),j); assert(strlen(name) < 255);
        strcpy(type,pso->type()); assert(strlen(type) < 255);

        pso->Rename(name,type);
    }
    return(1);
}
int RXSeamcurve::Compare_Pside_Lists(Site **ts,Site **ls,
                                     int newcount,int*Keep_Mark,int*Make_Mark)
{
    int i,j,nc;
    PSIDEPTR ps;
    nc = 0;
    std::vector<int> bad;
    bad.reserve(npss);
            for (j=0;j< this->npss;j++)
                bad[j] = pslist[j]->IsDisordered();

    for(i=0;i<newcount;i++) {
        Make_Mark[i]=1;
        for (j=0;j< this->npss;j++) {
            ps =  this->pslist[j];
            if(!bad[j] && ( ps->Ep(0)==ls[i] && ps->Ep(1)==ts[i]) ) {
                Keep_Mark[j]=1;  Make_Mark[i]=0;
                break;
            }
        }
    }
    for(i=0;i<newcount;i++) {
        if(Make_Mark[i]) nc++;
    }
    for (j=0;j< this->npss;j++) {
        if(!Keep_Mark[j])
        {
//#ifdef _DEBUG
            ps =  this->pslist[j];
            qDebug()<<" mark for deletion "<<ps->name()<<   ps->Ep(0)->name()<<" to  "<< ps->Ep(1)->name();
            qDebug()<<" When old sites are .";
                for(i=0;i<newcount;i++)
                    qDebug()<<ls[i]->name();
//#endif
            nc++;
        }
    }
    return(1);
    /*return(nc); MAYBE WRONG PH 30/3/96 */
}

/****************************************************/
int RXSeamcurve::Break_Curve() {
    /* Breaks down a SeamCurve into Psides. but does not set e??

 First sort IA.
 Then generate new candidates (ls ->ts) from IA
 Then compare the old and new lists
 Mark any psides for deletion if they don't match a candidate
 Mark any candidates for Making if they don't match a candidate
 Delete those psides marked
 Generate new psides from those candidates marked
 Sort the pside list into order
 Restore the material lists;


   End pointers.
 e1p, e2p are ptrs to relsite ENTITIES.
 e1,e2 are pointers to RELSITE
 e1,e2 MUST be (*e1p).dataptr. and this is done in Finish_Pside	  */

    int nchanges=0,	Make_Psides_Regardless=0, ist=0;

    if(!this->Make_Pside_Request ) 		return(0);
    Make_Psides_Regardless = (this->Make_Pside_Request>1);

    this->Make_Pside_Request =0;

    if (this->sketch )
        return(0);
    if(this->n_angles<2)
        return 0;
    else {
        RXEntity_p pp=NULL;
        Site* Last_Node=NULL,*This_Node=NULL;
        std::list<class RXPosOnCurve> mats;
        Site **ls=NULL;
        Site **ts=NULL;
        RXOffset*Last_Offset=NULL, *This_Offset=NULL;
        int newcount=0, *Make_Mark=NULL; int *Keep_Mark=NULL;

        Keep_Mark = (int*)CALLOC(this->npss+1,sizeof(int));
        Make_Mark = (int*)CALLOC(this->n_angles+1,sizeof(int));
        ts = (Site* *)CALLOC(this->n_angles+1,sizeof(RXEntity_p ));
        ls = (Site**)CALLOC(this->n_angles+1,sizeof(RXEntity_p ));

        ON_SimpleArray<RXOffset*> e1o;  //Thomas 28 OCT 04: WE CAN use ON_Simple array to store some pointer onto a class because we do not want to instanciate the oboejt in the cosntructor.
        ON_SimpleArray<RXOffset*> e2o;  //Thomas 28 OCT 04: WE CAN use ON_Simple array to store some pointer onto a class because we do not want to instanciate the oboejt in the cosntructor.
        e1o.SetCapacity(this->n_angles+1);
        e2o.SetCapacity(this->n_angles+1);
        e1o.SetCount(this->n_angles+1);
        e2o.SetCount(this->n_angles+1);

        int i;
        for (i=0;i<this->n_angles+1;i++)
        {	e1o[i]=e2o[i]=NULL;}

        assert(this->ia);
        Sort_IA(this->ia,this->n_angles);

        Put_Pside_Mats_Safe(this, mats);

        /* Find the index 'ist' of the first IA whose s is site or relsite */
        ist=this->n_angles;
        for (i=0;i<this->n_angles;i++) {
            if(this->ia[i].s->TYPE==SITE ||  this->ia[i].s->TYPE ==RELSITE ) {
                ist=i; break;
            }
        }
        Last_Node =dynamic_cast<Site*>((this->ia[ist]).s);
        e1o[0] =	Last_Offset =  (this->ia[ist]).m_Offset;
        This_Node = Last_Node;	This_Offset = Last_Offset;
        newcount=0;

        for (i=ist+1;i<this->n_angles;i++) {
            pp=this->ia[i].s;           /* the seamcurve abutting */
            if (pp->TYPE==SEAMCURVE)
                continue;
            else if(pp->TYPE ==SITE ||  pp->TYPE ==RELSITE ) 	   This_Node=(Site*)pp;
            else 			rxerror("(Break_Curve) unknown type",3);

            if (This_Node!=Last_Node) {

                e2o[newcount] = This_Offset =  (this->ia[i]).m_Offset;

                if(newcount) e1o[newcount] = Last_Offset;   /* site->Offset; e2o[newcount-1];   cut fiddling  site->Offset;   */
                ts[newcount]=This_Node; ls[newcount] =Last_Node; newcount++;
                Last_Node=This_Node;	 Last_Offset =  This_Offset;
            }

        }     /* end for */

        /* compare old and new lists */
        nchanges = this->Compare_Pside_Lists(ts,ls,newcount,Keep_Mark,Make_Mark);

        /* delete old psides not marked to keep */
        if(nchanges || Make_Psides_Regardless) {
            if(this->npss) {
                int oldnpss;
                RXEntity_p *oldps=(RXEntity_p *)MALLOC(this->npss*sizeof(RXEntity_p ));
                oldnpss = this->npss;
                memcpy(oldps,this->pslist,this->npss*sizeof(RXEntity_p ));
                for(i=0;i<oldnpss;i++) {
                    if(!Keep_Mark[i] || Make_Psides_Regardless){
                        oldps[i]->Kill();  //March 2015/* Modifies pslist */
                    }
                }
                RXFREE(oldps);
            }
            /* append_ any seglist items  marked as make */
            for(i=0;i<newcount;i++) {
                if(Make_Mark[i]|| Make_Psides_Regardless) {
                    this->Append_One_Pside(&ts[i],&ls[i],e1o[i],e2o[i]);
                    /* makes a new pside with postsc (new) */
                }
            }

            /* sort the psides into order */
            this->Sort_Pside_List();
            /* the names need re-making. New pside names are incorrect */
            Rename_Pside_List();

            /* regenerate the pside materials WRONG make sure they dont already have  materials, otherwise we double */
            Restore_Pside_Mats(this, mats);  /*defines matlists on the psides as before */
            /* sort the nodes afected */
            for(i=0;i<newcount;i++) {
                ls[i]->Sort_One_Node();
            }
            if(newcount>0)ts[newcount-1]->Sort_One_Node(); /* the if.. is a protection against delete bugs */

        }

        //CLEAN UP
        RXFREE(ts); RXFREE(ls);

        for (i=0;i<e1o.Count();i++)		{	e1o[i]=NULL;}
        for (i=0;i<e2o.Count();i++)		{	e2o[i]=NULL;}
        e1o.Empty();
        e2o.Empty();

        RXFREE(Make_Mark); RXFREE(Keep_Mark);
    }

    return(this->npss);
}// int Break_Curve

