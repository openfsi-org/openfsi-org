/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by P Heppel  February 1996
  5.2003  revised Compute_Snap_Intersections so that it ONLY deals with the ends. Leave CUT to deal with the middles.
  11.98  remove the old rtine.
 * 14.6.96   Delete and deps as a test. Maybe wrong but lets see

* 15.5.96  don't cut Cut lines

 *	status at 3/3/96
 Appears to work.
 Slow because the SCs are deleted and regenerated
 WRONG for RLX designs. The crank angle calc should look at the tangents just either
 side of the cut.
 Only works for 'sketch' cuts inserting flat curve SCs
 Should take the child SC info from the card.
 
 To get the arc-length, look at the compound curve <name>_CC

 *
 *
 */
#include "StdAfx.h"

#include "RXEntityDefault.h"
#include "RXSRelSite.h"
#include "RXOffset.h"
#include "RXSeamcurve.h"
#include "RXSail.h"
#include "rxsubmodel.h"
#include "global_declarations.h"

#ifdef linux
#include "../../stringtools/stringtools.h"
#else
#include "..\stringtools\stringtools.h"
#endif

#define CUTDB (0)
#include "entities.h"
#include "stringutils.h"
#include "RXCurve.h"
#include "etypes.h"
#include "iangles.h"

#include "slowcut.h"

#include "cut.h"


int PCC_Parallel_SC_Sense(sc_ptr sc1, sc_ptr sc2,char*o1,char *o2) {
    // return 1 if the two scs are head-to-tail
    // return 0 if the 2 scs are head-to-head
    ON_3dPoint p1; ON_3dVector t1;
    int c2;
    double s1,s2,dist,l_tol=sc1->Esail->m_Linear_Tol;

    // o1 and o2 are the offsets on sc1 of the ends of sc2
    // o1 must be lower than o2.
    // start of sc2 is sc2->C[1].m_p
    //end of sc2 is sc2->C[1].m_p[c2]

    //c1 = sc1->C[1].Get_c()  -1;
    c2 = sc2->m_pC[1]->Get_c()  -1;
    // start of sc2 on sc1
    s1=0.0;
    s2 = sc2->GetArc(1);
    VECTOR l_p;

    if (sc2->m_pC[1]->GetONCurve())
    {
        const ON_Curve * l_crv = dynamic_cast<const ON_Curve*>(sc2->m_pC[1]->GetONCurve());
        if (!l_crv)
        {
            sc1->OutputToClient("sc2->C[1].m_ONCurve  is not a ON_Curve object, in RXCurve::Drop_To_Curve() ",2);
        }
        ON_3dPoint l_point = l_crv->PointAtStart();
        l_p.x=l_point.x; l_p.y=l_point.y;l_p.z=l_point.z;
        if (0 >= sc1->m_pC[1]->Drop_To_Curve(l_point ,&p1,&t1,l_tol,&s1,&dist))
            sc1->OutputToClient("PC_Drop_To_Curve failed in PCC_Parallel_SC_Sense, cannot drop point on curve (ONCurve)",2);

        l_point = l_crv->PointAtEnd();
        l_p.x=l_point.x; l_p.y=l_point.y;l_p.z=l_point.z;
        if (0 >= sc1->m_pC[1]->Drop_To_Curve(l_point ,&p1,&t1,l_tol,&s2,&dist))
            sc1->OutputToClient("PC_Drop_To_Curve failed in PCC_Parallel_SC_Sense, cannot drop point on curve (ONCurve)",2);

        //Clean up
        if (l_crv) l_crv=NULL;
    }// if (sc2->C[1].GetONCurve())
    else
    {
        l_p = *(sc2->m_pC[1]->Get_p()); // means the start point

        ON_3dPoint ppp (l_p.x,l_p.y,l_p.z);
        if (0 >= sc1->m_pC[1]->Drop_To_Curve(ppp ,&p1,&t1,l_tol,&s1,&dist))
            sc1->OutputToClient("PC_Drop_To_Curve failed in PCC_Parallel_SC_Sense, cannot drop point on curve (polyline)",2);

        l_p = sc2->m_pC[1]->Get_p()[c2];  // means the last point
        ppp = ON_3dPoint(l_p.x,l_p.y,l_p.z);
        if (0 >= sc1->m_pC[1]->Drop_To_Curve(ppp ,&p1,&t1,l_tol,&s2,&dist))
            sc1->OutputToClient("PC_Drop_To_Curve failed in PCC_Parallel_SC_Sense",2);

    }//else


    // end of sc2 on sc1

    if(s2 > s1){
        sprintf(o1,"%f",s1);
        sprintf(o2,"%f",s2);
        return 0;
    }
    else {
        sprintf(o1,"%f",s2);
        sprintf(o2,"%f",s1);
        return 1;
    }


    return 0;
}
int PCC_Merge_Material_Properties(RXEntity_p e1,RXEntity_p e2,RXEntity_p pd){
    sc_ptr sc1;
    sc_ptr sc2;
    RXEntity_p e;
    char buf[1024];

    char o1[128], o2[128];

    int  k, l,l2, rev, iret =0 ;
    //  We are about to delete pd, which is either e1 or e2.
    //	pd may have material properties that are not in the one that is staying (e)
    //	In this case we ant to transfer them
    //	but we should only do this if the two SCs are identical
    //  Jan 01.  We put the atts of pd into the partial att list of e
    //  so we no longer need to transfer
    if(e1==pd) e=e2;
    else	e=e1;
    double l_Ctol = e->Esail ->m_Cut_Dist ;
    sc1 = (sc_ptr  )e;	//staying
    sc2 = (sc_ptr  )pd;	//going
    rev = PCC_Parallel_SC_Sense(sc1,sc2, o1,o2);
    // o1 and o2 are the offsets on sc1 of the ends of sc2
    // o1 must be lower than o2.

    cout<< "TODO::Append_Partial_Atts(sc1,pd,o1, o2, pd->attributes, rev);"<<endl;
    if(fabs(sc1->GetArc(1) - sc2->GetArc(1)) > 2.0 * l_Ctol)
        return 0; // lengths are different.


    for(l=0,k=1;l<3;l+=2,k++) {
        l2 = l;
        if(rev) l2 = 2-l;
        if(sc2->Mat_Angle[l].flag ) {
            memcpy(&sc1->Mat_Angle[l2],&sc2->Mat_Angle[l],sizeof(struct SIDE_ANGLE));

            //April 2003 without this there was a double-free, I think
            if( sc2->Mat_Angle[l].str)
                sc1->Mat_Angle[l2].str=STRDUP(sc2->Mat_Angle[l].str);

            iret++;
        }
        if(sc2->Defined_Material[l]){

            if(sc1->Defined_Material[l2]  && (sc1->Defined_Material[l2] != sc2->Defined_Material[l])) {

                sprintf(buf,"in delete of '%s'\n materials are transferred to\n '%s'\n where material was '%s' \n is '%s'",
                        pd->name(), e->name(), sc1->Defined_Material[l2]->name(), sc2->Defined_Material[l]->name());
                sc2->OutputToClient(buf,1);

            }
            sc1->Defined_Material[l2] = sc2->Defined_Material[l];
            //			now prepend ($mat1=mat) to sc1's atts


            iret++;
        }
    }
    return iret;
}
RXEntity_p PCC_Which_Parallel_To_Delete(RXEntity_p e1,RXEntity_p e2){
    sc_ptr sc1, sc2;
    int panelseam1,panelseam2;
    // If one is an edge and the other isnt, delete the other.
    // otherwise, if just one contains the word 'panel ' delete the other.
    // otherwise, delete the shorter.
    sc1 = (sc_ptr  )e1;
    sc2 = (sc_ptr  )e2;
    if(sc1->edge && !sc2->edge)
        return e2;
    if(sc2->edge && !sc1->edge)
        return e1;
    panelseam1 = (NULL==stristr(e1->name(),"interface"));
    panelseam2 = (NULL==stristr(e2->name(),"interface"));
    if(panelseam1 && !panelseam2)
        return e2;
    if(panelseam2 && !panelseam1)
        return e1;
    if(sc1->GetArc(1) < sc2->GetArc(1))
        return e1;

    return e2;
}

int PCCut_Set_Cut_Limits(sc_ptr sc,double *upper, double *lower) {
    int err = 1;
    double l_Ctol = sc->Esail ->m_Cut_Dist ;
    if(sc->End1site)
        *lower = Show_Offset_By_Entity(sc, sc->End1site,&err);
    if(err)
        *lower = - l_Ctol;
    err=1;
    if(sc->End2site)
        *upper = Show_Offset_By_Entity(sc, sc->End2site,&err);
    if(err)
        *upper = sc->GetArc(1) + l_Ctol;

    return 1;
}//int PCCut_Set_Cut_Limits

int Delete_Generated_Cross_Sites(RXEntity_p e) {
    /* this should really be dealt with via a master/slave relationship even more severe than
the parent/dependent relationship . For now we'll fudge it 
    This routine is meant to spot any cross sites which belong to e and have greater depth
    and delete them*/
    sc_ptr sc = (sc_ptr  )e;
    RXEntity_p se;
    Site *ss;
    int i;
    for(i=0;i<sc->n_angles; i++) {
        se = sc->ia[i].s;
        if(se->TYPE== RELSITE || se->TYPE ==SITE) {
            ss = (Site *)se;
            if(ss->CUrve[0] && ss->CUrve[1] && se->generated > e->generated) {
                se->Kill( ); // but does its other sc get Needs_Cutting?
                i--;
            }
        }
    }


    return 1;
}
struct PCC_CROSS {
    double s;
    RXEntity_p sce;
    int t;
};

int Set_All_Cut_Flags(SAIL *sail) {

    sc_ptr sc;

    ent_citer it;
    for (it = sail->MapStart(); it != sail->MapEnd(); ++it) {
        RXEntity_p p  = dynamic_cast<RXEntity_p>( it->second);
        if(p->TYPE==SEAMCURVE ){ /* we allow unFinished scs provided that they have their curves. */
            if(CUTDB) printf(" Set_All_Cut_Flags test %s  and %s ...", p->name(), p->name());
            sc=(sc_ptr  )p;
            if(!sc) continue;
            if(sc->FlagQuery(CUT_FLAG) || sc->FlagQuery(SNAP_TO_NODES)) {
                sc->Needs_Cutting=1;
                p->SetNeedsComputing();
            }
        }
    }
    return 1;
}
int PCC_Check_Cut_Parameters(sc_ptr sc){
    char buf[256];
    int flag=0;
    double l_tol = sc->Esail ->m_Linear_Tol;
    double c_tol = sc->Esail->m_Cut_Dist;
    if( l_tol  > sc->GetArc(1) * 2.0) {
#ifdef _DEBUG
        double l = sc->GetArc(1);
        sprintf(buf,"Deleting short '%s' Len measured as %f", sc->name() ,l );
        sc->OutputToClient(buf,2);
#endif

        return PCE_DELETED;
    }

    if(c_tol > sc->GetArc(1)/2.0) {
        sprintf(buf,"'%s' has length %f\n so reducing Gauss Cut Distance\n from %f to %f",
                sc->name(), sc->GetArc(1), c_tol , sc->GetArc(1)/2.2);
        sc->OutputToClient(buf,1);
        sc->Esail->m_Cut_Dist = sc->GetArc(1)/2.2;
        flag++;
    }
    if(l_tol > c_tol/2) {
        sprintf(buf,"Gauss Cut Distance is %f\n so reducing Linear Tolerance\n from %f to %f",
                c_tol , l_tol ,c_tol/2. );
        sc->OutputToClient(buf,1);
        sc->Esail->m_Linear_Tol = sc->Esail->m_Cut_Dist/2. ;
        flag++;
    }
    if(flag) Set_All_Cut_Flags(sc->Esail );
    return 1;
}//int PCC_Check_Cut_Parameters

int PCCut_Define_EndNsites(sc_ptr sc){
    /* Some of the IAs will be crosses of edges; There may be any number.
    We take the highest one in the bottom half as End1site
    and he lowest one in the  top half as End2Site.
    SITES have higher priority than edge-crosses
    WHAT A HORRIBLE HEURISTIC!!!!
  */
    int j,k; // ,c=0; April 2003
    RXEntity_p e;
    sc_ptr sc2;
    Site *p ;
    struct PCC_CROSS *crosses ;
    if(!sc->n_angles) return 0;  	//calloc doesnt like 0
    double l_Ctol = sc->Esail->m_Cut_Dist;

    crosses = (PCC_CROSS *)CALLOC(sc->n_angles, sizeof(struct PCC_CROSS ));

    Sort_IA(sc->ia,sc->n_angles);

    for (k=0;k<sc->n_angles ;k++) {
        e = sc->ia[k].s ;
        p=NULL; // debug.

        if(e->TYPE==SITE) {
            crosses[k].sce = e;
            crosses[k].s = (sc->ia[k].m_Offset->Evaluate(NULL,1));
            crosses[k].t = SITE;
        }
        else if(e->TYPE==RELSITE) {
            p = (Site *)e;
            for(j=0;j<2;j++) {
                if(p->CUrve[1-j] && p->CUrve[j]==(RXEntity_p) sc){
                    sc2 = (sc_ptr  )p->CUrve[1-j] ;
                    if(sc2->edge) {
                        crosses[k].sce = e;
                        crosses[k].s = (sc->ia[k].m_Offset->Evaluate(NULL,1));
                        crosses[k].t = SEAMCURVE;
                    }
                }
            }
        }
        else
            printf("(PCCut_Define_EndNsites) only takes SITE,RELSITE Unrecognised Ent TYPE (%s %s) for this IA\n",e->type(),e->name());
    }
    /* we now have the (sorted) crosses of edges and SITES
    first trawl for SITES, then for edges*/
    sc->End1site = sc->End2site= NULL;
    for(k=0;k<sc->n_angles;k++) {
        if(crosses[k].s < 2*l_Ctol && crosses[k].t== SITE){
            sc->End1site= (Site*) sc->ia[k].s ;
            sc->End1site->SetRelationOf(sc,child|niece,RXO_N1_OF_SC);
            if(CUTDB) printf(" highest  bottom SITE %s %s\n",sc->ia[k].s->type(),  sc->ia[k].s->name());
        }
        else if(crosses[k].s > sc->GetArc(1)- 2*l_Ctol && crosses[k].t== SITE)	{
            sc->End2site= (Site*) sc->ia[k].s ;
            sc->End2site->SetRelationOf(sc,child|niece,RXO_N2_OF_SC);
            if(CUTDB) printf(" lowest one at top SITE %s %s\n", sc->ia[k].s->type(), sc->ia[k].s->name());
            break;
        }
    }
    /* Now trawl for any */
    for(k=0;k<sc->n_angles;k++) {
        if(! sc->End1site && crosses[k].t && crosses[k].s <  2*l_Ctol){
            sc->End1site= (Site*) sc->ia[k].s ;	            sc->End1site->SetRelationOf(sc,child|niece,RXO_N1_OF_SC);	//highest one at bottom
            if(CUTDB) printf(" highest  bottom  ANY %s %s\n",sc->ia[k].s->type(),  sc->ia[k].s->name());
        }
        if(! sc->End2site && crosses[k].t && crosses[k].s > sc->GetArc(1) - 2*l_Ctol){
            sc->End2site= (Site*) sc->ia[k].s ;            sc->End2site->SetRelationOf(sc,child|niece,RXO_N2_OF_SC);
            if(CUTDB) printf(" lowest one at top  ANY %s %s\n", sc->ia[k].s->type(), sc->ia[k].s->name());
            //lowest one at top
            break;
        }

    }
    if (crosses)
    {
        RXFREE(crosses);
        crosses=NULL;
    }
    return 1;
}//int PCCut_Define_EndNsites

int PCCut_Remove_Crosses_Beyond_Ends(sc_ptr sc){
    /* if there are endNptrs, delete any Xsites beyond them provided that
    one of their curves belongs to sc
    separately, we have to:
    3)	Once we have EndNptrs, do not allow cut searching beyond the end.
    Nor do we allow Searchfor Close Sites to go past the end
    4)	Define an EndNptr when we find a cross to an edge.
    NOTE:  we could loop if
    */
    int j,k,c=0;
    RXEntity_p  p, q;
    Sort_IA(sc->ia,sc->n_angles);

    for (k=0;k<sc->n_angles ;k++) {
        p = sc->ia[k].s ;
        if(p == sc->End1site ){	// delete all below
            for(j=0;j<k;j++) {
                q = sc->ia[j].s ;
                if(CUTDB) printf(" PCCut_Remove_Crosses_Beyond_End 1 Delete %s %s\n",q->type(),q->name());
                q->Kill( );
                j--; k--; c++;
            }
            break;
        }
    }
    for (k=0;k<sc->n_angles ;k++) {
        p = sc->ia[k].s ;
        if(p == sc->End2site ){	// delete all above
            j=k+1;
            while(k+1<sc->n_angles) {
                q = sc->ia[k+1].s ;
                if(CUTDB) printf(" PCCut_Remove_Crosses_Beyond_End 2 Delete %s %s\n",q->type(),q->name());
                q->Kill( );
                c++;
            }

        }

    }
    return c;
}//int PCCut_Remove_Crosses_Beyond_Ends

int PCC_Find_Curve_Int(RXCurve *C1,
                       double s1s,double s1f,double*s1,
                       ON_3dPoint *v1,ON_3dVector*t1,
                       RXCurve *C2,double s2s,double s2f,double*s2,
                       ON_3dPoint*v2,ON_3dVector*t2)
{

    /*
     Find an intersection between C1 and C2
     seeding the search on s1 and s2
     Return PCC_IFOUND_OK if any intersection is between the limits s1s<s1<s1f, s2s<s2<s2f
otherwise

PCC_NOT_CONVERGED
PCC_OUT_OF_RANGE   
PCC_PARALLEL		

     An intersection is defined as a line normal to both curves,
      between	a point v1 at s1 on C1
      and		a point v2 at s2 on C2
Nov 00.  New function based on slowcut. 

 */
    int j;
    //double s2min=0.001;


    //VECTOR *p1,*p2;
    //int c1,c2;
    double *ss1, *ss2, errmin,err;
    int nc, retval,  iret,jmin;
    iret = 0;
    double l_CutTol = C1->Get_Ent()->Esail->m_Cut_Dist;
    double l_pTol = C1->Get_Ent()->Esail->m_Linear_Tol ;

    ss1=ss2=NULL;
    nc=0;
    //			retval = SlowCut(//e,
    //							 p1, c1,C1->Get_ds(),  p2, c2, C2->Get_ds(),  &ss1, &ss2, &nc,g_Gauss_Cut_Dist);
    retval = C1->IntersectRXC(*C2,&ss1, &ss2, &nc,l_CutTol,  l_pTol);
    // can only return PCC_OK,  PCC_PARALLEL
    switch (retval) {
    case PCC_PARALLEL : // some are parallel
    case PCC_OK:
    {
        iret = PCC_OUT_OF_RANGE;
        // bug sept 2002
        // find the J which minimises the difference (ss1[j]-*s1)^2 * ( ss2[j]- *s2)^2
        // provided that it is within the range
        // instead of taking the first.
        // then set *s1 and *s2 and get the tangents.
        jmin = -1;
        errmin = (s1f - s1s) * (s1f - s1s) * (s2f - s2s) * (s2f - s2s);
        for(j=0;j<nc;j++) {
            if(ss1[j] >= s1s && ss1[j] <= s1f &&
                    ss2[j] >= s2s && ss2[j] <= s2f) {

                err = (ss1[j]-*s1)* (ss1[j]-*s1) * ( ss2[j]- *s2)* ( ss2[j]- *s2);
                if(err < errmin) {
                    jmin = j;
                    errmin = err;
                }
            }
        } // end for j
        j = jmin;

        if(jmin >=0)
        {
            *s1 = ss1[j] ;
            C1->Find_Tangent(*s1, v1, t1);
            *s2= ss2[j];
            C2->Find_Tangent( *s2,v2, t2); //Not normalised
            iret =  PCC_IFOUND_OK ;
        }
        //else
        //{
        //	int toto = 4567;
        //}


        break;
    }
    default:{
        assert(0);
        iret=0;
    }
    }

    if(ss1) {RXFREE(ss1); ss1=NULL;}
    if(ss2) {RXFREE(ss2);	ss2=NULL;}

    //	if (iret==-2)
    //		int l_toto = iret;

    return iret;
}  //  PCC_Find_Curve_Int

int Compute_Cut_Intersections(sc_ptr sc){ /* 1998 SLOW version */
    int iret=0;
    // Feb  2005 don't seed not if it's a SC_3DM_CUT))
    /*  September 1997  New version
e must have flags 'cut, Needs Cutting'
All scs generate XSITES. The 
If they are 'fanned', this will follow in the normal course. else 'flat'
If they are 'draw' the redspace curve is discontinuous at shaped seams. else 'sketch'
If they are 'seam' we cant handle them at the moment. We could if we had discontinuous redcurves */

    sc_ptr sc2;
    int j;
    RXEntity_p se, e1,e2, *olde;
    RXsubModel *m1,*m2;
    char line[256],name[256];
    double *ss1, *ss2, upper , lower;
    int nc,flag,retval,depth, Old_NA;
    const char *pn;
    char buf[256];
    RXOffset*dummyo;
    if(sc->FlagQuery(SC_3DM_CUT)) {
        sc->Needs_Cutting =0;
        return 0;
    }
    m1 = RXsubModel::SubModel(sc);
    if(CUTDB) cout<<" \nstart NEW cutting with " <<sc->name();
    if(sc->Needs_Finishing){ sc->OutputToClient(" cant cut needs Finishing",2); return 0;}

    olde = (RXEntity_p *)MALLOC((1 + sc->n_angles)*sizeof(RXEntity_p ));

    for(j=0;j<sc->n_angles;j++)
        olde[j] = sc->ia[j].s;
    Old_NA = sc->n_angles;

    /* Compute all its relsites. That will delete any off the end.  */
    /* Search to see if any sites not in the IAlist are very close to the black-space line.*/
    /*  If one is, add it to the IAlist.*/
    /* The IA List may still have less than 2 entries - the minimum to define a pside
  to guard against this, we have to search the complete curve.
This is to get XYZabs curves (such as from autoCAD) started
NOTE that this will not trap all intersections of a complex curve with many inflections
or self-intersections. We just hope that it will trap enough to start off the search by psides
*/
    /*  for each segment (defined by the IAList), search for any crossing SCs.*/
    /*If we find one,   generate a XSITE then*/
    /* re-search that segment.*/

    /* Compute all its relsites. That will delete any off the end. */

    /* Search to see if any sites not in the IAlist are very close to the black-space line.
 If one is, add it to the IAlist. */
    sc->Remove_Distant_Sites(2.0*sc->Esail ->m_Linear_Tol );
    SearchForCloseSites(sc,sc->Esail->m_Linear_Tol);
    /* This Search should remove any sites (or Relsites) in the IA list whose geometry
 is not determined by this SC and which are no longer close to it.

*/

    /* search for any crossing SCs.
 If we find one,   generate a XSITE */

    PCCut_Set_Cut_Limits(sc,&upper, &lower);
    const char * l_name1	= sc->name();

    ent_citer it;
    for (it = sc->Esail->MapStart(); it != sc->Esail->MapEnd(); ++it) {
        sc2 = dynamic_cast<sc_ptr>( it->second);
        if(sc2==sc) continue;
        if(!sc2) continue;
        if(sc2->TYPE==SEAMCURVE ){ /* we allow unFinished scs provided that they have their curves. */
            if(sc2->Needs_Resolving)
                continue;
            m2= RXsubModel::SubModel(sc2);
            if(m1!=m2)
                continue;
            pn=sc2->name();
            if( !(sc2->sketch) && !(sc2->Needs_Finishing) )
            {
                ss1=ss2=NULL;
                nc=0;

                retval =(sc->m_pC[1])->IntersectRXC(*(sc2->m_pC[1]),&ss1, &ss2,
                                                    &nc,sc->Esail->m_Cut_Dist , sc->Esail->m_Linear_Tol  );

                // can only return 0, PCC_OK,  PCC_PARALLEL
                switch (retval) {
                case PCC_PARALLEL :{	// means some segs were parallel. Not necessarily bad.
                    RXEntity_p pd;
                    if(sc->m_pC[1]->Curves_Coincident(sc2->m_pC[1])) {
                        pd = PCC_Which_Parallel_To_Delete(sc,sc2);
                        sprintf(buf,"%s & %s coincident. demoting %s",sc->name(),sc2->name(), pd->name());

                        PCC_Merge_Material_Properties(sc,sc2,pd); // also sets the partials.

                        sc->OutputToClient(buf,1);
                        Delete_Generated_Cross_Sites(pd);// otherwise they are just unresolved

                        static_cast<RXSeamcurve*>(pd)->Demote_To_Sketch();

                        if(pd==sc) {
                            RXFREE(olde);
                            if(ss1) RXFREE(ss1);
                            if(ss2) RXFREE(ss2);

                            return PCE_DELETED;
                        }
                        else
                            nc=0;  // carry on so we fr e e things without referencing p
                    } //endif curves are coincident . dont break
                }
                case PCC_OK :
                {

                    for(j=0;j<nc;j++) {   //
                        if(ss1[j] <= lower || ss1[j] >= upper)
                            continue;
                        flag = Cross_Already(sc, ss1[j], sc2, ss2[j], &e1,&e2,sc->Esail->m_Cut_Dist );

                        switch (flag) {
                        case 1: { 	/* Insert an IA into the other sc but dont let our sc control the site pos.*/
                            sprintf(buf,"%f", ss2[j]);
                            dummyo= 0; assert("whats the context?"==0); //new RXOffset(NULL ,sc2,buf,(double)ss2[j]); //NULL means pt to offset
                            (dummyo->Evaluate(sc2 ,1));
                            sc2->Insert_One_IA(e1, dummyo);
                            if(CUTDB) cout<< " case 1 \n"<<endl;
                            break;
                        }
                        case 2: {		/* Insert an IA into our sc but dont let our sc control the site pos. */
                            sprintf(buf,"%f", ss1[j]);
                            dummyo=0; assert("whats the context?"==0); //new RXOffset(NULL,sc,buf,(double)ss1[j]);
                            (dummyo->Evaluate(sc ,1));
                            sc->Insert_One_IA(e2, dummyo);
                            if(CUTDB) cout<< " case 2 \n"<<endl;
                            break;
                        }
                        case 19:
                        case 3: {		/* no action. The cross is already identified. */
                            if(CUTDB) cout<< " case 3 no action \n"<<endl;
                            break;
                        }
                        case 11: {	/* An rxerror condition. Delete both sites and treat as a 0 */
                            //	assert(0);
                            sc->OutputToClient(" case 11 \n",2);
                            e1->Kill( );
                            e2->Kill( );


                        }
                        case 0: {		/*generate a cross site whose geometry is controlled by the two scs.*/

                            sprintf(name,"SS%s_%2.2f##%s_%2.2f",pn,ss2[j],l_name1,ss1[j]);
                            sprintf(line,"relsite:%s: %s : %f : intersect=%f cross=%s", name, pn, ss2[j],ss1[j],l_name1);
                            if(CUTDB) cout<<line<<endl;
                            depth = sc->generated + 1;
                            if(depth <= sc2->generated)
                                depth = sc2->generated + 1;

                            if((se = Just_Read_Card( sc->Esail , line,"relsite",name,&depth))) {
                                se->Resolve();
                                if(se->dataptr)
                                    se->Needs_Finishing=!se->Finish();
                                if(se->dataptr) // it wasnt deleted
                                    iret++;
                                // Finish_Relsite MAY decide to delete se if it disagrees about where it is
                            }
                            else
                                sc->OutputToClient(" Cut failed to insert an XSite",2);

                            break;
                        }
                        default: {
                            sc->OutputToClient("Cross_Already unknown case",2);
                        }

                        }/* end switch */

                    } /* for j */


                }
                default :
                    break;
                }// switch
                if(ss1) RXFREE(ss1); if(ss2) RXFREE(ss2);
            } // if eligible
        } // if seamcurve
    } // for i

    sc->Needs_Cutting = 0;
    //if(iret) {
    PCCut_Define_EndNsites(sc);// does the sort ia.
    PCCut_Remove_Crosses_Beyond_Ends(sc);// does the sort ia.
    //}
    if(CUTDB) {
        printf(" LEAVE CUTTING %s oldNA=%d n_angles=%d\n", sc->name(),Old_NA, sc->n_angles);
        for(int i=0;i<sc->n_angles ;i++) {
            printf(" IA(%d)  is '%s'  '%s'\n",i, sc->ia[i].s->type(),sc->ia[i].s->name());
        }
        if(sc->End1site)
            printf(" Seamcurve %s, Endsite1: %s \n",sc->name(),sc->End1site->name());
        else
            printf(" Seamcurve %s, Endsite1: NULL\n", sc->name());
        if(sc->End2site)
            printf(" Seamcurve %s, Endsite1: %s \n",sc->name(),sc->End2site->name());
        else
            printf(" Seamcurve %s, Endsite2: NULL\n", sc->name());
    }

    if(Old_NA != sc->n_angles) iret = 1;



    for(int i=0;i<sc->n_angles && i < Old_NA;i++) {
        if(sc->ia[i].s != olde[i])
            iret=1;
    }
    RXFREE(olde);

    return iret;
}



int Compute_Snap_Intersections(RXEntity_p e){ 
    int iret=0;
    /*  September 1997  New version
e must have flags 'cut, Needs Cutting'
All scs generate XSITES. The 
If they are 'fanned', this will follow in the normal course. else 'flat'
If they are 'draw' the redspace curve is discontinuous at shaped seams. else 'sketch'
If they are 'seam' we cant handle them at the moment. We could if we had discontinuous redcurves */

    sc_ptr sc = (sc_ptr  )e;
    int i,j;
    RXEntity_p * olde;
    double   upper , lower;
    int Old_NA;

    if(CUTDB) cout<<" start NEW Snapping  with " << e->name();

    if(e->Needs_Finishing){ e->OutputToClient(" cant snap needs Finishing",2); return 0;}

    olde = (RXEntity_p *)MALLOC((1 + sc->n_angles)*sizeof(RXEntity_p ));

    for(j=0;j<sc->n_angles;j++)
        olde[j] = sc->ia[j].s;
    Old_NA = sc->n_angles;

    /* Search to see if any sites not in the IAlist are very close to the black-space line.*/
    /*  If one is, add it to the IAlist.*/
    /* The IA List may still have less than 2 entries - the minimum to define a pside
*/

    /* Compute all its relsites. That will delete any off the end. */

    /* Search to see if any sites not in the IAlist are very close to the black-space line.
 If one is, add it to the IAlist. */
    sc->Remove_Distant_Sites(2.0*e->Esail ->m_Linear_Tol );
    SearchForCloseSites(sc,e->Esail ->m_Linear_Tol); // Computes all SITES
    /* This Search should remove any sites (or Relsites) in the IA list whose geometry
 is not determined by this SC and which are no longer close to it.*/
    PCCut_Define_EndNsites(sc); //HOPE this doesnt hurt
    PCC_Create_EndSites_If_Required(sc,e->Esail ->m_Linear_Tol);  // if no IA near the start, create a site and use it. Ditto the end.


    PCCut_Set_Cut_Limits(sc,&upper, &lower);  //AGAIN!! if its got endsites, use their offsets/ Else the

    PCCut_Define_EndNsites(sc);// does the sort ia.
    PCCut_Remove_Crosses_Beyond_Ends(sc);// does the sort ia.

    if(CUTDB) cout<<" LEAVE snapping "<< e->name()<<endl;

    if(Old_NA != sc->n_angles) iret = 1;
    for(i=0;i<sc->n_angles && i < Old_NA;i++) {
        if(sc->ia[i].s != olde[i])
            iret=1;
    }
    RXFREE(olde);
    sc->Needs_Cutting = 0;
    return iret;
}

int RXSeamcurve::Remove_Distant_Sites(const double tol){

    int k,Far;
    ON_3dPoint p1; ON_3dVector t1;
    Site *p;
    double dist=-1.0, s;
    int count  = 0;
    Site *se;
    /* Search to see if any sites in the IAlist are Far from to the black-space line. */

    for (k=0;k<this->n_angles ;k++) {
        p = (Site*)this->ia[k].s ;
        if(p->TYPE==SITE || p->TYPE==RELSITE ){
            dist=-1.0; Far =0;
            s = ((this->ia[k]).m_Offset->Evaluate(this,1));


            Site *q = (Site*) p;
            ON_3dPoint l_pt = p->ToONPoint();
            if (this->m_pC[1]->IsNear(l_pt,NULL,tol))
            {
                Far = 0;
                if (!this->m_pC[1]->GetONCurve())
                {assert(0); // v not initialised
                    if (0 < this->m_pC[1]->Drop_To_Curve(q->ToONPoint(),&p1,&t1, tol,&s,&dist))
                        Far = (dist >= tol);
                    else // do it again so we can step it in debugger
                    {
                        rxerror("PC_Drop_To_Curve failed in Remove_Distant_Sites",2);
                        int l_test = this->m_pC[1]->Drop_To_Curve(q->ToONPoint(),&p1,&t1, tol,&s,&dist);
                    }
                }
            }
            else
                Far=1;

            if(Far) {
                se = (Site *)p ;
                if(CUTDB) printf(" %s (%f %f %f) is Far from  %s\n",p->name(),se->x,se->y,se->z, this->name());
                this->Delete_One_IA(k);
                count++;
            }
        }
    }

    return count;

}
int PCC_Create_EndSites_If_Required(sc_ptr sc,double tol){ 
    // if no IA near the start, create a site and use it. Ditto the end.
    Site* e1, *e2;
    ON_3dPoint v1,v2;
    Site *s1=NULL,*s2=NULL;
    RXOffset*dummyo;
    char name[256],line[256],buf[256];
    int c, depth;
    e1 = sc->End1site; e2 = sc->End2site;
    if(e1) s1 = (Site *) e1 ;
    if(e2) s2 = (Site *) e2 ;
    c = sc->m_pC[1]->Get_c();

    if( sc->m_pC[1]->GetONCurve()) {
        v1 = sc->m_pC[1]->GetONCurve()->PointAtStart();
        v2 = sc->m_pC[1]->GetONCurve()->PointAtEnd();
    }
    else {
        VECTOR *vv = sc->m_pC[1]->Get_p();v1=ON_3dPoint(vv->x,vv->y,vv->z);
        vv = &(sc->m_pC[1]->Get_p()[c-1]);v2=ON_3dPoint(vv->x,vv->y,vv->z);
    }
    depth = 1 + sc->generated ;
    // no s1 means no end1site. We've already done Search ForCloseSites
    // so we create one.
    if(!s1) {
        sprintf(name,"%s_n1", sc->name());
        sprintf(line,"site:%s: %f : %f :%f", name, v1.x,v1.y,v1.z);
        depth = sc->generated + 1;
        if((e1 = (Site*) Just_Read_Card(sc->Esail,line,"site",name,&depth))) {
            e1->Resolve();
            if(!e1->Needs_Resolving) {
                e1->Needs_Finishing=!e1->Finish();
                dummyo= dynamic_cast< RXOffset*>( sc->AddExpression (new RXOffset( L"E0",L"0.0",sc,sc->Esail )));
                sc->SetRelationOf(dummyo,aunt,RXO_EXP_OF_ENT); //done
                dummyo->SetAccessFlags(RXE_PRIVATE);
                (dummyo->Evaluate(sc ,1));
                sc->Insert_One_IA(e1,dummyo );
                sc->End1site = e1;            sc->End1site->SetRelationOf(sc,child|niece,RXO_N1_OF_SC);
            }
            else   e1->Kill( );
        }
    }
    if(!s2) {
        sprintf(name,"%s_n2", sc->name());
        sprintf(line,"site:%s: %f : %f :%f", name, v2.x,v2.y,v2.z);
        depth = sc->generated + 1;
        if((e2 = (Site*) Just_Read_Card(sc->Esail, line,"site",name,&depth))) {
            e2->Resolve();
            if(!e2->Needs_Resolving) {
                e2->Needs_Finishing=!e2->Finish();

                dummyo= dynamic_cast< RXOffset*>( sc->AddExpression (new RXOffset( L"E1",L"100.0%",sc,sc->Esail )));
                sc->SetRelationOf(dummyo,aunt,RXO_EXP_OF_ENT);
                dummyo->SetAccessFlags(RXE_PRIVATE);
                dummyo->Evaluate(sc ,1);
                sc->Insert_One_IA(e2,dummyo );
                sc->End2site = e2;            sc->End2site->SetRelationOf(sc,child|niece,RXO_N2_OF_SC);
            }
            else e2->Kill( );
        }
    }
    return 1;
}

int SearchForCloseSites(sc_ptr sc,double tol) {
    int listIndex,close;
    ON_3dPoint  p1; ON_3dVector t1;
    RXOffset*dummyo=NULL;
    Site *se;
    char buf[128];
    double s=0.0, upper, lower;
    double dist=999;
    SAIL *l_sail = sc->Esail ;
    int count  = 0;
    class RXsubModel *m1, *m2;
    /* Search to see if any sites  are very close to the black-space line.
  if is is an end site of the sc, skip it.
  If one is close, and NOT in the IAList, add it to the IAlist.
We use the 'end' value of 99 to indicate such a one
If one is NOT close, and already in the IAlist with end value 99, remove it from the IAlist.
*/
    m1 = RXsubModel::SubModel(sc);
    PCCut_Set_Cut_Limits(sc,&upper, &lower);
    ent_citer it;
    for (it = l_sail->MapStart(); it != l_sail->MapEnd(); ++it) {
        RXEntity_p p  = dynamic_cast<RXEntity_p>( it->second);
        if(p->TYPE==SITE || p->TYPE==RELSITE ) {
            se = (Site *)p ;
            if(!se) continue;
            if(p->Needs_Finishing && p->TYPE==SITE )
                p->Finish();
            if(p->Needs_Finishing )
                continue;

            if(se->m_Site_Flags&PCF_SKETCH) continue;
            m2 = RXsubModel::SubModel(p);
            if(m1 !=m2)
                continue;

            if(p->TYPE==SITE) {
                if(p->Needs_Resolving) {	//this happens -dont know why- when a short head gets deleted
                    if(p->Resolve())
                        p->Finish();
                    else{
                        p->Kill();
                        continue;
                    }
                }

                if(p->NeedsComputing())
                    dynamic_cast<Site*> (p)->Compute();
            }

            Site *qqq= (Site*)p ;

            // let's try a bounding-box search. If v is nowhere near sc, say close is 0 without this search

            if(sc->m_pC[1]->Somewhere_Near(qqq->ToONPoint(), 5.0 * tol )) { // June 2003 x and y only

                dist=999.0; close=0;
                if (0 < sc->m_pC[1]->Drop_To_Curve(qqq->ToONPoint(),&p1,&t1, tol,&s,&dist))
                    close = (dist <tol);
                // return -1=err, 1=converged, 0=unconverged

                if(close && s > upper)
                    close = 0;
                else if(close && s < lower )
                    close = 0;
                listIndex = sc-> In_IA_List( p);

                if(CUTDB && close) {
                    if(CUTDB) cout<<"(snap) "<< p->name()<<" is close to "<< sc->name() ;
                    if(listIndex !=-1) cout<< " ...but its already in the list\n"<<endl;
                }
                if(close && listIndex == -1) {
                    if(CUTDB) cout<< "..Need to add it\n"<<endl;

                    sprintf(buf,"%20.10f",s); //PH increase precision Aug 2004
                    RXSTRING oname; oname=TOSTRING(sc->name()); oname+=TOSTRING(buf);
                    dummyo=new RXOffset(oname,TOSTRING(buf),sc,sc->Esail);
                    (dummyo->Evaluate(sc ,1));
                    sc->Insert_One_IA(p,dummyo );//  SearchForCloseSites P is the site
                    count++;
                }

                else if(!close && listIndex >=0)
                    count++;

                else if(close && listIndex >=0) {
                    sc->ia[listIndex].m_Offset->Change(TOSTRING(s));
                }
            } // if somewhere near
            else	{
                close=0; listIndex =-1;
            }

        }
    }
    return count;
}

RXEntity_p  Crossed(sc_ptr sc1,sc_ptr sc2, double*o1, double *o2){
    /* We test whether p1 and p2 are crossed by seeing if their IALists share any entities. */
    int j,k;
    for (k=0;k<sc1->n_angles ;k++) {
        j = sc2->In_IA_List(sc1->ia[k].s);
        if( -1 != j ) {
            *o1 = ((sc1->ia[k]).m_Offset->Evaluate(NULL,1));
            *o2 = (sc2->ia[j].m_Offset->Evaluate(NULL,1));
            if(CUTDB) printf(" dont cross again %s and %s\n", sc1->name(), sc2->name());
            return (sc1->ia[k].s);
        }
    }
    return NULL; // not crossed (topologically)
}

int Make_End_Ptrs(sc_ptr  sc) {
    /* Geodesics need the endNptrs Here we get them from
    We also need either pointer for snapping
    */
    double o1,o2;
    if(sc->End1site && sc->End2site)  return 1;
    if(! sc->ia) return 0;
    if (sc->n_angles < 2) return 0;

    o1 =(sc->ia[0].m_Offset->Evaluate(sc,1));
    o2 = (sc->ia[ sc->n_angles-1].m_Offset->Evaluate(sc,1));
    if(fabs(o2-o1) < sc->Esail ->m_Linear_Tol) return -1;
    if(o1 > o2)
        Sort_IA(sc->ia,sc->n_angles);

    sc->End1site = (Site*) sc->ia[0].s;                         sc->End1site->SetRelationOf(sc,child|niece,RXO_N1_OF_SC);
    sc->End2site = (Site*) sc->ia[ sc->n_angles-1].s;           sc->End2site->SetRelationOf(sc,child|niece,RXO_N2_OF_SC);
    return 1;
}

