
#include "StdAfx.h"
#include "RXGraphicCalls.h"
#ifndef DEBUG
#define DEBUG (0)
#endif 
#define LOCALDEBUGFLAG (0)
#ifdef WIN32
	#include <io.h>
#endif

#ifdef AMGTAPER
	#define IGS_NAME_SEPARATOR  "()/$[]{},:;"
	#include "rxON_Extensions.h"
	#include "hoopsToON.h"
	#include "stringtools.h"
	#include "RX_UI_types.h"
#else
	#include "iges.h"
	#ifdef linux
	#include "../../stringtools/stringtools.h"
	#else
	#include "..\stringtools\stringtools.h"
	#endif
	#include "entities.h"
	#include "resolve.h"
#ifdef HOOPS
	#include "hoopsToON.h"
#else
    #include "RXGraphicCalls.h"
#endif
	#include "RX_UI_types.h"
	#include "hoopcall.h"
	#include "bend.h"
	#include "nurbs.h"
	#include "interpln.h"

	#include "words.h"
	#include "script.h"
	#include "dxfin.h"
	#include "finish.h"
	#include "cut.h"
	#include "entities.h"
	#include "words.h"
	#include "global_declarations.h"
	#include "akmutil.h"
	#include "etypes.h"
	#include "RXCurve.h"

	#include "RLX3dm.h"
	#include "dovtemptest.h" // temporary for debugging the surface dove
#endif
#include "AMG3dm.h"


struct	PC_3DM_Model *g_Last3dmModel;

#define DRAW_TOO  1



bool RX_Destroy(struct PC_3DM_Model*p){

			if( p->m_name) RXFREE( p->m_name );
			if(p-> m_filename) RXFREE( p->m_filename );
			if( p->m_segname) RXFREE( p->m_segname );
			if( p->m_pOwner) RXFREE( p->m_pOwner ); 
			if( p->m_pONXModel) {
				delete p->m_pONXModel; // The model
			}
			delete p;
return true;

} // RX_Destroy(struct PC_3DM_Model*p)

struct PC_3DM_Model * AMGRead_3DM_File(const char*filename) { // for AMG
	HC_KEY rk=0;
 	struct PC_3DM_Model * l_p3dmM = new PC_3DM_Model;
	l_p3dmM->m_name = NULL;
	l_p3dmM->m_filename = NULL;
	l_p3dmM->m_segname = NULL;
	l_p3dmM->m_pOwner = NULL; 
	l_p3dmM->m_pONXModel = new rxONX_Model;	
	g_Last3dmModel=l_p3dmM;
	
   char  name[256];

		strcpy(name,"unnamed3dm");

        l_p3dmM->m_pOwner = 0;						
		l_p3dmM->m_name = STRDUP(name);
		l_p3dmM->m_filename = STRDUP(filename);
		l_p3dmM->m_segname = STRDUP(name);
		if(strstr(l_p3dmM->m_filename,"already done") == NULL) {
			  HC_Open_Segment("?Include Library");
				 if(HC_QShow_Existence(name,"self")) 
					HC_Delete_Segment(name);
			  HC_Close_Segment();		 
				
			  rk = PC_3dm_Read(l_p3dmM);

			  //if(rk==(void*)1)  {  // NONSENSE!!!!!!!!
				 // rk=0;
					//cout<< " 3dm read failed"<<endl;
					//delete 	l_p3dmM->m_pONXModel;
					//delete l_p3dmM;
					//g_Last3dmModel=0;
					//return 0;
			  //}
		} 

 return(l_p3dmM);
}//int AMGRead_3DM_File

HC_KEY PC_3dm_Read(struct PC_3DM_Model *p_p3dm, ON::unit_system p_unitsystem)
{

	char  l_ext[256],buf[256],seg1[256],seg2[256];
	HC_KEY dkey;


// change the extension to 3dm
	strcpy(buf,p_p3dm->m_filename);
	char *lp = strrchr(buf,'.'); 
	if(lp) {
		*lp=0;
		strcat(buf,".3dm");
	}
	strcpy(l_ext,"3dm");
	strcpy(p_p3dm->m_filename,buf);


	#ifdef strieq
		if (strieq(l_ext,"3dm")) 
	#else
		if (streq(l_ext,"3dm")) 
	#endif
		{
	   HC_KEY key;
	   HC_Show_Pathname_Expansion(".", seg2);
	   
	   key = dkey = PC_3dmin(p_p3dm,p_unitsystem); //TR 15 MAY 06 ON::meters);   //<<--  here is the reading process 
	   
	   	if(key){
	   		HC_Open_Segment_By_Key(key);
				HC_Set_Selectability("off");
				HC_Set_Visibility("faces=on,lines=on,edges=off,markers=off");
			HC_Close_Segment();

			HC_Show_Segment(key,seg1);
			if(LOCALDEBUGFLAG) printf(" %s was read into %s\n", p_p3dm->m_filename,seg1);

			HC_Open_Segment(p_p3dm->m_name);
				if(LOCALDEBUGFLAG) {
					HC_Show_Pathname_Expansion(".", seg2);
					printf("including %s \ninto %s\n", seg1,seg2);
				}
				HC_Include_Segment_By_Key(key);
			HC_Close_Segment();
		

		}

		return dkey;
   }
    else  {
    	cout<< " 3dmin requires file extension .3dm"<<endl;
		return(0);
   }
 
}// PC_3dm_Read



HC_KEY PC_3dmin(struct PC_3DM_Model *p_p3dm, ON::unit_system  p_units)
{
	HC_KEY key=0;
	char Namebuf[256];
	char l_3dmlibrary[1024];
	char stmp[1024];
ON::Begin();
	FILE * fp = ON::OpenFile(p_p3dm->m_filename,"rb");
	if(!fp) return 0;

	//TO Open an Archive from a the 3dm file and to read the archive in the model
	ON_BinaryFile l_archive(ON::read3dm,fp); 
	ON_TextLog l_dump_tostdout;
	ON_TextLog * l_dump = &l_dump_tostdout;	

	bool l_rc = p_p3dm->m_pONXModel->Read(l_archive,l_dump);
	if (!l_rc)
	{
		printf("Unable to read %s, file corrupted\n", p_p3dm->m_filename);
                return 0;// SB-1;
	}

// dir includes its trailing sep.  ext includes its leading dot
	char ext[256];
    rxfileparse(p_p3dm->m_filename ,0,stmp,ext, 256);
	strcat(stmp,ext);
	strcpy(Namebuf,stmp);		

	assert(strlen(p_p3dm->m_filename) >= strlen(stmp)); // A BUG Peter changed from '>' Sept 9 2005
	strcpy(p_p3dm->m_filename,stmp);

	strcpy(l_3dmlibrary,"?Include library/3dm/"); 
	strcat(l_3dmlibrary,Namebuf);
	sprintf(Namebuf,"_xxxx");// ,p_pSail);
	strcat(l_3dmlibrary,Namebuf);

		HC_Show_Pathname_Expansion(".",Namebuf); 
		int i;
		struct PC_3DM_Entity l_3dmEnt; 
		l_3dmEnt.m_pModel = NULL;
		l_3dmEnt.m_pObj   = NULL;
		l_3dmEnt.m_sail   = NULL;

		l_3dmEnt.m_pModel = p_p3dm->m_pONXModel; // To point on a model 
	
		int l_n = p_p3dm->m_pONXModel->m_object_table.Count(); //Number of object in the model
		HC_KEY l_new = 0;
		ON_3dmSettings   l_settings = p_p3dm->m_pONXModel->m_settings;
		double scalefactor=1.0; 
		if(p_units != ON::no_unit_system){
#ifndef ON_V3
			scalefactor=  l_settings.m_ModelUnitsAndTolerances.Scale( p_units ); // divide by this.  //ONV4
#else
			scalefactor=  l_settings.m_UnitsAndTolerances.Scale( p_units ); // divide by this. 
#endif
}
		scalefactor=1.0/ scalefactor;

		for (i=0;i<l_n;i++)
		{
			l_3dmEnt.m_pObj = &(p_p3dm->m_pONXModel->m_object_table[i]); //To point on the current object
			if (l_3dmEnt.m_pObj->m_attributes.IsVisible()) //to make sure only the selected object are added to the model 
			{
				int l_layerID = l_3dmEnt.m_pObj->m_attributes.m_layer_index;
				if (l_3dmEnt.m_pModel->m_layer_table[l_layerID].IsVisible())
				{
				//	Make_Name_Unique(&l_3dmEnt,l_3dmEnt.m_pObj->m_attributes.m_layer_index);

					l_3dmEnt.m_sail =0;// p_pSail;
					l_new  = AMGRead_3dmObject(&l_3dmEnt,scalefactor); // does the scaling
				}
			}

		}
		AMGRead_3dmProperties( &l_3dmEnt);
#ifndef ON_V3
		l_settings.m_ModelUnitsAndTolerances.m_unit_system=RLX_DEFAULT_UNITS ; // hopefully when we write the model it is now in metres.

#else
		l_settings.m_UnitsAndTolerances.m_unit_system=RLX_DEFAULT_UNITS ; // hopefully when we write the model it is now in metres.
#endif
		
		//Clean up 
		if (l_3dmEnt.m_pModel) l_3dmEnt.m_pModel = NULL;
		if (l_3dmEnt.m_pObj) l_3dmEnt.m_pObj = NULL;
		if (l_3dmEnt.m_sail) l_3dmEnt.m_sail = NULL;

//	HC_Close_Segment();


	if(l_dump) {l_dump=NULL; delete l_dump;} // WHY Delete a NULL??????
	
	  // close the file
    ON::CloseFile( fp );

ON::End();

	return(key);

}//  PC_3dmin


HC_KEY AMGRead_3dmPoint(struct PC_3DM_Entity * p_p3dmEnt)
{
	//old ONX_Model_Object l_obj(*(p_p3dmEnt->m_pObj));
	assert((PC_3DM_Entity*)p_p3dmEnt);
	assert(p_p3dmEnt->m_pObj);
	ONX_Model_Object * l_pObj = p_p3dmEnt->m_pObj; 
													

	HC_KEY  l_new = 0; 

	ON_String l_strname = l_pObj->m_attributes.m_name;

	if (l_strname==ON_String(""))
		l_strname = ON_String("point");
	l_strname.MakeLower();

//Before oppenening the HOOPS segment we have to take care of the name syntaxe
//PART of code copied form dxfin.c line 203->216
	int q = l_strname.Length(); //strlen(l_Name);
	assert(q < 256);
    char * lp = strpbrk(l_strname.Array(),IGS_NAME_SEPARATOR); // to just before the first $. () OK for entity name but NOT Hoops name
	char * l_atts = NULL; 
	char * l_NewAtts = NULL;
	

	int l_k = 32;
	l_atts = (char*)CALLOC(1,1024);
        if(lp) {
                strcat(l_atts," N>");// put the tail of name in atts
                strcat(l_atts,lp); 
                assert(strlen(l_atts) < 1023);
                *lp=0;
                }
	
	HC_Open_Segment(l_strname.Array());  
		const ON_Point* l_point= ON_Point::Cast(l_pObj->m_object);
		if ( !l_point ) 
		{
			if (l_atts) RXFREE(l_atts);
			if (l_NewAtts) RXFREE(l_NewAtts);	
			return 0; 
		}

		//At this stage everything is in the same segment
		HC_Set_Marker_Symbol("(.)");
		HC_Set_Visibility("markers=on");

		ON_Layer l_layer;
		ON_Material l_material;
		if (l_pObj->m_attributes.m_layer_index>0)
			l_layer = p_p3dmEnt->m_pModel->m_layer_table[l_pObj->m_attributes.m_layer_index];
		if (l_pObj->m_attributes.m_material_index>0)
			l_material = p_p3dmEnt->m_pModel->m_material_table[l_pObj->m_attributes.m_material_index];           
		ON_Color l_c;  
		//Set the color from the 3DM model
		switch (l_pObj->m_attributes.ColorSource())
		{	
		case 0:   //color_from_layer    
			l_c = l_layer.Color();
			break;
		case 1:   //color_from_object 
			l_c = l_pObj->m_attributes.m_color;
			break;
		case 2:   //color_from_material 
			l_c = l_material.Ambient();
			break;
		}
		HC_Set_Color_By_Value ("markers", "RGB", float(l_c.Red()/255.),float(l_c.Green()/255.),float(l_c.Blue()/255.));
                cout<<"TODO in AMGread3dmpoint"<<endl;
            //    HC_KInsert_Marker(l_point->point.x,l_point->point.y,l_point->point.z  );
	HC_Close_Segment();

	if (l_pObj) l_pObj= NULL;

	if (l_atts) RXFREE(l_atts);
	if (l_NewAtts) RXFREE(l_NewAtts);	
	return l_new;
}//extern int AMGRead_3DM_Point

HC_KEY AMGRead_3dmCurve(struct PC_3DM_Entity * p_p3dmEnt, const int draw_too)
{
	assert((PC_3DM_Entity*)p_p3dmEnt);
	assert(p_p3dmEnt->m_pObj);

	//old ONX_Model_Object l_obj(*(p_p3dmEnt->m_pObj));
	ONX_Model_Object * l_pObj = p_p3dmEnt->m_pObj; //= new ONX_Model_Object; 
	if (!l_pObj)
		return 0;
	
	const ON_Curve* l_curve = dynamic_cast<const ON_Curve*>(l_pObj->m_object);
	if (!l_curve)
		return 0;
	ON_3dmObjectAttributes l_att = l_pObj->m_attributes;
	ON_Color l_col;
	int l_LayerID = l_pObj->m_attributes.m_layer_index;
	ON_Layer l_layer =  p_p3dmEnt->m_pModel->m_layer_table[l_LayerID];
// figure the color
	if(l_att.ColorSource() == ON::color_from_layer) 
		l_col=l_layer.Color();
	else if(l_att.ColorSource() ==  ON::color_from_object ) 
		l_col=l_att.m_color;

	ON_String l_strname = l_pObj->m_attributes.m_name;
	if (l_strname==ON_String(""))
		l_strname = ON_String("curve")+p_p3dmEnt->m_pModel->m_layer_table[l_LayerID].LayerName();
	l_strname.MakeLower();

//SOME CKECKINGS ON THE CURVE
	//IF the curve is a polycurve we want to make sure all the subc urve are in the same direction
	const ON_PolyCurve* l_pPl = ON_PolyCurve::Cast(l_curve);
	if (l_pPl)
	{
		char l_buff[256];
		int l_count = l_pPl->Count();
		int i;
		int l_NotContinuousDomain = 0;
		
		const ON_Curve * l_prvCrv = NULL;
		ON_Curve * l_crtCrv = NULL;
		ON_3dPoint l_PtprvcrvEnd,l_PtcrtcrvStart;
		ON_Interval l_prvcrvDomain,l_crtcrvDomain;
		for (i=1;i<l_count;i++)
		{
			l_prvCrv = l_pPl->operator [](i-1);
			l_crtCrv = l_pPl->operator [](i);
			l_prvcrvDomain = l_prvCrv->Domain();
			l_crtcrvDomain = l_crtCrv->Domain();
			l_PtprvcrvEnd = l_prvCrv->PointAtEnd();
			l_PtcrtcrvStart = l_crtCrv->PointAtStart();

			if (l_PtprvcrvEnd.DistanceTo(l_PtcrtcrvStart) > 1e-9) // Peter April 2005
			{
//Peter added a dist check
				double length = l_PtprvcrvEnd.DistanceTo (l_PtcrtcrvStart);
				sprintf(l_buff,"<%s> is a polycurve which is made by sub curves separated by %f, please split some of these sub curves.",l_strname.Array(),length);
			//	error(l_buff,2);
				break;
			}

			if (l_prvcrvDomain.Max()!=l_crtcrvDomain.Min())
			{
				ON_3dPoint l_p = l_prvCrv->PointAtEnd();
				sprintf(l_buff,"(AMGRead_3dm advisory) '%s' is a polycurve made with sub curves with not continuous domain, Relax is changing the domain of some of these sub curves.",l_strname.Array());// could free
				//	error(l_buff,1);

				l_crtCrv->SetDomain(l_prvcrvDomain.Max(),l_prvcrvDomain.Max()+l_crtCrv->Domain().Length());
				ON_Interval l_testD = l_crtCrv->Domain();
				break;				
			}
		}
		if (l_prvCrv) l_prvCrv = NULL;
		if (l_crtCrv) l_crtCrv = NULL;
	}//	if (l_pPl)
	if (l_pPl) l_pPl=NULL;

#ifdef CHECK_SHORT_3DM_CURVES  
	//If the curve is too short 
	double l_len = 0;
	l_curve->GetLength(&l_len);
	if( l_LayerName && (!strnicmp( l_LayerName  , "GAUSS",5 )))
	{
		if (l_len<2*g_Gauss_Cut_Dist*1.05)
		{
			char l_buff[100];
			sprintf(l_buff,"'%s' is shorter than 2*g_Gauss_Cut_Dist*1.05 ",l_name);
			rxerror(l_buff,1);
		} 
	} 
#endif	
	if(draw_too) {
	char colstring[256];
	sprintf(colstring,"readecho/R%3dG%3dB%3d",l_col.Red(),l_col.Green(),l_col.Blue());
	HC_Open_Segment(colstring);
		HC_Set_Color_By_Value("lines", "RGB",((float)l_col.Red()/ 255.f),((float)l_col.Green()/ 255.f),((float)l_col.Blue()/ 255.f));

		 cout<<" NO l_pObj->m_object->DrawHoops(""readEcho_amg"");  "<<endl;
	 HC_Close_Segment();
	}
        assert(" what is the return value of AMGRead_3dmCurve"==0);
        return 0;

}//RXEntity_p  AMGRead_3dmCurve

HC_KEY  AMGRead_3dmSurface(struct PC_3DM_Entity * p_p3dmEnt, const int p_draw_too)
{	const ON_NurbsSurface *TheNS=0;
	ON_NurbsSurface *np2;
	assert((PC_3DM_Entity*)p_p3dmEnt);
	assert(p_p3dmEnt->m_pObj);
	
	ONX_Model_Object * l_pObj = p_p3dmEnt->m_pObj;

	const ON_Surface* l_psurface=0;
	l_psurface = ON_Surface::Cast(p_p3dmEnt->m_pObj->m_object);
	if (!l_psurface)
		return 0;
if ( p_draw_too)
	{
	ON_3dmObjectAttributes l_att = l_pObj->m_attributes;
	ON_Color l_col;
	int l_LayerID = l_pObj->m_attributes.m_layer_index;
	ON_String l_name = l_att.m_name;
	ON_String l_layername;
	ON_Layer l_layer =  p_p3dmEnt->m_pModel->m_layer_table[l_LayerID];
// figure the color
	if(l_att.ColorSource() == ON::color_from_layer) 
		l_col=l_layer.Color();
	else if(l_att.ColorSource() ==  ON::color_from_object ) 
		l_col=l_att.m_color;

	l_layername=ON_String(l_layer.LayerName());
	const ON_NurbsSurface *np=TheNS= ON_NurbsSurface::Cast(p_p3dmEnt->m_pObj->m_object);
#ifdef _DEBUG	
	const ON_PlaneSurface *npps = ON_PlaneSurface::Cast(p_p3dmEnt->m_pObj->m_object);
	const ON_RevSurface  *nprs  = ON_RevSurface::Cast(p_p3dmEnt->m_pObj->m_object);
	const ON_SumSurface *npss   = ON_SumSurface::Cast(p_p3dmEnt->m_pObj->m_object);	
	const ON_SurfaceProxy *npsp = ON_SurfaceProxy::Cast(p_p3dmEnt->m_pObj->m_object);
#endif
	char colstring[256];
	if(np)
	{
//		sprintf(colstring,"readecho/%s/%s/R%3dG%3dB%3d",l_layer.LayerName().Array(),l_name.Array(),l_col.Red(),l_col.Green(),l_col.Blue());
		sprintf(colstring,"readecho/%s/%s",l_layername.Array(),l_name.Array());		
		HC_Open_Segment(colstring);
			HC_Set_Color_By_Value("faces", "RGB",((float)l_col.Red()/ 255.f),((float)l_col.Green()/ 255.f),((float)l_col.Blue()/ 255.f));
			cout<<" sorry no np->DrawHoops(0); "<<endl;
		HC_Close_Segment();
	}
	else {
		np2 =new ON_NurbsSurface() ; // leaky but needed for dove trials.
		if( l_psurface->GetNurbForm(*np2) ) {

				sprintf(colstring,"readecho/%s/%s",l_layername.Array(),l_name.Array());
				
				//sprintf(colstring,"readecho/R%3dG%3dB%3d",l_col.Red(),l_col.Green(),l_col.Blue());
				HC_Open_Segment(colstring);
					HC_Set_Color_By_Value("faces", "RGB",((float)l_col.Red()/ 255.f),((float)l_col.Green()/ 255.f),((float)l_col.Blue()/ 255.f));
							cout<<" sorry no np2->DrawHoops(0); "<<endl; 
				HC_Close_Segment();

	TheNS= np2;  
		}
	}

	}


	int l_LayerID = l_pObj->m_attributes.m_layer_index;
//	char * l_pLayer_Name = ONStrin gToChar(p_p3dmEnt->m_pModel->m_layer_table[l_LayerID].LayerName());
	ON_String l_layerN = p_p3dmEnt->m_pModel->m_layer_table[l_LayerID].LayerName();
	const char * l_Layer_Name = (const char*) l_layerN.Array();
	ON_String l_strname = l_pObj->m_attributes.m_name;
//	l_strname = RemoveComents(l_strname);
	l_strname.MakeLower();
// temporary for development of the dovestructure class methods.

/*	if(streq(l_Layer_Name,"sa")) 
		g_sa=ON_NurbsSurface::Cast(TheNS);
	else if(streq(l_Layer_Name,"ca")) 
		g_ca=ON_NurbsSurface::Cast(TheNS);
	else 
			if(streq(l_Layer_Name,"thk")) 
		g_thk=ON_NurbsSurface::Cast(TheNS);
	else 
			if(streq(l_Layer_Name,"nu")) 
		g_nu=ON_NurbsSurface::Cast(TheNS);
	else if(streq(l_Layer_Name,"ky")) 
		g_ky=ON_NurbsSurface::Cast(TheNS);
	else if(streq(l_Layer_Name,"kg")) 
		g_kg=ON_NurbsSurface::Cast(TheNS);
	else */
#ifdef RX_USE_DOVE
	if(strieq(l_Layer_Name,"2dmould")) 
		g_2dmould=ON_NurbsSurface::Cast(TheNS);
	else if(strieq(l_Layer_Name,"3dmould")) 
		g_3dmould=ON_NurbsSurface::Cast(TheNS);
#endif
	HC_KEY l_new = 0;


	return 0;
}//RXEntity_p  AMGRead_3dmSurface

 HC_KEY  AMGDraw_3dmBrep(struct PC_3DM_Entity * p_p3dmEnt)
{	
	const ON_Brep* l_Brep=0;
	l_Brep = ON_Brep::Cast(p_p3dmEnt->m_pObj->m_object);
	if (!l_Brep)
		return 0;

	HC_KEY l_new = 0;

	int l_n,i; 
	//The Vertexis
	l_n = l_Brep->m_V.Count();
	for (i=0;i<l_n;i++)
	{
		//DO NOT WANT TO ADD THE VERTEXIS INTO THE RELAX MODEL FOR 
		//l_Result = AMGRead_3dmPoint(&(l_Brep->m_V[i]));
		//if (l_Result!=InputOK)
		///	return l_Result;
	}

	//The edges
	l_n = l_Brep->m_E.Count();
	for (i=0;i<l_n;i++)
	{
		//DO NOT WANT TO ADD THE EDGES INTO THE RELAX MODEL FOR 
		//l_Result = AMGRead_3dmCurve(&(l_Brep->m_E[i]));
		//if (l_Result!=InputOK)
		//	return l_Result;
	}

	//The faces
	l_n = l_Brep->m_F.Count();
	for (i=0;i<l_n;i++)
		 AMGDraw_BrepFace(l_Brep->m_F[i],p_p3dmEnt);

	return 0; 
}//int AMGRead_3dmBrep

 EXTERN_C HC_KEY AMGDraw_BrepFace(const ON_BrepFace &f,const struct PC_3DM_Entity * p_p3dmEnt ) {
	HC_KEY rc=0;
	char colstring[256];
	const ONX_Model_Object * l_pObj = p_p3dmEnt->m_pObj;
	const ON_SurfaceProxy* l_psurface = ON_SurfaceProxy::Cast(&f);
	if (!l_psurface)
		return 0;

	ON_3dmObjectAttributes l_att = l_pObj->m_attributes;
	ON_Color l_col;
	ON_Layer l_layer =  p_p3dmEnt->m_pModel->m_layer_table[ l_pObj->m_attributes.m_layer_index];

// figure the color
	if(l_att.ColorSource() == ON::color_from_layer)  
		l_col=l_layer.Color();
	else if(l_att.ColorSource() ==  ON::color_from_object ) 
		l_col=l_att.m_color;
	else cout << " Color source not defined for this Brep face"<<endl;
#ifdef HOOPS
	const ON_NurbsSurface *np=  ON_NurbsSurface::Cast(l_psurface);
	if(np)
	{
		sprintf(colstring,"readecho/%S/%S",l_layer.LayerName().Array(),l_att.m_name.Array());		
		HC_Open_Segment(colstring);
			HC_Set_Color_By_Value("faces", "RGB",((float)l_col.Red()/ 255.f),((float)l_col.Green()/ 255.f),((float)l_col.Blue()/ 255.f));
					cout<<" sorry no np->DrawHoops(0); "<<endl; 
		HC_Close_Segment();
	}
	else {
		RXON_NurbsSurface np2;
		if( l_psurface->GetNurbForm( np2) ) {
				sprintf(colstring,"readecho/%S/%S",l_layer.LayerName().Array(),l_att.m_name.Array());
				HC_Open_Segment(colstring);
					HC_Set_Color_By_Value("faces", "RGB",((float)l_col.Red()/ 255.f),((float)l_col.Green()/ 255.f),((float)l_col.Blue()/ 255.f));
					rc=np2.DrawHoops(); 
				HC_Close_Segment();
		}
	}
#endif
	return rc;
 }

HC_KEY AMGRead_3dmBrep(struct PC_3DM_Entity * p_p3dmEnt)
{	
	const ON_Brep* l_Brep=0;
	l_Brep = ON_Brep::Cast(p_p3dmEnt->m_pObj->m_object);
	if (!l_Brep)
		return 0;

	HC_KEY l_new = 0;

	int l_n,i; 
	//The Vertices
	l_n = l_Brep->m_V.Count();
	for (i=0;i<l_n;i++)
	{
		//DO NOT WANT TO ADD THE VERTEXIS INTO THE RELAX MODEL FOR 
		//l_Result = AMGRead_3dmPoint(&(l_Brep->m_V[i]));
		//if (l_Result!=InputOK)
		///	return l_Result;
	}

	//The edges
	l_n = l_Brep->m_E.Count();
	for (i=0;i<l_n;i++)
	{
		//DO NOT WANT TO ADD THE EDGES INTO THE RELAX MODEL FOR 
		//l_Result = AMGRead_3dmCurve(&(l_Brep->m_E[i]));
		//if (l_Result!=InputOK)
		//	return l_Result;
	}

	//The faces
	l_n = l_Brep->m_F.Count();
	for (i=0;i<l_n;i++)
	{
		//To reset the ON_Object associated to the entity on a surface and then read the surface
		p_p3dmEnt->m_pObj->m_object = (ON_Object*)(&(ON_Brep::Cast(p_p3dmEnt->m_pObj->m_object))->m_F[i]);
		l_new = AMGRead_3dmSurface(p_p3dmEnt,DRAW_TOO);
		if (!l_new)
			return l_new;
	}

	return l_new; // always zero (!)
}//int AMGRead_3dmBrep

HC_KEY AMGRead_3dmMesh(struct PC_3DM_Entity * p_p3dmEnt) // just draw it
{
	HC_KEY l_new = 0;
	ONX_Model_Object * l_pObj = p_p3dmEnt->m_pObj; 

	ON_String l_strname = l_pObj->m_attributes.m_name;

	if (l_strname.IsEmpty())
		l_strname = ON_String("point");
	l_strname.MakeLower();

//Before oppenening the HOOPS segment we have to take care of the name syntaxe
//PART of code copied form dxfin.c line 203->216
	int q = l_strname.Length(); //strlen(l_Name);
	assert(q < 256);
    char * lp = strpbrk(l_strname.Array(),IGS_NAME_SEPARATOR); // to just before the first $. () OK for entity name but NOT Hoops name
	char * l_atts = NULL; 
	char * l_NewAtts = NULL;
	

	int l_k = 32;
	l_atts = (char*)CALLOC(1,1024);
        if(lp) {
                strcat(l_atts," N>");// put the tail of name in atts
                strcat(l_atts,lp); 
                assert(strlen(l_atts) < 1023);
                *lp=0;
                }
	

		const ON_Mesh* l_msh= rxON_Mesh::Cast(l_pObj->m_object);
		if ( !l_msh ) 
		{
			if (l_atts) RXFREE(l_atts);
			if (l_NewAtts) RXFREE(l_NewAtts);	
			return 0; 
		}
	HC_Open_Segment(l_strname.Array());  
		//At this stage everything is in the same segment
		ON_Layer l_layer;
		ON_Material l_material;
		if (l_pObj->m_attributes.m_layer_index>0)
			l_layer = p_p3dmEnt->m_pModel->m_layer_table[l_pObj->m_attributes.m_layer_index];
		if (l_pObj->m_attributes.m_material_index>0)
			l_material = p_p3dmEnt->m_pModel->m_material_table[l_pObj->m_attributes.m_material_index];           
		ON_Color l_c;  
		//Set the color from the 3DM model
		switch (l_pObj->m_attributes.ColorSource())
		{	
		case 0:   //color_from_layer    
			l_c = l_layer.Color();
			break;
		case 1:   //color_from_object 
			l_c = l_pObj->m_attributes.m_color;
			break;
		case 2:   //color_from_material 
			l_c = l_material.Ambient();
			break;
		}
		HC_Set_Color_By_Value ("markers", "RGB", float(l_c.Red()/255.),float(l_c.Green()/255.),float(l_c.Blue()/255.));

		cout<<" no l_msh->DrawHoops ();"<<endl;
	HC_Close_Segment();

	if (l_pObj) l_pObj= NULL;

	if (l_atts) RXFREE(l_atts);
	if (l_NewAtts) RXFREE(l_NewAtts);	
	return l_new;

}//RXEntity_p  AMGRead_3dmMesh

HC_KEY AMGRead_3dmPointCloud(struct PC_3DM_Entity * p_p3dmEnt)
{
	HC_KEY l_new = 0;
	return l_new;
}//RXEntity_p  AMGRead_3dmPointCloud

int AMGRead_3dmProperties( struct PC_3DM_Entity * p_p3dmEnt)
{
	int l_Result = 0;
	ON_3dmProperties l_prop; 
	l_prop = p_p3dmEnt->m_pModel->m_properties;
  
	//Application
	ON_3dmApplication l_app(l_prop.m_Application);
	ON_String l_appURL(l_app.m_application_URL);
	ON_String l_appDetails(l_app.m_application_details);
	ON_String l_appName(l_app.m_application_name);

	//Notes
	ON_3dmNotes l_notes(l_prop.m_Notes);
	AMGRead_3dmNotes(p_p3dmEnt,l_notes);

	//Preview image
 	ON_WindowsBitmap l_WndBmp(l_prop.m_PreviewImage);
	//that is just an image of the model 

	//Revision history
	ON_3dmRevisionHistory l_History(l_prop.m_RevisionHistory);
//	l_History.m_create_time   
//	l_History.m_last_edit_time   
	int l_revisionCount = l_History.m_revision_count;   
	ON_String l_CreatedBy(l_History.m_sCreatedBy);
	ON_String l_LastEditedBy(l_History.m_sLastEditedBy);

	return l_Result;
}//int AMGRead_3dmProperties

int AMGRead_3dmNotes(struct PC_3DM_Entity * p_p3dmEnt, const ON_3dmNotes & p_notes)
{
	ON_String l_Notes(p_notes.m_notes);

	int l_Result = 0;
	int iret=0;
	int l_pos = l_Notes.Find("\r");   // \r <--> ACII 13
	while (l_pos!=-1)
	{
		l_Notes.SetAt(l_pos,char(10));  //10 is is th ASCII value for "new line"
		l_pos = l_Notes.Find("\r");
	}
	
 return iret;
}//int AMGRead_3dmNotes

HC_KEY AMGDraw_3dmObject(struct PC_3DM_Entity * p_p3dmEnt, double scalefactor)
{
	HC_KEY l_new = 0;
	ON_Brep * l_brp = (ON_Brep *) ON_Brep::Cast(p_p3dmEnt->m_pObj->m_object);

	if (l_brp)  {
		l_brp->Scale(scalefactor);
		l_new = AMGDraw_3dmBrep(p_p3dmEnt);
	}
#ifdef NEVER		
	ON_Point *l_p =	(ON_Point*) ON_Point::Cast(p_p3dmEnt->m_pObj->m_object);
	if (l_p) {
		l_p->Scale(scalefactor);
		return AMGRead_3dmPoint(p_p3dmEnt);
	}
	
	rxON_Mesh *l_m = (rxON_Mesh * ) rxON_Mesh::Cast(p_p3dmEnt->m_pObj->m_object);
	if (l_m)  {
		l_m->Scale(scalefactor);
		return  AMGRead_3dmMesh(p_p3dmEnt);
	}
	{
		ON_Curve *l_obj = (	ON_Curve * )dynamic_cast<const ON_Curve*>(p_p3dmEnt->m_pObj->m_object);
		if (l_obj) {
#ifdef _DEBUG
			{
		const ON_PolylineCurve *l_pl = ON_PolylineCurve::Cast(p_p3dmEnt->m_pObj->m_object);
		//if(l_pl)
		//	cout<< " its a polylinecurve"<<endl;
			}
#endif
			l_obj->Scale(scalefactor);
			return AMGRead_3dmCurve(p_p3dmEnt,DRAW_TOO); 
		}
	}
	{
	ON_Surface * l_obj = ( ON_Surface *) ON_Surface::Cast(p_p3dmEnt->m_pObj->m_object);
	if (l_obj) {
		l_obj->Scale(scalefactor);
		AMGRead_3dmSurface(p_p3dmEnt,DRAW_TOO);
	}
	}
	{
	ON_PointCloud *l_obj = (	ON_PointCloud * ) ON_PointCloud::Cast(p_p3dmEnt->m_pObj->m_object);
	if (l_obj)  {
		l_obj->Scale(scalefactor);
		l_new = AMGRead_3dmPointCloud(p_p3dmEnt);
	}
	}

#endif
	return l_new;
}//hc_key AMGDraw_3dmObject



HC_KEY AMGRead_3dmObject(struct PC_3DM_Entity * p_p3dmEnt, double scalefactor)
{
	HC_KEY l_new = 0;
	ON_Brep * l_brp = (ON_Brep *) ON_Brep::Cast(p_p3dmEnt->m_pObj->m_object);

	if (l_brp)  {
		l_brp->Scale(scalefactor);
		l_new = AMGRead_3dmBrep(p_p3dmEnt);
	}
		
	ON_Point *l_p =	(ON_Point*) ON_Point::Cast(p_p3dmEnt->m_pObj->m_object);
	if (l_p) {
		l_p->Scale(scalefactor);
		return AMGRead_3dmPoint(p_p3dmEnt);
	}
	
	rxON_Mesh *l_m = (rxON_Mesh * ) rxON_Mesh::Cast(p_p3dmEnt->m_pObj->m_object);
	if (l_m)  {
		l_m->Scale(scalefactor);
		return  AMGRead_3dmMesh(p_p3dmEnt); 
	}
	{
		ON_Curve *l_obj = (	ON_Curve * )dynamic_cast<const ON_Curve*>(p_p3dmEnt->m_pObj->m_object);
		if (l_obj) {
#ifdef _DEBUG
			{
		const ON_PolylineCurve *l_pl = ON_PolylineCurve::Cast(p_p3dmEnt->m_pObj->m_object);
		//if(l_pl)
		//cout<< " its a polylinecurve"<<endl;
			}
#endif
			l_obj->Scale(scalefactor);
			return AMGRead_3dmCurve(p_p3dmEnt,DRAW_TOO); 
		}
	}
	{
	ON_Surface * l_obj = ( ON_Surface *) ON_Surface::Cast(p_p3dmEnt->m_pObj->m_object);
	if (l_obj) {
		l_obj->Scale(scalefactor);
		AMGRead_3dmSurface(p_p3dmEnt,DRAW_TOO);
	}
		//l_new = NULL ; //WE HAVE ALLREADY READ THE SURFACE FROM THE BREP; AMGRead_3dmSurface(p_p3dmEnt);
	}
	{
	ON_PointCloud *l_obj = (	ON_PointCloud * ) ON_PointCloud::Cast(p_p3dmEnt->m_pObj->m_object);
	if (l_obj)  {
		l_obj->Scale(scalefactor);
		l_new = AMGRead_3dmPointCloud(p_p3dmEnt);
	}
	}
	//CLEAN UP

	return l_new;
}//int AMGRead_3dmObject



int AMG_3dm_Write(HC_KEY p_k, const char * pfilename,PC_3DM_Model* p_3dmModel )

{

	//Write everything in the segment tree p_k to a 3DM FILE
	if(!p_k) return 0;
	char l_fname[1024];
 
	filename_copy_local(l_fname,1024,pfilename );



	HC_Open_Segment_By_Key(p_k);
	ON::Begin();
	FILE* fp = ON::OpenFile( l_fname, "wb" );
		  // The OpenNURBS toolkit will write version 2 and 3 and read
		  // version 1, 2 and 3 of the 3DM file format.
		  // version to write
	if(!fp) {   HC_Close_Segment(); return 0;}
		int Version = 3;

		PC_3DM_Model * l_3dmModel = p_3dmModel;
		rxONX_Model* 	   l_ONXModel = NULL;
	 	if (p_3dmModel)
	 		l_ONXModel = p_3dmModel->m_pONXModel;
		rxONX_Model l_model;  //Model where we want to write to
		l_model.m_3dm_file_version =Version; l_model.m_3dm_opennurbs_version=4;
		l_model.Polish();
		ON_TextLog error_log(stderr);	

		if(l_ONXModel) {
		//We only need the model to set the layers the settings and all these things in the new 3DM file
#ifndef ON_V3
			l_model.m_3dm_file_version= l_ONXModel->m_3dm_file_version;
			l_model.m_3dm_opennurbs_version= l_ONXModel->m_3dm_opennurbs_version;
#else
			l_model.m_version     = l_ONXModel->m_version;
#endif
		l_model.m_settings    = l_ONXModel->m_settings;
		l_model.m_layer_table = l_ONXModel->m_layer_table;

		}
    l_model.m_layer_table.Empty();
	l_model.m_settings.m_ModelUnitsAndTolerances.m_unit_system = RLX_DEFAULT_UNITS;

// do we have to check for the existence of layers? 
  // layer table
 
    // OPTIONAL - define some layers
    ON_Layer layer[3];

    layer[0].SetLayerName("MyDefault");
#ifdef ON_V3
    layer[0].SetMode(ON::normal_layer);
#endif
  //  layer[0].SetLayerIndex(0);
    layer[0].SetColor( ON_Color(0,0,0) );

    layer[1].SetLayerName("Mygauss_nodes");
#ifdef ON_V3
    layer[0].SetMode(ON::normal_layer);
#endif
  //  layer[1].SetLayerIndex(1);
    layer[1].SetColor( ON_Color(255,0,0) );

    layer[2].SetLayerName("SC_Plines");
#ifdef ON_V3
    layer[0].SetMode(ON::normal_layer);
#endif
  //  layer[2].SetLayerIndex(2);
    layer[2].SetColor( ON_Color(0,0,255) );
		ON_String l_Notes; 
		l_Notes=ON_String(" written by Relax/Gauss suite (AMG_3dm_Write)");

		int rc = AMG_Segment_To3dm(l_model,".");	 

		//Write the note 
		l_model.m_properties.m_Notes.m_notes = l_Notes;

		ON_BinaryFile archive( ON::write3dm, fp ); // fp = pointer from fopoen(...,"wb")
		// start section comment
		const char* sStartSectionComment = __FILE__ " 3DMRelaxOut " __DATE__;

		// Set uuid's, indices, etc.
    	l_model.Polish();
		// writes model to archive
		bool ok = l_model.Write( archive, Version, sStartSectionComment, &error_log  ); //  fails with errorlog null
	ON::CloseFile( fp );


ON::End();
		HC_Close_Segment();	
	return 1;
}//    AMG_3dm_Write

int AMG_Segment_To3dm(rxONX_Model &l_model,const char*seg){
#ifndef HOOPS
 cout<< "AMG_Segment_To3dm needs hoops "<<endl; return 0;
#else
	 int rc=1;
	// finally, search the hoops tree for all geometry whose owner has User_Value=0. 
		int layerindex;
		char type[256],owner[256],name[256],layername[256],colorname[256]; 
		HC_KEY key;
		RXEntity_p l_ui=0;
		HC_Begin_Contents_Search("...","include");
		while (HC_Find_Contents(type, &key)) {
			 HC_KEY ikey=	HC_KShow_Include_Segment ( key);
			 HC_Open_Segment_By_Key(ikey);
					rc+=AMG_Segment_To3dm(l_model,".");
			 HC_Close_Segment();
		}
		HC_End_Contents_Search();

		HC_Begin_Contents_Search("...","polylines,polygons,lines,nurbs curves,nurbs surfaces,shells");
		while (HC_Find_Contents(type, &key)) {
			HC_Open_Segment_By_Key(HC_KShow_Owner_By_Key(key));
				HC_Show_One_Net_User_Index(RXCLASS_ENTITY,&l_ui);

				HC_Show_Owner_By_Key(key,owner);
				HC_Parse_String(owner,"/",-1,name);	
				HC_Parse_String(owner,"/",-1,layername); // was -2
				layerindex =  GetONXObjLayerByName(&(l_model),ON_String(layername) );
				
				if(layerindex<0) {
					ON_Layer newlayer;float out_value[3];
					newlayer.SetLayerName(layername);
#ifdef ON_V3
					 newlayer.SetMode(ON::normal_layer);
#endif
					newlayer.SetLayerIndex(l_model.m_layer_table.Count());
					HC_QShow_One_Net_Color(owner,"polyline",colorname);
					HC_Compute_Color (colorname, "RGB", out_value);

					newlayer.SetColor( ON_Color(out_value[0]*255,out_value[1]*255,out_value[2]*255) );
					l_model.m_layer_table.Append(newlayer);

					layerindex =  GetONXObjLayerByName(&(l_model),ON_String(layername) );
					//printf(" index of new layer '%s' is %d\n",layername,layerindex);
				} // if layerindex
				assert(layerindex>=0);


				if(streq(type,"polygon")) 
				{
					ON_3dmObjectAttributes l_atts;
					ON_Curve *theCurve = hoopsToON::Polygon(key);
					if(!theCurve) continue;
					ONX_Model_Object& mo = l_model.m_object_table.AppendNew(); 
					mo.m_object = theCurve;
					mo.m_bDeleteObject = true;  //??????????????
					ON_CreateUuid(mo.m_attributes.m_uuid);
					HC_Show_Owner_By_Key(key,owner);
					HC_Parse_String(owner,"/",-1,name);	HC_Parse_String(owner,"/",-2,layername);
					
					layerindex =  GetONXObjLayerByName(&(l_model),ON_String("polygon_")+ON_String(layername) );
					
					if(layerindex<0) {
						ON_Layer newlayer;float out_value[3];
						newlayer.SetLayerName(layername);
#ifdef ON_V3
						newlayer.SetMode(ON::normal_layer);
#endif
						newlayer.SetLayerIndex(l_model.m_layer_table.Count());
						HC_QShow_One_Net_Color(owner,"polyline",colorname);
						HC_Compute_Color (colorname, "RGB", out_value);
		
						newlayer.SetColor( ON_Color(out_value[0]*255,out_value[1]*255,out_value[2]*255) );
						l_model.m_layer_table.Append(newlayer);

						layerindex =  GetONXObjLayerByName(&(l_model),ON_String(layername) );
						//printf(" index of new layer '%s' is %d\n",layername,layerindex);
					}
					assert(layerindex>=0);
					l_atts.m_layer_index = layerindex;
					l_atts.m_name = ON_String("PgonOut_")+ON_String(name);
						mo.m_attributes=l_atts;
				}
				else

					if(streq(type,"polyline")) 
					{
						ON_3dmObjectAttributes l_atts;
						ON_Curve *theCurve = hoopsToON::Polyline(key);
						if(!theCurve) continue;
						ONX_Model_Object& mo = l_model.m_object_table.AppendNew(); 
						mo.m_object = theCurve;
						mo.m_bDeleteObject = true;  //??????????????
						assert(layerindex>=0);
						l_atts.m_layer_index = layerindex;
						l_atts.m_name = ON_String("PlineOut_")+ON_String(name);
						mo.m_attributes=l_atts;
					}
				else

				if(streq(type,"shell")) 
				{
					ON_3dmObjectAttributes l_atts;
					ON_Mesh *theCurve = hoopsToON::Mesh(key);
					if(!theCurve) continue;
					ONX_Model_Object& mo = l_model.m_object_table.AppendNew(); 
					mo.m_object = theCurve;
					mo.m_bDeleteObject = true;  //??????????????
					assert(layerindex>=0);
					l_atts.m_layer_index = layerindex;
					l_atts.m_name = ON_String("MeshOut_")+ON_String(name);

					mo.m_attributes=l_atts;
				}

				else if(streq(type,"line")) 
				{
					ON_3dmObjectAttributes l_atts;
					ON_Curve *theCurve = hoopsToON::Line(key);
					if(!theCurve) continue;
					ONX_Model_Object& mo = l_model.m_object_table.AppendNew(); 
					mo.m_object = theCurve;
					mo.m_bDeleteObject = true;  //??????????????

					HC_Show_Owner_By_Key(key,owner);
					HC_Parse_String(owner,"/",-1,name);	HC_Parse_String(owner,"/",-2,layername);
			
					layerindex =  GetONXObjLayerByName(&(l_model),ON_String(layername) );
					
					if(layerindex<0) {
						ON_Layer newlayer;float out_value[3];
						newlayer.SetLayerName(layername); printf("while writing a LINE, new layer named %s\n", layername );
#ifdef ON_V3
						newlayer.SetMode(ON::normal_layer);
#endif
						newlayer.SetLayerIndex(l_model.m_layer_table.Count());
						HC_QShow_One_Net_Color(owner,"polyline",colorname);
						HC_Compute_Color (colorname, "RGB", out_value);
		
						newlayer.SetColor( ON_Color(out_value[0]*255,out_value[1]*255,out_value[2]*255) );
						l_model.m_layer_table.Append(newlayer);

						layerindex =  GetONXObjLayerByName(&(l_model),ON_String(layername) );
						//printf(" index of new layer '%s' is %d\n",layername,layerindex);
					}
					assert(layerindex>=0);
					l_atts.m_layer_index = layerindex;
					l_atts.m_name = ON_String("LineOut_")+ON_String(name);
					ON_CreateUuid(l_atts.m_uuid);

					mo.m_attributes=l_atts;
				}
				else if(streq(type,"nurbs surface")) 
				{
					char shortowner[256];
					ON_NurbsSurface *theSurf = hoopsToON::NurbsSurfaceToON(key);
					if(!theSurf) continue;
					ONX_Model_Object& mo = l_model.m_object_table.AppendNew();
					mo.m_object = theSurf;
					mo.m_bDeleteObject = true;  //??????????????
					mo.m_attributes.m_layer_index = layerindex; 
				
					HC_Show_Owner_By_Key(key,owner);  
					HC_Parse_String(owner,"/",-1,shortowner);
					mo.m_attributes.m_name = ON_String("AMGOut_")+ON_String(type)+ON_String("_")+ON_String(shortowner);
				}
				else if(streq(type,"nurbs curve")) 
				{
					ON_NurbsCurve *theCurve = hoopsToON::NurbsCurveToON(key);
					if(!theCurve) continue;
					ONX_Model_Object& mo = l_model.m_object_table.AppendNew();
					mo.m_object = theCurve;
					mo.m_bDeleteObject = true;  //??????????????
					mo.m_attributes.m_layer_index = layerindex; 

					HC_Show_Owner_By_Key(key,type); 
					mo.m_attributes.m_name = ON_String("NCOut_")+ON_String(type);
				}
				else {
					printf("! no code to 3dmWrite a %s ",type); 
					HC_Show_Owner_By_Key(key,type); 
					printf(" in segment <%s>\n",type);
				//	l_Notes+=ON_String(type);
				}

			HC_Close_Segment();
		}
	HC_End_Contents_Search();
	return rc; // number of recursive calls
#endif
}

#ifdef  AMGTAPER
int GetONXObjLayerByName(rxONX_Model * p_Model,ON_String p_name)
{
	ON_String l_layer(p_name);
	l_layer.MakeUpper();
	ON_String l_temp;
	int i,l_n = p_Model->m_layer_table.Count();
	for (i=0;i<l_n;i++)
	{
		l_temp = p_Model->m_layer_table[i].LayerName();
		l_temp.MakeUpper();
		if (l_layer==l_temp)
			return i;
	}
	return -1;
}
#endif

