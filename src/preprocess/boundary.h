
#ifndef BOUNDARY_16NOV04
#define BOUNDARY_16NOV04

int Resolve_Boundary_Card (RXEntity_p e);
int Included_In(Site * ps,std::map<int,Site *> &list);
int Included_In(RXEntity_p ps,std::map<int,RXEntity_p> &list);
RXEntity_p  Find_Connecting_Edge( Site**s1, RXEntity_p NotThis,int*rev);
int Trace_Boundary(RXEntity_p e);

#endif //#ifndef BOUNDARY_16NOV04
