/* vppint.c  started January 1996 based on ideas in the original R2 prospectus*/

#include "StdAfx.h"
#include "stringutils.h"
#include "summary.h"
#include "telnet.h"
#include "global_declarations.h"

#include "vppint.h"



int Passive_VPP_Call() {
int OK=1,nr;
 char buf[256];
/* 
1) Write a short summary-out file just containing the header line and the current line
2) CALL System ("tellMeMore <sumfilename>") 
	-	this program re-writes the sumfile
3) Read and apply the first line of the sum-file
*/

/******** 
1) Write a short summary-out file just containing the header line and the current line ******/
	if(is_empty(g_VPP_Transfer_File)) {
		rxerror("Set g_VPP_Transfer_File in the Defaults file!",2);
		return 0;
	}
	sprintf(buf,"rm %s\n",  g_VPP_Transfer_File);
	system(buf); 

	Open_Summary_File(&g_VPP_Sum,g_VPP_Transfer_File,"w"); /* posts a row to VPPsum */
	Post_All_Summary_Values(&g_summout,&g_VPP_Sum);
	Write_Summary_Line (&g_VPP_Sum);
	Close_Summary_File(&g_VPP_Sum);	


/*******2) CALL System ("tellMeMore <sumfilename>") 
	-	this program Must re-write the sumfile  *******/

	sprintf(buf,"%s %s %s\n", g_VPP_Command, g_VPP_Address, g_VPP_Transfer_File);
	printf("%s\n",buf);
	if(!system(buf)) {


/*****   3) Read and apply the first line of the sum-file ( IF OK)*******/


  	Open_Summary_File(&g_VPP_Sum,g_VPP_Transfer_File,"r"); 
	nr = read_and_post_one_row(&g_VPP_Sum,0); 

	Post_All_Summary_Values(&g_VPP_Sum,&g_summout);
     	Apply_Summary_Values(&g_VPP_Sum);

	Close_Summary_File(&g_VPP_Sum);
}

return OK;
}


int Active_VPP_Call() {
int OK=1,nr;
 char buf[256];
/* 
1) Write a short summary-out file just containing the header line and the current line
2) CALL System ("tellMeMore <sumfilename>") 
	-	this program re-writes the sumfile
3) Read and apply the first line of the sum-file
*/

/******** 
1) Write a short summary-out file just containing the header line and the current line ******/
	if(is_empty(g_VPP_Transfer_File)) {
		rxerror("Set g_VPP_Transfer_File in the Defaults file!",2);
		return 0;
	}
	sprintf(buf,"rm %s\n",  g_VPP_Transfer_File);
	puts(buf);
	system(buf); 

	Open_Summary_File(&g_VPP_Sum,g_VPP_Transfer_File,"w"); /* posts a row to VPPsum */
	Post_All_Summary_Values(&g_summout,&g_VPP_Sum);
	Write_Summary_Line (&g_VPP_Sum);
	Close_Summary_File(&g_VPP_Sum);	


	sprintf(buf,"file%s\n", g_VPP_Transfer_File);
//	Do_Send_Telnet_Message(buf);
	printf("Telnet <%s>\n", buf);

return OK;
}
