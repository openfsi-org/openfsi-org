/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *  July 97 fp removed. There was a fclose(NULL)
 * 22/12/95 printf
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


#include "StdAfx.h"
#include "RelaxWorld.h"
//#include "hc.h"
#include "attach.h"
#include "vectors.h"

#include "pansail.h"
#include "offset.h"
#include "akmpretty.h"
#include "files.h"
#include "unixver.h"
#include "global_declarations.h"

#include "panout.h"

/* call is 
   Insert_Pansail_Model(N[model],M[model] xv[0][0][model],yv[0][0][model],zv[0][0][model] ) */


HC_KEY Insert_Pansail_Model(int Nin,int Min, float xv[] ,float yv[],float zv[]) 
{
  /* inserts a mesh into the currently open segment and returns its hoopskey */
  
  HC_KEY key;
  int i,j,k;
  char errStr[128];
  
  VECTOR*x;

  x = (VECTOR *) MALLOC(Nin*Min*sizeof(VECTOR));
   
  k=0;
  for(i=0;i<Nin;i++) {
    for(j=0;j<Min;j++) {
      
      x[k].x= xv[k];
      x[k].y= yv[k];
      x[k].z= zv[k];
      k++;
    }
  }

  if(Nin < 1 || Min < 1) {
    
    sprintf(errStr,"Insert Pansail shell has rows = %d, cols = %d",Nin,Min);
    g_World->OutputToClient(errStr,1);
    key = 0;
    
  }
  else {
    key=HC_KInsert_Mesh(Nin,Min,x); 
 //   HC_Flush_Contents("..","modelling matrix");
    HC_Flush_Contents(".","modelling matrix"); HC_Rotate_Object (0.0,0.0,-g_AWA*57.29577951 );
 
  HC_Open_Geometry(key);
	for(i=0;i<Nin-1; i++) {
	for(j=0;j<Min-1;j++) {
		int v1,v2;
		v1= (i+1) * Min +j;
		v2= i * Min +j+1;
 		 HC_Open_Edge(v1,v2);
			HC_Set_Visibility("edges=off");
 		 HC_Close_Edge();
		}
	}
  HC_Close_Geometry();

  }
  RXFREE(x);
  return(key);
}



HC_KEY int Insert_Pansail_Wake(int pMod,int has_foot_wake) 
{
  /* inserts a mesh into the currently open segment and returns its hoopskey  */
  /* NOTE: this places foot wakes in to the footwakes into the first shell that */
  /* that it can. NOT necessarily the one where it should be. To do this it is */
  /* necessary to know which OTHER sails have footwakes since they are packed into */
  /* an array by PANSAIL. */
  
  HC_KEY int key=0,pan_mod; //gcc
  int i,j,k;
  VECTOR*x;
  char errStr[128];
  char colStr[120];
  char colStr2[256];
  if(!g_PansailDone)
    return(0);
  if(!pMod)
    return(0);
  pan_mod = pMod-1;
  if(pan_mod >=MAXCOM) {
	printf(" trying to do wake %ld of %d\n", pan_mod, MAXCOM);
	g_World->OutputToClient("Skip this wake",2);

  }

Post_Singo_Summary(pMod); /* in attach.c.  posts a statistical summary of singo */

/* hopefully this will force hoops to update */
  HC_Set_Color("lines=green, faces=red");
  sprintf(colStr,"edges = wake_lines%d,faces = wake_faces%d",pMod,pMod);
 
  HC_Set_Color(colStr);
  HC_Show_Color(colStr2);

  if(g_nwko[pan_mod] < 0 || g_nwko[pan_mod] > 10000) {
    g_World->OutputToClient("INCORRECT WAKE GEOMETRY !! ",1);
    g_World->OutputToClient("Check .edg file         !! ",2);
    return(0);
  }
  if(g_mwko[pan_mod] < 0 || g_mwko[pan_mod] > 10000) {
    g_World->OutputToClient("INCORRECT WAKE GEOMETRY !! ",1);
    g_World->OutputToClient("Check .edg file         !! ",2);
    return(0);
  }
  
  x = (VECTOR *) MALLOC(g_mwko[pan_mod]*g_nwko[pan_mod]*sizeof(VECTOR));
  
  k=0;
  for(i=0;i<g_nwko[pan_mod];i++) {
    for(j=0;j<g_mwko[pan_mod];j++) {
      
      x[k].x= g_xwakeo[pan_mod][i][j];
      x[k].y= g_ywakeo[pan_mod][i][j];
      x[k].z= g_zwakeo[pan_mod][i][j];
       k++;
    }
  }
  if(g_nwko[pan_mod] < 1 || g_mwko[pan_mod] < 1) {
    
    sprintf(errStr,"Insert (leech) wake (model %d) has rows = %d, cols = %d",pMod,g_nwko[pan_mod],g_mwko[pan_mod]);
    g_World->OutputToClient(errStr,1);
    
  }
  else {
    key=HC_KInsert_Mesh(g_nwko[pan_mod],g_mwko[pan_mod],x); 
  }
  RXFREE(x);
  if(g_ntwako) {
    pan_mod += g_nwakeso;
    if(g_nwko[pan_mod] > 0 && g_mwko[pan_mod] > 0) { /* must be some foot wakes */
      
      x = (VECTOR *) MALLOC(g_mwko[pan_mod]*g_nwko[pan_mod]*sizeof(VECTOR));
   /*   printf("FOOT WAKE: g_mwko,g_nwko = %d, %d\n",g_mwko[pan_mod],g_nwko[pan_mod]); */
      
      k=0;
      for(i=0;i<g_nwko[pan_mod];i++) {
	for(j=0;j<g_mwko[pan_mod];j++) {
	  
	  x[k].x= g_xwakeo[pan_mod][i][j];
	  x[k].y= g_ywakeo[pan_mod][i][j];
	  x[k].z= g_zwakeo[pan_mod][i][j];
	 k++;
	}
      }
      if(g_nwko[pan_mod] < 1 || g_mwko[pan_mod] < 1) {
	
	sprintf(errStr,"Insert (foot) wake (model %d) has rows = %d, cols = %d",pMod,g_nwko[pan_mod],g_mwko[pan_mod]);
	g_World->OutputToClient(errStr,1);
	
      }
      else {
	key=HC_KInsert_Mesh(g_nwko[pan_mod],g_mwko[pan_mod],x); 
      }
      
      RXFREE(x);
    }
  }	
  
  return(key);
}

int Panel_Colour_Mesh(HC_KEY key,int Nin,int Min, float *vc,int quant){
  
  /* xv,yv,zv are NbyM. xc, yc, zc and vc are (N-1) by (M-1)  */
  int i,j,k;
  int mapcount,cnt;
  float *val,*value;
  float themax,themin,scale,SD,mean;
  static FILE *fp=NULL;
  float tval=0;
  
  if(g_ArgDebug) {  
    if(!fp) {
      char buf[120];
      sprintf(buf,"%s/colour.out",traceDir);	
      fp = FOPEN(buf,"w");
    }
    fprintf(fp,"Colour Mesh with N, M %d, %d\n",Nin,Min);
  }
  
  value= (float*)CALLOC((Nin-1)*(Min-1),sizeof(float));
  mean = 0;
  cnt = 0;

    themax=themin = *vc;
    for(i=0;i< Nin-1;i++) {
      for(j=0;j<Min-1;j++) {
	if(isnan(vc[i*(Min-1) + j])) {
			printf("vc NAN %d\n",i*(Min-1) + j  );
			vc[i*(Min-1) + j] = 0;
		}
	mean += vc[i*(Min-1) + j];
	themax = max(themax,vc[i*(Min-1) + j]);
	themin = min(themin,vc[i*(Min-1) + j]);
	value[cnt] = vc[i*(Min-1) + j];
	cnt++;
      }
    }
    mean = mean/cnt;
    SD=0;
    
    for(i=0;i<cnt;i++) {
      SD += (vc[i] - mean)*(vc[i] - mean);
    }
    SD = SD/cnt;						   /* standard deviation */
    SD = pow(SD,(float)0.5);
    
    if(g_ArgDebug) {
      fprintf(fp,"max,min = %6.5f, %6.5f\n",themax,themin);
      fprintf(fp,"mean,SD = %6.5f, %6.5f\n",mean,SD);
    }
    assert(quant+N_RELAX_SHELLS < VMAX);

  if(!g_default_maxmin[quant+N_RELAX_SHELLS]) {
    themax = mean + g_nSDs*SD;
    themin = mean - g_nSDs*SD;
    }
  else {
      themax = g_ValMax[quant+N_RELAX_SHELLS];
      themin = g_ValMin[quant+N_RELAX_SHELLS];
      mean = (themax + themin)/2;
    }
    if(g_ArgDebug) {
      fprintf(fp,"adjusted max,min = %6.5f, %6.5f\n",themax,themin);
    }
    
    mapcount = g_Mapcount-1;     /*MAXCOLOURS;*/
    if(mapcount <= 0) mapcount = 3;
    
    
    g_ValMin[quant+N_RELAX_SHELLS] = themin;
    g_ValMax[quant+N_RELAX_SHELLS] = themax;
    
    scale = ((themax-themin)/((float)(mapcount)));
    
    if(g_ArgDebug) {
      fprintf(fp,"scale = %6.5f\n",scale);
    }
    
    
    if(scale < 1e-4) {
      g_World->OutputToClient("scale < 1e-4 : Resetting to 1e-4",0);
      scale = 1e-4;
    }
    for(k=0,tval=themin;tval<(themax*1.0001);k++,tval+=scale) {
      g_PansailColour[quant][k] = tval;
    }
    g_PansailColour[quant][g_Mapcount-1] = themax;
    val = value;
    for(i=0;i<cnt;i++,val++) {
      *val = ((*val)-themin)/scale;
      if(*val <= 0 )  {
	(*val) = 0.00001;
      }
      if(*val >= mapcount) {
	*val = ((float)mapcount - 0.0001);
      }
    }
    fflush(fp);
    cnt=0;
    HC_Open_Geometry(key); {
      for(i=0;i<Nin-1;i++) {
	for(j=0;j<Min-1;j++) {
	  k = i*(Min) + j;
	  HC_Open_Face(k); {
	    HC_Set_Color_By_FIndex("face",value[cnt]);
	  } HC_Close_Face();
	  k = -(i*Min + j + Min+1);
	  
 	//   if(k>=0) {
	  	HC_Open_Face(k);  // Hoops 13 syntax.  For H14+, need 'legacy face numbering'
	    		HC_Set_Color_By_FIndex("face",value[cnt]);
	   	HC_Close_Face();
	  // 	} 
	   //	else printf("Why open face no  %d\n",k);
	  
	  cnt++;
	}
      }
    } HC_Close_Geometry();
    
    RXFREE(value);
    
    if(g_ArgDebug) {
      fflush(fp);
    }
    return(1);
  }
  
  
