/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * Dec 2003 coloring of strings according to tension. 
  But it's crazy to be reading the DAT file to get the string data.
 *
 * Modified :
 * may 30 02.  Soemtimes cannot insert_line because of zero s1 or s2 
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


#include "StdAfx.h"
/*

Read String Lengths from .DAT file !

*/

#include "do_boat.h"
#include "hoopcall.h"
#include "site.h"
#include "RXSail.h"

#include "sailutil.h"

#include "elements.h"
#include "entities.h"
#include "wrstate.h"
#include "f90_to_c.h"
#include "meshlib.h"
#include "hc.h"

#include "global_declarations.h"

#include "statestrings.h"


#define SEPTOK ", \t\n"
#define COMMA ","

#define  DEBUG (0)


int Read_StringState(SAIL *sail, const char *filename,int flags)
{ // opens a data file and reads in any string & batten data 

FILE *fp;
char line[256],buf[256];
char *sailName, *stringName=NULL, *lp,NameToKeep[256];
RXEntity_p  stringEnt;
SailData *sailD = NULL, *sd;
int count=0;
#ifndef NEVER
	rxerror("Read_StringState ",3);
#else
String_Data *str_data;

int nedge,j,k,what = 1;
double *slength, hue,t,tmin,tmax;
int eno,Use_Colors;
site *s1,*s2;
FortranEdge *ed;
long key;
int n1,n2,FileVersion=0;

NameToKeep[0]=0;
if DEBUG printf("Entered Read String State('%s')\n",filename);
fp = FOPEN(filename,"r");
if(!fp) return(0);

	fgets(line,255,fp);
	line[20]=0;

	if(strieq(line,"RELAX - II v1.0alpha")) FileVersion=1;
	else if(strieq(line,"RELAX - II v2.0alpha")) FileVersion=2;  
	else rxerror("This DAT File Version not supported ",2); 


if(!findString(fp,"StartString")) {
	FCLOSE(fp);
	return(0);
}

fgets(line,255,fp);
fgets(line,255,fp);	/* dump startstring & header line */

	sd = SailToData(sail);
if (DEBUG  && sd) printf(" Current sail is %s\n", sd->isail->GetType().Array());
int temp = sail->SailListIndex();
Use_Colors =  cf_get_tension_range_(&temp, &tmin, &tmax) ;
// if(Use_Colors) printf(" sail has tension range %f to %f\n", tmin,tmax);
Use_Colors = Use_Colors && (tmax > tmin + 0.00001) ;

count = 0;
while(fgets(line,255,fp)) {
	if(strstr(line,"EndString") || strstr(line,"Start")) 
		break; 

	sailName = strtok(line,COMMA);	
	if(sailName) 
		stringName = strtok(NULL,COMMA);	
	else
		stringName = NULL;
	
	PC_Strip_Trailing( stringName);
	PC_Strip_Trailing( sailName );
	if(!sailName || !stringName)
		break;
	strcpy(NameToKeep,sailName );

	SAIL *l_sail =g_World->FindModel(sailName);
	//sailEnt = 0;// PC_SG et_ Key(g_bo at,"sail",sailName); // looking for a hoisted sail named SailName
	if(!l_sail) {
		if DEBUG  printf(" NO hoisted sail called %s\n", sailName);
		break;
		}
	sailD = l_sail->sd;
 
	stringEnt =l_sail->GetKey("string",stringName);
	if(!stringEnt) {
		if DEBUG  printf("no stringEnt called %s\n", stringName);
		break;
	}

	str_data = (String_Data *) stringEnt->dataptr;
	if(!str_data) { printf(" NULL str_data\n"); break;}


	slength = &(str_data->Trim);
	key = stringEnt->hoopskey;

	if DEBUG printf(" name=%s Trim = %f\n", stringEnt->name,*slength);
	if(! fgets(line,255,fp)) { printf(" statestrings couldnt read file\n"); break;}
	if DEBUG printf(" next line=%s\n", line);
	lp = strtok(line,SEPTOK);	/* number */ 
	if(!lp)
		printf(" Crash reading %s\n", filename);
	else 
		what = atoi(lp);
	lp = strtok(NULL,SEPTOK);	/* no. of edges */ if(!lp)printf(" Crash reading %s\n", filename);
	nedge = atoi(lp);
	lp = strtok(NULL,SEPTOK);	/* Ti */ if(!lp)printf(" Crash reading %s\n", filename);
	lp = strtok(NULL,SEPTOK);	/* EA */ if(!lp)printf(" Crash reading %s\n", filename);
	lp = strtok(NULL,SEPTOK);	/* Zi */	 if(!lp)printf(" Crash reading %s\n", filename);
	if DEBUG printf("nedge = %d\n", nedge);
	(*slength) = atof(lp) ; /* - Get _String _Init_Length(strlink); */

	
	if(FileVersion<2) { 	(*slength) =0.0; rxerror(" Lost trims from V1 DAT File",2);
		}
	if DEBUG printf(" %s Trim becomes = %f (Fortran Zi was %f\n", stringEnt->name,*slength,atof(lp));

	HC_Open_Segment_By_Key(key);
	  HC_Flush_Contents(".","geometry,segments");
	  for(j=0;j<nedge;j++) {
		fgets(line,255,fp);
		eno = atoi(line);
		if(eno < 1) {
			sprintf(buf,"String Data wrong in '%s'\n expecting a number, got '%s' giving %d ",filename,line,eno);
		  	rxerror(buf,2);
		  	break;
		}
		ed =  Get_Edge(sailD->isail,eno);
		if(ed) {
			n1 = ed->node[0]->N;
			n2 = ed->node[1]->N;
			s1 = sailD->isail->GFNodeHead[n1];
			s2 = sailD->isail->GFNodeHead[n2];
			if(s1 && s2) 
			HC_Insert_Line( s1->x+s1->d_x,s1->y+s1->d_y,s1->z+s1->d_z,
				s2->x+s2->d_x,s2->y+s2->d_y,s2->z+s2->d_z);

		}
	}
	  if(Use_Colors) {
	   	temp = sail->SailListIndex();
		  if(cf_get_one_string_tension_(&temp,&what, &t)) {
			t = (t - tmin)/(tmax-tmin);
			hue = 270.0 - t*270.0;  // 270 =cold, descending to 0 =hot
			while(hue < 0.0)
				hue = hue + 360.00;
			while (hue > 360)
				hue = hue - 360.00;
		  }
		  else hue = 180;
	  }
	  else hue = 200;

	HC_Set_Color_By_Value("lines", "HSV",(float)hue,(float)1.0,(float)1.0);
	// HC_Set_Line_Weight(2.0);
	HC_Close_Segment();	
        count++;

} /* start again */


if(NameToKeep && *NameToKeep && findString(fp,"StartBatten")) {
char  l_batName[256];

HC_Open_Segment_By_Key(PostProcNonExclusive(sail)); HC_Open_Segment("battens");
HC_Flush_Contents(".","geometry,subsegments");
HC_Set_Color("lines=magenta");
HC_Set_Line_Weight(1.0);
	fgets(line,255,fp);
	fgets(line,255,fp);	/* dump startstring & header line */
	while(fgets(line,255,fp)) {
 		int ne,e;
 		float ei1,ei2;
		if(strstr(line,"EndBatten") || strstr(line,"Start")) 
			break; 
 		sscanf(line,"%d %s",&ne,l_batName); 
	if DEBUG
		printf(" No of edges in batten=%d\n", ne);

		fgets(line,255,fp); /* some rubbish */ 
		if(strstr(line,"EndBatten") || strstr(line,"Start")) 
			break; 
		for(k=0;k<ne;k++) {
			fgets(line,255,fp); 
			if(strstr(line,"EndBatten") || strstr(line,"Start")) 
			break; 
			if(3 !=sscanf(line," %d %f  %f",&e,&ei1,&ei2))
				 printf(" trouble reading '%s' in %s\n", line,filename);
			ed =  Get_Edge(sailD->isail,e);
			if(ed) {
				n1 = ed->node[0]->N;
				n2 = ed->node[1]->N;
				assert(sailD->isail==sail);
				s1 = sailD->isail->GFNodeHead[n1];
				s2 = sailD->isail->GFNodeHead[n2]; 
				ei1 = sqrt(sqrt(ei1/40E9 *64/3.142));
				ei2 = sqrt(sqrt(ei2/40E9 *64/3.142));
 				if(s1&&s2)  { // battenName was 'one_Batten'
					direct_make_cylinder(l_batName ,"magenta" ,s1->x+s1->d_x, s1->y+s1->d_y, s1->z+s1->d_z,
					s2->x+s2->d_x,s2->y+s2->d_y,s2->z+s2->d_z,(float)ei1,(float)ei2);
				}
			}

		}
	if(strstr(line,"EndBatten") || strstr(line,"Start")) 
			break; 
	fgets(line,255,fp); /* some rubbish */
	if(strstr(line,"EndBatten") || strstr(line,"Start")) 
			break; 

	}
HC_Close_Segment(); HC_Close_Segment();

} /* end of if( findString(fp,"StartBatten") */ 

FCLOSE(fp);
if DEBUG printf(" value (count) = %d\n",count);
#endif	
return(count);
}//int Read_StringState


