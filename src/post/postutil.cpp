/* routines used to make posting of some values to the
 * summary file easier

 * 17/3/97 added call to Post_VMC
 * 4.1.95        A K Molyneaux       created
 *
 */

#include "StdAfx.h"
#include "RelaxWorld.h"
#include "summary.h"

#include "global_declarations.h"
#include "RXDatabaseI.h"

#include "drawwind.h"



int RXWindGeometry::Post_Wind_Details( )
{
    /* includes all things in the control window
   * heading
   * boat speed
   * true wind speed
   * heel angle
   * mast length
   * leeway
   */
    if( !g_World->DBMirror()  )
        return 0;
    int retVal = 0;
    char buf[256];
    const char *noyes[] = {"no","yes"};
    Post_Summary_By_Sail(NULL,"Run_Name",g_World->GetRunName());

    sprintf(buf,"%f",m_TWA* 180.0/ON_PI);
    Post_Summary_By_Sail(NULL,"Heading",buf);
    sprintf(buf,"%f",m_TWS);
    Post_Summary_By_Sail(NULL,"TWS",buf);
    sprintf(buf,"%f",m_Vboat);
    Post_Summary_By_Sail(NULL,"Boat Speed",buf);
    sprintf(buf,"%f",g_World->HeelAngle());
    Post_Summary_By_Sail(NULL,"HeelAngle",buf);
    sprintf(buf,"%f",m_Leeway* 180.0/ON_PI);
    Post_Summary_By_Sail(NULL,"Leeway",buf);

    sprintf(buf,"%f",m_Reference_Length);
    Post_Summary_By_Sail(NULL,"Reference Height",buf);

    sprintf(buf,"%f" ,this->Get(AWA  )*57.29577951);
    Post_Summary_By_Sail(NULL,"AWA",buf);
    sprintf(buf,"%f" ,Get (RXWindGeometry::AWS));
    Post_Summary_By_Sail(NULL,"AWS",buf);



#ifdef USE_PANSAIL
    sprintf(buf,"%f",g_ReflectHeight);
    Post_Summary_By_Sail(NULL,"Reflection Height",buf);

    if(fabs( Pitch_Velocity )> 0.0) {
        sprintf(buf,"%f",g_PansailIn.Pitch_Velocity);
        Post_Summary_By_Sail(NULL,"Pitch Velocity",buf);
    }
    if(fabs( Roll_Velocity) > 0.0) {
        sprintf(buf,"%f",g_PansailIn.Roll_Velocity);
        Post_Summary_By_Sail(NULL,"Roll Velocity",buf);
    }
#endif
    Post_VMC();

    Post_Summary_By_Sail(NULL,"Reflection Plane",noyes[m_xyplane-1]);
    Post_Summary_By_Sail(NULL,"Wind File",qPrintable(m_windName));



    Draw_Wind((float)0.0,m_Reference_Length);
    return(retVal);
}


int RXWindGeometry::Post_VMC(){
    char value[64];
    double V,A;
    if (!g_World->DBMirror() )
        return 0;
    V = m_Vboat / cos(m_Leeway/57.293);
    A = (m_TWA + m_Leeway - m_Course) /57.293;
    m_Vmc = V * cos(A);

    sprintf(value,"%f",m_Course);
    g_World->DBMirror()->post_summary_value(  "Course" ,value);
    sprintf(value,"%f",m_Vmc);
    return(g_World->DBMirror()->post_summary_value(  "VMC" ,value));
}






