/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#define ADJDB (0)

#include "StdAfx.h"
#include "RXSail.h"
//#include "site.h"

#include "sailutil.h"


#include "elements.h"
#include "entities.h"
#include "offset.h"
#include "etypes.h"
//#include "readdat.h"

#include "adjlen.h"


int FindZlink0_of_Psides(SAIL *sail)
{
  int i,k,nedges;
  RXEntity_p  p;

  Pside *ps;
  float length,alen;
  int err,model,ed;
  
  if (ADJDB) printf("Entering FindZlin0_of_Psides\n"); 

  if(!sail) {
    return(-1);
  }
 assert("<sail->NITEMS"==0); 
#ifdef NEVER
  for (i=0;i<sail->NITEMS;i++) {
    p = sail->list[i];
    if (strieq(p->type,"pside")){
      length = 0;
      nedges=0;
      ps = (Pside *) p->dataptr;
      if (ADJDB) printf("Found Pside named '%s'\n",p->name);
      for(k=0; k<ps->eNo;k++) {
	if (ADJDB) printf("FEdge %d: %d -> %d\n",((ps->eList[k])->n),((ps->eList[k])->node[0])->N,((ps->eList[k])->node[1])->N); 
	model = sail->sd->MODel+1;
	ed = (ps->eList[k])->n;
	//cf_get_zlink0_(&model,&ed,&alen,&err);
          cf_get_zlink0bysli_(sail->SailListIndex(),&ed,&alen,&err);
	if(err != 0) {
	  g_World->OutputToClient ("invalid return code from fortran get_zlink0...",2);
	  getchar();
	  alen = 0;
	}
	length += alen;
	nedges++;
      }
      ps->zlink0 = length;
      if (ADJDB) printf("Length of pside '%s' (%d edges) = %f\n",p->name,nedges,length);
    }
  } // for i 
#endif
   sail->OutputToClient ("done FindZlink0_of_Psides",1);
  return(0);
}



