/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *  4 jan 2007  extra values at about line 1099
 
 *  20.7.97 leak repairs
 * 26/2/96 on porting to MSVC. 
 *		error has no return value
 *    GSHPRNT to turn off printing
 *       17/1/95     ' upper_P_Strain'   stub now in. Please make a shell of it 
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXSail.h"
#include "RXSRelSite.h"
#include "RX_FETri3.h"
#include "RXPanel.h"
#include "RXLayer.h"

#include "linuxdbg.h"
#include "colours.h"
#include "akmpretty.h"
#include "nlm.h"
 
#include "trivals.h"
#include "summary.h"

#include "f90_to_c.h"
 
#include "global_declarations.h"

#include "RXmaterialMatrix.h"
 
#include "fielddef.h"
#include "etypes.h"
#include "RXdovefile.h"
#include "dovestructure.h"
#include "getshell.h"
#if defined (_WINDOWS) && defined(DEBUG)
	#define GSHPRNT (0) //(3)
#else
	#define GSHPRNT (0)
#endif

#ifndef MIN_SCALE
	#define MIN_SCALE  1e-8 
#endif //#ifndef MIN_SCALE

#ifdef _WINDOWS
#define isnan _isnan
#endif
// produces::
// pts is a contiguous list of node coords.  Node 1 maps to pts[0]
// and any gaps in the node list are closed up. 
// this means that any gaps will cause flist to be wrong
// because flist assumes there is only one gap , the one at 0
// flist is the same. Gaps in the triangle list get closed.
// and flist[0] maps to triangle[1]
int Get_Shell_Data_Geom(class RXSail *sail,VECTOR **pts,int **flist,int *pcount,int *fcount)
{
  /* allocates space for vector points and faces
   * reads in data from a file (later from structure)
   * return :
   1 - failure
   0 - success
   */

   int sli = sail->SailListIndex();
 class RX_FESite *s;
 // int i;
  int lcnt;
  int cnt=0,nt=0; // ,ncnt;
  VECTOR *v;
  int fl;
  *pcount=0;

   lcnt = sail->m_Fnodes .size();// but as m_Fnodes[0] is always null, nnodes= lcnt-1
 
  if (*pts) RXFREE ((*pts));
  v = (*pts) = (VECTOR*)MALLOC((lcnt+2)*sizeof(VECTOR)); 
vector<RX_FESite*>::iterator it=sail->m_Fnodes.begin(); it++;
 	for( ;it!=sail->m_Fnodes.end();++it) {
		s = *it;
		if(s){
		if (s->GetN() ==0) continue;

		ON_3dPoint pp =s->DeflectedPos(RX_GLOBAL);
		if(GSHPRNT>2) printf("GSDG_DEfPos\t%d  \t%f\t%f\t%f\n",s->GetN(),pp.x,pp.y,pp.z);
		v->x = v->y =v->z =-3.0; 

		if(!isnan(pp[0]) && fabs(pp[0])< 10000.0 )		v->x = pp[0];  
		if(!isnan(pp[1]) && fabs(pp[1])< 10000.0 )		v->y = pp[1] ;
		if(!isnan(pp[2]) && fabs(pp[1])< 10000.0 )		v->z = pp[2];
		}
		else{
			 if(GSHPRNT)cout<<"(Get_Shell_Data_Geom) null node at count="<<cnt<<endl;
			v->x=-3;  v->y=4; v->z=5;
		}
		v++;     cnt++;
	} // for i

   if(GSHPRNT) printf("c in getshell pt count = %d\n",cnt);  

  *pcount = cnt; 
  
  lcnt = sail->m_FTri3s.size();
    if(GSHPRNT) printf("c in getshell face count = %d\n",lcnt-1);  
  /* now get face connections */
  fl=0;
  if(lcnt > 0) {
	if(*flist) RXFREE(*flist);  
    (*flist) = (int*)CALLOC(4*lcnt+8,sizeof(int));
 
for(vector<RX_FETri3*>::iterator ii = sail->m_FTri3s.begin()  ;ii != sail->m_FTri3s.end();ii++) { // for all in m_ftri3s
			FortranTriangle *te  = *ii;
			if(te == NULL) continue;
			if (!te->GetN() ) continue;
			if(!te->IsUsed()) continue;
		
			(*flist)[fl] = 3;
			(*flist)[fl + 1] =  te->Node(0)-1;
			(*flist)[fl + 2] =  te->Node(1)-1;
			(*flist)[fl + 3] =  te->Node(2)-1;
			fl=fl+4;
    
			  nt++;
		} 
  }// lcount
  if(GSHPRNT){cout << "flistlength= " << fl  << " nfaces = "<<nt<<endl;   }
  *fcount = fl;

 // if(GSHPRNT) printf("nt = %d\n ",nt);  
  return(nt);
  
}//int Get_Shell_Data_Geom
 


HC_KEY DrawArrow(float ox,float oy,float oz,float dx,float dy,float dz,
	       VECTOR *norm,float L)
{
  
  /* draw an arrow from o to d */
  /* and scale to length L      */
  
  float len,alen;
  HC_KEY key=0;
  float alpha;
  VECTOR v,a1,a2;
  float rotmx[16];
  
  len = sqrt((dx-ox)*(dx-ox) +(dy-oy)*(dy-oy)+(dz-oz)*(dz-oz));
  if(fabs(len)> 1e-10)  len = L/len;
  else len = 1;
  
  alen = L/10;
  
  
  dx = dx*len;  dy = dy*len;  dz = dz*len;
  key = HC_KInsert_Line(ox,oy,oz,dx,dy,dz);
  
  v.x = dx-ox;  v.y = dy-oy;  v.z = dz-oz;
  
  alpha=atan2(v.y,v.x);
  
  a1.x=a2.x=alen*0.707107; //cos
  a1.y = alen*0.707107; //sin
  a2.y = - a1.y;
  a1.z = a2.z = dz;
  
#ifdef HOOPS
  HC_Compute_Offaxis_Rotation(norm->x,norm->y,norm->z,-alpha,rotmx);
  HC_Compute_Transformed_Points(1,&a1,rotmx,&v);
  key = HC_KInsert_Line(dx,dy,dz,dx-v.x,dy-v.y,dz-v.z);
  
  HC_Compute_Transformed_Points(1,&a2,rotmx,&v);
  key = HC_KInsert_Line(dx,dy,dz,dx-v.x,dy-v.y,dz-v.z);
#endif
  return(key);
}//HC_KEY DrawArrow

 
int Get_Triangle_Results(SAIL * p_sail,TriStressStruct **tridata)
{
// called from attach.c  
  /* load the tri resulst values into 
   * an array from the fortran.
   * 
   */
// to freeze we might have a value 0 1 or 2
// 0 means no freezing
// 1 means use this call as basis for freezing
// 2 means report the difference between frozen and current values.
 
	int sli = p_sail->SailListIndex();
	TriStressStruct *val=NULL,*data=NULL, *v0=NULL;
  	int L, ntri, iii; // the tri is L
  	int  l_frozen;
	RXdovefile *theDove=0;
	RXEntity_p doveEnt=0;
	dovestructure *theDoveStructure=0;
	TriLamDefn *tld;
	class RXLayer *lay;
	RXEntity_p matent=0;
	double l_doveAngle=0;
	
	double q[9] = {2.05e6,0,0,0,0,0,0,0,0};
	  
  	double sxt=0.0 ,syt=0.0 ,sxyt=0.0 , ext=0.0 ,eyt=0.0 ,exyt=0.0 ;
	double  areaT=0.0, tapeAreaT =0.0;
	char vbuf[256];
 
	areaT=0.0; 
	if(GSHPRNT) cout<< " IN Get_Triangle_Result\n"<<endl; 
#ifdef RX_USE_DOVE	
 	RXmaterialMatrix qply = RXmaterialMatrix(q); 
#endif
  ntri =  p_sail->m_FTri3s.size(); 

 data = (TriStressStruct *)CALLOC(ntri+2,sizeof(TriStressStruct)) ;
  
  val = data; 
  v0 = p_sail->m_StressResults;
  l_frozen = p_sail->GetFlag(FL_ISFROZEN);
  if(l_frozen) {
	 assert(v0);
	 }
  
  if(ntri <= 0) {

	if(data) RXFREE(data);
	if(*tridata) RXFREE(*tridata);
	*tridata = NULL;
    	return(0);
  }
 
  for(vector<RX_FETri3*>::iterator ii = p_sail->m_FTri3s.begin()  ;ii != p_sail->m_FTri3s.end();ii++) { // for all in m_ftri3s
			FortranTriangle *te  = *ii;
			if(te == NULL) continue;
			if (!te->GetN() ) continue;
 			{
			L = te->GetN(); 
    
      		cf_one_tri_stress_op(L,sli, (val->m_stress),(val->m_eps),(val->m_princ),&(val->m_cre),&(val->m_Princ_Stiff));
    		
/// get the dove and its angle

				tld = te->m_laminae;
				for(iii=0;iii<te->laycnt;iii++) {
					lay = tld[iii].Layptr;
					matent = lay->matptr;
					if(matent->TYPE==FIELD) {
						struct PC_FIELD *f = (struct PC_FIELD*)matent->dataptr;
						if(f->flag&DOVE_FIELD) {
							doveEnt = f->mat;
							theDove = (RXdovefile *) doveEnt->dataptr;
							theDoveStructure=theDove->m_amgdove;
							l_doveAngle = tld[iii].Angle ;
							if(!theDoveStructure->GetStackType())
								theDoveStructure=0;
						}
					}
				}
// end get dove      		


//			RXmaterialMatrix l_mm = RXmaterialMatrix(te->mx);
//			l_mm.ToParameters(qply,&val->m_thk,&val->ke,&val->kg,&val->nu);

			if(theDoveStructure) {// see  print_triangle_data to evaluate theDove
				double epsm[3];	
				RXSitePt l_cs(0.,0.,0.,0.,0,0);
				te->Centroid(&l_cs);					
//				epsm[0] = val->eps[0]; 	epsm[1] = val->eps[1]; 
//				epsm[2] = val->eps[2] /2.0;rotate_stress(epsm, l_doveAngle );epsm[2] = epsm[2]*2.0;
//WRONG should use Dinv * stress to get epsm	

				ON_Matrix l_onm; //  = l_mm.GetMatrix(); l_onm.Invert(1e-10);

				ON_3dVector l_onstress = ON_3dVector(val->m_stress[0],val->m_stress[1],val->m_stress[2]);
				ON_Xform l_xf = ON_Xform(l_onm);
				ON_3dVector l_one = l_xf*l_onstress;
				epsm[0] = l_one[0]; 	epsm[1] = l_one[1]; 
				epsm[2] = l_one[2] /2.0;
				RXPanel::rotate_stress(epsm, l_doveAngle );
				epsm[2] = epsm[2]*2.0;				
					
				theDoveStructure->PlyStrains(l_cs.m_u,l_cs.m_v,epsm,  
				val->plyminX,(val->plymaxX),(val->plyminY),
				(val->plymaxY),(val->plymaxXY));
				}
		sxt += val->m_stress[0]*te->area;
		syt += val->m_stress[1]*te->area;
 		sxyt += val->m_stress[2]*te->area;
		ext  += val->m_eps[0]*te->area;
		eyt  += val->m_eps[1]*te->area;
		exyt  += val->m_eps[2]*te->area;
		areaT += te->area; 
		tapeAreaT += te->area* val->m_thk;

int k;		
		for (k=0;k<3;k++) {
			if(isnan(val->m_stress[k])  )   val->m_stress[k]=0.;
			if(isnan(val->m_eps[k]))   val->m_eps[k]=0.;
			if(isnan(val->m_princ[k]))   val->m_princ[k]=0.;
			if(isnan(val->m_Princ_Stiff))  val->m_Princ_Stiff=0.;
		}
		// now subtract the frozen values; 
		if(l_frozen) {
			for (k=0;k<3;k++) {
				val->m_stress[k] 	= val->m_stress[k]- v0->m_stress[k];
				val->m_eps[k]   	= val->m_eps[k]	  - v0->m_eps[k];
				val->m_princ[k] 	= val->m_princ[k] - v0->m_princ[k];
			}
			val->m_cre 		= val->m_cre 	- v0->m_cre;
		}	
		
    }
 	v0++;	  
    val++;
  }	// for i

	if(areaT > 0.00001) {
		sxt /=areaT; syt /=areaT;sxyt /=areaT;ext /=areaT;eyt /=areaT;exyt /=areaT;
		sprintf(vbuf,"%f",sxt);  Post_Summary_By_Sail(p_sail , "mean_sxx",vbuf);
		sprintf(vbuf,"%f",syt);  Post_Summary_By_Sail(p_sail, "mean_syy",vbuf);
		sprintf(vbuf,"%f",sxyt); Post_Summary_By_Sail(p_sail, "mean_txy",vbuf);
		sprintf(vbuf,"%f",ext);  Post_Summary_By_Sail(p_sail , "mean_exx",vbuf);
		sprintf(vbuf,"%f",eyt);  Post_Summary_By_Sail(p_sail , "mean_eyy",vbuf);
		sprintf(vbuf,"%f",exyt); Post_Summary_By_Sail(p_sail , "mean_gxy",vbuf);
		if(tapeAreaT > 0.00001) 		
			{sprintf(vbuf,"%f",tapeAreaT ); Post_Summary_By_Sail(p_sail ,"Tape_Area",vbuf);}
	}
	if(GSHPRNT) 
	{ 
		printf(" sail %s\n",p_sail->GetType().c_str ());
		printf(" sxt,syt,sxyt,ext,eyt,exyt,areaT, %f %f %f %f %f %f %f\n", sxt,syt,sxyt,ext,eyt,exyt,areaT);
  		HC_Show_Pathname_Expansion(".",vbuf); 
		printf(" in  seg <%s>\n",vbuf);
	}	
	
  *tridata = data;
 if(GSHPRNT) cout<< " return from Get Tri-angle Results\n"<<endl; 
  return(1);	
}//int Get_Triangle_Results

int Set_Shell_Data_Stress(class RXSail *sail,HC_KEY theShell,int what,TriStressStruct *tridata,const int simple)
{

	int k,i=-5,j=0,Index;
	TriStressStruct *data=NULL;
	 double scale;//,SD,mean;

	float *vp,*tvalue=NULL,*nvalue; 
	float cmin,cmax,tval;
	float nSDs;
	int ncol,ntodim,ierr;
	assert(tridata);  
	nSDs = g_nSDs;
	int ntri = sail->m_FTri3s.size()-1;
	int nNod = sail->m_Fnodes.size()-1;
	int sli= sail->SailListIndex();
	//SD=mean=0.0;
  switch(what) {
  case 1:
  case 2:
  case 3:
		HC_Open_Geometry(theShell);
		j=0;
		data = tridata;
		vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
		for(i=0;i<ntri;i++,vp++,data++)  {
		  *vp = data->m_stress[what-1];
		}
		nvalue = (float*)CALLOC((nNod+2),sizeof(float));

		ncol = g_Mapcount;
	    
		 if(!g_default_maxmin[what]) {
		  if(simple) {
		if(make_triangle_values(sail->SailListIndex (),tvalue,ntri,ncol,nSDs,&cmin,&cmax))
			AKM_Mset_Face_colors_By_FIndex(theShell,"faces",0,ntri,tvalue);
		  }
		  else {
		if(cf_make_nodal_values(sail->SailListIndex(),tvalue,nvalue,&ncol,&nSDs,&cmin,&cmax)){


#ifdef HOOPS
			int pc,fc;
			HC_Show_Shell_Size(theShell,&pc,&fc); 
#endif

			HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
		}
		  }
		  g_ValMax[what] = cmax;
		  g_ValMin[what] = cmin;
		}
		else {
		  cmax = g_ValMax[what];
		  cmin = g_ValMin[what];
		  nSDs = -1; /* flag to say use the cmax/min values */
		  if(cf_make_nodal_values(sail->SailListIndex(),tvalue,nvalue,&ncol,&nSDs,&cmin,&cmax))
   		   HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
		}
		scale = ((cmax-cmin)/((float)(g_Mapcount - 1)));
		if(scale < MIN_SCALE) {
		  cmax = cmax + MIN_SCALE/2;
		  cmin = cmin - MIN_SCALE/2;
		  scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
		}
		for( k=0,tval=cmin;tval<(cmax*(1+MIN_SCALE)) && k <= g_Mapcount;k++,tval+=scale) {
		  g_RelaxColour[what][k] = tval;
		}
		g_RelaxColour[what][g_Mapcount - 1] = cmax;
		RXFREE(nvalue);
		RXFREE(tvalue);
		HC_Close_Geometry();
		break;
    
  case 4:
  case 5:
  case 6:	
		assert(tridata);
		HC_Open_Geometry(theShell);
		j=0;
		data = tridata;
		vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
		for(i=0;i<ntri;i++,vp++,data++)  {
		  *vp = data->m_eps[what-4];
		}
		nvalue = (float*)MALLOC((nNod+2)*sizeof(float));
		ncol = g_Mapcount;
	       
		if(!g_default_maxmin[what]) {
		  if(simple) {
			make_triangle_values(sail->SailListIndex(),tvalue,ntri,ncol,nSDs,&cmin,&cmax);
			AKM_Mset_Face_colors_By_FIndex(theShell,"faces",0,ntri,tvalue);
		  }
		  else {
		if(cf_make_nodal_values(sail->SailListIndex(),tvalue,nvalue,&ncol,&nSDs,&cmin,&cmax))
			HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
		  }
		  g_ValMax[what] = cmax;
		  g_ValMin[what] = cmin;
		}
		else {
		  cmax = g_ValMax[what];
		  cmin = g_ValMin[what];
		  nSDs = -1; /* flag to say use the cmax/min values */
		  if(cf_make_nodal_values(sail->SailListIndex(),tvalue,nvalue,&ncol,&nSDs,&cmin,&cmax))
     		 HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
		}
		scale = ((cmax-cmin)/((float)(g_Mapcount - 1)));
		if(scale < MIN_SCALE) {
		  cmax = cmax + MIN_SCALE/2;
		  cmin = cmin - MIN_SCALE/2;
		  scale = ((cmax-cmin)/((float)(g_Mapcount - 1)));
		}
		for( k=0,tval=cmin;tval<(cmax*(1+MIN_SCALE)) && k <= g_Mapcount;k++,tval+=scale) {
		  g_RelaxColour[what][k] = tval;
		}
		g_RelaxColour[what][g_Mapcount - 1] = cmax;
		RXFREE(nvalue);
		RXFREE(tvalue);
		HC_Close_Geometry();
		break;
    
  case 7:	//what   UP Stress  
		assert(tridata);
		HC_Open_Geometry(theShell);
		j=0;
		data = tridata;
		vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
		for(i=0;i<ntri;i++,vp++,data++)  {
		  *vp = data->m_princ[0];
		//printf(" raw tri ups value %d %f\n", i, tvalue[i]);
		}
		nvalue = (float*)MALLOC((nNod+2)*sizeof(float));
	 
		ncol = g_Mapcount;
	    
		 if(!g_default_maxmin[what]) { 
		  if(simple) {
		make_triangle_values(sail->SailListIndex(),tvalue,ntri,ncol,nSDs,&cmin,&cmax);
		for(i=0;i< ntri; i++) { if(isnan(tvalue[i])) {tvalue[i]=0; printf("NAN face value  %d ", i);}}
		AKM_Mset_Face_colors_By_FIndex(theShell,"faces",0,ntri,tvalue);
		  }
		  else {
		if(cf_make_nodal_values(sail->SailListIndex(),tvalue,nvalue,&ncol,&nSDs,&cmin,&cmax)){
		
			for(i=0;i< nNod; i++) {
 				if(isnan(nvalue[i])) { nvalue[i] =0;  printf("NAN vertex color  %d \n", i);}
			 }
			HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
			}
		  }
		  g_ValMax[what] = cmax;
		  g_ValMin[what] = cmin;
		}
		else {printf("YES g_default_maxmin[7] (%f to %f)\n",g_ValMin[what], g_ValMax[what]);
		  cmax = g_ValMax[what];
		  cmin = g_ValMin[what];
		  nSDs = -1; /* flag to say use the cmax/min values */
		  if(cf_make_nodal_values(sail->SailListIndex(),tvalue,nvalue,&ncol,&nSDs,&cmin,&cmax)){
				for(i=0;i< nNod; i++) {
 					if(isnan(nvalue[i])) { nvalue[i] =0;  printf("NAN vertex color  %d \n", i);}
				 }
   	  			 HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
			}
		}
		scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
		if(scale < MIN_SCALE) {
		  cmax = cmax + MIN_SCALE/2;
		  cmin = cmin - MIN_SCALE/2;
		  scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
		}
		for(k=0,tval=cmin;tval<(cmax*(1+MIN_SCALE)) && k <= g_Mapcount;k++,tval+=scale) {
		  g_RelaxColour[what][k] = tval;
		}
		g_RelaxColour[what][g_Mapcount - 1] = cmax;
		RXFREE(nvalue);
		RXFREE(tvalue);
		HC_Close_Geometry();
		break;
     case 9:	//what   LP Stress  
			assert(tridata);
			HC_Open_Geometry(theShell);
			j=0;
			data = tridata;
			vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
			for(i=0;i<ntri;i++,vp++,data++)  {
			  *vp = data->m_princ[1];
			}
			nvalue = (float*)MALLOC((nNod+2)*sizeof(float));
		 
			ncol = g_Mapcount;
		    
			if(!g_default_maxmin[what]) {
			  if(simple) {
					make_triangle_values(sail->SailListIndex(),tvalue,ntri,ncol,nSDs,&cmin,&cmax);
					AKM_Mset_Face_colors_By_FIndex(theShell,"faces",0,ntri,tvalue);
			  }
			  else {
				if(cf_make_nodal_values(sail->SailListIndex(),tvalue,nvalue,&ncol,&nSDs,&cmin,&cmax))
				HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
			  }
			  g_ValMax[what] = cmax;
			  g_ValMin[what] = cmin;
			}
			else {
			  cmax = g_ValMax[what];
			  cmin = g_ValMin[what];
			  nSDs = -1; /* flag to say use the cmax/min values */
			  if(cf_make_nodal_values(sail->SailListIndex(),tvalue,nvalue,&ncol,&nSDs,&cmin,&cmax))
 				 HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
			}
			scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
			if(scale < MIN_SCALE) {
			  cmax = cmax + MIN_SCALE/2;
			  cmin = cmin - MIN_SCALE/2;
			  scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
			}
			for(k=0,tval=cmin;tval<(cmax*(1+MIN_SCALE)) && k <= g_Mapcount;k++,tval+=scale) {
			  g_RelaxColour[what][k] = tval;
			}
			g_RelaxColour[what][g_Mapcount - 1] = cmax;
			RXFREE(nvalue);
			RXFREE(tvalue);
			HC_Close_Geometry();
			break;
    
  case 8:	//pressure
		HC_Open_Geometry(theShell);
	     
		vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
		for(vector<RX_FETri3*>::iterator ii = sail->m_FTri3s.begin()  ;ii != sail->m_FTri3s.end();ii++ ) { // for all in m_ftri3s
			FortranTriangle *te  = *ii;
			if(te)
			{*vp = - te->GetPressure();vp++;} // its the first one which is null ( I think)
		}
		nvalue = (float*)MALLOC((nNod+2)*sizeof(float));
	 
		ncol = g_Mapcount;
	    
		if(!g_default_maxmin[what]) {
			  if(simple) {
					make_triangle_values(sail->SailListIndex(),tvalue,ntri,ncol,nSDs,&cmin,&cmax);
					AKM_Mset_Face_colors_By_FIndex(theShell,"faces",0,ntri,tvalue);
			  }
			  else {
					if(cf_make_nodal_values(sail->SailListIndex(),tvalue,nvalue,&ncol,&nSDs,&cmin,&cmax))
					HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
			  }
			  g_ValMax[what] = cmax;
			  g_ValMin[what] = cmin;
		}
		else {
			  cmax = g_ValMax[what];
			  cmin = g_ValMin[what];
			  nSDs = -1; /* flag to say use the cmax/min values */
			  if(cf_make_nodal_values(sail->SailListIndex(),tvalue,nvalue,&ncol,&nSDs,&cmin,&cmax))
      				HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
		}
		scale = ((cmax-cmin)/((float)(g_Mapcount -1 )));
		if(scale < MIN_SCALE) {
			  cmax = cmax + MIN_SCALE/2;
			  cmin = cmin - MIN_SCALE/2;
			  scale = ((cmax-cmin)/((float)(g_Mapcount -1 )));
		}
		for(k=0,tval=cmin;tval<(cmax*(1+MIN_SCALE)) && k <= g_Mapcount;k++,tval+=scale) {
				g_RelaxColour[what][k] = tval;
		}
		g_RelaxColour[what][g_Mapcount - 1] = cmax;
	    
		RXFREE(nvalue);
		RXFREE(tvalue);
		HC_Close_Geometry();
		break;
  /*case 9: now LP stress*/ /* dktest */ /* note: data at nodes not faces */
  
 
  case 9990011:	/* dktest */ /* note: data at nodes not faces */
 
    HC_Open_Geometry(theShell);
    nvalue = (float*)CALLOC((nNod+2),sizeof(float));
 
    ncol = g_Mapcount;
    nSDs = 0.002;
    Index = what - 8;
	
/*      if(what==9) test_deltak_(&Fmodel);  */

 /* PH test not production code  PASSED*/	
/*     delta_k_(&Fmodel,&Index,nvalue); */
    if(!g_default_maxmin[what]) {
      scale_nodal_values(nNod,nvalue,ncol,nSDs,&cmin,&cmax);
    }
    else {
      cmax = g_ValMax[what];
      cmin = g_ValMin[what];
      nSDs = -1; /* flag to say use the cmax/min values */
      scale_nodal_values(nNod,nvalue,ncol,nSDs,&cmin,&cmax);
    }
	
    HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
    
    g_ValMax[what] = cmax;
    g_ValMin[what] = cmin;
    scale = ((cmax-cmin)/((float)(g_Mapcount - 1)));
    if(scale < MIN_SCALE) {
      cmax = cmax + MIN_SCALE/2;
      cmin = cmin - MIN_SCALE/2;
      scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
    }
    for(k=0,tval=cmin;tval<(cmax*(1+MIN_SCALE)) && k <= g_Mapcount;k++,tval+=scale) {
      g_RelaxColour[what][k] = tval;
    }
    g_RelaxColour[what][g_Mapcount - 1] = cmax;
    
    RXFREE(nvalue);
    HC_Close_Geometry();
    break;
  case 10:	/* dk */ /* note: data at nodes not faces */
   	HC_Open_Geometry(theShell);

    	ncol = g_Mapcount;
    	nSDs = 0.2;
		float s_angle,g_angle;
    	ntodim = nNod+2; 
    	nvalue = (float*)MALLOC((ntodim)*sizeof(float));
	ierr=0;
	for(k=1 ;k <= nNod; k++) {
		cf_get_svalue(sail->SailListIndex (),k,&(s_angle),&ierr);  
		if(ierr) {
			printf(" (%s) get_svalue error = %d  k %d\n",sail->GetType().c_str(),ierr,k);
			break;
		}

		cf_get_gvalue(sail->SailListIndex(),k,&(g_angle),&ierr);  
		if(ierr) {
			printf(" (%s) get_gvalue error = %d  k %d\n",sail->GetType().c_str(),ierr,k);
			break;
		}
		nvalue[k-1] = g_angle - s_angle;
	}
    
    if(!g_default_maxmin[what]) {
      scale_nodal_values(nNod,nvalue,ncol,nSDs,&cmin,&cmax);
    }
    else {
      cmax = g_ValMax[what];
      cmin = g_ValMin[what];
      nSDs = -1; /* flag to say use the cmax/min values */
      scale_nodal_values(nNod,nvalue,ncol,nSDs,&cmin,&cmax);
    }
   
    HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
    
    g_ValMax[what] = cmax;
    g_ValMin[what] = cmin;
    scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
    if(scale < MIN_SCALE) {
      cmax = cmax + MIN_SCALE/2;
      cmin = cmin - MIN_SCALE/2;
      scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
    }
    for(k=0,tval=cmin;tval<(cmax*(1+MIN_SCALE)) && k <= g_Mapcount;k++,tval+=scale) {
      g_RelaxColour[what][k] = tval;
    }
    g_RelaxColour[what][g_Mapcount - 1] = cmax;
    
    RXFREE(nvalue);
    HC_Close_Geometry();
    break;
  case 12:	/* S_angles */ /* note: data at nodes not faces */
    HC_Open_Geometry(theShell);

    ncol = g_Mapcount;
    nSDs = 0.2;
    	ntodim = nNod+2;
    nvalue = (float*)MALLOC((ntodim)*sizeof(float));
	ierr=0;
	for(k=1 ;k <= ntodim; k++) {
		cf_get_svalue(sli,k,&(nvalue[k-1]),&ierr);  

		if(ierr) {
		break;
		}
	}
    
    if(!g_default_maxmin[what]) {
      scale_nodal_values(nNod,nvalue,ncol,nSDs,&cmin,&cmax);
    }
    else {
      cmax = g_ValMax[what];
      cmin = g_ValMin[what];
      nSDs = -1; /* flag to say use the cmax/min values */
      scale_nodal_values(nNod,nvalue,ncol,nSDs,&cmin,&cmax);
    }
   
    HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
    
    g_ValMax[what] = cmax;
    g_ValMin[what] = cmin;
    scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
    if(scale < MIN_SCALE) {
      cmax = cmax + MIN_SCALE/2;
      cmin = cmin - MIN_SCALE/2;
      scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
    }
    for(k=0,tval=cmin;tval<(cmax*(1+MIN_SCALE)) && k <= g_Mapcount;k++,tval+=scale) {
      g_RelaxColour[what][k] = tval;
    }
    g_RelaxColour[what][g_Mapcount - 1] = cmax;
    
    RXFREE(nvalue);
    HC_Close_Geometry();
    break;
  case 13:	/* ErrIndex */ /* note: data at nodes not faces */
    HC_Open_Geometry(theShell);
    nvalue = (float*)CALLOC((nNod+2),sizeof(float));
    ncol = g_Mapcount;
    
    if(g_default_maxmin[what]) {
      scale_nodal_values(nNod,nvalue,ncol,nSDs,&cmin,&cmax);
    }
    else {
      cmax = g_ValMax[what];
      cmin = g_ValMin[what];
      nSDs = -1; /* flag to say use the cmax/min values */
      scale_nodal_values(nNod,nvalue,ncol,nSDs,&cmin,&cmax);
    }
  
    HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
    
    g_ValMax[what] = cmax;
    g_ValMin[what] = cmin;
    scale = ((cmax-cmin)/((float)(g_Mapcount -1 )));
    if(scale < MIN_SCALE) {
      cmax = cmax + MIN_SCALE/2;
      cmin = cmin - MIN_SCALE/2;
      scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
    }
    for(k=0,tval=cmin;tval<(cmax*(1+MIN_SCALE)) && k <= g_Mapcount;k++,tval+=scale) {
      g_RelaxColour[what][k] = tval;
    }
    g_RelaxColour[what][g_Mapcount - 1] = cmax;
    RXFREE(nvalue);
    HC_Close_Geometry();
    
    break;
   
  case 11:
  case 14: // UP strain
  case 15:
  case 16: 
  case 17: 
  case 18: 
  case 19: 
  case 20:  
  case 21: //plyminX,
  case 22: // plymaxX,
  case 23:// plyminY,
  case 24:// plymaxY
  case 25:// plymaxXY
	assert(tridata);
  switch(what) {
  	case 11:	{// creasing
    		//float ex,ey,exy;

   		HC_Open_Geometry(theShell);
    		j=0;
    		data = tridata;
    		vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
    
    		for(i=0;i<ntri;i++,vp++,data++)  {
       			*vp =  data->m_cre;
    		} 
 		break;
		}
  	case 14:	{ // UP strain
    		float ex,ey,exy;
   		HC_Open_Geometry(theShell);
    		j=0;
    		data = tridata;
    		vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
    
    		for(i=0;i<ntri;i++,vp++,data++)  {
      			ex = data->m_eps[0];
      			ey = data->m_eps[1];
      			exy = data->m_eps[2];
      			*vp =  0.5*((ex+ey) + sqrt((ex-ey)*(ex-ey) + exy*exy));
    		} 
 		break;
		}
 	case 16:	{ // lp strain
    		float ex,ey,exy;
    		HC_Open_Geometry(theShell);
    		j=0;
    		data = tridata;
    		vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
    
    		for(i=0;i<ntri;i++,vp++,data++)  {
      			ex = data->m_eps[0];
      			ey = data->m_eps[1];
      			exy = data->m_eps[2];
      			*vp =  0.5*((ex+ey) - sqrt((ex-ey)*(ex-ey) + exy*exy));
    		} 
		break;
		}
 	case 15:	 /* cout<< " Generating   UP Stiffness \n"<<endl; */
    		HC_Open_Geometry(theShell);
    		j=0;
    		data = tridata;
    		vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
    
    		for(i=0;i<ntri;i++,vp++,data++)  {
        		*vp =  data->m_Princ_Stiff;
    		} 
		break;
 	case 17:	 // thk
    		HC_Open_Geometry(theShell);
    		j=0;
    		data = tridata;
    		vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
    
    		for(i=0;i<ntri;i++,vp++,data++)  {
        		*vp =  data->m_thk;
    		} 
		break;		
 	case 18:	 // ke
    		HC_Open_Geometry(theShell);
    		j=0;
    		data = tridata;
    		vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
    
    		for(i=0;i<ntri;i++,vp++,data++)  {
        		*vp =  data->ke;
    		} 
		break;	
	case 19:	 // kg
    		HC_Open_Geometry(theShell);
    		j=0;
    		data = tridata;
    		vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
    
    		for(i=0;i<ntri;i++,vp++,data++)  {
        		*vp =  data->kg;
    		} 
		break;	
	case 20:	 // nu
    		HC_Open_Geometry(theShell);
    		j=0;
    		data = tridata;
    		vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
    
    		for(i=0;i<ntri;i++,vp++,data++)  {
        		*vp =  data->nu;
    		} 
		break;	
	case 21:	 // ply min X
    		HC_Open_Geometry(theShell);
    		j=0;
    		data = tridata;
    		vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
    
    		for(i=0;i<ntri;i++,vp++,data++)  {
        		*vp =  data->plyminX;
    		} 
		break;	
	case 22:	 // ply max X
    		HC_Open_Geometry(theShell);
    		j=0;
    		data = tridata;
    		vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
    
    		for(i=0;i<ntri;i++,vp++,data++)  {
        		*vp =  data->plymaxX;
    		} 
		break;	
	case 23:	 // ply min Y
    		HC_Open_Geometry(theShell);
    		j=0;
    		data = tridata;
    		vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
    
    		for(i=0;i<ntri;i++,vp++,data++)  {
        		*vp =  data->plyminY;
    		} 
		break;	
	case 24:	 // ply max Y
    		HC_Open_Geometry(theShell);
    		j=0;
    		data = tridata;
    		vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
    
    		for(i=0;i<ntri;i++,vp++,data++)  {
        		*vp =  data->plymaxY;
    		} 
		break;	
	case 25:	 // ply max XY  
    		HC_Open_Geometry(theShell);
    		j=0;
    		data = tridata;
    		vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
    
    		for(i=0;i<ntri;i++,vp++,data++)  {
        		*vp =  data->plymaxXY;
    		} 
		break;			
		
	default :
		tvalue = NULL;
		assert(0);
	} // end switch local
// Next :    
    nvalue = (float*)MALLOC((nNod+2)*sizeof(float));
 
    ncol = g_Mapcount;
    
    if(!g_default_maxmin[what]) {
      if(simple) {
			make_triangle_values(sail->SailListIndex(),tvalue,ntri,ncol,nSDs,&cmin,&cmax);
			AKM_Mset_Face_colors_By_FIndex(theShell,"faces",0,ntri,tvalue);
      }
      else {
			if(cf_make_nodal_values(sail->SailListIndex(),tvalue,nvalue,&ncol,&nSDs,&cmin,&cmax))
				HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
      }
      g_ValMax[what] = cmax;
      g_ValMin[what] = cmin;
    }
    else {
      cmax = g_ValMax[what];
      cmin = g_ValMin[what];
      nSDs = -1; /* flag to say use the cmax/min values */
      if(cf_make_nodal_values(sail->SailListIndex(),tvalue,nvalue,&ncol,&nSDs,&cmin,&cmax))
  			HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
    }
    scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
    if(scale < MIN_SCALE) {
      cmax = cmax + MIN_SCALE/2;
      cmin = cmin - MIN_SCALE/2;
      scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
    }
    for(k=0,tval=cmin;tval<(cmax*(1+MIN_SCALE)) && k <= g_Mapcount;k++,tval+=scale) {
      g_RelaxColour[what][k] = tval;
    }
    g_RelaxColour[what][g_Mapcount - 1] = cmax;
    RXFREE(nvalue);
    RXFREE(tvalue);
    HC_Close_Geometry();
  //}
    break;
    
 
 case 15000:

    HC_Open_Geometry(theShell);
    nvalue = (float*)MALLOC((nNod+2)*sizeof(float));
     ncol = g_Mapcount;
    nSDs = 0.2;
	 ntodim = nNod+2;
    nvalue = (float*)MALLOC((ntodim+2)*sizeof(float));

	ierr=0;

	for(k=1; k<nNod; k++) {
		cf_get_gvalue(sail->SailListIndex (),k,&(nvalue[k]),&ierr);  //  may be out of synch
		assert(k<ntodim+2);
		if(ierr) break;
	}
    
    if(!g_default_maxmin[what]) {
      scale_nodal_values(nNod,nvalue,ncol,nSDs,&cmin,&cmax);
    }
    else {
      cmax = g_ValMax[what];
      cmin = g_ValMin[what];
      nSDs = -1; /* flag to say use the cmax/min values */
      scale_nodal_values(nNod,nvalue,ncol,nSDs,&cmin,&cmax);
    }
      cout<< " case 15000"<<endl;
    HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
    
    g_ValMax[what] = cmax;
    g_ValMin[what] = cmin;
    scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
    if(scale < MIN_SCALE) {
      cmax = cmax + MIN_SCALE/2;
      cmin = cmin - MIN_SCALE/2;
      scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
    }
    for(k=0,tval=cmin;tval<(cmax*(1+MIN_SCALE)) && k <= g_Mapcount;k++,tval+=scale) {
      g_RelaxColour[what][k] = tval;
    }
    g_RelaxColour[what][g_Mapcount - 1] = cmax;
    
    RXFREE(nvalue);
    HC_Close_Geometry();
    break;
    
  case 2000:  {	/* Displacement */ /* note: data at nodes not faces */
    RX_FESite *s;
    HC_Open_Geometry(theShell);

    nvalue = (float*)MALLOC((sail->m_Fnodes .size() +2)*sizeof(float));
    ncol = g_Mapcount;
    
    	for(vector<RX_FESite*>::iterator it=sail->m_Fnodes.begin();it!=sail->m_Fnodes.end();++it) {
		s = *it;

      if((s != NULL) && (s->GetN() >0)) {
		  	ON_3dVector d = s->Deflection();
			assert(i!=-5);
			nvalue[i-1] = d.Length ();    
      }
      else {
	nvalue[i-1] = 0.0 ;
      }
    }
    if(g_default_maxmin[what]) {
      scale_nodal_values(nNod,nvalue,ncol,nSDs,&cmin,&cmax);
    }
    else {
      cmax = g_ValMax[what];
      cmin = g_ValMin[what];
      nSDs = -1; /* flag to say use the cmax/min values */
      scale_nodal_values(nNod,nvalue,ncol,nSDs,&cmin,&cmax);
    }
  cout<< " case 2000"<<endl;
    HC_MSet_Vertex_Colors_By_FIndex(theShell,"faces",0,nNod,nvalue);
    
    g_ValMax[what] = cmax;
    g_ValMin[what] = cmin;
    scale = ((cmax-cmin)/((float)(g_Mapcount -1 )));
    if(scale < MIN_SCALE) {
      cmax = cmax + MIN_SCALE/2;
      cmin = cmin - MIN_SCALE/2;
      scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
    }
    for(k=0,tval=cmin;tval<(cmax*(1+MIN_SCALE)) && k <= g_Mapcount;k++,tval+=scale) {
      g_RelaxColour[what][k] = tval;
    }
    g_RelaxColour[what][g_Mapcount - 1] = cmax;
    RXFREE(nvalue);
    
  }
    HC_Close_Geometry();
    break;
    
  case 2001:	
    HC_Open_Geometry(theShell);
    
    vp = tvalue = (float*)MALLOC((ntri+2)*sizeof(float));
	for(vector<RX_FETri3*>::iterator ii = sail->m_FTri3s.begin()  ;ii != sail->m_FTri3s.end();ii++,vp++) { // for all in m_ftri3s
		FortranTriangle *te  = *ii;
		*vp = te->area;
    }
    nvalue = (float*)MALLOC((nNod+2)*sizeof(float));
    ncol = g_Mapcount;
    
   
    make_triangle_values(sail->SailListIndex(),tvalue,ntri,ncol,nSDs,&cmin,&cmax);
    AKM_Mset_Face_colors_By_FIndex(theShell,"faces",0,ntri,tvalue);
    g_ValMax[what] = cmax;
    g_ValMin[what] = cmin;
    scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
    if(scale < MIN_SCALE) {
      cmax = cmax + MIN_SCALE/2;
      cmin = cmin - MIN_SCALE/2;
      scale = ((cmax-cmin)/((float)(g_Mapcount -1)));
    }
    for(k=0,tval=cmin;tval<(cmax*(1+MIN_SCALE)) && k <= g_Mapcount;k++,tval+=scale) {
      g_RelaxColour[what][k] = tval;
    }
    
    RXFREE(nvalue);
    RXFREE(tvalue);
    HC_Close_Geometry();
    /* debug to see what interp shell is like */
    break;

  } /* end switch */
  return(j);
}//int Set_Shell_Data_Stress
 



