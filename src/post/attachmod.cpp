/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
June 1997
/include/various[/sailname]/ * are picked up 
Need proper organisation of the segment tree to get the
correct transformation matrices

 SubtractModel checks Hoops data root. Returns 0 if that was null
We then use that return value to skip the rest of the model clearing.
This is because there are TWO sail entities. in the boat. Deleting the second on  gave trouble
 * 24.6.96 test for non-zero key before deleting
 *     22/2/96 G angles removed. so now 15 shells
  *       9.2.95       plotpstrain call added 
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


#include "StdAfx.h" 
#include "RXGraphicCalls.h"
#include "RXSail.h" 
#include "add_hoops.h" // for listopen
#include "offset.h"
#include "entities.h"
#include "akmpretty.h"
#include "stringutils.h"
#include "RelaxWorld.h"
#include "attach.h"
#ifndef NO_PANSAIL
	#include "pansail.h"
	#include "panout.h"
#endif
#include "f90_to_c.h"
#include "doview.h"
#include "draweigenvectors.h"
#include "units.h"
#include "stripes.h"
#include "global_declarations.h"

#include "attachmod.h"


#define ADB (0)


static char gmask[] = "panSailDp";


/*  !!!!!!!!!!!!!!!! */
/*  NOTE : the SUM of above MUST be less than MAX_TOGGLES in add_frame.c */
/*  !!!!!!!!!!!!!!!! */

/* generates geometry etc. in the appropriate parts of the 
 * include library
 */

int FindWhat(const char*s) {
int i;

for(i=0;i<N_RELAX_SHELLS;i++) {
	if(strieq(s,g_relaxShell[i] ))
		return i;
	}
return -1;
}

int graphmask(const char *name) {
	if(stristr(gmask,name))
		return(1);
	else 
		return(0);
}

int colormask(char *name) {
	return(1);
}




#define PANSAIL_LINKED 1




int Hoops_PostProcess(class RXSail *sail , const int p_Wh)
{
 
// Create a model tree  under ?Include_His toric/models/<modelname>
// contains 'exclusive, non-exclusive,'''
// which is sdHOOPS_DataRoot

  

/* attach the sail named Sname and with model no. m to the boat & insert it
 * into all the view windows 
 It places the 
 
 1) exclusive
  If sd is the boat, we include ?Boat
 else {
 what &1 means  LoadPansailResults
  what &2 means call LoadSailResults
  }
  2) Non-exclusive
  ifwhat&2 {
  
 */

  int i ;
  char Hname[512],type[120],seg[256],  shortseg[64], buffer[512];
  char sz[512];
  HC_KEY theShell=0;
 
  
  int has_foot_wake=0;

  *Hname=*type=*seg=*shortseg=*buffer=*sz=0;
#ifdef NO_PANSAIL
  assert(N_RELAX_SHELLS  < MAX_TOGGLES);
#else
  assert(N_RELAX_SHELLS + N_PANSAIL_SHELLS < MAX_TOGGLES);
#endif

 if(ADB) printf("PostProcessOneModel with flag=%d  '%s' \n",p_Wh, sail->GetType().c_str());


//  if(wh & 1) means will do PANSAIL pics\n");
//  if(wh & 2) means will do RELAX pics

  	sprintf(Hname,"%s",sail->GetType ().c_str());
	PC_Strip_Leading(Hname);
	PC_Strip_Trailing(Hname);

	if(strlen(Hname) < 1) {
		 sail->OutputToClient("Addmodel called with bad Hname",2);
		return(0);
	}


  strrep(Hname,47,'+'); 		/* replace slashes with '+' */


      HC_Open_Segment_By_Key(sail->PostProcExclusive());
 	if(ADB) cout<< " exclusive..\n"<<endl;

	if(sail->GetType ()!="boat") 
	{ // not the boat .. first  deal with all the relax output 
	if(p_Wh & 2) {
  		for(i=0;i<N_RELAX_SHELLS;i++) {
  	 	 	sprintf(sz,"%s",g_relaxShell[i]);
			if(ADB) { printf(" go to load(1) %s\n",sz); fflush(stdout);}
	   	 	HC_Open_Segment(sz);
	       	 		theShell = sail->LoadSailResults(LR_DEFAULT_STATE,i,i==0);
	   	 	HC_Close_Segment();
			if(ADB) printf("loaded %s\n",sz);
			if(ADB) HC_Update_Display();
		}
	} // end wh&2
	if(p_Wh & 1) { // then all the pansail stuff 
#ifndef NO_PANSAIL	
       		if(PANSAIL_LINKED) {	/* allows us to cut out pansail for michel etc. */
  			for(i=0;i<N_PANSAIL_SHELLS;i++) {
  	  			sprintf(sz,"%s",g_pansailShell[i]);
  				if(ADB) printf(" go to load (2) %s\n",sz); 
	    			HC_Open_Segment(sz);
					LoadPansailResults(sail,i,
					sail->GetLoadingIndex(),
					sail->GetFlag(FL_HAS_AERODATA),
					g_PansailDone);
	    			HC_Close_Segment();
			}
       		} 
#endif		
	}// end wh&1		
	} // end if not boat 
HC_Close_Segment();  /* "exclusive"  */


HC_Open_Segment_By_Key(sail->PostProcNonExclusive());
	if(p_Wh &2) { /* do things associated with stress analysis */
		HC_Open_Segment("Battens");
			HC_Flush_Contents(".","style");			
			HC_Style_Segment_By_Key(sail->PostProcTransformSeg());
			HC_Set_Visibility("faces=on");
		HC_Close_Segment();

		HC_Open_Segment("strings");
			HC_Flush_Contents(".","style");	  
			//HC_Style_Segment_By_Key(sail->PostProcTransformSeg());			

		HC_Close_Segment();

		HC_Open_Segment("residuals");
		HC_Close_Segment();

		HC_Open_Segment("node numbers");
		  	if(sail->Hoops_Nodes && (check_for_include_by_key(sail->Hoops_Nodes) == 0)) {
		    		if(stristr(type,"segment"))
                                  	if(!sail->Hoops_Nodes) cout<< "include a NULL key (NODES)\n"<<endl;
		    		if(sail->Hoops_Nodes) HC_Include_Segment_By_Key(sail->Hoops_Nodes);
				//else cout<< "hoopsnodes zero"<<endl;
		  	}
		HC_Close_Segment();

		HC_Open_Segment("Element Numbers");
		  if(check_for_include_by_key(sail->Hoops_Nodes) == 0) {
		   if(g_ArgDebug) printf("sd-Hoops Nodes key type = '%s'\n",type);
		    if(stristr(type,"segment"))
                                  if(!sail->Hoops_Nodes) cout<< "include a NULL key (NODES)\n"<<endl;
		   if(sail->Hoops_Nodes) HC_Include_Segment_By_Key(sail->Hoops_Nodes);
		//else cout<< "hoopsnodes zero"<<endl;
		  }
		HC_Close_Segment();
		if(ADB) HC_Update_Display();
if(1) { // try the 'various' here
//		 Now the new (from june 97) method. Put anything you want to display in /include/various */

		//Draw_Material_Eigenvectors(sd->isail,RX_EVS+RX_EV_TENSORFORM);
#ifdef HOOPS
		sprintf(buffer,"/include/various/%s/*",Hname);
		assert(strlen(buffer) < 511);
		HC_Begin_Segment_Search(buffer);
		while(HC_Find_Segment(seg)) {
			if(HC_Parse_String(seg,"/",-1,shortseg)){ // this can stay
				HC_Open_Segment(shortseg);
					HC_Flush_Contents(".","include");
					HC_Include_Segment(seg);
					if(ADB) HC_Update_Display();
				HC_Close_Segment();
			}
		}
		HC_End_Segment_Search();
#endif
// April 2003. putting things into /include/various_Transform/<model>/* means they get included and transformed.

		sprintf(buffer,"/include/various_Transform/%s/*",Hname);
#ifdef HOOPS
		HC_Begin_Segment_Search(buffer);
		while(HC_Find_Segment(seg)) {
			if(HC_Parse_String(seg,"/",-1,shortseg)){ // this can stay
				HC_Open_Segment(shortseg);
					HC_Flush_Contents(".","include");
					HC_Include_Segment(seg);
		  			cout<<__FILE__ <<"Not setting ModMat in various_transform"<<endl;
		  		/*	for(i=0;i<4;i++) {
					for(j=0;j<4;j++) {
						mx[i][j] = off->mt[model][j][i];
					}
		  			}
		        		HC_Set_Modelling_Matrix(&(mx[0][0]));*/
					//HC_Style_Segment_By_Key(  PostProcTransformSeg(sd->isail));
				HC_Close_Segment();
			}
		}
		HC_End_Segment_Search();
#endif
}// if(0);



} /* end of if wh & 2 */
		HC_Open_Segment("Panel Edges");
		HC_Close_Segment();
		if(!sail->IsBoat()) {  
		if(p_Wh &2) {
			FILE *fp; string buf;
#ifdef _WINDOWS
			buf=string(g_workingDir) + string("\\stripe_") + sail->GetType()+string(".txt");
#else
			buf=string(g_workingDir) + string("/stripe_") + sail->GetType()+string(".txt");
#endif
			fp=FOPEN(buf.c_str(),"w");
			if(fp) {
				HC_Open_Segment("Stripes");
				int nstripes = Compute_All_Stripes(sail,fp);
				HC_Close_Segment();
				FCLOSE(fp); 
				if(!nstripes) {
#ifdef linux
					std::string s = string("rm  ")+string(buf);  
					system(s.c_str());
#else
					std::string s = string("del \"")+string(buf)+string("\"");  
					system(s.c_str());
#endif
				}
				if(ADB) HC_Update_Display();
			}
			else {
				sail->OutputToClient(buf +"  cannot be opened" ,2);
			}
		HC_Open_Segment("Stripe_Overlay");
			char l_buf[512];
			HC_Show_One_Net_User_Option("inc_list",l_buf);
			if (stristr(l_buf,"stripe_overlay")){
				printf("TODO:  create stripe overlay on inc_list<%s>\n",l_buf);
				HC_Show_Pathname_Expansion("..",l_buf);
			}
		HC_Close_Segment();

		HC_Open_Segment("Panel Edges");
	    		// see MSW version. 
		HC_Close_Segment();

		if(ADB) HC_Update_Display();
#ifndef _WINDOWS
		HC_Open_Segment("Vector Material");
			HC_Flush_Contents(".","geometry,subsegment");	
#ifdef FORTRANLINKED
	    		cf_plot_principal_stiff(sail->SailListIndex()); 
#endif
		HC_Close_Segment();
		if(ADB) HC_Update_Display();
		if(ADB) {cout<< "Vector warp "<<endl; fflush(stdout);}
		HC_Open_Segment("Vector Warp");
			HC_Flush_Contents(".","geometry,subsegment");
#ifdef FORTRANLINKED
	    		cf_plot_ref_direction(sail->SailListIndex()); 
#endif
		HC_Close_Segment();
		if(ADB) HC_Update_Display();
		HC_KEY kk =HC_KOpen_Segment("Vector Stress");
			HC_Flush_Contents(".","geometry,subsegment");
			if(PC_BeingLookedAt(".")){
	    		  sail->DrawPlotps( kk);   
			}
		HC_Close_Segment();
		if(ADB) HC_Update_Display();
		HC_Open_Segment("Vector Strain");
			HC_Flush_Contents(".","geometry,subsegment");
#ifdef FORTRANLINKED
	    		   cf_plotpstrain(sail->SailListIndex()); 
#endif
		HC_Close_Segment();
#endif
		if(ADB) HC_Update_Display();
}

#ifndef NO_PANSAIL
if(p_Wh &1) {
		HC_Open_Segment("Wakes");
			HC_Flush_Geometry("."); //wakes
			HC_Flush_Contents(".", "everything"); //wakes
			HC_Set_Visibility("edges=mesh quads only");
			if(1 || PC_BeingLookedAt(".")){

			  has_foot_wake = 1;	if(ADB) {cout<< "go into Insert_Pansail_Wake"<<endl; fflush(stdout);}
			HC_Flush_Contents(".","modelling matrix"); HC_Rotate_Object( 0.0,0.0, -g_AWA*57.29577951);
			Insert_Pansail_Wake(sail->GetLoadingIndex(),has_foot_wake);
	if(ADB) {cout<< "back from Insert_Pansail_Wake"<<endl; fflush(stdout);}
			}
		HC_Close_Segment();
}
#endif
	
	} // if is not boat
	 if(ADB) printf("  ListOpen LNG  = %d\n",List_Open_Segments());

     if(ADB) HC_Update_Display();
	 if(!g_Janet){
      HC_Open_Segment("values");
	  if(sail->GetType ()=="boat")  {  
  			sprintf(sz,"%s",g_relaxShell[0]);
  			HC_Open_Segment(sz);
			  HC_Open_Segment("shell");
				/* dummy */
			  HC_Close_Segment();
			HC_Close_Segment();
			if(ADB) HC_Update_Display();
	  }
	  else {
if(p_Wh &2) {
  		for(i=0;i<N_RELAX_SHELLS;i++) {
  			sprintf(sz,"%s",g_relaxShell[i]);
			HC_Open_Segment(sz);
			  HC_Open_Segment("shell");

				LoadSailValues(sail, LR_DEFAULT_STATE,i,i==0); 
			  HC_Close_Segment();
			HC_Close_Segment();
			if(ADB) HC_Update_Display();
		 }
}
if(p_Wh &1) {
#ifndef NO_PANSAIL
  		for(i=0;i<N_PANSAIL_SHELLS;i++) {
  			sprintf(sz,"%s",g_pansailShell[i]);
			HC_Open_Segment(sz);
			  HC_Open_Segment("shell");
				LoadPansailValues(sail,i,sail->GetLoadingIndex(),
				sail->GetFlag(FL_HAS_AERODATA),g_PansailDone);
			  HC_Close_Segment();
			  HC_Open_Segment("graph");
				if(graphmask(g_pansailShell[i]))
					LoadPansailAxes(sail,i,sail->GetLoadingIndex(),
					sail->GetFlag(FL_HAS_AERODATA),g_PansailDone);
			  HC_Close_Segment();
			HC_Close_Segment();
			if(ADB) HC_Update_Display();
		 }
#endif
		}
	}

	   HC_Close_Segment(); /* values */
	  }// not janet
	HC_Close_Segment(); /* Hname */

if(ADB) {printf("  ListOpen at end of PPOM  = %d\n",List_Open_Segments());
 HC_Update_Display();
 cout<< "Leaving PostProcessOneModel"<<endl; fflush(stdout);}
return(0);
}


HC_KEY check_for_include(const char *name) 
{
/* checks open hoops segment (not below) for an
 * include link to segment 'name'
 */
HC_KEY key;
char buf[128],fname[256];

HC_Show_Pathname_Expansion(name,fname);
assert(strlen(fname) < 128);
if(ADB) printf(" check_for_include on <%s>\n(%s)\t....",name,fname);
HC_Begin_Contents_Search(".","include");
	while(HC_Find_Contents(buf,&key)) {
		HC_Show_Include_Segment(key,buf);
		if(strieq(fname,buf)) {
			if(ADB) cout<< "FOUND\n"<<endl;
			return(key);
			}
	}
HC_End_Contents_Search();
if(ADB) cout<< "NOT found\n"<<endl;
return(0);  /* not found */
}

HC_KEY check_for_include_by_key(HC_KEY kseg) 
{
/* checks open hoops segment (not below) for an
 * include link to segment  with  Hoops key 'kseg'
 */
HC_KEY key;
char buf[128];

#ifdef HOOPS
HC_Begin_Contents_Search(".","include");
	while(HC_Find_Contents(buf,&key)) {
		if (HC_KShow_Include_Segment(key) == kseg) {
			return(key);
		}
	}
HC_End_Contents_Search();
#endif
return(0);  // not found 
}

int check_open(const char *hdr) 
{
char segment[512];
int found=0;
if(!g_ArgDebug) return(1);
#ifdef HOOPS
  	HC_Begin_Open_Segment_Search();
		while(HC_Find_Open_Segment(segment)) {
		  if(!found) {
			found++;
			  if(ADB) printf("check_open CALLED FROM :%s\n",hdr);
		  	  if(ADB) cout<< "Currently Open Segments :\n"<<endl;
		  }
			fprintf(stdout,"Found open segment '%s'\n",segment);
		}
	HC_End_Open_Segment_Search();
	return 0;
#else
return 1;
#endif
}

