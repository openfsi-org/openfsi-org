/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *  14/3/97  Thanks to HP we found that Area was variously float and double
 *  28.12.94		ignore first 5 an last 5 percentiles. 
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */
 
#include "StdAfx.h" 


#include "global_declarations.h"

#include "trivals.h"

/* NOTE: this routine takes Fmodel = Fortran model.
   This starts from 1 so must -1 to get to C indexing !! 
 */
 struct  percentile_list {
	  double v;	 /* value */
	  int i;	/* original index */
	  double w; /* weight */
 } ;
int Percent_Sort_Fn( const void *a,const void *b) {

struct percentile_list *ia = ( struct percentile_list *) a;
struct percentile_list *ib = ( struct percentile_list *) b;
	   if(ia->v > ib->v) return(1);
	   if(ia->v < ib->v) return(-1);
	   return(0);
 }
int Percentile_Limits (float *tvalue,int n,double *p_areas,float p1,float p2,float* themin, float*themax, double *Area ) {
 /* 
	returns themin and themax being the values of the p1th and p2th percentiles
 */
  int i,start;
  /* copy the values and areas */
   double total,sum;
 	float *vp;
	 struct percentile_list *thelist;

	 thelist= (struct percentile_list *)MALLOC( (n+2)*sizeof(struct percentile_list));
	 if(!thelist) return(0); 
	 vp = tvalue;
	 
	 total=0.0; 
	 if(p_areas) {
			for(i=0;i<n;i++,vp++,i++) { // why on earth two i++'s
 				thelist[i].i=i;
				thelist[i].w= p_areas[i]; 
				thelist[i].v= (*vp);
				total = total +  (*vp) * thelist[i].w;
			}
	 }
	 else { // no areas
			for(i=0;i<n;i++,vp++,i++) { 
 				thelist[i].i=i;
				thelist[i].w=1.;
				thelist[i].v= (*vp);
				total = total +  (*vp);
			}
	 }

/* sort the entries by value */
	qsort(thelist,n,sizeof( struct percentile_list),Percent_Sort_Fn );

  /* sum (value*area) from the bottom
     assign a percentile number to each entry */

	 sum=0.0;
	 start = 1;
	 for(i=0;i<n;i++) {
	  sum = sum + thelist[i].v*thelist[i].w;
	  if(sum > p1*total && start){
	  	 *themin = thelist[i].v;
		 start = 0;
		 }
		 if(sum > p2*total){
		 if(i)
	  	    *themax = thelist[i-1].v;
		 else 
		    *themax = thelist[i].v;
		 break;
		 }
	 }
	RXFREE(thelist);
	return(1);
}
/* the call 
n =  off->nte[m]- off->nts[m] + 1
Percentile_Limits (m,tvalue,n,&(tricom->area[off->nts[m]-1] ),(float)0.05,(float) 0.95,&themin, &themax, &Area );
 */
int make_triangle_values(const int sli,float *tvalue,const int ntval,const int ncol,const float nSDs,float *cmin,float *cmax)
{

int i, ne;
float mapcount;
float *vp;
float themax,themin,scale,SD,mean;
double Area;
double *areaRoot=0, *a_ptr;
Area=0;

// printf("make_triangle_values( sli =%d,int ntval=%d,int ncol=%d, float nSDs=%f)\n", sli,ntval,ncol,nSDs);
if(nSDs > 0) {
  mean = 0.0;
  vp = tvalue;
  themax=themin = *vp;
  for(i=0;i<ntval;i++,vp++) {
    themax = max(themax,*vp);
    themin = min(themin,*vp);
  }
  
  
  /* cut off the first and last */
  ne =  ntval;
  //printf(" before percentile themin %f themax %f\n",themin,themax);

  Percentile_Limits(tvalue,ne,areaRoot,(float)0.05,(float) 0.95,&themin, &themax, &Area );
  //printf(" After  percentile themin %f themax %f\n",themin,themax);
  
  Area = 0.0;
  mean=0.0;
 if(areaRoot) {
	  for(i=0, vp = tvalue ,a_ptr=areaRoot;i<ntval;i++,vp++,a_ptr++) {
	    if((*vp) >= themin && (*vp)	<=themax) {
	      Area += (*a_ptr);
	      mean += (*vp)*(*a_ptr);
	    }	
	  }
 }
else {
	  for(i=0, vp = tvalue ;i<ntval;i++,vp++) {
	    if((*vp) >= themin && (*vp)	<=themax) {
	      Area += 1.0;
	      mean += (*vp);
	    }	
	  }
}

  if(Area < 1e-6)
    Area = 1.0;
  mean = mean/Area;
  
  SD=0.0;
  vp = tvalue;
 if(areaRoot ) {
   for(i=0, vp = tvalue ,a_ptr=areaRoot;i<ntval;i++,vp++,a_ptr++) {
    if((*vp) >= themin && (*vp)	<=themax) {
      SD += (*vp - mean)*(*vp - mean)*(*a_ptr);
    }
  }
}
else{
   for(i=0, vp = tvalue ;i<ntval;i++,vp++) {
    if((*vp) >= themin && (*vp)	<=themax) {
      SD += (*vp - mean)*(*vp - mean);
    }
  }
}

  SD = SD/Area;						   /* standard deviation */
  SD = pow(SD,(float)0.5);
  
  themax = mean + nSDs*SD;
  themin = mean - nSDs*SD;
}
else {
  themax = *cmax;
  themin = *cmin;
}

mapcount = (float) g_Mapcount;     /*MAXCOLOURS;*/
 if(mapcount <= 0.0) mapcount = 4.0;
 scale = (themax-themin)/((mapcount-1));
 if(scale < 1e-4) scale = 1.0;

*cmin = themin;
*cmax = themax;

vp = tvalue;
for(i=0;i<ntval;i++,vp++) {
 	*vp = ((*vp)-themin)/scale;
	if(*vp < 0 ) (*vp) = (float)0.00001;
    	if(*vp > mapcount) *vp = ((float)mapcount - 1.0001);
}

return(0);
}

int scale_nodal_values(int nnod,float *nvalue, int ncol, float nSDs,float *cmin,float *cmax)
{

int i;
float mapcount;
float *nv;
float themax,themin,scale,SD,mean;

if(nSDs > 0) {
mean = 0.0;
themax=themin = *nvalue;

for(i=0,nv = nvalue;i<nnod;i++,nv++) {
	mean += *nv;
	themax = max(themax,*nv);
	themin = min(themin,*nv);
}

mean = mean / ((float)nnod);
SD=0.0;
for(i=0,nv = nvalue;i<nnod;i++,nv++) {
	SD += (*nv - mean)*(*nv - mean);
}
SD = SD;						   /* standard deviation */
SD = pow(SD,(float)0.5);

 themax = mean + nSDs*SD;
 themin = mean - nSDs*SD;
}
else {
  themax = *cmax;
  themin = *cmin;
}

mapcount = (float) g_Mapcount;     /*MAXCOLOURS;*/
 assert(mapcount > 0.0) ; 
 scale = (themax-themin)/((mapcount-1));
 if(scale < 1e-4) scale = 1.0;

*cmin = themin;
*cmax = themax;

for(i=0,nv = nvalue;i<nnod;i++,nv++) {
 	*nv = ((*nv)-themin)/scale; *nv=max(*nv,(float)0.00001  );
	if(*nv >= mapcount) *nv = ((float)mapcount - 1.0001);/* Peter Jan 98 '=' */
}


return(0);
}



int AKM_Mset_Face_colors_By_FIndex(HC_KEY theShell,const char *type,int offset,int nfaces,float *tvalue)
{
float *vp;
int i;
#ifdef HOOPS
vp = tvalue;
HC_Open_Geometry(theShell);
	for(i=offset;i<nfaces;i++,vp++) {
		HC_Open_Face(i);
			HC_Set_Color_By_FIndex(type,*vp);
		HC_Close_Face();
	}
HC_Close_Geometry();
#endif
return(0);
}




