
#include "StdAfx.h" // added linux port 

#include "RXSail.h"
#include "hoopcall.h"
 
#ifdef _X
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/StringDefs.h>
#include <X11/Intrinsic.h>
#include <X11/IntrinsicP.h>
#include <X11/Shell.h>

#include <Xm/Xm.h>
#include <Xm/XmP.h>
#endif

#include "GraphicStruct.h"
#include "hoopsw.h"  /* Hoops widget */
#include "hc.h"
#include "entities.h"
#include "parse.h" // for boat
#include "AttachHoops.h"
#include "eh_rotate.h"
#include "eh_zoom_in.h"
#include "MakeNewDrawingArea.h"

#include "savesailas.h"
#include "script.h"
#include "global_declarations.h"

#include "global_declarations.h"

#include "add_hoops.h"
//#include "debugspecial.h" 
// #define DEBUG (1)

/* test_event:

 * This is a filter applied to all events in the X queue.  It determines if
 * the event is worthy of interrupting HOOPS.  If an event is worthy, we
 * queue a special event.  This will force HOOPS to abort its current
 * update cycle.  This is a very simple filter.  A real application would
 * more severely restrict which events should stop an update in progress
 * (like if the "Q" key was hit or if a ButtonPress in the X window of a
 * Quit button widget happened).
 */
#ifdef EVENT_CHEKER
static Bool test_event (
    Display             *display,
    XEvent              *event,
    char                *type) {
    XAnyEvent           *xany = &event->xany;

    if ((event->type == ConfigureNotify && xany->window == hoops_window) ||
        (event->type == Expose && xany->window == hoops_window) ||
        event->type == ButtonPress ||
        event->type == KeyPress) {
        /* This puts a special event on the HOOPS queue which will interrupt any
         * update in progress.
         */
        HC_Queue_Special_Event ("foo", "foo");
      }

    return (False); /* leave the event on the queue no matter what */
  }


/* event_checker:

 *  This is the HOOPS event checker.  It will apply the filter routine
 * "test_event" to all events on the X event queue and queue a special HOOPS
 * event for "interrupt worthy" events.  The special event will cause HOOPS to
 * interrupt itself and give control back to the user (usually this means the
 * Update_Display will return prematurely).
 */
static void event_checker (void) {
    XEvent      xevent;

    XCheckIfEvent (display, &xevent, test_event, "");

    return;
  }

#endif

/* graphics_finish:

 * gfinish callbacks are called just before the widget is destroyed.
 * deletes all geometry (which allows HOOPS to close down faster).
 */
//dont#define LISTOPEN
int List_Open_Segments(){

char seg[256]; int i=0;
HC_Begin_Open_Segment_Search();
	while(HC_Find_Open_Segment(seg)) {
#ifdef LISTOPEN
		printf("open %s\n", seg); 
#endif
		i++;
		}
HC_End_Open_Segment_Search();	
return (i);
} 

int Print_UO_Tree(int*depth, char*seg) {
char buf[2048],head[256],type[64],s[512];
int k; long key;
(*depth)++;
for(k=0;k<(*depth);k++) head[k]='\t'; 
head[(*depth)]=0;


HC_Open_Segment(seg);
if(HC_Show_Existence("user options")) {
printf("%s recur into\n %ssegment %s\n",head,head,seg);
HC_Show_User_Options(buf);

printf("%s driver UOs are\n%s<%s>\n",head,head, buf);
} 

HC_Begin_Contents_Search(".","subsegment,include");
	while(HC_Find_Contents(type,&key)){
		if(strieq(type,"segment")) {
			HC_Show_Segment(key,s);
			Print_UO_Tree(depth, s);
		}
		else	if(strieq(type,"include")) {
			 printf("%stype is %s\n",head,type);
			HC_Show_Include_Segment(key,s);	
			printf("%sincludee is %s\n",head,s); 

		}
		else printf(" type is %s\n",type);

	}
	HC_End_Contents_Search();

HC_Close_Segment();
(*depth)--;
return 1;
}
/*
int Print_Finish(Graphic *g) {
int depth=0;
    if( g->hoops_driver&& HC_QShow_Existence(g->hoops_driver,"self")) {
	char buf[2048];
	List_Open_Segments();

	HC_Open_Segment_By_Key( g->baseSeg);
		HC_Show_Pathname_Expansion(".", buf);
		printf("BaseSeg is %s\n", buf);
		Print_UO_Tree(&depth, buf);
	HC_Close_Segment();
	printf("filename %s\n", g->filename);
	printf("defaultShell %d\n", g->defaultShell);
   
	}
return 1;
}*/
void NeverUsedCB_graphics_enter (
    Widget              widget,
    XtPointer           client_data,
    XtPointer           call_data) {
//   Graphic *g = (Graphic *) client_data;

//	if(!current Graph) current Graph=g;
	
//	if(!current Graph) {
//		error(" setting NULL CG to NULL", 3);
//		return;
//	}
//	if(current Graph->EMPTY) error("DANGER current Graph empty",4);
}

void CB_graphics_leave (
    Widget              widget,
    XtPointer           client_data,
    XtPointer           call_data) {

}

void CB_graphics_finish ( // Usually, the sail is already destroyed before we get here
    Widget              widget,
    XtPointer           client_data,
    XtPointer           call_data) {
	char buf[256];
    	Graphic *g = (Graphic *) client_data;
	SAIL *sail=0;
		
	 if(g && g->GSail) 
	 	sail = g->GSail;

      	if(sail && sail->Needs_Saving()) { 
		if(g->GSail->IsBoat())
			sprintf(buf,"SaveAs:  : %s/*.in",g_currentDir);
		else
			sprintf(buf,"SaveAs:  : %s/*.bag",g_currentDir);
		 Execute_Script_Line(buf,g);
 		}

	if(g == g_selectGraph) 
		g_selectGraph=NULL;


   	if(g->hoops_driver) { 
		//printf("Delete hoops_Driver > ");
		//puts(g->hoops_driver);
 		if(HC_QShow_Existence(g->hoops_driver,"self")) {
			//printf("Deleting driver segment <%s>\n\n",g->hoops_driver);
			//Print_Finish(g);  
			HUtility::CloseAllSegments();
     			//puts("sept 06 DONT	HC_Delete_Segment (g->hoops_driver)");
		}
		//else 
			//printf(" driver seg doesnt exist\n");
	}
//	else 
//		puts("hoops driver NULL");

	g->EMPTY = 1; 	g->topLevel=NULL;
	return;
  }


/* graphics_init:

 * This is the ginit callback.  It is meant for graphics device specific things a user
 * may want to do.  It is called right after the widget has been realized.
 */
void CB_graphics_init (
			 Widget              _w,
			 XtPointer           client_data,
			 XtPointer           call_data) {
       Graphic *g = (Graphic *) client_data;
    
         HT_Widget			w = (HT_Widget) _w;  

       
       /*  Flush the display so that the parameters passed into HOOPS are
	*  correct.
	*/
       XFlush(XtDisplay(g->topLevel));
	//Print_Graphic("CB_graphics_init ",g);
	//printf(" hoops driver is %s\n", g->hoops_driver);

    	HC_Open_Segment_By_Key( w->hoops.m_pHView->GetViewKey() );	
 	 	char buf[256];
   		HC_Show_Pathname_Expansion(".",buf);
		//printf(" m_pHView->GetViewKey() = %s\n",buf);
	 	HC_Set_Heuristics ("partial erase");
	 	init_hoops_view(_w,g,g->basename);  /* add the required Hoops structure to the hierarchy */
       HC_Close_Segment();
} // CB_graphics_init


/* this redirects CB appropriately and deals with down, motion down and up */

void CB_button (
    Widget                      w,
    XtPointer   *               client_data,
    HT_Widget_CallbackStruct *  call_data) {
       Graphic *g = (Graphic *) client_data;
       unsigned int               button;

 
    /*
     *  Check to see if we have a motion event with no buttons pushed.
     *  If so, exit;
     */
       
       switch(call_data->reason) {
       case HTCR_BTNMOTION : {
	 XMotionEvent *  motion_event = (XMotionEvent *) call_data->event;
	 if(motion_event->state & Button1Mask) {
	   if(g->ZoomFlag)
	     eh_zoom_in(w,client_data,(HT_Widget_CallbackStruct * )call_data); 
	 }
	 else if(motion_event->state & Button2Mask) {
	   	eh_rotate(w,client_data,( XmAnyCallbackStruct *)call_data); 
	 }
	else if(motion_event->state & Button3Mask) {  
	//	puts(" One day here is the contect-sensitive popup(Motion)");
	//   	eh_rotate(w,client_data,( XmAnyCallbackStruct *)call_data); 
	 }
       }
	 break;
       case HTCR_BTNDOWN:
       case HTCR_BTNUP: 
	 button = call_data->event->xbutton.button;
	 switch(button) {
	 case Button1:
	   if(g->ZoomFlag) {
	     eh_zoom_in(w,client_data,call_data); 
	   }
	   else if(HTCR_BTNDOWN == call_data->reason) {
	     select_routine(w,client_data,call_data);
	   }
	   break;
	 case Button2:	// March 2003
	   eh_rotate(w,client_data,( XmAnyCallbackStruct *)call_data); 
	   break;
	 case Button3:
		puts(" One day here is the contect-sensitive popupBTNDOWN|UP");
	 }  /* end button switch */
	 break;
	 
       } /* end event type switch */

}
      

HBaseModel*  gt_hbm; 

Widget create_hoops_widget (
    Graphic *p_g,
    Widget p_parent,
    HBaseModel* p_Phbm) {

    /*
     *  Add an instance of the HOOPS widget to the Motif hierarchy
     HBaseModel* p_Phbm  may have a value or may be NULL.
     If it's null the widget must create a model.
     Otherwise we pass it via gt_hbm   
     
     */
  Widget hoops_widget;
  static   Boolean  defaultPicture=FALSE;
  static Boolean use_colormap = FALSE;
  static Colormap cmap=0;

    use_colormap = TRUE;
    gt_hbm = p_Phbm;
   hoops_widget = XtVaCreateWidget ("hoops_widget",
	htWidgetClass, /* our class*/	p_parent, /* parent widget */
    /* HOOPS Widget resources */
	XmNheight,			400,
	XmNwidth,			400,
        HTNhoopsDriver,			NULL,

    /* do not make a window for HOOPS_PICTURE environment variable */
        HTNallowDefaultPicture,		False,

	HTNuseColormap,			use_colormap,

    /* set the routine which will queue special events when updates should be aborted */
    //    HTNinterruptUpdateFilter,	event_checker,

    /* set the double buffering boolean */
    //    HTNdoubleBuffering, 		double_buffering,

    /* primitive resources */
        XmNhighlightOnEnter,		True,
        XmNtraversalOn,			True,
// peter added
        XmNbottomAttachment,            XmATTACH_FORM,
        XmNrightAttachment,            XmATTACH_FORM,
        XmNleftAttachment,            XmATTACH_FORM,
        XmNtopAttachment,            XmATTACH_FORM , 	NULL);  
	
 /* adams 
    hoops_widget = XtVaCreateWidget ("hoops_widget",
        htWidgetClass, // our class   parent, // parent widget  
        HTNhoopsDriver,                 driver,
        HTNallowDefaultPicture,         defaultPicture,
//       HTNinterruptUpdateFilter,       event_checker,  
        HTNuseColormap,                 use_colormap,
        HTNuseColormapID,                 cmap,
	HTNdoubleBuffering, 		"True",
        XmNbottomAttachment,            XmATTACH_FORM,
        XmNrightAttachment,            XmATTACH_FORM,
        XmNleftAttachment,            XmATTACH_FORM,
        XmNtopAttachment,            XmATTACH_FORM,
	XmNwidth,			500,
	XmNheight,			400,

	XmNuserData, 			g,
        XmNhighlightOnEnter,            False,
        XmNtraversalOn,                 True,
        NULL); */

/*    Not needed usually */
/*
    XtAddCallback (hoops_widget, HTNexposeCallback, (XtCallbackProc) CB_Update, g);
    XtAddCallback (hoops_widget, HTNresizeCallback, (XtCallbackProc) CB_Update, g);
*/

    XtAddCallback (hoops_widget, HTNginitCallback, (XtCallbackProc) CB_graphics_init,p_g);

    XtAddCallback (hoops_widget, HTNbtnMotionCallback, (XtCallbackProc) CB_button,p_g);
    XtAddCallback (hoops_widget, HTNbtnDownCallback, (XtCallbackProc) CB_button,p_g);
    XtAddCallback (hoops_widget, HTNbtnUpCallback, (XtCallbackProc) CB_button, p_g);

    XtAddCallback (hoops_widget, HTNgfinishCallback, (XtCallbackProc) CB_graphics_finish,p_g);
 

    /* These are other callbacks that can be used - maybe for changing a cursor, etc.  THEY WORK */
 //   XtAddCallback (hoops_widget, HTNenterCallback, (XtCallbackProc) CB_graphics_enter,g);
 //   XtAddCallback (hoops_widget, HTNleaveCallback, (XtCallbackProc) CB_graphics_leave,g);


    XtManageChild(hoops_widget);

/* now find the colormap just set */
    return(hoops_widget);
}
