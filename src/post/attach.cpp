 /* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
    7.10.97 cplus comments removed 
   9.9.97   Start Peters_Generate_Geometry. Does
		velocities
  20.7.97    leak repairs 
  *   11/11/96  Check Sails now turns OFF DATA HAS CHANGED
  *   1/6/96  GLobal c by face interrogated correctly
  * 21/2/96 singo unpacking corrected
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


/* Object code to deal with all the associated attached objects */



#include "StdAfx.h" 
#include "RXGraphicCalls.h"
#include "RXSail.h"
#include "RelaxWorld.h"

#include "attachmod.h"
#include "units.h"
#include "getshell.h"
#include "akmpretty.h"
#include "drawforces.h"

#ifndef NO_PANSAIL
#include "panout.h"
#include "velocity.h"
#endif

#include "cfromf.h"
#include "global_declarations.h"
#include "filament.h"
#include "etypes.h"

#include "GraphicStruct.h"
#include "draweigenvectors.h"
#include "finishview.h"
#ifdef HOOPS
#include "hoopcall.h"
#endif
#include "attach.h"
#define PCDEBUG (0)


int PC_Set_BeingLookedat(const char*sz){// returns 1 if the status has changed
	char buf[256];
	int ret = 1;

return ret;
} 
int PC_BeingLookedAt(const char*seg) {
	char buf[256];
	int ret; 
	return 1; // dec 2005 
}
int Post_Singo_Summary(int Pmodel) {
int SStart, soff,i;
float  *vv,*vc; 
float v, dv,VV,VC;  
char pline[24];
float mean=0,Height=0, sum_sdv=0, sum_dv=0, sum_s_dv_v=0, MS=0, SD=0;

RXSTRING  buf,buf2;

vv = &(g_Vv[g_noffset[Pmodel-1]] ) ; 			// + (Pmodel-1)]);   Checked Sept 2000 
vc = &(g_Vc[g_noffset[Pmodel-1]  - (Pmodel-1) ]);  	//WRONG 

/* g_Singo is packed with (g_N[model]-1) values for each model */
	strncpy(pline,g_pName[Pmodel-1],16);
	pline[16] = '\0';
	printf("%d '%s' LE Singularity ",Pmodel, pline);

/* first count thru earlier models */
 		soff=0;
		 for(i=1;i<Pmodel;i++) {
			 soff +=g_N[i-1]-1;
			}
SStart=soff;
/* now extract singo values with DV values 
 cout<< " i  ind\t   uv     \t   vv     \t  uc      \t  vc     \t   v     \t   dv   \t singo\n"<<endl; 
   */
		for(i=0;i<g_N[Pmodel-1]-1;i++,soff++) {

			//l_index =  i*(g_M[Pmodel-1]-1); 
 			VV = vv[ i];
			VC = vc[ i]; 
			dv=vv[ i+1] - VV;
			v=  VC;

			//printf("  i=%d  ,soff=%d singo=%f dv=%f  v=%f\n", i, soff, g_Singo[soff], dv,v);
			sum_sdv += g_Singo[soff]*dv;
			sum_dv+=dv;
			sum_s_dv_v += g_Singo[soff]*dv*v;
		}
	if (fabs(sum_dv) == 0.0 || fabs(sum_sdv) ==0.0) return 0;
	mean = sum_sdv/sum_dv;
	Height = sum_s_dv_v/ sum_sdv;
	soff = SStart;
	for(i=0;i<g_N[Pmodel-1]-1;i++,soff++) {
		//l_index =  i*(g_M[Pmodel-1]-1); 
		dv=vv[ i+1] - vv[ i];
		v=  vc[ i];
		MS += (( g_Singo[soff]-mean)*dv) *  (( g_Singo[soff]-mean)*dv);
	}
SD = sqrt( MS ) / sum_dv;

 //buf = TOSTRING(pline)+_T("Singo_Mean$"); 
 //buf2 = TOSTRING(mean); 
 //Post_Summary_By_Sail(NULL ,buf.c_str(),buf2.c_str());

printf("mean=%f ave ht = %d%%, SD = %f\n", mean, (int)(Height*100.) , SD); fflush(stdout);  
 //buf = TOSTRING(pline)+_T("$Singo_MeanHeight");
 // buf2 = TOSTRING((int)Height*100.);
  // Post_Summary_By_Sail(NULL ,buf.c_str(),buf2.c_str());	

return 1;
}

/* 			?Incl ude_h ist oric
 *                         |        |
 *                       sails      bits
 *                      |    |     |   |   
 *                      s1   s2    b1  b2      sail / bit level
 *                    a,b,c..                  shell level
 */



int LoadPansailValues(SAIL *sail,int what, int Pmodel, int  HAS, int PDone)
{
#ifndef NO_PANSAIL
double yoffset;  
char buf[256];
int i;

yoffset = 12.5* (float)sail->SailListIndex();

if(HAS && PDone && Pmodel > 0) {

  HC_Flush_Contents(".","geometry,subsegments");	
        HC_Set_Text_Alignment("<");
	HC_Set_Color("Text = black");
	HC_Set_Visibility("faces=on,edges=on");
	  HC_Set_Text_Font("size=.016sru,no transforms,name=sans serif");

	HC_Open_Segment("");
	  HC_Set_Text_Font("size=.02sru"); // better in window units
	  sprintf(buf,"%s_PANSAIL",sail->GetType().c_str());
	  HC_Insert_Text(LEFT_EDGE,0.,yoffset,buf); 
	HC_Close_Segment();
	yoffset-= 0.8;


	for(i=0;i< g_Mapcount;i++) {
	VECTOR p[5];
		HC_Open_Segment("");
			HC_Set_Color_By_Index("faces",i);
			p[4].x=p[0].x = LEFT_EDGE-5;	p[4].y=p[0].y=0.0;	p[4].z=p[0].z= yoffset-0.4;
			p[1].x = LEFT_EDGE-5;		p[1].y=0.0;	p[1].z= yoffset+0.4;
			p[2].x = 10.0;			p[2].y=0.0;	p[2].z= yoffset+0.4;
			p[3].x = 10.0;			p[3].y=0.0;	p[3].z= yoffset-0.4;

			HC_Insert_Polygon(5,p);
			sprintf(buf,"%+9.3g",g_PansailColour[what][i]/ g_Units[g_pnslU[what]].f  );
// hoops13 fails
			HC_Insert_Text((double) LEFT_EDGE,(double) 0.,(double) yoffset,buf);
			yoffset -= 0.8;
		HC_Close_Segment();
	} 
	sprintf(buf,"%s [%s]",g_pansailShell[what],g_Units[ g_pnslU[what]].units );
	HC_Insert_Text(LEFT_EDGE,0.,yoffset,buf); 
yoffset-= 0.8;


}
#else 
cout<< " LoadPansailValues"<<endl;

#endif
return(1);
}
int LoadPansailAxes(SAIL *sail, int what, int Pmodel, int  HAS, int PDone)
{
#ifndef NO_PANSAIL

double yoffset;
char buf[128];
int i;
Multi_GR *mg;

yoffset = (( float )sail->SailListIndex() )*12.5;

if(HAS && PDone && Pmodel > 0) {
 HC_Flush_Contents(".","geometry,subsegments");		
        HC_Set_Text_Alignment("<");
	HC_Set_Color("Text = black");

	HC_Open_Segment("");
	  HC_Set_Text_Font("size=.02sru");
	  sprintf(buf,"%s",sail->GetType().c_str());
	  HC_Insert_Text(LEFT_EDGE,0.,yoffset,buf); 
	HC_Close_Segment();
yoffset-= 0.8;

mg = &(g_Axis_Data[what]);

	for(i=0;i<mg->ng ;i++) {
		HC_Open_Segment("");
			HC_Set_Color_By_Index("text,markers",mg->col_indx[i]);
			HC_Set_Marker_Symbol(g_mpat[mg->mpat_indx[i]]);
			HC_Set_Marker_Size((float)0.5);
			HC_Insert_Marker((float) -1.5,0.,yoffset);
			HC_Insert_Text(0.,0.,yoffset,mg->labels[i]);
			yoffset -= 0.8;
		HC_Close_Segment();
	} 
	sprintf(buf,"%s ",g_pansailShell[what]  );
	HC_Insert_Text(LEFT_EDGE,0.,yoffset,buf); 
yoffset-= 0.8;


}
return(1);
#else
return 0;
#endif

}

int LoadSailValues(SAIL *sail,int state,int what,int updateGeom)
{
double yoffset;
char buf[512];
int i;
/* BC28/04/97 define NMX*/
#define NMX	100
VECTOR xyz[NMX][2];

yoffset = (( float )sail->SailListIndex() )*12.5;
 
 HC_Flush_Contents(".","geometry,subsegments");			
        HC_Set_Text_Alignment("<");
	HC_Set_Color("Text = black");
	HC_Set_Visibility("faces=on,edges=on");

	  HC_Set_Text_Font("size=.016sru,no transforms,name=sans serif");

	HC_Open_Segment("");
	 	HC_Set_Text_Font("size=.02sru");
		 HC_Set_Text_Alignment("<v");
	 	 sprintf(buf,"%s",sail->GetType().c_str());
	  	HC_Insert_Text(LEFT_EDGE,0.0,yoffset,buf); 
	HC_Close_Segment();
yoffset-= 0.8;


/* BC28/04/97 check NMX*/
if(g_Mapcount>NMX)g_World->OutputToClient("Too many colors",3);
	for(i=0;i< g_Mapcount;i++) {
		VECTOR p[5];
		HC_Open_Segment("");
			HC_Set_Color_By_Index("faces",i);
			p[4].x=p[0].x = LEFT_EDGE-5;	p[4].y=p[0].y=0.0;	p[4].z=p[0].z= yoffset-0.4;
			p[1].x = LEFT_EDGE-5;		p[1].y=0.0;	p[1].z= yoffset+0.4;
			p[2].x = 10.0;			p[2].y=0.0;	p[2].z= yoffset+0.4;
			p[3].x = 10.0;			p[3].y=0.0;	p[3].z= yoffset-0.4;

			 HC_Insert_Polygon(5,p);
			sprintf(buf,"%+9.3g",g_RelaxColour[what][i]/ g_Units[g_rlxU[what]].f  );  // this wwas crashing
//hoops 13 fails 
			HC_Insert_Text((double) LEFT_EDGE,(double)0.0,(double) yoffset,buf);
			xyz[i][0].x =  LEFT_BOX_EDGE;
			xyz[i][0].y =  0;
			xyz[i][0].z =  yoffset;
			xyz[i][1].x =  RIGHT_BOX_EDGE;
			xyz[i][1].y =  0;
			xyz[i][1].z =  yoffset;
		
			yoffset -= 0.8;
		HC_Close_Segment();
	} 
	sprintf(buf,"%s (%s) ",g_relaxShell[what], g_Units[g_rlxU[what]].units);
	HC_Insert_Text(LEFT_EDGE,0.0,yoffset,buf); 
yoffset-= 0.8;
if(PCDEBUG) cout<< " leaving LoadSailValues "<<endl;
  return(0);

 } 

 
int LoadPansailResults(SAIL *sail, int what, int Pmodel, int  HAS, int PDone)
{
/* routine to load pansail results - neeeds work !! */

HC_KEY key;
float *xg,*yg,*zg,*sgo,*uc,*vc,*fu,*fl,*hu,*hl;
float *dp,*cu,*cl;
int *iblu,*ibll;
;
int i,soff, l_ISumn;
/* char cpname[256];
FILE *cpfile; */
  l_ISumn  = -1;
if(PCDEBUG) cout<< "LoadPansailResults "<<endl;
if(!HAS || !PDone ||Pmodel <= 0) {
	HC_Flush_Contents(".","geometry,subsegments");
	//HC_Insert_Text(10.0,1.0,10.0,"PANSAIL DATA !!!!!!!!!!!!"); 
	return(0);
}
xg = &(g_Xv[g_goffset[Pmodel-1]]);
yg = &(g_Yv[g_goffset[Pmodel-1]]);
zg = &(g_Zv[g_goffset[Pmodel-1]]);
dp = &(g_Dcp[g_coffset[Pmodel-1]]);
cu = &(g_CpU[g_coffset[Pmodel-1]]);
cl = &(g_CpL[g_coffset[Pmodel-1]]);
fu = &(g_Cfu[g_coffset[Pmodel-1]]);
fl = &(g_Cfl[g_coffset[Pmodel-1]]);
iblu = &(g_iblstu[g_coffset[Pmodel-1]]);
ibll = &(g_iblstl[g_coffset[Pmodel-1]]);
hu = &(g_Hu[g_coffset[Pmodel-1]]);
hl = &(g_Hl[g_coffset[Pmodel-1]]);

HC_Flush_Contents(".","geometry,text,subsegments");


HC_Set_Visibility("edges=(everything=off,mesh quads=on)");

switch (what) {
	case 0:  {
	//	const char*labels[5] = {"Upper cP","Lower cP","delta cP"};

	//	 float *data[5];
		int mod,end,m,n;

		HC_Open_Segment("shell");
		  l_ISumn = (g_N[Pmodel-1] - 1) * (g_M[Pmodel-1]-1);
#ifndef NO_PANSAIL	
		  key = Insert_Pansail_Model(g_N[Pmodel-1],g_M[Pmodel-1],xg,yg,zg);

		 Panel_Colour_Mesh(key,g_N[Pmodel-1],g_M[Pmodel-1],dp,what);   
#endif
		HC_Close_Segment();

		HC_Open_Segment("graph");
		  mod = sail->GetLoadingIndex()-1;  
		  m = g_M[mod]-1;
		  n = g_N[mod]-1;
		  end = m*n;
		  uc = &(g_Uc[g_moffset[mod] -mod]);
		  vc = &(g_Vc[g_noffset[mod] -mod]);  // WRONG BECAUSE NOT CHECKED
		//  data[0] = cu;
		//  data[1] = cl;
		//  data[2] = dp;

		  HC_Flush_Contents(".","geometry,subsegments");		
   	//	  Multi_Graph(2, data, m,n, uc,vc,labels,1.0,-3.0, &(g_Axis_Data[what])); 
	//cout<< " no graphs"<<endl;	
		HC_Close_Segment();
	break;
	}
	case 1:
#ifndef NO_PANSAIL
		HC_Open_Segment("shell");
		key = Insert_Pansail_Model(g_N[Pmodel-1],g_M[Pmodel-1],xg,yg,zg);
		sgo = (float*)CALLOC(g_N[Pmodel-1]*g_M[Pmodel-1],sizeof(float));

/* g_Singo is packed with (g_N[model]-1) values for each model */

 		soff=0;
		 for(i=1;i<Pmodel;i++) {
			 soff +=g_N[i-1]-1;
			}


		for(i=0;i<g_N[Pmodel-1]-1;i++,soff++) {
			sgo[i*(g_M[Pmodel-1]-1)] = g_Singo[soff]; //printf(" %d  %f\n", i, g_Singo[soff] );
			sgo[i*(g_M[Pmodel-1]-1)+1] = g_Singo[soff];
		}
	    Panel_Colour_Mesh(key,g_N[Pmodel-1],g_M[Pmodel-1],sgo,what); 
		RXFREE(sgo); 
		HC_Close_Segment();
#endif
	break;
	case 2:
		#ifndef NO_PANSAIL
		HC_Open_Segment("shell");
		key = Insert_Pansail_Model(g_N[Pmodel-1],g_M[Pmodel-1],xg,yg,zg);
	    Panel_Colour_Mesh(key,g_N[Pmodel-1],g_M[Pmodel-1],fu,what); 
		HC_Close_Segment();
#endif
	break;
	case 3:
		#ifndef NO_PANSAIL
		HC_Open_Segment("shell");
		key = Insert_Pansail_Model(g_N[Pmodel-1],g_M[Pmodel-1],xg,yg,zg);
		Panel_Colour_Mesh(key,g_N[Pmodel-1],g_M[Pmodel-1],fl,what);
		HC_Close_Segment();
#endif
	break;
	case 4: 
		#ifndef NO_PANSAIL
		{
	   float *fi;
		l_ISumn = (g_N[Pmodel-1] - 1) * (g_M[Pmodel-1]-1);
	 	fi = (float*)CALLOC(l_ISumn,sizeof(float));
		for(i=0;i<l_ISumn;i++) 
			fi[i] = iblu[i];

		HC_Open_Segment("shell");
		key = Insert_Pansail_Model(g_N[Pmodel-1],g_M[Pmodel-1],xg,yg,zg);
		Panel_Colour_Mesh(key,g_N[Pmodel-1],g_M[Pmodel-1],fi,what);
		HC_Close_Segment();
	        RXFREE(fi);
	}
#endif
	break;
	case 5: 
		#ifndef NO_PANSAIL
		{
	   float *fi;
		l_ISumn = (g_N[Pmodel-1] - 1) * (g_M[Pmodel-1]-1);
	 	fi = (float*)CALLOC(l_ISumn,sizeof(float));
		for(i=0;i<l_ISumn;i++) 
			fi[i] = ibll[i];
		HC_Open_Segment("shell");
		key = Insert_Pansail_Model(g_N[Pmodel-1],g_M[Pmodel-1],xg,yg,zg);
		Panel_Colour_Mesh(key,g_N[Pmodel-1],g_M[Pmodel-1],fi,what);
		HC_Close_Segment();
		RXFREE(fi);
	}
#endif
	break;
	case 7: {
		/* call graphing routine */
		//const char*labels[5] = {"Upper H","Lower H"};
	
		float *data[5];
		int mod,end,m,n;

		l_ISumn = (g_N[Pmodel-1] - 1) * (g_M[Pmodel-1]-1);
	
		mod = sail->GetLoadingIndex()-1; cout<< "GetLoadingIndex()????"<<endl;
		m = g_M[mod]-1;
		n = g_N[mod]-1;
		end = m*n;
		uc = &(g_Uc[g_moffset[mod]]);
		vc = &(g_Vc[g_noffset[mod]]);
		data[0] = hu;
		data[1] = hl;
		
		//HC_Flush_Contents(".","everything");
		HC_Flush_Contents(".","geometry,subsegments");	
   	//	Multi_Graph(2, data, m,n, uc,vc,labels,(float) 3.0,(float) 0.0, &(g_Axis_Data[what])); 
	//	cout<< " no graphs"<<endl;


	break;
        }
}


return(1);
}

HC_KEY RXSail::LoadSailResults( int p_state,int what,int updateGeom)
{
	//SAIL *sail =this;
 /* 	
 		Read the current node positions into a shell 
		create a segment for the shell (return key later)
 		based on 'q' read in the geometry and any requested results
		based on 'updateGeom' decide whether to update the coords
		close segments and return
 */


 char type[128],buf[256];

 const int simple = g_Colour_By_Face; 
 /* add the code to go to the correct state here !!!!!!!!!!!!!!!!!!! */


HC_Open_Segment("shell");

this->m_HoopsShell = 0;
HC_Begin_Contents_Search(".","shell");
	while(HC_Find_Contents(type,&(this->m_HoopsShell))) {
		if(strieq(type,"shell"))
			break;
		else 
			this->m_HoopsShell=0;
	}
HC_End_Contents_Search();
	
	
if(this->m_HoopsShell) { HC_Delete_By_Key(this->m_HoopsShell);assert(!g_Hoops_Error_Flag);	 /* temporary fix for state save problem */
 	this->m_HoopsShell=0;	

}

if(updateGeom) {		/* need to re-read the geometry data */


		if(PCDEBUG) cout<<" enter Get Shell_Data Geom with updateGeom="<<updateGeom<<endl;
  	  if((ntri=Get_Shell_Data_Geom(this,&(this->m_HoopsCoords),&(this->m_HoopsFlist),
		  &(this->m_HoopsPcount),&(this->m_HoopsFcount))) <= 0 || (m_HoopsPcount <= 3) ){
		  this->m_HoopsShell = 0; if(PCDEBUG) {cout<<"Get_Shell_Data_Geom returns 0"<<endl; }
	} 
	  else {
#ifdef HOOPS
		if(this->m_HoopsShell) {	
			HC_Edit_Shell_Points(this->m_HoopsShell,0,m_HoopsPcount,m_HoopsPcount,m_HoopsCoords);
			if(PCDEBUG)cout<<" shell edited\n" ;
		}
		else {
			m_HoopsShell = HC_KInsert_Shell(m_HoopsPcount,m_HoopsCoords,m_HoopsFcount,m_HoopsFlist);
		}
#endif	

	}
	if(m_tridata) RXFREE(m_tridata);/* peters leak repair */
	m_tridata = NULL;	
	if(PCDEBUG)cout<< " enter Get_Triangle_Results\n"<<endl;
  	  Get_Triangle_Results(this,&(m_tridata)); 

// color as the tri m_colour
	if(PCDEBUG)g_ArgDebug=1;
  	 this->Display_Panel_Colours(this->m_HoopsShell); // WIERD was GetCurrent before Jan 2006
	if(PCDEBUG)g_ArgDebug=0;
  	  HC_Set_Rendering_Options("no color interpolation,no color index interpolation");

   	 HC_Close_Segment();
  
   	 return(this->m_HoopsShell);
 	  }
else{
if(PCDEBUG) cout<<" enter Get_Shell_Data_Geom with updateGeom ZERO"<<endl;
}

 if(what  ) {
#ifdef HOOPS
	HC_UnSet_Rendering_Options();
#endif
	assert(what < N_RELAX_SHELLS );
	if(m_HoopsPcount >=3 && PC_BeingLookedAt(".") ){		// a dbg test 
#ifdef HOOPS
 	  	 if(this->m_HoopsShell) 	
			HC_Edit_Shell_Points(this->m_HoopsShell,0,m_HoopsPcount,m_HoopsPcount,m_HoopsCoords);
	   	 else
			this->m_HoopsShell = HC_KInsert_Shell(m_HoopsPcount,m_HoopsCoords,m_HoopsFcount,m_HoopsFlist);

   	 	if(m_tridata) 
			Set_Shell_Data_Stress(this,this->m_HoopsShell,what,this->m_tridata,simple);  
#endif
	}

//	else printf(" skipping %d  %s  not beinglookedAt \n", what,string);

   	 HC_Close_Segment();
  	  return(this->m_HoopsShell);
 } 
  if(m_HoopsCoords) {RXFREE(m_HoopsCoords); m_HoopsCoords = NULL;}
  if(m_HoopsFlist) {RXFREE(m_HoopsFlist); m_HoopsFlist = NULL;}
  if(m_tridata) {RXFREE(m_tridata); m_tridata = NULL;}
  HC_Close_Segment();
  return(this->m_HoopsShell);
}
	 

//int  ChangeQuantity(Graphic *g,int quant) {return(0);}

int RXSail::Peters_Generate_Geometry(){  
SAIL *s =this;  
if(g_PansailDone){
	HC_Open_Segment_By_Key( s->PostProcNonExclusive());
		HC_Open_Segment(s->GetType().c_str());
#ifndef NO_PANSAIL
			Compute_All_Velocities(s);
#endif
		HC_Close_Segment();
		if(s->IsBoat()) {
		HC_Open_Segment(s->GetType().c_str());
			HC_Open_Segment("forces");
				//HC_Flush_Contents(".","everything");
				HC_Flush_Contents(".","geometry,subsegments");
#ifdef linux
				PDF_Draw_All_Forces();
#else
			cout<< "TODO  PDF_Draw_All_Forces(); "<<endl;
#endif
			HC_Close_Segment();
		HC_Close_Segment();
		}
	HC_Close_Segment();
} 
  PCF_Draw_Colored_Filaments(s); 
 //Draw_Material_Eigenvectors( sd->isail );
return 1;
}

 

 



