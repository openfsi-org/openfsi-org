// OCt 2005 some memory problems arrived as a result of the 3D meshing routines.


/* 
Jan 98 F90

Routines to create a non-dim. state file of the format
	u v dx dy dz
 */
 
#include "StdAfx.h" 
#ifdef NEVER

#include "RelaxWorld.h"
#include "RXSail.h"
#include "RXEntity.h"
#include "RXSitePt.h"
#include "RX_FESite.h"
#include "RXShapeInterpolation.h"
#include "RXLogFile.h"

#include "etypes.h"
#include "summary.h"

#include "stringutils.h"

#include "pansin.h"


#define STEP_SIZE_U  ((double) (1.0/21.0))
#define STEP_SIZE_V  ((double) (1.0/21.0))
#define NROWS   (22)
#define NCOLS   (22)
double RXSail::ReferenceLength()
{
double d=1;
	if(m_uv2xyz && m_uv2xyz->HasMesh()){
		this->m_uv2xyz->LocateInitialize ();
		ON_3dPoint p1 = this->CoordsAt(ON_2dPoint(0,0),RX_GLOBAL);	
		ON_3dPoint p2 = this->CoordsAt(ON_2dPoint(1,1),RX_GLOBAL);
		return p1.DistanceTo (p2);
	}
cout<<"RXSail::ReferenceLength is defauilt value (1)"<<endl;
return d;
}

int RXSail::write_ND_state(const char *filename)
{


	class RXLogFile l_logfile;
	class RXLogFile * dbptr = & l_logfile ; //this->m_pSailSum;  	
	
	int i,q;
	int rc= 0;
	char fname[256];
	FILE *fa=NULL;
	double scale;

	double u,v;
	char  format[128],buf[256];
	string sbuf;
	ON_3dPoint smod;
	RXEntity_p nodeEnt;


	if((filename == NULL) || (*filename == '\0')) return(0);
	strcpy(fname,filename);

	sprintf(buf,"$%s$",this->GetType().c_str()); 
/*
December 2009. 
*/
/* Peter changed 2/2/97. was "%s$" but that gives trouble, for instance when there is a
model called <smallgenoa> which has different node naming to  <genoa> 
also the 'flush summary call stops rubbish being passed to subsequent models.
THis had a virus effect. summary data could pass via the boat NDD files between separate
kinds of model. This would survive flushing of the main summary file
 */
/* Feb 2003 a useful improvement would be to get Create_Filtered Summary to skip those records
which ought to have an entity but don't.  This stops things like the W60 spreacher block from getting
propagated through all files
*/
	dbptr->Flush ();
	if(g_World->Summout()  )
		if(!g_World->Summout()->Create_Filtered_Summary(dbptr,buf))
			 rxerror(" writeState no filtered summary",1);

 /* now post all the positions of the nodes */
// scale by a characteristic dimension - the diagonal of the bounding box.

	scale = this->ReferenceLength ();

	vector<RXEntity_p >thelist;
	this->MapExtractList(SITE,  thelist,PCE_ISRESOLVED) ;
	this->MapExtractList(RELSITE,  thelist,PCE_ISRESOLVED) ;
	vector<RXEntity_p >::iterator it;
	for (it = thelist.begin (); it != thelist.end(); it++)  {
		nodeEnt = *it;
	  	if  (stristr(nodeEnt->attributes,"editable")) // !nodeEnt->generated)  not generated  Fails on 3dm nodes 
	     	 {
	   		 RXSitePt *s = dynamic_cast<RXSitePt *> ( *it);
			*buf=0;
			ON_3dVector dd = s->Deflection();
			dbptr->post_summary_value(
				"$"+this->GetType()+ "$node$"+nodeEnt->name()+"$dx",
                                to_string(dd.x/scale));
			dbptr->post_summary_value(
				"$"+this->GetType()+ "$node$"+nodeEnt->name()+"$dy",
                                to_string(dd.y/scale));
			dbptr->post_summary_value(
				"$"+this->GetType()+"$node$"+nodeEnt->name()+"$dz",
                                to_string(dd.z/scale));
	  }
	}

// a fixup for empty sailsums
	if(!(dbptr->GetNCols()) )
		dbptr->post_summary_value("StateFile",fname); 
	

	dbptr->Write_Current_Summary_File(fname);

	if(m_uv2xyz && m_uv2xyz->HasMesh()){
		this->m_uv2xyz->LocateInitialize ();
	}
	else{
		rxerror(" No UV coords defined -cant write shape",1);
		return 1;
	}

	fa=FOPEN(fname,"a");
	if(fa == NULL) { 
		return(0); }
	printf("Writing ND file  %s\n",fname);

	/* print outside 4 points */

	strcpy(format,"%15.8f\t %15.8f\t %15.8f\t %15.8f\t %15.8f\t %15.8f\t %15.8f\n" );
	fprintf(fa,format,-1.0,-1.0,0.0,0.0,0.0,0.0,0.0);
	fprintf(fa,format,-1.0, 2.0,0.0,0.0,0.0,0.0,0.0);
	fprintf(fa,format, 2.0, 2.0,0.0,0.0,0.0,0.0,0.0);
	fprintf(fa,format, 2.0,-1.0,0.0,0.0,0.0,0.0,0.0);
/*
coordsAt uses site->DeflectedPosition.
called with GLOBAL it returns the current position in world space (not lastknowngood)
called with MODELSPACE it returns the current position in world space transformed them to modelspace.
*/		 

	for(i=0;i<NROWS;i++) {
	  for(q=0;q<NCOLS;q++) {
	    u = ((double) i) * STEP_SIZE_U;
	    v = ((double) q) * STEP_SIZE_V;
		  smod= this->CoordsAt(ON_2dPoint(u,v),RX_MODELSPACE );//
		  ON_3dVector dd =  this->DeflectionsAt(ON_2dPoint(u,v),RX_MODELSPACE ); 
		  dd=dd/scale;

	      fprintf(fa,"%15.8f\t %15.8f\t %15.8g\t %15.8g\t %15.8g\t %15.8f\t %15.8f\n",
		      u,v,dd.x,dd.y,dd.z,smod.x,smod.y);
			rc++;
	  }
	}

	FCLOSE(fa);
	return(rc);
}

/* pseudo code for read 

	sh= new class RXShapeInterpolationP;
	sh->Read(wstring(fname));

	if(!sh->HasMesh () ) { cout << "state(ndd)file doesnt create a mesh"<<endl; return 0 ;}

	sh->LocateInitialize();

	for each SITE which has (UV)
		ON_2dPoint p2 (u,v);
		ON_3dVector d= sh->ValueAt (p2) *scale;	
		SetDeflection(d)

// finally the entities whose deflections are recorded in the file.

*/
 
int RXSail::read_ND_state(const char *filename)
{
	RX_FESite *s;
	ON_3dVector dl ;  
	double res[5];
	double scale =   this->ReferenceLength ();
class RXLogFile l_logfile;
	class RXLogFile * dbptr = & l_logfile ; //this->m_pSailSum;  

// first apply the distributed properties, then the explicit nodal ones.

	int nlines;
	class RXShapeInterpolationP sh ;
	nlines=sh.Read(TOSTRING(filename),2,5); // -ve if file cant be opened
	if(3<=nlines) { // skiplines, dataperline.
		int npts = sh.NPoints() ;
		if(sh.NPoints()  >=3){
			sh.Triangulate();
			if( sh.HasMesh () ) {  
				sh.LocateInitialize();
				for(vector< RX_FESite *>::iterator it=this->m_Fnodes.begin();it!=this->m_Fnodes.end();++it) {
						s =dynamic_cast<RX_FESite*> ( *it);
						if(!s) continue;  
						if(!(s->m_Site_Flags&PCF_ISFIELDNODE))
							continue;
						 sh.ValueAt (ON_2dPoint(s->m_u,s->m_v),res);
						dl = ON_3dVector(res) *scale;
						s->SetDeflection (dl); //expects the vector in model space.  this updates the FEA DB too
				} // for it
			} 
			else //nomesh
				cout << "state(ndd)file doesnt create a mesh"<<endl;
		}
		else
			cout << "state(ndd)file has too few points"<<endl;
	}
	else
		if(nlines<0) cout << "cant read state(ndd)file (Permissions?, corrupt?) '"<<filename<<"'"<<endl;
// now the explicitly written nodal deflections
// the first two lines are a CSV file
	dbptr->SetFileName(filename);
	dbptr->read_and_post_one_row( (int) 0,"",dbptr);
	int i;
	class SUMMARY_ITEM *si;
	for( i=0,si=dbptr->GetListHead();i<dbptr->GetNCols();i++,si++) {
		if(stristr(si->header,"node$") &&(stristr(si->header,"$dx")||stristr(si->header,"$dy")||stristr(si->header,"$dz"))) {
		  char buf[128];
		  sprintf(buf,"%f",si->value * scale);
		  dbptr->post_summary_value(si->header,buf);
		}
	}
	dbptr->Apply_Summary_Values();
	dbptr->Flush();
	this->MakeLastKnownGoodDxDyDz();
  return(1);
}
#endif
