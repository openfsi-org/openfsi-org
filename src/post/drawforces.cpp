/*  This is DrawForces.c */

#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include "vectors.h"
 #include <RXDatabaseI.h>
#include "summary.h"

#ifdef USE_PANSAIL
#include "pansail.h"
#endif
#include "global_declarations.h"
#include "RelaxWorld.h"

#include "drawforces.h"


int PDF_Draw_One_Force(float fx,float fy,float fz, float mx,float my,float mz,float RefLen,const char*t)
 {
#ifdef HOOPS
	float a[4][4] = {{0,0,0,0},{0,0,0,0},{0,0,1,0},{0,0,0,1}};
	float b[4][4],  My0, scale;
	VECTOR x0, x1;
/* a11 is 
	0	-Fy	0	0
	Fy	0	0	0
	0	0	1	0
	0	0	0	1

b is its inverse
then [x,z] is b x [Mx, Mz]

  The force vector is in direction [fx,fy,fz]
  from [x0,0,zo]
  There is a moment, value My direction global Y
  where My is Full A times [X0,Z0,0]
	
Full A is 
	0	-Fy	Fz	0
	Fy	0	-Fx	0
	-Fz	Fx	0	0
	0	0	0	1			
	*/
//printf("Force  F(%f %f %f ) M(%f %f %f ) RL %f\n", fx, fy, fz,  mx, my, mz, RefLen);
	a[0][1] = -fy; a[1][0] = fy;
	HC_Compute_Matrix_Inverse ((float*) a,(float*) b);
	x0.x = b[0][0] * mx + b[0][1]*mz;
	x0.z = b[1][0] * mx + b[1][1]*mz;
	x0.y = 0.0;

	a[0][2] = fz;
	a[1][2]=-fx;
	a[2][0] = -fz;
	a[2][1] = fx;
	a[2][2] = 0;
	
	My0 = a[2][0] * x0.x + a[2][1] * x0.z + a[2][2] * x0.y; 
	scale = (float) sqrt(fx*fx + fy*fy + fz*fz);
	if(scale > 0.001)
		scale = RefLen/scale;
	else
		scale = 1.0;
	x1.x = x0.x + fx * scale;
	x1.y = x0.y + fy * scale;
	x1.z = x0.z + fz * scale;
	PDF_Draw_Arrow(x0,x1,t);
#endif
	return 1;
}

int PDF_Draw_Arrow(VECTOR x0,VECTOR x1,const char*t)
{

	if(PC_Dist(&x0,&x1) < 0.001) return 0;
#ifdef HOOPS

// a nice arrow has shaft dia 5% head dia 10% head length 15%
        HC_Insert_Line(x0.x,x0.y,x0.z,x1.x,x1.y,x1.z);
        HC_Insert_Marker(x1.x,x1.y,x1.z); //
	HC_Set_Line_Weight(4.0);
	HC_Insert_Text(x1.x,x1.y,x1.z,t);
	HC_Set_Text_Path(x1.x-x0.x,  x1.y-x0.y,x1.z-x0.z);
	HC_Set_Text_Font("transforms=on,rotation=follow path, size = 0.25oru");
	HC_Set_Text_Alignment("v<");
#else
         cout<< "TODO: Force "<<t <<x1.x<<","<<x1.y<<","<<x1.z<<endl;
#endif

return 1;
}

int PDF_Draw_All_Forces() 
{
double fx,fy,fz,mx,my,mz,fa,fb,ma,mb, reflen;
char buf[256];
/*	Trefftz_Lift
	Trefftz_Drag
	Trefftz_Heel_Moment
	PRSINT_Lift
	PRSINT_Drag
	PRSINT_Heel_Moment
	Viscous_Drag
	Fx_relax
	Fy_relax
	Fz_relax
	Mx_relax
	My_relax
	Mz_relax
  */
if(!g_World->DBMirror()) 
		return 0;
HC_Open_Segment("Pansail Pressure Integration");
//	cout<< " Prst Int green\n"<<endl;
	HC_Set_Color("green"); 
	g_World->DBMirror()->Extract_One_Text("PRS_Int_Fx" , buf,255); fa= atof(buf);
	g_World->DBMirror()->Extract_One_Text("PRS_Int_Fy" , buf,255); fb= atof(buf);
	g_World->DBMirror()->Extract_One_Text("PRS_Int_Fz" , buf,255); fz= atof(buf);
	g_World->DBMirror()->Extract_One_Text("PRS_Int_Mx" , buf,255); ma= atof(buf);
	g_World->DBMirror()->Extract_One_Text("PRS_Int_My" , buf,255); mb= atof(buf);
	g_World->DBMirror()->Extract_One_Text("PRS_Int_Mz" , buf,255); mz= atof(buf);

        double l_AWA = g_World->m_wind.Get(RXWindGeometry::AWA );
/* these are in wind axes.  Convert to global */
//	printf(" (PRS_Int_Fx,etc)fx=%f fy=%f fz=%f ma=%f mb=%f mz=%fawa= %f radians\n",fa,fb,fx,ma,mb,mz, g_AWA);
        fx = fa * cos(l_AWA) -fb * sin(-l_AWA);
        fy = fb * cos(l_AWA) + fa * sin(-l_AWA);
        mx =ma * cos(l_AWA)  - mb * sin(-l_AWA);
        my =mb * cos(l_AWA) +  ma * sin(-l_AWA);

        reflen =  g_World->m_wind.Get(RXWindGeometry::ReferenceLength);

        PDF_Draw_One_Force(fx,fy, fz, mx, my, mz, reflen,"Pansail Pressure Integration") ;
HC_Close_Segment();
HC_Open_Segment("Trefftz Plane analysis");
	//cout<< "Trefftz magenta\n"<<endl;
	HC_Set_Color("magenta");
	g_World->DBMirror()->Extract_One_Text("Trefftz_Drag",buf,255);fa= atof(buf);
	g_World->DBMirror()->Extract_One_Text("Trefftz_Lift",buf,255);fb= atof(buf);
	fz = 0.0;
	g_World->DBMirror()->Extract_One_Text("Trefftz_Heel_Moment",buf,255);ma= atof(buf);
	mz = 0.0;
/* these are in wind axes.  Convert to global */
        fx = fa * cos(l_AWA) -fb * sin(-l_AWA);
        fy = fb * cos(l_AWA) + fa * sin(-l_AWA);
        mx =    ma * cos(l_AWA);
        my =    ma * sin(-l_AWA);
        PDF_Draw_One_Force(fx,fy, fz, mx, my, mz, reflen,"Trefftz Plane Analysis") ;
HC_Close_Segment();
HC_Open_Segment("Relax Pressure Integration");
	//cout<< " relax blue\n"<<endl;
	HC_Set_Color("blue");
	g_World->DBMirror()->Extract_One_Text("Fx_relax",buf,255); fx=atof(buf);
	g_World->DBMirror()->Extract_One_Text("Fy_relax",buf,255 );fy=atof(buf);
	g_World->DBMirror()->Extract_One_Text("Fz_relax",buf,255);fz=atof(buf);
	g_World->DBMirror()->Extract_One_Text("Mx_relax",buf,255);mx=atof(buf);
	g_World->DBMirror()->Extract_One_Text("My_relax",buf,255);my=atof(buf);
	g_World->DBMirror()->Extract_One_Text("Mz_relax",buf,255);mz=atof(buf);
        PDF_Draw_One_Force(fx,fy, fz, mx, my, mz, reflen,"Relax Pressure Integration") ;
HC_Close_Segment();





	return 1;
}
