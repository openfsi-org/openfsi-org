/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
  6 12 00 removed CheckAllSliding as it has no effect.
  * 16/3/97 added P ost_VMC() call
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"
#undef strieq

#include "RXSail.h"


#include "CheckConnects.h"

#include "drawref.h"
#include "summary.h"

#include "text.h"
#include "Gflags.h"

#include "global_declarations.h"
#include "menuCB.h"
#ifdef USE_PANSAIL
#include "drawwind.h"
#endif
#include "menuCB.h"

#include "RelaxWorld.h"
#include "CheckAll.h"


#undef DEBUG

int AKM_Each_Cycle()
{
int retVal=0;
static int level = 0;

level++;

if(isGFlag(GLOBAL_DO_CAMERAS)) {
#ifdef _X
	SetCameraList();
#endif
	clGFlag(GLOBAL_DO_CAMERAS);
}

if(isGFlag(GLOBAL_DO_APPWIND)) {
#ifdef USE_PANSAIL
	Draw_Wind((float)0.0,g_PansailIn.Reference_Length);
#endif
        g_World->Post_Wind_Details(); // wasVMC
	clGFlag(GLOBAL_DO_APPWIND);
}

if(isGFlag(GLOBAL_DO_PANSAIL_TEXT)) {
	updateOneTextWindow(GLOBAL_DO_PANSAIL_TEXT);
	clGFlag(GLOBAL_DO_PANSAIL_TEXT);
}

if(isGFlag(GLOBAL_DO_RELAX_TEXT)) {
	updateOneTextWindow(GLOBAL_DO_RELAX_TEXT);
	clGFlag(GLOBAL_DO_RELAX_TEXT);
}

if(isGFlag(GLOBAL_DO_DEBUG_TEXT)) {
	updateOneTextWindow(GLOBAL_DO_DEBUG_TEXT);
	clGFlag(GLOBAL_DO_DEBUG_TEXT);
}

if(isGFlag(GLOBAL_DO_REFLECTION)) {
#ifdef HOOPS
	Draw_Reflection();
#endif
	clGFlag(GLOBAL_DO_REFLECTION);

}

level--;
if(!level && retVal) {
	retVal += g_World->PostProcessAllSails(3);
#ifdef _X
        UpdateMenu();
#endif  
}
#if defined(HOOPS) &&defined(_X)
int j;Graphic *g;
	for(j=g_hoops_count-1;  j>=0 ; j--)  {
		g = &g_graph[j];  
		if(g->EMPTY ) 	continue;
		if(g->HBV)	g->HBV->Update();	
	}
#endif

return(retVal);
}

