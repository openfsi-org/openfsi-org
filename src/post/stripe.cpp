/* april 2003  extended the search for SC stripes.

explored the mirror-text issue. 
IF we go:	HC_Set_Handedness("left"); 
this  makes the text readable
but it stuffs up the coordinate transformations.
The solution may be to set the handedness before doing any transformations.
NB we are doing all the drawing in world coordinates. If 'where' was a left-handed world and we transformed
to its system, that may work.
*/
/* 
Jul 2001  Tune font size.. Try erversing text path dirs to remove mirroring
Aug 97  The camera name (from UO 'petercam') now posted properly
and the orientation of nv2 text corrected 
The trasnverse lines were not correctly drawn. 
We now generate them in viewport space and transform to world space just before display

*/

/* 3/3/95   some printfs removed and the camera name in post_summary removed */

#include "StdAfx.h" 
#include "RelaxWorld.h"
#include "RXSail.h"
#include "hc.h"

#include "vectors.h"	/* sets up structure for data*/
#include "shapegen.h"	/* headers for shapegen.F*/

#include "sailutil.h"
#include "summary.h"
#include "polyline.h"
#include "GraphicStruct.h"

#include "stripe.h"

/* typical call
   Display_Stripe_Data(char*camseg,char*filter,char*where)
   where
   camseg = root of camera window
   filter = ".../stripes/ *" or something  We search for children of included segs that contain polylines
			  where ="overlay"   (segment to put the derived data)(relative to camseg) */


  int Draw_Transverse_Line_From_VPS(char *camseg,VECTOR e1,VECTOR cv,VECTOR nv,float chord,float pos, float length,char*s){
    VECTOR r0,r1;

    r0.x =  e1.x + cv.x*chord*pos;
    r0.y =  e1.y + cv.y*chord*pos;
    r0.z =  e1.z + cv.z*chord*pos;
    r1.x = r0.x + nv.x*length*chord;
    r1.y = r0.y + nv.y*length*chord;
    r1.z = r0.z + nv.z*length*chord;
   
    HC_Compute_Coordinates(camseg,"viewpoint",&r0,"world",&r0);
    HC_Compute_Coordinates(camseg,"viewpoint",&r1,"world",&r1);

    HC_Insert_Line(r0.x,r0.y,r0.z,r1.x,r1.y,r1.z);
    HC_Insert_Text((r0.x+r1.x)/(float)2.0,(r0.y+r1.y)/(float)2.0,(r0.z+r1.z)/(float)2.0,s);
 

return 1;
}

 int Draw_Transverse_Line(VECTOR e1,VECTOR cv,VECTOR nv,float chord,float pos, float length,char*s){
    VECTOR r[2];
    
    r[0].x =  e1.x + cv.x*chord*pos;
    r[0].y =  e1.y + cv.y*chord*pos;
    r[0].z =  e1.z + cv.z*chord*pos;
    r[1].x = r[0].x + nv.x*length*chord;
    r[1].y = r[0].y + nv.y*length*chord;
    r[1].z = r[0].z + nv.z*length*chord;
    HC_Insert_Line(r[0].x,r[0].y,r[0].z,r[1].x,r[1].y,r[1].z);
	HC_Set_Text_Path(nv.x,nv.y,nv.z); 
	HC_Insert_Text((r[0].x+r[1].x)/(float)2.0,(r[0].y+r[1].y)/(float)2.0,(r[0].z+r[1].z)/(float)2.0,s);

//	HC_Open_Segment("text"); trying to reverse the mirror. didnt work
//		HC_Set_Color("text=green");
//
//		HC_Translate_Object( (r[0].x+r[1].x)/(float)2.0,(r[0].y+r[1].y)/(float)2.0,(r[0].z+r[1].z)/(float)2.0);
//		HC_Rotate_Object_Offaxis(nv.x,nv.y,nv.z,180.0);
//		HC_Insert_Text(0.0,0.0,0.0,s);
//
//	HC_Close_Segment();
	
    return(1);
  } 
 int  LPS(const char *cam,const char*seg,const char*s,const float v) {
	 char b1[256],b2[32];
	 int model = -1; /* dont prepend model data */
	 sprintf(b1,"%s_%s_%s",s,cam,seg);
	 sprintf(b2,"%5.2f",v);

	 Post_Summary_By_SLI(&model,b1,b2);
    return(0);
 }
 HC_KEY Display_Stripe_Data_By_Key(Graphic *p_g_view,SAIL *sail)
{
	char camseg[512], where_seg[512],filterseg[512];

	//camseg is p_g_view(where the camera is) try m_ViewSeg
	HC_Open_Segment_By_Key(p_g_view->m_ViewSeg);
		HC_Show_Pathname_Expansion(".",camseg);
	HC_Close_Segment();
	// filter is a segment below which we can expect to find some stripes. try sail/stripe
	HC_Open_Segment_By_Key( sail->Graphic()->m_ModelSeg) ;
		HC_Begin_Segment_Search(".../stripe");
		if(!HC_Find_Segment(filterseg))
			*filterseg=0;
		HC_End_Segment_Search();
	HC_Close_Segment();
// whereseg is Graphic->m_OverlaySeg/stripe_overlay
	HC_Open_Segment_By_Key(p_g_view->m_OverlaySeg); 
	HC_Open_Segment("stripeOverlay");
		HC_Set_Visibility("on");
		HC_Flush_Contents(".","subsegments");
		HC_Show_Pathname_Expansion(".", where_seg );
	HC_Close_Segment();
	HC_Close_Segment();

//printf(" camseg is %s\n where_seg is %s\nfilter=%s\n",camseg,where_seg,filterseg);
if(!*filterseg) { 
	puts("no filterseg"); 
	return 0;
}
return Display_Stripe_Data(camseg,filterseg,where_seg) ;

}
 	
HC_KEY Display_Stripe_Data(const char*p_camseg,const char*p_filter,const char*p_where_seg) {
  HC_KEY retval = 1;
  char type[256];
  int k,c,c1;
  long int key;
  VECTOR *pw, *pv,cv,nv,cv2,axis,position,tar,nvv,cvv,l_Zaxis,nv2;
	VECTOR dum,dum2; float chord2;
  double *xg,*yg;
  double BaseTwist = 0.0;
  float rawchord=0,chord,cr=0,cp=0,th=0,fps=0,aps=0,ea=0,xa=0,aft_depth=0;
  char buf[64];
	char buff[256], Ibuff[256];
  char where[256],cam[256],str[256],owner[256];

    int usd=0;   /* 1 if curve is upsidedown */

  HC_Show_Pathname_Expansion(p_where_seg,where);
 l_Zaxis.x=l_Zaxis.y=0.0;  l_Zaxis.z=-1.0;
  
 if(!HC_QShow_Existence(p_filter,"self")) {
	printf("Display_Stripe_Data in <%s>\n",p_filter);
	g_World->OutputToClient (" No stripes: invalid filter",2);
	 return(0);
	}
 HC_Begin_Contents_Search(p_filter,"subsegments,includes");
   while (HC_Find_Contents(type,&key)){ 

	*buff=0;
	//printf(" found a %s\n",type);
	if(strieq(type,"include")) 
		HC_Show_Include_Segment(key,buff);
	if(strieq(type,"segment")) 
 		HC_Show_Segment(key,buff);
	if(*buff) {
		HC_Open_Segment(buff);
  HC_Begin_Contents_Search("(.,*,...)","polylines,includes"); { // was filter till nov 19 2002
    while (HC_Find_Contents(type,&key)){
	if(strieq(type,"include")) { //find key to any polyline in this included segment
		HC_Show_Include_Segment(key,Ibuff);
 		HC_Begin_Contents_Search(Ibuff,"polylines"); 
		if(!HC_Find_Contents(type,&key))
			key=0;
		HC_End_Contents_Search();
	}
	// else key is what we already found

      HC_Show_Polyline_Count(key,&c);
     HC_Show_Owner_By_Key(key,owner); 
      pw = (VECTOR*)MALLOC((c+1)*sizeof(VECTOR));
      pv  = (VECTOR*)MALLOC((c+1)*sizeof(VECTOR)); 
      xg = (double*)MALLOC((c+1)*sizeof(double));
      yg = (double*)MALLOC((c+1)*sizeof(double));
      HC_Show_Polyline(key,&c,pw);

	HC_QShow_Net_Camera_Target(p_camseg,&tar.x,&tar.y,&tar.z);			
	HC_QShow_Net_Camera_Position(p_camseg, &position.x,&position.y,&position.z);
	PC_Vector_Difference(&tar,&position,&axis);
//	PC_Vector_Difference(&position,&tar,&axis);

      	for(k=0;k<c;k++){HC_Compute_Coordinates(p_camseg,"world",&pw[k],"viewpoint",&pv[k]);}
      	for(k=0;k<c;k++){
		xg[k]=pv[k].x-pv[0].x; yg[k]=pv[k].y-pv[0].y;
	}
	//  section_analyse_(xg,yg,&c,&BaseTwist,&rawchord,&cr,&cp,&th,&fps,&aps,&ea,&xa,&aft_depth,&usd);
  puts ("no seciton analysis right now");
  
      c1=c-1;
      HC_Open_Segment(where); { 
	HC_Set_Line_Weight(2.0); 
	HC_Set_Handedness("right");  // a guess
 // a test July 2001 was .03sru
	 HC_Set_Text_Font("size = 0.03sru, transforms=on, rotation=follow path"); /* was .0175 */
	HC_Set_Color("lines=blue,text=black");
	HC_Set_Window(-1.0,1.0,-1.0,1.0);

	//Print_Camera("stripe Overlay",".") ; // for debugging this just showed the view windows camera
	HC_Set_Window_Pattern("clear"); 
	if(!HC_Show_Existence("style"))
		HC_Style_Segment(p_camseg);
	HC_Open_Segment(""); {
	  HC_Insert_Polyline(c,pw);

	 strcpy(cam,"_");
	  HC_Show_One_Net_User_Option("petercam",cam);	 

	  HC_Show_Owner_By_Key(key,owner);
	  HC_Parse_String(owner,"/",-1,str);

	  LPS(cam,str,"Depth",cr);	
	  LPS(cam,str,"Draft_Pos",cp);
	  LPS(cam,str,"twist",th);
	  LPS(cam,str,"fwd%",fps);
	  LPS(cam,str,"aft%",aps);
	  LPS(cam,str,"Entry_Angle",ea);
	  LPS(cam,str,"Exit_Angle",xa);
	  LPS(cam,str,"AftDepth%",aft_depth*100.0);
	
	  PC_Vector_Difference(&pw[c1],&pw[0],&cv);
	  chord = (float)HC_Compute_Vector_Length(&cv);
	  HC_Compute_Normalized_Vector(&cv,&cv);	
	  HC_Compute_Cross_Product(&cv,&axis,&nv);
	  HC_Compute_Normalized_Vector(&nv,&nv);

	  PC_Vector_Difference(&pv[c1],&pv[0],&cvv);
	  chord = (float)HC_Compute_Vector_Length(&cvv);
	  HC_Compute_Normalized_Vector(&cvv,&cvv);	
	  HC_Compute_Cross_Product(&cvv,&l_Zaxis,&nvv);
	  HC_Compute_Normalized_Vector(&nvv,&nvv);




	if(!usd) {
     		nv.x = - nv.x; nv.y = - nv.y; nv.z = - nv.z;
	} 
	if(rawchord < 0.0) {
  		nv.x = - nv.x; nv.y = - nv.y; nv.z = - nv.z;
	}
	  HC_Compute_Coordinates(p_camseg,"viewpoint",&cvv,"world",&cv2);
	  HC_Compute_Coordinates(p_camseg,"viewpoint",&nv,"world",&nv2);
// get the transformed chord


	dum.x=chord; dum.y= dum.z=0.0;
	  HC_Compute_Coordinates(p_camseg,"viewpoint",&dum,"world",&dum2);
	chord2 = HC_Compute_Vector_Length(&dum2);
 

	HC_Open_Segment("text"); {

	  	HC_Set_Text_Path(cvv.x,cvv.y,cvv.z);// aug 2001 try neg. 
		HC_Set_Text_Alignment("v");
      		HC_Open_Segment("cp"); 	{
			float x,y,z,f;
			f = cp/(float) 100.0;
			x = pw[0].x * ((float)1.0-f) + pw[c1].x * f;
			y = pw[0].y * ((float)1.0-f) + pw[c1].y * f;
			z = pw[0].z * ((float)1.0-f) + pw[c1].z * f;
			sprintf(buf,"cp=%5.2f",cp);
			
			HC_Insert_Text(x,y,z,buf);
		}HC_Close_Segment();
	  	HC_Open_Segment("twist");
			 sprintf(buf,"tw=%5.2f",th);
			 HC_Insert_Text((float)(pw[0].x+cv2.x*chord2/5.0),pw[0].y, pw[0].z,buf);	
  	  	HC_Close_Segment();
	}HC_Close_Segment();

	HC_Open_Segment("lines"); {
	  HC_Set_Line_Weight(1.0);
	  HC_Insert_Line(pw[0].x,pw[0].y,pw[0].z,pw[c1].x,pw[c1].y, pw[c1].z);
 	  HC_Insert_Line(pw[0].x,pw[0].y,pw[0].z,(float)(pw[0].x+cv2.x*chord2/4.0),pw[0].y, pw[0].z); 

	  HC_Open_Segment("trans");

	 	  sprintf(buf,"%5.2f",cr);
		  Draw_Transverse_Line(pw[0],cv,nv,chord,(float) (cp/100.0),(float)(cr/100.0),buf);
		  sprintf(buf,"%5.2f",fps);
		  Draw_Transverse_Line(pw[0],cv,nv,chord,(float) (cp/200.0),(float) (cr*fps/10000.0),buf);
		  sprintf(buf,"%5.2f",aps);
		  Draw_Transverse_Line(pw[0],cv,nv,chord,(float) ((cp+100.0)/200.0),(float) (cr*aps/10000.0),buf);
	  HC_Close_Segment();



    }HC_Close_Segment(); /* lines */
	}HC_Close_Segment(); /* blank this stripe */
   }HC_Close_Segment();	/* where */
      RXFREE(pw);
      RXFREE(pv);
      RXFREE(xg);
      RXFREE(yg);
    }
    
  	}	 HC_End_Contents_Search();
	HC_Close_Segment(); 
	} // if type I think
  }
   HC_End_Contents_Search();
  
  return(retval);
}
