/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


#include "StdAfx.h"

#include "RXEntityDefault.h"
#include "RXSail.h"

#include <X11/Xatom.h>
#include <X11/Intrinsic.h>
#include <X11/Shell.h>

#include <Xm/Xm.h>
#include <Xm/TextF.h>

#include "griddefs.h"
#include "entities.h"
#include "elements.h"
#include "controlmenu.h"
#include "Gflags.h"
#include "sailutil.h"
#include "stringutils.h" 
#include "controlmenu.h"

#include "global_declarations.h"

#include "mast.h"



#define EXTRA 1.05

int set_current_camera(char *seg,long int view) //  for the trim menu feedback window (altho a postproc window does it better
{
	VECTOR max,min;
	float xpos,ypos,zpos,dx,dy,dz;
	char info[64];


HC_Open_Segment(seg);
  HC_Open_Segment("base");

	HC_Show_Bounding_Info(info);
	if(stristr(info,"cuboid")) {
		if(!HC_Compute_Circumcuboid(".",&min,&max)) {
			g_World->OutputToClient ("Compute Circumcuboid failed",1);
			max.x = max.y = max.z = 1;
			min.x = min.y = min.z = -1;
		}
xpos = (max.x + min.x)/2;
ypos = (max.y + min.y)/2;
zpos = (max.z + min.z)/2;	/* centre of object */

dx = (max.x - min.x)*EXTRA;
dy = (max.y - min.y)*EXTRA;
dz = (max.z - min.z)*EXTRA;	/* size of object */

if(dx < 1e-6)			/* don't allow zero or -ve */
	dx = (EXTRA)-1;	
if(dy < 1e-6)
	dy = (EXTRA)-1;
if(dz < 1e-6)
	dz = (EXTRA)-1;

switch(view) {
	case SIDE:
		HC_Set_Camera_Projection("Orthographic");
		HC_Set_Camera_Up_Vector(1,0,0);
		HC_Set_Camera_Position(xpos,ypos,50);	/* out along z-axis */
		HC_Set_Camera_Target(xpos,ypos,zpos);	/* looking back at centre  */
	   	HC_Set_Camera_Field(dy,dx);		
	break;
	case AFT:
		HC_Set_Camera_Projection("Orthographic");
		HC_Set_Camera_Up_Vector(1,0,0);
		HC_Set_Camera_Position(xpos,50,ypos);	/* out along z-axis */
		HC_Set_Camera_Target(xpos,ypos,zpos);	/* looking back at centre  */
	   	HC_Set_Camera_Field(dz,dx);		
	break;

}
	      }
	else {
   //printf(" in %s \nBounding info wasnt a cuboid. it was\n<%s>\n",seg, info);
	/*	g_World->OutputToClient ("SERIOUS ERROR - missing masts in model\n",3); */
	}

  HC_Close_Segment();
HC_Close_Segment();
return(0);
}












