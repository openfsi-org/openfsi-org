//vppint.h : header file for vppint.cpp
//Thomas 10 06 04 

#ifndef RX_VPPINT_H
#define RX_VPPINT_H

#include "summary.h"

EXTERN_C int Passive_VPP_Call(void);
EXTERN_C int Active_VPP_Call(void);


#endif //#ifndef RX_VPPINT_H

