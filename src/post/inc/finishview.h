#ifndef _FINISHVIEW_H_
#define  _FINISHVIEW_H_

//#include "HBaseView.h"

class HBaseView;

EXTERN_C HC_KEY Draw_ViewContour_Scale(HBaseView *p_pView,
 	const char *p_UOS,
	const char *p_seg,
	const float step,
	const float cmin,
	const float x0);
 
EXTERN_C int Finish_This_View(Graphic *pG);
EXTERN_C int Finish_All_Views(void);

#endif
