/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 8.97 changed memset call
  * 10/96 now accepts a char*argument from which it sets up the camera & options
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"
#include "RXGraphicCalls.h"
#define OVDB (0)

#include "RXSail.h"
#include "rxsubwindowhelper.h"
#include "entities.h"
#ifdef _X
#include "Xm/Protocols.h"
#endif
#include "CreateMsgArea.h"

#include "script.h"
#include "camera.h"
#include "MakeNewDrawingArea.h"
#include "OpenBuildFile.h" 

#include "hoopcall.h"
#include "words.h"
#include "global_declarations.h"

#include "stringutils.h"
#ifdef HOOPS
#include "RXhClient.h" 
#include "viewmenu.h"
#endif


#include "RelaxWorld.h"
#include "RX_UI_types.h"

#include "finishview.h"

#include "OpenView.h"

#ifdef _X
void create_top_camera (Graphic *g, Widget parent,const char *p_driver) {
    Atom	wm_delete_window;
    int min=0;
    int l_ac=0;
    Arg al[64];
    XtSetArg(al[l_ac], XmNallowShellResize, TRUE); l_ac++;
    strcpy(g->filename,"Ofsi Viewport: ( Untitled )");
    l_ac=0;
    XtSetArg(al[l_ac],XmNtitle,g->filename); l_ac++;
    XtSetArg(al[l_ac],XmNiconName,"Ofsi View"); l_ac++;
    //	XtSetArg(al[l_ac],XmNwidth,600);l_ac++;  // Now in the resource file
    //	XtSetArg(al[l_ac],XmNheight,650);l_ac++;
    XtSetArg(al[l_ac],XmNwinGravity,NorthEastGravity);l_ac++;
    if(min) {
        XtSetArg(al[l_ac], XmNiconic, TRUE); l_ac++;
    }
    g->topLevel = XtCreatePopupShell ( "camera", topLevelShellWidgetClass, parent, al, l_ac );
    l_ac = 0;
    g->mainWindow = XmCreateMainWindow(g->topLevel,(char*)"mainWindow",al,l_ac);
    XtManageChild(g->mainWindow);
    g->menuBar = CreateViewMenuBar(g->mainWindow,g);
    XtManageChild(g->menuBar);

    CreateMsgArea(g);
    l_ac = 0;

    g->workArea = MakeNewDrawingArea(g->mainWindow,"camerabase",g,0);
    // last arg 0 for separate models g_World->GetHBaseModel() otherwise
    XmMainWindowSetAreas(g->mainWindow,g->menuBar,NULL,NULL,NULL,g->workArea);
    l_ac=0;
    XtSetArg(al[l_ac],XmNmessageWindow,(XtArgVal) g->msgform);
    l_ac++;
    XtSetValues(g->mainWindow,al,l_ac);

    wm_delete_window = XmInternAtom(XtDisplay(g->topLevel),
                                    (char*)"WM_DELETE_WINDOW",
                                    FALSE);

    XmAddWMProtocolCallback(g->topLevel, wm_delete_window, ViewDeleteCB, (XtPointer) g);

}

int create_remote_top_camera (Graphic *g, const char *p_Hoops_Driver) {
    Display         *l_display;
    char XDriver[256], buf[256];
    Atom	wm_delete_window;
    int min=0;
    Arg al[64]; int l_ac=0;
    // display_name typically 192.168.1.2:0.0
    // Hoops driver typically 'X11/othermachine:0.0'
    const char*lp = strrchr(p_Hoops_Driver,'/');
    if(lp) {lp++; strcpy( XDriver, lp);}
    else return 0;
    l_ac=1;
    {
        char **Args = (char **)CALLOC(32,1);
        Args[0]=STRDUP(g_Application_Name);
        l_display = XtOpenDisplay (g_app_context,XDriver,g_Application_Name,"RelaxII", NULL,0,(int*)&l_ac,Args);
        RXFREE(Args[0]);
        RXFREE(Args);
    }
    if(!l_display) {
        char str[256];
        sprintf(str," can't open Display '%s' for graphics driver '%s'",XDriver,p_Hoops_Driver);
        g_World->OutputToClient (str,2);
        return 0;
    }
    XSynchronize(l_display, True); puts("synchronizing = SLOW");

    l_ac=0;
    XtSetArg(al[l_ac], XmNallowShellResize, TRUE); l_ac++;
    strcpy(g->filename,"Ofsi Viewport: (Remote)");
    l_ac=0;
    XtSetArg(al[l_ac],XmNtitle,g->filename); l_ac++;
    XtSetArg(al[l_ac],XmNiconName,"Ofsi View"); l_ac++;
    XtSetArg(al[l_ac],XmNwinGravity,NorthEastGravity);l_ac++;
    XtSetArg(al[l_ac],XmNwidth,600);l_ac++;  // Now in the resource file
    XtSetArg(al[l_ac],XmNheight,650);l_ac++;
    if(min) {
        XtSetArg(al[l_ac], XmNiconic, TRUE); l_ac++;
    }


    g->topLevel = XtAppCreateShell ("OFSI Viewport", "XRelax",applicationShellWidgetClass,l_display, al,l_ac);

    l_ac = 0;
    g->mainWindow = XmCreateMainWindow(g->topLevel,(char*)"mainWindow",al,l_ac);
    XtManageChild(g->mainWindow);
    g->menuBar = CreateViewMenuBar(g->mainWindow,g);
    XtManageChild(g->menuBar);

    CreateMsgArea(g);
    l_ac = 0;
    sprintf(buf,"?driver/%s",p_Hoops_Driver);
    g->workArea = MakeNewDrawingArea(g->mainWindow,"camerabase",g,g_World->GetHBaseModel());
    XmMainWindowSetAreas(g->mainWindow,g->menuBar,NULL,NULL,NULL,g->workArea);
    l_ac=0;
    XtSetArg(al[l_ac],XmNmessageWindow,(XtArgVal) g->msgform);
    l_ac++;
    XtSetValues(g->mainWindow,al,l_ac);

    wm_delete_window = XmInternAtom(XtDisplay(g->topLevel),
                                    (char*)"WM_DELETE_WINDOW",
                                    FALSE);

    XmAddWMProtocolCallback(g->topLevel, wm_delete_window, ViewDeleteCB, (XtPointer) g);
    return 1;
}

#endif
/*
open camera :   astern  : genoa$shellname=pansail dp,inc_list=panel edges+strings+stripes+,mesh=off,lines=off,text=off,faces=on,hard contours=on,markers=off,graphstate=off : main$shellname=pansail dp,inc_list=strings+stripes+,mesh=off,lines=on,text=off,faces=on,hard contours=on,markers=off,graphstate=off : 
*/
#ifdef _X
Widget OpenView(const char*LineIn,const char *p_Driver)
#else
void * OpenView(const char*LineIn,const char *p_Driver)
#endif

{
    int i;
    char sailname[1256], options[1256], driver[1256],buf[8192];

    char**words=NULL;
    char *l_thisWord;
    int  nw =0, remote=0;
    RXEntity_p cEnt;
    char *line = NULL;
    if(LineIn) line = STRDUP(LineIn);
    RXSubWindowHelperPostProc *gfree=new RXSubWindowHelperPostProc();
            //gfree = nextGraphic(WT_VIEWWINDOW);
#ifdef HOOPS
    HC_QSet_User_Options("/","shellname=none,inc_list=strings+,visibility="); // panel edges are nice but (Jan 2006) they crash

    strcpy(driver,p_Driver);
    if(line)
        nw = make_into_words(line,&words,":\t");
    for(i=0;i<nw-1;i++){
        if(strieq(words[i],"driver")) {
            i++;
            strcpy(driver,words[i]);
            remote=1;
            if(i<nw-1) {
                PC_Strip_Trailing(driver);
                strcat(driver,":");
                i++; strcat(driver,words[i]);
            }

        }
    }
#endif
#ifdef _X
    if(!remote) {
        create_top_camera(gfree,g_graph[0].topLevel,driver);
        XtPopup(gfree->topLevel,XtGrabNone);
    }
    else {  if(OVDB) printf(" trying to open remotely on '%s'\n ",driver);   // something like ?driver/X11/othermachine:0
        if(! create_remote_top_camera(gfree,driver)){ gfree->EMPTY=1; return 0; }
    }


    XFlush(XtDisplay(gfree->topLevel));
#endif
#ifdef HOOPS
    HC_Show_Segment(gfree->m_ModelSeg ,buf);  if(OVDB) printf("OpenView with m_ModelSeg %s\n",buf);
    HC_Open_Segment_By_Key(gfree->m_ModelSeg );
    HC_Set_User_Options("relaxtype=camerabase");
#endif
#ifdef _X
    char l_buf[512],l_type[512]; HC_KEY l_k ;
    HC_Begin_Contents_Search("(.,*,...)","distant light");
    while(HC_Find_Contents(l_type,&l_k)) {
        HC_Show_Owner_By_Key(l_k,l_buf);
        if(OVDB) printf("Under ModelSeg, light <%s> in <%s>\n",l_type,l_buf);
        HC_Delete_By_Key(l_k);

    }
    HC_End_Contents_Search();


    HC_Close_Segment();
    HC_Open_Segment_By_Key(gfree->m_ViewSeg);
    HC_Set_User_Index(RXCLASS_GRAPHICSTRUCT, gfree);
    HC_Set_User_Options("relaxtype=camerabase");

    HC_Begin_Contents_Search("(.,*,...)","distant light");
    while(HC_Find_Contents(l_type,&l_k)) {
        HC_Show_Owner_By_Key(l_k,l_buf);
        //printf("under ViewSeg, delete light <%s> in <%s>\n",l_type,l_buf);
        HC_Delete_By_Key(l_k);
    }
    HC_End_Contents_Search();

#endif
    HC_Close_Segment();
    // turn off the default lights
#ifdef HOOPS
    HBaseView *l_hbv = gfree->HBV;

    HC_Open_Segment_By_Key( l_hbv->GetLightsKey()  );
    HC_Show_Pathname_Expansion(".",buf);// printf("lights Key is <%s>\n",buf);
    HC_Close_Segment();
    l_hbv->SetLightFollowsCamera(false);
#endif

    if(nw) {
        if(nw > 1){
            l_thisWord= words[1];
            if((cEnt=PC_Get_Key_Across_Models("camera",l_thisWord))) {
                SetHardCamera(cEnt,gfree);  // sets it on Viewseg.

                for(i=2;i<nw;i++ ) {
                    l_thisWord = words[i];  if(OVDB)printf(" %d %s\n",i,l_thisWord);
                    {
                        char localbuf[4096];
                        strcpy(localbuf,l_thisWord);
                        HC_Define_System_Options("C string length=1000");
#ifdef HOOPS
                        if(HC_Parse_String(localbuf,"$",0,sailname)&& HC_Parse_String(localbuf,"$",1,options)) {
                            PC_Strip_Leading(options); if(*options==',' ) *options=' ';
                            assert(strlen(options) < 1000);
                            HC_Open_Segment_By_Key(gfree->m_ModelSeg);
                            HC_Open_Segment("models");
                            HC_Open_Segment(sailname);
                            //printf(" setting UOs <%s>\n",options);
                            HC_Set_User_Options(options);
                            char buffer[1256]; HC_Show_Pathname_Expansion(".",buffer);
                            //printf(" OpenView setting <%s> in %s\n",options,buffer);
                            HC_Show_One_Net_User_Option("visibility",options);
                            PC_Replace_Char(options,'"',' ');
                            if(!is_empty(options)) { //printf(" visibility = %s\n",options);
                                HC_Set_Visibility(options);
                            }
                            HC_Close_Segment();
                            HC_Close_Segment();
                            HC_Close_Segment();
                        }
                        else  { // its a text or driver or network word.
                            char *lp;
                            if(strieq(localbuf,"text") && i < nw-1){
                                HC_Open_Segment_By_Key(gfree->m_OverlaySeg);
                                HC_Open_Segment("text_overlay"); // in m_OverlaySeg
                                lp = words[i+1];
                                if(strlen(lp ) > 1020) { puts(" truncating text overlay"); lp[1020]=0;}
                                sprintf(buf,"user_text=%s",words[i+1]);
                                PC_Replace_Char(buf, '~', '\n');

                                i++; HC_Set_User_Options(buf);
                                //printf(" User Options <%s>\n",buf);
                                HC_Close_Segment();
                                HC_Close_Segment();
                            }
                            else if(strieq(localbuf,"driver")  && (i < nw-1)) {
                                if(i == nw-3 && !is_empty(words[nw-1]) ) {
                                    if(OVDB) printf("Driver (2word)  <%s:%s>\n",words[i],words[i+1]);
                                    i+=2;
                                }
                                else {
                                    i++;  if(OVDB) printf(" Driver (one word) %s\n", words[i]);
                                }

                            }
                            else if(strieq(localbuf,"network")  && (i < nw-1)) {
                                printf(" network request\n");
                                if(i == nw-3 && !is_empty(words[nw-1]) ) {
                                    printf("network (2word)  <%s:%s>\n",words[i],words[i+1]);
                                    i+=2;
                                }
                                else {
                                    i++; printf(" Network (one word) %s\n", words[i]);
                                }

                            }
                            else printf(" (OpenView): %s not understood\n",localbuf);
                        }
#endif
                    }


                }// for
            } // if cEnt
        } // if parse
        RXFREE(words);
    }
    HC_Define_System_Options("C string length=255");

    TouchAllViewSegments();
#ifdef HOOPS
    Finish_This_View(gfree) ;
    HC_Update_Display();
#ifdef OLDHNETLINK	
    static int novs =0;
    sprintf(buf,"OfsiView_%d",novs++);
    //this is manual
    if(g_rxcs)
        g_rxcs->StartNewManager(buf,0,gfree->m_ModelSeg);
#endif
#endif
#ifdef _X
    XmUpdateDisplay(gfree->mainWindow);	// Athena Case 1_4A crash was here.. HC_Insert_Grid problematic
#endif
    gfree->GSail= NULL;// should this be sooner?
    if(line) RXFREE(line);
#ifdef _X
    return(gfree->mainWindow);
#else
    return 0;
#endif
}
#ifdef HOOPS
int TouchAllViewSegments() {
    //	for each  camera segment
    // for each hoisted sail open the segment in the camera to make sure it exists
    int i,j;
    char buf[1256];
    Graphic *gv,*gs;
    for(i=0;i<g_hoops_count;i++)  {
        gv = &g_graph[i];
        if(gv->EMPTY)
            continue;
        if(is_empty(gv->filename) )
            continue;
        if(!strstr(gv->filename,"Viewport:"))
            continue;
        assert(gv->GType==WT_VIEWWINDOW);
        for(j=0;j<g_hoops_count ;j++)  {
            gs = &g_graph[j];
            if(gs->EMPTY )
                continue;
            if(!gs->GSail)
                continue;
            if(OVDB) printf("TOUCHING view with model %s\n",gs->GSail->GetType().c_str());
            HC_Open_Segment_By_Key(gv->m_ModelSeg);
            HC_Open_Segment("models");
            HC_Open_Segment(gs->GSail->GetType().c_str());
            HC_Set_User_Index(RXCLASS_SAIL,gs->GSail);
            HC_Show_Pathname_Expansion(".",buf);
            if(OVDB) printf(" setting UI_SAIL %p on %s\n",gs->GSail,buf);
            HC_Close_Segment();
            HC_Close_Segment();
            HC_Close_Segment();
        } // for j
    } // for i
    return 1;
}
int UnTouchViewSegments(SAIL *sail) {
    //	for each  camera segment, clears the UI on

    int i,count=0;
    Graphic *gv,*gs;
    gs = sail->Graphic();
    assert(!gs->EMPTY ) ;
    if(!(gs->filename[0] && (!( strstr(gs->filename,"Viewport:") ) ) ))
        printf("Untouching viewseg on sail %s without gs->filename\n", sail->GetType().c_str());

    for(i=0;i<g_hoops_count;i++)  {
        gv = &g_graph[i];
        if(gv->EMPTY)
            continue;
        if(is_empty(gv->filename) )
            continue;
        if(!strstr(gv->filename,"Viewport:"))
            continue;

        assert(gs->GSail);
        if(OVDB) printf(" UNTOUCHING with model %s\n", gs->GSail->GetType().c_str());
        count++;
#ifdef HOOPS
        HC_Open_Segment_By_Key(gv->m_ModelSeg);
        HC_Open_Segment("models");
        HC_Open_Segment(gs->GSail->GetType().c_str());
        HC_UnSet_One_User_Index(RXCLASS_SAIL);
        HC_Close_Segment();
        HC_Close_Segment();
        HC_Close_Segment();
#endif
    } // for i
    if(count) Finish_All_Views();
    return 1;
}
#endif
#ifdef _X
void ViewDeleteCB(Widget w, XtPointer client_data, XtPointer call_data)
{
    Graphic *g = (Graphic *) client_data;

    if(!g) { puts(" ViewDeleteCB on NULL g");
        return;
    }

    if(g->EMPTY)
        return;  /* already been here */


    g->filename[0] = '\0';
    g->EMPTY = 1;


}//void ViewDeleteCB

#endif






