/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


#include "StdAfx.h"
#include "RXSail.h"
#include "traceback.h"
#include "GraphicStruct.h"
#include "Global.h"
#include "Dialog.h"
#include "Xm/FileSB.h"
#include "stringutils.h"
#include "words.h"
#include "sailutil.h"

#include "akmutil.h"

#include <sys/types.h>
#include <sys/stat.h>

#include "global_declarations.h"

#include "getfilename.h" 

static char *theFile;
static Widget open_dialog;

static int NOT_DONE;

static XmStringCharSet charset = (XmStringCharSet) XmSTRING_DEFAULT_CHARSET;
				/* used to set up XmStrings */

static XmString openString;
static XmString openTitleString;
static XmString patternString;
static XmString dirString;
static XmString promptString;

int FileDialog = 1;

int filename_copy_ssd(char *out,const char *in) { // the version in debug.cpp is better

char *a = STRDUP(in);
	 Replace_String(&a,"$R2ROOT",g_RelaxRoot);
	 Replace_String(&a,"$R2CODE", g_CodeDir);
	 Replace_String(&a,"$RWD", g_workingDir);
	strcpy(out,a);
	RXFREE(a);
return 1;

}//int filename_copy


char *getfilename(Widget parent,const char *patternin,const char *promptin,const char *dirin, const int ReadWrite)
{

if(!parent) {
	//	print_trace();
	//g_World->OutputToClient ("get filename with null parent  (shouldnt fail)",1);
	}
/*  the flag ReadWrite introduced 7/2000 for compatibility with MSW but Not used */
XtInputMask xmask;
// char *bpat="*";
//char *bprompt="Open File";
int directory_only=0;
SAIL *sail=0;
char pattern[256], prompt[256],dir[256];
	Arg al[64];
	int l_ac=0;
/* check parameters and use defaults if necessary */

	if(!patternin)
  		strcpy(pattern,"*");
	else
		filename_copy_local(pattern,256,patternin);

	if(!promptin)
  		strcpy(prompt ,"Open File");
	else
		strcpy(prompt,promptin);

	if(!dirin)
		strcpy(dir , g_currentDir);
	else
		filename_copy_local(dir,256,dirin);



/*   Get the filename   */
#ifndef linux
	theFile = (char *) MALLOC(256);
	fprintf(stdout,"Enter file name ('%s') : ",pattern);
	gets(theFile);
	fprintf(stdout,"\nEcho: '%s'\n",theFile);
	return(theFile);
#else
if(openString != NULL)
	{XmStringFree(openString);  openString=NULL;}
if(openTitleString != NULL)
	{XmStringFree(openTitleString);openTitleString=NULL;  }
if(patternString != NULL)
	{XmStringFree(patternString); patternString =NULL; }
if(promptString != NULL)
	{XmStringFree(promptString);  promptString =NULL; } 

openString = XmStringCreateSimple((char*) "Open");
openTitleString = XmStringCreateSimple((char*)"Open File");
patternString = XmStringCreateSimple(pattern);
promptString = XmStringCreateSimple(prompt);
dirString = XmStringCreateSimple(dir);
l_ac=0; 
if(stristr(prompt,"directory")) {
	directory_only = 1;
	XtSetArg (al[l_ac], XmNfileTypeMask,XmFILE_DIRECTORY);  l_ac++;  
}
else {
	XtSetArg (al[l_ac], XmNfileTypeMask,XmFILE_REGULAR);  l_ac++;  
}
XtSetArg (al[l_ac], XmNx,5);  l_ac++;  
XtSetArg (al[l_ac], XmNy, 5);  l_ac++;  
XtSetArg (al[l_ac], XmNdialogStyle, XmDIALOG_APPLICATION_MODAL);  l_ac++;  // Nov 2002 could try it NOT modal
XtSetArg( al[l_ac], XmNokLabelString, openString ); l_ac++;
XtSetArg( al[l_ac], XmNdialogTitle, promptString ); l_ac++;
XtSetArg( al[l_ac], XmNpattern, patternString ); l_ac++;
XtSetArg( al[l_ac], XmNdirectory, dirString ); l_ac++;
if(!open_dialog) {
  if(!parent) {
    if(sail) 
 	parent = sail->Graphic()->topLevel;

  }
   if(!parent)  /*   peter changed 2/5/99 in case there was garbage in lastGraph */
      	parent = g_graph[0].topLevel;
 
  if(!parent)  {
  	print_trace ();
  	  g_World->OutputToClient ("problem with getfile\n null parent",2); 
	 return (NULL);
	}
  
  open_dialog = XmCreateFileSelectionDialog(parent,prompt, al, l_ac);
  
  XtAddCallback(open_dialog, XmNokCallback,(XtCallbackProc)okCB, (XtPointer) NULL);
  XtAddCallback(open_dialog, XmNcancelCallback,(XtCallbackProc)cancelCB, (XtPointer) NULL);
}
else 
  XtSetValues(open_dialog,al,l_ac);

XtManageChild(open_dialog);

NOT_DONE = 1;
theFile = NULL;

	XmUpdateDisplay(parent);		// Peter test 7 Jan 2003 
 	XFlush(XtDisplay(parent));		// Peter test 7 Jan 2003 

while(NOT_DONE) {
	xmask = XtAppPending(g_app_context);
			if(xmask) 
	XtAppProcessEvent(g_app_context,xmask);
}
if(theFile && !directory_only) {
	if(is_directory(theFile)) {
		theFile = NULL;
	}
}

//XmUpdateDisplay(g_graph[0].topLevel);

 XmUpdateDisplay(parent);		// Peter test 7 Jan 2003 

HC_Update_Display();
return(theFile);
#endif
}//char *getfilename


/*-------------------------------------------------------------
**	okCB
**		Process callback from Dialog actions.
*/
/* ARGSUSED */
void okCB(Widget w,		/*  widget id		*/
		  XtPointer client_data,	/*  data from application   */
		  XtPointer call_data)	/*  data from widget class  */
{
  static char *filename=NULL;
  XmFileSelectionBoxCallbackStruct *fcb;

        if (filename != NULL) {
	   XtFree (filename);
	   filename = NULL;
         }
         fcb = (XmFileSelectionBoxCallbackStruct *) call_data;
   

  /* get the filename from the file selection box */
         XmStringGetLtoR(fcb->value, charset, &filename);
    

  /* popdown the file selection box */  	 
        XtUnmanageChild(open_dialog);
	XtDestroyWidget(open_dialog);
	open_dialog = NULL;

	if(filename)
		theFile = STRDUP(filename);
	NOT_DONE = 0;
}//void okCB

/*-------------------------------------------------------------
**	cancelCB
**		Process callback from Dialog cancel actions.
*/
void cancelCB(Widget	w,		/*  widget id		*/
			  XtPointer client_data,	/*  data from application   */
			  XtPointer call_data)	/*  data from widget class  */
{
      /* popdown the file selection box */
        XtUnmanageChild(open_dialog);
	XtDestroyWidget(open_dialog);
	open_dialog = NULL;
	theFile = NULL;

	NOT_DONE = 0;
}//void cancelCB



