



#include "StdAfx.h"
/* CreateMenuBar func. - Sailmenu */

#include "Global.h"
#include "Menu.h"
#include "custom.h"

#include "dirmenu.h" // for PC_File_Search
#include "global_declarations.h"
#include "stringutils.h"

//#include <string.h>

// new boat, view, workspace, sail


int Create_NewMenu(Widget pulldownPane,Graphic *g){ 

CreateMenuButton(pulldownPane,NULL,MENU_File_New,"Boat...",'N',PushButton,NULL,NULL,g);
CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_NEW,"Viewport",'N',PushButton,NULL,NULL,g);
CreateMenuButton(pulldownPane,NULL,MENU_Sail_New,"Sail From Script",'N',PushButton,NULL,NULL,g);
Insert_Custom_Buttons("NewSubmenu",pulldownPane, g);
AddSeparator(pulldownPane);
	
char dirbuf[256];
char **files = NULL;
char *lp;
int i,N=0,L;
 if(is_empty(templateDir)) { g_World->OutputToClient (" directory setup",2); return 0;}
 sprintf(dirbuf,"%s/*.in",templateDir );
 L = strlen(templateDir)+1;
PC_File_Search(&files,&N,dirbuf);

for(i=0;i< N ;i++) {
	lp = strrchr(files[i],'.'); if(lp) *lp=0;
	lp = files[i]; lp +=L;

	 CreateMenuButton(pulldownPane,NULL,MENU_SAIL_NEW_SUBMENU,lp,'1',PushButton,NULL,NULL,g);
	RXFREE(files[i]);
}

RXFREE(files); 

return(1);
}


int Create_OpenMenu(Widget pulldownPane,Graphic *g){


CreateMenuButton(pulldownPane,NULL,MENU_RUN_SCRIPT,"Session",'M',PushButton,NULL,NULL,g);
CreateMenuButton(pulldownPane,NULL,MENU_FILE_CWD,"Workspace",'A',PushButton,NULL,NULL,g);
CreateMenuButton(pulldownPane,NULL,MENU_File_Open,"Boat",'O',PushButton,NULL,NULL,g);
CreateMenuButton(pulldownPane,NULL,MENU_Sail_Open,"Sail Bag",'O',PushButton,NULL,NULL,g);
CreateMenuButton(pulldownPane,NULL,MENU_Sail_New,"Sail From Script",'N',PushButton,NULL,NULL,g);
CreateMenuButton(pulldownPane,NULL,MENU_UTILS_OPEN_SUMMARY,"Logfile",'O',PushButton,NULL,NULL,g);CreateMenuButton(pulldownPane,NULL,MENU_UTILS_OPEN_IN,"Logfile as Input",'I',PushButton,NULL,NULL,g);



Insert_Custom_Buttons("OpenSubmenu",pulldownPane, g);	

return(1);
}


