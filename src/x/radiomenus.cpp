/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 */
// function to put up dialog box and then get /set current ExpOpts. 

#include "StdAfx.h"

#include "Global.h"

#include "Menu.h"
#include "Xm/Protocols.h"
#include "Xm/PushB.h"
#include "Xm/ToggleB.h"
#include "Xm/DialogS.h"
#include "hoopcall.h"

#include "getglobal.h"
#include "stringutils.h"
#include "global_declarations.h"

#include "radiomenus.h"

#define DEBUG (0)
#define OKPRESS 20
#define CANCELPRESS 21
#define NROWDIM 20

static int NROWS=10; // ten for pdf../x/txt_over.cpp 
static Widget dgForm,dgShell;

 
int DoExpOptDlg(Widget parent,Graphic *g) { // works on g_Export_Options;

	Widget OKbutton,Cbutton;

	int top,i;
	Atom	wm_delete_window;
	Position x,y;
	Arg al[64];
	int l_ac=0;
	XtSetArg(al[l_ac], XtNx, &x);l_ac++;
	XtSetArg(al[l_ac], XtNy, &y);l_ac++;
	XtGetValues(parent,al,l_ac);

	l_ac=0;

XtSetArg (al[l_ac], XmNdialogStyle, XmDIALOG_FULL_APPLICATION_MODAL);  l_ac++;  

XtSetArg(al[l_ac], XtNallowShellResize, FALSE); l_ac++;
XtSetArg(al[l_ac], XmNresizable, FALSE); l_ac++;
XtSetArg(al[l_ac], XmNnoResize, TRUE); l_ac++;
XtSetArg(al[l_ac], XtNwidth, 136);l_ac++;
XtSetArg(al[l_ac], XtNheight, 20*NROWS+40);l_ac++;
XtSetArg(al[l_ac], XmNx, 10);l_ac++;
XtSetArg(al[l_ac], XmNy, 10);l_ac++;

dgShell = XmCreateDialogShell(parent,(char*)"Export Options",al,l_ac);

dgForm = XmCreateForm(dgShell,(char*)"dialog",al,l_ac);
    /*
     * intern the atoms and add mwm_messages to wm_protocols so that
     * mwm will look for mwm_messages property changes. Also add a
     * callback for handling WM_DELETE_WINDOW for when the user
     * selects the close button.
     */

    
    wm_delete_window = XmInternAtom(XtDisplay(dgShell),
				(char*)"WM_DELETE_WINDOW",
				FALSE);
    
    XmAddWMProtocolCallback(dgShell, wm_delete_window, ExpOptQuitCB, (XtPointer) g);

top = 8;

for(i=0;i<NROWS;i++) {
	l_ac=0;
	XtSetArg(al[l_ac],XmNfillOnSelect, True);l_ac++;
	XtSetArg(al[l_ac],XmNindicatorOn, True);l_ac++;
	XtSetArg(al[l_ac],XmNindicatorType, XmN_OF_MANY);l_ac++;
	XtSetArg(al[l_ac],XmNvisibleWhenOff, True);l_ac++;
	XtSetArg(al[l_ac],XmNtopAttachment,XmATTACH_FORM);l_ac++;
	XtSetArg(al[l_ac],XmNleftAttachment,XmATTACH_FORM);l_ac++;
	XtSetArg(al[l_ac],XmNleftOffset,30);l_ac++;
	XtSetArg(al[l_ac],XmNtopOffset,top);l_ac++;
	
	if (stristr(g_Export_Options,g_Expname[i]))  {
		XtSetArg(al[l_ac],XmNset,True); 	l_ac++;}
	else {
		XtSetArg(al[l_ac],XmNset,False); 	l_ac++; }
	g_visButton[i] = XmCreateToggleButton(dgForm,(char*)g_Expname[i],al,l_ac);
	XtAddCallback(g_visButton[i],XmNvalueChangedCallback,(XtCallbackProc)expopt_activateCB,mkID(i,g));
	XtManageChild(g_visButton[i]);
top += 20;
}

	l_ac=0;
	XtSetArg(al[l_ac],XmNtopAttachment,XmATTACH_FORM);l_ac++;
	XtSetArg(al[l_ac],XmNleftAttachment,XmATTACH_FORM);l_ac++;
	XtSetArg(al[l_ac],XmNleftOffset,12);l_ac++;
	XtSetArg(al[l_ac],XmNtopOffset,top);l_ac++;
	XtSetArg(al[l_ac],XmNmarginRight,10);l_ac++;
	XtSetArg(al[l_ac],XmNmarginLeft,10);l_ac++;
	
	OKbutton = XmCreatePushButton(dgForm,(char*)"OK",al,l_ac);
	XtAddCallback(OKbutton,XmNactivateCallback,(XtCallbackProc)expopt_activateCB,mkID(OKPRESS,g));
	XtManageChild(OKbutton);
	
	l_ac=0;
	XtSetArg(al[l_ac],XmNtopAttachment,XmATTACH_FORM);l_ac++;
	XtSetArg(al[l_ac],XmNleftAttachment,XmATTACH_FORM);l_ac++;
	XtSetArg(al[l_ac],XmNleftOffset,65);l_ac++;
	XtSetArg(al[l_ac],XmNtopOffset,top);l_ac++;
	XtSetArg(al[l_ac],XmNmarginRight,5);l_ac++;
	XtSetArg(al[l_ac],XmNmarginLeft,5);l_ac++;
	
	Cbutton = XmCreatePushButton(dgForm,(char*)"Cancel",al,l_ac);
	XtAddCallback(Cbutton,XmNactivateCallback,(XtCallbackProc)expopt_activateCB,mkID(CANCELPRESS,g));
	XtManageChild(Cbutton);

XtManageChild(dgForm);
return(1);
}

void expopt_activateCB(Widget w,caddr_t client_data,caddr_t call_data) {
//  XmToggleButtonCallbackStruct *tcs;
  Graphic *g;
	
  switch(((MenuID *)client_data)->id) {
     	case OKPRESS: {
  		g = (Graphic *) ((MenuID *)client_data)->g;
		ExpOptQuitCB(w,(XtPointer) g,(XtPointer) NULL);
		break;
	}
     	case CANCELPRESS: {
		
		XtUnmanageChild(dgForm);
		XtDestroyWidget(dgForm);
		XtUnmanageChild(dgShell);
		XtDestroyWidget(dgShell);
		break;
	}
	default: {		/* first button */
 	/* 	tcs = (XmToggleButtonCallbackStruct *) call_data;
    		printf("Button %d now set to %d\n",(int) client_data, tcs->set);*/	
	break;
	}
  }
}

void ExpOptQuitCB(Widget w,XtPointer client_data, XtPointer call_data){
	int i;
	char string[512];
	*string = 0;
	for(i=0;i<NROWS;i++) {
		if(XmToggleButtonGetState(g_visButton[i])) {
			strcat(string,g_Expname[i]); 
			strcat(string," ,"); 
			assert(strlen(string) < 480);
		}
	}
	PC_Strip_Trailing_Chars(string," ,");

	strcpy(g_Export_Options,string);
// ex: Export_Options : "jpg,dxf" 
	sprintf(string, "Export Options: ""%s"" ", g_Export_Options);
	printf(" GOT <%s> \n", string);
	Append_Defaults_File(string);
}
