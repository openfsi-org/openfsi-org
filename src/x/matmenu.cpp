/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by	P Heppel
 *
 * 
 *
 */

#include "StdAfx.h"

#include "custom.h"
#include "Global.h"

#include "global_declarations.h"

#include "Menu.h"

#include "matmenu.h"

Widget CreateMatMenuBar(Widget parent,Graphic *g) 
{
Widget menuBar;
Widget cascade;
Widget pulldownPane,submenuRotate;
	Arg al[64];
	int l_ac=0;
XmString label;

/* Create menu bar */

menuBar = XmCreateMenuBar(parent,(char*)"menuBar",al,l_ac);
/*****************************************************************************/
/***************************   FILE MENU   ***********************************/
/*****************************************************************************/

pulldownPane = XmCreatePulldownMenu(menuBar,(char*)"pulldownPane",al,l_ac);
/**** new button *****/
CreateMenuButton(pulldownPane,NULL,MENU_MAT_NEW,(char*)"New..",'N',PushButton,NULL,NULL,g);

/**** open button *****/
XtSetSensitive(CreateMenuButton(pulldownPane,NULL,MENU_MAT_OPEN,(char*)"Open...",'O',PushButton,NULL,NULL,g),0);


AddSeparator(pulldownPane);

/**** print button ***
**/
CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_PRINT,(char*)"Print...",'P',PushButton,NULL,NULL,g);

/**** save button *****/
XtSetSensitive(CreateMenuButton(pulldownPane,NULL,MENU_MAT_SAVE,(char*)"Save ",'S',PushButton,NULL,NULL,g),0);

/**** save as button *****/
CreateMenuButton(pulldownPane,NULL,MENU_MAT_SAVEAS,(char*)"Save As",'A',PushButton,NULL,NULL,g);

AddSeparator(pulldownPane);

/* export... button */

CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_EXPORT_HMF,(char*)"Export",'E',PushButton,NULL,NULL,g);
CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_DEBUG_1,(char*)"Exp Opts",'O',PushButton,NULL,NULL,g);
Insert_Custom_Buttons("matFile",pulldownPane, g);

/**** close button *****/
CreateMenuButton(pulldownPane,NULL,MENU_MAT_CLOSE,(char*)"Close",'C',PushButton,NULL,NULL,g);

/*** File Cascade button ***/
label = XmStringCreateSimple("File");
l_ac=0;
XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
XtSetArg(al[l_ac],XmNmnemonic,'F');l_ac++;
cascade = XmCreateCascadeButtonGadget(menuBar,NULL,al,l_ac);
XtManageChild(cascade);
XmStringFree(label);

/*****************************************************************************/
/***************************   VIEW MENU   ***********************************/
/*****************************************************************************/
l_ac=0;
pulldownPane = XmCreatePulldownMenu(menuBar,"pulldownPane",al,l_ac);


/**** New button *****/
g->attributeButton = CreateMenuButton(pulldownPane,NULL,MENU_VIEW_ATTRIBUTES,"Set Attributes",'A',PushButton,NULL,NULL,g);

AddSeparator(pulldownPane);

l_ac=0;
submenuRotate = XmCreatePulldownMenu(pulldownPane,"submenuRotate",al,l_ac);
/**** Left 22.5 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_LEFT22,"Rotate Left 22.5",'L',PushButton,NULL,NULL,g);

/**** Right 22.5 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_RIGHT22,"Rotate Right 22.5",'R',PushButton,NULL,NULL,g);

/**** Left 90 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_LEFT90,"Rotate Left 90",'e',PushButton,NULL,NULL,g);

/**** Right 90 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_RIGHT90,"Rotate Right 90",'i',PushButton,NULL,NULL,g);

/**** Up 22.5 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_UP22,"Rotate Up 22.5",'U',PushButton,NULL,NULL,g);

/**** Down 22.5 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_DOWN22,"Rotate Down 22.5",'D',PushButton,NULL,NULL,g);

/**** Up 90 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_UP90,"Rotate Up 90",'p',PushButton,NULL,NULL,g);

/**** Down 90 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_DOWN90,"Rotate Down 90",'o',PushButton,NULL,NULL,g);

label = XmStringCreateSimple("Rotate");
l_ac=0;
XtSetArg(al[l_ac],XmNsubMenuId,submenuRotate);l_ac++;
XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
XtSetArg(al[l_ac],XmNmnemonic,'o');l_ac++;
cascade = XmCreateCascadeButtonGadget(pulldownPane,NULL,al,l_ac);
XtManageChild(cascade);
XmStringFree(label);




/**** Zoom In button *****/
CreateMenuButton(pulldownPane,NULL,MENU_VIEW_ZOOMIN,"Zoom Window",'Z',PushButton,NULL,NULL,g);


AddSeparator(pulldownPane);

/**** Resize button *****/
CreateMenuButton(pulldownPane,NULL,MENU_VIEW_RESIZE,"Zoom Extents",'R',PushButton,NULL,NULL,g);
AddSeparator(pulldownPane);

/**** Resize button *****/
CreateMenuButton(pulldownPane,NULL,MENU_MAT_COLOURCHART,"Colours",'C',PushButton,NULL,NULL,g);

Insert_Custom_Buttons("matView",pulldownPane, g);

/*** File Cascade button ***/
label = XmStringCreateSimple("View");
l_ac=0;
XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
XtSetArg(al[l_ac],XmNmnemonic,'V');l_ac++;
cascade = XmCreateCascadeButtonGadget(menuBar,NULL,al,l_ac);
XtManageChild(cascade);
XmStringFree(label);


/*****************************************************************************/
/***************************   DEBUG MENU   ***********************************/
/*****************************************************************************/
#ifdef MAT_DEBUG_MENU // dont want this
l_ac=0;
pulldownPane = XmCreatePulldownMenu(menuBar,"pulldownPane",al,l_ac);
/**** New button *****/
/* CreateMenuButton(pulldownPane,NULL,MENU_DEBUG_WRITEALL,"Write root.hmf",'W',PushButton,NULL,NULL,g); */
CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_EXPORT_HMF,"Write This .hmf",'W',PushButton,NULL,NULL,g);

Insert_Custom_Buttons("matDebug",pulldownPane, g);

/*** File Cascade button ***/
label = XmStringCreateSimple("Debug");
l_ac=0;
XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
XtSetArg(al[l_ac],XmNmnemonic,'D');l_ac++;
cascade = XmCreateCascadeButtonGadget(menuBar,NULL,al,l_ac);
XtManageChild(cascade);
XmStringFree(label);

#endif
 
XtManageChild(menuBar);
return(menuBar);

} /* endo of CreateMenuBar()  */

