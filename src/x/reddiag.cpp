/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"
#include "RXSail.h"
/* NOTES:
 * 
 *
 */



#include <X11/Xatom.h>
#include <X11/Intrinsic.h>
#include <X11/Shell.h>

#include <Xm/Xm.h>
#include <Xm/Protocols.h>
#include <Xm/List.h>
#include <Xm/DialogS.h>
#include <Xm/Form.h>
#include <Xm/Frame.h>
#include <Xm/Label.h>
#include <Xm/PushB.h>
#include <Xm/ScrollBar.h>
#include <Xm/ScrolledW.h>
#include <Xm/TextF.h>
#include <Xm/LabelG.h>


#include "stringutils.h"

#include "griddefs.h"
#include "elements.h"
#include "controlmenu.h"
#include "mast.h"
#include "GraphicStruct.h"
#include "resolve.h"
#include "resdiag.h"
#include "global_declarations.h"
#include "global_declarations.h"
#include "redcb.h"
#include "reddiag.h"

#ifndef VERT_BUTTON_SPACE
	#define VERT_BUTTON_SPACE  ((int) (20))
#endif //#ifndef VERT_BUTTON_SPACE

#ifndef RIGHT_OF_LIST  
#define RIGHT_OF_LIST  ((int) (300))
#endif //#ifndef RIGHT_OF_LIST  



//static Widget Parent = (Widget)NULL;
	/* main parent - later to be used to only generate 
	* once and hence save a bit of time / effort */



Widget create_rename_shell(SAIL *p_sail,Widget parent,const char *filter,const char *prompt)
{
	Display *display;
	Widget children[100];      /* Children to manage */
	Arg al[64]; 
	Cardinal l_ac=0;
	Widget w_shell = (Widget)NULL;	/* main shell  */
	Widget w_form = (Widget)NULL;	/* main form  */
	Widget w_list = (Widget)NULL;	/* list widget */
	Widget w_listSW = (Widget)NULL;	/* list widget */

	Widget wb_ok = (Widget)NULL;	/* buttons */
	Widget wb_cancel = (Widget)NULL;

	Widget w_list_label = (Widget) NULL;	/* label */

	XmString *Items=NULL; 		/* For list items */
	int nitems;        		/* Index for list_items */
	XmString xmstrings[115];    	/* temporary storage for XmStrings */
	int i;
	Atom	wm_delete_window;
	char buf[1128];
	ResData *rd;
	int BlockSize;	/* size of memory block for items */
	RXEntity_p ep=0;  RXEntity_p *elist=0;
	l_ac=0;

	rd = (ResData *) MALLOC(sizeof(ResData));

	display = XtDisplay ( parent );

	XtSetArg (al[l_ac], XmNdialogStyle, XmDIALOG_FULL_APPLICATION_MODAL);l_ac++;
	XtSetArg(al[l_ac], XmNallowShellResize, FALSE); l_ac++;
	XtSetArg(al[l_ac],XmNtitle,"Choose Item"); l_ac++;
	w_shell = XmCreateDialogShell ( parent, (char*) "rename_shell", al, l_ac );
	l_ac = 0;

	/* This makes 'closing' the window == aborting the rename generation */
        wm_delete_window = XmInternAtom(XtDisplay(w_shell),
				(char*)"WM_DELETE_WINDOW",
				FALSE);
    
        XmAddWMProtocolCallback(w_shell, wm_delete_window, (XtCallbackProc)rename_abort_CB, (XtPointer) rd);



	XtSetArg(al[l_ac], XmNautoUnmanage, FALSE); l_ac++;
	w_form = XmCreateForm ( w_shell,(char*) "rename_form", al, l_ac );
	l_ac = 0;

	/* create the text label */
	
	xmstrings[0] = XmStringCreateLtoR((char*) prompt, (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac], XmNlabelString, xmstrings[0]); l_ac++;
	w_list_label = XmCreateLabel ( w_form,(char*) "rename_label", al, l_ac );
	l_ac = 0;
	XmStringFree ( xmstrings [ 0 ] );


/* add code here to generate the item list */
	BlockSize = 50;
// WRONG	items = CALLOC(BlockSize,sizeof(XmString));
	Items = (XmString *) XtCalloc (BlockSize, sizeof (XmString) );

	elist = (RXEntity_p *)CALLOC(BlockSize,sizeof(RXEntity_p ));
	nitems=0;
	ent_citer it;

    for (it = p_sail->MapStart(); it != p_sail->MapEnd(); it=p_sail->MapIncrement(it)) {
	RXEntity_p ep  = it->second;
	if(stristr(filter,ep->type())) {
            	sprintf(buf,"%s (%s)",ep->name(), ep->type());
	    	Items[nitems] = XmStringCreateLtoR(buf, (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);	
	    	elist[nitems] = ep;
		nitems++;
	    	if(nitems == BlockSize) {
		    BlockSize += 50;
		    Items = (XmString*) XtRealloc((char*) Items,sizeof(XmString)*BlockSize);
		    elist = (RXEntity_p *)REALLOC(elist,sizeof(RXEntity_p )*BlockSize);
	    	}
	    }
	}

 
	XtSetArg(al[l_ac], XmNitemCount, nitems); l_ac++;
	XtSetArg(al[l_ac], XmNitems, Items); l_ac++;
	XtSetArg(al[l_ac], XmNselectionPolicy, XmSINGLE_SELECT); l_ac++;
	XtSetArg(al[l_ac], XmNlistSizePolicy, XmCONSTANT); l_ac++;
	XtSetArg(al[l_ac], XmNscrollBarDisplayPolicy, XmAS_NEEDED); l_ac++;
	XtSetArg(al[l_ac], XmNvisibleItemCount, 12); l_ac++;
	w_list = XmCreateScrolledList ( w_form, (char*)"rename_list",al,l_ac);
	l_ac=0;
	
	w_listSW = XtParent(w_list);

	/* create the push buttons */

	xmstrings[0] = XmStringCreateLtoR((char*)" OK ", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac], XmNlabelString, xmstrings[0]); l_ac++;
	wb_ok = XmCreatePushButton ( w_form,(char*) "rename_button", al, l_ac );
	l_ac = 0;
	XmStringFree ( xmstrings [ 0 ] );

	xmstrings[0] = XmStringCreateLtoR((char*)" Cancel ", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac], XmNlabelString, xmstrings[0]); l_ac++;
	wb_cancel = XmCreatePushButton ( w_form, (char*) "rename_button", al, l_ac );
	l_ac = 0;
	XmStringFree ( xmstrings [ 0 ] );



/* 	Now give each a means of positioning iteself relative to 
	either another widget or the form.
 */
	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNtopOffset, 10); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_NONE); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, 10); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_NONE); l_ac++;

        XtSetValues ( w_list_label,al, l_ac );
	l_ac = 0;


	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_WIDGET); l_ac++;
	XtSetArg(al[l_ac], XmNtopOffset, 100); l_ac++;
	XtSetArg(al[l_ac], XmNtopWidget, w_list_label); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_NONE); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, RIGHT_OF_LIST+20); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNrightOffset, 20); l_ac++;
        XtSetValues ( wb_ok,al, l_ac );
	l_ac = 0;

	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_WIDGET); l_ac++;
	XtSetArg(al[l_ac], XmNtopOffset, VERT_BUTTON_SPACE); l_ac++;
	XtSetArg(al[l_ac], XmNtopWidget, wb_ok); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNbottomOffset, 10); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, RIGHT_OF_LIST+20); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNrightOffset, 20); l_ac++;
        XtSetValues ( wb_cancel,al, l_ac );
	l_ac = 0;

	children[l_ac++] = w_list_label;
	XtManageChildren(children, l_ac);
	l_ac=0;

	children[l_ac++] = wb_ok;
	children[l_ac++] = wb_cancel;
	XtManageChildren(children, l_ac);
	l_ac=0;


	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_WIDGET); l_ac++;
	XtSetArg(al[l_ac], XmNtopOffset, 10); l_ac++;
	XtSetArg(al[l_ac], XmNtopWidget, w_list_label); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_OPPOSITE_WIDGET); l_ac++;
	XtSetArg(al[l_ac], XmNbottomWidget, wb_cancel); l_ac++;
	XtSetArg(al[l_ac], XmNbottomOffset, 0); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, 10); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_OPPOSITE_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNrightOffset, -RIGHT_OF_LIST); l_ac++;
        XtSetValues ( w_listSW,al, l_ac );
	l_ac = 0;

	XtAddCallback(w_list, XmNsingleSelectionCallback, (XtCallbackProc)rename_single_select_CB,  (XtPointer)rd);
	XtAddCallback(w_list, XmNdefaultActionCallback, (XtCallbackProc)rename_ok_CB,  (XtPointer)rd);
	XtAddCallback(wb_ok, XmNactivateCallback, (XtCallbackProc)rename_ok_CB,  (XtPointer)rd);
	XtAddCallback(wb_cancel, XmNactivateCallback, (XtCallbackProc)rename_abort_CB,  (XtPointer)rd);

	children[l_ac++] = w_list;
	XtManageChildren(children, l_ac);
	l_ac=0;

	children[l_ac++] = w_listSW;
	XtManageChildren(children, l_ac);
	l_ac=0;

	children[l_ac++] = w_form;
	XtManageChildren(children, l_ac);
	l_ac=0;

	children[l_ac++] = w_shell;
	XtManageChildren(children, l_ac);
	l_ac=0;

	XtRealizeWidget(w_shell);
	XFlush(XtDisplay(w_shell));

	    rd->shell = w_shell;
	    rd->select = wb_ok;
	    rd->append = NULL;
	    rd->scan = NULL;
	    rd->rename = NULL;
	    rd->abort = wb_cancel;
	    rd->chosen = -1;
	    rd->m_g=0;
	    rd->m_ri=0;
	    rd->m_elist = elist;
	    rd->Items = (char *) Items;

//	XtAddGrab(w_shell,True,False); try removing this Aug 2003

return(w_shell);
}



