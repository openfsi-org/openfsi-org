#include "StdAfx.h"
#include "RelaxWorld.h"
#include "Menu.h"
#include "script.h"
#include "stringutils.h"

#include "custom.h"

struct Custom_Button  g_buttons[MAXCUSTOM ];
int buttoncount =0;

int Insert_Custom_Buttons(const char*what,Widget pulldownPane, Graphic *g) {
struct Custom_Button  *b;
int i;
for(i=0;i<MAXCUSTOM;i++) {
	b = &(g_buttons[i]); if(!b->used) continue; 
	if( (!b->where) || !strieq(b->where,what)) continue;
	AddSeparator(pulldownPane);
//	CreateMenuButton      (pulldownPane,NULL,MENU_CUSTOM+i,b->label,b->hotkey,PushButton,NULL,NULL,g);
	CreateMenuButtonForLP(pulldownPane,NULL,MENU_TEXT_CB,b->label,b->hotkey,PushButton,NULL,NULL,g,b->action);
	b->g = g;
}
return 1;
}

int nextbuttonindex( ) {
int j;
struct Custom_Button *b;
	for(j=0;j<buttoncount;j++) {
		b = &(g_buttons[j]); 
		if(!b->used) return j;
	}
	if(buttoncount<MAXCUSTOM-1) {buttoncount++; return (buttoncount-1);}
return -1;
}
int Create_Custom_Button(char*line) {
 /*  custom button : where: label : hotkey;  action; */
struct Custom_Button *b, *b2;
int i,j;
char str[256];
	i = nextbuttonindex(); if(i<0) { g_World->OutputToClient (" out of buttons",3); return 0;}
	b =&( g_buttons[i]); 
	if (HC_Parse_String(line,":",1,str)) {
		b->where = STRDUP(str);
		if (HC_Parse_String(line,":",2,str)) {
			b->label = STRDUP(str);
			if (HC_Parse_String(line,":",3,str)) {
				b->hotkey=str[0];
				if (HC_Parse_String(line,":",4,str)) {
					b->action =  STRDUP(str);
					strrep(b->action ,'"',' ');
					b->used=1;
				//printf(" custom button %s %s %c  %s\n", b->where, b->label, b->hotkey, b->action ); 

					for(j=0;j<buttoncount;j++) {	// look for duplicates and skip
						b2 = &(g_buttons[j]); 
						if(b2==b) continue;
						if(!b2->used) continue;
						if(!b2->label) continue;
						if(!b2->where) continue;
						if(strieq(b2->label, b->label) && strieq(b2->where,b->where)) {
							//printf("Duplicate button!!!!%s\n",b->label);
							b->used = 0;
						}
					}
				} 
				else b->action =NULL;
			} else b->hotkey= ' ';
		}
		else b->label = NULL;
	} 
	else b->where = NULL;

if(b->used) return 1;
if(b->where) RXFREE( b->where);
if(b->label) RXFREE( b->label);
if(b->action ) RXFREE( b->action);
b->where = NULL; b->label=NULL; b->action = NULL;
return 1;
}


