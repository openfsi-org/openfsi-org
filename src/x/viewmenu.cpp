/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 * June 1997 changed the word 'Camera' to 'Viewport'
 *1 /10/96 Previous Camera
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h" 

#include "Menu.h"
#include "Global.h"

#include "menuCB.h"
#include "camera.h"
#include "custom.h"
#include "sailmenu.h"

#include "global_declarations.h"

#include "viewmenu.h"
	/* kept to enable / disable items later */


Widget CreateViewMenuBar(Widget parent,Graphic *g)
{
Widget menuBar;
Widget cascade;
Widget pulldownPane,submenuRotate,submenuCameras;
//Widget button;
//MenuID *id;
int i;

XmString label;
Arg al[64]; 
/* Create menu bar */
int l_ac=0;
menuBar = XmCreateMenuBar(parent,(char*)"menuBar",al,l_ac);
/*****************************************************************************/
/***************************   FILE MENU   ***********************************/
/*****************************************************************************/
l_ac=0;
pulldownPane = XmCreatePulldownMenu(menuBar,(char*)"pulldownPane",al,l_ac);
/**** new button *****/
CreateMenuButton(pulldownPane,(char*)NULL,MENU_CAMERA_NEW,(char*)"New Viewport",(int)'N',PushButton,(char*)NULL,(char*)NULL,g);

/**** open button *****/
//CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_OPEN,"Open Viewport",'O',PushButton,NULL,NULL,g);

//AddSeparator(pulldownPane);

/**** save button *****/
XtSetSensitive(CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_SAVE,"Save Camera",'S',PushButton,(char*) NULL,(char*)NULL,g),0);

/**** save as button *****/
CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_SAVEAS,"Save Camera As",'A',PushButton,(char*)NULL,(char*)NULL,g);

AddSeparator(pulldownPane);

/**** print button *****/
//CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_PRINT,"Print",'P',PushButton,NULL,NULL,g);
//AddSeparator(pulldownPane);

/**** export button *****/
CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_EXPORT_HMF,"Export",'E',PushButton,NULL,NULL,g);
CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_DEBUG_1,"Exp Opts",'O',PushButton,NULL,NULL,g);
AddSeparator(pulldownPane);

/* Custom Buttons */
Insert_Custom_Buttons("Viewfile",pulldownPane, g);

/**** close button *****/
CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_CLOSE,"Close",'C',PushButton,NULL,NULL,g);

/*** File Cascade button ***/
label = XmStringCreateSimple((char*)"File");
l_ac=0;
XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
XtSetArg(al[l_ac],XmNmnemonic,'F');l_ac++;
cascade = XmCreateCascadeButtonGadget(menuBar,NULL,al,l_ac);
XtManageChild(cascade);
XmStringFree(label);

/*****************************************************************************/
/***************************   VIEW MENU   ***********************************/
/*****************************************************************************/
l_ac=0;
pulldownPane = XmCreatePulldownMenu(menuBar,(char*)"pulldownPane",al,l_ac);

/* generate the rotate submenu */
l_ac=0;
submenuCameras = XmCreatePulldownMenu(pulldownPane,(char*)"submenuCameras",al,l_ac);
SetCameraList();
	CreateMenuButton(submenuCameras,NULL,MENU_PREVIOUS_CAMERA,"Previous",'z',PushButton,NULL,NULL,g);
for(i=0;i<g_nCams;i++) {
	/**** New button *****/
	CreateMenuButton(submenuCameras,NULL,MENU_CAMERAS+i,g_camPlist[i]->name(),'z',PushButton,NULL,NULL,g);
}


label = XmStringCreateSimple((char*)"Preset Cameras");
l_ac=0;
XtSetArg(al[l_ac],XmNsubMenuId,submenuCameras);l_ac++;
XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
XtSetArg(al[l_ac],XmNmnemonic,'P');l_ac++;
cascade = XmCreateCascadeButtonGadget(pulldownPane,NULL,al,l_ac);
XtManageChild(cascade);
XmStringFree(label);

/* generate the rotate submenu */
l_ac=0;
submenuRotate = XmCreatePulldownMenu(pulldownPane,(char*)"submenuRotate",al,l_ac);
/**** Left 22.5 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_LEFT22,"Rotate Left 22.5",'L',PushButton,NULL,NULL,g);

/**** Right 22.5 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_RIGHT22,"Rotate Right 22.5",'R',PushButton,NULL,NULL,g);

/**** Left 90 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_LEFT90,"Rotate Left 90",'e',PushButton,NULL,NULL,g);

/**** Right 90 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_RIGHT90,"Rotate Right 90",'i',PushButton,NULL,NULL,g);

/**** Up 22.5 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_UP22,"Rotate Up 22.5",'U',PushButton,NULL,NULL,g);

/**** Down 22.5 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_DOWN22,"Rotate Down 22.5",'D',PushButton,NULL,NULL,g);

/**** Up 90 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_UP90,"Rotate Up 90",'p',PushButton,NULL,NULL,g);

/**** Down 90 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_DOWN90,"Rotate Down 90",'o',PushButton,NULL,NULL,g);

label = XmStringCreateSimple((char*)"Rotate");
l_ac=0;
XtSetArg(al[l_ac],XmNsubMenuId,submenuRotate);l_ac++;
XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
XtSetArg(al[l_ac],XmNmnemonic,'o');l_ac++;
cascade = XmCreateCascadeButtonGadget(pulldownPane,NULL,al,l_ac);
XtManageChild(cascade);
XmStringFree(label);


/**** New button *****/
CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_UP,"Restore Vertical",'U',PushButton,NULL,NULL,g);

/**** New button *****/
//CreateMenuButton(pulldownPane,NULL,MENU_VIEW_WIREFRAME,"Wireframe",'W',CheckBox,NULL,NULL,g);

//AddSeparator(pulldownPane);



/**** Zoom In button *****/
CreateMenuButton(pulldownPane,NULL,MENU_VIEW_ZOOMIN,"Zoom Window",'Z',PushButton,NULL,NULL,g);
//AddSeparator(pulldownPane);


/**** Resize button *****/
CreateMenuButton(pulldownPane,NULL,MENU_VIEW_RESIZE,"Zoom Extents",'E',PushButton,NULL,NULL,g);

/* Custom Buttons */
Insert_Custom_Buttons("ViewView",pulldownPane, g);

/*** File Cascade button ***/
label = XmStringCreateSimple((char*)"View");
l_ac=0;
XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
XtSetArg(al[l_ac],XmNmnemonic,'V');l_ac++;
cascade = XmCreateCascadeButtonGadget(menuBar,NULL,al,l_ac);
XtManageChild(cascade);
XmStringFree(label);



/*****************************************************************************/
/***************************   OPTIONS MENU   ***********************************/
/*****************************************************************************/
l_ac=0;
pulldownPane = XmCreatePulldownMenu(menuBar,(char*)"pulldownPane",al,l_ac);



/**** New button *****/
g->attributeButton = CreateMenuButton(pulldownPane,NULL,MENU_OPTIONS_SET,"Set",'O',PushButton,NULL,NULL,g);

AddSeparator(pulldownPane);

/**** Text Overlay button *****/
CreateMenuButton(pulldownPane,NULL,MENU_OPTIONS_OVERLAY,"Text Overlay",'T',PushButton,NULL,NULL,g);

/*** File Cascade button ***/
label = XmStringCreateSimple((char*)"Options");
l_ac=0;
XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
XtSetArg(al[l_ac],XmNmnemonic,'O');l_ac++;
cascade = XmCreateCascadeButtonGadget(menuBar,NULL,al,l_ac);
XtManageChild(cascade);
XmStringFree(label);

/* Custom Buttons */
Insert_Custom_Buttons("ViewOptions",pulldownPane, g);


/*****************************************************************************/
/***************************   DEBUG MENU   ***********************************/
/*****************************************************************************/
#ifdef DEBUG_MENU

l_ac=0;
pulldownPane = XmCreatePulldownMenu(menuBar,(char*)"pulldownPane",al,l_ac);
/**** New button *****/
CreateMenuButton(pulldownPane,NULL,MENU_DEBUG_WRITEALL,"Write .hmf",'W',PushButton,NULL,NULL,g);
CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_READ_HMF,"Read .hmf",'W',PushButton,NULL,NULL,g);
XtSetSensitive(CreateMenuButton(pulldownPane,NULL,MENU_SAIL_LIST_ENTITY,"List Entity",'L',PushButton,NULL,NULL,g),1);

CreateMenuButton(pulldownPane,NULL,MENU_VIEW_SET_LIGHT,"Insert Light",'L',PushButton,NULL,NULL,g);

CreateMenuButton(pulldownPane,NULL,MENU_VIEW_SET_COLOR,"Root Colour",'L',PushButton,NULL,NULL,g);

/**** Edit camera button *****/
CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_EDIT,"Edit Camera",'E',PushButton,NULL,NULL,g);

/**** debug  button *****/


/* Custom Buttons */
Insert_Custom_Buttons("ViewDebug",pulldownPane, g);

/*** File Cascade button ***/
label = XmStringCreateSimple((char*)"Debug");
l_ac=0;
XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
XtSetArg(al[l_ac],XmNmnemonic,'D');l_ac++;
cascade = XmCreateCascadeButtonGadget(menuBar,NULL,al,l_ac);
XtManageChild(cascade);
XmStringFree(label);

#endif
XtManageChild(menuBar);
return(menuBar);

} /* endo of CreateMenuBar()  */

