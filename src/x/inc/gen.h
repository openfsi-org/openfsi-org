//gen.h : header for gen.cpp
//Thomas 08 06 04

#ifndef HX_GEN
#define HX_GEN

Widget create_boat_shell (Widget parent);
EXTERN_C int Do_Generate_Boat(char *line,struct GRAPHIC_STRUCT *g);

EXTERN_C int set_one_data_item (int n);

#define MAX_VALUES 17
#define VAL_LEFT_OFFSET 200
#define MAIN_HEIGHT 660
#define MAIN_WIDTH 760

#endif //#define HX_GEN

