//opentext.h header for opentext.cpp
//Thomas 11 06 04 

#ifndef RX_OPENTEXT_H
#define RX_OPENTEXT_H

#ifdef _X
#include "Global.h" // for X definitions
EXTERN_C Widget OpenText(const char *data,const char *name);
#endif
#endif //#ifndef RX_OPENTEXT_H
