#ifndef _TXT_OVER_STUBS_H_
#define _TXT_OVER_STUBS_H_

#ifdef _X
//#inc lude "GraphicStruct.h"

EXTERN_C XtCallbackProc pop_overlay_box(Graphic *g,Widget parent);
EXTERN_C XtCallbackProc pop_up_overlay_CB (Widget w,XtPointer client_data,XtPointer call_data);
EXTERN_C XtCallbackProc pop_down_overlay_CB (Widget w,XtPointer client_data,XtPointer call_data);
EXTERN_C XtCallbackProc text_over_ok_CB (Widget w,XtPointer client_data,XmPushButtonCallbackStruct *call_data);
EXTERN_C XtCallbackProc text_over_cancel_CB (Widget w,XtPointer client_data,XmPushButtonCallbackStruct *call_data);
EXTERN_C XtCallbackProc show_toggle_changed_CB (Widget w,XtPointer client_data,XmToggleButtonCallbackStruct *call_data);
#endif
#endif  //#ifndef _TXT_OVER_STUBS_H_


