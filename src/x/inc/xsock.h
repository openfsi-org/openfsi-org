#ifndef _H_XSOCK_
#define _H_XSOCK_


#include <stdlib.h>
#include <stdio.h>
#include <poll.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>

#include <Xm/Xm.h>	/* Intrinsics Definitions */

typedef  void (*SocketClosedCB)(void *xs); /* used for callback from installing program */

struct XSDATA{
  SocketClosedCB cb;		/* callback  */
  XtInputId exceptID;		/* id for input proc */
  XtInputId inputID;		/* id for exception proc */
  int fid;			/* socket id no. */
  int id;			/* unique id for this attemp */
  char ip_name[256];
};
typedef struct XSDATA XSockData;


#define MSGSIZ  (20000)		/* max buffer size for transmit */

				/* below is macro for commands */
#define mkTok(ss)  (ss[0]*128+ss[1]*64 + ss[2]*32 + ss[3]*16)

/* a command is the first word of a string delimited by tab, space of end of line ( \r or \n)
 */
#define RESE (114*128+101*64+115*32+101*16) /* reset message buffer */
#define QUIT (113*128+117*64+105*32+116*16) /* quit */
#define SEND (115*128+101*64+110*32+100*16) /* add data to existing buffer */
#define ADCR (97*128 +100*64+ 99*32+114*16) /* append a CR to the buffer */
#define ATAB (97*128 +116*64+ 97*32+ 98*16) /* append a TAB to the buffer */
#define WRIT (119*128+114*64+105*32+116*16) /* write buffer to file */
#define PASS (112*128+97*64+115*32+115*16) /* pass rest of buffer to command procesor */
#define TALK (116*128+97*64+108*32+107*16) /* just drivel a bit */
#define OKOK (111*128+107*64+111*32+107*16) /* ignore */


/* call to start processing commands */
/* returns > 0 if successful else < 0 means failure */
/* if successfull returns a unique ID identifying this attempt to install
 * socket command processing which is passed as an argument to the 
 * callback supplied.
 */

XSockData *  InstallSocketInterface(XtAppContext theApp,char *ip_name,int port,SocketClosedCB cb);
int UnInstallSocketInterface(XSockData *xs);
#endif  //#ifndef _H_XSOCK_

