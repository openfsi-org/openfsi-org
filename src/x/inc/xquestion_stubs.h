#ifndef _XQUESTION_STUBS_H_
#define _XQUESTION_STUBS_H_


void qtext_create_cb (Widget w,XtPointer client_data,XmAnyCallbackStruct *call_data);
// thomas 10 06 04 void q_apply_cb (Widget w,XtPointer client_data,XmPushButtonCallbackStruct *call_data); 

//void  pop_up_question_CB (Widget w, XtPointer client_data,XmAnyCallbackStruct *call_data);
void  qtext_value_changed_cb ( Widget w, XtPointer client_data, XmAnyCallbackStruct *call_data);

void  qselect_cb ( Widget w, XtPointer client_data, XmPushButtonCallbackStruct *call_data);

void  qtext_focus_cb ( Widget w, XtPointer client_data, XmAnyCallbackStruct *call_data);
void  pop_up_question_CB ( Widget w,XtPointer client_data,XtPointer call_data);
void  qthis_default_cb ( Widget w, XtPointer client_data, XmPushButtonCallbackStruct *call_data);
void  all_defaults_cb ( Widget w, XtPointer client_data, XmPushButtonCallbackStruct *call_data);

void pop_down_question_CB (Widget w,XtPointer client_data, XtPointer call_data);
void q_apply_cb (Widget w,XtPointer client_data,XmPushButtonCallbackStruct *call_data);
void qdestroy_question_shell_CB (Widget w,XtPointer client_data,XtPointer call_data); 
void qcancel_cb (Widget w,XtPointer client_data,XmPushButtonCallbackStruct *call_data);
void  qtext_destroy_cb (  Widget w, XtPointer client_data, XtPointer call_data);
void  qText_losing_focus_CB ( Widget w, XtPointer client_data, XmTextVerifyCallbackStruct *call_data);



#endif  //#ifndef _XQUESTION_STUBS_H_
