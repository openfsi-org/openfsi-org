//onerow_stubs.h  header for onerow_stubs.cpp
//Thomas 08 06 04

#ifndef RX_ONEROW_STUBS
#define RX_ONEROW_STUBS

void pressure_method_cb (Widget w,XtPointer client_data,XmPushButtonCallbackStruct *call_data);
void pressure_values_cb (Widget w,XtPointer client_data,XmToggleButtonCallbackStruct *call_data);

#endif  //#ifndef RX_ONEROW_STUBS
