#ifndef _REDCB_H_
#define _REDCB_H_
/* created 15/3/97 as a result of HP  port */


RXEntity_p choose_database_item(SAIL *p_sail,Widget parent,const char *filter,const char *prompt);

void rename_close_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data);
void rename_abort_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data);
void rename_single_select_CB(Widget w,caddr_t client_data,XmListCallbackStruct *call_data);
void rename_ok_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data);


#endif //#ifndef _REDCB_H_
