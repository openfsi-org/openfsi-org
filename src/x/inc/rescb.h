#ifndef _RESCB_H_
#define _RESCB_H_


int do_resolve_dialog(Graphic *g);
#ifdef _X
void resolve_close_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data);

void resolve_abort_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data);
void single_select_CB(Widget w,caddr_t client_data,XmListCallbackStruct *call_data);
void resolve_scan_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data);
void resolve_select_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data);
void resolve_rename_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data);
void resolve_append_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data);
#endif
#endif  //#ifndef _RESCB_H_
