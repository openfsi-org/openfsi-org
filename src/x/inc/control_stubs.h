//control_stubs.h: header for control_stubs.cpp

#ifndef RX_CONTROL_STUBS
#define RX_CONTROL_STUBS

void wind_cb (Widget w,XtPointer client_data,XmPushButtonCallbackStruct *call_data);
void wake_cb (Widget w,XtPointer client_data,XmPushButtonCallbackStruct *call_data);
void options_cb (Widget w, XtPointer client_data, XmPushButtonCallbackStruct *call_data );
void ncycle_value_cb (Widget w, XtPointer client_data, XmAnyCallbackStruct *call_data );
void activate_button_cb (Widget w, XtPointer client_data, XmPushButtonCallbackStruct * call_data );
int print_control_data(control_data_t *data);
int delete_control_structures(Widget co_dlg);

#endif //#ifndef RX_CONTROL_STUBS
