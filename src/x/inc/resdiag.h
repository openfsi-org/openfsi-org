/* header file for resdiag.c and rescb.c */

	
#ifndef RESDIAG_16NOV04
#define RESDIAG_16NOV04

struct RESDATA {
	Widget shell;
	Widget select;
	Widget append;
	Widget scan;
	Widget rename;
	Widget abort;
	int chosen;
	//void *m_data; // see resdiag 322 this is a Graphic  
	Graphic *m_g;
	struct REPORT_ITEM *m_ri;
	RXEntity_p *m_elist;
	//or ( rescp.cpp 225 ) a (REPORT_ITEM*)
	//  or ( reddiag.cpp:272 ) an entity list
	char  *Items; //ph was void* Oct 2002
};
typedef struct RESDATA ResData;

#endif //#ifndef RESDIAG_16NOV04

	

