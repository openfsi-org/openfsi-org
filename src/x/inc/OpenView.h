#ifndef  _OPENVIEW_H_
#define _OPENVIEW_H_


#ifdef _X
EXTERN_C void ViewDeleteCB(Widget w, XtPointer client_data, XtPointer call_data);
EXTERN_C Widget OpenView(const char*line, const char *p_Driver);
EXTERN_C void create_top_camera (Graphic *g,Widget parent, const char *p_driver);
# else
#include <QStringList>
EXTERN_C void* OpenView(QStringList &line, const char *p_Driver);
#endif
//EXTERN_C int GraphicsSegmentSpecialCopy(Graphic *g);
EXTERN_C int TouchAllViewSegments() ;
EXTERN_C int UnTouchViewSegments(SAIL *sail) ;
#endif//#ifndef  _OPENVIEW_H_

