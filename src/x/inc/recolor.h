

#ifndef RECOLOR_16NOV04
#define RECOLOR_16NOV04


char *setColor(char *color);
char *getColor(void);
char *readColor(void);


EXTERN_C int ReColor;
EXTERN_C FILE *ReColFP;

EXTERN_C int MovingSite;
EXTERN_C int  Delete_OK;


#define  LIST_ENTITY 		1
#define  DELETE_ENTITY 		2
#define  EDIT_ENTITY 		4
#define  ENTITY_MATERIAL		8



#endif //#ifndef RECOLOR_16NOV04
