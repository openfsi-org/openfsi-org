//#ifndef _WINDOWS

#pragma once


 #include "controlmenu.h"  //  bad style

EXTERN_C void set_up_whole_trimmenu (void );
//EXTERN_C int  IncrementRunName(void);
//EXTERN_C int  ModifyRunName(const char*p_new);
EXTERN_C int  post_spline_data(class RXDatabaseI *db,RXEntity_p ent);
EXTERN_C int  apply_string_trims(int cnt,StringItem *slist);

#ifdef _X


#include "trimmenu.h"

EXTERN_C XtCallbackProc edit_changed_CB (Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data);
EXTERN_C XtCallbackProc edit_losing_focus_CB (Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data);
EXTERN_C void up_arrow_cb (Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data);
EXTERN_C void down_arrow_cb (Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data);
EXTERN_C void mast_trim_cb(Widget w,XtPointer client_data,XmAnyCallbackStruct *call_data);
EXTERN_C int  apply_string_trims(int cnt,StringItem *slist);
EXTERN_C int  add_lab_l_r(m_rowcol_p p_rc);
EXTERN_C int  add_edit_entry(editable_form_p edit_f);
EXTERN_C int  add_entry_row(string_form_p string_form);
EXTERN_C int  remove_lab_l_r(m_rowcol_p p_rc);
EXTERN_C int  apply_mast_trims(m_rowcol_p m_row);
EXTERN_C int  apply_edit_trims(int cnt,EditItem *slist);

#endif

