/* menu definition file */

#ifndef _HDR_MENU_
#define _HDR_MENU_ 1

//#inc lude "GraphicStruct.h"

typedef  struct{
   void *g;
   int id;
	char *lp;
   } MenuID;



#define PushButton	0
#define CheckBox	1
#define CheckBoxOn	2
#define RadioButton	3
#define RadioButtonOn	4
#define CascadeButton	5


#define MENU_File_New      	101
#define MENU_File_Open     	102
#define MENU_BOAT_SAVE 	103
#define MENU_BOAT_SAVEAS	104
#define MENU_RUN_SCRIPT	105
#define MENU_RECORD_SCRIPT	109
#define MENU_END_SCRIPT	119
#define MENU_SNAPSHOT		120

#define MENU_FILE_EXIT		106


#define MENU_SAVE_STATEAS	107
#define MENU_LOAD_STATE_BY_INTERP	118

#define MENU_SAIL_NEW		110	
#define MENU_SAIL_OPEN		111
#define MENU_SAIL_SAVE		112
#define MENU_SAIL_SAVEAS	113
#define MENU_SAIL_CLOSE		114
#define MENU_SAIL_APPEND	115
#define MENU_IMPORT_DXF         116
#define MENU_FILE_CWD         117
#define MENU_SAIL_NEW_SUBMENU 3100

#define MENU_Sail_New	201
#define MENU_Sail_Open	202
#define MENU_EXPORT_PANELS 203

#define MENU_CAMERA_NEW	301
#define MENU_CAMERA_OPEN	302
#define MENU_CAMERA_SAVE 303
#define MENU_CAMERA_SAVEAS 304
#define MENU_CAMERA_CLOSE 305
#define MENU_CAMERA_PRINT 306
#define MENU_CAMERA_EXPORT_HMF		307
#define MENU_CAMERA_EDIT     308
#define MENU_CAMERAS 309
#define MENU_PREVIOUS_CAMERA 	350
#define MENU_CAMERA_UP 	351
#define MENU_CAMERA_READ_HMF		352
#define  MENU_CAMERA_DEBUG_1 	353
#define  MENU_CAMERA_DEBUG_2 	354



#define MENU_ATTACH_SAIL	401 
#define MENU_DROP_SAIL	402 

//#define MENU_QUANTITY_SHAPE	411

/*
#define MENU_BUILD_PANELSOLVE	501
#define MENU_BUILD_MESH		502
*/
#define MENU_AERO_BASE		600
#define MENU_AERO_WIND		601
#define MENU_AERO_PANSAIL	602
#define MENU_AERO_RELAXII	603
#define MENU_AERO_NONE		604
#define MENU_AERO_CUSTOM        609
#define MENU_AERO_CUSTOM2        610
#define MENU_AERO_CONTROLS	605
#define MENU_DO_STRLIN 		606
#define MENU_USE_PRESTRESS 		607
#define MENU_SET_PRESTRESS 		608
#define MENU_RESET_ONE_EDGES    611
#define MENU_RESET_ALL_EDGES    612
#define MENU_SHRINK_ONE_EDGES    614
#define MENU_SHRINK_ALL_EDGES    615
#define MENU_FORM_FIND    616



#define MENU_ANALYSIS_TRIM	701
#define MENU_ANALYSIS_AUTOCALC	702
#define MENU_ANALYSIS_CALCNOW	703
#define MENU_ANALYSIS_SETSOLVEALL	704
#define MENU_ANALYSIS_KILL	705
#define MENU_ANALYSIS_VPPACTIVE	706
#define MENU_ANALYSIS_VPPPASSIVE	707

#define MENU_SAIL_BUILD_MESH		500
#define MENU_SAIL_BUILD_PANELSOLVE	501
// #define MENU_SAIL_BUILD_SETFABRIC	502
#define MENU_SAIL_BUILD_DELETE		503
#define MENU_SAIL_BUILD_MOVESITE	504
#define MENU_SAIL_BUILD_ASK_QUEST	505
#define MENU_SAIL_LIST_ENTITY		506
#define MENU_SAIL_MATERIAL_ENTITY	507
#define MENU_VIEW_ATTRIBUTES 210
#define MENU_VIEW_WIREFRAME 230
#define MENU_VIEW_ZOOMIN	211
#define MENU_VIEW_RESIZE		212	
#define MENU_VIEW_ROTATE_LEFT22		213
#define MENU_VIEW_ROTATE_RIGHT22		214
#define MENU_VIEW_ROTATE_LEFT90		215
#define MENU_VIEW_ROTATE_RIGHT90		216
#define MENU_VIEW_ROTATE_UP22		217
#define MENU_VIEW_ROTATE_DOWN22		218
#define MENU_VIEW_ROTATE_UP90		219
#define MENU_VIEW_ROTATE_DOWN90		220
#define MENU_VIEW_ADJUST_CONTOURS       221

#define MENU_VIEW_SET_LIGHT 	 222
#define MENU_VIEW_SET_COLOR     223
#define MENU_VIEW_ZOOMOUT	224


#define MENU_OPTIONS_SET 801
#define MENU_OPTIONS_OVERLAY  802


#define MENU_DEBUG_WRITEALL		901
#define MENU_DEBUG_FINDBATONS		902
#define MENU_DEBUG_PRINTALLENTITIES      903
#define MENU_DEBUG_DISPLAYNODES      904
#define MENU_DEBUG_SYSTEM      905
#define MENU_DEBUG_1      906
#define MENU_DEBUG_2      907
#define MENU_DEBUG_3      910
#define MENU_DEBUG_4      914
#define MENU_LOAD_DEFFILE	915
#define MENU_EDIT_DEFFILE	916


#define MENU_EDIT_SEAMCURVE 912
#define MENU_READ_PRESSURES      911
#define MENU_RELSITE_EDITS 921
#define MENU_SEAMCURVE_EDITS 922


#define MENU_SET_COLOR      908
#define MENU_RECOLOR      909

#define MENU_FILE_CASCADE 1001
#define MENU_SAIL_CASCADE 1002
#define MENU_VIEW_CASCADE 1003
#define MENU_BUILD_CASCADE 1004
#define MENU_ANALYSIS_CASCADE 1005


#define MENU_TRIM_NEW		1110	
#define MENU_TRIM_OPEN		1111
#define MENU_TRIM_SAVE		1112
#define MENU_TRIM_SAVEAS	1113
#define MENU_TRIM_CLOSE		1114
#define MENU_TRIM_PRINT         1115

#define MENU_MAT_NEW		1210	
#define MENU_MAT_OPEN		1211
#define MENU_MAT_SAVE		1212
#define MENU_MAT_SAVEAS	1213
#define MENU_MAT_CLOSE	1214
#define MENU_MAT_COLOURCHART	1215


#define MENU_UTILS_OPEN_SUMMARY   2001
#define MENU_UTILS_OPEN_IN   2002
#define MENU_UTILS_CHOOSE_RUN   2003
#define MENU_UTILS_CLOSE_SUMMARY   2004

#define MENU_OPTIMISE_INIT	3001
#define MENU_OPTIMISE_LOAD_HISTORY	3002
#define MENU_OPTIMISE	3003

#define  MENU_TEXT_CB    3900

void *mkID(int id,void *g);
void *mkIDLP(int id,void *g,const char *lp);

void AddSeparator(Widget parent);

EXTERN_C Widget CreateMenuBar(Widget parent,Graphic *g);

Widget CreateMenuButtonForLP(Widget parent,const char *name,const int bID,const char *bLabelStr,int bMnemonic,int bType,const char *bAcc,const char *bAccTextStr,Graphic*g, const char *lp);

Widget CreateMenuButton(Widget parent,const char *name,const int butID,const char *butLabelStr,char butMnic,const int butType,const char *butAcc,const char *buttonAccTextStr,Graphic *g);

void AddSeparator(Widget parent);
int Set_Kill_Sensitivity(int i);
int Set_USelect_Sensitivity(int i);
EXTERN_C int Set_Demo_Sensitivity(int i); 

#endif  //#ifndef _HDR_MENU_
