#ifndef  _CH_ROW_CB_H_
#define _CH_ROW_CB_H_
#ifndef _X

 	typedef void *XmListCallbackStruct ;
	typedef void * XmAnyCallbackStruct ;
	typedef void * XtPointer ;
	typedef void * XmPushButtonCallbackStruct;
	typedef void  XtCallbackProc;
#endif

void ch_action_cb (Widget w, XtPointer client_data,XmPushButtonCallbackStruct *call_data);
void ch_singleSelection_cb (Widget w, XtPointer client_data, XmListCallbackStruct *call_data );


#endif //#ifndef  _CH_ROW_CB_H_
