//control.h header for control.cpp  (changed from pc_control.h by Thomas 08 06 04)
/* control menu structure */
/* A K Molyneaux         20.1.95         Created   */
/*
 * 	26/2/96 Force units there appears to be an error
 *
 */
#ifndef _HDR_CONTROL_
#define _HDR_CONTROL_ 1

//

#ifdef _X
        #inc lude "xcon trol.h" // for the data types
#endif

#define RELAX_PRESSURE   (1)
#ifdef USE_PANSAIL
#define PANSAIL_PRESSURE (2)
#else
#define PANSAIL_PRESSURE (-1)
#define WERNER_PRESSURE (2)
#endif
#define CONSTANT_PRESSURE (4)
#define FILE_PRESSURE (8)
#define CUSTOM_PRESSURE (16)
#define SCRIPT_PRESSURE (32)
#define FIELD_PRESSURE (64)
#define INTERPOLATE_PRESSURE (128)

#define NON_LINEAR_FLAG (1)
#define CREASING_FLAG (2)
#define RE_USE_MASS_FLAG (4)
#define EXTRA_FLAG (8)

#define WAKE_TOGGLE  (1)
#define MATRIX_TOGGLE (2)
#define CONSTANT_TEXT (4)
#define NRELAX_TEXT (8)
#define OPT_BUTTON (16)

#define USE_OLD_WAKES  (1)
#define USE_OLD_MATRIX (2)


#define INITIAL_RUN (1)
#define TRIM_CHANGE (2)
#define MASS_SET_FLAG (4)
#define WIND_CHANGE (8)


#define mkFlag(G,f)   ((G) |= (f))
#define clFlag(G,f)   ((G) &=  ~(f))
#define rxFlag(G,f)   ((G) & (f))


typedef struct control_data_s {
  int phase;
  int press;  /* mutually exclusive set of above options */
  float value;
  int nrelax;
  int relax_flags;      /* additive choice of above flags          */
  char extra_flags_str[256]; /* extra flags for relax analysis          */
  int pansail_flags;
  char windfile[256];
  int ncycle;
  char wakefile[128];
  double m_ConvLimit;  // use the global if its not set locally
} control_data_t;


#include "elements.h"


#define HEADING 	0
#define LEEWAY          1
#define BOAT_SPEED 	2
#define WIND_SPEED	3
#define HEEL_ANGLE	4
#define MAST_LENGTH	5
#define RUN_NAME        6


#define DIFFERENT_TOLERANCE	((double)1e-5)
#define MIN_STRING_LENGTH	((double)1e-2)

#define LENGTH  0
#define TENSION 1
#define MAX_TEXT_LENGTH  (64)


struct STRINGITEM {
	class RXEntity * m_sEnt;
	double *value[2];  /* value of Length, Tension */
	char *valStr[2];  /* last value of length, Tension including units */
	int which;	 /* is 0 or 1 to say which of above was last set */
	int changed;  /* flag set to 1 if string has been edited */
};
typedef struct STRINGITEM StringItem;
struct STRINGITEMLIST {
  StringItem *slList;
  int slCount;
};
typedef struct STRINGITEMLIST StringItemList;
 
struct EDITITEM {
	class RXSRelSite *s;
	SAIL *sail;
	char *ValStr[3];  /* last value of length, Tension including units */
	int changed;  /* flag set to 1 if string has been edited */
	RXEntity_p e; 
};
typedef struct EDITITEM  EditItem;

struct EDITITEMLIST {
  	EditItem *elList;
  	int eilCount;
};
typedef struct EDITITEMLIST EditItemList;

#define LENGTH_UNITS "cm,in,ft,mm,m"
/* #define FORCE_UNITS "N",KN,MN,lb,kip,kg" 
 WRONG?? should be */
#define FORCE_UNITS "N,KN,MN,lb,kip,kg" 
#define LENGTH_STEPVAL ((double)0.001)
#define TENSION_STEPVAL ((double)10)

#ifdef _X
	/*static*/ void initialise_objects(Widget parent );
	Widget create_options_dialog (Widget parent);
	Widget create_wake_dialog(Widget parent,control_data_t *data);
//	press_rowcol_p create_press_rowcol (Widget parent);
	void delete_press_rowcol (press_rowcol_p  p_ress_rowcol );
//	relax_rowcol_p create_relax_rowcol (Widget parent);
	void delete_relax_rowcol (relax_rowcol_p  r_elax_rowcol );

	EXTERN_C Widget create_wind_dialog(Widget parent,control_data_t *data);


	Widget create_control_dialog (Widget parent);

	/*static*/ XmString xd_string_append ( XmString s1,XmString s2);

	EXTERN_C  int set_relrow_data(relax_row_p row_p,control_data_t *data); 

	EXTERN_C press_onerow_p create_press_onerow (Widget parent,char *widget_name);
	EXTERN_C void delete_press_onerow (press_onerow_p press_onerow );

	EXTERN_C int set_onerow_data(press_onerow_p r, control_data_t *d);
#endif  //#ifdef _X

#endif  //#ifndef _HDR_CONTROL_

