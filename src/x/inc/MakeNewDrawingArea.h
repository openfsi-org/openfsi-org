#ifndef  _MAKENEWDRA_H_
#define  _MAKENEWDRA_H_
#ifdef _X
	#include <X11/IntrinsicP.h>
	#include "hoopsw.h"  
#endif

#if defined(  __cplusplus)&&defined(HOOPS)
	#include "hc.h"
#endif


EXTERN_C int HighLightSelected(long selSeg,long *highSeg,int action,Graphic *g);
#ifdef _X
	EXTERN_C Widget MakeNewDrawingArea(Widget parent,const char *name,Graphic *g, HBaseModel* p_Phbm);
	EXTERN_C void  select_routine(Widget w,XtPointer* client_data,HT_Widget_CallbackStruct * call_data) ;
#endif

#endif  //#ifndef  _MAKENEWDRA_H_

