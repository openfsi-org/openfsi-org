/* definition of the data structure used by the qmenu */

#ifndef _qmenudef_h
#define _qmenudef_h

#include  "xquestion.h"

#ifdef _X
	#include <X11/Xatom.h>
	#include <X11/Intrinsic.h>
	#include <X11/Shell.h>

	#include <Xm/Xm.h>
	#include <Xm/DialogS.h>
	#include <Xm/PushB.h>
	#include <Xm/TextF.h>
#endif //#ifndef _WINDOWS


struct QDATA_ROW{
	char  *qrprompt; /* originally point to input string but might be realloc'd*/
	char *qrdefault;  /* To save MEMORY we could useflag1 to tell whether to free */
	char *qrtype;
	char *qrtext;
#ifdef _X
	Widget prompt_W;
	Widget default_W;
	Widget text_W;
#else
        void* prompt_W;
        void* default_W;
        void* text_W;

#endif
	int QTYPE; 
	int row_has_been_edited;
	int flag;
	struct QDATA *qdata_p;
};
typedef struct QDATA_ROW  qdataRow;

struct QDATA {
#ifdef _X
	Widget m_Current_Text_Widget;/* Is this ever used?? */
#else
        void* m_Current_Text_Widget;
#endif
	struct QDATA_ROW  *current_row;
	int Nrows;
	struct QDATA_ROW *TheRows;

 	question_shell_p m_q_shell;  /* filled in AFTER the create */
	int Status_Flag;
	int has_been_edited;
	int NOT_DONE;

};
typedef struct QDATA   qdata;

// for status_flag
#define QDESTROYED  63
#define QOK	0

#endif  //#ifndef _qmenudef_h

