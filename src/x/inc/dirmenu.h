/* file : dirmenu.h
date:  21 April 1993
 contains declarations for dirmenu.C */

#ifndef DIRMENU_15NOV04
#define DIRMENU_15NOV04

EXTERN_C int PC_File_Search(char **files[],int *p_pN,const char *filespec); 
EXTERN_C int Create_NewSail_Menu(Graphic *g);

extern  Widget NewSailSubmenu; 
extern Widget NewSailChildren[100];
	
#endif //#define #ifndef DIRMENU_15NOV04
