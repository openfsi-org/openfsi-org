#ifndef EH_ZOOM_IN_16NOV04
#define EH_ZOOM_IN_16NOV04


void insert_box (
   char		*seg,
   float       X0,
   float       Y0,
   float       Z0,
   float       X1,
   float       Y1,
   float       Z1);

void eh_zoom_in (
    Widget			w,
    XtPointer 	*		client_data,
    HT_Widget_CallbackStruct * 	call_data);


#endif //#ifndef EH_ZOOM_IN_16NOV04

