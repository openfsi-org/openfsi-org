/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */



#include "StdAfx.h"
#include "RelaxWorld.h"
#include "RXSail.h"


#ifdef _X
#include <X11/Xatom.h>
#include <X11/Intrinsic.h>
#include <X11/Shell.h>
#include <Xm/Xm.h>
#endif 

#include "tree.h"

#include "entities.h"
#include "resolve.h"

#include "files.h"
#include "stringutils.h"

#include "report.h"


/* struct CHAR_LIST {
  char *str;
  struct CHAR_LIST *next;
};
typedef struct CHAR_LIST CharList;

struct MISSING_DATA {
  CharList *needs;
  CharList *neededBy;
  int       needs_count;
  int       neededBy_count;
  char *    name;
  char *    type;
  struct REPORT_ITEM *data;*/	/* points to the report entry *//*
};
typedef struct MISSING_DATA MissingData;*/


static  struct tnode **MD_nodes;
static  int MD_count=0;
static  int MD_pos=0;

/* look for owner name in tree.
   if find it then 
   look for child
   add owner to childs 'neededBy' list
   add child to owners 'needs' list
   else 
   add it and do above
   BUT only add once. 
   
   */




int Report_Sort2_Fn(const void *va,const void *vb){
  MissingData *mda,*mdb;
  struct tnode **a, **b;
  int cmp;
  
  a =  (struct tnode **) va;
  b = (struct tnode **) vb;
  
  mda = (MissingData *) (*a)->data;
  mdb = (MissingData *) (*b)->data;
  
  if(mda->needs_count > mdb->needs_count) return(1);
  if(mda->needs_count < mdb->needs_count) return(-1);
  if(mda->neededBy_count < mdb->neededBy_count) return(1);
  if(mda->neededBy_count > mdb->neededBy_count) return(-1);
  
  cmp =stricmp(mdb->type,mda->type);
  
  return(cmp);	/* same on both counts */
  
}



int print_missing_data(FILE *fp,struct tnode *p) 
{
  MissingData *md;
  CharList *an;
  struct REPORT_ITEM *a;	
  
  fprintf(fp,"%4d %s \n",p->count,p->word);
  
  if(!p->data)
    return(-1);
  md = (MissingData *) p->data;
  
  if(md->data) {
    a = md->data;
    fprintf(fp,"\tInitial Data line '%s, %s, %s, %s'\n",a->otype,a->oname,a->ctype,a->cname);
  }
  fprintf(fp,"\t Needs %d : Needed By %d\n",md->needs_count,md->neededBy_count);
  an = md->needs;
  while(an) {
    fprintf(fp,"\tNeeds '%s'\n",an->str);
    an = an->next;
  }
  an = md->neededBy;
  while(an) {
    fprintf(fp,"\tNeeded By '%s'\n",an->str);
    an = an->next;
  }
  
  return(0);
}

int count_up(FILE *fp,struct tnode *p) 
{
  MD_count++;
  return(0);
}

int add_to_list(FILE *fp,struct tnode *p) 
{
  MD_nodes[MD_pos] = p;
  MD_pos++;
  return(0);
}




int Report_Sort2(struct REPORT_FORM*r) {
  int k,i;
  MissingData *md;
  Tnode *node,*root=NULL;
  struct REPORT_ITEM *a;
  char cbuf[128], obuf[128], buf[128], *lt;
  CharList *an;
  //FILE *fp;
  CharList *aa;
  CharList *ab;
  
  
  if(!r || r->N <= 0)
    return(0);


  if(MD_nodes) {
    for(i=0;i<MD_count;i++) {
      md = (MissingData *) (MD_nodes[i]->data);
      aa = md->needs;
      if(aa)
	ab = aa->next;
      else
	ab = NULL;
      while(ab) {
	RXFREE(aa);
	aa = ab;
	ab = aa->next;
      } 	
      if(aa)
	RXFREE(aa);
      
      aa = md->neededBy;
      if(aa)
	ab = aa->next;
      else
	ab = NULL;
      while(ab) {
	RXFREE(aa);
	aa = ab;
	ab = aa->next;
      } 	
      if(aa)
	RXFREE(aa);
      RXFREE(MD_nodes[i]);
    }
    
  }
  if(MD_nodes) 
    RXFREE(MD_nodes);
  
  
  MD_count = 0;
  MD_pos = 0;
  MD_nodes = NULL;	/* be TIDY !! */
  
  
  
  
  for(k=0;k<r->N;k++) {
    a =  &(r->z[k]);
    if(stristr(a->otype,"rename")) {
    }
    else if(stristr(a->otype,"site")) { /* sites & relsites*/
      sprintf(obuf,"%s$%s","site",a->oname);
      if(stristr(a->ctype,"curve"))
	sprintf(cbuf,"%s$%s","curve",a->cname);
      else
	sprintf(cbuf,"%s$%s",a->ctype,a->cname);
      
      /* process owner first, adding as necessary */
      if((node = findnode(root,obuf,strlen(obuf))) != NULL )
	md = (MissingData *) node->data;
      else {
	md = (MissingData *) CALLOC(1,sizeof(MissingData));
	md->needs = NULL;
	md->neededBy = NULL;
	md->needs_count = 0;
	md->neededBy_count = 0;
	md->data =  a;
	md->type = STRDUP(a->otype);
	md->name = STRDUP(a->oname);
	root = addtree(root,obuf,(void *) md);
      }
      /*  add new item to needs list */
      an = (CharList *) CALLOC(1,sizeof(CharList));
      an->next = md->needs;
      an->str = STRDUP(cbuf);
      md->needs = an;
      md->needs_count++;
      
      /* process child next, adding as necessary */
      if(node = findnode(root,cbuf,strlen(cbuf))) /* is it there ? */
	md = (MissingData *) node->data;
      else {
	md = (MissingData *) CALLOC(1,sizeof(MissingData));
	md->needs = NULL;
	md->neededBy = NULL;
	md->needs_count = 0;
	md->neededBy_count = 0;
	md->data =  a;
	md->type = STRDUP(a->ctype);
	md->name = STRDUP(a->cname);
	root = addtree(root,cbuf,(void *) md);
      }
      /*  add new item to neededBy list */
      an = (CharList *) CALLOC(1,sizeof(CharList));
      an->next = md->neededBy;
      an->str = STRDUP(obuf);
      md->neededBy = an;
      md->neededBy_count++;
    }  /* end of relsite section */
    
    else if(stristr(a->otype,"curve")) { /* sites & relsites*/
      sprintf(obuf,"%s$%s","curve",a->oname);
      if(stristr(a->ctype,"site"))
	sprintf(cbuf,"%s$%s","site",a->cname);
      else
	sprintf(cbuf,"%s$%s",a->ctype,a->cname);
      
      /* process owner first, adding as necessary */
      if(node = findnode(root,obuf,strlen(obuf)))
	md = (MissingData *) node->data;
      else {
	md = (MissingData *) CALLOC(1,sizeof(MissingData));
	md->needs = NULL;
	md->neededBy = NULL;
	md->needs_count = 0;
	md->neededBy_count = 0;
	md->data =  a;
	md->type = STRDUP(a->otype);
	md->name = STRDUP(a->oname);
	root = addtree(root,obuf,(void *) md);
      }
      /*  add new item to needs list */
      an = (CharList *) CALLOC(1,sizeof(CharList));
      an->next = md->needs;
      an->str = STRDUP(cbuf);
      md->needs = an;
      md->needs_count++;
      
      /* process child next, adding as necessary */
      if(node = findnode(root,cbuf,strlen(cbuf))) /* is it there ? */
	md = (MissingData *) node->data;
      else {
	md = (MissingData *) CALLOC(1,sizeof(MissingData));
	md->needs = NULL;
	md->neededBy = NULL;
	md->needs_count = 0;
	md->neededBy_count = 0;
	md->data =  a;
	md->type = STRDUP(a->ctype);
	md->name = STRDUP(a->cname);
	root = addtree(root,cbuf,(void *) md);
      }
      /*  add new item to neededBy list */
      an = (CharList *) CALLOC(1,sizeof(CharList));
      an->next = md->neededBy;
      an->str = STRDUP(obuf);
      md->neededBy = an;
      md->neededBy_count++;
    }  /* end of curve section */
    
    else if( stristr(a->otype,"tpn")) { /* tpn fabrics etc. */
      sprintf(obuf,"%s$%s",a->otype,a->oname);
      sprintf(cbuf,"%s$%s",a->ctype,a->cname);
      
      /* process owner first, adding as necessary */
      if(node = findnode(root,obuf,strlen(obuf)))
	md = (MissingData *) node->data;
      else {
	md = (MissingData *) CALLOC(1,sizeof(MissingData));
	md->needs = NULL;
	md->neededBy = NULL;
	md->needs_count = 0;
	md->neededBy_count = 0;
	md->data =  a;
	md->type = STRDUP(a->otype);
	md->name = STRDUP(a->oname);
	root = addtree(root,obuf,(void *) md);
      }
      /*  add new item to needs list */
      an = (CharList *) CALLOC(1,sizeof(CharList));
      an->next = md->needs;
      an->str = STRDUP(cbuf);
      md->needs = an;
      md->needs_count++;
      
      /* process child next, adding as necessary */
      if(node = findnode(root,cbuf,strlen(cbuf))) /* is it there ? */
	md = (MissingData *) node->data;
      else {
	md = (MissingData *) CALLOC(1,sizeof(MissingData));
	md->needs = NULL;
	md->neededBy = NULL;
	md->needs_count = 0;
	md->neededBy_count = 0;
	md->type = STRDUP(a->ctype);
	md->name = STRDUP(a->cname);
	md->data =  a;
	root = addtree(root,cbuf,(void *) md);
      }
      /*  add new item to neededBy list */
      an = (CharList *) CALLOC(1,sizeof(CharList));
      an->next = md->neededBy;
      an->str = STRDUP(obuf);
      md->neededBy = an;
      md->neededBy_count++;
    }  /* end of tpn section */
    
    else if( stristr(a->otype,"patch")) { /* patches and pockets. */
      strcpy(buf,a->ctype);
      lt = strtok(buf,",");
      while(lt) {	/* for each  type eg. batten, fabric etc. */
	sprintf(obuf,"%s$%s",a->otype,a->oname);
	sprintf(cbuf,"%s$%s",lt,a->cname);
	
	/* process owner first, adding as necessary */
	if(node = findnode(root,obuf,strlen(obuf)))
	  md = (MissingData *) node->data;
	else {
	  md = (MissingData *) CALLOC(1,sizeof(MissingData));
	  md->needs = NULL;
	  md->neededBy = NULL;
	  md->needs_count = 0;
	  md->neededBy_count = 0;
	  md->data = (REPORT_ITEM *)(void *) a;
	  md->type = STRDUP(a->otype);
	  md->name = STRDUP(a->oname);
	  root = addtree(root,obuf,(void *) md);
	}
	/*  add new item to needs list */
	an = (CharList *) CALLOC(1,sizeof(CharList));
	an->next = md->needs;
	an->str = STRDUP(cbuf);
	md->needs = an;
	md->needs_count++;
	
	/* process child next, adding as necessary */
	if(node = findnode(root,cbuf,strlen(cbuf))) /* is it there ? */
	  md = (MissingData *) node->data;
	else {
	  md = (MissingData *) CALLOC(1,sizeof(MissingData));
	  md->needs = NULL;
	  md->neededBy = NULL;
	  md->needs_count = 0;
	  md->neededBy_count = 0;
	  md->type = STRDUP(a->ctype);
	  md->name = STRDUP(a->cname);
	  md->data =  a;
	  root = addtree(root,cbuf,(void *) md);
	}
	/*  add new item to neededBy list */
	an = (CharList *) CALLOC(1,sizeof(CharList));
	an->next = md->neededBy;
	an->str = STRDUP(obuf);
	md->neededBy = an;
	md->neededBy_count++;
	
	lt = strtok(NULL,",");	 /* get next type and repeat */
      } /* end of while loop */
      
    }  /* end of patch section */
    else if( stristr(a->otype,"pocket")) { /* patches and pockets. */
      strcpy(buf,a->ctype);
      lt = strtok(buf,",");
      while(lt) {	/* for each  type eg. batten, fabric etc. */
	sprintf(obuf,"%s$%s",a->otype,a->oname);
	sprintf(cbuf,"%s$%s",lt,a->cname);
	
	/* process owner first, adding as necessary */
	if(node = findnode(root,obuf,strlen(obuf)))
	  md = (MissingData *) node->data;
	else {
	  md = (MissingData *) CALLOC(1,sizeof(MissingData));
	  md->needs = NULL;
	  md->neededBy = NULL;
	  md->needs_count = 0;
	  md->neededBy_count = 0;
	  md->data = (REPORT_ITEM *)(void *) a;
	  md->type = STRDUP(a->otype);
	  md->name = STRDUP(a->oname);
	  root = addtree(root,obuf,(void *) md);
	}
	/*  add new item to needs list */
	an = (CharList *) CALLOC(1,sizeof(CharList));
	an->next = md->needs;
	an->str = STRDUP(cbuf);
	md->needs = an;
	md->needs_count++;
	
	/* process child next, adding as necessary */
	if(node = findnode(root,cbuf,strlen(cbuf))) /* is it there ? */
	  md = (MissingData *) node->data;
	else {
	  md = (MissingData *) CALLOC(1,sizeof(MissingData));
	  md->needs = NULL;
	  md->neededBy = NULL;
	  md->needs_count = 0;
	  md->neededBy_count = 0;
	  md->type = STRDUP(a->ctype);
	  md->name = STRDUP(a->cname);
	  md->data =  a;
	  root = addtree(root,cbuf,(void *) md);
	}
	/*  add new item to neededBy list */
	an = (CharList *) CALLOC(1,sizeof(CharList));
	an->next = md->neededBy;
	an->str = STRDUP(obuf);
	md->neededBy = an;
	md->neededBy_count++;
	
	lt = strtok(NULL,",");	 /* get next type and repeat */
      } /* end of while loop */
      
    }  /* end of patch section */

    else  { /* DEFAULT*/
#ifdef DEFAULT_REPORTING
      strcpy(buf,a->ctype);
      lt = strtok(buf,",");
      while(lt) {	/* for each  type eg.  etc. */
	sprintf(obuf,"%s$%s",a->otype,a->oname);
	sprintf(cbuf,"%s$%s",lt,a->cname);
	
	/* process owner first, adding as necessary */
	if(node = findnode(root,obuf,strlen(obuf)))
	  md = (MissingData *) node->data;
	else {
	  md = (MissingData *) CALLOC(1,sizeof(MissingData));
	  md->needs = NULL;
	  md->neededBy = NULL;
	  md->needs_count = 0;
	  md->neededBy_count = 0;
	  md->data =  a;
	  md->type = STRDUP(a->otype);
	  md->name = STRDUP(a->oname);
	  root = addtree(root,obuf,(void *) md);
	}
	/*  add new item to needs list */
	an = (CharList *) CALLOC(1,sizeof(CharList));
	an->next = md->needs;
	an->str = STRDUP(cbuf);
	md->needs = an;
	md->needs_count++;
	
	/* process child next, adding as necessary */
	if(node = findnode(root,cbuf,strlen(cbuf))) /* is it there ? */
	  md = (MissingData *) node->data;
	else {
	  md = (MissingData *) CALLOC(1,sizeof(MissingData));
	  md->needs = NULL;
	  md->neededBy = NULL;
	  md->needs_count = 0;
	  md->neededBy_count = 0;
	  md->type = STRDUP(a->ctype);
	  md->name = STRDUP(a->cname); 
	  md->data =  a;
	  root = addtree(root,cbuf,(void *) md);
	}
	/*  add new item to neededBy list */
	an = (CharList *) CALLOC(1,sizeof(CharList));
	an->next = md->neededBy;
	an->str = STRDUP(obuf);
	md->neededBy = an;
	md->neededBy_count++;
	
	lt = strtok(NULL,",");	 /* get next type and repeat */
      } /* end of while loop */
#endif
    printf("Default Thing - '%s' '%s' wanted '%s' '%s'\n",a->otype,a->oname,a->ctype,a->cname);
    }  /* end of default section */
    
  }
  
  
  //sprintf(buf,"%s/report.out",traceDir);
  //fp = FOPEN(buf,"w");
 //treeprint(root,fp,print_missing_data);
  
  //fprintf(fp,"done\n");
  
  
  treeprint(root,NULL,count_up);    /* traverse tree counting the nodes */
  MD_nodes = (struct tnode **) CALLOC(MD_count+2,sizeof(struct tnode *));
  treeprint(root,NULL,add_to_list); /* traverse tree making array of pointers */
  
  qsort(MD_nodes,MD_count,sizeof(struct tnode *),Report_Sort2_Fn); 
  
  
  
 // for(i=0;i<MD_count;i++) {
//    print_missing_data(fp,MD_nodes[i]);
 // }
 // FCLOSE(fp);
  
  return(1);
}

#ifdef  _X
int get_unresolved_items(XmString **Items,int *nitems)
{ 
  XmString *xm;
  int ni=0,limit=1000;
  char buf[128];
  MissingData *md;
  int i;
  
  *nitems=0;
  
  if (MD_count < limit)
    limit = MD_count;
  
  xm = *Items = (XmString*) XtCalloc(limit,sizeof(XmString));
  
  for( i=0;i<limit;i++,xm++) {
    md = (MissingData *) MD_nodes[i]->data;		
    sprintf(buf,"'%s' :%s  NOT found but referenced",md->name,md->type);
    *xm = XmStringCreateLtoR(buf, (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);	
    ni++;
  }
  *nitems = ni;
  return(ni);
}

#endif

MissingData * get_Nth_unresolved_item(int n,char **type,char **name)
{ 
  MissingData *md;
 // int i;
  if(n > MD_count || n < 0) {
    g_World->OutputToClient ("invalid call to get_Nth_unresolved_item",2);
    return(NULL);
  }
  md = (MissingData *) MD_nodes[n]->data;		
  *type = md->type;
  *name = md->name;
  return(md);
}



