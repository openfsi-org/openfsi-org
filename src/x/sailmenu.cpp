/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

/* CreateMenuBar func. - Sailmenu */

#include "StdAfx.h" 

#include "Global.h"
#include "Menu.h"
#include "menuCB.h"
#include "custom.h"

#include "global_declarations.h"

#include "sailmenu.h"

Widget CreateSailMenuBar(Widget parent,Graphic *g)
{
Widget menuBar;
Widget cascade;
Widget pulldownPane,submenuRotate;
Arg al[64]; 

XmString label;

/* Create menu bar */
int l_ac=0;
menuBar = XmCreateMenuBar(parent,"menuBar",al,l_ac);
/*****************************************************************************/
/***************************   FILE MENU   ***********************************/
/*****************************************************************************/
l_ac=0;
pulldownPane = XmCreatePulldownMenu(menuBar,"pulldownPane",al,l_ac);
/**** new button *****/
CreateMenuButton(pulldownPane,NULL,MENU_SAIL_NEW,"From Script",'N',PushButton,NULL,NULL,g);

/**** open button *****/
XtSetSensitive(CreateMenuButton(pulldownPane,NULL,MENU_SAIL_OPEN,"Open Bagged",'O',PushButton,NULL,NULL,g),0);


/**** Append button *****/
CreateMenuButton(pulldownPane,NULL,MENU_SAIL_APPEND,"Append Data",'A',PushButton,NULL,NULL,g);

CreateMenuButtonForLP(pulldownPane,NULL,MENU_TEXT_CB ,"Hoist",'1',PushButton,NULL,NULL,g,"Hoist:");
CreateMenuButtonForLP(pulldownPane,NULL,MENU_TEXT_CB ,"Drop",'1',PushButton,NULL,NULL,g,"Drop:");

AddSeparator(pulldownPane);

/**** print button ***
**/
CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_PRINT,"Print",'P',PushButton,NULL,NULL,g);

/* export button */

CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_EXPORT_HMF,"Export",'E',PushButton,NULL,NULL,g);
CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_DEBUG_1,"Exp Opts",'O',PushButton,NULL,NULL,g);
AddSeparator(pulldownPane);

//CreateMenuButton(pulldownPane,NULL,MENU_EXPORT_PANELS,"Export Panels",'x',PushButton,NULL,NULL,g);
//AddSeparator(pulldownPane);

/**** save button *****/
XtSetSensitive(CreateMenuButton(pulldownPane,NULL,MENU_SAIL_SAVE,"Save Bagged",'S',PushButton,NULL,NULL,g),0);

/**** save as button *****/
CreateMenuButton(pulldownPane,NULL,MENU_SAIL_SAVEAS,"Save Bagged As",'A',PushButton,NULL,NULL,g);

AddSeparator(pulldownPane);

/* Custom Buttons */
Insert_Custom_Buttons("sailfile",pulldownPane, g);

/* AddSeparator(pulldownPane);*/
/**** close button *****/
CreateMenuButton(pulldownPane,NULL,MENU_SAIL_CLOSE,"Close",'C',PushButton,NULL,NULL,g);

/*** File Cascade button ***/
label = XmStringCreateSimple("File");
l_ac=0;
XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
XtSetArg(al[l_ac],XmNmnemonic,'F');l_ac++;
cascade = XmCreateCascadeButtonGadget(menuBar,NULL,al,l_ac);
XtManageChild(cascade);
XmStringFree(label);

/*****************************************************************************/
/***************************   VIEW MENU   ***********************************/
/*****************************************************************************/
l_ac=0;
pulldownPane = XmCreatePulldownMenu(menuBar,"pulldownPane",al,l_ac);


/**** New button *****/
g->attributeButton = CreateMenuButton(pulldownPane,NULL,MENU_VIEW_ATTRIBUTES,"Set Attributes",'A',PushButton,NULL,NULL,g);

AddSeparator(pulldownPane);

l_ac=0;
submenuRotate = XmCreatePulldownMenu(pulldownPane,"submenuRotate",al,l_ac);
/**** Left 22.5 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_LEFT22,"Rotate Left 22.5",'L',PushButton,NULL,NULL,g);

/**** Right 22.5 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_RIGHT22,"Rotate Right 22.5",'R',PushButton,NULL,NULL,g);

/**** Left 90 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_LEFT90,"Rotate Left 90",'e',PushButton,NULL,NULL,g);

/**** Right 90 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_RIGHT90,"Rotate Right 90",'i',PushButton,NULL,NULL,g);

/**** Up 22.5 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_UP22,"Rotate Up 22.5",'U',PushButton,NULL,NULL,g);

/**** Down 22.5 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_DOWN22,"Rotate Down 22.5",'D',PushButton,NULL,NULL,g);

/**** Up 90 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_UP90,"Rotate Up 90",'p',PushButton,NULL,NULL,g);

/**** Down 90 button *****/
CreateMenuButton(submenuRotate,NULL,MENU_VIEW_ROTATE_DOWN90,"Rotate Down 90",'o',PushButton,NULL,NULL,g);

label = XmStringCreateSimple("Rotate");
l_ac=0;
XtSetArg(al[l_ac],XmNsubMenuId,submenuRotate);l_ac++;
XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
XtSetArg(al[l_ac],XmNmnemonic,'o');l_ac++;
cascade = XmCreateCascadeButtonGadget(pulldownPane,NULL,al,l_ac);
XtManageChild(cascade);
XmStringFree(label);




/**** Zoom In button *****/
CreateMenuButton(pulldownPane,NULL,MENU_VIEW_ZOOMIN,"Zoom Window",'Z',PushButton,NULL,NULL,g);


AddSeparator(pulldownPane);

/**** Resize button *****/
CreateMenuButton(pulldownPane,NULL,MENU_VIEW_RESIZE,"Resize",'R',PushButton,NULL,NULL,g);
Insert_Custom_Buttons("sailview",pulldownPane, g);

/*** File Cascade button ***/
label = XmStringCreateSimple("View");
l_ac=0;
XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
XtSetArg(al[l_ac],XmNmnemonic,'V');l_ac++;
cascade = XmCreateCascadeButtonGadget(menuBar,NULL,al,l_ac);
XtManageChild(cascade);
XmStringFree(label);



/*****************************************************************************/
/***************************   BUILD MENU   ***********************************/
/*****************************************************************************/
l_ac=0;
pulldownPane = XmCreatePulldownMenu(menuBar,"pulldownPane",al,l_ac);
/**** PanelSolve button *****/
g_panelSolveButton = NULL; // CreateMenuButton(pulldownPane,NULL,MENU_SAIL_BUILD_PANELSOLVE,"Panel Solve",'S',PushButton,NULL,NULL,g);

/**** Mesh button *****/
g_meshButton = NULL; // CreateMenuButton(pulldownPane,NULL,MENU_SAIL_BUILD_MESH,"Mesh",'M',PushButton,NULL,NULL,g);

AddSeparator(pulldownPane);

/**** Add Fabric button *****/
//g_meshButton = CreateMenuButton(pulldownPane,NULL,MENU_SAIL_BUILD_SETFABRIC,"Add Fabric",'M',PushButton,NULL,NULL,g);

/**** Add Entity Delete button *****/
//g_meshButton = CreateMenuButton(pulldownPane,NULL,MENU_SAIL_BUILD_DELETE,"Delete Something",'M',PushButton,NULL,NULL,g);

/**** Add Relsite Move button *****/
//g_meshButton = CreateMenuButton(pulldownPane,NULL,MENU_SAIL_BUILD_MOVESITE,"Move a node",'M',PushButton,NULL,NULL,g);
/**** Add Question  button *****/
g_meshButton = CreateMenuButton(pulldownPane,NULL,MENU_SAIL_BUILD_ASK_QUEST,"Parameters",'M',PushButton,NULL,NULL,g);
Insert_Custom_Buttons("sailbuild",pulldownPane, g);


/*** File Cascade button ***/
label = XmStringCreateSimple("Build");
l_ac=0;
XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
XtSetArg(al[l_ac],XmNmnemonic,'B');l_ac++;
cascade = XmCreateCascadeButtonGadget(menuBar,NULL,al,l_ac);
XtManageChild(cascade);
XtAddCallback(cascade,XmNcascadingCallback,(XtCallbackProc)MenuCB,mkID(MENU_BUILD_CASCADE,g));
XmStringFree(label);


/*****************************************************************************/
/***************************   DEBUG MENU   ***********************************/
/*****************************************************************************/
#ifdef DEBUG_MENU
l_ac=0;
pulldownPane = XmCreatePulldownMenu(menuBar,"pulldownPane",al,l_ac);
/**** New button *****/
CreateMenuButton(pulldownPane,NULL,MENU_DEBUG_WRITEALL,"Write .hmf",'W',PushButton,NULL,NULL,g);


/**** Print All Entities button *****/
CreateMenuButton(pulldownPane,NULL,MENU_DEBUG_PRINTALLENTITIES,"Print All Entities",'P',PushButton,NULL,NULL,g);

/**** debug 1 button *****/
//CreateMenuButton(pulldownPane,NULL,MENU_DEBUG_DISPLAYNODES,"Display Internals",'C',PushButton,NULL,NULL,g);

#if 0
XtSetSensitive(CreateMenuButton(pulldownPane,NULL,MENU_DEBUG_2,"Constant Pressure",'D',PushButton,NULL,NULL,g),1);
#endif
/**** Print_DAT file button *****/
XtSetSensitive(CreateMenuButton(pulldownPane,NULL,MENU_SAIL_LIST_ENTITY,"List Entity",'L',PushButton,NULL,NULL,g),1);

g_MaterialWidget = CreateMenuButton(pulldownPane,NULL,MENU_SAIL_MATERIAL_ENTITY,"Change Material",'M',CheckBox,NULL,NULL,g);


Insert_Custom_Buttons("saildebug",pulldownPane, g);



/*** File Cascade button ***/
label = XmStringCreateSimple("Debug");
l_ac=0;
XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
XtSetArg(al[l_ac],XmNmnemonic,'D');l_ac++;
cascade = XmCreateCascadeButtonGadget(menuBar,NULL,al,l_ac);
XtManageChild(cascade);
XmStringFree(label);

#endif


/*****************************************************************************/
/***************************   PRESTRESS MENU   ***********************************/
/*****************************************************************************/
#ifdef PRE_STRESS
l_ac=0;
pulldownPane = XmCreatePulldownMenu(menuBar,"pulldownPane",al,l_ac);

/****Set Prestress button *****/
XtSetSensitive(CreateMenuButton(pulldownPane,NULL,MENU_RESET_ONE_EDGES ,"Reset These Edges",'h',PushButton,NULL,NULL,g),1);

XtSetSensitive(CreateMenuButton(pulldownPane,NULL,MENU_SET_PRESTRESS ,"Set Prestress",'P',PushButton,NULL,NULL,g),1);

/****EDIT button *****/
XtSetSensitive(CreateMenuButton(pulldownPane,NULL,MENU_EDIT_SEAMCURVE ,"Edit Seamcurve",'E',PushButton,NULL,NULL,g),1);

/****make relsite edits button *****/
XtSetSensitive(CreateMenuButton(pulldownPane,NULL,MENU_RELSITE_EDITS ,"Make Relsite Edits",'R',PushButton,NULL,NULL,g),1);

/****make seamcurve length edits button *****/
XtSetSensitive(CreateMenuButton(pulldownPane,NULL,MENU_SEAMCURVE_EDITS ,"Make SeamCurve Edits",'C',PushButton,NULL,NULL,g),1);

/****make seamcurve length edits button *****/
XtSetSensitive(CreateMenuButton(pulldownPane,NULL,MENU_SHRINK_ONE_EDGES,"Shrink These Edges",'S',PushButton,NULL,NULL,g),1);





/*** File Cascade button ***/
label = XmStringCreateSimple("PreStress");
l_ac=0;
XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
XtSetArg(al[l_ac],XmNmnemonic,'S');l_ac++;
cascade = XmCreateCascadeButtonGadget(menuBar,NULL,al,l_ac);
XtManageChild(cascade);
XmStringFree(label);

#endif

XtManageChild(menuBar);
return(menuBar);

} /* endo of CreateMenuBar()  */

