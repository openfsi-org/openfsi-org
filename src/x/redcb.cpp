/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */
 
#include "StdAfx.h"

/* callback functions for the rename_dialog
 * see reddiag.c for creation 
 */

#include <X11/Intrinsic.h>
#include <X11/Shell.h>

#include <Xm/Xm.h>
#include <Xm/List.h>

#include "griddefs.h"
#include "entities.h"
#include "elements.h"
#include "GraphicStruct.h"
#include "resdiag.h"
#include "resolve.h"
#include "global_declarations.h"

#include "reddiag.h"

#include "redcb.h"

static int NOT_DONE;
static int RETVAL;



/*    NOTE : This routine is MODAL. It only returns when
          the dialog is destroyed in some way.

 */


static RXEntity_p Ce = NULL; /* set to currently chosen entity */

RXEntity_p choose_database_item(SAIL *p_sail, Widget parent,const char *filter,const char *prompt)
{
Widget rshell = (Widget) NULL;
XtInputMask xmask;

Ce = NULL;

rshell = create_rename_shell(p_sail, parent,filter,prompt);	
RETVAL = 0;
NOT_DONE = 1;

while(NOT_DONE) {	
    xmask = XtAppPending(g_app_context);
    if(xmask) 
	XtAppProcessEvent(g_app_context,xmask);
}



return(Ce);
}//RXEntity_p choose_database_item



void rename_close_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data)
{
/* simply remove the window and tidy up 
 * 
 */
ResData *rd = ( ResData * ) client_data;

NOT_DONE = 0;
 // XtRemoveGrab(rd->shell); aug 2003 try removing this.
XtDestroyWidget(rd->shell); 	
XtFree(rd->Items);		/* RXFREE xmstring memory */
if(rd->m_elist)
	RXFREE(rd->m_elist);	/* RXFREE entity list */
RXFREE(rd);		/* RXFREE resdata memory  */
}//void rename_close_CB


/* function for list widget callback */

void rename_single_select_CB(Widget w,caddr_t client_data,XmListCallbackStruct *call_data)
{
ResData *rd = ( ResData * ) client_data;
RXEntity_p *elist;

  elist = rd->m_elist;

  if(call_data->reason == XmCR_SINGLE_SELECT && 
	call_data->selected_item_count > 0)      {


    rd->chosen = call_data->item_position - 1;
    Ce = elist[rd->chosen];
	assert(Ce);
	PC_UnHighlight_All_Entities(); //  25 June 2003
	PC_Highlight_Entity(Ce);		
  }
  else {
    rd->chosen = -1;
    Ce = NULL;	
  }

}//void rename_single_select_CB



void rename_ok_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data)
{
/* uses a simple prompt to fill in the 
 * missing portion of a rename card.
 */

if(Ce)  
    rename_close_CB(w, client_data, call_data ); /* close down */
 
}//void rename_ok_CB


void rename_abort_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data)
{
/* simply remove the window and tidy up 
 * 
 */

Ce = NULL;
rename_close_CB(w, client_data, call_data ); /* close down */

}//void rename_abort_CB


