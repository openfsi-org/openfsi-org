/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h" 

#include "Global.h"
#include "Menu.h"

#include <X11/X.h>

#include "rversion.h"
#include "SaveSail.h"
#include "akmutil.h"
#include "global_declarations.h"

#include "AskToOverwriteDlg.h"


void w_destroy_cb(Widget w,XtPointer client_data,XtPointer call_data)
{
	return;
}//void w_destroy_cb

int AskToOverwriteDlg(Graphic *g,char *filename,Widget dialog,int type)
{
String retString;

char fname[512];

char sz[512];
Arg al[64]; 
int l_ac=0;
sprintf(fname,"Do you want to overwrite '%s' ?",filename);

sDialog = dialog;		/* set local static for CB funcs */
File = filename;

if(overwriteString)
	XmStringFree(overwriteString);
overwriteString = XmStringCreateSimple(fname);
if(titleString)
	XmStringFree(titleString);
titleString = XmStringCreateSimple((char*)"Overwrite File");
if(cancelString)
	XmStringFree(cancelString);
cancelString = XmStringCreateSimple((char*)"No");
if(okString)
	XmStringFree(okString);
okString = XmStringCreateSimple((char*)"Yes");
if(type == WT_DESIGN)
  strcat(filename,".bag");
else if (type == WT_BOATFILE)
  strcat(filename,".in");


retString = XtFindFile(filename,NULL,0,NULL);

if(retString) { /* then existing file  */
        XtFree(retString);
	l_ac = 0;
	XtSetArg (al[l_ac], XmNdialogStyle, XmDIALOG_FULL_APPLICATION_MODAL);  l_ac++; 
	XtSetArg (al[l_ac], XmNdialogTitle,titleString );  l_ac++; 
	XtSetArg (al[l_ac], XmNmessageString, overwriteString);  l_ac++; 
	XtSetArg (al[l_ac], XmNokLabelString, okString);  l_ac++; 
	XtSetArg (al[l_ac], XmNcancelLabelString, cancelString);  l_ac++; 
	warning_dialog = XmCreateWarningDialog(g->mainWindow,(char*)"Overwrite File",al,l_ac);
	
		strcpy(g->filename,filename);
	XtAddCallback(warning_dialog, XmNokCallback,WarningOKCB, mkID(type,g));
	XtAddCallback(warning_dialog, XmNcancelCallback, WarningCancelCB, mkID(type,g));
 	XtAddCallback (warning_dialog, XmNdestroyCallback, w_destroy_cb,NULL);

	XtManageChild(warning_dialog); 
}
else {
	if(validfile(filename) < 0)
		return(0);	/* invalid filename */



	if(type == WT_DESIGN) {
		strcpy(g->filename,filename);
		l_ac=0;
		sprintf(sz,"%s Sail:",RELAX_VERSION);
		strcat(sz,filename);
		XtSetArg(al[l_ac],XmNtitle,sz); l_ac++;
		XtSetValues(g->topLevel,al,l_ac);
	}
	else if(type == WT_BOATFILE) {
	  strcpy(g->filename,filename);
	  l_ac=0;
	  sprintf(sz,"%s Boat:",RELAX_VERSION);
	  strcat(sz,filename);
	  XtSetArg(al[l_ac],XmNtitle,sz); l_ac++;
	  XtSetValues(g->topLevel,al,l_ac);

	}
	SaveSail(g,type,filename);
	/* popdown the file selection box */
	XtUnmanageChild(dialog);
        XtDestroyWidget(dialog);
}

return(0);
} //int AskToOverwriteDlg


void WarningOKCB(Widget w, XtPointer client_data, XtPointer  call_data) 
{

  	char sz[200];
	Arg al[64]; 
	int type;
	Graphic *g;
	int l_ac=0;
	type = ((MenuID *)client_data)->id;
	g = (Graphic *)((MenuID *)client_data)->g;

	if(g && g->topLevel && type == WT_DESIGN) {   	      /* popdown the file selection box */
		sprintf(sz,"%s Sail:","Gauss"); // was RELAX_VERSION);
		strcat(sz,((Graphic *) client_data)->filename);
		XtSetArg(al[l_ac],XmNtitle,sz); l_ac++;
		XtSetValues(g->topLevel,al,l_ac);
	}
        SaveSail(g,type,File);

	XtUnmanageChild(warning_dialog);
        XtDestroyWidget(warning_dialog);
        XtUnmanageChild(sDialog);
        XtDestroyWidget(sDialog);
}//void WarningOKCB


void WarningCancelCB(Widget	w,XtPointer client_data,XtPointer call_data) 
{
      /* popdown the file selection box */
int type;
Graphic *g;
	type = ((MenuID *)client_data)->id;
	g = (Graphic*)((MenuID *)client_data)->g;

	if(g && type == WT_DESIGN)
		g->filename[0] = '\0';
	XtUnmanageChild(warning_dialog);
        XtDestroyWidget(warning_dialog);
}//void WarningCancelCB


