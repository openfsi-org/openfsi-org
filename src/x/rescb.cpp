/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	 15/3/97  	choose_database_item prototyped
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


#include "StdAfx.h"
/* callback functions for the resolve_dialog
 * see resdiag.c for creation 
 */
#include "RXSail.h"
#include <X11/Intrinsic.h>
#include <X11/Shell.h>

#include <Xm/Xm.h>
#include <Xm/List.h>
#include <string>
using namespace std;

#include "hoopcall.h"

#include "elements.h"
#include "GraphicStruct.h"
#include "resdiag.h"
#include "resolve.h"
#include "script.h"
#include "report.h"
#include "stringutils.h"
#include "global_declarations.h"

#include "redcb.h"

#include "entities.h"

#include "rescb.h"

Widget create_resolve_shell(Graphic *g);


static int NOT_DONE;
static int RETRY;



/* returns 1 if its worth retrying the resolve
           0 to abort the loop and destroy the sail

   NOTE : This routine is MODAL. It only returns when
          the dialog is destroyed in some way.

 */

Widget resolveWidget=NULL;   /* so that eg. select is on top of it */

int do_resolve_dialog(Graphic *g)
{
Widget rshell = (Widget) NULL;
XtInputMask xmask;

rshell = create_resolve_shell(g);	
XtPopup(rshell,XtGrabNone);
resolveWidget = rshell;

RETRY = 1;
NOT_DONE = 1;
while(NOT_DONE) {		/* MODAL event loop */
    xmask = XtAppPending(g_app_context);
    if(xmask) 
	XtAppProcessEvent(g_app_context,xmask);
}//int do_resolve_dialog

XFlush(XtDisplay(g->topLevel));
resolveWidget = NULL;
return(RETRY);
}



void resolve_close_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data)
{
	/* simply remove the window and tidy up 
	 * 
	 */
	ResData *rd = ( ResData * ) client_data;


	NOT_DONE = 0;
	XtDestroyWidget(rd->shell); 	
	XtFree(rd->Items);		/* RXFREE xmstring memory */
	RXFREE(rd);		/* RXFREE resdata memory  */
}//void resolve_close_CB


/* function for list widget callback */
void single_select_CB(Widget w,caddr_t client_data,XmListCallbackStruct *call_data)
{
ResData *rd = ( ResData * ) client_data;
char *type,*name;
int sel=0,app=1,sc=0,ren=0,ab=1;

  if(call_data->reason == XmCR_SINGLE_SELECT && 
	call_data->selected_item_count > 0)      {

    rd->chosen = call_data->item_position - 1;	
	get_Nth_unresolved_item(rd->chosen,&type,&name);
	if(stristr(type,"site")) {
		sel = 1;ren = 1;sc = 1;	
	}
	else if(  stristr(type,"fabric")) {
		sel = 0;ren = 1;sc = 1;
	}
	else if(  stristr(type,"batten")) {
		sel = 0;ren = 1;sc = 1;		/* allows to alias, and scan */
	}
	else  {
		sel = 1;ren = 1;sc = 1;
/* Peter changed march 1997  from  sel = 0;ren = 0;sc = 0; */
	}

	XtSetSensitive(rd->select,sel);
	XtSetSensitive(rd->append,app);	/* always append */
	XtSetSensitive(rd->scan,sc);
	XtSetSensitive(rd->rename,ren);
	XtSetSensitive(rd->abort,ab);	/* always abort */
  }
  else {
	XtSetSensitive(rd->select,sel);
	XtSetSensitive(rd->append,app);	/* always append */
	XtSetSensitive(rd->scan,sc);
	XtSetSensitive(rd->rename,ren);
	XtSetSensitive(rd->abort,ab);	/* always abort */
    rd->chosen = -1;	
  }

}//void single_select_CB



void resolve_select_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data)
{
/* uses a simple prompt to fill in the 
 * missing portion of a rename card.
 */
char buf[256];
RXEntity_p  ep, newp;
int retVal = 1;
ResData *rd = ( ResData * ) client_data;
char *name, *type;
int depth;
SAIL *sail = rd->m_g->GSail; // GetCurrentSail();

if(rd->chosen < 0) {
	printf("invalid selection\n");
	retVal = 0;
}

get_Nth_unresolved_item(rd->chosen,&type,&name);

sprintf(buf,"Select object to use as %s\n",name );

newp = PC_Get_Key_Regardless(sail,(const char*) type,(const char*) name,(const char*) buf);

if(!newp) {
	ON_String mybuf("FAILED to select entity: "); mybuf+= ON_String (type)+ON_String(" , ")+ON_String(name);
    	g_World->OutputToClient (mybuf,2);
    	retVal = 0;
}    
else {
    sprintf(buf,"rename:%s:%s:%s:%s",name,newp->type(),newp->name(),name);
    depth = 0;
	cout<<" create rename card with line:"<<endl<<buf<<endl;
    ep = Just_Read_Card(sail,buf,"rename",name,&depth);
	if(!ep) cout<<" But its didnt make an entity"<<endl;
    
 }


  if(retVal) {
    RETRY = retVal;
    resolve_close_CB(w, client_data, call_data ); /* close down */
  }
}//void resolve_select_CB


void resolve_append_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data)
{
/* simply remove the window and tidy up 
 * 
 */
ResData *rd = ( ResData * ) client_data;
char * retVal;
retVal = 0;//Do_Append("read only",rd->m_g); /* NOTE that this call ONLY reads data */
					     /* it does not recursively resolve     */
  if(retVal) {
    RETRY = 1; // retVal;
    resolve_close_CB(w, client_data, call_data ); /* close down */
  }
}//void resolve_append_CB


void resolve_scan_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data)
{
/* simply remove the window and tidy up 
 * 
 */
RXEntity_p  ent;
struct REPORT_ITEM*z;
MissingData *md;
struct CHAR_LIST *cl;
ResData *rd = ( ResData * ) client_data;

char buf[256];
char *type,*name;
//printf("resolve_scan_CB  chosen=%d\n",rd->chosen );

if(rd->chosen < 0)
	return;

md = get_Nth_unresolved_item(rd->chosen,&type,&name);

	z = (REPORT_ITEM*)rd->m_ri;

 	Graphic *g = rd->m_g;

	cl = md->neededBy;
	while(cl) {
		char t[256];
		strcpy(t, cl->str);
 		PC_Replace_Char(t, '$', 0);
		name = strchr( cl->str,'$'); name++;
		printf(" t=<%s>  n = <%s>\n", t,name);
		ent = g->GSail->GetKeyWithAlias(t,name); 
		sprintf(buf,"Delete : %s : %s",t,name);

		ent->Kill(); 
		cl=cl->next;
	}
   RETRY = 1;
    resolve_close_CB(w, client_data, call_data ); /* close down */
}//void resolve_scan_CB

void resolve_rename_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data)
{

ResData *rd = ( ResData * ) client_data;

char *type,*name;
char buf[256],prompt[256];
int depth;
RXEntity_p  ep;

if(rd->chosen < 0)
	return;

get_Nth_unresolved_item(rd->chosen,&type,&name);

assert(strlen(type) + strlen(name) < (256 -48));

sprintf(prompt,"Select object to use as '%s' '%s':",type,name);
ep = choose_database_item( rd->m_g->GSail,rd->shell,type,prompt);


  if(ep) {
	if( strlen(name)+strlen(ep->type())+strlen(ep->name())+strlen(name) >120) 
		g_World->OutputToClient (" caught overflow in rescb",1);
	assert(strlen(name)+strlen(ep->type())+strlen(ep->name())+strlen(name) < 240);
    sprintf(buf,"rename:%s:%s:%s:%s",name,ep->type(),ep->name(),name);
    depth = 0;
    Just_Read_Card(rd->m_g->GSail,buf,"rename",name,&depth);
    RETRY = 1;
    resolve_close_CB(w, client_data, call_data ); /* close down */
  }
}//void resolve_rename_CB


void resolve_abort_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data)
{
/* simply remove the window and tidy up 
 * 
 */

RETRY=0;
resolve_close_CB(w, client_data, call_data ); /* close down */

}//void resolve_abort_CB


