#include "StdAfx.h"
#include <QDebug>
#include <QFileInfo>

#include "stringtools.h"

/*  Logic of line reading
line is the entire line. concatenated by a trailing '&' (same concatenation character that is used in commslib)
the line as input may be separated by colons or by tabs, although the colon in microsoft filenames  is not a separator
We will convert all separators to RXSCRIPTSEPS (const char*)

 Firstword consists of those characters up to the first "colon or tab"
*/
QStringList rxParseLine(const QString &qline,const QString &regexp ) // rebuilds Qline using newsep as separator
{
    QStringList rc;
    QString work;
    QRegExp  r (regexp);   //   not[A-Z] then : or \t  then NOT a backslash
    int kt = qline.indexOf('!' );  // strip any trailing comment
    if(kt>=0)
        work= qline.left(kt);
    else
        work=qline;
    rc =  rxqparsestring(work, r);

    for (int i=0;i<rc.size();++i)
        rc[i]=rc[i].trimmed();
    return rc;
}
int rxqfileparse(const QString a ,QString &dir,QString &fname,QString &ext )
{
    QFileInfo fi(a);
    ext=fi.suffix(); //without the '.'
    fname = fi.completeBaseName();//no extension // (Qt 8-linux) completeBaseName doesnt behave like Qt doc
    dir = fi.path(); //no trailing 'separator'/'
    return fi.isFile();
}

string rxstripcomment(const string &s){
    string rc;
    size_t i,last=0;

    string word;
    rc=s;
    i=rc.find_first_of("!");
    if(i==string::npos) return rc;
    rc.erase (i);
    return rc;
}
vector<string> rxparsestring(const string &str, const string &toks,const bool TreatMultiplesAsOne){
    vector<string> rc;
    size_t i,last=0;

    string word;
    if(str.empty())
        return rc;
    i=str.find_first_of(toks);
    while (i!=string::npos)
    {
        word = str.substr(last,i-last); last=i+1;
        while(word.size()&&isWhiteSpace(word[0]) )
        {
            word.erase(0,1);
        }
        while(word.size()&&isWhiteSpace(word[word.size()-1]) )
        {
            word.erase(word.size()-1,1);
        }
        if(!( word.size()==0 && TreatMultiplesAsOne ))
            rc.push_back(word);
        i=str.find_first_of(toks,i+1);
    }
    word = str.substr(last);
    while(word.size()&&isWhiteSpace(word[0]) )
    {
        word.erase(0,1);
    }
    while(word.size()&&isWhiteSpace(word[word.size()-1]) )
    {
        word.erase(word.size()-1,1);
    }
    if(!word.empty ())
        rc.push_back(word);
    return rc;

}
QStringList rxqparsestring(const QString & str, const QRegExp &toks,const bool TreatMultiplesAsOne )
{
    QStringList rc;

    // to jump over quotes we have to do it by hand
    QString word;
    QChar c,q (QLatin1Char(34)); //&#034; 	22 	" 	&quot; 	double quotation mark
    int startpoint=0;
    int j, i=0; int nc ;

    nc = str.size();
    if(!nc)
        return rc;
    for(i=0; i< nc;i++){
        c = str.at(i);
        if (c==q){ // walk forward till we find another one
            j = str.indexOf(q,i+1);
            if (j<0 ) { //no closing quite
                qDebug()<<" No closing quote in `"<<str<<"`";
            }
            else {
                i = j;
            }
        }
        if(toks.indexIn(c) >=0) { // a delimiter
            word = str.mid(startpoint, i-startpoint);
            rc<<word.trimmed();
            startpoint = i+1;
        }
    }
    word = str.mid(startpoint );
    rc<<word.trimmed();
    return rc;


}





vector<wstring> rxparsestring(const wstring &str, const wstring &toks,const bool TreatMultiplesAsOne){
    vector<wstring> rc;
    size_t i,last=0;

    wstring word;

    i=str.find_first_of(toks);
    while (i!=string::npos)
    {
        word = str.substr(last,i-last); last=i+1;
        while(word.size()&&isWhiteSpace(word[0]) )
        {
            word.erase(0,1);
        }
        while(word.size()&&isWhiteSpace(word[word.size()-1]) )
        {
            word.erase(word.size()-1,1);
        }
        if(!( word.size()==0 && TreatMultiplesAsOne ))
            rc.push_back(word);
        i=str.find_first_of(toks,i+1);
    }
    word = str.substr(last);
    while(word.size()&&isWhiteSpace(word[0]) )
    {
        word.erase(0,1);
    }
    while(word.size()&&isWhiteSpace(word[word.size()-1]) )
    {
        word.erase(word.size()-1,1);
    }
    if(!word.empty ())
        rc.push_back(word);
    return rc;

}





//A neat way is
//std::transform(str.begin(), str.end(), str.begin(), tolower);
//std::transform(str.begin(), str.end(), str.begin(), toupper);

RXSTRING rxtolower(const  RXSTRING &str) {
    RXSTRING rc;
    RXSTRING::const_iterator it;
    for ( it=str.begin() ; it < str.end(); it++ ) {
        rc.push_back (tolower(*it));
    }
    return rc;
}
string rxtolower(const  string &str) {
    string rc;
    string::const_iterator it;
    for ( it=str.begin() ; it < str.end(); it++ ) {
        rc.push_back (tolower(*it));
    }
    return rc;
}
int rxstriptrailing(std::string &s, const std::string &c){ // strip trailing if they are in c
    int rc=0;
    size_t f;
    do{
        f = s.length(); if(f==0) {return rc;}
        if(c.find(s[f-1]) !=string::npos)
            s.erase(f-1);
        else break;
        rc++;
    }while(true);
    return rc;
}
int rxstriptrailing(std::string &s){ // strip trailing whitespace
    int rc=0;
    size_t f;

    do{
        f = s.length(); if(f==0) {return rc;}
        if(s[f-1]==' '){
            s.erase(f-1);
        }
        else if(s[f-1]==0){
            s.erase(f-1);
        }
        else if(isspace(s[f-1])){
            s.erase(f-1);
        }
        else break;
        rc++;
    }while(true);
    return rc;

}
int rxstriptrailing(RXSTRING &s){
    int rc=0;
    size_t f;

    do{
        f = s.length(); if(f==0) {return rc;}
        if(s[f-1]==' '){
            s.erase(f-1);
        }
        else if(s[f-1]==0){
            s.erase(f-1);
        }
        else if(isspace(s[f-1])){
            s.erase(f-1);
        }
        else break;
        rc++;
    }while(true);
    return rc;
}
int rxstripleading(RXSTRING &s)
{
    int rc=0;
    do{
        if(!s.length()) {return rc;}
        if(s[0]==' '){
            s.erase(0,1);
        }
        else if(isspace(s[0])){
            s.erase(0,1);
        }
        else break;
        rc++;
    }while(true);
    return rc;
}


RXSTRING RXStringExtract(const RXSTRING &sin, const RXSTRING &key, bool&found, const RXSTRING &toks)
// we are looking for a sequence
/*
'key'[whitespace]'='[whitespace]xxxxx(one of toks)
and we return xxx
*/
{
    found=false;
    RXSTRING rv;
    int b1=0,b2=0,b3=0;

    size_t i, pos = sin.find(key);
    if(pos==string::npos) // not found. return empty string and 'found=false'
        return rv;
    found=true;
    RXSTRING s =sin.substr(pos); pos=0;
    // walk forwards from the end of key, skipping whitespace
    // if we first hit a non-'=' the result is empty
    // if we first hit a '=' we continue walking until we hit a non-whitespace
    // we read the result from there until we hit a tok which is outside any brackets in the result
    // We allow too many closing brackets

    i = s.find(L"=",pos);
    if(i !=string::npos) pos=i+1;
    for(i=pos;i<s.length();i++) {
        if(     s[i]=='(') b1++;
        else if(s[i]==')') b1--;
        else if(s[i]=='[') b2++;
        else if(s[i]==']') b2--;
        else if(s[i]=='{') b3++;
        else if(s[i]=='}') b3--;
        if(b1>0||b2>0||b3>0)
            continue;
        if(toks.find(s[i]) != string::npos){
            rv = s.substr(pos,i-pos);
            return rv;
        }
    }
    if(b1||b2||b3) {
        QString buf ( "Warning: braCKets dont match in <");
        buf+=QString::fromStdWString(s);
        buf+=">";
        buf+= "\n while extracting <";
        buf += QString::fromStdWString  (key);
        buf+= ">";
        cout << qPrintable(buf)<<endl;
    }
    rv=s.substr(pos);
    return rv;
}
bool isIdentStart(wchar_t ch) {
    return ((L'a' <= ch) && (L'z' >= ch)) || ((L'A' <= ch) && (L'Z' >= ch))
            || (L'_' == ch);
}

bool isIdentCont(wchar_t ch) {
    return ((L'a' <= ch) && (L'z' >= ch)) || ((L'A' <= ch) && (L'Z' >= ch))
            || (L'_' == ch) || (L'.' == ch) || ((L'0' <= ch) && (L'9' >= ch));
}

bool isWhiteSpace(wchar_t ch) {
    return (L' ' == ch) || (L'\t' == ch) || (L'\n' == ch) || (L'\r' == ch);
}

bool isDigit(wchar_t ch) {
    return (L'0' <= ch) && (L'9' >= ch);
}

bool isSymbol(wchar_t ch) {
    return (L'{' == ch) || (L'}' == ch) || (L',' == ch) || (L'=' == ch)
            || (L';' == ch);
}

bool isQuote(wchar_t ch) {
    return (L'\'' == ch) || (L'"' == ch);
}

bool isOnlyNumber(const RXSTRING &s){
    wchar_t*lp;
    size_t nc;
    double x = wcstod(s.c_str(),&lp);
    nc = lp-s.c_str();
    return (nc==s.length()) ;

}
string rxwordstostring(const vector<string> &w,const string &sep)
{
    string rc;
    ;
    for(vector<string>::const_iterator it=w.begin();it!=w.end();++it){
        rc = rc + (*it)+sep;
    }
    rc.erase(rc.end()-sep.length (),rc.end());
    return rc;
}
int rxReplaceWords(string &line,const int Index, vector<string> win, const string &seps){
    // replace the Ith word in line with win.begin(), and successsive words up to the end of win
    //	*line is the string to modify
    //	index is the index of the word in line to start replacing
    //  win is a  list of words

    //	seps are the separators to be used in parsing '*line'

    size_t i,c=0,j;
    vector<string> wds = rxparsestring(line,seps);

    for(i=Index,j=0;i<wds.size ()&&j<win.size();i++,j++){
        wds[i]=win[j]; c++;
    }
    for(;j<win.size();j++){
        wds.push_back(win[j]); c++;
    }

    line = rxwordstostring(wds, seps);

    return c;
}

int rxreplacestring( std::string &s, const std::string &old, const std::string &n)
{
    int rc=0;
    size_t found=s.find(old);

    while( found!=string::npos){
        s.replace (found,old.size(),n);
        found=s.find(old);
        rc++;
    }
    return rc;
}
int rxreplacestring( std::wstring &s, const std::wstring &old, const std::wstring &n)
{
    int rc=0;
    size_t found=s.find(old);

    while( found!=wstring::npos){
        s.replace (found,old.size(),n);
        found=s.find(old);
        rc++;
    }
    return rc;
}
const wchar_t* rxfindinstring( const RXSTRING &haystack, const RXSTRING &needle){
    if(! haystack.size()) // 29 march 2013 valgrind complains
        return 0;
    size_t  i = haystack.find(needle);
    if(i==wstring::npos)
        return 0;
    return haystack.c_str()+i;
}
const char* rxfindinstring( const std::string &haystack, const std::string  &needle){
    if(! haystack.size()) // 29 march 2013 valgrind complains
        return 0;
    size_t  i = haystack.find(needle);
    if(i==string::npos)
        return 0;
    return haystack.c_str()+i;
}
int rxgetword(const std::vector<std::string> wds  ,const int n, char*buf, const int buflen){
    if(buf) *buf=0;
    if(n>=wds.size())
        return 0;
    if(buf&&buflen)
        strncpy(buf,wds[n].c_str(),buflen);
    return 1;
}
// rxfileparse takes filename 'a' into extension, directory and filename

// dir includes its trailing sep.  ext includes its leading dot
int rxfileparse(const char*a ,char*dir,char*fname,char*ext,const int buflen){
    QString qa(a) , qdir,qfname,qext;

    int rc= rxqfileparse(qa ,qdir,qfname,qext );
    if(ext) {*ext='.'; strncpy(& (ext[1]),qPrintable(qext),buflen-1) ;}
    if(dir) { strncpy(ext,qPrintable(qdir),buflen-2) ;
#ifdef linux
        strcat(dir, "/" );
#else
        strcat(dir,"\\");
#endif
    }
    if(fname)  strncpy(ext,qPrintable(qfname),buflen) ;
    return rc;
#ifdef NEVER

    long int lbuflen = buflen;
    const char* k = strrchr(a,'.'); // k is last dot
    const char* j = strrchr(a,s); // j is last dirsep
    if(k<j) k=0;
    if(!k &&!j) {
        if(fname) strncpy(fname,a,buflen);
        *ext=0; *dir=0; return 1;
    }
    if(k && j) {
        j--;
        if(ext) strncpy(ext,k,buflen) ;
        if(dir)  { strncpy(dir,a, buflen); dir[min(j-a,lbuflen-1) ]=0;}
        if(fname) { strncpy(fname,j+1,buflen); fname[min(k-j,lbuflen-1)]=0;}
        return 1;
    }

    if(j){ // and not k
        assert("TODO: step rxfileparse 2"==0);
        j--;
        if(dir)  { strncpy(dir,a, buflen); dir[min(j-a,lbuflen-1) ]=0;}
        if(fname) strncpy(fname,j+1,buflen);
        *ext=0; return 1;
    }

    if(k) {// and not j
        if(dir)  *dir=0;
        if(fname) { strncpy(fname,a,buflen); fname[min(k-a,lbuflen-1)]=0;   }
        if(ext) strncpy(ext,k,buflen) ;
        return 1;
    }
    return 0;
#endif
}

