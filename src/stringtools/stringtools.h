#ifndef _STRINGTOOLS_H__

#define  _STRINGTOOLS_H__

#include <QStringList>
#include <vector>
#include <iostream>
#include <sstream>
#include <string>

#ifndef RXSTRING
#define RXSTRING std::wstring
#endif


#ifndef linux
#include "..\stringtools\UtfConverter.h" // defines FromUtf8 and ToUtf8
#else
#ifndef RXQT
#include "UtfConverter.h"
#else
#include "../stringtools/UtfConverter.h"
#endif
#endif

#ifdef linux
#define RX_QSTRINGTOWSTRING(qs) qs.toStdWString() 
#define RX_WSTRINGTOQSTRING(ws) QString::fromStdWString(ws.c_str())
#else
#define RX_QSTRINGTOWSTRING(qs)  qs.utf16()
#define RX_WSTRINGTOQSTRING(ws) QString::fromUtf16(ws.c_str())
#endif



using namespace UtfConverter;
using namespace std;

#define RXSCRIPTSEPS ":"
#define RXENTITYSEPS ":"
#define RXENTITYSEPS_W L":"
#define RXENTITYSEPSTOWRITE " : "
#define RXE_LSEPSTOWRITE L" : "
#define RXSCRIPTSEPREGEXP "(?![A-Z])[:\\t](?!\\\\)"    //   not[A-Z] then : or \t  then NOT a backslash
#define RXENTITYSEPREGEXP "(?![A-Z])[:\\t](?!\\\\)"    //   not[A-Z] then : or \t  then NOT a backslash

extern QStringList rxParseLine(const QString &qline,const QString &regexp  );

extern RXSTRING rxtolower(const RXSTRING &p);
extern std::string  rxtolower(const std::string &p);
extern QStringList rxqparsestring(const QString &p, const QRegExp &toks,   const bool TreatMultiplesAsOne=false);// simply splits into words.
extern std::vector<std::string> rxparsestring(const std::string &p, const std::string &toks,const bool TreatMultiplesAsOne=false);// simply splits into words.
extern std::vector<std::wstring> rxparsestring(const std::wstring &p, const std::wstring &toks,const bool TreatMultiplesAsOne=false  );// simply splits into words.
extern std::string rxstripcomment(const std::string &s);
extern int rxstriptrailing(std::string &s, const std::string &c);
extern int rxstriptrailing(RXSTRING &s);
extern int rxstriptrailing(std::string &s);
extern int rxstripleading(RXSTRING &s);
extern int rxReplaceWords(string &line,const int Index, vector<string> win, const string &seps);
extern string rxwordstostring(const vector<string> &win,const string &sep);
extern int rxreplacestring( std::string &s, const std::string &old, const std::string &n); 
extern int rxreplacestring( std::wstring &s, const std::wstring &old, const std::wstring &n);
 
extern RXSTRING RXStringExtract(const RXSTRING &s, const RXSTRING &key, bool&found, const RXSTRING &toks=L","); // parses out xxx as in 'key=xxx'
extern const wchar_t* rxfindinstring( const RXSTRING &haystack, const RXSTRING &needle);
extern const char*    rxfindinstring( const std::string  &haystack, const std::string  &needle);
extern int rxgetword(const std::vector<std::string> wds  ,const int n, char*outputbuf=0,const int buflen=0);
extern int rxfileparse(const char*a ,char*dir,char*fname,char*ext,const int buflen);
extern int rxqfileparse(const QString a ,QString &dir,QString &fname,QString &ext );

inline std::wstring to_wstring (const char* t)
{
	if(!t)
		return std::wstring(L"");
	std::wstringstream ss;
	ss << t;
	return ss.str();
}

// a neat way of converting anything to a wstring

template <class T>
inline std::wstring to_wstring (const T& t)
{
	std::wstringstream ss;
	ss << t;
	return ss.str();
}
template <class T>
inline std::string to_string (const T& t)
{
	std::stringstream sss;
	sss << t;
	return sss.str();
}

	bool isIdentStart(wchar_t ch) ;
	bool isIdentCont(wchar_t ch) ;
	bool isWhiteSpace(wchar_t ch) ;
	bool isDigit(wchar_t ch) ;
	bool isSymbol(wchar_t ch) ;
	bool isQuote(wchar_t ch) ;
	bool isOnlyNumber(const RXSTRING &s);



#endif

