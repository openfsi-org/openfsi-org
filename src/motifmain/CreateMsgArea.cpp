/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h" 
#include "GraphicStruct.h"

#include "Xm/Protocols.h"
#include "Xm/PushB.h"
#include "Xm/ToggleB.h"
#include "Xm/DialogS.h"

#include "global_declarations.h"

#include "CreateMsgArea.h"

int CreateMsgArea( Graphic * const g)
{

	Arg al[64];
	int l_ac=0;
	static XmStringCharSet charset;
	charset = (XmStringCharSet) XmSTRING_DEFAULT_CHARSET;


  if(!g || !g->mainWindow) {
	g_World->OutputToClient ("no main window defined !",2);
	return(0);
  }

/* create form for message area */
  l_ac=0;
  XtSetArg(al[l_ac],XmNbottomAttachment,XmATTACH_FORM);l_ac++;
  g->msgform = XmCreateForm(g->mainWindow,(char*) "msgform",al,l_ac);
  XtManageChild(g->msgform);

/* create label for msg - LHS of window*/
  l_ac=0;
  XtSetArg(al[l_ac],XmNlabelString,XmStringCreateLtoR((char*)"LH Message",charset));l_ac++;
  XtSetArg(al[l_ac],XmNbottomAttachment,XmATTACH_FORM);l_ac++;
  XtSetArg(al[l_ac],XmNtopAttachment,XmATTACH_FORM);l_ac++;
  XtSetArg(al[l_ac],XmNleftAttachment,XmATTACH_FORM);l_ac++;
  XtSetArg(al[l_ac],XmNleftOffset,10);l_ac++;
  XtSetArg(al[l_ac],XmNbottomOffset,5);l_ac++;
  XtSetArg(al[l_ac],XmNheight,10);l_ac++;
  XtSetArg(al[l_ac],XmNtopOffset,5);l_ac++;
  g->msgboxLHS = XmCreateLabelGadget(g->msgform,(char*)"msgbox",al,l_ac);
  XtManageChild(g->msgboxLHS);

/* create label for msg - RHS of window*/
  l_ac=0;
  XtSetArg(al[l_ac],XmNlabelString,XmStringCreateLtoR((char*)"RH message",charset));l_ac++;
  XtSetArg(al[l_ac],XmNbottomAttachment,XmATTACH_FORM);l_ac++;
  XtSetArg(al[l_ac],XmNtopAttachment,XmATTACH_FORM);l_ac++;
  XtSetArg(al[l_ac],XmNrightAttachment,XmATTACH_FORM);l_ac++;
  XtSetArg(al[l_ac],XmNrightOffset,10);l_ac++;
  XtSetArg(al[l_ac],XmNbottomOffset,5);l_ac++;
  XtSetArg(al[l_ac],XmNheight,10);l_ac++;
  XtSetArg(al[l_ac],XmNtopOffset,5);l_ac++;
  g->msgboxRHS = XmCreateLabelGadget(g->msgform,(char*)"msgbox",al,l_ac);
  XtManageChild(g->msgboxRHS);

return(1);
}


/* set the text to an appropriate string */

int SetMsgText(Graphic *const g,const char *text)  /* deals with LHS only */
{
	Arg al[64];
	int l_ac=0;
XmString xtext;
static XmStringCharSet charset;

charset = (XmStringCharSet) XmSTRING_DEFAULT_CHARSET;

if(g_Janet) 
	printf(" SetMsgText %s\n",text);


if(!g || !g->msgboxLHS || !text) {
	g_World->OutputToClient ("SetMsgArea - NULL pointer",2);
	return(0);
}
xtext = XmStringCreateLtoR((char*) text, XmSTRING_DEFAULT_CHARSET);
l_ac=0;
XtSetArg(al[l_ac],XmNlabelString,xtext);l_ac++;
XtSetValues(g->msgboxLHS,al,l_ac);
XmUpdateDisplay(g->msgboxLHS);
XmUpdateDisplay(g->msgform); // August 2003
return(1);
}
int SetMsgText2(Graphic *const g,const char *text,int right)  /* deals with both only */
{
	Arg al[64];
	int l_ac=0;

XmString xtext;
//static XmStringCharSet charset;

//charset = (XmStringCharSet) XmSTRING_DEFAULT_CHARSET;
l_ac = 0;
if(g_Janet)
	printf(" SetMsgText2 %s\n",text);


if(!g || !g->msgboxLHS || !g->msgboxRHS || !text) {
	g_World->OutputToClient ("SetMsgArea - NULL pointer",2);
	return(0);
}
xtext = XmStringCreateLtoR((char*) text, XmSTRING_DEFAULT_CHARSET);
l_ac=0;
XtSetArg(al[l_ac],XmNlabelString,xtext);l_ac++;
if(right) {
	XtSetValues(g->msgboxRHS,al,l_ac);
	XmUpdateDisplay(g->msgboxRHS);
	XmUpdateDisplay(g->msgform); // August 2003
}
else {
	XtSetValues(g->msgboxLHS,al,l_ac);
	XmUpdateDisplay(g->msgboxLHS);
	XmUpdateDisplay(g->msgform); // August 2003

}
return(1);
}


