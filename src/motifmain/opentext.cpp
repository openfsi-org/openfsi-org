/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"
/* Text editor display window.
 * A K Molyneaux 16 May 1994.
 */
//#include "GraphicStruct.h"
#include "Global.h"
#include "sailutil.h"
#ifdef _X
	#include "Xm/Protocols.h"
	#include "Xm/Text.h" 
#endif

#include "global_declarations.h"
#include "opentext.h"

#ifdef _X
Widget OpenText(const char *data,const char *name) 
{




        Arg al[64];                    /* Arg List */
        register int ac = 0;           /* Arg Count */
Widget theShell,textwindow=NULL;

XtSetArg(al[ac], XmNallowShellResize, TRUE); ac++;
theShell = XtCreatePopupShell ( "Text", topLevelShellWidgetClass, g_graph[0].topLevel, al, ac );

ac=0;
XtSetArg(al[ac],XmNtitle,name); ac++;
XtSetArg(al[ac],XmNiconName,name); ac++;
XtSetArg(al[ac],XmNwidth,500); ac++;
XtSetArg(al[ac],XmNallowShellResize,True);ac++;
XtSetArg(al[ac],XmNwinGravity,NorthWestGravity);ac++;
XtSetArg(al[ac],XmNinitialState,IconicState);ac++;
XtSetArg(al[ac],XmNdeleteResponse,XmDO_NOTHING); ac++;
XtSetValues(theShell,al,ac);

ac=0;
XtSetArg(al[ac],XmNeditMode,XmMULTI_LINE_EDIT); ac++;
XtSetArg(al[ac],XmNvisualPolicy,XmVARIABLE); ac++;
XtSetArg(al[ac],XmNscrollingPolicy,XmAUTOMATIC);ac++;
XtSetArg(al[ac],XmNscrollBarDisplayPolicy,XmAS_NEEDED);ac++;
XtSetArg(al[ac],XmNcolumns,80);ac++;
XtSetArg(al[ac],XmNrows,20);ac++;
XtSetArg(al[ac],XmNeditable,0);ac++;
XtSetArg(al[ac],XmNvalue,data);ac++;

textwindow = XmCreateScrolledText(theShell,"Text",al,ac);

XtManageChild(textwindow);
XtPopup(theShell,XtGrabNone);
XmUpdateDisplay(theShell);
return(textwindow);

}//Widget OpenText
#endif




