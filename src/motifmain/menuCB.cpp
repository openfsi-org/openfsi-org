/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *    Oct 98   getfilename result insulated. 
 *    11/5/96 no strlin when NO_PANSAIL
 *   15/1/96 in MENU_ATTACH_SAIL buf was incorrectly filled. 
 *    appears to be unused, so commented out
 *    12/1/96 Ncalls  posting removed. SOme debug lines
 *              12.3.95      Calculating <run name>...
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


#include "StdAfx.h"
#include "RelaxWorld.h"
#include "offset.h"
#include "global_declarations.h"
#include "RXSail.h"
#include "finishview.h"
#include "attach.h"
#include "stringutils.h"
#include "CreateMsgArea.h"
#include "asscb.h"
#include "dxfin.h"
 
#include "gen.h"
#include "gcurve.h"
#include "Menu.h"
#include "sailutil.h"
#include "etypes.h"
#include "segprint.h"
#include "checktop.h"
#include "colours.h"
#include "doview.h"
#include "drop.h"
#include "edit.h"
#include "entities.h"
#include "elements.h"
#include "summary.h"
#include "hoopcall.h"

#include "radiomenus.h"

#include "adjlen.h"
#include "camera.h"
#include "cp2.h"
#include "hcupdate.h"		/* replaces HC_Update_Display */
#include "Gflags.h"
#include "getglobal.h"
#include "MakeNewDrawingArea.h"
#include "Menu.h"
 
#include "postutil.h"
#include "redraw.h"

#include "snapshot.h"

#include "script.h"
#include "txt_over_stubs.h"

#include "recolor.h"
#include "files.h"

#include "pansail.h"
#include "printall.h"
#include "ReadBagged.h"
#include "reledit.h"
#include "SaveSail.h"
#include "scedit.h"

#include "summary.h"
#include "trimmenu_s.h"
#include "telnet.h"

#include "ch_row.h"

#include "savesailas.h"
#include "trimmenu.h"
#include "gcurve.h"
#include "question.h"
#include "image.h"
//#include "meshlib.h"
#include "OpenBuildFile.h"
#include "CheckConnects.h"
#include "custom.h"

#include "drawwind.h"
#include "CheckAll.h"
#include "global_declarations.h"
#include "f90_to_c.h"
#include "controlmenu.h" 
#include "menuCB.h"


/* percentage to add to the resize window function */

#define EXTRA 0.05


XtCallbackProc  Menu_TextCB(Widget w,caddr_t client_data,caddr_t call_data) {
// A menu callback function where client_data->lp is expected to be a complete
// script command ready to be passed to the script parser 
// along with the Graphic owning the button.

	char buf[256];
	Graphic *g;
	char *lpin;
	int i;
/* some menu commands don't return straght away so up@date the screen first */


	g = (Graphic*)((MenuID *)client_data)->g; 
	lpin= ((MenuID *)client_data)->lp; 
	i = ((MenuID *)client_data)->id;

//we might insert sd->type or something depending on the value of i

	XmUpdateDisplay( g->topLevel);

	sprintf(buf,"MEx%s", ((MenuID *)client_data)->lp);
	Set_MALLOC_DATA(buf) ;
	assert (i == MENU_TEXT_CB );
	//printf(" Menu_TextCB :: i = %d, filename = '%s' ::  %s\n",i,g->filename, lpin);
	Execute_Script_Line(lpin,g);
return 0;
}


XtCallbackProc  MenuCB(Widget w,caddr_t client_data,caddr_t call_data) {

	Widget topLevel;
	char dum[64];
	Graphic *g;
	char *lpin;
/* some menu commands don't return straght away so up@date the screen first */


	g = (Graphic*)((MenuID *)client_data)->g; 
	lpin= ((MenuID *)client_data)->lp; 
	topLevel =  g->topLevel;
	if(!topLevel)
		puts(" null toplevel");
	else
		XmUpdateDisplay(topLevel);

	sprintf(dum,"MuCB%d", ((MenuID *)client_data)->id);
	Set_MALLOC_DATA(dum) ;

switch (((MenuID *)client_data)->id) {

    case MENU_FILE_CASCADE:

	break;
    case MENU_BUILD_CASCADE:
/*	XtSetSensitive(panelSolveButton,(GetFlag(FL_HASGEOMETRY,GetCurr entSail()) && !(GetFlag(FL_HASPANELS,GetCurr entSail()))));	
	XtSetSensitive(meshButton,(GetFlag(FL_HASPANELS,GetCurre ntSail()) && !GetFlag(FL_HASMESH,GetCurr entSail())));	
*/
	break;

    case MENU_ANALYSIS_CASCADE:
	//XtSetSensitive(trimButton,!g_TrimExists);	
	break;

    case MENU_AERO_WIND: {
            g_wind_name= getfilename(g->topLevel,"win*","Open WIND File",g_currentDir,PC_FILE_READ);
            if(!g_wind_name.empty( ))
                Draw_Wind((float)0.0,g_PansailIn.Reference_Length);
            break;
    }
    case MENU_UTILS_OPEN_SUMMARY: {
      Do_Utils_Open_Summary(NULL,g);
	break;
    }
    case MENU_UTILS_CLOSE_SUMMARY: {
      Do_Utils_Close_Summary(NULL,g);
      Do_Utils_Close_SummaryIn(NULL,g);
	break;
    }
    case MENU_UTILS_OPEN_IN: {
      Do_Utils_Open_In(NULL,g);
	break;
    }
    case MENU_UTILS_CHOOSE_RUN: {
      Do_Utils_Choose_Line(NULL,g);
      break;
    }
    case MENU_IMPORT_DXF: {
	char *filename;
	long key;
	if((filename = getfilename(g->topLevel,"*.dxf","Open DXF File",g_currentDir,PC_FILE_READ))) {
	printf(" import dxf not checked\n");
	HC_Open_Segment_By_Key(g->m_ModelSeg); 
	  key = dxfin(g->GSail,filename);
	  if(key != 0) {
		HC_Open_Segment_By_Key(g->GSail->PostProcNonExclusive());	  
		HC_Open_Segment("dxfin"); // try this. It still doesnt transform with the boat.
			HC_Include_Segment_By_Key(key);
		HC_Close_Segment();
		HC_Close_Segment();
	  }
	HC_Close_Segment();
	RXFREE(filename);	
	}
	break;
    }

    case MENU_File_Open: {

      if(g_TrimExists) 
	g_World->OutputToClient ("Please close Trim menu First",2);
      else
		Execute_Script_Line("open boat: *.in\n",g);
      
      break;
    }

    case MENU_SNAPSHOT: {
	char  *theFile;   
	  if((theFile = getfilename(g->topLevel,"*.mac","Snapshot Macro?",g_currentDir,PC_FILE_WRITE))) {
		snapshot(theFile);
		RXFREE(theFile);
		}     
      break;
    }
    case MENU_RUN_SCRIPT: {
	char  *theFile;   
	  if((theFile = getfilename(g->topLevel,"*.mac","Which macro to run?",g_currentDir,PC_FILE_READ))) {
		Read_Script(theFile);
		RXFREE(theFile);
		}     
      break;
    }
	case MENU_RECORD_SCRIPT: {
		char  *theFile;   
		if((theFile = getfilename(g->topLevel,"*.mac","Which macro to Record?",g_currentDir,PC_FILE_WRITE))) {
		if(!Open_Script_File(theFile)) g_World->OutputToClient ("Couldnt open file",2);
		RXFREE(theFile);
		}     
	break;
	}
    	case MENU_END_SCRIPT: {
		Do_Close_Script();
		break;
	}
	case MENU_FILE_CWD: 
		Do_CWD(NULL,g);
		break;
    
		case MENU_File_New: {
		if(g_TrimExists) 
			g_World->OutputToClient ("Please close Trim menu First",2);
		else
			Do_Generate_Boat(NULL,g);
	break;
	}
    case MENU_BOAT_SAVE: {
 
      	puts(" MENU_BOAT_SAVE calls Do_Save_Script which calls write_script");
        Do_Save_Script(g_boat,g);

	break;
    }

    case MENU_FILE_EXIT: {

	Do_Exit(NULL,g);

	break;
	}
    case MENU_SAIL_NEW:		/* from the sail window */
    case MENU_Sail_New: {	/* from the BOAT window */
	Execute_Script_Line("open sail:\n",g);
	break;
    }
    case MENU_Sail_Open: {
	char buffer[512];
	sprintf(buffer, "open bag: %s/*.bag\n",g_currentDir);
	Execute_Script_Line(buffer,g);

	break;
	}
    case MENU_SAIL_NEW_SUBMENU: {
	char buffer[512];
	sprintf(buffer, "open sail:%s/%s.in\n",templateDir,lpin);
	//printf(" MENUSAIL_NEW_SUBMENU %s\n",buffer);
	Execute_Script_Line(buffer,g);

	break;
	}
#ifdef NEVER
    case MENU_SAIL_APPEND: {  /* also called for the boat append */

      RXEntity_p  p=0;

	Do_Append("solve as well",g);
    
        g_World->OutputToClient ("(MENU_SAIL_APPEND) p = PC_Get_Key_By_Ptr(g_boat,sd); // april 2005",4);
        
        if(!p) {
	  g_World->OutputToClient ("Sail entity not found - sail not hoisted",2);
	}
        else if(strieq(p->name(),"boat")) {
	  g_World->OutputToClient ("Just appending and solving in the boat",1);
	}
        else {
	  Do_Drop_Sail(NULL,g);  /*  was DropSail(p); topology might have changed radically */

	  Do_Hoist(NULL,g);
	  CheckAll(); printf(" back in  MENU_SAIL_APPEND\n");
	}
        HC_Update_Display();
	break;
    }
#endif
    case MENU_ATTACH_SAIL: {
	//Execute_Script_Line("hoist: \n",g);  doesnt work  
	 Do_Hoist("hoist: \n",g);
	break;
    }
    
    case MENU_CAMERA_NEW:
	Execute_Script_Line("open camera",NULL);  // was Do_Open("open camera",NULL);
	break;
   case MENU_CAMERA_UP: {
   
        VECTOR pos,target,up;
        char proj[32];
        float width,height;	
	HC_Open_Segment_By_Key(g->m_ViewSeg); 
		Record_Default_Camera();
	 	 HC_Show_Net_Camera(&pos,&target,&up,&width,&height,proj);
	 	 HC_Set_Camera(&pos,&target,&up,width,height,proj);
		 HC_Set_Camera_Up_Vector(0.0,0.0,1.0);
	HC_Close_Segment();
	if(g->axisSeg) {
	HC_Open_Segment_By_Key(g->axisSeg); 
		 HC_Set_Camera_Up_Vector(0.0,0.0,1.0);
	HC_Close_Segment();
	}
	HC_Update_Display();
        break;
    }
    case MENU_CAMERA_EDIT: {

        VECTOR pos,target,up;
        char proj[32];
        float width,height;
        char line[256];	
	fflush(stdout);
	HC_Open_Segment_By_Key(g->m_ViewSeg); {
	Record_Default_Camera();
	  HC_Show_Net_Camera(&pos,&target,&up,&width,&height,proj);
	  printf("Enter new pos X (%6.2f):",pos.x);
	  if(!is_empty(fgets(line,256,stdin))) 
	    pos.x = atof(line);
	  printf("pos.x set to %f\n",pos.x);
          printf("Enter new pos Y (%6.2f):",pos.y);
          if(!is_empty(fgets(line,256,stdin)))
            pos.y = atof(line);
	  printf("pos.y set to %f\n",pos.y);
          printf("Enter new pos Z (%6.2f):",pos.z);
          if(!is_empty(fgets(line,256,stdin)))
            pos.z = atof(line);
          printf("Enter new target X (%6.2f):",target.x);
          if(!is_empty(fgets(line,256,stdin)))
            target.x = atof(line);
          printf("Enter new target Y (%6.2f):",target.y);
          if(!is_empty(fgets(line,256,stdin)))
            target.y = atof(line);
          printf("Enter new target Z (%6.2f):",target.z);
          if(!is_empty(fgets(line,256,stdin)))
            target.z = atof(line);
	  printf("Enter new width (%6.2f):",width);
	  if(!is_empty(fgets(line,256,stdin)))
	    width = atof(line);
	  printf("Enter new height (%6.2f):",height);
          if(!is_empty(fgets(line,256,stdin)))
            height = atof(line);
	  printf("Enter new proj (%s):",proj);
          if(!is_empty(fgets(line,256,stdin))) {
	    if(stristr(line,"pe"))
	      strcpy(proj,"perspective");
	    else
	      strcpy(proj,"orthographic");
	  }
	  HC_Set_Camera(&pos,&target,&up,width,height,proj);
	} HC_Close_Segment();
	HC_Update_Display();
	fflush(stdout);
        break;
    }
    case MENU_CAMERA_SAVEAS: {
	char buf[128];
	char fname[64];
	char *theFile;
	char *lp;
	VECTOR pos,target,up;
	char proj[32];
	float width,height;
	FILE *fp;
	char line[256];
	int depth=0;
	RXEntity_p  e;
	
	  if((theFile = getfilename(g->topLevel,"*","Enter name for  camera file",g_currentDir,PC_FILE_WRITE))) {

	    lp = strchr(theFile,'.');
	    if(lp)
	      *lp=0;

	    sprintf(fname,"%s",theFile);

	    RXFREE(theFile);


	    strcpy(buf,fname);
	    strcat(fname,".txt");
	    HC_Open_Segment_By_Key(g->m_ViewSeg);
	    HC_Show_Net_Camera(&pos,&target,&up,&width,&height,proj);
	    HC_Close_Segment();
	    fp = FOPEN(fname,"w");
	    lp = strrchr(buf,'/');
	    if(!lp)
	      lp=buf;
	    else
	      lp++;
	    sprintf(line,"camera\t%s\t%f %f %f\t %f %f %f \t %f %f %f\t %f\t %f\t %s\n",
		    lp,pos.x,pos.y,pos.z,target.x,target.y,target.z,up.x,up.y,up.z,width,height,proj);
	    fprintf(fp,"%s",line);
	    FCLOSE(fp);

	    depth = 0;
	    PC_Set_Separators(line);
	    e = Just_Read_Card(g_boat,line,"camera",lp,&depth);
	    Resolve_Camera_Card(e);
	    g_World->OutputToClient ("camera saved\n, now save the boat\n",2);

          }

	break;
    }

    case MENU_CAMERA_EXPORT_HMF: {
	char fname[256],line[256];
	char *theFile;
	  if((theFile = getfilename(g->topLevel,"*","Enter name for export file",g_currentDir,PC_FILE_WRITE))) {
	  	sprintf(fname,"%s",theFile);
	    	RXFREE(theFile);
		sprintf(line,"Export : : %s : ",fname);
		Do_Export_Pic(line,g);

          }
	break;
    }
   case MENU_CAMERA_READ_HMF: {
	char buf[256],fname[256],hmfname[256];
	char *theFile;

	  if((theFile = getfilename(g->topLevel,"*.hmf","Enter name for HMF file",g_currentDir,PC_FILE_READ))) {
	   	 if(!strstr(theFile,".hmf"))
	  		sprintf(fname,"%s.hmf",theFile);
	   	 else
	  		sprintf(fname,"%s",theFile);
	    	RXFREE(theFile);

	    	HC_Show_Segment(g->m_ModelSeg,buf);
#ifdef ibm_RS6000
	sprintf(hmfname,"%s",fname);
#else
	sprintf(hmfname,"'%s'",fname);
#endif

	    	HC_Read_Metafile(hmfname,buf," "); /* restore state"); */
          }
	break;
    }

    case MENU_CAMERA_CLOSE:
	ViewDeleteCB((Widget) NULL,(XtPointer)g,(XtPointer) NULL); 
	XtUnrealizeWidget(g->topLevel);XtDestroyWidget(g->topLevel);
        break;

      case MENU_VIEW_ATTRIBUTES:  {
	DoAttributeDlg(g->mainWindow,g);
	break;
      }
      case MENU_CAMERA_DEBUG_1 :  {
	DoExpOptDlg(g->mainWindow,g);
	break;
      }
     case MENU_CAMERA_DEBUG_2 :  {
	g_World->OutputToClient ("MENU_DEBUG_2",2);

	break;
      }
	
	
	/* events from the sail windows */
      case MENU_BOAT_SAVEAS: 
      case MENU_SAIL_SAVEAS: {

	if(g->GSail == NULL) break;

	if(!g->GSail->GetFlag(FL_HASPANELS)){
	  g->GSail->SetFlag(FL_HASPANELS,1);  /* not needed now */
	}
	if(!g->GSail->GetFlag(FL_HASMESH )){
	  if(g_ArgDebug) printf("AUTO meshing \n");
	  Do_Mesh("nothing",g);		/* mesh it anyway */
	}
	
	if(g->GSail->IsBoat()) {
	      	puts(" MENU_BOAT_SAVEAS calls SaveSailAs which eventually calls SaveSail");
	 	SaveSailAs(g->mainWindow,g,WT_BOATFILE);
	}
	else {
	  SaveSailAs(g->mainWindow,g,WT_DESIGN);
	}
	//g->GSail->SetFlag(FL_MESH_HAS_CHANGED );


	break;
      }
      case MENU_SAIL_SAVE: {
	char info[128];

	if(!g->GSail->IsOK() ) {
	  g_World->OutputToClient ("need a sail name !",2);
	  sprintf(info,"script.NULL.in");
	}
	else {
	  sprintf(info,"script.%s.in",g->GSail->GetType().c_str() );
	}
	g->GSail->Write_Script(info,1); //write basic info 
	strcat(info,".gen");
	g->GSail->Write_Script(info,0);  //write generated info

	
	break;
      }
	
      case MENU_SAVE_STATEAS: {
	int i; char line[256];
	
	for(i=0;i<g_nChildren;i++) {
	 	 if(g_saveChildren[i] == w) {
			sprintf(line,"Save State : %s ", g_sailObject[i]->GetType().c_str());
			Do_Save_State(line, g);
			break;
	  	}
	
	}

	break;
      }
      case MENU_DROP_SAIL: {
	int i; char line[256];
	for(i=0;i<g_nChildren;i++) {
	  if(g_dropChildren[i] == w) {
		sprintf(line,"Drop : %s ", g_sailObject[i]->GetType().c_str());
 		Execute_Script_Line(line,g); 
	  	break;
	  }
	}
	HC_Update_Display();
	break;
      }
      case MENU_LOAD_STATE_BY_INTERP: {
	int i; char line[256];

	for(i=0;i<g_nChildren;i++) {
	  	if(g_loadinterpChildren[i] == w) {
 			SAIL *l_sail = g_sailObject[i];
			sprintf(line,"Load State : %s :*.ndd  ", g_sailObject[i]->GetType().c_str());
        	      	Do_Load_State( NULL, l_sail->Graphic() ) ; // the logic of this fn NEEDS a null line. Yuch
	   		break;
	  	}
	}
	break;
      }
	
      case MENU_SAIL_CLOSE:
	SailDeleteCB((Widget) NULL,(XtPointer)g,(XtPointer) NULL); 
	XtUnrealizeWidget(g->topLevel); XtDestroyWidget(g->topLevel); 
 
	break;
  case MENU_MAT_COLOURCHART:
	 Do_Create_ColourWindow(NULL, g);
	break;
	
    case MENU_MAT_CLOSE:
	 Do_Delete_MatWindow(g);
	break;

      case MENU_CAMERAS:
      case MENU_CAMERAS+1:
      case MENU_CAMERAS+2:
      case MENU_CAMERAS+3:
      case MENU_CAMERAS+4:
      case MENU_CAMERAS+5:
      case MENU_CAMERAS+6:
      case MENU_CAMERAS+7:
      case MENU_CAMERAS+8:
      case MENU_CAMERAS+9:
      case MENU_CAMERAS+10:
      case MENU_CAMERAS+11:
      case MENU_CAMERAS+12:
      case MENU_CAMERAS+13:
      case MENU_CAMERAS+14:
      case MENU_CAMERAS+15:
      case MENU_CAMERAS+16:
      case MENU_CAMERAS+17:
      case MENU_CAMERAS+18:
      case MENU_CAMERAS+19:
      case MENU_CAMERAS+20:
      case MENU_CAMERAS+21:
      case MENU_CAMERAS+22:
      case MENU_CAMERAS+23:
      case MENU_CAMERAS+24:
      case MENU_CAMERAS+25:
      case MENU_CAMERAS+26:
      case MENU_CAMERAS+27:
      case MENU_CAMERAS+28:
      case MENU_CAMERAS+29:
      case MENU_CAMERAS+30:

	SetHardCamera(g_camPlist[(((MenuID *)client_data)->id - MENU_CAMERAS)],g);
	//AttachCurrentObjects();
	Finish_This_View(g);	

    break;   
   case MENU_PREVIOUS_CAMERA:
	SetHardCamera(NULL,g);
	Finish_This_View(g);
	
    break;  
 
    case MENU_VIEW_ZOOMIN:  {
	g->ZoomFlag = EH_ROTATE; // see end of graphicstruct.h for options 
	HC_Open_Segment_By_Key(g->m_ViewSeg);
		Record_Default_Camera();
	HC_Close_Segment();
#ifdef USE_CURSORS
	XDefineCursor(XtDisplay(g->workArea),XtWindow(g->workArea),crosshair_cursor);
#endif
	if(g_ArgDebug)printf("Set Zoom Flag ON\n");
	break;
        }
    case MENU_VIEW_WIREFRAME:  {
	char driver[256];
	  HC_Show_Owner_By_Key(g->m_ViewSeg,driver);
	  HC_Open_Segment(driver);
	  if(XmToggleButtonGadgetGetState(w))  {
		/* lock the faces OFF on the camerabase driver */
		HC_Set_Visibility("faces=off");
		HC_Set_Rendering_Options("attribute lock=(visibility=(faces))");
		}
	  else {
		HC_Set_Visibility("faces=on");
		HC_Set_Rendering_Options("no attribute lock");
	  }
	 HC_Close_Segment();	  
	HC_Update_Display();
        break;	
        }

    case MENU_VIEW_RESIZE:  {
		//Do_Resize("nothing",g);
		Execute_Script_Line("resize: only",g);
	break;
			
        }

    case MENU_VIEW_ROTATE_LEFT22:
    case MENU_VIEW_ROTATE_LEFT90:
    case MENU_VIEW_ROTATE_RIGHT22:
    case MENU_VIEW_ROTATE_RIGHT90:
    case MENU_VIEW_ROTATE_DOWN22:
    case MENU_VIEW_ROTATE_DOWN90:
    case MENU_VIEW_ROTATE_UP22:
    case MENU_VIEW_ROTATE_UP90:  {
	float around,over;
	VECTOR pos,target,up;
	char proj[32];
	long segkey;
	float width,height;
		switch(((MenuID *)client_data)->id) {
		    case MENU_VIEW_ROTATE_LEFT22:
			around = -22.5;
			over = 0.0;
		    break;
    			case MENU_VIEW_ROTATE_LEFT90:
			around = -90;
			over = 0.0;
		    break;
		    case MENU_VIEW_ROTATE_RIGHT22:
			around = 22.5;
			over = 0.0;
		    break;
		    case MENU_VIEW_ROTATE_RIGHT90:
			around = 90;
			over = 0.0;
		    break;
		    case MENU_VIEW_ROTATE_DOWN22:
			around = 0.0;
			over = -22.5;
		    break;
		    case MENU_VIEW_ROTATE_DOWN90:
			around = 0.0;
			over = -90;
		    break;
		    case MENU_VIEW_ROTATE_UP22:
			around = 0;
			over = 22.5;
		    break;
		    case MENU_VIEW_ROTATE_UP90:  
			around = 0;
			over = 90;
		    break;
		    default:
			around = over = 0.0;
		    break;
		}
	segkey = g->m_ViewSeg;
	if(!segkey) {
		g_World->OutputToClient ("no segment found!! ",2);
		break;
	}
	HC_Open_Segment_By_Key(segkey);
		Record_Default_Camera();
		HC_Show_Net_Camera(&pos,&target,&up,&width,&height,proj);
		HC_Set_Camera(&pos,&target,&up,width,height,proj);
		HC_Orbit_Camera(around,over);
	HC_Close_Segment();
	HC_Open_Segment_By_Key(g->axisSeg);
		HC_Set_Camera_Target(0.0,0.0,0.0);
		  HC_Orbit_Camera(around,over);
		HC_Set_Camera_Target(0.5,0.5,0.5);
	HC_Close_Segment();
	HC_Update_Display();
	
	break;
     }
     case MENU_DEBUG_WRITEALL: {
		char fname[32];
		//long segkey;
		//segkey = g->baseSeg;
	
//#ifdef ibm_rs6000 
#ifdef linux
	sprintf(fname, "root.hmf");
#else 
	sprintf(fname, "'root.hmf'");
#endif
	printf("Printing metafile %s \n",fname);
	FILE *fp=fopen("mysegprint.txt","w");
	SegmentPrint("/", 0,fp);
	fclose(fp);
#define HMF_OPTIONS "save state"
//#define HMF_OPTIONS "no follow cross-references, no save state"
	if(g->GSail->IsBoat()) {
    		HC_Write_Metafile("/",fname,HMF_OPTIONS);
	}
	else {
    		HC_Write_Metafile("/",fname,HMF_OPTIONS);
	}		
        break;
     }

   case MENU_VIEW_SET_LIGHT: {
		long segkey;
	float x=0,y=0,z=0; int OK=1;
 	char line[256];
		segkey = g->m_ViewSeg;
	
	    printf("Enter new Light X ");
  	        if(!is_empty(fgets(line,256,stdin)))
    	    	    x = atof(line);
	else
		OK=0;
    	      printf("Enter new Light Y ");
   	       if(!is_empty(fgets(line,256,stdin)))
    		        y = atof(line);
	else
		OK=0;
       	   printf("Enter new Light Z ");
      	    if(!is_empty(fgets(line,256,stdin)))
      	      z = atof(line);
	else
		OK=0;


	if(OK) {
		HC_Open_Segment_By_Key(segkey);
	
			HC_Open_Segment("distant_lights");
				printf(" ADD light at %f %f %f\n",x,y,z);
				HC_Insert_Distant_Light(x,y,z);
			HC_Close_Segment();		
		HC_Close_Segment();
		HC_Update_Display();
	}		
        break;
     }

   case MENU_VIEW_SET_COLOR: {
	long segkey;
	int OK=1;
 	char line[256],buf[512];
	segkey = g->m_ViewSeg;
	HC_Open_Segment_By_Key(segkey);
		HC_Show_Net_Color(buf);
		printf(" net color is \n<%s>\n",buf);	
		printf(" Root Color ? "); 
		if(is_empty(fgets(line,255,stdin))) OK=0;

		if(OK) {
			PC_Strip_Trailing(line);	
			printf(" set to <%s>\n",line);
			HC_Set_Color(line);
			HC_Show_Net_Color(buf);
			printf(" net color now \n<%s>\n",buf);		
			HC_Update_Display();
		}
	HC_Close_Segment();		
        break;
     }
     case MENU_ANALYSIS_CALCNOW: {
   
//       	char buf[256];
//       	g_Relax_Tolerance Relax_Flag_Type relaxflagcommon;  
//        Post_Wind_Details(g_boat);
//	Do_ResetAll(NULL,g);  // FORCE all sails to solve 
//	Set_Kill_Sensitivity(1);
//        relaxflagcommon.Stop_Running = 0;
//        sprintf(buf,"Calculating %s...",Run_Name);
//        SetMsgText(g,buf);
	Do_Calculate("MyRun",g); // DONT CALL WITH NON-NULL see script.c 
//	Set_Kill_Sensitivity(0);
	SetMsgText(g,"Ready after analysis");
	 relaxflagcommon.Stop_Running = 0;
      }
	
        break;
     case MENU_ANALYSIS_KILL: {
			 XtSetSensitive(w,0); /* turn it off - don't want to stop twice */
			 SetMsgText(g,"Killed...");
			 relaxflagcommon.Stop_Running = 1;

     }	
        break;

     case MENU_DEBUG_PRINTALLENTITIES: {
	cf_model_hardcopy();
	Print_All_Entities(g->GSail,"Menu");

	mkGFlag(GLOBAL_DO_DEBUG_TEXT);
        break;
     }	

      case MENU_EXPORT_PANELS: {
		Do_Export_Panels(NULL,g);
		break;
    }


     case MENU_DEBUG_DISPLAYNODES: {
	long segkey;

		segkey = g->m_ModelSeg;
		if(!segkey) break;

		HC_Open_Segment_By_Key(segkey);
			HC_Open_Segment("data");
			cout << "NO display nodes"<<endl;
			//	Display_Nodes(g->GSail);
			//	Display_Edges(g->GSail);
			//	Display_Triangles(g->GSail);
			HC_Close_Segment();
		HC_Close_Segment();	
  	break;
     }	

     case MENU_SET_PRESTRESS: {
	char buf[128];
	static double val[3] = {0.0,0.0,0.0}; /* requires InitAuto */
	double redfac=1;


	fprintf(stdout,"Enter Pre-Stress x : (was %f) ",val[0]);
	fgets(buf,128,stdin);
	val[0] = atof(buf);
	fprintf(stdout,"Enter Pre-Stress y  :(was %f)  ",val[1]);
	fgets(buf,128,stdin);
	val[1] = atof(buf);
	fprintf(stdout,"Enter Pre-Stress xy  :(was %f)  ",val[2]);
	fgets(buf,128,stdin);
	val[2] = atof(buf);
	fprintf(stdout,"Enter Reduce Factor (was %f)  : ",redfac); // triloading_.reduce_factor );
	fgets(buf,128,stdin);
	redfac = atof(buf);
	fprintf(stdout,"Enter CONVERGENCE TOL  :((was %f) ", g_Relax_Tolerance);
	fgets(buf,128,stdin);
	g_Relax_Tolerance = atof(buf);
	fprintf(stdout,"Enter ITERATIONS  : ");
	fgets(buf,128,stdin);
	g_iterations = atoi(buf);

	printf("ITERATIONS set to %d\n",g_iterations);

       // triloading_.reduce_factor = redfac;
	printf("reduce_factor NOTTTTTTTTTTTTTTTTTTTTTT set to %12.5f\n",redfac);

	g->GSail->SetOneConstantPreStress(val);

    }
    break;
   case MENU_FORM_FIND: {
	int k;
	char buf[256];
 
	//if(triloading_.use_prestress == 0)*) g_World->OutputToClient (" Remember no prestress setting",2);

	relaxflagcommon.Stop_Running = 0;


	//XtSetSensitive(w,0); /* turn it off - don't want two analyses running !!*/
	//XtSetSensitive(analysisKillWidget,1);  
   	Set_Kill_Sensitivity(1);
	for(k=0;k<g_iterations;k++) {
		 printf(" \n\n ITERATION %d\n", k);
		 Do_Reset_All_Edges("FormFinding",NULL);  
         		 sprintf(buf,"Formfinding(%d of %d)",k,g_iterations);
         	 	 SetMsgText(g,buf);
		 Do_Calculate(NULL,g); /* DONT CALL WITH NON-NULL see script.c */
		 if(  relaxflagcommon.Stop_Running ) break;
	}
		// XtSetSensitive(w,1); /* re-enable */
		 //XtSetSensitive(analysisKillWidget,0);
		Set_Kill_Sensitivity(0);
		 SetMsgText(g,"Ready");

      }
	break; 
      case MENU_RESET_ALL_EDGES: {
	Do_Reset_All_Edges(NULL,NULL);  
      }
	break; 

      case MENU_SHRINK_ALL_EDGES: {
	Do_Shrink_All_Edges(NULL,NULL);  
      }
	break;

     case MENU_RESET_ONE_EDGES: {

       Do_Reset_One_Edges(g->GSail);  
    }
	break;
     case MENU_SHRINK_ONE_EDGES: {
       Do_Shrink_One_Edges(g->GSail);  
    }
    break;

      case MENU_OPTIONS_OVERLAY: {

       pop_overlay_box(g,g->mainWindow);


     }
        break;

    case MENU_OPTIMISE_INIT : {
	  /* read a list of ns state variable names and the goal name
	  	establish the two default perturbations for each state variable	 */
/* 	char *filename;
	A.coeffs=NULL; A.nc =0;
	A.Restraints=NULL; A.nres =0;
	A.History=NULL;  A.np=0	;
	A.ns=0;
	if(filename = getfilename(g->topLevel,"*.txt","Open Optimisation File",g_currentDir,PC_FILE_READ)) {

		Read_Optimisation(&A,filename);
		Create_Soft_Constraints(&A);
		RXFREE(filename);
	} */
	Do_Initialise_Optimisation(NULL, g);

     }
        break;
    case MENU_OPTIMISE_LOAD_HISTORY: {
	Do_Load_Optimisation_History(NULL, g);
        break;
     }

    case MENU_OPTIMISE:{ printf("MENU_OPTIMISE\n");
	Do_Optimise(NULL, g);
        break;}

     case MENU_SAIL_LIST_ENTITY:   
	g_Select_Action= LIST_ENTITY;    puts(" set g_Select_Action=LIST_ENTIT\n");
	break;
     case MENU_SAIL_MATERIAL_ENTITY:  
	if(XmToggleButtonGadgetGetState(w)   ) { 
		g_Select_Action= ENTITY_MATERIAL;  
		if(HC_QShow_Existence( "/.../mat_button" ,"self"))
			HC_QSet_Visibility("/.../mat_button","on");

	}
	else {
		g_Select_Action= 0; 
		g_MatEnt=NULL; 
		if(HC_QShow_Existence( "/.../mat_button" ,"self"))
			HC_QSet_Visibility("/.../mat_button","off");
		}
	HC_Update_Display();
	break;


  case MENU_LOAD_DEFFILE: {
     GetGlobalValues(DEFAULTS_FILE);
      }
	break;
  case MENU_EDIT_DEFFILE: {  
	Edit_Defaults_File();
      }
	break;
 case MENU_DEBUG_2: {
	List_C_Files();
   	 Check_All_Heap(DBG_FULLPRINT);
      }
	break;
      case MENU_SEAMCURVE_EDITS: {


	Gauss_All_Panels(PCG_DEFLECTED,g->GSail );
	Make_Gauss_Curves(g->GSail);
	printf("SeamCurve Edits DONE (new gausscurves)\nNOW YOU SHOULD SAVE AND QUIT\n");
      }
	break;

     case MENU_RELSITE_EDITS: {
    
 	 printf("TODO: FindZlink0_of_Psides is disabled\n");
	 
       printf("calling FindZlink0_of_Psides\n");
      printf("done (%d returned)\n",FindZlink0_of_Psides(g->GSail));

       Make_Relsite_Edits(g->GSail);
      printf("RelSite Edits DONE \n");
       Make_SeamCurve_Edits(g->GSail);
      printf("RelSite and SC  Edits DONE \n NOW YOU SHOULD DO SC edits\n");
      }
	break;

     case MENU_EDIT_SEAMCURVE: {
       if(do_entity_edit(g->topLevel,g->GSail,"curve,edge,seamcurve")) {
			 Do_Solve(NULL, g);
			 Do_Mesh(NULL,g);
#ifdef _C_CONNECTS
	 assert("DeleteConnects(sd);"==0);  /* topology might have changed radically */
#endif
	   g_boat->Pre_Process(NULL);
	 mkGFlag(GLOBAL_DO_TRANSFORMS);
       }
     }
	break;


/*	UserSelection2("curve,seamcurve,edge,compound curve" ,"?","Please Select a SEAM to edit",g);
*/
/*	get_best_visual (g); */



     case MENU_OPTIONS_SET: {

	HC_KEY segkey;
	Widget post_processor;
	char *sailnames[MDIM];
	int i, nsails;

	std::vector<class RXSail*> ll = g_World->ListModels(RXSAIL_ALLHOISTED); // tested OK
	nsails = ll.size();
	
	if(!nsails) {
	  g_World->OutputToClient ("NO SAILS",2);
	  break;
	}
 	for(i=0;i<nsails;i++) {
 		SAIL *s = ll[i];
 		sailnames[i]=STRDUP(s->GetType().c_str());
 	}

	segkey = g->m_ModelSeg;
	if(!segkey) break;
	SetMsgText(g,"Opening Options Set...");
	XtSetSensitive(g->attributeButton,0); /* turn it off - don't want two !!*/

	post_processor = create_post_processor(g->topLevel,nsails,sailnames,ll,g);

	XtManageChild (post_processor);
	XtRealizeWidget (post_processor); 
	XtResizeWindow(post_processor);
	g->options_menu = post_processor;
	SetMsgText(g,"Ready");
	HC_Update_Display();
	
      }
	break;

     case MENU_SET_COLOR: {


	if(g->GSail) {
		SetMsgText(g,"Enter color on console...");
				readColor();
			SetMsgText(g,"Ready");
	}
    }
    break;
     case MENU_RECOLOR: {
	char fname[128];
	static int count=0;

	if(XmToggleButtonGadgetGetState(w))  {
		sprintf(fname,"addfab%d.in",count);
		ReColFP = FOPEN(fname,"w");
		ReColor = 1;
	}
	else {
		ReColor = 0;
		FCLOSE(ReColFP);
		count++;
	}
        break;	

    }
    break;

/* Paneling routines & meshing routines */
		case MENU_SAIL_BUILD_MESH:{					 /* requires panel solve first */
			HC_KEY segkey;
		cout <<" TODO: MENU_SAIL_BUILD_MESH " <<endl;		

			break;
			
        }
//	case MENU_SAIL_BUILD_SETFABRIC:{		 /* requires panel solve first */
//			SetCu rrentSail(g->GSail);
//			do_set_fabric_dialog(g->topLevel,g->GSail);
//			break;
//        }
	case MENU_SAIL_BUILD_DELETE:{			 /* requires panel solve first */
			Delete_OK=1;
  			 printf(" Turning Delete_OK ON\n");
			break;
			
        }
	case MENU_SAIL_BUILD_MOVESITE:{		 /* requires panel solve first */
			puts("MENU_SAIL_BUILD_MOVESITE: NOT IMPLEMENTED");
			//do_movesite_dialog(g->topLevel,g->GSail); 
		
			break;
			
        }
    case MENU_SAIL_BUILD_ASK_QUEST: {
		HC_Open_Segment_By_Key(g->m_ModelSeg);
			HC_Open_Segment("data"); 
			if(Finish_All_Questions(g->GSail,1)) { 
					XmUpdateDisplay( g->topLevel);
					Do_Solve(NULL, g);
					Do_Mesh(NULL,g);
					g->GSail->SetFlag(FL_NEEDS_GAUSSING); //! so it goes again??
					g_boat->Pre_Process(NULL);
					g_World->Bring_All_Models_To_Date(1); // This does most of what the above few lines do 
					mkGFlag(GLOBAL_DO_TRANSFORMS); 
      			 }
				 else printf(" Finish All Questions returned 0\n");
		HC_Close_Segment();
		HC_Close_Segment();
		break;

     }
	case MENU_SAIL_BUILD_PANELSOLVE:{
			long segkey;

			if(g->GSail == NULL) break;

			if(!g->GSail->GetFlag(FL_HASGEOMETRY)) break;
			segkey = g->m_ModelSeg;
			if(!segkey) break;
			SetMsgText(g,"Finding Panels...");
			HC_Open_Segment_By_Key(segkey);
				HC_Open_Segment("data");
				    g->GSail->Pre_Process(NULL);
				HC_Close_Segment();
			HC_Close_Segment();
			SetMsgText(g,"Ready");
			g->GSail->SetFlag(FL_HASPANELS,1);

		HC_Update_Display();
		break;
			
        }

/* Quantity buttons */
 //    case MENU_QUANTITY_SHAPE: removed Jan 2009 
 //    case MENU_QUANTITY_SHAPE+1:
 //    case MENU_QUANTITY_SHAPE+2:
 //    case MENU_QUANTITY_SHAPE+3:
 //    case MENU_QUANTITY_SHAPE+4:
 //    case MENU_QUANTITY_SHAPE+5:
 //    case MENU_QUANTITY_SHAPE+6: 
 //    case MENU_QUANTITY_SHAPE+7:
 //    case MENU_QUANTITY_SHAPE+8:
 //    case MENU_QUANTITY_SHAPE+9:
 //    case MENU_QUANTITY_SHAPE+10:
 //    case MENU_QUANTITY_SHAPE+11:
 //    case MENU_QUANTITY_SHAPE+12:
 //    case MENU_QUANTITY_SHAPE+13: 
 //    case MENU_QUANTITY_SHAPE+14: 
 //    case MENU_QUANTITY_SHAPE+15: 
	// case MENU_QUANTITY_SHAPE+16: {


	//if(XmToggleButtonGadgetGetState(w)) {
	//	HighLightSelected(g->m_SelectedGraph,&(g->highlight),0,g);		
	//	XtSetSensitive(g->quantityButton,0); /* turn it off */

	//	cout<<"TODO: ChangeQuantity(g,(((MenuID *)client_data)->id - MENU_QUANTITY_SHAPE));"<<endl;
	//        g->m_SelectedGraph = 0;
	//}
 //       break;	
 //    }

/* Pressure buttons */
     case MENU_AERO_PANSAIL:
     case MENU_AERO_CUSTOM:
     case MENU_AERO_CUSTOM2:
     case MENU_AERO_RELAXII: 
     case MENU_AERO_NONE: {	
     }
	break;
/* Prestress button */
     case MENU_USE_PRESTRESS: {

	if(XmToggleButtonGadgetGetState(w))  
		{ puts(" triloading_.use_prestress = 1;"); }
	else {
		{ puts("triloading_.use_prestress = 0;"); }
		g_iterations = 1;
	}

	//printf("Set use prestress = %d\n",triloading_.use_prestress );
        break;	
     }
     case MENU_AERO_CONTROLS: {

	 g_control_dialog = create_control_dialog(g->topLevel);	
	XtManageChild(g_control_dialog);
        XtRealizeWidget(g_control_dialog);
        XtMapWidget(g_control_dialog);



	break;
     }

     case MENU_READ_PRESSURES: {

/*	SetPressuresOnActive();
	PostProcessAllSails(0,1); */
	g_World->OutputToClient (" this fn disabled",1);
	break;
     }

     case MENU_DO_STRLIN: {
		int ians2;
		HC_Open_Segment_By_Key(g_boat->PostProcNonExclusive( ));
		HC_Open_Segment("streamlines");
			HC_Flush_Contents(".","everything");
			HC_Set_Line_Weight(2.0);
			ians2=1;
#ifndef NO_PANSAIL
			strlin_(&ians2);
#endif
			HC_Set_Color("lines=blue");
			HC_Flush_Contents(".","modelling matrix"); HC_Rotate_Object( 0.0,0.0, -g_AWA*57.29577951);
		HC_Close_Segment();
		HC_Close_Segment();
		HC_Update_Display();
		break;

    }
     case MENU_ANALYSIS_AUTOCALC: 	
	if(XmToggleButtonGadgetGetState(w)) {
		g_AutoCalc = 1; }
	else{
		g_AutoCalc = 0;}
        break;
	
        case MENU_ANALYSIS_VPPACTIVE: 	
	if(XmToggleButtonGadgetGetState(w)) {
		relaxflagcommon.VPP_Connected = 1;
		//Do_Open_Telnet_Connection(NULL);
		HC_QSet_Color("/","windows=light grey");
/* 		 XtSetSensitive(Passive_VPP_Widget,0); */
		HC_Update_Display();
		}
	else	{
		relaxflagcommon.VPP_Connected  = 0;
		//Do_Close_Telnet_Connection(NULL);
		HC_QSet_Color("/","windows=white");
/* 		XtSetSensitive(Passive_VPP_Widget,1); */
		HC_Update_Display();
		}
        break;
      case MENU_ANALYSIS_VPPPASSIVE: 	
	if(XmToggleButtonGadgetGetState(w)) {
		relaxflagcommon.VPP_Connected = -1;
/*		 XtSetSensitive(Active_VPP_Widget,0); */
		HC_QSet_Color("/","windows=white");
		}
	else
		relaxflagcommon.VPP_Connected  = 0;
        break;	
     
     case MENU_ANALYSIS_TRIM: {	
	Arg al[64]; int l_ac=0;
	if( g_boat->IsOK()) {
	  	if(!g_trim_shell)  
		  set_up_whole_trimmenu ();
		
		assert(g_trim_shell);
		XtPopup(g_trim_shell->trim_shell,XtGrabNone);
		l_ac=0;
		XtSetArg(al[l_ac], XmNiconic, FALSE); l_ac++;
		XtSetValues ( g_trim_shell->trim_shell,al, l_ac );// should pop it up even if iconic
	      }


	break;
     } 

     default:
	printf("Unknown Menu item : %d\n",((MenuID *)client_data)->id);
	HC_Update_Display();
	break;
     }
return NULL;
}




int Update_Hoist_Menu(Graphic *g){

int n;//,err,h;
char buf[512];
Widget w;
static struct LINKLIST *cList;
#pragma message(" the puns are pop_pointer")

// if it isnt hoisted but is in the list it gets a menu button
// if it IS hoisted we skip
// This works OK (and it is a tidy way of getting a sail list) 
// 
// 1) delete the existing children.

	while(cList) {
		if(Pop_Pointer((void**) (&w),&cList)){
			//assert(XtIsWidget(w)); for some reason this always fails
			XtDestroyWidget(w);
		}
	}
	n = 6;
 	std::vector<RXSail*> lsl = g_World->ListModels(RXSAIL_NOTBOAT_NORHOISTED);  
 	
 	for(n=0;n<lsl.size();n++) {
 		SAIL *s = lsl[n];
 		assert(!s->IsBoat());
  		assert(!s->IsHoisted()) ;
 		sprintf(buf,"Hoist : %s ",s->m_fileBAG.Array()); // OK
		w = CreateMenuButtonForLP(g->Hoist_SailMenu,NULL,MENU_TEXT_CB ,buf,'1',PushButton,NULL,NULL,g,buf);
	  	Push_Pointer((void**)( &w), &cList);			
 	}
	
 /*	
	for(n=1;;n++) {
		h = cf_is_hoisted_(&n,&err); //  printf(" is_hoisted gave %d , err=%d\n",h,err);
		if(err) break;
		sd = (SailData*) cf_get_sail_sd_(&n,&err);// returns 0 if n unused
		if( !sd) continue;
		if(sd->isail->IsBoat()) continue;
		if(!h) {
			sprintf(buf,"Hoist : %s ",sd->fileBAG); // OK
			w = CreateMenuButtonForLP(g->Hoist_SailMenu,NULL,MENU_TEXT_CB ,buf,'1',PushButton,NULL,NULL,g,buf);
	  		 Push_Pointer((void**)( &w), &cList);
	
		}	

		} */
	w= CreateMenuButton(g->Hoist_SailMenu,NULL,MENU_ATTACH_SAIL,"Bagged..",'B',PushButton,NULL,NULL,g);
	Push_Pointer((void**)( &w), &cList);
return 1;
}

int UpdateMenu(Graphic *p_g){

	int i;
	char buf[512];
	for(i=0;i<g_nChildren;i++) {
	assert(g_saveChildren[i]);
	//assert(XtIsWidget(g_saveChildren[i])); //for some reason this always fails
	assert(g_loadinterpChildren[i]);
	//assert(XtIsWidget(g_loadinterpChildren[i]));


	XtDestroyWidget(g_saveChildren[i]);
        XtDestroyWidget(g_loadinterpChildren[i]);
	if(g_dropChildren[i]){
		//assert(XtIsWidget(g_dropChildren[i]));
		XtDestroyWidget(g_dropChildren[i]);

	}
	g_sailObject[i] = 0;
	g_saveChildren[i] = NULL;
	g_loadinterpChildren[i] = NULL;
	g_dropChildren[i] = NULL;


}
	g_nChildren = 0;

	std::vector<RXSail*> ll = g_World->ListModels(RXSAIL_ALLHOISTED); // update menus tested OK
	for(i=0;i<ll.size();i++)  {
		SAIL *s = ll[i];
		sprintf(buf,"%s (%s)",s->GetType().c_str(),s->m_attributes.Array());
		assert(DEBUG_MENU); // or else loadstatesubmenu is null
		assert(XtIsWidget(p_g->saveStateSubmenu));

		g_saveChildren[g_nChildren] = CreateMenuButton(p_g->saveStateSubmenu,NULL,MENU_SAVE_STATEAS,buf,'1',PushButton,NULL,NULL,p_g);
		//loadChildren[g_nChildren] = CreateMenuButton(g->loadStateSubmenu,NULL,MENU_LOAD_STATE,buf,'1',PushButton,NULL,NULL,g);

		g_loadinterpChildren[g_nChildren] = CreateMenuButton(p_g->loadInterpStateSubmenu, NULL,MENU_LOAD_STATE_BY_INTERP,buf,'1',PushButton,NULL,NULL,p_g);
		if(s->IsBoat())
			g_dropChildren[g_nChildren] = NULL;
		else
			g_dropChildren[g_nChildren] = CreateMenuButton(p_g->dropSailSubmenu, 0,MENU_DROP_SAIL, buf,'1',PushButton, 0 ,0,p_g);
		g_sailObject[g_nChildren] =s; 
		g_nChildren++;
	}


Update_Hoist_Menu(p_g);
return(0);
}


int SetCameraList(){

	g_nCams = 0;
	if(!g_boat) { g_World->OutputToClient (" in  no boat",1); return 0; }
	vector<RXEntity_p >thelist;
	g_boat->MapExtractList(PCE_CAMERA ,  thelist) ;
	vector<RXEntity_p >::iterator it;
	for (it = thelist.begin (); it != thelist.end(); it++)  {
		g_camPlist[g_nCams++] = *it;
		if(g_nCams > 29) break;
	}


return(g_nCams);
}









