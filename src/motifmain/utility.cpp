
#include "StdAfx.h"

// #include <varargs.h> // monarch
#include <string.h>

#include <X11/Intrinsic.h>
#include <X11/IntrinsicP.h>

#include <Xm/Xm.h>
#include <Xm/DialogS.h>
#include <Xm/MessageB.h>



#include "utility.h"

#include "hc.h"
#define local static

#define BUFSIZE 1024

void clamp_point_to_window (Point *point) {
    if      (point->x < -1.0)  point->x = -1.0;
    else if (point->x >  1.0)  point->x =  1.0;

    if      (point->y < -1.0)  point->y = -1.0;
    else if (point->y >  1.0)  point->y =  1.0;

    if      (point->z < -1.0)  point->z = -1.0;
    else if (point->z >  1.0)  point->z =  1.0;

    return;
}


int  parse_args (
    const char	*token,
    int		argc,
    char	**argv) {
    char	**strings;
    boolean	found_token;
    int cnt=0; 
    found_token = 0;

    /* check the arguments passed to the program for the string "token" */
    strings = argv;

    if (argc > 1) {
      while (cnt < argc) {
	if (!strcmp (strings[cnt], token)) {
	  found_token =  cnt;
	  break;
	}
	cnt++;
      }
    }
    return found_token;
}







Widget Get_Top_Shell (
    Widget 		widget) {

    /* Return the Top Shell of the passed in widget. */

    while (widget && !XtIsWMShell (widget))
	widget = XtParent (widget);

    return widget;
}


void insert_bbox (
    const char	*seg,
    Point	*max,
    Point	*min){
    int		point_count;
    Point	points[8];
    int		face_list_length;
    int		face_list[30];

    point_count = 8;

    points[0].x = min->x;
    points[0].y = min->y;
    points[0].z = min->z;

    points[1].x = min->x;
    points[1].y = min->y;
    points[1].z = max->z;

    points[2].x = min->x;
    points[2].y = max->y;
    points[2].z = max->z;

    points[3].x = min->x;
    points[3].y = max->y;
    points[3].z = min->z;

    points[4].x = max->x;
    points[4].y = max->y;
    points[4].z = min->z;

    points[5].x = max->x;
    points[5].y = min->y;
    points[5].z = min->z;

    points[6].x = max->x;
    points[6].y = min->y;
    points[6].z = max->z;

    points[7].x = max->x;
    points[7].y = max->y;
    points[7].z = max->z;

    face_list_length  = 30;

    face_list[0] = 4;
    face_list[1] = 0;
    face_list[2] = 1;
    face_list[3] = 2;
    face_list[4] = 3;

    face_list[5] = 4;
    face_list[6] = 0;
    face_list[7] = 3;
    face_list[8] = 4;
    face_list[9] = 5;

    face_list[10] = 4;
    face_list[11] = 3;
    face_list[12] = 4;
    face_list[13] = 7;
    face_list[14] = 2;

    face_list[15] = 4;
    face_list[16] = 1;
    face_list[17] = 2;
    face_list[18] = 7;
    face_list[19] = 6;

    face_list[20] = 4;
    face_list[21] = 2;
    face_list[22] = 3;
    face_list[23] = 4;
    face_list[24] = 7;

    face_list[25] = 4;
    face_list[26] = 7;
    face_list[27] = 4;
    face_list[28] = 5;
    face_list[29] = 6;

    HC_Open_Segment (seg);
	HC_Open_Segment ("bounding box");
	    HC_Flush_Segment (".");
	    HC_Set_Line_Pattern ("- -");
	    HC_Set_Line_Weight (0.5);
	    HC_Set_Visibility ("edges = on,  markers = off, faces = off");

	    HC_Insert_Shell (point_count, points, face_list_length, face_list);
	    HC_Set_Selectability("everything = off");
	HC_Close_Segment ();
    HC_Close_Segment ();

    return;
}



/*
 *	This routine came from the Motif Programming Manual, O'Reilly & Assoc.
 * 	Volume 6, page 485.  It accepts a variable argument formatted string
 *	like the printf function and displays the string in the message area
 *	widget.
 */


void compute_mesh (
    float	xstart,
    float	xend,
    int		xnodes,
    float	ystart,
    float	yend,
    int		ynodes,
    float	(*funct)(float x, float y),
    Point	*ipl) {
    Point	*pl = ipl;
    float	x, y;
    float	xinc, yinc;

    xinc = (xend - xstart) / (xnodes - 1);
    yinc = (yend - ystart) / (ynodes - 1);
    pl += xnodes * ynodes;

    for (x = xstart; x < xend + xinc/2.0; x += xinc) {
	for (y = ystart; y < yend + yinc/2.0; y += yinc) {
	    --pl;
	    pl->x = x;
	    pl->y = (*funct) (x, y);
	    pl->z = y;
	}
    }
}


local float sin_cos (
    float	x,
    float	y) {
    float 	xfreq = 2.0;
    float	yfreq = 1.0;

    return ((0.25 + 0.5 * sin(x * 1.5 + 1.0)) * sin (xfreq*x)*cos (yfreq*y));
}


void fit_segment (
    const char *	segment) {
    float	scale;
    float	smallest;
    Point	min, max;
    Point	diagonal;
    Point	position;
    Point	target;
    Point	up;
    float	width;
    float	height;
    char	projection[32];

    /*
     *	Compute the oru coordinate bounding box for the segment and its
     *  subsegments and then use this box to set the camera on ?Picture
     *  to just fit the box.
     */

    HC_Open_Segment (segment);
	HC_Show_Net_Camera (&position, &target, &up,
	    &width, &height, projection);

	/* Find the bouding box of the object */
	if (HC_Compute_Circumcuboid (".", &min, &max)) {

	    HC_Compute_Coordinates (".", "object", &min, "world", &min);
	    HC_Compute_Coordinates (".", "object", &max, "world", &max);
	    
	}
	else {
		min.x = -1.0; min.y = -1.0; min.z = -1.0;
		max.x =  1.0; max.y =  1.0; max.z =  1.0;
	}

	/* the object -> world conversion may have flipped the min and max points. */
	if (min.x > max.x) {
		float tmp;
		tmp = min.x ; min.x = max.x; max.x = tmp;
	}
	if (min.y > max.y) {
		float tmp;
		tmp = min.y ; min.y = max.y; max.y = tmp;
	}


	/* Assert -> min.x < max.x && min.y < max.y */
	/* This assumes that there is only one camera .. May not be a valid assumption. */
	/* Make this more general at some point. */
	HC_QSet_Camera_By_Volume ("?Picture", "p", 1.1 * min.x, 1.1 * max.x, 1.1 * min.y, 1.1 * max.y);


    HC_Close_Segment ();

    return;
}


