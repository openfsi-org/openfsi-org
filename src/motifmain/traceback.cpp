
#include "StdAfx.h"
#ifndef linux
	//#using <mscorlib.dll>
	//#using <System.dll>

	//using namespace System;
	//using namespace System::Diagnostics;
#else
    #include <execinfo.h>
#endif
	#include "traceback.h"
     
     /* Obtain a backtrace and print it to stdout. */

  void    print_trace (void) 
     {
       void *array[10];
       size_t size;
       char **strings;
       size_t i;
#ifdef _WINDOWS
#ifdef NEVER
                // Create a StackTrace that captures
                // filename, line number, and column
                // information for the current thread.
                StackTrace *st = new StackTrace(true);
                String *stackIndent = S"";
                for(int i =0; i< st->FrameCount; i++ )
                {
                    // Note that at this level, there are five
                    // stack frames, one for each method invocation.
                    StackFrame *sf = st->GetFrame(i);
                    Console::WriteLine();
                    Console::WriteLine(S"{0}Method: {1}",
                        stackIndent, sf->GetMethod() );
                    Console::WriteLine(S"{0}File: {1}", 
                        stackIndent, sf->GetFileName());
                    Console::WriteLine(S"{0}Line Number: {1}",
                        stackIndent, sf->GetFileLineNumber().ToString());
                    stackIndent = String::Concat(stackIndent,  S"  ");
                }

    
	   return ;
#endif
#else
       size = backtrace (array, 5);
       strings = backtrace_symbols (array, size);
     
       printf ("Obtained %zd stack frames.\n", size);
     
       for (i = 0; i < size; i++)
          printf ("%s\n", strings[i]);
//       free (strings); fails on wrap_debug
#endif
     }


