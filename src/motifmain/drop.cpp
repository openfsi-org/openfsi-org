/* routine to drop a sail after it has been hoisted for a while */
/*
Feb 2003  pansailEnt
Oct 97  DeleteOneConnectAndSites doesnt freepush. Leave that to Delet_Ent
	insulated the RXFREEs by setting the arg to null after.
 Sept 97  ifdef out some printfs
 27/3/96  change logic of delele connects to cope with
 paretn management now in Delete 
4/3/96 print in unr-solve, to see why the const connect is used 

 22.11.94        A K Molyneaux        created
 *
 * copyright Peter Heppel Associates.
 *
 */



#include "StdAfx.h"
#include "RXSail.h"
 
#ifdef _X
	#include <X11/Xatom.h>
	#include <X11/Intrinsic.h>
	#include <X11/Shell.h>

	#include <Xm/Xm.h>
	 
	#include <Xm/DialogS.h>
	#include <Xm/PushB.h>
	#include <Xm/ToggleB.h>
	#include <Xm/TextF.h>
        #include "menuCB.h"
    #include "trimmenu.h"
#endif

#include "entities.h"
 
#include "resolve.h"


//#include "doview.h"
#include "pansin.h"
 
#include "printall.h"
#include "f90_to_c.h"
#include "connect_f.h"

#include "script.h"
//#include "OpenView.h"

#ifdef _X
#include "find_include.h"
#include "cp2mainstub.h"
#endif

#include "global_declarations.h"

#include "drop.h"
#ifdef _X
int Close_All_Options()
{
  int i;
  int retVal = 0;
  FrameData FD;


  for(i=0;i<g_hoops_count;i++) {
    if((g_graph[i].GType == WT_VIEWWINDOW) && (g_graph[i].options_menu != NULL)) {
      /* add code here to close the options window etc. */
      retVal++;
      memset(&FD,0,sizeof( FrameData)); // Peter insurance Jan 2005
      
      FD.m_g = &(g_graph[i]);
      FD.m_post_processor = g_graph[i].options_menu;

     cp_cancel_CB(NULL,(caddr_t)&FD,NULL);

 
    }
  }
  return(retVal);
}
#endif



