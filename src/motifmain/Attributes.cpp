/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *  this was originally simple. It checked HCvisibility of text, lines, markers, etc
     and set  HC_Vis according to the answer.
 	However we now want to include more options.
	eg "orientation" would turn on .../orientation
	eg "MAt type " would turn on .../mat type 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"

#include "Global.h"
#include "Menu.h"
#include "Xm/Protocols.h"
#include "Xm/PushB.h"
#include "Xm/ToggleB.h"
#include "Xm/DialogS.h"
#include "radiomenus.h"

#include "hcupdate.h"		/* replaces HC_Update_Display */
#include "MakeNewDrawingArea.h"

#include "global_declarations.h"

#include "Attributes.h"
#define ATTDBG (0)
static Widget visButton[NROWDIM];
// static  extcheck didnt like this
static int NROWS=10;
static Widget dgForm,dgShell;

int Special_Show_One_Net_Visibility(const char *name ,char *s){

char buf[256];

/*  name may be a seg in which case it returns the visibility of the
FIRST seg that matches the name  */

sprintf(buf,".../%s",name);
HC_Begin_Segment_Search(buf);
if(HC_Find_Segment(buf)) {
	HC_QShow_Net_Visibility(buf,s); 
	if(ATTDBG)  printf(" Visibility on seg %s is %s\n", buf,s); 
	HC_End_Segment_Search();
	return 2;
}
HC_End_Segment_Search();

/* we hope than the name is a permissable Hoops type  */ 
	HC_Show_One_Net_Visibility(name ,s);
	if(ATTDBG)  printf(" Visibility of %s is %s\n", name ,s); 
	return 1;

} 


int DoAttributeDlg(Widget parent,Graphic *g) 
{


Widget OKbutton,Cbutton;
int top,i;
Atom	wm_delete_window;
Position x,y;
char s[200];
if(g->m_SelectedGraph == 0)     
//	g->selected = g->baseSeg; // May 2003 a guess
	g->m_SelectedGraph = g->m_ViewSeg; // Dec 2005 

Arg al[64]; 
int l_ac=0;

l_ac=0;
XtSetArg(al[l_ac], XtNx, &x);l_ac++;
XtSetArg(al[l_ac], XtNy, &y);l_ac++;
XtGetValues(parent,al,l_ac);
if(ATTDBG) printf("x,y found as %d %d\n",x,y);
/* create a form dialog then add the appropriate buttons etc.
 */
l_ac=0;
/*
 * XtSetArg (al[l_ac], XmNdialogStyle, XmDIALOG_FULL_APPLICATION_MODAL);  l_ac++;  
 */

XtSetArg(al[l_ac], XtNallowShellResize, FALSE); l_ac++;
XtSetArg(al[l_ac], XmNresizable, FALSE); l_ac++;
XtSetArg(al[l_ac], XmNnoResize, TRUE); l_ac++;
XtSetArg(al[l_ac], XtNwidth, 200);l_ac++;
XtSetArg(al[l_ac], XtNheight, 58*NROWS);l_ac++;
XtSetArg(al[l_ac], XmNx, 10);l_ac++;
XtSetArg(al[l_ac], XmNy, 10);l_ac++;

dgShell = XmCreateDialogShell(parent,(char*)"Attributes",al,l_ac);

dgForm = XmCreateForm(dgShell,(char*)"dialog",al,l_ac);
    /*
     * intern the atoms and add mwm_messages to wm_protocols so that
     * mwm will look for mwm_messages property changes. Also add a
     * callback for handling WM_DELETE_WINDOW for when the user
     * selects the close button.
     */

    
    wm_delete_window = XmInternAtom(XtDisplay(dgShell),
				(char*)"WM_DELETE_WINDOW",
				FALSE);
    
    XmAddWMProtocolCallback(dgShell, wm_delete_window, AttributeQuitCB, (XtPointer) g);

HC_Open_Segment_By_Key(g->m_SelectedGraph);

top = 40;
if(ATTDBG) {
 	char curseg[256];
	HC_Show_Segment(g->m_SelectedGraph,curseg);
 	printf ("Open Seg is %s\n", curseg);
} 

for(i=0;i<NROWS;i++) {
	l_ac=0;
	
	XtSetArg(al[l_ac],XmNfillOnSelect, True);l_ac++;
	XtSetArg(al[l_ac],XmNindicatorOn, True);l_ac++;
	XtSetArg(al[l_ac],XmNindicatorType, XmN_OF_MANY);l_ac++;
	XtSetArg(al[l_ac],XmNvisibleWhenOff, True);l_ac++;
	XtSetArg(al[l_ac],XmNtopAttachment,XmATTACH_FORM);l_ac++;
	XtSetArg(al[l_ac],XmNleftAttachment,XmATTACH_FORM);l_ac++;
	XtSetArg(al[l_ac],XmNleftOffset,30);l_ac++;
	XtSetArg(al[l_ac],XmNtopOffset,top);l_ac++;
	
	Special_Show_One_Net_Visibility(g_pbname[i],s);
if(ATTDBG)	printf("visibility of %s = %s\n",g_pbname[i],s);
	if (strstr(s,"on") && !strstr(s,"only")) {
		XtSetArg(al[l_ac],XmNset,True);
		l_ac++;
	}

	visButton[i] = XmCreateToggleButton(dgForm,(char*)g_pbname[i],al,l_ac);
	XtAddCallback(visButton[i],XmNvalueChangedCallback,(XtCallbackProc)attrib_activateCB,mkID(i,g));
	XtManageChild(visButton[i]);
top += 30;
}


	l_ac=0;
	
	XtSetArg(al[l_ac],XmNfillOnSelect, True);l_ac++;
	XtSetArg(al[l_ac],XmNindicatorOn, True);l_ac++;
	XtSetArg(al[l_ac],XmNindicatorType, XmN_OF_MANY);l_ac++;
	XtSetArg(al[l_ac],XmNvisibleWhenOff, True);l_ac++;
	XtSetArg(al[l_ac],XmNtopAttachment,XmATTACH_FORM);l_ac++;
	XtSetArg(al[l_ac],XmNleftAttachment,XmATTACH_FORM);l_ac++;
	XtSetArg(al[l_ac],XmNleftOffset,30);l_ac++;
	XtSetArg(al[l_ac],XmNtopOffset,top);l_ac++;
	
	HC_Show_One_Net_Rendering_Optio("color interpolation",s);
	if (strstr(s,"on")) {
		XtSetArg(al[l_ac],XmNset,True);
		l_ac++;
	}

	visButton[i] = XmCreateToggleButton(dgForm,(char*)"Color Interpolation",al,l_ac);
	XtAddCallback(visButton[i],XmNvalueChangedCallback,(XtCallbackProc)attrib_activateCB,mkID(i,g));
	XtManageChild(visButton[i]);
top += 30;i++;
	l_ac=0;
	
	XtSetArg(al[l_ac],XmNfillOnSelect, True);l_ac++;
	XtSetArg(al[l_ac],XmNindicatorOn, True);l_ac++;
	XtSetArg(al[l_ac],XmNindicatorType, XmN_OF_MANY);l_ac++;
	XtSetArg(al[l_ac],XmNvisibleWhenOff, True);l_ac++;
	XtSetArg(al[l_ac],XmNtopAttachment,XmATTACH_FORM);l_ac++;
	XtSetArg(al[l_ac],XmNleftAttachment,XmATTACH_FORM);l_ac++;
	XtSetArg(al[l_ac],XmNleftOffset,30);l_ac++;
	XtSetArg(al[l_ac],XmNtopOffset,top);l_ac++;
	
	HC_Show_One_Net_User_Option("panel_edges",s);
	if (strstr(s,"on")) {
		XtSetArg(al[l_ac],XmNset,True);
		l_ac++;
	}


	visButton[i] = XmCreateToggleButton(dgForm,(char*)"Panel Edges",al,l_ac);
	XtAddCallback(visButton[i],XmNvalueChangedCallback,(XtCallbackProc)attrib_activateCB,mkID(i,g));
	XtManageChild(visButton[i]);

HC_Close_Segment();

top += 30;

	l_ac=0;
	XtSetArg(al[l_ac],XmNtopAttachment,XmATTACH_FORM);l_ac++;
	XtSetArg(al[l_ac],XmNleftAttachment,XmATTACH_FORM);l_ac++;
	XtSetArg(al[l_ac],XmNleftOffset,12);l_ac++;
	XtSetArg(al[l_ac],XmNtopOffset,top);l_ac++;
	XtSetArg(al[l_ac],XmNmarginRight,10);l_ac++;
	XtSetArg(al[l_ac],XmNmarginLeft,10);l_ac++;

	
	OKbutton = XmCreatePushButton(dgForm,(char*)"OK",al,l_ac);
	XtAddCallback(OKbutton,XmNactivateCallback,(XtCallbackProc)attrib_activateCB,mkID(OKPRESS,g));
	XtManageChild(OKbutton);
	
	l_ac=0;
	XtSetArg(al[l_ac],XmNtopAttachment,XmATTACH_FORM);l_ac++;
	XtSetArg(al[l_ac],XmNleftAttachment,XmATTACH_FORM);l_ac++;
	XtSetArg(al[l_ac],XmNleftOffset,65);l_ac++;
	XtSetArg(al[l_ac],XmNtopOffset,top);l_ac++;
	XtSetArg(al[l_ac],XmNmarginRight,5);l_ac++;
	XtSetArg(al[l_ac],XmNmarginLeft,5);l_ac++;
	
	Cbutton = XmCreatePushButton(dgForm,(char*)"Cancel",al,l_ac);
	XtAddCallback(Cbutton,XmNactivateCallback,(XtCallbackProc)attrib_activateCB,mkID(CANCELPRESS,g));
	XtManageChild(Cbutton);



XtManageChild(dgForm);

return(1);
}

void attrib_activateCB(Widget w,caddr_t client_data,caddr_t call_data) 
{
  XmToggleButtonCallbackStruct *tcs;
  Graphic *g;


	
  switch(((MenuID *)client_data)->id) {
     	case OKPRESS: {
  		g = (Graphic *)((MenuID *)client_data)->g;
		AttributeQuitCB(w,(XtPointer) g,(XtPointer) NULL);
/*		
		XtUnmanageChild(dgForm);
		XtDestroyWidget(dgForm);
		XtUnmanageChild(dgShell);
		XtDestroyWidget(dgShell);
 */
		break;
	}
     	case CANCELPRESS: {
		
		XtUnmanageChild(dgForm);
		XtDestroyWidget(dgForm);
		XtUnmanageChild(dgShell);
		XtDestroyWidget(dgShell);
		break;
	}
	default: {		/* first button */
 	 	tcs = (XmToggleButtonCallbackStruct *) call_data;

	break;
	}
  }
return;
}


static void AttributeQuitCB(Widget w, XtPointer client_data, XtPointer call_data)
{
	Graphic *g;
	int i;
	int state[NROWDIM];
	char sz[512];
	g = (Graphic *) client_data;


	if(g->m_SelectedGraph == 0)
	   return; /* select all of the window */
	HC_Open_Segment_By_Key(g->m_SelectedGraph);
	for(i=0;i<NROWS;i++) {
		state[i] = XmToggleButtonGetState(visButton[i]);
if(ATTDBG)		printf("Toggle button no. %d = %d\n",i,state[i]);
		strcpy(sz,g_pbname[i]);
		if(i > 4) {
			char buf[256];
			sprintf(buf,".../%s",g_pbname[i]);
			if(state[i]) {
				HC_QUnSet_Visibility(buf);
if(ATTDBG)			printf(" UNsetting  vis in %s\n",buf);
			}
			else {
				HC_QSet_Visibility(buf,"off");
if(ATTDBG)			printf(" setting OFF segs %s\n",buf);
			}
if(ATTDBG)		HC_Show_Pathname_Expansion(".",buf);
if(ATTDBG)		printf(" in seg  %s\n",buf);			
		}
		else
		{
			strcpy(sz,g_pbname[i]);
			if(state[i])	{
				if(i==1) 
					strcat(sz,"=(on,mesh quads = off)");
				else
					strcat(sz,"=on");
			}
			else  {	
			/*	if(i==1) 
					strcat(sz,"=perimeters only");
				else   PH personal choice 17/5/96 */
					strcat(sz,"=off");
			}
			if(ATTDBG)printf(" %d Vis Set %s\n",i, sz);
		
			HC_Set_Visibility(sz);
		}
	}
		state[i] = XmToggleButtonGetState(visButton[i]);
if(ATTDBG)		printf("Toggle button no. %d = %d\n",i,state[i]);
		strcpy(sz,"color interpolation");
		if(state[i])	
			strcat(sz,"=on");
		else	
			strcat(sz,"=off");
		HC_Set_Rendering_Options(sz);
i++;
		state[i] = XmToggleButtonGetState(visButton[i]);
if(ATTDBG)		printf("Toggle button no. %d = %d\n",i,state[i]);
		strcpy(sz,"panel_edges");
		HC_Open_Segment("panel_edges_include");
		  if(state[i]) {	
			strcat(sz,"=on");
			HC_Set_Visibility("everything=on");
		  }
		  else {	
			strcat(sz,"=off");
			HC_Set_Visibility("everything=off");
		  }
		HC_Close_Segment();
		HC_Set_User_Options(sz);
			
	HC_Close_Segment();
	HighLightSelected(g->m_SelectedGraph,&(g->highlight),0,g); /*unhighlight selection */		
	g->m_SelectedGraph = 0;

}










