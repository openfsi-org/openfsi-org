/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux 
 * 		P Heppel
 *
 * 
 *
 * Modified :
8.97 args to memset changed
  March 1997. Current g_graph is set back to the previous value after opening mat window
  This is to stop build errors coming up in minimised windows
 
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


/* Open Mat Window(char*name) opens a window ready for a material display */

#include "StdAfx.h"

#include "sailutil.h"

#include "Xm/Protocols.h"
#include "CreateMsgArea.h"

#include "elements.h"
#include "entities.h"
#include "OpenBuildFile.h" 

#include "rversion.h"
#include "hcupdate.h"		/* replaces HC_Update_Display */
#include "ReadBagged.h" 
#include "MakeNewDrawingArea.h"
#include "global_declarations.h"
#include "matmenu.h"

#include "OpenMat.h"

#define FAILED (NULL)
     
void create_top_material (Graphic *g,Widget parent,const char *name)
{
	Atom	wm_delete_window;
	Arg al[64]; 
	int l_ac=0;
	char buf[200];
   int min=1;
	l_ac=0;
        XtSetArg(al[l_ac], XmNallowShellResize, TRUE); l_ac++;
	sprintf(buf,"%s ( %s )", name,RELAX_VERSION);
	XtSetArg(al[l_ac],XmNtitle,buf);l_ac++;
	XtSetArg(al[l_ac],XmNiconName,name);l_ac++;
	XtSetArg(al[l_ac],XmNwidth,300);l_ac++;
	XtSetArg(al[l_ac],XmNheight,350);l_ac++;
	XtSetArg(al[l_ac],XmNallowShellResize,True);l_ac++; /* maybe WRONG */
	if(min) {
		XtSetArg(al[l_ac], XmNiconic, TRUE); l_ac++;
	}
	XtSetArg(al[l_ac],XmNwinGravity,SouthWestGravity);l_ac++;
       /* XtSetArg(al[l_ac],XmNdeleteResponse,XmDO_NOTHING); l_ac++;  */
	if(parent == NULL) {
	  g_World->OutputToClient ("parent = NULL",2);
	}
        g->topLevel = XtCreatePopupShell ( "material", topLevelShellWidgetClass, parent, al, l_ac );
        l_ac = 0;
	g->mainWindow = XmCreateMainWindow(g->topLevel,(char*) "mainWindow",al,l_ac);
	XtManageChild(g->mainWindow);       
	g->menuBar = CreateMatMenuBar(g->mainWindow,g);
	XtManageChild(g->menuBar);
       
	CreateMsgArea(g);
	l_ac = 0;
	g->workArea = MakeNewDrawingArea(g->mainWindow,"matbase",g,(HBaseModel*)0); // the widget makes the model
	XmMainWindowSetAreas(g->mainWindow,g->menuBar,NULL,NULL,NULL,g->workArea);
	l_ac=0;
	XtSetArg(al[l_ac],XmNmessageWindow,(XtArgVal) g->msgform); 
	l_ac++;
	XtSetValues(g->mainWindow,al,l_ac);
       
	wm_delete_window = XmInternAtom(XtDisplay(g->topLevel),
					(char*) "WM_DELETE_WINDOW",
					FALSE);
       
	XmAddWMProtocolCallback(g->topLevel, wm_delete_window, MatDeleteCB, (XtPointer) g);

      }


     

Graphic *OpenMatWindow(const char*name) {

  Graphic *gfree;
  
   gfree = nextGraphic(WT_MATWINDOW);
  
  create_top_material(gfree,g_graph[0].topLevel,name);
  XtPopup(gfree->topLevel,XtGrabNone);
  
  XFlush(XtDisplay(gfree->topLevel));
  gfree->cursor = 0;
  gfree->GSail=NULL;

  SetMsgText(gfree,name);
  return(gfree);
}//Graphic *OpenMatWindow

void MatDeleteCB(Widget w,XtPointer client_data, XtPointer call_data)
{
  Graphic *g;
  char buf[512];
 
 // return; commenting out Oct 2002 could be dangerous
  	g = (Graphic *) client_data;
	if(!g) {
		puts(" Mat Delete CB G NULL");
		return;
	}
  	if(g->EMPTY){
   		printf("Mat Delete CB g EMPTY\n");
   		 return;  /* already been here */
 	}

  	HC_Show_Segment(g->m_ViewSeg,buf);
 	HC_Flush_Contents(buf,"includes");
 	memset(g,0,sizeof(Graphic)); // removed Peter april 2001 and replaced Oct 2002
  	g->filename[0] = '\0';
    	g->EMPTY = 1;

}//void MatDeleteCB



