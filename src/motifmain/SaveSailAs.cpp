/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


/* SaveSailAs routine to write mesh out etc. */

#include "StdAfx.h"

#include "Global.h"
#include "Menu.h"
#include "Dialog.h"
#ifdef _X
#include "Xm/FileSB.h"
#include "global_declarations.h"
#include "AskToOverwriteDlg.h"

#include "savesailas.h"

static Widget saveas_dialog;
static XmStringCharSet charset = (XmStringCharSet) XmSTRING_DEFAULT_CHARSET;
				/* used to set up XmStrings */
static XmString saveString;
static XmString saveTitleString;
static XmString patternString;
static XmString dirString;


int SaveSailAs(Widget parent, Graphic *g,int type)
{
int l_ac =0;
Arg al[32];
//Widget flist,flistlabel,ffilterlabel,ffilter;
l_ac=0;
//printf("SaveSailAs\n");
if(!g) {
	g_World->OutputToClient ("SaveSailAs - NULL graphic structure",2);
	return(0);
}
if(!parent) {
	g_World->OutputToClient ("SaveSailAs - NULL parent",2);
	return(0);
}

if(saveString != NULL)
	XmStringFree(saveString);

if(saveTitleString != NULL)
	XmStringFree(saveTitleString);

saveString = XmStringCreateSimple((char*)"Save");
if(type == WT_DESIGN)
  saveTitleString = XmStringCreateSimple((char*)"Save Sail As");
else if(type == WT_BOATFILE)
  saveTitleString = XmStringCreateSimple((char*)"Save Boat As");
else
  {
    g_World->OutputToClient ("SaveSailAs called with invalid WT type",2);
    saveTitleString = NULL;
    return(0);
  }
if(patternString != NULL)
	XmStringFree(patternString);
if(dirString != NULL)
	XmStringFree(dirString);

if(type == WT_BOATFILE) {
  patternString = XmStringCreateSimple((char*)"*.in");
  dirString = XmStringCreateSimple(g_boatDir);
}
else {
  patternString = XmStringCreateSimple((char*)"*.bag");
  dirString = XmStringCreateSimple(g_currentDir);
}

l_ac=0;
XtSetArg (al[l_ac], XmNfileTypeMask,XmFILE_REGULAR);  l_ac++;  	//added Feb 2003
XtSetArg (al[l_ac], XmNx,5);  l_ac++;   				 // guess feb 01
XtSetArg (al[l_ac], XmNy, 5);  l_ac++;   				 // guess feb 01
XtSetArg (al[l_ac], XmNdialogStyle, XmDIALOG_APPLICATION_MODAL);  l_ac++;  
XtSetArg( al[l_ac], XmNokLabelString, saveString ); l_ac++;
XtSetArg( al[l_ac], XmNdialogTitle, saveTitleString ); l_ac++;
XtSetArg( al[l_ac], XmNpattern, patternString ); l_ac++;
XtSetArg( al[l_ac], XmNdirectory, dirString ); l_ac++;
saveas_dialog = XmCreateFileSelectionDialog(parent,(char*)"Sail Save", al, l_ac);

  XtAddCallback(saveas_dialog, XmNokCallback,(XtCallbackProc)SaveSailAsOKCB, mkID(type,g));
  XtAddCallback(saveas_dialog, XmNcancelCallback,(XtCallbackProc)SaveSailAsCancelCB, (XtPointer) WT_BUILDFILE);

 // flist = XmFileSelectionBoxGetChild(saveas_dialog,XmDIALOG_LIST);
 // flistlabel = XmFileSelectionBoxGetChild(saveas_dialog,XmDIALOG_LIST_LABEL);
 // ffilter = XmFileSelectionBoxGetChild(saveas_dialog,XmDIALOG_FILTER_TEXT);
 // ffilterlabel = XmFileSelectionBoxGetChild(saveas_dialog,XmDIALOG_FILTER_LABEL);
//  XtUnmanageChild(XtParent(flist)); 
//  XtUnmanageChild(flistlabel);
//  XtUnmanageChild(ffilter);
//  XtUnmanageChild(ffilterlabel);
    XtManageChild(saveas_dialog);
/* update window name !! */

return (1); // Peter thanks to gcc
}


void SaveSailAsOKCB(Widget w, XtPointer client_data, XtPointer call_data) 
{

  static char *filename=NULL;
  static char theFile[256];
  XmFileSelectionBoxCallbackStruct *fcb;
  Graphic *g;
  char *lp;

   /* open the file and read it into the text widget */
         if (filename != NULL) {
	   XtFree (filename);
	   filename = NULL;
         }
         fcb = (XmFileSelectionBoxCallbackStruct *) call_data;
	g = (Graphic*)((MenuID *)client_data)->g;
	
  /* get the filename from the file selection box */
         XmStringGetLtoR(fcb->value, charset, &filename);
         printf("filename =: %s\n",filename);
         lp = strchr(filename,'.');
         if(lp)
	   *lp = '\0';
         sprintf(theFile,"%s",filename);
	if(!AskToOverwriteDlg(g,theFile,saveas_dialog,((MenuID *)client_data)->id)) {
	}
	else {
	  /* already written the file in AskTo.... */
		fprintf(stdout,"Written files %s\n",filename);
	}


}


void SaveSailAsCancelCB(Widget	w, XtPointer client_data, XtPointer call_data) 
{
      /* popdown the file selection box */
        XtUnmanageChild(saveas_dialog);
        XtDestroyWidget(saveas_dialog);
}
#endif


