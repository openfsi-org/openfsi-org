#ifndef RXSTIFFSOLVER_H
#define RXSTIFFSOLVER_H
#include "RXSparseMatrix1.h"

#include "rxodestepper.h"

/*
 * a method for solving the structure as a set of 'stiff' ODEs.
 *
 *This variant takes given mass and damping matrices

 *Thus for the jacobian we compute dfdy from (Mass^-1) . (a fresh stiffness matrix)
 *
 *For the derivative  y''  aka  (Mass^-1).Residual we take a constant mass matrix.

*/



class RXStiffSolver : public RXODEStepper
{
public:
    RXStiffSolver();
    ~RXStiffSolver();
    int test() ; //testrkdumb;
    int runtopeak() ;
     std::string Describe()const;
protected:
    int odeint(Vec_IO_DP &ystart, const DP eps,
               const DP h1, const DP hmin, int &nok, int &nbad );
    void derivs(const DP x, Vec_I_DP &y, Vec_O_DP &dydx);
    class RXSparseMatrix1  m_IMinusBh;
private:  // for stif
    // void jacobian(const DP x, Vec_I_DP &y, Vec_O_DP &dfdx, Mat_O_DP &dfdy, RXSparseMatrix1 &BS);
     void jacobian(const DP x, Vec_I_DP &y, Vec_O_DP &dfdx, RXSparseMatrix1 &BS);
     void stifbs(Vec_IO_DP &y, Vec_IO_DP &dydx, DP &xx, const DP htry,
                const DP eps, Vec_I_DP &yscal, DP &hdid, DP &hnext);//,
             //   void derivs(const DP, Vec_I_DP &, Vec_O_DP &));
     void simpr(Vec_I_DP &y, Vec_I_DP &dydx, Vec_I_DP &dfdx, class RXSparseMatrix1 &BS,
                      const DP xs, const DP htot, const int nstep, Vec_O_DP &yout);
                    //  void derivs(const DP, Vec_I_DP &, Vec_O_DP &));

    void pzextr(const int iest, const DP xest, Vec_I_DP &yest, Vec_O_DP &yz,
        Vec_O_DP &dy);

};
#endif // RXSTIFFSOLVER_H
