#include "StdAfx.h"
#include <QDebug>
#include <iomanip>
#include "cfromf.h"
#include "offset.h"
#include "rxstiffsolver.h"

RXStiffSolver::RXStiffSolver()
{
}
RXStiffSolver::~RXStiffSolver()
{
}
std::string RXStiffSolver::Describe()const
    {
        std::string rv;
        rv=RXODEStepper::Describe();
        rv += " Method= RXStiffSolver";
        return rv;
    }
int RXStiffSolver::test() // based on xstifbs
{
    int nbad,nok;
    DP eps,hstart,x1=0.0,x2=50.0;
    Vec_DP y(2);

    cout << fixed << setprecision(6);
    for (;;) {
        cout << endl << "Enter eps,hstart (or eps=0 to end)" << endl;
        // cin >> eps >> hstart;
        eps = 1.e-9; hstart=0.01;
        if (eps == 0.0) break;
        kmax=0;
        y[0]= 1.0;
        y[1]=2.0;
        odeint (y,eps,hstart,0.0,nok,nbad);//,Nrrk::derivs_s);
        cout << fixed << setprecision(6);
        cout << endl << "successful steps:" << setw(14) << " ";
        cout << setw(4) << nok << endl;
        cout << "bad steps:" << setw(21) << " " << setw(4) << nbad << endl;
        cout << "y(end) = " << setw(12) << y[0] << setw(12) << y[1];
        cout << setw(12) << y[2] << endl;
        break;
    }
    cout << "Normal completion" << endl;
    return 0;
}
int RXStiffSolver::runtopeak() // based on 'main from xodeint
{
    int N,i,nbad,nok,iexit;
    DP eps=m_eps,h1=this->m_h1,hmin=0.0005; //eps 1e-5 was the original
    N = cf_JaySize()-1;
    Vec_DP *ystart = new Vec_DP(2*N);
    for(i=0;i<2*N;i++) (*ystart)[i]=0.;

    cf_PackCoordinates(ystart->v); // X first then V
    cf_zeroVelocity();
    nrhs=0;

    iexit=odeint(*ystart,eps,h1,hmin,nok,nbad);
    cf_ApplyDisplacements(ystart->v, 0);
    if(1){
        cout  << "steps: ok= "<< setw(4) << nok;
        cout << ",  bad= " << setw(4) << nbad ;
        cout << ",  function calls= "<< setw(4) << nrhs<<endl;
        cout<< this->Describe()<<endl;
    }
    delete ystart;
    return iexit;
}

void RXStiffSolver::derivs(const DP x, Vec_I_DP &y, Vec_O_DP &dydx)
{
    nrhs++;

    int n = y.size();
    cf_DerivativeList(x,y.v  ,dydx.v,m_damp,n);
    return;
}

int RXStiffSolver::odeint(Vec_IO_DP &ystart,  const DP eps,
                          const DP h1, const DP hmin, int &nok, int &nbad)
{
    int iexit=0;
    int i,k,nstp;
    double ekt, xsav,x,hnext,hdid,h;

    int nvar=ystart.size();
    Vec_DP yscal(nvar),y(nvar),dydx(nvar);
    Vec_DP &xp=*xp_p;
    Mat_DP &yp=*yp_p;

    x=0.0;
    h=h1;
    nok = nbad = kount = 0;
    time_t now, olddrawtime; time(&olddrawtime);

    for (i=0;i<nvar;i++) y[i]=ystart[i];
    LogEnergy (0, 0);
    LogCoords(0.0,y.v,nvar);
    if (kmax > 0) xsav=x-dxsav*2.0;
    for (nstp=0;;nstp++) { //nstp<MAXSTP
        this->m_kit++;
        //    enable the UI
        k=2;  k=check_messages(&k);
        if (relaxflagcommon.Stop_Running != 0)
        {
            cf_ApplyDisplacements(y.v, 0);
            iexit=2 ;
            break;
        }
        // do we post-process now?
        if(relaxflagcommon.g_UpdateTime>=0  ) {
            time(& now);
            double drawtime = difftime(now,olddrawtime);
            if(drawtime >= relaxflagcommon.g_UpdateTime)  {
                cf_ApplyDisplacements(y.v, 0);
                k = cf_postprocess();
                olddrawtime = now;
            }
        }
        derivs(x,y,dydx);

        // scaling
        for (i=0;i<nvar/2;i++) // displacement limits
            yscal[i]= max( fabs(dydx[i]*h),max(relaxflagcommon.factor1 ,0.00001));

        for (i=nvar/2;i<nvar;i++) // velocity limits
            yscal[i]= max(y[i], max(relaxflagcommon.factor2 ,0.00001));

        if (kmax > 0 && kount < kmax-1 && fabs(x-xsav) > fabs(dxsav)) {
            for (i=0;i<nvar;i++) yp[i][kount]=y[i];
            xp[kount++]=x;
            xsav=x;
        }
        stifbs(y,dydx,x,h,eps,yscal,hdid,hnext);

        if (hdid == h) ++nok; else ++nbad;
        ekt = cf_MeasureKE(y.v,nvar);

        qDebug()<<"{odeintStep,x,hdid,ekt,nbad,nok,"<< x<<","<< hdid<<","<< ekt<<","<<nbad<<","<<nok<<"},";
        LogCoords(x,y.v,nvar);
        if( LogEnergy (x,ekt)) // do one more step (x+backtrack)
        {
            SetCoordsToPeak(y.v,nvar);

            for (i=0;i<nvar;i++) ystart[i]=y[i];
            return iexit;
        }

        if (fabs(hnext) <= hmin)
        { hnext=SIGN(hmin,hnext); qDebug()<<"Step size too small in odeint_stif"<<hnext;}
        h=hnext;
    }
    if(iexit !=2)
        cout<<"Too many steps in routine odeint"<<endl;
    return iexit;
}
void RXStiffSolver::stifbs(Vec_IO_DP &y, Vec_IO_DP &dydx, DP &xx, const DP htry,
                           const DP eps, Vec_I_DP &yscal, DP &hdid, DP &hnext)
{
    const int KMAXX=7,IMAXX=KMAXX+1;
    const DP SAFE1=0.25,SAFE2=0.7,REDMAX=1.0e-5,REDMIN=0.7;
    const DP TINY=1.0e-30,SCALMX=0.1;
    bool exitflag=false;
    int i,iq,k,kk,km,reduct;
    static int first=1,kmax,kopt,nvold = -1;
    DP eps1,errmax,fact,h,red,scale,work,wrkmin,xest;
    static DP epsold = -1.0,xnew;
    static Vec_DP a(IMAXX);
    static Mat_DP alf(KMAXX,KMAXX);
    static int nseq_d[IMAXX]={2,6,10,14,22,34,50,70};
    Vec_INT nseq(nseq_d,IMAXX);

    int nv=y.size();
    d_p=new Mat_DP(nv,KMAXX);
    x_p=new Vec_DP(KMAXX);
    Vec_DP dfdx(nv),err(KMAXX),yerr(nv),ysav(nv),yseq(nv);
    //   Mat_DP dfdy(nv,nv);
    class RXSparseMatrix1 BS(CSC); //   Bee . Stf
    if (eps != epsold || nv != nvold) {
        hnext = xnew = -1.0e29;
        eps1=SAFE1*eps;
        a[0]=nseq[0]+1;
        for (k=0;k<KMAXX;k++) a[k+1]=a[k]+nseq[k+1];
        for (iq=1;iq<KMAXX;iq++) {
            for (k=0;k<iq;k++)
                alf[k][iq]=pow(eps1,(a[k+1]-a[iq+1])/
                               ((a[iq+1]-a[0]+1.0)*(2*k+3)));
        }
        epsold=eps;
        nvold=nv;
        a[0] += nv;
        for (k=0;k<KMAXX;k++) a[k+1]=a[k]+nseq[k+1];
        for (kopt=1;kopt<KMAXX-1;kopt++)
            if (a[kopt+1] > a[kopt]*alf[kopt-1][kopt]) break;
        kmax=kopt;
    }
    h=htry;
    for (i=0;i<nv;i++) ysav[i]=y[i];
    jacobian(xx,y,dfdx, BS);

    if (xx != xnew || h != hnext) {
        first=1;
        kopt=kmax;
    }

    reduct=0;
    for (;;) {
        for (k=0;k<=kmax;k++) {
            xnew=xx+h;
            if (xnew == xx) cout<<"step size underflow in stifbs";
            simpr(ysav,dydx,dfdx, BS,xx,h,nseq[k],yseq);
            xest=SQR(h/nseq[k]);
            pzextr(k,xest,yseq,y,yerr);
            if (k != 0) {
                errmax=TINY;
                for (i=0;i<nv;i++) errmax=MAX(errmax,fabs(yerr[i]/yscal[i]));
                errmax /= eps;
                km=k-1;
                err[km]=pow(errmax/SAFE1,1.0/(2*km+3));
            }
            if (k != 0 && (k >= kopt-1 || first)) {
                if (errmax < 1.0) {
                    exitflag=true;
                    break;
                }
                if (k == kmax || k == kopt+1) {
                    red=SAFE2/err[km];
                    break;
                }
                else if (k == kopt && alf[kopt-1][kopt] < err[km]) {
                    red=1.0/err[km];
                    break;
                }
                else if (kopt == kmax && alf[km][kmax-1] < err[km]) {
                    red=alf[km][kmax-1]*SAFE2/err[km];
                    break;
                }
                else if (alf[km][kopt] < err[km]) {
                    red=alf[km][kopt-1]/err[km];
                    break;
                }
            }
        }
        if (exitflag) break;
        red=MIN(red,REDMIN);
        red=MAX(red,REDMAX);
        h *= red;
        reduct=1;
    }
    xx=xnew;
    hdid=h;
    first=0;
    wrkmin=1.0e35;
    for (kk=0;kk<=km;kk++) {
        fact=MAX(err[kk],SCALMX);
        work=fact*a[kk+1];
        if (work < wrkmin) {
            scale=fact;
            wrkmin=work;
            kopt=kk+1;
        }
    }
    hnext=h/scale;
    if (kopt >= k && kopt != kmax && !reduct) {
        fact=MAX(scale/alf[kopt-1][kopt],SCALMX);
        if (a[kopt+1]*fact <= wrkmin) {
            hnext=h/fact;
            kopt++;
        }
    }
    delete d_p;
    delete x_p;
}

void RXStiffSolver::simpr(Vec_I_DP &y, Vec_I_DP &dydx, Vec_I_DP &dfdx,class RXSparseMatrix1 &BS,
                          const DP xs, const DP htot, const int nstep, Vec_O_DP &yout)
{
    /*  Here we  we solve (1-h dfdy)  4 times with different RHSs
     *  The matrix isnt symmetric but it has the same structure each call
     *  (although note that some entries may be zero because of local buckling, etc)
     *  The matrix is likely to be ill-conditioned.  We'll probably need FGMRES plus (ILU0 or ILUT)
     *See /opt/intel/composer_xe_2013.4.183/mkl/examples/solverf/source/dcsrilu0_exampl2.f
     *https://software.intel.com/sites/products/documentation/hpc/mkl/mklman/index.htm#GUID-56F0C6ED-0A49-48C4-AF7B-33C3851A39CA.htm
     *
     *
     **/
    int i,j,nn;
    DP h,x;

    int n=y.size();


    Vec_DP del(n),ytemp(n);
    h=htot/nstep;

    int ok, err;
    //////////////SPARSE//////////////////////////////////////
    class RXSparseMatrix1 ii(RXM_SYMMETRIC ),  IminushBS(RXM_RECTANGLE ),work(BS.GetFlags()) ;
    assert(!BS.IsFlaggedSymmetric());
    work = BS * (-h);
    ii.IdentityMatrix(BS.Nr());
    ok=IminushBS.BinaryAdd(  work , ii); work.Init(CSC);
    if(IminushBS.StructureIsSameQ(&(this->m_IMinusBh)))
    {
        memcpy(this->m_IMinusBh.m_hbio.values ,IminushBS.m_hbio.values,BS.m_hbio.nnzero*sizeof(double));
    }
    else {
        if(this->m_IMinusBh.Nr()>0) {this->m_IMinusBh.dss_end();
            qDebug()<< "(RXStiffSolver::simpr) structure has changed.";}
        this->m_IMinusBh.Init(CSC);
        this->m_IMinusBh=IminushBS;
        // this->m_IMinusBh.HB_Write("IMinusBhFull")  ;
        err=this->m_IMinusBh.dss_start();
        if (err != MKL_DSS_SUCCESS)
            cout<<"WHOOPS (dss-start) ";
    }
    err = this->m_IMinusBh.dss_factor(this->m_IMinusBh.m_hbio.values);
    if (err != MKL_DSS_SUCCESS) cout<<"WHOOPS (factor) ";
    ///////////////////////END SPARSE ADDITION////////////////////////

    for (i=0;i<n;i++)    yout[i]=h*(dydx[i]+h*dfdx[i]);

    err = this->m_IMinusBh.dss_solve(yout.v);
    if (err != MKL_DSS_SUCCESS) cout<<"WHOOPS (1) ";

    for (i=0;i<n;i++)
        ytemp[i]=y[i]+(del[i]=yout[i]);
    x=xs+h;
    derivs(x,ytemp,yout);
    for (nn=2;nn<=nstep;nn++) {
        for (i=0;i<n;i++)
            yout[i]=h*yout[i]-del[i];

        err = this->m_IMinusBh.dss_solve(yout.v);
        if (err != MKL_DSS_SUCCESS) cout<<"WHOOPS (2) ";

        for (i=0;i<n;i++) ytemp[i] += (del[i] += 2.0*yout[i]);
        x += h;
        derivs(x,ytemp,yout);
    }
    for (i=0;i<n;i++)
        yout[i]=h*yout[i]-del[i];

    err = this->m_IMinusBh.dss_solve(yout.v);
    if (err != MKL_DSS_SUCCESS) cout<<"WHOOPS (3) ";


    for (i=0;i<n;i++)
        yout[i] += ytemp[i];
}

void RXStiffSolver::pzextr(const int iest, const DP xest, Vec_I_DP &yest, Vec_O_DP &yz,
                           Vec_O_DP &dy)
{
    int j,k1;
    DP q,f2,f1,delta;

    int nv=yz.size();
    Vec_DP c(nv);
    Vec_DP &x=*x_p;
    Mat_DP &d=*d_p;
    x[iest]=xest;
    for (j=0;j<nv;j++) dy[j]=yz[j]=yest[j];
    if (iest == 0) {
        for (j=0;j<nv;j++) d[j][0]=yest[j];
    } else {
        for (j=0;j<nv;j++) c[j]=yest[j];
        for (k1=0;k1<iest;k1++) {
            delta=1.0/(x[iest-k1-1]-xest);
            f1=xest*delta;
            f2=x[iest-k1-1]*delta;
            for (j=0;j<nv;j++) {
                q=d[j][k1];
                d[j][k1]=dy[j];
                delta=c[j]-q;
                dy[j]=f1*delta;
                c[j]=f2*delta;
                yz[j] += dy[j];
            }
        }
        for (j=0;j<nv;j++) d[j][iest]=dy[j];
    }
}

void RXStiffSolver::jacobian(const DP x, Vec_I_DP &y, Vec_O_DP &dfdx, RXSparseMatrix1 &pBS)
{
    // dfdx is d/dt of our residuals    - for steady problems it is zero,
    // but we might decide to ramp  up the residual rather than throwing them all in at the start
    /*
     * To construct S we follow subroutine SolveImplicit and SparseSolveLinearC
     **/

    /*  To test, try ODEExampleAstral.nb
**/
    RXSparseMatrix1 BS_small;
    int i;    int n=y.size();
    for (i=0;i<n;i++) dfdx[i]=0.0;
#ifdef NEVER
    // a first-order test problem, see ODEExample.nb
    dfdy[0][0] = -3.0*pow(y[0],2);
    dfdy[0][1] = -1.;
    dfdy[1][0] = 1.0;
    dfdy[1][1] = -0.2 *y[1];
    return;
#endif
    // x is time
    // y is our X followed by V
    // our dfdx is velocities followed by accelerations (see  subroutine DerivativeList)
    // dfdx is zero for steady-state
    // Question: what to take for XM??
    // dfdy is D[residual] by D[X]  ie like the global stiffness matrix.

    // packing
    /*  dfdx        X         V
   *         ---------------------
   *         |         |         |
   *    V    |   [0]   |   [I]   |
   *         |         |         |
   *         ----------------------
   *         |    -1   |   -1    |
   *    V'   |  xm  .S | XM  .Q  |
   *         |         |         |
   *         ---------------------
   **/

    double rcapvalue=-1;    int nnn=0, rc=0;
    bool bytest=true;
    class RXSparseMatrix1   InvMass(RXM_RECTANGLE)  ;
    class RXSparseMatrix1 *pBee = &InvMass;
    cf_FormInverseMassMatrix(&pBee);
    pBee->SetStorageType(CSC);
    if(m_dbg) pBee->HB_Write("RawInvMass");

    if(bytest) {
        class RXSparseMatrix1 *pStf =0; //the finite element stiffness matrix
        if(m_dbg){
            class RXSparseMatrix1 *pjay,*prhs;
            cf_FormJayMatrix(&pjay,&prhs,&nnn,rcapvalue); //  does RHS too. returns nrhs= (y.size())/2 + 1 "
            pjay->SetStorageType(CSC);
            pjay->HB_Write("jay");  delete pjay; delete prhs;

            cf_FormStiffnessMatrix(&pStf,0);  pStf->HB_Write("m");  delete pStf; pStf=0;
        }
        cf_FormStiffnessMatrixByTest(&pStf);
        pStf->SetStorageType(CSC) ;   if(m_dbg) pStf->HB_Write("RawStfByTest");
        int bad=pStf->Polish() ;   if(bad) cout<<" polish gave "<<bad<<endl;

        rc= pBee->Multiply (*pStf, BS_small) ;        // premultiply Stf by Inverse Mass to create B_S
        delete pStf;
    }
    else{
        class RXSparseMatrix1 *pm=0,*pjay,*prhs;

        cf_FormJayMatrix(&pjay,&prhs,&nnn,rcapvalue); //  does RHS too. returns nrhs= (y.size())/2 + 1 "
        pjay->SetStorageType(CSC);
        if(m_dbg){  pjay->HB_Write("jay");}
        prhs->Init(CSC);
        cf_FormStiffnessMatrix(&pm,0);    pm->Transpose();
        pm->SetStorageType(CSC);

        if(m_dbg)pm->HB_Write("originalstiff");
        assert((nnn -1)*2==y.size() );
        class RXSparseMatrix1 Stf(pm->GetFlags()); //the finite element stiffness matrix

        int bad= pm->Polish()+100* pjay->Polish() ;   if(bad) cout<<" polish gave "<<bad<<endl;

        pm->SetStorageType(CSC);			 pjay->SetStorageType(CSC);

        {   class RXSparseMatrix1 a(RXM_RECTANGLE);
            rc=  pm->Multiply(*pjay,a); pm->Init(CSC);
            if(rc !=0) {
                cout<< " (jacobn_s) first multiply failed: code="<<rc<<endl; return;
            }
            pjay->Transpose();
            a.SetStorageType(CSC);
            rc= pjay->Multiply (a,Stf) ; delete pjay; pjay=0;  delete prhs; prhs=0;
            if(rc !=0) {
                cout<< "(jacobn_s)  second multiply failed"; 	return;
            }
            if(m_dbg) Stf.HB_Write("JtMJ"); // JT.m.J
        } // scope of a;

        pBee->SetStorageType(CSC);
        // premultiply Stf by Inverse Mass to create B_S
        rc= pBee->Multiply (Stf, BS_small) ;

        delete pm; pm=0;
        if(rc !=0) {
            cout<< "(jacobn_s)  third multiply failed"; 	return;	}
    }

    BS_small.SetMatrixType(RXM_RECTANGLE) ;  // BS is B.stiffness matrix ie our dfdy

    /*  dfdx        X         V
   *         ---------------------
   *         |         |         |
   *    V    |   [0]   |   [I]   |
   *         |         |         |
   *         ----------------------
   *         |         |         |
   *    V'   |   B.S   |  B.C    |
   *         |         |         |
   *         ---------------------
   **/
    RXSparseMatrix1 ii(CSC), temp(CSC),DampMatrix(CSC),m21(CSC);
    int ok, nr = BS_small.Nr();

    //submatrix (1,2)
    ii.IdentityMatrix(nr);
    ii.AddCols(1,nr); ii.SetMatrixType(BS_small.GetFlags()  );
    ii.AddRows(nr+1,nr);


    //submatrix (2,1)
    m21.DeepCopy(BS_small); m21=m21*(-1.0);
    m21.AddCols(nr+1,nr);
    m21.AddRows(1,nr);
    ok=temp.BinaryAdd( m21,ii);
    if(m_dbg)m21.HB_Write("matrix21");
    //submatrix (2,2)

    //B.S multiplied by the damping factor, with a small amount added to the diagonal
    // to ensure it is not singular

    BS_small=BS_small * (- this->m_damp );
    // get the average of   BS_small
    double *lp, tot=0.0;
    for(i=0,lp=BS_small.m_hbio.values;i<BS_small.m_hbio.nnzero;i++,lp++) tot+=fabs(*lp);
    tot=tot/((double)BS_small.m_hbio.nnzero);

    ii.IdentityMatrix(nr); ii=ii*tot;
    ok=DampMatrix.BinaryAdd( BS_small,ii);  if(m_dbg) DampMatrix.HB_Write("DampMatrix");

    DampMatrix.AddCols(1,nr);
    DampMatrix.AddRows(1,nr);
    ok=pBS.BinaryAdd( temp,DampMatrix);

    if(m_dbg)  pBS.HB_Write("bs");


    assert(ok);

}

