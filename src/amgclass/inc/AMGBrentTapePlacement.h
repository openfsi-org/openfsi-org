// AMGBrentTapePlacement.h: interface for the AMGBrentTapePlacement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGBRENTTAPEPLACEMENT_H__5FA5A0F6_BB52_4BE5_910F_E6062F5630D8__INCLUDED_)
#define AFX_AMGBRENTTAPEPLACEMENT_H__5FA5A0F6_BB52_4BE5_910F_E6062F5630D8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class 	AMGZoneEdge;
class ON_Curve;
class AMGTape;
class dovestructure;
#include "AMGLevel.h"

#include "BrentMethod.h"

class AMGBrentTapePlacement : public BrentMethod  
{
public:
	int DBG_Listing(FILE *fp);
	int Bracket(double p_seed, double &xmin, double & xmax);
	AMGBrentTapePlacement();

AMGBrentTapePlacement(
	double m_alphaerror,
	double m_MaxLateralOverlap,
	double m_MinLength,
	double m_MaxLength,
	double m_tapewidth,
	int m_GoingUpTheCurve,
	int m_IsRightOnTape,
	AMGLevel m_level,
	AMGTape * m_pLastTape,
	dovestructure * m_dove,
	AMGZoneEdge * m_ze,
	ON_Curve * m_ptheCurve); 


	virtual ~AMGBrentTapePlacement();

	AMGTape * m_NewTape;

protected:
	double Func(const double x);

private:
	double m_alphaerror;
	double m_MaxLateralOverlap;
	double m_MinLength;
	double m_MaxLength;
	double m_tapewidth;
	int m_GoingUpTheCurve;
	int m_IsRightOnTape;

	AMGLevel m_level;
	AMGTape * m_pLastTape;
	dovestructure * m_dove;
	AMGZoneEdge * m_ze;
	ON_Curve * m_ptheCurve;

};

#endif // !defined(AFX_AMGBRENTTAPEPLACEMENT_H__5FA5A0F6_BB52_4BE5_910F_E6062F5630D8__INCLUDED_)
