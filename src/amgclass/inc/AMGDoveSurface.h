// AMGDoveSurface.h: interface for the AMGDoveSurface class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGDOVESURFACE_H__3EE998D4_F763_461B_A6BC_9AF7DABAE094__INCLUDED_)
#define AFX_AMGDOVESURFACE_H__3EE998D4_F763_461B_A6BC_9AF7DABAE094__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AMGHoopsObject.h"

#include "AMGLevel.h"

#include "layertst.h"
#include "dovestructure.h"

class AMGDoveSurface  : public AMGHoopsObject
{
public:
	ON_2dVector  GradAlphaByPolyCurve(ON_2dPoint p_pos);
	ON_2dVector EvaluateAlphaByPolyCurve(ON_2dPoint p_pos);

	ON_2dPoint XYZToUV(const ON_3dPoint & p_xyz) const;
	AMGDoveSurface();
	virtual ~AMGDoveSurface();

	AMGDoveSurface(dovestructure  * p_pDove,
					ON_Surface          * p_pSurface);	

	AMGDoveSurface(ON_ClassArray<ON_PolylineCurve>  p_AlphaPLs, 
					double p_Dalpha,
								ON_Surface          * p_pSurface);

	double GetCurvature(ON_2dPoint p_pos, const AMGLevel & p_level);

	virtual ON_2dVector EvalAlphaByXY(ON_2dPoint p_pos, const AMGLevel & p_level);
	virtual ON_2dVector EvalGradAlphaByXY(ON_2dPoint p_pos, const AMGLevel & p_level); //,const in & p_ply);

	ON_Curve * GetStartingCurve(const ON_3dPoint & p_point, 
									 const int & p_level);
			
protected:
	dovestructure  * m_pDove;
	ON_Surface          * m_pSurface;

	ON_ClassArray<ON_PolylineCurve> m_AlphaPLs;  //set of  polylines to define the field of Alpha 
	double m_DAlpha; //Dalpha for m_AlphaPLs in degrees

};

#endif // !defined(AFX_AMGDOVESURFACE_H__3EE998D4_F763_461B_A6BC_9AF7DABAE094__INCLUDED_)
