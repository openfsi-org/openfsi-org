// AMGZoneCycle.h: interface for the AMGZoneCycle class.
//
// this class is designed to be contained by AMGPanel and to be overridden for panels 
// with different types of geometrical description
/* For instance:
		A convex polygon
		A concave polygon
		A closed list of curves.
		An untrimmed surface
		A trimmed surface. 
  */
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGZONECYCLE_H__1D3583BA_6E4C_4E69_9FEB_1BA4B6A7B7BA__INCLUDED_)
#define AFX_AMGZONECYCLE_H__1D3583BA_6E4C_4E69_9FEB_1BA4B6A7B7BA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AMGHoopsObject.h"
#include "AMGTape.h"
#include "AMGZoneEdge.h"
#include "AMGZERef.h"
#include "AMGLevel.h"
class dovestructure;
class AMGTapingZone_V2;
class AMGTapingZone;
#include "opennurbs.h"

#ifndef TODO
#ifndef AMGUI_2005
	#define TODO assert(0);
#else
	#define TODO assert(1);  //TR 30 aug 06
#endif 
#endif

#define AMGRETVAL_KEEPGOING		-5
#define AMGRETVAL_DONE			-7
#define AMGRETVAL_PLEASE_RETAPE	-11
#define AMGRETVAL_ERROR			-13

enum ZCIntersectionResult
{
	NoIntersect         	= 0,    //!< self-explanatory
	DoesIntersect			= 1,    //!< self-explanatory
	IntersectFail			= 2,    //!< self-explanatory
	TypeNotHandled			= 3,    //!< self-explanatory
	BadInput				= 4,    //!< self-explanatory
	Undefined				=5
};


class AMGZoneEdgeLinear : public  AMGZoneEdge
{
public:

	AMGZoneEdgeLinear();
	AMGZoneEdgeLinear(ON_Curve*pc);
	virtual ~AMGZoneEdgeLinear();	

};

class AMGZoneCycle : public AMGHoopsObject ,public layerobject  // the default class takes a convex ON_PolylineCurve
{
public:
	AMGZoneEdge *GetIntersectedZoneEdge();
	double FindStartingT(ON_Curve *c,const double tapewidth,int*err);
	int ChooseEdgeToTape(const AMGLevel level);
	int Reverse();
	HC_KEY DrawHoops( const char*seg=0);
	void SetDove(dovestructure *p_dove);
	void SetPanel(AMGTapingZone_V2 *p_tz);
	int AddEdge(AMGZoneEdge *p_e);
	int FinishFromEdges();
	double GetMinLength();
	int RecentlyCrossedBoundary();
	double m_ButtOverlap;
	double m_maxgap;
	int KillIntersectionZER();
	int Print(FILE *lp,const int what =AMGPRINT_OBJ|AMGPRINT_RHINO );

	virtual 	ZCIntersectionResult ZCIntersect(ON_Line &p, double* t_on_p,int p_SkipThis);
	virtual 	int IsInside(const ON_3dPoint q);
	virtual		int MakeOneTapePass(const AMGLevel level);
	virtual 	int MakeTapes(const AMGLevel level);
	int SetParameters(const double aerr, const double mlo, const double minL, const double MaxL, const double w,const double buttOverlap, const double maxgap);

	AMGZoneCycle();
	AMGZoneCycle(const ON_String & p_path,
			const ON_String & p_name,
			ON_Curve * p_boundary,
			dovestructure * p_dove, 	
			AMGTapingZone *p_tz
			);
	virtual ~AMGZoneCycle();
	
	int TapeEdgeFirstOffset(	AMGZoneEdge *p_e, //  april 17th, this starts the new tape on a SPLIT zn.
	 const bool IsRightOnCurve, const AMGLevel level, 
	AMGTape** l_theTape,
 	double  &l_t,
	AMGZERef  **TheRef,
	int 	&l_RetFlags,
	int &ButtingStatus,// a flag telling whether the last tape was butted (ie intersects a far boundary) or free
	AMGZoneNode  **n1,	// the ZoneNode on which the current tapeend curve (endpts) started.
	AMGZoneNode  **n2,
	ON_3dPointArray &endpts 	// the polyline we construct thru the ends of the free tapes as we lay them. 
);
	
	ON_SimpleArray<AMGTape *> m_tapelistzc; // unstructured list of ALL tapes in this layer.
	dovestructure * m_dove;


protected:

	int GetBBox(double *pmin, double* pmax);
	int TapeEdgeFirstOnNode( AMGZoneEdge *p_e, //  april 17th, this starts the new tape on a SPLIT zn.
	 const bool IsRightOnCurve, const AMGLevel level, 
	AMGTape** l_theTape,
 	AMGZERef  **TheRef,	// the ZER gluing the start of the tape to p_e
	int 	&l_RetFlags,
	int &ButtingStatus,// a flag telling whether the last tape was butted (ie intersects a far boundary) or free
	AMGZoneNode  **n1,	// the ZoneNode on which the current tapeend curve (endpts) started.
	AMGZoneNode  **n2,
	ON_3dPointArray &endpts 	// the polyline we construct thru the ends of the free tapes as we lay them. 
);


	int  TapeEdgeNext(	AMGZoneEdge *p_e,
	 const int IsRightOnCurve, const AMGLevel level, const int instructions,
	AMGTape** l_theTape,
	AMGTape	**lastTape,
	ON_Curve  **TheCurve,	// the curve thru the tape ends.
 	double  &l_t,
	AMGZERef  **TheRef,
	AMGZERef  	**l_LastRef,
//	int &l_CountEndIntersections, 
	int 	&l_RetFlags,
	int &ButtingStatus,// a flag telling whether the last tape was butted (ie intersects a far boundary) or free
	AMGZoneNode  **n1,	// the ZoneNode on which the current tapeend curve (endpts) started.
	AMGZoneNode  **n2,
	ON_3dPointArray &endpts,	// the polyline we construct thru the ends of the free tapes as we lay them. 
	AMGTape **seedtape // the previous tape.  we use it to set out this one.
			// the curve of the old zoneedge (p_e) which we are taping.

);

	int ExtendAllTapes();
//	ON_Curve * TapeGap(AMGZERef *i,const int level);
//	AMGZERef  * ChooseGap(const int level);

	bool m_IsOnRight;
	double m_alphaerror;
	double m_MaxLateralOverlap;
	double m_minLength;
	double m_maxlength;
	double m_tapewidth;
 
	AMGZERef *m_IntersectionZER;// ephemeral for holding the intersection at the far end.  NOT MP-SAFE.
	ON_SimpleArray<AMGZoneEdge *>  m_edges; // was Linear.  a simple container for the tape intersection

	AMGTapingZone *m_panel; //  the AMGTZ which owns it. SSD by layerobject member.

	ON_Curve *TapeThisEdge(AMGZoneEdge *e,const bool IsOnRight, const AMGLevel level);
	ON_Curve *TapeThisCurve(ON_Curve *e,const bool IsOnRight, const AMGLevel level);

// for AMGTZ second method.
	ON_Curve *TapeThisEdge_2(AMGZoneEdge*e,const bool IsRightOnCurve, const AMGLevel level, const int instructions);	
	int TapeThisEdge_3(AMGZoneEdge*e,const bool IsRightOnCurve, const AMGLevel level, const int instructions);	


private:
	int TapingFinishBlock(AMGZoneEdge*e,AMGTape* lastTape,int ButtStatus,AMGZoneNode *n1,ON_3dPointArray &endpts);
	AMGZoneEdge * TapingButtStep(AMGTape*p_tape,AMGTape *p_lastTape,ON_3dPointArray &endpts,
		int ButtStatus,  // refers to the status of the lastTape not to the status of thistape.
		int IsIncreasing,
		AMGZoneNode **n1
 );

	int TapingStartNewButtFree(AMGTape*p_tape,AMGTape *p_lastTape,ON_3dPointArray &endpts,AMGZoneNode **n1);
};

#endif // !defined(AFX_AMGZONECYCLE_H__1D3583BA_6E4C_4E69_9FEB_1BA4B6A7B7BA__INCLUDED_)
