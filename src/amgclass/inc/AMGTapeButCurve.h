// AMGTapeButCurve.h: interface for the AMGTapeButCurve class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGTAPEBUTCURVE_H__F5CDBA86_0926_46E2_81D5_3B0766D20456__INCLUDED_)
#define AFX_AMGTAPEBUTCURVE_H__F5CDBA86_0926_46E2_81D5_3B0766D20456__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "opennurbs.h" // peter
#include "AMGHoopsObject.h"
#include "AMGDoveSurface.h"

#ifdef linux
#ifndef BOOL 
	#define BOOL int
#endif
#ifndef TRUE
	#define TRUE 1
#endif
#endif

class AMGTapeButCurve : public AMGHoopsObject 
{
public:

	int DrawAllTapes(ON_NurbsCurve & p_endcurve, 
								  const ON_3dmObjectAttributes * p_pAtt, 
								  BOOL p_TapeOnRigth, 
								  const ON_String & p_workingdir);
	AMGTapeButCurve();
	virtual ~AMGTapeButCurve();

	AMGTapeButCurve(ON_Curve * p_crv,	
					AMGHoopsObject * p_doveSrf = NULL,
					const int & p_ply = 0,
					BOOL p_tapeonright = FALSE, 
					BOOL p_tapeonleft = FALSE,
					const double & p_tapewidth = 100.0, 
					const double & p_lateraloverlap = 0.3, 
					const double & p_maxdeviation   = 5.0, 
					const double & p_buttoverlap    = 30.0, 
					const double & p_maxlength    = 1500.0, 
					const double & p_minlength    = 280.0);

	int DrawOneTape(double p_posoncurve,
				BOOL p_IsOnRigth, 
				double & p_nextstart, 
				ON_Polyline  & p_Pl, 
				const ON_3dmObjectAttributes * p_pAtt);

//	double FindPointOnCurve(ON_LineCurve p_LC);
	
protected:
	
	ON_Curve       * m_pcrv;
	AMGDoveSurface * m_pdoveSrf; 
	int m_ply;

	BOOL m_IsTapedOnRight;
	BOOL m_IsTapedOnLeft;

	double m_tapewidth;
	double m_lateraloverlap; //[0,1]
	double m_maxdeviation;
	double m_buttoverlap;
	double m_MaxLength;
	double m_MinLength; 

	ON_SimpleArray<AMGHoopsObject*> m_pTapes;

	int ExtendTapeEnds(ON_2dPoint & p_start,
			   ON_2dPoint & p_end,
			   const double p_ext);

	int GetNextStartingPoint(const double & p_currentt, 
										   double p_offset, 
										   BOOL p_IsOnRigth,
										   double & p_nextt);
};

double Modulos(const double & p_beta, const ON_Interval & p_bound); 

#endif // !defined(AFX_AMGTAPEBUTCURVE_H__F5CDBA86_0926_46E2_81D5_3B0766D20456__INCLUDED_)
