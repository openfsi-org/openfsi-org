// AMGPolygon.h: interface for the AMGPolygon class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGPOLYGON_H__F6EA9907_0FAB_4E41_AA2C_741AF78D46D7__INCLUDED_)
#define AFX_AMGPOLYGON_H__F6EA9907_0FAB_4E41_AA2C_741AF78D46D7__INCLUDED_

#include "opennurbs_polyline.h"	// Added by ClassView
#include "rxON_Extensions.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define AMGPVLIST  ON_SimpleArray<AMGVertex *> 
#define IsOdd(x) (x&1)

class AMGTapingZone_V3;
class AMGVertex;
class AMGVlink;
class AMGVlinkIntersection;
class AMGLevel;

#include "AMGTape.h"
#include "AMGTapingZone_V3.h"
#include "opennurbs_bounding_box.h"	// Added by ClassView
#include "AMGVlink.h"



#define AMGP_ANY					0
#define AMGP_IS_ALL_INSIDE			2
#define AMGP_IS_ALL_OUTSIDE			4
#define AMGP_IS_INTERSECTED			8
#define AMGP_IS_A_COPY   			16
#define AMGP_STRICTLYINSIDEOTHER	32
#define AMGP_INCONSISTENT			64
#define AMGP_DONE					128
#define VLINK_HAS_INTS				256
#define VLINK_PARALLEL				512
#define VLINK_STARTOFPARALLEL		VLINK_PARALLEL
#define VLINK_ENDOFPARALLEL			VLINK_PARALLEL

#define AMGP_DEAD					1024
class AMGPolygon : public AMGTape 
{

public:
// IsConsistent and IsInconsistent aren't mutually exclusive.
// IsConsistent verifies that the links are all owned by this and the vlist and the linked list are identical
//IsInconsistent returns true if the links and the vlist are out of whack. 
// OR if the links are owned by another, because in that case it can't walk. That's the case for result polys.

	int IsInconsistent()const; // verifies that walking the links and stepping m_vlist give the same result
	// Only works if the links are owned by this. If not, we cant tell.

	int IsConsistent()const; // verifies that walking the links and stepping m_vlist give the same result


	int FindBestEdge( double*score, AMGVertex **from, AMGVertex**to,const AMGLevel level); //uses m_vlist
	double CornerAngle(const int i);
	double FreeEdgeLength(const int p_flag)const; // walks taking vlist[0] as seed.
	double MomentOfArea(const ON_Line &axis)const;// from Vlist
	HC_KEY DrawHoops(char*seg=0);

	int ReplaceVertex( AMGVertex*v1_OldVertex,  AMGVertex *v2_NewVertex); // places v2 ptr at the location where v1 was found
	int HasDuplicateVertices(const double tol)const;   // looks in m_vlist;
	int IsClosed()const;   // uses GetPolyline and therefore works only from m_vlist.
	double Area() const;
	AMGVertex* GetSeedVertex(const int flag) const;  // looks in m_vlist
	AMGVlink* GetSeedLink(const int flag) const;
	AMGVlink* Walk(const AMGVertex *v, const AMGVlink *lastlink) const; //  looks at links not m_vlist

	AMGVlink* Connect(AMGVertex *v1,AMGVertex *v2  );
	int IsInside(const ON_3dPoint q)const;
	int WriteRhinoScript(AMGPVLIST *p_vv)const;
	int Print(FILE *fp)const;

	int MeasureIntersections(AMGPolygon *p_other);
	int MeasureIntersections_Old(AMGPolygon *p_other);

	ON_ClassArray<AMGPolygon*> BoolAndNot(AMGPolygon *p_other);
	ON_ClassArray<AMGPolygon*> BoolSubtract(AMGPolygon *p_other);
	ON_ClassArray<AMGPolygon*> BoolAdd(AMGPolygon *p_other);
	ON_ClassArray<AMGPolygon*> BoolIntersection(AMGPolygon *p_other);

	ON_Polyline GetPolyline() const; // works from vlist

	ON_BoundingBox GetBB() const;
	double Corner_Heuristic_01(const int i, const AMGLevel level);
	double Corner_Heuristic_02(const int i, const AMGLevel level);
	
	AMGPolygon();
	AMGPolygon(const rxON_PolylineCurve& c, AMGTapingZone_V3 *const p_tz);
	AMGPolygon(const AMGTape& t);
	AMGPolygon(const AMGTape *p_pt);
	virtual ~AMGPolygon();

	int m_Sign;
	AMGPVLIST m_vlist;

static	void DeleteVertexList(AMGPVLIST &l_vv);


protected:

	int Intersection_Process_Parallel(AMGVertex *a, AMGVertex *b,
									  AMGVertex *c, AMGVertex *d, 
									  ON_ClassArray<AMGVlinkIntersection> &p_IntList);

	int RebuildVertexList();
	int Intersection_Create_Vertices(ON_ClassArray<AMGVlinkIntersection> &p_Ints);	// by walk
	int Intersection_Repair_Vertices(ON_ClassArray<AMGVlinkIntersection> &p_Ints);	// by walk
	int Intersection_Repair_Edges(ON_ClassArray<AMGVlinkIntersection> &p_Ints);		// by walk
	int IntersectionTestByTopology(	ON_ClassArray<AMGVlinkIntersection> &p_Ints);	// by walk
	int Finish(); // makes the connections from the vlist 
	int PrepareBoolean(int m_IsMaster, AMGPVLIST &p_LocalVlist );  // looks only at m_vlist
	int MergeSimilarVertices(AMGPVLIST &p_vv1, AMGPVLIST &p_vv2 ,const double tol);
	int CountSimilarVertices(AMGPVLIST &p_vv1, AMGPVLIST &p_vv2 ,const double tol) const;
	int MarkEdgesInsideorOutside(AMGPolygon * p_theOther);			// uses vlist for seed, then walks.
	int CreateLocalIntList(AMGVlink *L,
		ON_ClassArray<AMGVlinkIntersection> &p_Ints,
		ON_ClassArray<AMGVlinkIntersection*> &rv );
		AMGVertex* m_seedVertex;
	};

struct AMGV_ParallelInt{
	AMGVertex *m_v;
	double m_t1,m_t2;
};

EXTERN_C int RXIntersectionVerif( const ON_Line& lineA, const ON_Line& lineB, 
                double* lineA_parameter, 
                double* lineB_parameter
                );
EXTERN_C int RX_CompareIncreasing_AMGParInt(const AMGV_ParallelInt *a,const  AMGV_ParallelInt *b   );
EXTERN_C  int RX_CompareIncreasing_AMGVlinkIntersection ( AMGVlinkIntersection *const*a, AMGVlinkIntersection *const*b);
#endif // !defined(AFX_AMGPOLYGON_H__F6EA9907_0FAB_4E41_AA2C_741AF78D46D7__INCLUDED_)
