// AMGReinfTrapeze.h: interface for the AMGReinfTrapeze class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGReinfTrapeze_MAY2006
#define AMGReinfTrapeze_MAY2006

#include "AMGHoopsObject.h"

#ifdef __cplusplus
	#define EXTERN_C extern "C"
#else
	#define EXTERN_C extern
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGReinfTrapeze : public  AMGHoopsObject
{
public:
	double GetTapeWidth() const;
	int SetDoc(CDocument * p_pdoc);
	int PrintToMpf(FILE * fp);
	AMGReinfTrapeze();
	AMGReinfTrapeze(const ON_String & p_model,
			   const ON_String & p_name);
	
	ON_String Serialize(); 
	char* Deserialize(char* p_lp );

	virtual ~AMGReinfTrapeze();

	int SetPly(const ON_String & p_plyname);
	ON_String GetPly() const;

	int SetSpan(const double & p_val);
	int SetThkOrg(const double & p_val);
	int SetThkExt(const double & p_val);
	int SetLengthOrg(const double & p_val);
	int SetLengthExt(const double & p_val);

	int SetPanelName(const ON_String & p_val);

	double GetSpan() const;
	double GetThkOrg() const;
	double GetThkExt() const;
	double GetLengthOrg() const;
	double GetLengthExt() const;

	ON_String GetMaterialCode();

	int ResetPosition(const ON_2dPoint & p_pos,const ON_3dVector& p_VBatten);

	int Redraw();

protected:
	CDocument * m_pdoc;
};

#endif // AMGReinfTrapeze_DEC2005
