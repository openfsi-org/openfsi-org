// AMGSolidDoveView.h: interface for the AMGSolidDoveView class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGSOLIDDOVEVIEW_H__B407601B_6E06_445C_BFD5_6F25E4010B0D__INCLUDED_)
#define AFX_AMGSOLIDDOVEVIEW_H__B407601B_6E06_445C_BFD5_6F25E4010B0D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CSolidHoopsView.h"

class AMGSolidDoveView : public CSolidHoopsView  
{
protected:
	AMGSolidDoveView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(AMGSolidDoveView)
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AMGSolidDoveView)
	public:
//	virtual void Serialize(CArchive& ar);   // overridden for document i/o
//	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
//	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	protected:
//	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	
	void OnFileLoad() ;


	virtual ~AMGSolidDoveView();
	void  OnFileSaveAs() ;
	void  OnFileSave() ;
	bool SaveFile( LPCTSTR csFilePathName );
	bool CopySafe() ;
	void LoadFile( LPCTSTR csFilePathName, HStreamFileToolkit* tk, bool isHNetServerStreaming );

	// Generated message map functions
protected:
	//{{AFX_MSG(AMGSolidDoveView)
		afx_msg void OnRButtonDown(UINT nFlags, CPoint point); // dec3 2006
		afx_msg void OnDoveToolsInfo();
		afx_msg void OnDoveToolsFilterAll();
		afx_msg void OnDoveToolsFilterThk();
		afx_msg void OnDoveToolsFilterKy();
		afx_msg void OnDoveToolsFilterKg();
		afx_msg void OnDoveToolsFilterAng();
		afx_msg void OnDoveToolsFilterNu();
		afx_msg void OnDoveToolsAddScrim();
		afx_msg void OnDoveToolsNoAddScrim();
		afx_msg void OnDoveToolsRebuildNu();
		afx_msg void OnDoveToolsRebuildKg();
		afx_msg void OnDoveToolsDrawTraceFast();
		afx_msg void OnDoveToolsDrawTraceSlow();

		afx_msg void OnDoveToolsZeroOrientation();
		afx_msg void OnDoveToolsViewOptions();
		afx_msg void OnDoveToolsRebuild();
		afx_msg void OnDoveToolsStyleEdit();
		afx_msg void OnDoveTools();


		afx_msg  void OnDoveThicknessEdit();
		afx_msg  void OnDoveKeEdit();
		afx_msg  void OnDoveKgEdit();
		afx_msg  void OnDoveNuEdit();
		afx_msg  void OnDoveAngleEdit();
		afx_msg  void OnDoveAngleEdit_NW();
		afx_msg  void OnDoveAngleEdit_NE();
		afx_msg  void OnDoveAngleEdit_SW();
		afx_msg  void OnDoveAngleEdit_SE();

		afx_msg void OnUpdateDoveAngleEdit(CCmdUI* pCmdUI);
		afx_msg void OnUpdateDoveThicknessEdit(CCmdUI* pCmdUI);
		afx_msg void OnUpdateDoveKeEdit(CCmdUI* pCmdUI);
		afx_msg void OnUpdateDoveKgEdit(CCmdUI* pCmdUI);
		afx_msg void OnUpdateDoveNuEdit(CCmdUI* pCmdUI);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AMGSOLIDDOVEVIEW_H__B407601B_6E06_445C_BFD5_6F25E4010B0D__INCLUDED_)
