// AMGNesting.h: interface for the AMGNesting class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGNesting_MAY2006
#define AMGNesting_MAY2006

#include "AMGHoopsObject.h"

#include "AMGTape2.h"

#ifdef __cplusplus
	#define EXTERN_C extern "C"
#else
	#define EXTERN_C extern
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGNesting : public  AMGHoopsObject
{
public:
	
	int PrintToMpf(FILE * fp, 
				   const ON_String & p_layername,
				   int & p_Speed,
				   int & p_plyID,
				   int & p_tapeID, 
				   int l_pdocptr = NULL, // VREY BAD practice a pointer to the document
				   const ON_String & p_Nextlayername = AMG_NO_NAME, 
				   const double    & p_tapeMinLength = 0.28, 
				   const double    & p_scalefactor = 1.0, 
				   const int         p_pPDF        = NULL,
				   ON_Xform        * p_TransToPdf  = NULL, 
				   ON_SimpleArray<double> * p_params = NULL); 
	
	int MovePanel(const ON_String & p_panel, const double & p_Tx, const double & p_Ty, const double & p_Rz, BOOL p_Initialize = FALSE);
	AMGNesting();
	AMGNesting(const ON_String & p_model,
			   const ON_String & p_name);
	
	ON_String Serialize(); 
	char* Deserialize(char* p_lp );

	virtual ~AMGNesting();

	AMGNesting& operator=(const AMGNesting&);


	ON_ClassArray<AMGTape2>  GetListMarkers(ON_String p_panelname);

	int AddPanel(const ON_String & p_panelname);
	int AddMarks(const ON_String & p_segPanel,const ON_ClassArray<ON_String> & p_segMarks);
	int RemovePanel(const ON_String &p_panelName);
	int GetPanelList(ON_ClassArray<ON_String> & p_PanelNames) const;
	int GetPlyList(ON_ClassArray<ON_String> & p_PanelNames) const;

	int IsInPanelList(const ON_String &p_panelName) const;
	int UpdatePanels();
	
	int SetDoc(CDocument * p_pdoc);
protected: 
	CDocument * m_pdoc;
};

#endif // AMGNesting_DEC2005
