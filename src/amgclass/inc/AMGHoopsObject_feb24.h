// AMGHoopsObject.h: interface for the AMGHoopsObject class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGHOOPSOBJECT_H__651B70BC_D005_4A19_97F6_C5D3A19F389C__INCLUDED_)
#define AFX_AMGHOOPSOBJECT_H__651B70BC_D005_4A19_97F6_C5D3A19F389C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AMGHoopsObject.h"
#include "opennurbs.h"
class AMGHoopsObject  
{
public:
	int Init();
	AMGHoopsObject();
	AMGHoopsObject(const ON_String & p_Model,const ON_String & p_name);
	
	virtual ~AMGHoopsObject();
	
	ON_String Serialize();
	char* Deserialize(char* p_lp );

	int GetModel(ON_String & p_Model) const;
	int SetModel(const ON_String & p_Model);
	int GetName(ON_String & p_Model) const;
	int SetName(const ON_String & p_Model);
	
	//Define or reset an user option define in the segment :
	//if p_localModel!=0 			m_Model / m_name  / p_localPath
	//if p_localModel==0 			m_Model / m_name  / 
	//the name of the user option is p_name the vqlue is set to p_val
	int SetUserOption(const ON_String & p_localpath, 
					  const ON_String & p_name, 
					  const float & p_val);

	//get the value of the user option defined in the segment :
	//if p_localpath!=0 			m_path / m_name  / p_localpath
	//if p_localpath==0 			m_path / m_name  / 
	//the name of the user option is p_name the value is retunr in p_val
	int GetUserOption(const ON_String & p_localpath, 
					  const ON_String & p_name, 
					  float & p_val) const;

	//Define or reset a polyline user option define in the segment :
	//if p_localpath!=0 			m_path / m_name  / p_localpath
	//if p_localpath==0 			m_path / m_name  / 
	int SetPolyline(const ON_String & p_localpath,  
					const ON_Polyline p_PL);



						
	//return a polyline user option define in the segment :
	//if p_localpath!=0 			m_path / m_name  / p_localpath
	//if p_localpath==0 			m_path / m_name  / 
	int GetPolyline(const ON_String & p_localpath,  
					ON_Polyline & p_PL) const;


	//Define or reset some markers user option define in the segment :
	//if p_localpath!=0 			m_path / m_name  / p_localpath
	//if p_localpath==0 			m_path / m_name  / 
	int SetPoints(const ON_String & p_localpath,  
					const ON_3dPointArray p_pts);

	//Define or reset a surface define in the segment :
	//if p_localpath!=0 			m_path / m_name  / p_localpath
	//if p_localpath==0 			m_path / m_name  / 
	int SetSurface(const ON_String & p_localpath,  
				   const ON_Surface * p_srf);

	ON_String GetFullPath(ON_String * p_localpath = NULL) const;

protected: 
	ON_String m_model; 
	ON_String m_name;

	ON_String SerializePolyline(const ON_String & p_localpath);
	// returns pointer t the char where reading should continue. 

	char* DeserializePolyline(const ON_String & p_localpath,  
								char* p_lp ); 

	ON_String SerializeUserOptions(const ON_String & p_localpath) ;

	char* DeserializeUserOptions(const ON_String & p_localpath, 
								char* p_lp);

	// returns pointer t the char where reading should continue. 
};

#endif // !defined(AFX_AMGHOOPSOBJECT_H__651B70BC_D005_4A19_97F6_C5D3A19F389C__INCLUDED_)
