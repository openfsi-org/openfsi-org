#ifndef AMGPROPERTIESFROMFEA_JUNE2007
#define AMGPROPERTIESFROMFEA_JUNE2007

class AMGPropertiesFromFEA
{
protected:
	ON_SimpleArray<int> m_node_ID;
	ON_SimpleArray<double> m_area;
	ON_SimpleArray<double> m_u;
	ON_SimpleArray<double> m_v;
	ON_SimpleArray<double> m_UP;
	ON_SimpleArray<double> m_LP;
	ON_SimpleArray<double> m_angle;
	ON_SimpleArray<double> m_Sx;
	ON_SimpleArray<double> m_Sy;
	ON_SimpleArray<double> m_Txy;
	ON_SimpleArray<double> m_Ex;
	ON_SimpleArray<double> m_Ey;
	ON_SimpleArray<double> m_Gxy;
	ON_NurbsSurface m_surf;
	bool surf_set;
public:
	//int AddNullBorderPoints(const int &N);
	//int AddContinuousBorderPoints(const int &N);
	int ReadFEAnalysisFromFile(ON_String name);//Read a entities.out file type from a relax analysis and store the results in the members
	int WritePropToFile(ON_String name);//Temporary function to output the interpreted file with the corresponding data
	int ReadSurfFrom3dm(ON_String name);//Read the blank surface from a rhino file
	int WritePointDataTo3dm(ON_String name,ON_String datatype, const double &scale);//write the data point to a rhino point cloud
	//int WriteDataFieldTo3dm(ON_String name,ON_String datatype, const double &scale);
	int WriteDataToNurbs(ON_String datatype, const double &scale, ON_NurbsSurface &l_res);//Create a Nurbs from a data points field
	int WriteNurbsTo3dm(ON_String name,ON_NurbsSurface const &l_res);//Write a nurbs to a rhino file
	int PlotAngleTo3dm(ON_String name);//Plot angle as vector fields in a rhino file
	int PlotAngleFromNurbsTo3dm(ON_NurbsSurface l_cos, ON_NurbsSurface l_sin, ON_String name);//Sample the data from a nurbs to a vecor field in rhino (angle)
	AMGPropertiesFromFEA(){m_node_ID.Empty();m_area.Empty();m_u.Empty();m_v.Empty();m_UP.Empty();
		m_LP.Empty();m_Sx.Empty();m_Sy.Empty();m_Txy.Empty();m_Ex.Empty();m_Ey.Empty();m_Gxy.Empty();surf_set=false;};
	~AMGPropertiesFromFEA(){m_node_ID.Destroy();m_area.Destroy();m_u.Destroy();m_v.Destroy();m_UP.Destroy();
		m_LP.Destroy();m_Sx.Destroy();m_Sy.Destroy();m_Txy.Destroy();m_Ex.Destroy();m_Ey.Destroy();m_Gxy.Destroy();m_surf.Destroy();};

	int SetSurface(const ON_NurbsSurface & p_surf);
	int PlotAngleToPolylines(ON_ClassArray<ON_Polyline> & p_vectors,ON_ClassArray<ON_Polyline> & p_heads);

	int WriteMaxStressToNurbs(const double & p_EpsMax,
							  const double & p_ThkMin,
							  const double & p_Etape,
							  ON_NurbsSurface & p_res);

};

#endif //AMGPROPERTIESFROMFEA_JUNE2007