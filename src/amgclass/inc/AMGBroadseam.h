// AMGBroadseam.h: interface for the AMGBroadseam class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGBroadseam_FEB2005
#define AMGBroadseam_FEB2005

#include "AMGHoopsObject.h"
#include <HEventManager.h> 
#include <HUtilitySubwindow.h> 
//#include "HNetClientMgr.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGBroadseam : public  AMGHoopsObject
{
public:
	AMGBroadseam();
	AMGBroadseam(const ON_String & p_model,
				 const ON_String & p_name,
				 const ON_2dPointArray * p_Offsets = NULL,  
				 const double * p_cord = NULL);
	
	virtual ~AMGBroadseam();

	int Remove();
	
	ON_String Serialize();
	char* Deserialize(char* p_lp );

	int GetOffsets(ON_2dPointArray &p_pl) const;
	double GetCord() const;

	double GetOffsetAt(const double & p_pos);

protected:
	
};




#endif // AMGBroadseam_DEC2005
