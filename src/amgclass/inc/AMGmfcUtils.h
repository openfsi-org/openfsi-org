// AMGmfcUtils.h: interface for the AMGmfcUtils class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGMFCUTILS_H__75AE5831_648D_4E13_805B_54F121E51D81__INCLUDED_)
#define AFX_AMGMFCUTILS_H__75AE5831_648D_4E13_805B_54F121E51D81__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Afx.h"

#include "opennurbs.h"

class AMGmfcUtils  
{
public:
	AMGmfcUtils();
	virtual ~AMGmfcUtils();

	int CStringToChar(CString p_string, char * p_char);
	
};

ON_String MakeLength(const ON_String & p_str, const int & p_len);

#endif // !defined(AFX_AMGMFCUTILS_H__75AE5831_648D_4E13_805B_54F121E51D81__INCLUDED_)
