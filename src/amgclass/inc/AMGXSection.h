// AMGXSection.h: interface for the AMGXSection class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGXSection_DEC2005
#define AMGXSection_DEC2005

#include "AMGHoopsObject.h"
#include <HEventManager.h> 
#include <HUtilitySubwindow.h> 
//#include "HNetClientMgr.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGXSection : public  AMGHoopsObject
{
public:
	int ComputeShnack();
	AMGXSection();
	AMGXSection(const ON_String & p_model,
						 const ON_String & p_name,
						 const ON_Polyline * p_curvatureMap = NULL,  
						 const double * p_relpos = NULL, 
						 const int * p_featidleech = NULL, 
						 const int * p_featidluff = NULL,
						 const int * p_featidvertcrv = NULL,
						 const int * p_featidrefplane = NULL,
						 const int * p_featidcsys = NULL,
						 const int * p_featidptonluff = NULL,
						 const int * p_featidsection = NULL,
						const int * p_featidptCrv = NULL,
						 const int * p_featidptYPos = NULL,
						 const double * p_chord = NULL,
						const double * p_depth = NULL,
						const double * p_twist = NULL,
						const double * p_draftpos = NULL,
						const double * p_draftfwd = NULL,
						const double * p_draftaft = NULL,
						const double * p_entryangle = NULL,
						const double * p_exitangle = NULL);
	
	virtual ~AMGXSection();

	int Remove();
	
	ON_String Serialize();
	char* Deserialize(char* p_lp );



	double GetPosition() const;
	int SetPosition(const float & p_val);

	// just for testing
	int TranslateOffsetCurve(const double & p_val);

	#ifdef _AMG2005
		ProError EditSection();
		int SetOffsetCurve(ON_Polyline p_pl, BOOL p_IsIn3dSpace);
	#endif
	int GetFeatIDPtCrv() const;
	int GetFeatIDPtYPos() const;
	int GetFeatIDPtOnLuff() const; 
	int GetFeatIDXSect() const;

	int GetCurvatureCurve(ON_Polyline &p_pl) const;
	int GetOffsetCurve(ON_Polyline &p_pl) const;

protected:
	int GetMaxDepthPosition(const ON_PolylineCurve & p_PLc,
									 double &		 p_CamberPosition, 
									 double &		 p_CamberRatio);
	ON_2dPoint Get2dPosition(const ON_PolylineCurve & p_PLc,ON_3dPoint l_ptmax);
	
	int GetDepthAt(const ON_Curve * p_pCrv, 
					const ON_3dPoint & p_pointInSectionplane,  // a point in the section plane to define the normal
					const ON_3dPoint	& p_PointOnCord, 
					ON_3dPoint	    & p_PointOnSection);
};




#endif // AMGXSection_DEC2005
