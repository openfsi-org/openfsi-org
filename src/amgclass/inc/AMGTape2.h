// AMGTape2.h: interface for the AMGTape2 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGTAPE2_H__947F604B_BAFB_4772_9D62_F9F91236915B__INCLUDED_)
#define AFX_AMGTAPE2_H__947F604B_BAFB_4772_9D62_F9F91236915B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "opennurbs.h"
//#include "Z:\build\cpp\on_relax\opennurbs_point.h"	// Added by ClassView removed PH March27 07

class AMGTape2  
{
public:
	double PrintToPdf(int p_pdf,const ON_Xform & p_TransToPdf) const;
	AMGTape2();
	AMGTape2(const ON_2dPoint & p_start,const ON_2dPoint & p_end, const double & p_width);
	// to define a marker
	AMGTape2(const ON_2dPoint & p_position);

	//create a tape from the key on a polygon of a marker
	AMGTape2(const HC_KEY & p_key, HC_KEY * p_KeySeg = NULL);
	
	virtual ~AMGTape2();

	AMGTape2& operator=(const AMGTape2&);

	BOOL IsOnTape(const ON_2dPoint & p_pt) const; 

	int SetPlyName(const ON_String & p_name);
	int GetPlyName(ON_String &p_name); 
	
	BOOL IsAMarker() const; 
	
	int SetPanelName(const ON_String & p_name);
	int GetPanelName(ON_String &p_name); 
	
	int IsInPly(const ON_String &p_name);
	int IsInPanel(const ON_String &p_name);

	ON_2dPoint GetStart() const;
	ON_2dPoint GetEnd() const;

	double GetWidth() const;


	ON_2dPoint GetPosition() const;
protected:
	ON_2dPoint m_start;
    ON_2dPoint m_end;
	double m_width;
	ON_String m_plyname;
	ON_String m_panelname;

	BOOL m_IsMarker; // is true if the object represents a marker, if the obect is a marker its position is m_start
};

#endif // !defined(AFX_AMGTAPE2_H__947F604B_BAFB_4772_9D62_F9F91236915B__INCLUDED_)
