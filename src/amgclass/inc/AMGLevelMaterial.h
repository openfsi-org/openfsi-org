#ifndef _AMG_LEVELMATERIAL_MAY07_H_
#define _AMG_LEVELMATERIAL_MAY07_H_

#include "RXmaterialMatrix.h"

class AMGLevelMaterial
{
public:
	AMGLevelMaterial(void);
	virtual ~AMGLevelMaterial(void);

	AMGLevelMaterial& operator=(const AMGLevelMaterial&);

	int SetMaterialMatrix(const RXmaterialMatrix & p_matrix);
	int Setlevel(const int & p_level);

	int GetLevel() const;
	RXmaterialMatrix GetMaterialMatrix() const;
protected:
	RXmaterialMatrix m_MaterialMatrix;
	int m_Level;
};
#endif
