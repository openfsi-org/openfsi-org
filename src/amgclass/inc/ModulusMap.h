// ModulusMap.h: interface for the ModulusMap class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MODULUSMAP_H__F28EEBF9_630D_417B_BAA7_DF90D20F7331__INCLUDED_)
#define AFX_MODULUSMAP_H__F28EEBF9_630D_417B_BAA7_DF90D20F7331__INCLUDED_

//#include "opennurbs.h"	// Added by ClassView (but ONV4 doesnt like it)
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "RXON_Surface.h"
#include "layerobject.h"
#include "RXmaterialMatrix.h"	// Added by ClassView
// #include "opennurbs.h"	// Added by ClassView


#define	m_u1	m_x[0	]
#define	m_u2	m_x[1	]
#define	m_x1	m_x[2	]
#define	m_x2	m_x[3	]
#define	m_x3	m_x[4	]
#define	m_E1111	m_x[5	]
#define	m_E1112	m_x[6	]
#define	m_E1122	m_x[7	]
#define	m_E1212	m_x[8	]
#define	m_E1222	m_x[9	]
#define	m_E2222	m_x[10	]
#define	m_Emin	m_x[11	]
#define	m_Emax	m_x[12	]

class doveforstack;

class ModulusMap : public layerobject  
{
public:
	RXON_NurbsSurface *Getsa() {return m_psa;} 
	RXON_NurbsSurface *Getca() {return m_pca;} 
	RXON_NurbsSurface *Getthk() {return m_pthk;} 
	RXON_NurbsSurface *Getnu() {return m_pnu;} 
	RXON_NurbsSurface *Getkg() {return m_pkg;} 
	RXON_NurbsSurface *Getke() {return m_pke;} 
	RXON_NurbsSurface *Getuvsurf() {return m_puvsurf;} 
	void Setuvsurf(RXON_NurbsSurface *x) { m_puvsurf=x;} 

	void InitGraphics();
	ON_3dPoint m_origin;
	HC_KEY Draw_Polar_E1111(const double scale);
	HC_KEY Draw_Polar_Modulus(const double scale);
	double GetAngle(){return atan2(m_sa,m_ca)/2.0;}
	int Write3dm(const char*filename);
	RXON_NurbsSurface * PointsToNurbs(ON_3dPointArray p, const int order);
	int MakeONSurfaces ( );
	int GuessKnotValues( ON_SimpleArray<double> &ku1, ON_SimpleArray<double> &ku2);
	int AppendPoints(ON_3dPoint &p);
	RXmaterialMatrix StiffnessPropertiesFromMM();
	RXmaterialMatrix StiffnessPropertiesFromMatrix(RXmaterialMatrix &p_m );
	int m_ncols;
	int m_nrows;
	double m_x[30];
	ModulusMap();
	virtual ~ModulusMap();

	double	m_TapeModulus;
	double	m_thk;
	double	m_sa;
	double	m_ca;
	double	m_ky;
	double	m_ks;
	double	m_nu;

protected:

	RXmaterialMatrix m_mm;
	ON_3dPointArray m_cv_thk;
	ON_3dPointArray m_cv_sa;
	ON_3dPointArray m_cv_ca;
	ON_3dPointArray m_cv_ky;
	ON_3dPointArray m_cv_ks;
	ON_3dPointArray m_cv_nu;
	ON_3dPointArray m_uv; 

	RXON_NurbsSurface *m_psa, *m_pca,*m_pthk,*m_pnu,*m_pkg, *m_pke, *m_puvsurf;

};


EXTERN_C int   TranslateTapeSamplingToModulusMap(const char*f1, const char*f2,
										const double p_Etape, 
										const double p_Eglue, 
										const double p_NuGlue, 
										doveforstack **p_Dove);

#endif // !defined(AFX_MODULUSMAP_H__F28EEBF9_630D_417B_BAA7_DF90D20F7331__INCLUDED_)

