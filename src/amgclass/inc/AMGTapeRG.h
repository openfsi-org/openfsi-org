#ifndef AMGTAPERG_MAY07
#define AMGTAPERG_MAY07
class AMG_TapeRG
{
private:
	ON_2dPoint m_tape_begin;
	ON_2dPoint m_tape_end;
	double m_width;
public:
	
	AMG_TapeRG(){m_tape_begin.Set(0,0);m_tape_end.Set(1,0);m_width=0;};
	AMG_TapeRG(ON_2dPoint p1,ON_2dPoint p2,double width){m_tape_begin=p1;m_tape_end=p2;m_width=width;};
	ON_2dPoint tape_begin(){return m_tape_begin;};
	ON_2dPoint tape_end(){return m_tape_end;};
	int Set (ON_2dPoint begin, ON_2dPoint end, double width){m_tape_begin=begin;m_tape_end=end;m_width=width;return 1;};
	double width(){return m_width;};
	ON_2dVector Dir(){ON_2dVector Dir(m_tape_end.x-m_tape_begin.x,m_tape_end.y-m_tape_begin.y);return Dir;};
	ON_2dVector Norm(){ON_2dVector Norm(m_tape_end.y-m_tape_begin.y,m_tape_begin.x-m_tape_end.x);Norm.Unitize();return Norm;};
	AMG_TapeRG Rotate(double angle, ON_2dPoint center){AMG_TapeRG out(ON_2dPoint(center.x+(m_tape_begin.x-center.x)*cos(angle)-(m_tape_begin.y-center.y)*sin(angle),center.y+(m_tape_begin.x-center.x)*sin(angle)+(m_tape_begin.y-center.y)*cos(angle)),ON_2dPoint(center.x+(m_tape_end.x-center.x)*cos(angle)-(m_tape_end.y-center.y)*sin(angle),center.y+(m_tape_end.x-center.x)*sin(angle)+(m_tape_end.y-center.y)*cos(angle)),m_width);return out;};
	AMG_TapeRG Translate(ON_2dVector vect){AMG_TapeRG out(ON_2dPoint(m_tape_begin.x+vect.x,m_tape_begin.y+vect.y),ON_2dPoint(m_tape_end.x+vect.x,m_tape_end.y+vect.y),m_width);return out;};
	double length(){return m_tape_begin.DistanceTo(m_tape_end);};
	ON_Polyline getPolylineForm(){ON_2dVector Norm(m_tape_end.y-m_tape_begin.y,m_tape_begin.x-m_tape_end.x);Norm.Unitize();
		ON_Polyline tape;tape.AppendNew() = ON_3dPoint(m_tape_begin.x+Norm.x*m_width/2,m_tape_begin.y+Norm.y*m_width/2,0);
		tape.AppendNew() = ON_3dPoint(m_tape_end.x+Norm.x*m_width/2,m_tape_end.y+Norm.y*m_width/2,0);
		tape.AppendNew() = ON_3dPoint(m_tape_end.x-Norm.x*m_width/2,m_tape_end.y-Norm.y*m_width/2,0);
		tape.AppendNew() = ON_3dPoint(m_tape_begin.x-Norm.x*m_width/2,m_tape_begin.y-Norm.y*m_width/2,0);
		tape.AppendNew() = ON_3dPoint(m_tape_begin.x+Norm.x*m_width/2,m_tape_begin.y+Norm.y*m_width/2,0);return tape;};
#ifdef RENAUD	
		Polygon_2 getPolygonForm(){Polygon_2 polytape;ON_2dVector Norm(m_tape_end.y-m_tape_begin.y,m_tape_begin.x-m_tape_end.x);Norm.Unitize();
		polytape.push_back(Point_2(m_tape_begin.x+Norm.x*m_width/2,m_tape_begin.y+Norm.y*m_width/2));
		polytape.push_back(Point_2(m_tape_end.x+Norm.x*m_width/2,m_tape_end.y+Norm.y*m_width/2));
		polytape.push_back(Point_2(m_tape_end.x-Norm.x*m_width/2,m_tape_end.y-Norm.y*m_width/2));
		polytape.push_back(Point_2(m_tape_begin.x-Norm.x*m_width/2,m_tape_begin.y-Norm.y*m_width/2));
		if (polytape.is_clockwise_oriented())
			polytape.reverse_orientation();
		return polytape;};
#endif ///RENAUD
		virtual ~AMG_TapeRG(){};

};
#endif //#ifndef AMGTAPERG_MAY07
