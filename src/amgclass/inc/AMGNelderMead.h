// AMGNelderMead.h: interface for the AMGNelderMead class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGNELDERMEAD_H__FDBF78A6_E40E_44D5_8064_9060123FA206__INCLUDED_)
#define AFX_AMGNELDERMEAD_H__FDBF78A6_E40E_44D5_8064_9060123FA206__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGTapingZone_V3;
#include "NelderMead.h"

class AMGNelderMead : public NelderMead  
{
public:
	AMGNelderMead(AMGTapingZone_V3*p_tz);
	AMGNelderMead();
	virtual ~AMGNelderMead();
	double func(ON_SimpleArray<double>  &x);

protected:
	AMGTapingZone_V3 * m_tz;
};

#endif // !defined(AFX_AMGNELDERMEAD_H__FDBF78A6_E40E_44D5_8064_9060123FA206__INCLUDED_)
