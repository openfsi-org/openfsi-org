// AMGReinfRosette.h: interface for the AMGReinfRosette class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGReinfRosette_MAY2006
#define AMGReinfRosette_MAY2006

#include "AMGHoopsObject.h"

#ifdef __cplusplus
	#define EXTERN_C extern "C"
#else
	#define EXTERN_C extern
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGReinfRosette : public  AMGHoopsObject
{
public:
	int PrintToMpf(FILE * fp);
	AMGReinfRosette();
	AMGReinfRosette(const ON_String & p_model,
			   const ON_String & p_name);
	
	ON_String Serialize(); 
	char* Deserialize(char* p_lp );

	virtual ~AMGReinfRosette();

	int SetDoc(CDocument * p_pdoc);

	int SetNbTape(const int & p_val);
	int SetArc(const int & p_val);
	int UnSetArc(); 

	int SetMultiplicity(const int & p_val);
	int SetRadius(const double & p_val);
	int SetPly(const ON_String & p_val);
	
	int SetPanelName(const ON_String & p_val);

	int GetNbTape() const;
	int GetArc(double & p_val) const;
	int GetMultiplicity() const;
	double GetRadius() const;
	double GetTapeWidth() const; 
	
	ON_String GetMaterialCode() const;		

	ON_String GetPly() const;

	int ResetPosition(const ON_2dPoint & p_pos,const ON_3dVector& p_VBatten);

	int Redraw();

	int GetNbInstancesOnSail();
protected:
	CDocument * m_pdoc;
};

#endif // AMGReinfRosette_DEC2005
