#ifndef AMGThkMatch_MAY07
#define AMGThkMatch_MAY07


#include "AMGTapeRG.h"
#include "AMG_PolygonThk.h"

    class AMGThkMatch
	{
	public:
		//Compute the integral on the mask (if any) from the mesh surface interpolation
		__declspec(dllexport) int IntegrateVolFromMeshedSurf(double &l_val);
		//One way to set the surface
		__declspec(dllexport) int SetSurf(ON_NurbsSurface Surf){m_surf=Surf;return 1;};
		//Set the value of power (default is one)
		__declspec(dllexport) int SetPower(int Power){m_power=Power;return 1;};
		//Set the mask shape, default is null which triggers the integration over the whole mesh
		__declspec(dllexport) int SetMask(ON_Polyline mask);
		//Mesh the surface (really simple case for now)
		__declspec(dllexport) int Mesh(int parameter);
		//Read Surface from a rhino file
		__declspec(dllexport) int SetSurfFrom3dm(ON_String name);
		//Add tape to the existing taping
		__declspec(dllexport) int AddTape(AMG_TapeRG &p_tape, double thk, int &cnt,double &time, double &timeglobal, FILE *fp, FILE *fp2);
		//Add tape test2
		__declspec(dllexport) int  AddTapeTest2(AMG_TapeRG p_tape,double thk,double &time,int &cnt);
		//test AddTape
		static __declspec(dllexport) ON_ClassArray<AMG_PolygonThk> AddTapeTest(AMG_TapeRG tape,double thk, ON_ClassArray<AMG_PolygonThk> polys);
		//Integrate thickness over a candidate tape (from polygons intersection)
		__declspec(dllexport) int IntegrateOverTapeCandidate(AMG_TapeRG &candidate_tape,double thk, double &l_val);
		//Print the taping intersections to a rhino file
		__declspec(dllexport) int print(ON_String);
		//Compute total volume from polygons
		__declspec(dllexport) int total_volume_from_polygons(double &val);
		//Compute total volume from tapes
		__declspec(dllexport) int total_volume_from_tapes(double &val);
		//Returns the number of tapes in the current taping
		__declspec(dllexport) int number_of_tapes(){return m_Tapes.Count();};
		//Returns the number of polygonsthk in the current taping
		__declspec(dllexport) int number_of_polysThk(){return m_ThkPolys.Count();};
		//Integrate over candidate tape from tapes (stupid)
		__declspec(dllexport) double IntegrateOverTapeCandidate(AMG_TapeRG &candidate_tape,double thk);
		//Add Tape only to the tape list, does not update the ThkPolys
		__declspec(dllexport) int AddTapeToList(AMG_TapeRG tape){m_Tapes.AppendNew() = tape; return 1;}; 
		//Add tape test3
		__declspec(dllexport) int  AddTapeTest3(AMG_TapeRG p_tape,double thk,double &time,int &cnt);

		

		//default constructor
		AMGThkMatch(){m_power=1;m_ThkPolys.Empty();m_Tapes.Empty();};
		//Second constructor
		AMGThkMatch(ON_ClassArray<AMG_TapeRG> Tapes, ON_ClassArray<AMG_PolygonThk> ThkPolys){m_ThkPolys=ThkPolys;m_Tapes=Tapes;m_power=1;};
		//default deconstructor
		virtual ~AMGThkMatch()
		{
			m_power=1;
#ifdef RENAUD
			m_mask.clear();
			m_mesh.clear();
#endif
			m_ThkPolys.Empty();m_Tapes.Empty();};
		
	protected:
		//Array of tapes
		ON_ClassArray<AMG_TapeRG> m_Tapes;
		//Array of polygons representing the various thicknesses
		ON_ClassArray<AMG_PolygonThk> m_ThkPolys;
		//Surface of thickness from Dove
		ON_NurbsSurface m_surf;
		//Triangulation representing the mesh
#ifdef RENAUD
		Triangulation m_mesh;
		//Mask
		Polygon_2 m_mask;
#endif
		int m_power;
    };


#endif //#define AMGThkMatch_MAY07