// AMGVertex.h: interface for the AMGVertex class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGVERTEX_H__45A6AE94_5D07_4758_A879_0C82A5BD81DD__INCLUDED_)
#define AFX_AMGVERTEX_H__45A6AE94_5D07_4758_A879_0C82A5BD81DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "opennurbs.h"
#include "AMGTapingZone_V3.h"
#include "layerobject.h"
#include "AMGVlink.h"

class AMGPolygon;
class AMGVlink;

#define AMGVERTEX_WALKED	2 

class AMGVertex  : public layerobject
{
	friend class AMGPolygon;
	friend class AMGVlink;
	friend class AMGTapingZone_V3;
public:
	int Absorb(AMGVertex *v2);
	AMGVlink * LinkEndingOn(AMGVertex *v) const;
	int IsOnEdgeOf( const AMGPolygon *p) const; // works from the vv's flags to catch original polyline pts AND intersections.
	int PrintRhino(FILE *fp);
	int Print(FILE *fp)const;
	int InsertBetween(AMGVertex *a,AMGVertex *b);
	int DisConnect(AMGVertex*v);
	AMGVertex();
	AMGVertex(ON_3dPoint p);
	virtual ~AMGVertex();

	ON_3dPoint m_p;
	ON_SimpleArray<AMGVlink*> m_neighbourLinks;
	AMGPolygon * m_owningPoly;
private: 	// permanent members.  There are set at polygon creation time

//	int m_IndexInOwner;

protected:
	int IsConnectedTo(AMGVertex *v)const;
	AMGVertex  * BestNext_AndNot( AMGPolygon *master, AMGPolygon *p_other,
										AMGPolygon *tracepolygon,  const AMGVertex *vlast);
	AMGVertex  * BestNext_Subtraction( AMGPolygon *master, AMGPolygon *p_other,
										AMGPolygon *tracepolygon,  const AMGVertex *vlast);
	AMGVertex  * BestNext_Addition( AMGPolygon *master, AMGPolygon *p_other,
										AMGPolygon *tracepolygon,  const AMGVertex *vlast);
	AMGVertex  * BestNext_Intersection( AMGPolygon *master, AMGPolygon *p_other,
										AMGPolygon *tracepolygon,  const AMGVertex *vlast);
// ephemeral members.  Set and used for intersections and Boolean operations
	int m_IsInt;
	int m_isina;
	int m_isinb;
	int m_IsStrictlyInOther;
	AMGPolygon *m_p1,*m_p2;
	int m_i1, m_i2;
	double m_t1,m_t2;
};


#endif // !defined(AFX_AMGVERTEX_H__45A6AE94_5D07_4758_A879_0C82A5BD81DD__INCLUDED_)
