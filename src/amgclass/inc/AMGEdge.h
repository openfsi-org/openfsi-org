// AMGEdge.h: interface for the AMGEdge class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGEdge_DEC2005
#define AMGEdge_DEC2005

#include "AMGHoopsObject.h"


#ifdef __cplusplus
	#define EXTERN_C extern "C"
#else
	#define EXTERN_C extern
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGEdge : public  AMGHoopsObject
{
public:
	int SetWindow(const double & p_left,  
						   const double & p_right,  
						   const double & p_bottom,  
						   const double & p_top, 
						   const double & p_xmargin, 
						   const double & p_ymargin);
	AMGEdge();
	AMGEdge(const ON_String & p_model,
				   const ON_String & p_name, 
				   const ON_Polyline * p_curvatureMap = NULL, 
				   const int * p_featidptCRV = NULL, 
				   const int * p_featidedge= NULL,
				   const int * p_featidptstart= NULL, 
				   const int * p_featidptend= NULL, 
				   const int * p_featidptyPositive= NULL);
	
	ON_String Serialize(); 
	char* Deserialize(char* p_lp );

	virtual ~AMGEdge();


	int GetOffsetCurve(ON_Polyline &p_pl) const;
	int SetCurvatureCurve(const ON_Polyline &p_pl);
	int GetCurvatureCurve(ON_Polyline &p_pl) const;

	
	int GetFeatIDPtCrv() const ;
	int GetFeatIDEdge() const;
	int GetFeatIDPtYPos() const;
	

#ifdef _AMG2005
	ProError EditEdge();
	int SetOffsetCurve(ON_Polyline p_pl, BOOL p_IsIn3dSpace);
	int SetPosPtYpositive();
#endif
	
};

#endif // AMGEdge_DEC2005
