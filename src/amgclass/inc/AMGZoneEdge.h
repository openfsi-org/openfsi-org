// AMGZoneEdge.h: interface for the AMGZoneEdge class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGZONEEDGE_H__7A9D6D99_F022_499A_BEBC_9CDE6EBFE12B__INCLUDED_)
#define AFX_AMGZONEEDGE_H__7A9D6D99_F022_499A_BEBC_9CDE6EBFE12B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "opennurbs.h"
#include "hc.h"
#include "AMGHoopsObject.h"
#include "layerobject.h" 

class AMGTape;
class AMGZERef;
class AMGZoneNode;
#include "AMGTapingConstants.h"




class AMGZoneEdge : public AMGHoopsObject  ,public layerobject 
{
public:
	int Verify() const;
	AMGZoneEdge * NextRight();
	HC_KEY DrawHoops(const char*seg=0);
	AMGZERef *HookToTape(AMGTape *p_t, int flags);
	AMGTape *HasStartingSeedTape(int *retFlags,AMGZoneNode **ppn1 );
	AMGTape *HasStartingSeedTapeSSD(int *retFlags,AMGZoneNode **ppn1 );
	AMGTape *HasEndingSeedTape( );
	int ComputeShuffle(AMGTape *EndSeedTape,double maxgap);

	void  SetGlue(AMGZERef * p){ m_glue=p;}
	AMGZERef * GetGlue(){ return m_glue;}
	AMGZoneEdge *GetPrev() { return m_prev;}
	AMGZoneEdge *GetNext() { return m_next;}
	void SetPrev(AMGZoneEdge *p) { m_prev=p;}
	void SetNext(AMGZoneEdge *p) { m_next=p;}

	int HasTapes() const;
	int Print(FILE*fp,const int what =AMGPRINT_OBJ|AMGPRINT_RHINO);
	int AddZERef(AMGZERef *theRef, AMGTape* pt);
	ON_Curve * GetZCurve() const;
	void SetZCurve(ON_Curve * p_c);

	AMGZoneEdge();
	AMGZoneEdge(ON_Curve *c);
	virtual ~AMGZoneEdge();

	AMGZERef * m_lastZER;
	AMGZERef * m_firstZER;
	AMGZoneNode * GetZnodeStart() const;
	AMGZoneNode * GetZnodeEnd()const;
	void SetZnodeStart(AMGZoneNode *p_zn);
	void SetZnodeEnd  (AMGZoneNode *p_zn);
	double m_TapeOverlap;
protected:
	int IsSomewhereNear(AMGTape *p_t);

	AMGZERef * m_glue;
	int TapeCount();
	AMGZoneNode * m_ZnodeStart;
	AMGZoneNode * m_ZnodeEnd;
	ON_Curve* m_c;
	ON_Interval *m_dom;
	AMGZoneEdge *m_prev,  *m_next;
private:
	int TapeIsInReverse(AMGTape *pt);
};

#endif // !defined(AFX_AMGZONEEDGE_H__7A9D6D99_F022_499A_BEBC_9CDE6EBFE12B__INCLUDED_)
