// AMGPlotter.h: interface for the AMGPlotter class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGPlotter_MAY2006
#define AMGPlotter_MAY2006

#include "AMGHoopsObject.h"

#ifdef __cplusplus
	#define EXTERN_C extern "C"
#else
	#define EXTERN_C extern
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGPlotter : public  AMGHoopsObject
{
public:
	AMGPlotter();
	AMGPlotter(const ON_String & p_model,
			   const ON_String & p_name);
	
	ON_String Serialize(); 
	char* Deserialize(char* p_lp );

	virtual ~AMGPlotter();

	int SetTableDimensions(const double & p_xmin,
					const double & p_xmax,
					const double & p_ymin,
					const double & p_ymax);

	int GetTableDimensions(double & p_xmin,
						   double & p_xmax,
						   double & p_ymin,
						   double & p_ymax);

};

#endif // AMGPlotter_DEC2005
