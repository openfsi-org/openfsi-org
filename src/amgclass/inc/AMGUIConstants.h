
#ifndef AMGUICONSTANTS_DEC2005
#define AMGUICONSTANTS_DEC2005

#ifndef AMG_PI
#define AMG_PI 3.14159265359
#endif

#ifndef AMG_NOT_SET 
#define AMG_NOT_SET -1
#else
//Constant allready defined
#endif

#ifndef AMG_NO_NAME
#define AMG_NO_NAME "no_name"
#else
Constant allready defined
#endif

#ifndef AMG_SLASH 
#define AMG_SLASH "/"
#else
Constant allready defined
#endif

#ifndef AMG_DOT
#define AMG_DOT "."
#else
Constant allready defined
#endif

#ifndef AMG_TAB 
#define AMG_TAB "\t"
#else
Constant allready defined
#endif


#ifndef AMG_PERCENT
#define AMG_PERCENT "%"
#else
Constant allready defined
#endif




#ifndef AMG_UNDERSCORE 
#define AMG_UNDERSCORE "_"
#else
Constant allready defined
#endif


#ifndef AMG_SPACE_CHAR
#define AMG_SPACE_CHAR " "
#else
Constant allready defined
#endif

#ifndef AMG_PIPE_CHAR
#define AMG_PIPE_CHAR "|"
#else
Constant allready defined
#endif

#ifndef AMG_BACKSLASH 
#define AMG_BACKSLASH "\\"
#else
Constant allready defined
#endif

#ifndef AMG_DOUBLEQUOTE 
#define AMG_DOUBLEQUOTE '"'
#else
Constant allready defined
#endif

#ifndef AMG_SEPARATOR
#define AMG_SEPARATOR "\t"
#else
Constant allready defined
#endif

#ifndef AMG_TERMCHAR
#define AMG_TERMCHAR "\n"  //char(27)
#else
Constant allready defined
#endif


#ifndef TABID_CONTROL 
#define TABID_CONTROL -1
#else
Constant allready defined
#endif

#ifndef TABID_TRIANGULATION 
#define TABID_TRIANGULATION 0
#else
Constant allready defined
#endif

#ifndef TABID_EDGES 
#define TABID_EDGES 1
#else
Constant allready defined
#endif

#ifndef TABID_XSECTIONS 
#define TABID_XSECTIONS 2
#else
Constant allready defined
#endif

#ifndef TABID_BATTENS 
#define TABID_BATTENS 3
#else
Constant allready defined
#endif

#ifndef TABID_MEASURMENT 
#define TABID_MEASURMENT 4
#else
Constant allready defined
#endif

#ifndef TABID_DOVE
#define TABID_DOVE 5
#else
Constant allready defined
#endif

#ifndef TABID_PRESS
#define TABID_PRESS 6
#else
Constant allready defined
#endif

#ifndef TABID_TAPES
#define TABID_TAPES 7
#else
Constant allready defined
#endif

#ifndef TABID_RELAX
#define TABID_RELAX 8
#else
Constant allready defined
#endif

#define RX_MSG_USR_BATTENCHANGE			"RX_MSG_USR_BATTENCHANGE"
#define RX_MSG_USR_BATTENDELETE			"RX_MSG_USR_BATTENDELETE"
#define RX_MSG_USR_BATTENINSERT			"RX_MSG_USR_BATTENINSERT"
#define RX_MSG_USR_CLOSE			"RX_MSG_USR_CLOSE"

#define RX_MSG_USR_DOVECHANGE			"RX_MSG_USR_DOVECHANGE"
#define RX_MSG_USR_DOVELOAD					"RX_MSG_USR_DOVELOAD"
#define RX_MSG_USR_DOVESAVE					"RX_MSG_USR_DOVESAVE"
#define RX_MSG_USR_DOVESAVEAS				"RX_MSG_USR_DOVESAVEAS"

#define RX_MSG_USR_EDGECHANGE			"RX_MSG_USR_EDGECHANGE"
#define RX_MSG_USR_NEWGEN			"RX_MSG_USR_NEWGEN"
#define RX_MSG_USR_NEW_MAIN			"RX_MSG_USR_NEW_MAIN"
#define RX_MSG_USR_OPENGENOA			"RX_MSG_USR_OPENGENOA"
#define RX_MSG_USR_OPENMAIN			"RX_MSG_USR_OPENMAIN"
#define RX_MSG_USR_OPEN			"RX_MSG_USR_OPEN"
#define RX_MSG_USR_PRESSCHANGE			"RX_MSG_USR_PRESSCHANGE"
#define RX_MSG_USR_PRESSDELETE			"RX_MSG_USR_PRESSDELETE"
#define RX_MSG_USR_PRESSINSERT			"RX_MSG_USR_PRESSINSERT"
#define RX_MSG_USR_SAVE			"RX_MSG_USR_SAVE"
#define RX_MSG_USR_TRICHANGE			"RX_MSG_USR_TRICHANGE"
#define RX_MSG_USR_TRY_TO_CONNECT			"RX_MSG_USR_TRY_TO_CONNECT"
#define RX_MSG_USR_XSECTIONCHANGE			"RX_MSG_USR_XSECTIONCHANGE"
#define RX_MSG_USR_XSECTIONDELETE			"RX_MSG_USR_XSECTIONDELETE"
#define RX_MSG_USR_XSECTIONINSERT			"RX_MSG_USR_XSECTIONINSERT"
					
#define RX_MSG_SRV_BATTENCHANGED			"RX_MSG_SRV_BATTENCHANGED"
#define RX_MSG_SRV_BATTENDELETED			"RX_MSG_SRV_BATTENDELETED"
#define RX_MSG_SRV_BATTENINSERTED			"RX_MSG_SRV_BATTENINSERTED"
#define RX_MSG_SRV_CLOSED			"RX_MSG_SRV_CLOSED"
#define RX_MSG_SRV_DOVECHANGED			"RX_MSG_SRV_DOVECHANGED"
#define RX_MSG_SRV_EDGECHANGED			"RX_MSG_SRV_EDGECHANGED"
#define RX_MSG_SRV_AFTER_NEWGEN			"RX_MSG_SRV_AFTER_NEWGEN"
#define RX_MSG_SRV_AFTER_NEWMAIN			"RX_MSG_SRV_AFTER_NEWMAIN"
#define RX_MSG_SRV_AFTER_OPENGENOA			"RX_MSG_SRV_AFTER_OPENGENOA"
#define RX_MSG_SRV_AFTER_OPENMAIN			"RX_MSG_SRV_AFTER_OPENMAIN"
#define RX_MSG_SRV_AFTER_OPEN			"RX_MSG_SRV_AFTER_OPEN"
#define RX_MSG_SRV_PRESSCHANGED			"RX_MSG_SRV_PRESSCHANGED"
#define RX_MSG_SRV_PRESSDELETED			"RX_MSG_SRV_PRESSDELETED"
#define RX_MSG_SRV_PRESSINSERTED			"RX_MSG_SRV_PRESSINSERTED"
#define RX_MSG_SRV_AFTER_SAVE			"RX_MSG_SRV_AFTER_SAVE"
#define RX_MSG_SRV_TRICHANGED			"RX_MSG_SRV_TRICHANGED"
#define RX_MSG_SRV_SERVER_CONNECTED			"RX_MSG_SRV_SERVER_CONNECTED"
#define RX_MSG_SRV_XSECTIONCHANGED			"RX_MSG_SRV_XSECTIONCHANGED"
#define RX_MSG_SRV_XSECTIONDELETED			"RX_MSG_SRV_XSECTIONDELETED"
#define RX_MSG_SRV_XSECTIONINSERTED			"RX_MSG_SRV_XSECTIONINSERTED"

#define RX_MSG_SRV_RELAXRUNCOMPLETED			"RX_MSG_SRV_RELAXRUNCOMPLETED"

#define RX_MSG_SRV_CRVCHANGED "RX_MSG_SRV_CRVCHANGED"

#define RX_MSG_CONTROLPLOTTER_NEW_LINE_IN_JOURNAL "RX_MSG_CONTROLPLOTTER_NEW_LINE_IN_JOURNAL"

#ifndef AMG_SEG_EDITABLE 
#define AMG_SEG_EDITABLE "editable"
#else
	key word allready defined
#endif

#ifndef AMG_SEG_INTERNAL 
#define AMG_SEG_INTERNAL "internal"
#else
	key word allready defined
#endif
	
#ifndef AMG_SEG_OUTPUT 
#define AMG_SEG_OUTPUT "output"
#else
	key word allready defined
#endif

#ifndef AMG_2DTACK
#define AMG_2DTACK "tack2d"
#else
	key word allready defined
#endif

#ifndef AMG_2DCLEW
#define AMG_2DCLEW "clew2d"
#else
	key word allready defined
#endif

#ifndef AMG_2DHEADFWD
#define AMG_2DHEADFWD "headfwd2d"
#else
	key word allready defined
#endif

#ifndef AMG_2DHEADAFT
#define AMG_2DHEADAFT "headaft2d"
#else
	key word allready defined
#endif


#ifndef AMG_EDGE_START
#define AMG_EDGE_START "edgestart"
#else
	key word allready defined
#endif

#ifndef AMG_EDGE_END
#define AMG_EDGE_END "edgeend"
#else
	key word allready defined
#endif

#ifndef AMG_EDGE_YINSIDE
#define AMG_EDGE_YINSIDE "edgeyinside"
#else
	key word allready defined
#endif

#ifndef AMG_SEG_WINDOWS
#define AMG_SEG_WINDOWS "windows"
#else
	key word allready defined
#endif

#ifndef AMG_SEG_NORTH
#define AMG_SEG_NORTH "north"
#else
	key word allready defined
#endif
#ifndef AMG_SEG_SOUTH
#define AMG_SEG_SOUTH "south"
#else
	key word allready defined
#endif
#ifndef AMG_SEG_EAST
#define AMG_SEG_EAST "east"
#else
	key word allready defined
#endif
#ifndef AMG_SEG_WEST
#define AMG_SEG_WEST "west"
#else
	key word allready defined
#endif
	
#define AMG_MARKS "marks"
#define AMG_MARKS_LENS "marks_lens"
#define AMG_MARKS_FLATPANEL_BTM "marks_flatpanel_btm"
#define AMG_MARKS_FLATPANEL_TOP "marks_flatpanel_top"

	


#define AMG_LINE "line"
	
#ifndef AMG_REL_POS 
#define AMG_REL_POS "rel_pos"
#else
	key word allready defined
#endif

#ifndef AMG_REL_POS_LEECH
#define AMG_REL_POS_LEECH "rel_pos_leech"
#else
	key word allready defined
#endif

#ifndef AMG_P 
#define AMG_P "p"
#else
	key word allready defined
#endif

#ifndef AMG_E 
#define AMG_E "e"
#else
	key word allready defined
#endif

#ifndef AMG_I 
#define AMG_I "i"
#else
	key word allready defined
#endif

#ifndef AMG_J 
#define AMG_J "j"
#else
	key word allready defined
#endif

#ifndef AMG_HEADW 
#define AMG_HEADW "headw"
#else
	key word allready defined
#endif

#ifndef AMG_TWISTHEAD 
#define AMG_TWISTHEAD "twisthead"
#else
	key word allready defined
#endif

#ifndef AMG_HEADSLOPE
#define AMG_HEADSLOPE "headslope"
#else
	key word allready defined
#endif

#ifndef AMG_RAKE
#define AMG_RAKE "rake"
#else
	key word allready defined
#endif

#ifndef AMG_DIAGONAL
#define AMG_DIAGONAL "diagonal"
#else
	key word allready defined
#endif

#ifndef AMG_2DLEECHLENGTH
#define AMG_2DLEECHLENGTH "leechlength2d"
#else
	key word allready defined
#endif
	
	
#ifndef AMG_POSONLEECH 
#define AMG_POSONLEECH "pos_on_leech"
#else
	key word allready defined
#endif

#ifndef AMG_POSONLUFF
#define AMG_POSONLUFF "pos_on_luff"
#else
	key word allready defined
#endif


#ifndef AMG_ORIENTATION 
#define AMG_ORIENTATION "orientation"
#else
	key word allready defined
#endif

#ifndef AMG_ISFULLLENGTH 
#define AMG_ISFULLLENGTH "is_full_length"
#else
	key word allready defined
#endif

#ifndef AMG_LENGTH
#define AMG_LENGTH "length"
#else
	key word allready defined
#endif

#ifndef AMG_ISONPRESS
#define AMG_ISONPRESS "is_on_press"
#else
	key word allready defined
#endif

#ifndef AMG_POCKET_WIDTH
#define AMG_POCKET_WIDTH "pocket_width"
#else
	key word allready defined
#endif

#ifndef AMG_RELAXBATTENREF
#define AMG_RELAXBATTENREF "relax_battenref"
#else
	key word allready defined
#endif

	

#ifndef AMG_PRESSNAME
#define AMG_PRESSNAME "press_name"
#else
	key word allready defined
#endif
	
#ifndef AMG_LISTPANELS
#define AMG_LISTPANELS "list_panels"
#else
	key word allready defined
#endif
	
	

#ifndef AMG_CORD
#define AMG_CORD "cord"
#else
	key word allready defined
#endif

#ifndef AMG_DEPTH
#define AMG_DEPTH "depth"
#else
	key word allready defined
#endif

#ifndef AMG_TWIST
#define AMG_TWIST "twist"
#else
	key word allready defined
#endif


#ifndef AMG_DRAFTPOS
#define AMG_DRAFTPOS "draftpos"
#else
	key word allready defined
#endif

#ifndef AMG_DRAFTFWD
#define AMG_DRAFTFWD "draftfwd"
#else
	key word allready defined
#endif

#ifndef AMG_DRAFTAFT
#define AMG_DRAFTAFT "draftaft"
#else
	key word allready defined
#endif

#ifndef AMG_ENTRYANGLE
#define AMG_ENTRYANGLE "entryangle"
#else
	key word allready defined
#endif

#ifndef AMG_EXITANGLE
#define AMG_EXITANGLE "exitangle"
#else
	key word allready defined
#endif

#ifndef AMG_CURVATUREMAP 
#define AMG_CURVATUREMAP "curvature_map"
#else
	key word allready defined
#endif

#ifndef AMG_OFFSETS
#define AMG_OFFSETS "offsets"
#else
	key word allready defined
#endif

#ifndef AMG_DSHOULDERS
#define AMG_DSHOULDERS "dshoulders"
#else
	key word allready defined
#endif

#ifndef AMG_DWARM
#define AMG_DWARM "dwarm"
#else
	key word allready defined
#endif

#ifndef AMG_PIED
#define AMG_PIED "pied"
#else
	key word allready defined
#endif

#ifndef AMG_DREFONPANEL
#define AMG_DREFONPANEL "drefonpanel"
#else
	key word allready defined
#endif

#ifndef AMG_DIST_STATION
#define AMG_DIST_STATION "dist_station"
#else
	key word allready defined
#endif

#ifndef AMG_DIST_QC
#define AMG_DIST_QC "dist_qc"
#else
	key word allready defined
#endif
	
#ifndef AMG_PRESS_LENGTH
#define AMG_PRESS_LENGTH "press_length"
#else
	key word allready defined
#endif
	
#ifndef AMG_BROADSEAM
#define AMG_BROADSEAM "broadseam"
#else
	key word allready defined
#endif

#ifndef AMG_DELTAFORMINGGBATTEN
#define AMG_DELTAFORMINGGBATTEN "deltaformingbatten"
#else
	key word allready defined
#endif

	

#ifndef AMG_FEATID_PRESS 
#define AMG_FEATID_PRESS "featid_press"
#else
	key word allready defined
#endif

#ifndef AMG_FORMINGBATTEN
#define AMG_FORMINGBATTEN "formingbatten"
#else
	key word allready defined
#endif

#ifndef AMG_PRESSMARKS
#define AMG_PRESSMARKS "marks"
#else
	key word allready defined
#endif
	
#ifndef AMG_FEATIDTACK
#define AMG_FEATIDTACK "featid_tack"
#else
	key word allready defined
#endif

#ifndef AMG_FEATIDCLEW
#define AMG_FEATIDCLEW "featid_clew"
#else
	key word allready defined
#endif
	
#ifndef AMG_FEATIDHEADFWD
#define AMG_FEATIDHEADFWD "featid_headfwd"
#else
	key word allready defined
#endif
	
#ifndef AMG_FEATIDHEADAFT
#define AMG_FEATIDHEADAFT "featid_headaft"
#else
	key word allready defined
#endif
	
#ifndef AMG_FEATID_PTCRV
#define AMG_FEATID_PTCRV "feat_ptcrv"
#else
	key word allready defined
#endif

#ifndef AMG_FEATID_PTSTART
#define AMG_FEATID_PTSTART "feat_ptstart"
#else
	key word allready defined
#endif

#ifndef AMG_FEATID_PTEND
#define AMG_FEATID_PTEND "feat_ptend"
#else
	key word allready defined
#endif

#ifndef AMG_FEATID_PTYPOSITIVE
#define AMG_FEATID_PTYPOSITIVE "feat_ptypositive"
#else
	key word allready defined
#endif

#ifndef AMG_FEATID_EDGE
#define AMG_FEATID_EDGE "feat_edge"
#else
	key word allready defined
#endif
	
#ifndef AMG_OFFSET_CURVE
#define AMG_OFFSET_CURVE "offset_curve"
#else
	key word allready defined
#endif

	
#ifndef AMG_IMPORTED_CURVE
#define AMG_IMPORTED_CURVE "imported_curve"
#else
	key word allready defined
#endif
	
	
#ifndef AMG_LUFF
	#define AMG_LUFF "luff"
#else
	key word allready defined
#endif

	
#ifndef AMG_2DLUFF
	#define AMG_2DLUFF "edge_luff"
#else
	key word allready defined
#endif

#ifndef AMG_2DLEECH
	#define AMG_2DLEECH "edge_leech"
#else
	key word allready defined
#endif

#ifndef AMG_2DFOOT
	#define AMG_2DFOOT "edge_foot"
#else
	key word allready defined
#endif

#ifndef AMG_2DHEAD
	#define AMG_2DHEAD "edge_head"
#else
	key word allready defined
#endif

#ifndef AMG_LEECH
	#define AMG_LEECH "leech"
#else
	key word allready defined
#endif

#ifndef AMG_LEECH_HEADFWD
	#define AMG_LEECH_HEADFWD "leech_headfwd"
#else
	key word allready defined
#endif

	

#ifndef AMG_FOOT
	#define AMG_FOOT "foot"
#else
	key word allready defined
#endif

#ifndef AMG_HEAD
	#define AMG_HEAD "head"
#else
	key word allready defined
#endif

#ifndef AMG_LP
	#define AMG_LP "lp"
#else
	key word allready defined
#endif
	
#ifndef AMG_VERTCRV
	#define AMG_VERTCRV "vertcrv"
#else
	key word allready defined
#endif

#ifndef AMG_FEATID_LEECH 
#define AMG_FEATID_LEECH "featid_leech"
#else
	key word allready defined
#endif

#ifndef AMG_FEATID_LUFF
#define AMG_FEATID_LUFF "featid_luff"
#else
	key word allready defined
#endif
	
#ifndef AMG_FEATID_VERTCRV
#define AMG_FEATID_VERTCRV "featid_vertcrv"
#else
	key word allready defined
#endif
	
#ifndef AMG_FEATID_REFPLANE
#define AMG_FEATID_REFPLANE "featid_refplane"
#else
	key word allready defined
#endif

#ifndef AMG_FEATID_CSYS
#define AMG_FEATID_CSYS "featid_csys"
#else
	key word allready defined
#endif
	
#ifndef AMG_FEATID_PTONLUFF
#define AMG_FEATID_PTONLUFF "featid_ptonluff"
#else
	key word allready defined
#endif
	
#ifndef AMG_FEATID_SECTION
#define AMG_FEATID_SECTION "featid_section"
#else
	key word allready defined
#endif

#ifndef AMG_FEATID_BATTEN
#define AMG_FEATID_BATTEN "featid_batten"
#else
	key word allready defined
#endif

#ifndef AMG_FEATID_PTONLEECH
#define AMG_FEATID_PTONLEECH "featid_ptonleech"
#else
	key word allready defined
#endif 

#ifndef AMG_FEATID_PLANEORIENT
#define AMG_FEATID_PLANEORIENT "featid_planeorient"
#else
	key word allready defined
#endif 

#ifndef AMG_FEATID_PTLENGTH
#define AMG_FEATID_PTLENGTH "featid_ptlength"
#else
	key word allready defined
#endif 

#ifndef AMG_DOC_INDEX
#define AMG_DOC_INDEX 1
#else
	key word allready defined
#endif
	
#ifndef AMG_TRIANGULATIONFORM_INDEX
#define AMG_TRIANGULATIONFORM_INDEX 2
#else
	key word allready defined
#endif

#ifndef AMG_EDGEFORM_INDEX
#define AMG_EDGEFORM_INDEX 3
#else
	key word allready defined
#endif

#ifndef AMG_XSECTIONFORM_INDEX
#define AMG_XSECTIONFORM_INDEX 4
#else
	key word allready defined
#endif

#ifndef AMG_BATTENFORM_INDEX
#define AMG_BATTENFORM_INDEX 5
#else
	key word allready defined
#endif

#ifndef AMG_MEASURMENTFORM_INDEX
#define AMG_MEASURMENTFORM_INDEX 6
#else
	key word allready defined
#endif


#ifndef AMG_DOVEFORM_INDEX
#define AMG_DOVEFORM_INDEX 544
#else
	key word allready defined
#endif
	
#ifndef AMG_PRESSFORM_INDEX
#define AMG_PRESSFORM_INDEX 445
#else
	key word allready defined
#endif

#ifndef AMG_TAPESFORM_INDEX
#define AMG_TAPESFORM_INDEX 446
#else
	key word allready defined
#endif

#ifndef AMG_RELAXFORM_INDEX
#define AMG_RELAXFORM_INDEX 447
#else
	key word allready defined
#endif

	
#ifndef AMG_NESTINGFORM_INDEX
#define AMG_NESTINGFORM_INDEX 448
#else
	key word allready defined
#endif


#ifndef AMG_HOOP_ADMIN
#define AMG_HOOP_ADMIN "Admin"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_PROECLIENT
#define AMG_HOOP_PROECLIENT "ProE"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_TRIANGULATION
#define AMG_HOOP_TRIANGULATION "Triangulation"
#else
	key word allready defined
#endif

	
#ifndef AMG_HOOP_2DEDGES
#define AMG_HOOP_2DEDGES "2dedges"
#else
	key word allready defined
#endif


#ifndef AMG_HOOP_EDGES
#define AMG_HOOP_EDGES "edges"
#else
	key word allready defined
#endif
	
#ifndef AMG_HOOP_STRIPES
#define AMG_HOOP_STRIPES "stripes"
#else
	key word allready defined
#endif
	

#ifndef AMG_HOOP_XSECTIONS
#define AMG_HOOP_XSECTIONS "xsections"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_BATTENS
#define AMG_HOOP_BATTENS "battens"
#else
	key word allready defined
#endif

	
#ifndef AMG_HOOP_MEASURMENT
#define AMG_HOOP_MEASURMENT "measurment"
#else
	key word allready defined
#endif
	

#ifndef AMG_HOOP_DOVE
#define AMG_HOOP_DOVE "dove"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_MODULUSMAP
#define AMG_HOOP_MODULUSMAP "modulusmap"
#else
	key word allready defined
#endif	

#ifndef AMG_HOOP_PANELS
#define AMG_HOOP_PANELS "panels"
#else
	key word allready defined
#endif


#ifndef AMG_HOOP_PRESSES
#define AMG_HOOP_PRESSES "presses"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_TAPES
#define AMG_HOOP_TAPES "tapes"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_RELAX
#define AMG_HOOP_RELAX "relax"
#else
	key word allready defined
#endif
	
#ifndef AMG_HOOP_NESTINGS
#define AMG_HOOP_NESTINGS "nestings"
#else
	key word allready defined
#endif

	
#ifndef AMG_HOOP_LAYERLIST
#define AMG_HOOP_LAYERLIST "layerlist"
#else
	key word allready defined
#endif

	
#ifndef AMG_HOOP_ROSETTES
#define AMG_HOOP_ROSETTES "rosettes"
#else
	key word allready defined
#endif
	
#ifndef AMG_HOOP_REINFORCMENTLIB
#define AMG_HOOP_REINFORCMENTLIB "reinforcmentlibrary"
#else
	key word allready defined
#endif

	
#ifndef AMG_HOOP_YARNLIB
#define AMG_HOOP_YARNLIB "yarnlibrary"
#else
	key word allready defined
#endif

	
		
#ifndef AMG_HOOP_ROSETTE_STDB
#define AMG_HOOP_ROSETTE_STDB "rosette_stbd"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_ROSETTE_PORT
#define AMG_HOOP_ROSETTE_PORT "rosette_port"
#else
	key word allready defined
#endif

	
	
#ifndef AMG_HOOP_TABLE
#define AMG_HOOP_TABLE "Table"
#else
	key word allready defined
#endif
	
	
#ifndef AMG_HOOP_MARKS
#define AMG_HOOP_MARKS "marks"
#else
	key word allready defined
#endif

	
#ifndef AMG_HOOP_GEOMETRY
#define AMG_HOOP_GEOMETRY "geometry"
#else
	key word allready defined
#endif

	
#ifndef AMG_HOOP_SEGMENT
#define AMG_HOOP_SEGMENT "seg"
#else
	key word allready defined
#endif

	
#ifndef AMG_HOOP_PLY_NAME
#define AMG_HOOP_PLY_NAME "ply_name"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_PANEL_NAME
#define AMG_HOOP_PANEL_NAME "panel_name"
#else
	key word allready defined
#endif
	
#ifndef AMG_MARK_PATCHEDGECONNECTION
#define AMG_MARK_PATCHEDGECONNECTION "mark_patchedgeconnection"
#else
	key word allready defined
#endif

	
#ifndef AMG_MARK_REINFORCMENT
#define AMG_MARK_REINFORCMENT "mark_reinforcment"
#else
	key word allready defined
#endif

#ifndef AMG_MARK_EDGES
#define AMG_MARK_EDGES AMG_MARK_PATCHEDGECONNECTION
#else
	key word allready defined
#endif

#ifndef AMG_MARK_BATTENPOCKET
#define AMG_MARK_BATTENPOCKET "mark_battenpocket"
#else
	key word allready defined
#endif
	
#ifndef AMG_MARK_CONNECTIONS
#define AMG_MARK_CONNECTIONS AMG_MARK_PATCHEDGECONNECTION
#else
	key word allready defined
#endif

	

	
	

#ifndef AMG_HOOP_ONEEDGE
#define AMG_HOOP_ONEEDGE "edge_"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_ONEXSECTION
#define AMG_HOOP_ONEXSECTION "section_"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_ONEBATTEN
#define AMG_HOOP_ONEBATTEN "batten_"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_ONEPRESS
#define AMG_HOOP_ONEPRESS "press_"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_ONEPANEL
#define AMG_HOOP_ONEPANEL "panel_"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_ONEPLY
#define AMG_HOOP_ONEPLY "ply_"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_ONELEVEL
#define AMG_HOOP_ONELEVEL "level_"
#else
	key word allready defined
#endif

	

#ifndef AMG_HOOP_ONETAPE
#define AMG_HOOP_ONETAPE "tape_"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_ONEROSETTE
#define AMG_HOOP_ONEROSETTE "rosette_"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_ONETRAPEZE
#define AMG_HOOP_ONETRAPEZE "trapeze_"
#else
	key word allready defined
#endif
	
#ifndef AMG_HOOP_ONELONGI
#define AMG_HOOP_ONELONGI "longi_"
#else
	key word allready defined
#endif
	

#ifndef AMG_HOOP_ONENESTING
#define AMG_HOOP_ONENESTING "nesting_"
#else
	key word allready defined
#endif

#ifndef AMG_DOVE_DOLDRUM
	#define AMG_DOVE_DOLDRUM "doldrum"
#else
	key word allready defined
#endif


#ifndef AMG_DOVESRF_THICKNESS
	#define AMG_DOVESRF_THICKNESS "thickness_surface"
#else
	key word allready defined
#endif


#ifndef AMG_DOVESRF_UPDIR
#define AMG_DOVESRF_UPDIR "updir_surface"
#else
	key word allready defined
#endif

#ifndef AMG_DOVESRF_POISSON
	#define AMG_DOVESRF_POISSON "poisson_surface"
#else
	key word allready defined
#endif

#ifndef AMG_DOVESRF_GYX
	#define AMG_DOVESRF_GYX "gxy_surface"
#else
	key word allready defined
#endif

#ifndef AMG_DOVESRF_EXTRA
	#define AMG_DOVESRF_EXTRA "doveextra_surface"
#else
	key word allready defined
#endif


#ifndef RXD_MATH_DOVE
	#define RXD_MATH_DOVE			4 
#else 
	Constant allready defined
#endif 

#ifndef RXD_USE_LAYERTST		
#define RXD_USE_LAYERTST		8 // means get alpha from interpolating the layertst grid
#else 
	Constant allready defined
#endif 

#ifndef RXD_USE_DOVESURFACES	
#define RXD_USE_DOVESURFACES	16 // means get the matrix from evaluating the 6 surfaces
#else 
	Constant allready defined
#endif 
#ifndef RXD_USE_LAYERSURFACES
#define RXD_USE_LAYERSURFACES	32 // means use dovestructurebylayersurfs
#else 
	Constant allready defined
#endif 

#ifndef AMG_YARN_GROUPS
#define AMG_YARN_GROUPS "yarngroups"
#else
	key word allready defined
#endif

#ifndef AMG_YARN_2D
#define AMG_YARN_2D "yarn2d"
#else
	key word allready defined
#endif

	
#ifndef AMG_PLY_NAME
#define AMG_PLY_NAME "plyname"
#else
	key word allready defined
#endif

#ifndef AMG_DEFAULT_PLY
#define AMG_DEFAULT_PLY "defaultply"
#else
	key word allready defined
#endif

#ifndef AMG_FILENAME
#define AMG_FILENAME "filename"
#else
	key word allready defined
#endif

	
#ifndef AMG_LINEAR	
#define AMG_LINEAR	 1
#else
	key word allready defined
#endif

#ifndef AMG_QUARTERGIRTH
#define AMG_QUARTERGIRTH "quarter_girth"
#else
	key word allready defined
#endif

	
#ifndef AMG_MIDGIRTH
#define AMG_MIDGIRTH "mid_girth"
#else
	key word allready defined
#endif
	
#ifndef AMG_THREEQUARTERGIRTH
#define AMG_THREEQUARTERGIRTH "threequarter_girth"
#else
	key word allready defined
#endif
	
#ifndef AMG_SEVENEIGHTGIRTH
#define AMG_SEVENEIGHTGIRTH "seveneight_girth"
#else
	key word allready defined
#endif

#ifndef AMG_MEASUREDLEECH
#define AMG_MEASUREDLEECH "measured_leech"
#else
	key word allready defined
#endif

#ifndef AMG_FORMAT_MM
#define AMG_FORMAT_MM "%f"  //"%1.1f"
#else
	key word allready defined
#endif

#ifndef AMG_FORMAT_2DECI
#define AMG_FORMAT_2DECI "%1.2f"
#else
	key word allready defined
#endif

#ifndef AMG_FORMAT_3DECI
#define AMG_FORMAT_3DECI "%1.3f"
#else
	key word allready defined
#endif

#ifndef AMG_FORMAT_POINT 
//#define AMG_FORMAT_POINT "%1.9f\t%1.9f\t%1.9f\t"  //"%15.12g\t%15.12g\t%15.12g\n"
#define AMG_FORMAT_POINT "%f\t%f\t%f\t"  //"%15.12g\t%15.12g\t%15.12g\n"
#else
	key word allready defined
#endif
	
#ifndef AMGPTK_FORMAT_POINT 
#define AMGPTK_FORMAT_POINT "%1.9f\t%1.9f\t%1.9f\n"  //"%15.12g\t%15.12g\t%15.12g\n"
#else
	key word allready defined
#endif

	
#ifndef AMG_SAIL_TYPE
#define AMG_SAIL_TYPE "sail_type"
#else
	key word allready defined
#endif

#ifndef AMG_SAIL_TYPE_MAINSAIL
#define AMG_SAIL_TYPE_MAINSAIL "main"
#else
	key word allready defined
#endif

#ifndef AMG_SAIL_TYPE_GENOA
#define AMG_SAIL_TYPE_GENOA "genoa"
#else
	key word allready defined
#endif

#ifndef AMG_TAPE_NBPLIES
#define AMG_TAPE_NBPLIES "nb_plies"
#else
	key word allready defined
#endif

#ifndef AMG_TAPE_CRTPLYID
#define AMG_TAPE_CRTPLYID "current_ply_id"
#else
	key word allready defined
#endif

#ifndef AMG_TAPE_CRTPANELID
#define AMG_TAPE_CRTPANELID "current_panel_id"
#else
	key word allready defined
#endif

#ifndef AMG_TAPE_CRTPLYNAME
#define AMG_TAPE_CRTPLYNAME "current_ply_name"
#else
	key word allready defined
#endif

#ifndef AMG_TAPE_CRTPANELNAME
#define AMG_TAPE_CRTPANELNAME "current_panel_name"
#else
	key word allready defined
#endif
	
#ifndef AMG_TAPE_MAXDEVIATION
#define AMG_TAPE_MAXDEVIATION "max_deviation"
#else
	key word allready defined
#endif
#ifndef AMG_TAPE_WIDTH 
#define AMG_TAPE_WIDTH "tape_width"
#else
	key word allready defined
#endif
#ifndef AMG_TAPE_LATOVERLAP 
#define AMG_TAPE_LATOVERLAP "lat_overlap"
#else
	key word allready defined
#endif
#ifndef AMG_TAPE_BUTTOVERLAP
#define AMG_TAPE_BUTTOVERLAP "butt_overlap"
#else
	key word allready defined
#endif
#ifndef AMG_TAPE_MAXLENGTH
#define AMG_TAPE_MAXLENGTH "max_length"
#else
	key word allready defined
#endif
#ifndef AMG_TAPE_MINLENGTH
	#define AMG_TAPE_MINLENGTH "min_length"
#else
	key word allready defined
#endif
#ifndef AMG_TAPE_DALPHA
	#define AMG_TAPE_DALPHA "d_alpha"
#else
	key word allready defined
#endif

#ifndef AMG_TAPE_MAXGAP
	#define AMG_TAPE_MAXGAP "max_gap"
#else
	key word allready defined
#endif

	
#ifndef AMG_ROSETTE_NBTAPE
	#define AMG_ROSETTE_NBTAPE "nbtape"
#else
	key word allready defined
#endif

#ifndef AMG_ROSETTE_ARC
	#define AMG_ROSETTE_ARC "arc"
#else
	key word allready defined
#endif

#ifndef AMG_ROSETTE_MULTIPLICITY
#define AMG_ROSETTE_MULTIPLICITY "multiplicity"
	#else
	key word allready defined
#endif
	
#ifndef AMG_ROSETTE_RADIUS
#define AMG_ROSETTE_RADIUS "radius"
	#else
	key word allready defined
#endif

#ifndef AMG_HOOP_TAPEREFERENCES 
#define AMG_HOOP_TAPEREFERENCES "tapereferences"
#else
	key word allready defined
#endif

	
#ifndef AMG_HOOP_TRAPEZE
#define AMG_HOOP_TRAPEZE "trapeze"
#else
	key word allready defined
#endif
	
#ifndef AMG_TRAPEZE_LENNGTHORG
#define AMG_TRAPEZE_LENNGTHORG "trapezelengthorg"
#else
	key word allready defined
#endif

#ifndef AMG_TRAPEZE_LENNGTHEXT
#define AMG_TRAPEZE_LENNGTHEXT "trapezelengthext"
#else
	key word allready defined
#endif
	
#ifndef AMG_TRAPEZE_THKORG 
#define AMG_TRAPEZE_THKORG "trapezethkorg"
#else
	key word allready defined
#endif
	
#ifndef AMG_TRAPEZE_THKEXT 
#define AMG_TRAPEZE_THKEXT "trapezethkext"
#else
	key word allready defined
#endif
	
#ifndef AMG_TRAPEZE_SPAN 
#define AMG_TRAPEZE_SPAN "trapezespan"
#else
	key word allready defined
#endif
	
#ifndef AMG_HOOP_ONETAPEREF
#define AMG_HOOP_ONETAPEREF "taperef_"
#else
	key word allready defined
#endif

	
#ifndef AMG_TAPEREF
#define AMG_TAPEREF "tapereference"
#else
	key word allready defined
#endif
	
#ifndef AMG_FIBER
#define AMG_FIBER "fiber"
	#else
	key word allready defined
#endif

#ifndef AMG_GLUE
#define AMG_GLUE "glue"
	#else
	key word allready defined
#endif

#ifndef AMG_EXTRA
#define AMG_EXTRA "extra"
	#else
	key word allready defined
#endif

#ifndef AMG_LIM_LENS_SUP
#define AMG_LIM_LENS_SUP "limitlenssup"
	#else
	key word allready defined
#endif

#ifndef AMG_LIM_LENS_INF
#define AMG_LIM_LENS_INF "limitlensinf"
	#else
	key word allready defined
#endif

#ifndef AMG_LIM_PANEL_SUP
#define AMG_LIM_PANEL_SUP "limitPanelsup"
	#else
	key word allready defined
#endif

#ifndef AMG_LIM_PANEL_INF
#define AMG_LIM_PANEL_INF "limitPanelinf"
	#else
	key word allready defined
#endif

#ifndef  RLX_DEFAULT_UNITS
#if defined (linux) || defined(PETERSMSW) 
		#define RLX_DEFAULT_UNITS  ON::meters
#else
		#define RLX_DEFAULT_UNITS  ON::no_unit_system
		#pragma message(" ----- RLX_DEFAULT_UNITS set to ON::no_unit_system")
#endif
#else
#pragma message("(  )AMGUIConstant.h) ----- RLX_DEFAULT_UNITS already defined")
#endif
 
	
#ifndef AMG_HOOP_BLANK
#define AMG_HOOP_BLANK "blank"
#else
	key word allready defined
#endif

#ifndef AMG_MARKS_GRID
#define AMG_MARKS_GRID "mark_grid"
#else
	key word allready defined
#endif

	
#ifndef AMG_STACK_COLOR_ID
#define AMG_STACK_COLOR_ID "stack_color_id"
#else
	key word allready defined
#endif

#ifndef AMG_STACK_LEVEL
#define AMG_STACK_LEVEL "stack_level"
#else
	key word allready defined
#endif

	

#ifndef AMG_MATERIAL_CODE
#define AMG_MATERIAL_CODE "material_code"
#else
	key word allready defined
#endif

	
#ifndef AMG_TAPE_PANEL
#define AMG_TAPE_PANEL "panel_name"
#else
	key word allready defined
#endif


#ifndef AMG_PLY_CONTOUR_ID
#define AMG_PLY_CONTOUR_ID "contour_id"
#else
	key word allready defined
#endif

	

#ifndef AMG_TAPE_THICKNESS_FACTOR
#define AMG_TAPE_THICKNESS_FACTOR "tape_thickness"
#else
	key word allready defined
#endif
	
#ifndef AMG_TAPE_LINEAR_MASS
#define AMG_TAPE_LINEAR_MASS "tape_linear_mass"
#else
	key word allready defined
#endif

#ifndef AMG_TAPE_NB_YARNS
#define AMG_TAPE_NB_YARNS "tape_nb_yarns"
#else
	key word allready defined
#endif

	
#ifndef AMG_YARN_TYPE
#define AMG_YARN_TYPE "yarn_type"
#else
	key word allready defined
#endif
	

	
	
#ifndef AMG_CVS
#define AMG_CVS "cvs"
#else
	key word allready defined
#endif

	

	
#ifndef AMG_TAPINGSPEED_NOTSET
#define AMG_TAPINGSPEED_NOTSET -1
#endif
#ifndef AMG_TAPINGSPEED_EXTERN_STARBOARD
#define AMG_TAPINGSPEED_EXTERN_STARBOARD 1
#endif
#ifndef AMG_TAPINGSPEED_INTERN
#define AMG_TAPINGSPEED_INTERN 2
#endif

#ifndef AMG_TAPINGSPEED_EXTERN_STARBOARD_FLAT
#define AMG_TAPINGSPEED_EXTERN_STARBOARD_FLAT 3
#endif

#ifndef AMG_TAPINGSPEED_EXTERN_STARBOARD_LENS
#define AMG_TAPINGSPEED_EXTERN_STARBOARD_LENS 4
#endif

#ifndef AMG_TAPINGSPEED_EXTERN_STARBOARD_XX
#define AMG_TAPINGSPEED_EXTERN_STARBOARD_XX 34
#endif

#ifndef AMG_TAPINGSTARTEGY_DEFAULT
#define AMG_TAPINGSTARTEGY_DEFAULT 0
#endif
#ifndef AMG_TAPINGSTARTEGY_XDECREASING
#define AMG_TAPINGSTARTEGY_XDECREASING 1
#endif
#ifndef AMG_TAPINGSTARTEGY_NEIGHBOR
#define AMG_TAPINGSTARTEGY_NEIGHBOR 2
#endif


#ifndef AMG_NAME_TEMPLATE_MAINSAIL
#define AMG_NAME_TEMPLATE_MAINSAIL "template_mainsail.amg"
#else
	key word allready defined
#endif

#ifndef AMG_NAME_TEMPLATE_GENOA
#define AMG_NAME_TEMPLATE_GENOA "template_genoa.amg"
#else
	key word allready defined
#endif

#ifndef AMG_CONTOURS
#define AMG_CONTOURS "contours"
#else
	key word allready defined
#endif
	
#ifndef AMG_ONECONTOUR
#define AMG_ONECONTOUR "contour_"
#else
	key word allready defined
#endif

#ifndef AMG_ROSETTE_POSITION	
#define AMG_ROSETTE_POSITION	 "rosette_position"
#else
	key word allready defined
#endif

#ifndef AMG_RADIUSMARK
#define AMG_RADIUSMARK 0.02
#else
	key word allready defined
#endif

#ifndef AMG_UNIT_SYSTEM
#define AMG_UNIT_SYSTEM "unit_system"
#else
	key word allready defined
#endif

#ifndef AMG_UNIT_METERS
#define AMG_UNIT_METERS "meters"
#else
	key word allready defined
#endif

#ifndef AMG_UNIT_MILLIMETERS
#define AMG_UNIT_MILLIMETERS "millimeters"
#else
	key word allready defined
#endif

#ifndef AMG_POS_ON_CURVE
#define AMG_POS_ON_CURVE "pos_on_curve"
#else
	key word allready defined
#endif

#ifndef AMG_PARENT_GEOM
#define AMG_PARENT_GEOM "parent_geom"
#else
	key word allready defined
#endif

#ifndef AMG_PARENT_1
#define AMG_PARENT_1 "parent1"
#else
	key word allready defined
#endif

#ifndef AMG_PARENT_2
#define AMG_PARENT_2 "parent2"
#else
	key word allready defined
#endif

#ifndef AMG_POS_ON_PARENT1
#define AMG_POS_ON_PARENT1 "pos_on_parent1"
#else
	key word allready defined
#endif

#ifndef AMG_POS_ON_PARENT2
#define AMG_POS_ON_PARENT2 "pos_on_parent2"
#else
	key word allready defined
#endif

#ifndef AMG_INCLUDE
#define AMG_INCLUDE "include"
#else
	key word allready defined
#endif
	
#ifndef AMG_HOOP_REINFORCMENTS
#define AMG_HOOP_REINFORCMENTS "reinforcments"
#else
	key word allready defined
#endif
	
#ifndef AMG_HOOP_ONEREINFORCMENT
#define AMG_HOOP_ONEREINFORCMENT "reinforcment_"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_CONSTRUCTIONLINES
#define AMG_HOOP_CONSTRUCTIONLINES "constructionlines"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_ONECONSTRUCTIONLINE
#define AMG_HOOP_ONECONSTRUCTIONLINE "line_"
#else
	key word allready defined
#endif
	
	
#ifndef AMG_HOOP_OBJECT_TYPE
#define AMG_HOOP_OBJECT_TYPE "objtype"
#else
	key word allready defined
#endif
	
#ifndef AMG_HOOP_TYPE_EDGE
#define AMG_HOOP_TYPE_EDGE "edge"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_TYPE_BATTEN
#define AMG_HOOP_TYPE_BATTEN "batten"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_TYPE_PRESS
#define AMG_HOOP_TYPE_PRESS "press"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_TYPE_LINE
#define AMG_HOOP_TYPE_LINE "line"
#else
	key word allready defined
#endif	

	
#ifndef AMG_HOOP_TYPE_ROSETTE
#define AMG_HOOP_TYPE_ROSETTE "rosette"
#else
	key word allready defined
#endif	
	
#ifndef AMG_HOOP_TYPE_TRAPEZE
#define AMG_HOOP_TYPE_TRAPEZE "trapeze"
#else
	key word allready defined
#endif	

#ifndef AMG_HOOP_TYPE_LONGI
#define AMG_HOOP_TYPE_LONGI "longi"
#else
	key word allready defined
#endif	

	

	
	
#ifndef AMG_TAPE_TAPING_VERSION
#define AMG_TAPE_TAPING_VERSION "taping_version"
#else
	key word allready defined
#endif	

#ifndef AMG_TAPE_TAPING_STACK_TYPE
#define AMG_TAPE_TAPING_STACK_TYPE "stack_type"
#else
	key word allready defined
#endif	


#ifndef AMG_DOVE_TYPE
#define AMG_DOVE_TYPE "dove_type"
#else
	key word allready defined
#endif	

#ifndef AMG_GAUSS_LAYER
#define AMG_GAUSS_LAYER "gauss_layer"
#else
	key word allready defined
#endif	

#ifndef AMG_GAUSS_LAYER_NODES
#define AMG_GAUSS_LAYER_NODES "gauss_nodes"
#else
	key word allready defined
#endif	


#ifndef AMG_GAUSS_LAYER_EDGES
#define AMG_GAUSS_LAYER_EDGES "gauss_edges"
#else
	key word allready defined
#endif	

#ifndef AMG_GAUSS_LAYER_BROADSEAMS
#define AMG_GAUSS_LAYER_BROADSEAMS "gauss_broadseams"
#else
	key word allready defined
#endif	

	
#ifndef AMG_GAUSS_LAYER_BATTEN_POCKET
#define AMG_GAUSS_LAYER_BATTEN_POCKET "gauss_pockets"
#else
	key word allready defined
#endif	

	
#ifndef AMG_GAUSS_LAYER_MOULD	
#define AMG_GAUSS_LAYER_MOULD	 "gauss_mould"
#else
	key word allready defined
#endif	

#ifndef AMG_LAYER_2DMOULD	
#define AMG_LAYER_2DMOULD	 "2dmould"
#else
	key word allready defined
#endif	
	
#ifndef AMG_RELAX_WORKING_DIR
#define AMG_RELAX_WORKING_DIR "$RWD"
#else
	key word allready defined
#endif


#ifndef AMG_RELAX_DIR_MAINSAILS
#define AMG_RELAX_DIR_MAINSAILS "mainsails"
#else
	key word allready defined
#endif

#ifndef AMG_RELAX_DIR_GENOAS
#define AMG_RELAX_DIR_GENOAS "genoas"
#else
	key word allready defined
#endif

	
#ifndef AMG_RELAX_GRIDDENSITY
#define AMG_RELAX_GRIDDENSITY "grid_density"
#else
	key word allready defined
#endif

#ifndef AMG_RELAX_SUBSTRATE
#define AMG_RELAX_SUBSTRATE "substrate"
#else
	key word allready defined
#endif

#ifndef AMG_REINF_TYPE_ROSETTE
#define AMG_REINF_TYPE_ROSETTE 1
#else
	key word allready defined
#endif

#ifndef AMG_REINF_TYPE_TRAPEZE
#define AMG_REINF_TYPE_TRAPEZE 2
#else
	key word allready defined
#endif

#ifndef AMG_AMG
#define AMG_AMG "amg"
#else
	key word allready defined
#endif

#ifndef AMG_DOV
#define AMG_DOV "dov"
#else
	key word allready defined
#endif

#ifndef AMG_HMF
#define AMG_HMF "hmf"
#else
	key word allready defined
#endif
	
#ifndef AMG_HSF
#define AMG_HSF "hsf"
#else
	key word allready defined
#endif

#ifndef AMG_3DM
#define AMG_3DM "3dm"
#else
	key word allready defined
#endif

#ifndef AMG_DOVE_HARD_COPY
#define AMG_DOVE_HARD_COPY "hardcopy"
#else
	key word allready defined
#endif

#ifndef AMG_DOVE_SEG_HANDLES
#define AMG_DOVE_SEG_HANDLES "handles"
#else
	key word allready defined
#endif

#ifndef AMG_PANEL_ALREADY_TAPED
#define AMG_PANEL_ALREADY_TAPED "alreadytaped"
#else
	key word allready defined
#endif

#ifndef AMG_NBDECITEX
#define AMG_NBDECITEX "nbdecitex"
#else
	key word allready defined
#endif

#ifndef AMG_TENSILEMODULUS
#define AMG_TENSILEMODULUS "tensilemodulus"
#else
	key word allready defined
#endif
	
#ifndef AMG_TENSILESTRENGTH
#define AMG_TENSILESTRENGTH "tensilestrength"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_ONEYARN
#define AMG_HOOP_ONEYARN "yarn_"
#else
	key word allready defined
#endif

#ifndef AMG_LONGI_L1
#define AMG_LONGI_L1 "l1"
#else
	key word allready defined
#endif

#ifndef AMG_LONGI_L2
#define AMG_LONGI_L2 "l2"
#else
	key word allready defined
#endif
	
#ifndef AMG_LONGI_W1
#define AMG_LONGI_W1 "w1"
#else
	key word allready defined
#endif

#ifndef AMG_LONGI_W2
#define AMG_LONGI_W2 "w2"
#else
	key word allready defined
#endif

#ifndef AMG_LONGI_THKMID
#define AMG_LONGI_THKMID "thkmid"
#else
	key word allready defined
#endif
	
#ifndef AMG_LONGI_THKBORDER
#define AMG_LONGI_THKBORDER "thkborder"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_HOLES	
#define AMG_HOOP_HOLES	"holes"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_ONE_HOLE
#define AMG_HOOP_ONE_HOLE "hole_"
#else
	key word allready defined
#endif
	
#ifndef AMG_G
#define AMG_G 9.81
#else
	key word allready defined
#endif
	
#ifndef AMG_DOVE_TYPE_TRIRADIAL
#define AMG_DOVE_TYPE_TRIRADIAL "doveTriradial"
#else
	key word allready defined
#endif
	
#ifndef AMG_DOVE_TYPE_TRIRADIAL_LAMBDANE
#define AMG_DOVE_TYPE_TRIRADIAL_LAMBDANE "doveTriradialLambdaNe"
#else
	key word allready defined
#endif

	
#ifndef AMG_UV_POSITION
#define AMG_UV_POSITION "uv_position"
#else
	key word allready defined
#endif

#ifndef AMG_DOLDRUM_W1
#define AMG_DOLDRUM_W1 "doldrum_w1"
#else
	key word allready defined
#endif

#ifndef AMG_DOLDRUM_W2
#define AMG_DOLDRUM_W2 "doldrum_w2"
#else
	key word allready defined
#endif

#ifndef AMG_DOLDRUM_W3
#define AMG_DOLDRUM_W3 "doldrum_w3"
#else
	key word allready defined
#endif

#ifndef AMG_HOOP_FEA
#define AMG_HOOP_FEA "fea"
#else
	key word allready defined
#endif

	
#ifndef AMG_FEA_MAXSTRESS
#define AMG_FEA_MAXSTRESS "MaxStress"
#else
	key word allready defined
#endif

#ifndef AMG_FEA_MINSTRESS
#define AMG_FEA_MINSTRESS "MinStress"
#else
	key word allready defined
#endif

#ifndef AMG_FEA_COSINE
#define AMG_FEA_COSINE "Cosine"
#else
	key word allready defined
#endif

#ifndef AMG_FEA_SINE
#define AMG_FEA_SINE "Sine"
#else
	key word allready defined
#endif

	
#ifndef AMG_FEA_UPPSTRESSVECTOR
#define AMG_FEA_UPPSTRESSVECTOR "upstressvector"
#else
	key word allready defined
#endif

	
#ifndef AMG_DOVE_TRACE_TOLERANCE
#define AMG_DOVE_TRACE_TOLERANCE "tracetolerance"
#else
	key word allready defined
#endif
	
#ifndef AMG_DOVE_TRACE_NR
#define AMG_DOVE_TRACE_NR "trace_nr"
#else
	key word allready defined
#endif
	
#ifndef AMG_DOVE_TRACE_NC
#define AMG_DOVE_TRACE_NC "trace_nrc"
#else
	key word allready defined
#endif

	
#ifndef AMG_LEVEL_DIR_ZERO
#define AMG_LEVEL_DIR_ZERO "zero"
#else
	key word allready defined
#endif

#ifndef AMG_LEVEL_DIR_ALPHA1PLUS
#define AMG_LEVEL_DIR_ALPHA1PLUS "alpha1_plus"
#else
	key word allready defined
#endif

#ifndef AMG_LEVEL_DIR_ALPHA1MINUS
#define AMG_LEVEL_DIR_ALPHA1MINUS "alpha1_minus"
#else
	key word allready defined
#endif

#ifndef AMG_LEVEL_DIR_ALPHA2PLUS
#define AMG_LEVEL_DIR_ALPHA2PLUS "Alpha2_plus"
#else
	key word allready defined
#endif

#ifndef AMG_LEVEL_DIR_ALPHA2MINUS
#define AMG_LEVEL_DIR_ALPHA2MINUS "Alpha2_minus"
#else
	key word allready defined
#endif

	
#ifndef AMG_LEVEL_DIR_ZERO_ID
#define AMG_LEVEL_DIR_ZERO_ID 32
#else
	key word allready defined
#endif

#ifndef AMG_LEVEL_DIR_ALPHA1PLUS_ID
#define AMG_LEVEL_DIR_ALPHA1PLUS_ID 1
#else
	key word allready defined
#endif

#ifndef AMG_LEVEL_DIR_ALPHA1MINUS_ID
#define AMG_LEVEL_DIR_ALPHA1MINUS_ID 2
#else
	key word allready defined
#endif

#ifndef AMG_LEVEL_DIR_ALPHA2PLUS_ID
#define AMG_LEVEL_DIR_ALPHA2PLUS_ID 4
#else
	key word allready defined
#endif

#ifndef AMG_LEVEL_DIR_ALPHA2MINUS_ID
#define AMG_LEVEL_DIR_ALPHA2MINUS_ID 8
#else
	key word allready defined
#endif

#ifndef AMG_TEMP
#define AMG_TEMP "temp"
#else
	key word allready defined
#endif


#ifndef AMG_SHORTCUT_EXTEND
#define AMG_SHORTCUT_EXTEND "e"
#else
	key word allready defined
#endif
	
		

#ifndef AMG_SHORTCUT_EXTENDTOCRV
#define AMG_SHORTCUT_EXTENDTOCRV "x"
#else
	key word allready defined
#endif	

	
#ifndef AMG_SHORTCUT_TRIMBYCRV
#define AMG_SHORTCUT_TRIMBYCRV "m"
#else
	key word allready defined
#endif	
	
	
#ifndef AMG_SHORTCUT_DUPLEFT
#define AMG_SHORTCUT_DUPLEFT "l"
#else
	key word allready defined
#endif	
	
	
#ifndef AMG_SHORTCUT_DUPRIGHT
#define AMG_SHORTCUT_DUPRIGHT "i"
#else
	key word allready defined
#endif	
	
	
#ifndef AMG_SHORTCUT_TRANSLATE
#define AMG_SHORTCUT_TRANSLATE "t"
#else
	key word allready defined
#endif	
	
	
#ifndef AMG_SHORTCUT_ROTATE
#define AMG_SHORTCUT_ROTATE "r"
#else
	key word allready defined
#endif	
	

#ifndef AMG_SHORTCUT_DELETETAPE
#define AMG_SHORTCUT_DELETETAPE "d"
#else
	key word allready defined
#endif	
	

#ifndef AMG_SHORTCUT_DELETEPOLYGON
#define AMG_SHORTCUT_DELETEPOLYGON "y"
#else
	key word allready defined
#endif	
	
#ifndef AMG_SHORTCUT_JOINTAPE
#define AMG_SHORTCUT_JOINTAPE "j"
#else
	key word allready defined
#endif	
	
#ifndef AMG_SHORTCUT_NEW
#define AMG_SHORTCUT_NEW "n"
#else
	key word allready defined
#endif	
	
	
#ifndef AMG_SHORTCUT_TAPEPOLYGON
#define AMG_SHORTCUT_TAPEPOLYGON "p"
#else
	key word allready defined
#endif	
	
	
#ifndef AMG_SHORTCUT_STOPOPERATOR
#define AMG_SHORTCUT_STOPOPERATOR "s"
#else
	key word allready defined
#endif	

#ifndef AMG_ASCII_F1
#define AMG_ASCII_F1 112
#else
	key word allready defined
#endif	

#ifndef AMG_ASCII_F2
#define AMG_ASCII_F2 113
#else
	key word allready defined
#endif	

#ifndef AMG_ASCII_F3
#define AMG_ASCII_F3 114
#else
	key word allready defined
#endif	

#ifndef AMG_ASCII_F4
#define AMG_ASCII_F4 115
#else
	key word allready defined
#endif	

#ifndef AMG_ASCII_F5
#define AMG_ASCII_F5 116
#else
	key word allready defined
#endif	

#ifndef AMG_ASCII_F6
#define AMG_ASCII_F6 117
#else
	key word allready defined
#endif	

#ifndef AMG_ASCII_F7
#define AMG_ASCII_F7 118
#else
	key word allready defined
#endif	

#ifndef AMG_ASCII_F8
#define AMG_ASCII_F8 119
#else
	key word allready defined
#endif	

#ifndef AMG_ASCII_F9
#define AMG_ASCII_F9 120
#else
	key word allready defined
#endif	

#ifndef AMG_ASCII_F10
#define AMG_ASCII_F10 121
#else
	key word allready defined
#endif	

#ifndef AMG_ASCII_F11
#define AMG_ASCII_F11 122
#else
	key word allready defined
#endif	

#ifndef AMG_ASCII_F12
#define AMG_ASCII_F12 123
#else
	key word allready defined
#endif	
	
#endif // RXUsrXSectionDeleteMessageHandler_DEC2005
