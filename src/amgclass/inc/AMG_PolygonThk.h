#ifndef AMGPOLYGONTHK_MAY07
#define AMGPOLYGONTHK_MAY07

class AMG_PolygonThk
{
public:
	double m_thk;
#ifdef RENAUD
	Polygon_with_holes_2 m_poly;
	AMG_PolygonThk(Polygon_with_holes_2 poly, double thk){m_thk=thk;m_poly=poly;};
#endif
	AMG_PolygonThk()
	{
		m_thk=0;
#ifdef RENAUD
		m_poly.clear();
#endif
	};
	~AMG_PolygonThk(){/*m_poly.clear();*/};
};
#endif //#ifndef AMGPOLYGONTHK_MAY07

