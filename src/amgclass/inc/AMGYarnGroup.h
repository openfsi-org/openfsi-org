// AMGYarnGroup.h: interface for the AMGYarnGroup class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGYarnGroup_APR2006
#define AMGYarnGroup_APR2006

#include "AMGHoopsObject.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGYarnGroup : public  AMGHoopsObject
{
public:
	ON_Curve * GetYarn(const int & p_ID) const;
	int SetYarn(const int &p_ID, ON_Curve * p_pcurve);

	AMGYarnGroup();
	AMGYarnGroup(const ON_String & p_path,
				const ON_String & p_name);
	
	virtual ~AMGYarnGroup();
	
	
	int GetPlyName(ON_String & p_name) const;
	int SetPlyName(const ON_String &p_name);
};

#endif // AMGYarnGroup_APR2006
