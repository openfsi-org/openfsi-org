// AMGZERef.h: interface for the AMGZERef class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGZEREF_H__89741BE7_A4B9_475A_89B4_7A274F879EEC__INCLUDED_)
#define AFX_AMGZEREF_H__89741BE7_A4B9_475A_89B4_7A274F879EEC__INCLUDED_

#include "opennurbs_point.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGTape;
class AMGZoneEdge;
class AMGZoneNode;
class AMGTapingZone ;
#include "layerobject.h"
#include "AMGTapingConstants.h"


class AMGZERef : public layerobject 
{
public:
//	void FlagSet(const int p);
//	static void SetPanel (AMGTapingZone *p_p)	{m_panel=p_p;}
	int SplitCurveHere(AMGTapingZone *p_tz);
	AMGTape * GetTape() const;
	AMGZERef * GetNext( int GoPastCorners=AMGZER_STOPATCORNERS) const; 
	AMGZERef * GetPrev( int GoPastCorners=AMGZER_STOPATCORNERS) const; 

	void SetNext(AMGZERef *p  ){m_next=p;}  
	void SetPrev( AMGZERef * p){m_prev=p;} 
//	ON_3dPoint GetPoint(int*err)const;

//	int IsContiguousWithNext(int GeometricTestToo=0) const;
	int Verify(FILE *fp) const;

	double GetParameter() const;
	void SetTape(AMGTape * pt);
	int Print(FILE *fp,int headertoo=0);
	int InsertAfter(AMGZERef *p);
	int InsertBefore(AMGZERef* p);
	void Empty();

	AMGZERef();
	AMGZERef( AMGZoneEdge *theEdge, double t2 , int Flag ,AMGTape*theTape ); 
	AMGZERef( const AMGZERef *p ); 
	virtual ~AMGZERef();

	AMGZoneEdge* m_ze; // the ZoneEdge
	AMGZoneEdge* GetZoneEdge(void)const{return m_ze;}
	AMGZoneNode* GetZoneNode();
	void SetZoneNode(AMGZoneNode*);

//	int m_FLAG;


protected:
	AMGZoneNode* m_zonenode; // the ZoneNode
	void SetParameter(const double t);
	AMGZERef* m_next;
	AMGZERef* m_prev;
	AMGTape* m_Tape;
	double m_t;

private:
//	static AMGTapingZone *m_panel;
};

#endif // !defined(AFX_AMGZEREF_H__89741BE7_A4B9_475A_89B4_7A274F879EEC__INCLUDED_)
