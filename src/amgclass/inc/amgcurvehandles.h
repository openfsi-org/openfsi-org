#ifndef _AMGCURVEHANDLES_H_
#define  _AMGCURVEHANDLES_H_

#include "hc.h"
#include "opennurbs.h"

#include "dovestructure.h"

#define EXTERN_C extern "C" 
#define AMG_TRANSLATION		0
#define AMG_ROTATION		1
#define AMG_EDITEDGES		8
#define AMG_EDITSECTIONS	16
// verified to work with transformations, not cameras.

class HBaseView;
EXTERN_C int AMGRotatePolylineToXAxis(const HC_KEY gkey);

// the following could be members of our derived view.
EXTERN_C HC_KEY Draw_Contour_Scale(HBaseView *p_pView, const char*p_uos, const float step,const float cmin);

//EXTERN_C int CreateNurbs2SurfAnglesEditScreen(HC_KEY p_SinSurface,HC_KEY p_CosSurface, HBaseView *p_pView);
EXTERN_C int CreateNurbsSpinnerEditScreen(HC_KEY p_SinSurface,HC_KEY p_CosSurface, HBaseView *p_pView, const int p_flag);
EXTERN_C void CleanUpSurfaceEdit(HBaseView *p_pView);
EXTERN_C int CreateNurbsSurfaceEditScreen(HC_KEY p_keySurface, HBaseView *p_pView, const char*p_uos);
EXTERN_C HC_KEY DrawNurbsEditBox(HC_KEY p_keySurface);
EXTERN_C int CreateXSectionScreen(HBaseView *p_pView);
EXTERN_C int CreateEdgesScreen(HBaseView *p_pView);
EXTERN_C int CreateTransformationsForCCEdit(HC_KEY p_segment, HBaseView *p_hbv, char *p_path, const int what);

EXTERN_C int CreateTransformationsForPolylines(HC_KEY  p_segment, HBaseView * p_hbv );

EXTERN_C int CreateHandlesOnCVs(HC_KEY  p_segment, 
								HBaseView * p_hbv,
								ON_SimpleArray<int> p_directions,
								const char* searchspec,
								const int rotation , /// TRANSLATION or ROTATION
								const int p_flags =0	// north south east west
								);

EXTERN_C int SetHandlesOnPolylineCVs(HC_KEY p_keyCurve,  
									HBaseView * p_hbv, 
									ON_SimpleArray<int> p_directions);

EXTERN_C HC_KEY SetupAxisManipulators(HC_KEY startkey,  // translation
									HBaseView *view, 
									HPoint *midpoint,
									ON_SimpleArray<int> p_directions);

int SetHandlesRotationOnSurfaceCVs(HC_KEY p_keySurface,  
									HBaseView * p_hbv,
									ON_SimpleArray<int> p_directions);

int SetHandlesRotationOnSurfaceCVs(HC_KEY p_keySurface, 
							HBaseView * p_hbv,
							ON_SimpleArray<int> p_directions,
							int p_flags);

EXTERN_C int SetHandlesTranslationOnSurfaceCVs(HC_KEY p_keySurface, 
							HBaseView * p_hbv,
							ON_SimpleArray<int> p_directions );
EXTERN_C HC_KEY SetupAxisRotationManipulators(HC_KEY startkey, 
												  HBaseView *p_hbv,  // only for SetHasObjects
												  HPoint *midpoint,
												  ON_SimpleArray<int> p_directions);

EXTERN_C int YZ_CopyPolyline(const HC_KEY XZcurveKey,const int index ,HPoint bpout);
EXTERN_C HC_KEY Draw_Swapped_Polyline(HC_KEY l_key);
EXTERN_C  int   CreateEdgeEditCurves(HC_KEY  p_segment);
EXTERN_C int AdjustAnglePair(float deltaang,HC_KEY sinSurf,HC_KEY cosSurf,int i, dovestructure*p_thedove);

EXTERN_C int SetAngleTR(HC_KEY sinSurf,HC_KEY cosSurf,int i, dovestructure*p_thedove,const double & p_anglerad);
EXTERN_C int GetAngleTR(HC_KEY sinSurf,HC_KEY cosSurf,int i, dovestructure*p_thedove,double & p_anglerad);

EXTERN_C int SetHandlesOnNurbsCurveCVs(HC_KEY p_keyCurve, 
									HBaseView * p_hbv,
									ON_SimpleArray<int> p_directions);

EXTERN_C int GetCV(HC_KEY p_nurbsKey,int p_i, HPoint *p);
// PutCV moved to doveforstack to tidy the build
// the following should stay here. They are unverified.

// unverified (by me)
EXTERN_C int DrawDirectionFrom2HoopsSurfaces(const HC_KEY p_kDrawSeg, dovestructure * p_theDove );
								
//EXTERN_C int CAmguiNurbsEditorViewDrawDirectionFrom2NurbsSurface(ON_NurbsSurface & p_nurbs1, 
//									ON_NurbsSurface & p_nurbs2,dovestructure * p_theDove,
//									int p_nbU,int p_nbV);

EXTERN_C int CAmguiNurbsEditorViewHoopsNurbsSurfaceToON(HC_KEY p_nurbsKey,ON_NurbsSurface & p_nurbs);

// WRONG Don't use
EXTERN_C int debugCreateCamerasForObjects(HC_KEY  p_segment, HBaseView * p_hbv ); // mouse coord problems
EXTERN_C int debugCreateHWIndowsForObjects(HC_KEY  p_segment, HBaseView * p_hbv ); // selection problems

EXTERN_C int nurbssurfaceeditburn(HC_KEY p_keySurface, HBaseView *p_pView);


EXTERN_C int AMGFormatSchnack_Text(char *buf);
#endif

