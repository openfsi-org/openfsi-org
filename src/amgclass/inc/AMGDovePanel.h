// AMGDovePanel.h: interface for the AMGDovePanel class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGDOVEPANEL_H__8C511C7B_DC7D_45F4_B92A_709406F06AEE__INCLUDED_)
#define AFX_AMGDOVEPANEL_H__8C511C7B_DC7D_45F4_B92A_709406F06AEE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "AMGHoopsObject.h"
#include "AMGDoveSurface.h"

class AMGDovePanel : public AMGDoveSurface  
{
public:
	AMGDovePanel();
	virtual ~AMGDovePanel();

	AMGDovePanel(dovestructure  * p_pDove,
							ON_Surface          * p_pSurface, 
							const ON_Surface          * p_pPanel);

	virtual ON_2dVector EvalAlphaByXY(ON_2dPoint p_pos, const int & p_ply);
	virtual ON_2dVector EvalGradAlphaByXY(ON_2dPoint p_pos, const int & p_ply);

	ON_Curve    *  m_IsoV;

protected: 
	const ON_Surface  * m_pPanel;
	

};

#endif // !defined(AFX_AMGDOVEPANEL_H__8C511C7B_DC7D_45F4_B92A_709406F06AEE__INCLUDED_)
