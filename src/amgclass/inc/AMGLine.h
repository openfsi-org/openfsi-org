// AMGLine.h: interface for the AMGLine class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGLINE_FEB2007
#define AMGLINE_FEB2007

#include "AMGHoopsObject.h"
//#include "Z:\build\cpp\on_relax\opennurbs_point.h"	// Added by ClassView


#ifdef __cplusplus
	#define EXTERN_C extern "C"
#else
	#define EXTERN_C extern
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGLine : public  AMGHoopsObject
{
public:
	int Update();
	AMGLine();
	AMGLine(const ON_String & p_model,
				   const ON_String & p_name, 
				   int               p_pdoc = NULL);
	ON_String Serialize(); 
	char* Deserialize(char* p_lp );

	virtual ~AMGLine();
	
	int SetParent1(const ON_String & p_path);
	int GetParent1(ON_String & p_seg) const;

	int SetParent2(const ON_String & p_path);
	int GetParent2(ON_String & p_seg) const;

	
	// set the position on on parent 
	// if p_id == 0 -> on parent 1
	// if p_id == 1 -> on parent 2
	// p_pos from 0.0 to 1.0 along the curve
	int SetPosition(const int & p_ID, 
					const double & p_pos);

	// Get the position on on parent 
	// if p_id == 0 -> on parent 1
	// if p_id == 1 -> on parent 2
	// p_pos from 0.0 to 1.0 along the curve
	int GetPosition(const int & p_ID, 
					double & p_pos) const;
	
	// if we can find a curve as a parent retrieve it
	//if p_ID==0 -> parent1
	//if p_ID==1 -> parent2
	ON_Curve * GetParentCurve(const int & p_ID) const;


	int SetReinforcment(const ON_String & p_seg);
	int GetReinforcment(ON_String & p_seg);

	int SetPlyName(const ON_String & p_name);
	int GetPlyName(ON_String & p_name);

	int ExportToRelax(ONX_Model & p_3dmModel);
protected: 

};

#endif // AMGLine_DEC2005
