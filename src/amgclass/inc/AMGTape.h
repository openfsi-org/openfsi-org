// AMGTape.h: interface for the AMGTape class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGTAPE_H__0CA8556A_A432_46D6_A484_F57C0DC3AAA5__INCLUDED_)
#define AFX_AMGTAPE_H__0CA8556A_A432_46D6_A484_F57C0DC3AAA5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifdef HOOPS
#include "hc.h"
#else
#error(" AMG Tape.h needs hoops ")
#endif
#include "opennurbs_extensions.h"
#include "AMGHoopsObject.h"
#include "opennurbs_point.h"	// Added by ClassView
#ifdef linux
#ifndef BOOL 
#define BOOL int
#endif
#endif

#include "layerobject.h"
#include "AMGLevel.h"	// Added by ClassView

class AMGZERef;
class AMGZoneNode;

class AMGTape  : public AMGHoopsObject, public layerobject 
{
public:
	AMGLevel GetLevel()const;
	int SetUVCoords(ON_2dPoint p, const int flag);
	void SetLevel(AMGLevel level);
	AMGZoneNode * GetNodeOnEnd();
 
	int SetWidth(const double & p_Width);

	int SetZER(AMGZERef * pzer, const int flag);
	int Reverse();
	int ExtendBothEnds(const double p_dl);
	AMGTape *GetTapeOnLeft() const {	return m_TapePrevious; }
	AMGTape *GetTapeOnRight()const {	return m_TapeNext; }
	int ConnectOnLeft(AMGTape*p_TheTapeOnLeft);

	HC_KEY DrawHoops(const char*seg);
	static int GoingUpTheCurve(const int IsOnRightOfCurve,const int IsRightOnTape);
	double GetPosOnCurve() const;
	void SetPosOnCurve(const double t) { m_posoncurve = t;}

	int MeasureOverlap(AMGTape*p_newtape,const ON_3dVector p_CurveTangentXYZ, const int InOnROfTape, double*distToMove);
	ON_2dPoint GetCorner(const int what, const int AllowForThk=0) const;
	ON_2dPoint GetMidSide(const int righthandside);
//#ifdef AMGUI_2005
	bool GetIsOnRight() { return m_IsOnRightOfStartCurve;}  
//#endif
	int SetStart(ON_2dPoint p);
	int SetEnd(ON_2dPoint p);
	int Print(ONX_Model & p_model);
	AMGTape();
	AMGTape(	 const ON_2dPoint & p_start, 
				 const ON_2dPoint & p_end, 
				 AMGHoopsObject * p_tapebutcurve = NULL,
				 bool  p_IsOnRigth = true, 
				 double * p_posoncurve = NULL, 
				 double * p_width = NULL);
	virtual ~AMGTape();

	int Print(ONX_Model & p_model,ON_3dmObjectAttributes * p_pAtt);
 
	AMGTape(ON_Surface * p_Sfr);

	ON_2dVector GetDir() const;  // unitized.
	ON_2dVector GetNormal() const;

	ON_2dPoint GetEnd() const;
	ON_2dPoint GetStart() const;

	double GetWidth() const;
	double GetLength() const;
	double SplitByCurve(ON_Curve * p_crv);
	int DrawInRhino(const ON_3dmObjectAttributes * p_pAtt) const;
	AMGZERef *GetZER_End() const{return m_ZER_End ;}
	AMGZERef *GetZER_Start() const{return m_ZER_Start ;}
	int PolylineIsFairlyParallel(ON_3dPointArray &endpts,const double tol);
protected:
	ON_2dPoint m_StartUV, m_EndUV;
	AMGLevel m_level;

	AMGZERef *m_ZER_Start, *m_ZER_End ;
	AMGTape * m_TapePrevious;
	AMGTape * m_TapeNext;
	ON_2dPoint m_start;
	ON_2dPoint m_end; 
	
	AMGHoopsObject * m_ptapebutcurve;

//#ifdef AMGUI_2005
	bool m_IsOnRightOfStartCurve;   // means Is on right of the curve through it's start point.
//#endif 
	double m_posoncurve;
	double m_width;
	int m_IsEndOnCurve;
};

#endif // !defined(AFX_AMGTAPE_H__0CA8556A_A432_46D6_A484_F57C0DC3AAA5__INCLUDED_)
