// AMGMeasurment.h: interface for the AMGMeasurment class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGMeasurment_DEC2005
#define AMGMeasurment_DEC2005

#include "AMGUIConstants.h"
#include "AMGHoopsObject.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGMeasurment : public  AMGHoopsObject
{
public:
	AMGMeasurment();
	AMGMeasurment(const ON_String & p_path,
			      const ON_String & p_name);
	virtual ~AMGMeasurment();
	
	int SetEdges(ON_SimpleArray<AMGHoopsObject*> p_edges);//p_Edges[0] is the luff 
													//p_Edges[1] is the leech 
													//p_Edges[2] is the FOOR
													//p_Edges[3] is the head
	
	int SetPresses(ON_SimpleArray<AMGHoopsObject*> p_presses);

	// set the battens they have to be sorted in increasing order along the leech
	int SetBattens(ON_SimpleArray<AMGHoopsObject*> p_battens); 

		
	/*First Method
	We draw a line from Point 1 ? Point 2
	Each time we cross a broad seam we extend the nearest end of the curve by the value of the the BS.
	To compute MID girth Point 1 == clew , Point 2 == Headfwd ? PtMid
	To compute 1/4 girth Point 1 == clew , Point 2 == PtMid  ? PtQuarter
	To compute 3/4 girth Point 1 == PtMid  , Point 2 == Headfwd ? PtThreeQuarter
	To compute 7/8 girth Point 1 == PtThreeQuarter , Point 2 == Headfwd ? PtSevenEigth

	Iterative method
	Find the min of the linear method*/
	ON_LineCurve GetGeodesic(const ON_2dPoint & p_start, 
							 const ON_2dPoint & p_end, 
							 const int & p_method = AMG_LINEAR);
	int ComputeMidGirth(const ON_2dPoint & p_start, 
						const ON_2dPoint & p_end, 
						double & p_val, 
						ON_2dPoint & p_ptLeech,
						ON_2dPoint & p_ptLuff);

	int ComputeAllGirth(double & p_Quarter, 
						double & p_Mid, 
						double & p_ThreeQuarter, 
						double & p_SevenEight);

	int ComputeLeech(double & p_val);
	int ComputeLeechHeadFwd(double & p_val);
	int ComputeLuff(double & p_val);
	int ComputeFoot(double & p_val);
	int ComputeLP(double & p_val);

	ON_Curve * GetLeech() const;
	ON_Curve * GetLuff() const;
	ON_Curve * GetFoot() const; 
	ON_Curve * GetHead() const; 

	ON_2dPoint GetClew() const ;
	ON_2dPoint GetTack() const; 
	ON_2dPoint GetHeadFwd() const ;
	ON_2dPoint GetHeadAft() const;

	ON_Curve * GetEdgeByName(const ON_String & p_name) const;

protected:
	ON_SimpleArray<AMGHoopsObject*> m_pEdges; //p_Edges[0] is the luff 
													//p_Edges[1] is the leech 
													//p_Edges[2] is the FOOR
													//p_Edges[3] is the head
	
	ON_SimpleArray<AMGHoopsObject*> m_Presses;
	ON_SimpleArray<AMGHoopsObject*> m_pBattens;
};

int SortPointsByYIncreasing(const ON_3dPoint * p_pt1, const ON_3dPoint * p_pt2);

#endif // AMGMeasurment_DEC2005
