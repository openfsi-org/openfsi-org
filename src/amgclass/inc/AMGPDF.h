#ifndef AMGPDF_MARCH_07
#define AMGPDF_MARCH_07

#ifndef MSVS2005
#import	 "PDFCreatorPilot3.dll"
using namespace PDFCreatorPilot3Lib;
#endif

#include "CAmguiDoc.h"


class AMGPdf
{
public: 
	AMGPdf(); 
	virtual ~AMGPdf();

	int InitPdf(const ON_String & p_filename, 
				const ON_String & p_tittle, 
				const BOOL      & p_IsLandScape);
	int TerminatePdf();

#ifndef MSVS2005
	IPDFDocument3* GetPdf();
#else
	int GetPdf();
#endif

	int NewPage(const double & p_page_Width,const double & p_page_Height); 
	int DrawHeader();

	int SetDoc(CAmguiDoc * p_pdoc);

	int SetPageDimensions(const double & p_width,const double & p_height);
	int SetFontSize(const int & p_size);

	int WriteGeometricalReport();
	int WriteReinforcmentReport();
	int TapingReport();

	int DrawCurve(const ON_Curve * p_pcrv, 
				  const int		  & p_nbpts);

	int DrawPolyline(const ON_Polyline & p_pl, 
					 const BOOL		   & p_IsPolygon, 
					 const BOOL		   & p_IsFill = TRUE);
	int DrawTapes(const ON_String & p_seg);
protected: 
	ON_String m_tittle;

#ifndef MSVS2005
	IPDFDocument3 * m_pPDF; 
#else
	int m_pPDF; 
#endif
	CAmguiDoc     * m_pdoc;

	ON_Xform m_TransToPdf;
};

#endif //#define AMGPDF_MARCH_07
