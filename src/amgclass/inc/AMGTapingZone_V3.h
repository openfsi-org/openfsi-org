// AMGTapingZone_V3.h: interface for the AMGTapingZone_V3 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGTAPINGZONE_V3_H__939AA3A1_BC70_4FD6_858C_F1BE64B9A394__INCLUDED_)
#define AFX_AMGTAPINGZONE_V3_H__939AA3A1_BC70_4FD6_858C_F1BE64B9A394__INCLUDED_

#define AMGTZV3_BYONC // or AMGTZV3_BYAMGP

#include "opennurbs_curve.h"	// Added by ClassView
#include "rxON_Extensions.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AMGPolygon.h"
#include "AMGTapingZone.h"
#include "AMGLevel.h"	// Added by ClassView
class AMGPolygon;
class AMGVertex;

class AMGTapingZone_V3 : public AMGTapingZone  
{

public:
	double MeasureOverlapArea(AMGTape *p_t);
	HC_KEY DrawHoops(const char*seg);
	int IsGoingToEdge();
	AMGLevel m_level;
	void SetButtCurve(rxON_LineCurve *pc);

	AMGTapingZone_V3(const ON_String & p_path,
									const ON_String & p_name,
									ON_Curve * p_boundary,
									dovestructure * p_dove);

	AMGTapingZone_V3();
	virtual ~AMGTapingZone_V3();

	int SetWeights(const double &lengthWeight, 
		const double &AreaWeight, 
		const double &MOIWeight,
		const double &FreeEdgeWeight ,
		const double &AngleWeight, 
		const double &ThkWeight, 
		const int p_sh_pow);
	int MakeTapes(const AMGLevel level);

	double Penalty_Shortness(AMGTape *pt);// (Will be zero if we are going to the edge.)
	double Penalty_OverlapAndFreeEdge(AMGTape *pt);
	double Penalty_OutsidePanel(AMGTape *pt);
	double Penalty_AngleError (AMGTape *pt);

	AMGTape *MakeOneTape(ON_SimpleArray<double>  &x);
	void AppendToVlist(AMGVertex *pv);
	AMGVertex* V(int i) const;
	int VlistCount() const;
protected:


	int SubtractOneTapeFromHoles(AMGTape *theTape);


	int FindAllHolesFromTapesByAMGP(void);
	int FindAllHolesFromTapesByONP(void);

	int TapeFirstChain(const AMGLevel level);
	int SetStartingValues( 	
						ON_SimpleArray<double> &x,
						ON_SimpleArray<double> &scales);

	AMGTape * TapeOneCorner(const AMGLevel level);
	int SSDFindNextCorner(const AMGLevel level);
	double DistanceToBoundary(AMGTape *pt, AMGPolygon *p_bdy);
	double DistanceToBoundary(AMGTape *pt, rxON_PolylineCurve *p_bdy);

	rxON_LineCurve *m_buttCurve; // was ON-Curve*
	double m_WeightLength;
	double m_WeightArea;
	double m_WeightMOI;
	double m_WeightFreeEdge;
	double m_WeightAngle;
	double m_WeightThickness;
	int  m_GoingToEdge; // to tell the length penalty what to do.
	int m_ShortnessPower;  //A big number drives the length towards the upper limit. nice range is 1 to 5.


		ON_ClassArray<AMGPolygon*> m_holes;
		rxON_PolylineCurve m_bndy;

#ifdef AMGTZV3_BYONC // otherwise AMGTZV3_BYAMGP
		rxON_PolylineCurve *m_CurrentHole;

#else
		AMGPolygon *m_CurrentHole;

#endif

		AMGPVLIST m_masterVlist; 


private:




};

#endif // !defined(AFX_AMGTAPINGZONE_V3_H__939AA3A1_BC70_4FD6_858C_F1BE64B9A394__INCLUDED_)
