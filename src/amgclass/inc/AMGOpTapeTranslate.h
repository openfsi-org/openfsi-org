//
// Copyright (c) 2000 by Tech Soft America, LLC.
// The information contained herein is confidential and proprietary to
// Tech Soft America, LLC., and considered a trade secret as defined under
// civil and criminal statutes.  Tech Soft America shall pursue its civil
// and criminal remedies in the event of unauthorized use or misappropriation
// of its trade secrets.  Use of this information by anyone other than
// authorized employees of Tech Soft America, LLC. is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
// $Header: /files/homes/master/cvs/hoops_master/shared/partviewer_common/AMGOpTapeTranslate.h,v 1.5 2000/06/14 17:47:00 Covey Exp $
//

// HOpSelectAperture.h : interface of the HOpSelectAperture class, derived from HOpSelect
// Handles aperture selection

#ifndef _AMGOpTapeTranslate_H
#define _AMGOpTapeTranslate_H

#include "HOpSelectAperture.h"

class AMGOpTapeTranslate : public HOpSelectAperture
{
public:
	void SetRootSegment(const ON_String & p_rootseg);

	void SetAlpha(const double & p_val);
    AMGOpTapeTranslate(HBaseView* view, int DoRepeat = 0, int DoCapture = 1);
    virtual int OnLButtonDown(HEventInfo &event);
	virtual int OnLButtonDownAndMove(HEventInfo &event); 

	int DoLButtonDown(HEventInfo &event);

	virtual int OnMButtonDownAndMove(HEventInfo &event);

	int OnMButtonDown(HEventInfo &event);
	int OnMButtonUp(HEventInfo &event);
protected:
	ON_2dPoint m_from;
	ON_2dPoint m_to;
	BOOL m_FirstPointSet;
	ON_ClassArray<ON_String> m_segtapes;
	ON_String m_root;
	BOOL m_ResetSelection;
	int TranslateTapes();

};

#endif
