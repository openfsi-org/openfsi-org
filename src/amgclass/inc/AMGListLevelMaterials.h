#ifndef _AMG_LISTLEVELMATERIALS_MAY07_H_
#define _AMG_LISTLEVELMATERIALS_MAY07_H_

#include "AMGLevelMaterial.h"

class AMGListLevelMaterials
{
public:
	int Trace(const int & p_ID, double & p_val) const;
	AMGListLevelMaterials(void);
	virtual ~AMGListLevelMaterials(void);

	AMGListLevelMaterials& operator=(const AMGListLevelMaterials&);
	
	int AddMaterial(const AMGLevelMaterial & p_mat);

protected:
	ON_ClassArray<AMGLevelMaterial> m_List;
};
#endif
