// AMGPly.h: interface for the AMGPly class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGPly_DEC2006
#define AMGPly_DEC2006

#include "AMGHoopsObject.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGPly : public  AMGHoopsObject
{
public:

	AMGPly();
	AMGPly(const ON_String & p_path,
		   const ON_String & p_name, 
		   const double & p_val = 0);
	
	virtual ~AMGPly();

	int SetDoc(CDocument * p_pdoc); 

	int SetTapeRef(const ON_String & p_str);

	double GetWidth() const;
	double GetMaxDeviation() const;
	double GetButtOverlap() const;
	double GetLatOverlap() const;
	double GetMinLength() const;
	double GetMaxLength() const;
	int GetContourID() const;

	int GetTapeModulus(double &p_val) const;

	int GetNbYarns() const;
	ON_String GetYarnType() const;
	ON_String GetGlue() const;
	ON_String GetExtra() const;

	ON_String GetTapeRef() const;
	double GetLinearMass() const;
	double GetThicknessFactor() const;
	int GetStackColorID() const;
	int GetColorID() const;
	ON_String GetMaterialCode() const;

protected: 
	CDocument * m_pdoc;
};

#endif // AMGPly_DEC06
