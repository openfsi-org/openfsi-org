// AMGZoneNode.h: interface for the AMGZoneNode class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGZONENODE_H__AF17E18B_3A30_4591_BF38_314D210ACECE__INCLUDED_)
#define AFX_AMGZONENODE_H__AF17E18B_3A30_4591_BF38_314D210ACECE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AMGZoneEdge.h"
#include "AMGZERef.h"
#include "layerobject.h"
#include "opennurbs_point.h"	// Added by ClassView
/*
	A ZoneNode may be on an edge.  It contains a list of edges which start or end on it and it knows whether it's the start or the end. 
Members
  */
class AMGZoneNode : public layerobject
{
public:
	int SortEdgeList();
	ON_3dPoint GetPosition();
	int RemoveZoneEdgeRef(AMGZoneEdge *p_ze);
	AMGTape* GetTape( );
	AMGZERef * GetZER();
	void SetZER(const AMGZERef *z);

	int HookToZERef(AMGZERef*p_zer); // call with null to unhook
	int HookToStartOf(AMGZoneEdge *e);
	int HookToEndOf(AMGZoneEdge *e);
	int Print(FILE *fp,const int what =AMGPRINT_OBJ|AMGPRINT_RHINO);
	AMGZoneNode();
	AMGZoneNode(AMGZoneEdge *p_prev,AMGZoneEdge *p_next);
	virtual ~AMGZoneNode( );
//	int m_Flag;
	ON_SimpleArray<AMGZoneEdge *> m_Zedges ;
	ON_SimpleArray<int>  m_IsOnEnd;
protected:

	AMGZERef *m_whereami; // glue. wherami is set only in HookToZERef.


};

#endif // !defined(AFX_AMGZONENODE_H__AF17E18B_3A30_4591_BF38_314D210ACECE__INCLUDED_)
