// AMGHoopsObject.h: interface for the AMGHoopsObject class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGHOOPSOBJECT_H__651B70BC_D005_4A19_97F6_C5D3A19F389C__INCLUDED_)
#define AFX_AMGHOOPSOBJECT_H__651B70BC_D005_4A19_97F6_C5D3A19F389C__INCLUDED_

#if _MSC_VER > 1000  
#pragma once
#endif // _MSC_VER > 1000

#include "opennurbs.h" 
//#ifdef linux
//#ifndef BOOL 
//	#define BOOL ON_BOOL
//#endif
//#endif

class AMGHoopsObject  
{
public:
	int Exists() const;
	int SetDoc(int p_pdoc);
	//unset if p_txt=""
	int SetColor(const ON_String & p_txt, const ON_String & p_localpath = "");
	//unset is p_txt==""
	int SetVisibility(const ON_String & p_txt, const ON_String &p_localpath = ""); 
	
	int UnsetColor(const ON_String & p_localpath = "");
	
	int Init();
	AMGHoopsObject();
	AMGHoopsObject(const ON_String & p_Model,const ON_String & p_name, int p_pdoc = NULL);
	
	virtual ~AMGHoopsObject();
	
	AMGHoopsObject& operator=(const AMGHoopsObject&);

	ON_String Serialize();
	char* Deserialize(char* p_lp );

	
	int SetObjectType(const ON_String & p_str);
	int GetObjectType(ON_String & p_str);

	int GetModel(ON_String & p_Model) const;
	ON_String GetModel() const;
	
	int SetModel(const ON_String & p_Model);
	
	int GetName(ON_String & p_Model) const;
	ON_String GetName() const;
	
	int AddText(const ON_String & p_localpath = "",  
				const double & p_x = 0,
				const double & p_y = 0,
				const double & p_z = 0,
				const ON_String & p_txt = "");

	int SetName(const ON_String & p_name, 
				const bool p_Rename = true);

	//Define or reset an user option define in the segment :
	//if p_localModel!=0 			m_Model / m_name  / p_localPath
	//if p_localModel==0 			m_Model / m_name  / 
	//the name of the user option is p_name the vqlue is set to p_val
	int SetUserOption(const ON_String & p_localpath, 
					  const ON_String & p_name, 
					  const float & p_val);

	int SetUserOption(const ON_String & p_localpath, 
								  const ON_String & p_name, 
								  const ON_String & p_str);

	int UnSetUserOption(const ON_String & p_localpath, 
						const ON_String & p_name);

	//get the value of the user option defined in the segment :
	//if p_localpath!=0 			m_path / m_name  / p_localpath
	//if p_localpath==0 			m_path / m_name  / 
	//the name of the user option is p_name the value is retunr in p_val
	int GetUserOption(const ON_String & p_localpath, 
					  const ON_String & p_name, 
					  float & p_val) const;

	int GetUserOption(const ON_String & p_localpath, 
								  const ON_String & p_name, 
								  ON_String & p_str) const;

	//Define or reset a line (define d as a polyline in hoops polyline) user option define in the segment :
	//if p_localpath!=0 			m_path / m_name  / p_localpath
	//if p_localpath==0 			m_path / m_name  / 
	int SetLine(const ON_String & p_localpath,  
								const ON_Line p_line); 

	//Define or reset a polyline user option define in the segment :
	//if p_localpath!=0 			m_path / m_name  / p_localpath
	//if p_localpath==0 			m_path / m_name  / 
	int SetPolyline(const ON_String & p_localpath,  
					const ON_Polyline p_PL);

	int SetPolygon(const ON_String & p_localpath,  
								const ON_Polyline p_PL);

	int SetPolyline(const ON_String & p_localpath,  
								const ON_2dPointArray p_PL);

	

	//Define or reset a curve user option define in the segment :
	//if p_localpath!=0 			m_path / m_name  / p_localpath
	//if p_localpath==0 			m_path / m_name  / 
	int SetCurve(const ON_String & p_localpath,  
				 const ON_Curve * p_curve);
		
	ON_Curve * GetCurve(const ON_String & p_localpath) const; 

	//return a polyline user option define in the segment :
	//if p_localpath!=0 			m_path / m_name  / p_localpath
	//if p_localpath==0 			m_path / m_name  / 
	int GetPolyline(const ON_String & p_localpath,  
					ON_Polyline & p_PL) const;

	int GetPolygon(const ON_String & p_localpath,  
					ON_Polyline & p_PL) const;

	//Define or reset some markers user option define in the segment :
	//if p_localpath!=0 			m_path / m_name  / p_localpath
	//if p_localpath==0 			m_path / m_name  / 
	int SetPoints(const ON_String & p_localpath,  
					const ON_3dPointArray p_pts);

	int GetPoints(const ON_String & p_localpath,  
				  ON_3dPointArray & p_pts); 

	//Define or reset a surface define in the segment :
	//if p_localpath!=0 			m_path / m_name  / p_localpath
	//if p_localpath==0 			m_path / m_name  / 
	int SetSurface(const ON_String & p_localpath,  
				   const ON_Surface * p_srf);

	ON_String GetFullPath(ON_String * p_localpath = NULL) const;

	
	//suppress from the hoops model tree
	void Suppress();

	int CopySegmentTo(const ON_String & p_localpath,  
								   const ON_String & p_SegmenttoCopy);

protected: 
	ON_String m_model; 
	ON_String m_name;

	int m_pdoc; //pointer unto the AMGUidoc given as an interger for code portibility
		
	ON_String SerializeCurve(const ON_String & p_localpath);

	ON_String SerializePolyline(const ON_String & p_localpath);
	// returns pointer t the char where reading should continue. 

	char* DeserializeCurve(const ON_String & p_localpath,  char* p_lp); 

	char* DeserializePolyline(const ON_String & p_localpath,  
								char* p_lp ); 

	ON_String SerializeUserOptions(const ON_String & p_localpath) ;

	char* DeserializeUserOptions(const ON_String & p_localpath, 
								char* p_lp);

	// returns pointer t the char where reading should continue. 
};

#endif // !defined(AFX_AMGHOOPSOBJECT_H__651B70BC_D005_4A19_97F6_C5D3A19F389C__INCLUDED_)
