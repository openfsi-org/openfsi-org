// AMGTriangulation.h: interface for the AMGTriangulation class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGTriangulation_DEC2005
#define AMGTriangulation_DEC2005

#include "AMGHoopsObject.h"

#include "opennurbs_extensions.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGTriangulation : public  AMGHoopsObject
{
public:
	int GetCorner(const ON_String & p_name, ON_2dPoint & p_pt );
	AMGTriangulation();
	AMGTriangulation(const ON_String & p_model,
								   const ON_String & p_name, 
								   const double * p_P = NULL, 
								   const double * p_E = NULL, 
								   const double * p_I = NULL,
								   const double * p_J = NULL,
								   const double * p_headW = NULL, 
								   const double * p_Twisthead = NULL,
								   const double * p_headslope = NULL,
								   const double * p_rake = NULL,
								   const double * p_diagonal = NULL,
								   const double * p_2dLeechLength = NULL,
								   const int * p_featidtack = NULL, 
								   const int * p_featidclew = NULL,
								   const int * p_featidheadfwd = NULL,
								   const int * p_featidheadaft = NULL,
								   const ON_2dPoint * p_tack    = NULL,
								   const ON_2dPoint * p_clew    = NULL,
								   const ON_2dPoint * p_headfwd = NULL,
								   const ON_2dPoint * p_headaft = NULL);

	virtual ~AMGTriangulation();

	ON_String Serialize();
	// returns pointer t the char where reading should continue. 
	char* Deserialize(char* p_lp );
	double GetP() const;
	double GetE() const; 
	double GetI() const; 
	double GetJ() const; 
	double GetHeadWidth() const;
	double GetHeadTwist() const;

	double GetHeadSlope() const;
	double GetRake() const; 
	double GetDiagonal() const;
	double Get2DLeechLength() const;

	int ExportToRelax(ONX_Model & p_3dmModel); 
	
	#ifdef _AMG2005
	ProError EditTriangulation();
	#endif
};

#endif // AMGTriangulation_DEC2005
