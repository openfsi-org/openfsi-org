// AMGRelax.h: interface for the AMGRelax class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGRelax_DEC2005
#define AMGRelax_DEC2005

#include "AMGHoopsObject.h"
#include "opennurbs_extensions.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGRelax : public  AMGHoopsObject
{
public:
	AMGRelax();
	AMGRelax(const ON_String & p_path,
		     const ON_String & p_name);
	
	virtual ~AMGRelax();
	
	
	int SetGridDensity(const double & p_val);
	int GetGridDensity(double &p_val) const;

	int SetSubstrate(const ON_String & p_str);
	int GetSubstrate(ON_String & p_str) const;

	int ExportSettings(ONX_Model & p_3dmModel);

};

#endif // AMGRelax_DEC2005
