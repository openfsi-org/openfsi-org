//
// Copyright (c) 2000 by Tech Soft America, LLC.
// The information contained herein is confidential and proprietary to
// Tech Soft America, LLC., and considered a trade secret as defined under
// civil and criminal statutes.  Tech Soft America shall pursue its civil
// and criminal remedies in the event of unauthorized use or misappropriation
// of its trade secrets.  Use of this information by anyone other than
// authorized employees of Tech Soft America, LLC. is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
// $Header: /files/homes/master/cvs/hoops_master/hoops_mvo/source/AMGOpJoinTape.h,v 1.19 2004/06/04 22:32:45 heppe Exp $
//

//	AMGOpJoinTape.h : interface of the AMGOpJoinTape class
//
//	Draws a polyline in the view plane based on pointer input
//	Polyline points are stored in protected member variables and ready after 
//	OnLButtonDblClk method has been called. 
//	Derived Classes should call this::OnLButtonDblClk in their overloaded implementation.
//	Polyline points are in window space.


#ifndef _AMGOPJOINTAPE_H
#define _AMGOPJOINTAPE_H


#include "HTools.h"
#include "HBaseOperator.h"

#include "HOpSelectAperture.h"

#include "dovestructure.h"
#include "AMGLevel.h"

#include "opennurbs.h"

//! The AMGOpJoinTape class computes a selection list for objects inside a user-defined polygonal area.
/*!
  AMGOpJoinTape employs all of the drawing functionality of HOpConstructPolyline to define a temporary, overlayed polygonal selection 
  area, and then maps the polygon information to the HOOPS routine HC_Compute_Selection_By_Polygon. The operation consists 
  of the following steps:
  <ol>
  <li>Left Button Down:				operation initiated, first point of the polygon recorded
  <li>No Button Down and Drag:		rubberband line segment to desired position of next point
  <li>Left Button Down:				next point in polygon recorded, etc.
  <li>Left Button Double Click:		polygon completed, and flushed from scene, selection list computed, objects highlighted, operation ended
  </ol>
  More Detailed Description: see event methods 
  Functional Note:  This operator does not provide exact results for HOOPS' Shell and Mesh primitives.
*/
class  AMGOpJoinTape : public HOpSelectAperture
{
public:
	/*! constructor */
  	AMGOpJoinTape(HBaseView* view, int DoRepeat=0, int DoCapture=1);
	~AMGOpJoinTape();

	/*!
		\return A pointer to a character string denoting the name of the operator  'AMGOpJoinTape'
	*/
	virtual const char * GetName();  

	HBaseOperator * Clone(); /*! returns a pointer to a copy of the operator */

	int SetWidth(const double & p_width);
	int SetSegmentTapes(const ON_String & p_seg);

	/*!
		OnLButtonDblClk passes the polyline array from HOpConstructPolyline::OnLButtonDblClk into the HOOPS routine
		HC_Insert_Polygon and inserts the polygon into the scene.  The polyline information is also passed to
		Compute_Selection_By_Polygon.  Currently selected items are highlighted, and items previously highlighted are
		de-selected.
		\param event An HEventInfo object containing information about the current event.
		\return A value indicating the result of the event handling.
	*/
 	virtual int OnLButtonDblClk(HEventInfo &event);

	/*!
		OnLButtonDown initiates the polygon selection mechanism.
		\param event An HEventInfo object containing information about the current event.
		\return A value indicating the result of the event handling.
	*/
	virtual int OnLButtonDown(HEventInfo &event); 

	int JoinTapes();
	
private:
		
	bool	m_bFirstTimeThrough;	// bool used to determine if we flush out the selection polygon

	ON_String m_segTapes;
	ON_String m_segFirstTape;
	ON_String m_segSecondTape;
	
	BOOL m_fistTapeSet;
};
#endif
