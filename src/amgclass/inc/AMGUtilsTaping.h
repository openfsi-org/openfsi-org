// AMGUtilsTaping.h: interface for the AMGmfcUtils class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AMGUtilsTaping_Feb06)
#define AMGUtilsTaping_Feb06

#include "AMGLevel.h"
#include "dovestructure.h"
#ifndef linux	// ph march 2007
#include "AMGTape2.h"
 #endif
#define AMG_TAPINGSTARTEGY_DEFAULT     0
#define AMG_TAPINGSTARTEGY_XDECREASING 1
#define AMG_TAPINGSTARTEGY_NEIGHBOR    2

#define AMG_TAPEALONGCURVE_OK 1
#define AMG_TAPEALONGCURVE_SMALL_RADIUS 2
#define AMG_TAPEALONGCURVE_SHORT_CURVE 3
#define AMG_TAPEALONGCURVE_FAIL 0


// return the id of the fisrt character of the substring p_Substr in pstr 
// return -1 if it cannot find p_Substr in p_str
int FindSubString(const ON_String & p_str, const ON_String & p_Substr); 


int AMGDrawPointFromPoint(FILE * fp, 
						const ON_2dPoint & p_StartPoint, //Position of the head before drawing
						const ON_Object * p_pt, 
						int & p_ID,
						ON_2dPoint & p_EndPoint,   ///Position of the head after drawing
						const double & p_CriticalDistance, // minimum distance to use PRINT1, 
						const double & p_scalefactor = 1.0, 
						const int         p_pPDF = NULL,
						ON_Xform  *       p_pTransToPdf = NULL);  
#ifdef AMGUI_2005
int AMGDrawPointFromMark(FILE * fp, 
							 const ON_2dPoint & p_StartPoint,
							AMGTape2 p_marl, //Mark define as a AMGTape2 Object
								int & p_ID,
								ON_2dPoint & p_EndPoint, 
							const double & p_CriticalDistance, 
							const ON_BoundingBox * p_Bb = NULL, //only add the point if the point is included in the bounding box 
							const float * p_transformationMatrix = NULL, //Transformation matrix
							const double & p_scalefactor = 1.0, 
							const int         p_pPDF = NULL,
							ON_Xform  *       p_pTransToPdf = NULL); 
#endif 
int AMGDrawPointsFromCurve(FILE * fp, 
							const ON_2dPoint & p_StartPoint, //Position of the head before drawing
							const ON_Object * p_crv, 
							int & p_ID,
							const double & p_StepL, ///distance between 2 points on along the curve
							ON_2dPoint & p_EndPoint) ; ///Position of the head afetr drawing

//returns the length of the new tape (0 if no tape found)
double AMGLayDownTapeFromObject(FILE * fp, 
							const ON_2dPoint & p_StartPoint, //Position of the head before taping
							const ON_Object * p_obj, 
							int & p_ID,
							ON_2dPoint & p_EndPoint, ///Position of the head afetr taping
							const double & p_LengthTapeMin, 
							const ON_Interval & p_AlphaBounds, 
							int	 p_TapingStrategy,///Position of the head afetr drawing		
							int p_idtape);  

double AMGLayDownTapeFromCurveWithPDF(FILE * fp, 
							const ON_2dPoint & p_StartPoint, //Position of the head before taping
							const ON_Curve * p_crv, 
							int & p_ID,
							ON_2dPoint & p_EndPoint, ///Position of the head afetr taping
							const double & p_LengthTapeMin, 
							const ON_Interval & p_AlphaBounds, 
							int	p_TapingStrategy, 
							const float * p_transformationMatrix = NULL,
							const double & p_scalefactor = 1.0, 
							const int         p_pPDF = NULL,
							ON_Xform  *       p_pTransToPdf = NULL, 
							const double &    p_width = 0); 

double AMGLayDownTapeFromSurface(FILE * fp, 
							const ON_2dPoint & p_StartPoint, //Position of the head before taping
							const ON_Surface * p_srf, 
							int & p_ID,
							ON_2dPoint & p_EndPoint, ///Position of the head afetr taping
							const double & p_LengthTapeMin, 
							const ON_Interval & p_AlphaBounds, 
							int	p_TapingStrategy);  

int SetTapesAlongCurve(const ON_Curve * p_pcurve,
					  double * p_tstart, 
					  double * p_tend, 
					  const double & p_dAlpha, //deviation in radian
					  const double & p_MaxLength, 
					   const double & p_MinLength, 
					  const double & p_ButtOverlap, 
					  const double & p_width,
					  const ON_String & p_segment);

#ifdef AMGUI_2005
int DrawTapeInSegment(const ON_String & p_seg, 
					  const ON_2dPoint & p_start, 
					  const double     & p_orient, //direction in degree 0 == Y clockwise, 
					  const double     & p_len, //length in mm
					  const double & p_width);
					  
int GetStartEndWidth(const ON_String & p_seg, 
					 ON_2dPoint & p_start, 
					 ON_2dPoint & p_end, 
					 double & p_width);

int GetStartEndWidth(const HC_KEY & p_key , //key on a polygon
					 ON_2dPoint & p_start, 
					 ON_2dPoint & p_end, 
					 double & p_width, 
					 HC_KEY * p_Kseginclude = NULL); // key on the segment which include de tape (for modeling matrix)

double AMGLayDownTapeFromPolyline(FILE * fp, 
								const ON_2dPoint & p_StartPoint,
								const HC_KEY p_key, //key on a polygon
								int & p_ID,
								ON_2dPoint & p_EndPoint, 
								const double & p_LengthTapeMin, 
								const ON_Interval & p_AlphaBounds, ///Position of the head afetr drawing		
								int	p_TapingStrategy= AMG_TAPINGSTARTEGY_DEFAULT); 
double AMGLayDownTapeFromPolygon(FILE * fp, 
							 const ON_2dPoint & p_StartPoint,
								const AMGTape2 & p_tape, //Tape
								int & p_ID,
								ON_2dPoint & p_EndPoint, 
								const double & p_LengthTapeMin, 
								const ON_Interval & p_AlphaBounds, ///Position of the head afetr drawing		
								int	p_TapingStrategy = AMG_TAPINGSTARTEGY_DEFAULT, 
								const float * p_transformationMatrix = NULL,  //Transformation matrix
								const double & p_scalefactor = 1.0, 
								const int         p_pPDF = NULL,
								ON_Xform  *       p_pTransToPdf = NULL);
//Search p_Crvs for the closest tape to draw. take care of the direction
// return the next tape to draw and remove it from p_Crvs
//p_Pos the satring point
AMGTape2 GetNextTapeToProcess(const ON_2dPoint & p_Pos, 
							ON_ClassArray<AMGTape2> & p_Tapes,
							  int	& p_TapingStrategy, 
							  const ON_Interval & p_AlphaBounds,  //to specify the strategy we want to use
							  const float * p_transformationMatrix);  //to specify the strategy we want to use
							  
int AMGIsShortTape(	const HC_KEY p_key, //key on a polygon
				const double & p_LengthTapeMin);


// if the point p_pt is on the tape return 1 and gives a value to alpha 
// else return 0
//return the direction in degrees 
// cockwise dierection Y == 0 == (0,1) (vertical)
//return the direction in degrees nclockwise direction 0 vertical upward (-180 to 180)
int GetDirectionAt(const ON_String & p_seg, 
			   const ON_2dPoint & p_pt,
			   double &            p_Alpha); 

// if the point p_pt is on the tape return 1 and gives a value to alpha 
// else return 0
//return the direction in degrees 
// cockwise dierection Y == 0 == (0,1) (vertical)
//return the direction in degrees nclockwise direction 0 vertical upward (-180 to 180)
int GetDirectionAt(const HC_KEY & p_key, 
			   const ON_2dPoint & p_pt,
			   double &            p_Alpha);

int GetTapeLength(const ON_String & p_seg, 
					 double & p_len);

int ReportTapeCount(const ON_String & p_seg,
					double & p_lenght, 
					int & p_nbtapes);

//retrun 1 if at least one tape covers the point p_point
//otherwise returns 0
int IsOnTapes(const ON_ClassArray<AMGTape2> p_tapes, const ON_2dPoint & p_point);

//retrun 1 if at least one of the corners is in the bounding box
//otherwise returns 0
// p_key key on polygons 
int IsInBoundingBox(const HC_KEY & p_key, 
					const ON_BoundingBox & p_BB, 
					HC_KEY * p_KeySeg = NULL); // Owner segment to deal witht the inclusion an the modeling matrices

int TapePolygonGeneric(ON_PolylineCurve p_PLc, 
				const ON_String & p_panelname, 
				const AMGLevel &  p_level, 
				const ON_String & p_segPanels, 
				const ON_String & p_segTapes,
				dovestructure *   p_pdove, 
				const double & p_MaxDev,
							const double & p_LatOverlap_mm,
								const double & p_MinLength,
								const double & p_MaxLength,
								const double & p_TapeWith,
								const double & p_ButtOverlap,
								const int & p_stacktype);

int ComputeBoundingBox(const ON_String & p_seg, ON_BoundingBox & p_Bb);


#endif


//return the direction in degrees 
// cockwise dierection Y == 0 == (0,1) (vertical)
//return the direction in degrees nclockwise direction 0 vertical upward (-180 to 180)
double GetDirection(const ON_2dPoint & p_start,
					const ON_2dPoint & p_end);

//WORKS in degrees
	BOOL IsInAlphaAboundaries(const double & p_alpha, 
							 const ON_Interval p_AlphaBounds);


#endif // !defined(AMGUtilsTaping_Feb06)



