#
# openfsi compiler configuration
#
    message("this is local/home/r3/Documents/qt/openFSI/openfsicompilerconfig.pri  ")
# We try to regroup the compiler options which are common to all the sub-projects
QT-=qt3support
QT-= WEBKIT
QT -= xml
QT +=widgets  # for qt > 4.8
## graphics system:
DEFINES += RXQT
#
# 1) detect the presence of the intel compilers
#
linux:{
!system(ifort --version){
        error( Please install Intel Fortran compiler and try again)
}
}
 linux-g++ :{
    system(icpc --version) {
        message( intel C compiler found - are you sure you want to compile with g++ -go 'qmake -r -spec linux-icc-64' )
    }
 }

# 2)############## detect  32-bit#####################
## this is long-winded but isEqual seems to fail otherwise
MYSIXTYFOUR = x86_64
MYHOSTARCH = $$QMAKE_HOST.arch
isEqual(MYHOSTARCH,$$MYSIXTYFOUR) {
   CONFIG +=rx64
 #   message("build for x86_64")
}
else {
     CONFIG +=rx32
    message("`$$MYHOSTARCH` Doesnt contain `x86_64` so compile as 32bit")

}
# 3) ################### H5FDDSM #########################
## the H5FDDSM interface (see www.http://nextmuse.cscs.ch/)  is tricky to build
## but very nice once it is sorted because it gives real-time update of the model in paraview
## if you have succeeded in building the libraries, you can try placing your own machine's name here.

MYMACHINE = r3-W860CU
MYHOST = $$QMAKE_HOST.name

## peter temp - for valgrind
#            QMAKE_CXXFLAGS+=  -g -fPIC
#            F90_CFLAGS +=  -g
#            QMAKE_LFLAGS += -g -fPIC
#            CONFIG -=coin3d
##end
isEqual(MYHOST ,$$MYMACHINE) {
    message ("this is Peters machine, so lets link H5FDDSM and do ODE dev")
    CONFIG += H5FDDSM
    CONFIG += ODETESTING
   ## CONFIG += PETERDEV
    DEFINES += LINK_H5FDDSM  ODETESTING
} else {
   message ("building without H5FDDSM")
 }
###############################profile guided optimisation ##################
  #   CONFIG +=profgen
# or
 #   CONFIG +=profuse

#Then in the sub-projects to be PGO-ed, you go

never: {
################ PGO details - switched on/off in our .pri file
profgen: {
GOAT
#  for PGO either -prof-gen or -prof-use
CONFIG(release,debug|release):          QMAKE_CFLAGS+=  -prof-gen  -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          QMAKE_CXXFLAGS+=  -prof-gen  -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          F90_CFLAGS += -prof-gen  -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          QMAKE_LFLAGS +=  -prof-gen  -prof-dir/home/r3/Documents/qt/openFSI

}
profuse:{
#  -prof-file ../myprof
CONFIG(release,debug|release):          QMAKE_CFLAGS+=  -prof-use -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          QMAKE_CXXFLAGS+=  -prof-use -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          F90_CFLAGS += -prof-use  -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          QMAKE_LFLAGS += -prof-use  -prof-dir/home/r3/Documents/qt/openFSI

}
}
# 4) ################### common defines########################



DEFINES	+=	UNICODE
DEFINES	+=	_UNICODE
DEFINES	+=	FORTRANLINKED

DEFINES +=  ANSI_DECLARATORS
DEFINES +=  TRILIBRARY

DEFINES += SAILPLUSPLUS_LIBRARY  # to build with sailpp

unix:{
DEFINES	+=	linux
}
win32:{
DEFINES	+=	_CRT_SECURE_NO_WARNINGS
DEFINES	+=	_AFXDLL
DEFINES	+=	NOMYSQL
}

coin3d:{
 DEFINES	+=	COIN3D
}
debug: CONFIG += warn_on
release: CONFIG += warn_off

rx64: {
    linux-ic*:{ # 64bit linux_icc*
 #       QMAKE_CXXFLAGS_RELEASE +=-parallel -qopt-report=1 -qopt-report-phase=par -O2
        QMAKE_CXXFLAGS_RELEASE +=-parallel -O2

        QMAKE_CXXFLAGS  += -xHost -m64 -Wno-return-type
        QMAKE_CXXFLAGS  += -diag-disable 271,10148,898,310,161,192,3180,10382,10006
    }
    linux-g*:{ # 64bit linux_g*
        QMAKE_CXXFLAGS_RELEASE += -O2 -m64
        QMAKE_CXXFLAGS  += -m64
    }
    win32-g++:{
        error( we havent coded this configuration  win32-g++)
        }
    else {
        win32: {
        message( (rx64) we havent coded this configuration win32)
        }
    }
}
rx32: {
    linux-ic*:{ # 32bit linux_icc*
        QMAKE_CXXFLAGS_RELEASE +=-parallel -qopt-report=1 -qopt-report-phase=par -xHost -O2 -m32
         QMAKE_CXXFLAGS  += -Wno-return-type -diag-disable 10148,271,898,310,161,192,3180,10382
    }
    linux-g*:{ # 32bit linux_g*
        QMAKE_CXXFLAGS_RELEASE += -O2 -m32
        QMAKE_CXXFLAGS  += -m32
    }
    win32-g++:{
        error( we havent coded this configuration  win32-g++)
        }
    else {
        win32: {
        error((rx32) we havent coded this configuration  win32)
        }
    }
}

# 4) ######################intel M K L setup#########################


#by default, ifort links - dynamically-  all the required libraries - except the F95 glue code in -lmkl_lapack95_lp64
# which you have to give explicitly.
 # if you want static linking (which makes it easier to move the executable to another machine)
# you have to specify the intel libraries yourself.
# For how to do that, go to http://software.intel.com/en-us/articles/intel-mkl-link-line-advisor
# and set the variable PETERSINTELLIBS to the link line that it gives.

win32: {
        F90_CFLAGS +=  /Qmkl
        QMAKE_LFLAGS += /Qmkl
}

rx64: {
     linux-ic*:{
        system(amplxe-cl --version){ ## check whether intels vtune is installed -
            message (  linux-icc 64-bit build  WITH -g for vtune)
            QMAKE_CXXFLAGS+=  -g
            F90_CFLAGS +=  -g
            QMAKE_LFLAGS += -g

## either this
          message ( dynamic link of MKL (LP64) )
        PETERSINTELLIBS += -lmkl_lapack95_lp64  # this is for LP64 (32-bit interface layer) - dynamic-link
# or this, for static link
 #           message (static link of MKL (LP64) )# the following copied from intel linklineadvisor
#           QMAKE_LFLAGS +=  -static-intel
#           PETERSINTELLIBS += $(MKLROOT)/lib/intel64/libmkl_lapack95_lp64.a
#           PETERSINTELLIBS += -Wl,--start-group  $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a
#           PETERSINTELLIBS += $(MKLROOT)/lib/intel64/libmkl_intel_thread.a $(MKLROOT)/lib/intel64/libmkl_core.a
#           PETERSINTELLIBS += -Wl,--end-group   -liomp5 -lpthread -lm
        }
else { ## no vtune
         message ( linux-icc 64-bit build- dynamic link of MKL (LP64) (no vtune) )
      PETERSINTELLIBS += -lmkl_lapack95_lp64  # this is for LP64 (32-bit interface layer) - dynamic-link
###   message ( try static link of MKL (LP64) )# the following copied from intel linklineadvisor
#        QMAKE_LFLAGS +=  -static-intel
#        PETERSINTELLIBS += $(MKLROOT)/lib/intel64/libmkl_lapack95_lp64.a
#        PETERSINTELLIBS += -Wl,--start-group  $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a
#        PETERSINTELLIBS += $(MKLROOT)/lib/intel64/libmkl_intel_thread.a $(MKLROOT)/lib/intel64/libmkl_core.a
#        PETERSINTELLIBS += -Wl,--end-group -liomp5 -lpthread -lm
    } ## no vtune

#######################for TBB ###################################


#        CONFIG(release,debug|release):  TBBLIB = -ltbb  # ??-tbb for icc
#        CONFIG(debug,debug|release):    TBBLIB  = -ltbb_debug
#        PETERSINTELLIBS +=  -ltbb -lrt

######################## end tBB#############################

#############a test to try to resolve meyermatrix hang-up
QMAKE_CXXFLAGS+=-threads -pthread -recursive -parallel -reentrancy=threaded
F90_CFLAGS += -reentrancy threaded  -threads
QMAKE_LFLAGS +=  -parallel# -reentrancy threaded  -threads  -recursive

################################################

CONFIG(debug,debug|release):            QMAKE_CXXFLAGS+= -g -ftrapuv -mkl -fstack-protector-all
CONFIG(debug,debug|release):            F90_CFLAGS += -ftrapuv -mkl -traceback -check all -fstack-protector-all
CONFIG(debug,debug|release):            QMAKE_LFLAGS += -ftrapuv -mkl  -opt-matmul -traceback -fstack-protector-all

CONFIG(release,debug|release):          QMAKE_CXXFLAGS+=  -mkl=parallel
CONFIG(release,debug|release):          F90_CFLAGS += -mkl=parallel
CONFIG(release,debug|release):          QMAKE_LFLAGS += -mkl=parallel -opt-matmul




    } # linux-ic*:
     linux-g*:{
        message (  linux_g* 64-bit build - DODGY- not tested)

        CONFIG(debug,debug|release):            QMAKE_CXXFLAGS+= -g -mkl
        CONFIG(debug,debug|release):            F90_CFLAGS += -mkl -ftrapuv -traceback -check all
        CONFIG(debug,debug|release):            QMAKE_LFLAGS += -mkl  -traceback -check all -opt-matmul# -openmp-stubs -O0

        CONFIG(release,debug|release):          QMAKE_CXXFLAGS+=  -mkl=parallel
        CONFIG(release,debug|release):          F90_CFLAGS += -mkl=parallel
        CONFIG(release,debug|release):          QMAKE_LFLAGS += -mkl=parallel


        LINKTAIL += -lmkl_lapack95_lp64  # this is for LP64 (32-bit interface layer)
    }
    win32:{
        win32: INCLUDEPATH += "C:/Program Files (x86)/Intel/ComposerXE-2011/mkl/include" # DODGY
        win32:     LIBS += mkl_lapack95.lib
        message( we havent coded this configuration win32(5))
    }
    win32:!win32-g++:{
        message( we havent coded this configuration   win32:!win32-g++:)
    }
    win32-g++:{
        error( we havent coded this configuration  win32-g++)
        }
    else {
        win32: {
        message((3) we havent coded this configuration win32)
        }
    }
}
rx32: {

    linux-ic*:{ # 32bit linux_icc*
       release: QMAKE_CXXFLAGS+=  -mkl=parallel
       release:  F90_CFLAGS += -mkl=parallel
        release: QMAKE_LFLAGS += -mkl=parallel
        LINKTAIL += -lmkl_lapack95_lp64  # this is for LP64 (32-bit interface layer)
        message((4) we havent coded this configuration win32)
    }
    linux-g*:{ # 32bit linux_g*
        QMAKE_CXXFLAGS+=  -mkl=parallel
        F90_CFLAGS += -mkl=parallel
        QMAKE_LFLAGS += -mkl=parallel

        LINKTAIL += -lmkl_lapack95_lp64  # this is for LP64 (32-bit interface layer)
        message((5) we havent coded this configuration win32)
    }
    win32-g++:{
        error( we havent coded this configuration  win32-g++)
        }
    else {
        win32: {
        error((4) we havent coded this configuration  win32)
        }
    }
}


# 5) ###################### whatever else #########################
#spare:{
#rx64: {
#    linux-ic*:{  # 64bit linux_icc*
#        error( we havent coded this configuration )

#    }
#    linux-g*:{ # 64bit linux_g*
#        error( we havent coded this configuration 64bit linux_g*)
#    }

#    win32-g++:{
#        error( we havent coded this configuration  win32-g++)
#        }
#    else {
#        win32: {
#        error((5) we havent coded this configuration win32)
#        }
#    }
#}
#rx32: {
#    linux-ic*:{ # 32bit linux_icc*
#        error( we havent coded this configuration linux-ic*)

#    }
#    linux-g*:{ # 32bit linux_g*
#        error( we havent coded this configuration 32bit linux_g*)

#    }

#    win32-g++:{
#        error( we havent coded this configuration  win32-g++)
#        }
#    else {
#        win32: {
#        error( (6) we havent coded this configuration  win32)
#        }
#    }
#}




