#ifndef RXQTDIALOGS_H
#define RXQTDIALOGS_H

EXTERN_C int akm_update_display_(int *what);
EXTERN_C int CloseDown() ;
EXTERN_C int SetMsgText(const char *text,class QObject *const client=0) ; /* deals with LHS only */
EXTERN_C int SetMsgText2(const char *text, const int right, class QObject *const client=0) ;
EXTERN_C int setStatusBar(const QString t,class QObject *const client=0);
#endif // RXQTDIALOGS_H
