# -------------------------------------------------
# Project created by QtCreator 2011-04-16T14:42:20
# -------------------------------------------------
QT += core
QT += gui
QT += network
QT += opengl
QT += sql

TARGET = rxkernel
TEMPLATE = lib
CONFIG += staticlib

include (../openfsicompilerconfig.pri)

DEFINES += RXKERNEL_LIBRARY

DEFINES +=  RXLIB
########DEFINES +=  RLX_64
####DEFINES +=  RX_ICC



INCLUDEPATH += ../src/preprocess
INCLUDEPATH += ../src/post/inc
INCLUDEPATH += ../src/stringtools
INCLUDEPATH += ../thirdparty/opennurbs
INCLUDEPATH += ../thirdparty/triangle
INCLUDEPATH += ../thirdparty/MTParserLib
INCLUDEPATH += ../src/x/inc
INCLUDEPATH += ../rxserver
INCLUDEPATH += ../rxcommon
INCLUDEPATH += ../src/infrastructure
INCLUDEPATH += ../src/ODE

unix:INCLUDEPATH += /usr/include/mysql
win32: INCLUDEPATH +="C:/Program Files/MySQL/MySQL Connector C 6.0.2/include"



SOURCES += ../src/post/script.cpp \
    ../src/motifmain/mainstuf.cpp \
    rxqtdialogs.cpp \
    ../src/preprocess/rxsubwindowhelper.cpp \
    ../src/motifmain/SaveSail.cpp \
    ../src/motifmain/OpenBuildFile.cpp \
    ../src/motifmain/drop.cpp \
    ../src/motifmain/opentext.cpp \
    ../src/motifmain/CalcNow.cpp \
    ../src/motifmain/question.cpp \
    ../src/post/postutil.cpp \
    ../src/post/intPress.cpp \
    ../src/post/drawwind.cpp \
    ../src/post/drawforces.cpp \
    ../src/post/CheckAll.cpp \
    OpenView.cpp \
    ../src/infrastructure/rxmirrorsimple.cpp \
    ../src/infrastructure/rxmirrorqt.cpp \
    ../src/infrastructure/rxdblinkqt.cpp \
    ../src/infrastructure/rxwindgeometry.cpp \
    ../src/infrastructure/rxdblinklongqt.cpp


#   ../src/motifmain/traceback.cpp \

HEADERS += rxkernel.h \
    rxkernel_global.h \
    rxqtdialogs.h \
    ../src/preprocess/RXDatabaseI.h \
    ../src/preprocess/RelaxWorld.h \
    ../src/preprocess/RXCurve.h \
    ../src/preprocess/rxvectorfield.h \
    ../src/preprocess/rxsubwindowhelper.h \
    ../src/preprocess/rximportentity.h \
    ../src/preprocess/mainstuf.h \
    ../src/stringtools/stringtools.h \
    ../src/x/inc/ch_rowt.h \
    ../src/infrastructure/rxmirrorsimple.h \
    ../src/infrastructure/rxmirrorqt.h \
    ../src/infrastructure/rxdblinkqt.h \
    ../src/infrastructure/rxwindgeometry.h \
    rxqapplication.h \
    ../src/infrastructure/rxdblinklongqt.h


OTHER_FILES += \
    postprocessingDataStructures.txt


































