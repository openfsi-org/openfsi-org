#include "StdAfx.h"
//#include <QtGui/QApplication> //qt 4.8
#include <QApplication> // qt 5
#include <QFileDialog>
#include <QString>


#include "rxmainwindow.h"

#include <iostream>
#include <stdio.h>
using namespace std;
#include "rxsubwindowhelper.h"
#include "RXEntity.h"
#include "menuCB.h"
#include "etypes.h"
#include "CreateMsgArea.h"

#include "RXSail.h"
#include "getfilename.h"
#include "uselect.h"

#include "resolve.h"

#include "finishview.h"
#include "text.h"
#include "drawref.h"
#include "rxqtdialogs.h"

int akm_update_display_(int *what)
{
  if(!QCoreApplication::hasPendingEvents()  )
    return 0;
    QCoreApplication::processEvents (QEventLoop::AllEvents ) ;
    return 1;
}
int initTextWindows(void) {
    qDebug()<<"dummy initTextWindows"<<endl;
    return 0;
}

int Finish_All_Views(void){
    //   cout<<"dummy Finish_All_Views"<<endl;
    return 0;
}

int UpdateMenu(){
    cout<<"TODO: dummy UpdateMenu "<<endl;
    return 0;
}

int do_resolve_dialog(Graphic *g){

    cout<<"TODO: dummy do_resolve_dialog"<<endl;
    SAIL *s = g->GSail;
    assert(s);

    char*rs = Report_To_String(s->GetReportForm(),1);
    if(rs) {
        QString ss (rs);
        cout<< rs ; //     i=rxerror(rs,2);
        RXFREE(rs);
        QStringList ll = ss.split(QRegExp( "(O>|\bneeds\b|,)"));

    RXEntity_p ee = UserSelection( qPrintable(ll.value(3)), qPrintable(ll.value(4)),qPrintable(ss) , g) ;
    }

    return 0;
}
int Create_Custom_Button(const char*line) {

    qDebug()<<"TODO: dummy Create_Custom_Button"<<endl;
    return 0;
}
RXEntity_p UserSelection(const char *type,const char *name,const char *prompt, Graphic *g) {


    class RXSail  *s = g->GetSail();
    vector<RXEntity * >thelist;
    RXEntity_p e=0;
    vector<RXEntity * >::iterator it;
    size_t count=0;
    QString  qtype (type);
    QStringList qtypes = qtype.split(",");
    QStringList candidates;

    int t, i;
    for(i=0; i<qtypes.length();i++) {
        thelist.clear();
        t = INTEGER_TYPE(qPrintable(qtypes[i]));
        count+= s->MapExtractList(t ,  thelist,PCE_ISRESOLVED) ;
        for(it=thelist.begin(); it!=thelist.end();++it){
            e = *it;
            candidates.append(QString(e->name() )+ QString(",")+ QString(e->type() ) );
        }
    }
    candidates.sort();candidates.removeDuplicates();
    //  qDebug()<<"TODO: userselect for s="<<s->GetType().c_str()<< " t="<<type<<" n=,"<<name<<"  prompt = "<<prompt;
    //  qDebug()<<candidates;

    QString ee = rxWorker::UserChoice(prompt, candidates) ;// eg "seamcurve,zh1"
    if(ee .isEmpty())
        return e;
    qtypes=ee.split(",");
    if(qtypes.length()<2)
        return 0;
    e = s->Entity_Exists(qPrintable(qtypes[1]), qPrintable(qtypes[0]));
    return e;

}
int SetMsgText(const char *text,class QObject *const client)  /* deals with LHS only */
{
    if(!text) return 0;
    return rxWorker::setStatusBar( QString (text),client,1) ;

}

int SetMsgText2(const char *text, const int right, class QObject *const client)   /* deals with both only */
{

    if(!text) return 0;
    return rxWorker::setStatusBar( QString (text),client,0) ;
}
int setStatusBar(const QString t,class QObject *const client) {
    return SetMsgText(qPrintable(t),client);
}
int CloseDown( )   // pglobal function to be called in the worker thread.
{

    return rxWorker:: shutdown();
}
QString getfilename(QObject *client,const QString pattern,const QString prompt,const QString currentDir,const int ReadWrite)
//std::string getfilename(const char *pparent,const char *pattern,const char *prompt,const char*currentDir,int ReadWrite)
{
    assert(!cout.bad());
    QString fileName ;

    switch (ReadWrite){
    case PC_FILE_READ://g_rxmw

        fileName = rxWorker::rxOpenFileName(client, prompt,
                                            currentDir,
                                            pattern); //pattern sb more like "Images (*.png *.xpm *.jpg)");
        assert(!cout.bad());
        return fileName;
    case PC_FILE_WRITE:
        fileName = rxWorker::rxSaveFileName(client, prompt,
                                            currentDir,
                                            pattern);
        assert(!cout.bad());
        return fileName;
    case PC_FILE_DIR:
        fileName = rxWorker::rxDirName(client, prompt,
                                       currentDir,
                                       pattern);

        assert(!cout.bad());
        return fileName;
    default:
        assert("getfilename unknown flag"==0);
        return fileName;
    };

    return 0;
}
