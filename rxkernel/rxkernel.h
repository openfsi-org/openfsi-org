#ifndef RXKERNEL_H
#define RXKERNEL_H
#include <QString>
#ifdef linux
#define RXKERNELSHARED_EXPORT extern "C"
#else
#define RXKERNELSHARED_EXPORT extern  
#endif

RXKERNELSHARED_EXPORT int RXStart(int argc, char**argv, class QObject* TheWorld);
RXKERNELSHARED_EXPORT int RXExecute(const QString line,class QObject * owner);
RXKERNELSHARED_EXPORT int RXEnd();
extern "C" int RXPrint(class QObject *q, const char*s,const int level);
extern "C" int rxWorkerDisplayMsgBox(class QObject *q,const QString s , const int level);

// calls which the kernel makes
RXKERNELSHARED_EXPORT int RXMsgBox (const QString &s,QObject *client );

RXKERNELSHARED_EXPORT int RXStatusBar (const QString &s,QObject *client );
RXKERNELSHARED_EXPORT QString RXGetExistingFileName(QObject *client,
                            const QString caption,
                           const QString dir,
                           const QString filter);
RXKERNELSHARED_EXPORT QString RXGetSaveFileName(QObject *client,
                            const QString caption,
                           const QString dir,
                           const QString filter);
RXKERNELSHARED_EXPORT QString RXGetDirName(QObject *client,
                            const QString caption,
                           const QString dir,
                           const QString filter);

#endif // RXKERNEL_H
