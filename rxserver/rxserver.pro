# -------------------------------------------------
# Project created by QtCreator 2010-12-13T18:57:06
# -------------------------------------------------
QT += core \
    gui
QT += network
QT += opengl
QT += sql
QT += widgets
TARGET = rxserver
TEMPLATE = app

include (../openfsicompilerconfig.pri)

DEFINES += RLX
DEFINES += RXKERNEL_LIBRARY

unix:{
 #   message(" rxserver build unix")
    CONFIG += coin3d
    CONFIG += soqt
    LIBS += -lSoQt
    LIBS += -lCoin
}
else : {message(" rxserver build without soqt")}
QMAKE_LINK = ifort
CONFIG += thread

# to enable fortran on windows, go something like
#> "C:\Program Files (x86)\Intel\ComposerXE-2011\bin\ifortvars.bat"  intel64 

rxserver.depends = rxkernel src/rxGraphicsStub
rxserver.depends += thirdparty/hbio
rxserver.depends += thirdparty/MTParserLib
rxserver.depends += thirdparty/triangle
rxserver.depends +=  preprocessing
rxserver.depends +=  src/stringtools




SOURCES += \
    rxmainwindow.cpp \
    rxmulticlientserver.cpp \
    rxstdoutredirector.cpp \
    ../rxkernel/rxkernel.cpp \
    rxcommandlinebox.cpp \
    rxviewframe.cpp \
    rxqapplication.cpp \
    dialoguselect.cpp \
    rxservermain.cpp

HEADERS += rxmainwindow.h \
    rxmulticlientserver.h \
    ../rxcommon/StdRedirector.h \
    rxtcpsocket.h \
    rxstdoutredirector.h \
    ../rxkernel/rxkernel.h \
    rxcommandlinebox.h \
    rxviewframe.h \
    rxqapplication.h \
    ../src/stringtools/stringtools.h \
    dialoguselect.h \
    ../src/preprocess/RXGraphicCalls.h

FORMS += rxmainwindow.ui \
    dialoguselect.ui



INCLUDEPATH += ../rxcommon
INCLUDEPATH += ../rxkernel
INCLUDEPATH += ../src/preprocess
INCLUDEPATH += ../thirdparty/opennurbs
INCLUDEPATH += ../thirdparty/MTParserLib
INCLUDEPATH +=  ../src/stringtools
INCLUDEPATH += ../src/x/inc
INCLUDEPATH += ../src/preprocess
INCLUDEPATH += ../src/infrastructure


unix:{
    QMAKE_LFLAGS +=  -cxxlib -threads
#    QMAKE_LFLAGS += -opt-matmul replace by O3 in release
    QMAKE_LFLAGS +=  -recursive
    QMAKE_LFLAGS += -nofor_main -fexceptions  -threads
    rx64:     QMAKE_LFLAGS += -m64
    rx32:     QMAKE_LFLAGS += -m32
    QMAKE_CXXFLAGS_RELEASE += -openmp
    QMAKE_LFLAGS_RELEASE +=  -parallel -openmp
    QMAKE_CXXFLAGS_DEBUG += -openmp-stubs
    QMAKE_LFLAGS_DEBUG +=-check all -ftrapuv # -O3#  -parallel -openmp-stubs
}
win32:CONFIG(release, debug|release):{
QMAKE_LFLAGS += /Qopt-matmul /recursive /Qparallel /Qopenmp  /Qoption,link,/NODEFAULTLIB:"libcmt.lib"
QMAKE_LFLAGS +=/MANIFEST:NO
}
win32:CONFIG(debug, debug|release):{
QMAKE_LFLAGS += /Qopt-matmul /recursive /Qparallel /Qoption,link,/NODEFAULTLIB:"libcmt.lib"
QMAKE_LFLAGS += /Qoption,link,/NODEFAULTLIB:"msvcrt.lib"
QMAKE_LFLAGS += /Qoption,link,/NODEFAULTLIB:"msvcrt.lib"
QMAKE_LFLAGS += /Qoption,link,/NODEFAULTLIB:"mfc80u.lib"
QMAKE_LFLAGS += /Qoption,link,/NODEFAULTLIB:"mfcs80u.lib"
}



DEPENDPATH += ../rxkernel
unix:{
    LIBS += -L../rxkernel     -lrxkernel
    LIBS += -L../preprocessing     -lpreprocessing
    LIBS += -L../src/rxGraphicsStub -lrxGraphicsStub
    LIBS += -L../thirdparty/hbio -lhbio
    LIBS += -L../thirdparty/triangle -ltriangle
    LIBS += -L../src/stringtools -lstringtools
    LIBS += -L../thirdparty/MTParserLib  -lMTParserLib
    LIBS += -L../thirdparty/sailpp  -lsailplusplus

    LIBS += $$PETERSINTELLIBS
#    LIBS +=-L$(TBBROOT)/lib/intel64 -ltbb

}
win32-g++:{
win32-g++:CONFIG(debug, debug|release): {
    LIBS += ../rxkernel/debug/librxkernel.a
    LIBS += ../ofsifortran/debug/libofsifortran.a
    LIBS += ../preprocessing/debug/libpreprocessing.a
    LIBS += ../src/rxGraphicsStub/debug/librxGraphicsStub.a
    LIBS +=  ../thirdparty/hbio/debug/libhbio.a
    LIBS += ../thirdparty/triangle/debug/libtriangle.a
    LIBS += ../src/stringtools/debug/libstringtools.a

    LIBS += ../thirdparty/MTParserLib/debug/libMTParserLib.a
    LIBS +=  ../thirdparty/sailpp/debug/libsailplusplus.a
    LIBS += ../src/stringtools/debug/libstringtools.a
}
win32-g++:CONFIG(release, debug|release): {
    LIBS += ../rxkernel/release/librxkernel.a
    LIBS += ../ofsifortran/release/libofsifortran.a
    LIBS += ../preprocessing/release/libpreprocessing.a
    LIBS += ../src/rxGraphicsStub/release/librxGraphicsStub.a
    LIBS +=  ../thirdparty/hbio/release/libhbio.a
    LIBS += ../thirdparty/triangle/release/libtriangle.a
    LIBS += ../src/stringtools/release/libstringtools.a

    LIBS += ../thirdparty/MTParserLib/release/libMTParserLib.a
    LIBS +=  ../thirdparty/sailpp/release/libsailplusplus.a
    LIBS += ../src/stringtools/release/libstringtools.a
}
 }
 else {
win32:CONFIG(release, debug|release):{ # this is a big guess and it doesnt work anyway.
    LIBS += ../rxkernel/debug/rxkernel.lib
    LIBS += ../preprocessing/release/preprocessing.lib
    LIBS += ../src/rxGraphicsStub/release/rxGraphicsStub.lib
    LIBS +=  ../thirdparty/hbio/debug/hbio.lib
    LIBS += ../thirdparty/triangle/debug/triangle.lib
     LIBS += ../src/stringtools/debug/stringtools.lib

    LIBS += ../thirdparty/MTParserLib/debug/MTParserLib.lib
    LIBS +=  ../thirdparty/sailpp/debug/sailplusplus.lib

    LIBS += ../src/stringtools/debug/stringtools.lib
    LIBS += rpcrt4.lib
}
win32:CONFIG(debug, debug|release):{ # this is a big guess and it doesnt work anyway.
    LIBS += ../rxkernel/debug/rxkernel.lib
    LIBS += ../ofsifortran/debug/ofsifortran.lib

    LIBS += ../preprocessing/debug/preprocessing.lib
    LIBS += ../src/rxGraphicsStub/debug/rxGraphicsStub.lib
    LIBS +=  ../thirdparty/hbio/debug/hbio.lib
    LIBS += ../thirdparty/triangle/debug/triangle.lib
    LIBS += ../src/stringtools/debug/stringtools.lib

    LIBS += ../thirdparty/MTParserLib/debug/MTParserLib.lib
    LIBS +=  ../thirdparty/sailpp/debug/sailplusplus.lib
   LIBS += -L$$OUT_PWD/../thirdparty/opennurbs/debug/ -lopennurbs

    LIBS += ../src/stringtools/debug/stringtools.lib
     LIBS += rpcrt4.lib
}
}


      LIBS +=  $${LINKTAIL}

OTHER_FILES += \
    GLCnotes.txt \
    ../src/thirdparty/soqttest/myfirstsoqt/soqtdev.txt \
    ../readme.txt \
    ../ofsidata.tar.gz


unix:{
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../thirdparty/opennurbs/release/ -lopennurbs
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../thirdparty/opennurbs/debug/ -lopennurbs
else:symbian: LIBS += -lopennurbs
else:unix: LIBS += -L$$OUT_PWD/../thirdparty/opennurbs/ -lopennurbs

INCLUDEPATH += $$PWD/../thirdparty/opennurbs
DEPENDPATH += $$PWD/../thirdparty/opennurbs
win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += ../thirdparty/opennurbs/debug/libopennurbs.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += ../thirdparty/opennurbs/debug/libopennurbs.a
else:win32:CONFIG(release, debug|release): PRE_TARGETDEPS += ../thirdparty/opennurbs/release/opennurbs.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += ../thirdparty/opennurbs/debug/opennurbs.lib
else:unix:!symbian: PRE_TARGETDEPS += ../thirdparty/opennurbs/libopennurbs.a
}

INCLUDEPATH += $$PWD/../preprocessing
DEPENDPATH += $$PWD/../preprocessing

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += ../preprocessing/release/preprocessing.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += ../preprocessing/debug/preprocessing.lib
else:unix:!symbian: PRE_TARGETDEPS += ../preprocessing/libpreprocessing.a





INCLUDEPATH += $$PWD/../src/rxGraphicsStub
DEPENDPATH += $$PWD/../src/rxGraphicsStub

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += ../src/rxGraphicsStub/release/rxGraphicsStub.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += ../src/rxGraphicsStub/debug/rxGraphicsStub.lib
else:unix:!symbian: PRE_TARGETDEPS += ../src/rxGraphicsStub/librxGraphicsStub.a


INCLUDEPATH += $$PWD/../rxkernel
DEPENDPATH += $$PWD/../rxkernel

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += ../rxkernel/release/rxkernel.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += ../rxkernel/debug/rxkernel.lib
else:unix:!symbian: PRE_TARGETDEPS += ../rxkernel/librxkernel.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../thirdparty/sailpp/release/ -lsailplusplus
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../thirdparty/sailpp/debug/ -lsailplusplus
else:symbian: LIBS += -lsailplusplus
#else:unix: LIBS += -L$$PWD/../thirdparty/sailpp/ -lsailplusplus

INCLUDEPATH += $$PWD/../thirdparty/sailpp
DEPENDPATH += $$PWD/../thirdparty/sailpp

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += ../thirdparty/sailpp/release/sailplusplus.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += ../thirdparty/sailpp/debug/sailplusplus.lib
else:unix:!symbian: PRE_TARGETDEPS += ../thirdparty/sailpp/libsailplusplus.a

LIBS +=  $${PETERSINTELLIBS}


H5FDDSM:{
    message ("             LINKING H5FDDSM             ")
    unix:!macx:!symbian: LIBS += -L$$PWD/../../../../software/paraView-icarus-3.14.1/org-build/bin/ -lXdmf

    INCLUDEPATH += $$PWD/../../../../software/paraView-icarus-3.14.1/org-build/bin
    DEPENDPATH += $$PWD/../../../../software/paraView-icarus-3.14.1/org-build/bin
    INCLUDEPATH += $$PWD/../../../../software/h5fddsm-0.9.9/build/bin
    DEPENDPATH += $$PWD/../../../../software/h5fddsm-0.9.9/build/bin
    INCLUDEPATH += $$PWD/../../../../software/hdf5-vfd-1.8.10/build/bin
    DEPENDPATH += $$PWD/../../../../software/hdf5-vfd-1.8.10/build/bin

    unix:!macx:!symbian: LIBS += -lmpich -lmpichcxx
    unix:!macx:!symbian: LIBS += -L$$PWD/../../../../software/h5fddsm-0.9.9/build/bin/ -lH5FDdsm
    unix:!macx:!symbian: LIBS += -L$$PWD/../../../../software/hdf5-vfd-1.8.10/build/bin/ -lhdf5 -lhdf5_hl
}
################ PGO details - switched on/off in our .pri file
profgen: {

    #  for PGO either -prof-gen or -prof-use
    CONFIG(release,debug|release):          QMAKE_CFLAGS+=  -prof-gen  -prof-dir/home/r3/Documents/qt/openFSI
    CONFIG(release,debug|release):          QMAKE_CXXFLAGS+=  -prof-gen  -prof-dir/home/r3/Documents/qt/openFSI
    CONFIG(release,debug|release):          F90_CFLAGS += -prof-gen  -prof-dir/home/r3/Documents/qt/openFSI
    CONFIG(release,debug|release):          QMAKE_LFLAGS +=  -prof-gen  -prof-dir/home/r3/Documents/qt/openFSI

}
profuse:{
    #  -prof-file ../myprof
    CONFIG(release,debug|release):          QMAKE_CFLAGS+=  -prof-use -prof-dir/home/r3/Documents/qt/openFSI
    CONFIG(release,debug|release):          QMAKE_CXXFLAGS+=  -prof-use -prof-dir/home/r3/Documents/qt/openFSI
    CONFIG(release,debug|release):          F90_CFLAGS += -prof-use  -prof-dir/home/r3/Documents/qt/openFSI
    CONFIG(release,debug|release):          QMAKE_LFLAGS += -prof-use  -prof-dir/home/r3/Documents/qt/openFSI
}

unix: {
    LIBS += -luuid -lm
}

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../thirdparty/triangle/release/ -ltriangle
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../thirdparty/triangle/debug/ -ltriangle
else:symbian: LIBS += -ltriangle
else:unix: LIBS += -L$$PWD/../thirdparty/triangle/ -ltriangle

INCLUDEPATH += $$PWD/../thirdparty/triangle
DEPENDPATH += $$PWD/../thirdparty/triangle

ODETESTING:{
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../thirdparty/nrrk/release/ -lnrrk
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../thirdparty/nrrk/debug/ -lnrrk
else:symbian: LIBS += -lnrrk
else:unix: LIBS += -L$$OUT_PWD/../thirdparty/nrrk/ -lnrrk

INCLUDEPATH += $$PWD/../thirdparty/nrrk
DEPENDPATH += $$PWD/../thirdparty/nrrk

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../thirdparty/nrrk/release/nrrk.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../thirdparty/nrrk/debug/nrrk.lib
else:unix:!symbian: PRE_TARGETDEPS += $$OUT_PWD/../thirdparty/nrrk/libnrrk.a
}

