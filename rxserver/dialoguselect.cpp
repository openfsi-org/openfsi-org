#include "dialoguselect.h"
#include "ui_dialoguselect.h"
DialogUSelect:: DialogUSelect(const QString Prompt, const  QStringList c, QWidget *parent)  :
    mPrompt(Prompt),
    mCandidates(c),
    QDialog(parent),
    ui(new Ui::DialogUSelect)
{
    ui->setupUi(this);
}

DialogUSelect::~DialogUSelect()
{
    delete ui;
}
