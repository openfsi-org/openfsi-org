#ifndef DIALOGUSELECT_H
#define DIALOGUSELECT_H

#include <QDialog>

namespace Ui {
    class DialogUSelect;
}

class DialogUSelect : public QDialog
{
    Q_OBJECT

public:
    explicit DialogUSelect(const QString Prompt, const  QStringList c, QWidget *parent = 0);
    ~DialogUSelect();

private:
    Ui::DialogUSelect *ui;
    QString mPrompt;
    QStringList mCandidates;
    QString mTheChosen;
};

#endif // DIALOGUSELECT_H
