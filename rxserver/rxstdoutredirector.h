#ifndef RXSTDOUTREDIRECTOR_H
#define RXSTDOUTREDIRECTOR_H

#include <QThread>
/// class to capture printf output so we can display it in a widget
/// but this is buggy - better to disable it via command-line flag -noredirprintf

class rxStdoutRedirector : public QThread
{
        Q_OBJECT

    public:
        explicit rxStdoutRedirector(QWidget *parent = 0);
        ~rxStdoutRedirector();
        int m_PleaseStop;
    protected:
        void run();

};

#endif // RXSTDOUTREDIRECTOR_H
