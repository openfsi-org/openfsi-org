#ifndef RXCOMMANDLINEBOX_H
#define RXCOMMANDLINEBOX_H

#include <QLineEdit>

class RXcommandlinebox : public QLineEdit
{
    Q_OBJECT
public:
    explicit RXcommandlinebox(QWidget *parent = 0);
    ~RXcommandlinebox();
void  keyPressEvent ( QKeyEvent * event ) ;
int PushToHistory();
int onUpArrow();
int onDownArrow();
int SetModelList(const QString s);
signals:

private slots:

private :
QStringList m_history;
int m_historyIndex;
QString m_mlist;

};

#endif // RXCOMMANDLINEBOX_H
