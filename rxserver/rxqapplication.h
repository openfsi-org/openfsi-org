#ifndef RXQAPPLICATION_H
#define RXQAPPLICATION_H

#include <QApplication>

class RXQApplication : public QApplication
{
    Q_OBJECT
public:
    //   explicit rxqapplication(QObject *parent = 0);
    explicit RXQApplication (int & argc, char *argv[]);
    bool notify ( QObject * receiver, QEvent * e ) ;
signals:

public slots:

};

#endif // RXQAPPLICATION_H
