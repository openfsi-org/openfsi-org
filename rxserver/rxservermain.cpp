#include "StdAfx.h"
#include <QApplication> // qt5+

#include "rxstdoutredirector.h"
#ifdef linux
    #include <unistd.h>

#endif
#define RXSTD
#include "rxqapplication.h"

#ifdef COIN3D
#include <Inventor/Qt/SoQt.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#include "rxqapplication.h"

#include "rxmainwindow.h"

int main(int argc,char*argv[])
{
#ifdef _DEBUG  // the override provides some error handling.
    RXQApplication a(argc,argv);
#else
    QApplication a(argc,argv);
#endif
    rxStdoutRedirector *rsd =0;
    QStringList args =  a.arguments() ;
    RXMainWindow *w=new RXMainWindow(args,0);
    SoQt::init(w);
    SoQt::show(w);
#ifdef RXSTD
    // rxStdoutRedirector directs printf output to our text area
    if( args.indexOf( "-noredirect")<0){
        rsd = new rxStdoutRedirector();
        w->connect(w,SIGNAL(destroyed()),rsd,SLOT(quit()));
        rsd->start();
    }
#endif
    w->ProcessCommandLine(args);

    SoQt::mainLoop();
#ifdef RXSTD
    if(rsd)
        rsd->m_PleaseStop=1;
    printf("ciao\n");fflush(stdout);
    usleep(1000);
#endif

    SoQt::done();
    delete w;
#ifdef RXSTD
    if(rsd) delete rsd;
#endif
    return 0;
}
#else  // pure QT
#include "rxmainwindow.h"
int main(int argc, char *argv[])
{
    rxStdoutRedirector *rsd =0;
    QApplication a(argc, argv);
    QStringList args = a.arguments() ;
    int k=args.indexOf( "-help");
    if(k>=0)
    {
        cout <<" syntax is "<<qPrintable(args[0]);
        cout<< " options are:\n\n";
        cout<<"\t-exec <ofsicommand>         runs the command"<<endl;
        cout<<"\t-noredircout                disables cout redirection"<<endl;
        cout<<"\t-noredirprintf              disables printf redirection"  <<endl;
        cout<<"\t-noredirect                 disables both redirections"<<endl;
        cout<<"\t-help                       displays this message"<<endl;

        exit(2);
    }


    RXMainWindow w(args,0);
   //  QMainWindow w;
    assert(!cout.bad());
    w.show(); 	assert(!cout.bad());
#ifdef RXSTD
    // rxStdoutRedirector directs printf output to our text area
    if( args.indexOf( "-noredirect")<0  &&  args.indexOf( "-noredirprintf")<0 ){
        rsd = new rxStdoutRedirector();
        w.connect(&w,SIGNAL(destroyed()),rsd,SLOT(quit()));
        rsd->start();
    }
#endif
    assert(!cout.bad());
    w.ProcessCommandLine(args);
    assert(!cout.bad());
    int rc = a.exec();
#ifdef RXSTD
    if(rsd)
        rsd->m_PleaseStop=1; puts("ciao");cout<<"ciao"<<endl;
#ifdef linux
    usleep(10000); // microseconds
#endif

    if(rsd){
        rsd->quit();
        rsd->wait(1000);
        delete rsd;
    }
#endif
    // qDebug()<<"CloSing down";
    return rc;
}
#endif
