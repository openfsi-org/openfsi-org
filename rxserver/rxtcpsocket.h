#ifndef RXTCPSOCKET_H
#define RXTCPSOCKET_H

#include <QTcpSocket>
#ifdef NEVER

class RXTcpSocket : public QTcpSocket
{
        Q_OBJECT
public:
    RXTcpSocket();
    explicit  RXTcpSocket(QObject *parent = 0);

 public slots:
        void readOnSocket();
};
#else
 #define  RXTcpSocket QTcpSocket
#endif
#endif // RXTCPSOCKET_H
