#include "StdAfx.h"
#include <QDebug>
//#include <QtGui/QApplication> // qt4.8
#include <QApplication> // qt5+
 #include <typeinfo>
#include "rxqapplication.h"


RXQApplication::RXQApplication ( int & argc, char ** argv ):
    QApplication ( argc, argv )
{

}
bool RXQApplication::notify ( QObject * receiver, QEvent * event )
    {
    try {
        return QApplication::notify(receiver, event);
    } catch (std::exception &e) {

        qDebug()<<"Error "<< e.what() <<  " sending event" <<  typeid(*event).name();
        qDebug()<< " to object" <<qPrintable(receiver->objectName()) << "  ("<< typeid(*receiver).name()  << ")";

    } catch (...) {
        qFatal("Error <unknown> sending event %s to object %s (%s)",
            typeid(*event).name(), qPrintable(receiver->objectName()),
            typeid(*receiver).name());
    }

    // qFatal aborts, so this isn't really necessary
    // but you might continue if you use a different logging lib
    return false;

    }
