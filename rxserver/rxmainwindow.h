#ifndef RXMAINWINDOW_H
#define RXMAINWINDOW_H
#include "RXGraphicCalls.h"
#ifdef USE_GLC
#include <GLC_Global>
#endif
#include <QMainWindow>
#include <QThread>
#include <QMutex>
#include <QLocalSocket>
#include <QTcpServer>
#include <QStringList>
#include "StdRedirector.h"
/*
C:\QtSDK\Desktop\Qt\4.8.0\msvc2010\include\QtCore;C:\QtSDK\Desktop\Qt\4.8.0\msvc2010\include\QtNetwork;C:\QtSDK\Desktop\Qt\4.8.0\msvc2010\include\QtGui;C:\QtSDK\Desktop\Qt\4.8.0\msvc2010\include\QtOpenGL;C:\QtSDK\Desktop\Qt\4.8.0\msvc2010\include;..\preprocess;..\..\rxserver;..\..\rxcommon;..\..\rxkernel;C:\QtSDK\Desktop\Qt\4.8.0\msvc2010\include\ActiveQt;debug;C:\QtSDK\Desktop\Qt\4.8.0\msvc2010\include\Qt;
*/
extern class  RXMainWindow* g_rxmw;
namespace Ui {
class RXMainWindow;
}

class RXMainWindow : public QMainWindow
{
    Q_OBJECT
    friend class rxProcessor;
    friend class rxStdoutRedirector;
    friend class RXDBLinkThreadLibMySQL ;
    friend class RXDBLinkThreadLibQT;
public:
    explicit RXMainWindow(QStringList args,QWidget *parent = 0);
    ~RXMainWindow();
    int ProcessCommandLine(const QStringList &args);
    // Convenience function for use ONLY IN THE GUI THREAD
    void Execute(const QString s) { emit RXCommand(this, s   );}
protected:
    void incomingConnection(int socketDescriptor);
public slots:
    void onFileNew();
    void startWorker();
    void OnAbout();
    void CommandEcho(QObject *client, const QString &s);

    void onMsgbox_request(QObject *client, const QString &s, int *prv);
    void onStatusbarRequest(QObject *client, const QString &s);
    void onUserchoiceRequest(QObject*client,const QString &prompt,const QStringList&cands,QString *result);
    void onFilename_request (QObject *client ,const QString &caption ,const QString &dir,const QString &filter , QString *fName);
    void onSavefilename_request (QObject *client ,const QString &caption ,const QString &dir,const QString &filter , QString *fName);
    void onDirnameRequest(QObject *client ,const QString &caption ,const QString &dir,const QString &filter , QString *fName);
    void onNewGraphicviewrequest();
    void onCloseGraphicview();
    void onNewWindow(const QString caption, void *ptr,RXGRAPHICSEGMENT *rc);
    void onDeleteWindow(void *ptr,int*rc);
    void onNewGNode(const QString caption,RXGRAPHICSEGMENT  pParent,RXGRAPHICSEGMENT *rc);

    void onGNodeAddChild( RXGRAPHICSEGMENT   parent, RXGRAPHICSEGMENT   pchild ,RXGRAPHICSEGMENT * rc  );
    void onGNodeRemoveChild( RXGRAPHICSEGMENT , RXGRAPHICSEGMENT   ,RXGRAPHICSEGMENT * );


    void UpdateAllGraphicsRequested(void);
    void WriteRequested(const char*,RXGRAPHICSEGMENT );
    void onInsertNurbsCurve(const class ON_NurbsCurve *c,RXGRAPHICSEGMENT  o,RXGRAPHICOBJECT*rc);
    void onInsertPolyline(const int count,const float*poly,RXGRAPHICSEGMENT  o,RXGRAPHICOBJECT *rc);
    void onInsertPolyhedron(const float* xyz,const int cp,
                             const int*faceSet, const int cf,
                             RXGRAPHICSEGMENT  o ,RXGRAPHICOBJECT*rc);

    void onSetColor(RXGRAPHICOBJECT  id,const char*color);
    void onDeleteGraphicObject(RXGRAPHICOBJECT  ptr);
    void onFlushGraphicsNode(RXGRAPHICSEGMENT  n);
    void onnew_client_request();

private:
    Ui::RXMainWindow *ui;
	QMutex m_ocbmutex;
    static void outcallback( const char* ptr, std::streamsize count,void *pTextBox );
    int deleteSOChildren(RXGRAPHICSEGMENT  n);
    StdRedirector<> * myRedirector;

    class RXMultiClientServer *m_SocketServer;
    QLocalSocket *m_qlc;
public:
    class rxWorkerThread *mWorkerThread;//g_rxmw->mThread;
    int RXPrint(QObject *q, const QString &s,const int level);
    int DisplayMsgBox(QObject *q,const QString &s,const int level);
signals:
    void RXCommand(QObject *client, const QString &s);

    void RXPleasePrint(QObject *client, const QString &s);
    void RXPleaseMsgBox(QObject *client, const QString &s,const int level, int* rv);
    void BroadcastToClients(const QString &s);
    void RXMWIsEnding();


private slots:
    void onActionExit_triggered();
    void on_rxcommandline_returnPressed();
    void onSendMessage();
    void onSendClientMessage();
    void onRcvPrintText(QObject *client, const QString &s);

    void  onRcvMsgBox(QObject *o, const QString &s,const int level, int*retVal);

    void onRcvStdoutMsg();
    void on_actionOpen_triggered();

    void on_actionNew_triggered();

protected:

};

class rxWorkerThread : public QThread
{
    Q_OBJECT
public:
    explicit rxWorkerThread(QWidget *parent = 0);
    ~rxWorkerThread();
    class rxWorker *m_rxw;

signals:
    void RXCommand_rxwt(QObject *client, const QString &s);

protected:
    void run();

};


// To access the GUI from the app, the calling sequence is:
//    1) the app ( in RXWorkerThread)  calls  (eg)/*static */ rxWorker::setStatusBar( QString (text),0);

//    2) which, still in RXWorkerThread , emits g_rxw->pleaseMsgBox(QObject *client, const QString &s,int*prv);
//      The app could do the emit itself if it is from a QOBJECT-derived class. But most of the app isnt .
//    2a) pleaseMsgBox is a signal of RXWorker, which exists in RXWorkerThread:
//        the signal is connected to g_rxmw,SLOT(onMsgbox_request(QObject*,const QString &,int*))
//        The connect is made in the RXWorker constructor.
//    3) onMsgbox_request  is a SLOT of RXMainWindow so it executes in the GUI thread
//    4) onMsgbox_request makes the Qt GUI calls
//
//        methods like  RXMsgBox, RXGetfilename, etc. emit a BlockingQueuedConnection signal.
//        The GUI thread catches it with a slot which does the dialog.
//        this transfers control to the GUI thread until the slot returns.
//        for actions like drawing we can use the default connection type.

// so to add a new GUI action we have write an action-chain to:
//1) define a static RXWorker method which emits a signal.
//2) define the RXWorker signal
//3) define a slot in RXMainWindow
//4) do the connect in the RXWorker constructor.
//
// eventually we will need action-chains for all the graphics calls.
//Q workface..

class rxWorker: public QObject
{
    Q_OBJECT
public:
    explicit rxWorker(QWidget *parent = 0);
    ~rxWorker();
    static rxWorker * TheOnly() { return m_this;}
private:
    static rxWorker *m_this;
signals:
    void pleaseMsgBox(QObject *client, const QString &s,int*prv);
    void pleaseStatusBar(QObject *client, const QString &s );
    void pleaseExit();
    void pleaseUserChoice(QObject *client ,const QString &caption ,const QStringList &candidates,QString *fName );
    void pleaseGetFileName     (QObject *client ,const QString &caption ,const QString &dir,const QString &filter , QString *fName);
    void pleaseGetSaveFileName (QObject *client ,const QString &caption ,const QString &dir,const QString &filter , QString *fName);
    void pleaseGetDirName (QObject *client ,const QString &caption ,const QString &dir,const QString &filter , QString *fName);

    void pleaseNewWindow(const QString caption, void *ptr,RXGRAPHICSEGMENT *rc);
    void pleaseDeleteWindow(void *ptr,int *rc);
    void pleaseNewGNode(const QString caption,RXGRAPHICSEGMENT  pParent,RXGRAPHICSEGMENT *rc);

    void pleaseGNodeAddChild( RXGRAPHICSEGMENT  pParent, RXGRAPHICSEGMENT   pChild ,RXGRAPHICSEGMENT *rc);
    void pleaseGNodeRemoveChild(RXGRAPHICSEGMENT  pParent,RXGRAPHICSEGMENT pChild ,RXGRAPHICSEGMENT *rc);

    void pleaseWrite (const char* filename,RXGRAPHICSEGMENT  o);
    void PleaseUpdateAllGraphics();

    void pleaseInsertNurbsCurve(const class ON_NurbsCurve *c,RXGRAPHICSEGMENT  o, RXGRAPHICOBJECT  *rc);

    void pleaseInsertPolyline(const int count,const float*poly,RXGRAPHICSEGMENT  o,RXGRAPHICOBJECT  *rc);

    void pleaseInsertPolyhedron(const float* xyz,const int cp,
                                const int*faceSet, const int cf,
                                RXGRAPHICSEGMENT  o ,RXGRAPHICOBJECT *rc);

    void pleaseDeleteGraphicObject(RXGRAPHICOBJECT ptr);
    void pleaseFlushGraphicsNode(RXGRAPHICSEGMENT  n);
    void pleaseSetColor(RXGRAPHICOBJECT   g,const char*c);
public slots:
    void rxExecute_w(QObject *client, const QString &s);
protected:
    void run();
public:
    static int setStatusBar(const QString &s,QObject *client,const int onright=0 ) ;
    static  QString UserChoice(const QString & prompt,const QStringList &candidates) ;
    static int shutdown();
    static QString rxOpenFileName(QObject *client,
                                   const QString caption,
                                   const QString dir,
                                   const QString filter
                                   );
    static QString rxSaveFileName(QObject *client,
                                   const QString caption,
                                   const QString dir,
                                   const QString filter
                                   );
    static QString rxDirName(QObject *client,
                              const QString caption,
                              const QString dir,
                              const QString filter
                              );
    static RXGRAPHICSEGMENT   NewWindow(const QString caption, void *ptr);
    static int DeleteWindow(void* ptr);
    static RXGRAPHICSEGMENT   NewGNode(const char* caption,RXGRAPHICSEGMENT  pParent);
    static RXGRAPHICSEGMENT  GNodeAddChild(RXGRAPHICSEGMENT pParent, RXGRAPHICSEGMENT pChild);
    static RXGRAPHICSEGMENT  GNodeRemoveChild(RXGRAPHICSEGMENT  pParent,RXGRAPHICSEGMENT pChild);

    static int  GNodeWrite(const char* filename,RXGRAPHICSEGMENT  root);
    static void UpdateAllGraphics(void);

    static RXGRAPHICOBJECT  InsertNurbsCurve(const class ON_NurbsCurve *c, RXGRAPHICSEGMENT  o);
    static RXGRAPHICOBJECT  InsertPolyline(const int count,const float*poly,RXGRAPHICSEGMENT  o);
    static RXGRAPHICOBJECT  InsertPolyhedron(const float* xyz,const int cp, const int*faceSet, const int cf, RXGRAPHICSEGMENT  o);
    static void SetColor(RXGRAPHICOBJECT id,const char *color);
    static bool DeleteGraphicObject(RXGRAPHICOBJECT ptr);
    static bool FlushGraphicsNode (RXGRAPHICSEGMENT   n);
};

#endif // RXMAINWINDOW_H
