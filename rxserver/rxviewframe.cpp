#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include "RXSail.h"
#include "rxmainwindow.h"
#include "rxviewframe.h"


RXViewFrame::RXViewFrame(QWidget *parent) :
    QFrame(parent)
  ,m_Sail(0)
{
}
RXViewFrame::~RXViewFrame()
{
    if(m_Sail) {
        m_Sail->m_viewFrame=0;// wrong thread
        if(g_rxmw )
        {
            QString s = QString("Deletemodel:") + m_Sail->GetQType();
            g_rxmw->Execute(s);
        }
    }
}
