/* RXServer
 We can now send commands from a client to the worker thread. We can close the worker thread
 cleanly when no sockets are active.
 NEXT.  Handle close-down of a socket thread, of the worker thread and of the app.
    that includes the clients discovering that their partner socketthread no longer exists.
    Equally, handle closedown of a client: Kill its socket thread.
    THEN. Figure out how the worker thread can send a message (or just print text)
        to a given socket thread.
        Get the relax messaging to accept a pointer to the socket thread it must send to.
        Get all relax writing to appear on the owning clients ticker.

 */
#include "StdAfx.h"
#include "RXGraphicCalls.h"

#include "RXSail.h"
#include <QtGlobal>
#include <QMutexLocker>
#include "rxkernel.h"
#include "rxviewframe.h"
#include <assert.h>

#include <QProcess>
#include <iostream>
#include <stdio.h>
using namespace std;
#include "ui_rxmainwindow.h"
#include <QDebug>
#include <QFileDialog> // try without
#include <QMessageBox>
#include "StdRedirector.h"

#ifdef USE_GLC
#include "glwidgetpreprocess.h"
#elif defined(COIN3D)
#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoNurbsCurve.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoComplexity.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoCoordinate4.h>
#include <Inventor/nodes/SoBaseColor.h>
#include <Inventor/nodes/SoIndexedFaceSet.h>

#include <Inventor/actions/SoWriteAction.h>
#include <Inventor/SoOutput.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#include <QMdiSubWindow>
#endif
#include "rxmulticlientserver.h"

#include "global_declarations.h"
#include "rxmainwindow.h"
//
//  contains the kernel GUI and worker thread.
//   on startup we also start the socket server.
//
RXGRAPHICSEGMENT g_graphicsRoot; // for debugging only

class  RXMainWindow* g_rxmw;

RXMainWindow::RXMainWindow(QStringList args, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::RXMainWindow),
    myRedirector(0),
    m_qlc(0),
    mWorkerThread(0)

{
    qRegisterMetaType<RXGRAPHICSEGMENT>("RXGRAPHICSEGMENT");
    qRegisterMetaType<RXGRAPHICOBJECT>("RXGRAPHICOBJECT");

    ui->setupUi(this);

    QTextEdit * tb =  ui->textEdit ; //  textBrowser;
    // redirect cout (and fortran write(6)) to our textwindow

    if( args.indexOf( "-noredirect") <0 &&  args.indexOf( "-noredircout")<0  )
        myRedirector =  new StdRedirector<>( std::cout, RXMainWindow::outcallback,(void *)tb );
    assert(!cout.bad());
    g_rxmw=this;
    connect(ui->actionExit,SIGNAL(triggered()),SLOT(close()));
    connect(ui->actionNew,SIGNAL(triggered())  ,SLOT(onFileNew()));
    connect(ui->actionStart_worker, SIGNAL(triggered()),SLOT(startWorker()));
    connect(ui->actionSendMessage,SIGNAL(triggered()) ,this,SLOT(onSendMessage()));
    connect(g_rxmw->ui->actionMessage_to_Client,SIGNAL(triggered()),g_rxmw,SLOT(onSendClientMessage()));
    connect(this,SIGNAL(RXCommand(QObject *, const QString &)), this, SLOT(CommandEcho(QObject *, const QString &)));
    connect(this,SIGNAL(RXPleasePrint(QObject *, const QString &)), this, SLOT(onRcvPrintText(QObject *, const QString &)));
    connect(this,SIGNAL(RXPleaseMsgBox(QObject *, const QString &,const int, int*)), this, SLOT(onRcvMsgBox(QObject *, const QString &,const int, int*)),Qt::BlockingQueuedConnection);
    connect(ui->actionNew_graphicsView,SIGNAL(triggered()),this,SLOT(onNewGraphicviewrequest()));
    connect(ui->actionNew_Client,SIGNAL(triggered()),this,SLOT(onnew_client_request()));
    connect(ui->actionAbout,SIGNAL(triggered()),SLOT(OnAbout()));
    assert(!cout.bad());

  m_SocketServer = new RXMultiClientServer(this);
  assert(!cout.bad());
  m_SocketServer->start();
  assert(!cout.bad());
  this->startWorker();

  assert(!cout.bad());
  QThread *ct= QThread::currentThread (); ct->setObjectName("peters GUI thread");
  assert(!cout.bad());

}

RXMainWindow::~RXMainWindow()
{
  if(m_SocketServer ) delete m_SocketServer;

  emit RXMWIsEnding();  // this does a DeleteLater on the worker thread.
  if(mWorkerThread){
    mWorkerThread->exit();
    mWorkerThread->wait();
    delete mWorkerThread;  mWorkerThread=0;
  }

  if(m_qlc) delete m_qlc;
  if(myRedirector ) delete myRedirector;
  delete ui;
    g_rxmw=0;
}
void RXMainWindow::OnAbout() {

  QString text = " this is OpenFSI.org (ofsi to its friends)"
      " written by Peter Heppel and colaborators since 1980\n\n"

      "with special thanks to Peter Ronan Rice for the concept\n\n"

      "Thanks also to the many open-source authors who have made it possible.\nIn no particular order:\n\n"
      "John Biddiscombe , Jerome Soumagne , Guillaume Oger , David Guibert , Jean-Guillaume Piccinali\n"
      " Parallel Computational Steering and Analysis for HPC Applications\n"
      " using a ParaView Interface and the HDF5 DSM  Virtual File Driver\n"
      " Eurographics Symposium on Parallel Graphics and Visualization (2011) \n\n"

      "Mathieu Jacques for his excellent MTParser which we have extended\n"
      "Jonathan Richard Shewchuk,  Triangle: Engineering a 2D Quality Mesh Generator and Delaunay Triangulator, in ``Applied Computational Geometry: Towards Geometric Engineering''  volume 1148 of Lecture Notes in Computer Science, pages 203-222, Springer-Verlag, Berlin, May 1996.\n"

      "Mathieu Desbrun, Mark Meyer, and Pierre Alliez. Intrinsic Parameterizations of Surface Meshes. In Eurographics ’02 Proceed ings, 2002.\n"

      "Coin3D\n"
      "the GNU xml library\n"
      "sparskit\n"
      "openNURBS"
      "\n Sofia Werner for her sail++ panel method - which is integrated AS IS\n"
      "and of  course the people at Qt\n"
      "\nreleased under LGPL license\n"

      "\nThe program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.\n"

      "\nFor reference documentation you can try the rather dated www.peterheppel.com/r2_doc \n"

      "\n(Hint for getting started: type a space into the command-line box at the bottom)\n"
      "\nbuild date: " __DATE__ " " __TIME__ " "
 #ifdef  __INTEL_COMPILER
 //         "\nCompiler (Intel)        : "__INTEL_COMPILER" "
 #endif
#ifdef __GNUG__
//          "\nCompiler (g++)        :  " __GNUG__ "" __GNUC_MINOR__
#endif
      "\nbuilt with Qt Version: "  QT_VERSION_STR
      "\nrunning    Qt Version: ";
  text +=   qVersion();
  ;

  QMessageBox::about (this, "About OpenFSI.org", text );
}
int RXMainWindow::ProcessCommandLine(const QStringList &args){
  int k, i=0;
  k=args.indexOf( "-exec");
  if(k>=0)
  {i++; emit this->RXCommand(0,args.value(k+1));}
  if(args.size()>1 && !i )
    cout <<" syntax is "<<qPrintable(args[0])<< " [ -exec <ofsicommand>]"<<endl;
  return 0;
}

void RXMainWindow::onFileNew()
{
  // qDebug()<<"g_graphicsRoot->removeAllChildren";
  //    cout<<endl;
  //    for(int i=0;i<2;i++)
  //    {  printf("%dthis is from printf%d\n",i,i); fflush(stdout);}
  //    cout<<"that was printf";
  //    cout<<endl;assert(!cout.bad());
  //    emit RXCommand(this,"onfileNew Relax command");
  //  deleteSOChildren(g_graphicsRoot );
  //  ->removeAllChildren();
}
void RXMainWindow::CommandEcho(QObject *client, const QString &s)
{
#ifdef DEBUG
  if(client){
    assert(!cout.bad());
    cout<<"CommandEcho from'"<<client->objectName().toStdString().c_str()<<"',  '"  <<s.toStdString().c_str()<<"'"<<endl;
    assert(!cout.bad());
    qDebug()<<"CommandEcho"<<client->objectName()<<s;
  }
  else
    qDebug()<<"CommandEcho: "<<s;
#endif
}
void RXMainWindow:: onRcvMsgBox(QObject *o, const QString &s,const int level, int*retVal)
{
  // if client is null or the instance of rxmw, append to  textbrowser
  // else send a messsage to the appropriate client
  RXTcpSocket *client = ( RXTcpSocket * )o;

  if(!client || o==g_rxmw) {


    QMessageBox msgBox;
    msgBox.setText(s);
    //  msgBox.setInformativeText("Do you want to save your changes?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Yes);
    if(level >1 ) msgBox.setIcon(QMessageBox::Information  );
    if(level >2 ) msgBox.setIcon(QMessageBox::Warning  );
    if(level >3 ) msgBox.setIcon(QMessageBox::Critical  );

    *retVal = msgBox.exec(); // QMessageBox::Cancel,QMessageBox::Yes,QMessageBox::No
  }

  else {
    QString aa("RX Msgbox )"); aa+= s;
    g_rxmw->m_SocketServer ->WriteOnSocket(client,aa); //WRONG - should block and wait for the client to reply
  }

}
void RXMainWindow:: onRcvPrintText(QObject *o, const QString &s)
{
  // if client is null or the instance of rxmw, append to  textbrowser
  // else send a messsage to the appropriate client
  RXTcpSocket *client = ( RXTcpSocket * )o;
  if(!g_rxmw) return;
  if(s.contains("(broadcast"))
    g_rxmw ->m_SocketServer->sendMessageToAllClients(s );
  else
  {
    if (o==g_rxmw)
      g_rxmw->ui->textEdit->append(s);
    else if(!client)
      g_rxmw->ui->textEdit->append(s);
    else
      g_rxmw->m_SocketServer ->WriteOnSocket(client,s);
  }
}
void RXMainWindow::onSendMessage()
{
  emit RXCommand(this,"RXMainWindow::onSendMessage myfirstcommand");
}

void RXMainWindow:: onSendClientMessage()
{
  cout<<"OnSendClientMessage"<<endl;
  this->m_SocketServer->sendMessageToAllClients("to all clients: my first broadcast" );
}

void RXMainWindow:: onRcvStdoutMsg(){

  QByteArray data = this->m_qlc->readAll();
  qDebug()<<"Rcvd stdout :"<<data;
}
void RXMainWindow::startWorker()
{
  if(this->mWorkerThread)
    return;
  mWorkerThread = new rxWorkerThread(this);
  mWorkerThread->setObjectName (  QString("RXMainWindow") );
    connect(this,SIGNAL( RXMWIsEnding( )), g_rxmw->mWorkerThread, SLOT(deleteLater()));
  mWorkerThread->start ();
  assert(!cout.bad());
}

void RXMainWindow::on_rxcommandline_returnPressed()
{
  QString myline = ui->rxcommandline->displayText();

  if(!myline.isEmpty()) {
    emit RXCommand(this,myline);
    ui->rxcommandline->PushToHistory();
    ui->rxcommandline->clear();

  }
}

void RXMainWindow::onActionExit_triggered()
{
  if(this->mWorkerThread) delete mWorkerThread;
  mWorkerThread=0;
  this->close();
}
void  RXMainWindow::onMsgbox_request(QObject *client, const QString &s, int *prv){

  QMessageBox::StandardButton reply;
  reply = QMessageBox::question(this, tr("QMessageBox::question()"),
                                s,
                                QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
  *prv=reply;
}
void  RXMainWindow::onStatusbarRequest(QObject *client, const QString &s){

  this->statusBar()->showMessage(s);
}
void RXMainWindow::onUserchoiceRequest(QObject*client,const QString &prompt,const QStringList&cands,QString *result)
{
    qDebug()<< "TODO:RXMainWindow::onuserchoice_request:" <<prompt;
  //   qDebug()<< cands;
  result->clear();
}

//tr("All Files (*);;Text Files (*.txt)")
void RXMainWindow::onFilename_request (QObject *client ,const QString &caption ,const QString &dir,const QString &filter , QString *fName)
{
  QFileDialog::Options options;
  //   if (!native->isChecked())
  //        options |= QFileDialog::DontUseNativeDialog;
  QString selectedFilter;
  *fName = QFileDialog::getOpenFileName(this,
                                        caption,
                                        dir,
                                        filter +QString ( ";;All files (*)" ),
                                        &selectedFilter,
                                        options);
}

void RXMainWindow::onSavefilename_request(QObject *client ,const QString &caption ,const QString &dir,const QString &filter , QString *fName){
  *fName = QString("savefilename.txt");
  QFileDialog::Options options;
  //      if (!native->isChecked())
  //          options |= QFileDialog::DontUseNativeDialog;
  QString selectedFilter;
  *fName = QFileDialog::getSaveFileName(this,
                                        caption,
                                        dir,
                                        filter,
                                        &selectedFilter,
                                        options);
}
void RXMainWindow::onDirnameRequest(QObject *client ,const QString &caption ,const QString &dir,const QString &filter , QString *dirName){
  //      if (!native->isChecked())
  //          options |= QFileDialog::DontUseNativeDialog;
  QString selectedFilter;
  *dirName = QFileDialog::getExistingDirectory(this, caption +QString(" (") +dir +QString(")"),
                                               dir,
                                               QFileDialog::ShowDirsOnly
                                               | QFileDialog::DontResolveSymlinks);
  qDebug()<<" selected dir = "<<*dirName;
}

void RXMainWindow::onnew_client_request()
{
  QString program = "rxclientbase";
  QStringList arguments;
  arguments << "--connect"<<"--help";

  QProcess *myProcess = new QProcess(this);
  myProcess->start(program, arguments);
}
void RXMainWindow::onCloseGraphicview()
{
  qDebug()<<"On CLOSE graphics View"<<endl;
}

void RXMainWindow::onNewGraphicviewrequest()
{
#ifndef USE_GLC
  qDebug()<<" NO  onNew_graphicviewrequest";
  RXGRAPHICSEGMENT groot;
  this->onNewWindow("test", 0l,&groot);
#else

  QWidget *subwindow =new QWidget(this);
  subwindow->setObjectName(QString::fromUtf8("GraphicsView"));
  subwindow->setWindowTitle(tr("Graphics View"));
  subwindow->setAttribute(Qt::WA_DeleteOnClose);
  subwindow->setMinimumSize(300,300);

  QGridLayout *gridLayout_2 = new QGridLayout(subwindow);
  gridLayout_2->setSpacing(6);
  gridLayout_2->setContentsMargins(11, 11, 11, 11);
  gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));

  GLWidgetPreprocess* l_GLWidget= new GLWidgetPreprocess(this);
  gridLayout_2->addWidget(l_GLWidget, 0, 0, 1, 1);

  ui->mdiArea->addSubWindow(subwindow);
  l_GLWidget->setMinimumSize(300,300);
  subwindow->show();
#endif
}
    void RXMainWindow::onDeleteWindow(void *ptr,int*rc)
    {
        *rc=1;
       class  RXViewFrame *view =(class  RXViewFrame *) ptr;
       if(view){
           view->m_Sail=0;
      //     delete view->parent();
           view->parent()->deleteLater();
           *rc=0;
       }

    }

void RXMainWindow::onNewWindow(const QString caption, void *theSail,RXGRAPHICSEGMENT  *ppgroot)
{
#ifdef USE_GLC
  QWidget *subwindow =new QWidget(this);
  subwindow->setObjectName(caption);
  subwindow->setWindowTitle(QString("Title:")+ caption);
  subwindow->setAttribute(Qt::WA_DeleteOnClose);
  subwindow->setMinimumSize(300,300);
  //   subwindow->setProperty("userptr",QVariant(ptr));

  QGridLayout *gridLayout_2 = new QGridLayout(subwindow);
  gridLayout_2->setSpacing(6);
  gridLayout_2->setContentsMargins(11, 11, 11, 11);
  gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));

  GLWidgetPreprocess* l_GLWidget= new GLWidgetPreprocess(this);
  gridLayout_2->addWidget(l_GLWidget, 0, 0, 1, 1);

  ui->mdiArea->addSubWindow(subwindow);
  l_GLWidget->setMinimumSize(300,300);
  *rc= l_GLWidget->m_World.rootOccurence();
  subwindow->show();
#elif defined (COIN3D)
  SoQtExaminerViewer * eviewer;
  QMdiSubWindow *subwindow = new QMdiSubWindow (this);
  //    connect(subwindow,SIGNAL(destroyed()),SLOT(onClose_graphicview()));
  subwindow->setObjectName(caption);
  subwindow->setAttribute(Qt::WA_DeleteOnClose);
  subwindow->setWindowTitle(caption);
  subwindow->setMinimumSize(300,300);

  ui->mdiArea->addSubWindow(subwindow,Qt::SubWindow);
  RXViewFrame *view = new RXViewFrame();
  view->m_Sail = (class RXSail*) theSail;
  //     if(theSail)
  //         theSail->m_view = view;NEED emit SetVptr(theSail,view)

  view->setParent(subwindow);
  subwindow->setWidget(view);

  view->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Ignored);
  view->setAttribute(Qt::WA_DeleteOnClose);

  // Use one of the convenient SoQt viewer classes.
  //  SoQtExaminerViewer *
  eviewer = new SoQtExaminerViewer(view);

  // to hook up a model we go:

  SoSeparator * root = new SoSeparator;
  root->ref(); root->renderCaching=SoSeparator::OFF;

  // try a light between Scenegraph root and  modelrooot
  SoDirectionalLight *lll = new SoDirectionalLight;
  lll->direction.setValue(0.,1.,0.);
  root->addChild(lll);

  *ppgroot = new SoGroup; // the seg that we will draw into.
  (*ppgroot)->setUserData(view) ; // theSail);

  root->addChild(*ppgroot);

  // our model tree is a tree of SoGroup*'s.   We pass back groot
  // and it becomes roughly  equivalent to the HOOPS modelsegment.

  eviewer->setSceneGraph(root);
  eviewer->setHeadlight(true);
  SoQt::show(view);
  //   qDebug()<<" New GWindow: thread is "<<QThread::currentThreadId();
#endif
}

void RXMainWindow::onNewGNode(const QString caption,
                              RXGRAPHICSEGMENT pParent,RXGRAPHICSEGMENT *rc) // public slot
{
  //   qDebug()<<" New GNode "<<caption<<" thread is "<<QThread::currentThreadId();
#ifdef COIN3D
  *rc = new SoGroup();
  SbName c = SbName( qPrintable(caption));
  (*rc)->setName(c);
  if(pParent) pParent->addChild(*rc);
#endif
}

void RXMainWindow::onGNodeAddChild( RXGRAPHICSEGMENT   parent, RXGRAPHICSEGMENT  pchild ,RXGRAPHICSEGMENT * rc  )
{
  *rc=0;
#ifdef COIN3D
  parent->addChild(pchild);
#endif
}

void RXMainWindow::onGNodeRemoveChild( RXGRAPHICSEGMENT parent , RXGRAPHICSEGMENT  child ,RXGRAPHICSEGMENT *rc)
{
  *rc=0;
  cout<<"TODO:  RXMainWindow::onGNodeRemoveChild  " <<endl;
#ifdef COIN3D
  parent->removeChild(child);
#endif
}


void RXMainWindow::UpdateAllGraphicsRequested(void) {
  cout<<" TODO:  RXMainWindow::UpdateAllGraphicsRequested  "<<endl;
  // if(eviewer)
  //     eviewer->render();
}

void RXMainWindow::WriteRequested(const char*fname,RXGRAPHICSEGMENT root)
{
#if defined (COIN3D)
  FILE *fp = RXFOPEN(fname,"w");
  if(!fp) {
    cout<<"cant open OI dumpfile ";
    if(fname) cout<<fname;
    cout<<endl;
    return ;
  }
  SoOutput out;

  out.setFilePointer(fp);
  SoWriteAction wa(&out);
  wa.apply(root);
  FCLOSE(fp);
#endif
}
void RXMainWindow::onInsertNurbsCurve(const class ON_NurbsCurve *c,RXGRAPHICSEGMENT oo,RXGRAPHICOBJECT*rc)
{
  float xyz[4]; int i,k;
  float*lp;
  // ex from doc   page 99
  //    // The control points for this curve
  //    float pts[7][3] = {
  //    { 4.0, -6.0, 6.0},
  //    {-4.0, 1.0, 0.0},
  //    {-1.5, 5.0, -6.0},
  //    { 0.0, 2.0, -2.0},
  //    { 1.5, 5.0, -6.0},
  //    { 4.0, 1.0, 0.0},
  //    {-4.0, -6.0, 6.0}};
  //    // The knot vector
  //    float knots[10] = {1, 2, 3, 4, 5, 5, 6, 7, 8, 9};
  //    // Create the nodes needed for the B-Spline curve.
#if defined (COIN3D)
  SoSeparator *curveSep = new SoSeparator();
  //     SoGroup *curveSep = new SoGroup(); // was SoSeparator
  //  curveSep->ref();
  curveSep->setName("ANurbsCurve");
  SoDrawStyle *drawStyle = new SoDrawStyle;
  drawStyle->lineWidth = 1;
  curveSep->addChild(drawStyle);
  //    // Define the NURBS curve including the control points
  //    // and a complexity.
  SoComplexity *complexity  = new SoComplexity;
  SoCoordinate4 *controlPts = new SoCoordinate4;
  SoNurbsCurve *curve       = new SoNurbsCurve;
  complexity->value = 0.8;

  curve->numControlPoints = c->CVCount();
  for( i = 0; i<c->CVCount();i++) {
    lp=xyz;
    ON_3dPoint pp = c->CV(i);
    controlPts->point.set1Value(i,pp.x,pp.y,pp.z,c->Weight(i));
  }
  // knots ON-style
  //    int nk = c->KnotCount();
  //    float knots[nk];
  //    for(i=0;i<nk;i++)  knots[i] = c->Knot(i);
  // knots hoopsstyle
  int L_OFFSET =0;
  int kc_ON=c->KnotCount(),kc_h,cpcount =c->CVCount() ;
  kc_h=cpcount +c->Order();
  float knots[kc_h];
  L_OFFSET = 1;
  float kspan = 1.0; // Knot(kc_on-1)-Knot(0);
  for(i=0,k=0;i<L_OFFSET;i++,k++){
    knots[k]=(float)(c->Knot(0)-c->Knot(0));
  }
  for(i=0;i<kc_ON;i++,k++) {
    knots[k]=(float)((c->Knot(i)-c->Knot(0))/kspan);
  }
  for(;k<kc_h;k++){
    knots[k]=(float)((c->Knot(kc_ON-1)-c->Knot(0))/kspan);
  }
  // try normalizing the knots
  knots[0]=0.0;
  for(i=1;i<kc_h;i++) {
    if(knots[i]<knots[i-1])
      knots[i]=knots[i-1];
  }
  curve->knotVector.setValues(0, kc_h, knots);

  curveSep->addChild(complexity);
  curveSep->addChild(controlPts);
  curveSep->addChild(curve);
  // curveSep->unrefNoDelete(); // WHY
  oo->addChild(curveSep);
  *rc= curveSep;
#endif
}

void RXMainWindow::onInsertPolyline(const int c,
                                    const float*p,RXGRAPHICSEGMENT o,RXGRAPHICOBJECT*rc)
{
#if defined (COIN3D)
  //  qDebug()<<" New Pline in "<<o->getName().getString() <<" thread is "<<QThread::currentThreadId();

  // Set the draw style of the curve.
  SoDrawStyle *drawStyle  = new SoDrawStyle;
  drawStyle->lineWidth = 1;
  // Define the NURBS curve including the control points
  // and a complexity.

  //  SoComplexity  *complexity = new SoComplexity;
  //  complexity->value = 0.8; // high value= better render

  SoCoordinate3 *controlPts = new SoCoordinate3;
  SoNurbsCurve  *curve      = new SoNurbsCurve;


  float xyz[c][3] ;
  memcpy(xyz,p,3*c*sizeof(float));
  controlPts->point.setValues(0, c, xyz);
  curve->numControlPoints = c;
  int nkts=c+2;
  float knots[nkts];
  for(int i=0;i<nkts;i++)knots[i]=i+1;
  curve->knotVector.setValues(0, nkts, knots);

  //  SoBaseColor * col2 = new SoBaseColor;
  //  col2->rgb = SbColor(0.9, 0,0);

  SoGroup *curveSep = new SoGroup(); // was SoSeparator
  curveSep->setName("APolyline");

  curveSep->addChild(drawStyle);
  //   curveSep->addChild(complexity);
  //   curveSep->addChild(col2);
  curveSep->addChild(controlPts);
  curveSep->addChild(curve);

  o->addChild(curveSep);
  *rc= curveSep;

#elif defined (USE_GLC)
  *rc=  GLWidgetPreprocess::InsertPolyline( o, c,p);
#endif
}
void RXMainWindow::onInsertPolyhedron(const float* xyz,const int cp,
                                      const int*indices, const int cf,
                                      RXGRAPHICSEGMENT  o ,RXGRAPHICOBJECT*rc)
{
#if defined (COIN3D)
  SoSeparator *result = new SoSeparator;
#ifdef REALLYLIKEMENTOR
  result->ref();

  // Define colors for the faces
  SoMaterial *myMaterials = new SoMaterial;
  myMaterials->diffuseColor.setValues(0, 12, colors);
  result->addChild(myMaterials);
  SoMaterialBinding *myMaterialBinding = new SoMaterialBinding;
  myMaterialBinding->value = SoMaterialBinding::PER_FACE;
  result->addChild(myMaterialBinding);
#endif
  // LINEs might draw as wireframe FILLED;
  SoDrawStyle * drawStyle = new SoDrawStyle;
  drawStyle->style = SoDrawStyle:: LINES;
  drawStyle->lineWidth=1.0;
  result->addChild(drawStyle);

  // Define coordinates for vertices
  SoCoordinate3 *myCoords = new SoCoordinate3;

  int k,n=cp/3;
  SbVec3f vv [n];
  const float *lp =xyz;
  for (k=0;k<n;k++)
  { vv[k]=SbVec3f(lp); lp+=3;}
  myCoords->point.setValues(0, n, vv);
  result->addChild(myCoords);
  // Define the IndexedFaceSet, with indices into
  // the vertices:
  SoIndexedFaceSet *myFaceSet = new SoIndexedFaceSet;
  myFaceSet->coordIndex.setValues(0, cf, indices);
  result->addChild(myFaceSet);
  //  result->unrefNoDelete();
  o->addChild(result);
  *rc= result;
#endif

}



void RXMainWindow::onSetColor(RXGRAPHICOBJECT id,const char*color)
{
#ifndef USE_GLC
  // qDebug()<<" RXMainWindow::onSetColor  "<<color<<"on "<<id->getName();
#elif defined (USE_GLC)
  GLWidgetPreprocess::SetColor(id,color);
#endif
}

void RXMainWindow::onDeleteGraphicObject(RXGRAPHICOBJECT ptr)
{
  assert(0);
}

// march 2013 this fn is painfully slow so lets explore
// NOT calling it recursively.
// BBUUTT  maybe theer was a good reason like memory leakage
//for doing the recursion
int RXMainWindow::deleteSOChildren(RXGRAPHICSEGMENT  n)
{
#if defined (COIN3D)
  int rc=0;
  if(0) {
    rc=n->getNumChildren();

    for(int i=0;i< n->getNumChildren();i++){
      SoNode *c = n->getChild(i);
      SoGroup *g = dynamic_cast<SoGroup *>(c);
      if(g) {

        rc+=deleteSOChildren(g);
      }
    }
  }
  n->removeAllChildren();

  return rc;
#endif
  return 0;
}

void RXMainWindow::onFlushGraphicsNode(RXGRAPHICSEGMENT  n)
{
#if defined (COIN3D)
  //    qDebug()<<"onFlushGraphicsNode"<<n->getName().getString()<<"nc="<<n->getNumChildren()<<"thread is"<<QThread::currentThreadId();
  deleteSOChildren(n);

#elif defined(USE_GLC)
  GLWidgetPreprocess::flushnode(n) ;// cant figure this. Thats why we dropped GLC
#endif
}

void RXMainWindow::outcallback( const char* ptr, std::streamsize count, void *pTextBox )
{
  // int count;

  //   qDebug()<<"try outcallback: thread is "<<QThread::currentThreadId();
  QMutexLocker lck(&(g_rxmw->m_ocbmutex));
  QString input;
  static QString lLine;
  const char *lp = ptr;
  int i;
  assert(ptr);
  for(i=0;i<count;i++,lp++){
    char dum = *lp;
    char dum2 = dum&127; // keep the character set simple
    if(dum !=dum2)
      qDebug()<< " dum is '"<<dum<<"' dum2 is '"<<dum2<<"'";
    input.append( dum2 );
    //    input.append( *lp  );
  }
  // qDebug()<<"(RXMainWindow::outcallback)input string is" <<input<<endl;
  // logic::
  // if input doesnt contain a EOL, we append it to lLine and return
  //  if it does contain a EOL, {
  //     we append it up to but not including the EOL , to lLine
  //     we remove the first chars from input;
  //     we send lLine.
  //     we flush lLine
  //      if input is short we break;
  //    }
  for(;!input.isEmpty();){
    i = input.indexOf("\n");
    if(i<0){
      lLine.append(input);
      return;
    }
    lLine.append(input.left(i)); input.remove(0,i+1);
    qDebug()<< QString("(outCB):")<<lLine;
    if(cout.bad())
      qDebug()<< "BAD cout";

    emit g_rxmw->RXPleasePrint(NULL, lLine);
    lLine.clear();
  }
}

//////////////////////////////////////////////////////////////////////////////////////////
rxWorkerThread::rxWorkerThread(QWidget *parent):
  m_rxw(0)
{
  this->setObjectName("TheWorkerThread"); 	assert(!cout.bad());
}

rxWorkerThread::~rxWorkerThread()
{
  this->quit();
  this-> wait();
}


void rxWorkerThread::run()
{
  this->setPriority(QThread::HighestPriority); // not sure this does much for us
  m_rxw  =new  rxWorker() ;

  QThread::exec();
  delete m_rxw;
}
class rxWorker *rxWorker::m_this;

rxWorker::rxWorker(QWidget *parent)
{	
  qDebug()<<"rxWorker::rxWorker: thread is "<<QThread::currentThreadId();
  m_this=this;assert(!cout.bad());
  int argc=0; char**argv=0;
  //qRegisterMetaType<RXGRAPHICSEGMENT>("RXGRAPHICSEGMENT"); april 2012 already done in rxmw constructor
  //qRegisterMetaType<RXGRAPHICOBJECT>("RXGRAPHICOBJECT");
  connect(this,SIGNAL(pleaseMsgBox(QObject*,const QString &,int*))
          ,g_rxmw,SLOT(onMsgbox_request(QObject*,const QString &,int*))
          ,Qt::BlockingQueuedConnection) ;
  connect(this,SIGNAL(pleaseStatusBar(QObject*,const QString & ))
          ,g_rxmw,SLOT(onStatusbarRequest(QObject*,const QString &))) ;
  connect(this,SIGNAL(pleaseExit())
          ,g_rxmw,SLOT(onActionExit_triggered())) ;
  connect(this,SIGNAL(pleaseUserChoice(QObject *,const QString  ,const QStringList,QString *))
          ,g_rxmw,SLOT(onUserchoiceRequest(QObject*,const QString,const QStringList,QString *)) ,Qt::BlockingQueuedConnection) ;

  connect(this,SIGNAL(pleaseGetFileName(QObject*,const QString,const QString,const QString,QString *))
          ,g_rxmw,SLOT(onFilename_request(QObject*,const QString,const QString,const QString,QString *)) ,Qt::BlockingQueuedConnection) ;


  connect(this,SIGNAL(pleaseGetSaveFileName(QObject*,const QString,const QString,const QString,QString *))
          ,g_rxmw,SLOT(onSavefilename_request(QObject*,const QString,const QString,const QString,QString *))   ,Qt::BlockingQueuedConnection) ;

  connect(this,SIGNAL(pleaseGetDirName(QObject*,const QString,const QString,const QString,QString *))
          ,g_rxmw,SLOT( onDirnameRequest(QObject*,const QString,const QString,const QString,QString *))   ,Qt::BlockingQueuedConnection) ;


  connect(g_rxmw,SIGNAL(RXCommand(QObject *, const QString &)), this, SLOT(rxExecute_w(QObject *, const QString &)));

  // maybe thes graphics calls dont need to be blocking but its safer.And easier to debug

  connect(this,SIGNAL(pleaseNewWindow(const QString,void *, RXGRAPHICSEGMENT*)),
          g_rxmw,SLOT(onNewWindow(const QString , void *,RXGRAPHICSEGMENT*)) ,Qt::BlockingQueuedConnection);

  connect(this,SIGNAL(pleaseDeleteWindow(void *, int*)),
          g_rxmw,SLOT(onDeleteWindow( void *,int*)) ,Qt::BlockingQueuedConnection);
  connect(this,SIGNAL(pleaseNewGNode(const QString ,RXGRAPHICSEGMENT,RXGRAPHICSEGMENT*)),
          g_rxmw,SLOT(onNewGNode(const QString ,RXGRAPHICSEGMENT,RXGRAPHICSEGMENT*)),Qt::BlockingQueuedConnection);


  connect(this,SIGNAL(pleaseGNodeAddChild(RXGRAPHICSEGMENT ,RXGRAPHICSEGMENT ,RXGRAPHICSEGMENT*  )),
          g_rxmw,SLOT(onGNodeAddChild(RXGRAPHICSEGMENT,RXGRAPHICSEGMENT ,RXGRAPHICSEGMENT*   )),Qt::BlockingQueuedConnection);

  connect(this,SIGNAL(pleaseGNodeRemoveChild( RXGRAPHICSEGMENT , RXGRAPHICSEGMENT   ,RXGRAPHICSEGMENT*  )),
          g_rxmw,SLOT(onGNodeRemoveChild( RXGRAPHICSEGMENT , RXGRAPHICSEGMENT   ,RXGRAPHICSEGMENT*   )),Qt::BlockingQueuedConnection);



  connect(this,SIGNAL(PleaseUpdateAllGraphics (void)),
          g_rxmw,SLOT(UpdateAllGraphicsRequested(void)));


  connect(this,SIGNAL(pleaseWrite(const char*,RXGRAPHICSEGMENT)),
          g_rxmw,SLOT(WriteRequested(const char*,RXGRAPHICSEGMENT)));// ,Qt::BlockingQueuedConnection);
  //  pleaseInsertNurbsCurve(const class ON_NurbsCurve &c,RXGRAPHICSEGMENT o, RXGRAPHICOBJECT*rc);
  connect(this,SIGNAL(pleaseInsertNurbsCurve(const class ON_NurbsCurve *,RXGRAPHICSEGMENT,RXGRAPHICOBJECT*)),
          g_rxmw,SLOT(onInsertNurbsCurve(const class ON_NurbsCurve *,RXGRAPHICSEGMENT,RXGRAPHICOBJECT*))
          ,Qt::BlockingQueuedConnection );

  connect(this,SIGNAL(pleaseInsertPolyline(const int,const float*,RXGRAPHICSEGMENT,RXGRAPHICOBJECT*)),
          g_rxmw,SLOT(onInsertPolyline(const int,const float*,RXGRAPHICSEGMENT,RXGRAPHICOBJECT*))
          ,Qt::BlockingQueuedConnection  );

  connect(this,SIGNAL(pleaseInsertPolyhedron(const float* ,const int ,
                                             const int*, const int ,
                                             RXGRAPHICSEGMENT ,RXGRAPHICOBJECT*) ),
          g_rxmw,SLOT(onInsertPolyhedron(const float* ,const int ,
                                         const int*, const int ,
                                         RXGRAPHICSEGMENT ,RXGRAPHICOBJECT*) )
          ,Qt::BlockingQueuedConnection  );


  //    void onInsertPolyhedron(const float* xyz,const int cp,
  //  const int*faceSet, const int cf,
  //  RXGRAPHICSEGMENT o ,RXGRAPHICOBJECT *rc);

  connect(this,SIGNAL(pleaseDeleteGraphicObject(RXGRAPHICOBJECT)),
          g_rxmw,SLOT(onDeleteGraphicObject(RXGRAPHICOBJECT)) );

  connect(this,SIGNAL(pleaseSetColor(RXGRAPHICOBJECT,const char*)),
          g_rxmw,SLOT(onSetColor(RXGRAPHICOBJECT,const char*)));//,Qt::BlockingQueuedConnection);

  connect(this,SIGNAL(pleaseFlushGraphicsNode(RXGRAPHICSEGMENT)  ),
          g_rxmw,SLOT(onFlushGraphicsNode(RXGRAPHICSEGMENT)));//,Qt::BlockingQueuedConnection);

  this->setObjectName("TheWorker"); 	assert(!cout.bad());
  RXStart(argc, argv, g_rxmw); // OK its thread 12
  assert(!cout.bad());
}
rxWorker::~rxWorker(){
  //  RXEnd();
}
void rxWorker::rxExecute_w(QObject *client, const QString &s)
{//OK we are in RXWorkerThread. the arg 'client'  may be null.
  RXExecute(s , client); // I THINK its client - not sure
}


//
/*static */  int rxWorker::shutdown() {
  int k=0;
  emit rxWorker::TheOnly()->pleaseExit() ;
  return k;
}
/*static */  int rxWorker::setStatusBar(const QString &s,QObject *client,const int onright) {
  int k=0;
  emit rxWorker::TheOnly()->pleaseStatusBar(client,s);
  return k;
}
//static
QString rxWorker::UserChoice(const QString & prompt,const QStringList &candidates)
{QObject *client=0;
  QString rc;
  emit rxWorker::TheOnly()->pleaseUserChoice(client ,prompt , candidates,&rc );
  return rc;
}

// static
QString rxWorker::rxOpenFileName(QObject *client,
                                 const QString caption,
                                 const QString dir,
                                 const QString filter
                                 )
{
  QString fName; // we are in thread 3
  emit rxWorker::TheOnly()->pleaseGetFileName(client ,caption ,dir,filter ,&fName);
  return fName;
}
QString rxWorker::rxSaveFileName(QObject *client,
                                 const QString caption,
                                 const QString dir,
                                 const QString filter
                                 )
{
  QString k;
  emit rxWorker::TheOnly()->pleaseGetSaveFileName(client ,caption ,dir,filter ,&k);
  return k;
}
QString rxWorker::rxDirName(QObject *client,
                            const QString caption,
                            const QString dir,
                            const QString filter
                            )
{
  QString k;
  emit rxWorker::TheOnly()->pleaseGetDirName(client ,caption ,dir,filter ,&k);
  return k;
}

int RXPrint(QObject *q, const char*s,const int level)
{
  int rc=0; assert(!cout.bad());
  g_rxmw->RXPrint(q, QString(s),level);
  assert(!cout.bad());
  return rc;
}
int rxWorkerDisplayMsgBox(QObject *q, const QString qs,const int level)
{
  int lev,rc=0;
  lev = level; if   ( g_NON_INTERACTIVE ) lev=1;
  rc=g_rxmw->DisplayMsgBox(q,qs,lev);
  assert(!cout.bad());
  return rc;
}

int  RXMainWindow::RXPrint(QObject *q, const QString &s,const int level)
{
  int rc=0;
  QString aa = QString("(from RX): ")+s;
  emit g_rxmw->RXPleasePrint(q,aa  );
  return rc;
}

int  RXMainWindow::DisplayMsgBox(QObject *q, const QString &s,const int level)
{
  int rc=0;
  emit g_rxmw->RXPleaseMsgBox(q,s ,level,&rc );
  return rc;
}


// static
RXGRAPHICSEGMENT  rxWorker::NewWindow(const QString acaption, void *modelptr)
{
  RXGRAPHICSEGMENT  rc=0;
  QString caption=acaption;
  emit g_rxmw->mWorkerThread->m_rxw->pleaseNewWindow(caption,modelptr, &rc);
  return rc;
}

// static
int rxWorker::DeleteWindow( void *viewfrmptr)
{
  int  rc=0;
  emit g_rxmw->mWorkerThread->m_rxw->pleaseDeleteWindow(viewfrmptr, &rc);
  return rc;
}

// static
RXGRAPHICSEGMENT rxWorker::NewGNode(const char* caption,RXGRAPHICSEGMENT  pParent)
{
  RXGRAPHICSEGMENT  rc=0;
  emit rxWorker::TheOnly()->pleaseNewGNode(QString(caption),pParent,&rc);
  return rc;
}
//static
RXGRAPHICSEGMENT rxWorker::GNodeAddChild(RXGRAPHICSEGMENT pParent,RXGRAPHICSEGMENT pChild)
{
  RXGRAPHICSEGMENT  rc=0;
  emit rxWorker::TheOnly()->pleaseGNodeAddChild(  pParent,   pChild, &rc);
  return rc;
}

//static
RXGRAPHICSEGMENT rxWorker::GNodeRemoveChild(RXGRAPHICSEGMENT  pParent,RXGRAPHICSEGMENT pChild)
{
  RXGRAPHICSEGMENT rc=0;
  emit rxWorker::TheOnly()->pleaseGNodeRemoveChild(  pParent,   pChild, &rc);
  return rc;
}

//static
void rxWorker::UpdateAllGraphics(void)
{
  emit rxWorker::TheOnly()->PleaseUpdateAllGraphics();
}

//static
int  rxWorker::GNodeWrite(const char* filename,RXGRAPHICSEGMENT  o)
{
  int rc=1;
  emit rxWorker::TheOnly()->pleaseWrite(filename,o);//(const char* filename,RXGRAPHICSEGMENT o)
  return rc;
}
//static
RXGRAPHICOBJECT  rxWorker::InsertNurbsCurve(const class ON_NurbsCurve *c,RXGRAPHICSEGMENT  o)
{
  RXGRAPHICOBJECT rc=0;
  emit rxWorker::TheOnly()->pleaseInsertNurbsCurve(c,o,&rc);
  return rc;
}

//static
RXGRAPHICOBJECT rxWorker::InsertPolyline(const int count,const float*poly,RXGRAPHICSEGMENT  o)
{
  RXGRAPHICOBJECT rc=0;
  emit rxWorker::TheOnly()->pleaseInsertPolyline(count,poly,o,&rc);
  return rc;
}
//static
RXGRAPHICOBJECT rxWorker::InsertPolyhedron(const float* xyz,const int cp,
                                           const int*faceSet, const int cf,
                                           RXGRAPHICSEGMENT  o)
{
  RXGRAPHICOBJECT rc=0;
  emit rxWorker::TheOnly()->pleaseInsertPolyhedron(xyz,cp,faceSet,cf,o,&rc);
  return rc;
}

//static
void rxWorker::SetColor( RXGRAPHICOBJECT id,const char*color)
{
  emit rxWorker::TheOnly()->pleaseSetColor(id,color);
}

// static
bool rxWorker::DeleteGraphicObject(RXGRAPHICOBJECT ptr)
{
  bool rc=0;
  emit rxWorker::TheOnly()->pleaseDeleteGraphicObject(ptr);
  return rc;
}
// static
bool rxWorker::FlushGraphicsNode (RXGRAPHICSEGMENT  n)
{
  bool rc=0;
  if(n){
    // QString s( n->getName().getString());
    emit rxWorker::TheOnly()->pleaseFlushGraphicsNode(n);
  }
  else
    qDebug()<<"skipFlushGraphicsNode on null node";
  return rc;
}

void RXMainWindow::on_actionOpen_triggered()
{
  emit RXCommand(this, "macro: " );
}

void RXMainWindow::on_actionNew_triggered ()
{
  emit RXCommand(this, "macro: " );
}
