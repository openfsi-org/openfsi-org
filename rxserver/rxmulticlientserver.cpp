#include "StdAfx.h"
#include "rxmulticlientserver.h"
#include "rxmainwindow.h"
#include "rxtcpsocket.h"
#include <QDebug>
#include <QFile>
#include <QTime>

RXMultiClientServer::RXMultiClientServer(QObject *parent) :
    QTcpServer(parent)
{
    connect(this, SIGNAL(newConnection()), this, SLOT(handleNewConnection()));
    connect(this,SIGNAL(RXMCVCommand(QObject *, const QString &)), g_rxmw, SIGNAL(RXCommand(QObject *, const QString &)));

}
RXMultiClientServer::~RXMultiClientServer()
{
    stop();
}
void RXMultiClientServer::stop()
{
    close();
    qDeleteAll(clientConnections);

}
void RXMultiClientServer::start()
{
    quint16 ServerPort =61291;

    if(!this->listen(QHostAddress::Any,ServerPort))
        qDebug()<<"cannot start Socket Server" << this->errorString();
    else
        qDebug()<<"server is running on"<<this->serverAddress()<<this->serverPort();
    this->setObjectName("myRXMCServer");
}


void RXMultiClientServer::handleNewConnection()
{
    //   qDebug()<<"we subclass QTcpSocket so that it's THIS is available in its readOnSocket slot";
    RXTcpSocket *client =  (RXTcpSocket * ) nextPendingConnection();
    connect(client, SIGNAL(readyRead()), this, SLOT(readOnSocket()));
    connect(client, SIGNAL(disconnected()), this, SLOT(clientDisconnected()));
    clientConnections.append(client);
    sendHello(client);
}


void RXMultiClientServer::clientDisconnected()
{
    RXTcpSocket *client = qobject_cast<RXTcpSocket *>(sender());

    if (!client)
        return;

    clientConnections.removeAll(client);
    client->deleteLater();
}


void RXMultiClientServer::sendHello(RXTcpSocket *client)
{
    if (!client)
        return;
    WriteOnSocket(client,QString("hello from RX server\n"));

}
void RXMultiClientServer::WriteOnSocket(RXTcpSocket *client, const QString &s)

{   qint64 nSent;
    if(!client)
    {
        return;
    }
    qint64 nccc;
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_7);
    out << (qint64)s.length(); // all this was quint16
    out << qPrintable(s); // was just S
   // qDebug()<<"MCS sending string length = "<<s.length();
    out.device()->seek(0);
    nccc= (qint64)(s.size()+1);
    nccc+=  sizeof(qint32); // the qstring header
    out << nccc;
 //qDebug()<<" announcing packet size "<< nccc;
    nSent = client->write(block);
    if(nSent<0) {
        qDebug()<< "failed to send QString of length "<<s.size();
    }
    //    else
    //       qDebug()<< "write returned "<<nSent<< " on sending block of length " << block.size()<< ", QString length= "<<s.size();
}
void RXMultiClientServer::readOnSocket( )
{
    RXTcpSocket *client = qobject_cast<RXTcpSocket *>(sender());   //  QTimer *timer = qobject_cast<QTimer *>(obj);
    if(!client)
        return;
    QString s;
    QDataStream in(client);
    in.setVersion(QDataStream::Qt_4_7);
    quint16 blockSize=0;
    if (blockSize == 0) {
        if (client->bytesAvailable() < (int)sizeof(quint16))
            return;

        in >> blockSize;
    }

    if (client->bytesAvailable() < blockSize)
        return;

    in >> s;

    Interpret(client,s);

}

void RXMultiClientServer::sendMessageToAllClients(const QString &s)
{
    QString message = s  + QString(" Time=: %1\n").arg(QTime::currentTime().toString("hh:mm:ss"));

    foreach(QTcpSocket *client, clientConnections) {
        WriteOnSocket(client, message);
    }
}
void RXMultiClientServer::Interpret(RXTcpSocket *client, const QString &s){
    //   qDebug()<<"RXMCS::Interpret  received <"<<s;
    //   qDebug()<<"RXMCS::Interpret nchars received <"<<s.size();
    //    if first word is 'iam'
    //    else if its 'ping'
    //    else pass on the command.
   // qDebug( )<<"receive\t"<<QDateTime::currentDateTime () <<"\t"<<s<<"\n";

    if(s.startsWith("iam")) {
        QString name = s.mid(4);
        client->setObjectName(name);
    }
    else if(s.startsWith("ping")) {
        QString text = s.mid(5);
        this->WriteOnSocket(client,text);
    }
    else {

        emit RXMCVCommand(client,s);
    }
}
