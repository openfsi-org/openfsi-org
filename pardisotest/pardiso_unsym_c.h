#ifndef PARDISO_UNSYM_C_H
#define PARDISO_UNSYM_C_H

#include "RXHarwellBoeingIO.h"
class pardiso_unsym_c
{
public:
    pardiso_unsym_c();
    int solveharddata();
        int solveunsym();
    int load ( const char*fname);
    class RXHarwellBoeingIO m_hb;
};

#endif // PARDISO_UNSYM_C_H
