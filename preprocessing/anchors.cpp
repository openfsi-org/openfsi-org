/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by	P Heppel
 *
 *
 *
 * Modified :
 * August 2004 exports displacements if the site has the flag set
 * started June 1997. Based on r2af.c, this generates a list of anchor loads.
 See r2af for a complete template
 */
#include "StdAfx.h"
#include "RelaxWorld.h"
#include "RXSail.h"
#include "RXSRelSite.h"
#include "f90_to_c.h"
#include "RXDatabaseI.h"
#include "summary.h"
#include "cfromf.h"


int Anchor_Loads(double *tmax) // also posts the displacements. prototype is in cfromf.h
{
    int j,k;
    size_t ip;
    char  name[256]; //errString[120],
    FILE *fp;
    double lim;
    RXSRelSite *s;
    int sli;
    double xin[3],l_r[3], l_tot[3];
    RXSTRING dum;

    //if the DB 'Status' is '-1 Running' we ony post the reactions with an explicit flag
    // otherwise we flood the DB with nodal reactions
    bool AnalysisIsConverged = false;
    class RXMirrorDBI *sum = g_World->DBMirror();
    if( sum)
    {
        char buffer[256];
        if(sum->Extract_One_Text("Status",buffer,255))
            if(!streq(buffer,"-1 Running"))
                AnalysisIsConverged =true;
    }

    lim = *tmax;

    fp = RXFOPEN("anchorloads.txt","w");
    l_tot[0]= l_tot[1]= l_tot[2] = 0.0;

    fprintf(fp," \n Anchor Loads, Clipped at %f N export flags=%d, IsConverged=%d\n",lim,g_Export_Reactions,AnalysisIsConverged);

    vector<RXSail*> ll = g_World->ListModels(RXSAIL_ALLHOISTED); // boat too
    for(ip=0;ip<ll.size();ip++)  {
        SAIL *sail = ll[ip];

        sli = sail->SailListIndex();
        fprintf(fp,"! %s (%S)", sail->GetType().c_str() , sail->GetName().c_str() );
        fprintf(fp,"N   \t name    \tFx(KN)    \t Fy (KN)   \t Fz (KN)  \t at \t      X     \t    Y   \t   Z\n");

        for(vector<RX_FESite*>::iterator it=sail->m_Fnodes.begin();it!=sail->m_Fnodes.end();++it) {
            s =dynamic_cast< RXSRelSite*> ( *it);
            if((s != NULL) && (s->GetN() >0)) {

                j = s->GetN() ;
                cf_get_coords(sli,j,xin);
                cf_get_raw_residual(sli,j,l_r);
                int ExplicitExport =  s->RXEntity::AttributeGet(L"$export_reaction",dum);
                if(fabs( l_r[0]) > lim || fabs( l_r[1]) > lim  || fabs(l_r[2]) > lim)  {

                    for(k=0;k<3;k++) {
                        l_tot[k] = l_tot[k] + l_r[k];
                    }
                    sprintf(name," %s",s->name());

                    // displacements are logged on those sites with "$export_Reaction"
                    // depending on the value of setting 'Export_Reactions'
                    // options are:  All gt tol (-1)
                    // or none (0)
                    // or flagged (the default) (-2)
                    // or an integer value = 1&&2&&4&&8&&16&&32 for exporting 123456 respectively
                    int ger = g_Export_Reactions;
                    if (!AnalysisIsConverged )  { // only allow explicitly exported reactions
                        if(ger!=0) ger=-2;
                    }

                    bool do_X = (((ger ==-1 )&& fabs( l_r[0]) > lim)
                             || ((ger==-2 )&& ExplicitExport)
                                 || ((ger>0 )&&  (ger&1 ) && fabs( l_r[0]) > lim)) ;
                    bool do_Y = (((ger ==-1) && fabs( l_r[1]) > lim )
                                 || ((ger==-2) && ExplicitExport)
                                 || ((ger>0 )&& (ger&2 ) && fabs( l_r[1]) > lim )) ;

                    bool do_Z = (((ger ==-1) && fabs( l_r[2]) > lim )
                                 || ((ger==-2 )&& ExplicitExport)
                                 || ((ger>0) && (ger&4)  && fabs( l_r[2]) > lim)) ;

                    char lab[256],v[256];
                    if(do_X ){
                        sprintf(lab,"Reaction$%s$X",s->name());
                        sprintf(v,"%f",l_r[0]);
                        Post_Summary_By_Sail(s->Esail,lab,v);
                    }
                    if(do_Y ){
                        sprintf(lab,"Reaction$%s$Y",s->name());
                        sprintf(v,"%f",l_r[1]);
                        Post_Summary_By_Sail(s->Esail,lab,v);
                    }
                    if(do_Z){
                        sprintf(lab,"Reaction$%s$Z",s->name());
                        sprintf(v,"%f",l_r[ 2]);
                        Post_Summary_By_Sail(s->Esail,lab,v);
                    }

                    fprintf(fp,"%d ",j);
                    fprintf(fp,"\t%9s",name);
                    fprintf(fp," ( \t%11.4f \t%11.4f \t%11.4f\t )",l_r[0]/1000.,l_r[1]/1000.,l_r[2]/1000.);
                    fprintf(fp," ( \t%8.3f \t %8.3f \t%8.3f\t ) ",xin[0],xin[1],xin[2]);
                    fprintf(fp,"\t  flags are ( \t%d \t %d \t%d )\n ",do_X,do_Y,do_Z);
                }
            }
        }

        //} if sail
    } // for ip
    fprintf(fp,"\n");
    fprintf(fp,"Sum of residuals (N) \t%f \t%f \t%f\n\n",l_tot[0],l_tot[1],l_tot[2]);

    FCLOSE(fp); fp=NULL;

    return(0);
}//int Anchor_Loads


