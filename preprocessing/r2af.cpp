/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by	P Heppel
 *
 *
 *
 * Modified :
 *
 * started 13/8/96
 */
/* routine to export residual forces on each of the 
 * nodes in each of the models in an AIRFRAME script format
LOGIC
FOR EACH MODEL IN TURN
A)  zero the droploads 
B)  export a set of DROPLOAD cards for all sliding nodes.
C) export a datblock of loads for all other nodes

So as a start, lets dump all nodes with non-zero R, together with node name and attributes
if they exist
 */
#include "StdAfx.h"
#include "RelaxWorld.h"
#include "RXSail.h"
#include "RX_FESite.h"
#include "RXEntity.h"
#include "stringutils.h"

#include "f90_to_c.h"

#include "global_declarations.h"

#include "r2af.h"

//int  Sliding(char*ain)
//{
//    /* returns 1 if a is the atts of a sliding node, else 0
//a sliding node has "$fix=something" */
//    char *lp;
//    char *a = STRDUP(ain);

//    if((lp=strstr(a,"$fix"))) {
//        lp+=4;
//        PC_Strip_Leading(lp);
//        if(*lp=='=') {
//            lp++;
//            if(*lp) {
//                printf(" Routine Sliding on '%s' got '%s'\n",ain,lp);
//                return 1;
//            }
//        }

//    }


//    RXFREE(a);
//    return 0;
//}//int  Sliding

int R_To_Af(double *r, double *tmax)
{
    int ip,j,jr=-5;
    RXEntity_p e;
    char buf[256], seg[256];
    static FILE *fp=NULL;
    double lim;
    RX_FESite *s;
    double xin[3],xout[3];

    lim = *tmax;

    if(!fp) {
        strcpy(seg,g_World->GetRunName());
        PC_Strip_Leading(seg);
        sprintf(buf,"%s/resids_%s.txt",traceDir,seg);
        fp = RXFOPEN(buf,"w");
        printf("Resids output in %s\n", buf);


    }

    fprintf(fp,"data \t (node/...,beam/...,bar/...,rod/...) \t drx=\n");
    fprintf(fp,"data \t (node/...,beam/...,bar/...,rod/...) \t dry=\n");
    fprintf(fp,"data \t (node/...,beam/...,bar/...,rod/...) \t drz=\n");

    fprintf(fp,"data \t ?root\t additive=off\n");
    fprintf(fp,"data \t ?root\t distributive=on\n");
    fprintf(fp,"data \t ?root\t additive=on\n");

    fprintf(fp,"template\tlabel\t drx \tdry \t drz\n");
    cout<< " r2af with listmodel"<<endl;
    vector<RXSail*> ll = g_World->ListModels(RXSAIL_ALLHOISTED); // boat too
    for(ip=0;ip<ll.size();ip++)  {
        SAIL *sail = ll[ip];
        fprintf(fp,"! %s \n", sail->GetType().c_str());

        for(vector<RX_FESite*>::iterator it=sail->m_Fnodes.begin();it!=sail->m_Fnodes.end();++it) {
            s= *it;
            if(s  &&  s->GetN() >0 ) {
                int j2;
                j = s->GetN();
                cf_get_coords(sail->SailListIndex(),j,xin);

                assert(jr!=-5);
                if(fabs( r[jr*3]) > lim || fabs( r[jr*3 + 1]) > lim  || fabs(r[jr*3 + 2]) > lim)  {
                    if(e = dynamic_cast<RXEntity*> (s )) { // ( *s)->m_SiteOwner) {
                        QString v;
                        int IsSliding=0;
                        if(e->AttributeGet("$fix",v))
                            IsSliding= (!v.isEmpty()&&  v !="full");


                        if(IsSliding )

                            strcpy(seg,"(beam/...,rod/...,bar/...)");
                        else
                            strcpy(seg," node/... ");
                        strcpy(seg," node/...                 ");

                    }
                    else {
                        fprintf(fp,"!     NULL OWNER \t");
                        strcpy(seg," node/... ");
                    }
                    /*	fprintf(fp,"%15.8f %15.8f %15.8f %d",
                    (*s)->x + (*s)->dx ,(*s)->y + (*s)->dy,(*s)->z + (*s)->dz,(*s)->n);

                    fprintf(fp,"! Xglob=%d (%f \t %f \t%f )\n ",j,xin[0],xin[1],xin[2]); */


                    ON_3dPoint pp = g_boat-> XToLocal(ON_3dPoint(xout));
                    fprintf(fp," data\t nearest in %s [% 12.3f  % 12.3f % 12.3f ]", seg, pp.x,pp.y,pp.z);

                    /*	fprintf(fp," \nRES=%d ( %12.5f \t%12.5f \t%12.5f ) \n",jr,r[jr*3],r[jr*3 +1],r[jr*3 +2]);	*/

                    ON_3dVector vv = g_boat->VToLocal(ON_3dVector( &(r[jr*3]) ));
                    //untransform_vector_(&boatModel,&( r[jr*3] ),xout);
                    fprintf(fp," \t%12.5f \t%12.5f \t%12.5f \n", vv.x,vv.y,vv.z);

                }
            }
        }

        //} if sail
    }
    fprintf(fp,"endblock\n");
    fprintf(fp,"data \t ?root\t additive=off,distributive=off\n");
    FCLOSE(fp); fp=NULL;

    return(0);
}//int R_To_Af


