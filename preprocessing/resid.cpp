/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	12/8/96		tmax as arg. dont plot or file results < this. 
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"
 #include "RXGraphicCalls.h"
#include "RelaxWorld.h"
#include "RXSail.h"
#include "RXSRelSite.h"
#include "global_declarations.h"
#include "f90_to_c.h"

#include "cfromf.h"

#define LENGTH_FACTOR ((float) 5e-4)

int Draw_Resids(double*tmax)
{
int i,j;
float x1,Y1,z1,x2,y2,z2;

char buf[256],errString[256];
static FILE *fp=NULL;
 double lim;


lim = *tmax;

	if(!fp) {
		sprintf(buf,"%s/resids.out",traceDir);
		fp = RXFOPEN(buf,"w");
		if(!fp) {
			sprintf(errString, " cannot open %s\n",buf); 
			g_World->OutputToClient(errString,2);
			fp = stdout;
		}
	}

 	vector<RXSail*> ll = g_World->ListModels(RXSAIL_ALLHOISTED); // boat too
	for(i=0;i<ll.size();i++)  {
		SAIL *sail = ll[i];
		double v[3], l_r[3];

			int sli = sail->SailListIndex();
				HC_Open_Segment_By_Key( sail->PostProcNonExclusive());
				HC_Open_Segment("residuals");

		 	  	HC_Flush_Contents(".","geometry,subsegment,include");  
				HC_Set_Color("lines=navy blue");
				if(fp) {
					fprintf(fp," sail_%s_%d %s \n", sail->GetType().c_str(),sli, sail->GetType().c_str() );
					fprintf(fp,"%5s \t%12s \t%12s \t%12s \t %s\t %s\t%s\n","N ","rx ","ry ","rz ", "x ","Y ","z ");	
					}
				for(vector<RX_FESite*>::iterator it=sail->m_Fnodes.begin();it!=sail->m_Fnodes.end();++it) {
					RX_FESite *s= *it;
					if(!s) continue;
					if(s->GetN()<=0 ) 
						continue;
					j = s->GetN();

					cf_get_coords(sli,j,v);
					cf_get_raw_residual(sli,j,l_r);
					if(_isnan(v[0])) v[0]=0; if(_isnan(v[1])) v[1]=0; if(_isnan(v[2])) v[2]=0; 
 					x1 = v[0]; 	 
 					Y1 = v[1];assert(!_isnan(v[1]));		 
 					z1 = v[2];assert(!_isnan(v[2]));		 
					x2 = x1 + l_r[0]*LENGTH_FACTOR; 	
					y2 = Y1 + l_r[1]*LENGTH_FACTOR; 	
					z2 = z1 + l_r[2]*LENGTH_FACTOR;	
					
					if(fabs( l_r[0]) > lim || fabs( l_r[1]) > lim  || fabs(l_r[2]) > lim) {
						if(fp)
							fprintf(fp,"%5d \t%12.5f \t%12.5f \t%12.5f \t %f \t %f \t%f\n",
							j,l_r[0],l_r[1],l_r[2], x1,Y1,z1);
                            RX_Insert_Line(x1,Y1,z1,x2,y2,z2,sail->GNode());
					}
				} //for
  			    HC_Close_Segment();
  			   HC_Close_Segment();
	}
if(fp) FCLOSE(fp); fp=NULL;

return(0);
}//int Draw_Resids


