///////////////////////////////////////////////////////////////////////////
//                                                                       //
//  Simple C++ implementation of:                                        //
//   "Generalized Barycentric Coordinates on Irregular Polygons"         //
//   by Mark Meyer, Haeyong Lee, Alan Barr, and Mathieu Desbrun          //
//   appearing in The Journal of Graphics Tools, 7(1):13-22, 2002        //
//                                                                       //
//   This code calculates the barycentric coordinates for a point        //
//   within a convex, irregular polygon.                                 //
//                                                                       //
//   Note, this code only works for a point that is strictly inside the  //
//   polygon.  If the point is on (or too close to) an edge, this code   //
//   will divide by zero.  As discussed in the text, this can be tested  //
//   by checking if |(q[j+1]-q[j]) X (p-q[j])| <= EPSILON * |p-q[j]|     //
//   and if so doing linear interpolation between q[j] and q[j+1]        //
//                                                                       //
///////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
 
#include "barycentric.h"
#include "layernode.h"

#define crossLength(a,b) (fabs(a.x * b.y - a.y * b.x))

/////////////////////////////////////////////////////////////////////
//  Computes the cotangent at b
/////////////////////////////////////////////////////////////////////
double
cotangent(const ON_2dPoint& a, const ON_2dPoint& b, const ON_2dPoint& c)
{
	ON_2dVector ba = a - b;
	ON_2dVector bc = c - b;

	return ((bc*ba)/(crossLength(ba,bc)));
}

/////////////////////////////////////////////////////////////////////
//  Computes the barycentric coordinates of p in the n-gon defined
//   by the (counter-clockwise) points q, and returns them in w.
/////////////////////////////////////////////////////////////////////
int 
meyerBarycentric(const ON_2dPoint& p, const ON_2dPointArray &q,  ON_SimpleArray<double> *w)
{
	int j, prev, next;
	double a, weightSum = 0.f;
	int n = q.Count();
	w->Empty();  

	// For each vertex q[j] of Q:
	//   Grab the previous and next q's and compute the barycentric weight
	for(j = 0; j < n; j++)
	{
		prev = (j+n-1)%n;
		next = (j+1)%n;
		double lenSq = (p - q[j]).LengthSquared();
		a = (cotangent(p,q[j],q[prev]) + cotangent(p,q[j],q[next])) / lenSq;
		w->Append(a);  
		weightSum += a;
	}

	// Normalize the weights
	for(j = 0; j < n; j++)
		(*w)[j] /= weightSum;
	return 1;
}


/////////////////////////////////////////////////////////////////////
//  Computes the barycentric coordinates using different argument type
/////////////////////////////////////////////////////////////////////
int meyerBarycentric(const ON_2dPoint& p, 	ON_SimpleArray<layernode *> &thePts,  ON_SimpleArray<double> *w) 
{
	ON_2dPointArray q; 
	int j, n = thePts.Count();
	q.Empty(); 
	for(j = 0; j < n; j++)
		q.Append(ON_2dPoint(thePts[j]->m_u, thePts[j]->m_v));

	return meyerBarycentric( p,q,w);
}

////////////////// JUST FOR TESTING ////////////////////////////////////
#define N 6
#ifdef _MEYER_MAINFUNCTION
int
ma in()
{
	ON_2dPoint q[N];
	for(int i = 0; i < N; i++)
	{
		q[i].x = cos((((double)i)/N)*6.28);
		q[i].y = sin((((double)i)/N)*6.28);
	}
	q[1].x = .8;
	ON_2dPoint p(0.5,0.3);

	double w[N];
	meyerBarycentric(p,q,N,w);

	ON_2dPoint r(0.,0.);
	for(int i = 0; i < N; i++)
	{
		printf("q%i = %10g\t%10g,\tw = %f\n",i,q[i].x,q[i].y,w[i]);
		r.x += q[i].x * w[i];
		r.y += q[i].y * w[i];
	}
	printf("p  = %10g %10g\n",p.x,p.y);
	printf("r  = %10g %10g\n",r.x,r.y);

	return 1;
}
#endif
