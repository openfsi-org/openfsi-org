#-------------------------------------------------
#
# Project created by QtCreator 2012-01-05T22:53:59
#
#-------------------------------------------------

QT       += gui core
TARGET = preprocessing
TEMPLATE = lib
CONFIG += staticlib

include (../openfsicompilerconfig.pri)

preprocessing.depends +=ofsifortran
preprocessing.depends +=triangle
ODETESTING:{
preprocessing.depends +=nrrk
}



DEFINES +=  RXLIB
#######DEFINES +=  RLX_64
##########DEFINES +=  RX_ICC

win32: {
    QMAKE_LFLAGS += /nodefaultlib:libcmt.lib  /nodefaultlib:MSVCRTD.lib
}

unix: INCLUDEPATH += /usr/include/mysql

INCLUDEPATH += ../src/preprocess
INCLUDEPATH += ../thirdparty/opennurbs
INCLUDEPATH += ../thirdparty/triangle
INCLUDEPATH += ../thirdparty/MTParserLib
INCLUDEPATH += ../thirdparty/hbio
INCLUDEPATH += ../src/x/inc
INCLUDEPATH += ../src/amgclass/inc
INCLUDEPATH += ../src/infrastructure
INCLUDEPATH += ../rxkernel
INCLUDEPATH += ../rxserver
INCLUDEPATH += ../rxcommon
INCLUDEPATH += ../src/ODE

INCLUDEPATH += ../src/stringtools
INCLUDEPATH += ../src/post/inc
unix:  INCLUDEPATH +=$(TBBROOT)/include

################################
########for H5FDDSM#############
H5FDDSM: {
    INCLUDEPATH +=/home/r3/software/hdf5-vfd-1.8.10/src
    INCLUDEPATH +=/home/r3/software/hdf5-vfd-1.8.10/c++
    INCLUDEPATH +=/home/r3/software/hdf5-vfd-1.8.10/hl
    INCLUDEPATH +=/usr/include/mpich2
    #########the following includes are wrong.
    INCLUDEPATH += $$PWD/../../../../software/paraView-icarus-3.14.1/org-build/bin
    DEPENDPATH += $$PWD/../../../../software/paraView-icarus-3.14.1/org-build/bin
    INCLUDEPATH += $$PWD/../../../../software/h5fddsm-0.9.9/build/bin
    DEPENDPATH += $$PWD/../../../../software/h5fddsm-0.9.9/build/bin
    INCLUDEPATH += $$PWD/../../../../software/hdf5-vfd-1.8.10/build/bin
    DEPENDPATH += $$PWD/../../../../software/hdf5-vfd-1.8.10/build/bin
    INCLUDEPATH += $$PWD/../../../../software/paraView-icarus-3.14.1/Utilities/Xdmf2/libsrc

################################
}
ODETESTING:{
    DEFINES += ODETESTING
}
INCLUDEPATH += $$PWD/../thirdparty/sailpp
DEPENDPATH += $$PWD/../thirdparty/sailpp

FOBJECTS = $$PWD/../ofsifortran/*.o

OBJECTS +=  $$FOBJECTS

QMAKE_CLEAN += ../ofsifortran/*.mod

OTHER_FILES += $$F90_SOURCES \
    ../readme.txt \
    ../f90.prf

SOURCES += \
    ../src/preprocess/words.cpp \
    ../src/preprocess/uvcurve.cpp \
    ../src/preprocess/transform.cpp \
    ../src/preprocess/text.cpp \
    ../src/preprocess/TensylProp.cpp \
    ../src/preprocess/TensylNode.cpp \
    ../src/preprocess/TensylLink.cpp \
    ../src/preprocess/targets.cpp \
    ../src/preprocess/table.cpp \
    ../src/preprocess/summary.cpp \
    ../src/preprocess/stringutils.cpp \
    ../src/preprocess/slowcut.cpp \
    ../src/preprocess/seamcurve.cpp \
    ../src/preprocess/scedit.cpp \
    ../src/preprocess/rxvectorfield.cpp \
    ../src/preprocess/RXUpoint.cpp \
    ../src/preprocess/RXTRObject.cpp \
    ../src/preprocess/RXTriplet.cpp \
    ../src/preprocess/RXTriangle.cpp \
    ../src/preprocess/RXSummaryTolerance.cpp \
    ../src/preprocess/RXSRelSite.cpp \
    ../src/preprocess/rxspline.cpp \
    ../src/preprocess/RXSparseMatrix1.cpp \
    ../src/preprocess/RXSparseMatrix.cpp \
    ../src/preprocess/RXSitePt.cpp \
    ../src/preprocess/RXSiteBase.cpp \
    ../src/preprocess/RX_SimpleTri.cpp \
    ../src/preprocess/RXShapeInterpolation.cpp \
    ../src/preprocess/RX_SeamcurveHelper.cpp \
    ../src/preprocess/RXSeamcurve.cpp \
    ../src/preprocess/RXScalarProperty.cpp \
    ../src/preprocess/RXSail.cpp \
    ../src/preprocess/RXQuantity.cpp \
    ../src/preprocess/RXPside.cpp \
    ../src/preprocess/RXPressureInterpolation.cpp \
    ../src/preprocess/RXPolyline.cpp \
    ../src/preprocess/RXPointOnCurve.cpp \
    ../src/preprocess/RX_PCFile.cpp \
    ../src/preprocess/RXPanel.cpp \
    ../src/preprocess/RXOptimisation.cpp \
    ../src/preprocess/RXON_Surface.cpp \
    ../src/preprocess/RXON_Matrix.cpp \
    ../src/preprocess/RXON_Extensions.cpp \
    ../src/preprocess/RX_OncIntersect.cpp \
    ../src/preprocess/RXON_3dPoint.cpp \
    ../src/preprocess/RXOffset.cpp \
    ../src/preprocess/RXObject.cpp \
    ../src/preprocess/RXMTEnquire.cpp \
    ../src/preprocess/RXMesh.cpp \
    ../src/preprocess/RXLogFile.cpp \
    ../src/preprocess/RXLayer.cpp \
    ../src/preprocess/RXInterpolationI.cpp \
    ../src/preprocess/RXIntegration.cpp \
    ../src/preprocess/RX_integrate.cpp \
    $$PWD/../src/preprocess/rximportentity.cpp \
    ../src/preprocess/RXHarwellBoeingIO1.cpp \
    ../src/preprocess/RXHarwellBoeingIO.cpp \
    ../src/preprocess/RXGridDensity.cpp \
    ../src/preprocess/rxfixity.cpp \
    ../src/preprocess/RX_FETri3.cpp \
    ../src/preprocess/RX_FEString.cpp \
    ../src/preprocess/RX_FESite.cpp \
    ../src/preprocess/RX_FEPocket.cpp \
    ../src/preprocess/RX_FEObject.cpp \
    ../src/preprocess/RX_FELinearObject.cpp \
    ../src/preprocess/RX_FEedge.cpp \
    ../src/preprocess/RX_FEBeam.cpp \
    ../src/preprocess/RXExpression.cpp \
    ../src/preprocess/RXExceptions.cpp \
    ../src/preprocess/RXEntityDefault.cpp \
    ../src/preprocess/RXEntity.cpp \
    ../src/preprocess/RXDrawingLayer.cpp \
    ../src/preprocess/RXCurveIntersection.cpp \
    ../src/preprocess/RXCurve.cpp \
    ../src/preprocess/RXCrossing2.cpp \
    ../src/preprocess/RXContactSurface.cpp \
    ../src/preprocess/RXCompoundCurve.cpp \
    ../src/preprocess/rxbcurve.cpp \
    ../src/preprocess/RXAttributes.cpp \
    ../src/preprocess/rlxNearestPoint.cpp \
    ../src/preprocess/RLX3dmbuffer.cpp \
    ../src/preprocess/RLX3dm.cpp \
    ../src/preprocess/resolve.cpp \
    ../src/preprocess/reledit.cpp \
    ../src/preprocess/RelaxWorld.cpp \
    ../src/preprocess/redraw.cpp \
    ../src/preprocess/ReadStripe.cpp \
    ../src/preprocess/readstring.cpp \
    ../src/preprocess/readname.cpp \
    ../src/preprocess/ReadFix.cpp \
    ../src/preprocess/ReadBoat.cpp \
    ../src/preprocess/ReadBagged.cpp \
    ../src/preprocess/printall.cpp \
    ../src/preprocess/pressure.cpp \
    ../src/preprocess/preproc.cpp \
    ../src/preprocess/peternew.cpp \
    ../src/preprocess/peterfab.cpp \
    ../src/preprocess/pcspline.cpp \
    ../src/preprocess/pc_solvecubic.cpp \
    ../src/preprocess/pansin.cpp \
    ../src/preprocess/paneldata.cpp \
    ../src/preprocess/panel.cpp \
    ../src/preprocess/oncut.cpp \
    ../src/preprocess/nurbs.cpp \
    ../src/preprocess/nlm.cpp \
    ../src/preprocess/NelderMead.cpp \
    ../src/preprocess/matinvd.cpp \
    ../src/preprocess/material.cpp \
    ../src/preprocess/layerobject.cpp \
    ../src/preprocess/interpln.cpp \
    ../src/preprocess/iges.cpp \
    ../src/preprocess/iangles.cpp \
    ../src/preprocess/global_declarations.cpp \
    ../src/preprocess/gle.cpp \
    ../src/preprocess/getglobal.cpp \
    ../src/preprocess/geodesic.cpp \
    ../src/preprocess/gen.cpp \
    ../src/preprocess/gcurve.cpp \
    ../src/preprocess/gauss.cpp \
    ../src/preprocess/freeze.cpp \
    ../src/preprocess/finish.cpp \
    ../src/preprocess/findtransform.cpp \
    ../src/preprocess/filament.cpp \
    ../src/preprocess/fields.cpp \
    ../src/preprocess/etypes.cpp \
    ../src/preprocess/entutils.cpp \
    ../src/preprocess/entities.cpp \
    ../src/preprocess/editword.cpp \
    ../src/preprocess/dxfout.cpp \
    ../src/preprocess/dxfin.cpp \
    ../src/preprocess/dxf.cpp \
    ../src/preprocess/drawing.cpp \
    ../src/preprocess/delete.cpp \
    ../src/preprocess/debug.cpp \
    ../src/preprocess/cut.cpp \
    ../src/preprocess/curveplaneintersection2.cpp \
    ../src/preprocess/curveintersection.cpp \
    ../src/preprocess/compute_stripes.cpp \
    ../src/preprocess/compute.cpp \
    ../src/preprocess/cfromf.cpp \
    ../src/preprocess/cam2.cpp \
    ../src/preprocess/BrentMethod.cpp \
    ../src/preprocess/breakc.cpp \
    ../src/preprocess/boxsearch.cpp \
    ../src/preprocess/boundingbox.cpp \
    ../src/preprocess/boundary.cpp \
    ../src/preprocess/boatgen.cpp \
    ../src/preprocess/batstiff.cpp \
    ../src/preprocess/batpatch.cpp \
    ../src/preprocess/arclngth.cpp \
    ../src/preprocess/appwind.cpp \
    ../src/preprocess/akmutil.cpp \
    ../src/preprocess/aero.cpp \
    vectors.cpp \
    polyline.cpp \
    barycentric.cpp \
    aeromik.cpp \
    anchors.cpp \
    resid.cpp \
    r2af.cpp \
    ../src/preprocess/read.cpp \
    ../src/preprocess/rxpshell.cpp \
    ../src/preprocess/rxrigidbody.cpp \
    ../src/preprocess/rxnodecurve.cpp \
    ../src/preprocess/rxsubmodel.cpp

H5FDDSM: {
    message("adding to sources  ../rxkernel/rxh5fddsmlink.cpp")
    SOURCES +=    ../rxkernel/rxh5fddsmlink.cpp
}

HEADERS += \
    ../src/preprocess/zoneedge.h \
    ../src/preprocess/words.h \
    ../src/preprocess/viewmenu.h \
    ../src/preprocess/velocity.h \
    ../src/preprocess/vectors.h \
    ../src/preprocess/uvcurve.h \
    ../src/preprocess/utility.h \
    ../src/preprocess/uselect.h \
    ../src/preprocess/UpdateActive.h \
    ../src/preprocess/unixver.h \
    ../src/preprocess/units.h \
    ../src/preprocess/trivals.h \
    ../src/preprocess/tris.h \
    ../src/preprocess/transf90.h \
    ../src/preprocess/traceback.h \
    ../src/preprocess/tpn.h \
    ../src/preprocess/tohoops.h \
    ../src/preprocess/text.h \
    ../src/preprocess/TensylProp.h \
    ../src/preprocess/TensylNode.h \
    ../src/preprocess/TensylLink.h \
    ../src/preprocess/targets.h \
    ../src/preprocess/table.h \
    ../src/preprocess/surface.h \
    ../src/preprocess/summary.h \
    ../src/preprocess/stripes.h \
    ../src/preprocess/stringutils.h \
    ../src/preprocess/spline.h \
    ../src/preprocess/slowcut.h \
    ../src/preprocess/shapegen.h \
    ../src/preprocess/settrim.h \
    ../src/preprocess/script.h \
    ../src/preprocess/scedit.h \
    ../src/preprocess/scanfile.h \
    ../src/preprocess/savesailas.h \
    ../src/preprocess/SaveSail.h \
    ../src/preprocess/sailutil.h \
    ../src/preprocess/rxvectorfield.h \
    ../src/preprocess/RXUPoint.h \
    ../src/preprocess/RX_UI_types.h \
    ../src/preprocess/RXTRObject.h \
    ../src/preprocess/RXTriplet.h \
    ../src/preprocess/RXTriangle.h \
    ../src/preprocess/RXTesting.h \
    ../src/preprocess/RXSummaryTolerance.h \
    ../src/preprocess/rxsubwindowhelper.h \
    ../src/preprocess/RXStrFuncI.h \
    ../src/preprocess/RXSRelSite.h \
    ../src/preprocess/rxspline.h \
    ../src/preprocess/RXSparseMatrix1.h \
    ../src/preprocess/RXSparseMatrix.h \
    ../src/preprocess/RXSitePt.h \
    ../src/preprocess/RXSiteBase.h \
    ../src/preprocess/RX_SimpleTri.h \
    ../src/preprocess/RXShapeInterpolation.h \
    ../src/preprocess/rxselect.h \
    ../src/preprocess/RX_SeamcurveHelper.h \
    ../src/preprocess/RXSeamcurve.h \
    ../src/preprocess/RXScalarProperty.h \
    ../src/preprocess/RXSail.h \
    ../src/preprocess/RXRelationDefs.h \
    ../src/preprocess/RXQuantity.h \
    ../src/preprocess/RXPside.h \
    ../src/preprocess/RXPressureInterpolation.h \
    ../src/preprocess/RXPolyline.h \
    ../src/preprocess/RXPointOnCurve.h \
    ../src/preprocess/RX_PCFile.h \
    ../src/preprocess/RXPattern.h \
    ../src/preprocess/RXParser.h \
    ../src/preprocess/RXPanel.h \
    ../src/preprocess/RXOptVariable.h \
    ../src/preprocess/RXOptimisation.h \
    ../src/preprocess/RXON_Surface.h \
    ../src/preprocess/RXON_Matrix.h \
    ../src/preprocess/rxON_Extensions.h \
    ../src/preprocess/RX_OncIntersect.h \
    ../src/preprocess/RXON_3dPoint.h \
    ../src/preprocess/RXOffset.h \
    ../src/preprocess/RXObject.h \
    ../src/preprocess/RXMTEnquire.h \
    ../src/preprocess/RXMesh.h \
    ../src/preprocess/RXMathlink.h \
    ../src/preprocess/RXmaterialMatrix.h \
    ../src/preprocess/RXLogFile.h \
    ../src/preprocess/RXLayer.h \
    ../src/preprocess/RXInterpolationI.h \
    ../src/preprocess/RXIntegration.h \
    ../src/preprocess/RX_integrate.h \
    ../src/preprocess/rximportentity.h \
    ../src/preprocess/RXHarwellBoeingIO1.h \
    ../src/preprocess/RXHarwellBoeingIO.h \
    ../src/preprocess/RXGridDensity.h \
    ../src/preprocess/RXGraphicCalls.h \
    ../src/preprocess/rxfixity.h \
    ../src/preprocess/RX_FETri3.h \
    ../src/preprocess/RX_FEString.h \
    ../src/preprocess/RX_FESite.h \
    ../src/preprocess/RX_FEPocket.h \
    ../src/preprocess/RX_FEObject.h \
    ../src/preprocess/RX_FELinearObject.h \
    ../src/preprocess/RX_FEEdge.h \
    ../src/preprocess/RX_FEBeam.h \
    ../src/preprocess/RXExpression.h \
    ../src/preprocess/RXException.h \
    ../src/preprocess/RXEntityDefault.h \
    ../src/preprocess/RXEntity.h \
    ../src/preprocess/RXENode.h \
    ../src/preprocess/RXDrawingLayer.h \
    ../src/preprocess/RXdovefile.h \
    ../src/preprocess/RXDatabaseI.h \
    ../src/preprocess/RXCurveIntersection.h \
    ../src/preprocess/RXCurve.h \
    ../src/preprocess/RXCrossing2.h \
    ../src/preprocess/RXContactSurface.h \
    ../src/preprocess/RXCompoundCurve.h \
    ../src/preprocess/rxbcurve.h \
    ../src/preprocess/RXAttributes.h \
    ../src/preprocess/RX3fPoint.h \
    ../src/preprocess/RX3dPoint.h \
    ../src/preprocess/rsummary.h \
    ../src/preprocess/rlxNearestPoint.h \
    ../src/preprocess/rlxMasterReaction.h \
    ../src/preprocess/RLXDblMatrix.h \
    ../src/preprocess/RLX3dm.h \
    ../src/preprocess/rlx3dmEntities.h \
    ../src/preprocess/rlx3dmbuffer.h \
    ../src/preprocess/rh_geodesic.h \
    ../src/preprocess/resolve.h \
    ../src/preprocess/resid.h \
    ../src/preprocess/reset.h \
    ../src/preprocess/reportform.h \
    ../src/preprocess/reledit.h \
    ../src/preprocess/RelaxWorld.h \
    ../src/preprocess/RelaxConnect.h \
    ../src/preprocess/relax32.h \
    ../src/preprocess/redraw.h \
    ../src/preprocess/ReadStripe.h \
    ../src/preprocess/readstring.h \
    ../src/preprocess/readrlx.h \
    ../src/preprocess/ReadName.h \
    ../src/preprocess/ReadGenDp.h \
    ../src/preprocess/readedg.h \
    ../src/preprocess/ReadBoat.h \
    ../src/preprocess/ReadBagged.h \
    ../src/preprocess/read.h \
    ../src/preprocess/rdpfile.h \
    ../src/preprocess/question.h \
    ../src/preprocess/quantity_def.h \
    ../src/preprocess/printall.h \
    ../src/preprocess/pressure.h \
    ../src/preprocess/preproc.h \
    ../src/preprocess/polyline.h \
    ../src/preprocess/peternew.h \
    ../src/preprocess/peterfab.h \
    ../src/preprocess/pctable.h \
    ../src/preprocess/pcspline.h \
    ../src/preprocess/pc_solvecubic.h \
    ../src/preprocess/pchoops.h \
    ../src/preprocess/pansin.h \
    ../src/preprocess/pansail_struct.h \
    ../src/preprocess/pansail.h \
    ../src/preprocess/panout.h \
    ../src/preprocess/panllist.h \
    ../src/preprocess/paneldata.h \
    ../src/preprocess/panel.h \
    ../src/preprocess/oncut.h \
    ../src/preprocess/offset.h \
    ../src/preprocess/nurbs.h \
    ../src/preprocess/nonlin.h \
    ../src/preprocess/nlm.h \
    ../src/preprocess/newmenu.h \
    ../src/preprocess/NelderMead.h \
    ../src/preprocess/mgraphs.h \
    ../src/preprocess/meshlib.h \
    ../src/preprocess/material.h \
    ../src/preprocess/mast.h \
    ../src/preprocess/linklist.h \
    ../src/preprocess/layervertex.h \
    ../src/preprocess/layertst.h \
    ../src/preprocess/layerobject.h \
    ../src/preprocess/layernode.h \
    ../src/preprocess/layerlink.h \
    ../src/preprocess/layerlayer.h \
    ../src/preprocess/layercontour.h \
    ../src/preprocess/layerangle.h \
    ../src/preprocess/jpeg.h \
    ../src/preprocess/intrpxyz.h \
    ../src/preprocess/interpln.h \
    ../src/preprocess/interact.h \
    ../src/preprocess/image.h \
    ../src/preprocess/iges.h \
    ../src/preprocess/iangles.h \
    ../src/preprocess/griddefs.h \
    ../src/preprocess/GraphicStruct.h \
    ../src/preprocess/global_declarations.h \
    ../src/preprocess/Global.h \
    ../src/preprocess/gle.h \
    ../src/preprocess/Gflags.h \
    ../src/preprocess/getshell.h \
    ../src/preprocess/getglobal.h \
    ../src/preprocess/getfilename.h \
    ../src/preprocess/geodesic.h \
    ../src/preprocess/gcurve.h \
    ../src/preprocess/gauswidt.h \
    ../src/preprocess/gauss.h \
    ../src/preprocess/freeze.h \
    ../src/preprocess/freetype.h \
    ../src/preprocess/freestac.h \
    ../src/preprocess/finish.h \
    ../src/preprocess/findtransform.h \
    ../src/preprocess/findmodel.h \
    ../src/preprocess/files.h \
    ../src/preprocess/filament.h \
    ../src/preprocess/fields.h \
    ../src/preprocess/fielddef.h \
    ../src/preprocess/f90_to_c.h \
    ../src/preprocess/etypes.h \
    ../src/preprocess/entutils.h \
    ../src/preprocess/entities.h \
    ../src/preprocess/elements.h \
    ../src/preprocess/editword.h \
    ../src/preprocess/edit.h \
    ../src/preprocess/dxfout.h \
    ../src/preprocess/dxfin.h \
    ../src/preprocess/dxf.h \
    ../src/preprocess/drop.h \
    ../src/preprocess/drawing.h \
    ../src/preprocess/drawforces.h \
    ../src/preprocess/draweigenvectors.h \
    ../src/preprocess/dovtemptest.h \
    ../src/preprocess/doview.h \
    ../src/preprocess/doveTriradialLambdaNe.h \
    ../src/preprocess/doveTriradial.h \
    ../src/preprocess/dovestructurebylayersurfs.h \
    ../src/preprocess/dovestructure.h \
    ../src/preprocess/doveGeometric.h \
    ../src/preprocess/doveforstack.h \
    ../src/preprocess/dovechoose.h \
    ../src/preprocess/dovecheckerboard.h \
    ../src/preprocess/do_boat.h \
    ../src/preprocess/delete.h \
    ../src/preprocess/debug.h \
    ../src/preprocess/cut.h \
    ../src/preprocess/custom.h \
    ../src/preprocess/curves.h \
    ../src/preprocess/curveplaneintersection2.h \
    ../src/preprocess/curveintersection.h \
    ../src/preprocess/connect_f.h \
    ../src/preprocess/compute.h \
    ../src/preprocess/CheckConnects.h \
    ../src/preprocess/CheckAll.h \
    ../src/preprocess/cfromf.h \
    ../src/preprocess/camera.h \
    ../src/preprocess/cam2.h \
    ../src/preprocess/BrentMethod.h \
    ../src/preprocess/boxsearch.h \
    ../src/preprocess/boundingbox.h \
    ../src/preprocess/boundary.h \
    ../src/preprocess/boatgen.h \
    ../src/preprocess/blkdsail.h \
    ../src/preprocess/bend.h \
    ../src/preprocess/batstiff.h \
    ../src/preprocess/batpatch.h \
    ../src/preprocess/batdefs.h \
    ../src/preprocess/barycentric.h \
    ../src/preprocess/arclngth.h \
    ../src/preprocess/appwind.h \
    ../src/preprocess/angcheck.h \
    ../src/preprocess/anchors.h \
    ../src/preprocess/alias.h \
    ../src/preprocess/akmutil.h \
    ../src/preprocess/akmpretty.h \
    ../src/preprocess/aeromik.h \
    ../src/preprocess/aero.h \
    ../src/stringtools/stringtools.h \
    ../src/stringtools/UtfConverter.h \
    ../src/stringtools/ConvertUTF.h \
    ../src/post/inc/finishview.h \
    ../src/preprocess/rxpshell.h \
    ../src/preprocess/rxrigidbody.h \
    ../src/preprocess/rxnodecurve.h \
    ../rxkernel/rxh5fddsmlink.h \
    ../src/preprocess/rxsubmodel.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
#    INSTALLS += target
}




###############################################################

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../thirdparty/MTParserLib/release/ -lMTParserLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../thirdparty/MTParserLib/debug/ -lMTParserLib
else:symbian: LIBS += -lMTParserLib
else:unix: LIBS += -L$$PWD/../thirdparty/MTParserLib/ -lMTParserLib

INCLUDEPATH += $$PWD/../thirdparty/MTParserLib
DEPENDPATH += $$PWD/../thirdparty/MTParserLib

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../thirdparty/MTParserLib/release/MTParserLib.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../thirdparty/MTParserLib/debug/MTParserLib.lib
else:unix:!symbian: PRE_TARGETDEPS += $$PWD/../thirdparty/MTParserLib/libMTParserLib.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../thirdparty/MTParserLib/release/ -lMTParserLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../thirdparty/MTParserLib/debug/ -lMTParserLib
else:symbian: LIBS += -lMTParserLib
else:unix: LIBS += -L$$PWD/../thirdparty/MTParserLib/ -lMTParserLib

#INCLUDEPATH += $$PWD/../thirdparty/MTParserLib
#DEPENDPATH += $$PWD/../thirdparty/MTParserLib

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../thirdparty/MTParserLib/release/MTParserLib.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../thirdparty/MTParserLib/debug/MTParserLib.lib
else:unix:!symbian: PRE_TARGETDEPS += $$PWD/../thirdparty/MTParserLib/libMTParserLib.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../thirdparty/MTParserLib/release/ -lMTParserLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../thirdparty/MTParserLib/debug/ -lMTParserLib
else:symbian: LIBS += -lMTParserLib
else:unix: LIBS += -L$$PWD/../thirdparty/MTParserLib/ -lMTParserLib

#INCLUDEPATH += $$PWD/../thirdparty/MTParserLib
#DEPENDPATH += $$PWD/../thirdparty/MTParserLib

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += ../thirdparty/MTParserLib/release/MTParserLib.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += ../thirdparty/MTParserLib/debug/MTParserLib.lib
else:unix:!symbian: PRE_TARGETDEPS += ../thirdparty/MTParserLib/libMTParserLib.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../thirdparty/hbio/release/ -lhbio
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../thirdparty/hbio/debug/ -lhbio
else:symbian: LIBS += -lhbio
else:unix: LIBS += -L$$PWD/../thirdparty/hbio/ -lhbio

INCLUDEPATH += $$PWD/../thirdparty/hbio
DEPENDPATH += $$PWD/../thirdparty/hbio

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += ../thirdparty/hbio/release/hbio.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += ../thirdparty/hbio/debug/hbio.lib
else:unix:!symbian: PRE_TARGETDEPS += ../thirdparty/hbio/libhbio.a

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../ofsifortran/release/ofsifortran.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../ofsifortran/debug/ofsifortran.lib
else:unix:!symbian: PRE_TARGETDEPS += ../ofsifortran/libofsifortran.a

ODETESTING:{
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../thirdparty/nrrk/release/ -lnrrk
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../thirdparty/nrrk/debug/ -lnrrk
else:symbian: LIBS += -lnrrk
else:unix: LIBS += -L$$PWD/../nrrk/nrrk/ -lnrrk

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../thirdparty/nrrk/release/nrrk.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../thirdparty/nrrk/debug/nrrk.lib
else:unix:!symbian: PRE_TARGETDEPS += ../thirdparty/nrrk/libnrrk.a
}
################ PGO details - switched on/off in our .pri file
profgen: {

#  for PGO either -prof-gen or -prof-use
CONFIG(release,debug|release):          QMAKE_CFLAGS+=  -prof-gen  -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          QMAKE_CXXFLAGS+=  -prof-gen  -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          F90_CFLAGS += -prof-gen  -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          QMAKE_LFLAGS +=  -prof-gen  -prof-dir/home/r3/Documents/qt/openFSI

}
profuse:{
#  -prof-file ../myprof
CONFIG(release,debug|release):          QMAKE_CFLAGS+=  -prof-use -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          QMAKE_CXXFLAGS+=  -prof-use -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          F90_CFLAGS += -prof-use  -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          QMAKE_LFLAGS += -prof-use  -prof-dir/home/r3/Documents/qt/openFSI
}



