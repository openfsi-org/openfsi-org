#pragma once

// touched by PH:  
#include <string>
#include <stdio.h>

#define HB_FLOAT  double
#define HB_THROW(a) _hbthrow (a);

class HBIO_X
{
public:
	HBIO_X(void);
	HBIO_X(int a);
	HBIO_X(char *a);
	HBIO_X(string a);
	~HBIO_X(void);
	int Print(FILE *fp=stdout);

protected:
	int m_i;
	string m_s;
	int m_type; 
};
void _hbthrow (int a);
void _hbthrow (char *a);
void _hbthrow (string a);




bool ch_eqi ( char c1, char c2 );
bool ch_is_digit ( char c );
bool ch_is_format_code ( char c );
int ch_to_digit ( char c );
void hb_exact_read ( ifstream &input, int nrow, int nrhs, int rhscrd, 
  char *rhsfmt, char *rhstyp, HB_FLOAT exact[] );
void hb_exact_write ( ofstream &output, int nrow, int nrhs, int rhscrd, 
  char *rhsfmt, char *rhstyp, HB_FLOAT exact[] );
void hb_file_read ( ifstream &input, std::string &title,  std::string &key, int *totcrd,
                   int *ptrcrd, int *indcrd, int *valcrd, int *rhscrd,  std::string &mxtype, int *nrow,
				   int *ncol, int *nnzero, int *neltvl, char **ptrfmt, char **indfmt, char **valfmt, 
				   char **rhsfmt, char **rhstyp, int *nrhs, int *nrhsix, int **colptr, 
				   int **rowind, HB_FLOAT **values, HB_FLOAT **rhsval, int **rhsptr, int **rhsind,  
				   HB_FLOAT **rhsvec, HB_FLOAT **guess, HB_FLOAT **exact );
void hb_file_write (ofstream &output, const char *title, const char *key, int totcrd,
                    int ptrcrd, int indcrd, int valcrd, int rhscrd, const char *mxtype, int nrow,
                    int ncol, int nnzero, int neltvl, char *ptrfmt, char *indfmt, char *valfmt,
                    char *rhsfmt, char *rhstyp, int nrhs, int nrhsix, const int colptr[],
                    const int rowind[], const double values[], HB_FLOAT rhsval[], int rhsptr[], int rhsind[],
                    HB_FLOAT rhsvec[], HB_FLOAT guess[], HB_FLOAT exact[] );
void hb_guess_read ( ifstream &input, int nrow, int nrhs, int rhscrd, 
  char *rhsfmt, char *rhstyp, HB_FLOAT guess[] );
void hb_guess_write ( ofstream &output, int nrow, int nrhs, int rhscrd, 
  char *rhsfmt, char *rhstyp, HB_FLOAT guess[] );
void hb_header_print ( char *title, char *key, int totcrd, int ptrcrd, 
  int indcrd, int valcrd, int rhscrd, char *mxtype, int nrow, int ncol, 
  int nnzero, int neltvl, char *ptrfmt, char *indfmt, char *valfmt, 
  char *rhsfmt, char *rhstyp, int nrhs, int nrhsix );
void hb_header_read (ifstream &input, std::string &title, std::string &key, int *totcrd,
  int *ptrcrd, int *indcrd, int *valcrd, int *rhscrd, std::string &mxtype, int *nrow,
  int *ncol, int *nnzero, int *neltvl, char **ptrfmt, char **indfmt,
  char **valfmt, char **rhsfmt, char **rhstyp, int *nrhs, int *nrhsix );
void hb_header_write (ofstream &output, const char *title, const char *key, int totcrd,
  int ptrcrd, int indcrd, int valcrd, int rhscrd, const char *mxtype, int nrow,
  int ncol, int nnzero, int neltvl, char *ptrfmt, char *indfmt, char *valfmt,
  char *rhsfmt, char *rhstyp, int nrhs, int nrhsix );
HB_FLOAT *hb_matvec_a_mem ( int nrow, int ncol, int nnzero, int nrhs, 
  int colptr[], int rowind[], HB_FLOAT values[], HB_FLOAT exact[] );
void hb_rhs_read ( ifstream &input, int nrow, int nnzero, int nrhs, int nrhsix, 
  int rhscrd, char *ptrfmt, char *indfmt, char *rhsfmt,const char *mxtype, 
  char *rhstyp, HB_FLOAT rhsval[], int rhsind[], int rhsptr[], HB_FLOAT rhsvec[] );
void hb_rhs_write ( ofstream &output, int nrow, int nnzero, int nrhs, int nrhsix, 
  int rhscrd, char *ptrfmt, char *indfmt, char *rhsfmt, const char *mxtype,
  char *rhstyp, HB_FLOAT rhsval[], int rhsind[], int rhsptr[], HB_FLOAT rhsvec[] );
void hb_structure_print ( int ncol, char *mxtype, int nnzero, int neltvl, 
  int colptr[], int rowind[] );
void hb_structure_read (ifstream &input, int ncol, std::string &mxtype, int nnzero,
  int neltvl, int ptrcrd, char *ptrfmt, int indcrd, char *indfmt,
  int colptr[], int rowind[] );
void hb_structure_write (ofstream &output, int ncol, const char *mxtype,
  int nnzero, int neltvl, char *ptrfmt, char *indfmt, const int colptr[],
  const int rowind[] );
int *hb_ua_colind ( int ncol, int colptr[], int nnzero );
void hb_values_print ( int ncol, int colptr[], char *mxtype, int nnzero, 
  int neltvl, HB_FLOAT values[] );
void hb_values_read (ifstream &input, int valcrd, std::string &mxtype, int nnzero,
  int neltvl, char *valfmt, HB_FLOAT values[] );
void hb_values_write (ofstream &output, const int valcrd, const char *mxtype,
  const int nnzero, const int neltvl, const char *valfmt, const double values[] );
HB_FLOAT *hb_vecmat_a_mem ( int nrow, int ncol, int nnzero, int nrhs, 
  int colptr[], int rowind[], HB_FLOAT values[], HB_FLOAT exact[] );
int i4_max ( int i1, int i2 );
int i4_min ( int i1, int i2 );
void i4vec_print ( int n, int a[], char *title );
void i4vec_print_some ( int n, int a[], int max_print, char *title );
void r4mat_print ( int m, int n, HB_FLOAT a[], char *title );
void r4mat_print_some ( int m, int n, HB_FLOAT a[], int ilo, int jlo, int ihi, 
  int jhi, char *title );
void r4vec_print ( int n, HB_FLOAT a[], char *title );
void r4vec_print_some ( int n, HB_FLOAT a[], int max_print, char *title );
int s_len_trim (const char *s );
char *s_substring ( char *s, int a, int b );
void s_to_format (const char *s, int *r, char *code, int *w, int *m );
void s_trim ( char *s );
void timestamp ( void );
