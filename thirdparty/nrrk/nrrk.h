#ifndef NRRK_H
#define NRRK_H

#include "/home/r3/software/nr/cpp/other/nr.h"
#include "RXSparseMatrix1.h"
class  Nrrk
{
public:
    Nrrk();
    int testrkdumb();
    int testodeint();
    int rksolve(void);
    int stiffsolve(void);
    int odeint(Vec_IO_DP &ystart, const DP eps,
               const DP h1, const DP hmin, int &nok, int &nbad );
    int teststifbs();
    int m_kit;

private:  // for stif
     void jacobn_s(const DP x, Vec_I_DP &y, Vec_O_DP &dfdx, Mat_O_DP &dfdy, RXSparseMatrix1 &BS);
     void derivs_s(const DP x, Vec_I_DP &y, Vec_O_DP &dydx);
     void stifbs(Vec_IO_DP &y, Vec_IO_DP &dydx, DP &xx, const DP htry,
                const DP eps, Vec_I_DP &yscal, DP &hdid, DP &hnext);//,
             //   void derivs(const DP, Vec_I_DP &, Vec_O_DP &));
     void simpr(Vec_I_DP &y, Vec_I_DP &dydx, Vec_I_DP &dfdx, Mat_I_DP &dfdy,class RXSparseMatrix1 &BS,
                      const DP xs, const DP htot, const int nstep, Vec_O_DP &yout);
                    //  void derivs(const DP, Vec_I_DP &, Vec_O_DP &));

    void pzextr(const int iest, const DP xest, Vec_I_DP &yest, Vec_O_DP &yz,
        Vec_O_DP &dy);

private:
    void  rkqs(Vec_IO_DP &y, Vec_IO_DP &dydx, DP &x, const DP htry,
               const DP eps, Vec_I_DP &yscal, DP &hdid, DP &hnext );/*,
            void derivs(const DP, Vec_I_DP &, Vec_O_DP &));*/

    void derivs(const DP x, Vec_I_DP &y, Vec_O_DP &dydx);
    void rkck(Vec_I_DP &y, Vec_I_DP &dydx, const DP x,
              const DP h, Vec_O_DP &yout, Vec_O_DP &yerr) ;

    int odeint_stif(Vec_IO_DP &ystart, const DP eps,
                     const DP h1, const DP hmin, int &nok, int &nbad);
                   //  void derivs_s(const DP, Vec_I_DP &, Vec_O_DP &));//,
                    // void rkqs(Vec_IO_DP &, Vec_IO_DP &, DP &, const DP, const DP,
                     //          Vec_I_DP &, DP &, DP &, void (*)(const DP, Vec_I_DP &, Vec_O_DP &)));
class RXSparseMatrix1  m_IMinusBh;
    int nrhs;
    // NR  'globals'
    DP dxsav;
    int kmax,kount;
    Vec_DP *xp_p;
    Mat_DP *yp_p;

     Vec_DP *x_p;
     Mat_DP *d_p;
         double m_damp;


    int LogEnergy (const double x, const double ekt);
    double EnergyPeak();
    std::vector< pair<double,double > > m_history;

};

#endif // NRRK_H
