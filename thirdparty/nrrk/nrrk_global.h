#ifndef NRRK_GLOBAL_H
#define NRRK_GLOBAL_H

#include <QtCore/qglobal.h>
goat
#if defined(NRRK_LIBRARY)
#  define NRRKSHARED_EXPORT Q_DECL_EXPORT
#else
#  define NRRKSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // NRRK_GLOBAL_H
