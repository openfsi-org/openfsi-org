#ifndef OPENNURBS_5_H
#define OPENNURBS_5_H

#include "opennurbs_5_global.h"

class OPENNURBS_5SHARED_EXPORT Opennurbs_5 {
public:
    Opennurbs_5();
};

#endif // OPENNURBS_5_H
