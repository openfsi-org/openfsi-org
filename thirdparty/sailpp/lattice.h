// lattice
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LATTICE_H__FECD7277_8EF1_11D4_B1C9_0060B068485C__INCLUDED_)
#define AFX_LATTICE_H__FECD7277_8EF1_11D4_B1C9_0060B068485C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "point.h"
#include "math.h"

#define pi 3.141592653589793238462643 

class lattice  
{
private:
	WernerPoint ip[4];	//intersecting points
	WernerPoint cp;		//control point	
	int type;		//finite: type=1, semi-infinite: type=2 
	int nabour[4];	//neighbors
	WernerPoint d1,d2;	//diagonals
	
	//Calculats the diagonals
	void diag()
	{	d1=ip[0].vec(ip[2]);
		d2=ip[1].vec(ip[3]);
	}	

public:
	lattice();
        lattice(WernerPoint, WernerPoint, WernerPoint, WernerPoint);
	lattice(WernerPoint, WernerPoint, WernerPoint, WernerPoint, WernerPoint);
	lattice(WernerPoint, WernerPoint, WernerPoint, WernerPoint, WernerPoint, int);
	virtual ~lattice();
	
	void setIp(WernerPoint a, WernerPoint b, WernerPoint c, WernerPoint d)
	{
		ip[0]=a;ip[1]=b;ip[2]=c;ip[3]=d;
		diag();
	}
	
	void setCp(WernerPoint a)
	{
		cp=a;  
	}

	void setNabour(int a, int b, int c, int d)
	{
		nabour[0]=a;nabour[1]=b;nabour[2]=c;nabour[3]=d;  
	}

	void getNabour(int&,int&,int&,int&);
	
 const   WernerPoint* getIp() const
	{	
                const WernerPoint* p=ip;
		return p;
	}	
	
        WernerPoint getCp() const
	{	
                WernerPoint p=cp;
		return p;
	}
  WernerPoint getNorm() const
	{	
                 WernerPoint y;
		y=(d1.cross(d2)).normalize();
		return y;
	}

        double getArea() const
	{	int i;
		double A1,A2;
		double X[4],Y[4],Z[4];
		
		for (i=0; i<4;i++)
			ip[i].getPos(X[i],Y[i],Z[i]);
		A1=0.5*(X[0]*Y[1]+Y[0]*X[2]+Y[2]*X[1]-Y[1]*X[2]-Y[0]*X[1]-X[0]*Y[2]);
		if (A1<0) A1=-A1;
		A2=0.5*(X[0]*Y[2]+Y[0]*X[3]+Y[3]*X[2]-Y[2]*X[3]-Y[0]*X[2]-X[0]*Y[3]);
		if (A2<0) A2=-A2;
		return (A1+A2);
	}
			
        WernerPoint vInduced(WernerPoint p) const // if I understand, this is the influence coeff not the velocity.
	{	// The velocity in point p induced by this lattice 
		int i,j,k;
		double a,b,c,x,y,z,x1,y1,z1,x2,y2,z2,temp;
		double v[3];
		double d[3];
		int singular=1;
		WernerPoint vp;
		
		for (k=0;k<4;k++) {if (p.equal(ip[k])) singular=0;} //check for singualrity
		
		if (singular==0) vp=WernerPoint(); //singular, return (0,0,0)
		else
		{
		p.getPos(x,y,z);
		
		for (k=0;k<3;k++) {	v[k]=0;}
				
		for (i=0; i<4;i++)
		{	
			ip[i].getPos(x1,y1,z1);
			if (i==3) k=-1; else k=i;
			ip[k+1].getPos(x2,y2,z2);
		
			a=pow((x-x1),2)+pow((y-y1),2)+pow((z-z1),2);
			b=-2*((x2-x1)*(x-x1)+(y2-y1)*(y-y1)+(z2-z1)*(z-z1));
			c=pow((x2-x1),2)+pow((y2-y1),2)+pow((z2-z1),2);
			d[0]=(z2-z1)*(y-y1)-(y2-y1)*(z-z1);
			d[1]=(x2-x1)*(z-z1)-(z2-z1)*(x-x1);
			d[2]=(y2-y1)*(x-x1)-(x2-x1)*(y-y1);
			
                        temp=4.*a*c-b*b;
                        if (fabs(temp)>1e-12) temp=-2./((temp))*((2.*c+b)/sqrt(a+b+c)-b/sqrt(a));
			else temp=0; //(singular)
			if (nabour[i]==-1 ) temp=0; //(wake shedding edge)
			for (j=0;j<3;j++)
				v[j]=temp*d[j]+v[j];
		
		}	
		
		vp=WernerPoint(v[0],v[1],v[2]);
		}
		return vp;
	}

	WernerPoint vInducedMirr(WernerPoint p)
	{	// The velocity in point p induced by the mirror of this lattice
		// The lattice is mirrored in the x-z-plane
		int i,j,k;
		double a,b,c,x,y,z,x1,y1,z1,x2,y2,z2,temp;
		double v[3];
		double d[3];
		int singular=1;
		WernerPoint vp;
		
		for (k=0;k<4;k++) {if (p.equal(ip[k])) singular=0;} //check for singualrity
		
		if (singular==0) vp=WernerPoint(); //singular, return (0,0,0)
		else
		{
		p.getPos(x,y,z);
		
		for (k=0;k<3;k++) {	v[k]=0;}
				
		for (i=0; i<4;i++)
		{	
			ip[i].getPos(x1,y1,z1);
			y1=-y1;
			if (i==3) k=-1; else k=i;
			ip[k+1].getPos(x2,y2,z2);
			y2=-y2;
			a=pow((x-x1),2)+pow((y-y1),2)+pow((z-z1),2);
			b=-2*((x2-x1)*(x-x1)+(y2-y1)*(y-y1)+(z2-z1)*(z-z1));
			c=pow((x2-x1),2)+pow((y2-y1),2)+pow((z2-z1),2);
			d[0]=(z2-z1)*(y-y1)-(y2-y1)*(z-z1);
			d[1]=(x2-x1)*(z-z1)-(z2-z1)*(x-x1);
			d[2]=(y2-y1)*(x-x1)-(x2-x1)*(y-y1);
			
			temp=4*a*c-b*b;
			if (fabs(temp)>1e-12) temp=2/((temp))*((2*c+b)/sqrt(a+b+c)-b/sqrt(a));
			else temp=0; //(singular)
			if (nabour[1]<0 && i==1) temp=0; //(trailing edge)
			for (j=0;j<3;j++)
				v[j]=temp*d[j]+v[j];
		
		}	
		
		vp=WernerPoint(v[0],v[1],v[2]);
		}
		return vp;
	}

	double m_cPu;
	double m_cPl;
};

#endif // !defined(AFX_LATTICE_H__FECD7277_8EF1_11D4_B1C9_0060B068485C__INCLUDED_)
