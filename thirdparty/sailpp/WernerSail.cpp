
#include "stdafx.h"
#include <assert.h>
#include <QtCore>
#include "WernerWorld.h"
#include "WernerSail.h"
//#include <omp.h>
#include "wakeline.h"
#include "lattice.h"
#include <sstream>
#include <iostream>
#include <fstream>
using namespace std;



//////////////////////////////////////////////////////////////////////
//																	//
//        SAIL++													//
//																	//
//////////////////////////////////////////////////////////////////////
/*		NOTATION

a		Angel of incident, degrees
A		Matrix for influence coefficient from wing
alpha	Angel of incidence, radians
B		Matrix for influence coefficient from wing + wake
cp		Control point
cpfrac	spanwise positions of control point, expressed as fraction of panelspan
cpi		Array of the control points at trailing edge
dir		Direction of wake line segment
dist	Distribution, (1=simple, 2=semi cosine, 3=full cosine, 4=elliptic planform
eo		Direction of free stream vector
err		iteration stops when err<tol
Fp		Array for pressure forces on panels
Fptot	Total force on vector form
Fsec	Forces on spanvise sections
G		Circulation matrix
hwl		Length of wake line segments
hx		Panel length
hy		Panel length
ihwl	Length of wake line segments
ip 		Intersecting point, corner of panel
la		Array for all panel objects
loc_err	local "error", the change of wakeline position
loops	Number of iterations performed
Lx		Length of plate in x direction
Ly		Length of plate in y direction
M		Array for pressure moments on panels, moment about (0,0,0)
mirr	Water surface boundary condition applied if mirr=1
Msec	Moments on spanvise section
Mtot	Total moment about (0,0,0)
N		Number of rows/columns of panels
n		Array for indices of neighbouring panels
NOE		Number Of Elements, N*N
norm	Vector normal to a panel
NWLS	Number of wake line segments
rho		Density of air
start	Start point of wake line segment
stop	End point of wake line segment
TE_side	Trailing edge side of panel, =1
tol		Iteration tolerance
Uo		Free stream vector
UoL		Magnitude of free stream vector
vi		Induced velocity
vicp	Induced velocity at control points
viwl	Induced velocity at wakeline
Vjump	Velocity jump
wl		Matrix for all wake line segment objects

*/
//            NUMBERING OF PANELS
//			 _____________________________________
//	j=N-1	|N-1_|____|____|____|____|____________
//	j=N-2	|____|____|____|____|____|____________
//			|____|____|____|____|____|____________
//			|____|____|iN+j|____|____|____________
//	j=2		|_2__|____|____|____|____|____________
//	j=1		|_1__|_N+1|____|____|____|____________
//	j=0		|_0__|_N__|____|____|____|____________

//			i=0	   i=1  i=2      i=N-1


//			 INTERSECTING POINTS:
//
//			   3 _____ 2
//				|	  |
//			    |     |
//				|_____|
//			   0       1

//  		   NEIGHBOURS:
//
//				 __2__
//				|	  |
//			   3|     |1
//				|_____|
//				   0






WernerSail::WernerSail(void):
    m_la(0)
  , m_wl(NULL)
  , m_N(0)
  , m_NWLS(0)
  , m_w(NULL)
  ,m_IndexInWorldList(0)
{
}
WernerSail::WernerSail(const int N, const int nwls,
                       class WernerWorld*w, const QString &   pname):
    m_la(0)
  , m_wl(NULL)
  ,m_N(N)
  ,m_NWLS(nwls)
  ,m_w(w)
  ,m_name(pname)
  ,m_IndexInWorldList(0)
{
    //-1 for wakeshedding edge, -2 for non wakeshedding edge
    wake_side[0]=-2;  // lower edge (a verif)
    wake_side[1]=-1;  // trailing edge OK
    wake_side[2]=-2; // upper edge (a verif)
    wake_side[3]=-2;
    m_IndexInWorldList = m_w->m_Sails.size();
    m_w->m_Sails.push_back(this);

    m_la=new lattice[N*N];
    for(int i = 0; i<N*N;i++) {
        m_la[i].m_cPl = m_la[i].m_cPu=0;
    }
    m_wl=new wakeline*[N+2];
    for(int i=0;i<N+2;i++)  m_wl[i]=new wakeline[nwls+1];
}

bool WernerSail::IsMain() // really means that it is the second in the list
{ 
    class WernerSail* theLast = m_w->m_Sails.back();
    return(this == theLast);
}
WernerSail::~WernerSail(void)
{
    if(m_la) delete[] m_la;
    if(m_wl) {
        for(int i=0;i<m_N+2;i++)
            delete[]  m_wl[i];
        delete[] m_wl;

        m_w->m_Sails.remove(this);
    }
}

int WernerSail::SetVVs(std::vector<WernerPoint>vv )   // expect (N+1)^2
{
    if((int) vv.size() !=  (this->m_N+1)*(this->m_N +1))
    {assert(0); return 0;}
    m_vvs = vv;
    return m_vvs.size();
}

int WernerSail::SetCPs(std::vector<WernerPoint>cps )  // expect (N)^2
{
    if((int) cps.size() != ( this->m_N)*(this->m_N))
    { assert(0);  return 0; }
    m_cps = cps;
    return m_cps.size();
}
std::vector<double> WernerSail::GetPressures()
{
    std::vector<double> rv;
    int i, NOE= this->m_N * this->m_N;

    for (i=0;i<NOE;i++)
        rv.push_back(  this->m_la[i].m_cPl - this->m_la[i].m_cPu   );  //laJ[i].m_cPl- laJ[i].m_cPu

    return rv;
}

//-------------Define wakelines-------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void WernerSail::wakelineCreate( ) // could generate all wakes here
{
    //	N				Number of panels equals N*N
    //	NWLS				Number of Wake Line Segments
    //	hwl				length of wake line segments
    //	Uo				Free stream
    //  mirr				=1 for water surface boundary condition
    // int NOE=N*N;		//number of elements
    //  int TE_side=1;		//trailing edge side of panel
    //  peter we'll increase the angle of the wake a bit to keep it clear of the main

    lattice *la = this->m_la;
    wakeline **wl = this->m_wl;
    int N = this->m_N;
    int NWLS = this->m_NWLS;
    double wlength = this->m_w->ihwl*1;

    int i,j,k;
    const WernerPoint *ip;			//intersecting points (corner of panel)
    WernerPoint start, stop;

    WernerPoint wakedir = m_w->Uo; // wakedir.z = wakedir.z;// * 1.25;
    wakedir=wakedir.normalize();
    ip=la[0].getIp();
    double hwl=(wlength)/((NWLS)*1.0);

    //Wakelines behind trailing edge
    j=0;
    for (i=(N-1)*N;i<(N-1)*N+N;i++) //Panels on the trailing edge
    {	ip=la[i].getIp();
        start=ip[1];

        //define wakelines directed along trailing edge
        for(k=0;k<NWLS;k++)
        {
            //     if (k==0)
            //         stop=start.plus((ip[3].vec(ip[2])).normalize().prod(hwl));//.prod(0.25); //peter test
            //    else
            stop=start.plus(wakedir.prod(hwl));//(ip[3].vec(ip[2]))
            wl[j][k]=wakeline(start,stop,true);
            start=stop;
        }
        stop=wakedir;	//=direction
        wl[j][NWLS]=wakeline(start,stop,false); //semi-infinit
        j++;
    }
    ip=la[(N-1)*N+N-1].getIp();
    start=ip[2];
    for(k=0;k<NWLS;k++)
    {

        stop=start.plus(wakedir.prod(hwl));//(ip[3].vec(ip[2]))
        wl[N][k]=wakeline(start,stop,true);
        start=stop;
    }
    stop=wakedir;	//=direction
    wl[N][NWLS]=wakeline(start,stop,false); //semi-infinit
} // wakelines


std::string WernerSail::pressure_summation( double *G)//,
//    double *GblobF, int *NMWLF,
//    double *GblobH, int *NMWLH,int blob_av)
{	
    lattice *laM =this->m_la ;
    int N=this->m_N ;
    assert(this->m_w->NWLS == this->m_NWLS) ; //int mirr=this->m_w->mirr;
    WernerPoint Uo=this->m_w->Uo ;

    double rho=1.205;//density of air
    double UoL=Uo.length();
    int NOE=N*N;
    WernerPoint eo=Uo.normalize(); //direction of free stream

    int i,j ;
    int n[4]; //nabour
    double DX,DX0,DX1, Vjumpi,Vjumpj,Cdisec,paneledge,CDi,Clef,CMle,CL;
    double *Clsec=new double[N];
    double *V_lower=new double[NOE];
    double *V_upper=new double[NOE];
    double *F=new double[NOE];
    WernerPoint ti,tj, Cfpsec, cp,Vu,Vl,Fptot,leftot,norm;
    WernerPoint  vi;
    WernerPoint *Fsec=new WernerPoint[N];
    WernerPoint *Msec=new WernerPoint[N];
    const WernerPoint *ip;
    WernerPoint *M=new WernerPoint[NOE];
    WernerPoint Mtot=WernerPoint(0,0,0);
    WernerPoint *Fp=new WernerPoint[NOE];	//aerodynamic force
    WernerPoint *Vjump=new WernerPoint[NOE];//velocity jump
    double *Cpu=new double[NOE];
    double *Cpl=new double[NOE];

    WernerPoint Mleftot=WernerPoint(0,0,0);
    std::stringstream strOut;

    //--Calculate the velocity jumps--
    //#pragma omp parallel shared(i,NOE, laM,G,Vjump ) private(n,ip,cp,DX,ti,tj, Vjumpi,Vjumpj,DX0,DX1) default (none)  // not tested - needs intel compiler
    {
        for (i=0;i<NOE;i++)
        {
            laM[i].getNabour(n[0],n[1],n[2],n[3]);
            ip=laM[i].getIp();
            cp=laM[i].getCp();

            //Vectors in "i" and "j" direction, tangential to panel
            ti=((ip[0].vec(ip[1]).plus(ip[3].vec(ip[2]))).prod(0.5)).normalize();
            tj=((ip[1].vec(ip[2]).plus(ip[0].vec(ip[3]))).prod(0.5)).normalize();

            //Velocity jump in "i" direction (local panel coordinates)
            if (n[3]<0) //leading edge
            {	//1st order forward difference
                DX=(cp.vec(laM[n[1]].getCp())).dot(ti);//(delta x)
                Vjumpi=(G[n[1]+NOE]-G[i+NOE])/DX;
            }
            else if (n[1]<0) //trailing edge
                Vjumpi=0;
            else  //internal panel
            {	//second order finite difference
                DX1=(cp.vec(laM[n[1]].getCp())).dot(ti);//(delta x_n+1)
                DX0=(laM[n[3]].getCp().vec(cp)).dot(ti);//(delta x_n)
                Vjumpi=(G[n[1]+NOE]*DX0*DX0-G[n[3]+NOE]*DX1*DX1+G[i+NOE]*(DX1*DX1-DX0*DX0))/(DX1*DX0*(DX0+DX1));
            }

            //Velocity jump in "j" direction (local panel coordinates)
            if (n[0]<0) //lower edge
            {	//1st order forward difference
                DX=(cp.vec(laM[n[2]].getCp())).dot(tj);//(delta x)
                Vjumpj=(G[n[2]+NOE]-G[i+NOE])/DX;
            }
            else if (n[2]<0) //upper edge
            {	//1st order forward difference
                DX=(laM[n[0]].getCp().vec(cp)).dot(tj);//(delta x)
                Vjumpj=(G[i+NOE]-G[n[0]+NOE])/DX;
            }

            else  //internal panel
            {	//second order finite difference
                DX1=(cp.vec(laM[n[2]].getCp())).dot(tj);//(delta x_n+1)
                DX0=(laM[n[0]].getCp().vec(cp)).dot(tj);//(delta x_n)
                Vjumpj=(G[n[2]+NOE]*DX0*DX0-G[n[0]+NOE]*DX1*DX1+G[i+NOE]*(DX1*DX1-DX0*DX0))/(DX1*DX0*(DX0+DX1));
            }

            //Velocity jump in global coordinates
            Vjump[i]=(ti.prod(Vjumpi).plus(tj.prod(Vjumpj))).prod(2*pi);
        }
    }

    //--Calculate the total velocity and pressure force in element i, force in the normal direction--
    Fptot.setPos(0,0,0);
    for (i=0;i<NOE;i++)
    {	norm=laM[i].getNorm();// vector normal to panel surface
        vi.setPos(0,0,0);
        cp=laM[i].getCp();
        //sum the induced velocities from wing panels
        vi=this->m_w->vInduced_w(cp, G );
        //Total velocity at control point i, upper and lower side
        Vu=Uo.plus(vi).plus(Vjump[i]);
        Vl=Uo.plus(vi).plus(Vjump[i].prod(-1));
        //Pressure force from element i
        Fp[i]=norm.prod(0.5*rho*laM[i].getArea()*(-Vu.length()*Vu.length()+Vl.length()*Vl.length()));//
        Fptot=Fptot.plus(Fp[i]);
        //Moment about (0,0,0)
        M[i]=(laM[i].getCp().cross(Fp[i]));
        Mtot=Mtot.plus(M[i]);
        //Pressure coefficient
        laM[i].m_cPu =Cpu[i]=1-(Vu.length()*Vu.length())/(UoL*UoL);
        laM[i].m_cPl =Cpl[i]=1-(Vl.length()*Vl.length())/(UoL*UoL);

        // Peter  the equal repartition  of vjump^2    onto ths upper and lower surfaces
        // is only good for small perturbations
        // for finite perturbations, some people use a heuristic trig function
        //to divide unequally so that the positive cps do not exceed 1.0
        // here we'll be lazy and just clip them.

        laM[i].m_cPu = max(-5.0,min(1.0, laM[i].m_cPu  ));
        laM[i].m_cPl = max(-5.0,min(1.0, laM[i].m_cPl  ));
    }

    //Sectional pressure forces
    for (j=0;j<N;j++)
    {	for (i=0;i<N;i++)
        {
            Fsec[j]=Fsec[j].plus(Fp[j+i*N]);
            Msec[j]=Msec[j].plus(M[j+i*N]);
        }
    }

    leftot=leading_edge_Fiddes( G );

    //Total force = panel pressure force + leading edge force
    Fptot=Fptot.plus(leftot);
    //Total moment = panel pressure moment + leading edge moment
    Mtot=Mtot.plus(Mleftot);

    //Sum the panel areas
    double area=0;
#pragma parallel
    for (i=0;i<NOE;i++)  area=area+laM[i].getArea();

    //Lift and drag coefficiens
    WernerPoint CFp=Fptot.prod(1/(0.5*rho*area*UoL*UoL)); //Force coefficient
    CDi=CFp.dot(eo);								//Induced drag
    CL=sqrt(CFp.length()*CFp.length()-CDi*CDi);		//Lift
    Clef=leftot.length()/(0.5*rho*area*UoL*UoL);	//Leading edge force
    CMle=Mtot.y/(0.5*rho*area*UoL*UoL*1);			//Moment about leading edge
    //OBS div with root cord length!!

    for (j=0;j<N;j++)								//Sectional lift and moment
    {	ip=laM[j].getIp();
        paneledge=(ip[1].vec(ip[2])).length();
        Cfpsec=(Fsec[j].prod(1/(0.5*rho*area*UoL*UoL)));
        Cdisec=Cfpsec.dot(eo);
        Clsec[j]= sqrt(Cfpsec.length()*Cfpsec.length()-Cdisec*Cdisec)/paneledge;
        Msec[j]=Msec[j].prod(1/(0.5*rho*area*UoL*UoL*1*paneledge));
        //OBS div with root cord length!!
    }

    // Output strings to dialog
    strOut<<qPrintable(this->m_name)<<"\t Cl:\t"<<CL <<endl;
    strOut<<qPrintable(this->m_name)<<"\t CF:\t"<<CFp.length()<<endl;
    strOut<<qPrintable(this->m_name)<<"\t CMLe:\t"<<CMle<<endl;
    strOut<<qPrintable(this->m_name)<<"\t Kv:\t"<<2.0*pi*CDi/(CL*CL)<<endl;
    strOut<<qPrintable(this->m_name)<<"\t C_lef:\t"<<Clef<<endl;


    delete[] Clsec;
    delete[] V_lower;
    delete[] V_upper;
    delete[] F;

    delete[] Fsec;
    delete[] Msec;
    delete[] M;

    delete[] Fp;
    delete[] Vjump;
    delete[] Cpu;
    delete[] Cpl;

    return strOut.str()   ;


}
WernerPoint WernerSail::leading_edge_Fiddes( double *G )
{
    int pisMain = IsMain();
    lattice *la=this->m_la;
    int N=this->m_N;
    double rho=1.205;//density of air
    //  double NOE=N*N;
    //main=1 if the sail is the main and 0 if the jib
    int mAin;
    if(pisMain==1)mAin=N*N; else mAin=0;
    assert(  mAin== this->m_IndexInWorldList*this->m_N*this->m_N);

    int i;
    double P,R,Q,s,lefCoeff,lef_pul,edge;
    WernerPoint cp,lefDir,lef; const WernerPoint *ip;
    WernerPoint leftot=WernerPoint(0,0,0);


    //OBS! when swept leading edge, distance s should be different!!!!!!!!
    for (i=0;i<N;i++)
    {	ip=la[i].getIp();
        cp=la[i].getCp();
        P=ip[3].vec(cp).length();
        Q=ip[0].vec(cp).length();
        R=ip[0].vec(ip[3]).length();
        //distance from leading edge to control points in first row
        s=sqrt(Q*Q-(Q*Q-P*P+R*R)/(2.*R)*(Q*Q-P*P+R*R)/(2.*R));
        //s=cp.x; //For LanSTark distribution
        //direction of leading edge force:
        //tangelntial to panel, perpendiqular to leading edge
        lefDir=(la[i].getNorm().cross(ip[0].vec(ip[3]))).normalize();
        //leading edge force coefficient
        lefCoeff=G[i+mAin]*4.*pi/sqrt(s);
        //leading edge force per unit length
        lef_pul=pi*rho*lefCoeff*lefCoeff/16.;
        //length of the trailing edge of the panel
        edge=ip[0].vec(ip[3]).length();
        //leading edge force on this panel
        lef=lefDir.prod(lef_pul*edge);
        leftot=leftot.plus(lef);
        //		Fsec[i]=Fsec[i].plus(lef);
        //moment about (0,0,0)
        //	Mlef=cp.cross(lef);
        //	Mleftot=Mleftot.plus(lef);
        //	Msec[i]=Msec[i].plus(Mlef);

    }
    return leftot;
}




//------------------Write output to AutoCad-----------------------------------------------------
//----------------------------------------------------------------------------------------------
int WernerSail::acad(QString fname )
{

    int i,j,k;
    QFile  file(fname);

    const WernerPoint *pos;
    WernerPoint posc;
    lattice *laJ = this->m_la;
    wakeline **wlJ	=this->m_wl;
    int N = this->m_N;
    int NWLS =this->m_NWLS;
    //    int NWLSF = this->m_NWLS;
    int NOE=N*N;
    if (! file.open(QIODevice::WriteOnly | QIODevice::Text))
        return 0;

    QTextStream out(&file);

    out<<"\n; erASe all ";
    out<<"\n; -view top zoom window 0,0 0.05,0.05" ;
    out<<"\n; layer set 4jib " ;
    for(i=0;i<NOE;i++)
    {	pos=laJ[i].getIp();
        out<<"\n pOLyline " ;	 //Peter
        for(j=0;j<4;j++)
            out<<" "<<pos[j].x<<","<<pos[j].y<<","<<pos[j].z  ;
        out<<" "<<pos[0].x<<","<<pos[0].y<<","<<pos[0].z  ;
    }
    out<<"\n; layer set 4m " ;


    /**/
    //Draw edges of wing
    /*	str.Format(str+" line ");
 pos=la[0].getIp();
 str.Format(str+"%0.4f,%0.4f,%0.4f ",pos[0].x,pos[0].y,pos[0].z) ;
 pos=la[(N-1)*N].getIp();
 str.Format(str+"%0.4f,%0.4f,%0.4f ",pos[1].x,pos[1].y,pos[1].z) ;
 pos=la[(N-1)*N+N-1].getIp();
 str.Format(str+"%0.4f,%0.4f,%0.4f ",pos[2].x,pos[2].y,pos[2].z) ;
 pos=la[(N-1)].getIp();
 str.Format(str+"%0.4f,%0.4f,%0.4f ",pos[3].x,pos[3].y,pos[3].z) ;
 pos=la[0].getIp();
 str.Format(str+"%0.4f,%0.4f,%0.4f ",pos[0].x,pos[0].y,pos[0].z);
*/
    out<<" \n; laYer set 2jib " ;
    for(j=0;j<=N;j++)
        for(k=0;k<NWLS;k++)
        {	if(wlJ[j][k].deleted==false)
            {	out<<"\n  liNe " ;
                out<<" "<<wlJ[j][k].getStart().x<<","<<wlJ[j][k].getStart().y<<","<<wlJ[j][k].getStart().z ;
                out<<" "<<wlJ[j][k].getStop().x<<","<<wlJ[j][k].getStop().y<<","<<wlJ[j][k].getStop().z ;
            }
        }

    out<<"\n; layer set 3m " ;
    for(k=0;k<NWLS;k++)
    {	out<<" "<<"\n  polyline " ;
        for(j=0;j<=N;j++)
        {	if(wlJ[j][k].deleted==false)
                out<<" "<<wlJ[j][k].getStart().x<<","<<wlJ[j][k].getStart().y<<","<<wlJ[j][k].getStart().z ;
        }
    }

    out<<"\n";
    //Controlpoints
    for(j=0;j<NOE;j++)
    {	posc=laJ[j].getCp();
        out<<" \n  circle ";
        out <<"  "<<posc.x<<","<<posc.y<<","<<posc.z  ;
        out<<" "<<"0.0500" ;
    }

    //Write to file


    double x,y,z;
    const WernerPoint *ptemp;
    //3D MESH


    //WAKE

    out<<" "<<"\n;SRfPtGrid "<<N+1<<" "<<NWLS*2 ;

    for(j=0;j<=N;j++)
        for(k=0;k<NWLS;k++)
        {
            out<<" "<<"\npoLYline " ;
            out<<" "<<wlJ[j][k].getStart().x<<","<<wlJ[j][k].getStart().y<<","<<wlJ[j][k].getStart().z ;
            out<<" "<<wlJ[j][k].getStop().x<<","<<wlJ[j][k].getStop().y<<","<<wlJ[j][k].getStop().z ;
        }


    //WING
    //     out<<" "<<"\n;layer set 4  " ;


    out<< "\n\n SrfPTGrid   " <<" "<<N+1 <<" "<<N+1 ;

    ptemp=laJ[(N-1)*N+N-1].getIp();
    ptemp[2].getPos(x,y,z);
    out<<" "<<"\n "  <<x<<" "<< y<<" "<<z  ;
    out<<" "<<"\n;layer Set 4jib  " ;
    out<<" "<<"\n\n SrFPtGrid  " << N+1<<" "<<N+1 ;
    for(i=0;i<N;i++)
    {	for(j=0;j<N;j++)
        {	ptemp=laJ[i*N+j].getIp();
            ptemp[0].getPos(x,y,z);
            out<<" "<<"\n "<<x<<","<<y<<","<<z  ;
        }
        ptemp=laJ[i*N+N-1].getIp();
        ptemp[3].getPos(x,y,z);
        out<<" "<<"\n  "<<x<<","<<y<<","<<z  ;
    }
    for(j=0;j<N;j++)
    {	ptemp=laJ[(N-1)*N+j].getIp();
        ptemp[1].getPos(x,y,z);
        out<<" "<<"\n "<<x<<","<<y<<","<<z  ;
    }
    ptemp=laJ[(N-1)*N+N-1].getIp();
    ptemp[2].getPos(x,y,z);
    out<<" "<<"\n "<<x<<","<<y<<","<<z  ;

    file.close();

    return 1;
}

int  WernerSail::WakeToVTK(QString fname)// write the wake
{
    int  j,k;
    int N = this->m_N;

    ofstream os (qPrintable(fname ) );
    if(!os.is_open())
        return 0;

    os<<"# vtk DataFile Version 3.0 "<<endl;
    os<<"vtk output "<<endl;
    os<<"ASCII "<<endl;
    // the WAKE
    os<<"DATASET STRUCTURED_GRID "<<endl;

    os<<"DIMENSIONS "<<N+1<<" "<< this->m_NWLS  <<" "<<1 <<endl;
    os<<"POINTS  "<<(N+1)* this->m_NWLS   <<" float "<<endl;
    for(k=0;k<this->m_NWLS ;k++)
        for(j=0;j<=N;j++)

        {
            os<< m_wl[j][k].getStart().x<<" "<<-m_wl[j][k].getStart().z<<" "<<m_wl[j][k].getStart().y<<endl;
            //           os<< m_wl[j][k]. getStop().x<<" "<<-m_wl[j][k]. getStop().z<<" "<<m_wl[j][k]. getStop().y<<endl;
        }

    os.close();
    return 1;
}

//------------------Write output to Paraview----------------------------------------------------
//----------------------------------------------------------------------------------------------
int WernerSail::toVTK(std::string fname ) //.vtK
{

    int i,j,k;

    const WernerPoint *pos=0;
    lattice *laJ = this->m_la;

    int N = this->m_N;
    int NOE=N*N;

    ofstream os (fname.c_str() );
    if(!os.is_open())
        return 0;

    os<<"# vtk DataFile Version 3.0 "<<endl;
    os<<"vtk output "<<endl;
    os<<"ASCII "<<endl;

    // the WING

    os<<"DATASET STRUCTURED_GRID "<<endl;

    os<<"DIMENSIONS "<<N+1<<" "<<N+1 <<" "<<1 <<endl;
    os<<"POINTS  "<<(N+1)*(N+1) <<" float "<<endl;
    // leading edge
    for(j=0,k=0;j<N;j++,k++) {
        pos=m_la[k].getIp();
        os<<pos[0].x<<" "<<-pos[0].z<<" "<<pos[0].y<<endl;
    }
    os<<pos[3].x<<" "<<-pos[3].z<<" "<<pos[3].y<<endl;
    // the rest or the surface
    for(k=0,i=0;i<N;i++) {
        for(j=0;j<N;j++,k++) {
            pos=m_la[k].getIp();
            os<<pos[1].x<<" "<<-pos[1].z<<" "<<pos[1].y<<endl;
        }
        os<<pos[2].x<<" "<<-pos[2].z<<" "<<pos[2].y<<endl;
    }
    os<<"CELL_DATA "<<N*N<<"\nSCALARS dCp double\nLOOKUP_TABLE default"<<endl;
    for(i=0;i<NOE;i++)
        os<<laJ[i].m_cPl- laJ[i].m_cPu<<" "<<endl; // cp is the control pt;

    os<< "\nSCALARS cPU double\nLOOKUP_TABLE default"<<endl;
    for(i=0;i<NOE;i++)
        os<<laJ[i].m_cPu<<" "<<endl; // cp is the control pt;

    os<< "\nSCALARS cPL double\nLOOKUP_TABLE default"<<endl;
    for(i=0;i<NOE;i++)
        os<<laJ[i].m_cPl<<" "<<endl; // cp is the control pt;

    // the WAKE
    wakeline **wlJ = this->m_wl;

    os<<"DATASET STRUCTURED_GRID "<<endl;

    os<<"DIMENSIONS "<<N+1<<" "<< this->m_NWLS *2  <<" "<<1 <<endl;
    os<<"POINTS  "<<(N+1)* this->m_NWLS *2 <<" float "<<endl;

    for(j=0;j<=N;j++)
        for(k=0;k<this->m_NWLS ;k++)
        {
            os<< wlJ[j][k].getStart().x<<" "<<-wlJ[j][k].getStart().z<<" "<<wlJ[j][k].getStart().y<<endl;
            os<< wlJ[j][k]. getStop().x<<" "<<-wlJ[j][k]. getStop().z<<" "<<wlJ[j][k]. getStop().y<<endl;
        }


    //       // leading edge
    //       for(j=0,k=0;j<N;j++,k++) {
    //           pos=m_la[k].getIp();
    //           os<<pos[0].x<<" "<<pos[0].z<<" "<<pos[0].y<<endl;
    //       }
    //       os<<pos[3].x<<" "<<pos[3].z<<" "<<pos[3].y<<endl;
    //       // the rest or the surface
    //       for(k=0,i=0;i<N;i++) {
    //           for(j=0;j<N;j++,k++) {
    //               pos=m_la[k].getIp();
    //               os<<pos[1].x<<" "<<pos[1].z<<" "<<pos[1].y<<endl;
    //           }
    //           os<<pos[2].x<<" "<<pos[2].z<<" "<<pos[2].y<<endl;
    //       }





    os.close();
    return 1;
}
//------Calculates the forces acting on the lattice using Joukowski law, Guermond's approach--
//--------------------------------------------------------------------------------------------
std::string WernerSail::joukowski( double *G) //, double *GblobF,
// int *NMWLF,double *GblobH, int *NMWLH,int blob_av)
{
    //    double a = this->m_w->Incidence_Deg;

    lattice *laM = this->m_la;
    //   wakeline **wlM = this->m_wl;

    //int mirr ,int NWLS, point Uo,
    int N = m_N;
    //  CString tempstr;
    std::string rv;//("(( joukowski output))");
    double rho=1.205;//density of air
    // double UoL=this->m_w->Uo.length();
    int NOE=m_N*m_N;
    //   double alpha=-a*pi/180;	//angel of incidence, radians
    WernerPoint eo=this->m_w->Uo.normalize(); //direction of free stream

    int i,j ;
    double g;
    double  *Gw =new double[m_N];
    WernerPoint  *vi  =new WernerPoint[NOE];
    WernerPoint F,side,A,temp,t,p,cp,v,leftot;

    //sum induced velocities
    for (i=0;i<NOE;i++)
    {	cp=laM[i].getCp();
        vi[i].setPos(0,0,0);
        vi[i]=this->m_w->vInduced_w(cp, G );
        vi[i]=vi[i].plus(this->m_w->Uo);
    }
    F.setPos(0,0,0);
    //Sum contributions from vertical lines
    for (i=0;i<N;i++)
    {	for (j=0;j<N;j++)
        {	side=laM[i*N+j].getIp()[0].vec(laM[i*N+j].getIp()[3]);//side vector
            if(i==0)
            {	g=-G[(0)*N+j+NOE];
                A=(laM[(0)*N+j].getCp().vec(laM[(1)*N+j].getCp())).normalize();
                temp=A.vec(side.normalize());
                temp.setPos(1/temp.x,1/temp.y,1/temp.z);
                t=laM[i*N+j].getIp()[0].vec(laM[(0)*N+j].getCp()).prod(temp);
                p=(laM[(0)*N+j].getCp()).plus(A.prod(t));
                v=WernerWorld::interpol(laM[(0)*N+j].getCp(),laM[(1)*N+j].getCp(),p,vi[0*N+j],vi[1*N+j]);
            }
            else
            {	g=G[(i-1)*N+j+NOE]-G[i*N+j+NOE];  //circulation in side j
                A=(laM[(i-1)*N+j].getCp().vec(laM[(i)*N+j].getCp())).normalize();
                temp=A.vec(side.normalize());
                temp.setPos(1/temp.x,1/temp.y,1/temp.z);
                t=laM[i*N+j].getIp()[0].vec(laM[(i-1)*N+j].getCp()).prod(temp);
                p=(laM[(i-1)*N+j].getCp()).plus(A.prod(t));
                v=WernerWorld::interpol(laM[(i-1)*N+j].getCp(),laM[(i)*N+j].getCp(),p,vi[(i-1)*N+j],vi[i*N+j]);
            }
            F=F.plus(v.cross(side).prod(rho*g*4*pi));
        }

    }
    //Contributions from horizontal lines
    for (j=0;j<N;j++)
    {	for (i=0;i<N;i++)
        {	side=laM[i*N+j].getIp()[0].vec(laM[i*N+j].getIp()[1]);//side vector
            if(j==0)
            {	g=G[(i)*N+0+NOE];
                A=(laM[(i)*N+0].getCp().vec(laM[(i)*N+1].getCp())).normalize();
                temp=A.vec(side.normalize());
                temp.setPos(1/temp.x,1/temp.y,1/temp.z);
                t=laM[i*N+j].getIp()[0].vec(laM[(i)*N+0].getCp()).prod(temp);
                p=(laM[(i)*N+j].getCp()).plus(A.prod(t));
                v=WernerWorld::interpol(laM[(i)*N+0].getCp(),laM[(i)*N+1].getCp(),p,vi[i*N+0],vi[0*N+1]);
            }
            else
            {	g=G[(i)*N+(j)+NOE]-G[i*N+(j-1)+NOE];  //circulation in side j
                A=(laM[(i)*N+(j-1)].getCp().vec(laM[(i)*N+j].getCp())).normalize();
                temp=A.vec(side.normalize());
                temp.setPos(1/temp.x,1/temp.y,1/temp.z);
                t=laM[i*N+j].getIp()[0].vec(laM[(i)*N+(j-1)].getCp()).prod(temp);
                p=(laM[(i)*N+(j-1)].getCp()).plus(A.prod(t));
                v=WernerWorld::interpol(laM[(i)*N+(j-1)].getCp(),laM[(i)*N+j].getCp(),p,vi[(i)*N+(j-1)],vi[i*N+j]);
            }
            F=F.plus(v.cross(side).prod(rho*g*4*pi));
        }
    }
    //Upper edge
    for (i=0;i<N;i++)
    {	side=laM[i*N+N-1].getIp()[3].vec(laM[i*N+N-1].getIp()[2]);//side vector
        g=-G[i*N+N-1+NOE];
        A=(laM[(i)*N+N-2].getCp().vec(laM[(i)*N+N-1].getCp())).normalize();
        temp=A.vec(side.normalize());
        temp.setPos(1/temp.x,1/temp.y,1/temp.z);
        t=laM[i*N+N-1].getIp()[3].vec(laM[i*N+N-2].getCp()).prod(temp);
        p=(laM[(i)*N+N-2].getCp()).plus(A.prod(t));
        v=WernerWorld::interpol(laM[i*N+N-2].getCp(),laM[i*N+N-1].getCp(),p,vi[i*N+N-2],vi[i*N+N-1]);
        F=F.plus(v.cross(side).prod(rho*g*4*pi));
    }




    //Leading edge force Guerrmond
    //OBS Works only for the flat plate with Stark/Lan distribution!!!!!!!!!
    /*	double l1,l2,A1,A2,W;
 for (j=0;j<N;j++)
 {	ip=la[j].getIp();
  g=-G[j]*4*pi;
  l1=ip[0].x;
  W=2*pi/(2*N+1)*sin(pi/(2*N+1));
  A1=pow(g/(W),2)*l1;
  g=(G[j]-G[N+j])*4*pi;
  l2=ip[1].x;
  W=2*pi/(2*N+1)*sin(3*pi/(2*N+1));
  A2=pow(g/(W),2)*l2;

  //leading edge force per unit length
  lef_pul=rho*pi*interpol(l2,l1,0,A2,A1);


  //direction of leading edge force:
  //tangelntial to panel, perpendiqular to leading edge
  lefDir=(la[j].getNorm().cross(ip[0].vec(ip[3]))).normalize();

  //length of the trailing edge of the panel
  edge=ip[0].vec(ip[3]).length();
  //leading edge force on this panel
  lef=lef.plus(lefDir.prod(lef_pul*edge));

 }
*/

    leftot= leading_edge_Fiddes(  G );
    F=F.plus(leftot);

    CString str;

    /*	str.Format("A=[");
 for (i=0;i<NOE;i++)
 {
  cp=la[i].getCp();
  str.Format(str+"%i\t%0.8f\t%0.8f\t%0.8f\t%0.8f\n",i,cp.x,cp.y,vi[i].x,vi[i].y) ;
 }
  str.Format(str+"];");
 CFile acfile;
 acfile.Open("C:\\SofiaWerner\\sail++\\matlab\\vi.m",CFile::modeCreate +CFile::modeWrite);
 acfile.Write(str,str.GetLength());
 acfile.Close();
*/

    //Sum the panel areas
    double area=0;
    for (i=0;i<NOE;i++)  area=area+laM[i].getArea();

#ifndef SAILPLUSPLUS_LIBRARY
    //Lift and drag coefficiens
    point CFp=F.prod(1/(0.5*rho*area*UoL*UoL)); //Force coefficient


    double CDi=CFp.dot(eo);					//Induced drag


    double CL=sqrt(CFp.length()*CFp.length()-CDi*CDi);		//Lift
    double Clef=leftot.length()/(0.5*rho*area*UoL*UoL);	//Leading edge force

    CString strOut;

    strOut.Format("J dCl/da:\t%f\n",CL/alpha);
    strOut.Format(strOut+"J CF:\t%f\n",CFp.length());
    strOut.Format(strOut+"J C_lef/a^2::\t%8.4f\n",Clef/(alpha*alpha));
    strOut.Format(strOut+"J Kv:\t%f\n",2*pi*CDi/(CL*CL));
#endif
    delete[]  Gw ;
    delete[]  vi  ;
    return rv;
}


//-------------Define neighbours of all panels on wing---------------------------------------
//-------------------------------------------------------------------------------------------
void WernerSail::neighbours()
{
    // Values of "nabour" array equal the indices of the neighouring panels
    // For non-wakeshedding edge nabour=-2. For wakeshedding edges nabour=-1
    lattice *la=this->m_la;
    int N = this->m_N;


    int i,j;

    for (i=1;i<N-1;i++)
    {	for(j=1;j<N-1;j++)
            la[i*N+j].setNabour(i*N+(j-1),(i+1)*N+j,i*N+(j+1),(i-1)*N+j);
        la[i*N].setNabour(wake_side[0],(i+1)*N,i*N+1,(i-1)*N);                   //lower edge
        la[i*N+N-1].setNabour(i*N+N-2,(i+1)*N+N-1,wake_side[2],(i-1)*N+N-1);     //upper edge
        la[i].setNabour(i-1,N+i,i+1,wake_side[3]);							     //leading edge
        la[(N-1)*N+i].setNabour((N-1)*N+i-1,wake_side[1],(N-1)*N+i+1,(N-2)*N+i); //trailing edge
    }
    la[0].setNabour(wake_side[0],N,1,wake_side[3]);								  //lower left corner
    la[N-1].setNabour(N-2,N+N-1,wake_side[2],wake_side[3]);						  //upper left corner
    la[(N-1)*N].setNabour(wake_side[0],wake_side[1],(N-1)*N+1,(N-2)*N);           //lower right corner
    la[(N-1)*N+N-1].setNabour((N-1)*N+N-2,wake_side[1],wake_side[2],(N-2)*N+N-1); //upper right corner

}




