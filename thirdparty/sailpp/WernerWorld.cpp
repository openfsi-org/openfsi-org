
#include "stdafx.h"
#include <iostream>
#include <QDebug>
#include "mkl_lapacke.h"
#include <assert.h>
#include "wakeline.h"
#include "lattice.h"
#include "WernerSail.h"
#include "WernerWorld.h"


//////////////////////////////////////////////////////////////////////
//																	//
//        SAIL++													//
//																	//
//////////////////////////////////////////////////////////////////////
/*		NOTATION

a		Angel of incident, degrees
A		Matrix for influence coefficient from wing
alpha	Angel of incidence, radians
B		Matrix for influence coefficient from wing + wake
cp		Control point
cpfrac	spanwise positions of control point, expressed as fraction of panelspan
cpi		Array of the control points at trailing edge
dir		Direction of wake line segment
dist	Distribution, (1=simple, 2=semi cosine, 3=full cosine, 4=elliptic planform
eo		Direction of free stream vector
err		iteration stops when err<tol
Fp		Array for pressure forces on panels
Fptot	Total force on vector form
Fsec	Forces on spanvise sections
G		Circulation matrix
hwl		Length of wake line segments
hx		Panel length
hy		Panel length
ihwl	Length of wake line segments
ip 		Intersecting point, corner of panel
la		Array for all panel objects
loc_err	local "error", the change of wakeline position
loops	Number of iterations performed
Lx		Length of plate in x direction
Ly		Length of plate in y direction
M		Array for pressure moments on panels, moment about (0,0,0)
mirr	Water surface boundary condition applied if mirr=1
Msec	Moments on spanvise section
Mtot	Total moment about (0,0,0)
N		Number of rows/columns of panels
n		Array for indices of neighbouring panels
NOE		Number Of Elements, N*N
norm	Vector normal to a panel
NWLS	Number of wake line segments
rho		Density of air
start	Start point of wake line segment
stop	End point of wake line segment
TE_side	Trailing edge side of panel, =1
tol		Iteration tolerance
Uo		Free stream vector
UoL		Magnitude of free stream vector
vi		Induced velocity
vicp	Induced velocity at control points
viwl	Induced velocity at wakeline
Vjump	Velocity jump
wl		Matrix for all wake line segment objects

*/
//            NUMBERING OF PANELS
//			 _____________________________________
//	j=N-1	|N-1_|____|____|____|____|____________
//	j=N-2	|____|____|____|____|____|____________
//			|____|____|____|____|____|____________
//			|____|____|iN+j|____|____|____________
//	j=2		|_2__|____|____|____|____|____________
//	j=1		|_1__|_N+1|____|____|____|____________
//	j=0		|_0__|_N__|____|____|____|____________

//			i=0	   i=1  i=2      i=N-1


//			 INTERSECTING POINTS:
//
//			   3 _____ 2
//				|	  |
//			    |     |
//				|_____|
//			   0       1

//  		   NEIGHBOURS:
//
//				 __2__
//				|	  |
//			   3|     |1
//				|_____|
//				   0





WernerWorld::WernerWorld(void):
    N(20),
    dist(5),
    NWLS(20),
    NWLSF(20),
    ihwl(20)
{
    mirr=1;		//=1 for water surface boundary condition
    tol=1e-3;		//iteration tolerance
    Incidence_Deg = 22;
    Uo=WernerPoint(6.26*cos(Incidence_Deg*pi/180),0,6.26*sin(Incidence_Deg*pi/180));	//freestream for sail
}


WernerWorld::~WernerWorld(void)
{
    while(this->m_Sails.size())
        delete (*(this->m_Sails.begin()));
}

int WernerWorld::SetParameters(double pinc,int pN,int pdist,int pNWLS,int pNWLSF,int pihwl,const double pv0)
{
    this->Incidence_Deg=pinc;
    this->N=pN;
    this->dist=pdist;  //Distribution(1=simple, 2=semi cosine, 3=full cosine, 4=elliptic planform)
    this->NWLS=pNWLS;			//Number of wake line segments
    this->NWLSF = pNWLSF;//Number of wake line segments, foot
    this->ihwl=pihwl;//Length of wake line segments
    this-> Uo=WernerPoint(pv0*cos(Incidence_Deg*pi/180.),0,pv0*sin(Incidence_Deg*pi/180.));	//freestream for sail
    return 1;

}

class WernerSail*  WernerWorld::AddSail( const QString pname)
{
    class WernerSail *s = new class WernerSail(N,NWLS,this, pname);
    return s;
}

QString WernerWorld::justRun()
{
    if(this->m_Sails.size() !=2){
        qDebug()<<" WernerWorld probably needs exactly two sails (there are "<< this->m_Sails.size() <<")";
        //  return 0;
    }
    int nSails = (int)m_Sails.size();
    int  maxloops = 3;
    QString text,str1,str2,str3,str4,str5;
    std::list<class WernerSail*>::iterator  it;
    //  int TE_side=1;			//trailing edge side of panel
    //    int blob_av=2; //Number of wakelines that the blob position is based on

    int NOE=N*N;			//Number Of Elements per model
    //   int wlength=ihwl*1;	//Length of wake

    double *GblobF=new double[NWLS+1];
    double *GblobH=new double[NWLS+1];
    std::cout<<" start Sail++.."<<std::endl; // for timing

    /*-----Variables-----*/
    int i,loops;
    double err;
    double **A=new double*[nSails*NOE+1];
    for(i=0;i<nSails*NOE+1;i++)
    {
        A[i]=new double[nSails*NOE+1];
        memset (A[i],0,( nSails*NOE+1 )*sizeof(double) );
    }

    double **B=new double*[nSails*NOE+1];
    for(i=0;i<nSails*NOE+1;i++)
    {
        B[i]=new double[nSails*NOE+1]; // peter added the +1
        memset (B[i],0,( nSails*NOE+1 )*sizeof(double) );
    }

    double *G=new double[nSails*NOE]; // I think that these are the vortex strengths
    CString strOut1,strOut2,strOut3;
    assert(NWLS==NWLSF );

    int *NMWLF=new int[NWLS+1];
    int *NMWLH=new int[NWLS+1];
    for(i=0;i<NWLS+1;i++) NMWLF[i]=0;
    for(i=0;i<NWLS+1;i++) NMWLH[i]=0;
    for(i=0;i<NWLS+1;i++) GblobF[i]=0;
    for(i=0;i<NWLS+1;i++) GblobH[i]=0;
#ifndef SAILPLUSPLUS_LIBRARY
    CFile file;
#endif

    SetupModels();// lJib, main );

    //Define wakelines
    for(it=m_Sails.begin();it!=m_Sails.end(); ++it)
    {
        (*it)->wakelineCreate();
    }


  //   wakelinesFoot(la,wl,wlf, N, NWLS,NWLSF, wlength, Uo, mirr,G);
    //Define neighbouring panels
    for(it=m_Sails.begin();it!=m_Sails.end(); ++it)
        (*it)->neighbours();

    //Calculate influence coefficients from wing panels
    influence_coefficients(  A);//laJ,laM,
    //(The matrix A consists of influence
    //coefficients from wing panels only, matrix B also from wakelines)

    //----------------------ITERATION LOOP-------------------------//
    err=tol+1;
    loops=0;/* lets try */ loops=maxloops+1;
    while ( (err>tol) &(loops<maxloops))
    {
        loops++;
        //Calculate influence coefficients from wake and add them to matrix
        influence_coefficients_wakeline( A, B );//laJ,laM, wlJ, wlM,
        //Solve system of equations
        gauss(B,nSails*NOE,G);
        //	Direct the wake along the local flow

        WernerPoint testcp=(*this->m_Sails.begin())->m_la[1].getCp();
        WernerPoint vi= vInduced_w(testcp, G );
        vi= Uo.plus(vi);
        // double 	bvi=vi.dot(laJ[1].getNorm());

        //

        err=tol-1;
        //     if(0)
        //          direct_wake(k,laJ,laM, wlJ, wlM,N, NWLS,NWLSF, Uo ,G, mirr,NMWLF,GblobF,NMWLH,GblobH,blob_av);


        //plot trefftz plane to Matlab
        //strOut.Format(strOut+output(la, wl,  N, NWLS, NMWLF, k, Gblob, blob_av));
        /**/
    }
    //-------------------------------------------------------------//
    influence_coefficients_wakeline(A, B );
    //Solve system of equations
    std::cout<<" start solver .."; // for timing
    gauss(B,nSails*NOE,G);
    std::cout<<"  done sail++"<<std::endl;
#ifdef STREAMLINES
    streamlines( laJ,laM, wlJ,wlM, N,  mirr, Uo, NWLS,NWLSF, G, GblobF, NMWLF,GblobH, NMWLH, blob_av )	;
#endif
    /*
     point testcp=point(10.50,16.869,-0.1536);
     point vi=vInduced(testcp, la, wl, wlf, N,  mirr, Uo, NWLS,NWLSF, G, GblobF, NMWLF,GblobH, NMWLH, blob_av);
     vi=Uo.plus(vi);

     testcp=point(18.20,17.555,0);
     vi=vInduced(testcp, la, wl, wlf, N,  mirr, Uo, NWLS,NWLSF, G, GblobF, NMWLF,GblobH, NMWLH, blob_av);
     vi=Uo.plus(vi);



    */

    //Calculate the forces with pressure summation
    //	strOut1=pressure_summation(laJ,laM,wlJ,wlM, N, NWLS, ww.Uo, G, ww.Incidence,ww.mirr,GblobF, NMWLF,GblobH, NMWLH,blob_av);
    std::string sds;
    for(it=m_Sails.begin();it!=m_Sails.end(); ++it)
        sds += (*it)->pressure_summation(  G);//, GblobF, NMWLF,GblobH, NMWLH,blob_av);sds += std::string("\n");
    qDebug()<<"pressure summation done";
    strOut1 = CString(sds.c_str());

    if(false) { // we can skip this for FSI
        //Calculate the forces with Trefftz plane analysis
        strOut2=(*this->m_Sails.begin())->trefftz_plane( G ); // shouldnt this be a Sial method too
        std::string  xxxx    ;
        for(it=m_Sails.begin();it!=m_Sails.end(); ++it)
            xxxx+=(*it)->joukowski( G);//, GblobF, NMWLF,GblobH, NMWLH,blob_av);
        strOut3=CString(xxxx.c_str());
   }
    //#ifndef SAILPLUSPLUS_LIBRARY

    //--Write output text to dialog
    str1=QString ("N:\t %1\n").arg(N);
    //    str2=QString("alpha:\t %1\n").arg(Incidence);
    str3=QString("Loops %1 \n").arg(loops);
    str4=QString("NMWLF[NWLS-1] %1 \n").arg(NMWLF[NWLS-1])  +QString(", NWLS %1 \n").arg(NWLS);
    //#endif
    //	str5.Format("AR %0.8f\n",AR);
    qDebug()<<" wind vector = " <<  Uo.x<< Uo.y<<Uo.z ;
    text = str1+str2+str3+str4+str5+strOut1+strOut2+strOut3;
#ifdef NEVER
    UpdateData(FALSE);
#endif
    for(it=m_Sails.begin();it!=m_Sails.end(); ++it) {
        QString ff = (*it)->m_name + "_Werner.vtk";
        (*it)->toVTK ( ff.toStdString())   ;

         ff = (*it)->m_name + "_Werner_wake.vtk";
         (*it)->WakeToVTK(ff);

        //        ff = (*it)->m_name + "_Werner.txt";
        //        (*it)->acad(qPrintable(ff));
    }



    //Write Matlab file
    /*	str1.Format("%% N=%i\tNWLS=%i\tk=%i\talpha=%i\tLength of wake:%i\n",N,NWLS,k,a,wlength);
     str2.Format("\n%% At 1/3 from edge: ");
     file.Open("C:\\SofiaWerner\\matlab\\trefftz.m",CFile::modeCreate +CFile::modeWrite);
     file.Write(str1,str1.GetLength());
     file.Write(strOut,strOut.GetLength());
     file.Write(str2,str2.GetLength());
     file.Close();
    */
    /*
     file.Open("C:\\SofiaWerner\\convIt.txt",CFile::modeCreate +CFile::modeWrite);
     file.Write(strConv,strConv.GetLength());
     file.Close();

    */


    //	if(loops==1)
    //	{	dt=0.05;
    //		err=direct_wake4(la, wl, N, NWLS, Uo ,G, mirr,NMWLF,Gblob,dt,blob_av);
    //	}
    //	else
    //	{
    //		dt=0.005;
    //		err=direct_wake4(la, wl, N, NWLS, Uo ,G, mirr,NMWLF,Gblob,dt,blob_av);
    //	}
    //	err=direct_wake3(la, wl, N, NWLS, Uo ,G, mirr,NMWLF,Gblob,dt,blob_av);
    //	err=direct_wake2(la, wl, N, NWLS, Uo ,G, mirr,NMWLF,Gblob);



    /*	//Circulation
     str.Format("");
     for (i=0;i<NOE;i++)
     { 	cp=la[i].getCp();
      str.Format(str+"%i\t%0.8f\t%0.8f\t%0.8f\t%0.8f\n",i,cp.x,cp.y,cp.z,G[i]) ;
     }

     file.Open("C:\\SofiaWerner\\circulation.txt",CFile::modeCreate +CFile::modeWrite);
     file.Write(str,str.GetLength());
     file.Close();
    */
    for(i=0;i<nSails*NOE+1;i++)delete[]  B[i];
    delete[]  NMWLF ;
    delete[]  NMWLH ;
    delete[] GblobF ;
    delete[] GblobH ;

    for(i=0;i<nSails*NOE+1;i++) delete[] A[i];

    delete [] A;
    delete [] B;
    delete [] G;
        std::cout<<" end Sail++.."<<std::endl; // for timing
    return text;

} // justrun


//Calculates the induced velocity from wings and wakes . Should add foot too
WernerPoint  WernerWorld::vInduced_w(WernerPoint cp,
                                     double *G)
{

    assert(this->m_Sails.size()==2);
    WernerSail *jib = *(this->m_Sails.begin());

    WernerSail *main = *(this->m_Sails.rbegin());
    lattice *laJ=jib->m_la ;
    lattice *laM=main->m_la;
    wakeline **wlJ = jib->m_wl;
    wakeline **wlM=main->m_wl;

    int N = main->m_N; assert(main->m_N ==jib->m_N );
    int NOE=N*N;
    int i,j,k ;
    double *GwJ=new double[N+1]; // peter increased the allocation
    double *GwM=new double[N+1];
    WernerPoint vi;
    wakeline blob;
    blob=wakeline();

    //Define strength of wake lines behind TE, JIB
    GwJ[0]=G[(N-1)*N];
    for(i=1;i<N;i++)
        GwJ[i]=G[(N-1)*N+i]-G[(N-1)*N+i-1];
    GwJ[N]=0;

    GwJ[N]=-G[(N-1)*N+N-1];

    //Define strength of wake lines behind TE, MAIN
    GwM[0]=G[NOE+(N-1)*N];
    for(i=1;i<N;i++)
        GwM[i]=G[(N-1)*N+i+NOE]-G[(N-1)*N+i-1+NOE];
    GwM[N]=-G[(N-1)*N+N-1+NOE];

    //induced velocities from wing
    vi.setPos(0,0,0);
    for(j=0;j<NOE;j++)
    {	vi=vi.plus((laJ[j].vInduced(cp)).prod(G[j]));
        if (mirr==1) vi=vi.plus((laJ[j].vInducedMirr(cp)).prod(G[j]));
        vi=vi.plus((laM[j].vInduced(cp)).prod(G[j+NOE]));
        if (mirr==1) vi=vi.plus((laM[j].vInducedMirr(cp)).prod(G[j+NOE]));
    }

    //sum induced velocity from wakelines
    for(k=0;k<NWLS;k++)
        for(j=0;j<=N;j++)
        {	vi=vi.plus((wlJ[j][k].vInduced(cp)).prod(GwJ[j]));
            vi=vi.plus((wlM[j][k].vInduced(cp)).prod(GwM[j]));
        }

    //If water surface BC is applied
    if (mirr==1)
    {	for(k=0;k<NWLS;k++) //sum induced velocity from mirror wakelines in startpoint
            for(j=0;j<=N;j++)
            {	vi=vi.plus((wlJ[j][k].vInducedMirr(cp)).prod(GwJ[j]));//
                vi=vi.plus((wlM[j][k].vInducedMirr(cp)).prod(GwM[j]));//
            }
    }
    delete[] GwJ;
    delete[] GwM;
    return vi;
}

//------------------Equation solver-------------------------------------------------------------
//----------------------------------------------------------------------------------------------
void WernerWorld::gauss(double **A, int NOE, double *r)
{

    int i, iret;
    iret =  mklSolve(A,NOE, r);
    if(iret !=0) {
        int imax=NOE-1;
        int jmax=NOE;
        int m,j;
        double temp;
        std::cout<<" Use slow Sail++ solver...";
        for (m=0;m<=imax; m++)
        {
            //normailze
            for (i=m; i<=imax; i++)
            {
                temp=A[i][m];
                if (temp!=0) {  // peter moved outside J loop
                    for (j=m;j<=jmax; j++)
                        A[i][j]=A[i][j]/temp;
                }
                else
                    qDebug()<<"zero matrix at "<<i<<m;
            }
            //subtract
            for (i=m+1;i<=imax; i++)
            {
                for (j=m;j<=jmax; j++)
                    A[i][j]=A[i][j]-A[m][j];
            }
            //back substitute
            for (i=imax;i>=0; i--)
            {
                r[i]=A[i][jmax];
                for (j=jmax-1;j>i; j--)
                    r[i]=r[i]-A[i][j]*r[j];
            }
        }
    }
     std::cout<< " end solver\n";
}
int  WernerWorld::mklSolve( double **A, int NOE, double *r)
{
    //lapack_int LAPACKE_dgesv( int matrix_order, lapack_int n, lapack_int nrhs, <datatype>* a, lapack_int lda, lapack_int* ipiv, <datatype>* b, lapack_int ldb );
    lapack_int iret;

    int matrix_order =  LAPACK_ROW_MAJOR ; // LAPACK_ROW_MAJOR or column-major (LAPACK_COL_MAJOR).
    lapack_int n = NOE;
    lapack_int nrhs=1;

    lapack_int lda=NOE;
    lapack_int * ipiv =new lapack_int[NOE];
    if(!ipiv) {
        std::cout<<" cant allocate for Werner MKLSolve" <<std::endl;
        return -1;
    }
    double *a = new double[NOE*NOE];
    if(!a) {
        std::cout<<" cant allocate for Werner MKLSolve" <<std::endl;
		delete[]ipiv;
        return -1;
    }

    lapack_int ldb=1;
    int i,j,k;
    for(i=0,k=0;i< n;i++)
    {
        for(j=0;j< n;j++,k++)
        {  a[k] = A[i][j];}
    }
    for(i=0;i< n;i++ )
        r[i]=A[i][NOE];

    iret= LAPACKE_dgesv(  matrix_order,n,  nrhs,a,  lda, ipiv, r, ldb );
  //  qDebug()<<"LAPACKE_dgesv  gives iret="<<iret;
    //    for(i=0;i< n;i++ )
    //        qDebug()<<i<< r[i];
    //    qDebug()<< " that was lapack";
    delete[] a;
	delete[]ipiv;
    return iret;
}

void WernerWorld::SetupModels() //class WernerSail *pjib,class WernerSail *pmain )
{
    //    class WernerSail *lJib = *(this->m_Sails.begin()); assert(lJib==pjib);
    //    class WernerSail *lmain = *(this->m_Sails.rbegin()); assert(lmain==pmain);


    int i,j, nPoints=(N+1)*(N+1);//10*10;//;// //31*3113*1315*1519*1926*26
    double x,y,z;
    WernerPoint *vp=new WernerPoint[nPoints]; // working array for grid points
    WernerPoint *theCcp=new WernerPoint[N*N]; // working arra for control points
    WernerPoint ip[4];
    int r,c;
    //   FILE *fp = fopen("mainsailInput.txt","w");
    //    fprintf(fp," ///////////////////  MAINSAIL Vertices\n");

    if(true)
    {
        std::list<class WernerSail*>::iterator it ;
        for ( it = m_Sails.begin(); it!=m_Sails.end();++it) {
            class WernerSail *main   = *it;
            lattice *laM = main->m_la;
            i=0;
            for (c = 0;c<N+1;c++){
                for (r = 0;r< N+1;r++)	{
                    x=main->m_vvs[i].x;  y=main->m_vvs[i].y;  z=main->m_vvs[i].z;
                    vp[i++].setPos(x,y,z);
                }
            }
            i=0; 	//fprintf(fp," ///////////////////  MAINSAIL CPs\n");
            for (c = 0;c<N;c++){
                for (r = 0;r< N;r++)	{
                    x=main->m_cps[i].x;  y=main->m_cps[i].y;  z=main->m_cps[i].z;
                    theCcp[i++].setPos(x,y,z);
                }
            }

            for (i=0;i<N;i++)
                for (j=0;j<N;j++)
                {
                    ip[0]=vp[j*(N+1)+i];
                    ip[1]=vp[j*(N+1)+i+1];
                    ip[2]=vp[(j+1)*(N+1)+i+1];
                    ip[3]=vp[(j+1)*(N+1)+i];

                    //	point cp=(ip[0].plus(ip[1].plus(ip[2].plus(ip[3])))).prod(0.25);
                    laM[i*N+j].setIp(ip[0],ip[1],ip[2],ip[3]);
                    WernerPoint norm =laM[i*N+j].getNorm();
                    WernerPoint xa=(ip[0].plus(ip[1].plus(ip[2].plus(ip[3])))).prod(0.25);
                    WernerPoint cpp=norm.prod(  (xa.vec(theCcp[j*N+i])).dot(norm)  ).vec(theCcp[j*N+i]);
                    laM[i*N+j].setCp(cpp);//cp
                }

        } // for it
    }
    delete[] vp;
    delete[] theCcp;
}

#ifdef NEVER
//Reads input file and Writes acad script for sail
void WernerWorld::readData(lattice *laJ,lattice *laM, int N)
{
    int i,j, nPoints=(N+1)*(N+1);//10*10;//;// //31*3113*1315*1519*1926*26
    double x,y,z;
    WernerPoint *vp=new WernerPoint[nPoints];
    WernerPoint *theCcp=new WernerPoint[N*N];
    WernerPoint ip[4];


    FILE *fp = fopen("mainsailInput.txt","w");

    fprintf(fp," ///////////////////  MAINSAIL Vertices\n");
    i=0;
    int r,c;
    double dc = 2.00 /( (double)N);
    double ds = 20.000 /( (double)N);
    double zz = 0;
    for (c = 0;c<N+1;c++){
        for (r = 0;r< N+1;r++)	{

            x=dc*((double) r) ;
            y=ds*((double) c) ;
            z=zz+0.*x/4.;
            fprintf(fp," %f %f %f\n",x,y,z);
            vp[i++].setPos(x,y,z);
        }
    }
    i=0; 	fprintf(fp," ///////////////////  MAINSAIL CPs\n");
    for (c = 0;c<N;c++){
        for (r = 0;r< N;r++)	{

            x=dc*(0.5+(double) r) ;
            y=ds*(0.5+(double) c) ;
            z=zz+0.*x/4.;
            fprintf(fp," %f %f %f\n",x,y,z);
            theCcp[i++].setPos(x,y,z);
        }
    }

    /**/	for (i=0;i<N;i++)
        for (j=0;j<N;j++)
        {	ip[0]=vp[j*(N+1)+i];
            ip[1]=vp[j*(N+1)+i+1];
            ip[2]=vp[(j+1)*(N+1)+i+1];
            ip[3]=vp[(j+1)*(N+1)+i];

            //	point cp=(ip[0].plus(ip[1].plus(ip[2].plus(ip[3])))).prod(0.25);
            laM[i*N+j].setIp(ip[0],ip[1],ip[2],ip[3]);
            WernerPoint norm =laM[i*N+j].getNorm();
            WernerPoint xa=(ip[0].plus(ip[1].plus(ip[2].plus(ip[3])))).prod(0.25);
            WernerPoint cpp=norm.prod(  (xa.vec(theCcp[j*N+i])).dot(norm)  ).vec(theCcp[j*N+i]);
            laM[i*N+j].setCp(cpp);//cp
            //	la[i*N+j].setCp(cp[j*N+i]);
        }

    fprintf(fp," ///////////////////  JIB verticess\n");
    i=0;
    dc = 2.000 /( (double)N);
    ds = 20.000 /( (double)N);
    zz = 0.000;
    for (c = 0;c<N+1;c++){
        for (r = 0;r< N+1;r++)	{

            x=dc*((double) r) -4.0;
            y=ds*((double) c) ;
            z=zz+0.*x/5. +1.;
            fprintf(fp," %f %f %f\n",x,y,z);
            vp[i++].setPos(x,y,z);
        }
    }
    i=0; fprintf(fp," ///////////////////  JIB  CPs\n");
    for (c = 0;c<N;c++){
        for (r = 0;r< N;r++)	{

            x=dc*(0.5+(double) r)-4.0 ;
            y=ds*(0.5+(double) c) ;
            z=zz+0.*x/5.+1.;
            fprintf(fp," %f %f %f\n",x,y,z);
            theCcp[i++].setPos(x,y,z);
        }
    }
    fclose(fp);
    /**/	for (i=0;i<N;i++)
        for (j=0;j<N;j++)
        {	ip[0]=vp[j*(N+1)+i];
            ip[1]=vp[j*(N+1)+i+1];
            ip[2]=vp[(j+1)*(N+1)+i+1];
            ip[3]=vp[(j+1)*(N+1)+i];

            //			point cp=(ip[0].plus(ip[1].plus(ip[2].plus(ip[3])))).prod(0.25);
            laJ[i*N+j].setIp(ip[0],ip[1],ip[2],ip[3]);
            WernerPoint norm =laJ[i*N+j].getNorm();
            WernerPoint xa=(ip[0].plus(ip[1].plus(ip[2].plus(ip[3])))).prod(0.25);
            WernerPoint cpp=norm.prod(  (xa.vec(theCcp[j*N+i])).dot(norm)  ).vec(theCcp[j*N+i]);
            laJ[i*N+j].setCp(cpp);//cp
            //	la[i*N+j].setCp(cp[j*N+i]);
        }
    delete[] vp;
    delete[] theCcp;
}
#endif
void WernerWorld::wakelinesFoot(lattice *la, wakeline **wlf,
                                int N, int NWLS, int NWLSF,
                                double wlength )    // should be a method of WernerSail
{
    //	N					Number of panels equals N*N
    //	NWLS				Number of Wake Line Segments
    //	hwl					length of wake line segments
    //	Uo					Free stream
    //  mirr				=1 for water surface boundary condition
    //   int NOE=N*N;		//number of elements
    return;
    assert(0);
    int i,k;
    const WernerPoint *ip;			//intersecting points (corner of panel)
    WernerPoint start, stop ;
    int *NMWLF=new int[NWLS];
    int *NMWLH=new int[NWLS];
    for(i=0;i<NWLS+1;i++) NMWLF[i]=0;
    for(i=0;i<NWLS+1;i++) NMWLH[i]=0;
    double *GblobF=new double[NWLS+1];
    double *GblobH=new double[NWLS+1];
    for(i=0;i<NWLS+1;i++)	GblobF[i]=0;
    for(i=0;i<NWLS+1;i++)	GblobH[i]=0;

    //   int M=N/2.0;
    ip=la[0].getIp();
    double hwl=(wlength)/((NWLS)*1.0);

    //Wakelines under foot
    for (i=0;i<=N;i++) //Panels on the trailing edge
    {	ip=la[i*N].getIp();
        start=ip[0];


        //define wakelines directed
        for(k=0;k<NWLSF;k++)
        {	if(k==0) stop=start.plus( (ip[3].vec(ip[0])).normalize().prod(hwl)); else
                stop=start.plus(Uo.normalize().prod(hwl));//( (ip[0].vec(ip[1])).cross(ip[0].vec(ip[3])) )
            //	vi=vInduced(start, la, wl, wlf, N,  mirr, Uo, NWLS, 0,G, GblobF, NMWLF,GblobH, NMWLH, 0);
            //	vi=vi.plus(Uo);
            //	stop=start.plus( vi.normalize().prod(hwl));//  Uo
            wlf[i][k]=wakeline(start,stop,true);
            start=stop;
        }
        wlf[i][NWLSF]=wakeline(start,stop,false); //semi-infinit

    }
    for(k=0;k<NWLSF;k++) wlf[N][k].Delete();
    for(k=0;k<NWLSF;k++) wlf[0][k].Delete();
    for(k=0;k<NWLSF;k++) wlf[1][k].Delete();
    for(k=0;k<NWLSF;k++) wlf[2][k].Delete();
    for(k=0;k<NWLSF;k++) wlf[3][k].Delete();
    /**/
    /**/

}

#ifdef NEVER
//-------------Calculate the influence coefficients from wing panels-------------------------
//-------------------------------------------------------------------------------------------
void WernerWorld::influence_coefficients(lattice *laJ, lattice *laM, double **A)
{
    int i,j;

    WernerPoint cp, norm, vi;
    int NOE=N*N;
    // sequence
    //    loop over the lattices  i
    // A[i,j] is the v due to lattice J at point i
    // the numbering of A

    for (i=0;i<NOE;i++)
    {
        cp=laJ[i].getCp();     //control point
        norm=laJ[i].getNorm(); //normal to panel
        for(j=0;j<NOE;j++)
        {
            vi=laJ[j].vInduced(cp); //induced velocity from lattice "j" in control point "i"
            if (mirr==1) vi=vi.plus(laJ[j].vInducedMirr(cp));
            A[i][j]=vi.dot(norm);
        }
        for(j=NOE;j<2*NOE;j++)
        {
            vi=laM[j-NOE].vInduced(cp); //induced velocity from lattice "j" in control point "i"
            if (mirr==1) vi=vi.plus(laM[j-NOE].vInducedMirr(cp));
            A[i][j]=vi.dot(norm);
        }
    }

    for (i=NOE;i<2*NOE;i++)
    {
        cp=laM[i-NOE].getCp();     //control point
        norm=laM[i-NOE].getNorm(); //normal to panel
        for(j=0;j<NOE;j++)
        {
            vi=laJ[j].vInduced(cp); //induced velocity from lattice "j" in control point "i"
            if (mirr==1) vi=vi.plus(laJ[j].vInducedMirr(cp));
            A[i][j]=vi.dot(norm);
        }
        for(j=NOE;j<2*NOE;j++)
        {
            vi=laM[j-NOE].vInduced(cp); //induced velocity from lattice "j" in control point "i"
            if (mirr==1) vi=vi.plus(laM[j-NOE].vInducedMirr(cp));
            A[i][j]=vi.dot(norm);
        }
    }
}
//-------------Calculate the influence coefficients from wakelines and add them to the matrix-
//--------------------------------------------------------------------------------------------
void WernerWorld::influence_coefficients_wakeline(lattice *laJ,lattice *laM,
                                                  wakeline **wlJ,wakeline **wlM,
                                                  double **A, double **B
                                                  )
{

    int i,j,k;

    WernerPoint cp, norm, vi;
    int NOE=N*N;
    for (i=0;i<NOE;i++)//on JIB
    {
        cp=laJ[i].getCp();
        norm=laJ[i].getNorm();
        //from JIB
        for(j=0;j<NOE;j++)
        {	vi.setPos(0,0,0);
            if(j>((N-2)*N+N-1))
            {	for(k=0;k<NWLS;k++)//induced velocity from the 2 wakelines abutting to lattice "j" in controlpoint "i"
                {
                    vi=vi.plus((wlJ[j-(N-1)*N+1][k].vInduced(cp)).prod(-1));
                    vi=vi.plus((wlJ[j-(N-1)*N][k].vInduced(cp)));
                    if (mirr==1) {
                        vi=vi.plus((wlJ[j-(N-1)*N+1][k].vInducedMirr(cp)).prod(-1));
                        vi=vi.plus((wlJ[j-(N-1)*N][k].vInducedMirr(cp)));
                    }
                }
            }
            B[i][j]=A[i][j]+vi.dot(norm);
        }
        //from MAIN
        for(j=NOE;j<2*NOE;j++)
        {	vi.setPos(0,0,0);
            if((j-NOE)>((N-2)*N+N-1))
            {	for(k=0;k<NWLS;k++)//induced velocity from the 2 wakelines abutting to lattice "j" in controlpoint "i"
                {
                    vi=vi.plus((wlM[(j-NOE)-(N-1)*N+1][k].vInduced(cp)).prod(-1));
                    vi=vi.plus((wlM[(j-NOE)-(N-1)*N][k].vInduced(cp)));
                    if (mirr==1) {
                        vi=vi.plus((wlM[(j-NOE)-(N-1)*N+1][k].vInducedMirr(cp)).prod(-1));
                        vi=vi.plus((wlM[(j-NOE)-(N-1)*N][k].vInducedMirr(cp)));
                    }
                }
            }
            B[i][j]=A[i][j]+vi.dot(norm);


        }
        B[i][2*NOE]=-Uo.dot(norm); //free stream
    } // for i


    for (i=NOE;i<2*NOE;i++)//on MAIN
    {	cp=laM[i-NOE].getCp();
        norm=laM[i-NOE].getNorm();
        //from JIB
        for(j=0;j<NOE;j++)
        {	vi.setPos(0,0,0);
            if(j>((N-2)*N+N-1))
            {	for(k=0;k<NWLS;k++)
                {//induced velocity from the 2 wakelines abutting to lattice "j" in controlpoint "i"
                    vi=vi.plus((wlJ[j-(N-1)*N+1][k].vInduced(cp)).prod(-1));
                    vi=vi.plus((wlJ[j-(N-1)*N][k].vInduced(cp)));
                    if (mirr==1) {	vi=vi.plus((wlJ[j-(N-1)*N+1][k].vInducedMirr(cp)).prod(-1));
                        vi=vi.plus((wlJ[j-(N-1)*N][k].vInducedMirr(cp)));}
                }
            }
            B[i][j]=A[i][j]+vi.dot(norm);
        }
        //from MAIN
        for(j=NOE;j<2*NOE;j++)
        {	vi.setPos(0,0,0);
            if((j-NOE)>((N-2)*N+N-1))
            {	for(k=0;k<NWLS;k++)
                {//induced velocity from the 2 wakelines abutting to lattice "j" in controlpoint "i"
                    vi=vi.plus((wlM[(j-NOE)-(N-1)*N+1][k].vInduced(cp)).prod(-1));
                    vi=vi.plus((wlM[(j-NOE)-(N-1)*N][k].vInduced(cp)));
                    if (mirr==1) {	vi=vi.plus((wlM[(j-NOE)-(N-1)*N+1][k].vInducedMirr(cp)).prod(-1));
                        vi=vi.plus((wlM[(j-NOE)-(N-1)*N][k].vInducedMirr(cp)));}
                }
            }
            B[i][j]=A[i][j]+vi.dot(norm);
        }
        B[i][2*NOE]=-Uo.dot(norm); //free stream
    } //for i
}

#else
//-------------Calculate the influence coefficients from wing panels-------------------------
//-------------------------------------------------------------------------------------------
void WernerWorld::influence_coefficients( double **A)
{
    int i,j;

    WernerPoint cp, norm, vi;
    int NOE=N*N;
    // sequence
    //    loop over the lattices  i
    // A[i,j] is the v due to lattice J at point i
    // the numbering of A
    int r,c;
    std::list<class WernerSail*>::iterator it1,it2;
    r=0;
    for (r=0,it1 = m_Sails.begin(); it1!=m_Sails.end();++it1) {
        for(i=0;i<NOE;i++,r++)
        {
            cp= (*it1)->m_la[i].getCp();     //control point
            norm=(*it1)->m_la[i].getNorm(); //normal to panel
            for (c=0,it2 = m_Sails.begin(); it2!=m_Sails.end();++it2) {
                for(j=0;j<NOE;j++,c++)
                {
                    vi=(*it2)->m_la[j].vInduced(cp); //induced velocity from lattice "j" in control point "i"
                    if (mirr)
                        vi=vi.plus((*it2)->m_la[j].vInducedMirr(cp));
                    A[r][c]=vi.dot(norm);
                }
            }

        }
    }
    return;
}
//-------------Calculate the influence coefficients from wakelines and add them to the matrix-
//--------------------------------------------------------------------------------------------
void WernerWorld::influence_coefficients_wakeline( double **A, double **B ) // should add foot in here
{
    int i,j,k;

    WernerPoint cp, norm, vi;
    // four blocks. B is a copy of A except for the last few Js
    // i=0;i<NOE {  use jib lattiv[i].cp
    //          j = 0;j<NOE  from jib wakelines  only the last (n+1_
    //          for(j=NOE;j<2*NOE;j++) from main wakelines, only
    //}
    //    for (i=NOE;i<2*NOE;i++)  use main lattice[i] cp
    //      for(j=0;j<NOE;j++) cllec from jib wakelines
    //       for(j=NOE;j<2*NOE;j++) collect from main wakelines.
    // }
    int NOE=N*N;
    // pseudo-code
    // memcpy A into B
    // for (r=0; it1=bgn // rappel B[r,c]
    //      for each la[i], get cp
    //          for (c=0; it2=bgn
    //              for some j in it2
    //                      sum the wakes from J and add to B[r,c]
    //          end for c, it2
    // end for r, it1

    int r,c;
    size_t nv = m_Sails.size();
    std::list<class WernerSail*>::iterator it1,it2;

    for (r=0,it1 = m_Sails.begin(); it1!=m_Sails.end();++it1) {
        for(i=0;i<NOE;i++,r++)
        {
            cp= (*it1)->m_la[i].getCp();     //control point
            norm=(*it1)->m_la[i].getNorm(); //normal to panel
            for (c=0,it2 = m_Sails.begin(); it2!=m_Sails.end();++it2)
            {
                wakeline **wlJ = (*it2)->m_wl;
                for(j=0;j<NOE;j++,c++)
                {
                    vi.setPos(0,0,0);
                    if(j>((N-2)*N+N-1)) // aka  NOE - (n+1)  ie the last row.
                    {
                        for(k=0;k< NWLS;k++)//induced velocity from the 2 wakelines abutting to lattice "j" in controlpoint "i"
                        {
                            vi=vi.plus((wlJ[j-(N-1)*N+1][k].vInduced(cp)).prod(-1));
                            vi=vi.plus((wlJ[j-(N-1)*N][k].vInduced(cp)));
                            if (this->mirr ) {
                                vi=vi.plus((wlJ[j-(N-1)*N+1][k].vInducedMirr(cp)).prod(-1));
                                vi=vi.plus((wlJ[j-(N-1)*N][k].vInducedMirr(cp)));
                            }
                        }
                        // qDebug()<<(*it1)->m_name <<r<< (*it2)->m_name<<c<< vi.dot(norm);
                    }

                    B[r][c]=A[r][c]+vi.dot(norm);

                }
            }
            B[r][nv*NOE]=-Uo.dot(norm);    //free stream
            // qDebug()<<(*it1)->m_name <<r <<","<<nv*NOE<<    B[r][nv*NOE]<<"Free-Stream";
        }
    }
    return;
}

#endif

double WernerWorld::interpol(double x1, double x2, double x, double y1, double y2)
{
    double k,m;
    double y=-9999;

    if((fabs(y2-y1))<1e-6)
        y=y1;
    else
    {	if( (fabs(x2-x1))>1e-15)
        {
            k=(y2-y1)/(x2-x1);
            m=y2-k*x2;
            y=k*x+m;
        }
    }
    assert(y!=-9999);
    return y;
}

WernerPoint WernerWorld::interpol(WernerPoint x1, WernerPoint x2, WernerPoint x, WernerPoint y1, WernerPoint y2)
{	int i,j,l;
        double k,m;
            WernerPoint y;
                // peter changed to save the memory leak
                double y1s[3];
                    double y2s[3];
                        double ytemp;
                            double  ys[3];
                                double  x1s[3];
                                    double  x2s[3];
                                        double  xs[3];

                                            //double *y1s=new double[3];
                                            //double *y2s=new double[3];
                                            //double ytemp;
                                            //double *ys=new double[3];
                                            //double *x1s=new double[3];
                                            //double *x2s=new double[3];
                                            //double *xs=new double[3];
                                            y1.getPos(y1s[0],y1s[1],y1s[2]);
                                                y2.getPos(y2s[0],y2s[1],y2s[2]);
                                                    x1.getPos(x1s[0],x1s[1],x1s[2]);
                                                        x2.getPos(x2s[0],x2s[1],x2s[2]);
                                                            x.getPos(xs[0],xs[1],xs[2]);
                                                                for (j=0;j<3;j++) //the three components of a point
                                                                {	l=0;
                                                                    ytemp=0;
                                                                    if((fabs(y2s[j]-y1s[j]))<1e-6) ys[j]=y1s[j]; else
                                                                    {for (i=0;i<3;i++)
                                                                        {	if( (fabs(x2s[i]-x1s[i]))>1e-15)
                                                                            {	k=(y2s[j]-y1s[j])/(x2s[i]-x1s[i]);
                                                                                m=y2s[j]-k*x2s[i];
                                                                                ytemp=k*xs[i]+m;
                                                                                l++;
                                                                            }
                                                                        }
                                                                        ys[j]=(ytemp);
                                                                    }
                                                                }
                                                                    y.setPos(ys[0],ys[1],ys[2]);
                                                                        return y;

}

//------------------Calculate the forces on the wing by TREFFTZ plane analysis------------------
//----------------------------------------------------------------------------------------------
QString WernerSail::trefftz_plane( double *G )
{	
    lattice *la=this->m_la;
    wakeline **wl=this->m_wl;
    int N=this->m_N;
    int NWLS=this->m_NWLS;
    double rho=1.205;		//density of air
    WernerPoint Uo= m_w->Uo;
    double UoL= Uo.length(); //Free stream
    //    double alpha = - a * pi/180.0; // for MSW
    int i,j,k;
    double proj,w,Cl,CDi,P,Q,R,s;
    double Lift=0;
    double Di=0;
    double  *Lsec =new double[N+1];
    double  *Dsec =new double[N+1];
    WernerPoint wDir,cp,dirS,side,vi;
    const WernerPoint *ip;
    CString strOut;

    j=N-1;
    for (i=(N-1)*N+N-1;i>(N-2)*N+N-1;i--) //trailing edge panels
    {	ip=la[i].getIp();
        cp=la[i].getCp();
        side=ip[1].vec(ip[2]); //side of trailing edge panel
        //length of panel edge projected to a plan perpendiqular to the free stream:
        proj=sqrt(pow(side.length(),2)-pow(side.dot(Uo.normalize()),2));

        //Lift
        Lsec[j]=rho*UoL*G[i]*4*pi*proj;
        Lift=Lift+Lsec[j];

        //Find a point on trailing edge, same spanwise position as control pint
        P=ip[1].vec(cp).length();
        Q=ip[2].vec(cp).length();
        R=ip[1].vec(ip[2]).length();
        //distance from leading edge to control points in first row:
        s=sqrt(Q*Q-(Q*Q-P*P+R*R)/(2*R)*(Q*Q-P*P+R*R)/(2*R));
        dirS=(ip[0].vec(ip[1]).plus(ip[3].vec(ip[2]))).prod(0.5).normalize();
        cp=cp.plus(dirS.prod(s));

        /*
 point start1=wl[i+1-(N-1)*N][0].getStart();//obs k-1 for cp
  point start0=wl[i-(N-1)*N][0].getStart();
  cp=(start1.plus(start0)).prod(0.5);
*/

        //induced velocity from wake
        vi.setPos(0,0,0);
        for(k=0;k<NWLS;k++)
        {	for(j=1;j<N;j++)
                vi=vi.plus((wl[j][k].vInduced(cp)).prod(-G[(N-1)*N+j]+G[(N-1)*N+j-1]));
            vi=vi.plus((wl[0][k].vInduced(cp)).prod(-G[(N-1)*N+0]));
            vi=vi.plus((wl[N][k].vInduced(cp)).prod(G[(N-1)*N+N-1]));
        }
        vi=vi.prod(2); //goes to infinity in both direction
        //direction vector for downwash
        wDir=(side.cross(Uo)).normalize();
        //downwash
        w=vi.dot(wDir);

        //Induced drag
        Dsec[j]=-0.5*rho*w*G[i]*4*pi*proj;
        Di=Di+Dsec[j];
        j--;
    }

    //Sum the panel areas
    double area=0;
    for (i=0;i<N*N;i++)  area=area+la[i].getArea();

    Cl=Lift/(0.5*rho*area*UoL*UoL);
    CDi=Di/(0.5*rho*area*UoL*UoL);
    //#ifndef SAILPLUSPLUS_LIBRARY
    strOut=QString("TA Cl::\t%1\n").arg(Cl );

    strOut+=QString("TA Kv::\t%1\n").arg(2*pi*CDi/(Cl*Cl));
    //#endif
    delete[]  Lsec;
    delete[]  Dsec;

    return strOut;
}



