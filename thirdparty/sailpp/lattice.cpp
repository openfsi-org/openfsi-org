// lattice.cpp: implementation of the lattice class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#ifndef SAILPLUSPLUS_LIBRARY
#include "sejl1.h"
#endif

#include "lattice.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

lattice::lattice(WernerPoint a, WernerPoint b, WernerPoint c, WernerPoint d)
{
	ip[0]=a;ip[1]=b;ip[2]=c;ip[3]=d; 
	diag();
	
}

lattice::lattice(WernerPoint a, WernerPoint b, WernerPoint c, WernerPoint d,WernerPoint e,int t)
{
	ip[0]=a;ip[1]=b;ip[2]=c;ip[3]=d;cp=e;type=t;
	
}

lattice::lattice(WernerPoint a, WernerPoint b, WernerPoint c, WernerPoint d, WernerPoint e )
{
	ip[0]=a;ip[1]=b;ip[2]=c;ip[3]=d;cp=e;

}


void lattice::getNabour(int& a, int& b, int& c, int&d)
{
	a=nabour[0];b=nabour[1];c=nabour[2];d=nabour[3];
}





lattice::lattice()
: m_cPu(0)
, m_cPl(0)
{	WernerPoint dummy=WernerPoint(0,0,0);
	ip[0]=dummy;ip[1]=dummy;ip[2]=dummy;ip[3]=dummy;cp=dummy;
}





lattice::~lattice()
{

}
