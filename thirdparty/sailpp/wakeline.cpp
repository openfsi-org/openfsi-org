// wakeline.cpp: implementation of the wakeline class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#ifndef SAILPLUSPLUS_LIBRARY
#include "sejl1.h"
#endif
#include "wakeline.h"
#include "math.h"
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
wakeline::wakeline()
{
	start=WernerPoint(); stop=WernerPoint(); 
}
wakeline::wakeline(WernerPoint a, WernerPoint b, boolean c)
{	//c true-> b=stop, c false-> b=dir
	start=a; type=c;
	if (c) 
	{	stop=b;
		length=sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)+(a.z-b.z)*(a.z-b.z));
		dir=(start.vec(stop)).normalize();
	}
	else dir=b;
	deleted=false;
}
wakeline::~wakeline()
{
}
