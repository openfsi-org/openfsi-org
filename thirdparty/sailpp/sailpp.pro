#-------------------------------------------------
#
# Project created by QtCreator 2012-02-11T19:31:05
#
#-------------------------------------------------

QT       -= gui

TARGET = sailplusplus
TEMPLATE = lib
CONFIG  +=staticlib
include (../../openfsicompilerconfig.pri)


DEFINES += SAILPLUSPLUS_LIBRARY
DEFINES += boolean=bool
DEFINES += CString=QString
#DEFINES += TRY_HEADFOOTWAKE
SOURCES += sailplusplus.cpp \
    WernerWorld.cpp \
    WernerSail.cpp \
    wakeline.cpp \
    point.cpp \
    lattice.cpp

HEADERS += sailplusplus.h\
        sailplusplus_global.h \
    WernerWorld.h \
    WernerSail.h \
    wakeline.h \
    point.h \
    lattice.h \
    calc.h \
    stdafx.h

symbian {
    MMP_RULES += EXPORTUNFROZEN
    TARGET.UID3 = 0xE65D1236
    TARGET.CAPABILITY = 
    TARGET.EPOCALLOWDLLDATA = 1
    addFiles.sources = sailplusplus.dll
    addFiles.path = !:/sys/bin
    DEPLOYMENT += addFiles
}

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}




