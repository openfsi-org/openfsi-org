#pragma once
#include <QString>
#include <vector>


class WernerSail
{
    friend class WernerWorld;
public:

    virtual ~WernerSail(void);
    int toVTK(std::string fname);
    int WakeToVTK(QString fname);
    int GetNPanels() { return this->m_N;}// one-dee

    int SetVVs(std::vector<WernerPoint>vv );  // expect (N+1)^2
    int SetCPs(std::vector<WernerPoint>cps ); // expect (N)^2

    std::vector<double> GetPressures();

protected:
    int acad(QString fname );
    WernerSail(const int n, const int nwls,class WernerWorld*w,const QString & name);
    WernerSail(void);

    std::string joukowski( double *G);//, double *GblobF,
                          // int *NMWLF,double *GblobH,
                          // int *NMWLH,int blob_av);
    WernerPoint leading_edge_Fiddes( double *G );
    QString trefftz_plane(  double *G );
    std::string pressure_summation( double *G);//,
                                  //  double *GblobF, int *NMWLF,
                                  //  double *GblobH, int *NMWLH,int blob_av);
    void wakelineCreate();
    void neighbours();
private:
    bool IsMain() ;// YUCH!!!

   protected:
    std::vector<WernerPoint>m_cps , m_vvs;
    class lattice  *m_la;
    class wakeline **m_wl;
    // // number of panels - chordwise and spanwise
    int m_N;
    // // no of wakeline segments
    int m_NWLS;
    class WernerWorld *m_w;
    QString m_name;
    int wake_side[4];
    int m_IndexInWorldList;

};
