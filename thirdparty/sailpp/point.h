// point
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_POINT_H__FECD7275_8EF1_11D4_B1C9_0060B068485C__INCLUDED_)
#define AFX_POINT_H__FECD7275_8EF1_11D4_B1C9_0060B068485C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "math.h"
//Defines a 3D point or vector
class WernerPoint
{private:
		
public:
	double x,y,z;

        WernerPoint();
        WernerPoint(double ,double ,double );
        virtual ~WernerPoint();
		
	void setPos(double a, double b,double c)
	{	x=a; y=b; z=c;
	}

	void setPosF(float a, float b,float c)
	{	x=a; y=b; z=c;
	}

        void getPos(double&,double&,double&) const;

        double dot(WernerPoint p1)//dot product
	{
		double x1,y1,z1,q;
		p1.getPos(x1,y1,z1);
		q=x1*x+y1*y+z1*z;
		return q;
	}

	
        WernerPoint cross(WernerPoint p1) const//cross product
        {	WernerPoint q;
		double x1,y1,z1,x2,y2,z2;
		p1.getPos(x1,y1,z1);
		x2=y*z1-y1*z; y2=z*x1-x*z1; z2=x*y1-y*x1;
		q.setPos(x2,y2,z2);
		return q;
	}

	//vestor subtraction, point p1 minus THIS point
	//or a vecor from THIS to p1
        WernerPoint vec(const WernerPoint p1) const
        {	WernerPoint q;
		double x1,y1,z1,x2,y2,z2;
		p1.getPos(x1,y1,z1);
		x2=x1-x; y2=y1-y; z2=z1-z;
		q.setPos(x2,y2,z2);
		return q;				
	}

        WernerPoint plus(const WernerPoint p1)//vector addition
        {	WernerPoint q;
		double x1,y1,z1,x2,y2,z2;
		p1.getPos(x1,y1,z1);
		x2=x1+x; y2=y1+y; z2=z1+z;
		q.setPos(x2,y2,z2);
		return q;		
		
	}

        WernerPoint prod(const double a) //THIS vector times a real number
        {	WernerPoint q;
		double x2,y2,z2;
		
		x2=a*x; y2=a*y; z2=a*z;
		q.setPos(x2,y2,z2);
		return q;				
	}

        WernerPoint prod(const WernerPoint p) //THIS vector times another vector, {x*x1, y*y1, z*z1} called in Joukowski
        {	WernerPoint q;
		double x1,y1,z1,x2,y2,z2;
		p.getPos(x1,y1,z1);
		
		x2=x1*x; y2=y1*y; z2=z1*z;
		q.setPos(x2,y2,z2);
		return q;		
		
	}

	
	double length()//length of THIS vector
	{	double l;
		l=sqrt(x*x+y*y+z*z);
		return l;		
	}

        WernerPoint normalize()//normalizes THIS vector
        {	WernerPoint q;
		double l;
		l=sqrt(x*x+y*y+z*z);
		q.setPos(x/l,y/l,z/l);
		return q;
	}

        bool equal(WernerPoint q)	//returns true if q equals THIS
	{	double eps=1e-12,x1,y1,z1;
		q.getPos(x1,y1,z1);
		if((fabs(x1-x)<eps) & (fabs(y1-y)<eps) & (fabs(z1-z)<eps))
			return true;
		else
			return false;
	
	}

	




};

#endif // !defined(AFX_POINT_H__FECD7275_8EF1_11D4_B1C9_0060B068485C__INCLUDED_)
