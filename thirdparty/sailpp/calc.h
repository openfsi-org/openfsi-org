// calc.h: interface for the calc class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CALC_H__E9663691_B169_11D4_B1CE_0060B068485C__INCLUDED_)
#define AFX_CALC_H__E9663691_B169_11D4_B1CE_0060B068485C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "math.h"
#include "point.h"
#include "lattice.h"
#include "wakeline.h"
class calc  
{
private:
	int N,NWLS,NOE,NTEE,TE_side;    
	double tol,h,rho;	
	WernerPoint Uo;
	int i,j,jj,kk,k,loops;
	int n[4]; //nabour
	double g,G_nab,L_Kutta,err,lift;
	double K[4]; //coefficents for average circulation	
	lattice *la;
	wakeline **wl;
	double **B,**A,*G,*V_lower,*V_upper;
	WernerPoint side,Vjump,L_tot,indw,start, stop, po, vel,dir, viw;
	WernerPoint vi; //induced velocity
	WernerPoint ip[4]; //intersecting points
	WernerPoint cp; //control point
WernerPoint *L,*G_av;

public:
	calc();
	virtual ~calc();






};

#endif // !defined(AFX_CALC_H__E9663691_B169_11D4_B1CE_0060B068485C__INCLUDED_)
