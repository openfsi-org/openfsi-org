// wakeline.
//
//////////////////////////////////////////////////////////////////////
#if !defined(AFX_WAKELINE_H__5541E161_9B6D_11D4_B1CC_0060B068485C__INCLUDED_)
#define AFX_WAKELINE_H__5541E161_9B6D_11D4_B1CC_0060B068485C__INCLUDED_
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "point.h"
#include "math.h"
#define pi 3.141592653589793238462643 
class wakeline  
{	bool  type; //true=finite   false=semi-infinite
	
	WernerPoint start,stop,dir;
	double length;
public:
	
	wakeline();
        wakeline(WernerPoint ,WernerPoint,bool  );
	virtual ~wakeline();
        bool  deleted; //=true of wakeline is deleted, else false
	void setStart(WernerPoint a)
	{	
		start=a;
		//adjust stop 
		stop=start.plus(dir.prod(length));
	}
	void setStop(WernerPoint a)
	{
		stop=a;
		//adjust dir 
		dir=(start.vec(stop)).normalize();
	}
	void setDir(WernerPoint a)
	{
		dir=a;
		//adjust stop 
		stop=start.plus(a.prod(length));
	}
	WernerPoint getStart()
	{	
		return start;
	}
	WernerPoint getStop()
	{	
		return stop;
	}

	void Delete()
	{	WernerPoint pZero=WernerPoint(0,0,0);
		start=pZero;
		stop=pZero;
		length=0;
		deleted=true;
	}
	WernerPoint vInduced(WernerPoint p)
	{	// The velocity in point p induced by this wake-line 
		int j;
		double a,b,c,x,y,z,x1,y1,z1,x2,y2,z2,ex,ey,ez,temp;
		double v[3];
		double d[3];
		
		WernerPoint vp;
	if(deleted==false)
	{	p.getPos(x,y,z);
		start.getPos(x1,y1,z1);
		stop.getPos(x2,y2,z2);
		dir.getPos(ex,ey,ez);
		if (p.equal(start) | p.equal(stop)) vp=WernerPoint(); //singular, return (0,0,0)
		else
		{
		a=pow((x-x1),2)+pow((y-y1),2)+pow((z-z1),2);
		
		if (type) //finite line
		{	b=-2*((x2-x1)*(x-x1)+(y2-y1)*(y-y1)+(z2-z1)*(z-z1));
			c=pow((x2-x1),2)+pow((y2-y1),2)+pow((z2-z1),2);
			d[0]=(z2-z1)*(y-y1)-(y2-y1)*(z-z1);
			d[1]=(x2-x1)*(z-z1)-(z2-z1)*(x-x1);
			d[2]=(y2-y1)*(x-x1)-(x2-x1)*(y-y1);
			temp=4*a*c-b*b;
			if (fabs(temp)>1e-10) temp=-2/((temp))*((2*c+b)/sqrt(a+b+c)-b/sqrt(a));
			else temp=0; //(singular)
			for (j=0;j<3;j++)
				v[j]=temp*d[j];
		}
		else  //semi-infinite line
		{	b=-2*((x-x1)*ex+(y-y1)*ey+(z-z1)*ez);
			c=pow(ex,2)+pow(ey,2)+pow(ez,2);
			d[0]=ez*(y-y1)-ey*(z-z1);
			d[1]=ex*(z-z1)-ez*(x-x1);
			d[2]=ey*(x-x1)-ex*(y-y1);
			temp=2*a*sqrt(c)+b*sqrt(a);
			if (fabs(temp)>1e-10) temp=-2/((temp));
			else temp=0; //(singular)
			for (j=0;j<3;j++)
				v[j]=temp*d[j];
		}
		
		vp=WernerPoint(v[0],v[1],v[2]);
		}
	}
	else
		vp.setPos(0,0,0);
		return vp;
		
	}
	WernerPoint vInducedMirr(WernerPoint p)
	{	// The velocity in point p induced by the mirror of this wake-line 
		// The wake line is mirrored in the x-z-plane
		int j;
		double a,b,c,x,y,z,x1,y1,z1,x2,y2,z2,ex,ey,ez,temp;
		double v[3];
		double d[3];
		WernerPoint vp;
	
	if(deleted==false)
	{	p.getPos(x,y,z);
		start.getPos(x1,y1,z1);
		stop.getPos(x2,y2,z2);
		dir.getPos(ex,ey,ez);
		y1=-y1; y2=-y2; ey=-ey;
		if (p.equal(start) | p.equal(stop)) vp=WernerPoint(); //singular, return (0,0,0)
		else
		{
		a=pow((x-x1),2)+pow((y-y1),2)+pow((z-z1),2);
		
		if (type) //finite line
		{	b=-2*((x2-x1)*(x-x1)+(y2-y1)*(y-y1)+(z2-z1)*(z-z1));
			c=pow((x2-x1),2)+pow((y2-y1),2)+pow((z2-z1),2);
			d[0]=(z2-z1)*(y-y1)-(y2-y1)*(z-z1);
			d[1]=(x2-x1)*(z-z1)-(z2-z1)*(x-x1);
			d[2]=(y2-y1)*(x-x1)-(x2-x1)*(y-y1);
			temp=4*a*c-b*b;
			if (fabs(temp)>1e-10) temp=2/((temp))*((2*c+b)/sqrt(a+b+c)-b/sqrt(a));
			else temp=0; //(singular)
			for (j=0;j<3;j++)
				v[j]=temp*d[j];
		}
		else  //semi-infinite line
		{	b=-2*((x-x1)*ex+(y-y1)*ey+(z-z1)*ez);
			c=pow(ex,2)+pow(ey,2)+pow(ez,2);
			d[0]=ez*(y-y1)-ey*(z-z1);
			d[1]=ex*(z-z1)-ez*(x-x1);
			d[2]=ey*(x-x1)-ex*(y-y1);
			temp=2*a*sqrt(c)+b*sqrt(a);
			if (fabs(temp)>1e-10) temp=2/((temp));
			else temp=0; //(singular)
			for (j=0;j<3;j++)
				v[j]=temp*d[j];
		}
		
		vp=WernerPoint(v[0],v[1],v[2]);
		}
	}
	else
		vp.setPos(0,0,0);
		return vp;
		
	}
};
#endif // !defined(AFX_WAKELINE_H__5541E161_9B6D_11D4_B1CC_0060B068485C__INCLUDED_)
