// point.cpp: implementation of the point class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#ifndef SAILPLUSPLUS_LIBRARY
#include "sejl1.h"
#endif
#include "point.h"
#include "math.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

WernerPoint::WernerPoint(double a,double b,double c)
{
x=a; y=b; z=c;
}

WernerPoint::WernerPoint()
{
x=0;y=0;z=0;
}

void WernerPoint::getPos(double& a, double& b, double& c) const
{
a=x;b=y;c=z;
}

WernerPoint::~WernerPoint()
{

}
