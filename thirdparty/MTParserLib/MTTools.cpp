#include "MTTools.h"
#include <math.h>
#include <float.h>
#ifdef WIN32
#define isnan _isnan
#endif
#ifdef linux
#include "mtlinux.h"
#include <wctype.h>
#include <assert.h>
#else
#include <iostream>

#include <windows.h>
#include <string.h>  // for wcslen
#include <wchar.h> //ditto
#endif

#include <iostream>
using namespace std;
#include <stdlib.h>		// for the MTLTOSTR macro that becomes _ltoa 

bool MTTools::findSubStr(const MTSTRING &str, const MTSTRING &subStr, unsigned int &pos)
{
    unsigned int strLength = str.size();
    unsigned int subStrLength = subStr.size();

    if( subStrLength > strLength )
    {
        return false;
    }

    unsigned int end = strLength - subStrLength;

    for( unsigned int t=0; t <= end; t++ )
    {
        if( isStrBegin(str.c_str()+t, subStr.c_str(), strLength-t, subStrLength ) )
        {
            pos = t;
            return true;
        }

    }

    return false;
}

void MTTools::replaceSubStr(MTSTRING &str, const MTSTRING &subStr, const MTSTRING &newSubStr)
{
    unsigned int pos;
    if( findSubStr(str, subStr, pos) )
    {
        str.replace(pos, subStr.size(), newSubStr);
    }
}

bool MTTools::isStrBegin(const MTCHAR *str, const MTCHAR *beginStr, unsigned int strLength, unsigned int beginStrLength)
{	
    if( beginStrLength > strLength )
    {
        return false;	// cannot find a substring that is lengthier than the string itself!
    }

    for(unsigned int pos=0; pos < beginStrLength; pos++ )
    {
        if( str[pos] != beginStr[pos] )
        {
            return false;	// no match...!
        }
    }

    return true;	// match!
}

void MTTools::removeSpaces(const MTSTRING &str, std::vector<unsigned int> &originalPos, MTSTRING &newStr)
{
    unsigned int length = str.size();
    newStr = _T("");
    newStr.reserve(length);
    originalPos.clear();
    originalPos.reserve(length);

    for( unsigned int t=0; t<length; t++ )
    {
        // add this character only if it is not a space character
        if( str[t] != ' ' )
        {
            newStr += str[t];
            originalPos.push_back(t);
        }
    }
}

bool MTTools::findCharPos(const MTSTRING &str, unsigned int beginPos, const MTCHAR &c, unsigned int &pos)
{
    unsigned int length = str.size();

    for( unsigned int t=beginPos; t<length; t++ )
    {
        if( str[t] == c )
        {
            pos = t;
            return true;	// character found
        }
    }

    return false;		// character not found
}

void MTTools::parseString(const MTSTRING &str, const MTCHAR &c, std::vector<MTSTRING> &tokens)
{
    unsigned int length = str.size();
    unsigned int pos = 0;
    unsigned int separatorPos;

    tokens.clear();

    while( pos < length )
    {
        if( !findCharPos(str, pos, c, separatorPos) )
        {
            separatorPos = length;	// end of the string
        }

        if( pos < separatorPos )
        {
            tokens.push_back(str.substr(pos, separatorPos-pos));
        }

        pos = separatorPos+1;
    }
}

// check if this word contains only numerical characters
bool MTTools::isOnlyNum(const MTSTRING &word, const MTCHAR &decimalPoint)
{	

    // peters variation, accepts leading minus and exponential notation.
    bool retval;
    const wchar_t *lp;
    wchar_t *end;
    double x;
    int nread,nleft;
    lp = word.c_str();
    x = wcstod( lp, &end);
    if(isnan(x))        // because mtparser uses the word 'NaN'
        return false;
    nread = end-lp; if(!nread) return false;
    nleft = wcslen(end);
    retval=  (nread && !nleft); // we've read all the string
    return retval;
    // end peters

    unsigned int length = word.size();
    bool decimal = false;	// indicate if a decimal point character has been encountered
    // at most it can only have one decimal point character
    // else, this is not a numerical only string

    for( unsigned int t=0; t<length; t++ )
    {
        // if this is not a numeric character and not the decimal point character
        if( !MTISDIGIT(word[t]) && word[t] != decimalPoint )
        {
            if(retval) {
                if(!_isnan(x))
                    wcout <<L"peter disagree(1) on IsOnlyNum. text is "<< word.c_str()<<" strtod gives "<<x<<endl;
            }
            return false;	// alpha or special character
        }

        if( word[t] == decimalPoint )
        {
            if( decimal )
            {
                if(retval)
                    wcout <<L"peter disagree(2) on IsOnlyNum. text is "<< word.c_str()<<" strtod gives "<<x<<endl;
                return false;	// a second decimal point character!
            }
            else
            {
                decimal = true;
            }

        }


    }
    if(!retval)
        wcout<<L"peter disagree(3) on IsOnlyNum "<< word.c_str()<<" MTP says yes, strtod says no"<<endl;
    return true;
}


double MTTools::generateNaN()
{
    unsigned long nan[2]={0xffffffff, 0x7fffffff};	// code representing a NaN
    return *( double* )nan;
}

#ifndef linux
bool MTTools::registerCOMObject(const MTCHAR *file)
{
    HINSTANCE hLib = LoadLibrary(file);
    HRESULT (*lpDllEntryPoint)();

    if (hLib < (HINSTANCE)HINSTANCE_ERROR)
    {
        return 0;
    }

    bool success = true;
    lpDllEntryPoint = (HRESULT (*)())GetProcAddress(hLib, "DllRegisterServer");
    if (lpDllEntryPoint != NULL)
    {
        HRESULT hr;
        hr = (*lpDllEntryPoint)();
        if (FAILED(hr))
        {
            success = false;
        }

    }
    else
    {
        success = false;
    }
    
    FreeLibrary(hLib);
    return success;

}


bool MTTools::unregisterCOMObject(const MTCHAR *file)
{
    HINSTANCE hLib = LoadLibrary(file);
    HRESULT (*lpDllEntryPoint)();

    if (hLib < (HINSTANCE)HINSTANCE_ERROR)
    {
        return 0;
    }


    bool success = 1;
    lpDllEntryPoint = (HRESULT (*)())GetProcAddress(hLib, "DllUnregisterServer");
    if (lpDllEntryPoint != NULL)
    {
        HRESULT hr;
        hr = (*lpDllEntryPoint)();
        if (FAILED(hr))
        {
            success = false;
        }
    }
    else
    {
        success = false;
    }

    FreeLibrary(hLib);
    return success;

}

bool MTTools::registerTypeLib(const MTCHAR *file)
{
    ITypeLib *pTypeLib = NULL;

    if( FAILED(LoadTypeLib(MTSTRINGTOUNICODE(file),&pTypeLib) ))
    {
        return false;
    }

    bool ret = true;
    if( FAILED(RegisterTypeLib(pTypeLib, (wchar_t*)MTSTRINGTOUNICODE(file), NULL) ))
    {
        ret = false;
    }

    pTypeLib->Release();
    pTypeLib = NULL;

    return ret;
}

bool MTTools::unregisterTypeLib(const MTCHAR *file)
{
    ITypeLib *pTypeLib = NULL;

    if( FAILED(LoadTypeLib(MTSTRINGTOUNICODE(file),&pTypeLib) ))
    {
        return false;
    }

    TLIBATTR *pLibAttr;
    pTypeLib->GetLibAttr(&pLibAttr);

    bool ret = true;
    if( FAILED(UnRegisterTypeLib(pLibAttr->guid, pLibAttr->wMajorVerNum, pLibAttr->wMinorVerNum, pLibAttr->lcid, pLibAttr->syskind) ))
    {
        ret = false;
    }

    pTypeLib->Release();
    pTypeLib = NULL;

    return ret;
}
#endif //linux

MTSTRING MTTools::doubleToS(double number, const MTCHAR &decimalPoint, unsigned int digits)
{
    // Custom double to string conversion since there is no
    // Unicode function to do this...

    MTSTRING str = longToS((int)number);
    str += decimalPoint;

    unsigned int t;
    double fracPart = fabs(number) - fabs((double)((int)number));
    for( t=0; t<digits-1; t++ )
    {
        fracPart *= 10.0;
        if( (int)fracPart > 0 )
        {
            break;
        }
        str += _T("0");
    }

    fracPart *= pow(10.0, (int)(digits-t));
    str += longToS((int)fracPart);

    return str;
}

MTSTRING MTTools::longToS(long val)
{
    MTCHAR str[200];
#ifdef linux
  //  printf("MTTools::longToS %ld ",val);
    swprintf(str,(size_t) 20, L"%ld", val);
   // printf("gives %S \n",str);
    return str;
#else

    return MTLTOSTR(val, str, 10);
#endif

}

