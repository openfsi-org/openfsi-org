#include "StdAfx.h"
#include <iostream>
using namespace std;

 
#ifndef RXSTRING // horrible. this is for netgen mtparser
#include <string>
#define RXSTRING  std::wstring
 
#define SAIL  void
#endif

#include "RXExpression.h"
#include "MTParserCompiler.h"
#include "RXStrFuncI.h"


RXStrFuncI::RXStrFuncI(void) :
m_l(0),
m_pCompilerState(0)
{
 assert(" dont use the default RXStrFuncI::RXStrFuncI constructor"==0);
}

RXStrFuncI::RXStrFuncI(class RXSail *const p, class RXObject *const obj) :
m_l(p),
m_pCompilerState(0),
m_rxo(obj)
{
//m_pCompilerState= new MTRXStrCompilerState();  
}

RXStrFuncI::~RXStrFuncI(void)
{
	if(m_pCompilerState )  delete  m_pCompilerState;
}

class MTCompilerStateI* RXStrFuncI::getCompilerState(){ 
	return m_pCompilerState;  
}	

RXDBFnc::RXDBFnc() 
{

}


RXDBFnc::RXDBFnc(class RXSail *p, class RXObject *const o):
RXStrFuncI(p,o) 
{
}

RXDBFnc::~RXDBFnc(void)
{
}

MTDOUBLE RXDBFnc::evaluate(const MTSTRING*s)
{
      wcout << L"please go to the database and evaluate <";
      wcout <<    s   ;
      wcout << L"> "  << endl;
	return 777.0;
}








void MTRXStrCompilerState::onOpenBracket() {
cout<< "I dont think we've been here"<<endl;
	MTCompilerDefState::onOpenBracket();
	this->m_pRegistrar->setVarFactory(new MyStringVarFactory());
}

void MTRXStrCompilerState::onCloseBracket()
{

	this->m_pRegistrar->setVarFactory(new MyStringVarFactory());
	MTCompilerDefState::onCloseBracket();
	 	this->m_pRegistrar->setVarFactory((MTVariableFactoryI *)-1); // YUCH

//setVarFactory(previous);
}

RXMTStringVariable::RXMTStringVariable():
m_ent(0),
m_exp(0) 
{
 
}

RXMTStringVariable::~RXMTStringVariable(){
	m_exp=0;
}

const MTCHAR* RXMTStringVariable::getSymbol()
{ 
		return (m_MTIVname.c_str());
}    
 MTDOUBLE RXMTStringVariable::evaluate(unsigned int nbArgs, const MTDOUBLE *pArg)
 {
		  return m_exp->evaluate (); 
  }

  MTSTRING RXMTStringVariable::evaluate()
  {
	return this->GetMTIVName();
  }
MTVariableI* RXMTStringVariable::spawn() throw(MTParserException)
  { 
	  return new RXMTStringVariable(); 
  }



