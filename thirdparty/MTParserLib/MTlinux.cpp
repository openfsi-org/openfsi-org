#include "mtlinux.h"

#ifdef linux
#include <math.h>
#include <malloc.h>
#include <cstdlib>
#include "MTUnicodeANSIDefs.h"
#ifdef UNICODE
 
	typedef wchar_t  *BSTR;
	#define lstrcmp wcscmp

	#define lstrlen wcslen
	#define lstrcpy  wcscpy
	#define _TEOF WEOF
	#define __T(x)	L ## x

	#define _T(x)	__T(x)
	#define _TEXT(x) __T(x)	 
	typedef unsigned long DWORD;
	typedef DWORD  LCID;

	#define SysFreeString(a) free(a)
	BSTR SysAllocStringByteLen(const char* string,const unsigned int c)
           {
            size_t size = c + 1;
            wchar_t *buf =(wchar_t *) malloc (size * sizeof (wchar_t));
          
            size = mbstowcs (buf, string, size);
            if (size == (size_t) -1)
              return NULL;
            buf = (wchar_t *)realloc (buf, (size + 1) * sizeof (wchar_t));
            return buf;
          }
	int CharUpper( MTCHAR *s ){
		MTCHAR* c; int rc=0;
		c = s;
		while (*c) {
			*c = toupper(*c);
			c++; rc++;
		}
		return rc;
	}

#else
	typedef char  *BSTR;
	#define lstrcmp strcmp
	#define lstrlen wstrlen
	#define lstrcpy strcpy
#endif // char
#define __max(a,b) (((a) > (b))  ? (a)  : (b))
#define __min(a,b) (((a) < (b))  ? (a)  : (b))

#define _isnan  isnan
#define _finite(x) ( !isnan(x) && !isinf(x) )
#define LOCALE_SDECIMAL   0x0000000E
#endif //linux

 

