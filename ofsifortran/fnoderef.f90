module fnoderef_f
use ftypes_f
! this module deals with the FNODEREF_P (for persistent) type
! the faster FnodeRef type is used in elements
! and it relies on the Hoop.. functions to  keep current after reallocation

implicit none
contains

function decodeNodeRef( nref) result (n)
    use ftypes_f
    use saillist_f
    type(fnoderef_p),intent(in) ::nref
    type(fnode), pointer ::n

    n=> saillist(nref%m_fnr_Sli  )%nodes%xlist(nref% m_fnrN) 
end function decodeNodeRef       
        
function decodeDofoRef( dr) result (d)
    use ftypes_f
    type(doforef) ,intent(in) ::dr  ! jul 4 was pointer
#ifdef NO_DOFOCLASS    
    type(dofo), pointer ::d ! or class
#else
    class (dofo), pointer ::d 
#endif
    d=>dr%d
end function decodeDofoRef
 
 
function UnhookDofoAndNode(node,d) result(nfound)
    use ftypes_f
    use removeElement_f
    implicit none
    type(fnode) ,pointer ::node  
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d,d2 ! or class
#else
           class (dofo), pointer ::d,d2 ! or type
#endif

    integer :: j,k,nfound ,err,nd
    logical :: stillGoing, yes,bad
    stillGoing=.true.
    nfound=0  ! it is WRONG to rely on dofono because we can't be sure that 
! find 'node' in d%nn and remove that member of d%nn
!d1tto. find 'd' in node%m_dofos and remove that member. 
sloop : do while (stillgoing)
jloop:do j=1,ubound(d%m_Fnodes,1)
         node=>decodeNodeRef( d%m_Fnodes(j))  
kloop:  do k=1,ubound(node%m_pdofo,1)
            d2=>decodeDofoRef(node%m_pdofo(k))  
            if((d%m_dofon.eq.d2%m_dofon) .and. (d%m_dofosli.eq.d2%m_dofosli) ) then
                    ! remove element k from   node%m_ pdofo  
                    ! remove element j from   d%nn    
                    !  decrement j  and break out of  k loop 
                    err = remove(node%m_pdofo,k); node%masterdofoNo =-1
                    err = remove(d%m_Fnodes,j)     
                    nfound=nfound + 1
                    stillgoing= .true.
                exit jloop  
             endif  
    enddo    kloop


    enddo jloop
    stillGoing=.false.
    exit sloop
    enddo sloop

    yes = associated(node%m_pdofo) 
    if(yes) then
    	if(ubound(node%m_pdofo,1)<1) yes=.false.
    endif
    node%m_isunderdofocontrol = yes   
    node%masterdofoNo= -1;   node%m_ElementCount =  node%m_ElementCount -1
    if(.not. yes) return
    
    nd = ubound( node%m_pdofo,1)
    bad=.true.
dk: do k= nd,1,-1
        if(.not. associated(node%m_pdofo(k)%d%child)) then
            bad=.false.
            node%masterdofoNo=k
            exit dk
        endif
        bad=.true.    
    enddo dk
    if(bad) then
        k=0; k=1/k
    endif
end function UnhookDofoAndNode

function hookDofoToNode(node,d,rowno,flag) result(kkk)  ! only called by Complete_xxx_dofo
    use ftypes_f
    use dofoprototypes_f
    USE realloc_f
    implicit none
    type(fnode),target ::node  !,pointer

#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif
    integer, intent(in) ::rowno  ! a multiple of 3. It is the row in the dofo J corresponding to the node's X
                                 ! it is placed in the dofo's fnoderef and in the node's doforef.
    integer, intent(in) ::flag
    logical bad
    integer ::i,kkk,k,nd
    integer err; err=0; kkk=0

 !    "this does a symmetric append of the dofo to the nodes dofolist and vice verse"
 !    "dont forget to unhook the node and dofo when we delete either"

ifa: if( associated(d%m_Fnodes)) then
        do i=1,ubound(d%m_Fnodes,1)
            if(d%m_Fnodes(i)%m_fnrN ==node%nn .and.d%m_Fnodes(i)%m_fnr_sli ==node%m_sli ) then
                write(outputunit,*) ' node ', node%NN, ' already  with this dofo at row ',d%m_Fnodes(i)%m_fnrRow
                kkk=1
                return 
            endif
        enddo
    endif ifa
    i=0; if(associated(d%m_Fnodes)) i = ubound(d%m_Fnodes,1)
    call reallocate(d%m_Fnodes,i+1,err)
    d%m_Fnodes(i+1) = createfnoderef(node,rowno)
    
    i=0; if( IsUnderDOFOControl(node)) i = ubound(node%m_pdofo,1)
    call reallocate(node%m_pdofo,i+1,err)
    
    node%m_pdofo(i+1) = createdoforef(d,rowno,flag)
    node%m_isunderdofocontrol=.true.
    node%masterdofoNo =-1 ! set masterdofoNo  to the last dofo without children  
    node%m_ElementCount =  node%m_ElementCount+1
    nd = ubound( node%m_pdofo,1)
    bad=.true.; node%masterdofoNo=-1
dk: do k= nd,1,-1
        if(.not. associated(node%m_pdofo(k)%d%child)) then
            bad=.false.
            node%masterdofoNo=k           
            exit dk
        endif
        bad=.true.    
    enddo dk
    if(bad) then
        k=0; k=1/k
    endif
end  function hookDofoToNode

function createfnoderef(p_node, row) result(r)
    use ftypes_f
    use saillist_f
    implicit none
    type(fnode),  intent(in) :: p_node
    integer, intent(in):: row
    type(fnoderef_p) ::r
    r%m_fnrn    = p_node%nn
    r%m_fnr_Sli = p_node%m_sli
    r%m_fnrRow  = row
 end function createfnoderef
 
 
function createdoforef(d,rowno,flag) result(r)
    use ftypes_f
    implicit none
#ifdef NO_DOFOCLASS
           type (dofo),pointer, intent(in) ::d  ! or class
#else
           class (dofo),pointer, intent(in) ::d ! or type
#endif

    integer, intent(in) :: rowno,flag
    type(doforef) ::r
     r%d=>d
     r%m_rowno= rowno
     r%m_dflg = flag
end function createdoforef

subroutine PrintableNodeName(theNode,rv)
    use nodelist_f
    use cfromf_f
    use parse_f
    use saillist_f
    type(fnode),   intent(in) ::theNode
    character(len=*),intent(out) :: rv
    character(len=128) :: buf
    integer:: j,k
    type(sail), pointer :: Thesail
    type(fnode), pointer  ::nn

    k= issixnode(thenode)
    if(0 /= thenode%m_RXFEsiteptr .and. .not. ( k<0) ) then
        j=fc_getSiteName( thenode%m_RXFEsiteptr,rv )  ; call denull(rv)
    else
     rv = ' '
    endif

    if(k <0)   then ! the trans-node is -k
        Thesail=>saillist(thenode%m_sli)
        write(rv,'("rotOf_",i6)') -k
        nn=> Thesail%nodes%xlist(-k)
        if(0 /= nn%m_RXFEsiteptr  ) then
        j=fc_getSiteName( nn%m_RXFEsiteptr,rv )  ; call denull(rv)
    else
        rv = ' '
    endif
        rv = "rot_"//rv
    endif
    if (thenode%m_sli<10 .and. thenode%nn < 1000 ) then
         write(buf,"('{',i1,',',i3,'},'   )",iostat=k) thenode%m_sli,thenode%nn
    else     if (thenode%m_sli<10 .and. thenode%nn < 10000 ) then
         write(buf,"('{',i1,',',i4,'},'   )",iostat=k) thenode%m_sli,thenode%nn
    else
        write(buf,"('{',i3,',',i6,'},'   )",iostat=k) thenode%m_sli,thenode%nn
    endif
    if(k .ne.0 )  write(buf,*,iostat=k)'{', thenode%m_sli,",",thenode%nn,"}"

    rv = trim(buf) //trim(rv)
end subroutine PrintableNodeName

function PrintableFlags( a ) result(s)
use ftypes_f
implicit none

type(fnoderef_p), intent(in)  ::a

character*8 :: s
s=""

if (BTEST(a%m_fnrFlags,DOFO_FLAG_NL    ) ) s(1:2)='NL'
if (BTEST(a%m_fnrFlags,DOFO_FLAG_SLAVE ) ) s(3:4)='SV'
if (BTEST(a%m_fnrFlags,DOFO_FLAG_SHARED) ) s(5:6)='SH'
end function PrintableFlags

function printfnodereflist(s,a,u,depth) result(i)
    use ftypes_f
    use saillist_f
    use cfromf_f  
    use parse_f  
    use nodelist_f
    use dofoprototypes_f
    implicit none
    character(len=*),intent(in) ::s
    type(fnoderef_p), intent(in), pointer,dimension(:) ::a 
    type(fnode), pointer ::theNode
    integer, intent(in) ::u
    integer, intent(in),optional ::depth
    integer i,k,j
    character*256 :: buf
    type(sail), pointer :: Thesail
    logical :: pd
    pd=Present(depth)

        if (associated(a )) then  
          !  if(pd) call PrintDepth(u,depth);
            write(u,'(7x,a4,a8,a8,a8,2x,a)',iostat=i) trim(s) ,'sli    ','    N ','  row ', 'flags  [nodename])'
            do i=1,ubound(a,1)
               write(u,'(10x,i8,i8,i8,2x,a8,\)') a(i)%m_fnr_sli, a(i)%m_fnrn, a(i)%m_fnrRow , PrintableFlags(a(i))
                thenode=>decodenoderef(a(i))
                 k= issixnode(thenode)
                if(0 /= thenode%m_RXFEsiteptr .and. .not. ( k<0) ) then
                  j=fc_getSiteName( thenode%m_RXFEsiteptr,buf )  ; call denull(buf)
                else
                   buf = ' '
                endif

                if(k <0)   then ! the trans-node is -k
                    Thesail=>saillist(thenode%m_sli)
                    write(buf,'("rotOf_",i6)') -k
                   thenode=> Thesail%nodes%xlist(-k)
                   if(0 /= thenode%m_RXFEsiteptr  ) then
                     j=fc_getSiteName( thenode%m_RXFEsiteptr,buf )  ; call denull(buf)
                   else
                      buf = ' '
                   endif
                   buf = "rot_"//buf
                endif
                write(u,'(2x,a,\)') trim(buf)
!end new
                write(u,*)
            enddo
        endif
end function printfnodereflist
function printdoforeflist(s,a,u) result(i)
    use ftypes_f
    use fglobals_f
    implicit none
    character(len=*),intent(in) ::s
    type(doforef), intent(in), pointer,dimension(:) ::a 
    integer, intent(in) ::u
    integer i
     i =0
        if (associated(a )) then  
              write(u,'(8x,a)') trim(s)  
            do i=1,ubound(a,1)
                if(associated(a(i)%d)) then
                 write(u,*)'        D is associated ', loc(a(i)%d), ' rowno = ', a(i)%m_rowno
                endif
            enddo
        endif
end function printdoforeflist

end module fnoderef_f
