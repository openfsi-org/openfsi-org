MODULE stringList_f
IMPLICIT NONE
	integer  STRINGLISTBLOCKSIZE
	parameter (STRINGLISTBLOCKSIZE = 100 )! BLOCK  for janetwe used  20000 I think odd numberss give a problem else 5
	integer, parameter :: PCS_LENGTHFROMFILE = 1, PCS_NOTRIM=2
	logical :: somedeleted = .false.
	
	type ::segband
        real(kind=8) :: y, xprop1,xprop2
        integer i
    end   type segband
CONTAINS  
#ifdef NEVER
function dbs(a,i) result(k)
	use basictypes_f
	use saillist_f
	IMPLICIT NONE
	integer, intent(in) :: i
	character*(*) a
 
	TYPE (stringlist), pointer 	:: strings
	type (string) , pointer		:: this

	integer ::  k,sli, iret, s, c

	iret=0
	k=1; sli = 1
		!write(outputunit,*)
		!write(outputunit,*)'debug Func ',a ,i, 'sli=', sli,' name: ',trim(saillist(sli)%sailname)


		if(.not. is_active(sli)) return
		c = 0
		strings=>saillist(sli)%strings
		if(.not. associated(strings) )return
		write(outputunit,*)' model ',sli,' has ', strings%count ,' strings'
		if(strings%count <=0) return
		if(.not. associated(strings%list)) then
				write(outputunit,*) ' stringlist not associated '

		endif
			write(outputunit,*) ' string list assoc OK'
		 s = 1

			this=>strings%list(s)%o
			write(outputunit,*) ' string ',s ,'name=', trim(this%text) 
			if(.not. associated(strings%list(s)%o) ) then
				write(outputunit,*) ' list(',s,') not associated '

			endif
			if(.not. strings%list(s)%o%used )return

			c=c+1 
			write(outputunit,*) 'NE = ', strings%list(s)%o%d%Ne  
			write(outputunit,*) ' associated(se) = ', associated( strings%list(s)%o%se)
			write(outputunit,*)  strings%list(s)%o%se 
			write(outputunit,*) ' survived'; k=0			
end function dbs
#endif
!///////////////////////////////////////////////////////////
!
! Hook_String_Memory2 allocates the arrays for se, rev and tq, hooks up D, and passes back ptrs to these three arrays. 

! extern "C" int cf_Hook_String_Memory2(const int sli, const int n, void *d,int ne,
!									  int** f_septr ,int**f_revptr, double** f_tqptr,
!									  int *tl,char *text);
	!
!									  
	
integer(c_int) function Hook_String_Memory2(sli,n, p_d,ndim,septr,revptr,tqptr ,p_textlength,pp_text)   bind(C,name=  "cf_Hook_String_Memory2" )
USE, INTRINSIC :: ISO_C_BINDING
use ftypes_f
use saillist_f
implicit none
    integer(C_INT), intent(in),value :: sli,n,ndim
! parameters. All pointers to memory in the caller.
    type (mystringdata),target::p_d
    integer(c_int), intent(in) :: p_textlength
 
        CHARACTER(len=1),dimension(p_textlength ) :: pp_text
    type(c_PTR), intent(out) :: septr,revptr,tqptr 	
	integer ::k
	
   type(string), pointer  ::p 
    character, parameter :: SPACE = ' '
        CHARACTER(len=p_textlength ) :: p_text
        p_text=TRANSFER(pp_text,p_text)
   p=>saillist(sli)%Strings%list(n)%o		

    if(ndim .lt.1) write(outputunit,*) 'ShOrt string ',trim(p_text),' : n edges= ',ndim
        p%d=> p_d
        allocate(p%se(max(ndim,0)  )); p%se=-99
        allocate(p%rev(max(ndim,0) ));
        allocate(p%tq_s(max(ndim,0) )); p%tq_s=-999.
        septr =c_loc(p%se ); revptr=c_loc(p%rev ); tqptr =c_loc(p%tq_s );
        p%m_text= trim(p_text) !  p%text is a 256 buffer
        do k=1,256
            if(ichar(p%m_text(k:k)) .eq. 0) then
               ! write(outputunit,*) ' string name has(had) zeroes at',k,'<', p%m_text(:k),'>'
                p%m_text(k:k) = ' '  
                if(  p%m_text(k:k) .ne. char(32)) then 
                        write(outputunit,*) ' corruption in F character constant  <',p%m_text(k:k) ,'> , is', ichar( p%m_text(k:k))
		                p%m_text(k:k) =char(32) ! bizarrely when this was ' ' it was getting corrupted
		        endif
		        p%m_text(k:256) =SPACE ! bizarrely when this was ' ' it was getting corrupted		        
            endif
        enddo


    Hook_String_Memory2=1
end function Hook_String_Memory2
	
SUBROUTINE RemoveFortranString (sn, NN,err) 
USE realloc_f
use saillist_f 
!use fspline_f
use ftypes_f
use connect_intrf_f
IMPLICIT NONE
	INTEGER , intent(in) :: sn,nn
	INTEGER , intent(out) :: err
	TYPE ( stringlist) ,POINTER :: strings
	type(string), pointer ::this
	TYPE (Connect), POINTER :: c
	integer :: i,istat=0
	err = 16

	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return
	strings=>saillist(sn)%strings
!
!   What it does.
!	unsets the USED flag. realloc if  it can
!
! USUAL Conditions

!   NN =  count  >> decrement count
!    if then count < ubound-block, reallocate
!

! Unusual conditions
!   sailno out of range  >> err 16
!   stringlist not associated  >>err 8
! NN < lbound or > Ubound  >>err 8
!
	err=8
	if(.not. ASSOCIATED(strings%list)) return
	if(NN > UBOUND(strings%list,1)) return
	if(NN  <LBOUND(strings%list,1)) return
	err=0
	this=>strings%list(NN)%o
	if(.not. strings%list(NN)%o%used ) return
     	!write(outputunit,*) 'RemoveFortranString', sn, NN
		!strings%list(NN)%o%d=>NULL()

		strings%list(NN)%o%Nn=NN

 		if(associated(strings%list(NN)%o%se) ) then
 			DEALLOCATE(strings%list(NN)%o%se,stat=istat) !WRONG these belong to C
		endif
		if(associated(strings%list(NN)%o%rev)) then
		 	DEALLOCATE(strings%list(NN)%o%rev,stat=istat)
		endif
		if(associated(strings%list(NN)%o%tq_s)) then
		 	DEALLOCATE(strings%list(NN)%o%tq_s,stat=istat)
		endif
		 NULLIFY(strings%list(NN)%o%se)   
		 !NULLIFY(strings%list(NN)%o%rev)
		 NULLIFY(strings%list(NN)%o%tq_s)
		 if(associated(strings%list(NN)%o%m_fspline)) then
                     call strings%list(NN)%o%m_fspline%ClearFspline( )  ! this will leave a set of dofos pointing at it.
		     strings%list(NN)%o%m_fspline=>Null()
		 endif

		strings%list(NN)%o%used = .FALSE.
		somedeleted=.true.
		if(associated(this%m_Connects) ) then
		    do i=1, ubound(this%m_Connects,1)
                if(.not. associated( this%m_connects(i)%o )) cycle
                c=>this%m_connects(i)%o
                call UnResolve_One_Connect(this%m_connects(i)%o)
			enddo
		endif
        deallocate(this%m_Connects  ,stat=istat)
	if(NN == strings%count) THEN
		 strings%count =strings%count-1
! was < until 19 may 2003
		if( strings%count+strings%SLblock <= ubound(strings%list,1) ) THEN
			CALL reallocate(strings%list, ubound(strings%list,1) - strings%SLblock, err)
			strings%SLblock = max(strings%SLblock/2,2)
		ENDIF
	ENDIF

END SUBROUTINE RemoveFortranString

function ReserveStringSpace(sli, n) result(ok) bind(c)
	USE realloc_f
	USE saillist_f  
	IMPLICIT NONE
	integer, intent(in)  :: sli,n
	INTEGER  ::  ok
	integer :: istat 
	TYPE ( stringlist) ,POINTER :: sList

	ok = 0
	slist=>saillist(sli)%strings
ifna: if(.not. ASSOCIATED(sList%list)) THEN
		sList%SLBlock= STRINGLISTBLOCKSIZE
		call reALLOCATE(sList%list, n, istat)
        somedeleted=.false.
		if(istat ==0) THEN
			ok = 1
		else
			return
		endif
	else
		write(outputunit,*) ' strings already started'
    endif ifna
   
end function ReserveStringSpace

INTEGER FUNCTION   NextFreeString(sn,err)
	USE realloc_f
	USE saillist_f  
	IMPLICIT NONE
	integer, intent(in)   :: sn
	INTEGER,INTENT (out)  ::  err
	integer :: i,k, istat 
	TYPE ( stringlist) ,POINTER :: sList
	!TYPE ( string) ,POINTER :: t
  	NextFreeString = 0
	err = 0
	slist=>saillist(sn)%strings
ifna: if(.not. ASSOCIATED(sList%list)) THEN
		sList%SLBlock= STRINGLISTBLOCKSIZE
		call reALLOCATE(sList%list, sList%SLblock, istat)
        somedeleted=.false.
		if(istat /=0) THEN
			err = 1
			return
		endif

#ifdef NEVER ! debug
		i = lbound(sList%list,1) 
		l_count = ubound(sList%list,1) 
		do i = 1,l_count 
			! write(outputunit,*) '  NextFreeString init ',i
			t=>sList%list(i)%o
			  if(t%used) write(outputunit,*)"string created used !"
			t%used = .FALSE.
			!t%d=>NULL()
			NULLIFY(t%theConn)
			if(associated(t%se)) then
				DEALLOCATE(t%se,stat=istat);			 write(outputunit,*) "DEALLOC!!!!!"
				NULLIFY(t%se)
			endif
			if(associated(t%rev)) then
				DEALLOCATE(t%rev,stat=istat);			 write(outputunit,*) "DEALLOC!!!!!"
				NULLIFY(t%rev)
			endif		
			if(associated(t%tq_s)) then
				DEALLOCATE(t%tq_s,stat=istat);			 write(outputunit,*) "DEALLOC!!!!!"
				NULLIFY(t%tq_s)
			endif
		enddo
#endif		
		NextFreeString = 1
		sList%count =1

		sList%list(1)%o%used = .TRUE.
		sList%list(1)%o%nn=1
		sList%list(1)%o%m_sli=sn		
		return  ! verified nn and m_sli
	ENDIF ifna
	
! the following logic wont fill in holes in the string array. 
! but it  is very slow to do a simple trawl for holes.  Need a freelist. 	
b:	if(.true.) then  
	do i=sList%count+ 1,ubound(sList%list,1)
		if(.not. sList%list(i)%o%used) THEN
			NextFreeString =i
			sList%list(i)%o%used = .true.
    		sList%list(i)%o%nn=i
		    sList%list(i)%o%m_sli=sn			
			
			if(i > sList%count) then
				sList%count= NextFreeString
			endif
	
			return  ! NN and m_sli verified
		endif		
	enddo
	else b
	        sList%count=sList%count+1
			NextFreeString =sList%count	
			return
	endif  b ! somedeleted
	! write(outputunit,*) ' we get here if no spare spaces in stringlist'
	i =  ubound(sList%list,1) 
        sList%SLblock = max(i,2);! write(outputunit,*) ' nextfreestring block now ', sList%SLblock
	!kkk = dbs("BEFORE realloc ub = ",i)
     	CALL reallocate(sList%list, ubound(sList%list,1) + sList%SLblock,err)
	!kkk = dbs("after realloc",i)
        somedeleted=.false.
	if(err /=0) then
		write(outputunit,*) ' reallocation error in StringList'
		STOP
	endif	

ifk:	do k= i+1,ubound(sList%list,1) 
		sList%list(k )%o%used = .FALSE.

	!	sList%list(k)%o%d=>NULL()
		NULLIFY(sList%list(k )%o%m_Connects)
		if(associated(sList%list(k)%o%se))   DEALLOCATE(sList%list(i)%o%se,stat=istat)
		if(associated(sList%list(k)%o%rev))  DEALLOCATE(sList%list(i)%o%rev,stat=istat)		
		if(associated(sList%list(k)%O%tq_s)) DEALLOCATE(sList%list(i)%o%tq_s,stat=istat)
		sList%list(k)%o%m_fspline =>Null() 
	enddo ifk
	sList%list(i+1)%o%used = .true.
	sList%list(i+1)%o%nn=i+1
	sList%list(i+1)%o%m_sli=sn
	NextFreeString = i+1
	sList%count=NextFreeString 
		

END  FUNCTION   NextFreeString

SUBROUTINE SetStringUsed   (sn, NN,flag,err)
use ftypes_f
use saillist_f
IMPLICIT NONE
	INTEGER , intent(in) :: sn,nn,flag
	INTEGER , intent(out) :: err
	TYPE ( stringlist) ,POINTER :: strings
!
! sets the Used flag depending on the value of 'flag'
!  the usual error returns. 
!  the only twist is that if this is the last string, we decrement count if flag=0
!  similarly, we increment count if NN is big. But only up to 
!
!	write(outputunit,*) 'SetStringUsed   ', sn, NN,flag
	err=16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	err=8
	strings=>saillist(sn)%strings
	if(.not. ASSOCIATED(strings%list)) return
	if(NN > UBOUND(strings%list,1)) return
	if(NN  <LBOUND(strings%list,1)) return
	err=0
	if(flag/=0) THEN
		if(strings%list(NN)%o%used) err=2
		strings%list(NN)%o%used=.TRUE.
		if(NN > strings%count) strings%count=NN 
	else
		if(.NOT. strings%list(NN)%o%used) err=1
		strings%list(NN)%o%used=.FALSE.
		if(NN ==  strings%count) strings%count=strings%count-1 
	endif

END SUBROUTINE SetStringUsed 

function SetStringTrim (sn, NN,value) result(err)
use ftypes_f
use saillist_f
IMPLICIT NONE 

	INTEGER , intent(in) :: sn,nn
	real(kind=double),intent(in) :: value
	INTEGER  :: err
	TYPE ( stringlist) ,POINTER :: strings
!
! sets the string trim. SHould be safe at any time.
!  the usual error returns. 
!  the only twist is that if this is the last string, we decrement count if flag=0
!  similarly, we increment count if NN is big. But only up to 
!
	err=16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	err=8
	strings=>saillist(sn)%strings
	if(.not. ASSOCIATED(strings%list)) return
	if(NN > UBOUND(strings%list,1)) return
	if(NN  <LBOUND(strings%list,1)) return
	err=0
	strings%list(NN)%o%d%m_Trim=value
END function SetStringTrim 

SUBROUTINE  ShowFstringCount(sn, u,l,c, err) 
USE ftypes_f
use saillist_f
IMPLICIT NONE
	INTEGER , intent(in) :: sn
	INTEGER , intent(out) :: err,u,l,c

	TYPE ( stringlist) ,POINTER :: strings

	err = 16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	err=8
	strings=>saillist(sn)%strings
	if(.not. ASSOCIATED(strings%list)) return
	u =UBOUND(strings%list,1)
	L  =LBOUND(strings%list,1)
	c = strings%count

	err=0
END SUBROUTINE  ShowFstringCount

function GetStringSpline(t,IsInterpolatory ,degree) result(s) ! degree should be 3 by default.
USE ftypes_f
use fspline_f
use fsplinecubic_f
implicit none

type (string) ,pointer ::t
logical, intent(in) :: IsInterpolatory
integer, intent(in) ::degree
! return value 
class (fsplineI), pointer ::s
type (fspline), pointer ::sb
type (fsplinecubic), pointer :: fc
!locals
integer::ok

if(associated(t%m_fspline)) then
    s=>t%m_fspline
    return
endif
if(degree<0) then
    allocate(fc); t%m_fspline=>fc ; s=>fc;
    ok=fc%createStringSplineC(t)
else
    allocate(sb); t%m_fspline=>sb ; s=>sb;
    if( IsInterpolatory ) then
        ok=sb%createStringSplineB(t,degree, INTERPOLATING)
    else
        ok=sb%createStringSplineB(t,degree,STANDARD )
    endif
 endif
end function GetStringSpline

SUBROUTINE   PrintFstrings_As_PC (sli,unit,length_factor) 
USE ftypes_f
USE saillist_f

IMPLICIT NONE
    INTEGER , intent(in) :: sli,unit
    real(c_double), intent(in) :: length_factor

    TYPE ( stringlist) ,POINTER :: strings
    TYPE ( string) ,POINTER :: this,t
    INTEGER :: i, k,nn,n1,n2
    real(kind=8):: totlength
    TYPE (edgelist),pointer:: eds
    type(Fedge),pointer ::e 
    eds=>saillist(sli)%edges 	
    if(.not. associated(saillist)) return
    if(sli > ubound(saillist,1)) return	
    if(sli < lbound(saillist,1)) return


	strings=>saillist(sli)%strings
	if(.not. ASSOCIATED(strings%list)) return
	
	write(unit,'(/,a)') '// First line is number of Links '
	write(unit,*)'// Then Num, StartN, EndN, SlackLength, PrpNo, Control, Boundary, Field'
    	write(unit,*) '<LINKS>'
 
! count the number of strings
    nn=0
	do i =1,  UBOUND(strings%list,1)
		this=>strings%list(i)%o
		if(this%used) then
				if(associated(this%se).and. associated(this%rev) .and. associated(this%tq_s)) then
 		        do k= 1,this%d%Ne
                    nn=nn+1
		        enddo
            endif
		endif
	ENDDO
    write(unit,'(i,//)') nn

    nn=0; totlength=0;
	do i =1,  UBOUND(strings%list,1)
		this=>strings%list(i)%o
		t=>this
ifused:		if(this%used) then
            if(.not. associated(this%se) ) cycle
dok: 		do k= 1,this%d%Ne
               nn=nn+1
 !Number, n1, n2, SlackLength, PropNo, Control, Boundary, Field
 
                n1 = this%se(k); 
                e=>eds%list(n1)
                n2 = e%m_L12(2);	n1 = e%m_L12(1)
                write(unit,'(3(1x,i8),F16.10,4i4)') nn,n1,n2,t%d%m_zisLessTrim*length_factor ,1,1,0,0
                totlength = totlength + t%d%m_zisLessTrim*length_factor
		enddo dok
		endif ifused
	ENDDO
    	write(unit,*) '</LINKS>'
    	    	write(unit,*) '// total length ', totlength
10	format(A,6G13.5)
20	format(A,6L13)
30	format(A,6I13)
40	format(A,12I6)
50	format(12L6)
60	format(12G10.3)
100	format(i6,L6,G15.5)

END  SUBROUTINE  PrintFstrings_As_PC

subroutine  PrintOneFstringRelations (t,unit)
USE ftypes_f
implicit none
	TYPE ( string),intent(in) ,POINTER :: t
	INTEGER , intent(in) :: unit
	
		INTEGER :: j, k
	character(len=256) :: buf
ifu:		if(t%used) then	
			buf = adjustl(trim(t%m_text))
		    do k=1,24
		     if(ichar(buf(k:k)) .eq. 0) buf(k:k)=' '
		    enddo
		    write(unit,102,err=300)  buf,  t%Nn,t%d%Ne 
300		    if(associated(t%m_connects)) then
		         do j=1,ubound(t%m_connects,1)
		             if(associated(t%m_Connects(j)%o)) then
		                    write(unit,'(10x,i5,1x,a)')j, trim(t%m_Connects(j)%o%string)
		             else
		                    write(unit,'(10x,i5,a)') j,' empty slot'
		            endif
		        enddo
		    endif
		    else ifu
		    write(unit,*) ' not used'
		    endif  ifu
		    102 format(TR1,a16,tr1,I4,tr1,I4,\)
end subroutine PrintOneFstringRelations

SUBROUTINE  PrintFstrings (sn,unit) 
USE ftypes_f
USE saillist_f

IMPLICIT NONE
	INTEGER , intent(in) :: sn,unit

	TYPE ( stringlist) ,POINTER :: strings
	TYPE ( string) ,POINTER :: t
	INTEGER :: i,j, k,count
	character(len=256) :: buf
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return


	strings=>saillist(sn)%strings
	if(.not. ASSOCIATED(strings%list)) return
	write(unit,*) ' '; 	write(unit,*) ' '; 

101 format(TR1,a6,tr1,a24,tr1,a2,tr1,a6,tr1,a6,1x,6(tr1,a13),tr1,a6,tr1,a6,TR1,a6,tr1,a15 ,a6,a6,a11 )

!101 format(20(TR1,a12))
102 format(TR1,i6,tr1,a24,tr1,I2,tr1,I6,tr1,I6,1x,\)
	
103 format(6(tr1,EN13.4),\)
109 format(tr1,i6,tr1,i6,\)
105 format(TR1,i6,tr1,EN15.5)	
106 format(i6,L6,G15.5)
107 format(' <',a1,'>=',i)
117 format(tr1,g14.4,\)
   count=0
    write(unit,*) ' strings connect relations'
doi: 	do i =1,  UBOUND(strings%list,1)
		t=>strings%list(i)%o
ifu:		if(t%used) then	 ; count=count+1
			buf = adjustl(trim(t%m_text))
		    do k=1,24
		     if(ichar(buf(k:k)) .eq. 0) buf(k:k)=' '
		    enddo
		    write(unit,102,err=300) i, buf,t%d%m_slider,  t%Nn,t%d%Ne 
		      if(associated(t%se).and. associated(t%rev) .and. associated(t%tq_s).and.  t%d%Ne>0) then
				   write(unit,117,err=300)  t%tq_s(1)
			endif
300	    write(unit,*)
	    if(associated(t%m_connects)) then
		         do j=1,ubound(t%m_connects,1)
                             if(associated(t%m_Connects(j)%o)) then
		                    write(unit,'(10x,i5,a)')j, trim(t%m_Connects(j)%o%string)
		             else
		                    write(unit,'(10x,i5,a)') j,' empty slot'
		            endif
		        enddo
		    endif
		    endif  ifu
    enddo doi
        write(unit,'("count=",i5,a,i5)') count,' of ', UBOUND(strings%list,1)
	write(unit,*) ' StringList for model ', trim(saillist(sn)%sailname)
        write(unit,101) 'index','name', ' slide', ' N ',' ne ' , 'zisLessTrim', '  zt_s  ','EAS   ', 'TIS    ','trim   ','Mass  ',&
            ' TConst','canB3' ,'edge1',  'tq'
	do i =1,  UBOUND(strings%list,1)
	
		t=>strings%list(i)%o
		if(t%used) then	
			buf = adjustl(trim(t%m_text))
		    do k=1,24
		        j = ichar(buf(k:k))
		        if(j .eq. 0) buf(k:k)=' '
		    enddo
		    write(unit,102,err=200) i, buf,t%d%m_slider, t%Nn,t%d%Ne  
		    
200		    write(unit,103,err=210) t%d%m_ZisLessTrim,t%d%m_zt_s, t%d%eas,t%d%tis,t%d%m_Trim,t%d%m_Mass

210		    write(unit,109,err=220) t%d%m_tcnst ,t%d%m_CanBuckle3 

220		    if(associated(t%se).and. associated(t%rev) .and. associated(t%tq_s).and.  t%d%Ne>0) then
                        write(unit,105,err=230) t%se(1),  t%tq_s(1)
230			if(t%d%Ne>1) then
                            do k= 1,t%d%Ne
                              write(unit,106,err=240) t%se(k), t%rev(k), t%tq_s(k)
240		            enddo
                        endif
		    else if( associated(t%se) .or.associated(t%rev)  .or.associated(t%tq_s)   ) then
			    if(associated(t%se) ) 	write(unit,40) 'se ', t%se 
			    if( associated(t%rev) )	write(unit,50) t%rev 
			    if(associated(t%tq_s) )	write(unit,60) t%tq_s
			else
			   write(unit,*)'( arrays not allocated)'
		    endif
		endif
	ENDDO
	write(unit,*) ' count is ',strings%count
40	format(A,12I6)
50	format(12L6)
60	format(12G10.3)


END  SUBROUTINE  PrintFstrings 

! p_Comp must have value YES (+1) or NO (-1)
SUBROUTINE CS_String_Resids(sli,p_Comp)! adds into TLINK
    use saillist_f
    IMPLICIT NONE
    integer, intent(in) :: sli
    logical ,intent(in) :: p_Comp


!* locals
    INTEGER Ns,ne, buckleEncouragement
    TYPE (stringlist), pointer 	:: strings
    type (string) , pointer		:: theString
    TYPE (edgelist),pointer:: eds
    type(sail),pointer ::sl

    if(.not. is_active(sli) ) return
    sl=>saillist(sli)
    eds=>sl%edges
    strings=>sl%strings
    if(p_comp) then ;buckleEncouragement =1; else ;  buckleEncouragement =-1 ;endif

    CALL CS_String_Tensions(sli,buckleEncouragement)! last arg is now integer. 1 encourages buckling

!!!!$ONOT MP PARALLEL DO PRIVATE(ns,thestring,ne) shared(strings,eds) default(none)       	
    do_n : DO Ns=1,strings%count
    theString =>strings%list(ns)%o
    ne = TheString%d%Ne
    if(theString%d%m_slider.ne.0) then
        eds%list(theString%se(1:ne))%m_Tlink = eds%list(theString%se(1:ne))%m_Tlink  +  theString%tq_s(1)
    else
        eds%list(theString%se(1:ne))%m_Tlink = eds%list(theString%se(1:ne))%m_Tlink +  theString%tq_s(1:ne)
    endif
    ENDDO do_N
!!!!$O NOT MP END PARALLEL DO

END   SUBROUTINE CS_String_Resids

SUBROUTINE CS_String_Tensions(sli,buckleEncouragement)
	use vectors_f
	use saillist_f
	use links_pure_f
	use coordinates_f
      IMPLICIT NONE

!* parameters
    integer, intent(in) :: sli
    integer, intent(in)  :: buckleEncouragement   ! 1 encourages buckling.  0 discourages it-1 or 1 tension allowed 
!* locals
        INTEGER Ns,i, nee
        REAL*8 EA,d(3), ztl,zi,factor
        LOGICAL maybuckle   ! TRUE if negative tension allowed 
        TYPE (stringlist), pointer 	:: strings
        type (string) , pointer		:: t
        TYPE (edgelist),pointer:: eds
        type (fnode), pointer :: n1,n2
		type(sail),pointer ::s
      
	if(.not. is_active(sli)) return
	s=>saillist(sli)
        eds=>s%edges 	
	strings=>s%strings
	if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 13'
!$NOTOMP PARALLEL DO PRIVATE(ns,t,nee,ea,maybuckle,zi,factor,n1,n2,D,ztl) shared(strings,buckleEncouragement,eds,G_PLEASEHOOKELEMENTS,s ) default(none) 	
do_n : DO Ns=1,strings%count
      	t =>strings%list(ns)%o
      	if(.not. t%used)  cycle
ifNZ:	if(t%d%Ne .gt.0) then
            nee = t%d%Ne
            if(t%d%m_tcnst .ne.0 ) THEN
                ea=0.0
                t%tq_s(1:nee) = t%d%tiS;
            ELSE
                EA = t%d%EAS
                t%d%m_zt_s=0.0
                if(0 .eq. t%d%m_slider) then
                    factor = (t%d%m_Trim + t%d%m_ZisLessTrim) / Sum(  eds%list(t%se(1:nee)  )%m_zlink0)		! longer by trim
                    do i=1,nee
                        n1 => eds%list(t%se(i))%m_ee1;
                        n2 => eds%list(t%se(i))%m_ee2;
                        D =get_Cartesian_X_by_Ptr(n2)-get_Cartesian_X_by_Ptr(N1)
                        ZTl= norm(d)		! would be faster to use zlink0, etc
                        t%d%m_zt_s = t%d%m_zt_s + ztl
                        zi =  eds%list(t%se(i))%m_zlink0 *factor
                        t%tq_s(i)=EA *(ztl -ZI)/ZI +t%d%tis
                        maybuckle =  ((t%d%m_CanBuckle3 + buckleEncouragement) >=0)
                        if(maybuckle ) then
                             t%tq_s(i) = max(t%tq_s(i),0.0d00)
                         endif
                    enddo

                else
                    maybuckle =  ((t%d%m_CanBuckle3 + buckleEncouragement) >=0)
                    zi = t%d%m_zisLessTrim+t%d%m_Trim
                    CALL One_String_T(t%se,nee,t%d%tiS,zi,ea,t%tq_s(1),t%d%m_zt_s,maybuckle,Get_SLI(s))! tq scalar se(1) removed PH dec 31 04
                    t%tq_s(2:)=t%tq_s(1) ! june 2013 for better reporting
                endif
            ENDIF
            t%d%m_tqOut =  t%tq_s(1)
            endif  ifNZ
       ENDDO  do_n
!$ONOTMP END PARALLEL DO
END SUBROUTINE CS_String_Tensions
SUBROUTINE AssembleStringElts(sli,StringCompAllowed,m)
    use basictypes_f
    use saillist_f
    use coordinates_f
    use vectors_f
    use bar_elements_f
    use links_pure_f
    use cfromf_f
    use stiffness_f
    IMPLICIT NONE

! parameters
    integer, intent(in) :: sli
    LOGICAL, intent(in) ::  StringCompAllowed   ! TRUE if negative tension allowed 
    integer(kind=cptrsize)  :: m
 ! returns
 ! nothing, but it increments the global stiffness matrix M

!* locals
    REAL (kind=double), allocatable, dimension (:,:) ::D
    REAL (kind=double), allocatable, dimension (:,:) ::de
    REAL (kind=double), allocatable, dimension (: ) ::zte
    INTEGER , allocatable, dimension(:) :: nodes
    INTEGER N,i,k,size,buckleEncouragement
    logical :: bucklePermitted
    REAL (kind=double), dimension (6,6 ) ::s6
    real*8 F,dloc(3),t,l_zi
    type(sail), pointer         ::sl
    type (string) , pointer	:: theString
    type (Fedge) , pointer	:: e

    if(.not. is_active(sli)) return
    sl=>saillist(sli)
    CALL CS_Check_All_String_Order(sli)  ! this is SLOW BUT SAFE

    if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 12'
    if(StringCompAllowed) then ;buckleEncouragement =1; else ;  buckleEncouragement =-1 ;endif

    CALL CS_String_Tensions(sli,buckleEncouragement) !last arg is now integer. 1 encourages buckling

      do_n : DO N=1,sl%strings%count
      	theString =>sl%strings%list(n)%o
    	if(theString%d%Ne .le.0) cycle
        size = 3*(theString%d%Ne+1)
        allocate(D(size,size))
        allocate(de(3,TheString%d%Ne))
        allocate(nodes(theString%d%Ne+1))
        allocate (zte(theString%d%Ne))
slid:   if(0 .eq.  theString%d%m_slider) then ! treat as a set of links
            l_zi=0.0;
            do k=1,theString%d%Ne
                e => sl%edges%list(theString%se(k))
                l_zi=l_zi + e%m_Zlink0
            enddo
                f = (theString%d%m_Trim + theString%d%m_ZisLessTrim) / l_zi
d1 :            do i=1,theString%d%Ne
                    e => sl%edges%list(theString%se(i))
                    dloc = get_Cartesian_X_By_Ptr(e%m_ee2) - get_Cartesian_X_by_Ptr(e%m_ee1)
                    t = F* e%m_Zlink0  ! initial length
                    call One_Link_Global_Stiffness(Dloc,theString%Tq_s(i),theString%d%eas, s6,t)
                    if(any(isnan(s6))) write(outputunit,*) ' NAN OLGS'
                    call AddStiffness(sl,e%m_L12,s6,m)
                 enddo d1
        else slid ! it slides
            bucklePermitted =  ((theString%d%m_CanBuckle3 + buckleEncouragement) >=0)
ifb:        if ((theString%TQ_S(1) .GT. 0.0) .OR.  bucklePermitted) THEN
do_i:		 DO I = 1,TheString%d%Ne
                     e => sl%edges%list(theString%se(i))
                     if(theString%rev(i)) THEN
                            de(1:3,i) =-e%m_dlink; Nodes(i)=e%m_L12(2);  Nodes(i+1)= e%m_L12(1)
                     ELSE
                            de(1:3,i) = e%m_dlink; Nodes(i)=e%m_L12(1);  Nodes(i+1)= e%m_L12(2);
                     ENDIF
                     zte(I) = e%m_Zlink0 + e%m_delta0
                 ENDDO do_i

                 D = 0.0D00
                 CALL One_String_Geo_Stiffness(size,D,de,theString%d%Ne,theString%tq_s(1),zte) ! Doesnt check for buckling
if0:             if(0 .eq.  TheString%d%m_TCnst) THEN  ! ea is non-zero
                    l_zi = theString%d%m_zisLessTrim + theString%d%m_Trim
                    CALL One_String_Sliding_Dir_Stiffness(size,D,de,theString%d%Ne,l_zi,theString%d%EAS)! Doesnt check for buckling
                 ENDIF   if0
                 call AddStiffness(sl, nodes,d,m)
           !  else ifb
            !     write(*,*) ' skip sliding string  assy tq=', theString%TQ_S(1)  , 'canB=', theString%d%m_CanBuckle3 ,'benc=', buckleEncouragement
             ENDIF ifb
	endif slid

	deallocate(D)
	deallocate(de)
	deallocate(nodes)
	deallocate(zte)
    ENDDO do_n   
    
end  SUBROUTINE AssembleStringElts

SUBROUTINE CS_String_Masses(sli,p_Comp)
    use saillist_f
	use coordinates_f
	use vectors_f
	use bar_elements_f
	use links_pure_f
      IMPLICIT NONE

! parameters
    integer, intent(in) :: sli
    LOGICAL, intent(in) ::  p_Comp  ! TRUE if negative tension allowed 

! returns
 ! nothing, but increments nodelist%XM 

!* locals
      REAL (kind=double), allocatable, dimension (:,:) ::D
      REAL (kind=double), allocatable, dimension (:,:) ::de
      REAL (kind=double), allocatable, dimension (: ) ::zte
      INTEGER , allocatable, dimension(:) :: nodes
      INTEGER N,i,j,k,l, nn  ,size,row,c,TheNode ,buckleEncouragement
      logical ::maybuckle
	real*8 s(6,6),F,dloc(3),t,l_zi
	real(kind=double),dimension(3,3) :: s3
     type(sail), pointer  ::sl=>NULL()
	type (string) , pointer		:: theString
  	type (Fedge) , pointer		:: e
    if(.not. is_active(sli)) return
	sl=>saillist(sli)
	CALL CS_Check_All_String_Order(sli)
	
        if(p_comp) then ;buckleEncouragement =1; else ;  buckleEncouragement =-1 ;endif
    CALL CS_String_Tensions(sli,buckleEncouragement) !last arg is now integer. 1 encourages buckling

      do_n : DO N=1,sl%strings%count
      	theString =>sl%strings%list(n)%o
ifNZ:	if(theString%d%Ne .gt.0) then
	size = 3*(theString%d%Ne+1)
	allocate(D(size,size))
	allocate(de(3,TheString%d%Ne))
	allocate(nodes(theString%d%Ne+1))
	allocate (zte(theString%d%Ne))
	if(0 .eq.  theString%d%m_slider) then
	    l_zi=0.0;
	    do k=1,theString%d%Ne
	    e => sl%edges%list(theString%se(k))
	    l_zi=l_zi + e%m_Zlink0
	    enddo
		f = (theString%d%m_Trim + theString%d%m_ZisLessTrim) / l_zi
d1 :		do i=1,theString%d%Ne
				    if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 12'
	            e => sl%edges%list(theString%se(i)) 
			    dloc = get_Cartesian_X_By_Ptr(e%m_ee2) - get_Cartesian_X_by_Ptr(e%m_ee1)
			    t = F* e%m_Zlink0  ! initial length 
			    if(.true.) then
 				    j = Bar_Matrix(dloc,t,norm(dloc),theString%tq_s(i),theString%d%EAS,s3)
				    DO j=1,2
					    TheNode = e%m_L12(j)
					    sl%nodes%Xlist(TheNode)%XM(1:3,1:3) = sl%nodes%Xlist(TheNode)%XM(1:3,1:3) + s3
				    ENDDO
 			    else
			        call One_Link_Global_Stiffness(Dloc,theString%Tq_s(i),theString%d%eas, s,t)
			        if(any(isnan(dloc))) write(outputunit,*) ' NAN OLGS'
    d2 :			DO j=1,2
			            TheNode = e%m_L12(j)
    d3 :			    DO K=1,3
				            row = (J-1) * 3 + K
				            DO l=1,3
					            c = l + (j-1)*3
					            sl%nodes%Xlist(TheNode)%XM(k,l) =sl%nodes%Xlist(TheNode)%XM(k,l) + s(row,c)
				            ENDDO
			            ENDDO  d3
			        ENDDO d2 
			    endif
		    enddo d1
	else ! it slides
	maybuckle =  ((theString%d%m_CanBuckle3 + buckleEncouragement) >=0) 
iftq:	if ((theString%TQ_S(1) .GT. 0.0) .OR. maybuckle) THEN
		do_i : DO I = 1,TheString%d%Ne
			 e => sl%edges%list(theString%se(i)) 	
	 		 if(theString%rev(i)) THEN
				de(1:3,i) =-e%m_dlink
				Nodes(i)   = e%m_L12(2)
				Nodes(i+1) = e%m_L12(1)
	 		 ELSE
				Nodes(i)   = e%m_L12(1)
				Nodes(i+1) = e%m_L12(2)
				de(1:3,i) = e%m_dlink 
	  		ENDIF
	  		zte(I) = e%m_Zlink0 + e%m_delta0
		    ENDDO do_i

		    D = 0.0D00
		    CALL One_String_Geo_Stiffness(size,D,de,theString%d%Ne,theString%tq_s(1),zte)
		    if(any(isnan(d))) then
			        write(outputunit,*) 'NAN String Geo Stiffness',N,trim(thestring%m_text)
			        D=0.0
		    endif
     		if(0 .eq.  TheString%d%m_TCnst) THEN
			l_zi = theString%d%m_zisLessTrim + theString%d%m_Trim
			CALL One_String_Sliding_Dir_Stiffness(size,D,de,theString%d%Ne,l_zi,theString%d%EAS )
			   if(any(isnan(d))) write(outputunit,*) 'NAN String Dir Stiffness',N, trim(thestring%m_text)
		ENDIF  
	     	DO I=1,theString%d%Ne+1
	     	 nn = Nodes(i)
	    	  DO J=1,3
	    	   DO K=1,3
			    sl%nodes%Xlist(nn)%XM(j,k) = sl%nodes%Xlist(nn)%xm(J,K) + D( 3*(i-1)+J, 3*(i-1)+k)
	   	    ENDDO
	   	   ENDDO
	   	ENDDO
             else iftq
             !   write(*,"(' skip sliding string mass tq=',g11.3,' canBuckle=', i3,' bencouragement=', i3  )")  theString%TQ_S(1)  ,theString%d%m_CanBuckle3 , buckleEncouragement
             ENDIF iftq
	endif
	deallocate(D)
	deallocate(de)
	deallocate(nodes)
	deallocate(zte)
!debug
  !  write(outputunit,10) i,trim( theString%m_text),  theString%d%m_Trim ,theString%d%m_ZisLessTrim, theString%tq_s(1)
10   format(i6,1x,a20,' trim=',g12.4,' m_ZisLessTrim= ',g12.4,'tq ',g12.4 )   
!end dbg 	
	endif ifNZ
      ENDDO do_n
END SUBROUTINE CS_String_Masses

SUBROUTINE  CS_Check_All_String_Order(sli) ! does order then reversal 
	use saillist_f
	use fstrings_f
      	IMPLICIT NONE
      	integer, intent(in) :: sli
	    integer iret

!* locals
        INTEGER N!,n_e
        type (string) , pointer		:: theString
        type(sail) ,pointer:: s 
      	
	if(.not. is_active(sli) ) return
	s=>saillist(sli)
	!strings=>s%strings
      do_n : DO N=1,s%strings%count
      	        theString=>s%strings%list(n)%o
	            iret = CS_Check_One_String_Order(sli,n)
	            if(iret==0) then
		            write(outputunit,*) ' string Order failed ', trim(TheString%m_text)
		            TheString%d%Ne=0
	            else
		            iret = CS_Check_One_String_Reversal(TheString)
		            if(iret==0) then
			            write(outputunit,*)'Check_All_String_Order -Reversal ', trim(TheString%m_text)
			            TheString%d%Ne=0
		            endif
	            endif
          ENDDO do_n
END SUBROUTINE CS_Check_All_String_Order

subroutine FindNearestSegOnString(t,q,SlowMethod ,index,frac)

! this is Find-Nearest for polylines. The Slow Method should be watertight
! for OPEN polylines however pathological.
! We measure the distances from each vertex and each segment to our test point Q
! The 'Band' of a segment is the disc of infinite radius with the segment forming its axis.
! For the first and last segments, we extend its band to infinity. 
! then
! Identify the band with min radius (Y) and the closest vertex.
! If Q is within no band, the closest point is the closest vertex
! If Q is within just one band, then the closest point is the projection of
! Q onto the segment, UNLESS a vertex is closer.
! If Q is within multiple bands, the closest point will be the projection of Q onto
! the segment which gives the smallest radius Y, unless there is a vertex which is closer.


use ftypes_f
use saillist_f
use coordinates_f
use vectors_f
implicit none
	type (string),intent(in) :: T
	REAL (kind=double)  ,dimension(3),intent(in) :: q
	logical, intent(in) :: SlowMethod
	integer, intent(out) :: index
	REAL (kind=double)  , intent(out) :: frac 
	integer :: i,LL,nbands,imin
	 TYPE (edgelist),pointer:: eds
      type(Fedge),pointer ::e 
      type (Fnode), pointer :: n1,n2
      real(kind=double), dimension(3) :: x1,x2,dx,a1,a2
       real(kind=double) :: seglensq,dx1,dx2,amin  ! dx1,dx2 are proportions
       
       type (segband) , dimension(t%d%Ne) :: thebands
       real(kind=double) , dimension(t%d%Ne+1) :: dists  ! distance squared
          
      eds=>saillist(t%m_sli)%edges
      index=-99; frac=-1;
	if(.not. SlowMethod) then
	    write(outputunit,*) ' fast findnearest not implemented'
	    index=-99
	    return
	endif
   
!Identify whether q is in the transverse beam of any segments
!If it isnt, that means that the nearest point is the nearest vertex
!If it is in just one beam, that is it.
!if it is multiple beams, take the closest.	
! in general we can't use the nearest point as a guide
    nbands=0
	DO I = 1,t%d%Ne
	     LL = t%se(i); e=>eds%list(LL)

	      if(t%rev(i)) THEN
		    N1 => e%m_ee2
		    N2 => e%m_ee1
	     ELSE
		    N1 => e%m_ee1
		    N2 => e%m_ee2
         ENDIF
         x1 = Get_Cartesian_X_By_Ptr(n1); x2 = Get_Cartesian_X_By_Ptr(n2)
 
         dx = x2-x1
         seglensq = (sum(dx**2)) 
         a1 = q - x1; 	 a2 = q - x2;         dists(i) = sum(a1**2)
         dx1 = Dot_Product(a1,dx)  
         if(I .ne.1 .and. dx1<0.) cycle;
         dx1 = dx1/seglensq; 
         dx2 = -dot_product(a2,dx)    
         if(I .ne. t%d%ne  .and. dx2 <=0.) cycle
         dx2 = dx2/seglensq  ! proportion        
     ! its in the band; 
        nbands = nbands+1
        thebands(nbands)%i = i;
        thebands(nbands)%xprop1 = dx1; thebands(nbands)%xprop2 = dx2;
        thebands(nbands)%y =(  norm(Cross_Product( a1,dx)) +norm( Cross_Product(a2,dx)))/sqrt(seglensq)/2.0d00
	ENDDO	
	  dists(t%d%Ne+1) = sum(a2**2)
	if(nbands .eq. 0) then  ! we want the nearest vertex to q
        index = minloc(dists,1) 
        frac=0.0d00
        return
	else 	if(nbands .eq. 1) then ! 
	    index = thebands(1)%i; frac =( thebands(1)%xprop1 + ( 1.- thebands(1)%xprop2 ))/2.0d00
	else  ! multiple bands. we take the one with lowest band%Y
        amin = thebands(1)%y
        index = thebands(1)%i; frac =( thebands(1)%xprop1 + ( 1.- thebands(1)%xprop2 ))/2.0d00
        do i=2,nbands
        if( thebands(i)%y < amin) then
            amin = thebands(i)%y
            index = thebands(i)%i; frac =( thebands(i)%xprop1 + ( 1.- thebands(i)%xprop2 ))/2.0d00            
        endif
	enddo
	amin = amin**2
	if( any(dists  < amin)) then 
	    imin = minloc(dists,1)
	    if(imin .eq. ubound(dists,1) .and. index .eq. t%d%Ne ) return ! off the end
	    if(imin .eq. 1 .and. index .eq.1 ) return ! off the start	    
        index = minloc(dists,1) 
        frac=0.0d00	
	endif
	endif
!	type ::segband
!        real(kind=8) :: y, xprop
!        integer i
!    end   type segband
 
end subroutine FindNearestSegOnString
END MODULE stringList_f

SUBROUTINE removeAllFStrings(sn,err)
use ftypes_f
use saillist_f
use stringlist_f
IMPLICIT NONE
	INTEGER , intent(in) :: sn
	INTEGER , intent(out) :: err

	integer i,SomeErrors
	TYPE ( stringlist) ,POINTER :: strings
	err = 16
	SomeErrors=0
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	strings=>saillist(sn)%strings
	do i = strings%count,1,-1
		call RemoveFortranString (sn, i,err) 
		if(err /=0) SomeErrors = SomeErrors+1
	enddo
	if(SomeErrors /=0) then
		write(outputunit,*) 'model',sn,' removeFortranString (from remove ALL) Nerr=',SomeErrors
	endif
END SUBROUTINE removeAllFStrings
