

subroutine DofoPullAll( err, SomethingChanged) ! adjust Tees to ensure compatibility of Xs at shared nodes
!  A Newton-Raphson scheme to adjust the Tees so that dofos (including non-linear ones) give
! consistent results for nodal coordinates
!  If all dofos were linear, this would only be necessary once.
!   where a non-linear dofo has a shared node, this should be called each time
!  the Tees are updated.

! something like
!   1   collect all dofos without parents (ie root dofos)and with some common nodes
!   2   (Maybe) treat for master/slave relations
!   3   identify nodes shared between dofos

!   4 start loop
!   5   Form the Jay-matrices based on (existing Tee + Delta-Tee) (without creating combos)
!       form the B-vector, the NL error on coords of shared nodes, ordered as in A1
!       if it is small, exit loop
!   6   form the A1 matrix which has a 3-row for each common-node pair
!       and a column for each Tee
!   7   form the A2 matrix which has a 3-row for each NON-shared node
!       and the SAME ordering of Tees for its columns.

!   8   SVD A1 into (u,w,vt)
!   9   set Q as the dead columns of Vt and P as its live columns
!   10  Define TeeDash st  delta-Tee= Vt.TeeDash.  Work in Tee-Dash space
!   11  get TeeDash0 by solving the linear system A1.p . TeeDash0 == B (may be under-determined)
!   12  write the second equation as 0 == A2. (p .TeeDash0 + q.TeeDash1)
!       and find a least-squares solution for TeeDash1
!   13  now update the Tees by ( q.TeeDash0 + p.TeeDash1)
!
!   END LOOP
!
!   If anything has changed we need to renew all the Jacobians.

!   UNKNOWNS  Jan 2014
!           - what to do if A1 has linearly-dependent rows?
!           - for rotational nodes, how do we scale so TeeDash has length units?
    use ftypes_f
    use saillist_f
    use fnoderef_f
    use dofoprototypes_f
    use dofomath
    use dofoio_f
    use dbg
    use rx_control_flags_f
    USE mkl95_LAPACK !, ONLY: gesvd
    implicit none

    TYPE (dofolist),target :: ll
     TYPE (dofolist),pointer :: llp
    integer,intent(out) :: err,SomethingChanged

    ! locals
    integer, pointer, dimension(:) :: nn1,nn2,c0,ra1,ra2
    integer i,j,k,nn,nr1,nr2,ncc,MinMN,r,c,info,m,n,rw,errCode,ncycles,rj1,rj2
#ifdef NO_DOFOCLASS
       type (dofo),pointer, ::d1,d2 ! or class
#else
       class (dofo),pointer,::d1,d2 ! or type
#endif
    type(fnode), pointer :: theNode,thenode2
    logical::StillGoing
    real(wp), dimension (:,:) ,allocatable  :: A1,A2, u, A1P,A1Work,A
     real(wp), dimension (:,:) ,target ,allocatable  ::  v
     real(wp), dimension (:,:)  ,pointer  :: p,q
    real(wp), dimension (:)  ,allocatable  :: B,S,TeeDash0,TeeDash1,Y,dt
    real(wp) ,parameter :: tol = 1.0D-8

    err=0; SomethingChanged=0; ncycles=0;

 !

    call PullNNCs()

    nullify(nn1); nullify(nn2)
    call CollectDofosForPull(ll,err) ! all dofos without parents, but with common nodes
    llp=>ll
    k= PrintdofoList(llp,1,417,.false. ); close(417)
     allocate(c0(ll%count+1));    allocate(ra1(ll%count+1));    allocate(ra2(ll%count+1));

! shared nodes - first count rows and columns and clear the 'shared' bit

    nr1=0;  ncc=0; c0(1)=0
di:     do i = 1,ll%count
            d1 => ll%list(i)%o
            do k=1,ubound(d1%m_fnodes ,1)
                d1%m_fnodes(k)%m_fnrFlags= ibclr(d1%m_fnodes(k)%m_fnrFlags,DOFO_FLAG_SHARED)
            enddo
            ncc = ncc + d1%ndof; c0(i+1)= c0(i)+ d1%ndof
dj:         do j = i+1,ll%count
                d2 => ll%list(j)%o
                nn= DOFO_commonNodes(d1,d2, nn1,nn2) ! nn1,nn2 are indices in the dofos %m_fnodes
                nr1 = nr1 + 3*nn
             enddo dj
        enddo di

!  nr1 is the count of shared rows. nr2 is unshared rows.

        if(nr1 .eq. 0 ) then
            return  ! no common nodes'
        endif
        m=nr1; n=ncc ! traditional matrix sizing
        allocate(A1(m,n)); MinMN=min(n,m); A1=0.
        allocate(A1Work(m,n));
        allocate( S(minMN) )
        allocate( U(m,m) )
        allocate( V (n,n) )   ! starts life as its transpose
        allocate(B(nr1)); b=0;


        StillGoing=.true.
sg:     do while (stillGoing) !******************START LOOP*************************
        errCode=1000; ncycles=ncycles+1;
        StillGoing=.false.
!   Form the J matrices for the set of DOFOs
di0:    do i = 1,ll%count
              d1 => ll%list(i)%o
             info = ComputeDOFOJacobian(d1)
       enddo di0

! form A1 and B . If we want to prevent slaves of one dofo being moved by another, we dont set A1 if m_fnrFlags says 'i am a slave'
        r=1; c=1;
di2:    do i = 1,ll%count
            d1 => ll%list(i)%o
dj2:        do j = i+1,ll%count
                d2 => ll%list(j)%o
                nn= DOFO_commonNodes(d1,d2, nn1,nn2) ! nn1,nn2 are indices in the dofos %m_fnodes
 dk2:           do k = 1,nn
                    rj1= d1%m_fnodes(nn1(k))%m_fnrRow;
                    rj2= d2%m_fnodes(nn2(k))%m_fnrRow;
                    A1(r:r+2,c0(i)+1:c0(i+1)) = d1%jacobi(rj1:rj1+2,:)
                    A1(r:r+2,c0(j)+1:c0(j+1)) = -d2%jacobi(rj2:rj2+2,:)
                    theNode=>decodenoderef(d1%m_fnodes(nn1(k)))
                    theNode2=>decodenoderef(d2%m_fnodes(nn2(k))); ! we've checked the nodes are the same
                    B(r:r+2) =   Dofo_Evaluate(d1,d1%m_fnodes(nn1(k))%m_fnrRow)
                    B(r:r+2) = B(r:r+2) -   Dofo_Evaluate(d2,d2%m_fnodes(nn2(k))%m_fnrRow)
                    r=r+3
                    d1%m_fnodes(nn1(k))%m_fnrFlags= ibset(d1%m_fnodes(nn1(k))%m_fnrFlags,DOFO_FLAG_SHARED)
                    d2%m_fnodes(nn2(k))%m_fnrFlags= ibset(d2%m_fnodes(nn2(k))%m_fnrFlags,DOFO_FLAG_SHARED) ! not needed
                enddo dk2
           enddo dj2
       enddo di2
 ! count nr2
 nr2=0;
 di1:    do i = 1,ll%count
             d1 => ll%list(i)%o
dk1:         do k = 1,ubound( d1%m_fnodes,1)
                 if( btest(d1%m_fnodes(k)%m_fnrFlags,DOFO_FLAG_SHARED  ))  cycle dk1
                 nr2=nr2+3
             enddo dk1
        enddo di1


 !       write(outputunit,*) ' A1 is ',nr1,' by',ncc,'  A2 is ',nr2,' by ' ,ncc
  !      flush(outputunit)


        allocate(A2(nr2,n)); A2=0.
       StillGoing= ANY(abs(B)>tol)
       if(StillGoing) SomethingChanged= SomethingChanged+1
       if(StillGoing) write(*, "('(DofoPullAll )ncycles=',i6 , ' max of B is ', G13.5   )")  ncycles,  maxval(abs(b))
        if(ncycles > 1000) then
            write(*,*) ' not going to get there'
            exit sg
        endif
!   form the A2 matrix which has a 3-row for each NON-shared node
!       and the SAME ordering of Tees for its columns.

        r=1; c=1;
di3:    do i = 1,ll%count
            d1 => ll%list(i)%o
 dk3:           do k = 1,ubound(d1%m_fnodes,1)
                    if (BTEST( d1%m_fnodes(k)%m_fnrFlags,DOFO_FLAG_SHARED  ) )  cycle dk3
                    rj1= d1%m_Fnodes(k)%m_fnrRow
                    A2(r:r+2,c0(i)+1:c0(i+1)) = d1%jacobi(rj1:rj1+2,:)
                    r=r+3
                enddo dk3
        enddo di3


! SVD of A1

          A1Work=A1; ! because gesvd modifies the matrix

!*****************************************
        call gesvd(A1Work, s ,u ,v,info=info)

        if(info .eq.0) then
        err=0
        else
        write(*,*) ' gesvd gave ',info, ' errCode was ',errCode
        errCode = info;
        exit sg
        endif
!*****************************************
        rw = count(s .gt. 1.0E-6) ;
        v=Transpose(v)
!   9   set Q as the dead columns of Vt and P as its live columns
        p=>v(:,:rw)
        q=>v(:,rw+1:)


!   10  Define TeeDash st  delta-Tee= Vt.TeeDash.  Work in Tee-Dash space
!    TeeDash is divided into two parts: TeeDash0 associates with the live part and TeeDash1 with the dead part
!   11  get TeeDash0 by solving the linear system A1.p . TeeDash0 == B

    allocate(a1p(m,rw ),stat=info );errCode=errCode+1; if(info .ne. 0) exit sg
    allocate(TeeDash0( rw),stat=info);errCode=errCode+1; if(info .ne. 0) exit sg
    A1P = Matmul(A1,P)


     if(ubound(A1p,1) .ne. ubound(A1p,2)) then  ! A1 has rank < n-rows so we get a least-squares solution
             write(*,*) ' dims of A1p are ', ubound(a1p)
         !   write(*,*) ' dims of Teedash0 are ', ubound(Teedash0), ', rw= ',rw
       !     call PrintOneMatrixForMtka( 'A1', A1)
       !     call PrintOneMatrixForMtka( 'A2',A2)
     !       call PrintOneVectorForMtka( 'B',b)
     !       call PrintOneMatrixForMtka( 'P',p)
    !        call PrintOneMatrixForMtka( 'Q',q)
    !        call PrintOneMatrixForMtka( 'u',u)
    !        call PrintOneMatrixForMtka( 'v',v)
   !         call PrintOneVectorForMtka( 's',s)
             call  LeastSquaresSolution(B,A1P,TeeDash0)

    else  ! A1p is square
             if(ubound(b,1 ) .ne. ubound(Teedash0,1 ) ) then
             write(*,*) 'Crash!!!!!!!!!'
             endif
            Teedash0 = B
            call gesv (A1P,TeeDash0,info=info)

            errCode=errCode+1
            if(info .ne. 0) then
                write(*,*) ' gesv gave info=',info
                write(500,*)   ' dims of A1p are ', ubound(a1p)
                 write(500,*) ' dims of Teedash0 are ', ubound(Teedash0), ', rw= ',rw
                 call PrintOneMatrixForMtka( 'A1', A1)
                 call PrintOneMatrixForMtka( 'P',p)
                 call PrintOnevectorForMtka("TeeDash0", TeeDash0  )
               close(500)
             exit sg
            endif
     endif
     deallocate(A1P,stat=info); errCode=errCode+1; if(info .ne. 0) exit sg

!   12  write the second equation as 0 == A2. (p .TeeDash0 + q.TeeDash1)

! find a least-squares solution for TeeDash1

    Allocate(A(nr2,n-rw ),stat=info) ;errCode=errCode+1;    if(info .ne. 0) exit sg
    allocate(TeeDash1(n-rw),stat=info);errCode=errCode+1;    if(info .ne. 0) exit sg
    allocate(y(nr2 ),stat=info);errCode=errCode+1;   if(info .ne. 0) exit sg
    y = matmul(A2,matmul(P,TeeDash0))
    A = matmul(A2,q)

    call  LeastSquaresSolution(y,a,TeeDash1)

     deallocate(a,y,stat=info);                errCode=errCode+1;    if(info .ne. 0) exit sg

!   13  now update the Tees by ( q.TeeDash0 + p.TeeDash1)
    allocate(dt(n),stat=info);                 errCode=errCode+1;    if(info .ne. 0) exit sg
    dt =-( matmul(p,teedash0) + matmul(q,TeeDash1))

    deallocate(TeeDash1,TeeDash0,A2,stat=info);errCode=errCode+1;    if(info .ne. 0) exit sg
    write(*,*) 'dt range is ',minval(dt), ' to ',maxval(dt)
di4:    do i = 1,ll%count
            d1 => ll%list(i)%o
            d1%t = d1%t + dt( c0(i)+1:c0(i+1) )
        enddo di4
    deallocate(dt,stat=info);    if(info .ne. 0) exit sg
    if (Stop_Running .NE. 0) then;
           write(outputunit,*) ' break requested inDofoPullAll' ;
           exit SG
     endif
   enddo sg  ! the 'still-going' loop
   if(info .ne. 0) then
       write(*,*) ' something failed in DofoPullAll. errcode= ',errcode,'info=',info
       err = info
   endif

    deallocate(A1,B ,  u,v,s, stat=k)
    deallocate(A1Work, stat=k)
    deallocate(nn1,stat=k)
    deallocate(nn2,stat=k)
    deallocate(ll%list,stat=k)

end subroutine DofoPullAll

subroutine PullNNCs()
    use ftypes_f
    use dofo_subtypes_f
    IMPLICIT NONE
    INTEGER  :: i,j,k, err
    TYPE (dofolist)  :: ll
#ifdef NO_DOFOCLASS
   type (dofo),pointer ::d   ! or class
#else
   class (dofo),pointer::d  ! or type
#endif
    err=0
    call CollectDofosToList( ll  , DOFO_EITHER, DOFO_EITHER, C_DOFO_NODENODE_CONNECT, err)

    write(*,*) ' identify all NNCs and pull them '
    write(*,*) ' there are ', ll%count
    do i=1,ll%count
          d=>ll%list(i)%o
    enddo

end subroutine PullNNCs
