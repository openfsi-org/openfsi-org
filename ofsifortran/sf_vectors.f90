MODULE deadsf_vectors_f
USE basictypes_f 
IMPLICIT NONE


CONTAINS


 pure REAL*8 FUNCTION PH_atan2(dy,dx)
      IMPLICIT NONE
!* this functn cannot cope with angles close to 180 degrees but is safer
!* otherwise.
!* lahey ATAN2 has trouble with (0,small)

 	  real(kind=double),intent(in) ::dy,dx
	  real(kind=double):: grad

       PH_atan2 = ATAN2(dy,dx)
       RETURN

      if ( ABS(dx) .gt. 1.0d-8) THEN
        grad = dy/dx
        if (ABS(dy) .lt. 1.0d-8) THEN
          PH_atan2 = ATAN(grad)
        ELSE
          PH_atan2 = ATAN2(dy,dx)
        ENDIF
      ELSE
 !       write(outputunit,*)' atan2 error '
        PH_atan2= 0.0d00
      ENDIF
      END FUNCTION PH_atan2

pure function IdentityMatrix (n) result (c)  
		IMPLICIT NONE
		integer , intent(in):: n
		REAL (kind= double),  dimension(n,n)	 :: c
		integer :: i
		c = 0
		do i=1,n
			c(i,i) = 1
		enddo
end function IdentityMatrix

pure function Outer_Product (a,b,n) result (c)  ! CHECKED.  Sense is OK
		IMPLICIT NONE
		integer , intent(in):: n
		REAL (kind= double), INTENT(in), dimension(n)	 :: a,b
		REAL (kind= double),  dimension(n,n)	 :: c
		integer :: i

		do i=1,n
			c(1:n,i) = a * b(i)
		enddo
end 	function Outer_Product   
  
pure SUBROUTINE normalise(vector,length,error)  
      IMPLICIT NONE      
!* Finds the length of a vector and makes the vector unit length. 
!* Parameters:  
      REAL(kind=double), intent(inout), dimension(:) :: vector 
      REAL(kind=double),intent(out) :: length  
      LOGICAL,intent(out) :: error   

      length=norm(vector)  
      IF(length .lt. 1d-5) THEN 
         error = .true.   
        RETURN       
      ENDIF         
      vector=vector/length
      error = .FALSE.          
END  SUBROUTINE normalise 
   
pure FUNCTION norm(Vector)  result(x)
      IMPLICIT NONE            
      REAL(kind=double), intent(in), dimension(:) :: Vector  
      real(kind=double) ::x                                 
      x =SQRT(Vector(1)**2 + Vector(2)**2 + Vector(3)**2) 
END FUNCTION norm  
     
pure function Mnorm(a) result (b) ! the sum of the vector norms of each row. 
	implicit none

	real(kind=double),target, intent(in), dimension(:,:) :: a
!	real(kind=double), pointer, dimension(:) :: row
	real(kind=double) :: b
	integer::  i,n1,n2, rank
	rank = SIZE(SHAPE(a))
	if(rank .eq. 1) then
!		b = sqrt(dot_product(a,a))
!		return
	endif
	if(rank .gt. 2) i=0/0 ! write(outputunit,*) ' norm not coded above rank 2'
	n1 = UBOUND(a,1); n2 = UBOUND(a,2)
	b = 0.0d00
	do i=1,n1
		!row=>a(i,1:n2)
		b = b + SQRT(DOT_PRODUCT(a(i,1:n2),a(i,1:n2)))
	enddo

end function Mnorm	 
	     
 pure function Cross_product(a,b) result (c)
		implicit none
		real(kind=double), intent(in), dimension(:) ::  a,b
		real(kind=double),  dimension(3) :: c
		Call vprod(a,b,c)
end function Cross_product
 
                          
pure SUBROUTINE vprod(A,B,C)  !finds the vector product  C = A x B
    IMPLICIT NONE   
	REAL (kind=double)  ,dimension(:), intent(in) :: A,B
	REAL (kind=double)  ,dimension(:), intent(out) :: C ! nov 2007  were dim(*)
                            
      	C(1)=    A(2)*B(3)  -   A(3)*B(2)
      	C(2)=   -A(1)*B(3)  +   A(3)*B(1) 
      	C(3)=    A(1)*B(2)  -   A(2)*B(1)
END SUBROUTINE vprod

pure function crossproduct(A,B) result(C)  !finds the vector product  C = A x B
IMPLICIT NONE   
	REAL (kind=double)  ,dimension(:), intent(in) :: A,B
	REAL (kind=double)  ,dimension(3) :: C
                            
      	C(1)=    A(2)*B(3)  -   A(3)*B(2)
      	C(2)=   -A(1)*B(3)  +   A(3)*B(1) 
      	C(3)=    A(1)*B(2)  -   A(2)*B(1)
END function crossproduct
                                                
SUBROUTINE MULT(A,B,C)
!* postmultiplies Matrix B by Matrix C to give matrix A 
      IMPLICIT NONE                               

      REAL*8 A(3,3),B(3,3),C(3,3)    
!*
!*   A= B . C
!* 

   A=MATMUL(B,C)
   return

      END  SUBROUTINE MULT

      SUBROUTINE MULTV(A,B,C)
!* multiplies Matrix B by vector C to give vector A   
 
      IMPLICIT NONE 
      REAL*8 A(3),B(3,3),C(3) 
!*     
!*   A = B X C
!*      
   A=MATMUL(B,C)
   return
      A(1)=B(1,1)*C(1)+B(1,2)*C(2)+B(1,3)*C(3) 
      A(2)=B(2,1)*C(1)+B(2,2)*C(2)+B(2,3)*C(3)
      A(3)=B(3,1)*C(1)+B(3,2)*C(2)+B(3,3)*C(3) 
      END    SUBROUTINE MULTV
           
 SUBROUTINE Zero_Matrix(A,n)
      IMPLICIT NONE  
           
      REAL*8 A(*) 
      INTEGER n,k 
      DO k = 1,n
       A(k) = 0.0d00 
      ENDDO    
      END SUBROUTINE Zero_Matrix


SUBROUTINE Make_Symmetrical(A,nd,n)
      IMPLICIT NONE
      INTEGER,intent(in) ::  nd,n	
      REAL*8 A(nd,nd)


!* locals
       integer i,j
       
       DO i=1,n
       DO j=i,n
      A(i,j) = (A(i,j) + A(j,i) )/2.0D00
       ENDDO
       ENDDO
       DO i=1,n
       DO j=1,i-1
      A(i,j) = A(j,i)
       ENDDO
       ENDDO

END SUBROUTINE Make_Symmetrical

pure function kdel(i,j) result(k)	! kronecker delta
	integer, intent(in) ::  i,j
	integer :: k

	if( i .eq. j) then
		k = 1
	else
		k = 0
	endif
end function kdel

pure function sf_tri_area( x1,x2,x3) result (a)
	real(kind=double), intent(in) , dimension(3) :: x1,x2,x3

	real(kind=double) :: a

	a =norm( Cross_product(x2-x1,x3-x1)) /2.0d00

end  function sf_tri_area
	   
pure function sf_SCaxisTrigraph(x1,x2,x3,y) result (a) 

	implicit none
	real(kind=double), intent(in), dimension(3)	:: x1,x2,x3
	integer ,intent(in) :: y       ! a flag, negative for a left hand set



!               note y can be -ve, meaning a lh coordinate set

	real*8 a(3,3)   !       the matrix we are making
!       finds the axis matrix 

	integer  k
	real*8 xaxis(3),yaxis(3) ,zaxis(3)
	real*8 z,sign
	logical error

	if(y .gt. 0) then
		sign = 1.0d00
	else
		sign = -1.0d00
	endif


	yaxis = ( x3-x1 ) * sign
	 xaxis = x2-x1

	call normalise(xaxis,z,error)
	do 20 k=1,3
20      a(k,1) = xaxis(k)  

	call vprod(xaxis,yaxis,zaxis)   ! returns zaxis

	call normalise(zaxis,z,error)

	call vprod(zaxis,xaxis,yaxis)   ! returns yaxis
	call normalise(yaxis,z,error)
	do 120 k=1,3
	a(k,2) = yaxis(k)       
120     a(k,3) = zaxis(k)        
	
end function sf_SCaxisTrigraph

END MODULE deadsf_vectors_f

