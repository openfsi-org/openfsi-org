!   fsplinecubic.f90
!   implements a cubic spline in 3D interpolating the given points.
!   Uses chord-length spacing by default for the parameter.
!  The parameter is normalised to the domain (0 - 1)
!  but the line and its derivative may be evaluated outside this domain.

! basics of the method:
!  the evaluation fn is a piecewise cubic.  The coefficients
!  are functions of the knot Tees  and coordinates
! Create is:
! 1)  get the Tees by chord-length
! 2)  We get the cubic coefficients by solving a linear system
!   in which the matrix is a function of the tees only
!     and the rhs is a function of the Xs  only
!  this matrix is sparse & non-symmetric
! - it has 4 steep bands
!  3) we solve it 3 times - once for each ordinate
! and we get a set of coefficients for each.
! so  the coefficient matrix has dims ( 3, order, nspans)
!  the painful thing is that we have to re-solve the system
!  each time a CV moves, unlike a basis-function approach.
! that's not hard to do, but its going to be slow if we need to
! do it every time we want to evaluate the spline;

! evaluate is:
!  if its a CV node, just return it.
!  if its an interpolated node, re-solve the system for the new polynomial coeffs
!  and evaluate the relevant polynomial.
!  (but if we are outside the domain, do a linear extrapolation)

#ifdef NEVER
To interpolate a polyline,
We will subclass class (fsplineCubic) with one that doesnt use a fstring
(class (fsplineCubic) only uses m_ownerString in fns Pt() and npts() )

1) creation see  fn getstringspline
 class (fsplineCubic_Static) :: fc
    ok=fc%createStringSplineC(thePoints)

(2) to evaluate, (remember T goes 0 to 1)

     do i = -5,5  ! see function ListFsplineC(this,u,fullprint) result(ok)
           s = tmin + 0.1*real(i); y=s ! or whatever
           x = fc%FsplineEvaluate(s)
    !       d = fc%derivativebyds(s)
           !          i      y
           write(u,'(i4,1x,g16.9,6(1x,g16.9))',iostat=istat) i,y,x,d
    enddo
! remember to make the 1st point = inputpt1 and last = inputptLast
  (3) clean up
fc%ClearFspline( )

#endif


module fsplinecubic_f
    use basictypes_f
    use ftypes_f
    implicit none

type ,extends(fsplineI) :: fsplineCubic   ! the base type
   real(kind=double), allocatable, dimension(:,:,:) :: a
   real(kind=double), allocatable, dimension(:,:) :: mi   ! inverse of the M matrix
    contains
 !inherited methods
            procedure ,pass(this)  :: ListFspline => ListFsplineC
            procedure ,pass(this)  :: ClearFspline => ClearFsplineC
            procedure ,pass(this)  :: FSplineJMatrix =>FSplineJMatrixC
            procedure ,pass(this)  :: FSplineJMatrixMultiple =>FSplineJMatrixMultipleC
            procedure ,pass(this)  :: FsplineEvaluate =>FsplineEvaluateC
            procedure ,pass(this)  :: derivativebyds =>derivativebydsC
            procedure ,pass(this)  :: FindNearest =>FindNearestC
!locals
            procedure ,pass(this)  :: createStringSplineC
            procedure ,pass(this)  :: CreateArcLengthKnots
            procedure ,pass(this)  :: WhichSpan   ! could be a binary search, or could use a seed, or  simple linear search
            procedure ,pass(this)  :: Pt
            procedure ,pass(this)  :: npoints
            procedure ,pass(this)  :: FSplineJMatrixSlideC
            procedure ,pass(this)  :: FSplineJMatrixNoSlideC
            procedure ,pass(this)  :: SlaveCoefficients
end type fsplineCubic

contains
function npoints(this) result (np)
implicit none
! parameters
integer :: np
      class (fsplineCubic),intent(in) :: this
    np = ubound(this%m_ownerString%m_pts,1)
end function npoints

function SlaveCoefficients(this,s) result(TheRow)
!  For the interpolating cubic spline, this function returns a vector of influence coefficients
 ! of the displacement of a slave node at s due to the master node displacements
 ! for input it takes only s and matrix MI.
!
! See  splineinterpolation.nb
    implicit none
! parameters
      class (fsplineCubic),intent(in) :: this
      real(kind=double) ,intent(in) ::s
!result
      real(kind=double), pointer, dimension(:)::TheRow
!locals
    integer:: i,np,n1,m,c
    real(kind=double) :: z,s2,s3
    np = npoints(this) ! ubound(this%m_owner String%m_pts,1)
    allocate(TheRow(np));

    associate( mi=>this%mi)
    i=this%WhichSpan(s) !  0 for below start, NP for above end.
    n1=np-1    ; s2=s*s; s3=s2*s

!  column 1

c1: if( i ==0 ) then
       z=this%mi(1, 2)  + s * this%mi (np, 2)           ! ok
    elseif( i == np )then
        z= mi( n1, 2)  + s *mi(2*n1,2)  + (-1. + 2.*s)*mi(3*n1, 2) + (-2. + 3.*s)*mi(4*n1, 2)  ! OK
    else
        z =          mi(i,2) +   s*          mi(n1+i,2) + s2 *        mi(2*n1+i,2) + s3*mi(3*n1+i,2) ! OK
    endif c1
    theRow(1)=-z

!  column np
    m = 4*np-5
ce: if (i ==0) then
         z=mi(1,m) + s*mi(np,m)                            ! chk OK
    elseif (i == np) then
        z =    mi( n1,m) + s*mi(2*n1,m) + (-1. + 2.*s)* mi(3*n1,m) + (-2. + 3.*s)* mi(4*n1,m)  !
    else
        z=            mi(i,m) +    s          *mi(n1+i ,m) + s2*mi(2*n1+i,m) + s3*mi(3*n1+i,m) ! OK
endif ce
   theRow(np)=-z

! intermediate columns  I is segment no, C is column number
dc:  do c=2,n1
cn:   if (i ==0) then
            z=  mi(1, 4*(c-1)-1) + mi( 1, 4*(c-1))                    + s* (mi(np, 4*(c-1)-1 )+mi( np, 4*(c-1)))  ! checked OK

     elseif (i == np) then
            z=  mi( n1,4*c-5) + mi(n1,4*c-4) + s*( mi(2*n1,4*c-5) +   mi(2*n1, 4*c-4)) + (-1.+ 2.*s)*(  mi(3*n1, 4*c-5)      +mi( 3*n1,4*c-4)) + (-2. + 3.*s)*(mi(  4*n1, 4*c-5) +mi(  4*n1, 4*c-4))
     else
            z =  mi(i,4*c-5) + mi(i,4*c-4)  + s* (mi(i+n1,4*c-5) + mi (i+n1, 4*c-4))   + s2*(  mi(2*n1+i, 4*c-5) + mi (2*n1 +i , 4*c-4) )   + s3*(  mi(3*n1 +i, 4*c-5) + mi( 3*n1 +i , 4*c-4) )! OK
    endif cn
    theRow(c)=-z
    enddo dc

  end associate
end  function SlaveCoefficients



function FSplineJMatrixC(this,d,origin,ss) result(j)
      implicit none
! parameters
      class (fsplineCubic),intent(in) :: this
#ifdef NO_DOFOCLASS
           type (dofo),intent(inout) ::d  ! or class
#else
           class (dofo), intent(inout) ::d ! or type
#endif
      real(kind=double) ,intent(in) ::ss
      real(kind=double) ,intent(inout),dimension(:) ::origin
!result
      real(kind=double), pointer, dimension(:,:):: J
      write(*,*) ' lets not use FSplineJMatrixC'
      if(this%Sliding) then
        J =>this%FSplineJMatrixSlideC(d,d%origin, ss)
      else
        J =>this%FSplineJMatrixNoSlideC(d,d%origin, ss)
      endif
end  function FSplineJMatrixC


function  FSplineJMatrixMultipleC(this,d,origin,ss) result(j)! SS are the spline parameters of the slaves
      implicit none
! parameters
       class (fsplineCubic),intent(in) :: this
#ifdef NO_DOFOCLASS
           type (dofo),intent(inout) ::d  ! or class
#else
           class (dofo), intent(inout) ::d ! or type
#endif
      real(kind=double),dimension(:) ,intent(in) ::ss
      real(kind=double) ,intent(inout),dimension(:) ::origin
!result
      real(kind=double), pointer, dimension(:,:):: J

      if(this%Sliding) then
         J =>FSplineJMatrixMultSlide(this,d,origin, ss)
      else
         J =>FSplineJMatrixMultNoSlide(this,d,origin, ss)
      endif
end  function FSplineJMatrixMultipleC

 function FSplineJMatrixMultNoSlide(this,d,origin, ss) result(j)
 ! For the J matrix, the size is (3* no spline pts + 3) by (3* no spline pts))
! the first (npts) 3x3s relate the spline points.  The last row relates the slave point
! if it is sliding, there is one last column  relating to its T-parameter
    use vectors_f
    implicit none
! parameters
       class (fsplineCubic),intent(in) :: this
#ifdef NO_DOFOCLASS
           type (dofo),intent(in) ::d  ! or class
#else
           class (dofo), intent(in) ::d ! or type
#endif
      real(kind=double),dimension(:) ,intent(in) ::ss
      real(kind=double) ,intent(inout),dimension(:) ::origin
!result
      real(kind=double), pointer, dimension(:,:):: J
!locals
                real(kind=double), pointer, dimension(:)::  coeffs
                real(kind=double), dimension( 3) :: slavesOrigin  ! non-unitized
                real(kind=double)  :: k
                integer:: i,np, r,nslaves,L

                np =  npoints(this) !ubound(this%m_owner String%m_pts,1)
                 nslaves =ubound(ss,1)
                allocate(j(3*np+3*nslaves,3*np)); j=0.

            ! fill in the master node portions.
                do i = 1,np
                    r = 3*i-2
                    j(r:r+2,r:r+2) = Identity(3)
                enddo
            write(*,*) ' TODO: step FSplineJMatrixMultNoSlide'
! now fill in the  slave rows of J
             r = 3*np+1
      LL:    do L = 1,nslaves
                SlavesOrigin= this%FsplineEvaluate(ss(L))

                coeffs=>this%SlaveCoefficients(ss(L))
                do i = 1,np
                    k = coeffs(i)
                    J(r:r+2,3*i-2:3*i) = k* Identity(3)
                    slavesOrigin = slavesOrigin - k* d%t(3*i-2:3*i)
                enddo
                Origin(r:r+2)  =  slavesOrigin
                deallocate(coeffs)
                r=r+3;
          end do LL
end function FSplineJMatrixMultNoSlide

 function FSplineJMatrixMultSlide(this,d,origin, ss) result(j)
 ! For the J matrix, the size is (3* no spline pts + 3) by (3* no spline pts))
! the first (npts) 3x3s relate the spline points.  The last rows relates the slave points
! if it is sliding, the last columns  relating to the T-parameters of the slaves
    use vectors_f
    implicit none
! parameters
       class (fsplineCubic),intent(in) :: this
#ifdef NO_DOFOCLASS
           type (dofo),intent(in) ::d  ! or class
#else
           class (dofo), intent(in) ::d ! or type
#endif
      real(kind=double),dimension(:) ,intent(in) ::ss  ! curve-parameters of te slave pts
      real(kind=double) ,intent(inout),dimension(:) ::origin
!result
      real(kind=double), pointer, dimension(:,:):: J
!locals
          real(kind=double), pointer, dimension(:)::  coeffs
          real(kind=double), dimension( 3) :: deriv,splinept ,slavesOrigin  ! non-unitized
          real(kind=double)  :: k
          integer:: i,np, r,c,nslaves,L

          np =  npoints(this)
           nslaves =ubound(ss,1)
          allocate(j(3*np+3*nslaves,3*np+nslaves)); j=0.

      ! fill in the master node portions.
          do i = 1,np
              r = 3*i-2
              j(r:r+2,r:r+2) = Identity(3)
          enddo

      ! now fill in the  slave rows of J
       r = 3*np+1
       c = 3*np + 1
LL:    do L = 1,nslaves
          deriv =  this%derivativebyds(ss(L)) ! this will be WRONG when the node thinks THIS is its master dofo.
          coeffs=>this%SlaveCoefficients(ss(L))
          do i = 1,np
              k = coeffs(i)
              J(r:r+2,3*i-2:3*i) = k* Identity(3)
          enddo
          j(r:r+2,c) = deriv
          deallocate(coeffs)
          r=r+3; c = c + 1
    end do LL
!  finally, calculate the origin

    r = 3*np+1
    c = 3*np + 1
LL2:    do L = 1,nslaves
       deriv =  this%derivativebyds(ss(L))
       splinept =this%FsplineEvaluate(ss(L))
       SlavesOrigin=splinept

       coeffs=>this%SlaveCoefficients(ss(L))
       do i = 1,np
           k = coeffs(i)
           slavesOrigin = slavesOrigin - k* d%t(3*i-2:3*i)
       enddo
       j(r:r+2,c) = deriv
       slavesOrigin  =  slavesOrigin - deriv * d%t(c) ! ss(L)
       Origin(r:r+2)  =  slavesOrigin
       deallocate(coeffs)
       r=r+3; c = c + 1
 end do LL2


end  function FSplineJMatrixMultSlide

function FSplineJMatrixNoSlideC(this,d,origin,s) result(j)
    use vectors_f
    implicit none
! parameters
      class (fsplineCubic),intent(in) :: this
#ifdef NO_DOFOCLASS
           type (dofo),intent(inout) ::d  ! or class
#else
           class (dofo), intent(inout) ::d ! or type
#endif
      real(kind=double) ,intent(in) ::s
      real(kind=double) ,intent(inout),dimension(:) ::origin
!result
      real(kind=double), pointer, dimension(:,:):: J
      !locals
          real(kind=double), pointer, dimension(:)::  coeffs
          real(kind=double), dimension( 3) :: splinept ,slavesOrigin  ! non-unitized
          real(kind=double)  :: k
          integer:: i,np, r
          np =  npoints(this) ! ubound(this%m_owner String%m_pts,1)

          allocate(j(3*np+3,3*np)); j=0.

      ! fill in the master node portions.
          do i = 1,np
              r = 3*i-2
              j(r:r+2,r:r+2) = Identity(3)
          enddo

      ! now fill in the  slave row of J
          splinept =this%FsplineEvaluate(s)
          SlavesOrigin=splinept
          r = 3*np+1
          coeffs=>this%SlaveCoefficients(s)
          do i = 1,np
              k = coeffs(i)
              J(r:r+2,3*i-2:3*i) = k* Identity(3)
              slavesOrigin = slavesOrigin - k* d%t(3*i-2:3*i)
          enddo
          Origin(r:r+2)  =  slavesOrigin
          deallocate(coeffs)
     write(*,*) 'TODO: STEP FSplineJMatrixNoSlideC';

end function FSplineJMatrixNoSlideC

function FSplineJMatrixSlideC(this,d,origin,s) result(j)
! For the J matrix, the size is (3* no spline pts + 3) by (3* no spline pts))
! the first (npts) 3x3s relate the spline points.  The last row relates the slave point
! there is one last column  relating to its T-parameter

! for the argument Origin.. This is presumed to be the 'origin' member
! of type DOFO.
! we assume that on input the first (3*npts) rows of the origin are
! the last-known-good (aka (wrongly) undeflected) positions of the
! master points.
! These dont get changed.
! the last 3 rows in 'origin'  become
!       Sum over points of (origin-triplet times j(i))
!       Plus  (minus deriv times sin)
    use vectors_f
    implicit none
! parameters
      class (fsplineCubic),intent(in) :: this
#ifdef NO_DOFOCLASS
           type (dofo),intent(inout) ::d  ! or class
#else
           class (dofo), intent(inout) ::d ! or type
#endif
      real(kind=double) ,intent(in) ::s
      real(kind=double) ,intent(inout),dimension(:) ::origin
!result
      real(kind=double), pointer, dimension(:,:):: J
!locals
    real(kind=double), pointer, dimension(:)::  coeffs
    real(kind=double), dimension( 3) :: deriv,splinept ,slavesOrigin  ! non-unitized
    real(kind=double)  :: k
    integer:: i,np, r
    np =  npoints(this) ! ubound(this%m_owner String%m_pts,1)

    allocate(j(3*np+3,3*np+1)); j=0.
            write(*,*) ' TODO: step FSplineJMatrixSlideC'
! fill in the master node portions.
    do i = 1,np
        r = 3*i-2
        j(r:r+2,r:r+2) = Identity(3)
    enddo

! now fill in the  slave row of J
    deriv =  this%derivativebyds(s)
    splinept =this%FsplineEvaluate(s)
    SlavesOrigin=splinept
    r = 3*np+1
    coeffs=>this%SlaveCoefficients(s)
    do i = 1,np
        k = coeffs(i)
        J(r:r+2,3*i-2:3*i) = k* Identity(3)
        slavesOrigin = slavesOrigin - k* d%t(3*i-2:3*i)
    enddo
    j(3*np +1:3*np+3,ubound(j,2)) = deriv
    slavesOrigin  =  slavesOrigin - deriv * s
    Origin(r:r+2)  =  slavesOrigin
    deallocate(coeffs)
end  function FSplineJMatrixSlideC

function npts(this) result (np)
        implicit none
        integer :: np
        class (fsplineCubic),intent(in) :: this
        np = ubound(this%m_ownerString%m_pts,1)
end function npts

function Pt(this,i) result (x)
    use fnoderef_f
    use coordinates_f
    implicit none
    class (fsplineCubic),intent(in) :: this
    integer, intent(in) :: i
    real(kind=double), dimension(3) :: x
    type(fnode) , pointer :: theNode
        thenode=>decodenoderef(this%m_ownerString%m_pts(i))
        x=theNode%xxx

!        x = get_Cartesian_X_By_Ptr( saillist(this%m_ownerString%m_pts(i)%m_fnr_Sli  )%nodes%xlist(this%m_ownerString%m_pts(i)%m_fnrn) )
end function Pt

function createStringSplineC(this, t)result(ok)
    USE LAPACK95 , ONLY: gesv,getrf,getri
   implicit none
   class (fsplineCubic) :: this
   type (string) ,pointer ::t
!locals
    real(kind=double), allocatable, dimension(:,:) ::m,rhs
    integer, allocatable, dimension(:) ::ipiv
    integer::ok,istat,neq,np,r,c,n1,k

    this%m_ownerString=>t
    this%degree = 3
    this%m_np= ubound(t%m_pts,1); np = this%m_np
    ok = this%CreateArcLengthKnots()


! a is the array of polynomial coefficients for the piecewise cubic.
! first index is xyz
! second index is the power of s, from 0 to 3
! 3rd index is which span. There is one less spn than number of points
    deallocate(this%a,stat=istat)
    allocate(this%a(3,0:3,this%m_np-1),stat=istat)

!  generate our equation system and solve it 3 times, once for each X ordinate
! our equation set is (4 np -4) dimension
    neq = 4* this%m_np -4
        allocate(rhs( neq,3))
        allocate(m(neq,neq)); m=0
       allocate(ipiv(neq))
! first eqns
    m(1,2*np-1) = -2.0d00;          m(1,3*np-2) = -6.0d00*this%kt(1)
    m(2,1)              = -1.0d0;
    m(2, np)            = -this%kt(1) ;
    m(2,2*np-1)         = -this%kt(1)**2;
    m(2,3*np-2)         = -this%kt(1)**3
! last eqns
    m(neq-1,np-1)       = -1.0d0;
    m(neq-1, 2*np-2)    = -this%kt(np) ;
    m(neq-1,3*np-3)     = -this%kt(np)**2;
    m(neq-1,neq)        = -this%kt(np)**3
    m(neq,3*np-3) = -2.0d00;    m(neq,neq) = - 6.0d00*this%kt(np)

! equations for each internal point. there are 4*np -8
r = 2; c=0; n1 = np-1
    do k=2,np-1
    r=r+1; c=c+1
        m(r,  c)      =  -1;
        m(r,  c+n1)   =  -this%kt(k)
        m(r,  c+2*n1) =  -this%kt(k)**2
        m(r,  c+3*n1) =  -this%kt(k)**3
    r=r+1; c=c+1
        m(r,  c)      =  -1;
        m(r,  c+n1)   =  -this%kt(k)
        m(r,  c+2*n1) =  -this%kt(k)**2
        m(r,  c+3*n1) =  -this%kt(k)**3
    r=r+1; c=c-1
        m(r,  c+ 2*n1)   = -2.0d00;
        m(r,  c+ 2*n1+1) =  2.0d00;
        m(r,  c+ 3*n1)   = -6.0d00* this%kt(k);
        m(r,  c+ 3*n1+1) =  6.0d00* this%kt(k);
    r=r+1
        m(r,  c+n1) =  -1.0d00;
        m(r,  c+np) =  1.0d00;
        m(r,  c+2*n1 )   = -2.0d00* this%kt(k);
        m(r,  c+2*n1+1 ) =  2.0d00* this%kt(k);
        m(r,  c+3*n1 )   = -3.0d00* this%kt(k)**2;
        m(r,  c+3*n1+1 ) =  3.0d00* this%kt(k)**2;
    enddo
! now the rhs
    rhs=0
    rhs(2,1:3) = this%pt(1)
    rhs(neq-1,1:3) = this%pt(np)
    r = 2
   do k=2,np-1
         r=r+1;    rhs(r,1:3) = this%pt(k)
         r=r+1;    rhs(r,1:3) = this%pt(k)
         r=r+1;    rhs(r,1:3) = 0.0d00
         r=r+1;    rhs(r,1:3) = 0.0d00
    enddo

! Change sign because our original formulation using CoefficientArrays gave the negative of LinearSolve
    rhs = - rhs
    if(allocated(this%mi  )) deallocate(this%mi,stat=istat)
    allocate(this%mi(neq,neq));
    this%mi=m
    ipiv=0
    call getrf(this%mi,ipiv)
    call getri(this%mi,ipiv)  ! we now have Mi - inverse of M
    deallocate(rhs,m,ipiv)
    call recalc_Amatrix(this)
end  function createStringSplineC

! When we evaluate the spline the A-coefficients need to be up to date.
! So in-between changing a CV and evaluating the spline we have to call this procedure
! The easy way is to call it before each evaluation but that is not threadsafe.

! the correct way would be to call it after each block of code that changes the coordinates
! ie an implicit cycle, a DR integration, a model transformation, or ANY interaction that changes the models.
! is there a On-Coordinates-might-have-changed procedure anywhere?

! If we were to call it from evaluate, we could make it faster by adding a WhichSpan argument


subroutine recalc_Amatrix(this)
   implicit none
   class (fsplineCubic) :: this
!locals
    real(kind=double), allocatable, dimension(:,:) :: rhs
    integer::neq,np,r,k,i
    real*8 x(3)

    np = this%m_np
    neq = 4* this%m_np -4
    allocate(rhs( neq,3))

    rhs=0
    rhs(2,1:3) = this%pt(1)
    rhs(neq-1,1:3) = this%pt(np)
    r = 2
     do k=2,np-1
           r=r+1;    rhs(r,1:3) = this%pt(k)
           r=r+1;    rhs(r,1:3) = rhs(r-1,1:3)! this%pt(k)
           r=r+1;    rhs(r,1:3) = 0.0d00
           r=r+1;    rhs(r,1:3) = 0.0d00
      enddo

  ! Change sign because our original formulation using CoefficientArrays gave the negative of LinearSolve
     rhs = - rhs
     rhs = matmul(this%mi,rhs)

      r=0
     do k=0,3
       do i = 1,np-1
          r=r+1
          x= rhs(r,1:3)
          this%a(1:3,k,i) = x
      enddo
      enddo
    deallocate (rhs)

end subroutine recalc_Amatrix

function CreateArcLengthKnots(this) result (ok)
    use saillist_f
    use coordinates_f
        implicit none
        class (fsplineCubic) :: this
        integer i,ok,istat
        real(kind=double), dimension(3) :: x,xlast
        real(kind=double)   :: tot
        ok=0
        deallocate(this%kt,stat=istat)
        allocate(this%kt(this%m_np),stat=istat)
        ok=1
        ! can we use method Pt here???
        xlast = this%Pt(1)  ! get_Cartesian_X_By_Ptr( saillist(this%m_owner String%m_pts(1)%m_fnr_Sli  )%nodes%xlist(this%m_owner String%m_pts(1)%m_fnrn) )
        this%kt(1) =   0.0d00
        do i=2,this%m_np
            x = this%Pt(i) ! get_Cartesian_X_By_Ptr( saillist(this%m_owner String%m_pts(i)%m_fnr_Sli  )%nodes%xlist(this%m_owner String%m_pts(i)%m_fnrn) )
            this%kt(i) =   this%kt(i-1)  + sqrt(sum((x-xlast)**2))
            xlast=x
        enddo
        tot = this%kt(this%m_np)
        this%kt = this%kt/tot
        this%kt(this%m_np )=1.0d00
end function CreateArcLengthKnots

subroutine mprintd(u, s,x)
    USE basictypes_f
    implicit none
    integer, intent(in) ::u
    character(len=*),intent(in) :: s
    real(kind=double), intent(in), dimension(:,:):: x
    real(kind=double) :: mx
    character(len=64) :: buf
    integer ::m,n,i,k;
   ! if (.not. dbgprnt) return
    mx=maxval(abs(x))
    m = ubound(x,1); n = ubound(x,2)
    if(n.gt. 32) then
        write(buf,*,iostat=i)'(a1,i2.2,',n,'(1x,g9.2))'
    else
        write(buf,*,iostat=i)'(a1,i2.2,',n,'(1x,g13.5))'
    endif

    write(u,'(/a,a,i4,a,i4)',iostat=k) trim(s) ,' ',m,' by ',n
    if(i .ne. 0) then
      write(u,*) ' cannot write matrix'; return
    endif
    do i=1,m
    write(u,buf,iostat=k)'r',i, x(i,1:n);
    enddo
    if(m>0 .and. n>0) write(u,*,iostat=k) 'max is ' ,mx
end subroutine mprintd

subroutine ClearFsplineC(this)
    class(fsplineCubic) ::this ! Intel 2015 compained - no intent(in)
    integer::alloc_err
    this%m_np=-1
    this%degree=-1; this%m_type=-1
!    write(outputUnit,*) ' ClearFsplineC '  ; Flush(outputUnit)
    if(associated(this%m_ownerString)) this%m_ownerString%m_fspline=>NULL();
    this%m_ownerString =>NULL()
    deallocate(this%kt  ,stat=alloc_err)
    deallocate(this%localCVs  ,stat=alloc_err)
    deallocate(this%a  ,stat=alloc_err)
     deallocate(this%mi  ,stat=alloc_err)
end subroutine ClearFsplineC


RX_PURE recursive function FsplineEvaluateC(this,s) result(x) ! linear interpolation outside the domain
    implicit none
    class (fsplineCubic), intent(in) ::this
    real(kind=double), intent(in) :: s
! returns
    real(kind=double),dimension(3)  :: x
!locals
    real(kind=double)  :: ps  ! power of s
    integer ::i,k,ns
    ns = this%m_np-1

    call recalc_Amatrix(this) ! not thread-safe. but its atest

   if(s<=this%kt(1)) then
        x = this%a(1:3,0,1) + s * this%a(1:3,1,1)
    else if (s>=this%kt(this%m_np)) then
        x=  this%a(1:3, 0, ns) + s*this%a(1:3, 1, ns) + (-1.0d0 + 2.d0*s)* this%a(1:3, 2, ns) + (-2.d0 + 3.d0*s) *this%a(1:3, 3, ns)
    else
        i = this%WhichSpan(s);
        if(i .lt. 1) then
             write(*,*) '(eval) but I is <1 :i=',i
            i=1
         else if (i .ge. this%m_np) then
                      write(*,*) '(eval) but I is >=np ',i,">=",this%m_np
            i = this%m_np-1
        endif

        x = this%a(1:3,0,i)
        ps = s
        do k=1,3
            x = x + ps * this%a(1:3,k,i)
            ps = ps * s
        enddo
      endif
end  function FsplineEvaluateC

RX_PURE function derivativebydsC(this,sin) result(x)
    implicit none
    class (fsplineCubic), intent(in) ::this
    real(kind=double), intent(in) :: sin
    real(kind=double),dimension(3)  :: x
    !locals
        real(kind=double)  ::s, ps  ! power of s
        integer ::i,k,ns
        ns = this%m_np-1
        s=max(sin,0.0); s = min(s,1.0d0)

        call recalc_Amatrix(this) ! not thread-safe. but its atest

        i = this%WhichSpan(s) ;
        if(i .lt. 1) then
              s = this%kt(1)
              i=1
        else if (i>ns) then
            s = this%kt( this%m_np)
            i=ns
        endif

        x=0.0d00
        ps = 1.0d00
        do k=1,3
            x = x + ps * this%a(1:3,k,i)*(dble(k))
            ps = ps * s
        enddo

end function derivativebydsC

function FindNearestC(this,q) result(t)
        implicit none
        class (fsplineCubic),intent(in) :: this
        real(kind=double),dimension(:) ,intent(in) ::q
        real(kind=double) ::t
! locals
        logical :: stillgoing
        real(kind=double),dimension(3)  ::x,dx
        real(kind=double)   ::dmin , d ,dt ,damp
        integer ::imin  , i,count
        damp =1.0
        x = this%Pt(1)
!  first a rough guess. Find the closest CV
        dmin = sum((x-q)**2)
        imin=1
        do i = 2,this%m_np
           x =  this%Pt(i)
           d = sum((x-q)**2)
           if(d < dmin) then; dmin=d; imin=i; endif
       enddo
        t = real(imin-1)/real(this%m_np-1) ! a lousy guess
! now newton-raphson.
        count=1;
         stillgoing= .true.
         do while (stillgoing)
            x = FsplineEvaluateC(this,t)
            dx = derivativebydsC(this,t)
            dt = Dot_Product(q-x,dx)/ sum(dx**2)
            t=t+dt*damp
            if(abs(dt) .le. 1.d-7) stillgoing=.false.
            if( mod(count,100) .eq.0) damp=damp/2.0
             if(count > 1000 ) then
             write(outputunit,*)' fspline C find-nearest not converged'
                 t =  real(imin,8)/real(this%m_np,8)  ! use the lousy guess
                   exit
               endif
               count=count+1
         end do
end function FindNearestC

function ListFsplineC(this,u,fullprint) result(ok)
    implicit none

    integer,intent(in) ::u
    class (fsplineCubic)  :: this
    logical, intent(in),optional :: fullprint
    real(kind=double),dimension(3)  :: x  ,d,dlast
    real(kind=double) :: tmin,tmax ,lastt   ,s,y ,ds !,zns
    real(kind=double),dimension(:) ,pointer ::slaveCoeffs
    integer:: i,ok,istat
    character*64  fmt
    ok=1
    write(u,*) ' fspline-Cubic '
    write(u,*) '  np,degree,m_type   ',this%m_np,this%degree,this%m_type
    write(u,*) ' Sliding =',this%sliding   ,'Interpolatory=',this%Interpolatory
    if(associated(this%m_ownerString))then; write(u,*) ' Has ownerString   '  ; else; write(u,*) ' no ownerString'; endif

    tmin = minval(this%kt)+RXEPSILON; tmax=maxval(this%kt)-RXEPSILON;
    write(u,*) ' knots from  ' ,tmin,' to ', tmax
    lastt = this%kt(1)
    do i=1,ubound(this%kt,1)
       write( u,'(i3, 2g15.5,L8)')i, this%kt(i)   ,  this%kt(i)-lastt , (this%kt(i).ge. lastt )
       lastt =     this%kt(i)
    enddo
    write( u,*)' CVs'
    do i = 1,this%m_np
        x = this%Pt(i)
        write(u,'(i5,3f14.5 )',iostat=istat) i,x
    enddo
  ! as a test sample at ends
  !     ns=50; zns=real(ns)
       write(u,*) ' sampling'
       write(u,*) '   I     s       x1     x2     x3    deriv1    ...2   ....3 '
       do i = -5,5
           s = tmin + 0.1*real(i); y=s; x = this%FsplineEvaluate(s); d = this%derivativebyds(s)
           !          i      y
           write(u,'(i4,1x,g16.9,6(1x,g16.9))',iostat=istat) i,y,x,d
    enddo
       do i=-10, 10
            s =  tmin   +  RXEPSILON * real(i) /2.0d00; y=s; x = this%FsplineEvaluate(s);
            d = this%derivativebyds(s)
            !          i      y
            write(u,'(i4,1x,g16.9,6(1x,g16.9))',iostat=istat) i,y,x,d
       enddo
       dlast=d
! at each knot
       do i=1,ubound(this%kt,1)
            s =  this%kt(i);  y=s;   x = this%FsplineEvaluate(s)
            d = this%derivativebyds(s)
            if (dot_product(d,dlast)<=0) then
             write(u,*)'WARNING CVS ZIGZAG. USE A FINER GRID '
             endif
             dlast=d
            !          i      y
            write(u,'(i4,1x,g16.9,6(1x,g16.9))',iostat=istat) i,y,x,d
       enddo

! at the end

       do i=-10, +10
             s =  tmax  +  RXEPSILON * real(i) /2.0d00; y=s; x = this%FsplineEvaluate(s)
             d = this%derivativebyds(s)
             write(u,'(i4,1x,g16.9," + ",g16.9,6(1x,g16.9))',iostat=istat) i,tmax,y-tmax,x,d
       enddo
 ! around the end
     do i = -5,5
         s = tmax + 0.1*real(i); y=s; x = this%FsplineEvaluate(s)
         d = this%derivativebyds(s)
         write(u,'(i4,1x,g16.9,6(1x,g16.9))',iostat=istat) i,y,x,d
    enddo
fp1: if( present(fullprint))  then
fp2:  if( fullprint) then

        write(u,*) 'big dump'
        ds = (tmax-tmin)/100.
            s = (tmax + tmin)/2. - 100.* ds
            do i = 1,201
                y=s;  x = this%FsplineEvaluate(s)
                d = this%derivativebyds(s)
                write(u,'(i6,1x,g16.9,7(1x,g16.9))',iostat=istat) i,y,x,d ,s
                s = s + ds
           enddo

        write(u,*) 'polynomial coefficients '
        call mprintD(u, ' In X' ,this%a(1,:,:))
        call mprintD(u, ' In Y' ,this%a(2,:,:))
        call mprintD(u, ' In Z' ,this%a(3,:,:))
       endif fp2
       endif fp1

!    call mprintD(u, 'Minv ' ,this%mi)

    write(u,*) ' testing the slave coeffs'
    write(u,*) ' the knots again'
    do i=1,ubound(this%kt,1)
       write( u,'(i4, g15.5)')i, this%kt(i)
    enddo
    write(fmt,'(a,i3,a  ) ' )     " (i6,1x,g16.9,",this%m_np,'(1x,g16.9)) '
    ds = (tmax-tmin)/13.
        s = (tmax + tmin)/2. - 13.* ds
        do i = 0,26
            slaveCoeffs=>this%SlaveCoefficients(s)
            write(u,fmt,iostat=istat) i,s, slaveCoeffs
            deallocate(slaveCoeffs )
            s = s + ds
       enddo
     write(u,*)

 end function ListFsplineC

function WhichSpan(this,s) result(i)
     use basictypes_f
     implicit none
      class (fsplineCubic),intent(in) :: this
      real(kind=double), intent(in) :: s
      integer i
! span 0 is s<= knot(1)  - the first knot
! span i is knot(i) < s <= knot(i+1)
! span np is s >  knot(m_np) - the last knot

     do i = 0, this%m_np-1
         if (s <= this%kt(i+1)) return
     enddo
      i = this%m_np
end function WhichSpan

end module fsplinecubic_f

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! fsplineCubicNS is a free-standing cubic spline designed for interpolating given points
! it holds its own master points and accesses them via an over-ridden function Pt()

module fsplinecubicNoString_f
    use basictypes_f
    use fsplinecubic_f
    use ftypes_f
    implicit none

type ,extends(fsplinecubic) :: fsplineCubicNS   ! the base type
   real(kind=double), allocatable, dimension(:,:) :: xin
    contains
 !inherited methods
             procedure ,pass(this)  :: ClearFspline => ClearFsplineNS
             procedure ,pass(this)  :: Pt =>PtNS
             procedure ,pass(this)  :: npoints =>nptsNS
!locals
             procedure ,pass(this)  :: createStringSplineNS
end type fsplineCubicNS

contains


SUBROUTINE fcspline(Pin,ng,p,npo) bind(C,name="rxfcspline_" )
    use basictypes_f  ! for outputunit
    use, intrinsic :: ISO_C_BINDING, only: C_INT, C_FLOAT
    IMPLICIT NONE
    INTEGER(C_INT),intent(in),value :: ng
    INTEGER(C_INT),intent(in),value :: npo
    REAL(C_FLOAT) ,intent(in), dimension (3,ng) ::Pin
    REAL(C_FLOAT) ,intent(out),dimension (3,npo) ::p
    type (fsplineCubicNS) :: fc
    integer::ok,i
    real(kind=double) :: s
    real(kind=double),dimension(3) :: x

    ok=fc%createStringSplineNS(Pin)

     do i = 1,npo
           s = dble(i-1)/dble(npo-1) ;
           x = fc%FsplineEvaluate(s) ! in fcspline
           p(:,i) = x
    enddo
! remember to make the 1st point = inputpt1 and last = inputptLast
    p(:,1)   = pin(:,1)
    p(:,npo) = pin(:,ng)
!  (3) clean up
 call ClearFsplineNS(fc )

end  SUBROUTINE fcspline

function  createStringSplineNS(this,pp) result (ok)
    USE LAPACK95 , ONLY: gesv,getrf,getri
    implicit none
    class  (fsplineCubicNS),intent(inout) :: this
    REAL*4 ,intent(in), dimension (:,:) ::pp
    integer :: ok
!locals
    real(kind=double), allocatable, dimension(:,:) ::m,rhs
    integer, allocatable, dimension(:) ::ipiv
    integer:: istat,neq,np,r,c,n1,k


    allocate(this%xin(3,ubound(pp,2)),stat=ok)
    if(ok) then
          ok=0
          return
    endif
    this%xin = pp
    ok=1

    this%m_ownerString=>NULL()
    this%degree = 3
    this%m_np= ubound(this%xin,2); np = this%m_np

    ok = this%CreateArcLengthKnots()

! a is the array of polynomial coefficients for the piecewise cubic.
! first index is xyz
! second index is the power of s, from 0 to 3
! 3rd index is which span. There is one less spn than number of points
    deallocate(this%a,stat=istat)
    allocate(this%a(3,0:3,this%m_np-1),stat=istat)

!  generate our equation system and solve it 3 times, once for each X ordinate
! our equation set is (4 np -4) dimension
    neq = 4* this%m_np -4
        allocate(rhs( neq,3))
        allocate(m(neq,neq)); m=0
       allocate(ipiv(neq))
! first eqns
    m(1,2*np-1) = -2.0d00;          m(1,3*np-2) = -6.0d00*this%kt(1)
    m(2,1)              = -1.0d0;
    m(2, np)            = -this%kt(1) ;
    m(2,2*np-1)         = -this%kt(1)**2;
    m(2,3*np-2)         = -this%kt(1)**3
! last eqns
    m(neq-1,np-1)       = -1.0d0;
    m(neq-1, 2*np-2)    = -this%kt(np) ;
    m(neq-1,3*np-3)     = -this%kt(np)**2;
    m(neq-1,neq)        = -this%kt(np)**3
    m(neq,3*np-3) = -2.0d00;    m(neq,neq) = - 6.0d00*this%kt(np)

! equations for each internal point. there are 4*np -8
r = 2; c=0; n1 = np-1
    do k=2,np-1
    r=r+1; c=c+1
        m(r,  c)      =  -1;
        m(r,  c+n1)   =  -this%kt(k)
        m(r,  c+2*n1) =  -this%kt(k)**2
        m(r,  c+3*n1) =  -this%kt(k)**3
    r=r+1; c=c+1
        m(r,  c)      =  -1;
        m(r,  c+n1)   =  -this%kt(k)
        m(r,  c+2*n1) =  -this%kt(k)**2
        m(r,  c+3*n1) =  -this%kt(k)**3
    r=r+1; c=c-1
        m(r,  c+ 2*n1)   = -2.0d00;
        m(r,  c+ 2*n1+1) =  2.0d00;
        m(r,  c+ 3*n1)   = -6.0d00* this%kt(k);
        m(r,  c+ 3*n1+1) =  6.0d00* this%kt(k);
    r=r+1
        m(r,  c+n1) =  -1.0d00;
        m(r,  c+np) =  1.0d00;
        m(r,  c+2*n1 )   = -2.0d00* this%kt(k);
        m(r,  c+2*n1+1 ) =  2.0d00* this%kt(k);
        m(r,  c+3*n1 )   = -3.0d00* this%kt(k)**2;
        m(r,  c+3*n1+1 ) =  3.0d00* this%kt(k)**2;
    enddo
! now the rhs
    rhs=0
    rhs(2,1:3) = this%pt(1)
    rhs(neq-1,1:3) = this%pt(np)
    r = 2
   do k=2,np-1
         r=r+1;    rhs(r,1:3) = this%pt(k)
         r=r+1;    rhs(r,1:3) = this%pt(k)
         r=r+1;    rhs(r,1:3) = 0.0d00
         r=r+1;    rhs(r,1:3) = 0.0d00
    enddo

! Change sign because our original formulation using CoefficientArrays gave the negative of LinearSolve
    rhs = - rhs
    if(allocated(this%mi  )) deallocate(this%mi,stat=istat)
    allocate(this%mi(neq,neq));
    this%mi=m
    ipiv=0
    call getrf(this%mi,ipiv)
    call getri(this%mi,ipiv)  ! we now have Mi - inverse of M

    deallocate(rhs,m,ipiv)
    call recalc_Amatrix(this)

end function  createStringSplineNS

subroutine ClearFsplineNS(this)
    use fsplinecubic_f
    class(fsplineCubicNS)  ::this ! Intel 2015 compained - no intent(in)
    integer::alloc_err
    deallocate(this%xin  ,stat=alloc_err)
    call ClearFsplineC(this)
end subroutine ClearFsplineNS

function PtNS(this,i) result (x)
 !  use saillist_f
 !   use coordinates_f
        implicit none
        class (fsplineCubicNS),intent(in) :: this
        integer, intent(in) :: i
        real(kind=double), dimension(3) :: x
        x = this%xin(:,i)
end function PtNS

function nptsNS(this) result (np)
      implicit none
! parameters
    integer :: np
    class (fsplineCubicNS),intent(in) :: this
    np = ubound(this%xin,1)
end function nptsNS


end module fsplinecubicNoString_f
