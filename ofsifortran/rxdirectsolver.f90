module myprint_f
contains
subroutine myprint(a, s)
     USE, INTRINSIC :: ISO_C_BINDING
     implicit none
    real (c_double), dimension(:,:), intent(in) :: a

    integer i,j, nr,nc
    character *(*)  s
    write(*,*)  trim(s)
    nr = ubound(a,1); nc = ubound(a,2)
    do i = 1,nr
    do j = 1,nc
        write(*,'(1x,f14.4,\ )') a(i,j)
    enddo
    write(*,*)
    enddo


end subroutine myprint
end module myprint_f

module mybits
contains
function bandCondition (ab,kl,ku,nr) result (rv)
! returns the lapack estimate of condition number for a banded matrix.
    use lapack95  ! was  mkl95_lapack
    USE, INTRINSIC :: ISO_C_BINDING

     implicit none

    real(c_DOUBLE), dimension(:,:), intent(in):: ab
    integer,intent(in) :: kl,ku,nr

    real(c_DOUBLE):: rv

!locals
    integer, dimension(:), allocatable:: ipiv;

    real(c_DOUBLE), dimension(:,:), allocatable:: abd
    integer info,istat,ldab,j
    real(c_DOUBLE):: anorm = 1
#ifdef WRITEMATRIXTOO

#endif

    rv=0
    ldab = ubound(ab,1); j = ubound(ab,2)
    allocate(abd(ldab,j), stat =istat);
    allocate(ipiv(nr), stat =istat);
    abd=ab
    anorm = maxval(sum(abd,dim=2))

    if(ldab >= 2*kl + ku + 1)  then
        call dgbtrf( nr, nr, kl, ku, abd, ldab, ipiv, info )
        if(info /=0)then
            write(*,*)' dgbtrf (LU factorization) in function bandCondition  gave ',info
            write(*,*) '(If info = 0, the execution is successful.'
            write(*,*) 'If info = -i, the i-th parameter had an illegal value.'
            write(*,*)'If info = i, uii is 0.  U is exactly singular.'
        endif
        call gbcon( abd, ipiv, anorm, rv ,kl ,'1',info  )
        write(*, "(' norm is ',en13.4, ' rcond= ',en13.4,'  info= ',i4  )",iostat=istat  ) anorm,rv,info
        if(info /=0)  write(*,*) ' WHY DID GBCON RETURN AN ERROR??????????????'
    else
         write(*,*) ' ldab, kl,kucv',ldab,kl,ku
    endif


#ifdef WRITEMATRIXTOO
!  write the matrix after condition no check
   call  bndcsr (n,abd,nabd,lowd,ml,mu,a,ja,ia,len,ierr)
   call  prtmt (nrow,ncol,a,ja,ia,rhs,"  ","As condition no was measured" ,key,type,13,2,23)
    deallocate(a,ia,ja)
#endif
    deallocate(ipiv,abd)
end function bandCondition

end module mybits

!extern "C" int cf_solveDirect(int *ia, int*ja,              must match  f90_to_c.h
!             double*values  , const int nnzero,const int nr,const int bw,
!               int *perm, double* rhs,double* result  );
!#define FPRINTING

function solveDirect(ia, ja,a,nnzero,nr,bw, perm,rhs,result  )result( err)
!DEC$ ATTRIBUTES  C,DECORATE, REFERENCE  ,ALIAS:'cf_solveDirect':: solveDirect

    USE, INTRINSIC :: ISO_C_BINDING
    use myprint_f
    use sparskit
    use lapack95
    use mybits
    implicit none
! return value =the info value from the lapack solver (0 for success)
! the matrix (a,ia,ja,nr)  is a structurally symmetric matrix in CSC form. ie the transpose of CSR
! CAREFUL  !  this function overwrites (a,ia,ja,nr)   with its transpose

     integer(C_INT), dimension(nr+1)   , intent(inout) :: ia
     integer(C_INT), dimension(nnzero) , intent(inout) :: ja
     real(c_DOUBLE), dimension(nnzero),  intent(inout) :: a
     integer(C_INT), intent(in),    value      :: nnzero, nr, bw
     integer(C_INT), dimension(nr), intent(in) :: perm      ! permutation matrix
     real(C_DOUBLE), dimension(nr), intent(in) :: rhs
     real(C_DOUBLE), dimension(nr), intent(out):: result

     integer(c_int) ::err

!  locals
    integer:: istat, job,info,i,j
    integer ::nrhs
    integer(C_INT), dimension(:) , allocatable:: jao,iao
    integer , dimension(:) , allocatable::  ipiv
    real(c_DOUBLE), dimension(:), allocatable::  ao, delta,x
    integer:: ml,mu,ierr,nabd,lowd
    real(c_DOUBLE), dimension(:,:), allocatable, target:: abd,rhs2d
    real(c_DOUBLE), dimension(:), pointer:: TheDiagonal   ! an alias
#ifdef FPRINTING
    character*72 title
     integer , dimension(:) , allocatable::   ipv
    real(c_DOUBLE), dimension(:), allocatable::  vr,vc
    character*1 equed,fact,trans
    logical:: expert
    real(c_double) ::rcond
#endif
    real(c_double) ::fmax,  cond,deltadiag, desiredRcond,lastfmax

    err = 16
    desiredRcond = 1e-9
    nrhs=1
    write(*,*) 'nr= ',nr, ' nnzero= ',nnzero ,  ' bw= ',bw

!(0) Transpose the input matrix
    allocate(jao(nnzero),stat=istat);if(istat/=0) then;write(*,*) ' alloc failed (1)'; return; endif;
    allocate(iao(nr+1),stat=istat) ;if(istat/=0) then;write(*,*) ' alloc failed (2)'; return; endif;
    allocate(ao(nnzero),stat=istat);if(istat/=0) then;write(*,*) ' alloc failed (3)', nnzero,' doubles'; return; endif;
!    jao=0;
!    iao=0;
 !   ao=0;

#ifdef FPRINTING
    write(*,*) ' input matrix A (CSR)'
    call dump (1,nr,.true.,a,ja,ia,6)
    open(99,file='matinTr.rua')
    title = 'input matrix (tr to CSR)'
    call prtmt (nr,nr,a,ja,ia,rhs,'  ', title,'key     ','RUA',4,2,99)
    close(99)
#endif

! measure the bandwidth

    call getbwd(nr,a,ja,ia,ml,mu)
!    write(*,*) ' lower and upper bandwidths  ', ml,mu
!1) sparskit dperm to permute the matrix.

    job=1
    if(.true.) then ! do the permutation'
        call  dperm (nr,a,ja,ia,ao,jao,iao,perm,perm,job)
    else
       ao=a; jao=ja; iao=ia
    endif

    call getbwd(nr,ao,jao,iao,ml,mu)
   ! write(*,*) ' lower and upper bandwidths after permutation ', ml,mu

!2)  sparskit csrbnd to store in banded form
    job=1           ! csrbnd will calculate ml and mu
    ierr=0
    nabd =  2* ml+mu+1; i = ml; j=mu ! lapack asks for this
    lowd =  nabd
    !write(*,*) 'ml ',ml,' mu', mu ,  '  lowd ',lowd
   ! write(*,*) 'abd dimensions: ',nabd,' by', nr ;
    allocate(abd(nabd,nr), stat=istat); if(istat/=0) then;write(*,*) ' alloc failed (4) abd ',nabd,' by', nr ; return; endif
    abd=0 !-99
    call csrbnd (nr,ao,jao,iao,job,abd,nabd,lowd,ml,mu,ierr)
!   the diagonal is on row (lowd - ml.  CHECKED
    cond = bandCondition (abd,i,j,nr)

 !  if cond is too low we add a constant  delta to the leading diagonal
 !  delta is (desired rcond - measured rcond) * max (diag)

    if( desiredRcond>cond) then
        TheDiagonal=>abd(lowd-ml,:);
        deltadiag = (desiredRcond-cond) * maxval(abs(TheDiagonal))
        TheDiagonal =TheDiagonal+deltadiag
        write(*,'( "ADDED TO DIAGONAL :",g11.4, "(",g11.3,"%) rqd cond=",g11.3 )' )deltadiag, (desiredRcond-cond)*100., desiredRcond
        cond = bandCondition (abd,i,j,nr)
    end if

!3) mkl dgbsv  to solve the system

    allocate(rhs2d(nr,nrhs),stat=istat);if(istat/=0) then;write(*,*) ' alloc failed (5)'; return; endif;
    allocate(ipiv(nr),stat=istat);if(istat/=0) then;write(*,*) ' alloc failed (6)'; return; endif;

     forall (i=1:nr) rhs2d( perm(i),1)=rhs(i)
    lastfmax = maxval(abs(rhs)) *2.0
#ifdef FPRINTING
    write(*,*) ' lower and upper bandwidths now ', ml,mu, 'ierr = ',ierr !   call myprint(abd, "band matrix")!    call myprint(rhs2d, "rhs2D")
#endif

!   If info = 0, the execution is successful.
!   If info = -i, the i-th parameter had an illegal value.
!   If info = i, U(i, i) is exactly zero. The factorization has been completed, but the factor U is exactly singular, so the solution could not be computed.


! simple solver
        call dgbsv( nr, ml, mu, nrhs, abd, nabd, ipiv, rhs2d, nr, info )
        err = info
        if(info .ne. 0) then
             write(*,*) ' dgsv failed . info=',info
             return
        endif

! an iterative improvement
!        err1 = rhs - A. rhs2d(:,1)  ! force units
!        dx = solve( A, err1)
!        x = x + dx

        allocate(delta(nr), stat=istat) ;if(istat/=0) then;write(*,*) ' alloc failed (21)'; return; endif;
        allocate(x(nr), stat=istat) ;if(istat/=0) then;write(*,*) ' alloc failed (22)'; return; endif;

         x = rhs2d(:,1);       write(*,*) 'first estimate: biggest X=', maxval(abs(x(1:nr)))
 doi:   do i= 1,0
            call mkl_dcsrgemv('N', nr, ao, iao, jao, x, delta)  ! x is the current solution. delta = A . x

            forall (i=1:nr) rhs2d( perm(i),1)=rhs(i)                     ! rhs2d(:,1) is the original rhs
!  the error is the difference
           delta = rhs2d(:,1) - delta                                    ! delta now rhs - A.x
!  measure the force error (biggest of err1)
            fmax = maxval(abs(delta));
 !  solve again to put dx in delta

            call csrbnd (nr,ao,jao,iao,job,abd,nabd,lowd,ml,mu,ierr)
            call dgbsv( nr, ml, mu, nrhs, abd, nabd, ipiv, delta, nr, info )   ! delta is now a displacement
            write(*,'("loop ",i3," info=",i5," max force error=",EN12.3,"  max disp this iteration=",EN12.3,"  maxdisp (absolute) =" ,EN12.3 )',iostat=istat) &
                i,info,fmax, maxval(abs(delta)), maxval(abs(x))
            x=x + delta
            if(fmax < 1e-12) exit doi
            if(fmax >lastfmax) exit doi
            lastfmax=fmax
        enddo doi
        rhs2d(:,1) =x
        deallocate(delta, x, stat=istat)

!4)  depermutate the result.
        forall (i=1:nr) result(i) = rhs2d(perm(i),1)

!       write(*,*) 'maxmove in total=', maxval(abs(result(1:nr)))

    deallocate(jao,iao, ao,abd,rhs2d,ipiv, stat=istat)

end function solveDirect

 !expert in work  - havent got the arguments right
 !   write(*,*) 'allocate for gbsvx with nr=',nr,' nrhs=',nrhs,' nabd=',nabd,' ml= ',ml
!    allocate(x(nr,nrhs),stat=istat);if(istat/=0) then;write(*,*) ' alloc failed (7)'; return; endif;
 !   allocate(afb(nabd,nr), stat=istat); if(istat/=0) then;write(*,*) ' alloc failed (8) afb ',nabd,' by', nr ; return; endif;
!     allocate(vr (nr ),stat=istat);if(istat/=0) then;write(*,*) ' alloc failed (9)'; return; endif;
!     allocate(vc (nr ),stat=istat);if(istat/=0) then;write(*,*) ' alloc failed (10)'; return; endif;
!     allocate(ipv(nr),stat=istat);if(istat/=0) then;write(*,*) ' alloc faileD (11)'; return; endif;
!    vr=1; vc=1;
!    trans='N'
!    equed =  'N'
!    fact='E'
!    rcond=1; info=0
!    !call gbsvx(  ab     b  x [,kl] [,afb] [,ipiv][,fact] [,trans] [,equed] [,r] [,c] [,ferr] [,berr] [,rcond] [,rpvgrw] [,info] )
 !   call gbsvx( abd, rhs2d, x,  ml, fact=fact ,  trans=trans, equed=equed, rcond=rcond,info=info ) !  afb ,  ipv,  ) ! ,    vr,vc ) ! r  [] []  [,ferr] [,berr]  [,rpvgrw] [,info] )
!    write(*,*) ' reciprocal condno = ',rcond

!       forall (i=1:nr) result(i) = x(perm(i),1)
!finally
!deallocate(xxx,vr,vc,ipv,stat=istat)
!deallocate(afb,stat=istat)

