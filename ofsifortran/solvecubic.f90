

!Description:
 ! Solve the cubic equation q*X^3 + r*X^2 + s*X + t = 0. // PH Method
!Returns:
!  0: failure (q = 0, r = 0, s = 0 case)
!  1: three REAL roots (r1 <= r2 <= r3)
!  2: one REAL root (r1) and two complex conjugate roots (r2 +/- (r3)*sqrt(-1))
!  3: two REAL roots (r1 <= r2) (q = 0, r != 0 case)
!  4: two complex conjugate roots (r1 +/- (r2)*sqrt(-1)) (q = 0, r != 0 case)
!  5: one REAL root (r1) (q = 0, r = 0, s != 0 case)

function solvecubic(p_q,p_r,p_s,p_t,p_r1,p_r2,p_r3) result (rc)  bind(C,name='solvecubic' )
      use basictypes_f  ! for outputunit
        implicit none

        REAL (kind=8) ,intent(in)	:: p_q,p_r,p_s,p_t
        REAL (kind=8),intent(out)	:: p_r1,p_r2,p_r3
        integer			:: rc

        REAL (kind=8)			:: q,r,s,t
        REAL(kind=8) ,parameter :: sqr3 = 1.73205080756888
        complex (kind=8) ::		r1,r2,r3;
        complex (kind=8) ::		f4q,f4cr,f4a
        REAL(kind=8)	::		f1,f2,f3
        REAL(kind=8),dimension(3)	::	f
        integer	,dimension(1)		:: i1,i3

	p_r1= 0; p_r2=0; p_r3 =0
	rc=99
	q=p_q; r=p_r; s=p_s;t=p_t

	if(q==0 ) then
		if(r==0 ) then
			if(s==0) then
				rc=0
				return
			else  ! just S non-zero
					p_r1 = -t/s
					rc=5
					return
			endif
		else ! q is 0, r s non-zero its a quadratic return 3 or 4
!  3: two REAL roots (r1 <= r2) (q = 0, r != 0 case)
!  4: two complex conjugate roots (r1 +/- (r2)*sqrt(-1)) (q = 0, r != 0 case)
			!	roots are -s +- sqrt(ssq-4rt)/2r
			f1 = s**2 -4.0*r*t
			if(f1 >=0.0) then
				f2=SQRT(f1)
				if( r > 0) then
					p_r1 = (-s -f2)/ (2*r)
					p_r2 =(-s + f2)/ (2*r)
				else
					p_r1 = (-s + f2)/ (2*r)
					p_r2 =(-s - f2)/ (2*r)
				endif
				rc=3
			else
				f2=SQRT(-f1)
				p_r1 = (-s)/ (2*r)
				p_r2 = (f2)/ (2*r)
				rc=4
			endif

		endif
		return
	endif
	! here Q is non-zero 
!   BEWARE  the following has plenty of rounding errors. 
	t=t/q; s=s/q; r=r/q; q=1.0d00;

	f1  =2*r**3 - 9*q*r*s
	f2  =-r**2 + 3*q*s
	f3  = 2.0d00*r**3 - 9.0d00*q*r*s + 27.0d00*q**2*t
	f4a = -4.0d00*(r**2 - 3.0d00*q*s)**3 + (2.0d00*r**3 - 9.0d00*q*r*s + 27.0d00*q**2*t)**2
	f4q  = 2.0d00*r**3 - 9.0d00*q*r*s + 27.0d00*q**2*t
	f4q  = f4q + CDSQRT(f4a)
	f4cr = f4q**(0.33333333333333D00)

	r1 =  (-2.0d00*2.00d00**(1/3)*f2)
	r1 = r1 /f4cr  
	r1 = r1 +  2.0d00**(2/3)*f4cr 
	r1 = r1 + 2.0d00*r
	r1 = -r1 /(6.0d00) ! requires q=1


	r2=    -((1 + (0.0d00,1.0d00)*sqr3)*f2)/  &
			(3.*2**(2/3)*f4cr*q) + &
		   ((1 - (0.0d00,1.0d00)*sqr3)*f4cr)/&
			(6.*2**(1/3)*q) - r/(3.*q)

	r3=   -((1 - (0.0d00,1.0d00)*sqr3)*f2)/&
			(3.*2**(2/3)*f4cr*q) + &
		   ((1 + (0.0d00,1.0d00)*sqr3)*f4cr)/&
			(6.*2**(1/3)*q) - r/(3.*q)

	!  1: three REAL roots (r1 <= r2 <= r3)
	!  2: one REAL root (r1) and two complex conjugate roots (r2 +/- (r3)*sqrt(-1))


		if(ABS(IMAG(r1) ) < 1D-5 .and. ABS(IMAG(r2)) <1d-5 .and. ABS(IMAG(r3)) < 1D-5 ) then
			f(1)=REAL(r1,8)
			f(2)=REAL(r2,8)
			f(3)=REAL(r3,8)	
			i1 = MINLOC(f); 
			i3 = MAXLOC(f)
			p_r1=f(i1(1)); p_r3=f(i3(1))
			select case (i1(1)+4*i3(1))
			case (11, 14 )
				p_r2= f(1)
			case (7,13 )
				p_r2= f(2)
			case (6,9 )
				p_r2= f(3)
			case default
				write(outputunit,*) ' shouldnt be here'
			end select  
								
			rc=1
			return
		endif

		if(ABS(IMAG(r1) ) < 1D-12  ) then
			p_r1=REAL(r1,8)
			p_r2=REAL(r2,8)
			p_r2= (p_r2 + REAL(r3))/2 ! a bit more accuracy
			p_r3=IMAG(r2)
			p_r3= (p_r3 - IMAG(r3))/2 ! a bit more accuracy
			rc=2
			return
		endif

		rc=0   ! shouldnt ever get here
! a more generous test  actually WRONG because it assumes case 2
		if(ABS(IMAG(r1) ) < 1D-02* ABS(REAL(r1,8))  ) then
			p_r1=REAL(r1,8)
			p_r2=REAL(r2,8)
			p_r2= (p_r2 + REAL(r3))/2 ! a bit more accuracy
			p_r3=IMAG(r2)
			p_r3= (p_r3 - IMAG(r3))/2 ! a bit more accuracy
			rc=2
			return
		endif
		rc=0   ! shouldnt ever get here


end function  solvecubic
