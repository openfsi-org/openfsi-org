
MODULE Dofo_f
USE ftypes_f
use dofo_subtypes_f
USE  linklist_f
use vectors_f
use dofoprototypes_f
use basictypes_f
use dofoio_f
use fnoderef_f
IMPLICIT NONE
!integer, parameter :: dofounit = 6
!integer, parameter :: dofodbgunit = 15 ! write here if flag /D1 is set
CONTAINS




 function DOFOCalcR(d)  result (ok)  
    implicit none
#ifdef NO_DOFOCLASS
           type (dofo),pointer,intent(in) ::d   ! or class
#else
           class (dofo),pointer,intent(in)::d  ! or type
#endif
    type(fnode) , pointer :: theNode
    integer :: ok,j,r                  
    d%rt=0; ok=1
    do j=1,ubound(d%m_Fnodes,1)
        r = d%m_Fnodes(j)%m_fnrRow
        thenode=>decodenoderef(d%m_Fnodes(j))
        d%rt= d%rt + matmul(d%invjacobi(:,r:r+2),thenode%r)
    enddo 
end  function DOFOCalcR    

function DOFOCalcV(d)  result (ok)  
!    use ftypes_f
!    use fnoderef_f
    implicit none

#ifdef NO_DOFOCLASS
           type (dofo),pointer,intent(in) ::d   ! or class
#else
           class (dofo),pointer,intent(in)::d  ! or type
#endif
    type(fnode) , pointer :: theNode
    integer :: ok,j ,r                  
    d%vt=0; ok=1
    do j=1,ubound(d%m_Fnodes,1)
        r = d%m_Fnodes(j)%m_fnrRow
        thenode=>decodenoderef(d%m_Fnodes(j))
        d%vt=d%vt+matmul(d%invjacobi(:,r:r+2),thenode%v)
    enddo 
end  function DOFOCalcV

function  PleaseRelease(d) result (yes)
    implicit none
#ifdef NO_DOFOCLASS
           type (dofo),pointer,intent(in)  ::d  ! or class
#else
           class (dofo),pointer,intent(in) ::d  ! or type
#endif
    logical yes
    yes=.false.
     if(d%m_dofotype /=  C_dofo_SlideFixedSurface) return
     yes = (d%rt(3) >1.0d-8)
   !  if(yes) write(dofounit,*) 'please Release rt=', sngl(d%rt(3))
end function PleaseRelease

! the nodal masses have already been trimmed, scaled for alfa, etc.
! So we just combine them onto the dofo

function  ComputeDOFOMass(d,doit) result(ok)
    use ftypes_f
    use invert_f
    use fnoderef_f
    use saillist_f
    use math_f
    use fglobals_f, only : g_admas
    USE LAPACK95
    implicit none
#ifdef NO_DOFOCLASS
       type (dofo),intent(inout) ::d   ! or class
#else
       class (dofo),intent(inout)::d  ! or type
#endif

    logical, intent(in),optional :: doit  ! a nasty way of disabling dofo mass from the Complete_xx functions

    integer ::ok,i,row,eerr
    logical :: err
 !   integer, allocatable, dimension( :) :: ipiv
    type (fnode) ,pointer ::theonlynode,TheNode
    type(fnoderef_p),pointer:: nreff
    real(kind=double) ::    total1, total2,emin,emax
    logical :: TrimDofoMass,DiagonalizeDofoMass

    ok=1; err=.false.
    if(.not. present(doit)) return
    if(.not. doit)  return
    call check_cl("DD",DiagonalizeDofoMass)
    call check_cl("TD",TrimDofoMass)
    d%xmt=0.; total1=0; total2=0;
    if(associated(d%child)) return;
  select case (d%m_dofotype) 
        case (C_DOFO_NODENODE_CONNECT,  C_DOFO_COMPOSITE,C_DOFO_NODESPLINE_CONNECT,C_DOFO_STRINGSPLINE_CONNECT,C_DOFO_RIGIDBODY,C_dofo_SlideFixedPlane)
            ok=0;                
!  the mass in dofotee space is just the sum of (jinv(i) . xm(ii) . j(i) ) over the I nodes.
!      The stiffness in dofo space has to include the cross terms   xm(ij)    

            do i=1, ubound(d%m_Fnodes,1)
                theNode=>decodenoderef(d%m_Fnodes(i))
                row= d%m_Fnodes(i)%m_fnrRow
                d%xmt=d%xmt + matmul(d%invjacobi(:,row:row+2), matmul(theNode%xm,d%jacobi(row:row+2,:)))
                ! this line is really slow because the jacobi rows are mainly zero. 
                total2 = total2 + sum(abs(theNode%xm))
                ok=i
           enddo
ub:          if (ubound(d%xmt,1)>0) then
                if(TrimDofoMass) then
                 call MinMaxEigen(d%xmt,g_admas**2,emin,emax,eerr); !write(*,'(a,i4,3G14.5 )') ' eerr, eMin,eMax ',eerr, emin,emax
                endif
                if(DiagonalizeDofoMass)   call Diagonalise(d%xmt);
                    d%bt = ( d%xmt + transpose(d%xmt))/2.0d00  ! force symmetry
                    d%bt = Minverse_e(d%bt,err)  ; if(err) d%bt=0
               else ub
               err = .false.
            endif ub
        case default
             if( ubound( d%m_Fnodes  ,1) > 1) write(outputunit,*)'   ComputeDOFOMass Fnode count = WRONG'
             if( ubound( d%jacobi ,1) > 3)       write(outputunit,*)'   ComputeDOFOMass jacobi row count'
iuj:         if(ubound( d%jacobi ,2) >0) then
                nreff => d%m_Fnodes(1)
                theonlynode => decodenoderef(nreff)
               ! write(outputunit,"(': J dims',i5,i5,$)"),ubound( d%jacobi,1),ubound( d%jacobi,2)
               ! write(outputunit,'(a,i5,i5)') ' XMT  dims'  , ubound( d%xmt ,1),ubound( d%xmt ,2)
                d%xmt = matmul( d%invjacobi,matmul(theonlynode%xm, d%jacobi))     ! SLOW WAIT
                total2 = total2 + sum(abs(theonlynode%xm));
                if(TrimDofoMass)  call MinMaxEigen(d%xmt,g_admas**2,emin,emax,eerr);
                if(DiagonalizeDofoMass)   call Diagonalise(d%xmt);
                d%bt = Minverse_e(d%xmt,err);
                if(err) then
                  d%bt=0
                endif
             endif iuj

        end select
         total1 = sum(abs(d%xmt))
!        if(err .and. d%m_dofotype /= C_dofo_FixedPoint)then
!        write(outputunit, '( "cant invert dofo mass type ",i2," dofo (", i2,1x,i4," ) ",a )') d%m_dofotype,d%m_dofosli ,d%m_dofoN ,trim(d%m_DofoLine )
!        endif
!      write(outputunit,'( "dofo (", i2,1x,i4," ) ",a,"  tot xmt=",g13.5," tot xm=",g13.5,"err=",L4 )') d%m_dofosli ,d%m_dofoN ,trim(d%m_DofoLine ),total1,total2,err
           
end function  ComputeDOFOMass

!  t is the current displacement (zero) and origin is the current coordinates
! ( for a sliding node, it is the displacement from the position on the spline
! when the jacobi was created)

! WE MUST NOT CHANGE THE ORIGIN DURING A RUN - except on a change in model transformation. 

      
function NodeSplineDOFO_Inverse(d) result(ok)
! if its NOT a slider, the inv is just the transpose.
! if it IS a slider, it is the transpose except that the tangent vector is divided by the 
! square of its length
    implicit none
#ifdef NO_DOFOCLASS
           type (dofo),pointer,intent(inout) ::d   ! or class
#else
           class (dofo),pointer,intent(inout)::d  ! or type
#endif
    integer :: ok
    integer nr,nc
    ok=0   
    nr =  ubound(d%jacobi,1); nc= ubound(d%jacobi,2)
    if(associated(d%invjacobi)) deallocate(d%invjacobi  )
    allocate(d%invjacobi(nc,nr ))
    
    d%invjacobi= transpose(d%jacobi); 
  
end function NodeSplineDOFO_Inverse

function dofoStringSplineJacobian(d)  result(ok)
    implicit none
#ifdef NO_DOFOCLASS
         type (dofo),pointer ::d ! or class
#else
        class (dofo),pointer ::d
#endif 
     
    integer::ok 
    integer ::istat, SlaveRowStart =-1
    real(kind=double), pointer, dimension(:) :: ss
 
    allocate(ss(ubound(d%d5%OriginalSlaveTees  ,1)))

    if(d%d5%m_spl%Sliding) then
        SlaveRowStart = ubound(d%d5%m_spl%m_ownerString%m_pts,1)*3+1
        ss= d%t(SlaveRowStart:) + d%d5%OriginalSlaveTees
    else
        ss= d%d5%OriginalSlaveTees             
    endif 
    deallocate( d%jacobi, d%invjacobi ,stat=istat); 
    d%jacobi=>d%d5%m_spl%FSplineJMatrixMultiple(d,d%origin,ss) ! might be dangerous for ss to be a pointer into persistent data
    ok=NodeSplineDOFO_Inverse(d)  
    deallocate(ss)
end function dofoStringSplineJacobian

!   the Complete function allocates the sub-dofo, tell it about its nodes and other data, and fills it in. It
!   make further calls to get Jacobian, mass and pseudo-motion. That means that a DOFO can be created on the fly.

!   Those calls should be safe to be called when the nodes dont have mass or velocity.

function  Complete_FixedPointDOFO(d, node,origin ) result(OK)
    use fnoderef_f
    implicit none
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d,d2  ! or class
#else
           class (dofo),pointer::d,d2 ! or type
#endif
    type(fnode) :: node  !, pointer
    real(kind=double), intent(in),dimension(3) :: origin
    integer :: ok,j

    ok= hookDofoToNode(node,d,1,0)
    d%ndof=0 
    
! common to all types
    allocate(d%origin(3*ubound(d%m_Fnodes,1)))      ;         d%origin=origin;  
    allocate(d%jacobi( 3*ubound(d%m_Fnodes,1),d%ndof)) ;     d%jacobi=0  
    allocate(d%invjacobi(d%ndof ,3*ubound(d%m_Fnodes,1))) ;  d%invjacobi=0  
 	allocate(d%t(d%ndof));   allocate(d%vt(d%ndof));  allocate(d%rt(d%ndof)) ;
	allocate(d%xmt(d%ndof ,d%ndof ));    allocate(d%bt(d%ndof,d%ndof ))
    d%t=0;  d%vt=0;  d%rt=0; d%xmt=0; d%bt=0;
    ok= ok+ ComputeDOFOJacobian(d)
    ok=ok+ ComputeDOFOMass(d)

    if(FindSharedDOFO(d,d2)) then
        j = DofoMerge(d,d2)  !   !in Complete_FixedPointDOFO
    endif

end function  Complete_FixedPointDOFO

!   the Complete function allocates the sub-dofo, tell it about its nodes and other data, and fills it in. It
!   make further calls to get Jacobian, mass and pseudo-motion. That means that a DOFO can be created on the fly.
!   Those calls should be safe to be called when the nodes dont have mass or velocity.

function Complete_SlidelineDOFO(d, node, origin, vector) result(OK)
    use fnoderef_f
    implicit none

#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d,d2  ! or class
#else
           class (dofo),pointer::d,d2 ! or type
#endif
    type(fnode):: node   ! , pointer 
    real(kind=double), intent(in),dimension(3) :: origin,vector
    integer :: ok,j
    ok= hookDofoToNode(node,d,1,0)
    d%ndof=1
    
! common to all types
    allocate(d%origin(3*ubound(d%m_Fnodes,1)))  ;               d%origin=0;
    allocate(d%jacobi( 3*ubound(d%m_Fnodes,1),d%ndof)) ;       d%jacobi=0 
    allocate(d%invjacobi(d%ndof, 3*ubound(d%m_Fnodes,1))) ;     d%invjacobi=0   
 	allocate(d%t(d%ndof));   allocate(d%vt(d%ndof));  allocate(d%rt(d%ndof)) ;
	allocate(d%xmt(d%ndof ,d%ndof ));    allocate(d%bt(d%ndof,d%ndof ))
    d%t=0;  d%vt=0;  d%rt=0; d%xmt=0; d%bt=0;
    
 ! for this type   
    allocate(d%d1)
     d%origin= origin
    d%d1%vector = vector

    ok= ComputeDOFOJacobian(d)
     
     d%t= matmul(transpose(d%jacobi),(node%XXX-d%origin))  !slideline OK could be invjacobi
     d%vt=matmul(transpose(d%jacobi),node%v)
     d%rt =matmul(transpose(d%jacobi),node%r);
    ok= ComputeDOFOMass(d)

    if(FindSharedDOFO(d,d2)) then
        j = DofoMerge(d,d2)  !   !in Complete_SlidelineDOFO
    endif
end  function Complete_SlidelineDOFO

!   the Complete function allocates the sub-dofo, tell it about its nodes and other data, and fills it in. It
!   make further calls to get Jacobian, mass and pseudo-motion. That means that a DOFO can be created on the fly.
!   Those calls should be safe to be called when the nodes dont have mass or velocity.

function Complete_SlidePlaneDOFO(d,node, origin, normal) result(OK)
        use fnoderef_f 
        implicit none
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d,d2  ! or class
#else
           class (dofo),pointer::d,d2 ! or type
#endif
        type(fnode):: node  !,pointer 
        real(kind=double), intent(in),dimension(3) :: origin,normal
        integer :: ok,j
        ok= hookDofoToNode(node,d,1,0)

        d%ndof=2   
 ! common to all types
        allocate(d%jacobi(3*ubound(d%m_Fnodes,1), d%ndof)) ;     d%jacobi=0  
        allocate(d%origin(ubound(d%jacobi,1)))  
        allocate(d%invjacobi( d%ndof,3*ubound(d%m_Fnodes,1))) ;     d%invjacobi=0  
 	    allocate(d%t(d%ndof));   allocate(d%vt(d%ndof));  allocate(d%rt(d%ndof)) ;
	    allocate(d%xmt (d%ndof ,d%ndof));    allocate(d%bt(d%ndof,d%ndof ) )
        d%t=0;  d%vt=0;  d%rt=0; d%xmt=0; d%bt=0;
    
 ! for this type        
         
        allocate(d%d2)    
        d%origin= origin
        d%d2%normal = normal
        ok=ComputeDOFOJacobian(d)
         d%t= matmul(transpose(d%jacobi),(node%XXX-d%origin))  !slideplane  could be invjacobi
         d%vt=matmul(transpose(d%jacobi),node%v);
         d%rt =matmul(transpose(d%jacobi),node%r);  ! this is Complete_SlidePlaneDOFO
         ok=ComputeDOFOMass(d)
      !  ok=ok+CreateChildDofo(node)
         if(FindSharedDOFO(d,d2)) then
             j = DofoMerge(d,d2)   !in  Complete_SlidePlaneDOFO
         endif
end function Complete_SlidePlaneDOFO

function SetDofoUsed   (sn, NN,flag,err) result(ok) ! sets the Used flag depending on the value of 'flag'
    USE  saillist_f
    IMPLICIT NONE
	INTEGER , intent(in) :: sn,nn,flag
	INTEGER , intent(out) :: err
	TYPE ( Dofolist) ,POINTER :: Dofos
	integer	:: ok

!  the usual error returns. 
!  the only twist is that if this is the last Dofo, we decrement count if flag=0
!  similarly, we increment count if NN is big. But only up to 
!
	ok = 0
	err=16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	err=8
	Dofos=>saillist(sn)%Dofos
	if(.not. ASSOCIATED(Dofos%list)) return
	if(NN > UBOUND(Dofos%list,1)) return
	if(NN  <LBOUND(Dofos%list,1)) return
	err=0
	if(flag/=0) THEN
		if(Dofos%list(NN)%used) err=2
		Dofos%list(NN)%used=.TRUE.
		if(NN > Dofos%count) Dofos%count=NN 
	else
		if(.NOT. Dofos%list(NN)%used) err=1
		Dofos%list(NN)%used=.FALSE.
		if(NN ==  Dofos%count) Dofos%count=Dofos%count-1 
	endif
	ok=1
END function SetDofoUsed 

function FindSharedDOFO(d1,d2) result(HaveCommonNodes ) ! Only those without child. returns the 'other' one in d2
    use fnoderef_f
    implicit none
#ifdef NO_DOFOCLASS
           type (dofo),pointer, intent(in) ::d1  ! or class
           type (dofo),pointer, intent(out) :: d2 ! or class
#else
           class (dofo),pointer, intent(in)::d1 ! or type
           class (dofo),pointer, intent(out) :: d2 ! or type
#endif

    type(fnode), pointer ::n
    logical :: HaveCommonNodes
    integer ::i,j
    HaveCommonNodes =.false. 
    d2=>NULL()

di:     do i= 1, ubound(d1%m_fnodes,1)
            n=>decodenoderef(d1%m_fnodes(i))
dj:          do j=1, ubound( n%m_pdofo,1)
                d2=> decodedoforef(n%m_pdofo(j))
                if(associated(d2%child)) cycle dj
                if( ( d2%m_dofosli .ne. d1%m_dofosli) .or.( d2%m_dofoN .ne. d1%m_dofoN) ) then  
                HaveCommonNodes=.true. 
                return     
                endif              
            enddo dj
        enddo di
    d2=>NULL()
end function FindSharedDOFO


recursive function DofoMerge(d1,d2)  result(ok)
    use ftypes_f
    use fglobals_f
    use dofolist_f
    use dofoprototypes_f
    use dofomath
    use fnoderef_f
    use realloc_f
    use rx_control_flags_f
   USE F95_PRECISION, ONLY: WP => DP
    IMPLICIT NONE
#ifdef NO_DOFOCLASS
           type (dofo),pointer, intent(inout) ::d1,d2  ! or class
#else
           class (dofo),pointer, intent(inout) ::d1,d2 ! or type
#endif
    integer::ok,error,errr
    integer ::i,j,k,  m,n,n1,n2,k2,nntot,dum,r
#ifdef NO_DOFOCLASS
         type (dofo),pointer ::dnew ,dtemp ! or class
#else
        class (dofo),pointer ::dnew ,dtemp ! or type
#endif  
    
    real(kind=double), dimension(:,:),pointer ::j1 ,j2
    integer, dimension(:),allocatable :: r1     
    integer, dimension(:),allocatable  :: r2     
    real(kind=double), dimension(:),pointer ::O1  
    real(kind=double), dimension(:),pointer  ::O2   
    real(kind=double), dimension(:),pointer  ::t1  
    real(kind=double), dimension(:),pointer  :: t2  
    real(kind=double),  allocatable, dimension(:):: neworigin,tnew 
    real(wp),  allocatable, dimension(:,:):: Jnew  
    logical ::  HaveCommonNodes  
    ok=0


    if(associated(d1%child)) write(outputunit,*) ' Merge a parent (1)!!!!!!!!!!!!'
     if(associated(d2%child)) write(outputunit,*) ' Merge a parent (2)!!!!!!!!!!!!'   
! d1 and d2 may share a few nodes. They must have no children. They may both have parents.      
!  they will become parents of the new child.
! so 1)  
!   2) make the new child dofo and hook to the parents.
!   3) identify the J1,r1,O1,t1 arrays and point to them
!   4) Call Reduced JMatrix to determine JNew, NewOrigin, Tnew. 

    n1 = ubound(d1%m_fnodes,1);   n2 = ubound(d2%m_fnodes,1)
    HaveCommonNodes=.false.
! are there any common nodes  
dj:    do J=1,N2
di:     do i= 1,n1
         if((d1%m_fnodes(i)%m_fnr_Sli .eq. d2%m_fnodes(j)%m_fnr_Sli) .and.(d1%m_fnodes(i)%m_fnrN .eq. d2%m_fnodes(j)%m_fnrN)  ) then
            HaveCommonNodes=.true.
            exit dj
        endif
        enddo di
   enddo dj
    if(.not. HaveCommonNodes) return
    ok=1;

!   2) make the new child dofo and hook to the parents. We put it in d2's model - assymetric.
    dnew=> CreateDOFO(d2%m_dofosli , C_DOFO_COMPOSITE ,error) ! a clean initialisation

    if(len_trim(d1%m_DofoLine ) + len_trim(d2%m_DofoLine ) < 240) then
         dnew%m_DofoLine = '<'//trim(d1%m_DofoLine)//'&'//trim(d2%m_DofoLine)//'>'
     else
       dnew%m_DofoLine = "(combo)"
     endif    
    dnew%parent1=>d1
    dnew%parent2=>d2
    d1%child=>dnew   
    d2%child=>dnew  
 !   write(outputunit,'(3(a,i2,i6,1x,a))')'(dofoMerge)',d1%m_dofosli,d1%m_dofoN, trim(d1%m_DofoLine)," and",d2%m_dofosli,d2%m_dofoN, trim(d2%m_DofoLine) ,' into',dnew%m_dofosli,dnew%m_dofoN
!   3) identify the J1,r1,O1,t1 arrays and point to them

    dnew%IsLinear =  (d1%IsLinear .and. d2%IsLinear )  .or. BTEST( ForceLinearDofoChildren,0) ! getglobal idem
    j1=>d1%jacobi
    j2=>d2%jacobi 
    o1=>d1%origin
    o2=>d2%origin
    t1=>d1%t
    t2=>d2%t 
! r1 and r2 are the rows in the new J which will correspond to rows in the old J;
! so we walk thru m_Fnodes for each dofo
! first for D1 we assign rows sequentially to the dofs of its m_Fnodes
! then for D2 , if a node is common to a node in D1 we set its R2 the same as that nodes R1
! else we tack them onto the tail of r1 and r2. 
! at the same time we can build m_Fnodes for theDOFO   

  allocate(r1( 3*n1));   allocate(r2( 3*n2)); r2=-1
  k2=1; nntot=n1;
  do i=1, 3*n1
        r1(i) = i;
  enddo  
!DEC$ NOPARALLEL
   do i=1, n1   
        dum = hookDofoToNode(decodeNodeRef(d1%m_fnodes(i) ),dnew,(3*i-2),0 )
  enddo
! now we have  the nodes of D1 in the new list, in their original sequence.  
! Now we append the nodes of D2 except where they are in the list of D1
! in pseudocode:
!  for each node in D2
!       Is it in the list of D1????
! if it is:
!       DONT need to hook it to Dnew again
!       DO  copy its R1 row values into D2
! else if it isnt in D1
!       Hook it to DNew at the next available row position
!       ITS r2 values are the next 3 availab

!DEC$ NOPARALLEL 
    do J=1,N2
         r=-1;
doi:     do i= 1,n1
         if((d1%m_fnodes(i)%m_fnr_Sli .eq. d2%m_fnodes(j)%m_fnr_Sli) .and.(d1%m_fnodes(i)%m_fnrN .eq. d2%m_fnodes(j)%m_fnrN)  ) then
            r=i
            exit doi
        endif
        enddo doi
        if( r/=-1) then  ! J is already there at triplet K
            do k=1,3
                r2(3*j-3+k) = r1(3*r-3+k)
            enddo
         else   ! J isnt there so append it.
            nntot=nntot+1
            dum = hookDofoToNode(decodeNodeRef(d2%m_fnodes(j)),dnew,(nntot*3-2),0)
            do k=1,3
               r2(3*j-3+k) =  nntot*3-3+k
            enddo
        endif
    enddo  
   ! call dofoMatchCoords( d1,d2,r1,r2)   !(QUESTION:: only for node-node connects??
!   4) Call ReducedJmatrix to determine JNew, NewOrigin, Tnew.                
 !   write(outputunit,*)'Before DofoMerge '
 !   ok=ok+ PrintOneDOFO(d1 ,outputunit,.false.)
 !   ok=ok+ PrintOneDOFO(d2 ,outputunit,.false.)

    deallocate(dnew%parent1%dofosv ,dnew%parent2%dofosv,dnew%parent1%m_svOrg ,dnew%parent2%m_svOrg,stat=k)    !m_svOrg + dofoSV
    errr= ReducedJmatrix1( J1,J2,r1,r2,O1,O2,t1,t2,jnew, neworigin,tnew, dnew%parent1%dofosv, dnew%parent2%dofosv,dnew%parent1%m_svOrg ,dnew%parent2%m_svOrg,d1,d2) ! DofoMerge
    if(errr ==99) then
    write(outputunit,'(2(" `", a,"`",i4,i5," `", a,"`") )') 'restr mismatch(DofoMerge) in ',d1%m_dofosli ,d1%m_dofoN ,trim(d1%m_DofoLine),'and ',d2%m_dofosli ,d2%m_dofoN ,trim(d2%m_DofoLine)
    endif
! now create the dofo j, etc. =-999  ,=-1; 
! common to all types

     m = ubound(jnew,1); n = ubound(jnew,2);    dnew%ndof=n;
    allocate(dnew%jacobi( m,n)) ;               dnew%jacobi=jnew
    allocate(dnew%origin(m))  ;                 dnew%origin=neworigin
 	allocate(dnew%t(n));                        dnew%t=tnew 

    allocate(dnew%invjacobi(n,m)) ;     dnew%invjacobi=transpose(dnew%jacobi);
 	allocate(dnew%vt(n));               
 	allocate(dnew%rt(n)) ;
	allocate(dnew%xmt(n,n ));   
	allocate(dnew%bt(n,n ))
    dnew%vt=0;  dnew%xmt=0;dnew%bt=0; ! dnew%rt=0;
    ok= DOFOCalcR(dnew)  
    ok =ok + DOFOCalcV(dnew)  
    ok = CheckSVMatrix(dnew)
    if(.not. ok) then
          write(outputunit,*)'DofoMerge CheckSV failure'  
            write(321,*)'DofoMerge CheckSV failure'            
            ok=ok+ PrintOneDOFO(d1 ,321,.true.)
            ok=ok+ PrintOneDOFO(d2 ,321,.true.)
            ok=ok+ PrintOneDOFO(dnew ,321,.true.)   
     endif 


 ! for this type   
    deallocate(r1,r2,jnew,neworigin, tnew)
    ok= ComputeDOFOMass(dnew)
   
    if(FindSharedDOFO(dnew,dtemp)) then
        i = DofoMerge(dnew,dtemp) 
    endif
end function DofoMerge


function CheckSVMatrix(dnew) result(ok)
#ifdef NO_DOFOCLASS
           type (dofo), intent(in) ::dnew  ! or class
#else
           class (dofo), intent(in) ::dnew ! or type
#endif
    integer::ok,i,n
    real(kind=double), allocatable, dimension(:) :: mytee;
     real(kind=WP) ,parameter :: tol = 1.0D-7
! 1) for each parent compare its TEE with its SV. dnew%t
    ok =1
#ifndef DOFO_CHECKING
       return
#endif
    if(allocated(dnew%parent1%dofosv )) then
        allocate(mytee(ubound( dnew%parent1%t,1)))
        mytee = matmul(dnew%parent1%dofosv,dnew%t)
        if(allocated(dnew%parent1%m_svOrg)) mytee = mytee + dnew%parent1%m_svOrg       
        if(any( abs(mytee - dnew%parent1%t) .gt. tol)) then
        write(outputunit,'("parent1 mytee mismatch on dofo",i6, "( sv.dnew%t,parent%t,diff)") ')dnew%m_dofoN   ; ok=0
        n = ubound(mytee,1)
        do i = 1,n
            if(abs(mytee(i)- dnew%parent1%t(i))<tol ) cycle;
              write(outputunit,'(2i6,1x,2g15.6)')dnew%m_dofoN, i, mytee(i), dnew%parent1%t(i) ; ok=0;
        enddo
        endif
        deallocate(mytee)
    endif
    if(allocated(dnew%parent2%dofosv )) then    
        allocate(mytee(ubound( dnew%parent2%t,1)))
        mytee = matmul(dnew%parent2%dofosv,dnew%t)
        if(allocated(dnew%parent2%m_svOrg)) mytee = mytee + dnew%parent2%m_svOrg            
        if(any( abs(mytee - dnew%parent2%t) .gt.tol)) then
        write(outputunit,'("parent2 mytee mismatch on dofo",i6, "( sv.dnew%t,parent%t,diff)") ')dnew%m_dofoN   ; ok=0
        do i = 1,ubound(mytee,1)
            if(abs(mytee(i)- dnew%parent2%t(i))<tol ) cycle;
            write(outputunit,'(i6,1x,2g15.5,2x,g15.7 )')i, mytee(i), dnew%parent2%t(i),mytee(i)- dnew%parent2%t(i)
        enddo
        endif
        deallocate(mytee)
    endif
end function CheckSVMatrix


function UniqueNodeOfDofo(d) result(node) ! in general dofos will have multiple nodes, 
! but while they only have one we can use this to find it.
    use fnoderef_f
    implicit none
#ifdef NO_DOFOCLASS
           type (dofo),intent(in) ::d   ! or class
#else
           class (dofo),intent(in)::d  ! or type
#endif

    type(fnode), pointer ::node
	type (fnoderef_p) , pointer ::nref
    integer::ok
    ok=0
    if(ubound(d%m_Fnodes,1) /= 1  )  ok=0/OK
     nref=>d%m_Fnodes(1)
     node=>decodeNodeRef(nref )
end function UniqueNodeOfDofo
 



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!  dofoMatchCoordinates is called before ReducedDofoJacobian to ensure that the two dofos
!   give the same X result on their common nodes
!  We implement the general case:
!     we do a Newton-Raphson
!     to find the intersection of the two dofos which is 'closest' to their current positions
!
! there are several heuristics we could use to simplify this:
! for instance
! where the two dofos are linear,  there's nothing to do unless they are parallel.
!
! Where one of the dofos effect on 'thisnode' is non-linear and the others is linear
!  we could adjust the linear dofo so it matches the non-linear evaluation of the other.
! note that not all the degrees of freedom of a 'NL' dofo are non-linear
! for instance the translation of a rigid-body is linear.
! and for a spline-connect,the masters are linear.
!
! the adjustment consists of changing the  tees that affect the linear DOF so that its XXX matches
! the evaluation of the non-linear DOFO.
!  but adjusting the Tees is dangerous if they affect more than one node.


subroutine dofoMatchCoords( d1,d2,p_r1,p_r2)
     use fnoderef_f
    IMPLICIT NONE
#ifdef NO_DOFOCLASS
           type (dofo),intent(inout),target ::d1,d2   ! or class
#else
           class (dofo),intent(inout,target)::d1,d2  ! or type
#endif

    integer, intent(in),dimension(:)  :: p_r1, p_r2
    type(fnode), pointer :: n
    integer:: linearCount,i,j, k,r1,r2,nd1,nd2,c,err
    real(kind=double), dimension(3) :: x1,x2

    character*256 t
    logical   NeedsFixing
     linearCount=0

     t=" "

     write(*,*) 'disabled dofoMatchCoords ',d1%m_dofoN,'and ', d2%m_dofoN
     return
    if(d1%IsLinear .and. d2%IsLinear) then
           write(t,*)'Linear dofos'
    endif
 ! for each common node,  decide if each of the dofos treat it as 'linear' or 'non-linear'

    if(.not. d1%IsLinear .and.  .not. d2%IsLinear) then
        write(t,*)', NL dofos'
    else
         write(t,*)'merge L & NL dofos ',d1%m_dofoN,',', d2%m_dofoN ! , trim(d1%m_DofoLine),'  and ',trim(d2%m_DofoLine)
    endif
    di:     do i= 1, ubound(d1%m_fnodes,1)
    dj:     do j= 1, ubound(d2%m_fnodes,1)
                if( d1%m_fnodes(i)%m_fnrN     /= d2%m_fnodes(j)%m_fnrN   ) cycle
                if( d1%m_fnodes(i)%m_fnr_Sli  /= d2%m_fnodes(j)%m_fnr_Sli) cycle
                r1 = d1%m_fnodes(i)%m_fnrRow
                r2 = d2%m_fnodes(j)%m_fnrRow
                n => decodeNodeRef(d1%m_fnodes(i)  )
                x1 =d1%origin(r1:r1+2) + matmul(d1%jacobi(r1:r1+2,:),d1%t)
                x2 =d2%origin(r2:r2+2) + matmul(d2%jacobi(r2:r2+2,:),d2%t)
                 if (any(abs(x2-x1) .gt. 1e-11)) then
                     write(outputUnit,'(a,\)') trim(t)
                     write(outputunit,'("-   linear mismatch, node=",i3,i5,1x,  3f13.5,1x,3f13.5 )'), n%m_sli,n%nn,  x1(1),x1(2),x1(3),x2(1),x2(2),x2(3)
                endif
                nd1=-1; nd2=-1
    dok:        do k = 1,ubound( n%m_pdofo,1 )
                    if(n%m_pdofo(k)%d%m_dofosli .eq. d1%m_dofosli .and. n%m_pdofo(k)%d%m_dofoN .eq. d1%m_dofoN) then
                       nd1=k
                    exit dok
                    endif
                enddo dok
    dok2:        do k = 1,ubound( n%m_pdofo,1 )
                    if(n%m_pdofo(k)%d%m_dofosli .eq. d2%m_dofosli .and. n%m_pdofo(k)%d%m_dofoN .eq. d2%m_dofoN) then
                       nd2=k
                        exit dok2
                    endif
                enddo dok2
                x1 = Get_DOFO_Cartesian_X( N,nd1 )
                x2 = Get_DOFO_Cartesian_X( N,nd2 )
                NeedsFixing=.false.
                if (any(abs(x2-x1)  .gt. 1e-11) ) then
                     write(outputUnit,'(a,\)') trim(t)
                      write(outputunit,'(" -NONlin err,node=",i3,i5,1x,  3g13.5,1x,3g13.5 )'), n%m_sli,n%nn,  x1(1),x1(2),x1(3),x2(1),x2(2),x2(3)
                     NeedsFixing=.true.
                endif

               enddo dj
            enddo di
        c=0
nf:       do while(NeedsFixing)
            write(outputunit, '("count",i6,\ )') c;
            call DofoPullTwo(d1,d2,p_r1,p_r2,   NeedsFixing,err)  ! in dofoMatchCoords
            if(c>1) exit nf
            c=c+1
          end do nf

end subroutine dofoMatchCoords

!++++++++++++++++++DOFOPullTwo++++++++++++++++++++++++++++
!     get current x1,x2 and measure difference. exit loop if small.
!     call  Least SquaresSolution(cartesian,a,dt)
!          where 'cartesian' is x2-x1, A is {J1,-J2} concatenated and dt is {dt1,dt2} concatenated
!
!      this is on the FULL matrix. - we set dx to zero except where there's a difference.
!      then where some of the tees have changed we should re-compute the jacobians
!      but while the changes are small we could approximate and leave them the same.
!
!  IE the setup is very similar to ReducedJMatrix1 - perhaps we do this inside it?
!  ! - count the common rows and allocate 'cartesian' and 'A'

! t1 should be sv1.tnew t2 sb sv2.tnew at the end of this.


subroutine DofoPullTwo(  d1,d2,p_r1,p_r2,HasChanged,err)
!  .. "Use Statements" ..
    USE LAPACK95 !, ONLY: gels
    use basictypes_f
    use dofomath
    use fnoderef_f
!  .. "Implicit Statement" ..
         IMPLICIT NONE
!  .. "arguments" ..
#ifdef NO_DOFOCLASS
       type (dofo),intent(inout),target ::d1,d2   ! or class
#else
       class (dofo),intent(inout),target::d1,d2  ! or type
#endif
    integer, intent(in),dimension(:)  :: p_r1, p_r2
    real(kind=double), allocatable  ,dimension(:)  :: X1nl ,X2nl
    logical,intent(out) :: hasChanged
    integer,intent(out) :: err
!  .. "Local Scalars" ..

    integer ::m ,n,minMN,nc1,nc2, nc ,nr1,nr2
    integer :: info=0,i,j,k,r,nd,r2
    type(fnode), pointer :: TheNode
!  .. "Local Arrays" ..! m rows, n cols

    real(kind=double), pointer  ,dimension(:,:):: J1, J2
    real(kind=double), pointer  ,dimension(:)  :: O1 ,O2
    real(kind=double), pointer  ,dimension(:)  :: t1 ,t2

    ! the values(input) in r1 and r2 are the rows of the result matrix (Jnew)
    ! which correspond to the rows of the input matrices J1 and J2
    ! We will equate the rows of J1 with the corresponding row
    ! of J2 where their corresponding R1 and R2 are equal.

        real(wp), dimension (:,:),allocatable  :: A
        real(wp), dimension (:,: ),allocatable ::  bbb
        integer, dimension (:), allocatable :: c1,c2;
       err=0; HasChanged = .false.


        allocate(c1(min(ubound(p_r1,1),ubound(p_r2,1))));allocate(c2(min(ubound(p_r1,1),ubound(p_r2,1))))
        nc= MakeCommonRows(p_r1,p_r2,c1,c2)  ! number of common rows


       J1=>d1%Jacobi; J2=>d2%Jacobi
       t1=>d1%t; t2=>d2%t
       O1=>d1%origin; O2=>d2%origin
       allocate( x1nl(ubound(O1,1)));    allocate( x2nl(ubound(O2,1)));

! evaluate each DOFO
! D1
dj1:     do j= 1, ubound(d1%m_fnodes,1)
            r2 = d1%m_fnodes(j)%m_fnrRow
            TheNode => decodeNodeRef(d1%m_fnodes(j)  )
            nd=-1
dok1:        do k = 1,ubound(TheNode%m_pdofo,1 )
                if(TheNode%m_pdofo(k)%d%m_dofosli .eq. d1%m_dofosli .and. TheNode%m_pdofo(k)%d%m_dofoN .eq. d1%m_dofoN) then
                   nd=k
                    exit dok1
                endif
            enddo dok1
            x1nl(r2:r2+2 ) = Get_DOFO_Cartesian_X( TheNode,nd )
           enddo dj1

! D2
dj2:     do j= 1, ubound(d2%m_fnodes,1)
            r2 = d2%m_fnodes(j)%m_fnrRow
            TheNode=> decodeNodeRef(d2%m_fnodes(j)  )
            nd=-1
dok2:        do k = 1,ubound( TheNode%m_pdofo,1 )
                if(TheNode%m_pdofo(k)%d%m_dofosli .eq. d2%m_dofosli .and. TheNode%m_pdofo(k)%d%m_dofoN .eq. d2%m_dofoN) then
                   nd=k
                    exit dok2
                endif
            enddo dok2
            x2nl(r2:r2+2 ) = Get_DOFO_Cartesian_X(TheNode,nd )
           enddo dj2



    ! first: identify the common rows
    ! c1, c2 are indices into J1 and J2. r1 and r2 are indices into the result
    ! checking
            if(count(c1>0) .ne. count(c2>0))  err=16
            if(any(c1 >ubound(J1,1))) err=err+8
            if(any(c2 >ubound(J2,1))) err=err+4
            if(count(c1>0) .ne. count(c2>0)) err=err+2

            if(err .gt.0) then
                deallocate(c1,c2); write(*,*) ' common row error'
                return
            endif
            err=1;

            m = count(c1>0)
            nc1 = ubound(J1,2) ; nc2 = ubound(J2,2)
            nr1 = ubound(J1,1) ; nr2 = ubound(J2,1)
            n = nc1 + nc2
            minMN=min(M,n)
            if(m .eq.0) then ! no rows in common. J is just a diagonal matrix comprising J1 and J2.
                deallocate(c1,c2)
                return
            endif

            allocate( A(m,n) )
            allocate(bbb(max(m,n),1)) ; bbb=0;    !  bbb =  (X2 - X1)  taking the common rows.
            r=0
            do i= 1,nc
                if(c1(i) ==0) cycle
                r=r+1; A(r,1:nc1) = -J1(c1(i),:);  A(r,1+nc1:) = J2(c2(i),:)
                bbb(r,1) = X1nl(c1(i) ) -X2nl(c2(i) )
            enddo
            if(any(abs(bbb) .gt. 1e-10)) then
                 write(*,"('two dofos, X err is ',f12.6,\ ) "), maxval(abs(bbb))
!*****************************************
                call gels(a, bbb ,trans= 'N' ,info=info)
                if(info .eq.0) err=0
!*****************************************
                if(info .eq. 0) then

                     HasChanged = any(abs(bbb) .gt. 1e-10)
                     write(*,"('would change Tee by ',g13.6,1x,L8,' but force exit' ) "), maxval(abs(bbb)),HasChanged
                   ! d1%t = d1%t + bbb(1:d1%ndof,1)
                   ! d2%t = d2%t + bbb(1+d1%ndof:,1)
                     HasChanged = .false.
                else
                    write(*,*) '(DofoPullTwo) GELS failed:',info
                    HasChanged = .false.
                endif
        endif
    ! bbb is now the changes in the Tees
    deallocate(X1nl ,X2nl)
    deallocate(A, bbb)

end subroutine DofoPullTwo

END MODULE Dofo_f

RX_PURE  function  Dofo_Evaluate(d, row ) result (rv) ! row is (eg)  p_N%m_pdofo(k)%m_rowno
    use cfromf_f
    use dofo_f
    use dofoprototypes_f
    use doforigidbody_f
    use rx_control_flags_f

#ifdef NO_DOFOCLASS
           type (dofo),intent(in) ::d   ! or class
#else
           class (dofo),intent(in)::d  ! or type
#endif
    integer, intent(in) ::row
    real(kind=double), dimension(3)	:: rv
    integer::nd,nmasters,i
    real(double) :: s

   if(.not. associated(d%jacobi)) then
        write(*,*) ' ! THIS IS UGLY! IN  Dofo_Evaluate we are in the middle of getting the jacobi '
        rv = 888
        return
    endif
    rv=777
    if( BTEST( ForceLinearDofoChildren,1) ) then
           rv = d%origin(row:row+2)
           if(associated(d%t)) then
               if(ubound(d%t ,1)>0) then
                    rv=rv + matmul(d%jacobi(row:row+2,:),d%t)
               endif
           else
                write(*,*) 'Dofo_Evaluate . T not allocated ',trim(d%m_DofoLine)
           endif
        return
     endif

   select case (d%m_dofotype)
        case (C_dofo_FixedPoint )
            rv = d%origin;         !  write(*,*) ' !IN  Dofo_Evaluate fixed point '
        case (C_dofo_SlideFixedLine,C_dofo_SlideFixedPlane )
            rv = d%origin
            rv=rv + matmul(d%jacobi,d%t) !

        case (C_DOFO_COMPOSITE,C_DOFO_NODENODE_CONNECT)
            rv = d%origin(row:row+2)
            rv=rv + matmul(d%jacobi(row:row+2,:),d%t)

       case (C_DOFO_NODESPLINE_CONNECT )

                 if(row >= ubound(d%jacobi,1)-3 ) then ! its the slave - evaluate the spline at last t
                   s = d%t(d%ndof )
                   rv =d%d5%m_spl%FsplineEvaluate(s) !NNC
                else
                   rv =d%origin(row:row+2)
                   rv=rv + matmul(d%jacobi(row:row+2,:),d%t)
                endif
        case (C_DOFO_STRINGSPLINE_CONNECT )
            nmasters = ubound(d%d5%m_spl%m_ownerString%m_pts,1)
             if(row > nmasters*3 ) then ! its the slave - evaluate the spline at t
                i = 2*nmasters + (2+row)/3
                s = d%t(i )
                rv =d%d5%m_spl%FsplineEvaluate(s) !SSC eval
            else
               rv = d%origin(row:row+2)
               rv=rv + matmul(d%jacobi(row:row+2,:),d%t)
            endif

        case ( C_dofo_SlideFixedSurface  )
                call contact_evaluate(d%d3%m_connectSurf,d%t,rv)
        case ( C_DOFO_SlideFixedCurve  )
                call slidecurve_evaluate(d%d4%m_slider,d%t(1),rv)

        case (C_DOFO_RIGIDBODY)
               rv= Get_DOFO_RigidBodyXWorker(d,row)
        case default  ! intended crash
                nd =0; nd= 1/nd;
    end select
 end function  Dofo_Evaluate



recursive function CreateChildDofo(theNode)  result(ok)
    use ftypes_f
    use dofolist_f
    use dofo_f
    use dofoprototypes_f
    use fglobals_f
    use dofomath
    use fnoderef_f
    use realloc_f
    use rx_control_flags_f
    IMPLICIT NONE
    type(fnode),intent(in) ::theNode

    integer::ok,error
    integer ::i,j,k, nd,m,n,n1,n2,k2,nntot,dum,r
    TYPE( dofoholder), dimension(2) :: dd
#ifdef NO_DOFOCLASS
         type (dofo),pointer ::dnew ,dtemp,d1,d2 ! or class
#else
        class (dofo),pointer ::dnew ,dtemp,d1,d2 ! or type
#endif

    real(kind=double), dimension(:,:),pointer ::j1 ,j2
    integer, dimension(:),allocatable :: r1
    integer, dimension(:),allocatable  :: r2
    real(kind=double), dimension(:),pointer ::O1 ! = (/ 0.,.0,.0,  0.,1.,1. /)
    real(kind=double), dimension(:),pointer  ::O2  ! =(/0.,1,1,0.,0.,2.,0,0.5,1.5 /)
    real(kind=double), dimension(:),pointer  ::t1 ! = (/ 0,0,0,0,0,0  /) ,t2= (/0,0,0,0,0,0/)
    real(kind=double), dimension(:),pointer  :: t2 != (/0,0,0,0,0,0/)
    real(kind=double),  allocatable, dimension(:):: neworigin,tnew
    real(WP),  allocatable, dimension(:,:):: Jnew
     ok=0; OK=1/0;
!  We expect that TheNode has exactly two dofos without children One may have parents.
! so 1) identify the two dofos we want to coalesce.
!   2) make the new child dofo and hook to the parents.
!   3) identify the J1,r1,O1,t1 arrays and point to them
!   4) Call Reduced JMatrix to determine JNew, NewOrigin, Tnew.
    nd=0
di:    DO I = 1, UBOUND(theNode%m_pdofo  ,1)
        dtemp=>theNode%m_pdofo(i)%d
        if(.not. associated(dtemp%child)) then
            nd=nd+1
            dd(nd)%o=>dtemp
            if(nd .ge. 2) exit di
        endif
    enddo di
    if(nd .lt. 2) return

! dd(1)%o and dd(2)%o are the two dofos which share node 'TheNode'
!   2) make the new child dofo and hook to the parents.
    dnew=> CreateDOFO(theNode%m_sli , C_DOFO_COMPOSITE ,error) ! a clean initialisation
    write(outputunit,'(3(a,i2,i6,1x,a))')'(CreateChildDofo)',dd(1)%o%m_dofosli,dd(1)%o%m_dofoN, trim(dd(1)%o%m_DofoLine)," and"  ,dd(2)%o%m_dofosli,dd(2)%o%m_dofoN, trim(dd(2)%o%m_DofoLine) ,' into',dnew%m_dofosli,dnew%m_dofoN

    dnew%m_DofoLine = ' child_dofo'
    dnew%parent1=>dd(1)%o ; d1=> dnew%parent1;
    dnew%parent2=>dd(2)%o; d2=> dnew%parent2;
    dd(1)%o%child=>dnew  ! dd(1)%o is the first dofo
    dd(2)%o%child=>dnew

!   3) identify the J1,r1,O1,t1 arrays and point to them
    dnew%IsLinear = (dd(1)%o%IsLinear .and. dd(2)%o%IsLinear ).or.   BTEST( ForceLinearDofoChildren,0)  ! flag 'LC'
    j1=>dd(1)%o%jacobi
    j2=>dd(2)%o%jacobi
    o1=>dd(1)%o%origin
    o2=>dd(2)%o%origin
    t1=>dd(1)%o%t
    t2=>dd(2)%o%t
! r1 and r2 are the rows in the new J which will correspond to rows in the old J;
! so we walk thru m_Fnodes for each dofo
! first for D1 we assign rows sequentially to the dofs of its m_Fnodes
! then for D2 , if a node is common to a node in D1 we set its R2 the same as that nodes R1
! else we tack them onto the tail of r1 and r2.
! at the same time we can build m_Fnodes for theDOFO
  n1 = ubound(dd(1)%o%m_fnodes,1);   n2 = ubound(dd(2)%o%m_fnodes,1)
  allocate(r1( 3*n1));   allocate(r2( 3*n2)); r2=-1
  k2=1; nntot=n1;
  do i=1, 3*n1
        r1(i) = i;
  enddo
!DEC$ NOPARALLEL
   do i=1, n1
        dum = hookDofoToNode(decodeNodeRef(dd(1)%o%m_fnodes(i) ),dnew,(3*i-2),0 )
  enddo
! now we have  the nodes of D1 in the new list, in their original sequence.
! Now we append the nodes of D2 except where they are in the list of D1
! in pseudocode:
!  for each node in D2
!       Is it in the list of D1????
! if it is:
!       DONT need to hook it to Dnew again
!       DO  copy its R1 row values into D2
! else if it isnt in D1
!       Hook it to DNew at the next available row position
!       ITS r2 values are the next 3 availab
!DEC$ NOPARALLEL
    do J=1,N2
 ! is it in D1  , and where??
         r=-1;
doi:     do i= 1,n1
         if((dd(1)%o%m_fnodes(i)%m_fnr_Sli .eq. dd(2)%o%m_fnodes(j)%m_fnr_Sli) .and.(dd(1)%o%m_fnodes(i)%m_fnrN .eq. dd(2)%o%m_fnodes(j)%m_fnrN)  ) then
            r=i
            exit doi
        endif
        enddo doi
        if( r/=-1) then  ! J is already there at triplet K
            do k=1,3
                r2(3*j-3+k) = r1(3*r-3+k)
            enddo
         else   ! J isnt there so append it.
            nntot=nntot+1
            dum = hookDofoToNode(decodeNodeRef(dd(2)%o%m_fnodes(j)),dnew,(nntot*3-2),0)
            do k=1,3
               r2(3*j-3+k) =  nntot*3-3+k
            enddo
        endif
    enddo
  !  call dofoMatchCoords (dnew%parent1  ,dnew%parent2,r1,r2 )   ! only for node-node connects
!   4) Call  ReducedJmatrix to determine JNew, NewOrigin, Tnew.
     deallocate(dnew%parent1%dofosv ,dnew%parent2%dofosv,stat=k)
      deallocate(dnew%parent1%m_svOrg ,dnew%parent2%m_svOrg,stat=k)
     ok= ReducedJmatrix1( J1,J2,r1,r2,O1,O2,t1,t2,jnew, neworigin,tnew, dnew%parent1%dofosv ,dnew%parent2%dofosv, dnew%parent1%m_svOrg ,dnew%parent2%m_svOrg,dnew%parent1  ,dnew%parent2 )  ! Create ChildDofo
     if(ok ==99) then
    write(outputunit,'(2(a,i4,i5,a) )')'restr mismatch(2) in ',d1%m_dofosli ,d1%m_dofoN ,trim(d1%m_DofoLine),'and ',d2%m_dofosli ,d2%m_dofoN ,trim(d2%m_DofoLine)
    endif
! now create the dofo j, etc.
! common to all types

     m = ubound(jnew,1); n = ubound(jnew,2);    dnew%ndof=n;
    allocate(dnew%jacobi( m,n)) ;               dnew%jacobi=jnew
    allocate(dnew%origin(m))  ;                 dnew%origin=neworigin
        allocate(dnew%t(n));                   dnew%t=tnew

    allocate(dnew%invjacobi(n,m)) ;     dnew%invjacobi=transpose(dnew%jacobi);
        allocate(dnew%vt(n));
        allocate(dnew%rt(n)) ;
        allocate(dnew%xmt(n,n ));
        allocate(dnew%bt(n,n ))
    dnew%vt=0;  dnew%rt=0; dnew%xmt=0;dnew%bt=0;
    ok= DOFOCalcR(dnew)
    ok =ok + DOFOCalcV(dnew)
    ok = CheckSVMatrix(dnew)
     if(.not. ok) then
        write(outputunit,*)'CreateChildDofo CheckSV failure'
            write(321,*)'CreateChildDofo  CheckSV failure'
            ok=ok+ PrintOneDOFO(dd(1)%o ,321,.true.)
            ok=ok+ PrintOneDOFO(dd(2)%o ,321,.true.)
            ok=ok+ PrintOneDOFO(dnew ,321,.true.)
     endif
 ! for this type
    deallocate(r1,r2,jnew,neworigin, tnew)
    ok= ComputeDOFOMass(dnew)

         ok=ok+CreateChildDofo(theNode)   !in Create Child Dofo
end function  CreateChildDofo




function define_All_DOFO_masses() result(ok)
     use saillist_f
     use dofo_f
     use dofoprototypes_f
    IMPLICIT NONE
	INTEGER  :: sli 
	TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
       type (dofo),pointer ::d ! or class
#else
       class (dofo),pointer ::d
#endif
	INTEGER :: i,ok
	ok=0
	if(.not. associated(saillist)) return
dosli:    do sli = 1,ubound(saillist,1)
            if (0== is_Hoisted(sli) ) cycle
            if (0== is_active(sli)) cycle

	        Dofos=>saillist(sli)%Dofos
	        if(.not. ASSOCIATED(Dofos%list)) cycle

	        do i =1, dofos%count  
	        if (.not. associated(Dofos%list(i)%o))  cycle
                if(.not. Dofos%list(i)%used) cycle	  
                d=>Dofos%list(i)%o
                if(associated(d%child)) cycle;
                ok = ComputeDOFOMass(d,.true.)   ! in define All DOFO masses. this fn needs the 'true' to activate it
            enddo
 enddo dosli
    ok=1
  
end function define_All_DOFO_masses

!extern "C"  int cf_create_dofo_rigidbody (const int sli,const int nn, int*theNodes, const RXObject *e, const char*att, const int attlen);
function create_dofo_rigidbody (sli,nnodes, theNodes, e,attin,attlen) result(dofono) bind(C,name="cf_create_dofo_rigidbody" )
    USE, INTRINSIC :: ISO_C_BINDING
    USE ftypes_f
    use dofo_f
    use dofoprototypes_f
    INTEGER (c_int), intent(in),value :: sli,nnodes,attlen
    INTEGER(c_int), dimension(nnodes)   :: theNodes
    integer(kind=cptrsize) , value ::e
    character(len=1),dimension(attlen), intent(in) :: attin

    INTEGER(c_int) ::  dofono

! locals
#ifdef NO_DOFOCLASS
           type (dofo),pointer  ::d  ! or class
#else
           class (dofo),pointer ::d  ! or type
#endif
    integer err,ok
    character(len=attlen) :: att
    att=Transfer(attin,att)
    d=> CreateDOFO(sli, C_DOFO_RIGIDBODY ,err)
    d%m_DofoLine="RB"
    d%m_rxfptr=e
    ok= Complete_rigidBodyDOFO(d, theNodes ,att(:attlen))
    dofono = d%m_dofoN
end function create_dofo_rigidbody

! the Complete function allocates the sub-dofo, tell it about its nodes and other data, and fills it in. It
!make further calls to get Jacobian, mass and pseudo-motion. That means that a DOFO can be created on the fly.
!Those calls should be safe to be called when the nodes dont have mass or velocity.

! NOTE: for a RB dofo, if the preprocessor moves the nodes' undeflected positions
! we need to redo some of this.

function Complete_rigidBodyDOFO(d, theNodes,pAtts )  result (ok)
   use fnoderef_f
   use saillist_f
   use coordinates_f
   use nodeList_f
   use dofoprototypes_f
   use dofo_f
    use ftypes_f
    implicit none
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d,d2   ! or class
#else
           class (dofo),pointer::d ,d2 ! or type
#endif
    integer, dimension(:) ,intent(in) ::  theNodes
    character(len=*), intent(in) :: pAtts
    integer ::  ok
    type (fnode),pointer :: theNode
    type(fnode),pointer :: node
    integer ::i, r, nsixes,j,n1,n2,err ,k
    real(kind=double), dimension(:,:),allocatable :: xi, xc
    real(kind=double), dimension(3) ::gi
    logical :: IsSix
    r=1; nsixes=0
    if(associated(d%d6)) then
         write(*,*) 'WHY ON EARTH??????? re-complete a RB dofo'
         deallocate(d%d6,stat=i)
    endif
    allocate(d%d6)

!    $dof=123 or $dof=123456 ( $fix too for backward compatibility
    if(trim(pAtts) .eq. '123') then
         IsSix=.false.
   else if(trim(pAtts) .eq. '123456') then
        IsSix=.true.
    else
        write(outputunit,*) 'Complete_rigidBodyDOFO ' ,d%m_dofoN,'  atts `',trim(pAtts), "` NOT CODED"
        IsSix=.false.
    endif

      d%ndof=3
      d%d6%nTransNodes=ubound(theNodes,1)
    do i = 1,d%d6%nTransNodes
        node=>saillist(d%m_dofosli)%nodes%xlist(theNodes(i))
       if ( IsSixNode(node)>0)  nsixes = nsixes +1;
        ok= hookDofoToNode(node,d,r,0)
        r=r+3
    end do
    d%d6%firstRotRow = r ; 
    if(IsSix .and. nsixes>0) then
        d%ndof=6  
        do i = 1,d%d6%nTransNodes
            node=>saillist(d%m_dofosli)%nodes%xlist(theNodes(i))
            j= IsSixNode(node) 
            if(j>0) then
                node=>saillist(d%m_dofosli)%nodes%xlist(j)        
                ok= hookDofoToNode(node,d,r,0) ! not sure about flag=0
                r=r+3
            endif
        end do
    endif
    
    ! common to all types
    allocate(d%origin(3*ubound(d%m_Fnodes,1)));              d%origin=0;
    allocate(d%jacobi( 3*ubound(d%m_Fnodes,1),d%ndof)) ;     d%jacobi=0
    allocate(d%invjacobi(d%ndof ,3*ubound(d%m_Fnodes,1))) ;  d%invjacobi=0
    allocate(d%t(d%ndof));   allocate(d%vt(d%ndof));  allocate(d%rt(d%ndof)) ;
    allocate(d%xmt(d%ndof ,d%ndof ));    allocate(d%bt(d%ndof,d%ndof ))
    d%t=0;  d%vt=0;  d%rt=0; d%xmt=0; d%bt=0;    
   
    
    n1 = d%d6%nTransNodes
    n2 =ubound(d%m_Fnodes,1)
    if(allocated( d%d6%xl)) deallocate(d%d6%xl ,stat=err )
    allocate(xi(3,n1)); allocate(xc(3,n2)); allocate(d%d6%xl(3,n1));
    do j=2,n1
        d%m_Fnodes(j)%m_fnrFlags=ibset(d%m_Fnodes(j)%m_fnrFlags  ,DOFO_FLAG_NL ) ! in complete_RB
    enddo
    do j=1,n1
        r = d%m_Fnodes(j)%m_fnrRow
        thenode=>decodenoderef(d%m_Fnodes(j))
        k= fc_getundeflected(theNode%m_RXFEsiteptr, xi(1:3,j))
        xc(1:3,j) =  theNode%XXX ! not sure - is this getcoord??
        if ( any(  abs( xc(1:3,j) - theNode%xxx)>1e-7)) then
            write(outputunit,'(a,2(2x,3g13.5))')'CompleteRBD XXX(1)? ', xc(1:3,j),  theNode%xxx
        endif
        d%origin(r:r+2) = xi(1:3,j)
    enddo
    do j=n1+1,n2   ! the rotational nodes
        r = d%m_Fnodes(j)%m_fnrRow;
        thenode=>decodenoderef(d%m_Fnodes(j))
        xc(1:3,j) =  theNode%XXX
        if ( sum(   ( xc(1:3,j) - theNode%xxx)**2) >1e-7) then
                write(outputunit,*) ' WAS IT RIGHT TO USE XXX HERE (2) ???????????????'
        endif
    enddo
 !   gi = sum(xi, DIM=2)/(dble(  n1))

    gi = xi(1:3,1)    ! orign is the master - and it makes the matrices easier for humans to read
    forall (j=1:n1) d%d6%xl(1:3,j)=  xi(1:3,j) - gi
    d%d6%centroidUndefl = gi
    deallocate(xi,xc)

! common to all complete_XXXDOFO
    ok= ok+ ComputeDOFOJacobian(d)
    ok= ok+ ComputeDOFOMass(d)

    if(FindSharedDOFO(d,d2)) then
        i = DofoMerge(d,d2)  !  !in Complete_rigidBodyDOFO
    endif
 
end function Complete_rigidBodyDOFO

!leave these fns outside or else we get re-entrant 'use' statements.

! the right way to do this for composites would be to walk up the SV tree until we get to the originals, then take the
! mean of the NL evalations of the originals.

RX_PURE  function  Get_DOFO_Cartesian_X(p_N, k ) result (rv) !see prototype in dofoprototypes.f90. K is index into p_N%m_pdofo
    USE IFPORT
    use dofo_f
    use cfromf_f
    use doforigidbody_f
    use dofoprototypes_f
    use fnoderef_f
    use rx_control_flags_f

    type(Fnode),intent(in) :: p_N;
    integer, intent(in) ::k
    real(kind=double), dimension(3)	:: rv
    integer::nd,nmasters,i
    integer:: row
    real(double) :: s 

   if(.not. associated( p_N%m_pdofo(k)%d%jacobi)) then
!        write(*,*) ' ! THIS IS UGLY! IN  Get_DOFO_Cartesian_X we are in the middle of getting the jacobi '
        rv = p_n%XXX
        return
    endif   
    if( BTEST( ForceLinearDofoChildren,1) ) then
#ifdef _DEBUG
            if(.not. associated( p_N%m_pdofo))  write(*,*) ' .not. associated( p_N%m_pdofo) '
            if(k>ubound(  p_N%m_pdofo ,1) ) write(*,*) 'k>ubound(  p_N%m_pdofo ,1) ',k,ubound(  p_N%m_pdofo)
            if(.not. associated( p_N%m_pdofo(k)%d%origin)) write(*,*) ' .not. associated(p_N%m_pdofo(k)%d%origin) '

            row = p_N%m_pdofo(k)%m_rowno
            if(row+2>ubound( p_N%m_pdofo(k)%d%origin ,1) )write(*,*)'row=',row, 'row+ 2>ubound( p_N%m_pdofo(k)%d%origin ,1)'
#endif
           row = p_N%m_pdofo(k)%m_rowno
           rv = p_N%m_pdofo(k)%d%origin(row:row+2)
           if(associated(p_N%m_pdofo(k)%d%t)) then
           if(ubound(p_N%m_pdofo(k)%d%t ,1)>0) then
           rv=rv + matmul(p_N%m_pdofo(k)%d%jacobi(row:row+2,:),p_N%m_pdofo(k)%d%t)
           endif
           else
           write(*,*) 'Get_DOFO_Cartesian_X . T not allocated ',trim( p_N%m_pdofo(k)%d%m_DofoLine)
           endif
     return
     endif

   select case (p_N%m_pdofo(k)%d%m_dofotype) 
        case (C_dofo_FixedPoint )
            rv = p_N%XXX
            
        case (C_dofo_SlideFixedLine,C_dofo_SlideFixedPlane ) 
            rv = p_N%m_pdofo(k)%d%origin
            rv=rv + matmul(p_N%m_pdofo(k)%d%jacobi,p_N%m_pdofo(k)%d%t) ! 

        case (C_DOFO_COMPOSITE,C_DOFO_NODENODE_CONNECT)
#ifndef _DEBUG 
!$OMP CRITICAL(DOFO_01)
#else
            if(.not. associated( p_N%m_pdofo))  write(*,*) ' .not. associated( p_N%m_pdofo) '
            if(k>ubound(  p_N%m_pdofo ,1) ) write(*,*) 'k>ubound(  p_N%m_pdofo ,1) ',k,ubound(  p_N%m_pdofo)
            if(.not. associated( p_N%m_pdofo(k)%d%origin)) write(*,*) ' .not. associated(p_N%m_pdofo(k)%d%origin) '

            row = p_N%m_pdofo(k)%m_rowno
            if(row+2>ubound( p_N%m_pdofo(k)%d%origin ,1) )write(*,*)'row=',row, 'row+ 2>ubound( p_N%m_pdofo(k)%d%origin ,1)'
#endif
            row = p_N%m_pdofo(k)%m_rowno
            rv = p_N%m_pdofo(k)%d%origin(row:row+2)
            rv=rv + matmul(p_N%m_pdofo(k)%d%jacobi(row:row+2,:),p_N%m_pdofo(k)%d%t)  
#ifndef _DEBUG              
!$OMP END CRITICAL(DOFO_01)
#endif       
       case (C_DOFO_NODESPLINE_CONNECT ) 
                row = p_N%m_pdofo(k)%m_rowno
                 if(row >= ubound(p_N%m_pdofo(k)%d%jacobi,1)-3 ) then ! its the slave - evaluate the spline at last t
                   s = p_N%m_pdofo(k)%d %t( p_N%m_pdofo(k)%d%ndof )
                   rv =p_N%m_pdofo(k)%d%d5%m_spl%FsplineEvaluate(s) !NNC getcoord
                else
                   rv = p_N%m_pdofo(k)%d%origin(row:row+2)
                   rv=rv + matmul(p_N%m_pdofo(k)%d%jacobi(row:row+2,:),p_N%m_pdofo(k)%d%t)
                endif
        case (C_DOFO_STRINGSPLINE_CONNECT )
            nmasters = ubound(p_N%m_pdofo(k)%d%d5%m_spl%m_ownerString%m_pts,1)!  method NPTS does this too
            row = p_N%m_pdofo(k)%m_rowno
             if(row > nmasters*3 ) then ! its the slave - evaluate the spline at t
                i = 2*nmasters + (2+row)/3

                s = p_N%m_pdofo(k)%d%t(i )+  p_N%m_pdofo(k)%d%d5%OriginalSlaveTees(i-3*nmasters)
                rv =p_N%m_pdofo(k)%d%d5%m_spl%FsplineEvaluate(s) !SSC getcoord
            else
               rv = p_N%m_pdofo(k)%d%origin(row:row+2)
               rv=rv + matmul(p_N%m_pdofo(k)%d%jacobi(row:row+2,:),p_N%m_pdofo(k)%d%t)
            endif

        case ( C_dofo_SlideFixedSurface  )
                call contact_evaluate(p_N%m_pdofo(k)%d%d3%m_connectSurf,p_N%m_pdofo(k)%d%t,rv) 
        case ( C_DOFO_SlideFixedCurve  )
                call slidecurve_evaluate(p_N%m_pdofo(k)%d%d4%m_slider,p_N%m_pdofo(k)%d%t(1),rv)                                

        case (C_DOFO_RIGIDBODY)
                row = p_N%m_pdofo(k)%m_rowno
               rv= Get_DOFO_RigidBodyXWorker(p_N%m_pdofo(k)%d,row)
        case default  ! intended crash
                nd =0; nd= 1/nd;
    end select       
 end function  Get_DOFO_Cartesian_X

pure function  Get_DOFO_Cartesian_V(p_N) result (rv) !see prototype in dofoprototypes.f90
    use dofo_f

    type(Fnode),intent(in) :: p_N;
    real(kind=double), dimension(3)	:: rv 
    integer::k ,  row

    k = p_n%masterdofoNo
  ! row = p_N%m_pdofo(k)%d%m_dofotype  ! debug trace only
   if(.not. associated( p_N%m_pdofo(k)%d%jacobi)) then
        rv = p_n%V  ! we are in the middle of getting the jacobi
        return
    endif   
   
   select case (p_N%m_pdofo(k)%d%m_dofotype) 
        case (C_dofo_FixedPoint )
            rv = p_N%V
 
        case default  
            row = p_N%m_pdofo(k)%m_rowno       
            rv= matmul(p_N%m_pdofo(k)%d%jacobi(row:row+2,:),p_N%m_pdofo(k)%d%vt)   
    end select       
end function  Get_DOFO_Cartesian_V

pure function  Get_DOFO_Cartesian_R(p_N) result (rv) !see prototype in dofoprototypes.f90
    use dofo_f
    type(Fnode),intent(in) :: p_N;
    real(kind=double), dimension(3)	:: rv 
    integer::k,nd, row
    nd = ubound( p_N%m_pdofo,1)

    k = p_n%masterdofoNo
   if(.not. associated( p_N%m_pdofo(k)%d%jacobi)) then
        rv = p_n%R  ! we are in the middle of getting the jacobi
        return
    endif   
   
   select case (p_N%m_pdofo(k)%d%m_dofotype) 
        case (C_dofo_FixedPoint )
            rv = p_N%R
 
        case default  
            row = p_N%m_pdofo(k)%m_rowno       
            rv= matmul(p_N%m_pdofo(k)%d%jacobi(row:row+2,:),p_N%m_pdofo(k)%d%rt)   
    end select       
 
end function  Get_DOFO_Cartesian_R
 
 
subroutine MapDofoResiduals()  ! a lash-up for dofoTee and dofoJay not being separate.
! the idea here is that after we have calculated the nodal residuals we have to treat those dofos
! in particular the fspline dofo 
!which have some T's corresponding to nodal degrees of freedom and some corresponding to dofo 
!degrees of freedom. 
!SO here we'll calculate the Rt's (and dont do it again this analysis cycle)
!  and redistribute them as necessary

    use saillist_f
    use dofo_f
    use dofolist_f
    use dofoprototypes_f
    use cfromf_f
    use  fnoderef_f
    implicit none
    integer::ok,   sli ,i!,c,j 
    TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
       type (dofo),pointer ::d ! or class
#else
       class (dofo),pointer ::d
#endif

	ok=0
	if(.not. associated(saillist)) return
dosli:  do sli = 1,sailcount  
            if (0== is_Hoisted(sli) ) cycle
            if (0== is_active(sli)) cycle
	        Dofos=>saillist(sli)%Dofos
	        if(.not. ASSOCIATED(Dofos%list)) cycle

doi:       do i = 1, dofos%count  
                if(.not. Dofos%list(i)%used)  cycle
                d=>Dofos%list(i)%o ! 
                if(associated(d%child)) then; cycle; endif
                if(d%ndof ==0) cycle
                ok= DOFOCalcR(d)                         
            enddo doi
        enddo dosli   

end subroutine MapDofoResiduals


function Integrate_DOFO_Motion(starting,ekt,dbgg) result(ok) ! all models
     use saillist_f
     use dofo_f
     use dofoprototypes_f
     use cfromf_f
     use fglobals_f
     use fnoderef_f
     use rx_control_flags_f
    implicit none
    real(kind=double), intent(inout) :: ekt
    logical, intent(in) ::starting
    logical, intent(in), optional ::dbgg
    integer::ok,   sli ,i,err
    logical:: debugg=.false. 
    real(kind=double) :: tempdbl,maxv
    TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
       type (dofo),pointer ::d ! or class
#else
       class (dofo),pointer ::d
#endif
 	type(fnode), pointer ::node
    character*32  fmt
	real(kind=double) :: WentThisFar
	real(kind=double),dimension(2) ::newptuv
	integer,save :: lineNumber
#ifdef PETERDEV
    integer:: SomethingChanged
    SomethingChanged =0
#endif
	ok=0
	maxv=0.01
	if(.not. associated(saillist)) return
  	debugg = .false.
 	if(present(dbgg)) debugg =  dbgg 

!we ASSUME all the Jacobians up to date, for non-linear dofos like slide surfaces.
!Next, we create combination dofos (see Algebra) for all the pairs of DOFOa which share a common node.	
!  see 'grab' in contact_Surfaces.doc

!	                    Starting cycle	    Running cycle
!New dofo, Just grabbed	Accept vt           Accept Vt
!                       T += (1-k) vt       T += (1-k) vt

!Old dofo	            Vt = bt.rt (1/2)    Vt += Bt. Rt
!                       T += vt	            T += vt
    i=0 
 dosli:  do sli = 1,sailcount
            if (0== is_Hoisted(sli) ) cycle
            if (0== is_active(sli)) cycle
	        Dofos=>saillist(sli)%Dofos
	        if(.not. ASSOCIATED(Dofos%list)) cycle
	        i = 1
doi:       do while (i <= dofos%count ) 
                if(.not. Dofos%list(i)%used) then; i=i+1; cycle;endif;
                d=>Dofos%list(i)%o !      if dofo is parent of a conbination cycle (and increment i)
                 ! rt calc is now in   MapDofoResiduals      ! here only one node
           
                if(associated(d%child)) then; i=i+1;  cycle; endif
                if(ubound(d%vt,1) <1)   then; i=i+1; cycle; endif
                if(starting) then              
                     d%vt =          matmul(d%bt , d%rt) * 0.5d00
                else
                     d%vt = d%vt +   matmul(d%bt , d%rt)
                endif

                if(limitV) then
                        tempdbl = sum(abs(d%vt))
                        if(tempdbl .gt. maxv) then
                                d%vt=d%vt/tempdbl*maxv
                        endif
                endif
        select case (d%m_dofotype )
                case(C_dofo_SlideFixedSurface )  
                   newptuv = d%t + d%vt * d%m_remaining_timestep
     ifoff:         if(0 /=  contact_test_passedge(d%d3%m_connectSurf, d%t, newptuv, WentThisFar) ) then

                        d%t = newptuv                  
                        ok = ComputeDOFOJacobian(d) 
                        if(debugg) then
                        write(dofounit,*) 'Integrate DOFO_Motion vt'
                        write(dofounit,*)'(integrate) dofo ',i,'slid off edge -DOnt reset nodal B'
                        endif
                          node=>decodeNodeRef(d%m_Fnodes(1) )
                        node%v = matmul(d%jacobi(1:3,1:2),d%vt) ;  
                        ok=1 ;  
                        node%XXX = Get_DOFO_Cartesian_X(node, node%masterdofoNo) + (1.-WentThisFar) * node%V
                        err=  DestroyDofo (sli, i) 
			            write(dofounit,*) ' dofo destroyed'
                       !   skip the I increment so it cant be a doloop
                        cycle doi
                    else ifoff
                        d%t =d%t + d%vt * d%m_remaining_timestep;
                    endif ifoff
                case(C_dofo_SlideFixedCurve )  
                     if(debugg) write(outputunit,*) i, 'd%t was ', d%t,' V= ',d%vt,' remstep= ', d%m_remaining_timestep
                    ! d%t =d%t + d%vt * d%m_remaining_timestep; 
                     ok = slidecurve_update_param(d%d4%m_slider,d%t(1) ,d%vt(1) * d%m_remaining_timestep ) 
                     if(debugg) write(outputunit,*) '        d%t now ', d%t        
                case default
                        d%t =d%t + d%vt * d%m_remaining_timestep;
                     if(debugg) then  
                            write(fmt,'(  "(i8,1x,a,2i6,"i6,"g14.6)"  )') ubound(d%t,1)
                            write(215,fmt)lineNumber,'tee ',d%m_dofosli,d%m_dofoN, d%t
                            write(216,fmt)lineNumber,'vee ',d%m_dofosli,d%m_dofoN,d%vt
                            write(217,fmt)lineNumber,'rrt ',d%m_dofosli,d%m_dofoN,d%rt
                            lineNumber=lineNumber+1
                       endif
                end select  
                d%m_remaining_timestep=1.0;
                d%ke = dot_product (d%vt,matmul(d%xmt ,d%vt ))
                ekt = ekt + d%ke !  in t-space units but numerically equal to world-space units.
                i=i+1
            enddo doi
        enddo dosli   
#ifdef PETERDEV
      if(.not. BTEST( ForceLinearDofoChildren,0) )  call DofoPullAll( err, SomethingChanged)
#endif
end function Integrate_DOFO_Motion

function Backtrack_DOFO_Motion( dbg) result(ok)
     use dofo_f
     USE saillist_f
     use fnoderef_f
     use dofoprototypes_f
     use cfromf_f
     use rx_control_flags_f
    implicit none
    logical, intent(in), optional ::dbg
    integer::ok  ,nbacks

    TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d   ! or class
#else
           class (dofo),pointer::d  ! or type
#endif
    type(fnode), pointer ::nodee
    real(kind=double) :: WentThisFar, tempdbl, maxv=0.01;
    real(kind=double),dimension(2) ::newptuv
    INTEGER :: i ,err,sli
    logical debugg
#ifdef PETERDEV
    integer:: SomethingChanged
    SomethingChanged =0
#endif

        debugg = .false.
        if (present(dbg))  debugg =  dbg
        nbacks=0
        ok=0;
        if(.not. associated(saillist)) return

        dosli:  do sli = 1,sailcount
                   if (0== is_Hoisted(sli) ) cycle dosli
                   if (0== is_active(sli)) cycle dosli
                   Dofos=>saillist(sli)%Dofos
                   if(.not. ASSOCIATED(Dofos%list)) cycle dosli
	        i = 1
doi:       do while (i <= dofos%count ) 
                if(.not. Dofos%list(i)%used) then; i=i+1; cycle doi ; endif;
                d=>Dofos%list(i)%o
                if(associated(d%child)) then; i=i+1; cycle; endif
                if(ubound(d%vt,1) <1)   then; i=i+1; cycle; endif
                d%vt = -(1.5D0*d%vt) + matmul(d%bt ,d%Rt)* 0.5d0
				if(limitV) then
					tempdbl = sum(abs(d%vt))
					if(tempdbl .gt. maxv) then
						d%vt=d%vt*maxv/tempdbl
						nbacks=nbacks+1 
					endif
				endif
! now we test if any dofo is outside its domain of T. If so we delete it and do the rest of the integration
! directly on the node  
                select case  (  d%m_dofotype)  
                 case (C_dofo_SlideFixedSurface  )           
                        newptuv = d%t + d%vt * d%m_remaining_timestep
     ifoff:             if(0 /= contact_test_passedge(d%d3%m_connectSurf, d%t, newptuv, WentThisFar) ) then

                            nodee=>decodenoderef(d%m_Fnodes(1))  ! nasty to assume its the first in m_fnodes
                            d%t = newptuv                  
                            ok = ComputeDOFOJacobian(d) 
                            write(dofounit,*)'Backtrack_DOFO_Motion vt OK??, K = ', WentThisFar
                                                       
                            nodee%v = matmul(d%jacobi(1:3,1:2),d%vt)        
                             ok=1 
                            nodee%XXX = Get_DOFO_Cartesian_X(nodee, nodee%masterdofoNo) + (1.-WentThisFar) * nodee%V
                            err= DestroyDofo (sli, i) 
                            write(dofounit,*) '(backtrack)DOFO ',i,' slid off the edge (2)- dont reset nodal B'
                            i = i -1 ! so it cant be a doloop
                        else ifoff
                            d%t =d%t + d%vt * d%m_remaining_timestep;
                        endif ifoff
                    case default
                        d%t =d%t + d%vt * d%m_remaining_timestep;
                    end select
                    d%m_remaining_timestep=1.0;
               i=i+1     
            enddo doi
           enddo dosli
!      if(nbacks >0) write(outputunit,*) '       (backtrack) V limited on ', nbacks
    
!Then we map the combination DOFO Jacobians and pseudo-cordinates back onto the raw DOFOs. NO we dont do this
ok=1
#ifdef PETERDEV
 if( .not. BTEST( ForceLinearDofoChildren,0) ) call DofoPullAll( err, SomethingChanged)
#endif
end function Backtrack_DOFO_Motion


subroutine Max_DOFO_Residuals(smax,nmax,rmax)
! records the node with the highest R as (nmax,smax)
! note that smax isnt necessarily the same as the dofo's owning SLI.
    use dofo_f
    USE saillist_f
    use fnoderef_f
    use dofoprototypes_f
    implicit none

    integer, intent(out) :: smax,nmax  ! SLI and node number of the node with highest R. Need not be in this model
    real(kind=double),intent(out),dimension(3) :: rmax
 ! locals

    integer :: sli
    integer:: i ,j,r ! ,k(1) ,kmax,dmax
    real(kind=double), allocatable, dimension(:) ::rr 
    TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
           type (dofo),pointer  ::d   ! or class
#else
           class (dofo),pointer ::d  ! or type
#endif
    type (fnode), pointer :: THeNode
    real(kind=double) :: rThis,rMaxD,temp
    smax=0; nmax=0
! We want to measure the DOFO pseudo-residuals and then map them to the same unit system as the Cartesian residuals.  
!Then the Cartesian residuals will be JT x [pseudo-residuals]	
        rmaxD=-1
        rmax=0;
	if(.not. associated(saillist)) return
mdo2:   DO sli = 1,sailcount
        if(.not. is_Active(sli)) cycle
        Dofos=>saillist(sli)%Dofos
        if(.not. ASSOCIATED(Dofos%list)) cycle
di:     do i =1, dofos%count
            if(.not. Dofos%list(i)%used)  cycle
        d=>Dofos%list(i)%o
        if(d%ndof .eq. 0) cycle
        if(associated(d%child)) cycle

        allocate(rr(ubound(d%jacobi,1))) ! AKA  d%ndof
        select case (d%m_dofotype)
            case( C_dofo_SlideFixedSurface)
                temp = d%rt(3); d%rt(3)=0;
                rr = matmul(d%jacobi,d%rt)
                d%rt(3)  =  temp
            case default
                rr =  matmul(d%jacobi,d%rt) !in world-space
        end select

        do j=1,ubound(d%m_Fnodes,1)
            r = d%m_Fnodes(j)%m_fnrRow
            thenode=>decodenoderef(d%m_Fnodes(j))
            rThis = maxval(abs(rr(r:r+2)  ))
            if(rThis .gt. rMaxD) then
                rMaxD = rThis
                rmax = rr(r:r+2)
                smax = thenode%m_sli
                nmax = thenode%nn
            endif
        enddo

        deallocate(rr)
    enddo di


enddo mdo2
    
end subroutine Max_DOFO_Residuals


function  DOFO_TestForCapture(kit) result (ok) 
   !  for each node which has a contact surface but isnt under dofo control, check wether
 !  it wants to come under dofo control 
    use basictypes_f
    use fglobals_f
    use saillist_f
    use cfromf_f
    use dofoprototypes_f
    use dofolist_f
    use dofo_f
    use coordinates_f
    implicit none
    integer, intent(in) :: kit
    integer :: ok

!locals
    integer ::sli,n,error
    TYPE (sail) ,POINTER :: thesail
    type (fnode), pointer :: nn
     real(kind=double),dimension(3)	:: perpPointUVW, l_xxprop
#ifdef NO_DOFOCLASS
           type (dofo),pointer  ::theDOFO  ! or class
#else
           class (dofo),pointer ::theDOFO  ! or type
#endif
    TYPE ( nodelist) ,pointer	:: nodes
    ok=1
      return   ! disable DOFO_TestForCapture
 	DO sli = 1,SailCOunt
    	    thesail=>saillist(sli)
		  if(is_active(sli)) THEN
		  nodes=>thesail%nodes
do_n:		   	do N=1,nodes%Ncount
		   		nn=>nodes%xlist(N)
		   		if(.not. nn%m_Nused) cycle
                                if(nn%m_connectSurf.ne.0)  cycle
                                if( IsUnderDOFOControl(nn) ) cycle   ! ??WRONG. it may be under control of another dofo

                                l_xxprop= nn%XXX
	            isblw:  if(0 .ne. contact_test_isbelow( nn%m_connectSurf , l_xxprop, perpPointUVW)) then
!$OMP CRITICAL(TestForCapture)
  
                    write(dofounit,'(i6,a,i6,3(1x,f12.5))') kit, ' capture    ' , nn%nn, nn%XXX
                    write(dofodbgunit,'(i6,a,i6,3(1x,f12.5))') kit, ' capture    ' , nn%nn, nn%XXX
            		nn%m_trace=.true.
!               	newptuvw is the Projection the endpt to the surface, 
!               	create a DOFO there with velocity but with remaining timestep zero.
                	theDOFO=> CreateDOFO(sli, C_dofo_SlideFixedSurface ,error) 
                 	ok= Complete_SlideFixedSurfaceDOFO(theDOFO, nn,perpPointUVW,0.0d00)   ! ; write(dofounit,*) ' dofo is ',loc(theDOFO);
                                write(dofounit   ,'(i6,a,i6,4(1x,f12.5))') kit, ' dofoval    ' , nn%nn, get_Cartesian_X_By_Ptr(nn)
            		write(dofodbgunit,'(i6,a,i6,4(1x,f12.5))') kit, ' dofoval    ' , nn%nn, get_Cartesian_X_By_Ptr(nn)  
!$OMP END CRITICAL(TestForCapture)   	    	

                endif isblw
  
			ENDdo do_n
		  ENDIF
		ENDDO  
END FUNCTION  DOFO_TestForCapture  


function  DOFO_UpdateJay( ) result (ok)   ! all dofos, all models
    use dofoprototypes_f
    use coordinates_f
    use saillist_f
    use dofo_f
    implicit none
    integer::ok,   sli
    TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
           type (dofo),pointer  ::d  ! or class
#else
           class (dofo),pointer ::d  ! or type
#endif
    type(sail), pointer :: s
#ifndef NO_DOFOUPDATEORIGIN
     type(fnode), pointer :: thenode
#endif
    INTEGER :: i
    ok=0
    if(.not. associated(saillist)) return
    
dosli:  do sli = 1,sailcount  
            if (0== is_Hoisted(sli) ) cycle
            if (0== is_active(sli)) cycle
            s=>saillist(sli)
#ifndef NO_DOFOUPDATEORIGIN
don:        DO  i=1,s%nodes%ncount
                thenode=> s%nodes%xlist(i)
                if(.not. thenode%m_Nused)   cycle
                thenode%xxx = get_Cartesian_X_by_Ptr(thenode) !   DOFOUPDATEORIGIN
            ENDDO don

#endif
	        Dofos=>saillist(sli)%Dofos
	        if(.not. ASSOCIATED(Dofos%list)) cycle
	        i = 1
doi:       do while (i <= dofos%count )  ! SLOW.  If we have a long chain of nonlinears we call N/2 times too often (!!!!!!)
                if(.not.Dofos%list(i)%used) then ; i=i+1; cycle; endif 
                d=>Dofos%list(i)%o
              
                if(d%IsLinear) then ; i=i+1; cycle; endif 
                If(associated(d%child)) then; i=i+1; cycle; endif
                ok = ComputeDOFOJacobian(d) ! does the call to the child
                i=i+1 
            enddo doi
        enddo dosli   
END FUNCTION DOFO_UpdateJay

function  DOFO_TestForRelease( ) result (ok)  
    use dofoprototypes_f
    use saillist_f
    use dofo_f
    use invert_f
    use fnoderef_f
    implicit none
	integer::ok,   sli 
	TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
           type (dofo),pointer  ::d ! or class
#else
           class (dofo),pointer ::d  ! or type
#endif
	type(fnode), pointer ::node

	INTEGER :: i, err
	ok=0 
	if(.not. associated(saillist)) return
    
dosli:  do sli = 1,sailcount  
            if (0== is_Hoisted(sli) ) cycle
            if (0== is_active(sli)) cycle

	        Dofos=>saillist(sli)%Dofos
	        if(.not. ASSOCIATED(Dofos%list)) cycle
	        i = 1
doi:       do while (i <= dofos%count ) 
                if(.not.Dofos%list(i)%used) then ; i=i+1; cycle; endif 
                d=>Dofos%list(i)%o
              
                if(d%m_dofotype .ne. C_dofo_SlideFixedSurface) then ; i=i+1; cycle; endif 

pr:            if(PleaseRelease(d) ) then ! just tests for positive Rt(3) on slideFixedSurface dofos

                      ok=1  
                   node=>decodeNodeRef( d%m_Fnodes(1) )     

                    ! set   nodal X and V
                    node%v = matmul(d%jacobi(1:3,1:2),d%vt)   ! could generalize to bounds of vt

                    node%XXX = Get_DOFO_Cartesian_X(node, node%masterdofoNo)
                    node%b=minverse(node%xm)  
                    ! delete the dofo
                    err=DestroyDofo (sli, i)
                    exit doi  ! patch for loop. means we can only release on per analysis cycle.
               else pr
                i=i+1 
               endif pr
            enddo doi
        enddo dosli   
 
end function DOFO_TestForRelease

RX_PURE function IsUnderDOFOControl(nn) result (rv)
!DEC$ ATTRIBUTES INLINE :: IsUnderDOFOControl
use ftypes_f
    implicit none
     type(Fnode) :: nn  ! type(Fnode), intent(inout) :: nn
    logical rv  
#ifdef DOFO_CHECKING
    integer:: nd
#endif
     rv = nn%m_isunderdofocontrol   
     if(.not. rv) return
 ! some more checking  (november 2013) we don't believe it if it says YES
#ifdef DOFO_CHECKING
     if( .not. associated( nn%m_pdofo)) then
      write(*,*) ' ISUDC but mpdofo not assoc '
       nn%m_isunderdofocontrol  =.false.
       rv=.false.
    endif
    nd = ubound( nn%m_pdofo  ,1)
     if( nd <1 ) then
        write(*,*) ' ISUDC but mpdofo small ubound ',nd
        nn%m_isunderdofocontrol  =.false.
        rv=.false.
    endif
    if( nd <nn%masterdofoNo ) then
        write(*,*) ' ISUDC but  masterdofono big ',nn%masterdofoNo  ,'ubound =',nd
        nn%m_isunderdofocontrol  =.false.
        rv=.false.
    endif
!    if(associated(nn%m_pdofo(nn%masterdofoNo )%d%child)) then   this occurs in hookdofotonode
!    write(*,*) ' (ISUDC) master dofo has child'
!    endif
#endif
end function IsUnderDOFOControl
 
 

! the Complete function allocates the sub-dofo, tell it about its nodes and other data, and fills it in. It 
!make further calls to get Jacobian, mass and pseudo-motion. That means that a DOFO can be created on the fly.
!Those calls should be safe to be called\\\\\\\ when the nodes dont have mass or velocity.

function Complete_SlideFixedSurfaceDOFO(d, nn,newptuvw, remTimStep)  result(OK)
    use ftypes_f
    use fnoderef_f 
    use dofo_f
    implicit none
#ifdef NO_DOFOCLASS
           type (dofo),pointer  ::d,d2  ! or class
#else
           class (dofo),pointer ::d,d2  ! or type
#endif
    type(fnode) :: nn ! maay try woutptr
    real(kind=double), intent(in), dimension(:) :: newptuvw
    real(kind=double), intent(in) :: remTimStep
    integer :: ok,j
    integer(kind=cptrsize) :: surfaceptr
    real(kind=double), dimension(3) ::vlocal 
        surfaceptr=nn%m_connectSurf  
        ok= hookDofoToNode(nn,d,1,0)
        d%m_Fnodes(1)%m_fnrFlags=ibset(d%m_Fnodes(1)%m_fnrFlags  , DOFO_FLAG_NL )
if(ubound( d%m_Fnodes,1) .ne. 1) then; ok=0; ok=1/ok; endif !force a crash
  
       d%ndof=2  
  ! common to all types (but note jacobian and rt have dimension 3
        allocate(d%jacobi(3*ubound(d%m_Fnodes,1),3)) ;     d%jacobi=0  
        allocate(d%invjacobi(3,3*ubound(d%m_Fnodes,1))) ;     d%invjacobi=0  
 	    allocate(d%t(d%ndof));   allocate(d%vt(d%ndof));  allocate(d%rt(3)) ;
	    allocate(d%xmt (d%ndof ,d%ndof));    allocate(d%bt(d%ndof,d%ndof ) )
        d%t=0;  d%vt=0;  d%rt=0; d%xmt=0; d%bt=0;
    
 ! for this type        
      ok=0;  
       allocate(d%d3)  
  ! calculate and fill in RT, VT   
       d%m_remaining_timestep = remTimStep
       d%t= newptuvw(1:2)
       
       d%d3%m_connectSurf =	surfaceptr
       d%d3%m_DOFO_Connect_Status =1
        ok=ComputeDOFOJacobian(d)

        vlocal = matmul(d%invjacobi,nn%v) ! tr dec 20
        d%vt = vlocal(1:2)
        d%rt = matmul(d%invjacobi,nn%r) ! tr dec 20 ! this is Complete_SlideFixedSurfaceDOFO
        
        ok =ComputeDOFOMass(d)
      !  ok=ok+CreateChildDofo(nn)
        if(FindSharedDOFO(d,d2)) then
            j = DofoMerge(d,d2)  !in Complete_SlideFixedSurfaceDOFO
        endif
end function Complete_SlideFixedSurfaceDOFO

function Complete_NNCDOFO(d, n1,n2)  result(OK)
        use ftypes_f
        use fnoderef_f 
        use dofo_f
        use coordinates_f
        use cfromf_f
        implicit none       
#ifdef NO_DOFOCLASS
           type (dofo),pointer  ::d ,d2 ! or class
#else
           class (dofo),pointer ::d ,d2 ! or type
#endif
        type(fnode) :: n1,n2
        integer ::j, ok
        real(kind=8), dimension(3) :: or,x
        j = fc_getundeflected(n1%m_RXFEsiteptr, or)
        x = get_Cartesian_X_By_Ptr(n1)
        n2%xxx=x
        ok= hookDofoToNode(n1,d,1,0)
        ok= hookDofoToNode(n2,d,4,DOFO_FLAG_SLAVE)
        d%m_Fnodes(2)%m_fnrFlags=ibset(d%m_Fnodes(2)%m_fnrFlags  , DOFO_FLAG_SLAVE )
        d%m_DofoLine="NNC"
       d%ndof=3  

  ! common to all types (but note jacobian and rt have dimension 3
        allocate(d%jacobi(3*ubound(d%m_Fnodes,1),d%ndof)) ;         d%jacobi=0
        allocate(d%invjacobi(d%ndof, 3*ubound(d%m_Fnodes,1))) ;     d%invjacobi=0
        allocate(d%t(d%ndof));   allocate(d%vt(d%ndof));  allocate(d%rt(d%ndof)) ;
        allocate(d%xmt (d%ndof ,d%ndof));    allocate(d%bt(d%ndof,d%ndof ) )
        allocate(d%origin(3*ubound(d%m_Fnodes,1)))
        d%t=0;  d%vt=0;  d%rt=0; d%xmt=0; d%bt=0;
        d%t=x-or
   ! calculate and fill in RT, VT   
    do j=1,4,3
        d%jacobi(j:j+2,:)    = IdentityMatrix(3) 
        d%invjacobi(:,j:j+2) = IdentityMatrix(3)    
    enddo
    d%origin(1:3)= or;  d%origin(4:6)= or  ! rigid link would be the same but we keep origin different
    ok = DOFOCalcV(d)
    ok = DOFOCalcR(d)

    ok =ComputeDOFOMass(d)    

    if(FindSharedDOFO(d,d2)) then
        j = DofoMerge(d,d2)  ! this is in Complete_NNC DOFO
    endif
 
end function Complete_NNCDOFO

function Complete_SlideFixedCurveDOFO(d, nn,u)  result(OK)
   use ftypes_f
    use fnoderef_f 
    use cfromf_f
    use parse_f
    use dofo_f
    implicit none
#ifdef NO_DOFOCLASS
           type (dofo),pointer  ::d,d2  ! or class
#else
           class (dofo),pointer ::d,d2 ! or type
#endif
    type(fnode) :: nn 
    real(kind=double), intent(in) :: u
    integer :: j,ok
    integer(kind=cptrsize) :: surfaceptr
    real(kind=double), dimension(3) ::vlocal 
        surfaceptr=nn%m_slider  
        ok= hookDofoToNode(nn,d,1,0) ;
        d%m_Fnodes(1)%m_fnrFlags=ibset(d%m_Fnodes(1)%m_fnrFlags  , DOFO_FLAG_NL )
        if(ubound( d%m_Fnodes,1) .ne. 1) then; j=0; j=1/j; endif !force a crash

       d%ndof=1  
  ! common to all types (but note jacobian and rt have dimension 3
        allocate(d%jacobi(3*ubound(d%m_Fnodes,1),d%ndof)) ;     d%jacobi=0  ! WHY NOPT 1  WRONG
        allocate(d%invjacobi(d%ndof,3*ubound(d%m_Fnodes,1))) ;     d%invjacobi=0  
 	    allocate(d%t(d%ndof));   allocate(d%vt(d%ndof));  allocate(d%rt(3)) ;
	    allocate(d%xmt (d%ndof ,d%ndof));    allocate(d%bt(d%ndof,d%ndof ) )
        d%t=0;  d%vt=0;  d%rt=0; d%xmt=0; d%bt=0;
    
 ! for this type        
      ok=0;  
       allocate(d%d4)  
  ! calculate and fill in RT, VT   
       d%t= u
      
       d%d4%m_slider =	surfaceptr
       j= fc_getEntityName(surfaceptr, d%m_DofoLine, 250) ; call denull(d%m_DofoLine)
  
        ok=ComputeDOFOJacobian(d)

        vlocal = matmul(d%invjacobi,nn%v) ! tr dec 20
        d%vt = vlocal(1:2)
        d%rt = matmul(d%invjacobi,nn%r) ! tr dec 20 this is Complete_SlideFixedCurveDOFO
        
        ok =ComputeDOFOMass(d) 
     !   ok=ok+CreateChildDofo(nn)
        if(FindSharedDOFO(d,d2)) then
            j = DofoMerge(d,d2)  !!in Complete_SlideFixedCurveDOFO
        endif
end  function Complete_SlideFixedCurveDOFO 


function dofoCompositeJacobian(dnew)  result(ok)
    USE F95_PRECISION, ONLY: WP => DP
    use ftypes_f
    use dofo_f
    use dofomath
    use fnoderef_f
    use rx_control_flags_f
    implicit none
#ifdef NO_DOFOCLASS
     type (dofo),pointer ::dnew  ,d1,d2 ! or class
#else
    class (dofo),pointer ::dnew  ,d1,d2
#endif 
     
    integer::ok 
    integer ::i,j,k, m,n,n1,n2,k2,nntot ,r
    TYPE( dofoholder), dimension(2) :: dd
    
    real(kind=double), dimension(:,:),pointer ::j1 ,j2
    integer, dimension(:),allocatable :: r1, r2     
    real(kind=double), dimension(:),pointer :: O1,O2,t1,t2  

    real(kind=double),  allocatable, dimension(:) :: neworigin,tnew 
    real(wp),  allocatable, dimension(:,:):: Jnew  
    ok=0; 

   dd(1)%o=>dnew%parent1   ;d1=> dnew%parent1;
   dd(2)%o=>dnew%parent2    ;d2=> dnew%parent2;
    
    dnew%IsLinear =  (dnew%parent1%IsLinear .and. dnew%parent2%IsLinear )  .or. BTEST( ForceLinearDofoChildren,0) ! getglobal idem
!   3) identify the J1,r1,O1,t1 arrays and point to them


    j1=>dd(1)%o%jacobi ; 
    j2=>dd(2)%o%jacobi ; 
    o1=>dd(1)%o%origin;  
    o2=>dd(2)%o%origin;  
    t1=>dd(1)%o%t;       
    t2=>dd(2)%o%t ;     
! r1 and r2 are the rows in the new J which will correspond to rows in the old J;
! the following copied from Create Child Dofo above. MUST be consistent with it.
      n1 = ubound(dd(1)%o%m_fnodes,1);   n2 = ubound(dd(2)%o%m_fnodes,1)
      allocate(r1( 3*n1));   allocate(r2( 3*n2)); r2=-1
      k2=1; nntot=n1;
      do i=1, 3*n1
            r1(i) = i;
      enddo  
  
    do J=1,N2
 ! is it in D1  , and where?? 
         r=-1;
doi:     do i= 1,n1
         if((dd(1)%o%m_fnodes(i)%m_fnr_Sli .eq. dd(2)%o%m_fnodes(j)%m_fnr_Sli) .and.(dd(1)%o%m_fnodes(i)%m_fnrN .eq. dd(2)%o%m_fnodes(j)%m_fnrN)  ) then
            r=i
            exit doi
        endif
        enddo doi
  ! now we know if its in D1
        if( r/=-1) then  ! J is already there at triplet K
            do k=1,3
                r2(3*j-3+k) = r1(3*r-3+k)
            enddo
         else   ! J isnt there so append it.
            nntot=nntot+1
       
            do k=1,3
               r2(3*j-3+k) =  nntot*3-3+k
            enddo
        endif
    enddo  
! the danger with this is that we are NOT resetting the rows of the dofo's Fnode list
! to correspond to this new one. We are just following the same algorithm as before
! in the hope that nothing has happened in-between to upset the row nos in Dnew.
    
  !  call dofoMatchCoords( d1,d2,r1,r2)   ! only for node-node connects
!   4) Call ReducedJmatrix to determine JNew, NewOrigin, Tnew.                
    deallocate(dnew%parent1%dofosv ,dnew%parent2%dofosv,stat=k)
     deallocate(dnew%parent1%m_svOrg ,dnew%parent2%m_svOrg,stat=k)   
     ok= ReducedJmatrix1( J1,J2,r1,r2,O1,O2,t1,t2,jnew, neworigin,tnew,dnew%parent1%dofosv ,dnew%parent2%dofosv,dnew%parent1%m_svOrg ,dnew%parent2%m_svOrg, dd(1)%o,dd(2)%o)  !dofo CompositeJacobian
#ifndef NEVER
    if(ok ==99) then
    write(outputunit,'(2(a,i4,i5,1x,a) )')'(dofoCompositeJacobian)restr mismatch(3) in ',d1%m_dofosli ,d1%m_dofoN ,trim(d1%m_DofoLine),'and ',d2%m_dofosli ,d2%m_dofoN ,trim(d2%m_DofoLine)
    endif
#endif
! now create the dofo j, etc.
! common to all types

   deallocate(dnew%jacobi,dnew%origin,dnew%t,dnew%invjacobi,dnew%vt,dnew%rt,dnew%xmt,  dnew%bt);


     m = ubound(jnew,1); n = ubound(jnew,2);    dnew%ndof=n;
    allocate(dnew%jacobi( m,n)) ;               dnew%jacobi=jnew
    allocate(dnew%origin(m))  ;                 dnew%origin=neworigin
 	allocate(dnew%t(n));                   dnew%t=tnew 

    allocate(dnew%invjacobi(n,m)) ;     dnew%invjacobi=transpose(dnew%jacobi);
 	allocate(dnew%vt(n));               
 	allocate(dnew%rt(n)) ;
	allocate(dnew%xmt(n,n ));   
	allocate(dnew%bt(n,n ))
    dnew%vt=0;  dnew%rt=0; dnew%xmt=0;dnew%bt=0;
     ok = CheckSVMatrix(dnew) 
     if(0== ok) then
            write(outputunit,*)'dofo CompositeJacobian CheckSV failure. see file fort.321'
            write(321,*)'dofo CompositeJacobian CheckSV failure'            
            ok=ok+ PrintOneDOFO(dd(1)%o ,321,.true.)
            ok=ok+ PrintOneDOFO(dd(2)%o ,321,.true.)
            ok=ok+ PrintOneDOFO(dnew ,321,.true.)           
     endif     
    k = DOFOCalcR(dnew)  
    k = DOFOCalcV(dnew) 
    deallocate(r1,r2,jnew,neworigin, tnew)
    k= ComputeDOFOMass(dnew,.true.) ! aug 2013 !!!!!!!!!!!!!!!!
end  function dofoCompositeJacobian
 
function update_dofo_origin(sli, dofoN, x )  result(changed)  bind(C,name="cf_update_dofo_origin"  )
    use saillist_f
    use dofo_f
    use dofoprototypes_f
    use fnoderef_f
    IMPLICIT NONE
    integer(c_int),intent(in),value ::sli  ,dofoN
    real(c_double),intent(in), dimension(3) :: x   
    integer (c_int) :: changed 
! locals    
#ifdef NO_DOFOCLASS
           type (dofo),pointer  ::d,d2  ! or class
#else
           class (dofo),pointer ::d,d2  ! or type
#endif
    real(kind=double) distsq   
    integer  ds,dn , j, err,ok
    TYPE ( dofolist) ,POINTER :: dofos
    err=16
    ok=0
    if(.not. associated(saillist)) return
    if(sli > ubound(saillist,1)) return	
    if(sli < lbound(saillist,1)) return

    dofos=>saillist(sli)%dofos
    err=0 ; ok=1

    d=>dofos%list(dofoN)%o 

    distsq = dot_product(d%origin(1:3) - x, d%origin(1:3) - x)
    if(distsq > 1e-10) then
       !   write(outputunit,*)' change dofo origin'
      !   write(outputunit,'(a,3f14.5)')'new value( from LKG):',x
       !   write(outputunit,'(a,3f14.5)')'origin was          :',d%origin(1:3);

        if(associated( d%child))  then
            ds =d%child%m_dofosli ; dn = d%child%m_dofoN;
           ! write(outputunit,'(a,i5,i5,a,i5,i5,a  )')"(update_dofo_origin) deleting childDofo ",ds,dn
            err =err +DestroyDofo ( ds ,dn)
        endif 
        d%origin= x;                     
        ok= ok+ ComputeDOFOJacobian(d)
        ok= ok+ ComputeDOFOMass(d)
         if(FindSharedDOFO(d,d2)) then
             j = DofoMerge(d,d2)  ! this is in update_dofo_origin
         endif
        changed=1
        return
     endif
    changed=0
 end  function update_dofo_origin
  
    
function dofo_from_fixingstring(sli,theNode, line) result(DofoNumber)
    use coordinates_f
    use saillist_f
    use dofolist_f
    use dofo_f
    use dofoprototypes_f
    use parse_f
    use cfromf_f
        IMPLICIT NONE
        integer,intent(in) ::sli  
        type(fnode) ::theNode 
        character(len=*), intent(in) ::line
        integer DofoNumber,ok,ds,dn
        Logical Lerror
        REAL*8 z
        real(kind=double):: xlkg(3)	
        CHARACTER (len=32),  dimension(10) ::  items
        CHARACTER*1  Delimiters(6)
        data delimiters /' ', '//' , ',' ,'\\','	',':'/
        integer ni,k, l_dofotype
        integer i,nd,error,j
#ifdef NO_DOFOCLASS
           type (dofo),pointer  ::theDOFO,d2 ! or class
#else
           class (dofo),pointer ::theDOFO,d2 ! or type
#endif
        real(kind=double), dimension(3):: origin,v,vt,Slide_Vector
        error=0; lerror=.false.
        CALL Strip_Brackets(line)  ! only a closing round bracket
    
        if(isunderdofocontrol(theNode ) ) then    ! second call to dofo-from-fixingstring.
             nd = ubound(theNode%m_pdofo ,1)
di:          do i=1, ubound(theNode%m_pdofo ,1)
                theDOFO=>TheNode%m_pdofo(i)%d
                if(theDOFO%m_DofoLine .eq. trim(line)) then
                    ok = fc_getlastknowngood (TheNode%m_RXFEsiteptr,xlkg)	               
                    write(outputunit,*)'node',theNode%nn,' if coords are new we should change the dofos here'
                    write(outputunit,'(a,3f14.5)')'nodeXXX is ',thenode%XXX
                    write(outputunit,'(a,3f14.5)')'LKG   is   ',xlkg
                    write(outputunit,'(a,3f14.5)')'origin is  ',theDofo%origin(1:3);
                    DofoNumber = theDOFO%m_dofoN  
 
                    if(associated( thedofo%child))  then
                        ds =thedofo%child%m_dofosli ; dn = thedofo%child%m_dofoN;
                        write(outputunit,'(a,i5,i5,a,i5,i5,a  )')"deleting childDofo ",ds,dn   
                        error=error+DestroyDofo ( ds ,dn)
                    endif 
                    thedofo%origin= xlkg;                     
                    ok= ok+ ComputeDOFOJacobian(thedofo)
                    ok=ok+ ComputeDOFOMass(thedofo)
                 !   ok=ok+CreateChildDofo(TheNode)
                    if(FindSharedDOFO(thedofo,d2)) then
                         j = DofoMerge(thedofo,d2)  !   !in    !dofo_from_fixingstring
                    endif
                    return 
                else
                    write(outputunit,*) 'DOFO lines differ ',i,"<", trim(theDOFO%m_DofoLine ),">,<", trim(line),">"
                    DofoNumber = -1
                endif
             enddo di
        endif

        ni = 10  
        CALL Parse(line,Delimiters, items, ni)

if_ni_0:  If(NI .lt.3 .or. line .eq.'full' ) THEN   ! fully fixed
                    l_dofotype= C_dofo_FixedPoint
         else if_ni_0  ! its a slider
  		            l_dofotype= C_dofo_SlideFixedLine       	    
  	   	            read(line,*,err=14) (v(k),k=1,3)
if_ni_3:	   	    if(NI .gt. 3) THEN
			            read(line,*,err=20) (v(k),k=1,3) ,   k   
			            l_dofotype= C_dofo_SlideFixedPlane
		            endif if_ni_3
20	   	            CALL normalise(v,z,Lerror)
		            if(Lerror)  error = error + 1
                    Slide_Vector =Transform_VectorBySLI(sli,v )
        ENDIF if_ni_0 
lrror:  if(.not. Lerror) then
        origin =   get_Cartesian_X_By_Ptr(theNode) ! dofo direction is V, origin is theNode
        select case (l_dofotype) 
            case (C_dofo_FixedPoint )
                    theDOFO=> CreateDOFO(sli, l_dofotype ,error) ! a clean intiialisation
                    ok= Complete_FixedPointDOFO(theDOFO, theNode,origin) 
                    DofoNumber = theDOFO%m_dofoN
            case (C_dofo_SlideFixedLine ) 
                    theDOFO=> CreateDOFO(sli, l_dofotype ,error) 
                    vt =Transform_VectorBySLI(sli,v )
                    ok= Complete_SlidelineDOFO(theDOFO, theNode, origin, vt) 
                    DofoNumber = theDOFO%m_dofoN
            case (C_dofo_SlideFixedPlane)
                    theDOFO=> CreateDOFO(sli, l_dofotype ,error) 
                    vt=  Transform_VectorBySLI(sli,v )
                    ok= Complete_SlidePlaneDOFO(thedofo,thenode, origin, vt)
                    DofoNumber = theDOFO%m_dofoN
            case default
                write(outputunit,*) 'default fixity case ', l_dofotype,"  ", trim(line) 
            end select  
            theDOFO%m_DofoLine=line
      return 
    endif lrror
14    write(outputunit,*)' read fixings error Vector <', trim(line),'>'

      DofoNumber = - 1 
     ! returns DofoNumber
end function dofo_from_fixingstring
#ifdef NeVER
function DOFOCalcLinearT(d) result (ok) !  verified. estimates T via least-squares from X
    use ftypes_f
    use fnoderef_f
    use coordinates_f
    use dofomath  
    implicit none
#ifdef NO_DOFOCLASS
           type (dofo),pointer,intent(inout)  ::d  ! or class
#else
           class (dofo),pointer,intent(inout) ::d  ! or type
#endif
    type(fnode) , pointer :: theNode
    real(kind=double), allocatable, dimension(:) :: cartesian
    real(kind=double), allocatable, dimension(:,:) :: a 
    integer :: ok,j ,r,np                  
    ok=1; np = ubound(d%m_Fnodes,1);
    if(np<1) return;
    allocate(cartesian(3*np)); 
    write(outputunit,*) 'DOFOCalcLinearT on ', d%m_dofoN   
    write(outputunit,*) ' but it looks wrong' 
    ok=0; ok=1/ok;
    
    do j=1,np
        r = d%m_Fnodes(j)%m_fnrRow
        thenode=>decodenoderef(d%m_Fnodes(j))
        cartesian(r:r+2) = get_Cart esian _X_By_Ptr(thenode)-d%origin(r:r+2)
    enddo 
    allocate(a(ubound(d%jacobi,1),ubound(d%jacobi,2))); a=d%jacobi
    call  LeastSquaresSolution(cartesian,a,d%t) 
 
    deallocate(cartesian,a);
end function DOFOCalcLinearT
#endif
recursive function SetParentTeesBySV(d,IsRecursive) result(ok)
    use ftypes_f
#ifdef NO_DOFOCLASS
           type (dofo),pointer, intent(inout)  ::d  ! or class
#else
           class (dofo),pointer, intent(inout) ::d  ! or type
#endif
    logical, intent(in) :: IsRecursive
    integer::ok
    ok=0
    if(associated(d%parent1 )) then
        if(d%parent1%ndof> 0  ) then
        if(allocated(d%parent1%dofosv )) then
            d%parent1%t= matmul(d%parent1%dofosv,d%t)
            if(allocated(d%parent1%m_svOrg )) then
                  d%parent1%t= d%parent1%t + d%parent1%m_svOrg
            endif            
            ok=1;
         else
             if(d%parent1%m_dofotype .ne.1) write(outputunit,*)' Its WIERD that a parent1 has no SV.  its dofotype is ', d%parent1%m_dofotype
         endif
         endif
    endif
    if(.not. associated(d%parent2 )) return;
    if( d%parent2%ndof == 0)  return
    if(.not. allocated(d%parent2%dofosv )) then
         if(d%parent2%m_dofotype .ne.1) write(outputunit,*)'WIERD a parent2 has no SV. its dofotype is ', d%parent2%m_dofotype, 'its ndof ', d%parent2%ndof
        return; 
    endif
    d%parent2%t = matmul(d%parent2%dofosv,d%t)
    if(allocated(d%parent2%m_svOrg )) then
         d%parent2%t= d%parent2%t + d%parent2%m_svOrg
    endif 
    ok=ok+1
    if(IsRecursive) then
     if(associated(d%parent1 )) then
     ok=ok + SetParentTeesBySV(d%parent1 ,.TRUE.)
    endif
    if(associated(d%parent2 )) then
    ok=ok + SetParentTeesBySV(d%parent2 ,.TRUE.)
   endif
    endif
end function SetParentTeesBySV

! there are big errors in transformDOFO. some Tees have units of coordinates and some (like rotational DOFs) of vectors
! also some Origins refer to rotations and so are vectors.
! we should identify these cases and do the work.
! but the August 2013 ordering problem isnt related by this.
! and anyway, I think its only a problem when we hoist onto a deflected model.
	
function TransformDofo(d,mt) 	result(err)
use ftypes_f
use dofo_f
use basictypes_f
use saillist_f
    IMPLICIT NONE  
#ifdef NO_DOFOCLASS
           type (dofo),pointer  ::d  ! or class
#else
           class (dofo),pointer ::d  ! or type
#endif
	REAL (kind=double),intent(in), dimension(4,4):: mt 
        INTEGER :: err,ok
        integer :: sn,i,n1,n2
        real(kind=double), dimension(3) :: xx
        ! write(*,*) 'TransfO rmDofo',d%m_dofosli, d%m_dofon, d%m_dofotype
        err=0; ok=1
  	sn = d%m_dofosli
        do i=1, ubound(d%origin,1),3  ! this is WRONG for six-nodes
                CALL XtoLocal(sn , d%origin(i:i+2) ,  xx) ; 		! write(*,'("from ",3(1x,g14.8),$)') d%origin(i:i+2)
                 d%origin(i:i+2) = MATMUL(TRANSPOSE(mt(1:3,1:3)), xx)+ mt(4,1:3) ; !write(*,'(" to ",3(1x,g14.8) )') d%origin(i:i+2)
        enddo
 
! It was wrong to do the jacobians here because the model is only partially transformed at the time
 	
        select case (d%m_dofotype) 
            case (C_dofo_SlideFixedLine)
                CALL VtoLocal(sn ,d%d1%vector  ,  xx)
                d%d1%vector  = MATMUL(TRANSPOSE(mt(1:3,1:3)), xx)
             case (C_dofo_SlideFixedPlane)
                CALL VtoLocal(sn ,  d%d2%normal  ,  xx)
                d%d2%normal  = MATMUL(TRANSPOSE(mt(1:3,1:3)), xx)
            case (C_DOFO_RIGIDBODY)
                  if(associated(d%d6) ) then
                    n1 = d%d6%nTransNodes
                    n2  =ubound(d%d6%xl,2)
                    do i=1, n2
                        CALL VtoLocal(sn , d%d6%xl(1:3,i)  ,  xx)
                        d%d6%xl(1:3,i)  = MATMUL(TRANSPOSE(mt(1:3,1:3)), xx)
                    end do
                    CALL XtoLocal(sn , d%d6%centroidUndefl ,  xx) ;
                    d%d6%centroidUndefl = MATMUL(TRANSPOSE(mt(1:3,1:3)), xx)+ mt(4,1:3)
                else
                    write(outputunit,*) '    ................... NO D6'
                endif
             case default
               ok=1
         end select    
        if(ok ==0) err=err+1
end  function TransformDofo

! the Complete function allocates the sub-dofo, tell it about its nodes and other data, and fills it in. It 
!make further calls to get Jacobian, mass and pseudo-motion. That means that a DOFO can be created on the fly.
!Those calls should be safe to be called when the nodes dont have mass or velocity.

function Complete_NodeSplineDOFO(d, p_theNode,spl)  result (ok)
   use fnoderef_f
   use coordinates_f
   use dofoprototypes_f
   use dofo_f
    implicit none
#ifdef NO_DOFOCLASS
           type (dofo),pointer  ::d,d2  ! or class
#else
           class (dofo),pointer ::d ,d2 ! or type
#endif
    type(fnode), pointer,intent(in) :: p_theNode 
    class(fsplineI), pointer ::spl
    integer :: i,r,ok ,nrows
    type(fnode), pointer :: l_Node
    real(kind=double),dimension(3)  ::x, splinept,deriv
    real(kind=double)  ::s 
    nrows = ubound(spl%m_ownerString%m_pts,1)*3+3
    d%m_DofoLine = 'nodeSPlineDOFO'
    allocate(d%origin(nrows) )
 ! we have to call these before we hook up the spline nodes or else we cant evaluate the spline   
    x = get_Cartesian_X_By_Ptr(p_theNode)    
    s = spl%FindNearest(x)
        splinept = spl%FsplineEvaluate(s)   ! s is d%t(d%ndof) Complete_NodeSplineDOFO
        deriv    = spl%derivativebyds(s)
     write(outputunit,'(a,3f11.5,a,g14.5,a,3f11.5)',iostat=i) 'Complete_NSD ',x,' initialT=',s, 'found= ',splinept
     if(i /=0) write(outputunit,*) '(Complete nodeSplineDoFO) BAD Node  ',sngl(x)	   
    r=1
!DEC$ NOPARALLEL    
    do i=1, ubound(spl%m_ownerString%m_pts,1)
        l_Node=>decodenoderef(spl%m_ownerString%m_pts(i))
        d%origin(r:r+2) = get_Cartesian_X_By_Ptr (l_Node)
        ok= hookDofoToNode(l_Node,d,r,0)
        r=r+3
    enddo
    d%origin(r:r+2) = -99799.  ! gets filled in by Jacobi. splinept-deriv*s
    ok= hookDofoToNode(p_theNode,d,r,DOFO_FLAG_SLAVE)   ! the slave is last in the dofo's list

    d%m_Fnodes(ubound( d%m_Fnodes,1))%m_fnrFlags=ibset(d%m_Fnodes(ubound( d%m_Fnodes,1))%m_fnrFlags  , DOFO_FLAG_NL )
    d%m_Fnodes(ubound( d%m_Fnodes,1))%m_fnrFlags=ibset(d%m_Fnodes(ubound( d%m_Fnodes,1))%m_fnrFlags  , DOFO_FLAG_SLAVE )
    allocate(d%t(nrows-2));  d%t=0;
    d%jacobi=>spl%FSplineJMatrix(d,d%origin,s);
    d%ndof =  ubound(d%jacobi,2)
    if( d%ndof .ne. nrows-2) stop ' NROWS'
   
    allocate(d%d5); d%d5%m_spl=>spl  ; d%d5%OriginalSlaveTee=s
    ok=NodeSplineDOFO_Inverse(d) ! in the end, just the transpose

        allocate(d%vt(d%ndof));  allocate(d%rt(d%ndof)) ;
	allocate(d%xmt(d%ndof ,d%ndof ));    allocate(d%bt(d%ndof,d%ndof ))
     d%vt=0;  d%rt=0; d%xmt=0; d%bt=0;
    if( d%d5%m_spl%sliding  ) then
        d%t(d%ndof) = s
    else
        write(outputunit,*) "OOF! found the typo"
    endif
    ok= ComputeDOFOMass(d)

    if(FindSharedDOFO(d,d2)) then
        i = DofoMerge(d,d2)  ! this is in Complete_NodeSplineDOFO
    endif

end function Complete_NodeSplineDOFO

! the Complete function allocates the sub-dofo, tell it about its nodes and other data, and fills it in. It 
!make further calls to get Jacobian, mass and pseudo-motion. That means that a DOFO can be created on the fly.
!Those calls should be safe to be called when the nodes dont have mass or velocity.
function Complete_StringSplineDOFO(d, theConn,spl)  result (ok)
    use fnoderef_f
    use coordinates_f
     use saillist_f
    use dofo_f
    use dofoprototypes_f
    implicit none

#ifdef NO_DOFOCLASS
           type (dofo),pointer  ::d  ! or class
#else
           class (dofo),pointer ::d  ! or type
#endif
    TYPE (Connect),POINTER,intent(inout) :: theConn

    class(fsplineI), pointer,intent(inout) ::spl
    integer :: i,j,r,ok ,nrows,nslaves, SlaveRowStart
    type(fnode), pointer :: l_Node
    real(kind=double), dimension(3)  ::xtemp
    real(kind=double),allocatable, dimension(:,:)  ::x, splinept,deriv
    real(kind=double),allocatable, dimension(:)  ::ss
    TYPE (Nconntype)  ,POINTER :: sl
#ifdef NO_DOFOCLASS
           type (dofo),pointer  ::otherd ! or class
#else
           class (dofo),pointer ::otherd ! or type
#endif
     type(fnode), pointer :: theNode    
      theNode=>NULL()
  nslaves = ubound(theConn%slavelist  ,1)
  nrows = ubound(spl%m_ownerString%m_pts,1)*3+3*nslaves
  allocate(x(nslaves,3));   allocate(splinept (nslaves,3));   allocate(deriv (nslaves,3));  allocate(ss(nslaves))   
 ! sample
 
      do j=1, nslaves
                sl=>theConn%slavelist(j)
                if( sl%m_ncsli .ne. theConn%m_sli2) write(outputunit,*) ' Complete_StringSplineDOFO(1) DOFO SLI Disagree'
                theNode=>saillist(sl%m_ncsli)%nodes%xlist(sl%m_ncn)
       enddo 
! end sample     
 

    d%m_DofoLine = 'SSD' ! 'StringSPlineDOFO'
     allocate(d%origin(nrows) )    
 ! we have to call these before we hook up the spline nodes or else we cant evaluate the spline   
      do j=1, nslaves
                sl=>theConn%slavelist(j)
                if( sl%m_ncsli .ne. theConn%m_sli2) write(outputunit,*) 'Complete_StringSplineDOFO(2) DOFO SLI Disagree'
                theNode=>saillist(sl%m_ncsli)%nodes%xlist(sl%m_ncn) 
                x(j,:) = get_Cartesian_X_By_Ptr(theNode)  ; xtemp = x(j,:)  
                ss(j) = spl%FindNearest(x(j,:))
                    splinept(j,:) = spl%FsplineEvaluate(ss(j))   ! s is d%t(d%ndof) !completeSSD
                    deriv(j,:)    = spl%derivativebyds(ss(j))
     enddo	   
    r=1
! hook up the masters 
!DEC$ NOPARALLEL  
    do i=1, ubound(spl%m_ownerString%m_pts,1)
        l_Node=>decodenoderef(spl%m_ownerString%m_pts(i))
        d%origin(r:r+2) = get_Cartesian_X_By_Ptr (l_Node)
        ok= hookDofoToNode(l_Node,d,r,0)
        r=r+3
    enddo
   
! hook up the slaves
      SlaveRowStart=r
!DEC$ NOPARALLEL
      do j=1,nslaves
                sl=>theConn%slavelist(j)
                if( sl%m_ncsli .ne. theConn%m_sli2) write(outputunit,*) 'Complete_StringSplineDOFO(3) DOFO SLI Disagree'
                theNode=>saillist(sl%m_ncsli)%nodes%xlist(sl%m_ncn) 
                d%origin(r:r+2) = -99779.  ! gets filled in by Jacobi. splinept-deriv*s
                ok= hookDofoToNode(theNode,d,r,DOFO_FLAG_SLAVE)   ! the slaves are last in the dofo's list
                d%m_Fnodes(ubound( d%m_Fnodes,1))%m_fnrFlags=ibset(d%m_Fnodes(ubound( d%m_Fnodes,1))%m_fnrFlags  , DOFO_FLAG_NL )
                d%m_Fnodes(ubound( d%m_Fnodes,1))%m_fnrFlags=ibset(d%m_Fnodes(ubound( d%m_Fnodes,1))%m_fnrFlags  , DOFO_FLAG_SLAVE )
                r=r+3
    enddo
    i = 3* ubound(spl%m_ownerString%m_pts,1)
    if (spl%Sliding ) i=i+ nslaves
    allocate(d%t(i));    d%t=0;
    d%jacobi=>spl%FSplineJMatrixMultiple(d,d%origin,ss);
    d%ndof =  ubound(d%jacobi,2)  
    if( d%ndof .ne. i) then
        write(*,*) " BUG in dofo jacobi dims", ubound(d%jacobi) ," but t dims ", ubound(d%t), "nslaves= ",nslaves
        write(*,*)' NROWS Multiple - crash likely' ; flush(6);

    endif

    allocate(d%d5); d%d5%m_spl=>spl ;
    allocate( d%d5%OriginalSlaveTees(nslaves )); d%d5%OriginalSlaveTees=ss
    allocate( d%d5%SlaveTeeOrigin(nslaves ));
    ok=NodeSplineDOFO_Inverse(d) ! in the end, just the transpose

    allocate(d%vt(d%ndof));  allocate(d%rt(d%ndof)) ;
    allocate(d%xmt(d%ndof ,d%ndof ));    allocate(d%bt(d%ndof,d%ndof ))
    d%vt=0;  d%rt=0; d%xmt=0; d%bt=0;
    if( d%d5%m_spl%sliding  ) then
        d%t(SlaveRowStart:d%ndof) = 0  ! ss
    endif
    
    deallocate(x,splinept,deriv ,ss )     

    ok= ComputeDOFOMass(d)

    if(FindSharedDOFO(d,otherd)) then
        i = DofoMerge(d,otherd)  ! this is in Complete_StringSplineDOFO
    endif

end function Complete_StringSplineDOFO



recursive function ComputeDOFOJacobian(d) result(ok)
    use dofoprototypes_f  ! this function is prototyped there
    use ftypes_f
    use doforigidbody_f
    use cfromf_f
    use invert_f
    use fnoderef_f,only:decodenoderef
    use coordinates_f
    use dofo_f

    implicit none
#ifdef NO_DOFOCLASS
           type (dofo),pointer, intent(inout)  ::d  ! or class
#else
           class (dofo),pointer, intent(inout)  ::d  ! or type
#endif

    integer ::ok,istat
    real(kind=double), dimension (3) :: yaxis  
    real(kind=double), dimension (3) :: zaxis  
    real(kind=double), dimension (9) :: j  
    real(kind=double), dimension (:),pointer :: localU, localV
    real(kind=double), dimension( 3) ::  lsplinept
    REAL(kind=double) :: length  ,s
    integer, dimension(2) :: shape = (3,3)
    LOGICAL :: l_Lerr   
    ok=1;
    data yaxis /0.,1.e0,0./
    data zaxis /0.,0.,1.e0/
    l_Lerr =.false. 

    ok = SetParentTeesBySV(d,.FALSE. ) ! in ComputeDOFOJacobian
     if(associated(d%parent1)) then
        if(.not. associated(d%parent1%parent1)) then
            if ( associated(d%parent1%parent2)) write(outputunit,*)' parents inconsistent'
        endif
        ok=ok + ComputeDOFOJacobian(d%parent1)
     endif
      if(associated(d%parent2)) then
        if(.not. associated(d%parent2%parent1)) then
            if ( associated(d%parent2%parent2)) write(outputunit,*)' parents inconsistent'
        endif
        ok=ok + ComputeDOFOJacobian(d%parent2)
     endif    
     
    select case (d%m_dofotype) 
        case (C_dofo_FixedPoint )
             d%jacobi=0;   d%invjacobi=0;    
        
        case (C_dofo_SlideFixedLine ) 
            d%jacobi(1:3,1)=d%d1%vector 
            d%invjacobi = transpose(d%jacobi)
            
        case (C_dofo_SlideFixedPlane)
           !  call normalise( d%d2%normal,length,err) bad not to do it but it fails to build under intel
             localU =>d%jacobi(1:3,1)
             localV =>d%jacobi(1:3,2)

             localU = crossproduct(yaxis, d%d2%normal) 
             if (dot_product(localU,localU) < 1e-6) then
                 localU = crossproduct(zaxis, d%d2%normal)              
             endif   
             call normalise(localU,length,l_Lerr)
             localV = crossproduct(d%d2%normal,localU)
             d%invJacobi = transpose(d%jacobi)
        case ( C_dofo_SlideFixedSurface  )
             ok = ok+ contact_find_jacobian(d%d3%m_connectSurf,d%t(1:2),j)
             ! now reshape J into 
             d%jacobi=reshape(j, shape) ! verified it's not the trans pose
             d%invJacobi = minverse(d%jacobi)
             write(outputunit,*) 'what about oRIGin of SlideFixedSurfaceDOFO'
          
         case ( C_DOFO_SlideFixedCurve  )  ! note these dont have an origin so we cant combine them with others
             ok = slidecurve_find_jacobian(d%d4%m_slider,d%t(1),j)  
             ! now reshape J into 
             d%jacobi=reshape(j, shape)  

            d%invJacobi = transpose(d%jacobi) 
             
         case( C_DOFO_NODESPLINE_CONNECT)

            if(d%d5%m_spl%Sliding) then
                s= d%t(d%ndof)
                lsplinept = d%d5%m_spl%FsplineEvaluate(s)   ! s is d%t(d%ndof) !NSC ComputeDOFOJacobian
            else
                s = d%d5%OriginalSlaveTee             
            endif 
            deallocate( d%jacobi, d%invjacobi ,stat=istat); 
            d%jacobi=>d%d5%m_spl%FSplineJMatrix(d,d%origin,s)

             ok=NodeSplineDOFO_Inverse(d)
        case(C_DOFO_COMPOSITE ) 
             ok =ok+  dofoCompositeJacobian(d)
         case(C_DOFO_NODENODE_CONNECT)
            ! do nothing. It will still be  a sequence of identity matrices 
        case (C_DOFO_STRINGSPLINE_CONNECT )
             ok = ok+ dofoStringSplineJacobian(d)
         case (C_DOFO_RIGIDBODY)
             ok =ok+ dofoRigidBodyJacobian(d);
        case default
            write(dofounit,*) 'ComputeDOFOJacobian default  case ', d%m_dofotype
            ok=0/0
        end select 
        ok = .not. l_Lerr   

end function  ComputeDOFOJacobian

function DOFO_commonNodes(d1,d2, nn1,nn2) result (k) ! nn1,nn2 are indices in the dofos %m_fnodes
use ftypes_f
use dofo_f
use dofoprototypes_f
use realloc_f
implicit none
#ifdef NO_DOFOCLASS
           type (dofo),pointer, intent(in) :: d1,d2  ! or class
#else
           class (dofo),pointer, intent(in) :: d1,d2 ! or type
#endif

        integer, pointer, dimension(:), intent(out):: nn1,nn2
        integer:: i,j,k,n1,n2,err
        k=0
        n1=ubound(d1%m_fnodes,1)
        n2=ubound(d2%m_fnodes,1)
        do J=1,N2
        doi:     do i= 1,n1
             if((d1%m_fnodes(i)%m_fnr_Sli .eq. d2%m_fnodes(j)%m_fnr_Sli) .and.(d1%m_fnodes(i)%m_fnrN .eq. d2%m_fnodes(j)%m_fnrN)  ) then
                k=k+1
                call reallocate(nn1,k,err);
                if(err .ne. 0) return
                call reallocate(nn2,k,err);
                if(err .ne. 0) return
                nn1(k)=i
                nn2(k)=j
            endif
            enddo doi
        enddo

end function DOFO_commonNodes


subroutine UpdateAllTeesBySV()

use ftypes_f
use dofoprototypes_f
IMPLICIT NONE
   INTEGER   :: err
   TYPE (dofolist) :: ll
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif
   INTEGER :: i,ok
    ok=0
    call CollectDofosToList( ll  , DOFO_ONLY, DOFO_EXCEPT, C_DOFO_ANY, err)
   ! write(*,*) 'UpdateAllTeesBySV ', ll%count
     if(ll%count .eq.0) return

     do i= 1,ll%count
         d=>ll%list(i)%o
        ok = OK+ SetParentTeesBySV(d,.TRUE. ) ! in Update AllTeesBySV
     enddo

    deallocate(ll%list, stat=err)

end subroutine UpdateAllTeesBySV
