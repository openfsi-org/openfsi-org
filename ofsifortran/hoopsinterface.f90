module hoopsinterface
               USE, INTRINSIC :: ISO_C_BINDING
!
!  Dont forget to append a char(0) to every input string.  
!
!   EG  call hf_open_segment('?Picture'//char(0)) 
!	or
!  	character s[256]; s = '?Picture'
! 	call hf_open_segment(trim(s)//char(0)) 
!

implicit none
   INTERFACE

   SUBROUTINE HF_CLOSE_SEGMENT()  bind(C, name= 'HC_Close_Segment')

   END SUBROUTINE HF_CLOSE_SEGMENT

     SUBROUTINE  HF_UPDATE_DISPLAY() bind(C, name= 'HC_Update_Display')

      END SUBROUTINE HF_UPDATE_DISPLAY
      
    subroutine hf_flush_contents(a,b)
    !DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_Flush_Contents' :: hf_flush_contents
                CHARACTER*(*) a,b
    !DEC$ ATTRIBUTES REFERENCE :: a,b
    end subroutine hf_flush_contents

    subroutine hf_set_color(a)
    !DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_Set_Color' :: hf_set_color
         CHARACTER*(*) a
    !DEC$ ATTRIBUTES REFERENCE :: a
    end subroutine hf_set_color
           
#ifdef NEVER
      integer function HF_QSHOW_EXISTENCE(a,b) bind(C, name='HC_QShow_Existence')
        !!!DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_QShow_Existence' :: HF_QSHOW_EXISTENCE
        CHARACTER*(*) a,b
        !DEC$ ATTRIBUTES REFERENCE :: a
        !DEC$ ATTRIBUTES REFERENCE :: b      
    end function HF_QSHOW_EXISTENCE
 
 !  
       subroutine  hf_qset_visibility(a,b)   bind(C, name='HC_QSet_Visibility')
            !!!DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_QSet_Visibility' :: hf_qset_visibility
            CHARACTER*(*) a,b
            !DEC$ ATTRIBUTES REFERENCE :: a,b
    end subroutine hf_qset_visibility   
    subroutine  hf_qset_line_pattern(a,b)   bind(C, name= 'HC_QSet_Line_Pattern')
            !D!!EC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_QSet_Line_Pattern' :: hf_qset_line_pattern
            CHARACTER*(*) a,b
            !DEC$ ATTRIBUTES REFERENCE :: a,b
    end subroutine hf_qset_line_pattern  
      subroutine hf_qunset_line_pattern(a)   bind(C, name= 'HC_QUnSet_Line_Pattern')
            !!!DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_QUnSet_Line_Pattern' :: hf_qunset_line_pattern
            CHARACTER*(*) a
            !DEC$ ATTRIBUTES REFERENCE :: a
    end subroutine hf_qunset_line_pattern 
      subroutine hf_qunset_visibility(a)   bind(C, name= 'HC_QUnSet_Visibility')
            !!!DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_QUnSet_Visibility' :: hf_qunset_visibility
            CHARACTER*(*) a
            !DEC$ ATTRIBUTES REFERENCE :: a
    end subroutine hf_qunset_visibility        
    subroutine hf_set_text_alignment(a)  
            !DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_Set_Text_Alignment' :: hf_set_text_alignment
            CHARACTER*(*) a
            !DEC$ ATTRIBUTES REFERENCE :: a
    end subroutine hf_set_text_alignment 
  
    subroutine hf_set_user_options(a)  
            !DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_Set_User_Options' :: hf_set_user_options
            CHARACTER*(*) a
            !DEC$ ATTRIBUTES REFERENCE :: a
    end subroutine hf_set_user_options 
         subroutine  hf_qset_user_options (a,b)  
            !DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_QSet_User_Options' :: hf_qset_user_options 
            CHARACTER*(*) a,b
            !DEC$ ATTRIBUTES REFERENCE :: a,b
    end subroutine   hf_qset_user_options
#endif  
     subroutine hf_show_one_net_user_option (a,b)  
            !DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_Show_One_Net_User_Option' :: hf_show_one_net_user_option
            CHARACTER*(*) a,b
            !DEC$ ATTRIBUTES REFERENCE :: a,b
    end subroutine hf_show_one_net_user_option    
       subroutine hf_qshow_one_net_user_option (a,b,c)  
            !DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_QShow_One_Net_User_Option' :: hf_qshow_one_net_user_option
            CHARACTER*(*) a,b,c
            !DEC$ ATTRIBUTES REFERENCE :: a,b,c
    end subroutine hf_qshow_one_net_user_option               
      subroutine hf_open_segment(a)  
            !DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_Open_Segment' :: hf_open_segment
            CHARACTER*(*) a
            !DEC$ ATTRIBUTES REFERENCE :: a
    end subroutine hf_open_segment       
    subroutine hf_delete_segment(a)
            !DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_Delete_Segment' :: hf_delete_segment
            CHARACTER*(*) a
            !DEC$ ATTRIBUTES REFERENCE :: a
    end subroutine hf_delete_segment    
    
subroutine hf_set_window(a,b,c,d)
    USE, INTRINSIC :: ISO_C_BINDING
!DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_Set_Window' :: hf_set_window
     real(C_Double),intent(in),value :: a,b,c,d  
    end subroutine hf_set_window 
 !
 ! for set window, with just real 4 and  !DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_Set_Window' :: hf_set_window  it failed
 ! now testing with             !DEC$ ATTRIBUTES VALUE :: a,b,c,d   
 !  ifthat doesnt work try real(kind=8)
 !   
subroutine hf_set_color_by_value(s,g, a,b,c)! bind(C, name= 'HC_Set_Color_By_Value')
        USE, INTRINSIC :: ISO_C_BINDING
!DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_Set_Color_By_Value' :: hf_set_color_by_value
        real(C_Double),intent(in),value :: a,b,c  ! HC_Set_Color_By_Value should prefer value
        CHARACTER*(*) s,g
!DEC$ ATTRIBUTES REFERENCE :: s,g
end subroutine hf_set_color_by_value    
#ifdef NEVER      
    subroutine hf_set_camera_by_volume(s, a,b,c,d) bind(C, name='HC_Set_Camera_By_Volume' )
             USE, INTRINSIC :: ISO_C_BINDING
            !!!DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_Set_Camera_By_Volume' :: hf_set_camera_by_volume
           real(C_FLOAT),intent(in), value :: a,b,c,d  
            CHARACTER*(*) s
            !DEC$ ATTRIBUTES REFERENCE :: s
    end subroutine hf_set_camera_by_volume 
#endif    
    subroutine hf_INSERT_LINE(x1,y1,z1,x2,y2,z2) bind(C, name='HC_Insert_Line' )
       USE, INTRINSIC :: ISO_C_BINDING
       real(C_FLOAT),intent(in), value :: x1,y1,z1,x2,y2,z2  
    end subroutine hf_insert_line     
#ifdef NEVER     
     subroutine hf_insert_text( a,b,c,s) bind(C, name='HC_Insert_Text' )
              USE, INTRINSIC :: ISO_C_BINDING
            !DE!!C$ ATTRIBUTES C, DECORATE,ALIAS:'HC_Insert_Text' :: hf_insert_text
            real(C_FLOAT),intent(in), value :: a,b,c  
            CHARACTER*(*) s
            !DEC$ ATTRIBUTES REFERENCE :: s
    end subroutine hf_insert_text  
#endif    
    subroutine hf_scale_object( a,b,c ) bind(C, name= 'HC_Scale_Object')
        USE, INTRINSIC :: ISO_C_BINDING
            !!!DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_Scale_Object' :: hf_scale_object
             real(C_Double),intent(in),value :: a,b,c  
    end subroutine hf_scale_object     
    subroutine hf_set_line_weight( a ) bind(C, name= 'HC_Set_Line_Weight')
       USE, INTRINSIC :: ISO_C_BINDING
       !D!!EC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_Set_Line_Weight' :: hf_set_line_weight
            real(C_Double),intent(in),value :: a  
    end subroutine hf_set_line_weight    
#ifdef NEVER     
    subroutine hf_qset_line_weight(s, a ) bind(C, name='HC_QSet_Line_Weight'  )
        USE, INTRINSIC :: ISO_C_BINDING
     
        CHARACTER*(*) s
        !DEC$ ATTRIBUTES REFERENCE :: s
        real(C_Double),intent(in),value :: a  
    end subroutine hf_qset_line_weight 
#endif
     subroutine hf_qunset_line_weight(s  )
       !DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_QUnSet_Line_Weight' :: hf_qunset_line_weight
        CHARACTER*(*) s
        !DEC$ ATTRIBUTES REFERENCE :: s
    end subroutine hf_qunset_line_weight   
    subroutine hf_qset_color (s, t)
       !DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_QSet_Color' :: hf_qset_color
        CHARACTER*(*) s,t
        !DEC$ ATTRIBUTES REFERENCE :: s,t
     end subroutine hf_qset_color 
         subroutine hf_qunset_color (s)
       !DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_QUnSet_Color' :: hf_qunset_color
        CHARACTER*(*) s
        !DEC$ ATTRIBUTES REFERENCE :: s
     end subroutine hf_qunset_color 

    subroutine HF_EXIT_PROGRAM (  )
       !DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_Exit_Program' ::  HF_EXIT_PROGRAM
    end subroutine  HF_EXIT_PROGRAM  

! HFP_xx are like HF except the array is transposed wrt fortran. 
 
     subroutine hfp_insert_polyline(k,p)
        !DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_Insert_Polyline' :: hfp_insert_polyline
        integer, intent(in) ::k
        real (kind=4), dimension(k,3),intent(in) ::p 
    end subroutine hfp_insert_polyline  
    subroutine hfp_insert_polygon(k,p)
        !DEC$ ATTRIBUTES C, DECORATE,ALIAS:'HC_Insert_Polygon' :: hfp_insert_polygon
        integer, intent(in) ::k
 
        
        real (kind=4), dimension(k,3),intent(in) ::p 
    end subroutine hfp_insert_polygon 


   END INTERFACE
   
 ! The calls with arrays need the transpose of the array.  
   contains
   
   subroutine hf_insert_polyline(n,pp)
    integer, intent(in) ::n
    real*4 pp(3,*) 
    real (kind=4), dimension(n,3) ::pt
    real (kind=4), dimension(3,n) ::p4
 !  integer i
    pt = transpose(pp(1:3,1:n)) 
    p4 = pp(1:3,1:n)
    ! write(outputunit,*) ' hfp polyline with n = ',n
   !  do i=1,n
   ! write(outputunit,*) p8(1:3,i)
   ! enddo
    call hfp_insert_polyline(n,p4(1,1))   
   end subroutine hf_insert_polyline
  
    subroutine hf_insert_polygon(n,pp)
    integer, intent(in) ::n
    real*4 pp(3,*) 
    real (kind=8), dimension(n,3) ::pt
    real (kind=4), dimension(3,n) ::p4 
 !   integer i
    pt = transpose(pp(1:3,1:n)) 
    p4  = pp(1:3,1:n)
 !   write(outputunit,*) ' hfp polygon with n = ',n
 !   do i=1,n
 !   write(outputunit,*) p8(1:3,i)
 !   enddo
 
    call hfp_insert_polygon(n,p4(1,1))   
   end subroutine hf_insert_polygon 
end module hoopsinterface
