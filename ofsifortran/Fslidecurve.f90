module slidecurve_f

contains
function makeSlideCurveDOFO(nn) result(rflag)
use ftypes_f
! use coo rd in ates_f
use basictypes_f
implicit none
type(fnode) ::nn

integer:: rflag
rflag=0
     if( ( nn%m_slider.eq.0) ) then 
   !     rflag=CONTACT_NONE
        return
     endif
write(outputunit,*) 'OMP CRITICAL(contactcreate) makeSlideCurveDOFO'
!$OMP CRITICAL(contactcreate)
	!!!    	rflag=CONTACT_HAS_DOFO

! for now 		intersectresult = slidecurve_find_intersection(nn%m_slider,nn%xx, newptxyz,intersectPtUVW,k)   
!            Create a DOFO at the first intersection point (newptuvw). 
 
     !!!   theDOFO=> CreateDOFO(sli, C_DOFO_SlideFixedCurve ,error) 
        
    
! for now  	    ok= Complete_SlideFixedCurveDOFO(theDOFO, nn,intersectPtUVW, (1.0-k))  ! ; write(dofounit,*)' dofo is ', loc(theDOFO);
!$OMP END CRITICAL(contactcreate)   	    	
 
end   function makeSlideCurveDOFO



end module slidecurve_f
