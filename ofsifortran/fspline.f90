!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  notes prior to using polymorphism to allow several types of spline
!!

!NOTE*********
!This uses the mathematica formulation.
!it multiplies the gradient by (degree) at the end points
!for evenly spaced knots
!this will tend to make the analysis less stable for sliding points near the ends
!
! also, outside (and at the end points of) the domain the evaluation gives zero,
!  which is correct isnt healthy for the analysis. 
! 
!So for all evaluations and derivatives we 
!  trim the parameter to {tmin+eps, tmax-eps}
! and we do a linear extrapolation outside the ends. 
! 
! A note on the jacobi matrix.
!a strict differential of the spline gives a component due to each
!CV which is Identity Matrix times the influence coefficient.
!and the component due to change in parameter is just the curve's derivative
!
!BUT. this means that the parallel component of the slide node's residual is placed
!on the CV nodes in addition to being placed on the T parameter.
!It is counted twice. 
!The analysis should relax out the T parameter to make it's residual zero,
!but while it isn't in equilibrium it is throwing an artefactual parallel component
!onto the CVs.
!It may be smarter to use a different J and Jt.
!For J we use the strict derivative.  For Jt we try the version with the parallel
!component stripped.

module fspline_f
    use basictypes_f
    use ftypes_f
    implicit none
    integer, parameter ::LOCALCOORDS=1    ! np is length of localCVs order anything
    integer, parameter ::LOCALINTERP=2    !np = (length of localCVs)*2 -2, order is 2
    integer, parameter ::INTERPOLATING =3  ! np = (length of owner's d5%masters)*2 -2, order is 2
    integer, parameter :: STANDARD  =4    ! np = length of owner's d5%masters, order anthing
    real(double),  parameter :: fac = 8.0  ! the false CVs in an interpolation are x(n) +- (x(n+1)-x(n-1))/fac
! Release is 32
! fac=8 puts the false CVs at 25% and 75% of the masternode segments. That seems OK

type ,extends(fsplineI) :: fspline   ! the base type

!   integer:: m_np =0 ! the number of CVs
  ! note for an interpolating spline this is NOT the length of localCVS
!   ! nor the length of the string's pointlist
!   integer:: degree,m_type
!    LOGICAL :: Sliding , Interpolatory
!   type (string),pointer ::owner =>NULL()
 !  real(kind=double), allocatable, dimension(:) :: t  ! the knots
!   real(kind=double), allocatable, dimension(:,:) :: localCVs
!   real(kind=double):: tmin = 0.0d00
 !  real(kind=double):: tmax = 1.0d00

    contains
            procedure ,pass(this)  ::  ListFspline => ListFsplineB
            procedure ,pass(this)  ::  ClearFspline => ClearFsplineB
            procedure ,pass(this)  :: FSplineJMatrix =>FSplineJMatrixB
            procedure ,pass(this)  :: FSplineJMatrixMultiple =>FSplineJMatrixMultipleB
            procedure ,pass(this)  :: FsplineEvaluate =>FsplineEvaluateB
            procedure ,pass(this)  :: derivativebyds =>derivativebydsB
            procedure ,pass(this)  :: FindNearest =>FindNearestB
            procedure ,pass(this)  :: createStringSplineB ! => createStringSplineB
end type fspline

contains

function createLocalFspline(pts,degree,this) result(ok)
        implicit none
        real(kind=double), dimension(:,:) :: pts
        integer degree
        class (fspline) :: this
        integer:: ok; ok=1
        this%m_type=LOCALCOORDS
        this%degree = degree
        allocate(this%localCVs(3,ubound(pts,2))); 
        this%localCVs=pts;
        this%m_np = ubound(pts,2)
        ok=createUniClampedKnotsOurInterp(this,this%m_np )  ! interpSpline
         write(*,'( "Test Tees (CLFS) ")')
        write(*,'(f15.5)') this%kt

        ok=createUniClampedKnots(this, ubound(pts,2))
 end  function createLocalFspline
#ifdef BENCHTEST
 function createLocalInterpSpline(pts,deg_unused,this) result(ok)
        implicit none
        real(kind=double), dimension(:,:) :: pts
        integer deg_unused
        type (fspline) :: this
        integer:: ok; ok=1
        this%m_type=LOCALINTERP
        this%degree=2
        allocate(this%localCVs(3,ubound(pts,2))); 
        this%localCVs=pts;
        this%m_np = ubound(pts,2)*2-2
        ok=createUniClampedKnotsOurInterp(this,this%m_np )  ! interpSpline
         write(*,'( "Test Tees  ")')
        write(*,'(f15.5)') this%kt
        ok=createUniClampedKnots(this,this%m_np )  ! interpSpline
        
        if( .false.==  CheckInterpSplinePts(pts,fac)) then
             write(outputunit,*) ' in the spline some points are too unevenly spaced '
                ok=0
        endif
 end  function createLocalInterpSpline
#endif
 function CheckInterpSplinePts(p,f) result (ok) ! this is a mess. Luckily its not called anywhere
         implicit none
        real(kind=double), dimension(:,:),intent(in) :: p
         real(kind=double), intent(in) :: f  
          real(kind=double) ::  LM1, L, LM2
     
         integer i,n,npts
         logical ok, nptsIsOdd; ok=.true.
  ! first interval
 
        LM1 = sqrt(sum( (p(2,:) - p(1,:))**2))
        L = sqrt(sum( (p(3,:) - p(2,:))**2))
        if(LM1 < (LM1 +L)/f) ok=.false.
        
        npts = ubound(p,1); 
        if(mod(npts,2) .eq. 0) then ! even number of points
            n = npts-1; nptsIsOdd=.false.
         else
            n=npts-1; nptsIsOdd = .true.
        endif
         do i = 3, n-3 
           LM1 = sqrt(sum( (p(i-1,:) - p(i,:))**2))
           LM2 = sqrt(sum( (p(i-2,:) - p(i-1,:))**2)) 
           L   = sqrt(sum( (p(i,:) - p(i+1,:))**2)) 
           if ( (LM1 + L) *f < (LM2 + LM1 ) )  ok=.false.  
         enddo
! last
        if( nptsIsOdd) return
        i =  npts -2
        LM1 = sqrt(sum( (p(i-1,:) - p(i,:))**2))
        LM2 = sqrt(sum( (p(i-2,:) - p(i-1,:))**2)) 
        L   = sqrt(sum( (p(i,:) - p(i+1,:))**2)) 

        if(LM1 + L < (LM2 + LM1)/f   )  ok = .false. 
  
 end  function CheckInterpSplinePts
 
 function createStringSplineB(this, t,degree, what)result(ok)
    implicit none
     class (fspline) :: this
     type (string) ,pointer ::t
     integer degree,what
     integer::ok,npts;
      write(*,*) ' FSplineB needs the Tee-Origin shift - DONT USE'
        ok=1
        this%m_ownerString=>t
        this%m_type=what
        this%degree = degree
         select case (what)
          case(INTERPOLATING )  ! np = (length of owner's d5%masters)*2 -2, degree is 2
            npts = ubound(t%m_pts,1) *2-2
            this%m_np=npts
             this%degree=min(this%degree,2); if(this%degree<2)write(outputunit,*)' MUST STEP LINEAR SPLINE!!!!!'
         case(STANDARD )
            npts = ubound(t%m_pts,1);
            this%m_np=npts
          case default
          ok=ok/0
         end select

       ok=createUniClampedKnotsOurInterp(this, npts)

  !    ok=createUniClampedKnots(this, npts)
  !    write(*,*) ' standard tees '
  !    write(*, '(f14.7 )') this%t

end  function createStringSplineB

function createUniClampedKnotsOurInterp(this,np) result(ok)! mathematica style
! uneven knot spacing:  a bigger interval at the end because the first CV is at 3/2 x the spacing
    type (fspline) :: this
     real(double) :: x  ,dx
    real(double) :: RXEPSILON_L
     integer::i, nk, np, ok,istat; ok=1

     nk = np + this%degree+1
     deallocate(this%kt,stat=istat)
     allocate(this%kt(nk))
      RXEPSILON_L = RXEPSILON
     this%kt(nk-3:) = 1.0d0+RXEPSILON_L;
     this%kt(1:this%degree+1)=0.

     dx = real((nk-2*this%degree-1 +4),double) + RXEPSILON_L
     do i=this%degree+1+1,nk-this%degree
        x = real(i-(this%degree+1)+2,double);
        x=x/dx ! uniform, clamped
        this%kt(i) = x
     enddo
      this%kt(np+1)= 1.0d00 - RXEPSILON_L
end  function createUniClampedKnotsOurInterp

function createUniClampedKnots(this,np) result(ok)! mathematica style
     type (fspline) :: this
      real(double) :: x   
     integer::i, nk, np, ok,istat; ok=1
      nk = np + this%degree+1 
     deallocate(this%kt,stat=istat)
     allocate(this%kt(nk))

     this%kt = 1.0d0+RXEPSILON;
     this%kt(1:this%degree+1)=0.
     do i=this%degree+1,nk-this%degree
        x = real(i-(this%degree+1),double);
         x=x/(real((nk-2*this%degree-1),double) + RXEPSILON) ! uniform, clamped
        this%kt(i) = x
     enddo
end  function createUniClampedKnots 
 
function createUniClampedKnotsExp(this,np) result(ok)! peter 2011
     type (fspline) :: this
      real(double) :: x,dk   
     integer::i, nk, np, ok,d; ok=1
     d = this%degree
     nk = np +  d*2 -2
     allocate(this%kt(nk))
     
     dk = (this%tmax-this%tmin) /( real(np-1,double)); 
     x =this%tmin 
     this%kt(1:d-1)=x

     do i=1,np  ! x goes 0 to 1
         this%kt(i + d-1) = x
        x=x+dk 
     enddo
     this%kt(np+d-1:) = this%tmax
    
 end  function createUniClampedKnotsExp
 
! function createUniClampedKnots_r(this,np) result(ok)  ! rhino style
! ! with this evaluation this give zero at the end
!     type (fspline) :: this
!      real(double) :: x   
!     integer::i, nk, np, ok; ok=1
!      nk = np + this%degree+1 
!     allocate(this%t(nk))
!     this%t = 1.0+RXEPSILON;
!     this%t(1:this%degree)=0.
!     do i=this%degree,nk-this%degree
!        x = real(i-(this%degree),double);
!         x=x/(real((nk-2*this%degree+1),double) + RXEPSILON) ! uniform, clamped
!        this%t(i) = x
!     enddo
!  !  this%t= this%t*12.0
! end  function createUniClampedKnots_r
 
RX_PURE recursive function FsplineEvaluateB(this,s) result(x) ! linear interpolation outside the domain
    implicit none
    class (fspline), intent(in) ::this
    real(kind=double), intent(in) :: s

    real(kind=double),dimension(3)  :: x
    real(kind=double)   :: tmin,tmax,fac
    integer ::i
    x=0.0
    tmin = this%kt(1)+RXEPSILON;
    if(s .lt. tmin) then
        x = FsplineEvaluateB(this,tmin ) + (s-tmin)*this%derivativebyds(tmin)
        return
    endif
     tmax = this%kt(ubound(this%kt,1))-RXEPSILON*this%degree
    if(s .gt. tmax) then
        x = FsplineEvaluateB(this, tmax) + (s-tmax)*this%derivativebyds(tmax)
        return
    endif
    do i=1,this%m_np 
        fac =  fspline_n(this,i,this%degree,s)
        if(abs(fac).gt. 1.0d-15) then
        x=x+  fac* fspline_GetP(this,i)
        endif
    enddo
end function FsplineEvaluateB

RX_PURE function derivativebydsB(this,sin) result(x)
    implicit none
    class (fspline), intent(in) ::this
    real(kind=double), intent(in) :: sin
    real(kind=double)  :: s
    real(kind=double),dimension(3)  :: x
    integer ::i
    

    if(sin .lt. this%kt(1)) then
         s=this%kt(1)+RXEPSILON
    elseif(sin .ge. this%kt(ubound(this%kt,1))) then  ! june 2011 was gt.
         s=this%kt(ubound(this%kt,1))-RXEPSILON*this%degree
    else
       s=sin      
    endif

    x=0.0
    do i=1,this%m_np-1
        x=x+ fspline_n(this,i+1,this%degree-1,s) &
        * (fspline_GetP(this,i+1) - fspline_GetP(this,i)  ) &
        *  REAL (this%degree, double)/(this%kt(i+this%degree+1) -this%kt(i+1)  )
    enddo
end function derivativebydsB

 
recursive RX_PURE function fspline_n(this,i,j,s) result (x)
    use basictypes_f
    implicit none
    type (fspline), intent(in) ::this
    integer, intent(in)::i,j
    real(kind=double), intent(in) :: s

    real(kind=double)  :: x

    if(j .eq. 0) then  !
        if(this%kt(i).le. s .and. s .lt. this%kt(i+1)) then
            x=1.0;
        else
            x=0.
        endif
        return
     endif
! for  breakpoint
    if(j .eq. this%degree .and.  i .eq. this%m_np) then
    x=0; ! just for a breakpt
    endif

#ifdef NEVER
    if(i+j>this%degree .and. i<this%m_np) then
        x = (s-this%kt(i))/( this%kt(i+j )-this%kt( i)  ) * fspline_n(this,i,j-1,s)
    else
        x=0.0d00        
    endif
     if( i+j+1>this%degree .and.i+1 < this%m_np   ) then    
        x=x + (this%kt(i+j+1) - s ) / (this%kt(i+j+1)-this%kt(i+1) ) * fspline_N(this,i+1,j-1,s)
    endif
    return
#endif
! pre-2011 code below   June 2013 tried GE
    if( this%kt(i+j )>this%kt( i)  ) then
        x = (s-this%kt(i))/( this%kt(i+j )-this%kt( i)  ) * fspline_n(this,i,j-1,s)
    else
        x=0.0d00
    endif
     if(this%kt(i+j+1)>this%kt(i+1)  ) then
        x=x + (this%kt(i+j+1) - s ) / (this%kt(i+j+1)-this%kt(i+1) ) * fspline_N(this,i+1,j-1,s)
    endif



end function fspline_n

RX_PURE function  fspline_GetP(this,i) result(x)
#ifndef BENCHTEST
    use fnoderef_f
    use coordinates_f
    use saillist_f

!    type(fnode) ,pointer ::thenode 
#else
     implicit none
#endif

    type(fspline),intent(in) ::this
    integer, intent(in) :: i
    real(kind=double),dimension(3)  :: x,x1
    integer i1,i2,i3

 select case( this%m_type)
    case(LOCALCOORDS)
        x=this%localCVs(1:3,i)
    case (LOCALINTERP)
       if(i ==1 ) then
            x=this%localCVs(1:3,i)  
        else if(i==this%m_np ) then
            i2 = i/2 +1
           x=this%localCVs(1:3,i2)      
         else
            i2 = floor(sngl(i/2)) +1
            i1 = i2 + 2*mod(i,2)-1
            i3 = i2 - 2*mod(i,2)+1
            x=this%localCVs(1:3,i2)
            x = x+ this%localCVs(1:3,i1) /fac
            x = x - this%localCVs(1:3,i3) /fac                    
        endif
        
#ifndef BENCHTEST
    case (INTERPOLATING)
        if(i ==1 ) then
            x = get_Cartesian_X_By_Ptr( saillist(this%m_ownerString%m_pts(i)%m_fnr_Sli  )%nodes%xlist(this%m_ownerString%m_pts(i)%m_fnrn) )
        else if(i==this%m_np ) then
            i2 = i/2 +1
            x = get_Cartesian_X_By_Ptr(saillist(this%m_ownerString%m_pts(i2)%m_fnr_Sli  )%nodes%xlist(this%m_ownerString%m_pts(i2)%m_fnrn))
         else
            x=0
            i2 = floor(sngl(i/2)) +1
            i1 = i2 + 2*mod(i,2)-1
            i3 = i2 - 2*mod(i,2)+1
                      
            x = get_Cartesian_X_By_Ptr(saillist(this%m_ownerString%m_pts(i2)%m_fnr_Sli  )%nodes%xlist(this%m_ownerString%m_pts(i2)%m_fnrn))

            x1 = get_Cartesian_X_By_Ptr(saillist(this%m_ownerString%m_pts(i1)%m_fnr_Sli  )%nodes%xlist(this%m_ownerString%m_pts(i1)%m_fnrn))  /fac
            x = x+ x1

            x1=get_Cartesian_X_By_Ptr(saillist(this%m_ownerString%m_pts(i3)%m_fnr_Sli  )%nodes%xlist(this%m_ownerString%m_pts(i3)%m_fnrn) )  /fac
            x = x - x1
        endif
    
    case (STANDARD)
!           ! nref => this%owner%m_pts(i)
!            thenode => decodenoderef(this%owner%m_pts(i))
            x = get_Cartesian_X_By_Ptr(saillist(this%m_ownerString%m_pts(i)%m_fnr_Sli  )%nodes%xlist(this%m_ownerString%m_pts(i)%m_fnrn))
#endif  
    case default
    x=0/0
    end select
     
 
 end function  fspline_GetP


subroutine ClearFsplineB(this)
    class(fspline) ::this ! Intel 2015 compained - no intent(in)
    integer::alloc_err
    this%m_np=-1
    this%degree=-1; this%m_type=-1
        if(associated(this%m_ownerString)) this%m_ownerString%m_fspline=>NULL();
    this%m_ownerString =>NULL()
    deallocate(this%kt  ,stat=alloc_err)
    deallocate(this%localCVs  ,stat=alloc_err)
end subroutine ClearFsplineB


Function CheckFSpline(this,u) result (ok)
use saillist_f
use coordinates_f
    implicit none

    integer,intent(in) ::u 
    type (fspline) :: this
         integer:: i,ok 
    real(kind=double),dimension(3)  :: x,x1
    integer i1,i2,i3   ,npts      
         
    real(kind=double), dimension(2*this%m_np ,3) :: pts
    ok=1
    write( u,*)' CVs'
 
 
 select case( this%m_type)
    case(LOCALCOORDS)
      npts =  this%m_np 
        do i = 1,npts     ;      pts(i,:)=this%localCVs(1:3,i) ; enddo
    case (LOCALINTERP)
       i =1 
            pts(i,:)=this%localCVs(1:3,i)  
       i=this%m_np *2 
            i2 = i/2 +1
            pts(i,:)=this%localCVs(1:3,i2)      
 
         npts =  this%m_np *2
        do i = 1,npts            
            pts(i,:)=this%localCVs(1:3,i)
        enddo
 
        
#ifndef BENCHTEST
    case (INTERPOLATING)
         i =1  
         pts(i,:) =get_Cartesian_X_By_Ptr( saillist(this%m_ownerString%m_pts(i)%m_fnr_Sli  )%nodes%xlist(this%m_ownerString%m_pts(i)%m_fnrn) )
        i=this%m_np  
            i2 = i/2 +1
         !   nref => this%owner%m_pts(i2)
          !  thenode => decodenoderef(this%owner%m_pts(i2))
          !  x = get_Cartesian_X_By_Ptr(theNode)              
          !  thenode => saillist(this%owner%m_pts(i2)%m_fnrSli  )%nodes%xlist(this%owner%m_pts(i2)%n)             
            x = get_Cartesian_X_By_Ptr(saillist(this%m_ownerString%m_pts(i2)%m_fnr_Sli  )%nodes%xlist(this%m_ownerString%m_pts(i2)%m_fnrn))
         !  Do LOOP
            x=0
            i2 = floor(sngl(i/2)) +1
            i1 = i2 + 2*mod(i,2)-1
            i3 = i2 - 2*mod(i,2)+1
                    
            x = get_Cartesian_X_By_Ptr(saillist(this%m_ownerString%m_pts(i2)%m_fnr_Sli  )%nodes%xlist(this%m_ownerString%m_pts(i2)%m_fnrn))

            x1 = get_Cartesian_X_By_Ptr(saillist(this%m_ownerString%m_pts(i1)%m_fnr_Sli  )%nodes%xlist(this%m_ownerString%m_pts(i1)%m_fnrn))  /fac
            x = x+ x1
                
            x1=get_Cartesian_X_By_Ptr(saillist(this%m_ownerString%m_pts(i3)%m_fnr_Sli  )%nodes%xlist(this%m_ownerString%m_pts(i3)%m_fnrn) )  /fac
            x = x - x1
       ! enddo
    
    case (STANDARD)
!           ! nref => this%owner%m_pts(i)
!            thenode => decodenoderef(this%m_ownerString%m_pts(i))
            x = get_Cartesian_X_By_Ptr(saillist(this%m_ownerString%m_pts(i)%m_fnr_Sli  )%nodes%xlist(this%m_ownerString%m_pts(i)%m_fnrn))
#endif  
    case default
    x=0/0
    end select
end Function CheckFSpline
 
function ListFsplineB(this,u,fullprint) result(ok)
    implicit none

    integer,intent(in) ::u 
    class (fspline)  :: this
    logical, intent(in),optional :: fullprint
    real(kind=double),dimension(3)  :: x  ,d,dlast
    real(kind=double) :: tmin,tmax ,lastt   ,s,y ,ds !,zns
    integer:: i,ok,istat
    ok=1
    write(u,*) ' fspline '
    write(u,*) '  np,degree,m_type   ',this%m_np,this%degree,this%m_type
    write(u,*) ' Sliding =',this%sliding   ,'Interpolatory=',this%Interpolatory
    if(associated(this%m_ownerString))then; write(u,*) ' Has OwnerString   '  ; else; write(u,*) ' no OwnerString'; endif

    tmin = minval(this%kt)+RXEPSILON; tmax=maxval(this%kt)-RXEPSILON;
    write(u,*) ' knots from  ' ,tmin,' to ', tmax 
    lastt = this%kt(1)
    do i=1,ubound(this%kt,1)
       write( u,'(i3, 2g15.5,L8)')i, this%kt(i)   ,  this%kt(i)-lastt , (this%kt(i).ge. lastt )
       lastt =     this%kt(i)
    enddo
    write( u,*)' CVs'
    do i = 1,this%m_np
        x =  fspline_GetP(this,i)
        write(u,'(i5,3f14.5 )',iostat=istat) i,x
    enddo
  ! as a test sample at ends
  !     ns=50; zns=real(ns)
       write(u,*) ' sampling'
       write(u,*) '   I     s       x1     x2     x3    deriv1    ...2   ....3 '
       do i = -5,5
           s = tmin + 0.1*real(i); y=s
           x = FsplineEvaluateB(this,s)
           d = this%derivativebyds(s)
           !          i      y
           write(u,'(i4,1x,g16.9,6(1x,g16.9))',iostat=istat) i,y,x,d   ! '(i4,1x,g16.9,2(1x,3g16.9))'
    enddo
       do i=-10, 10
            s =  tmin   +  RXEPSILON * real(i) /2.0d00; y=s
            x = FsplineEvaluateB(this,s)
            d = derivativebydsB(this,s)
            !          i      y  
            write(u,'(i4,1x,g16.9,6(1x,g16.9))',iostat=istat) i,y,x,d   ! '(i4,1x,g16.9,2(1x,3g16.9))'
       enddo  
       dlast=d    
! at each knot
       do i=1,ubound(this%kt,1)
            s =  this%kt(i);  y=s
            x = FsplineEvaluateB(this,s)
            d = derivativebydsB(this,s)
            if (dot_product(d,dlast)<=0) then
             write(u,*)'WARNING CVS ZIGZAG. USE A FINER GRID '
             endif
             dlast=d
            !          i      y  
            write(u,'(i4,1x,g16.9,6(1x,g16.9))',iostat=istat) i,y,x,d   ! '(i4,1x,g16.9,2(1x,3g16.9))'
       enddo   

! at the end
       do i=-10, +10
             s =  tmax  +  RXEPSILON * real(i) /2.0d00; y=s
            x = FsplineEvaluateB(this,s)
            d = derivativebydsB(this,s)
             write(u,'(i4,1x,g16.9," + ",g16.9,6(1x,g16.9))',iostat=istat) i,tmax,y-tmax,x,d 
       enddo     
 ! around the end
     do i = -5,5
         s = tmax + 0.1*real(i); y=s
         x = FsplineEvaluateB(this,s)
         d = derivativebydsB(this,s)
         write(u,'(i4,1x,g16.9,6(1x,g16.9))',iostat=istat) i,y,x,d
    enddo

! big dump
    write(u,*)'big dump'
    ds = (tmax-tmin)/50.
        s = (tmax + tmin)/2. - 50.* ds
        do i = 1,101
            y=s
            x = FsplineEvaluateB(this,s)
            d = derivativebydsB(this,s)
            write(u,'(i6,1x,g16.9,7(1x,g16.9))',iostat=istat) i,y,x,d ,s
            s = s + ds
       enddo
  
    if(.not. present(fullprint)) return
     if(.not. fullprint) return   
#ifdef NEVER ! this was just for debugging
       ns=50; zns=real(ns)
       write(u,*) ' sampling'
       write(u,*) '   I     s       x1     x2     x3    d1     d2     d3 '  
       do i=-5, 5
            s =  tmin   +  RXEPSILON * real(i) /2.0d00; y=s
            x = FsplineEvaluateB(this,s)
            d = derivativebydsB(this,s)
            write(u,'(i4,1x,g16.12,2(1x,3g16.12))',iostat=istat) i,y,x,d
       enddo      
        
       do i=-5,ns+5
            s =( tmin *real(ns-i) +  tmax * real(i))/zns; y=s
            x = FsplineEvaluateB(this,s)
            d = derivativebydsB(this,s)
            write(u,'(i4,1x,f6.3,2(1x,3f11.5))',iostat=istat) i,y,x,d
       enddo
       do i=-5, +5
             s =  tmax  +  RXEPSILON * real(i) /2.0d00; y=s
            x = FsplineEvaluateB(this,s)
            d = derivativebydsB(this,s)
            write(u,'(i4,1x,g16.12,2(1x,3g16.12))',iostat=istat) i,y,x,d
       enddo      
     
       write(u,*) '   I     s       b0  b1    ... b(np)   '
       ns=50; zns=real(ns)
       do k=-5,ns+5
           s =( tmin *real(ns-k) +  tmax * real(k))/zns
           
           write(u,'(i4,1x,f6.3,1x\ )',iostat=istat)k,s 
             if(s .lt. this%t(1)) then
                s=this%t(1)+RXEPSILON
            elseif(s .gt. this%t(ubound(t%t,1))) then
                s=this%t(ubound(this%t,1))-RXEPSILON
            endif          
           
           do i = 1,this%m_np
           y =    fspline_n(this,i,t%degree,s)
           write(u,'(f8.4,\)',iostat=istat) y
           enddo
           write(u,*)
       enddo
#endif
 end  function ListFsplineB
 
function FSplineJMatrixB(this,d,origin,ss) result(j)
      implicit none
! parameters     
       class (fspline),intent(in) :: this
#ifdef NO_DOFOCLASS
           type (dofo), intent(inout) ::d  ! or class
#else
           class (dofo),intent(inout)::d ! or type
#endif
      real(kind=double) ,intent(in) ::ss
      real(kind=double) ,intent(inout),dimension(:) ::origin
!result
      real(kind=double), pointer, dimension(:,:):: J
      if(this%Sliding) then
      J =>FSplineJMatrixSlide(this,origin, ss)
      else
      J =>FSplineJMatrixNoSlide(this,origin, ss)
      endif     
end  function FSplineJMatrixB


function FSplineJMatrixSlide(t,origin,sin) result(j)
! For the J matrix, the size is (3* no spline pts + 3) by (3* no spline pts)) 
! the first (npts) 3x3s relate the spline points.  The last row relates the slave point
! there is one last column  relating to its T-parameter

! for the argument Origin.. This is presumed to be the 'origin' member
! of type DOFO.
! we assume that on input the first (3*npts) rows of the origin are
! the last-known-good (aka (wrongly) undeflected) positions of the
! master points.
! These dont get changed.  
! the last 3 rows in 'origin'  become
!
!  Sum over points of (origin-triplet times j(i))
! Plus  (minus deriv times sin)

    use vectors_f
    implicit none
! parameters     
       type (fspline),intent(in) :: t
      real(kind=double) ,intent(in) ::sin
      real(kind=double) ,intent(inout),dimension(:) ::origin
!result
      real(kind=double), pointer, dimension(:,:):: J
!locals
    real(kind=double), allocatable, dimension(:,:):: pq
    real(kind=double), allocatable, dimension(:):: Jq,jp
    real(kind=double), dimension( 3) :: deriv,splinept ,slavesOrigin  ! non-unitized

    integer:: i,npts,i1,i2,i3,r
    real(kind=double) :: k,s !,lsq

    s=sin; !write(outputunit,*) ' J matrix on S=',s
    deriv =  derivativebydsB(t,s)
    splinept = FsplineEvaluateB(t,s)  ! only dbg ! s is d%t(d%ndof)
    SlavesOrigin=0
      
    if(s .lt. t%kt(1)) then
       s=t%kt(1)+RXEPSILON
    elseif(s .gt. t%kt(ubound(t%kt,1)) - 4.* RXEPSILON) then  ! JUNE try this
      s=t%kt(ubound(t%kt,1))-4.*RXEPSILON
    endif 
    select case( t%m_type)
    case(LOCALCOORDS, LOCALINTERP)    
          npts = ubound(t%localCVs,2); k=0; k=1/0
    case (INTERPOLATING, STANDARD)
        npts = ubound(t%m_ownerString%m_pts,1) ! will be a lot less than t%np
    case default
        npts=0/0
    end select

    allocate(j(3*npts+3,3*npts+1)); j=0.

! fill in the master node portions.
    do i = 1,npts
        r = 3*i-2
        j(r:r+2,r:r+2) = Identity(3)
    enddo   


! now fill in the rest of J    
      select case( t%m_type)
        case(LOCALCOORDS,STANDARD)  
              if(npts .ne. t%m_np) write(outputunit,*) 'whoops fspline B'
              r = 3*npts+1 ; 
               do i = 1,t%m_np
                   k  = fspline_n(t,i,t%degree,s);  
                   J(r:r+2,3*i-2:3*i) = Identity(3)*k
               enddo           
            
        case (INTERPOLATING,LOCALINTERP )
              allocate(jp(npts))
              allocate(jq(t%m_np))  
              allocate(pq(npts,t%m_np)); pq=0 ! its very banded so this is slow, but...
               do i = 1,t%m_np
                   jq(i) = fspline_n(t,i,t%degree,s)
               enddo    
                pq(1,1)=1.0;  pq(npts  ,t%m_np)=1.0d0; ! 
                do i = 2,t%m_np-1
                        i2 = floor(sngl(i/2)) +1
                        i1 = i2 + 2*mod(i,2)-1
                        i3 = i2 - 2*mod(i,2)+1
                        pq(i2  ,i)=1.0d0; ! x=this%localCVs(1:3,i2)
                        pq(i1  ,i)= 1.0d00/fac !  !  x = x+ this%localCVs(1:3,i1) /fac
                        pq(i3,i) = -1.0d00/fac !  !x = x - this%localCVs(1:3,i3) /fac                    
                enddo  
                jp = matmul(pq,jq); 
                r = 3*npts+1
                do i = 1,npts
                   k = jp(i)
                   J(r:r+2,3*i-2:3*i) = k* (Identity(3))!  -( ncn)) 
                   slavesOrigin = slavesOrigin +k*  origin(3*i-2:3*i)
               enddo  
                j(3*npts +1:3*npts+3,ubound(j,2)) = deriv  
                  slavesOrigin  =  slavesOrigin - deriv * sin  
                  Origin(r:r+2)  =  slavesOrigin        
                 deallocate(jp,jq,pq)
        case default
            npts=0/0
    end select   
 end  function FSplineJMatrixSlide  
 
 function FSplineJMatrixNoSlide(t,origin, sin) result(j)
 ! For the J matrix, the size is (3* no spline pts + 3) by (3* no spline pts)) 
! the first (npts) 3x3s relate the spline points.  The last row relates the slave point
! if it is sliding, there is one last column  relating to its T-parameter
    use vectors_f
    implicit none
! parameters     
       type (fspline),intent(in) :: t
      real(kind=double) ,intent(in) ::sin
      real(kind=double) ,intent(inout),dimension(:) ::origin
!result
      real(kind=double), pointer, dimension(:,:):: J
!locals
      real(kind=double), allocatable, dimension(:,:):: pq  
      real(kind=double), allocatable, dimension(:):: Jq,jp     
    real(kind=double), dimension( 3) :: slaveOrigin  
     integer:: i,Jcols,npts,i1,i2,i3,r 
      real(kind=double) :: k,s !  ,lsq  
     slaveOrigin=0
     s=sin
      if(s .lt. t%kt(1)) then
           s=t%kt(1)+RXEPSILON
      elseif(s .ge. t%kt(ubound(t%kt,1))-RXEPSILON  ) then ! peter june14 2013 was gt without the -
          s=t%kt(ubound(t%kt,1))-RXEPSILON
      endif 
      select case( t%m_type)
        case(LOCALCOORDS, LOCALINTERP)    
              npts = ubound(t%localCVs,2);
        case (INTERPOLATING, STANDARD)
            npts = ubound(t%m_ownerString%m_pts,1) ! will be a lot less than t%np
        case default
            npts=0/0
       end select
      Jcols = 3*npts
      allocate(j(jcols+3,jcols)); j=0.
 
 ! fill in the master node portions.
    do i = 1,npts
        r = 3*i-2
        j(r:r+2,r:r+2) = Identity(3)
    enddo   


! now fill in the rest of J    
      select case( t%m_type)
        case(LOCALCOORDS,STANDARD)  
              if(npts .ne. t%m_np) write(outputunit,*) 'whoops'
              r = 3*npts+1
               do i = 1,t%m_np
                   k  = fspline_n(t,i,t%degree,s);  
                   J(r:r+2,3*i-2:3*i) = Identity(3)*k
               enddo           
            
        case (INTERPOLATING,LOCALINTERP )
              allocate(jp(npts))
              allocate(jq(t%m_np))  
              allocate(pq(npts,t%m_np)); pq=0 ! its very banded so this is slow, but...
               do i = 1,t%m_np
                   jq(i) = fspline_n(t,i,t%degree,s)
               enddo    
                pq(1,1)=1.0;  pq(npts  ,t%m_np)=1.0d0; ! 
                do i = 2,t%m_np-1
                        i2 = floor(sngl(i/2)) +1
                        i1 = i2 + 2*mod(i,2)-1
                        i3 = i2 - 2*mod(i,2)+1
                        pq(i2  ,i)=1.0d0;  
                        pq(i1  ,i)=1.0d00/fac 
                        pq(i3,i) = -1.0d00/fac                     
                enddo  
                jp = matmul(pq,jq); 
                r = 3*npts+1
                do i = 1,npts
                   k = jp(i)
                   J(r:r+2,3*i-2:3*i) = Identity(3) * k
                   slaveOrigin = slaveOrigin +k*  origin(3*i-2:3*i)
               enddo  
               Origin(r:r+2)  =  slaveOrigin                
               deallocate(jp,jq,pq)
        case default
            npts=0/0
    end select   
 end  function FSplineJMatrixNoSlide  
 

function  FSplineJMatrixMultipleB(this,d,origin,ss) result(j)
      implicit none
! parameters     
       class (fspline),intent(in) :: this
#ifdef NO_DOFOCLASS
           type (dofo),intent(inout)  ::d  ! or class
#else
           class (dofo),intent(inout) ::d ! or type
#endif

      real(kind=double),dimension(:) ,intent(in) ::ss
      real(kind=double) ,intent(inout),dimension(:) ::origin
!result
      real(kind=double), pointer, dimension(:,:):: J
      
      if(this%Sliding) then
       J =>  FSplineJMatrixMultSlide(this,d,origin, ss)
      else
      J =>FSplineJMatrixMultNoSlide(this,d,origin, ss)
      endif
      
end  function FSplineJMatrixMultipleB

 
function FSplineJMatrixMultSlide(t,d,origin,ss) result(j)
! For the J matrix, the size is (3* no spline pts + 3) by (3* no spline pts)) 
! the first (npts) 3x3s relate the spline points.  The last row relates the slave point
! there is one last column  relating to its T-parameter

! for the argument Origin.. This is presumed to be the 'origin' member
! of type DOFO.
! we assume that on input the first (3*npts) rows of the origin are
! the last-known-good (aka (wrongly) undeflected) positions of the
! master points.
! These dont get changed.  
! the last 3 rows in 'origin'  become
!
!  Sum over points of (origin-triplet times j(i))
! PLUs  (minus deriv times sin)


    use vectors_f
    implicit none
! parameters     
       class (fspline),intent(in) :: t
#ifdef NO_DOFOCLASS
      type (dofo),intent(in) ::d   ! or class
#else
      class (dofo),intent(in)::d  ! or type
#endif
      real(kind=double),dimension(:) ,intent(in) ::ss
      real(kind=double) ,intent(inout),dimension(:) ::origin
!result
      real(kind=double), pointer, dimension(:,:):: J
!locals
    real(kind=double), allocatable, dimension(:,:):: pq  
    real(kind=double), allocatable, dimension(:):: Jq,jp  
    real(kind=double), allocatable,dimension(:,:) :: deriv,splinept ,slavesOrigin  ! non-unitized

    integer:: i,npts,i1,i2,i3,r ,n,nslaves
    real(kind=double) :: k 
    nslaves =ubound(ss,1)
    allocate(deriv(nslaves,3));  allocate(splinept(nslaves,3));  allocate(slavesOrigin(nslaves,3))  
    SlavesOrigin=0      
    do n=1,nslaves
        deriv(n,:) =  derivativebydsB(t,ss(n))
        splinept(n,:) = FsplineEvaluateB(t,ss(n))  ! only dbg ! s is d%t(d%ndof)
        if(ss(n) .lt. t%kt(1)) then
            write(*,*) 'ss(n)=t%kt(1)+RXEPSILON'
        elseif(ss(n).gt. t%kt(ubound(t%kt,1))) then
             write(*,*) 'ss(n)=t%kt(ubound(t%kt,1))-RXEPSILON'
         elseif(ss(n).gt. t%kt(ubound(t%kt,1))  -RXEPSILON) then
             write(*,*) 'ss(n)=t%kt(ubound(t%kt,1))-RXEPSILON'
        endif 
    enddo
    select case( t%m_type)
    case(LOCALCOORDS, LOCALINTERP)    
          npts = ubound(t%localCVs,2); k=0; k=1/0
    case (INTERPOLATING, STANDARD)
        npts = ubound(t%m_ownerString%m_pts,1) ! will be a lot less than t%np
    case default
        npts=0/0
    end select

    allocate(j(3*npts+3*nslaves,3*npts+nslaves)); j=0.

! fill in the master node portions.
    do i = 1,npts
        r = 3*i-2
        j(r:r+2,r:r+2) = Identity(3)
    enddo   
   r = 3*npts +1 

! now fill in the rest of J  
slaves: do n=1,nslaves  
          select case( t%m_type)
            case(LOCALCOORDS,STANDARD)  
                  if(npts .ne. t%m_np) write(outputunit,*) 'whoops'
                 !  i=0; i=1/i
                   do i = 1,t%m_np
                       k  = fspline_n(t,i,t%degree,ss(n));  
                       J(r:r+2,3*i-2:3*i) = Identity(3)*k
                   enddo           
                
            case (INTERPOLATING,LOCALINTERP )
                  allocate(jp(npts))
                  allocate(jq(t%m_np))  
                  allocate(pq(npts,t%m_np)); pq=0 ! its very banded so this is slow, but...
                   do i = 1,t%m_np
                       jq(i) = fspline_n(t,i,t%degree,ss(n))
                   enddo    
                    pq(1,1)=1.0;  pq(npts  ,t%m_np)=1.0d0; ! 
                    do i = 2,t%m_np-1
                            i2 = floor(sngl(i/2)) +1
                            i1 = i2 + 2*mod(i,2)-1
                            i3 = i2 - 2*mod(i,2)+1
                            pq(i2  ,i)=1.0d0; ! x=this%localCVs(1:3,i2)
                            pq(i1  ,i)=1.0d00/fac ! !  x = x+ this%localCVs(1:3,i1) /fac
                            pq(i3,i) = -1.0d00/fac !!x = x - this%localCVs(1:3,i3) /fac                    
                    enddo  
                    jp = matmul(pq,jq); 
                    do i = 1,npts
                       k = jp(i)
                       J(r:r+2,3*i-2:3*i) = k* (Identity(3))!  -( ncn)) 
                       slavesOrigin(n,:) = slavesOrigin(n,:) +k*  origin(3*i-2:3*i)
                   enddo  
                     j(r:r+2,3*npts +n  ) = deriv(n,:) 
                    slavesOrigin(n,:)  =  slavesOrigin(n,:) - deriv(n,:) * ss(n)  
                    Origin(r:r+2)  =  slavesOrigin(n,:)        
                    deallocate(jp,jq,pq)
            case default
                npts=0/0
        end select  
        r=r+3
    enddo slaves
    
    deallocate(deriv,splinept,slavesOrigin)
 end  function FSplineJMatrixMultSlide  
 
 function FSplineJMatrixMultNoSlide(t,d,origin, ss) result(j)
 ! For the J matrix, the size is (3* no spline pts + 3) by (3* no spline pts)) 
! the first (npts) 3x3s relate the spline points.  The last row relates the slave point
! if it is sliding, there is one last column  relating to its T-parameter
    use vectors_f
    implicit none
! parameters     
      class (fspline),intent(in) :: t
#ifdef NO_DOFOCLASS
           type (dofo),intent(in) ::d  ! or class
#else
           class (dofo),intent(in) ::d ! or type
#endif
      real(kind=double),dimension(:) ,intent(in) ::ss
      real(kind=double) ,intent(inout),dimension(:) ::origin
!result
      real(kind=double), pointer, dimension(:,:):: J
!locals
      real(kind=double), allocatable, dimension(:,:):: pq  
      real(kind=double), allocatable, dimension(:):: Jq,jp     
      real(kind=double), dimension( 3) :: slaveOrigin  
     integer:: i,n,Jcols,npts,i1,i2,i3,r ,nslaves
      real(kind=double) :: k  
     nslaves =ubound(ss,1)     
      
     slaveOrigin=0

      do n=1,nslaves
          if(ss(n) .lt. t%kt(1)) then
               write(*,*) ' ss(n)=t%kt(1)+RXEPSILON'
          elseif(ss(n) .gt. t%kt(ubound(t%kt,1))) then
             write(*,*) '  ss(n)=t%kt(ubound(t%kt,1))-RXEPSILON'
          endif 
      enddo
      select case( t%m_type)
        case(LOCALCOORDS, LOCALINTERP)    
              npts = ubound(t%localCVs,2);
        case (INTERPOLATING, STANDARD)
            npts = ubound(t%m_ownerString%m_pts,1) ! will be a lot less than t%np
        case default
            npts=0/0
       end select
      Jcols = 3*npts
      allocate(j(jcols+3*nslaves,jcols)); j=0.
 
 ! fill in the master node portions.
    do i = 1,npts
        r = 3*i-2
        j(r:r+2,r:r+2) = Identity(3)
    enddo   

   r = 3*npts +1 
! now fill in the rest of J for each ss(j)    
slaves: do n=1,nslaves
          select case( t%m_type)
            case(LOCALCOORDS,STANDARD)  
                  if(npts .ne. t%m_np) write(outputunit,*) 'whoops'
                   do i = 1,t%m_np
                       k  = fspline_n(t,i,t%degree,ss(n));  
                       J(r:r+2,3*i-2:3*i) = Identity(3)*k
                   enddo           
                
            case (INTERPOLATING,LOCALINTERP )
                  allocate(jp(npts))
                  allocate(jq(t%m_np))  
                  allocate(pq(npts,t%m_np)); pq=0 ! its very banded so this is slow, but...
                   do i = 1,t%m_np
                       jq(i) = fspline_n(t,i,t%degree,ss(n))
                   enddo    
                    pq(1,1)=1.0;  pq(npts  ,t%m_np)=1.0d0; ! 
                    do i = 2,t%m_np-1
                            i2 = floor(sngl(i/2)) +1
                            i1 = i2 + 2*mod(i,2)-1
                            i3 = i2 - 2*mod(i,2)+1
                            pq(i2  ,i)=1.0d0;  
                            pq(i1  ,i)=1.0d00/fac !
                            pq(i3,i) = -1.0d00/fac !                  
                    enddo  
                    jp = matmul(pq,jq); 
                    slaveorigin=0
                    do i = 1,npts
                       k = jp(i)
                       J(r:r+2,3*i-2:3*i) = Identity(3) * k
                       slaveOrigin = slaveOrigin +k*  origin(3*i-2:3*i)
                   enddo  
                   Origin(r:r+2)  =  slaveOrigin                
                   deallocate(jp,jq,pq)
            case default
                npts=0/0
        end select 
        r = r+3
    enddo slaves  
 end  function FSplineJMatrixMultNoSlide 
     
     
#ifdef BENCHTEST
function FSplineJMatrixShort(t,sin,StripNormalComponent) result(j)

 use dbg

 ! For the J matrix, the size is (3 by ((3* original no of pts)   +1)) The last column is for S.
     implicit none
! parameters     
       type (fspline),intent(in) :: t
      real(kind=double) ,intent(in) ::sin
      logical, intent(in) :: StripNormalComponent
!result
      real(kind=double), pointer, dimension(:,:):: J
!locals
      real(kind=double),dimension(3,3)  :: mm
      real(kind=double), allocatable, dimension(:,:):: pq  
      real(kind=double), allocatable, dimension(:):: Jq,jp     

     integer:: i,Jcols,npts,i1,i2,i3,r,c
      real(kind=double) :: k,s   ,lsq  
     
     s=sin
      if(s .lt. t%kt(1)) then
           s=t%kt(1)+RXEPSILON
      elseif(s .gt. t%kt(ubound(t%kt,1))) then
          s=t%kt(ubound(t%kt,1))-RXEPSILON
      endif 
      select case( t%m_type)
        case(LOCALCOORDS, LOCALINTERP)    
              npts = ubound(t%localCVs,2);
        case (INTERPOLATING, STANDARD)
            npts = ubound(t%owner%m_pts,1) ! will be a lot less than t%np
        case default
            npts=0/0
       end select
      Jcols = 1 + 3*npts
      allocate(j(3,jcols))
! last column is the derivative by S      
      J(1:3,jcols) = derivativebydsB(t,s)
!
!  if we don't strip the parallel component, the contribution
! of each CV is simply the spline-basis-function times the identity matrix I
! If we do strip the parallel component   , instead of I we use 
! (I - outer(derivative)/derivativesLengthSquared )
mm = 0.0d00; mm(1,1) = 1.0d00; mm(2,2) =1.0d00; mm(3,3) = 1.0d00;
    lsq = sum( J(1:3,jcols)**2)
 ! try july 2009 normalise the last column so that the dofo residuals are unscaled.
  !  J(1:3,jcols) = J(1:3,jcols)/sqrt(lsq) ; lsq=1.0d00;  
    if( StripNormalComponent ) then
        do r=1,3
        do c=1,3
            mm(r,c) = mm(r,c) - J(r,jcols)*J(c,jcols)/lsq
        enddo
        enddo
    endif


! now fill in the rest of J    
      select case( t%m_type)
        case(LOCALCOORDS,STANDARD)  
              if(npts .ne. t%m_np) write(outputunit,*) 'whoops'
               do i = 1,t%m_np
                   k  = fspline_n(t,i,t%degree,s); write(outputunit,*) i,k
                   J(:,3*i-2:3*i) = mm*k
               enddo           
            
        case (INTERPOLATING,LOCALINTERP )
              allocate(jp(npts))
              allocate(jq(t%m_np))  
              allocate(pq(npts,t%m_np)); pq=0 ! its very banded so this is slow, but...
               do i = 1,t%m_np
                   jq(i) = fspline_n(t,i,t%degree,s)
               enddo    
                pq(1,1)=1.0;  pq(npts  ,t%m_np)=1.0d0; ! 
                do i = 2,t%m_np-1
                        i2 = floor(sngl(i/2)) +1
                        i1 = i2 + 2*mod(i,2)-1
                        i3 = i2 - 2*mod(i,2)+1
                        pq(i2  ,i)=1.0d0; ! x=this%localCVs(1:3,i2)
                       pq(i1  ,i)=1.0d00/fac !!  x = x+ this%localCVs(1:3,i1) /fac
                        pq(i3,i) = -1.0d00/fac ! !x = x - this%localCVs(1:3,i3) /fac                    
                enddo  
                jp = matmul(pq,jq); 

                call vprintd("jp  ",jp)

                do i = 1,npts
                   k = jp(i)
                   J(:,3*i-2:3*i) = mm * k
               enddo                 
                 deallocate(jp,jq,pq)
        case default
            npts=0/0
    end select   
 end  function FSplineJMatrixShort 
#endif
function FindNearestB(this,q) result(t)
        implicit none
        class (fspline),intent(in) :: this
        real(kind=double),dimension(:) ,intent(in) ::q
        real(kind=double) ::t
! locals
        logical :: stillgoing 
        real(kind=double),dimension(3)  ::x,dx   
        real(kind=double)   ::dmin , d ,dt ,damp   
        integer ::imin  , i,count  
        damp =1.0
        x = fspline_GetP(this,1)
!  first a rough guess. Find the closest CV  
        dmin = sum((x-q)**2)
        imin=1
        do i = 2,this%m_np
           x =  fspline_GetP(this,i)
           d = sum((x-q)**2)
           if(d < dmin) then; dmin=d; imin=i; endif
       enddo
        t = real(imin-1)/real(this%m_np-1) ! a lousy guess
! now newton-raphson.   
        count=1;
         stillgoing= .true.
         do while (stillgoing)
            x = FsplineEvaluateB(this,t)
            dx = derivativebydsB(this,t)
            dt = Dot_Product(q-x,dx)/ sum(dx**2)
            t=t+dt*damp 
            if(abs(dt) .le. 1.d-7) stillgoing=.false. 
            if( mod(count,100) .eq.0) damp=damp/2.0
             if(count > 1000 ) then
             write(outputunit,*)' fspline B find-nearest not converged'
                 t =  real(imin,8)/real(this%m_np,8)  ! use the lousy guess
                   exit
               endif
               count=count+1
         end do
end function FindNearestB
end module fspline_f



