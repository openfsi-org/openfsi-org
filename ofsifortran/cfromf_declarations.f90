! prototype for the functions called by C from fortran
! must match cfromf.c  and be consistent with msc_stuf.f (the stub fns) and with cfromf.h

module cfromf_f
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f
    implicit none
    
INTERFACE 

!EXTERN_C int fc_runtopeak(const int method, int*kit) //returns iexit
function fc_runtopeak(method,kit) result  (iexit) bind(C,name='fc_runtopeak' )
     USE, INTRINSIC :: ISO_C_BINDING
     integer(c_int), intent(in), value :: method
     integer(c_int), intent(inout) :: kit
     integer(c_int) ::iexit
end function fc_runtopeak


!EXTERN_C int cf_gravity_field ( double* x,  double* acc);
function  cf_gravity_field(x,acc) result(ok)  bind(C,name='cf_gravity_field' )
        USE, INTRINSIC :: ISO_C_BINDING
        real(C_DOUBLE), intent(in) , dimension(3) :: x
        real(C_DOUBLE), intent(out) , dimension(3) :: acc
        logical(c_int) ::ok
end function cf_gravity_field


!EXTERN_C int cf_SetParentRelation ( RX_FESite p1,  RXEntity_p p2);
function  cf_SetParentRelation(p1,p2) result(k)  bind(C,name='cf_SetParentRelation' )
     USE, INTRINSIC :: ISO_C_BINDING
     use basictypes_f
     integer(kind=cptrsize),value :: p1,p2
     integer(c_int) ::k
end function cf_SetParentRelation

!EXTERN_C int cf_UnSetParentRelation ( RX_FESite p1,  RXEntity_p p2);
function  cf_UnSetParentRelation(p1,p2) result(k)  bind(C,name='cf_UnSetParentRelation' )
     USE, INTRINSIC :: ISO_C_BINDING
     use basictypes_f
     integer(kind=cptrsize),value :: p1,p2
     integer(c_int) ::k
end function cf_UnSetParentRelation


!EXTERN_C int cf_SetParentRelationStr ( RX_FEString p1,  RXEntity_p p2);
function  cf_SetParentRelationStr(p1,p2) result(k)  bind(C,name='cf_SetParentRelationStr' )
     USE, INTRINSIC :: ISO_C_BINDING
     use basictypes_f
     integer(kind=cptrsize),value :: p1,p2
     integer(c_int) ::k
end function cf_SetParentRelationStr

!EXTERN_C int cf_UnSetParentRelationStr ( RX_FEString p1,  RXEntity_p p2);
function  cf_UnSetParentRelationStr(p1,p2) result(k)  bind(C,name='cf_UnSetParentRelationStr' )
     USE, INTRINSIC :: ISO_C_BINDING
     use basictypes_f
     integer(kind=cptrsize),value :: p1,p2
     integer(c_int) ::k
end function cf_UnSetParentRelationStr


!extern "C" class RXSparseMatrix *NewSparseMatrix(int);
function  cf_NewSparseMatrix(t) result(k)  bind(C,name='NewSparseMatrix' )
     USE, INTRINSIC :: ISO_C_BINDING
     use basictypes_f
     integer(kind=cptrsize) :: k
     integer, intent(in), value::t
end function cf_NewSparseMatrix

!extern "C" vo id DestroySparseMatrix( class RXSparseMatrix *m);
subroutine  cf_DestroySparseMatrix(m)   bind(C,name= 'DestroySparseMatrix')
     USE, INTRINSIC :: ISO_C_BINDING
     use basictypes_f
      integer(kind=cptrsize),intent(in),value :: m
end subroutine  cf_DestroySparseMatrix

!extern "C" void AddToSparseMatrix(RXSparseMatrix *m,const int r,const int c, const double v);
subroutine  cf_AddToSparseMatrix(m,r,c,v)  bind(C,name='AddToSparseMatrix' )
     USE, INTRINSIC :: ISO_C_BINDING
      use basictypes_f
     integer(kind=cptrsize),intent(in),value :: m
     integer(C_INT), intent(in), value :: r,c
     real(C_DOUBLE), intent(in), value :: v   
end subroutine  cf_AddToSparseMatrix

! extern "C" void SparseMatrixDimensions(RXSparseMatrix1 *m, int* nr, int *nc)
subroutine  cf_SparseMatrixDimensions(m,nr,nc)  bind(C,name='SparseMatrixDimensions' )
     USE, INTRINSIC :: ISO_C_BINDING
      use basictypes_f
     integer(kind=cptrsize),intent(in),value :: m
     integer(C_INT), intent(out) :: nr,nc
end subroutine  cf_SparseMatrixDimensions

function  cf_SparseMatrixElement(m,r,c)  result(x)  bind(C,name='SparseMatrixElement' )
     USE, INTRINSIC :: ISO_C_BINDING
      use basictypes_f
     integer(kind=cptrsize),intent(in),value :: m
     integer(C_INT),value, intent(in) :: r,c
     real(c_double) :: x
end function  cf_SparseMatrixElement

! extern "C" void WriteSparseMatrix(RXSparseMatrix *m,const char*filename);
subroutine  cf_WriteSparseMatrix(m, f)   bind(C,name='WriteSparseMatrix' )
     USE, INTRINSIC :: ISO_C_BINDING
     use basictypes_f
     integer(kind=cptrsize),intent(in),value :: m
     character(len=1), dimension(*),intent(in) :: f
!DEC$ ATTRIBUTES REFERENCE :: f
end subroutine  cf_WriteSparseMatrix

!extern "C" int  SparseSolveLinear( RXSparseMatrix1 *m, RXSparseMatrix1 *jay,RXSparseMatrix1 *rhs,double*r ,int *myflags);
function cf_SparseSolveLinear(m,jay,rhs,res,flags) result(solverResult) bind(C,name='SparseSolveLinear' )
        USE, INTRINSIC :: ISO_C_BINDING
        use basictypes_f
        implicit none
        integer(kind=cptrsize),intent(in),value :: m,jay
        integer(kind=cptrsize),intent(in),value    :: rhs  ! the pointer wont change but its contents will
        real(C_DOUBLE), intent(out), dimension(*)  :: res
        integer(c_int),intent(inout)              :: flags  ! various meanings on input and output
        integer :: solverResult
        !solverResult = non-zero ! failure - do an explicit solution

end function cf_SparseSolveLinear
 
! extern "C" int  SparseSolveSVD( RXSparseMatrix1 *m, RXSparseMatrix1 *jay,RXSparseMatrix1 *rhs,double*r ,int *myflags);
function cf_SparseSolveSVD(m,jay,rhs,res,flags) result(solverResult)  bind(C,name='SparseSolveSVD' )
     USE, INTRINSIC :: ISO_C_BINDING
     use basictypes_f
 implicit none
 integer(kind=cptrsize),intent(in),value :: m,jay 
 integer(kind=cptrsize),intent(in),value    :: rhs  ! the pointer wont change but its contents will
 real(C_DOUBLE), intent(out), dimension(*)  :: res
 integer(c_int),intent(inout)              :: flags  ! various meanings on input and output
 integer :: solverResult
 !solverResult = non-zero ! failure - do an explicit solution

end function cf_SparseSolveSVD

function  cf_postprocess() result(k)  bind(C,name='cf_postprocess' )
     USE, INTRINSIC :: ISO_C_BINDING
      integer(c_int) :: K 
end function  cf_postprocess	

function IsEdgeNode(CSAILPTR,n) result(k) bind(C,name='cf_IsEdgeNode' )
     USE, INTRINSIC :: ISO_C_BINDING
     use basictypes_f
     implicit none	
     integer(kind=cptrsize) ,intent(in),value:: CSAILPTR
     integer(c_int), intent(in),value :: n
     integer(c_int) :: K  
end function IsEdgeNode	
	
function Post_Summary_By_SLI(sli, L,V) result(ok) bind(C,name='Post_Summary_By_SLI') ! see summary.h
     USE, INTRINSIC :: ISO_C_BINDING
      implicit none	
      integer(c_int), intent(in) :: sli
      character(len=1), dimension(*), intent(in) :: L,V
      !DEC$ ATTRIBUTES REFERENCE :: L,V

      integer(c_int) :: OK
end function Post_Summary_By_SLI
	
function CF_getTensyllinkPropNo(ptr) result (c)  bind(C,name="cf_getTensyllinkPropNo" )
     USE, INTRINSIC :: ISO_C_BINDING
     use basictypes_f
    implicit none
    integer(kind=cptrsize),value, intent(in) ::ptr 
    integer (C_INT) :: c
end  function cf_getTensyllinkPropNo


subroutine akm_insert_line(a,b,c,d,e,f,k,h,s,v) bind(C,name="akm_insert_line_" )
     USE, INTRINSIC :: ISO_C_BINDING
     use basictypes_f
     implicit none
     REAL(C_FLOAT) :: a,b,c,d,e,f
     integer(kind=cptrsize),intent(in)  ::k
     real(c_double), intent(in), optional ::h,s,v
end subroutine akm_insert_line

!extern "C" int akm_set_color_by_value_(const char*what,const char*t,float *a,float*b,float *c)
subroutine akm_set_color_by_value(what,t,a,b,c) bind(C,name="Akm_Set_Color_By_Value"  )
     USE, INTRINSIC :: ISO_C_BINDING 
     implicit none
     character(len=1), dimension(*),intent(in) :: what,t
     !DEC$ ATTRIBUTES REFERENCE ::what,t
     REAL(C_FLOAT) ::     a,b,c
end subroutine akm_set_color_by_value

integer(c_int) function fc_unhookfromfea(PTR)  bind(C,name= "unhookfromfea" )
     USE, INTRINSIC :: ISO_C_BINDING
     use basictypes_f
     implicit none
     integer(kind=cptrsize),intent(in), value :: PTR
end function fc_unhookfromfea
 
integer(c_int) function fc_write_pcfile(SAILPTR, filename) bind(c,name="write_pcfile")
     USE, INTRINSIC :: ISO_C_BINDING
     use basictypes_f
     implicit none
     integer(kind=cptrsize) :: SAILPTR
     character(len=1), dimension(*),intent(in) ::filename
     !DEC$ ATTRIBUTES REFERENCE ::filename
end function fc_write_pcfile

! EXTERN_C int check_messages(int *k);
integer(c_int) function check_messages (k) bind(c,name="check_messages" )
     USE, INTRINSIC :: ISO_C_BINDING
     implicit none
     integer(c_int) :: k
end  function check_messages 

! EXTERN_C void appwind_(float *z,float *u, float *v, float *w);
subroutine APPWIND (z,u,v,w)  bind(C,name="appwind_" )
     USE, INTRINSIC :: ISO_C_BINDING
     implicit none
     REAL(C_FLOAT),intent(in) :: z
     REAL(C_FLOAT),intent(out) :: u,v,w         
end  subroutine appwind 

!EXTERN_C void getcurrentdir(char *s, int *l); see getglobal.h

subroutine getcurrentdir(s,L) bind(C, name= "getcurrentdir" )
     USE, INTRINSIC :: ISO_C_BINDING
     implicit none
     integer(C_int),intent(in) ::L 
     character(len=1), dimension(L),intent(in)::S
end subroutine getcurrentdir 

integer function pcc_send_as_client(s,L)  bind(C,name="Pcc_Send_As_Client" )
     USE, INTRINSIC :: ISO_C_BINDING
     implicit none
     integer(C_int),intent(in) ::L 
     character(len=1), dimension(*),intent(in) :: S
     !DEC$ ATTRIBUTES REFERENCE ::S
end   function pcc_send_as_client   
     
! void Get_Next_Material_Stress(Ftri**trino,double *eg,int *Force_Linear,double *slr,int *flag)
pure subroutine Get_Next_Material_Stress(tc, eg,force_linear, slr, flag)  bind(C,name= "Get_Next_Material_Stress"  )
     USE, INTRINSIC :: ISO_C_BINDING
     use basictypes_f
     implicit none
     integer(kind=cptrsize),intent(in) :: tc
     integer(C_int) ,intent(in) :: flag, force_linear
     real(c_double),dimension(3) ,intent(in) ::eg 
     real(c_double),dimension(3) ,intent(out) :: slr
end subroutine Get_Next_Material_Stress

!EXTERN_C int Anchor_Loads(double *tmax);   
subroutine anchor_loads(tmax)  bind(C,name= "Anchor_Loads" )
     USE, INTRINSIC :: ISO_C_BINDING
     implicit none
     real(c_double),intent(in) ::tmax
end subroutine anchor_loads

! EXTERN_C int fc_getlastknowngood(RX_FESite*cptr, double x[3]);
integer(c_int) function fc_getlastknowngood (cptr,x) bind(c, name="fc_getlastknowngood")
 USE, INTRINSIC :: ISO_C_BINDING
 use basictypes_f
 integer(kind=cptrsize) , intent(in),value :: cptr
real(c_double),dimension(3), intent(out) ::x
end function fc_getlastknowngood

! EXTERN_C int fc_getundeflected(RX_FESite*cptr, double x[3]);
integer(c_int) function fc_getundeflected (cptr,x)  bind(C,name="fc_getundeflected"  )
        USE, INTRINSIC :: ISO_C_BINDING
        use basictypes_f
        integer(kind=cptrsize) , intent(in),value :: cptr
        real(c_double),dimension(3), intent(out) ::x
end function fc_getundeflected
	
! EXTERN_C int draw_resids (double*tmax);
integer(c_int) function draw_resids (tmax)  bind(C,name="Draw_Resids" )
        USE, INTRINSIC :: ISO_C_BINDING
        real(c_double) :: tmax
end function draw_resids

! extern "C" int fc_OnReload(void);

integer(c_int) function fc_OnReload () bind(C,name="fc_OnReload" )
 USE, INTRINSIC :: ISO_C_BINDING
end  function fc_OnReload 

! EXTERN_C int ClearBatten(void *ptr);
integer (C_INT) function ClearBatten(ptr) bind(C, name= "ClearBatten")
        USE, INTRINSIC :: ISO_C_BINDING
        use basictypes_f
        implicit none
        integer(kind=cptrsize) :: ptr
end function ClearBatten	
	
! EXTERN_C int getfortrannodeno(char *model, char *nname);
integer (C_INT) function getfortrannodeno(modelname, nname) bind(c, name= "getfortrannodeno" )
        USE, INTRINSIC :: ISO_C_BINDING
        implicit none
        character(len=1), dimension(*),intent(in)::modelname,nname
!DEC$ ATTRIBUTES REFERENCE ::modelname,nname
end function getfortrannodeno
    
!EXTERN_C int contact_find_intersection(RXEntity_p *cptr, double oldpt[3], double proposedpt[3], double newptxyz[3],double*k) ;
! returns 0 if no intersection was found, else C_GOING_UP or C_GOING_DOWN
 ! *k is the parameter on the line from oldpt to newpt 
function  contact_find_intersection(cptr,oldpt,proppt,newptxyz,uvw,k) result(rc) bind(C,name="contact_find_intersection" )
     USE, INTRINSIC :: ISO_C_BINDING
        use basictypes_f
      	implicit none       
        integer(kind=cptrsize) ,intent(in) ::cptr ! a RXEntity contact surface
        real(kind=double), dimension(*):: oldpt,proppt,newptxyz,uvw ! always 3
        real(kind=double) :: k
        integer ::rc
end function  contact_find_intersection

!EXTERN_C int contact_find_jacobian(RXEntity_p *cptr, double uv[2],double j[9]); // return OK
function contact_find_jacobian(cptr,uv,j) result(rc)  bind(C,name= "contact_find_jacobian" )
        USE, INTRINSIC :: ISO_C_BINDING
        use basictypes_f
        implicit none
        integer(kind=cptrsize),intent(in) ::cptr ! a RXEntity contact surface
        real(kind=double), dimension(*):: uv ! always 2
        real(kind=double), dimension(*):: j   ! always 9
        integer ::rc
    end function  contact_find_jacobian
  
!EXTERN_C int contact_test_passedge(RXEntity_p *cptr,const double uv1[2],const double uv2[2],double *k); 
						   ! return non-zero if uv2 is off edge
    
function contact_test_passedge( cptr, uv1, uv2 , k) result(yes)bind(c, name= "contact_test_passedge")
        USE, INTRINSIC :: ISO_C_BINDING
        use basictypes_f
         implicit none
       integer(kind=cptrsize),intent(in) ::cptr ! a RXEntity contact surface
        real(kind=double), dimension(*):: uv1,uv2  ! always 2
        real(kind=double), intent(out) :: k
        integer:: yes					   
end function contact_test_passedge
						   
pure subroutine  contact_evaluate(cptr,uv,p) bind(c, name= "contact_evaluate" )
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f
        integer(kind=cptrsize),intent(in) ::cptr ! a RXEntity contact surface
        real(C_double), dimension(*),intent(in) :: uv ! always 2
        real(C_double), dimension(*),intent(out) :: p ! always 3

end subroutine  contact_evaluate

function contact_test_isbelow(cptr, proposedpt, newptuvw) result(found) bind(C, name= "contact_test_isbelow")
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f
    integer(kind=cptrsize) ::cptr ! a RXEntity contact surface
    real(kind=double), dimension(*):: proposedpt,newptuvw
     integer ::found
end function contact_test_isbelow

!!
!!!!!!!!!!!!!!!!!!!Slide curve functions


function slidecurve_update_param(cptr,t,dt) result(ok)bind(c, name="slidecurve_update_param")
    USE, INTRINSIC :: ISO_C_BINDING
        use basictypes_f
        integer(kind=cptrsize),intent(in) ::cptr ! a RXEntity 
        real(C_DOUBLE), intent(inout) :: t 
        real(C_DOUBLE), intent(in)    :: dt  
        integer(C_INT) ::ok
end function  slidecurve_update_param

! EXTERN_C void slidecurve_lengthto(const RXEntity_p *cptr,const double *p_t,double *length); 
pure subroutine slidecurve_lengthto(cptr,t,length)   bind(C,name= "slidecurve_lengthto")
    USE, INTRINSIC :: ISO_C_BINDING
        use basictypes_f
        integer(kind=cptrsize),intent(in) ::cptr ! a RXEntity 
        real(C_DOUBLE), intent(in) :: t 
        real(C_DOUBLE),intent(out) :: length 
 end subroutine  slidecurve_lengthto


! EXTERN_C void slidecurve_evaluate_(const RXEntity_p *cptr,const double *p_t,double p[3]); 
pure subroutine slidecurve_evaluate(cptr,t,p)   bind(C,name="slidecurve_evaluate"  )
    USE, INTRINSIC :: ISO_C_BINDING
        use basictypes_f
        integer(kind=cptrsize),intent(in) ::cptr ! a RXEntity 
        real(C_DOUBLE), intent(in) :: t 
        real(C_DOUBLE), dimension(*),intent(out) :: p ! always 3
 end subroutine  slidecurve_evaluate


!EXTERN_C int int slidecurve_find_intersection_(RXEntity_p *cptr,
!					   double p_newptxyz[3],double *u) ;
					   
function  slidecurve_find_intersection(cptr,oldpt,newptxyz,t) result(rc) bind(c, name=  "slidecurve_find_intersection")
     USE, INTRINSIC :: ISO_C_BINDING
        use basictypes_f
      	implicit none       
        integer(kind=cptrsize) ,intent(in) ::cptr ! a RXEntity contact surface
        real(C_double), dimension(3):: oldpt,newptxyz ! always 3
        real(c_double) :: t
        integer(C_INT) ::rc
end function  slidecurve_find_intersection				   
					   
!EXTERN_C int slidecurve_find_jacobian_(RXEntity_p *cptr, double *t,double j[9]); // return OK
function slidecurve_find_jacobian(cptr,t,j) result(rc) bind(C,name="slidecurve_find_jacobian" )
        USE, INTRINSIC :: ISO_C_BINDING
        use basictypes_f
        implicit none
        integer(kind=cptrsize),intent(in) ::cptr ! a RXEntity contact surface
        real(kind=double),intent(in)    :: t
        real(kind=double), dimension(*):: j   ! always 9
        integer ::rc
end function  slidecurve_find_jacobian					   

function fc_dump_snode(snode) result (rc)  bind(C,name= "fc_dump_snode"  )
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f
    implicit none
    integer(kind=cptrsize)   ,intent(in),value ::snode
    integer(c_int) :: rc
end function fc_dump_snode


integer(C_INT)function fc_getEntityName(eptr,buf,lbuf)   bind(C,name="fc_getentityname" )
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f
    implicit none
    integer(kind=cptrsize)   ,intent(in),value ::eptr
    character(len=1), dimension(*) ::buf
    integer(c_int), intent(in), value :: lbuf
end function fc_getEntityName

integer(C_INT) function  fc_SetSiteAttribute(snodeptr ,what,val ) bind(C,name="fc_SetSiteAttribute"  )
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f
    implicit none
    integer(kind=cptrsize)   ,intent(in),value ::snodeptr
    character(len=1), dimension(*),intent(in):: what, val
end  function fc_SetSiteAttribute

integer(C_INT)function fc_getsitename(snodeptr,buf)  bind(C,name="fc_getsitename"  )
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f
    implicit none
    integer(kind=cptrsize)   ,intent(in),value ::snodeptr
    character(len=1), dimension(*) :: buf
!DEC$ ATTRIBUTES REFERENCE ::buf
end function fc_getsitename

function fc_get_slidecurve(snode) result(ent) bind(C, name= "get_slidecurve" )
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f
    implicit none
    integer(kind=cptrsize) ,intent(in),value ::snode
    integer(kind=cptrsize) ::ent
end function fc_get_slidecurve

function fc_get_contact_surface(snode) result (ent) bind(C,name=  "get_contact_surface" )
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f
    implicit none
    integer(kind=cptrsize)   ,intent(in),value ::snode
    integer(kind=cptrsize) ::ent
end function fc_get_contact_surface

! EXTERN_C int cf_set_dofo_eptr (const int sli,const int dofoN,const class RXObject*ptr);
 function cf_set_dofo_eptr(sli,dofon, ptr) result(ok) bind(C,name="cf_set_dofo_eptr" )
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f
    implicit none
    integer(kind=c_int) ,intent(in),value ::sli,dofoN    
    integer(kind=cptrsize) ,intent(in),value ::ptr
    integer(C_INT)::ok
end function cf_set_dofo_eptr

integer(C_INT) function get_nodal_fixing(snodeptr, buf)  bind(C,name= "cf_get_nodal_fixing" )
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f
    implicit none
    integer(kind=cptrsize) ,intent(in),value ::snodeptr
    character(len=1), dimension(*) ::buf
    !DEC$ ATTRIBUTES REFERENCE ::buf
end function get_nodal_fixing

function get_vector_plot_scale() result (x)  bind(C,name= "get_vector_plot_scale")
   USE, INTRINSIC :: ISO_C_BINDING
    implicit none
	real(c_double) :: x
end function get_vector_plot_scale

! EXTERN_C int fc_delete_object(class RXObject *ptr)
function  fc_delete_object(ptr) result (ok)  bind(C,name="fc_delete_object" )
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f
    implicit none
    integer(kind=cptrsize) ,intent(in),value ::ptr  ! a RXObject
    integer(c_int) :: ok
end function fc_delete_object

END INTERFACE
end module cfromf_f
