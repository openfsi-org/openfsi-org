MODULE vectors_f
USE basictypes_f 
IMPLICIT NONE


CONTAINS
 pure     REAL*8 FUNCTION PH_atan2(dy,dx)
      IMPLICIT NONE
!* this functn cannot cope with angles close to 180 degrees but is safer
!* otherwise.
!* lahey ATAN2 has trouble with (0,small)

 	  real(kind=double),intent(in) ::Dy,dx
	  real(kind=double):: grad

       PH_atan2 = atan2(dy,dx)
       RETURN

      if ( abs(dx) .gt. 1.0d-8) THEN
        grad = dy/dx
        if (abs(dy) .lt. 1.0d-8) THEN
          PH_atan2 = atan(grad)
        ELSE
          PH_atan2 = atan2(dy,dx)
        ENDIF
      ELSE
 !       write(outputunit,*)' atan2 error '
        PH_Atan2= 0.0d00
      ENDIF
      END FUNCTION PH_atan2

pure function IdentityMatrix (n) result (c)  
		IMPLICIT NONE
		integer , intent(in):: n
		REAL (kind= double),  dimension(n,n)	 :: c
		integer :: i
		c = 0
		do i=1,n
			c(i,i) = 1
		enddo
end function IdentityMatrix

pure function Outer_Product (a,b,n) result (c)  ! CHECKED.  Sense is OK
		IMPLICIT NONE
		integer , intent(in):: n
		REAL (kind= double), INTENT(in), dimension(n)	 :: a,b
		REAL (kind= double),  dimension(n,n)	 :: c
		integer :: i

		do i=1,n
			c(1:n,i) = a * b(i)
		enddo
end 	function Outer_Product   
  
pure SUBROUTINE normalise(vector,length,error)  
      IMPLICIT NONE      
!* Finds the length of a vector and makes the vector unit length. 
!* Parameters:  
      REAL(kind=double), intent(inout), dimension(:) :: vector 
      REAL(kind=double),intent(out) :: length  
      LOGICAL,intent(out) :: error   

      length=norm(Vector)  
      IF(length .lt. 1d-5) THEN 
         error = .true.   
        RETURN       
      ENDIF         
      vector=vector/length
      error = .FALSE.          
END  SUBROUTINE normalise 
   
pure FUNCTION norm(Vector)  result(x)
      IMPLICIT NONE            
      REAL(kind=double), intent(in), dimension(:) :: vector  
      real(kind=double) ::x                                 
      x =sqrt(Vector(1)**2 + Vector(2)**2 + Vector(3)**2) 
END FUNCTION norm  
     
pure function Mnorm(a) result (b) ! the sum of the vector norms of each row. 
	implicit none

	real(kind=double),target, intent(in), dimension(:,:) :: a
!	real(kind=double), pointer, dimension(:) :: row
	real(kind=double) :: b
	integer::  i,n1,n2, rank
	rank = size(shape(a))
	if(rank .eq. 1) then
!		b = sqrt(dot_product(a,a))
!		return
	endif
	if(rank .gt. 2) then
	    i = 0
	     i=0/i ! write(outputunit,*) ' norm not coded above rank 2'
    endif
	n1 = ubound(a,1); n2 = ubound(a,2)
	b = 0.0d00
	do i=1,n1
		!row=>a(i,1:n2)
		b = b + sqrt(dot_product(a(i,1:n2),a(i,1:n2)))
	enddo

end function mnorm	 
	     
 pure function Cross_product(a,b) result (c)
		implicit none
		real(kind=double), intent(in), dimension(3) ::  a,b
		real(kind=double),  dimension(3) :: c
		Call vprod(a,b,c)
end function Cross_product
 
                          
pure SUBROUTINE vprod(A,B,C)  !finds the vector product  C = A x B
    IMPLICIT NONE   
	REAL (kind=double)  ,dimension(:), intent(in) :: a,b
	REAL (kind=double)  ,dimension(:), intent(out) :: c
                            
      	C(1)=    A(2)*B(3)  -   A(3)*B(2)
      	C(2)=   -A(1)*B(3)  +   A(3)*B(1) 
      	C(3)=    A(1)*B(2)  -   A(2)*B(1)
END SUBROUTINE vprod

function crossproduct(A,B) result(C)  !finds the vector product  C = A x B
IMPLICIT NONE   
	REAL (kind=double)  ,dimension(:), intent(in) :: a,b
	REAL (kind=double)  ,dimension(3) :: c
                            
      	C(1)=    A(2)*B(3)  -   A(3)*B(2)
      	C(2)=   -A(1)*B(3)  +   A(3)*B(1) 
      	C(3)=    A(1)*B(2)  -   A(2)*B(1)
END function crossproduct
                                                
SUBROUTINE MULT(A,B,C)
!* postmultiplies Matrix B by Matrix C to give matrix A 
      IMPLICIT NONE                               

      REAL*8 A(3,3),B(3,3),C(3,3)    
!*
!*   A= B . C
!* 

   a=matmul(b,c)
   return

      END  SUBROUTINE MULT

      SUBROUTINE MULTV(A,B,C)
!* multiplies Matrix B by vector C to give vector A   
 
      IMPLICIT NONE 
      REAL*8 A(3),B(3,3),C(3) 
!*     
!*   A = B X C
!*      
   a=matmul(b,c)
   return
      A(1)=B(1,1)*C(1)+B(1,2)*C(2)+B(1,3)*C(3) 
      A(2)=B(2,1)*C(1)+B(2,2)*C(2)+B(2,3)*C(3)
      A(3)=B(3,1)*C(1)+B(3,2)*C(2)+B(3,3)*C(3) 
      END    SUBROUTINE MULTV
           
      SUBROUTINE Zero_Matrix(a,n)  
      IMPLICIT NONE  
           
      REAL*8 A(*) 
      INTEGER N,k 
      DO K = 1,n
       A(K) = 0.0d00 
      ENDDO    
      END SUBROUTINE Zero_Matrix

pure function Identity(i) result (a)
	 implicit none
	 integer, intent(in) :: i
	real(kind=double), dimension(i,i) :: a
	integer :: k

	a = 0.0d0;
	do k=1,i
		a(k,k) = 1.0d0
	enddo

end function identity

SUBROUTINE Make_Symmetrical(A,nd,n)
      IMPLICIT NONE
      INTEGER,intent(in) ::  nd,n	
      REAL*8 A(nd,nd)


!* locals
       integer i,j
       
       DO I=1,n
       DO J=i,n
      A(i,j) = (A(I,j) + a(j,i) )/2.0D00
       ENDDO
       ENDDO
       DO I=1,n
       DO J=1,i-1
      A(i,j) = A(j,i)
       ENDDO
       ENDDO

END SUBROUTINE Make_Symmetrical

pure function kdel(i,j) result(k)	! kronecker delta
	integer, intent(in) ::  i,j
	integer :: k

	if( i .eq. j) then
		k = 1
	else
		k = 0
	endif
end function kdel

pure function eijk(i,j,k) result (p)
!  the Levi-Civita symbol AKA  permutation symbol, antisymmetric symbol, or alternating symbol
    integer, intent(in) ::i,j,k
    integer::p
    p=0;

    if(i.eq.j .or. j.eq.k .or. k.eq.i ) return
    p=1
    if(i.eq.1 .and. j.eq.2 .and. k.eq.3 ) return
    if(i.eq.2 .and. j.eq.3 .and. k.eq.1 ) return
    if(i.eq.3 .and. j.eq.1 .and. k.eq.2 ) return
    p=-1
    if(i.eq.3 .and. j.eq.2 .and. k.eq.1 ) return
    if(i.eq.1 .and. j.eq.3 .and. k.eq.2 ) return
    if(i.eq.2 .and. j.eq.1 .and. k.eq.3 ) return

    p=0; p=1/p  ! force a crash
end function eijk

pure function tri_area( x1,x2,x3) result (a)
	real(kind=double), intent(in) , dimension(3) :: x1,x2,x3

	real(kind=double) :: a

	a =norm( Cross_product(x2-x1,x3-x1)) /2.0d00

end  function tri_area
	   
pure function SCaxisTrigraph(x1,x2,x3,y) result (a) 

	implicit none
	real(kind=double), intent(in), dimension(3)	:: x1,x2,x3
	integer ,intent(in) :: y       ! a flag, negative for a left hand set



!               note y can be -ve, meaning a lh coordinate set

	real*8 a(3,3)   !       the matrix we are making
!       finds the axis matrix 

	integer  k
	real*8 xaxis(3),yaxis(3) ,zaxis(3)
	real*8 z,sign
	logical error

	if(y .gt. 0) then
		sign = 1.0d00
	else
		sign = -1.0d00
	endif


	yaxis = ( x3-x1 ) * sign
	 xaxis = x2-x1

	call normalise(xaxis,z,error)
	do 20 k=1,3
20      a(k,1) = xaxis(k) ! a(1,k) = xaxis(k)

	call vprod(xaxis,yaxis,zaxis)   ! returns zaxis

	call normalise(zaxis,z,error)

	call vprod(zaxis,xaxis,yaxis)   ! returns yaxis
	call normalise(yaxis,z,error)
	do 120 k=1,3
	a(k,2) = yaxis(k)       !       a(2,k) = yaxis(k)
120     a(k,3) = zaxis(k)       !       a(3,k) = zaxis(k)
	
end function SCaxisTrigraph

END MODULE vectors_f

