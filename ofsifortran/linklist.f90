
MODULE linklist_f
IMPLICIT NONE

TYPE :: linklist
	! SEQUENCE
	INTEGER :: v
	TYPE (linklist), POINTER ::n
END TYPE
CONTAINS

function  pop(l,error)  result(n)
IMPLICIT NONE
TYPE (linklist), POINTER ::l
INTEGER,INTENT(out)  :: error
integer ::n 

	TYPE (linklist), POINTER ::temp

	if(.not. associated(l)) THEN
	    n=0
		error=1
		return
	endif
	temp => l
	n = l%v
	l=> l%n
        DEALLOCATE(temp,stat=error)
        if(error /=0) then
                    write(*,*)' Linklist deallocate error!!!!!!!!!!!!!!!!'
                    CALL PrintList(l)
        endif
	error=0
END function  pop


SUBROUTINE  push(l,n,error)
IMPLICIT NONE
TYPE (linklist), POINTER :: l
INTEGER,INTENT(in)  :: n
INTEGER,INTENT(out)  :: error

	TYPE (linklist), POINTER ::temp
	INTEGER :: istat=0
	!write(outputunit,*) ' at start of push ' ,n
!	CALL PrintList(l)
	if(ASSOCIATED(l)) THEN
		ALLOCATE(temp,stat=istat)
		temp%n=>l
		temp%v=n
		l=>temp
	ELSE
		ALLOCATE(temp,stat=istat)
		l=>temp
		NULLIFY(l%n)
		l%v=n
	ENDIF
	error = istat
!	write(outputunit,*) ' at end of pushing ',N 
!	CALL PrintList(l)
END SUBROUTINE  push

SUBROUTINE  pull(p_l,n,error)
      use basictypes_f  ! for outputunit
    IMPLICIT NONE
    TYPE (linklist), POINTER :: p_l
    INTEGER,INTENT(in)  :: n
    INTEGER,INTENT(out)  :: error
 ! locals   
    TYPE (linklist), POINTER :: p
	INTEGER :: istat=0

!	write(outputunit,*) ' at start of pull ' 
!	CALL PrintList(p_l)
!  walk the list.  If we find a member contining n=N, we pop that member. and continue. 	
	if(.not. associated(p_l))THEN
		! write(outputunit,*) ' Nothing in list'
		return
	endif
 	p=>p_l
d:	DO
		if(.not. associated(p)) exit d
		if(p%v==n) then
             write(outputunit,*) ' need to pull ',n 
  	    	p=>p%n         
		else
	    	p=>p%n
		endif
	END DO d

	error = istat
END SUBROUTINE  pull


SUBROUTINE ClearList(l)
use basictypes_f  ! for outputunit
IMPLICIT NONE
TYPE(linklist), POINTER :: l

INTEGER :: n,error
    if (.not. associated(l)) return
	write(outputunit,*) 'ClearList'

	DO
		n= pop(l, error) 
		if(error>0 ) exit
		write(outputunit,*)' popped ',n
	END DO
	write(outputunit,*) 'End ClearList'
END SUBROUTINE ClearList


SUBROUTINE PrintList(p_l)
use basictypes_f  ! for outputunit
IMPLICIT NONE
TYPE(linklist), POINTER :: p_l
TYPE(linklist), POINTER :: p 

	write(outputunit,*) 'PrintList'
	if(.not. associated(p_l))THEN
		 write(outputunit,*) ' Nothing in list'
		return
	endif
 	p=>p_l
d:	DO
		if(.not. associated(p)) exit d
		write(outputunit,*)p%v
		p=>p%n
	END DO d
END SUBROUTINE PrintList


END MODULE linklist_f

