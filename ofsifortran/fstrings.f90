MODULE fstrings_f
use ftypes_f

contains


integer function CS_Check_One_String_Reversal(t) result(iret)
	use links_pure_f
	IMPLICIT NONE

	type(string),intent(inout) :: t
	iret = SP_Check_One_String_Reversal(t%m_sli, t%d%Ne,t%rev,t%se)
 
END FUNCTION CS_Check_One_String_Reversal

SUBROUTINE CS_GetStringNodeList(t, nodes,nn,PleaseDropEnds)
use saillist_f
	implicit none
	type(string)		:: t		! the string
	integer, dimension(:)	:: nodes	! the list of nodes
	integer , intent(out)	:: nn	! the number in nodes
	logical, intent(in) :: PleaseDropEnds
 	integer i,ll,sli ,J 
	integer nodecapacity
   
     TYPE (edgelist),pointer:: eds
      type(Fedge),pointer ::e 
      eds=>saillist(t%m_sli)%edges
	nodecapacity = size(nodes)
	!write(outputunit,*) ' in CSGTStringNodeList with node cap ',nodecapacity

	sli = t%m_sli

	i=   CS_Check_One_String_Reversal(t) ;  
	if(i==0) then
		t%d%Ne = 0
		write(outputunit,*) ' dead string'
	endif
    if(PleaseDropEnds) then
	DO J =2 ,t%d%Ne -1
	 LL = t%se(J); e=>eds%list(LL)
    i = J-1
	  if(t%rev(J)) THEN
		Nodes(i)   = e%m_L12(2)
		Nodes(i+1) = e%m_L12(1)
	 ELSE
		Nodes(i)   = e%m_L12(1)
		Nodes(i+1) = e%m_L12(2)
       	ENDIF
	ENDDO
	nn = t%d%Ne-1    
    else
	DO I = 1,t%d%Ne
	 LL = t%se(i); e=>eds%list(LL)

	  if(t%rev(i)) THEN
		Nodes(i)   = e%m_L12(2)
		Nodes(i+1) = e%m_L12(1)
	 ELSE
		Nodes(i)   = e%m_L12(1)
		Nodes(i+1) = e%m_L12(2)
       	ENDIF
	ENDDO
	nn = t%d%Ne+1 
	endif

END SUBROUTINE CS_GetStringNodeList


integer function CS_Check_One_String_Order(sli,n) result(iret)! reorders string N and sets rev
use stringsort_f
use saillist_f
	IMPLICIT NONE
	integer, intent(in) :: sli,n
	type (string), pointer  :: t  ! 16:55 guessed the pointer not inout
! LOCALS
	integer  N_e ,k  
    type (stringsortInterfaceItem), allocatable, dimension(:) ::  ss  

    integer, parameter :: edim = 100
 	t=>saillist(sli)%strings%list(n)%o
        if( .not. t%used) then
            write(*,*) "CHecking Order in an unused string ", n
            iret=0
            return
        endif
	iret = 1	! 0 means bad, 1 means OK

	N_e = t%d%Ne 
	Allocate(ss(n_e))
!  se(i,n) is the i'th edge in string n

	if(N_E .le.1) then
   	    	t%rev = .false.
	else
        ss(1:n_e)%ne = 	t%se(1:n_e)
  	    ss(1:n_e)%m_r = t%rev(1:n_e)        
        do k=1,n_e
            ss(k)%m_L12 = 	saillist(t%m_sli)%edges%list(t%se(k))%m_L12     
        enddo

	!    seLoc(1:n_e) = 	t%se(1:n_e)
	!    rLoc(1:n_e) = t%rev(1:n_e)
 !	    iret = string_sort(seLoc(1:n_e),n_e,rLoc(1:n_e),edim,L12)
 	     iret = string_sort2(ss(1:n_e),n_e)
	    t%se(1:n_e) = ss(1:n_e)%ne
	    t%rev(1:n_e) = ss(1:n_e)%m_r
 	    t%d%Ne=n_e
	endif
	DeAllocate(ss)
	!DeAllocate(seLoc)
	!DeAllocate(rLoc)

END function CS_Check_One_String_Order
 
function CS_Find_String_By_Name( name, theString) result(found)
	use saillist_f
	use ftypes_f
      IMPLICIT NONE
	character*(*) name

	TYPE ( stringlist) ,POINTER  :: strings
	type (string), pointer  :: theString

	integer sn,i,l1,l2
	logical found
	l1 = len(trim(name)); 

	DO sn=1,SailCount
		if( .not. is_active(sn) ) cycle
		strings =>saillist(sn)%strings
		if(.not. associated(strings%list)) cycle
    		 DO i=1, strings%count
			theString => strings%list(i)%o
			if(.not. theString%used) cycle

			l2 = len_trim(trim(theString%m_text))
			!write(outputunit,*) '    test ',sn,' <',trim(theString%m_text),'>  len = ',l2
			if (l1 == l2) then
				if(theString%m_text(:l2) == name(:l1)) then
				    found = .true.
				    return
				endif
			endif		
		enddo
	enddo

	NULLIFY(theString)
	found = .false. 
end function CS_Find_String_By_Name

function CS_GetStringEdgeLengths(this, t)   result(OK) 
use saillist_f
implicit none
	type (string) , pointer :: this  !POINTER????????
	REAL  (kind=double), dimension (:) , POINTER :: t
	logical OK

	integer ::i,ll,sli
    type(edgelist) ,pointer ::ee
    
 	sli = this%m_sli   
    ee=>saillist(sli)%edges
	OK = .true.
	t(1) = 0.0
	DO I = 1,this%d%Ne
		 LL = this%se(i)	! the link
		t(i+1) = t(i) + ee%list(ll)%m_ZLINK0
	enddo

end function CS_GetStringEdgeLengths
end MODULE fstrings_f

