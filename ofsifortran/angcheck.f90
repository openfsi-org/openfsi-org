
module angcheck_F

contains
function Tri_anglesDEAD(E,A)   result (Abis)!  bind(C,name="cf_tri_angles" )
	USE, INTRINSIC :: ISO_C_BINDING
	use mathconstants_f
      IMPLICIT NONE
      REAL(C_DOUBLE), intent(in), dimension(3) :: E
      REAL(C_DOUBLE), intent(out), dimension(3) :: A
      REAL(C_DOUBLE), dimension(3) :: Abis
!*     use the cosine rule to calculate the internal angles of a triangle
      
      INTEGER I,Ia,Ib,Ic
      REAL*8 V, asum
       asum = 0.0D00
      DO I=1,3        ! corner number
        Ia= I+1
        Ib= I+2
        Ic= I
        if(ia .gt. 3) ia = ia - 3
        if(ib .gt. 3) ib = ib - 3
        V = (e(Ib)**2 + e(Ic)**2 - e(Ia)**2)/(2.0d00*E(Ib)*E(Ic))
        V= MIN(1.0d00, V)

        V= MAX(-1.0d00,V)
        A(I)= acos(V)
        asum = asum + A(I)
      ENDDO
    !    if (ABS(asum-PI) .gt. 1.0D-12) THEN
     !   write(outputunit,*)' asum - pi is small ',  asum - 4.0D00*atan(1.0D00)
     !  ENDIF
     abis=a
END function Tri_anglesDEAD



       SUBROUTINE Test_DeltaK(sli)
	use fglobals_f
	use basictypes_f
           IMPLICIT NONE
           integer(C_int), intent(in),value ::sli

#ifndef NEVER
	write(outputunit,*) ' dummy test_DeltaK',sli
#else
      INCLUDE 'sailcom.f'
       include 'edgecom.f'
     
      include 'offsets.f'
       include 'sailrun.f'


! method
!*	1) get the solid angles based on current shape and on initial shape
!*	2) get all three delta_k components
!*	3) print all of these

        REAL*4 d(NDOD,3)
     	character*16 buf
        integer :: I,J,K,L,N,u = 1011
       REAL*8 A1(3),a2(3),dl1,dl2,dl3,eps(3), ref, ds, SUmDK
	REAL*8 zir(3) ,s1(NDOD) ,s2(NDOD)

         RETURN		!  commented out
	D=0.0
	

        write(outputunit,*) ' Test_Delta_K on model ',Model
  
        DO I=1, NDOD  ! nds(Model),nde(Model)
               s1(I) = 0.0D00
              s2(I) = 0.0D00
      ENDDO

	write(buf,'(a,i2.2,a)')'dk',model,'.out'
	       write(outputunit,*) ' printing to ',buf
        open(u,file=buf,status='unknown')

      DO L=nts(Model),nte(MODEL)
 
         if(use_tri(L) ) then
            DO K=1,3
               zir(K) = Zlink0(edge(K,L))
            ENDDO
            
            A1= Tri_angles(Zir,A1)
	      DO K=1,3
               zir(K) = zir(K)+delta0(edge(K,L))
            ENDDO

            A2= Tri_angles(Zir,A2)
        
            DO j=1,3
               I=N13(L,J) -nds(Model) + 1
               s1(I) = s1(I) + A1(J)
	       s2(I) = s2(I) + A2(J)
            ENDDO

            N=L
           dl1 = delta0(edge(1,n))
           dl2 = delta0(edge(2,n))
           dl3 = delta0(edge(3,n))
           CALL Get_Tri_Ref_Angle(N,ref)
           Eps(1)=G(1,1,N)*dl1+G(1,2,N)*dl2+G(1,3,N)*dl3 !+ E0(1,N)
           Eps(2)=G(2,1,N)*dl1+G(2,2,N)*dl2+G(2,3,N)*dl3 !+ E0(2,N)
           Eps(3)=G(3,1,N)*dl1+G(3,2,N)*dl2+G(3,3,N)*dl3 !+ E0(3,N)
           write(u ,111) ' N, eps,thetae, refang ',N,eps,thetae(N), ref

111      FORMAT (a,2x,i5,2x,3G16.5,2(2x,g12.4))
         endif
      ENDDO

!         DO K=1,3
!C        CALL delta_K(Model,K,d(1,K))
 !       ENDDO

      write(u,'(a)')' I,s1(I),s2(I),(d(I,k),K=1,3 ), Sdiff, SUm DKS'

        DO I=1,nde(Model)-nds(Model) + 1
                   ds = s2(i) - s1(i)
                   sumDK = D(I,1) + D(I,2) + D(I,3)
                  write(u,10) I,s1(I),s2(I),(d(I,k),K=1,3 ), ds, sumDK
        ENDDO
10      FORMAT(i5,5G15.7,2x,2G16.5)
      	CLOSE(u)

#endif
END SUBROUTINE Test_DeltaK
      
! flag==0 means get the raw angular deficits.
SUBROUTINE Calc_S_Angles(sli,p_DivByArea, total) bind(C,name="cf_calc_s_angles" )  !flag !=0 means divide by area
!
!	call with flag=1 for internal surveys
!   	call with flag=0  for boundary surveys  
	USE tris_f
	USE, INTRINSIC :: ISO_C_BINDING
	use mathconstants_f
	use saillist_f
	use fglobals_f
	use cfromf_f
        IMPLICIT NONE
        integer(C_INT), intent(in),value ::sli
        REAL(c_double),intent(out) :: total
        integer(c_int),intent(in),value ::p_DivByArea

        REAL(kind=double), allocatable, dimension(:) :: Nodal_area, angles
        REAL*8 A(3) ,zir(3), edgeangle
        INTEGER I,J,K,L, NDOD,err
        type(tri), pointer :: t 
        type(Fnode), pointer :: N
        type(sail), pointer ::s
        integer(kind=cptrsize) :: CSAILPTR	
        
       if( g_PleaseHookElements  )        return        
        s=> saillist(sli)
        CSAILPTR=getsailpointer(sli,err);

        ndod = s%nodes%ncount;
        allocate(nodal_area(ndod)) ;allocate(angles(ndod))
        Nodal_Area=0.0; angles=0;


        CALL AreaCalc(sli)
        total = 0.0D00
        edgeangle=0.0D00
 
      
LL :   DO L=1,s%tris%count
            t=>s%tris%list(L)
            if(.not. t%used) cycle

            DO K=1,3
               zir(K) =  t%m_er(k)%m_fepe%m_ZLINK0  
            ENDDO
            
            A= Tri_anglesDEAD(Zir,A)
            DO j=1,3
               I=t%N13(J)
               angles(I) = angles(I) + A(J)
               Nodal_Area(I) = Nodal_Area(I) + T%m_Area/3.0d00
            ENDDO
      ENDDO  LL
    total=0
    iloop : DO I=1,ndod
        if(i>ubound(s%nodes%xlist ,1)) write(*,*) ' (1) thats why it crashes on exit'
        n=>s%nodes%xlist(i)
        if(.not.n%m_Nused) cycle
	    if( p_DivByArea .ne. 0) then
         	IF ( IsEdgeNode(CSAILPTR,i)) THEN !  an edge
			    edgeangle = edgeangle + (PI - angles(I) ) 
            	n%S_angle =  0.0
          	ELSE
            	n%S_angle  = (2.0D00*PI - angles(I) ) 
            	total = total + n%S_angle 
               	if(nodal_Area(I) .gt. 1.0d-12) THEN
                    n%S_angle  = n%S_angle  / Nodal_Area(I)
               ! else
                !   write(outputunit,*) 'zero nodal_area' ,I,sngl(n%xx)
          	ENDIF
		    endif
	    else
           	n%S_angle  =  angles(I)   ! when getting fanned edges
	    endif
      ENDDO iloop 

       edgeangle =  2.0D00*PI - edgeangle 
!       if(p_DivByArea.ne.0) then; write(outputunit,*) 'deficits total=',sngl(total)
!      else ;	write(outputunit,*) 'vertex deficits total=',sngl(total); endif
     deallocate(nodal_area) ;deallocate(angles)
END SUBROUTINE Calc_S_Angles
      
SUBROUTINE Calc_G_Angles(sli,p_flag, total) bind(C,name="cf_calc_g_angles") !flag !=0 means divide by area
 	use fglobals_f
	use mathconstants_f
	use tris_f
	use cfromf_f
	IMPLICIT NONE
	integer(c_int), intent(in),value ::sli
	integer(c_int), intent(in),value :: p_flag
	real(c_double),intent(out) :: total

!C     This is the same as solid angles but on current lengths dec 2000
        
       REAL(kind=double), allocatable, dimension(:) :: Nodal_area, angles
       REAL*8 A(3) ,zir(3)
       INTEGER I,J,K,L , NDOD,err
        type(tri), pointer :: t 
        type(Fnode), pointer :: N
        type(sail), pointer ::s
        integer(kind=cptrsize) :: CSAILPTR	
        
        if( g_PleaseHookElements  )        return
        
        s=> saillist(sli)
        CSAILPTR=getsailpointer(sli,err)
        ndod = s%nodes%ncount;
        allocate(nodal_area(ndod)) ;allocate(angles(ndod))
        Nodal_Area=0.0; angles=0;
      
    CALL AreaCalc(sli)
    total = 0.0D00
    nodal_area  =0.0d00
    angles=0
  
  LL :   DO L=1,s%tris%count
            t=>s%tris%list(L)
            if(.not. t%used) cycle

            DO K=1,3
               zir(K) =  t%m_er(k)%m_fepe%m_ZLINK0  +  t%m_er(k)%m_fepe%m_DELTA0
            ENDDO
            
            A= Tri_anglesDEAD(Zir,A)
            DO j=1,3
               I=t%N13(J)
               angles(I) = angles(I) + A(J)
               Nodal_Area(I) = Nodal_Area(I) + T%m_Area/3.0d00
            ENDDO
      ENDDO  LL   
      
      if(p_flag .eq. 0) then
             if(ndod>ubound(s%nodes%xlist ,1)) write(*,*) ' (2) thats why it crashes on exit'
             forall( i=1:ndod)  s%nodes%xlist(i)%g_angle=angles(i)
		    write(outputunit,*) ' G angles (deformed) flag=',p_flag,' total=',total
		  	deallocate(nodal_area) ;deallocate(angles)
            return ! else divide by nodal area.
       endif
      
    iloop : DO I=1,ndod
                    if(i>ubound(s%nodes%xlist ,1)) write(*,*) ' (3) thats why it crashes on exit'
        n=>s%nodes%xlist(i)
         	IF ( IsEdgeNode(CSAILPTR,i)) THEN !  an edge
            		n%g_angle =  0.0
         !	ELSEIF (ANGLES(i) .le. 0.0D00 ) THEN
          !     		n%g_angle = 0.0d00
          	ELSE
            		n%g_angle = (2.0D00*PI - angles(I) ) 
            		total = total + n%g_angle
                	if(nodal_Area(I) .gt. 1.0d-12) THEN
                    		n%g_angle = n%g_angle / Nodal_Area(I)
			ENDIF
		endif
      ENDDO iloop 

       	write(outputunit,*) ' G angles (deformed) flag=',p_flag,' total=',total
       	deallocate(nodal_area) ;deallocate(angles)
END SUBROUTINE Calc_G_Angles

end module angcheck_f
      

