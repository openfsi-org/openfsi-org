MODULE removeElement_f
INTERFACE remove
	MODULE PROCEDURE removefnoderef
	MODULE PROCEDURE removedoforef
END INTERFACE !remove
CONTAINS


function removefnoderef(list, n) result(error)
    USE ftypes_f
	IMPLICIT NONE
	TYPE (fnoderef_p), dimension(:) ,POINTER :: list
	INTEGER, INTENT (IN) :: n
	INTEGER              :: error
	INTEGER :: istat, upr,lwr
	TYPE (fnoderef_p), dimension(Lbound(list,1):UBOUND(list,1) ) :: temp	
     error=0  
!	copy to safe elements from lbound to n-1 and from n+1 to ubound
!	deallocate list and reallocate one smaller
!	copy the saved items into it
	lwr = LBOUND(list,1) ;upr = UBOUND(list,1) 
	if(n < lwr .or. n>upr) THEN
		write(outputunit,*) ' remove element off end n =',n,' range ', lwr,upr
		upr=0/error
	ENDIF

	temp = list					
	DEALLOCATE(list,stat=istat)
	if(istat /=0) THEN
		error = istat
		return
	endif
     if(lwr>upr-1) then; 
         !write(outputunit,*) 'dont allocate nodelist ',lwr,upr-1 ;
         return	; 
     endif
   ! write(outputunit,*)'allocate fnodelist ', lwr,upr-1

	ALLOCATE( list(lwr:upr-1),stat=istat)
	if(istat /=0) THEN
		error = istat
		return
	endif
	if(n>lwr) list(:n-1) = temp(:n-1) 
	if(n<upr) list(n:) = temp(n+1:)
end function removefnoderef


function removedoforef(list, n) result(error)
    USE ftypes_f
	IMPLICIT NONE
	TYPE (doforef), dimension(:) ,POINTER :: list
	INTEGER, INTENT (IN) :: n
	INTEGER              :: error
	INTEGER :: istat, upr,lwr
	TYPE (doforef), dimension(Lbound(list,1):UBOUND(list,1) ) :: temp   
!	copy to safe elements from lbound to n-1 and from n+1 to ubound
!	deallocate list and relaoocate one smaller
!	copy the saved items into it

    error=0
	lwr = LBOUND(list,1) ;upr = UBOUND(list,1) 
	if(n < lwr .or. n>upr) THEN
		write(outputunit,*) ' remove element off end n =',n,' range ', lwr,upr
		upr=0/error
	ENDIF

	temp = list					
	DEALLOCATE(list,stat=istat)
	if(istat /=0) THEN
		error = istat
		return
	endif
	if(lwr>upr-1) then; 
	    return	;
	 endif

	ALLOCATE( list(lwr:upr-1),stat=istat)
	if(istat /=0) THEN
		error = istat
		return
	endif
	if(n>lwr) list(:n-1) = temp(:n-1) 
	if(n<upr) list(n:) = temp(n+1:)
end function removedoforef


end MODULE removeElement_f