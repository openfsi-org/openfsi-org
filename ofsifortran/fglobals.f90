MODULE  fglobals_f   ! ought to share with C - but we dont
USE, INTRINSIC :: ISO_C_BINDING
USE ftypes_f
 
IMPLICIT NONE
SAVE
    LOGICAL:: g_Gle_By_Diagonal, g_converged=.false.

    REAL*8 TOTAL_Force(3), moms(3)   ! pressure loading
    REAL*8 grav(3), windage (3)   ! inertial and windage loading
    REAL(kind=C_DOUBLE) :: g_alfa=2.8 ,g_admas =0.1

    logical :: limitV,DrawEachCycle,ImplicitSolver, RKSolver
    logical :: pleaseprint ,Windage_Each_Cycle ,dofotrace
END  MODULE fglobals_f

module tanglobals_f
implicit none

! CONTROL FLAGS for beam elements
	logical :: G_Bowing    ! an experimental flag, very unstable so
	logical :: g_BowShear  !  an experimental flag, very unstable so
	logical :: g_dxdr
	logical :: geomatrix
!	logical::  g_RelieveCompressiveStress  ! for beam elements only. Why would you??
	logical :: g_no_Qmatrix  ! may be faster at very small rotations
end module tanglobals_f

