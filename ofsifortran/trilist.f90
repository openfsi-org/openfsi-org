MODULE triList_f
USE ftypes_f
USE  linklist_f
USE  saillist_f
use basictypes_f
IMPLICIT NONE

CONTAINS
SUBROUTINE CreateFortranTri (sn, NN,ptr,err) 
USE realloc_f
USE, INTRINSIC :: ISO_C_BINDING
IMPLICIT NONE
	INTEGER , intent(in) :: sn,nn
	integer(kind=cptrsize) , intent(in) ::ptr
	INTEGER , intent(out) :: err
	TYPE ( trilist) ,POINTER :: tris
	INTEGER :: istat,k
	err = 16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return
	tris=>saillist(sn)%tris
	err=8
	if(NN < 1) return
	err = 0
!
! Normal conditions
!	just starting : 
!	NN <= count. >> set X,Y,Z used
!	NN > ubound >> realloc then set X,Y,Z used  if alloc OK. set count
!	NN > count but < Ubound  set count.set  X,Y,Z used

! error conditions
!   unable to alloc
!   NN < 0
!
	if(.not. ASSOCIATED(tris%list)) THEN

		ALLOCATE(tris%list(tris%block), stat=istat)
		if(istat /=0) THEN
			err = 1
			return
		endif
		tris%count=0
	ENDIF
	DO
		if(NN >= UBOUND(tris%list,1)) THEN
                        !write(outputunit,*) 'n tris= ', NN
			CALL reallocate(tris%list, ubound(tris%list,1) + tris%block, err)
			if(err > 0) return
		ELSE
			exit
		ENDIF
	ENDDO
		if(NN > tris%count) tris%count= NN
		forall(k=1:3) tris%list(NN)%m_er(k)%m_fepe=>NULL()
		forall(k=1:4) tris%list(NN)%nr(k)%m_fnpe=>NULL()	
		tris%list(NN)%m_eps=0.0
		tris%list(NN)%tedge=0.0
		tris%list(NN)%m_stress=0.0
		tris%list(NN)%dstfold=0.0
		tris%list(NN)%m_tnorm =0.0
		tris%list(NN)%G=0.0
		tris%list(NN)%m_ps=0.0
		tris%list(NN)%m_e0=0.0
		tris%list(NN)%m_area=0.0
		tris%list(NN)%Currentarea=0.0

		tris%list(NN)%N=NN
		tris%list(NN)%N13=0
		tris%list(NN)%edgeno=0
		tris%list(NN)%edgeno=0
		tris%list(NN)%used= .true.
		tris%list(NN)%Ctriptr = ptr
		if(.not. g_PleaseHookElements ) write(outputunit,*)' caught g_PleaseHookElements'
        g_PleaseHookElements=.true.
END SUBROUTINE CreateFortranTri 

SUBROUTINE RemoveFortranTri (sn, NN,err) 
USE realloc_f
use cfromf_f  ! for zeroptr
IMPLICIT NONE
	INTEGER , intent(in) :: sn,nn
	INTEGER , intent(out) :: err
	TYPE ( trilist) ,POINTER :: tris
	integer::k
	err = 16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return
	tris=>saillist(sn)%tris
!
!   What it does.
!	unsets the USED flag on this element. realloc the model's elist downwards if it can
!
! USUAL Conditions

!   NN =  count  >> decrement count
!    if then count < ubound-block, reallocate
!

! Unusual conditions
!   sailno out of range  >> err 16
!   trilist not associated  >>err 8
! NN < lbound or > Ubound  >>err 4
!
	err=8
	if(.not. ASSOCIATED(tris%list)) return
        err=4
	if(NN > UBOUND(tris%list,1)) return
	if(NN  <LBOUND(tris%list,1)) return
	err=0
        tris%list(NN)%m_eps=0.0
        tris%list(NN)%tedge=0.0
        tris%list(NN)%m_stress=0.0
        tris%list(NN)%dstfold=0.0
        tris%list(NN)%G=0.0
        tris%list(NN)%m_ps=0.0
        tris%list(NN)%m_e0=0.0
        tris%list(NN)%thetae_rad=0.0
        tris%list(NN)%m_area=0.0
        tris%list(NN)%prest=0.0
        tris%list(NN)%Currentarea=0.0

        tris%list(NN)%N=NN
        tris%list(NN)%N13=0
        tris%list(NN)%edgeno=0
        forall(k=1:3) tris%list(NN)%m_er(k)%m_fepe=>NULL()
        forall(k=1:4) tris%list(NN)%nr(k)%m_fnpe=>NULL()
        tris%list(NN)%used= .FALSE.
        tris%list(NN)%Ctriptr = 0

	if(NN == tris%count) THEN ! count should be the last index which is used;
dw:        do while( (.not. tris%list(tris%count)%used))
 		    tris%count =tris%count-1  
 		    if( tris%count .le. 0) exit dw   
        enddo dw
        if( tris%count+tris%block < ubound(tris%list,1) ) THEN
                CALL reallocate(tris%list, ubound(tris%list,1) - tris%block, err)
                g_PleaseHookElements=.true.
        ENDIF

	ENDIF



END SUBROUTINE RemoveFortranTri


SUBROUTINE SetTriUsed   (sn, NN,flag,err)
IMPLICIT NONE
	INTEGER , intent(in) :: sn,nn,flag
	INTEGER , intent(out) :: err
	TYPE ( trilist) ,POINTER :: tris
!
! sets the Used flag depending on the value of 'flag'
!  the usual error returns. 
!  the only twist is that if this is the last tri, we decrement count if flag=0
!  similarly, we increment count if NN is big. But only up to 
!
!	write(outputunit,*) 'SetTriUsed   ', sn, NN,flag
	err=16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	err=8
	tris=>saillist(sn)%tris
	if(.not. ASSOCIATED(tris%list)) return
	if(NN > UBOUND(tris%list,1)) return
	if(NN  <LBOUND(tris%list,1)) return
	err=0
	if(flag/=0) THEN
		if(tris%list(NN)%used) err=2
		tris%list(NN)%used=.TRUE.
		if(NN > tris%count) tris%count=NN 
	else
		if(.NOT. tris%list(NN)%used) err=1
		tris%list(NN)%used=.FALSE.
		if(NN ==  tris%count) tris%count=tris%count-1 
	endif

END SUBROUTINE SetTriUsed 

END MODULE triList_f

function PutPressure (sn, NN,p,err) result(ok) bind(C,name="cf_putpressure" )
USE ftypes_f
USE saillist_f
use mathconstants_f
IMPLICIT NONE
!  (xyz) mat be in gauss space or world space
!   ! they may refer to an increment or absolute values
!
	INTEGER  (c_int) , intent(in), value        :: sn,nn
	real(c_double), intent(in)   ,value        :: p
	
	INTEGER (c_int)  , intent(out) :: err	
	integer(c_int) :: OK

	TYPE ( trilist) ,POINTER :: tris
	type(tri), pointer ::t
    OK=0
	tris=>saillist(sn)%tris
	if(NN  > tris%count) return
	err= 0

	if(.not. tris%list(NN)%used) err=1
	if(err .eq.0) then
	     t=> tris%list(NN)
        t%prest =p
        ok=1
        endif
END function PutPressure

function GetPressure (sn, NN,err) result(p)  bind(C,name="cf_getpressure" )
USE ftypes_f
USE saillist_f
use mathconstants_f
IMPLICIT NONE
!  (xyz) mat be in gauss space or world space
!   ! they may refer to an increment or absolute values
!
	INTEGER  (c_int) , intent(in), value        :: sn,nn
	INTEGER (c_int)  , intent(out) :: err	
	real(c_double)       :: p

	TYPE ( trilist) ,POINTER :: tris
	type(tri), pointer ::t
 
	tris=>saillist(sn)%tris
	if(NN  > tris%count) return
	err= 0
    p=0.
	if(.not. tris%list(NN)%used) err=1
	if(err .eq.0) then
	     t=> tris%list(NN)
        p =  t%prest
        endif
END function GetPressure

!extern "C"  int     cf_IsTriUsed    (const int sn, const int NN, int*err) ;
function IsTriUsed (sn, NN,err) result(rv)  bind(C,name="cf_IsTriUsed" )
USE ftypes_f
USE saillist_f
use mathconstants_f
IMPLICIT NONE
!  (xyz) mat be in gauss space or world space
!   ! they may refer to an increment or absolute values
!
	INTEGER  (c_int) , intent(in), value        :: sn,nn
	INTEGER (c_int)  , intent(out) :: err	
	integer(c_int)       :: rv

	TYPE ( trilist) ,POINTER :: tris
 	rv=1;   err=99
	tris=>saillist(sn)%tris
	if(NN  > tris%count) return
	if(NN  < lbound(tris%list,1)) return
    err=0;
	if(.not. tris%list(NN)%used)  rv=0 

END function IsTriUsed

!extern "C"  double* cf_TriGlobalStressTensor (const int sn, const int NN, double*t, int*err) ;
function TriGlobalStressTensor (sn, NN,ten,err)result(ok)   bind(C,name="cf_TriGlobalStressTensor" )
USE ftypes_f
USE saillist_f
use mathconstants_f
use tris_f
use math_f
IMPLICIT NONE

    INTEGER  (c_int) , intent(in), value        :: sn,nn
    INTEGER (c_int)  , intent(out) :: err
    real (C_DOUBLE),dimension(9) ,intent(out)      :: ten
    real(c_double), dimension(3,3) :: tt, st,sg,rot
    TYPE ( trilist) ,POINTER :: tris
    type(tri), pointer ::t
    REAL(kind=double), dimension(3) :: stress,eps,princ	
    REAL(kind=double)   ::crease,PST,thet	
    integer ok
    err=1; ok=0;    ten=0   
    tris=>saillist(sn)%tris
    if(NN  > tris%count) return
    if(.not. tris%list(NN)%used) return
    err=0
    t=> tris%list(NN)
    stress=0
    ok = One_Tri_Stress_OP(NN,sn,stress,eps,princ,crease,PST)
    st=0
    st(1,1)= stress(1);  st(2,1)= stress(3);
    st(1,2)= stress(3);  st(2,2)= stress(2);

    tt= scaxismatrixByPtr(t%nr(1)%m_fnpe,t%nr(2)%m_fnpe,t%nr(3)%m_fnpe ) ! x is along first edge
    
    rot=0; 
    thet = t%THETAE_rad                          ! nov 1 2012 remove the negative
    rot(2,2) = cos(thet);   rot(1,1) = rot(2,2)
    rot(2,1) = -sin(thet); rot(1,2) = -rot(2,1); 
    rot(3,3)=1.0d00;

!1) rotate to element frame

    sg=matmul(rot,matmul(st,transpose(rot)))

!2)  transform to global

    sg=matmul(tt,matmul(sg,transpose(tt)))

    ten=pack(sg,.true.)
    ok=1;
end function TriGlobalStressTensor 

!extern "C"  double* cf_TriGlobalStrainTensor (const int sn, const int NN, double*t, int*err) ;
function TriGlobalStrainTensor (sn, NN,ten,err) result(ok) bind(C,name="cf_TriGlobalStrainTensor" )
USE ftypes_f
USE saillist_f
use mathconstants_f
use tris_f
use invert_f
use math_f
IMPLICIT NONE

    INTEGER  (c_int) , intent(in), value        :: sn,nn
    INTEGER (c_int)  , intent(out) :: err
    real (C_DOUBLE),dimension(9) ,intent(out)      :: ten
    real(c_double), dimension(3,3) :: tt, st,sg,rot, compliance
    TYPE ( trilist) ,POINTER :: tris
    type(tri), pointer ::t
    REAL(kind=double), dimension(3) :: stress,eps,princ
    REAL(kind=double)   ::crease,PST,thet
    integer ok
    logical lerr
    err=1; ok=0;    ten=0
    tris=>saillist(sn)%tris
    if(NN  > tris%count) return
    if(.not. tris%list(NN)%used) return
    err=0
    t=> tris%list(NN)
    ok = One_Tri_Stress_OP(NN,sn,stress,eps,princ,crease,PST)
    compliance = minverse_E(t%dstfold,lerr)
    if(lerr) then
        eps=0
    else
        eps = matmul(compliance, stress)
    endif

    st=0
    st(1,1)= eps(1);      st(2,1)= eps(3)/2.0; ! eps is in engineering strain units
    st(1,2)= eps(3)/2.0;  st(2,2)= eps(2);

    tt= scaxismatrixByPtr(t%nr(1)%m_fnpe,t%nr(2)%m_fnpe,t%nr(3)%m_fnpe ) ! x is along first edge

    rot=0;
    thet = t%THETAE_rad
    rot(2,2) = cos(thet);   rot(1,1) = rot(2,2)
    rot(2,1) = -sin(thet); rot(1,2) = -rot(2,1);
    rot(3,3)=1.0d00;

!1) rotate to element frame

    sg=matmul(rot,matmul(st,transpose(rot)))

!2)  transform to global

    sg=matmul(tt,matmul(sg,transpose(tt)))

    ten=pack(sg,.true.)
    ok=1;
end function TriGlobalStrainTensor

! extern "C"  double* cf_TriGlobalAxisVector (const int sn, const int NN, double*t, int*err) ;
function TriGlobalAxisVector (sn, NN,global,err)result(ok) bind(C,name="cf_TriGlobalAxisVector" )
    USE ftypes_f
    USE saillist_f
    use math_f

IMPLICIT NONE

    INTEGER  (c_int) , intent(in), value        :: sn,nn
    INTEGER (c_int)  , intent(out) :: err
    real (C_DOUBLE),dimension(3) ,intent(out)      :: global
    real(c_double), dimension(3,3) :: tt
    TYPE ( trilist) ,POINTER :: tris
    type(tri), pointer ::t
    REAL(kind=double), dimension(3) :: local
    integer ok
    err=1; ok=0;    global=0
    tris=>saillist(sn)%tris
    if(NN  > tris%count) return
    if(.not. tris%list(NN)%used) return
    err=0;     ok=1
    t=> tris%list(NN)

!  the axis is Cos(-t%THETAE_rad),Sin(-t%THETAE_rad),0

    local(1) = Cos(-t%THETAE_rad)
    local(2) = Sin(-t%THETAE_rad)
    local(3) = 0
    tt= scaxismatrixByPtr(t%nr(1)%m_fnpe,t%nr(2)%m_fnpe,t%nr(3)%m_fnpe ) ! x is along first edge

    global=matmul(tt,local)
    ok=1;
end function TriGlobalAxisVector



!extern "C"  double* cf_TriGlobalStiffnessTensor (const int sn, const int NN, double*t, int*err) ;
function TriGlobalStiffnessTensor (sn, NN,ten,err) result(ok) bind(C,name="cf_TriGlobalStiffnessTensor" )
USE ftypes_f
USE saillist_f

use tris_f
use rx_control_flags_f
use math_f
use nonlin_f

IMPLICIT NONE

    INTEGER  (c_int) , intent(in), value        :: sn,nn
    INTEGER (c_int)  , intent(out) :: err
    real (C_DOUBLE),dimension(9) ,intent(out)      :: ten
    real(c_double), dimension(3,3) :: tt, st,sg,rot
    TYPE ( trilist) ,POINTER :: tris
    type(tri), pointer ::t
    REAL(kind=double), dimension(3) :: stress,eps
    REAL(kind=double)   ::crease, thet
    integer ok
    LOGICAL c
    INTEGER state

    err=1; ok=0;    ten=0
    tris=>saillist(sn)%tris
    if(NN  > tris%count) return
    if(.not. tris%list(NN)%used) return
    err=0
    t=> tris%list(NN)

!  material properties  (= stress with a strain of (1,1,0);
    ok=1
    Eps(1)=1.0 ;       Eps(2)=1.0 ;       Eps(3)=0.0

    c = gPrestress; gPrestress=.false.
    call Get_NL_Stress_From_Tri(t,eps,stress,crease,state )
    gPrestress=c

    st=0
    st(1,1)= stress(1);      st(2,1)= stress(3) ;
    st(1,2)= stress(3);  st(2,2)= stress(2);

    tt= scaxismatrixByPtr(t%nr(1)%m_fnpe,t%nr(2)%m_fnpe,t%nr(3)%m_fnpe ) ! x is along first edge

    rot=0;
    thet = t%THETAE_rad
    rot(2,2) = cos(thet);   rot(1,1) = rot(2,2)
    rot(2,1) = -sin(thet); rot(1,2) = -rot(2,1);
    rot(3,3)=1.0d00;

!1) rotate to element frame

    sg=matmul(rot,matmul(st,transpose(rot)))

!2)  transform to global

    sg=matmul(tt,matmul(sg,transpose(tt)))

    ten=pack(sg,.true.)
    ok=1;
end function TriGlobalStiffnessTensor



function PutFtriProperties (sn, NN,n13,e,&
                        THETAE_rad, AREA, & 
                        ps,  &
                        d 	, &	 
                        creaseable, CtriPtr	,&
                        err)  result(ok)   bind(C,name="cf_putftriproperties"  )
USE ftypes_f
USE saillist_f
use mathconstants_f
use basictypes_f
IMPLICIT NONE
!  (xyz) mat be in gauss space or world space
!   ! they may refer to an increment or absolute values
!
	INTEGER  (c_int) , intent(in), value        :: sn,nn
	integer(c_int), dimension(3), intent(in)   :: n13,e
	real(c_double), intent(in)   ,value        :: THETAE_rad, AREA
	real(c_double), intent(in), dimension(3)   :: ps 
	real(c_double), intent(in), dimension(9)  :: d
	logical(c_int), intent(in) ,value          :: creaseable
	integer(kind=cptrsize) , intent(in)   , value          :: CtriPtr
	
	INTEGER (c_int)  , intent(out) :: err	
	integer(c_int) :: OK,k
	real(c_double),  dimension(3,3)  :: d3
	TYPE ( trilist) ,POINTER :: tris
	type(tri), pointer ::t
    OK=0
	err = 16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	err=8
	tris=>saillist(sn)%tris
	if(.not. ASSOCIATED(tris%list)) return
	if(NN > UBOUND(tris%list,1)) return
	if(NN  <LBOUND(tris%list,1)) return
	err=4

	if( tris%list(NN)%N /=NN) THEN
		write(outputunit,*) ' Attempt to PUT onto wrong tri',nn
	endif
	if(NN  > tris%count) return
	err= 0

	if(.not. tris%list(NN)%used) err=1
	 if(any(isnan(d))) then
		write(outputunit,*) ' NAN D in PutFtriProperties'
	endif
	 d3(1,1:3) = d(1:3)
	 d3(2,1:3) = d(4:6)
	 d3(3,1:3) = d(7:9)  
	 
	 if(err ==0) then
	     t=> tris%list(NN)
	     t%n13(1:3) = n13; t%n13(4) = n13(1)
	     t%edgeno = e
	     forall(k=1:3) t%m_er(k)%m_fepe=> saillist(sn)%edges%list(t%edgeno(k))
        t%THETAE_rad    =THETAE_rad 
        t%m_AREA =AREA
        t%m_ps  = ps 
        t%DSTFold =d3
        t%creaseable=  creaseable
        t%CtriPtr	 =CtriPtr
        t%use_tri=.true.
	 ok=1
	 endif
END function PutFtriProperties

!SUBROUTINE  GetFtriX (sn, NN,x,y,z, err) 
!USE ftypes_f
!USE fglobals_f
!USE trilist_f

!IMPLICIT NONE
!	INTEGER , intent(in) :: sn,nn
!	INTEGER , intent(out) :: err
!	REAL (kind=single) , intent(out)  :: x,y,z
!	TYPE ( trilist) ,POINTER :: tris

!	x=99;y=99; z=99;
!	err = 16
!	if(.not. associated(saillist)) return
	!if(sn > ubound(saillist,1)) return	
	!if(sn < lbound(saillist,1)) return

!	err=8
!	tris=>saillist(sn)%tris
!	if(.not. ASSOCIATED(tris%list)) return
!	if(NN > UBOUND(tris%list,1)) return
!	if(NN  <LBOUND(tris%list,1)) return

!	err=4
!	if( tris%list(NN)%N /=NN) write(outputunit,*) ' Attempt to PUT onto wrong tri',nn
!	if(NN  > tris%count) return
!
!	if(.not. tris%list(NN)%used) err=1
!	write(outputunit,*) ' GetFtriX '
!END SUBROUTINE  GetFtriX 

SUBROUTINE  ShowFtriCount(sn, u,l,c, err) 
USE ftypes_f
USE trilist_f
IMPLICIT NONE
	INTEGER , intent(in) :: sn
	INTEGER , intent(out) :: err,u,l,c

	TYPE ( trilist) ,POINTER :: tris

	err = 16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	err=8
	tris=>saillist(sn)%tris
	if(.not. ASSOCIATED(tris%list)) return
	u =UBOUND(tris%list,1)
	L  =LBOUND(tris%list,1)
	c = tris%count

	err=0

END SUBROUTINE  ShowFtriCount



SUBROUTINE  PrintFtris (sn,unit) 
USE ftypes_f
USE fglobals_f
USE trilist_f
IMPLICIT NONE
	INTEGER , intent(in) :: sn,unit

	TYPE ( trilist) ,POINTER :: tris
	type(tri),pointer ::e
	INTEGER :: i 
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return


	tris=>saillist(sn)%tris
	if(.not. ASSOCIATED(tris%list)) return

	write(unit,*) ' TriList for model ', trim(saillist(sn)%sailname)
	
	
	write(unit,'(a,\)') "i,  e%N, e%used, e%n13(1:3),e%edge,  e%creaseable, e%THETAE(rads), e%AREA, e%PREST"
  	write(unit,'(a)')	 "e%DSTFold ,    e%eps,  e%stress, e%currentarea"
	do i =1,tris%count !   UBOUND(edges%list,1)
	    e=>tris%list(i)
	    if(.not. e%used) then
		    write(unit,10) i,  e%N, e%used   
	        cycle
	    endif
	    
 !		REAL (kind=double) ,dimension(3):: eps, tedge, stress
!		REAL (kind=double) ,dimension(3:3) :: DSTFold	! unwrinkled material matrix
!		REAL (kind=double) ,dimension(3:3) :: G    		! strain from edge lengths
 !		REAL (kind=double) ,dimension(3:3) :: Tri_K 		! tedge from delta  
!		REAL (kind=double) ,dimension(3)::ps, E0
!		REAL (kind=double)  ::THETAE, AREA, PREST, CurrentArea, Ustiff
!		INTEGER,dimension(4) ::  N13
!		INTEGER,dimension(3) ::  edge			! pointer-to-edge ??
!		LOGICAL ::  starboard, creaseable,used	
!		INTEGER :: N  !  integer*4  flag to use / not use triangle	
!		INTEGER ::tribuckled, Ccount  ! count since last state change
    
 	    write(unit,10) i,  e%N, e%used, e%n13(1:3),e%edgeno,e%creaseable,    &
 	    e%THETAE_rad, e%m_AREA, e%PREST
  	    write(unit,20)	 e%DSTFold ,    e%m_eps,  e%m_stress, e%currentarea
 
	ENDDO
10  format(2i6,l5, 2(1x,3i5) L6,3G14.5,\)
20  format(	3(2x,3G14.5),5x,3(1x,3G14.5))
	write(unit,*) ' count is',tris%count

END  SUBROUTINE  PrintFtris




SUBROUTINE removeAllFTris(sn,err)
use ftypes_f
use trilist_f
use saillist_f
use basictypes_f
IMPLICIT NONE
	INTEGER , intent(in) :: sn
	INTEGER , intent(out) :: err

	integer i,SomeErrors
	TYPE ( trilist) ,POINTER :: L
	err = 16
	SomeErrors=0
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	L=>saillist(sn)%tris
	do i = L%count,1,-1
		call RemoveFortranTri (sn, i,err) 
		if(err /=0) SomeErrors = SomeErrors+1
	enddo
		if(associated(L%list)) deallocate(L%list,stat=err)
	if(SomeErrors /=0) then
		write(outputunit,*) 'model',sn,' removeFortranEdge (from remove ALL) Nerr=',SomeErrors
	endif
END SUBROUTINE removeAllFTris
