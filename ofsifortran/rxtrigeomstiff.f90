module trigeomstf_f
! see trianglerotations.nb
! We have a triangular element defined by 3 points  in R[3]
!We want to know the rotation matrix which corresponds to the 9 components of nodal displacement
!We do this by decomposing the (infinitesimal) displacements into 3 stretchings, 3 translations and 3 rotations
!For simplicity we will take the 3 stretchings being along each of the 3 edges.
!We take the rotations and translations in global axes

!We can write down the 9 displacements in terms of these 9 parameters. We solve the resulting equation system.
!
!Definitions.
!{xn,yn,zn}  position of node N
!{un,vn,wn}  displacement of node N
!NOTE if we have pressure (or other following)  loads we can use this formulation to get the stiffness due to loading too.
 contains

function tri3geomstiff(t,f) result (Kg)
use basictypes_f
use ftypes_f
use vectors_f
use invert_f
use nodalrotations_f
!use dbg, only: PrintOneMatrixForMtka
use coordinates_f,only: get_Cartesian_X_by_PTR

implicit none
    type(tri), intent(in) :: t
    real(kind=double), intent(in), dimension(9) :: f

! returns
    real(kind=double),  dimension(9,9) :: Kg

! locals
    real(kind=double),  dimension(4,3) :: v ,x
    real(kind=double),  dimension(9,9)  :: bigM,im

    integer:: i,j,k,m
    logical err
    real(kind=double),  dimension(3,3)  :: q


 xv:  associate(x1=> x(1,1:3), x2=> x(2,1:3),x3=> x(3,1:3),v1=>v(1,1:3), v2=> v(2,1:3),v3=>v(3,1:3))


 ! body

    do i=1,4
        x(i,1:3) = get_Cartesian_X_by_PTR(t%nr(i)%m_fnpe )
    enddo
    do i=1,3
        v(i,1:3) = x(i+1,1:3) -  x(i,1:3)
        v(i,1:3) =    v(i,1:3) /Norm(    v(i,1:3) )
    enddo
    if(.false.) then
    write(*,'("X1={", 2( F13.6,","),f13.6,"};" )') x1
    write(*,'("X2={", 2( F13.6,","),f13.6,"};" )') x2
    write(*,'("X3={", 2( F13.6,","),f13.6,"};" )') x3
    endif
! Stretching. Coefficients s1,s2,s3 on each of the edges V1,V2,V3

    bigM(1:3,1:3) = RESHAPE(SOURCE = (/Dot_Product( x1,v1)*v1 ,Dot_Product( x1,v2)*v2,Dot_Product( x1,v3)*v3 /), SHAPE = (/3,3/))
    bigM(4:6,1:3) = RESHAPE(SOURCE = (/Dot_Product( x2,v1)*v1 ,Dot_Product( x2,v2)*v2,Dot_Product( x2,v3)*v3 /), SHAPE = (/3,3/))
    bigM(7:9,1:3) = RESHAPE(SOURCE = (/Dot_Product( x3,v1)*v1 ,Dot_Product( x3,v2)*v2,Dot_Product( x3,v3)*v3 /), SHAPE = (/3,3/))

! rotation, small angles
    bigM(1:3,4:6) = - RESHAPE(SOURCE = (/ 0.d00, x1(3), -x1(2) ,  -x1(3), 0.d00, x1(1) ,  x1(2), -x1(1), 0.d00 /), SHAPE = (/3,3/))
    bigM(4:6,4:6) = - RESHAPE(SOURCE = (/ 0.d00, x2(3), -x2(2) ,  -x2(3), 0.d00, x2(1) ,  x2(2), -x2(1), 0.d00 /), SHAPE = (/3,3/))
    bigM(7:9,4:6) = - RESHAPE(SOURCE = (/ 0.d00, x3(3), -x3(2) ,  -x3(3), 0.d00, x3(1) ,  x3(2), -x3(1), 0.d00 /), SHAPE = (/3,3/))

! translation
    BigM(1:3,7:9) = IdentityMatrix (3)
    BigM(4:6,7:9) = IdentityMatrix (3)
    BigM(7:9,7:9) = IdentityMatrix (3)
end associate xv

 im = minverse_e(BigM,err);
 if(err)  then
        im=0.
        write(*,*) ' (*BigM inverse fails*) '
     endif



!  Now the rigid-body rotations (theta) are im(4:6,:) . (delta-coordinate )
!  so each of the 3 forces changes by eijk.F.theta

! so each set or 3 cols in the result is f(i) . QQ.  im(4:6,:)

    Kg=0.
 kga: associate(ims=>im(4:6,:))  ! k1=> Kg(1:3,:),k2=>Kg(4:6,:),k3=> Kg(7:9,:),

    do m=1,7,3
        q=0
        do i=1,3
        do j=1,3
        do k=1,3
        q(i,j) = q(i,j) + eijk(i,j,k) * f(m-1+k)
        enddo
        enddo
        enddo
        Kg(m:m+2,:) = matmul(Q,ims);
    enddo


    if(.false.) then
            write(*,'(" f={", 8( F13.6,","),f13.6,"}" )') f
            write(*,*) " Kg={ "
            do i=1,9
            write(*,'(" { ", 8( F13.6,","),f13.6,"},")') Kg(i,:)
            enddo
            write(*,*) ' }'
    endif
 end associate kga
end function tri3geomstiff


end module trigeomstf_f
