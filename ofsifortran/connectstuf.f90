
module connect_intrf_f  
interface
subroutine Resolve_Connects(c)
 use ftypes_f
        IMPLICIT NONE
        integer, intent(out)  :: c
end subroutine Resolve_Connects

subroutine UnResolve_One_Connect(c) 
 use ftypes_f
	IMPLICIT NONE
 	TYPE(connect),pointer, intent(inout)  :: c 
end subroutine UnResolve_One_Connect
subroutine Set_Connect_Relations()
end subroutine Set_Connect_Relations

end interface

end module connect_intrf_f
