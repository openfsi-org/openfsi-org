module dofoio_f
USE ftypes_f
use dofo_subtypes_f
USE  linklist_f
use vectors_f
use dofoprototypes_f
use basictypes_f

IMPLICIT NONE
integer, parameter :: dofounit = 6
integer, parameter :: dofodbgunit = 15 ! write here if flag /D1 is set
contains

!   Graph[{
!    qq \[DirectedEdge] {a, b},
!    {a, b} \[DirectedEdge] {c, d},
!    {c, d} \[DirectedEdge] qq

!}, VertexLabels -> "Name",  PlotRangePadding -> 1]

! ways of tagging the edges

!  Graph[{1 \[UndirectedEdge] 2, 2 \[UndirectedEdge] 3, Property[3 \[UndirectedEdge] 1, EdgeLabels -> "hello"]}]
! Graph[{Style[a \[DirectedEdge] b, Red],   Style[b \[DirectedEdge] c, Dashed],   Labeled[c \[DirectedEdge] a, "hello"]}]

! for style and label and thickness go
! Style[Labeled[{ 1, 5,"full"} \[DirectedEdge] { 1,23,"c"},mylabel], Red,Dashed, Thickness[2 myThickness]]
!  'None' is acceptable for mylabel


subroutine DofoWriteAllAsGraph(u)!  make a mathematica  graph of children
use parse_f
use dofoprototypes_f
    integer, intent(in) :: u
    TYPE (dofolist) :: ll
    integer:: err,i,nc,j,k
    logical starting

    character( len=256):: s1,s2,n1,n2,s3
#ifdef NO_DOFOCLASS
       type (dofo),pointer ::d1,d2   ! or class
#else
       class (dofo),pointer ::d1,d2  ! or type
#endif
    integer, pointer, dimension(:) :: nn1,nn2

    call CollectDofosToList( ll  , DOFO_EITHER, DOFO_EITHER, C_DOFO_ANY, err)  ! all
    starting=.true.
    if(ll%count<1) return
    write(u,'("myThickness=0.004;" )')
    write(u,'("dofograph:= Graph[{ " )')

    do i= 1,ll%count
    d1=>ll%list(i)%o
        if( .not. associated( d1%child)) cycle
        d2=>d1%child
        n1 = d1%m_DofoLine;        if( d1%m_DofoLine(:1) .eq. "<") n1="c"
        n2 = d2%m_DofoLine;        if( d2%m_DofoLine(:1) .eq. "<") n2="c"
        write(s1, " ( '{' ,i4, ',' , i8, ',""' , a,  '""}'  ) " ) d1%m_dofosli , d1%m_dofoN ,trim( n1)
        write(s2, " ( '{' ,i4, ',' , i8, ',""' , a,  '""}'  ) " ) d2%m_dofosli , d2%m_dofoN ,trim( n2)
        call ReplaceInString(s1," ","")
        call ReplaceInString(s2," ","")

! for the edge label s3
        nc= DOFO_commonNodes(d1,d2, nn1,nn2) ! reallocates nn1,nn2 , the indices in the dofos %m_fnodes
        if(nc>0) then
        s3=""
doj:    do j=1,nc
        if(j>3) then
        s3=trim(s3)//"(continues)"
        exit doj
        endif
          write(n1, " ( '' ,i12, '-' , i12,  ';'  ) " ) d1%m_fnodes(nn1(j))%m_fnr_Sli  ,  d1%m_fnodes(nn1(j))% m_fnrN
          call ReplaceInString(n1," ","")
          s3=trim(s3)//n1

        enddo doj
        call ReplaceInString(s3," ","")
        if(len_trim(s3)>250) s3=s3(:250)
        s3='"nn'//trim(s3)//'"'
        else
             s3="None"
        endif
        if(.not. starting) write(u,"(',')")
        write(u,'("Style[Labeled[",a,"\[DirectedEdge]",a, ",",a,"],",a,"]",\ )') trim(s1),trim(s2),trim(s3), "Red"
        starting=.false.
    enddo

    write(u, "(' }, VertexLabels->""Name"",ImagePadding->60')")
 ! here , optionally something like
 !<< ,EdgeLabels->{ {1,25,"c"} \[DirectedEdge] {1,26,"c"}->"LABEL", {1,24,"c"}\[DirectedEdge]{1,25,"c"}->"LABEL" }  >>

    write(u, "(' ];')")

 if(associated(nn1)) deallocate(nn1,  stat=k)
 if(associated(nn2)) deallocate(nn2, stat=k)
end subroutine DofoWriteAllAsGraph

subroutine PrintDepth(u,n)
     integer, intent(in) :: u,n
      integer ::k
      do k=1,n
        write(u,'("   .",$)')
      enddo
end subroutine PrintDepth


subroutine  PrintNodalDofos(theNode,unit)
use ftypes_f
 use fnoderef_f
    type(fnode), intent(in) :: TheNode
    integer, intent(in) ::unit
    real(kind=double), dimension(3) :: x
    character (len=256) ::  nodename
    integer ds,dn,j
#ifdef NO_DOFOCLASS
       type (dofo),pointer  ::d   ! or class
#else
       class (dofo),pointer ::d  ! or type
#endif
        if(.not. associated(theNode%m_pdofo )) return
        ds = 0; dn=0;
        if( thenode%masterdofoNo>0) then
            ds = thenode%m_pdofo(thenode%masterdofoNo )%d%m_dofosli
            dn = thenode%m_pdofo(thenode%masterdofoNo )%d%m_dofoN
            endif
            call PrintableNodeName(theNode,nodename)
            write(unit,'(i8,1x,a30,2x,"master=(",i3,i5,"} ndofo=",i4)',iostat=j) thenode%nn,trim(nodename),ds,dn,ubound( thenode%m_pdofo,1)
    dj:     do j=1, ubound( thenode%m_pdofo,1)
             d=> decodedoforef(thenode%m_pdofo(j))
             x = Get_DOFO_Cartesian_X( theNode,j )
             write(unit,'(22x, 2i6,1x,3(1x,f14.7),1x,a)',iostat=ds) d%m_dofosli,d%m_dofoN  ,x, trim(d%m_DofoLine)
          enddo dj
end subroutine  PrintNodalDofos


recursive function DOFO_PrintAncestors(d,u,depth) result(ok)! walk up the parent tree printing
USE  saillist_f
    integer,intent(in) :: u,depth
#ifdef NO_DOFOCLASS
       type(dofo),pointer,intent(in) ::d ! or class
#else
       class(dofo),pointer ,intent(in)::d
#endif

    integer:: ok
    logical done; done=.false.
    ok=0;
     call PrintDepth(u,depth); write(u,'(2i8,1x,a)')d%m_dofosli, d%m_dofoN,trim(d%m_DofoLine)
     if(.not. associated(d%parent1))  then
              ok = ok +   PrintOneDOFO(d,u,.false. ,depth)
              done=.true.
     else
                ok=ok+ DOFO_PrintAncestors(d%parent1,u,depth+1)
     endif
     if(.not. associated(d%parent2))  then
              if(.not. done) ok = ok +   PrintOneDOFO(d,u,.false. ,depth)
     else
                ok=ok+ DOFO_PrintAncestors(d%parent2,u,depth+1)
     endif

end function DOFO_PrintAncestors

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function DOFO_PrintTree(sli,u) result(ok)
    USE  saillist_f
    use dofoprototypes_f
        INTEGER , intent(in) :: sli,u

        TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
       type (dofo),pointer ::d1 ! or class
#else
       class (dofo),pointer ::d1 ! or type
#endif
        INTEGER :: i,ok, depth
        ok=0
        depth=0
        Dofos=>saillist(sli)%Dofos

        if(.not. ASSOCIATED(Dofos%list)) return
        return
        write(u,*) ' Dofo tree for model: ', trim(saillist(sli)%sailname)
! for each dofo which has no children
! walk up the parent tree and print its top-level parents
    ok=0

di:	do i =1, dofos%count
            if(.not. Dofos%list(i)%used) cycle;
        d1=>Dofos%list(i)%o
        if(associated(d1%child)) cycle;
       write(u,*)'&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& '
        write(u,*)'   ', i,' A dofo without children'
        ok=ok+ DOFO_PrintAncestors(d1,u,depth)
 enddo di
end function DOFO_PrintTree
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

function PrintOneDOFO(d,u,pfullprint,pdepth) result (ok)
use fnoderef_f
use rx_control_flags_f
use cfromf_f
use coordinates_f

#ifdef NO_DOFOCLASS
           type (dofo),intent(in) ::d   ! or class
#else
           class (dofo),intent(in)::d  ! or type
#endif
    integer,intent(in) ::u
    logical, intent(in) :: pfullprint
    integer, intent(in),optional :: pdepth
    integer ::ok,k,j,r,err,nr,nc,kkk,nd1  ,l_LinearFlag
    character (len=32) :: vform
    character (len=32) :: mform
     character (len=256) :: buf ,nodename
    real(kind=double), dimension(3):: xx,xNL,xGC
    real(C_DOUBLE) :: crvlen
    type(fnode), pointer ::theNode
    logical pd
    integer depth
    pd=.false.
    depth=0
    if(present(pdepth))then
     depth=pdepth
     pd=.true.
     endif
    ok=1;

    write(vform,'(a,i,a)')"(",ubound(d%jacobi,1),"g14.6)"
    write(mform,'(a,i,a)')"(",d%ndof,"g14.6)"

    if(pd) call PrintDepth(u,depth);

    write(u,'(5(a,i5),1x,\)')'(sli,dofoN) ',d%m_dofosli,' ',d%m_dofoN,' ndof=',d%ndof,' nnds=',ubound(d%m_fNodes,1),' dofotype = ',d%m_dofotype

     if(pd) call PrintDepth(u,depth)
     write(u,'(" `",a,"`",\)')  trim(d%m_DofoLine)

    if(associated(d%parent1))then
        if(pd) call PrintDepth(u,depth)
        write(u,'(a,i6,1x,i6,1x,a,$)')' parent1 is ',d%parent1%m_dofosli ,d%parent1%m_dofoN ,trim(d%parent1%m_DofoLine)
      endif
    if(associated(d%parent2))then
        if(pd) call PrintDepth(u,depth)
        write(u,'(a,i6,1x,i6,1x,a,$)')' parent2 is ',d%parent2%m_dofosli ,d%parent2%m_dofoN ,trim(d%parent2%m_DofoLine)
      endif
    if(pd) call PrintDepth(u,depth); write(u,*)' '
    if(associated(d%child))then
        if(pd) call PrintDepth(u,depth);
        write(u,'(a,i6,1x,i6,1x,a)')  '   child is ',d%child%m_dofosli ,d%child%m_dofoN ,trim(d%child%m_DofoLine)
    endif

    if(associated(d%m_Fnodes ))ok= printfnodereflist(" ", d%m_Fnodes,u,depth )

    if(pd) call PrintDepth(u,depth); write(u,*)'  nodal coords ....linear this dofo ...... diff Lin from gCXByP ......diff NL from gCXByP '
    if(d%m_dofotype .ne. C_DOFO_SlideFixedCurve) then
 doj:     do j=1,ubound(d%m_Fnodes,1)
              thenode=>decodenoderef(d%m_Fnodes(j))
              call PrintableNodeName(theNode,nodename)
              xGC=get_Cartesian_X_By_Ptr(theNode)
! non-linear evaluation on current dofo
            nd1=-1;
dok:        do k = 1,ubound( thenode%m_pdofo,1 )
                if(thenode%m_pdofo(k)%d%m_dofosli .eq. d%m_dofosli .and. thenode%m_pdofo(k)%d%m_dofoN .eq. d%m_dofoN) then
                   nd1=k
                exit dok
                endif
            enddo dok
            xnl=xgc
           l_LinearFlag = ForceLinearDofoChildren
           ForceLinearDofoChildren =0
           if(nd1>0) xnl = Get_DOFO_Cartesian_X( theNode,nd1 )
           ForceLinearDofoChildren =l_LinearFlag

! xx is linearized evaluation on current dofo
            r = d%m_Fnodes(j)%m_fnrRow
            xx=d%origin(r:r+2)
            if(associated(d%t )) then
             xx=xx+matmul(d%jacobi(r:r+2,:),d%t)
            endif
            if(pd) call PrintDepth(u,depth); write(u,'(i4,a26, 3(2x,3(1x,g14.5)) )',iostat=err) j,trim(nodename),xx,xx-xGC ,xNL-xGC
        enddo doj
    else
            thenode=>decodenoderef(d%m_Fnodes(1))
         if(pd) call PrintDepth(u,depth); write(u,'(6x,1(2x,3(1x,f14.5)) )',iostat=err) get_Cartesian_X_By_Ptr(theNode)
    endif
      select case (d%m_dofotype)
            case (C_dofo_FixedPoint )
                 if(pd) call PrintDepth(u,depth); write(u,*) '       FULL fixity type=', d%m_dofotype

           case (C_dofo_SlideFixedLine )
                 if(pd) call PrintDepth(u,depth);write(u,*) '      slideline fixity case ', d%m_dofotype
                 if(pd) call PrintDepth(u,depth); write(u,*)'      vector ',sngl(d%d1%vector)

            case ( C_DOFO_SlideFixedCurve )
                    call slidecurve_lengthto(d%d4%m_slider,d%t(1),crvlen)
                    if(pd) call PrintDepth(u,depth);
                    write(u,'(a,a,i6,a,g15.5,a,g15.5 )')'SlideonFixedCrv ',trim(d%m_DofoLine),d%m_dofoN,' t= ',d%t(1),'s= ',crvlen
                    ! lets put it onto the first node's attributes
                    thenode=>decodenoderef(d%m_Fnodes(1))
                    if(thenode%m_RXFEsiteptr .ne.0  ) then
                    write(buf,'(f15.7 )') crvlen
                    k = fc_SetSiteAttribute(thenode%m_RXFEsiteptr,'$arclength'C,trim(buf)//char(0) )
                    endif

            case (C_dofo_SlideFixedPlane)
                   if(pd) call PrintDepth(u,depth);write(u,*)'      vector ',sngl(d%d2%normal)
            case(C_DOFO_NODESPLINE_CONNECT)
                 if(pd) call PrintDepth(u,depth);write(u,*) '       Node-Spline connect ', d%m_dofotype
                 if(pd) call PrintDepth(u,depth);write(u,*) '       original Tee= ',d%d5%OriginalSlaveTee
         !        ok=d%d5%m_spl%ListFspline( u,fullprint=pfullprint)
            case(C_DOFO_STRINGSPLINE_CONNECT )
                 if(pd) call PrintDepth(u,depth);write(u,*) '       String-Spline connect ', d%m_dofotype
            !     ok= d%d5%m_spl%ListFspline(u,fullprint=.false. ) ! pfullprint)
           case(C_DOFO_COMPOSITE)
                 if(pd) call PrintDepth(u,depth);write(u,*) '     composite dofo'
            case(C_DOFO_NODENODE_CONNECT )
                 if(pd) call PrintDepth(u,depth);write(u,*) ' Node-Node connect '
             case(C_DOFO_RIGIDBODY)
                 if(pd) call PrintDepth(u,depth);write(u, '(a)')  "rigid Body :xl"
                 if(pd) call PrintDepth(u,depth);write(u, '(9x, 3g15.5 )')  d%d6%xl
                  if(pd) call PrintDepth(u,depth); !write(u, '("nTransNodes =", i6," firstrotrow=",i6)' )  d%d6%nTransNodes, d%d6%firstRotRow
                  if(pd) call PrintDepth(u,depth); write(u, '("centroid:", 3g15.5 )')  d%d6%centroidUndefl
            case default
                write(u,*) '       default fixity case ', d%m_dofotype
                k=0/0
        end select
         if(.not. pfullprint)  return


ndof:    if(d%ndof >0) then
             if(pd) call PrintDepth(u,depth);write(u,*) "       (d%t         vt          rt         origin)"
            do k=1,d%ndof
                    if(k<=ubound(d%origin ,1)) then
                         write(u,'(4g13.5)')  d%t(k),d%vt(k),d%rt(k),d%origin(k)
                    else
                        if(pd) call PrintDepth(u,depth);
                        write(u,'(3g13.5,2x,a)')  d%t(k),d%vt(k),d%rt(k),"off end";
                    endif
                 enddo
                 do k=d%ndof+1, ubound(d%origin ,1)
                 if(pd) call PrintDepth(u,depth); write(u,'(3a13,g13.5)')  '-','-','-',d%origin(k)
                 enddo
             endif ndof

        if(associated(	d%jacobi))  then
        if(ubound( d%jacobi,1)>0 .and. ubound( d%jacobi,2)>0   ) then
             write(u,*) "Jacobian, dims ",ubound( d%jacobi,1)," by ", ubound( d%jacobi,2)
                 write(u,mform) transpose(d%jacobi)
             write(u,*)
             endif
             endif
             if(associated(	d%invjacobi))  then
  !      if(ubound( d%invjacobi,1)>0 .and. ubound( d%invjacobi,2)>0   ) then
 !            write(u,*) "Inverse Jacobian, dims ",ubound( d%invjacobi,1)," by ", ubound( d%invjacobi,2)
  !               !write(u,vform) Transpose(d%invjacobi)
 !            endif
             endif
        write(u,*)
        if(d%m_dofotype==C_DOFO_NODESPLINE_CONNECT) then
             if(pd) call PrintDepth(u,depth);write(u,*) '       Node-Spline connect ', d%m_dofotype
             if(pd) call PrintDepth(u,depth);write(u,*) '       original Tee= ',d%d5%OriginalSlaveTee
             ok=d%d5%m_spl%ListFspline( u,fullprint=pfullprint)
        else if(d%m_dofotype==C_DOFO_STRINGSPLINE_CONNECT ) then
                  if(pd) call PrintDepth(u,depth);write(u,*) '       String-Spline connect ', d%m_dofotype
                  ok= d%d5%m_spl%ListFspline(u,fullprint=.false. ) ! pfullprint)
        endif
axm:     if(associated(	d%xmt))  then
             if(ubound( d%xmt,1)>0 .and. ubound( d%xmt,2)>0   ) then
                if(any(d%xmt .ne. 0)) then
                    write(u,*) "XM**************************,size=",ubound(d%xmt)
                    write(u,mform) ( d%xmt  )
                else
                    write(u,*) ' XM zero'
                endif
             endif
          endif axm
abt:      if(associated(d%bt))  then
             if(ubound( d%bt,1)>0 .and. ubound( d%bt,2)>0   ) then
                if(any(d%bt .ne. 0)) then
                    write(u,*) "BMatrix**********************,size=" ,ubound(d%bt)
                     write(u,mform,err=100) ( d%bt  )
100                  continue
                else
                    write(u,*) ' B zero'
                endif
            endif
       endif abt
 ast:     if(associated(	d%Stifft))  then
                    if(ubound( d%Stifft,1)>0 .and. ubound( d%Stifft,2)>0   ) then
                       if(any(d%Stifft .ne. 0)) then
                           write(u,*) "XM**************************,size=",ubound(d%Stifft)
                           write(u,mform) ( d%Stifft  )
                       else
                           write(u,*) ' XM zero'
                       endif
                    endif
                 endif ast

    if(allocated(d%dofosv)) then
        nr = ubound(d%dofosv,1);
        nc = ubound(d%dofosv,2);
            write(u,*) ' SV has dimensions ',nr,nc
             write(mform,'(a,i,a)')"(",nc,"g14.6)"
             do j = 1,nr
                 write(u,mform, iostat=kkk)d%dofosv(j,:)
             enddo
    endif
     if(allocated(d%m_svOrg)) then
        nr = ubound(d%m_svOrg,1); nc=1

            write(u,*) ' SV Origin  has dimensions ',nr
             write(mform,'(a,i,a)')"(",nc,"g14.6)"
             do j = 1,nr
                 write(u,mform, iostat=kkk)d%m_svOrg(j)
             enddo
    endif

end function PrintOneDOFO


 function  PrintOneDOFOForMtka(d,u ) result (ok)
 use fnoderef_f
 use cfromf_f
 use coordinates_f
#ifdef NO_DOFOCLASS
    type (dofo),intent(in) ::d   ! or class
#else
    class (dofo),intent(in)::d  ! or type
#endif
     integer,intent(in) ::u
     integer ::ok,k,j,r,err,nr,nc,kkk
     character (len=256) :: vform
     character (len=256) :: mform,nodelistform,svform,mformLeadingComma,vformLeadingComma

     real(kind=double), dimension(3):: xx

     type(fnode), pointer ::theNode
    ok=0
  !  write(u,*) '{'
    nr= ubound(d%jacobi,1)
     ok=1;
     write(vform,'("( ""{"",  ",i4,"( f30.15 ,"","") ,f30.15 ,""},""   )" )' )ubound(d%jacobi,1)-1
     write(vformLeadingComma,'("( "",{"",  ",i4,"( f30.15 ,"","") ,f30.15 ,""} ""   )" )' )ubound(d%jacobi,1)-1
     write(mform,'("( ""{"",  ",i4,"( f30.15 ,"","") ,f30.15 ,""},""   )" )' )d%ndof-1
     write(mformLeadingComma,'("( "",{"",  ",i4,"( f30.15 ,"","") ,f30.15 ,""}""   )" )' )d%ndof-1

     nodelistform = "(""{ "",2(i6,"",""), 5(f30.15,"",""),f30.15,""} "",\ )"

     write(u,'("(*",4(a,i5),1x,\)')'(sli,dofoN) ',d%m_dofosli,' ',d%m_dofoN,' ndof=',d%ndof,' dofotype = ',d%m_dofotype

      write(u,'(" `",a,"`", "*)", \)')  trim(d%m_DofoLine)
      if(associated(d%parent1))then
            write(u,'("  (* p1,p2 are *)  { ",i6, $)')  ,d%parent1%m_dofoN
            if(associated(d%parent2))then
                write(u,'("," i6,"}," )') d%parent2%m_dofoN
            else
                  write(u,'( "(*noparent2*) }," )')
            endif
       else
            write(u,'( "(*noparent1*)" )')
       endif
      if(associated(d%child))then
      write(u,'( " (* child is ",i6,1x,a," *)" )') ,d%child%m_dofoN ,trim(d%child%m_DofoLine)
      endif

      write(u,*)'(*   nodal sli,nn, coords ....linear this dofo ...... diff from  gCXByP *)'
      write(u,*)'{'
         do j=1,ubound(d%m_Fnodes,1)
             r = d%m_Fnodes(j)%m_fnrRow
             xx=d%origin(r:r+2)
             thenode=>decodenoderef(d%m_Fnodes(j))
             xx=xx+matmul(d%jacobi(r:r+2,:),d%t)
             write(u,nodelistform,iostat=err) thenode%m_sli , thenode%nn ,xx,xx-get_Cartesian_X_By_Ptr(theNode)
             if(j .ne. ubound(d%m_Fnodes,1))  write(u,'(",")')
         enddo

         write(u,*)'}'
 ndof:    if(d%ndof >0) then
                write(u,'(" (*   (d%t   ,then origin)  *)  " ) ')
              write(u,mformLeadingComma)d%t
              write(u,vformLeadingComma)d%origin

               endif ndof


      if(associated(	d%jacobi))  then
         if(ubound( d%jacobi,1)>0 .and. ubound( d%jacobi,2)>0   ) then
              write(svform,'("( ""{"",  ",i4,"( f30.15 ,"","") ,f30.15 ,""}"",\ )" )' )d%ndof-1
              write(u,*) "(*Jacobian, dims ",ubound( d%jacobi,1)," by ", ubound( d%jacobi,2),'*)'
              write(u,'( ",{" )')
                do k=1,nr
                  write(u,svform)   d%jacobi (k,:)
                   if(k<nr) write(u,*)','
                  enddo
              write(u,'( TL2,a)') '}  (*end J *)'
              endif
       endif


     if(allocated(d%dofosv)) then
         nr = ubound(d%dofosv,1);
         nc = ubound(d%dofosv,2);
         write(svform,'("( ""{"",  ",i4,"( f30.15 ,"","") ,f30.15 ,""} "",\ )" )' )nc-1
         write(u,*) '(* SV has dimensions ',nr,", ",nc,'*) ,{'
        do j = 1,nr
            write(u,svform, iostat=kkk)d%dofosv(j,1:nc)
            if(j<nr) write(u,*)','
         enddo
                write(u,*) '}  (*end SV *)'

     endif
      if(allocated(d%m_svOrg)) then
         nr = ubound(d%m_svOrg,1); nc=1
        write(mform,'("( ""{"",  ",i4,"( f30.15 ,"","") ,f30.15 ,""}""   )" )' )nr-1
         write(u,*) '(* SV Origin  has dimensions ',nr ,'*), {'
         write(u,mform, iostat=kkk)d%m_svOrg

         write(u,*) '}'
     endif
 !    axm:     if(  associated(	d%xmt))  then
 !                 if(ubound( d%xmt,1)>0 .and. ubound( d%xmt,2)>0   ) then
 !                    if(any(d%xmt .ne. 0)) then
 !                        write(u,*) "(*XM**************************,size=",ubound(d%xmt),"*) ,{"
 !                        write(u,mform, iostat=kkk) ( d%xmt  )
 !                                write(u,*) '}'
 !                    else
 !                        write(u,*) '(* XM zero*)'
 !                    endif
 !                 endif
 !              endif axm
     write(u,*) '}'
 end function PrintOneDOFOForMtka

function PrintDofoMasses(sli,unit) result (ok)
    USE  saillist_f
    use dofoprototypes_f
        INTEGER , intent(in) :: sli,unit
        TYPE ( Dofolist) ,POINTER :: Dofos

#ifdef NO_DOFOCLASS
   type (dofo),pointer ::d ! or class
#else
   class (dofo),pointer ::d
#endif
        INTEGER :: i,ok,r,c
        ok=0
        if(.not. associated(saillist)) return
        if(sli > ubound(saillist,1)) return
        if(sli < lbound(saillist,1)) return


        Dofos=>saillist(sli)%Dofos
        if(.not. ASSOCIATED(Dofos%list)) return

        write(unit,*) ' DofoMasses for model: ', trim(saillist(sli)%sailname)
    write(unit,*) '         no       ndof'
    ok=1
        do i =1, dofos%count
            if(.not. Dofos%list(i)%used) cycle;
        d=>Dofos%list(i)%o
        write(unit,'(i6,"ndof=",i6 )' ) i,d%ndof
        if(.not. associated(d%xmt)) then ;write(unit,*) ' no mass '; cycle; endif
        do c=1,d%ndof
        do r=1,d%ndof
        write(unit,'(g14.5,$ )' ) d%xmt(r,c)
        enddo
        write(unit,*) ' '
        enddo
    enddo
end function PrintDofoMasses


function  PrintdofoList(dofos,sli,unit,plong) result(ok)
    use parse_f
    use cfromf_f
    USE  saillist_f
    use dofoprototypes_f
        INTEGER , intent(in) :: sli,unit
        logical, intent(in) :: plong
        logical::verbose, starting
        TYPE ( Dofolist) ,POINTER,intent(in) :: Dofos
#ifdef NO_DOFOCLASS
           type(dofo),pointer ::d,d2 ! or class
#else
           class(dofo),pointer ::d,d2
#endif

        INTEGER :: i,k,ok,u
        character*256 dum
        CHARACTER*512 mtkafile
        dum = ' '
        mtkafile = ' '
        call getcurrentdir(dum,255);
        call denull(dum)
        write(mtkafile,'(a,a,i2.2,a)') TRIM(dum),'/mathdofos_',sli,'.txt'
        ok=0
        if(.not. associated(saillist)) return
     !   if(sli > ubound(saillist,1)) return
     !   if(sli < lbound(saillist,1)) return
       CALL CHECK_CL('VV',verbose)

         if(.not. ASSOCIATED(Dofos%list)) return

        write(unit, '(a,a,a)') 'Short DofoList for model `', trim(saillist(sli)%sailname),"`"
        write(unit,'(a)')'      no   dofono     eptr           ndof nNodes  dofotype   t(1),  IsLinear  KE    Line'
        do i =1, dofos%count
            if(.not. Dofos%list(i)%used) cycle;
        d=>Dofos%list(i)%o
        if(associated(d%t).and. size(d%t)>0 ) then
            write(unit,'(i6,i6,i,i5,i5,i5,g14.4,\ )',iostat=k) i,d%m_dofoN ,d%m_rxfptr,d%ndof,ubound(d%m_fNodes,1) ,d%m_dofotype ,d%t(1)
        else
            write(unit,'(i6,i6,i,i5,i5,i5,a14,\ )',iostat=k) i,d%m_dofoN ,d%m_rxfptr,d%ndof,ubound(d%m_fNodes,1) ,d%m_dofotype," No_T  "
        endif
        write(unit,"(2x,'LNR=',L,1x,g12.4, '<',a,'>')",iostat=k)d%IsLinear, d%ke,trim(d%m_DofoLine)
        if(d%m_dofosli /= sli) write(unit,*) ' sli disagree ', sli, d%m_dofosli
        if(associated(d%child)) then
            d2=>d%child
             write(unit,'(a,i6,i6,i6,6x,a)',iostat=k)"              child      (N,type,ndof ",d2%m_dofoN,d2%m_dofotype,d2%ndof,trim(d2%m_DofoLine)
        endif
         if(associated(d%parent1)) then
            d2=>d%parent1
             write(unit,'(a,i6,i6,i6,6x,a)',iostat=k)"              1st parent (N,type,ndof ",d2%m_dofoN,d2%m_dofotype,d2%ndof,trim(d2%m_DofoLine)
        endif
          if(associated(d%parent2)) then
            d2=>d%parent2
             write(unit,'(a,i6,i6,i6,6x,a)',iostat=k)"              2nd parent (N,type,ndof ",d2%m_dofoN,d2%m_dofotype,d2%ndof,trim(d2%m_DofoLine)
        endif
        ENDDO
        if(.not. plong ) return
        ok=ok+ DOFO_PrintTree(sli,unit)
        if(verbose) then
        write(unit,*) ;write(unit,'(a,a)') 'Long DofoList for model ', trim(saillist(sli)%sailname)
        write(unit,'(a)') '################################################################################# '
        write(unit,*) ;
        do i =1, dofos%count
                if(.not. Dofos%list(i)%used) cycle;
        d=>Dofos%list(i)%o
         write(unit,*); write(unit,'(a,i5)') 'DOFO no ',i
        k= PrintOneDOFO(d,unit,plong)
        ENDDO
        endif
     u = unit+1+sli
   open(unit=u,file= trim( mtkafile) , iostat=k)
   if(k .eq. 0) then

        write(u,*) ;write(u,'(a,a)') '(*mathematica DofoList for model ', trim(saillist(sli)%sailname)
        write(u,'(a)') '################################################################################# *)'
        write(u,*) 'dofos= {';
        starting = .true.
        do i =1, dofos%count
                if(.not. Dofos%list(i)%used) cycle;
                 d=>Dofos%list(i)%o
                 if(.not. starting)  write(u,'(",")')
                 starting= .false.
                 write(u,'(a,i5,a)') '{   (* DOFO no ',i, ' *)'
                 k= PrintOneDOFOForMtka(d,u)
        ENDDO
     write(u,*) '}';
     else
     write(*,*) 'cant open mathematica listing, unit= ',u,' ', trim ( mtkafile)
     endif
     close(u,iostat=k)
         ok=1
END  function  PrintdofoList

end module dofoio_f
