
! various small legacy routines 

      SUBROUTINE RXF_Terminate(s)
      use basictypes_f  ! for outputunit
      IMPLICIT NONE
      CHARACTER*(*) s
      write(outputunit,*)' terminate with ',s
      STOP
      END
      SUBROUTINE Mdisp(Unit,A,s,n,ND)
      IMPLICIT NONE
      INTEGER ND,n ,Unit
      REAL*8 A(ND,nd)
      CHARACTER*(*) S

      INTEGER I,J
      CHARACTER*256 string
      write(Unit,'(a,a,i3,a,i3)') S,' size ',n,' dimension ',nd

      DO I = 1,n
      	DO J=1,N
          if(abs(a(i,j)) .gt. 1.0D-11) THEN
             string(j:j)='X'  
           ELSE
           string(j:J) = ' '
          ENDIF
        ENDDo
        string(N+1:)='>'
        write(Unit,'(2H <,a,i3)') string(:n+2),i
      ENDDO
      write(Unit,*)
      END

      SUBROUTINE mprint(Unit,a,s,N,Ndim)
      IMPLICIT NONE
      INTEGER Unit ,N,NDIM
      REAL*8 a(Ndim,Ndim)

      CHARACTER*(*) s
   
 
      INTEGER r,c
      write(Unit,*) s,'  is a ',N,' by ',N, ' (dimensioned',Ndim,')'
      DO r=1,N
         write(Unit,10)(A(r,c),C=1,N)
      ENDDO
10      FORMAT(40(g13.3,1h	))
      END