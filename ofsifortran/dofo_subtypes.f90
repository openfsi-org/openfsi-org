
! modelled on ftypes.f  this 

MODULE dofo_subtypes_f
    USE, INTRINSIC :: ISO_C_BINDING
    USE  basictypes_f
	IMPLICIT NONE
        integer, parameter :: C_DOFO_ANY              = -1 ! for searching only
        integer, parameter :: C_DOFO_UNDEFINED              = 0
        integer, parameter :: C_dofo_FixedPoint             = 1
        integer, parameter :: C_dofo_SlideFixedLine         = 2
        integer, parameter :: C_dofo_SlideFixedPlane        = 3
        integer, parameter :: C_DOFO_NODENODE_CONNECT       = 4
        integer, parameter :: C_DOFO_FIXED_GLE              = 5
 ! if type <=5 dofos are born linear.
        integer, parameter :: C_DOFO_SlideFixedCurve        = 6
        integer, parameter :: C_dofo_SlideFixedSurface      = 7
        integer, parameter :: C_DOFO_RIGIDBODY              = 8 ! d6
        integer, parameter :: C_DOFO_FREE_GLE               = 9
        integer, parameter :: C_DOFO_NODESPLINE_CONNECT     = 10
        integer, parameter :: C_DOFO_STRINGSPLINE_CONNECT   = 11
        integer, parameter :: C_DOFO_COMPOSITE              = 12
!   might also Restrain to slide on a moving rigid body (curve or surface)

integer, parameter :: DOFO_EITHER = 0
integer, parameter :: DOFO_ONLY =1
integer, parameter :: DOFO_EXCEPT = 2



!values for  m_fnrFlags. See also function PrintableFlags
!   1 (bit 0) = 'this node has a non-linear relation with this dofo' flag=ior(flag,1), BTEST(0)
!   2 (bit 1) = 'this node is a slave node of this dofo'             flag=ior(flag,2), BTEST(1)
!   4 (bit 2) = 'this node is shared by this dofo'                   flag=ior(flag,4), BTEST(2)
integer, parameter :: DOFO_FLAG_NL = 0
integer, parameter :: DOFO_FLAG_SLAVE =1
integer, parameter :: DOFO_FLAG_SHARED = 2


	TYPE :: fnoderef_p ! P for persistent. It survives reallocation of the nodelist
	    integer :: m_fnr_Sli  = -1          ! sli and N refer to the Fnode
	    integer :: m_fnrN      =-1   
	    integer :: m_fnrRow    = -1         ! the first row in the jacobian
	    integer :: m_fnrFlags  = 0          ! what sort? useage is specific to the kind of DODO
	end TYPE fnoderef_p

type :: dofo_Subtype
		integer :: whatami = C_DOFO_UNDEFINED
end type dofo_Subtype
type,extends( dofo_Subtype ) :: dofo_SlideSegment
		integer :: n1=-1,n2=-1
end type dofo_SlideSegment

TYPE :: dofo_SlideLine          
    integer :: ndofs = 1
    real(kind=double), dimension(3) ::  vector
END TYPE dofo_SlideLine

TYPE :: dofo_SlidePlane ! ndofs = 2
        real(kind=double), dimension(3) ::  normal
END TYPE dofo_SlidePlane

TYPE :: dofo_SlideSurface !ndofs = 2
    integer(kind=cptrsize) :: m_connectSurf =0
    integer :: m_DOFO_Connect_Status =0  ! different name from the fnode member
END TYPE dofo_SlideSurface

TYPE :: dofo_SlideCurve !ndofs = 1
    integer(kind=cptrsize) :: m_slider 	
END TYPE dofo_SlideCurve

type :: dofo_rigidBody
    integer, allocatable, dimension(:) :: m_nn
    real(kind=double), allocatable, dimension(:,:) :: xl ! local coords of the nodes
    integer :: firstRotRow= 999999999
    integer ::nTransNodes=0
    real(kind=double),  dimension(3) :: centroidUndefl   
end type dofo_rigidBody


end MODULE  dofo_subtypes_f
