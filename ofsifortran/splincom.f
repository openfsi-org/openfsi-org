!  date: Saturday  8/1/1994
!   translated from LAHEY FFTOSTD output
!
      INTEGER NumOfspls
      PARAMETER (NumOfSpls = 4)
      REAL*8 coef(1:3,0:3,20,NumOfSpls)
      REAL*8 xkb(21,NumOfSpls)    ! S ordinates of knots
      REAL*8 start_Spl(3,NumOfSpls)   ! x,y,z of first point
      INTEGER Spl_ord(NumOfSpls)   ! number of knots
      COMMON/splines/ coef,xkb,Start_Spl,Spl_Ord
