MODULE relaxsq_f
implicit none
integer,parameter :: TRIMMASSFAILED = -99
contains


! april 00  connects instead of chains
! april 00 slope is on tcon not on log. Less weight to the small wobbles near convergence.
!C Aug 1977 tries SGI parallelising in All_Resids
!C June 1997  admas forced to be > .1 ish at startup
!C march 1997 Mass_Set_Flag is multi-valued
!C  0 = no mass set, just pressure set. 		AND run stress analysis
!C  2 =     mass set including pressure set. 		AND NO  stress analysis
!C  any other = mass set including pressure set. 	AND  run stress analysis
!
!C 12/8/96 draw resids has tmax as arg so it can Plot selectively
!C
!C 10/2/96  areacalc called on startup
!C   NB as areas come from readdat, this is ONLY to post the totals
!C
!C  transformation matrices now printed
!C no of chains now printed 
!C n lpt removed 12/3/95
!C    20/4/95 /HC works even when  analysis killed
!C   11/2/95  Debug2 now means Skip AKM_Update_Display
!C
!C 29.12.94 some minor changes to convergence management
!C      nmax node is weighted especially - but this is lost on next mass reset
!C      experiments with viscous damping. inconclusive
!C
!C  19/11/94 set up rundr for nultiple calls (eg from main X loop
!C  Mass_Set_Flag MUST be set in calling program
!C  Hard copy and Draw_Resifs ONLY if a converged solution
!C
!C   17:52 25/03/94
!*   date: 21/march 94   sort Chains added
!*   we try to sort the chain relationships into a sequence
!*   then we pass any fixities up to the top of the chain
!*
!*
!*   translated from LAHEY FFTOSTD output
!*
!* file :relaxsq.f
!* date:  Wednesday 1 December 93
!* start of RELAX 2.00 development
!* TO DO
!* square masses - for sliding fixities
!* TAN-style battens
!* output. A file of stresses for tris.
!* INput. From the builder, includes edges
!* Trim masses by eigenvalue ratio
!*
!
!* 28th November 93

!* alt-S now increases AD MAS by 0.1 with a max of 0.4
!* NB this requires a call to Define All_Masses to take effect

!* date:  Tuesday 10 August 93
!*
!* divide-by-zero testing in CalcSlope
!*
!* date:  Thursday 8 July 93
!* note a horrible heuristic just before BATTSET call.
!* If battens, increase AD MAS
!* All mass set routines are now grouped together so they may be called
!* at any time during the analysis
!* in particular, they are called if an upward trend is found
!* this (should) allow a very low AD MAS
!* date  Monday 14 June 93
!* for TTMASS8 ( mass set by formal stiffness method)
!*
!* DSTF,G,DD ,AREA are now in SAILRUN.F
!* TTMASS8.f contains a similar set of routines to tris.f (in the TAN sui
!*te)
!* the only real difference is in the include 'file names.'
!
!* NOTE the big difference from before.
!* DSTF and S are now referenced by ELEMENT, not MAterial Type.
!* They are set in Make_DSTF(), where thickness is allowed for.
!*
!* FUNCTIONAL CHANGE
!* DD, G, AREA are based on Current coords, not initial coords.
!* typical difference from before is 3*tmax
!*
!*
!CONTAINS

SUBROUTINE RUNDR(Ktot,tmax,iexit,V_Damp)

    use OMP_LIB
    use IFPORT  ! for secnds
	USE basictypes_f
        use dofolist_f
	USE connects_f
	use saillist_f
	USE gle_f
 	USE tris_f
	USE vectors_f
	USE nograph_f
 	USE nodelist_f
	USE gravity_f
	use coordinates_f
	use battenf90_f
 	use dofoprototypes_f
 	use ttmass_f
  	use cfromf_f
 	use feaprint_f
 	use solveimplicit_f
    use rx_control_flags_f
    IMPLICIT NONE

    INTEGER(C_INT),intent(out):: Iexit 		! return 0=OK, 1=not conv, 2=user abort
    INTEGER(c_int), intent(in):: Ktot		! PARAMETER. No of cycles this run
    REAL(kind=8),intent(in),volatile :: tmax	! convergence limit in Newtons
    real (kind=double) ,intent(in) , volatile:: v_damp

    logical :: dbgTurnedOff
    logical, parameter ::dbg = .false.
    integer  ::sli
    real(kind=4):: xPLOT,yPLOT

    INTEGER :: I, K,KIT,N, NOUT, nnn, ierr=0	,ok
    INTEGER :: KitLastReset ,Kitstart,KitReduceCount=400,kitLastIM !, slimax =0
    real(kind=8)::  slope,AdmasMax,alfamin , tcon,factor, EKT,  EKL
    integer ::rmaxSli=-1,rmaxN=-1
    real(kind=double), dimension(3) :: rmax

    LOGICAL :: l_damping= .FALSE. , load_worst, l_UseCurrentArea,   ZeroV = .TRUE.

    real(kind=double) , dimension(3) ::  l_acceleration, v,myvector !ttt ,

    real (kind=double) :: l_startcputime,l_thiscputime,tempdbl!rdmax,
    real*4 l_startSecTime,l_thisSecTime , drawtime,olddrawtime

    TYPE (Fnode), DIMENSION(:), POINTER :: nlist
    type(Fnode), pointer:: thenode =>NULL()

    SAVE  slope,AdmasMax,alfamin
    
     dbgTurnedOff=.false. 
#ifndef _DEBUG     
     if(NompThreads >0)	  CALL OMP_SET_NUM_THREADS(NompThreads)
#endif
    if(.not. associated(saillist) ) then
        write(*,*) 'no models!'
        iexit=0
        return
    endif
    call check_cl('RK',RKSolver)

    if(RKSolver)then
         call SolveRK(Ktot,tmax,iexit,V_Damp)
          return
     endif


    call check_cl('IM',ImplicitSolver)
    
    if(ImplicitSolver)then
         call SolveImplicit(Ktot,tmax,iexit,V_Damp) 
         if(iexit .ge.0) return  ! else drop back to explicit
         kitLastIM=500
     endif
	call check_cl('DC',DrawEachCycle) 
	call check_cl('LV',LimitV);  

	  CALL cpu_time(l_StartCpuTime)
	  l_StartSecTime = secnds(0.0);
	 
!*****************************************
      IEXIT = 0
!C  the damping flag causes current residual to be replaced by a
!C  combination of it and its previous
!C

    forall(sli=1:sailcount, is_hoisted(sli))
     saillist(sli)%active_Originally = is_active(sli)
    end forall
    k=0;
   do sli=1,sailcount
    saillist(sli)%nmax=0
    if( is_active(sli)) k=k+ saillist(sli)%nodes%ncount
    end do 
    
    KitReduceCount = 400. * (real( k)/900.  ) ** (0.75); KitReduceCount=max(KitReduceCount,300)
    
    load_worst=.FALSE.
    alfamin = 2.50
    l_acceleration = 0.0
    l_acceleration(3) = v_damp  !   TEMP for Janet
    
    CALL CHECK_CL('WE', Windage_Each_Cycle);
    CALL CHECK_CL('DA',l_Damping)
    CALL CHECK_CL('GD',g_Gle_By_Diagonal)
    CALL CHECK_CL('CA',l_UsecurrentArea)
    if(gPrestress ) l_UseCurrentArea=.true.
    
      rmaxSli=0
      NOUT= 0
      EKL=1.0D-12  ! tiny(0.0d00)
      KIT= 0
      KitLastReset= -5
      K=2                 ! for call to akm_update_display



      	 g_admas = max(g_admas,admasmin)
     
      	IF((.NOT. Force_Linear) .OR. Debug2) THEN  ! try very low values with special K
			admasmax = max(0.2,admasmin)  ! .02 was unstable try more than 0.1
	 		g_alfa =min(150.0D00,g_alfa) 
			g_admas = admasmin
     	 ELSE
			AdmasMax= max(0.5,admasmin)
			g_alfa =min(50.0D00,g_alfa) 
     	 ENDIF
     	 if(form_2006) then ! override
            alfamin = 1.6
     	 endif
  	
        g_admas = min(admasmax,g_admas)
        g_admas = max(admasmin,g_admas)
zv:     if(ZeroV.OR. (Mass_Set_Flag.ne.0)) THEN		!  First Run these need to be called after a Bring All not here
                 call  Analysis_Prep()  !BringALl has already done this
                saillist%tcon_this = 2.0 * Tmax
                zeroV = .TRUE.
                K=2                 ! for call to akm_update_display
                g_converged=.TRUE.
                NOUT=0
                tcon = 2.0 * tmax
        endif  zv

        CALL Init_Slope()

        CALL  Write_All_Flags(outputunit)

 !   forall(sli=1:sailcount,is_hoisted(sli))
 !           if(.not. is_active(sli))  All_Models_Active=.false.
 !   end forall
 
      write(outputunit,1503)
      write(outputunit,52)g_ALFA,g_admas,tmax
52     FORMAT(1H ,4(2X,G12.4))


1503   FORMAT('   Inverse     Eigenvalue          Max  ',/ &
             ,'  Timestep       Ratio       Force Imbalance')

    drawtime=0.0
        olddrawtime= secnds(drawtime)  ! timer for /DC ( = 'drawOnCycle' ) we draw every second or 10 cycles, whichever is greater

        Kitstart=Kit
        if(Mass_Set_Flag .eq.0) then
                ierr=  All_Loading(l_acceleration,psilent=.false.)
        else if(Mass_Set_Flag .eq.2) THEN
                ierr=  All_Loading(l_acceleration,psilent=.false.)

                write(outputunit,*) ' SKIP stress analysis'
                k=2
                k=check_messages(k);
                ok=HookAllElements() ! because of  check_messages
                goto 410
        endif
        call check_cl('D3',pleaseprint)
        if(pleasePrint) then
                ok=HookAllElements()
                CALL Set_Connect_Coords();
                iexit = 999
                return
        endif    
!       START OF BOTH OUTER AND INNER LOOPS
!
!*****************************************************
199     KIT=KIT+1
	if(debug1) then
            if(ZeroV) then
              write(outputunit,'(a,$)')'R'
            endif
            k = printNodalTrace(sli=1,u=outputunit,kit=kit)
	endif
 dc:    if(DrawEachCycle  ) then
            drawtime = secnds(olddrawtime)
            if(drawtime .ge. g_UpdateTime) then
                k = cf_postprocess()
                olddrawtime = secnds(0.0)
            endif
        endif dc
        if(mod(kit,10) .eq.1) then
            k=2; k=check_messages(k);
            if(k .ne. 0) ok=HookAllElements() ! because of check_messages (But I think that BringAll will have done it)
        endif
        CALL Set_Connect_Coords(); ! for grandfathering ancient types of connect

      If(Mass_Set_Flag .ne.0) THEN
			CALL Define_All_Masses();   
                        ierr=  All_Loading(l_acceleration,psilent=.false.)
			Mass_Set_Flag=0
			CALL Init_Slope()   ! experimental October 2002
      ENDIF
      call check_cl('D4',pleaseprint)
      if(pleasePrint) then
          write(outputunit,*) ' getting out on /D4'
          iexit=2
          return
    endif
       If(Windage_Each_Cycle) THEN
                        ierr=  All_Loading(l_acceleration,GNOPRINT,GWITHV,psilent=.false.)
                        !write(outputunit,'(1h+,g15.5)') ekt
      ENDIF     
      if(ZeroV) then
		do sli=1,sailcount
                        if( is_active(sli))  ok=zero_r(sli);
		enddo	
      endif

     do sli=1,sailcount
        if(.not. is_active  (sli)) cycle
  !$Odoesnt like OMP
		   do  i = 1,saillist(sli)%nodes%ncount
                saillist(sli)%nodes%xlist(i)%Rold = get_Cartesian_R(i,sli) 
                !dp = P(i,1:3)
                call Set_R(i,sli, saillist(sli)%nodes%xlist(i)%P )
		   enddo
!$OOMP END PARALLEL DO 		
      ENDdo
  
! RESIDUALS 
 
      k = DOFO_TestForCapture(kit) ! test for wrong side.  Cant test for scoot here
      if(.not. BTEST( ForceLinearDofoChildren,2) )  k = DOFO_UpdateJay( )
      CALL All_Residuals()
      k = DOFO_TestForRelease( ) 

!*  UPDATE    V     X

    EKT = 0.0d00
zv2:	IF(zeroV) THEN
        nnn=0
       ! call check_cl('M4',pleaseprint);  if(pleasePrint) then; call print_All_Masses(110);endif
		DO sli = 1,sailcount
		  if(.not. is_Active(sli)) cycle
		   	do N=1, saillist(sli)%nodes%ncount
		   		thenode=>saillist(sli)%nodes%xlist(N)
		   		if(.not. thenode%m_Nused) cycle
		   		if( IsUnderDOFOControl(thenode) ) cycle  ! slow but it sets the flag
                                v = matmul( thenode%b, get_Cartesian_R(N,sli)) * 0.5d00
				if(limitV) then
					tempdbl = sum(abs(v))
					if(tempdbl .gt. c_factor2) then
						v=v*c_factor2/tempdbl
						nnn=nnn+1 
					endif
				endif
				call increment_coord(v,thenode,sli ,k)  ! SIDEEFFECT. Modified V to the amount moved
				thenode%v=v
				if(k .ne. CONTACT_HAS_DOFO) then
			    	ekt = ekt + dot_product (thenode%v,matmul(thenode%xm ,thenode%v )) 
				endif
			END do
		ENDDO
                if(nnn>0) write(outputunit,*) '       (first pass) V limited on ',nnn,'nodes'
		N = Integrate_DOFO_Motion(starting=.true., ekt=ekt,dbg=dofotrace);

                ZeroV = .FALSE.
 		ekt = max(ekt, eps_double + ekl)  ! oct 2008 to force at least one cycle after an update.

	ELSE zv2  ! not zeroV
limv: if (limitV) then
		DO sli = 1,sailcount
		  if(.not. is_Active(sli)) cycle
		  nlist=>saillist(sli)%nodes%xlist
!$OMP PARALLEL DO PRIVATE (N,k,tempdbl,theNode) shared(nlist,sli,kit,saillist,LimitV,c_factor2) DEFAULT(none) reduction(+ : ekt) SCHEDULE(AUTO)
		   	do N=1, saillist(sli)%nodes%ncount
		   		if(.not. nlist(N)%m_Nused) cycle		
		 	   	if(IsUnderDOFOControl(nlist(N)) ) cycle
		 	   	theNode=>nlist(N)
  		 	   	nlist(N)%V =nlist(N)%V 	+ matmul(nlist(N)%B,nlist(N)%r) !SB GetcartesianR
!what about incrment-coord. If this is a contact surface node its WRONG
	 	      	ekt = ekt+  theNode%v(1)**2 * theNode%xm(1,1) + theNode%v(2)**2 * theNode%XM(2,2) &
                   + theNode%v(3)**2 *theNode%xm(3,3) &
                   + 2* theNode%xm(1,2)*theNode%v(1)*theNode%v(2) + 2*(theNode%xm(1,3)*theNode%v(1)&
                   + theNode%xm(2,3)* theNode%v(2) )*theNode%v(3)
	 	      	
				tempdbl = sum(abs(nlist(N)%v))
				if(tempdbl .gt. c_factor2) then
					nlist(N)%v=nlist(N)%v*c_factor2/tempdbl
                                endif
		 	   	call increment_coord(nlist(N)%v,nlist(N) ,sli,k)  ! SIDEEFFECT. Modified V to the amount moved
			ENDDO
!$OMP END PARALLEL DO
		ENDDO
		N = Integrate_DOFO_Motion(starting=.false.,ekt=ekt, dbg=dofotrace);	
	  else limV  ! NOT limitV
		DO sli = 1,sailcount
		  if(.not. is_Active(sli)) cycle
		  nlist=>saillist(sli)%nodes%xlist
		  nnn = saillist(sli)%nodes%ncount
!$OMP PARALLEL  PRIVATE (N,k,theNode) shared(nlist,nnn,sli) DEFAULT(none) reduction(+ : ekt)  
!$OMP DO SCHEDULE(guided)
		   	do N=1, nnn
		   	    theNode=>nlist(N)
		   		if(.not. theNode%m_Nused) cycle		
		 	   	if(IsUnderDOFOControl(theNode) ) cycle
  		 	   	theNode%V =	theNode%V 	+ matmul(theNode%B,theNode%r) !SLOW! !SB GetcartesianR
		 	   	call increment_coord(theNode%v,theNode,sli,k)  ! SIDEEFFECT. Modified V to the amount moved
kne:		 	  	if( k .ne. CONTACT_HAS_DOFO) then    ! because xm is symmetric we can say
                                  ekt = ekt &
                                  + theNode%v(1)**2 * theNode%xm(1,1) &
                                  + theNode%v(2)**2 * theNode%XM(2,2) &
                                  + theNode%v(3)**2 * theNode%xm(3,3) &
                                   + 2.0d00*( &
                                   theNode%xm(1,2)*theNode%v(1)*theNode%v(2) + &
                                   theNode%xm(1,3)*theNode%v(1)*theNode%v(3) + &
                                   theNode%xm(2,3)*theNode%v(2)*theNode%v(3)   &
                                   )
                                endif kne
			ENDDO
!$OMP END DO NOWAIT
!$OMP END PARALLEL  
		ENDDO
		N = Integrate_DOFO_Motion(starting=.false.,ekt=ekt,dbg=dofotrace);
		endif limv
	ENDIF zv2
! end integration
        call check_cl('DV',pleaseprint)
        if(pleasePrint) then	
	        do sli=1,sailcount	
	        call drawVelocity (sli) 
	        enddo 
        endif
        k = Update_All_Batten_Rotations()  ! does nothing for Kbattens
        call check_cl('D5',pleaseprint)
        if(pleasePrint) then
            write(outputunit,*) ' getting out on /D5 with kit=',kit
            ok=HookAllElements()
            CALL Set_Connect_Coords(); ZeroV = .TRUE.
            iexit=2  ! we must do a user-abort or else we'll flood the DB with residual forces.
            return
      endif 

    IF (EKT .GT. 1.0D20) THEN
        	if (Stop_Running .NE. 0) then;
        	       write(outputunit,*) ' break requested at reload (1)' ;iexit=2;
        	       goto 410
        	 endif
    endif
 
! analysis control heuristics

 heur:  IF(KIT.GT.KitLastReset+KitReduceCount) THEN  ! too slow
  	 		Factor=0.925 
 
     	 	CALL MassSet(Factor,g_alfa)
        	EKL=EKL*Factor 
       	 	EKT= EKT*Factor
      		KitLastReset=KTOT + 1  + Kitstart	! this disables a second reduction this time
      		KitLastReset= kit  ! this means another reduction after   KitReduceCount   		
            g_admas = min(g_admas,0.5D00*(g_admas+admasmin))  !only kicks in at next reset	
               ! write(outputunit,'(i6,1x,a,f12.4,a,f12.4)') kit,'drop alfa',g_alfa, ' admas to ', g_admas
	        if(   dbgTurnedOff) then;   dbgTurnedOff=.false.;  debug2=.true.; endif

      ELSE IF (EKT .GT. 1.0D28) THEN  heur ! was D14 major instability 
        	g_alfa = g_alfa *1.5
         	g_admas = min(g_admas+0.1D00,AdmasMax)
        	if(debug2) then
 			    write(outputunit,*) 'RE-LOAD and turn off wrinking (debug2)' !,limit dispPerCycle 
 			    debug2=.false.
 			    dbgTurnedOff=.true.
 			 else
        	    write(outputunit,*) 'RE-LOAD' !,limit dispPerCycle '  			    
 		    endif

        	if (Stop_Running .NE. 0) then;
        	    write(outputunit,*) ' break requested at reload(2). Set LKG and getout' ; iexit=2
		 	    DO sli = 1,sailcount
               			 if(.not. is_Active(sli)) cycle
       	     			ok= RestoreLastKnownGoodCoords(sli)
        		ENDDO
        	    goto 410
        	 endif
		    DO sli = 1,sailcount
                if(.not. is_Active(sli)) cycle
       	     	ok= RestoreLastKnownGoodCoords(sli)
        	ENDDO
         	ZeroV = .TRUE.
         	Mass_Set_Flag = 1
        	GOTO 199
      ENDIF heur
	    if(ekt .lt. 1.0D-24)  then
			write(outputunit,'(a,2g14.4)') ' ekt, ekl ',ekt,ekl 
			ekt = ekl -1.0d00
		endif
	
 ifekl:     IF(EKT .LE. EKL) THEN
!************************************************************************
!*
!
!* On the reset, we use array V for the amount the nodes are moved by
!* This is passed to Update_Rotations
      NOUT=NOUT+1
      KitLastReset=KIT

mdo1: DO sli = 1,sailcount
        saillist(sli)%tcon_this=0
        if(.not. is_Active(sli)) cycle
        if(l_UseCurrentArea) call CurrentAreaCalcBySLI(sli)
!$O M P PARALLEL DO PRIVATE (N,k,ttt) shared (fixed,V,b,Kmax,Nmax,tcon_this) default(none)
       ndo1 : DO N=1,saillist(sli)%nodes%Ncount
            	thenode=>saillist(sli)%nodes%xlist(N)
            	if(.not. thenode%m_Nused) cycle
                if(IsUnderDOFOControl(thenode)) cycle
                thenode%V = -(1.5D0*thenode%V) + matmul(thenode%b,get_Cartesian_R(n,sli))* 0.5
			if(limitV) then
				tempdbl = sum(abs(thenode%v))
				if(tempdbl .gt. c_factor2) then
					thenode%v=thenode%v*c_factor2/tempdbl
					!write(outputunit,*) '(reset) V limited by ', c_factor2/tempdbl,' on node ',N
				endif
                        endif

		        call increment_coord(thenode%V,thenode ,sli,k )  ! SIDEEFFECT. Modified V to the amount moved
            ENDDO ndo1
!$O M P END PARALLEL DO 
       

      ENDDO  mdo1
          k = Backtrack_DOFO_Motion()
          k = k+ Update_All_Batten_Rotations()
         tcon =  measureResiduals(rmaxSli,rmaxN,rmax,.false.)

	  k = Zero_All_Batten_Residuals(tmax)

	 DO sli = 1,sailcount
		  if(.not. is_Active(sli)) cycle
!!!dir$ loop count min(128) Intel 2015 doesnt like this
		  forall(n=1:saillist(sli)%nodes%Ncount) saillist(sli)%nodes%xlist(n)%Rold=0.0
         enddo
      
dbg2: if(Debug2  .and.  .false. ) then  ! turn off converged models
	 DO sli = 1,sailcount
		  if(.not. is_Active(sli)) cycle
			if( saillist(sli)%tcon_this.lt. tmax) then
				ok =set_Active(sli,.false.) 
				write(outputunit,*)' Turn OFF model ', sli	,  saillist(sli)%tcon_this,ok
			endif	
	enddo
	endif  dbg2 
!      call  gle_Export_Forces()

		xPLOT = FLOAT(KIT)    ! REAL 4
		if(tcon .lt. 1.0D-12) THEN
			Yplot = 0.0
		ELSE
			Yplot = tcon/tmax
		ENDIF
                CALL CalcSLOPE(xPLOT,yPLOT,13,slope)

                IF(Slope .GT. 1E-8)THEN ! upwards trend in tcon over 5 - 13 resets
			Factor = 1.25 ! May 2003 even more aggressive
                        g_alfa = g_alfa *Factor
			g_admas = min(g_admas+0.05D00,  AdmasMax)
			Mass_Set_Flag = 1
!                       i= fc_onReload() drawdeflected - not needed now we use a timer
		ENDIF
        if(rmaxSli>0 .and. BumperSticker ) then
        if( is_Active(rmaxSli).and.saillist(rmaxSli )%Nmax .gt.0 ) then
            theNode => saillist(rmaxSli )%nodes%xlist(saillist(rmaxSli )%Nmax );
            theNode%ErrIndex =theNode%ErrIndex +1.;
            theNode%B = thenode%B / (theNode%ErrIndex +1.) *  ( theNode%ErrIndex ) 
            theNode%xm= thenode%xm* (theNode%ErrIndex +1.) /  ( theNode%ErrIndex ) 
        Endif
        endif

401    FORMAT(2x,2(1X,I4),1X,a1,2(2X,E12.3),2i5, 3f10.3)
        CALL cpu_time(l_thisCputime); l_thisCputime=l_thisCputime- l_startCputime
        l_ThisSecTime = secnds(l_StartSecTime);
        if(rmaxSli>0) then
          if(rmaxN .ne. 0 .and. is_active(rmaxSli)) then
             myvector = get_Cartesian_X(rmaxN,rmaxSli)
             k= rmaxN
          else
             myvector = 999
             k=0
          endif
        else
          myvector = 999
          k=0
        endif 
      call drawonreset(kit,tcon/tmax,g_alfa,g_admas,k ,myvector,l_thisCpuTime,l_ThisSecTime,ekt)

      ekl = eps_double !      EKL=1.0D-15 ! tiny(0.0D00)  ! or else a perfect solution doesnt break out
      ZeroV = .TRUE.
      KITLASTRESET=KIT
      if (Stop_Running .NE. 0) then; iexit=2 ;goto 410; endif
      if(ImplicitSolver .and. (tcon .gt. tmax  ) .and. kit .gt. kitLastIM)then; write(outputunit,*) 'try IM again' ;iexit=1; goto 410; endif
      IF (tcon .gt. tmax) GOTO 199
!C we get here if, after a reset, the max residual is small
!C but we only exit if the sum of the residuals is small. This
!C only happens if an analysis fails to start because the change in 
!C loading is very small

 !iffa :    IF(dbg .and. debug1) THEN

  !      Total= 0.0/0.0D00
  !      DO sli = 1,sailcount
 !       if(.not. is_Active(sli)) cycle
  !      if( gle_count(model) .ne. 0) cycle
   !     DO N=nds(Model),nde(Model)
   !       total = total + get_ Cart esian _R(N)-P(N,1:3)
  !      ENDDO
   !     write(outputunit,*) 'model ',model,' gle count',gle_count(model) 
   !     write(outputunit,*) ' total=',sngl(total)
   !      ENDDO
 
  
   !    	if (any(abs(total(1:3)) .gt. tmax )) THEN
	!	write(outputunit,*) ' total over limit',total
   !    		GOTO 199
    !  	ENDIF

   !   ENDIF iffa

       if( dbgTurnedOff) iexit = 1  ! to force another 
      ELSE ifekl ! end of reset block
!*****************************************************************

      	EKL=EKT + 1.0D-13
      
      	IF( KIT .LT. (KTOT+ Kitstart) .AND. (Stop_Running .NE. 1)) THEN
         	GOTO 199               ! continue analysis
      	ELSE       ! time limit exceeded
        	Mass_Set_Flag = 1 ! Ph sept 2000
         	if(Stop_Running .ne. 0) then	
         	    write(outputunit,*) 'Analysis Killed by user'
            		if(Stop_Running .eq. 99) then
            	
               			iexit = 3
            		else
               			IEXIT=2
            		endif
         	else  
         		write(outputunit,*) 'Time limit exceeded',kit,ktot,kitstart
            		IEXIT=1
                endif
         	g_converged=.FALSE.
         	 goto 410   ! getout
      	ENDIF
      ENDIF  ifekl! reset block

!*       END OF INNER LOOP

410    CONTINUE

! reset the active flags. was 	active = active_Originally
    do  sli=1,sailcount
    if( is_hoisted(sli))ok=Set_Active(sli,saillist(sli)%active_Originally)
    enddo


  	CALL Set_Connect_Coords()
  
ifhc:   IF(HardCopy) THEN
            !call UpdateAllTeesBySV()  !so dofoPrint is current
            CALL  PrintGlobalStuff(60)
             k=Windage_Loads(GPRINT)
            DO sli = 1,sailcount
                if(.not. is_Active(sli)) cycle
                 CALL Print_Model_As_PC(sli )                
                 CALL Print_Model(sli ) 
                 !  CALL Print_Model_Short(sli ) 
            ENDDO
            !       CALL  r_to_af(R,tmax)
      ENDIF ifhc

    ZeroV = .FALSE.

END SUBROUTINE RUNDR 
 
SUBROUTINE MassSet(INFACTOR,p_alfa)
      use ftypes_f
      use saillist_f
      IMPLICIT NONE
 
      REAL(kind=double):: p_alfa,INfactor ,FACTOR,oldalfa
    type(fnode), pointer :: t
#ifdef NO_DOFOCLASS
       type (dofo),pointer  ::d   ! or class
#else
       class (dofo),pointer ::d  ! or type
#endif
	integer  ::sli,n
      factor = infactor ! = (alfa+(infactor-1) ) / alfa

!C march 1997 was 1.5 min factor but that seems to explode 
 
      oldalfa=p_alfa
      p_alfa=MAX (p_alfa*FACTOR, 1.60) ! was 1.75
      factor = p_alfa/oldalfa
!C      write(outputunit,*)'  massset (new alfa,factor) ', alfa,factor
      DO sli = 1,sailcount
      if(.not.is_Active(sli)) cycle
      	do n=1,saillist(sli)%nodes%Ncount
      	    t=> saillist(sli)%nodes%xlist(n)
		    t%xm =  t%xm * Factor
		    t%B  =  t%B  / Factor
	    end do

      	do n=1,saillist(sli)%dofos%count
      	    if(.not. saillist(sli)%dofos%list(n)%used) cycle;
      	    d=> saillist(sli)%dofos%list(n)%o
        	if(.not. associated(d%xmt) ) cycle   
        	if(.not. associated(d%bt) ) cycle  	    
		    d%xmt =  d%xmt * Factor
		    d%Bt  =  d%Bt  / Factor
	    end do

       ENDDO
END SUBROUTINE MassSet
 

SUBROUTINE CalcSLOPE(Xnew,Ynew,N,slope)
      use basictypes_f  ! for outputunit
         IMPLICIT NONE
!*
!*      This routine maintains a running regression analysis of Y against
!*      over N points
!* running variables are initialised in Init_slope()
!*
      real(kind=4):: Xnew,Ynew
      REAL(kind=8),intent(out) :: Slope
      integer N ,Ntotal
      REAL(kind=8)::SumX,SumY,SumXX,SumYY,SumXY,Number
      REAL(kind=8):: Xrun(20),Yrun(20)
      INTEGER INDEX 
      COMMON/slopecom/ Index,Ntotal,SumX,SumY,SumXX,SumYY,SumXY,Number,Xrun,Yrun

      REAL(kind=8):: XMAX,XMIN,tconMAX,tconMIN,EKLMAX,EKLMIN,lastekl,lasttcon
      real(kind=8):: lastX1,lastX2, X1,Y2,XLEN,YLEN2,Y1,Ylen1,DEnom
      COMMON/PLOTTING/XMAX,XMIN,tconMAX,tconMIN,EKLMAX,EKLMIN,lastekl,lasttcon, lastX1,lastX2,x1,y1,y2,xlen,ylen1,ylen2

      Index=Index+1
      IF(Ntotal.lt. N) Ntotal=Ntotal+1
      IF (index .gt.N) Index=index-N

        Xrun(index)=Xnew
        Yrun(index)=Ynew
        SumX=SUM( Xrun(1:Ntotal))
        SumY=SUM( Yrun(1:Ntotal))
        SumXX=SUM( Xrun(1:Ntotal) **2 ) 
        SumYY=SUM( Yrun(1:Ntotal) **2 ) 
        SumXY=	SUM(  Xrun(1:Ntotal) * Yrun(1:Ntotal) )



      if(ntotal .lt. 2)then
!	write(outputunit,*) 'slope zero. not enough points ' 
     	  Slope = 0.0D00
     	 return ! not enough points
      endif
      number=float(ntotal)

      denom = SumXX * Number-SumX**2
      if (ABS (denom) .lt. 1.0E-7) THEN
            write(outputunit,*) 'denom small' ,denom
            Slope = 0.0d00
            return
      ENDIF
      slope=  (number*SumXY -SumX*SumY) /denom

      IF (abs(slope) .lt. 0.0D-07) THEN
            write(outputunit,*) 'slope very small ',slope
            slope = 0.0d00
            return
      ENDIF
 	 if(Ntotal .lt.5) then
!		write(outputunit,*) Ntotal, N
!		write(outputunit,*) slope
		slope=0.0d00 ! SLOPE zero if not 5 resets alre
	endif
	if(slope .gt. 1.) then
!		write(outputunit,'(13f9.1)') xrun(1:Ntotal)
!		write(outputunit,'(13g9.2)') yrun(1:Ntotal)
!		write(outputunit,*) 'ntotal set to 0'
		Ntotal=0
	endif

END SUBROUTINE CalcSLOPE

SUBROUTINE Init_Slope()   ! Linux found a common block size mismatch here
      IMPLICIT NONE

      integer Ntotal
      REAL(kind=8):: SumX,SumY,SumXX,SumYY,SumXY,Number
      REAL(kind=8):: Xrun(20),Yrun(20)
      INTEGER INDEX
      COMMON/slopecom/ Index,Ntotal,SumX,SumY,SumXX,SumYY,SumXY,Number,Xrun,Yrun
 
 
      Number=0.0
      SumX=0.0
      SumY=0.0
      SumXX=0.0
      SumYY=0.0
      SumXY=0.0

      Ntotal=0

      Xrun =0.0
      Yrun =0.0
 
      Index=0
END SUBROUTINE Init_Slope


function Trim_Mass( M,p_admas) result(nchanged)
	  use math_f
	  use basictypes_f  ! for outputunit
      IMPLICIT NONE
	integer :: nchanged
      REAL(kind=8):: M(3,3)
      real(kind=8),intent(in) :: p_admas

      real(kind=8):: Lhigh,LLow, aa,bb 
      REAL(kind=8):: V(3,4), v_0(3,4)
      integer i 
	nchanged = 0
	if(any(isnan(M))) then;
	    write(outputunit,*) ' NAN mass in TrimMass'; 
	    write(outputunit,*) sngl(m);
	    m = 0.
	    nchanged=TRIMMASSFAILED
	    return 
	endif
      CALL EigenVectors(M,V)
      Lhigh = max(v(1,4),v(2,4),v(3,4))
      LLow  = min(v(1,4),v(2,4),v(3,4))

      if( (LLow .lt. LHigh * p_admas)  .AND. (Llow .ne. LHigh)) THEN
            bb = (p_admas*LHigh - LLow)/(p_admas-1.0)
            aa = LHigh* (p_admas-1.0)/(LLow-LHigh)
            DO I=1,3
                M(i,1:3) = aa*M(i,1:3)
                M(i,i) = M(i,i) - bb*aa
            ENDDO
            nchanged = nchanged + 1
      ENDIF

#ifndef WINTEST  ! else verify that all eigenvalues >= vmax*admas
	return
#endif
    v_0=v  ! 
    CALL EigenVectors(M,V)
   Lhigh = max(v(1,4),v(2,4),v(3,4))
   LLow  = min(v(1,4),v(2,4),v(3,4))
   if( (LLow .lt. LHigh * p_admas* ( 0.99999D00) )  .AND. (Llow .ne. LHigh)) THEN
      	write(outputunit,*) 'three eigenvalues ', v_0(1,4),v_0(2,4),v_0(3,4)
      	write(outputunit,*) 'After  trimming   ', v(1,4),v(2,4),v(3,4)
      	write(outputunit, *) ' Lowest eigenvalue is below admas x highest', llow,lhigh
	    write(outputunit,*) ' admas is ',p_admas
	endif

END function Trim_Mass

 END MODULE relaxsq_f  

SUBROUTINE Define_All_Masses() 
    use relaxsq_f 
	use connects_f
	use gle_f
        use tris_f !,only:make_DD_And_G, currentareacalcbysli
	use ttmass_f
	use kbatten_f
	use batten_f
	use stringlist_f
	use saillist_f
	use links_pure_f
	use battenf90_f
	use math_f
	use fbeamList_f,only:MakeAllBeamMasses
	use invert_f
	use dofoprototypes_f
	use flinkList_f
	use feaprint_f
	use solveimplicit_f
    use rx_control_flags_f
    USE basictypes_f
      IMPLICIT NONE
!*locals
      INTEGER  k,N,sli
      integer :: nchanged,iret 
      LOGICAL error, wsafe,  l_UseCurrentArea
      type(sail), pointer ::s
     
!* Wrinkling Not allowed by default.
!* switch /D2 turns it on
!* If this is NOT turned off, a further switch WK turns ON
!* allowance for wrinkling in the calculation of the stiffness matrix.

!* NOTE  g_Wrinkling_Allowed doesnt do anything. debug2 is the wrinkling flag

        CALL CHECK_CL('CA',l_UsecurrentArea)
        if(gPrestress ) l_UseCurrentArea=.true.
      wsafe = Debug2
      Debug2=K_on_Wrinkled_Matrix
do_1: DO sli = 1,sailcount
ifa1:     if(is_Active(sli)) THEN
                s=>saillist(sli)
!!!dir$ loop count min(32)  Intel 2015 doesnt like this
                forall (n=1:saillist(sli)%nodes%Ncount)
                    saillist(sli)%nodes%xlist(n)%XM = 0.0d00
                end forall
              if(l_UseCurrentArea) call CurrentAreaCalcBySLI(sli)
                CALL Make_Current_Edge_Lengths(sli) !this is in DefineAllMasses
                CALL make_DD_and_G(sli)  ! uses   call check_cl('GC',UseCurrentGeom)

                CALL TTMASS9(s)   ! adds XM due to tris
                if(Mass_By_Test) then
                    write(outputunit,*) 'mass by test';CALL Make_Current_Edge_Lengths(sli)
                 endif

                k= FMake_All_Batten_Matrices(sli)
                CALL CS_String_Masses(sli,StringCompAllowedFlag)
                k = k+MakeAllLinkMasses(sli,StringCompAllowedFlag) ! are you sure???????????
                k=  k+MakeAllBeamMasses(sli)

                call check_cl('M1',pleaseprint)
                if(pleasePrint) then; write(outputunit,*) ' raw masses on unit 100'; 
                call print_All_Masses(100,sli);endif
                
                if(dm) THEN   !   diagonalise XM
!dir$ loop count min(512)
                    do  n=1,saillist(sli)%nodes%Ncount
                    if(IsUnderDOFOControl( saillist(sli)%nodes%xlist(n) )) cycle
                    CALL Diagonalise3(saillist(sli)%nodes%xlist(n)%XM )
                    end do             
                ENDIF
            ENDIF ifa1
      ENDDO do_1

 	CALL gle_masses()
    CALL TransferConnectMasses() ! obs. just does old-style non-dofo string connects


!  Trimming the masses
!  modify them st the lowest eigenvalue is >= admas times the highest
! then multiply by g_alfa/2
!  BumperSticker (/BS) slows down nodes which once were the most active.
! in the special case of contact nodes, the mass becomes isotropic (all eigenvalues equal to the highest)

  nchanged = 0
  DO sli = 1,sailcount
    if(is_Active(sli)) THEN
       	do  n=1,saillist(sli)%nodes%Ncount
       	    iret =  Trim_Mass(saillist(sli)%nodes%xlist(n)%XM,g_admas)
       	    if(iret .eq. TRIMMASSFAILED) then
       	         saillist(sli)%nodes%xlist(n)%XM=0
       	         saillist(sli)%nodes%xlist(n)%B=0  
       	     else     	     
        	    nchanged = nchanged + iret
        	    if(saillist(sli)%nodes%xlist(n)%m_connectSurf.ne.0) then  ! to avoid thinking about a R going IN but a V going out
        	      nchanged = nchanged +  Make_Mass_Isotropic(saillist(sli)%nodes%xlist(n)%XM)  
        	      !  write(outputunit,*)' make mass iso for connectsurf nodes'
        	    endif
                    saillist(sli)%nodes%xlist(n)%XM=saillist(sli)%nodes%xlist(n)%XM* g_alfa*0.5d00
      		    CALL INVERT3(saillist(sli)%nodes%xlist(n)%XM,saillist(sli)%nodes%xlist(n)%B,error)
      		    if(error) then
      		   ! write(outputunit,*) ' cannot invert mass sli = ',sli,' n=',n
      		    endif
      		    if(BumperSticker) then; 
      		    saillist(sli)%nodes%xlist(n)%B = saillist(sli)%nodes%xlist(n)%B /(1.+ saillist(sli)%nodes%xlist(n)%ErrIndex)
      		    saillist(sli)%nodes%xlist(n)%xm= saillist(sli)%nodes%xlist(n)%xm*(1.+ saillist(sli)%nodes%xlist(n)%ErrIndex)
      		     endif
      		endif
      	ENDDO
       ENDIF
      ENDDO

 ! SetConnectB_Matrix is also for old-style string connects only
 ! CALL SetConnectB_Matrix()

     Debug2 =wsafe
      
      n= define_All_DOFO_masses()
      call check_cl('M3',pleaseprint)
      if(pleasePrint) then;write(outputunit,*) 'final masses on unit 103'; call print_All_Masses(103);endif
                
END SUBROUTINE Define_All_Masses 

