MODULE betype_f
USE basictypes_f
use ftypes_f



	integer, parameter ::	BEAM_TOO_SHORT		= -1
	integer, parameter ::	BEAM_INCONSISTENT	= -2
	integer, parameter ::	BEAM_OK				= 1

contains
elemental subroutine Empty_BE(be)
type(beamelement) ,intent(inout):: be
		be%IsUpright=.false. ; be%IsBar	=.false. 

		  be%L =0; be%n3=0 ; be%npo=0
		  be%n12=0

		 be%zt =0.0d00 ; be%zi =0.0d00 ; be%tq =0.0d00 ; be%ti =0.0d00 ; be%beta1 =0.0d00
		 be%ttu =0.0d00
		 be%emp =0.0d00

		be%xN1=>NULL()  ; be%xN2=>NULL() ; be%tn1=>NULL() ; be%tn2 =>NULL()

		be%m_part_name=" "


end subroutine Empty_BE


END MODULE betype_f