!   date: Saturday  8/1/1994
!   translated from LAHEY FFTOSTD output
!  10/1/96 write
! 24/10/95 infinite loop avoided via tenloopcount
!
! date:  Wednesday 4 August 1993
! in make_Coefs and TwoD_Spline_Init C(0:50) not, as before, C(0:30)

!       file: bspline.f
!       DATE:  Thursday 14 September 1989
!
! These are the two-D and three-D Bspline routines
      BLOCK DATA BSPCOMINIT

      include 'splincom.f'

      DATA COEF/960*0.000/ ! depends on NumOfSpls
      DATA XKB/84*0.0/
      DATA start_spl/12*0.00/
      DATA spl_ord/4*0/

      END


      LOGICAL FUNCTION OKSPLINE(sn, factor)
      IMPLICIT NONE
      INTEGER sn                              ! spline number
! 
      include 'splincom.f'
      REAL*8 factor

      INTEGER I ,axis
      REAL*8 H, thresh, diff

      h = xkb(2,sn) - xkb(1,sn)
      thresh = factor / H**2
      OKspline = .TRUE.
      DO 10 axis = 1,3
      DO 10 i=1,SPl_Ord(sn) -1
      diff =  ( coef(axis,3,i+1,sn) - coef(axis,3,I,sn) )
      IF (abs(diff) .gt. thresh)      THEN
          Okspline = .false.
        !  write(outputunit,*) " Spline Wavy!!"
          OKspline = .TRUE.
!	Above is a temp fix 10 aug 94 
      RETURN
      ENDIF
10      CONTINUE
      END ! of OKSpline



      SUBROUTINE TwoD_Spline_Init(Xg,Yg,Np, ord,C,new,failed)
      use basictypes_f  ! for outputunit
      IMPLICIT NONE
!               generates the coefs C(*) for use with fnctn BSPLINE
!               NOTE C is now 0:50
      INTEGER,intent(inout) :: Np                   ! number of given points
      REAL*8,intent(in),dimension(np):: xg,Yg
      REAL*8 ,intent(out)::  C(0:50)

      INTEGER,intent(in) :: Ord                     ! the number of Base fncts
      LOGICAL New
! true if we need to build the matrix
      LOGICAL Failed          ! returned TRUE if this routine fails

!       bspline COMMN BLOCK
! ktchanged 25/08/92 include 'filename'
      include 'bspcom.f'

!       The knot spacing is (chord / (order-1) )

!       Local Variables
      REAL*8  a(30,30),y(30) , Csolve(30)
      REAL*8 Bnorm, Ynorm             ! fctns
      INTEGER I,J
      LOGICAL SolveError

        SAVE a	! Peter Aug 97 YUCH

      if (nP .GT. Max_Ngivens) THEN
      	write(outputunit,*)' spline truncated ',Np, ' to ', Max_Ngivens
      	Np = Max_Ngivens
      ENDIF

      Failed = .FALSE.
      order = ord

      H = ( xg(np) - xg(1) )/ float(order-1)

      Ngiven = Np
      	DO  i=1,Np
     		Xgiven(I) = Xg(I)
       		Ygiven(I) = Yg(I)  ! used in Ynorm
	enddo

      	DO i=0,order+2
      		xk(i+1) = float(I) * H + xgiven(1)
	enddo


      DO i=1,order
      		Y(I) = Ynorm(I)
      		IF(new) THEN
      			DO  J = 1,I
      				A(I,J) = Bnorm(I,J)
                		A(j,I) = A(i,j)
			enddo
      		ENDIF
	enddo

!       call matrix_analyse(A,30,order)

      CALL BandSOLVE(order,A,Y,Csolve,30,3,solveError) ! Z solution
      IF(solveError) THEN
      	Failed = .TRUE.
      	RETURN
      ENDIF


      	DO  j=1,order
	     C(j) = Csolve(J)
	enddo

      C(order+1) = 0.0d00

!       our spline is now sigma over ORDER of C(J) Bspline(X,J)
      END ! of TwoD_Spline_Init

      REAL*8 FUNCTION TwoD_Spline_Eval(X,C)
      IMPLICIT NONE
      REAL*8 X                ! an ordinate
      REAL*8 C(0:50)  ! coefficients
!       returns the Y value of the spline at X based on C(*) and fnctn B
!SPLINE

! ktchanged 25/08/92 include 'filename'
      include 'bspcom.f'
      REAL*8 Bspline
      INTEGER I

      TwoD_Spline_Eval = 0.0d00

      DO 10 i = 1,order
10      TwoD_Spline_Eval = TwoD_Spline_Eval + C(I) * Bspline(X,I)
      END     ! of TwoD_Spline_Eval

      SUBROUTINE Make_Coefs(C,sn,axis)
      IMPLICIT NONE
!               Takes the coefs C (weights for each Bspline)
!               Converts these into weights for each Base_spline
!               and makes a set of coefficients COEF.
!               These coefs are intended for use multiplying powers of(x
!-xk)
!               where xk is the knot just underneath this segment.


      REAL*8 C(0:50)
      REAL*8 Hsq,Hs3 ,CI_1

      INTEGER axis,sn

! ktchanged 25/08/92 include 'filename'
      include 'bspcom.f'
      include 'splincom.f'
      INTEGER I
      EXTERNAL BSPCOMINIT

!
!convert the coefs to coefs of base_splines

      C(0) = C(1)
      C(1) = 0.5d00*( C(1) + C(2) )

      C(order+1) = C(order)
      C(order) = 0.5d00* ( C(order) + C(order-1))


      DO 10 i=1,order-1
      CI_1 = C(i-1)

      coef(axis,0,i,sn) = CI_1 + 4.0*c(I)  + 1.0*c(i+1) + 0.0

      coef(axis,1,i,sn) = CI_1*(-3.0) + c(I)*(0.0) + c(I+1)*(3.) + c(I+2)*0.0
      coef(axis,1,i,sn) = coef(axis,1,i,sn) /h
      Hsq = H **2
      Coef(axis,2,i,sn) = CI_1*(3.) + C(I)*(-6.) + C(I+1)*3. + C(I+2)*0.00
      coef(axis,2,i,sn) = coef(axis,2,i,sn) /hsq
      hs3 = Hsq * H
      COEF(axis,3,i,sn) = CI_1*(-1.) + C(I)*(3.) + C(I+1)*(-3.) + C(I+2)*1.
      COEF(axis,3,i,sn) = coef(axis,3,i,sn)/hs3

10      xkb(i,sn) = xk(I)

      xkb(order,sn) = xk(order)
      Spl_Ord(sn)  = order
      END ! of Make_Coefs



      REAL*8 FUNCTION Bspline(X,I)
      IMPLICIT NONE
      REAL*8 X
      INTEGER I
      REAL*8 Base_Bspline
! ktchanged 25/08/92 include 'filename'
      include 'bspcom.f'

      IF (i.eq. 1) THEN
      Bspline = 0.5d00* Base_Bspline(X,1) + Base_Bspline(X,0)
      ELSE IF (i.eq. 2) THEN
      Bspline = 0.5d00* Base_Bspline(X,1) + Base_Bspline(X,2)
      ELSE IF (i .eq. order) THEN
      Bspline = 0.5d00* Base_Bspline(X,I) + Base_Bspline(X,I+1)
      ELSE IF (i .eq. order-1 ) THEN
      Bspline = 0.5d00* Base_Bspline(X,order) + Base_Bspline(X,I)
      ELSE

      Bspline = Base_Bspline(X,I)
      ENDIF
      END ! of Bspline



      REAL*8 FUNCTION Base_Bspline(X,I)
      IMPLICIT NONE
!       evaluates the base function I at X
      REAL*8 X
      INTEGER I
! ktchanged 25/08/92 include 'filename'
      include 'bspcom.f'
      REAL*8 dx, xpeak, TwoH

      IF(i .ne.0) THEN
      Xpeak = xk(I)
      ELSE
      Xpeak = xk(1) - H
      ENDIF
      TwoH = 2.0d00* H


      IF (abs (X - Xpeak)  .GE.  TwoH) THEN
      Base_Bspline = 0.0d00
      RETURN
      ELSEIF ( X .lt. (Xpeak - H)) THEN
      Base_Bspline = ( (X - (Xpeak - TwoH))/H) **3
      RETURN
      ELSEIF ( X .lt. Xpeak ) THEN
      dx = ( X - (Xpeak - H) ) /H
      Base_Bspline = 1.0d00+ 3.00d00*dx *( 1.0d00 + dx*( 1.0d00 - dx))
      RETURN
      ELSEIF ( X .lt. ( Xpeak+ H) ) THEN
      dx = ( Xpeak + H - X) /H
      Base_Bspline = 1.0d00 + dx* 3.0d00*( 1.0d00 + dx*( 1.0d00 - dx ))
     &
      RETURN
      ELSE                    !   IF( X .lt. (Xpeak +TwoH ) ) THEN
      Base_Bspline = ( ((Xpeak+2.0d00*H) - X ) / H )**3
!       ELSE
!               Base_Bspline = 0.0d00
      ENDIF
      END ! of Base_Bspline


      REAL*8 FUNCTION BNORM(I,J)
      IMPLICIT NONE
      INTEGER I,J
! ktchanged 25/08/92 include 'filename'
      include 'bspcom.f'

      INTEGER K
      REAL*8 Bspline   ,b1
      Bnorm = 0.0d00
      DO 10 K=1,Ngiven
      b1 = Bspline(Xgiven(K),I)
      if (b1 .GT. 0.0D00) then
      IF ( I .eq. J) THEN
      Bnorm = Bnorm + B1*b1
      ELSE
      Bnorm = Bnorm + b1 * Bspline (Xgiven(K), J)
      ENDIF
      endif           !ELSE dont bother with the second evaluation
10      continue
      END     ! of Bnorm

      REAL*8 FUNCTION YNORM(I)
      IMPLICIT NONE
      INTEGER I
! ktchanged 25/08/92 include 'filename'
      include 'bspcom.f'
      REAL*8 Bspline
      INTEGER K

      Ynorm = 0.0d00
      DO 10 K=1,Ngiven
      if(Ygiven(K) .ne. 0.0) THEN
      Ynorm = Ynorm + Bspline(Xgiven(K),I) * Ygiven(K)
      ENDIF
10      CONTINUE
      END     ! of Ynorm


! file: Bspline3.f
! DATE  Thursday 7 September 1989

!       contains the 3-D spline routines
!       link with bspline2 and SOLVE.FOR  and VECTORS

!       the two-D routines work with Np = order = 2
!       the three-D routines require Np > 3, and Order>3

      SUBROUTINE ThreeD_Spline_init(xg,yg,zg,ng,order,sn,chord,noSPline)
      IMPLICIT NONE

      REAL*8 xg(*),yg(*), Zg(*)               ! the points
      INTEGER Ng
! number of pts
      INTEGER Order
! the number of B splines
      INTEGER Sn
! spline number
      REAL*8 chord
      LOGICAL NoSPline
! TRUE if the routine failed

!       the order of the curve is reduced if the spline is too wavy

      INTEGER Max_Ngivens
      PARAMETER (Max_Ngivens = 150)
!LOCALS
      REAL*8 s(Max_Ngivens ), C(0:50), change, smeasured(Max_Ngivens)
      REAL*8 :: tol = .001
      INTEGER  I,count,tenloopcount
      REAL*8 :: Factor = 2.0
      LOGICAL failed, OKSpline
! a fnctn

      NoSpline = .FALSE.
1000    CONTINUE
        if(Ng .gt. Max_Ngivens) THEN
		Nospline = .true.
     		RETURN
         ENDIF

      CALL Arc_Approx(xg,yg,zg,ng,S)

      tol = 0.01 * S(ng) / float(order-1)

      CALL start_spline(xg(1),yg(1),zg(1),sn)
      count = 0
! start of loop
          tenloopcount=0
10	  CONTINUE
        tenloopcount= tenloopcount + 1
        CALL TwoD_Spline_Init (S,xg,ng,order,C,.TRUE.,failed) ! returns C , weights for each spline
f1:      IF (.NOT. Failed) THEN
            CALL Make_Coefs(C,sn,1)         ! axis 2 on spline 1
            CALL TwoD_Spline_Init (S,yg,ng,order,C,.FALSE.,failed)! returns C , weights for each spline
f2:         IF(.NOT. Failed) THEN
                CALL Make_Coefs(C,sn,2)         ! axis 2 on spline sn
                CALL TwoD_Spline_Init (S,zg,ng,order,C,.FALSE.,failed)! returns C , weights for each spline
f3:             IF (.not. Failed) THEN
                    CALL Make_Coefs(C,sn,3)         ! axis 3 on spline sn
                    Change = 0.0
                    CALL Measure_Arc(S,Smeasured,ng,sn)! returns arc lengths at each S(i)
                    DO  i=1,ng
                         change = max(change,abs(Smeasured(I)-s(I)) )
                    enddo
ic:                 IF ((change .gt. tol) .and. (  tenloopcount .lt. 50 )) THEN
                        DO  i= 1, ng
                           S(I) = Smeasured(I)
                        ENDDO
                        count = count + 1
                        IF(count .lt. 50) THEN
                          GOTO 10
                        ELSe
                          NoSpline = .TRUE.
                          RETURN
                        ENDIF
                    ENDIF ic
                    chord = S(ng)
                    IF (.not. OKSpline(sn,factor) ) THEN
                      Order = Order - 1
                      if(order .lt. 4) THEN
                          NoSPline = .true.
                          RETURN
                      ELSE
                         GOTO 1000
                      ENDIF
                    ELSE
                      RETURN          ! ok
                    ENDIF
               ENDIF f3
        ENDIF f2
    ENDIF f1
!       we end up here if an inversion failed
      ORder = order -1
      if(order .lt. 4) THEN
      NoSPline = .true.
      RETURN
      ELSE
      GOTO 1000
      ENDIF
      END ! of ThreeD_Spline_init


      SUBROUTINE ThreeD_SPline_Eval(s,p,sn)
      IMPLICIT NONE ! returns P(*) given s
      REAL*8 S                ! position
      INTEGER Sn      ! spline number

!returns
      REAL*8 P(3)
!locals

      INTEGER K
      REAL*8  Spline_eval
      DO 10 k=1,3
10      P(K) = Spline_eval (s,sn,K)
      END ! of ThreeD_Spline_Eval

      REAL*8 FUNCTION Spline_eval (x,sn,axis)
      IMPLICIT NONE
! evaluates from the stored coefs
      REAL*8 x                                        ! one X value
      INTEGER sn                              ! the spline number
      INTEGER axis                            ! the axis (2 or 3)

! ktchanged 25/08/92 include 'filename'
      include 'splincom.f'
      INTEGER I
      REAL*8 dx

      DO i=Spl_ord(sn)-1,1,-1
      if(( xkb(i,sn) .LE. X).or. (i .eq.1)) THEN
      dx = x -xkb(i,sn)
      Spline_eval = coef(axis,0,i,sn)+ dx*(coef(axis,1,i,sn)+dx*(coef(axis,2,i,sn)+ dx*(coef(axis,3,i,sn) )))
      RETURN
      ENDIF
      ENDDO
      END ! of Spline_Eval


      SUBROUTINE Arc_Approx(xg,yg,zg,ng,S)
	use vectors_f
      IMPLICIT NONE
      REAL*8 Xg(*), Yg(*), Zg(*)              ! coordinates
      INTEGER Ng                              ! number of points
      REAL*8 S(*)
! the arc lengths (output)

!        the arc lengths have an arc/chord ratio correction
!       Local variables

      include 'bspcom.f'
      REAL*8 chord(Max_Ngivens ), tangent(3,Max_Ngivens )

      REAL*8 A(3),B(3), tang(3),dummy, ds, sina, angle,  COSINE
      INTEGER I,K
      LOGICAL error
      IF (Ng .gt. Max_Ngivens ) THEN
      write(outputunit,*)'Arc_Approx: too many points - truncating'
      Ng = Max_Ngivens 
      ENDIF

      DO 10 I = 1,Ng-1

      A(1) = Xg(I+1) - Xg(I)
      A(2) = Yg(I+1) - Yg(I)
      A(3) = Zg(I+1) - Zg(I)

      CALL Normalise(a,chord(I),error)
      IF(i .eq.1) THEN
      DO 20 k=1,3
20      B(K) = A(K)
      ENDIF

      DO 30 k=1,3
30      tang(K) = a(k) + B(K)

      CALL Normalise(tang,dummy,error)      ! tangent at left end of arc

      DO 10 k=1,3
      B(k) = A(k)
10      tangent (k,i) = tang(k)

      DO 50 k=1,3
50      tangent(k,ng) = A(K)            ! last one parallel


      S(1) = 0.0d00
      DO 100 i=1,Ng - 1
      COSINE = dot_product(tangent(1:3,I), tangent(1:3,I+1) )
      COSINE = MIN(1.0D00,COSINE)
      cosine = max(-1.0D00,COSINE)

      angle = acos(COSINE) / 2.0


      IF (angle .gt. 0.00001) THEN
      sina = sin (angle/2.0)
      ds = chord(I) * angle /2.00 / sina
      ELSE
      ds = chord(I)
      ENDIF
100     s(I+1) = s(i) + ds

      END     ! of Arc_Approx

      SUBROUTINE Spline_tangent(s,t,ModT,sn)
	use vectors_f
      IMPLICIT NONE

      REAL*8 s                ! the position where a tangent is wanted
      INTEGER sn      ! the spline number
      REAL*8 T(3)     ! the tangent vector (normalised)
      REAL*8 ModT     ! the length of the tangent vector

! ktchanged 25/08/92 include 'filename'
      include 'splincom.f'
      INTEGER I       ,K
      REAL*8 dx
      LOGICAL error

      DO 10 i=Spl_ord(sn)-1,1,-1
      if( ( xkb(i,sn) .LE. s).or. (i .eq.1)) THEN
      dx = s -xkb(i,sn)
      DO 30 K = 1,3
      T(K) = coef(k,1,i,sn) + dx* (coef(k,2,i,sn) * 2.0d00  + coef(k,3,i,sn) * 3.0d00 * dx )
30    CONTINUE

      CALL Normalise(T,ModT,error)
      RETURN
      ENDIF
10              CONTINUE
      END ! of Spline_Tangent



      SUBROUTINE Measure_Arc(S,Smeasured,ng,sn)
      IMPLICIT NONE
! returns arc lengths at each S(i)
      INTEGER sn
      REAL*8 S(*)
      INTEGER Ng      ! the number of values in S(*)
      REAL*8 Smeasured(*)             ! values returned

! ktchanged 25/08/92 include 'filename'
!      include 'splincom.f'

!LOCALS
      INTEGER I ,J
      REAL*8 T(3), B,C,E, Dp,P,Midpoint,ModT, Sum,sm(9)
      data SM/1.0,4.0,2.0,4.0,2.0,4.0,2.0,4.0,1.0/
      Smeasured(1) = 0.0d00

      DO 10 i=1,ng -1

      E = S(I+1)
      B =  S(I)
      C = E - B                         ! span of parameter

      Dp = C/8.0d00
      Sum = 0.0d00

      P = B
      DO 20 j=1,9
      Midpoint = P  !  + Dp /2.0d00
      CALL Spline_tangent(Midpoint,T,ModT,sn)
      P = P + dp
20              Sum = Sum + Dp * ModT  *sm(j)/3.0D00

10      Smeasured(I+1) = Smeasured(I) + Sum


      END ! of Measure_Arc



      SUBROUTINE Start_Spline(x,y,z,sn)
      IMPLICIT NONE
      REAL*8 x,y,z
      INTEGER sn
! ktchanged 25/08/92 include 'filename'
      include 'splincom.f'

      start_Spl(1,sn) = x
      start_Spl(2,sn) = y
      start_Spl(3,sn) = z

      END ! of Start_Spline

      SUBROUTINE Put_on_ThreeD_Spline (P1,P2,Z,S,Sn)
	use vectors_f
      IMPLICIT NONE
!       drops the point P1 normally onto spline Sn

      REAL*8 P1(3)
      INTEGER Sn              ! spline number

!RETURNS
      REAL*8 P2(3)
      REAL*8 Z,S              ! Z is distance off, S is position

! ktchanged 25/08/92 include 'filename'
      include 'splincom.f'
      include 'bspcom.f'
      REAL*8 :: a(3), t(3), normal(3), ds, tol =0.01, ModT,limit
      INTEGER K, count
 

!starting value
      S = 0.0d00

      count = 0
      DO 10 K=1,3
      A(K) = P1(K) - Start_Spl(k,sn)
10      S = S + A(K)**2
      S = SQRT(S)

      limit = 0.0001
100     CALL Spline_tangent(s,t,ModT,sn)        ! gets the tangent T

      CALL ThreeD_Spline_Eval (S,p2,sn)

      DO 20 k=1,3
20      normal(K) = P1(k) - P2(K)

      Ds = dot_product(t,normal)
      if(abs(ds) .gt. 0.000001) THEN
      Z = norm(normal)
      limit = max(tol*Z, 0.0001D00)
      ENDIF
      IF(abs(ds) .gt. limit) THEN
      count = count + 1
      S =     S + ds
      if(count .lt. Max_Ngivens )              GOTO 100
      ENDIF
      CALL ThreeD_Spline_Eval (S,p2,sn)

      END ! of Put_on_ThreeD_Spline



      SUBROUTINE BandSolve (NN,Ain,Y,X,ISIZE,Bwidth,solveError)
      IMPLICIT NONE
!
!  THIS ROUTINE SOLVES THE MATRIX EQUATION  [Y] = [Ain][X]
!  WHERE  [Y] AND [A] ARE KNOWN
!
      INTEGER ISIZE
      REAL*8  Ain(ISIZE,ISIZE),Y(*),X(*)
      INTEGER bwidth, bot
!
      INTEGER  NN, I, J,K, Ipivot, Irow, Icolumn
      REAL*8 A(30,30), FAC
      LOGICAL SOLVEERROR

      SOLVEERROR=.FALSE.
      IF ((NN .GT.ISIZE).or. (nn .gt. 30))  THEN
      WRITE(0,*)' Dimensions exceeded in SOLVE'
      WRITE(0,*)' Array size called was ',NN
      WRITE(0,*)' Limit is              ',ISIZE
      STOP
      ENDIF

      DO 5 i=1,nn
      do 5 j=1,nn
5      A(i,j) = Ain(i,j)

      DO 10 IPIVOT=1,NN-1
!
      IF (A(IPIVOT,IPIVOT) .EQ. 0.0) THEN
!       write(outputunit,*)'  ERROR Pivot zero at row ',ipivot
      SOLVEERROR=.TRUE.
      RETURN
      endif

      bot = min(nn, ipivot + Bwidth)

      DO 10 IROW=IPIVOT+1,bot ! was NN
      FAC=A(IROW,IPIVOT)/A(IPIVOT,IPIVOT)
      DO 20 ICOLUMN=1,NN
20     A(IROW,ICOLUMN)=A(IROW,ICOLUMN)-FAC*A(IPIVOT,ICOLUMN)
!
10     Y(IROW)=Y(IROW)-FAC*Y(IPIVOT)
!
!    BACK SUBSTITUTION
!
      DO 40 I=NN,1,-1
      IF (A(I,I) .EQ. 0.0) THEN
!      write(outputunit,*)'  ERROR IN BACK SUBSTITUTION'
!      write(outputunit,*)' Modified pivot is zero at row' ,I
!      write(outputunit,*)' Reduced matrix is'
!      write(outputunit,*)' '
!      DO 200 II=1,NN
!  200 write(outputunit,'(13F10.4)')(A(II,JJ),JJ=1,NN)
!      STOP
      SOLVEERROR=.TRUE.
      RETURN

      ENDIF
      X(I)=Y(I)
      DO 50 K=I+1,NN
50     X(I)=X(I)-A(I,K)*X(K)
40     X(I)=X(I)/A(I,I)
!

      END ! of bandsolve

