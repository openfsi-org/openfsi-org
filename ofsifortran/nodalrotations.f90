module nodalrotations_f
use basictypes_f
use tanglobals_f
use vectors_f
!
! Comment 27 Dec  This version givea a small improvement in convergence of PH Jun19 model
! but it isn't as big as an improvement as expected.
! Let's rethink how we use this in the stiffness matrix formulation.
! April 2004. THere is an inconsistency between Sbytest and S at finite RBR. 



contains

function drdxtest() result(i)
real(kind=double), dimension(3)		:: x
real(kind=double), dimension(3,3)		:: q
integer:: i
	write(999,*)'X'
	do i = 0,100,10
		x(1) = i * 1D-3
		x(2) = 0
		x(3) = 0
		q = drdx(x) 
		write(999,10) x, q
	enddo
	write(999,*)'now y'
	do i = 0,100,10
		x(2) = i * 1D-3
		x(1) = 0
		x(3) = 0
		q = drdx(x) 
		write(999,10) x, q
	enddo
	write(999,*)'now Z'	
	do i = 0,100,10
		x(3) = i * 1D-3
		x(2) = 0
		x(1) = 0
		q = drdx(x) 
		write(999,10) x, q
	enddo
	write(999,*)'done'

10	format(3E20.10,9E20.10)

	stop
end function drdxtest

function drdx12(x1,x2) result (d)
!use sf_vectors_f
implicit none
real(kind=double), intent(in), dimension(3)		:: x1,x2
real(kind=double), dimension(12,12)				:: d

		d = IdentityMatrix(12)
		d(4:6,4:6)		= drdx(x1)
		d(10:12,10:12)	= drdx(x2)

end function drdx12

function dxdr12(x1,x2) result (d)
!use sf_vectors_f
implicit none
real(kind=double), intent(in), dimension(3)		:: x1,x2
real(kind=double), dimension(12,12)				:: d

		d = IdentityMatrix(12)
		d(4:6,4:6)		= dxdr(x1)
		d(10:12,10:12)	= dxdr(x2)

end function dxdr12


function dxdr(x) result (m) ! the vector expression for dxdr global. By inverse. 
	use invert_f
!	use vectors_f
	implicit none
	real(kind=double), intent(in), target, dimension(3)		:: x
	real(kind=double), dimension(3,3)						:: m

	if(g_No_Qmatrix .or. all(abs(x).lt. 1.0D-7)) then
		m = IdentityMatrix(3)
                write(*,*) ' dxdr small X',x
		return
	endif
	m = minverse(drdx(x))
end function dxdr

pure function QmatrixLinear(x) result (q)
implicit none
real(kind=double), intent(in), dimension(:) :: x ! a rotation vector NOV 2007 was dim(3)
real(kind=double), dimension(3,3)           :: q

   q = RESHAPE(SOURCE = (/ 0.d00, x(3), -x(2) ,  -x(3), 0.d00, x(1) ,  x(2), -x(1), 0.d00 /), SHAPE = (/3,3/))

end function QmatrixLinear

pure function Qmatrix(l_x) result (q)

!use vectors_f
implicit none
real(kind=double), intent(in), dimension(:) :: l_x ! a rotation vector NOV 2007 was dim(3)
real(kind=double), dimension(3,3)		 :: q,a
real(kind=double), dimension(3)			:: n
real(kind=double)						 :: beta

! 1D-8 is indistinguishable. 1D-6 also 
!  1D-5 we see the o(1e-10) jump at the barrier
		if(g_no_Qmatrix) then
			q(1,1) = 1.0D00;
			q(1,2) =  - l_x(3);	
			q(1,3) =  l_x(2) 
			q(2,1) =  + l_x(3);
			q(2,2) = 1.0d00;				
			q(2,3) =-l_x(1)
			q(3,1) =-l_x(2) 
			q(3,2) = l_x(1) 	
			q(3,3) = 1.0d00
			return
		endif
		if(all(abs(l_x).lt. 1.0D-7)) then  ! first term only of Taylor expansion error o(l_x^2)
			q(1,1) = 1.0D00;
			q(1,2) = (l_x(1)*l_x(2))/2.0D00 - l_x(3);	
			q(1,3) =  l_x(2) + (l_x(1)*l_x(3))/2.0D00
			q(2,1) = (l_x(1)*l_x(2))/2.D00 + l_x(3);
			q(2,2) = 1.0d00;				
			q(2,3) =-l_x(1) + (l_x(2)*l_x(3))/2.0D00
			q(3,1) =-l_x(2) + (l_x(1)*l_x(3))/2.0D00;	
			q(3,2) = l_x(1) + (l_x(2)*l_x(3))/2.0D00	
			q(3,3) = 1.0d00
			return
		endif
		beta = norm(l_x);  
		n = l_x/beta
		q = identitymatrix(3)
		a(1,1) =   0.0;	a(1,2) = -n(3);	a(1,3) =  n(2)
		a(2,1) =  n(3);	a(2,2) = 0.0d0;	a(2,3) = -n(1)
		a(3,1) = -n(2);	a(3,2) =  n(1);	a(3,3) = 0.0d0

		q = q + a * sin(beta) + matmul(a,a) * (1.0d0-cos(beta))
end function Qmatrix

function DQDr(x,k) result (m) 

    implicit none
    real(kind=double), intent(in), dimension(3) :: x
    integer, intent(in)				:: k   ! the index 1,2,3
    real(kind=double), dimension(3,3)		:: m
    real(kind=double), dimension(3)		::  dx
    real(kind=double), parameter		::  delta = 0.00001

        dx = 0.0; dx(k) = delta
	m  =  (Qmatrix(x+dx) - Qmatrix(x-dx))/(2.0* delta)
end function DQDr

RX_PURE function drLocalBydxGlobal  (p_r,t) result (m)
!	use sf_vectors_f
	use invert_f
	implicit none
        real(kind=double), intent(in), target, dimension(:)	:: p_r  ! nodal rot vec was dim(3)
	real(kind=double), intent(in), target, dimension(:,:)	:: t  ! was dim(3,3)
        real(kind=double), dimension(3,3)			:: m,m1,m2
        real(kind=double)					:: ang,asq,s,c,det_rhs,det_lhs
        real(kind=double)					:: x1,x2,x3
	if(g_no_Qmatrix) then
		m = t
		return
	endif
	x1= p_r(1); x2= p_r(2) ; x3= p_r(3)
	asq = x1**2 + x2**2 + x3**2
	ang = sqrt(asq)
	s = sin(ang) ; c = cos(ang)
	if(asq .lt. 1D-12) then
		m = t
#ifdef _DEBUG
                write(*,*) 'drLocalBydxGlobal asq zero (looks wrong) '
#endif
		return
	endif
! is the det of RHS zero??

	det_rhs = -1 - x1**2/4. + x2**2/4. - (x1*x2*x3)/4. - x3**2/4.

	det_lhs = t(1,3)*t(2,2)*t(3,1) + x1**2*t(1,3)*t(2,2)*t(3,1) - t(1,2)*t(2,3)*t(3,1) - x1**2*t(1,2)*t(2,3)*t(3,1) - &
     t(1,3)*t(2,1)*t(3,2) - x1**2*t(1,3)*t(2,1)*t(3,2) + t(1,1)*t(2,3)*t(3,2) + x1**2*t(1,1)*t(2,3)*t(3,2) + &
     t(1,2)*t(2,1)*t(3,3) + x1**2*t(1,2)*t(2,1)*t(3,3) - t(1,1)*t(2,2)*t(3,3) - x1**2*t(1,1)*t(2,2)*t(3,3)


!  The equations are  [lhsmat] .{drr[1],drr[2],drr[3]}  == rhsmat . {dx1,dx2,dx3}
! so drbydx = inv(lhsmat) . rhsmat
! lhsmat lhsmatout   ang^2 /. Cos[ang] -> c /. Sin[ang] -> s // FortranForm

   if(asq .gt. 1D-12) then   
 		m1(1,1) =  -((ang**2 - x1**2 - x2**2 + c*(x1**2 + x2**2))*t(1,2)) - (ang*s*x1 - x2*x3 + c*x2*x3)*t(1,3)
 		m1(1,2) =  -((ang**2 - x1**2 - x2**2 + c*(x1**2 + x2**2))*t(2,2)) - (ang*s*x1 - x2*x3 + c*x2*x3)*t(2,3)
 		m1(1,3) =  -((ang**2 - x1**2 - x2**2 + c*(x1**2 + x2**2))*t(3,2)) - (ang*s*x1 - x2*x3 + c*x2*x3)*t(3,3)
 
 		m1(2,1) =  (-(ang*s*x1) - x2*x3 + c*x2*x3)*t(1,2) + (ang**2 - x1**2 - x3**2 + c*(x1**2 + x3**2))*t(1,3)
 		m1(2,2) =  (-(ang*s*x1) - x2*x3 + c*x2*x3)*t(2,2) + (ang**2 - x1**2 - x3**2 + c*(x1**2 + x3**2))*t(2,3)
 		m1(2,3) =  (-(ang*s*x1) - x2*x3 + c*x2*x3)*t(3,2) + (ang**2 - x1**2 - x3**2 + c*(x1**2 + x3**2))*t(3,3)
  
 		m1(3,1) =  (ang**2 - x1**2 - x2**2 + c*(x1**2 + x2**2))*t(1,1) + (-(ang*s*x2) - x1*x3 + c*x1*x3)*t(1,3)
 		m1(3,2) =  (ang**2 - x1**2 - x2**2 + c*(x1**2 + x2**2))*t(2,1) + (-(ang*s*x2) - x1*x3 + c*x1*x3)*t(2,3)
 		m1(3,3) =  (ang**2 - x1**2 - x2**2 + c*(x1**2 + x2**2))*t(3,1) + (-(ang*s*x2) - x1*x3 + c*x1*x3)*t(3,3)

		m1 = m1 / asq

! rhsmat M2
! rhsmatout ang^4 /. Cos[ang] -> c /. Sin[ang] -> s // FortranForm
   
 		m2(1,1) =  -2*(-1 + c)*x1**2*x3 - ang**2*(c*x1*x2 + x3 - c*x3) + ang*s*x1*(x2 - x1*x3)
  		m2(1,2) =  -(ang**3*s) - ang**2*c*x2**2 - 2*(-1 + c)*x1*x2*x3 + ang*s*x2*(x2 - x1*x3)
 		m2(1,3) =  -2*(-1 + c)*x1*x3**2 + ang*s*x3*(x2 - x1*x3) + ang**2*((-1 + c)*x1 - c*x2*x3)
 
 		m2(2,1) =  -2*(-1 + c)*x1**2*x2 - ang*s*x1*(x1*x2 + x3) + ang**2*((-1 + c)*x2 + c*x1*x3)
 		m2(2,2) =  -2*(-1 + c)*x1*x2**2 - ang*s*x2*(x1*x2 + x3) + ang**2*((-1 + c)*x1 + c*x2*x3)
  		m2(2,3) =  ang**3*s - (-2 + 2*c + ang*s)*x1*x2*x3 + ang*(ang*c - s)*x3**2
	  
   		m2(3,1) =  ang**3*s + ang*(ang*c - s)*x1**2 - (-2 + 2*c + ang*s)*x1*x2*x3
  		m2(3,2) =  -2*(-1 + c)*x2**2*x3 + ang**2*(c*x1*x2 + (-1 + c)*x3) - ang*s*x2*(x1 + x2*x3)
   		m2(3,3) =  -2*(-1 + c)*x2*x3**2 + ang**2*((-1 + c)*x2 + c*x1*x3) - ang*s*x3*(x1 + x2*x3)

		m2 = m2/asq**2
	else ! small angle

		m1(1,1) = -t(1,2) - x1*t(1,3);	m1(1,2)=-t(2,2) - x1*t(2,3);	m1(1,3)=-t(3,2) - x1*t(3,3)
		m1(2,1) =-(x1*t(1,2)) +t(1,3);	m1(2,2)=-(x1*t(2,2)) +t(2,3);	m1(2,3)= -(x1*t(3,2)) + t(3,3)
		m1(3,1) = t(1,1) - x2*t(1,3);	m1(3,2)=t(2,1) - x2*t(2,3);		m1(3,3)= t(3,1) - x2*t(3,3)


		m2(1,1) = -x3/2.;	m2(1,2) =-1.0d00;	m2(1,3) =-x1/2.
		m2(2,1) = -x2/2.;	m2(2,2) = -x1/2.;	m2(2,3) =1.0d00
		m2(3,1) = 1.0d00;	m2(3,2) = -x3/2.;	m2(3,3) = -x2/2.
	endif
! "he equations are now  [lhsmat] .{drlocal}  == rhsmat \{dx1,dx2,dx3}"

	m = matmul(minverse(m1) ,m2) 

end function  drLocalBydxGlobal


function drdx  (p_r) result (m)
!	use sf_vectors_f
	use invert_f
!	use pf_calls_f
	implicit none
	real(kind=double), intent(in), target, dimension(3)		:: p_r  ! the rotation vector
	real(kind=double), dimension(3,3)						:: m,m1,m2
	real(kind=double)										:: ang,asq,s,c,det_lhs,det_rhs,det
	real(kind=double), pointer								:: x1,x2,x3

	if(g_no_Qmatrix) then  ! guessing a bit


		 m(1,1)= 1;			m(1,2) =  - x3/2.	;	m(1,3) =  x2/2.
		 m(2,1)= x3/2.;		m(2,2) = 1.0			;	m(2,3) = -x1/2. 
		 m(3,1)=-x2/2.;		m(3,2) = x1/2.		;	m(3,3) = 1.0d00
		return

	endif

	x1=>p_r(1); x2=>p_r(2) ; x3=>p_r(3)
	asq = x1**2 + x2**2 + x3**2
	ang = sqrt(asq)
	s = sin(ang) ; c = cos(ang)


	if(asq .lt. 1D-12) then
		det = (27*(4 + x3**2) + x2**2*(27 + 6*x3**2) + x1**2*(27 + 6*x3**2 + x2**2*(6 + x3**2)))/108.

		if(abs(det) .lt. 1D-12) then
			write(outputunit,*)"(drdx) small drdx zero. Haven't coded this case" 
		endif	


		 m(1,1)= 1;						m(1,2) = (x1*x2)/6. - x3/2. ;	m(1,3) = x2/2. + (x1*x3)/6.
		 m(2,1)=(x1*x2)/6. + x3/2.;		m(2,2) = 1;						m(2,3) = -x1/2. + (x2*x3)/6.
                 m(3,1)=-x2/2. + (x1*x3)/6.;	m(3,2) = x1/2. + (x2*x3)/6.;	m(3,3) = 1.0d00
		return
	endif

! large deflection case

! is the det of RHS zero??

	det_rhs = -((ang**5*s**3 + ang**4*c*s**2*(x1**2 + x2**2 + x3**2) + 2*(-1 + c)**3*x1*x2*x3*(x1**2 + x2**2 + x3**2) - &
		ang**3*s*((-1 + 2*c - c**2 + s**2)*x1**2 + (1 - 2*c + c**2 + s**2)*x2**2 + (-2*c + 2*c**2 + s**2)*x1*x2*x3 + &
		(-1 + 2*c - c**2 + s**2)*x3**2) - ang*(-1 + c)**2*s*  &
		(x1**4 - x2**4 - x1**3*x2*x3 + 2*x1**2*x3**2 + x3**4 - x1*x2*x3*(x2**2 + x3**2)) + &
		ang**2*(-1 + c)*(-2*x1*x3*(x2 + s**2*x1*x3) + c*(-x1**4 + x2**4 + 4*x1*x2*x3 + 2*x1**2*x3**2 - x3**4) + &
		c**2*(x1**4 - x2**4 - 2*x1*x2*x3 - 2*x1**2*x3**2 + x3**4)))/ang**8)

	if(abs(det_rhs) .lt. 1D-12) then
			write(outputunit,*)"(drdx)  rhs zero. Haven't coded this case" 
	endif
	det_lhs =   -(((x3**2 + (x1**2 + x2**2)*Cos(Sqrt(x1**2 + x2**2 + x3**2)))* &
           (x1**2 + (x2**2 + x3**2)*Cos(Sqrt(x1**2 + x2**2 + x3**2))))/(x1**2 + x2**2 + x3**2)**2)

	if(abs(det_lhs) .lt. 1D-12) then
		   write(outputunit,*)"(drdx) Lhs zero. Haven't coded this case" 
	endif


!  The equations are  [lhsmat] .{drr[1],drr[2],drr[3]}  == rhsmat . {dx1,dx2,dx3}
! so drbydx = inv(lhsmat) . rhsmat
! lhsmat lhsmatout   ang^2 /. Cos[ang] -> c /. Sin[ang] -> s // FortranForm

 		m1(1,1) =       0.0d00
 		m1(1,2) =  	   -ang**2 + x1**2 + x2**2 - c*(x1**2 + x2**2)
 		m1(1,3) = 	   -(ang*s*x1) + x2*x3 - c*x2*x3
 
 		m1(2,1) =     0.0d00
 		m1(2,2) = 	-(ang*s*x1) - x2*x3 + c*x2*x3
 		m1(2,3) = 	ang**2*(1 + ((-1 + c)*(x1**2 + x3**2))/ang**2)
  
 		m1(3,1) =    ang**2*(1 + ((-1 + c)*(x1**2 + x2**2))/ang**2)
 		m1(3,2) =  	0.0d00
 		m1(3,3) = 	-(ang*s*x2) - x1*x3 + c*x1*x3

	m1 = m1 / asq

! rhsmat M2
! rhsmatout ang^4 /. Cos[ang] -> c /. Sin[ang] -> s // FortranForm
     
 		m2(1,1) =  -2*(-1 + c)*x1**2*x3 - ang**2*(c*x1*x2 + x3 - c*x3) + ang*s*x1*(x2 - x1*x3)
  		m2(1,2) =  -(ang**3*s) - ang**2*c*x2**2 - 2*(-1 + c)*x1*x2*x3 + ang*s*x2*(x2 - x1*x3)
 		m2(1,3) =  -2*(-1 + c)*x1*x3**2 + ang*s*x3*(x2 - x1*x3) + ang**2*((-1 + c)*x1 - c*x2*x3)
 
 		m2(2,1) =  -2*(-1 + c)*x1**2*x2 - ang*s*x1*(x1*x2 + x3) + ang**2*((-1 + c)*x2 + c*x1*x3)
 		m2(2,2) =  -2*(-1 + c)*x1*x2**2 - ang*s*x2*(x1*x2 + x3) + ang**2*((-1 + c)*x1 + c*x2*x3)
  		m2(2,3) =  ang**3*s - (-2 + 2*c + ang*s)*x1*x2*x3 + ang*(ang*c - s)*x3**2
	  
   		m2(3,1) =  ang**3*s + ang*(ang*c - s)*x1**2 - (-2 + 2*c + ang*s)*x1*x2*x3
  		m2(3,2) =  -2*(-1 + c)*x2**2*x3 + ang**2*(c*x1*x2 + (-1 + c)*x3) - ang*s*x2*(x1 + x2*x3)
   		m2(3,3) =  -2*(-1 + c)*x2*x3**2 + ang**2*((-1 + c)*x2 + c*x1*x3) - ang*s*x3*(x1 + x2*x3)

	m2 = m2/asq**2



! "he equations are now  [lhsmat] .{drlocal}  == rhsmat \{dx1,dx2,dx3}"

	m = matmul(minverse(m1) ,m2) 

end function  drdx

#ifdef NEVER
function following_Load_Stiffness(i) result (t)
use tanglobals_f
implicit none
integer, intent(in) :: i
real(kind=double),dimension(6,6)	:: t

t=0

t(1:3,4) = matmul(DQDr(x(i,4:6) ,1) , g_p(i,1:3))
t(1:3,5) = matmul(DQDr(x(i,4:6) ,2) , g_p(i,1:3))
t(1:3,6) = matmul(DQDr(x(i,4:6) ,3) , g_p(i,1:3))

! sign is in the same convention as other S's
! transpose or No is verified

end function following_Load_Stiffness
#endif
end module nodalrotations_f
