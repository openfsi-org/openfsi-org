MODULE basictypes_f
	IMPLICIT NONE
	INTEGER, PARAMETER :: double = 8 ! KIND(1.0D00)
	INTEGER, PARAMETER :: single = 4 ! KIND(1.0)
	INTEGER, PARAMETER :: rxquad = 8		! beware this is not conventionl
	real(kind=double) ,parameter:: eps_double = epsilon(1.0D00)
	real(kind=double) ,parameter:: RXEPSILON =  epsilon(1.0D00) 
	character, parameter :: TAB = char(9)
	integer, parameter :: outputunit = 6  
	
#ifdef GFORTRAN	
	INTEGER, PARAMETER ::cptrsize = 8
#else   	
	INTEGER, PARAMETER ::cptrsize = int_ptr_kind()
#endif
END  MODULE basictypes_f
