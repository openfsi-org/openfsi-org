!* 12/3/95 N LPT removed
!*   date: Saturday  8/1/1994
!*   translated from LAHEY FFTOSTD output
!*
!* date:  Tuesday 14 September 1993
!* now uses TRIS.f as developed for JACOBI
!*
!* 1) Geometric stiffness added 
!* 2) Subroutine titles re-arranged. (see DDNEW.f of same date)
!*  SEEMS TO WORK VERY WELL
!module triscreen_f
!contains
!function screenTri(t,u) result(rc)
!    use coordinates_f
!    use invert_f
!    use tris_f
!    IMPLICIT NONE
!    TYPE ( tri),pointer    	:: t
!	integer, intent(in) :: u
!	integer rc; rc=0
!	
!3	format(a12,3(1x,g13.4))
!
!		if(any(isnan( t%eps)) )write(u,3) 'eps is NAN  ' 
!		if(any(isnan( t%tedge)) )write(u,3) 'tedge is NAN '
!		if(any(isnan( t% stress )) )write(u,3) 'stress is NAN ' 
!
!		if(any(isnan( t%DSTFold )) )write(u,3) 'dstf  is NAN  '	! unwrinkled material matrix
!		if(any(isnan( t%g )) )write(u,3) 'G   is NAN    '  		! strain from edge lengths
! 		if(any(isnan( t%Tri_K )) )write(u,3) 'tri_K is NAN  '	! tedge from delta  
!		if(any(isnan( t%m_ps )) )write(u,3) 'ps  is NAN     ' 
! 		if(any(isnan( t%E0 )) )write(u,3) 'e0   is NAN    ' 
!
!		if(isnan( t%THETAE_rad ) )write(u,3) 'theta  is NAN ' 
!		if(isnan( t%AREA ) )write(u,3) 'area is NAN '
!		if(isnan( t%PREST  ) )write(u,3) 'p is NAN '
!
!		if(isnan( t%CurrentArea ))write(u,3) 'cA is NAN ' 
!		if(isnan( t%Ustiff ))write(u,3) 'Ustf is NAN ' 
!		if(isnan( t%wr_angle  ))write(u,3) 'wrng is NAN ' 
!end  function screenTri
!end module triscreen_f
module ttmass_f
contains
subroutine AssembleTriElts(mdl,mat) 
use basictypes_f 
use mtkaCST_F
use saillist_F
USE tris_f
use coordinates_f
use sf_tris_pure_f
use wrinkle_m_f
use stiffness_f
use invert_f
use rx_control_flags_f
use basictypes_f
 
        IMPLICIT NONE
        type(sail),pointer  ::mdl
     integer(kind=cptrsize) :: mat
      INTEGER  K,  N, state
        REAL (kind= double), target  , dimension(9,9) :: S9,kg

        LOGICAL Saved_Force_Linear, saved_DB2, flag
 
        logical, parameter :: OldGeoMatrix = .false. 
        logical, parameter :: NewGeoMatrix = .true.
        real(kind=double), pointer, dimension(:,:) :: l_GG
         real(kind=double),  dimension(3) :: l_psa
        real(kind=double), pointer  :: l_area ,l_triPressure
        real(kind=double), dimension(3)  :: x1,x2,x3  ,d,l_zir !, l_stress 
        real(kind=double), dimension(3,3)  :: l_d
        type(tri), pointer :: t   
        real(kind=8), dimension(9) ::X    ! current   coords packed as a vector
        real(kind=8), dimension(6)  ::forces  ! the corner forces in element local axes
 
   
        Saved_Force_Linear = Force_Linear
        saved_DB2=Debug2
        if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 1'
        
 do_N:  DO N=1,mdl%tris%count
        flag = .false. 
        t=>mdl%tris%list(N)
        if(.not. t%use_tri) cycle
        t%m_stress = 0  
f2006:  if(form_2006) then

            l_GG=>t%g   
            l_area=> t%m_area
            x1=get_Cartesian_X_by_ptr(t%nr(1)%m_fnpe);
            x2=get_Cartesian_X_by_ptr(t%nr(2)%m_fnpe);
            x3=get_Cartesian_X_by_ptr(t%nr(3)%m_fnpe);
            forall(k=1:3)  
                l_zir(k) = mdl%edges%list(t%edgeno(k))%m_ZLINK0 ! initial lengths of the edge  !zir(1:3,L)
             end forall
            l_triPressure=>t%prest
            forall(k=1:3) d(k) = t%m_er(k)%m_fepe%m_delta0
            t%m_eps = matmul(t%g,d)  
            if(  any(t%m_ps/=0.0) ) then           
                    t%m_eps =  t%m_eps - matmul(minverse(t%dstfold), t%m_ps )
            endif   
            l_psa=0.
            state = get_wrinkled_stiffness(t%N, t%m_eps, t%dstfold, &
            K_on_Wrinkled_Matrix .and. t%creaseable, l_d,t%m_stress, t%wr_angle)
! this kills off any negative principle stress

            s9 =sf_one_tri_global_stiffness_2006(OldGeoMatrix , l_GG,l_D, &
            t%m_stress,   &
            x1,x2,x3,&
            l_zir,l_psa, l_area,l_tripressure) ! this is the stroona validated one - but dont let it get KG


             if( NewGeoMatrix) then            !  add the KG
                    x(1:3)=x1; x(4:6)=x2; x(7:9)=x3;
                    forces=-LocalForces(t)
                    call mtkaelementGeoMat(x,forces,kg)  ! the geometric stiffness matrix, global
                    s9=s9+Transpose(kg)
            endif
         else f2006
            CALL one_tri_global_stiffness_oona(t,s9)
         endif f2006

         call  AddStiffness(mdl,t%n13(1:3) ,s9,mat)
      ENDDO do_N

      Force_Linear=Saved_Force_Linear
      Debug2=saved_DB2 
end subroutine AssembleTriElts

SUBROUTINE TTMASS9(s )
    use ftypes_f
    use saillist_F
    USE tris_f
    use coordinates_f
    use sf_tris_pure_f
    use wrinkle_m_f
    use invert_f
    use mtkaCST_F
    use rx_control_flags_f
    use fglobals_f
        IMPLICIT NONE
        type(sail)  ::s

        INTEGER J,K, L, N, Row, c,state
        REAL (kind= double), target  , dimension(9,9) :: S9
        LOGICAL Saved_Force_Linear, saved_DB2, flag
        logical, parameter :: massOnGeoMatrix = .true. 
        real(kind=double), pointer, dimension(:,:) :: l_GG
        real(kind=double),  dimension(3) :: l_psa
        real(kind=double), pointer  :: l_area ,l_triPressure
        real(kind=double), dimension(3)  :: x1,x2,x3   ,d,l_zir
        real(kind=double), dimension(3,3)  :: l_d
        type(tri), pointer :: t   
        type(Fnode), pointer :: thisnode   
    
        Saved_Force_Linear = Force_Linear
        saved_DB2=Debug2
        if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 1'
        
 do_N:  DO N=1,s%tris%count
        flag = .false. 
        t=>s%tris%list(N)
        if(.not. t%use_tri) cycle
        t%m_stress = 0  
        if(.not. t%used) cycle
f2006:  if(form_2006) then
            l_GG=>t%g  
            l_area=> t%m_area
            x1=get_Cartesian_X_by_ptr(t%nr(1)%m_fnpe);
            x2=get_Cartesian_X_by_ptr(t%nr(2)%m_fnpe);
            x3=get_Cartesian_X_by_ptr(t%nr(3)%m_fnpe);
            forall(k=1:3)  
                l_zir(k) = s%edges%list(t%edgeno(k))%m_ZLINK0 ! initial lengths of the edge  
            end forall
            l_triPressure=>t%prest
            forall(k=1:3) d(k) = t%m_er(k)%m_fepe%m_delta0
            t%m_eps = matmul(t%g,d)  
            if(  any(t%m_ps/=0.0) ) then           
                   ! write(outputunit,*) ' but it would be faster to adjust the link initial lengths'     
                    t%m_eps =  t%m_eps - matmul(minverse(t%dstfold), t%m_ps )
            endif   
            l_psa=0.   
            state = get_wrinkled_stiffness(t%N, t%m_eps, t%dstfold, &
            K_on_Wrinkled_Matrix .and. t%creaseable, l_d, t%m_stress, t%wr_angle)
! this kills off any negative principle stress

            s9 =sf_one_tri_global_stiffness_2006(massOnGeoMatrix, l_GG,l_D, &
            t%m_stress ,   &
            x1,x2,x3,&
            l_zir,l_psa, l_area,l_tripressure) ! this is the stroona validated one

         else f2006
            CALL one_tri_global_stiffness_oona(t,s9)
         endif f2006
         if(any(isnan(s9))) then
		write(outputunit,*) '(ttmass9) NAN Tri Global Stiffness trino=',N ,' n13=',t%n13(1:3)

		do j=1,9
			!write(outputunit,'(9g10.3)') s9(j,1:9)
			do k=1,9
				if(isnan(s9(j,k))) s9(j,k)=0
			enddo
		enddo

	endif   

            DO j=1,3
            thisNode => t%nr(j)%m_fnpe
            DO K=1,3
                row = (J-1) * 3 + K
                if(s9(row,row) .lt. 0.0) flag=.true. 
                DO L=1,3
                   c = L + (j-1)*3        
                   thisnode%XM(k,l) = thisnode%XM(k,l) + s9(row,c)
                ENDDO
            ENDDO
          ENDDO
      ENDDO do_N

      Force_Linear=Saved_Force_Linear
      Debug2=saved_DB2
END subroutine TTmass9

SUBROUTINE NodalNormals(sli)
    use saillist_f
    use coordinates_f
    use math_f
#ifdef _DEBUG 
    use hoopsinterface
    use cfromf_f  ! for akm insert line
#endif
    IMPLICIT NONE
    integer, intent(in) :: sli
      INTEGER I,K,N
      REAL(kind=8) :: Tt(3,3) 
      real(kind=8) :: z
    !  real(C_FLOAT) , dimension(3):: a,b
      type(sail), pointer :: s
      type(tri), pointer :: t   
      type(Fnode), pointer :: fn   
      

		if(.not. is_active(sli) ) return
		s=>saillist(sli)
		forall (k=1:saillist(sli)%nodes%ncount)
				s%nodes%xlist(k)%m_Nnor=0
		end forall

!$OMP PARALLEL DO PRIVATE(N,t,tt,fn,i  ) shared(saillist,sli,s) default(none)
do_N:      DO  N=1, s%tris%count 
                t=>s%tris%list(N)
                if(.not. t%Used) cycle
                tt=scaxismatrixByPtr(t%nr(1)%m_fnpe,t%nr(2)%m_fnpe,t%nr(3)%m_fnpe ) ; 
                t%m_tnorm=  tt(1:3,3)
                DO I=1,3
                   fn=>t%nr(i)%m_fnpe
                   fn%m_Nnor=fn%m_Nnor +  t%m_tnorm * t%m_area
                ENDDO
          END DO do_N
!$OMP END PARALLEL DO  

!$OMP PARALLEL DO PRIVATE(N,fn,i ,z ) shared(saillist,sli,s) default(none)
do_NN:      DO  N=1, s%nodes%ncount 
                fn=>s%nodes%xlist(N)
                if(.not. fn%m_Nused) cycle
                z = sum( fn%m_Nnor**2)
               if(z>tiny(z)) then; z=sqrt(z); fn%m_Nnor=fn%m_Nnor/z
               else ;  fn%m_Nnor=0.0; endif
          END DO do_NN
!$OMP END PARALLEL DO  
#ifdef NEVER ! draw
 call HF_Open_Segment("normals"//char(0));
 call hf_flush_contents("."//char(0),"everything"//char(0))
 do_NNN:  DO  N=1, s%nodes%ncount 
               fn=>s%nodes%xlist(N)
               if(.not. fn%m_Nused) cycle
			   a = get_Cartesian_X_By_Ptr(fn) 
               b= a + fn%m_Nnor
               call akm_insert_line(a(1),a(2),a(3),b(1),b(2),b(3))  
          END DO do_NNN
  call hf_close_segment() 
#endif 
end SUBROUTINE NodalNormals

SUBROUTINE Pressure_Load(psilent)
    use  fglobals_f
    use saillist_f
    use coordinates_f
    use math_f
    IMPLICIT NONE

    Logical, intent(in), optional :: psilent
    logical silent
      REAL(kind=8), dimension(3) :: centroid,extra
      INTEGER I,K,N,sli
      REAL(kind=8) :: Tt(3,3) , l_pgravSSD(101,3)
      type(sail), pointer :: s
      type(tri), pointer :: t   
      type(Fnode), pointer :: fn   
        silent=.false.
        if(present(psilent)) silent=psilent

     l_pgravSSD = 0.0D00
      extra=0.0d00
      total_Force=0.0
      moms=0.0

do_S:  DO sli=1,sailcount
        if(.not. is_active(sli) ) cycle
        s=>saillist(sli)
do_N:   DO N=1,s%tris%count
            t=>s%tris%list(N)
            if(.not. t%used) then
                write(*,*) ' tri ',N, ' isnt used'
                cycle
            endif
            if(.not. t%use_tri) then
                write(*,*) ' tri ',N, ' isnt use_tri'
                cycle
            endif
            if( isNAN(t%prest)) then
                t%prest = 0.
                write(outputunit,*) ' BAD pressure ',n
            endif
            if( isNAN(t%m_Area)) then
                write(outputunit,*) ' BAD area ',n
                T%m_Area  = 0.
            endif
            tt=scaxismatrixByPtr(t%nr(1)%m_fnpe,t%nr(2)%m_fnpe,t%nr(3)%m_fnpe ) ; t%m_tnorm=  tt(1:3,3)
            centroid=0.0d00
            DO I=1,3
               fn=>t%nr(i)%m_fnpe
               centroid = centroid + get_Cartesian_X_By_Ptr(fn)/3.0D00
            ENDDO
! if tri elements had mass we could add their weight here.
            EXTRA= t%m_Area* (l_pgravSSD(sli,1:3) + t%PREST *tt(1:3,3))
             DO I=1,3
                fn=>t%nr(i)%m_fnpe
                fn%P=fn%P + EXTRA /3.000
             ENDDO

           TOTAL_Force = TOTAL_Force  + EXTRA

           moms(1)= moms(1)+extra(3)*centroid(2) -extra(2)* centroid(3)
           moms(2)= moms(2)+extra(1)*centroid(3) -extra(3)* centroid(1)
           moms(3)= moms(3)+extra(2)*centroid(1) -extra(1)* centroid(2)
      ENDDO do_N
     ENDDO do_S
         
     moms = MOMS/1000.0D00
     total_Force = TOTAL_Force/1000.0D00
     if(.not. silent) then
     if(any(total_Force .ne. 0.0) .or. any(moms .ne. 0.0)) then
          write(outputunit,910)'Fx   ','Fy   ','Fz   ','Mx   ','My   ','Mz    '
         write(outputunit,900)(TOTAL_Force(K),K=1,3),(moms(K),K=1,3)
     endif
     endif
900     FORMAT(6(1x,g11.4))
910     FORMAT(6(1x,a11))

END SUBROUTINE Pressure_Load

subroutine  Post_Relax_Forces()
    use fglobals_f
    use cfromf_f
    implicit none
    character(len=12)::v
    character c
    integer k,rv
    K=-1
    c=CHAR(0)
        
            write(v,'(G12.5)') total_Force(1)
            rv= Post_Summary_By_SLI(K, 'Fx_relax'//c,v//c)
            write(v,'(G12.5)') total_Force(2)
            rv= Post_Summary_By_SLI(K, 'Fy_relax'//c,v//c)
            write(v,'(G12.5)') total_Force(3)
            rv= Post_Summary_By_SLI(K, 'Fz_relax'//c,v//c)

            write(v,'(G12.5)') moms(1)
            rv= Post_Summary_By_SLI(K, 'Mx_relax'//c,v//c)
            write(v,'(G12.5)') moms(2)
            rv= Post_Summary_By_SLI(K, 'My_relax'//c,v//c) 
            write(v,'(G12.5)') moms(3)
            rv= Post_Summary_By_SLI(K, 'Mz_relax'//c,v//c)

      CALL Print_Force_Totals(outputunit)

      END subroutine  Post_Relax_Forces

subroutine  Print_Force_Totals(unit)
    use fglobals_f
    implicit none
    integer, intent(in) :: unit
    character (len=64) :: fmt10
    integer :: k
    write(fmt10,'("(a17,3(1H",a1,",EN14.4))")') achar(9)  ! tab character to help spreadsheet
    if(unit /= outputunit .or. any(total_force /=0)) then
        write(unit,fmt10,iostat=k) '  Total Forces', total_Force
        if(any (grav /=0)) write(unit, fmt10,iostat=k)   'of which gravity', grav
        if(any (windage /=0)) write(unit,fmt10,iostat=k) '     and windage', windage
    endif
    if(unit /= outputunit .or.any(moms /=0)) write(unit,fmt10,iostat=k) ' Total Moments', moms

END subroutine  Print_Force_Totals

function All_Loading(acceleration,pf1,pf2,psilent)  result(ierr)! f1 is GNOPRINT, f2 is GWITHV
    use basictypes_f
    use gravity_f
    use coordinates_f
    use saillist_f
    implicit none
        integer::ierr
        logical ::no_pressureLoads
        real(kind=double), intent(in),dimension(3) ::acceleration
        integer, intent(in), optional ::pf1,pf2
         logical, intent(in), optional :: psilent
        integer  :: sli, ok,f1,f2
        logical:: dum,silent
        silent = .true.
        if(present(psilent)) silent=psilent
	f1 = 0;	if(present(pf1)) f1=pf1
	f2 = 0;	if(present(pf2)) f2=pf2
	 ierr=0; ok=1;
        CALL CHECK_CL('NP',no_pressureLoads)
	do sli=1,sailcount
             if( is_active(sli))  ok=zero_p(sli)  !sets to fnode%m_Nodal_Load
             if(ok==0) ierr=ierr+1
	enddo	
	ierr = Gravity_loads(acceleration,f1) +ierr
        if(.not. no_pressureLoads) then
            ierr = Windage_loads(f2) +ierr
            CALL Pressure_Load(silent)! adds nodal loads due to PREST
        endif
        call check_cl("ZS",dum)  ; 	if(dum) call  LoadZeroSum()
end function All_Loading

subroutine  LoadZeroSum()  
    use basictypes_f
    use saillist_f
    implicit none
    real(kind=double), dimension(3) :: tot
    integer :: nn,sli,j,k
    tot=0.; nn=0;
    do sli=1,sailcount
        if(.not. is_active(sli)) cycle
        j =   saillist(sli)%nodes%ncount
        do k=1,j
             tot = tot + saillist(sli)%nodes%xlist(k)%p
        end do
        nn = nn + j
    enddo
    if(nn<1) return
    write(*,'(" balanced out loading :",3f15.5," on ",i8,"nodes" )',iostat=k) tot,nn
    tot = -tot/(dble(nn))
  
  	do sli=1,sailcount
			 if(.not. is_active(sli)) cycle
            j =   saillist(sli)%nodes%ncount
            do k=1,j
                  saillist(sli)%nodes%xlist(k)%p = saillist(sli)%nodes%xlist(k)%p +tot
            end do
    enddo  
end subroutine  LoadZeroSum
end module ttmass_f
