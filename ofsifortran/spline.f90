! This is the new mast spline code. maybe like	threedee
! Given ng points (xg,yg,zg) it returns NP points which are equally spaced
! along a spline between (xg,yg,zg

! based on Get Even Stripes
! a simple spline interpolation intended to be used by both pre and post-processing.
! USE WITH CARE . - it does some fairing

      SUBROUTINE fcspline(Pin,ng,p,npo)  bind(C,name="oldrxfcspline_" )
      use basictypes_f  ! for outputunit
      IMPLICIT NONE
      REAL*4 Pin(3,*)
      INTEGER ng
        INTEGER npo

      REAL*4 p(3,*)
     
      INTEGER I,K,np,order,sn
      REAL*8 arclength,ss
      REAL*8 xp(200),yp(200),zp(200) ,v(3)
      LOGICAL failed,error
	arclength=0.0 ! Peter Aug 97
	ss=0.0 ! Peter Aug 97
            if(ng .gt. 200) then
              	write(outputunit,*) ' more than 200 spline points '
!              	PA USE
           ENDIF
           if(ng .lt. 2) then
      	     write(outputunit,*) ' TOO FEW spline  points  '
                  STOP
           ENDIF
      DO I=1,ng
      	xp(I) = Pin(1,i) 
      	yp(i) = Pin(2,I)
      	zp(I) = Pin(3,I)
      ENDDO

      np = ng
! raw data, in order 
      Order = 4
! Peter Jan 2004
	order = ng   ! for mast spline when more than 4 pts     

      CALL Sort_Stripe(xp,yp,zp,Np) ! ALSO REMOVES DUPLICATES
! this code borrowed from SHAPE.F

25     CONTINUE 

       DO WHILE (np .lt. 4)
   	   CALL addapointtoxg  ( xp,yp,zp,np,error)
                 if(error) write(outputunit,*) ' addapointtoxg  in spline(1)'
       ENDDO

       Sn = 1
      CALL ThreeD_Spline_init(xp,yp,zp,np,order,sn,arclength,failed)

      IF (.not. Failed) THEN
      	do  i=1,Npo
      	SS = FLOAT(i-1) * arclength / FLOAT(Npo-1)
      	CALL ThreeD_Spline_Eval(ss,v,sn)
		DO K=1,3
		 p(K,i) = v(k)
		ENDDO
              ENDDO
!
! force first and last points
!
	DO K=1,3
		 p(K,1) = pin(K,1)
                             p(K,npo) = pin(K,ng)
	ENDDO
    	return
      ELSE            ! spline initialisation failed
!               ! add a pnt by linear interpolation

      CALL addapointToxg  ( xp,yp,zp,np,error)
       if(error) write(outputunit,*) ' addapointToxg  in spline(2)'
      GOTO 25
! try again
      ENDIF

      END
