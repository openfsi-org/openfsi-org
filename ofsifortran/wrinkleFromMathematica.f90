!
!	IMPORTANT.  These routines need consistency at high precision because there are
!	many comparisons to zero of very small numbers. 
!
!	In particular in LF95/Linux you must compile with --ap

!   Nov 24 2006  more speed optimisations.  Pass SU(unwrinkled stress) down 
 !             and reformulate d_de_by_de (see math notebook wrinkling_debyd_eN_nov2006.nb)
 !            note line 134 needs to be if(false) to enable the reformulation
!	Nov 15th 2003.  Some speed optimisations. Matmul in fn wt
!
!
!DEC$ DEFINE compaq


MODULE WRINKLE_M_F
 use basictypes_f
 use mathconstants_f
IMPLICIT NONE
TYPE Wr_Result
   integer :: wr_state
   real (kind=double) :: wt2, angle
	
END TYPE wr_result 

CONTAINS
pure function  Principle_Strain(e) result(p)
      IMPLICIT NONE
      REAL(kind=double), intent(in) :: e(3)
      REAL(kind=double):: 	  p(3)
	p(3) = sqrt((e(1)-e(2))**2 + e(3)**2)
	p(1) =  0.5d00*((e(1)+e(2)) + p(3))
 	p(2) =  0.5d00*((e(1)+e(2)) - p(3))

	p(3) = 0.0 ! WRONG
end function  Principle_Strain

pure function Principle_Stress(stress) result(princ)
      IMPLICIT NONE
      REAL(kind=double), intent(in) :: stress(3)
	  
      REAL(kind=double):: 	  princ(3)

!   locals
      REAL*8 s1,s2,root,phi,t2

      S1=0.5D00*(stress(1)+STRESS(2))
      S2=0.5D00*(stress(1)-STRESS(2))
!   NOTE.. PHI IS TWICE ANGLE OF P S FROM AXIS
      T2=STRESS(3)**2
      ROOT=DSQRT(S2**2+T2)
      princ(1) = s1+Root
      princ(2) = s1-Root

      IF(DABS(S2).GT.1.0D-12) THEN
	      PHI=DATAN2(stress(3),S2)
      ELSE

	      PHI=SIGN(PI/2.0d00,stress(3))
          if(dabs(stress(3)) .lt. 1.0D-12) PHI=0.0D00
      ENDIF
      princ(3)=(0.5D00*PHI)

      END function Principle_Stress

pure function IsLowerPrincipleStressNegative(stress) result(rc)
      IMPLICIT NONE
      REAL(kind=double), intent(in) :: stress(3)
	 logical :: rc, p2gezero
      REAL(kind=double) :: sig1,sig2,sig3
        rc= .false.
	
	 sig1 = 1/0 
	sig1 = stress(1)
	sig2= stress(2)
	sig3= stress(3)

    
!  this is the condition for p2 to be >=0 
	p2gezero = .false.

!	p2gezero = (sig1.eq.0.and.sig2.ge.0.and.sig3.eq.0) &
!	.or. 
 !      (sig1.gt.0.and.((sig2.eq.0.and.sig3.eq.0) &
 !      .or. &
!       (sig2.gt.0.and. &
!       Inequality(-1.*Sqrt(sig1*sig2),LessEqual,sig3,LessEqual,
!     -         Sqrt(sig1*sig2)))))

	rc= .not. p2gezero

END function IsLowerPrincipleStressNegative

FUNCTION d_de_by_de (a,d,e,su) result(dfde)  ! The 3 derivatives d( d(energy/da) / de_n)
!
! called by da_by_de (a)  which CHECKS OK See dade_check.xls
!
!
		IMPLICIT NONE
		REAL (kind= double), INTENT(in)	 :: a	
		REAL (kind= double), intent(in), target, dimension(3,3) :: d
		REAL (kind= double), intent(in), target,  dimension(3) :: e	,su		
		REAL (kind= double), dimension(3) :: dfde

		REAL (kind= double),target :: c,s,c2,s2,c3,s3,c4,s4 ,c5,s5,c6,s6,  denom
		REAL (kind=double)  :: d11,d12,d13,d21,d22,d23,d31,d32,d33,e1,e2,e3
			REAL (kind= double), pointer :: su1,su2,su3, sxx,syy,sxy

		d11= d(1,1) ;	d12= d(1,2) ;	d13=d(1,3)
		d21=d(2,1) ; 	d22=d(2,2) ;	d23=d(2,3)
		d31=d(3,1) ;	d32=d(3,2) ;	d33=d(3,3)
		e1=e(1)    ;	e2=e(2)    ;	e3=e(3)
	!	sxx = d11 * e1 + d12 * e2 + d13 * e3  ;
	!	syy = d21 * e1 + d22 * e2 + d23 * e3  ;
	!	sxy = d31 * e1 + d32 * e2 + d33 * e3  ;
	
	    sxx=>su(1); syy=>su(2); sxy=>su(3)
	    su1=>sxx ;	su2=>syy ;	su3=>sxy		

		c = Cos(a) ;		s = Sin(a) ;    !	s2a = sin(2*a)
		c2 = c**2  ;		s2 = s**2
		c3 = c2*c  ;		s3 = s2*s
		c4 = c2*c2 ;		s4 = s2*s2
		c5 = c4*c  ;		s5 = s4*s   ! guess july 2010 need to verify with mtka
		c6 = c3*c3 ;        s6 = s3*s3
    if(.false.) then  ! trueb gives the old formulation
	denom = (d22*c4 - 2*(d23 + d32)*c3*s + (d12 + d21 + 4*d33)*c2*s2 - 2*(d13 + d31)*c*s3 + d11*s4)**2 !

! numerators only!!!!!!!!!! divide at end

	dfde(1) =  2*((d22*(d13 + d31)*syy + d12*(d22*sxy - (d23 + d32)*syy) + d21*(d22*sxy - (d23 + d32)*syy))*	&
     c**6 + (d12**2*syy + d21**2*syy - 2*d22*(2*d13*sxy + 2*d31*sxy + d11*syy) +								&
     d21*(-(d22*sxx) + 4*d33*syy) + d12*(-(d22*sxx) + 2*(d21 + 2*d33)*syy))*c**5*s + 							&
    (d21*d23*sxx + 3*d22*d31*sxx + d21*d32*sxx - d12**2*sxy - d21**2*sxy + 6*d11*d22*sxy + 						&
    4*d23*d31*sxy + 4*d31*d32*sxy - 4*d21*d33*sxy + 2*d11*d23*syy - 4*d21*d31*syy + 							&
    2*d11*d32*syy - 4*d31*d33*syy + d12*(d23*sxx + d32*sxx - 2*d21*sxy - 4*d33*sxy - 4*d31*syy) + 				&
    d13*(3*d22*sxx - 4*(-(d23*sxy) - d32*sxy + d12*syy + d21*syy + d33*syy)))*c4*								&
    s2 + 2*(d11*(-2*d22*sxx - 4*d23*sxy - 4*d32*sxy + d12*syy + d21*syy) + 										&
    2*(d13 + d31)*(-(d23*sxx) - d32*sxx + d12*sxy + d21*sxy + d13*syy + d31*syy))*c**3*							&
    s**3 + (-4*(d13 + d31)*(-(d33*sxx) + (d13 + d31)*sxy) + 													&
    d11*(6*d23*sxx + 6*d32*sxx - d12*sxy - d21*sxy + 8*d33*sxy - 5*d13*syy - 5*d31*syy))*						&
    c2*s4 + d11*(-(d12*sxx) - d21*sxx - 8*d33*sxx + 4*d13*sxy + 4*d31*sxy + 									&
    2*d11*syy)*c*s**5 + d11*(d13*sxx + d31*sxx - 2*d11*sxy)*s**6)						


	dfde(2) =    2*(d22*(2*d22*sxy - (d23 + d32)*syy)*c**6 + 													&
	d22*(-2*d22*sxx - 4*d23*sxy - 4*d32*sxy + d12*syy + d21*syy + 8*d33*syy)*c**5*s + 							&
	(d22*(5*d23*sxx + 5*d32*sxx + d12*sxy + d21*sxy - 8*d33*sxy - 6*d13*syy - 6*d31*syy) + 						&
	4*(d23 + d32)*(d23*sxy + d32*sxy - d33*syy))*c4*s2 - 														&
	2*(d12*(d22*sxx + 2*(d23 + d32)*sxy) + d21*(d22*sxx + 2*(d23 + d32)*sxy) - 									&
	2*(-(d23**2*sxx) - d32**2*sxx + (d13 + d31)*d32*syy + 														&
	d22*(2*d13*sxy + 2*d31*sxy + d11*syy) + d23*(-2*d32*sxx + (d13 + d31)*syy)))*c**3*							&
	s**3 - (-4*d21*d23*sxx + 2*d22*d31*sxx - 4*d21*d32*sxx - 4*d23*d33*sxx - 									&
	4*d32*d33*sxx - d12**2*sxy - d21**2*sxy + 6*d11*d22*sxy + 4*d23*d31*sxy + 4*d31*d32*sxy - 					&
	4*d21*d33*sxy + 3*d11*d23*syy + d21*d31*syy + 3*d11*d32*syy + 												&
	d13*(2*d22*sxx + 4*d23*sxy + 4*d32*sxy + d12*syy + d21*syy) - 												&
	d12*(4*d23*sxx + 4*d32*sxx + 2*d21*sxy + 4*d33*sxy - d31*syy))*c2*s4 + 										&
	(-(d12**2*sxx) - d21**2*sxx + 2*d11*d22*sxx - 4*d21*d33*sxx + 4*d11*d23*sxy + 4*d11*d32*sxy + 				&
	d11*d21*syy + d12*(-2*d21*sxx - 4*d33*sxx + d11*syy))*c*s**5 + 												&
	(d13*d21*sxx - d11*d23*sxx + d21*d31*sxx - d11*d32*sxx - d11*d21*sxy + 										&
	d12*(d13*sxx + d31*sxx - d11*sxy))*s**6) 

	dfde(3) = 2*((-((d23 + d32)**2*syy) + d22*(d23*sxy + d32*sxy + 2*d33*syy))*c**6 + 							&
	((d23 + d32)*(d12 + d21 + 4*d33)*syy - d22*(d23*sxx + d32*sxx + 8*d33*sxy + d13*syy + d31*syy))*c**5*s + 	&
	(d23**2*sxx + d32**2*sxx + 6*d22*d33*sxx + 3*d13*d22*sxy + 3*d22*d31*sxy - 2*d12*d33*syy - 					&
	2*d21*d33*syy - 8*d33**2*syy + d23*(2*d32*sxx - d12*sxy - d21*sxy + 4*d33*sxy - 2*d13*syy - 2*d31*syy) - 	&
	d32*(d12*sxy + d21*sxy - 4*d33*sxy + 2*d13*syy + 2*d31*syy))*c4*s2 - 										&
	2*(d13*d22*sxx + d22*d31*sxx + 4*d23*d33*sxx + 4*d32*d33*sxx - d11*d23*syy - d11*d32*syy - 					&
	4*d13*d33*syy - 4*d31*d33*syy)*c**3*s**3 + 																	&
	(2*d23*d31*sxx + 2*d31*d32*sxx + 2*d12*d33*sxx + 2*d21*d33*sxx + 8*d33**2*sxx - 							&
	3*d11*d23*sxy + d12*d31*sxy + d21*d31*sxy - 3*d11*d32*sxy - 4*d31*d33*sxy - d13**2*syy - 					&
	d31**2*syy - 6*d11*d33*syy + d13*(2*d23*sxx + 2*d32*sxx + d12*sxy + d21*sxy - 4*d33*sxy - 2*d31*syy))*c2*	&
	s4 + (d11*d23*sxx - d21*d31*sxx - d12*(d13 + d31)*sxx + d11*d32*sxx - 4*d31*d33*sxx + 						&
	8*d11*d33*sxy + d11*d31*syy + d13*(-(d21*sxx) - 4*d33*sxx + d11*syy))*c*s**5 + 								&
	(d13**2*sxx + 2*d13*d31*sxx + d31**2*sxx - 2*d11*d33*sxx - d11*d13*sxy - d11*d31*sxy)*	s**6)  

	dfde = dfde/denom
    else  ! nov 2006 formulation
    su1=>sxx; su2=>syy; su3=>sxy
    denom =  (d22*c4 - 2.*(d23 + d32)*c3* s + (d12 + d21 + 4.*d33)*c2*s2 - 2.*(d13 + d31)*c*s3 + d11*s4)**2
!
    dfde(1) =  2.*(d13*d22*su2 - d12*(d23 + d32)*su2 + d12*d22*su3)*c6 +                       &
     2.*(d12**2.*su2 + d12*(-(d22*su1) + (d21 + 4.*d33)*su2) - d22*(d11*su2 + 4.*d13*su3))*c5*Sin(a) +              &
      2.*(d12*d23*su1 + d12*d32*su1 + d11*d23*su2 - 3*d12*d31*su2 + d11*d32*su2 - (-3*d11*d22 + d12*(d12 + d21 + 4.*d33))*su3 +              &
        d13*(3*d22*su1 - (4.*d12 + d21 + 4.*d33)*su2 + 4.*(d23 + d32)*su3))*c4*s2 +              &
     4.*(-2.*d13*(d23 + d32)*su1 + 2.*d13*(d13 + d31)*su2 + 2.*d12*(d13 + d31)*su3 - d11*(d22*su1 - d12*su2 + 2.*(d23 + d32)*su3))*             &
      c3*s3 + 2.*(-(d12*d31*su1) - 4.*d13**2.*su3 + d13*(d21*su1 + 4.*d33*su1 - 4.*d11*su2 - 4.*d31*su3) +              &
        d11*(3*(d23 + d32)*su1 - d31*su2 + (-2.*d12 + d21 + 4.*d33)*su3))*c2*s4 +              &
      2.*d11*(-((d21 + 4.*d33)*su1) + d11*su2 + 4.*d13*su3)*Cos(a)*s5 + 2.*d11*(d31*su1 - d11*su3)*s6             

    dfde(2) =   2.*d22*(-(d32*su2) + d22*su3)*c6 + 2.*d22*(-(d22*su1) + d12*su2 + 4.*d33*su2 - 4.*d23*su3)*c5*Sin(a) +                  &
      2.*(-(d12*d23*su2) + (d21*d32 - 4.*d23*d33)*su2 + 4.*d23*(d23 + d32)*su3 +                  &
        d22*(4.*d23*su1 + d32*su1 - 3*(d13 + d31)*su2 - (d12 - 2.*d21 + 4.*d33)*su3))*c4*s2 +                  &
      4.*(-(d21*d22*su1) - 2.*d23*(d23 + d32)*su1 + d11*d22*su2 + 2.*d23*(d13 + d31)*su2 + 2.*d22*(d13 + d31)*su3 -                  &
        2.*d21*(d23 + d32)*su3)*c3*s3 +                  &
     2.*(-(d13*d22*su1) + d12*d23*su1 + 4.*d21*d23*su1 - d22*d31*su1 + 3*d21*d32*su1 + 4.*d23*d33*su1 - d13*d21*su2 - 3*d11*d23*su2 -                  &
         d21*d31*su2 + (-3*d11*d22 - 4.*d23*(d13 + d31) + d21*(d12 + d21 + 4.*d33))*su3)*c2*s4 +                  &
      2.*(-(d12*d21*su1) - d21**2.*su1 + d11*d22*su1 - 4.*d21*d33*su1 + d11*d21*su2 + 4.*d11*d23*su3)*Cos(a)*s5 +                  &
      2.*(d13*d21*su1 + d21*d31*su1 - d11*(d23*su1 + d21*su3))*s6

    dfde(3) =    2.*(-((d32*(d23 + d32) - d22*d33)*su2) + d22*d32*su3)*c6 +                  &
       2.*(d32*(d12 + d21 + 4.*d33)*su2 - d22*(d32*su1 + d31*su2 + 4.*d33*su3))*c5*Sin(a) +                  &
       2.*(d32**2.*su1 + 3*d22*d33*su1 - d33*(d12 + d21 + 4.*d33)*su2 + 3*d22*d31*su3 - d32*(3*d13*su2 + 2.*d31*su2 + (d12 + d21)*su3) +                  &
          d23*(d32*su1 + d31*su2 + 4.*d33*su3))*c4*s2 +                  &
       4.*(-(d22*d31*su1) - 2.*d32*d33*su1 + d11*d32*su2 + 2.*d13*d33*su2 + 2.*d31*d33*su2 + 2.*d13*d32*su3 - 2.*d23*(d33*su1 + d31*su3))*                 &
        c3*s3 + 2.*(3*d23*d31*su1 + 2.*d31*d32*su1 + d12*d33*su1 + d21*d33*su1 + 4.*d33**2.*su1 - d31**2.*su2 -                  &
         3*d11*d33*su2 + d12*d31*su3 + d21*d31*su3 - 3*d11*d32*su3 - d13*(d32*su1 + d31*su2 + 4.*d33*su3))*c2*s4 +                  &
       2.*(-(d12*d31*su1) - d21*d31*su1 + d11*d32*su1 - 4.*d31*d33*su1 + d11*d31*su2 + 4.*d11*d33*su3)*Cos(a)*s5 +                  &
       2.*((d31*(d13 + d31) - d11*d33)*su1 - d11*d31*su3)*s6

    	dfde = dfde/denom
    endif
  
	end FUNCTION d_de_by_de 

pure FUNCTION dws_by_de (a,d,e) result(dw)  ! NOT TESTED
!
!	Calculates d(wt2) by d(e[i]) on the basis that the angle a is constant
!
!
IMPLICIT NONE
		REAL (kind= double), INTENT(in)	 :: a
		REAL (kind= double), intent(in), dimension(3,3) :: d
		REAL (kind= double), intent(in), dimension(3) :: e			
		REAL (kind= double), dimension(3) :: dw

		REAL (kind= double) :: c,s,c2,s2,c3,s3,c4,s4 , s2a , denom
		REAL (kind=double) :: d11,d12,d13,d21,d22,d23,d31,d32,d33,e1,e2,e3

		d11=d(1,1) ;	d12=d(1,2) ;	d13=d(1,3)
		d21=d(2,1) ; 	d22=d(2,2) ;	d23=d(2,3)
		d31=d(3,1) ;	d32=d(3,2) ;	d33=d(3,3)
		e1=e(1)    ;	e2=e(2)    ;	e3=e(3)

		c = Cos(a) 
		s = Sin(a) ;  s2a = sin(2*a)
		c2 = c**2
		s2 = s**2
		c3 = c2*c
		s3 = s2*s
		c4 = c2*c2
		s4 = s2*s2

		denom= d22*C4 - 2*(d23 + d32)*C3*S + (d12 + d21 + 4*d33)*C2*S2 - 2*(d13 + d31)*C*S3 + d11*S4

! -(d21*Cos(a)**2) - d11*Sin(a)**2 + d31*Sin(2*a)
	dw(1) = (-(d21*c2) - d11*s2  + d31*s2a)/ denom

 !-(d22*Cos(a)**2) - d12*Sin(a)**2 + d32*Sin(2*a)
	dw(2) =  (-(d22*c2) - d12*s2 + d32*s2a)/ denom

 ! -(d23*Cos(a)**2) + 2*d33*Cos(a)*Sin(a) - d13*Sin(a)**2
 	 dw(3) = (-(d23*C2) + 2*d33*C*S - d13*S2   )  / denom
  
	end FUNCTION dws_by_de

SUBROUTINE Stress_to_princ(stress,princ)
      IMPLICIT NONE
      REAL*8 stress(3),princ(3)
!   locals
      REAL*8 s1,s2,root,phi,t2

      S1=0.5D00*(stress(1)+STRESS(2))
      S2=0.5D00*(stress(1)-STRESS(2))
!   NOTE.. PHI IS TWICE ANGLE OF P S FROM AXIS
      T2=STRESS(3)**2
      ROOT=DSQRT(S2**2+T2)
      princ(1) = s1+Root
      princ(2) = s1-Root

      IF(DABS(S2).GT.1.0D-12) THEN
	      PHI=DATAN2(stress(3),S2)
      ELSE

	      PHI=SIGN(PI/2.0d00,stress(3))
                     if(dabs(stress(3)) .lt. 1.0D-12) PHI=0.0D00
      ENDIF
      princ(3)=(0.5D00*PHI)

END SUBROUTINE Stress_to_princ

 

!  Expressions from Mathematica 

pure function wrinkle_J(a) RESULT (J)

	IMPLICIT NONE
	REAL (kind= double), dimension(3,3) :: j
	REAL (kind= double), INTENT(in)	 :: a
  
		j(1,1) = Cos(a)**2;		j(1,2) = Sin(a)**2;		j(1,3) = 2*Cos(a)*Sin(a)
		j(2,1) =   j(1,2)			! Sin(a)**2
		j(2,2) =   j(1,1) 			! Cos(a)**2
		j(2,3) =  -j(1,3)			! -2*Cos(a)*Sin(a)

		 j(3,1) = 0.5D00*j(2,3)		! -Cos(a)*Sin(a)
		 j(3,2) = -j(3,1)			!Cos(a)*Sin(a)
		 j(3,3) =j(1,1)-j(1,2)		!Cos(a)**2 - Sin(a)**2

end function wrinkle_J



pure function wrinkle_Jd(a) RESULT (J)

	IMPLICIT NONE

	REAL (kind= double), dimension(3,3) :: j

	REAL (kind= double), INTENT(in)	 :: a

 

	 j(1,1) = -2*Cos(a)*Sin(a)
	 j(1,2) =    Sin(2*a)
	 j(1,3) =  2*Cos(2*a)


    	 j(2,1) = j(1,2) !					 Sin(2*a)
	 j(2,2) = j(1,1) !					-2*Cos(a)*Sin(a)
	 j(2,3) = - j(1,3) !					-2*Cos(2*a)


    	 j(3,1) = -0.5D00*j(1,3) !				- Cos(2*a)
	 j(3,2) = - j(3,1)			!		 Cos(2*a)
	 j(3,3) = 2.0D0* j(1,1)	!				-4*Cos(a)*Sin(a)

end function wrinkle_Jd


pure function wrinkle_Jdd(a) RESULT (J)
	IMPLICIT NONE
	REAL (kind= double), dimension(3,3) :: j
	REAL (kind= double), INTENT(in)	 :: a

	 j(1,1) = -2*Cos(2*a);		j(1,2) = -j(1,1);			j(1,3) = -4.0*Sin(2*a)
   	 j(2,1) =  -j(1,1);		j(2,2) =  j(1,1);			j(2,3) = -j(1,3)
   	 j(3,1) =  4*Cos(a)*Sin(a);	j(3,2) = -j(3,1);			j(3,3) = 2.0* j(1,1) 


end function wrinkle_Jdd

integer function wt_roots(sxxo,syyo,sxyo,results ) result(n)
		implicit none
		real( kind=double), intent(in) :: sxxo,syyo,sxyo
		real (kind= double), intent (out), dimension(4) :: results

! This is the solution for the roots of wt
! We got this expression by going TrigToExp before solving.  Otherwise
! Mathematica kept on giving solutions in arccos so there were twice as many. 

	complex (kind=rxquad) ::  denom, csq, a,b,c,x1,x2
	complex (kind= rxquad),  dimension(4) :: roots
	real (kind=rxquad) :: dummy,small ! careful. basictypes defines this as 8
	integer i
	n=0

	results = 0.0d00
	a = dcmplx(syyo ,0.0d00)  ! was qcmplx
	b = dcmplx(sxxo ,0.0d00)  
	c = dcmplx(sxyo ,0.0d00) 
!	denom   = qcmplx(syyo - sxxo , 2.0d00 * sxyo) 
!	csq = CqSqrt(a * b - c**2)
	denom   = dcmplx(syyo - sxxo , 2.0d00 * sxyo) 
	csq = CdSqrt(a * b - c**2)

	x1 = (- a - b -  2.0d00*csq )
	x2 = (- a - b +  2.0d00*csq)
	if(abs(denom) > 0.0) then
		x1 = CdSqrt(x1 /denom) 
		x2 = CdSqrt(x2 /denom)
	else
		x1=0.0d00; x2=0.0d00
		results = 0.0d00
		return
	endif
	if(x1 .eq. x2) then
		write(outputunit,*) ' coincident roots ',x1
		!pause
	endif
! csq zero means syyo *sxxo - sxyo**2 is zero

	if( abs(syyo *sxxo - sxyo**2 ) .lt. 1.0D-15) then
	!	write(outputunit,*) ' coincident roots'
	!	pause
	endif


	roots(1) = dcmplx(0.0d00,-1.0d00) * CdLog(-x1) ! was qcmplx and cqlog
	roots(2) = dcmplx(0.0d00,-1.0d00) * CdLog( x1)
	roots(3) = dcmplx(0.0d00,-1.0d00) * CdLog(-x2)
	roots(4) = dcmplx(0.0d00,-1.0d00) * CdLog( x2)



!	roots(1) = dcmplx(0.0d00,-1.0d00) * CdLog(-CdSqrt(- a/denom - b/denom -  2.0d00*csq /denom))
!	roots(2) = dcmplx(0.0d00,-1.0d00) * CdLog( CdSqrt(- a/denom - b/denom -  2.0d00*csq /denom))

!	roots(3) = dcmplx(0.0d00,-1.0d00) * CdLog(-CdSqrt(- a/denom - b/denom +  2.0d00*csq /denom))
!	roots(4) = dcmplx(0.0d00,-1.0d00) * CdLog( CdSqrt(- a/denom - b/denom +  2.0d00*csq /denom))


!If any roots have non-zero imaginary components,  there is no wrinkling.
! lets count the roots that have zero IM
	small = 1.0D-15 ! 10.0*tiny(dummy)   ! change in linux.  We seem not to capture double-wrinkle cleanly
	do i=1,4
		dummy = abs(imag(roots(i))) ! dummy = qabs(qimag(roots(i)))
		if(  dummy .lt.  small ) then ! a real root
			n=n+1     
		endif
	enddo

!  here there are 4 real roots
!  OF these 4 roots, only 2 will be in the range -pi/2 to pi/2   

!	if (n .ne. 0 .and. n .ne. 4) then
!		write(outputunit,*) ' Wierd number of roots ', N
!		write(outputunit,*) roots
!		
!	endif

	do i=1,4
			results(i) = real(roots(i),double )  
	enddo
!
!	its slow to have this here but it lets us check for near-double roots. 
	call wr_sort(results,4)


end function wt_roots


 FUNCTION wt (t,d,e,s) result(r)
	IMPLICIT NONE
	REAL (kind= double), INTENT(in)	 :: t
	REAL (kind= double), intent(in), dimension(3,3),target :: d
	REAL (kind= double), intent(in), dimension(3),target :: e,s
		
	REAL (kind= double)  :: r !  sxxo,syyo,sxyo
!	REAL (kind=double), dimension(3) :: s 
	REAL (kind=double) :: d11,d12,d13,d21,d22,d23,d31,d32,d33,e1,e2,e3
	REAL (kind= double)  :: ct,ct2,ct3,ct4,st,st2,st3,st4
	ct = Cos(t); ct2 = ct**2; ct3 = ct*ct2; ct4 = ct2*ct2
	st = Sin(t); st2 = st**2; st3 = st*st2; st4 = st2*st2

	d11=d(1,1);d12=d(1,2);d13=d(1,3);
	d21=d(2,1);d22=d(2,2);d23=d(2,3);
	d31=d(3,1);d32=d(3,2);d33=d(3,3);
	e1=e(1);	e2=e(2);	e3=e(3);

!	s = matmul(d,e)  ! sxxo-> s(1); syyo->s(2) sxyo->s(3)

      r = (-(s(2)*ct2) - s(1)*st2 + s(3)*Sin(2*t))/					&
      (d22*ct4 - 2*(d23 + d32)*ct3*st + (d12 + d21 + 4*d33)*ct2*st2 -	&
      2*(d13 + d31)*ct*st3 + d11*st4)

end FUNCTION wt



pure  FUNCTION dwt(t,d,e) result (r) ! SLOW

	IMPLICIT NONE

	REAL (kind= double), INTENT(in)	 :: t
	REAL (kind= double), intent(in),  dimension(3,3) :: d
	REAL (kind= double), intent(in),  dimension(3) :: e	!,s0 ! s0 is D E
	REAL (kind= double)  :: r, sxxo,syyo,sxyo,ct,st,denom,c4, c3s1,c2s2,c1s3,s4 
	REAL (kind=double)   :: d11,d12,d13,d21,d22,d23,d31,d32,d33,l_e1,l_e2,l_e3

	d11=d(1,1);	d12=d(1,2);	d13=d(1,3)
	d21=d(2,1);	d22=d(2,2);	d23=d(2,3)
	d31=d(3,1);	d32=d(3,2);	d33=d(3,3)
	l_e1=e(1);	l_e2=e(2);	l_e3=e(3)

	sxxo = d11 * l_e1 + d12 * l_e2 + d13 * l_e3  
	syyo = d21 * l_e1 + d22 * l_e2 + d23 * l_e3  
	sxyo = d31 * l_e1 + d32 * l_e2 + d33 * l_e3 
!	sxxo = s0(1); syyo=s0(2); sxyo = s0(3);

	ct = Cos(t)  
	st = Sin(t)  ; c4  = ct**4  ; c3s1 = ct**3*st ;c2s2= (ct*st)**2 ; 
	c1s3=  ct * st**3 ; s4=  st**4

	denom= (d22*c4 - 2*(d23+d32)*c3s1 + (d12+d21+4*d33)*c2s2 - 2*(d13+d31)*c1s3 + d11*s4)**2


	if(abs(denom) > tiny(denom)) then
        r =         (-2*((-d23 - d32)*c4 + (d12 + d21 - 2*d22 + 4*d33)*c3s1 - 			&
        3*(d13 - d23 + d31 - d32)*c2s2 + (2*d11 - d12 - d21 - 4*d33)*c1s3 + 		&
        (d13 + d31)*s4)*(-(syyo*ct**2) - sxxo*st**2 + sxyo*Sin(2*t)) + 						&
        (d22*c4 - 2*(d23 + d32)*c3s1 + (d12 + d21 + 4*d33)*c2s2 - 			&
        2*(d13 + d31)*c1s3 + d11*s4)*(2*sxyo*Cos(2*t) + (-sxxo + syyo)*Sin(2*t)))/denom
	else
		!write(outputunit,*) 'zero denom in dwt'
		r=0.0d00
	endif
end function dwt


pure FUNCTION ddwt(t,d,e) result(res)  ! SLOW
	IMPLICIT NONE
	REAL (kind= double), INTENT(in)	 :: t
	REAL (kind= double), intent(in),  dimension(3,3) :: d
	REAL (kind= double), intent(in),  dimension(3) :: e	
	REAL (kind= double)  :: res	
	
	REAL (kind= double)  :: sxxo,syyo,sxyo
	REAL (kind=double) :: d11,d12,d13,d21,d22,d23,d31,d32,d33,e1,e2,e3
	REAL (kind= double)  :: ct,st,c2t,s2t

	d11=d(1,1);	d12=d(1,2);	d13=d(1,3);
	d21=d(2,1);	d22=d(2,2);	d23=d(2,3);
	d31=d(3,1);	d32=d(3,2);	d33=d(3,3);
	e1=e(1);	e2=e(2);	e3=e(3)

	sxxo = d11 * e1 + d12 * e2 + d13 * e3  
	syyo = d21 * e1 + d22 * e2 + d23 * e3  
	sxyo = d31 * e1 + d32 * e2 + d33 * e3 
	ct = Cos(t)
	st = Sin(t)
	c2t = Cos(2*t)
	s2t = Sin(2*t)
         res = (-2*(2*sxyo*C2t - 2*sxxo*ct*st + 2*syyo*ct*st)*							&
          (-2*(d23 + d32)*ct**4 - 4*d22*ct**3*st + 2*(d12 + d21 + 4*d33)*ct**3*st -			&
            6*(d13 + d31)*ct**2*st**2 + 6*(d23 + d32)*ct**2*st**2 + 4*d11*ct*st**3 -	&
            2*(d12 + d21 + 4*d33)*ct*st**3 + 2*(d13 + d31)*st**4))/									&
        (d22*ct**4 - 2*(d23 + d32)*ct**3*st + (d12 + d21 + 4*d33)*ct**2*st**2 - 			&
           2*(d13 + d31)*ct*st**3 + d11*st**4)**2 + 												&
       (-(sxxo*(2*ct**2 - 2*st**2)) - syyo*(-2*ct**2 + 2*st**2) - 4*sxyo*Sin(2*t))/				&
        (d22*ct**4 - 2*(d23 + d32)*ct**3*st + (d12 + d21 + 4*d33)*ct**2*st**2 - 			&
          2*(d13 + d31)*ct*st**3 + d11*st**4) + 													&
       ((2*(-2*(d23 + d32)*ct**4 - 4*d22*ct**3*st + 2*(d12 + d21 + 4*d33)*ct**3*st - 		&
       6*(d13 + d31)*ct**2*st**2 + 6*(d23 + d32)*ct**2*st**2 + 4*d11*ct*st**3 - 		&
                2*(d12 + d21 + 4*d33)*ct*st**3 + 2*(d13 + d31)*st**4)**2)/							&
           (d22*ct**4 - 2*(d23 + d32)*ct**3*st + (d12 + d21 + 4*d33)*ct**2*st**2 - 			&
              2*(d13 + d31)*ct*st**3 + d11*st**4)**3 - 												&
          (-4*d22*ct**4 + 2*(d12 + d21 + 4*d33)*ct**4 - 12*(d13 + d31)*ct**3*st + 				&
             20*(d23 + d32)*ct**3*st + 12*d11*ct**2*st**2 + 12*d22*ct**2*st**2 - 		&
             12*(d12 + d21 + 4*d33)*ct**2*st**2 + 20*(d13 + d31)*ct*st**3 - 					&
             12*(d23 + d32)*ct*st**3 - 4*d11*st**4 + 2*(d12 + d21 + 4*d33)*st**4)/				&
           (d22*ct**4 - 2*(d23 + d32)*ct**3*st + (d12 + d21 + 4*d33)*ct**2*st**2 - 			&
       2*(d13 + d31)*ct*st**3 + d11*st**4)**2)*(-(syyo*ct**2)-sxxo*st**2 +sxyo*S2t)
 

end function ddwt 

 
function Min_Energy_Angle(ain,d,e,su) result(a)
implicit none
 	REAL (kind= double), INTENT(in):: ain
 	REAL (kind= double), intent(in),  dimension(3,3) :: d
	REAL (kind= double), intent(in),  dimension(3) :: e	,su

! CAREFUL  this ASSUMES that any minimum of E we find is valid 
!
	real(kind=double) :: ed, edd, err,tol, inc, delta, a, l_damp
	integer :: count, jumpflag,c2

	a=ain

	inc = Pi/12

	tol = 1.00D-12  ! 1D-15 is getting close to rounding. doesnt always get there
	count = 0 ;	jumpflag = 0

! a reliable method.
!	given a, get e', e'' 
!   if BOTH <= 0, inc Pi/12  and set Flag JUMP
! if  e' >=0 and e'' <=0 we are HI.  decrement by Pi/12 and set JUMP
!  if e''>0 we can N-R with step limited to Pi/12. If we limit the step, set JUMP.
!
!	If we get two Jumps in succession and they are opposing, halve the jump interval. 
	c2=5; 	l_damp = 1.0
	err = tol * 2
	
c2do:	do while(c2 >0) 
	a = ain

cdo:	do while (err > tol .and. count < 100)

		ed = de(a,d,e,su)
		edd = dde(a,d,e,su)
		if((ed <=0 .and. edd <= 0)  ) then	! lower sector.
			if (jumpflag < 0) then  ! just jumped down
				inc = inc/2
			endif
			jumpflag = 1
			a = a + inc
		else if (ed >=0 .and. edd <=0)  then ! upper sector
			if (jumpflag > 0) then ! just jumped up
				inc = inc/2
			endif
			jumpflag = -1
			a = a - inc
		else if (edd > 0) then  ! middle sector
			delta = - ed/edd
			if(delta > inc) then
				delta = inc
				jumpflag = 1
			else if (delta < -inc) then
				delta = -inc
				jumpflag = -1
			endif
			a = a + delta *l_damp
			err = abs(delta)
		else ! unknown case
			!write(outputunit,*) ' unknown wrinkle case'
			!pause
		endif
		count = count + 1
		if(err < tol) exit c2do
	enddo cdo
	l_damp = l_damp / 2.0; c2 = c2 -1
	!write(outputunit,*) c2, ' halving damp to ',l_damp, 'with err = ',err
	enddo c2do
!	if(c2 <=0) write(outputunit,*) 'Min Energy angle NOT converged ', sngl(err)

end function Min_Energy_Angle

	function wrinkling_angle(a,lwr,upr,d,e,su) result (ok)
	implicit none
 	REAL (kind= double), INTENT(in out):: a,lwr,upr
 	REAL (kind= double), intent(in),  dimension(3,3) :: d
	REAL (kind= double), intent(in),  dimension(3) :: e	,su

! CAREFUL  this ASSUMES that LWR and UPR are set correctly and that there is a minimum of E between them.
!
	real(kind=double) ed, edd, err,tol, inc, delta
	integer :: count, jumpflag,ok

	inc = min(Pi/16.d0, (upr-lwr)/4.0d00)

	tol = 1.00D-12  ! 1D-8 wasnt stable 1D-15 is getting close to rounding
	count = 0 ;	jumpflag = 0

! a reliable method.
!	given a, get e', e'' 
!   if BOTH <= 0, inc Pi/12  and set Flag JUMP
! if  e' >=0 and e'' <=0 we are HI.  decrement by Pi/12 and set JUMP
!  if e''>0 we can N-R with step limited to Pi/12. If we limit the step, set JUMP.
!
!	If we get two Jumps in succession and they are opposing, halve the jump interval. 
		ok = 1
	err = tol * 2

	do while (err > tol .and. count < 100)

		ed = de(a,d,e,su)
		edd = dde(a,d,e,su)
		if( a <= lwr) then
			if (jumpflag < 0) then  ! just jumped down
				inc = inc/2
			endif
			jumpflag = 1
			a = lwr + inc
		else if ( a >= upr) then ! upper sector
			if (jumpflag > 0) then ! just jumped up
				inc = inc/2
			endif
			jumpflag = -1
			a = upr - inc		
		else if((ed <=0 .and. edd <= 0)  ) then	! lower sector.
			if (jumpflag < 0) then  ! just jumped down
				inc = inc/2
			endif
			jumpflag = 1
			a = a + inc
		else if (ed >=0 .and. edd <=0)  then ! upper sector
			if (jumpflag > 0) then ! just jumped up
				inc = inc/2
			endif
			jumpflag = -1
			a = a - inc
		else if (edd > 0) then  ! middle sector
			delta = - ed/edd
			if(delta > inc) then
				delta = inc
				jumpflag = 1
			else if (delta < -inc) then
				delta = -inc
				jumpflag = -1
			endif
			a = a + delta
			err = abs(delta)
		else ! unknown case
			write(outputunit,*) ' unknown wrinkle case'
			!pause
		endif
		count = count + 1
	enddo
	if(count >= 100) then
!		 write(outputunit,*) 'wr angle NOT converged ', sngl(err),sngl(lwr),sngl(upr)
		ok = 0
	endif
end function  wrinkling_angle

function wrinkle_e(a,d,e,su) result (r)
implicit none
 	REAL (kind= double), INTENT(in):: a
	REAL (kind= double), dimension(3,3) :: Jt
	REAL (kind= double), dimension(3) :: w, x
	REAL (kind= double), intent(in),  dimension(3,3) :: d
	REAL (kind= double), intent(in),  dimension(3) :: e	,su
	real(kind=double) :: r

! E = D (e + Jt w) dot (e + Jt w)
! where w = (0,wt2,0)
	w=0 ; 	w(2) = wt(a,d,e,su)

	jt = Transpose(wrinkle_j(a))
	x =  matmul(Jt,w)
	x =x + e
	r = dot_product(matmul(d,x),x)
end function wrinkle_e

 function de(a,d,e,su) result(r)
implicit none
 	REAL (kind= double), INTENT(in):: a
 	REAL (kind= double), intent(in),  dimension(3,3) :: d
	REAL (kind= double), intent(in),  dimension(3) :: e	,su
	REAL (kind= double), dimension(3,3) :: Jt,Jtd
	REAL (kind= double), dimension(3) :: w,wd, x,y
	real(kind=double) :: r
	! 2 * D (E + Jt w) (Jt' wt + Jt wt')

	w=0;	w(2) = wt(a,d,e,su)
	jt = Transpose(wrinkle_j(a))
	wd=0;	wd(2) = dwt(a,d,e)
	jtd = Transpose(wrinkle_jd(a))

	x =  matmul(Jt,w) + e  
	y = matmul(jtd,w) + matmul(jt,wd)
	r = 2.* dot_product(matmul(d,x),y)
end function de

function dde(a,d,e,su) result (r)
    implicit none
 	REAL (kind= double), INTENT(in):: a
 	REAL (kind= double), intent(in),  dimension(3,3) :: d
	REAL (kind= double), intent(in),  dimension(3) :: e	,su
	real(kind=double) :: r
	REAL (kind= double), dimension(3,3) :: Jt,Jtd,Jtdd
	REAL (kind= double), dimension(3) :: w,wd,wdd, x,y,z
	! D (e + Jt w) ( 2 Jt' w' + w Jt'' + Jt w'')	X * Y
	! + 2D (w Jt' + Jt w')^2						Z * Z
	! + D (e + Jt w)(2 Jt' w' + w Jt'' + Jt w'') 	X * Y

	w=0 ;	w(2) = wt(a,d,e,su)

	jt = Transpose(wrinkle_j(a))
	wd=0;	wd(2) = dwt(a,d,e)
	jtd = Transpose(wrinkle_jd(a))

	wdd=0;	wdd(2) = ddwt(a,d,e)
	jtdd = Transpose(wrinkle_jdd(a))

	x =  matmul(Jt,w) + e  
	y = 2*matmul(Jtd,wd) + matmul(Jtdd,w) + matmul(Jt,wdd)
	z = matmul(Jtd,w) + matmul(Jt,wd)
	r = 2*(dot_product(matmul(d,x),y) +  dot_product(matmul(D,z),z))

end function dde

subroutine wr_sort(	a,n)
	implicit none
	REAL (kind= double), dimension(4) :: a
	INTEGER ,intent (in) :: n
	integer i
	logical changing
	REAL (kind= double) b
	

!			sort this array

	changing = .true.	
	do while (changing)
	changing = .false. 
	do i=1,n-1
		if(a(i) .gt. a(i+1)) then
			changing = .true.
			b = a(i+1) 
			a(i+1) = a(i)
			a(i) = b
		endif

	enddo
	enddo


end subroutine wr_sort


function find_a_domain(rootsin,nroots, lwr,upr,angle,d,e) result(ok)
! 	USE DFLIB only for compaq fortran
 	implicit none
		REAL (kind= double),intent (in), dimension(4) :: rootsin
		INTEGER ,intent (in) :: nroots
		REAL (kind= double), intent(in),  dimension(3,3) :: d
	    REAL (kind= double), intent(in),  dimension(3) :: e	
		REAL (kind= double),intent (out) :: lwr,upr
		REAL (kind= double),intent (in out) :: angle
		integer i,ok,i2
		REAL (kind= double), dimension(4) :: roots, s
!	 	REAL (kind= double) :: a
	!	integer fc_isnan_d    !for lahey not compag

!  sort the 4 roots.  to find a region where wt is Positive

	roots = rootsin
	call wr_sort(roots,nroots)! now in wt_roots but leave here for safety

! the first root with slope > 0 is lwr
! then the next root is upr. 

	do i=1,nroots
		s(i) = dwt(roots(i),d,e) 
! 		in lahey this is 0 .ne. fc_isnan_d  not isnan
!DEC$ IF DEFINED (compaq) 
!		if( isnan(s(i))) then		! for instance, unidirectional materials.
!			ok=0
!			return
!		endif
!DEC$ ELSE 
		if( 0 .ne. fc_isnan_d (s(i))) then		! for instance, unidirectional materials.
			ok=0
			return
		endif
!DEC$ ENDIF 

	enddo

! we now have the gradients s(i) at roots roots(i)
! first check if the existing angle is between a roots with +ve slope and one with -ve.
! if it is, return lwr, upr, and the old angle as a seed
! if it isnt, go and get the first hump

	do i = 1,nroots -1 
		if(s(i) > 0.0) then
			lwr = roots(i)
			if(s(i+1) < 0) then
				upr=roots(i+1)
				if(angle > lwr .and. angle < upr) then ! use existing angle
					ok=1
					return 
				endif
			endif
		endif
	enddo


	do i = 1,nroots -1 
		if(s(i) > 0.0) then
			lwr = roots(i)
			
			i2 = i + 1
			if(s(i2) < 0) then
				ok=1
				upr=roots(i2)
				angle = 0.5*(lwr + upr)
				return 
			else
				write(outputunit,*) ' consecutive positive roots'
			endif
		endif
	enddo

	ok = 0

	end function find_a_domain

pure function dw_by_da (a,d,e) result (dw)  ! NOT TESTED
		IMPLICIT NONE
		REAL (kind= double), INTENT(in)	 :: a
		REAL (kind= double), intent(in),  dimension(3,3) :: d
	    REAL (kind= double), intent(in),  dimension(3) :: e	
		REAL (kind= double), dimension(3) :: dw
		dw = 0.0
		dw(2) = dwt(a,d,e)
end function dw_by_da 

function da_by_de (a,d,e,su) result (dade)  ! CHECKED See dade_check.xls
		IMPLICIT NONE
		REAL (kind= double), INTENT(in)	 :: a
		REAL (kind= double), intent(in),  dimension(3,3) :: d
	    REAL (kind= double), intent(in),  dimension(3) :: e	,su
		REAL (kind= double), dimension(3) :: dade, dfde

		dfde = d_de_by_de (a,d,e,su)   ! the 3 derivatives d(debyda), de_n)
		dade = - dfde / dde(a,d,e,su)
end function da_by_de 

function dd_wrinkled (a,d,e,su) result (ddmod)  !  TESTED 
!
!	see calc sheet WD1 et seq 15 june 02	
	
		IMPLICIT NONE
		REAL (kind= double), INTENT(in)		:: a
		REAL (kind= double), intent(in),  dimension(3,3) :: d
	    REAL (kind= double), intent(in),  dimension(3) :: e	,su
		REAL (kind= double), dimension(3,3) :: ddmod, jd,j ,jt,jtd

		REAL (kind= double), dimension(3,3),target :: s
		REAL (kind= double), dimension(3)	::  w,dade,dwda,dwde !v1,v2,v3,
		REAL (kind= double)	,pointer	::  s11,s12,s13,s21,s22,s23,s31,s32,s33
	!	data unity /0,1.0d00,0/
	s11=>s(1,1); s12=>s(1,2); s13=>s(1,3)
	s21=>s(2,1); s22=>s(2,2); s23=>s(2,3)
	s31=>s(3,1); s32=>s(3,2); s33=>s(3,3)
	w = 0; w(2) = wt(a,d,e,su)

	jd  = wrinkle_jd(a) ; jtd = transpose(jd)
	j   = wrinkle_j(a)  ; jt  = transpose(j)
	dade =  da_by_de (a,d,e,su)
	dwda = dw_by_da(a,d,e)  ! this is in alpha coords not global
	dwde = dws_by_de(a,d,e)

10	format(a10,3G15.7)
! 	t1  = Outer_Product(matmul(Transpose(jd),w),dade,3)  this lot is WRONG
!	t2  = Outer_Product(dade, dwda ,3)
!	t2a = Outer_Product(unity,dwde,3)
!	t2  = t2 + t2a
!	t2t = matmul(Transpose(J),t2)
!	t3  = t1 + t2t
!	t4 = matmul(d, t3) 
!	ddmod = d + t4

!
! get t4 the slow way. This checks out 
	
	s11 = d(1,1) * (jt(1,2)*(dwda(2)*dade(1) + dwde(1)) + w(2) * jtd(1,2)* dade(1)) &
		+ d(1,2) * (jt(2,2)*(dwda(2)*dade(1) + dwde(1)) + w(2) * jtd(2,2)* dade(1)) &
		+ d(1,3) * (jt(3,2)*(dwda(2)*dade(1) + dwde(1)) + w(2) * jtd(3,2)* dade(1))

	s12 = d(1,1) * (jt(1,2)*(dwda(2)*dade(2) + dwde(2)) + w(2) * jtd(1,2)* dade(2)) &
		+ d(1,2) * (jt(2,2)*(dwda(2)*dade(2) + dwde(2)) + w(2) * jtd(2,2)* dade(2)) &
		+ d(1,3) * (jt(3,2)*(dwda(2)*dade(2) + dwde(2)) + w(2) * jtd(3,2)* dade(2))

	s13 = d(1,1) * (jt(1,2)*(dwda(2)*dade(3) + dwde(3)) + w(2) * jtd(1,2)* dade(3)) &
		+ d(1,2) * (jt(2,2)*(dwda(2)*dade(3) + dwde(3)) + w(2) * jtd(2,2)* dade(3)) &
		+ d(1,3) * (jt(3,2)*(dwda(2)*dade(3) + dwde(3)) + w(2) * jtd(3,2)* dade(3))

	s21 = d(2,1) * (jt(1,2)*(dwda(2)*dade(1) + dwde(1)) + w(2) * jtd(1,2)* dade(1)) &
		+ d(2,2) * (jt(2,2)*(dwda(2)*dade(1) + dwde(1)) + w(2) * jtd(2,2)* dade(1)) &
		+ d(2,3) * (jt(3,2)*(dwda(2)*dade(1) + dwde(1)) + w(2) * jtd(3,2)* dade(1))

	s22 = d(2,1) * (jt(1,2)*(dwda(2)*dade(2) + dwde(2)) + w(2) * jtd(1,2)* dade(2)) &
		+ d(2,2) * (jt(2,2)*(dwda(2)*dade(2) + dwde(2)) + w(2) * jtd(2,2)* dade(2)) &
		+ d(2,3) * (jt(3,2)*(dwda(2)*dade(2) + dwde(2)) + w(2) * jtd(3,2)* dade(2))

	s23 = d(2,1) * (jt(1,2)*(dwda(2)*dade(3) + dwde(3)) + w(2) * jtd(1,2)* dade(3)) &
		+ d(2,2) * (jt(2,2)*(dwda(2)*dade(3) + dwde(3)) + w(2) * jtd(2,2)* dade(3)) &
		+ d(2,3) * (jt(3,2)*(dwda(2)*dade(3) + dwde(3)) + w(2) * jtd(3,2)* dade(3))

	s31 = d(3,1) * (jt(1,2)*(dwda(2)*dade(1) + dwde(1)) + w(2) * jtd(1,2)* dade(1)) &
		+ d(3,2) * (jt(2,2)*(dwda(2)*dade(1) + dwde(1)) + w(2) * jtd(2,2)* dade(1)) &
		+ d(3,3) * (jt(3,2)*(dwda(2)*dade(1) + dwde(1)) + w(2) * jtd(3,2)* dade(1))

	s32 = d(3,1) * (jt(1,2)*(dwda(2)*dade(2) + dwde(2)) + w(2) * jtd(1,2)* dade(2)) &
		+ d(3,2) * (jt(2,2)*(dwda(2)*dade(2) + dwde(2)) + w(2) * jtd(2,2)* dade(2)) &
		+ d(3,3) * (jt(3,2)*(dwda(2)*dade(2) + dwde(2)) + w(2) * jtd(3,2)* dade(2))

	s33 = d(3,1) * (jt(1,2)*(dwda(2)*dade(3) + dwde(3)) + w(2) * jtd(1,2)* dade(3)) &
		+ d(3,2) * (jt(2,2)*(dwda(2)*dade(3) + dwde(3)) + w(2) * jtd(2,2)* dade(3)) &
		+ d(3,3) * (jt(3,2)*(dwda(2)*dade(3) + dwde(3)) + w(2) * jtd(3,2)* dade(3))
	ddmod = d + s
        return

	!write(105,*)'matrix DDMOD at' ,a*180/pi
	!write(105,'(2x,3G13.4)') ddmod
end function dd_wrinkled

function get_wrinkled_stiffness(p_N, epsin,ddin,wrinklable,ddmod,stress,angle) result (l_Wr_State)

	implicit none
	integer , intent(in) :: p_N
 	REAL (kind= double), intent (in) , dimension(3) :: epsin
 	REAL (kind= double), intent(in),  dimension(3,3) :: ddin
	LOGICAL ,intent(in) :: wrinklable
	REAL (kind= double), INTENT(out), dimension(3,3) :: ddmod
	REAL (kind= double), INTENT(out), dimension(3) :: stress
	REAL (kind= double),intent(in out)  :: angle

	integer :: l_wr_state

	!type (wr_result),target::  l_wr_out
! LOCALS

    REAL (kind= double), dimension(4) :: roots, testwt !s
	REAL (kind= double) :: lwr,upr, anglein,e3 !dummy,
	REAL(kind= double), dimension(3) :: w,stheta, ep
	integer :: nroots,i, ok
	REAL (kind= double), target, dimension(3,3) :: d
	REAL (kind= double), target, dimension(3) :: e ,su
 	logical :: debugprint =.false.	

	character :: cr
	cr = '+' ! char(0)  ! for lahey its '+'
	
	! wr_state =>wr_out%wr_state in work

! because we cannot solve a sixth-order equation.
! we use an energy minimisation method (Newton-Raphson) to get the angle
! After finding the roots by N-R we can still get derivatives using OShift theory
	anglein = angle ! only for debug
	l_wr_state = 0

        stress=matmul(ddin,epsin) ; su=stress
	if( .not. wrinklable) then
		ddmod = ddin
		! angle = 0  ! better to keep the angle for next time
		return
	endif

        d = ddin
	e = epsin
	nroots = wt_roots(stress(1),stress(2),stress(3),roots ) ! normally 4 real roots.  19 june they are now sorted

! doubly-wrinkled is 'no real roots AND all w(a) > 0 - but not the converse'
! unwrinkled is 'no real roots and all w(a) <=0)'
! a good place to test is half-way between the roots

	do i=1,3
                testwt(i) = wt( (roots(i+1) + roots(i) )/2.0d00,d,e,su )
	enddo
	testwt(4) = wt( (roots(4) + roots(1) - 2*Pi )/2.0d00,d,e ,su)

!	if(any(testwt .gt. 0.0) .and. any(testwt .lt. 0.0)) then
!	!	if(nroots .ne. 4) then
!			! write(outputunit,*) ' ROOTS BUG FOUND', testwt
!			
!	!	endif
!	endif

	if(all( testwt .le. 0.0d00) ) then 	! unwrinkled  WAS LT but the le gives some bias towards UnWrinkled
			ddmod = ddin
		!	if(debugprint) write(105,'(a,a,4g15.6,a,3g15.6)') cr,' noWrinkle ',stress,0,' 0 0 PRINC',Principle_Stress(stress)
			l_wr_state=0
			return
	else if(all( testwt .gt. 0.0d00) ) then 	! MAY be doubly wrinkled  GE 0 was dodgy
			angle = Min_Energy_Angle(angle,d,e,su)
			l_wr_state = 1  ! not sure. Test the stress later 
			!if(debugprint)  write(105,'(a,a)' ) cr,'MIN'
	else  ! wt straddles zero. use wt roots to identify the energy min Single wrinkling
			l_wr_state = 1
			nroots = 4	
			if( 0 .ne. find_a_domain(roots,nroots, lwr,upr,angle,d,e)) then
		!		this is the heavy routine. It finds the angle of the wrinkles by constrained NR
				ok = wrinkling_angle(angle, lwr,upr,d,e,su)
				if(ok .eq. 0) then
					write(outputunit,*) 'Wrangle not converged in ', p_N,' ', anglein
				!	write(105,*) ' Wrangle not converged in ', N,' in ',anglein,' now ',angle
				!	write(105,*) epsin
				!	write(105,'(3(g17.9,1x))') ddin
				endif
			else
				write(outputunit,*) '(get_wrinkled_stiffness) find a domain returned an error'
				angle = Min_Energy_Angle(angle,d,e,su)
				write(outputunit,*) 'then Min_Energy_Angle gave ',angle
				if(debugprint)  write(105,'(a,a)' ) cr,'ERR'			
			endif
	endif
! we now have the wrinkling angle. Get ddmod and stress and return. 

if(.false. ) then
! some checks
! Is this really a min of energy?
! Is wt positive here?

			e3 =  dde(angle,d,e,su)
			if(e3 .gt. 0.0) then
			!    e2 = de(angle)
			!	write(outputunit,*) ' 2nd derivative says minimum ',e3,' slope= ',e2, ' at ',angle , ' tri ', N
			!	write(outputunit,*) epsin
			else
				write(outputunit,*) ' 2nd derivative says NOT MIN ' , e3
			endif
endif ! end of checking

	w=0; 	w(2) = wt(angle,d,e,su)
	if(w(2) .lt. 0.0d00) then
		write(outputunit,*) ' wrinkling with NEGATIVE wr!!!!!!!!', w(2)
	endif

	ep = Principle_Strain(epsin) ! debug only BEWARE FOR Speed doenst give the angle
	stress = matmul(ddin, (epsin + matmul(Transpose(wrinkle_j(angle)),w)))   ! maybe this is better than ddmod.epsin
	stheta = matmul(wrinkle_J(angle),stress)  ! a bit wasteful. We really only need the first term. 

	if(stheta(1) .lt. 0.0d00 ) then  ! doubly wrinkled
			stress = 0.0d00
                        ddmod=  ddin * 0.1   ! Need something in case a node gets stranded - BUT WRONG FOR IMPLICIT
			l_wr_state=2 
	else
			ddmod = dd_wrinkled(angle,d,e,su)  !  The magic differential routine
			l_wr_state = 1
	endif
	
	return

! debug  lets calc sigma(theta) and print 

	write(outputunit,*)' wrinkling angle tri 2 is' ,angle*180/pi
		if(abs(stheta(3)) .gt. 1D-5 )	write(outputunit,'(a,3G13.4)') 'sigma in wr axes', stheta
	write(outputunit,*)'stress direct, princ ' 
	write(outputunit,'(2(1x,3G12.4))') stress,stheta
	write(outputunit,*)'from ddmod ' 

	write(outputunit,'(2(1x,3G12.4))')	matmul(ddmod,epsin),matmul(wrinkle_J(angle),matmul(ddmod,epsin))

end function get_wrinkled_stiffness



 subroutine get_wrinkled_stress_only(t, epsin,ddin,wrinklable, stress,angle,p_crease,l_WrState)
 use ftypes_f
! return value is 0,1, or 2 for unwrinkled, singly wrinkled, doubly wrinkled
	implicit none
	type(tri) , intent(in) :: t
 	REAL (kind= double), intent (in) , dimension(3) :: epsin
 	REAL (kind= double), intent(in),  dimension(3,3) :: ddin
	LOGICAL ,intent(in) :: wrinklable
	REAL (kind= double), INTENT(out), dimension(3) :: stress
	REAL (kind= double),intent(in out)  :: angle
	REAL (kind= double),intent(out)  :: p_crease
	integer :: l_wrState

!	type (wr_result),target::  wr_out
! LOCALS

 	REAL(kind= double), dimension(3,3) :: wja ! ds,
	REAL (kind= double), dimension(4) :: roots, testwt
	REAL (kind= double) :: lwr,upr, anglein !dummy,
	REAL(kind= double), dimension(3) :: w,stheta, ep,su
	integer :: nroots,i, ok
	REAL (kind= double), target, dimension(3,3) :: d
	REAL (kind= double), target, dimension(3) :: e 
 	logical,parameter :: debugprint =.false.	
	character,parameter :: cr = '+' ! char(0)  ! for lahey its '+'
	
	l_wrstate = 0
	! wr_state =>wr_out%wr_state in work

! because we cannot solve a sixth-order equation.
! we use an energy minimisation method (Newton-Raphson) to get the angle
! After finding the roots by N-R we can still get derivatives using OShift theory
	anglein = angle ! only for debug
	l_wrState = 0
	p_crease = 0.0
	stress=matmul(ddin,epsin); su=stress;
!	if(debugprint)  write(105,'(a,a,4G15.7)' ) cr,'  princin', Principle_Stress(stress)
	!write(105,'(a,i5,3F12.6)' ) cr,N,epsin
	if( .not. wrinklable) then
		return
	endif

! load up the globals
	d = ddin
	e = epsin

	nroots = wt_roots(stress(1),stress(2),stress(3),roots ) ! normally 4 real roots.  19 june they are now sorted

! doubly-wrinkled is 'no real roots AND all w(a) > 0 - but not the converse'
! unwrinkled is 'no real roots and all w(a) <=0)'
! a good place to test is half-way between the roots

	do i=1,3
		testwt(i) = wt( (roots(i+1) + roots(i) )/2.0d00 ,d,e,su)
	enddo
	testwt(4) = wt( (roots(4) + roots(1) - 2*Pi )/2.0d00 ,d,e,su)

!	if(any(testwt .gt. 0.0) .and. any(testwt .lt. 0.0)) then
!	!	if(nroots .ne. 4) then
!			! write(outputunit,*) ' ROOTS BUG FOUND', testwt
!			
!	!	endif
!	endif

	if(all( testwt .le. 0.0d00) ) then 	! unwrinkled  WAS LT but the le gives some bias towards UnWrinkled
			l_wrState=0
			return
	else if(all( testwt .gt. 0.0d00) ) then 	! MAY be doubly wrinkled  GE 0 was dodgy
	
			angle = Min_Energy_Angle(angle,d,e,su)
			l_wrState = 1  ! not sure. Test the stress later 
			!if(debugprint)  write(105,'(a,a)' ) cr,'MIN'
	else  ! wt straddles zero. use wt roots to identify the energy min Single wrinkling
			l_wrState = 1
			nroots = 4	
			if( 0 .ne. find_a_domain(roots,nroots, lwr,upr,angle,d,e)) then
		!		this is the heavy routine. It finds the angle of the wrinkles by constrained NR
				ok = wrinkling_angle(angle, lwr,upr,d,e,su)
				if(ok .eq. 0) then
				!	write(outputunit,*) ' Wrangle not converged in ', N,' ', anglein
				!	write(105,*) ' Wrangle not converged in ', N,' in ',anglein,' now ',angle
				!	write(105,*) epsin
				!	write(105,'(3(g17.9,1x))') ddin
				endif
			else
				write(outputunit,*) 'get_wrinkled_stress_only:: find a domain returned error'
				
				angle = Min_Energy_Angle(angle,d,e,su)
				write(outputunit,*) ' so Min_Energy_Angle gave  ',angle
				if(debugprint)  write(105,'(a,a)' ) cr,'ERR'			
			endif
	endif
! we now have the wrinkling angle. Get ddmod and stress and return. 


	w=0; 	w(2) = wt(angle,d,e,su); p_crease = w(2)
	if(w(2) .lt. 0.0d00) then
		write(outputunit,*) ' wrinkling with NEGATIVE wr!!!!!!!!', w(2)
	endif

	ep = Principle_Strain(epsin) ! debug only BEWARE FOR Speed doenst give the angle
	wja = wrinkle_j(angle)
	stress = matmul(ddin, (epsin + matmul(Transpose(wja),w)))   ! maybe this is better than ddmod.epsin
	stheta = matmul(wja,stress)  ! a bit wasteful. We really only need the first term. 

	if(stheta(1) .lt. 0.0d00 ) then  ! doubly wrinkled 
			stress = 0.0d00
			l_wrState=2 
!			if(any(ep(1:2) .gt. 0.0d00) )then
!				! write(outputunit,*) ' positive pr strain on double !!!!!!!!!!!! ',sngl(ep)
!			endif
	else
			l_wrState = 1
!			if(all(ep(1:2) .lt. 0.0d00)) then
!				write(outputunit,*) ' Negative pr strains ',t%N,' on single ',sngl(ep(1:2))
!! if after some burning in, these messages don't show, we can afford to use ep as the criterion of double wrinkling
!			endif

	endif
end subroutine get_wrinkled_stress_only
	
END MODULE WRINKLE_M_F  ! here in lahey

! EXTERN_C int  cf_GetWrinkledStress(double e[3], double d[3][3], double  sl[3]);
! this is an interface function to be called from C. 
function cf_getwrinkledstress(e, d, sl) result(rc) bind(C,name= "cf_GetWrinkledStress")
   USE, INTRINSIC :: ISO_C_BINDING
   use ftypes_f
   use WRINKLE_M_F 
    implicit none
	real(c_double),dimension(3), intent(in) :: e
	real(c_double),dimension(3), intent(out) :: sl	
	real(c_double),dimension(3,3), intent(in) :: d	  ! its symmetrical so C/F index ordering doestnmatter
    integer(c_int) ::rc
	type(tri) :: t 
	REAL (kind= double):: angle
	REAL (kind= double) :: p_crease
	angle=0.
    call get_wrinkled_stress_only(t, e,d,.true., sl,angle,p_crease,rc) 

end function cf_getwrinkledstress
