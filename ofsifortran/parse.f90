!C  16/5/94. These are the only routines from strings.f here for brevity
!C
MODULE parse_f
      use basictypes_f  ! for outputunit
IMPLICIT NONE
CONTAINS
	subroutine strip_comment(line)
	IMPLICIT NONE
	character*(*) line
	integer ::i 
	i = index(line,'!')
	if( i ==0) return
	line(i:)=" "
end subroutine strip_comment

subroutine strip_brackets(line)  ! a nasty fixup . should do it in Extract_Fixing_String
	implicit none
	character *(*)  line
	integer :: i
	do i=1, len(line)
		if( line(i:i) .eq. ")" ) line(i:i) = " "
	enddo
end subroutine  strip_brackets

	subroutine denull(s)
	implicit none
	character*(*) s
	integer :: k,n
	n = len(s)
	do k = 1,n
		if(ichar(s(k:k)) .eq. 0)then
		 s(k:) = ' '
		 return
		 endif
	enddo
	end subroutine denull
      LOGICAL FUNCTION SPACER(A,Delimiters)
      IMPLICIT NONE
      CHARACTER*1 A
      CHARACTER*1  Delimiters(6)
! original       CHARACTER*1  Delimiters(6)/' ', '//' , ',' ,'\\','	',':'/  but connect words can have spaces
      INTEGER  I,ndel
	data ndel /6/
      delimiters(5) = char(9)
      	spacer = .FALSE.
      	DO i=1,ndel
	       IF(a .EQ. Delimiters(I) ) spacer = .true.
	enddo
      END FUNCTION SPACER

      SUBROUTINE parse(stringin,Delimiters, output,Nout)
      IMPLICIT NONE
      CHARACTER*(*) stringin
      INTEGER Nout,k
      CHARACTER (len=1),  intent(in),    dimension(6) :: Delimiters
      CHARACTER (len=*),  intent(out) , dimension(Nout) :: output
      INTEGER L,I,J,IS , ndim
      LOGICAL IaSpace, JaSpace
     CHARACTER (len=512) :: string

!  a lash-up.  This routine doesnt find the last word unless there is a delimiter after it
!  so lets concatenate a delimiter to be sure.
  
     ndim = nout
     L = len_trim(stringin)
        if( L .gt. 510) then
        write(outputunit,   "('String too long to parse ',a)" ) trim(stringin(:L)  )
	endif
        string = stringin(:min(510,L)) // Delimiters(1)
      Nout = 0

      L = len_trim(string)

      DO 10 i=1 ,L
      J=i+1
      IaSpace = Spacer(string(i:i),Delimiters)
      JaSpace = Spacer(string(j:j),Delimiters)

      IF  (IaSpace .AND. (.NOT. JaSpace) ) THEN       ! a start
      IS = j
      ELSE IF ( (I .eq.1) .AND. (.NOT. Iaspace) ) THEN
      IS = i
      ENDIF

      IF( (.NOT. Iaspace ) .AND.( JaSpace) ) THEN ! end = I

      Nout = nout+ 1
      output(Nout)= string(IS:I)
      if(nout .eq. ndim ) then
	write(outputunit,*) ' skipped some words in ', stringin
	do k = 1, Nout
	write(outputunit,*)  k, output(k)	
	enddo
	return
     endif

      ENDIF

10      CONTINUE
END SUBROUTINE parse

function nextfixingword(buf) result(word)
! buf may be something like '( (1,0,0), (0,1,0))'
! for each call, we return the next piece as in '( piece , piece , piece )' 
! and we strip that piece off the beginning of buf.
implicit none
character(len=*), intent(inout) :: buf
character(len=256) :: word

! 1) strip the leading '(' or ','
! 2) read till the first ')' (this doesnt support nests)
!
logical :: changing
integer::i
changing = .true. 
    do while(changing)
    changing = .false. 
        if(buf(1:1)=='(') then
         buf=buf(2:); changing= .true.
        endif
         if(buf(1:1)==' ') then
         buf=buf(2:); changing= .true.
        endif  
         if(buf(1:1)==',') then
         buf=buf(2:); changing= .true.
        endif  
        if(buf =="") then
            word=""
            return
        endif
    enddo
! now we have stripped all the {open-bracket  space and comma) from the front of buf.
  i = index(buf,')')
 if(i>0) then
     word = buf(:i-1)
     buf = buf(i+1:)
     return
 endif
word=buf 
buf=""    	       
end function nextfixingword

subroutine ReplaceInString(s,a,b)
character(len=*), intent(inout) ::s
character(len=*), intent(in) :: a,b
integer :: ls,la,i

 ls=len_trim(s); la=len (a)
 if(la<1) return
 if(ls<la) return
 i = index(s(:ls),a(:la))
 do while (i>0)
    s = s(:i-1)//trim(b)//s(i+la:)
    ls=len_trim(s)
    i = index(s(:ls),a(:la))
end do
end subroutine ReplaceInString

END MODULE parse_f

