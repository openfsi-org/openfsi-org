module kbat_els_F

!  here are the routines for the knuckle-batten routines


contains
SUBROUTINE KB_Find_TTmatrix(cbv,t)  ! horribly old
	use vectors_f
      IMPLICIT NONE
! this routine finds the local element axes (rotation about the
! axis excepted) from the new CBV   alone

      REAL*8 cbv(3) !assumed unit vector

!returns
      REAL*8 T(3,3)

       
      REAL*8 dummy
           
      REAL*8  sib
      REAL*8 Xaxis(3) 
	data Xaxis / 1.0d00,2*0.0d00/
	 
      REAL*8 xa(3),Xb(3)  , BN, BML, BA,BL
      REAL*8:: uptest= 0.7071
     
      LOGICAL error
      integer  j,K
      
  
      t(1,1:3)=CBV


   
      SIB=0.0D00
      IF(t(1,3).GT.1.0D0) t(1,3)=1.0D0
      BN=t(1,3)
      BA=SQRT(1.0D00-BN*BN)	     ! ba is projection on XY plane
!

      IF(BA .GT. UPTEST) THEN	     ! its horizontal - ish

        BML=t(1,2)/BA
        BL=t(1,1)/BA
        t(2,1)=-BML
        t(2,2)=BL
        t(2,3)=0.0D00
        t(3,1)=-BN*BL
        t(3,2)=-BN*BML
        t(3,3)=BA
      ELSE

        CALL Vprod( CBV,Xaxis,XA)
        if(DOT_product(XA,XA) .lt. 1.0d-8) THEN ! beam lies along X axis
          DO j=1,3
           DO K=1,3
            t(j,k) = 0.0d00
           ENDDO
           t(j,j) = 1.0d00
          ENDDO

         ELSE
          CALL Normalise(XA,dummy,error)
          CALL Vprod(CBV,XA,XB)
          CALL Normalise( XB,dummy,error)
          DO  K=1,3
           t(1,K) = CBV(K)
           t(2,K) = XA(K)
           t(3,K) = XB(K)
          ENDDO
         ENDIF
       ENDIF
      
   
      END SUBROUTINE KB_Find_TTmatrix

! Rotations are now in an axis system local to the batten
! defined by TTU    
!
! start of development of BATTEN elements.
! the main diference is that there are no common blocks used. This is to
! enable malocing of an indefinite number of elements. 

!   date: Tuesday  11/1/1994
!   translated from LAHEY FFTOSTD output



  
      SUBROUTINE  KPCB_One_Global_Stiffness(e,Xloc,TTU,ZI,GlobalStiff,ZT)
       use vectors_f
       use batstuf_f
      IMPLICIT NONE 

      REAL*8 E(6)      ! stiffnesses Eixx,Eiyy,GJ,EA ,fict, ti
      REAL*8 Xloc(3,2) ! nodal positions at each end
     
      REAL*8 TTU(3,3)  ! undeformed TT matrix
   
      REAL*8 ZI		! initial length. 
   

!returns

      REAL*8 GlobalStiff(12,12)
      REAL*8 ZT

      REAL*8 t(12,12) 
      REAL*8 BigS(12,12)
      REAL*8 Ke(6,6),Meq(12,6)   ,Kg(12,12)
    
!  REAL*8 Tension   ! +ve = tension (increasing stiffness)

      integer I2

      CALL KPCB_TTmatrix(TTu,xloc,t,ZT) ! returns t for link L (12x12)


!    Now get local displacements Uhat = T * Uglobal

!    Now get the local stiffness matrix  Ke (6x6, symmetrical)

      CALL KPCB_Local_Stiff(E,ZI,Ke)

!    Now generate the equilibrium matrix Meq (12x6)

      CALL PCB_Equil_matrix(Zt,Meq)

!    Now generate the geometric stiffness matrix Kg(12x12)

      I2=144
      kg = 0.0d00 ! CALL Zero_Matrix(Kg,I2)
!  				      t       t
!    Finally the tangent stiffness matrix is T *( Meq * Ke * Meq  + Kg)* T


      CALL combine(bigS, Meq,Ke,Kg)

! so combine all the above into BigS(*)

!      CALL bigmult(BigS,t,spare)
!      CALL pc_transpose(12,t,ttrans,12)
!      CALL bigmult(ttrans,spare,GlobalStiff)


!      CALL bigmult(BigS,t,spare)
!      CALL pc_transpose(12,t,ttrans,12)
	
	GlobalStiff= MATMUL(TRANSPOSE(t),matmul(BigS,t))	!      CALL bigmult(TRANSPOSE(t),spare,GlobalStiff)




      END SUBROUTINE  KPCB_One_Global_Stiffness
  
      SUBROUTINE KPCB_Local_Stiff(ei,L,K)
	use vectors_f
      IMPLICIT NONE
!**************************************************************
!    gets the local stiffness matrix  K (6x6, symmetrical)
      REAL*8 EI(5)
      REAL*8 L    ! unstressed length

!returns
      REAL*8 K(6,6)

!locals
      REAL*8  EI2,EI3,GJ,EA
      integer I,J


      EI2 = EI(1)
      EI3 = EI(2)
      GJ  = EI(3)
      EA  = EI(4)


!     e   = Uhat(7) - Uhat(1) 
! otherwiseZT(elem) - ZI(elem) ! extension
!      T12 = Uhat(5)  ! end 1 axis 2 rotations relative to chord
!      t13 = Uhat(6)
!      t22 = Uhat(11)
!      t23 = Uhat(12)

!*  the following are from TAN P41-42

      K(1,1) = 4.*EI3/L 

      K(1,2) = 2.*ei3/L 
 
      K(1,3) =0.0
      K(1,4) = 0.0
    
      K(1,5) = 0.0d00

      K(1,6) = 0.0

      K(2,2) = 4.* ei3/L
  

      K(2,3) = 0.0
      K(2,4) = 0.0
      K(2,5) = 0.0d00

      K(2,6) = 0.0

      K(3,3) = 4.*ei2/L 
     
      K(3,4) = 2.*ei2/L
   
      K(3,5) = 0.0d00

      K(3,6) = 0.0

      K(4,4) = 4.*ei2/L 
   
      K(4,5) = 0.0d00

      K(4,6) = 0.0
      K(5,5) = GJ/L

      K(5,6) = 0.0d00

      K(6,6) = EA/L


!  fill in the lower half of the matrix
      DO I=2,6  ! row
      DO J=1,I-1
      K(I,J) = K(J,I)
      END DO
      END DO

      END       SUBROUTINE KPCB_Local_Stiff
!*****************************************************
      SUbroutine KPCB_ttmatrix(TTund,xloc,tt,ZT)
	use vectors_f
      IMPLICIT NONE
!  returns the transformation matrix for beam ELEM, (based on BCV)
      REAL*8 TTund(3,3)
      REAL*8 Xloc(3,2) 		!nodal coords at each end
!returns
       REAL*8 TT(12,12)
       REAL*8 ZT
  
      integer I,  K, M, row,Col
      REAL*8  tt3(3,3) , A(3,3)
! integer I2
! I2 = 144
      tt = 0.0d00 ! CALL Zero_Matrix(TT,I2)

      CALL KPCB_TTmatrix3by3(xloc,TT3,ZT)

! A converts from batten axes to element
! tt3 converts from global to element

! so A = tt3 * TTund(transposed)
      
      A = 0 ! CALL Zero_Matrix(A,9)
 !     DO I=1,3
!      DO J=1,3
 !     DO K=1,3
 !           A(I,K) = a(I,K) + tt3(i,j) * TTund(K,J) 
 !       ENDDO
!      ENDDO
!      ENDDO	
	  A = matmul(tt3,Transpose(ttund))

      DO 20 i = 0,3,2
      do 20 k = 1,3
      do 20 m = 1,3
      row = K + I*3
      Col = M + i*3
20     tt(row,col) = tt3(K,M)
      
      DO 25 i = 1,3,2
      do 25 k = 1,3
      do 25 m = 1,3
      row = K + I*3
      Col = M + i*3
25     tt(row,col) = A(K,M)


      END       SUbroutine KPCB_ttmatrix

      SUBROUTINE KPCB_TTmatrix3by3(xloc,TT,ZT)
      IMPLICIT NONE  

      REAL*8 Xloc(3,2) 		!nodal coords at each end
!returns
       REAL*8 TT(3,3)	 	! converts from global to element axes
       REAL*8 ZT
!locals

      REAL*8 CBV(3)
      integer K 

      DO K=1,3
        CBV(K)=Xloc(k,2) - Xloc(k,1)
      ENDDO
      zt = sqrt(cbv(1)**2 + cbv(2)**2 + cbv(3)**2)
      DO K=1,3
      	CBV(K) = CBV(K) /ZT
      ENDDO

      CALL KB_Find_TTmatrix(CBV,TT)! ignore beta

!      CALL PCB_Find_TTmatrix(TTund,Rloc,CBV,TT)

    
      END SUBROUTINE KPCB_TTmatrix3by3

      SUBROUTINE KPCB_One_Initial_TT(count,Xib,B0,TTU,Zib)
	use vectors_f
      IMPLICIT NONE
      INTEGER count
      REAL(kind=double), intent(in), dimension(3,count+1)::Xib
      REAL(kind=double), intent(in), dimension(count):: B0

!returns
      REAL(kind=double), intent(out), dimension(3,3,count):: TTU
      REAL(kind=double), intent(out), dimension(count):: Zib

! elements
! 24-jan-1990 this routine is designed to create
! the undeformed TTUndeformed
!

      integer L
      REAL*8 TT(3,3),CBV(3)  ,BETA

      integer I,J, K
      REAL*8 sum,cob,sib, ang, dl(3,3)
      REAL*8 Xaxis(3) 
	data  Xaxis / 1.0d00,2*0.0d00/
      REAL*8 xa(3),Xb(3)  , dummy, RZT, BN, BML, BA,BL
      REAL*8::  uptest = 0.7071
      LOGICAL Error
!***************************************************
    
      DO 5 L=1,count

      CBV(1)= XIb(1,L+1)- XIb(1,L)
      CBV(2)= XIb(2,L+1)- XIb(2,L)
      CBV(3)= XIb(3,L+1)- XIb(3,L)
      SUM=CBV(1)**2+CBV(2)**2+CBV(3)**2
      ZIb(L)=SQRT(SUM)
      if(zib(L) .lt. 0.0001) THEN
        write(outputunit,*)' beam too short ',L
      ENDIF
      RZT = 1D0/Zib(L)
      DO  I=1,3
          CBV(I)=CBV(I)*RZT
          TT(1,I)=CBV(I)
      ENDDO

      ANG=0.0D00
!      DO 504 I=1,3
!504    ANG=ANG+TT(1,I)* XI(IN1,I+3)
      DL(1,1)=ANG
      ANG=0.0D00
!      DO 5041 I=1,3
!5041   ANG=ANG+TT(1,I)* XI(IN2,I+3)
      DL(1,2)=ANG
!
      BETA=B0(L)+ (DL(1,1)+DL(1,2))*0.5D00
!   ! guessed -ve 24-1-90 PH
      if(ABS(beta) .lt.1.0D-8) THEN
     	Beta = 0.0 
      ENDIF
      COB=COS(BETA)
      SIB=SIN(BETA)
      IF(TT(1,3).GT.1.0D0) TT(1,3)=1.0D0
      BN=TT(1,3)
      BA=SQRT(1.0D00-BN*BN)
!

      IF(BA .GT. UPTEST) THEN

!      IF (.NOT. UPRIGHT(L) ) THEN
!
        BML=TT(1,2)/BA
        BL=TT(1,1)/BA
        TT(2,1)=-COB*BML-BN*BL*SIB
        TT(2,2)=BL*COB-BN*BML*SIB
        TT(2,3)=BA*SIB
        TT(3,1)=BML*SIB-BN*BL*COB
        TT(3,2)=-BL*SIB-BN*BML*COB
        TT(3,3)=BA*COB
      ELSE

        CALL Vprod( CBV,Xaxis,XA)
        if(DOT_product(XA,XA) .lt. 1.0d-8) THEN ! beam lies along X axis
	  write(outputunit,*) ' beam on X axis '
          DO j=1,3
           DO K=1,3
            TT(j,k) = 0.0d00
           ENDDO
           TT(j,j) = 1.0d00
          ENDDO

         ELSE
          CALL Normalise(XA,dummy,error)
          CALL Vprod(CBV,XA,XB)
          CALL Normalise( XB,dummy,error)
          DO  K=1,3
           Tt(1,K) = CBV(K)
           Tt(2,K) = XA(K)
           Tt(3,K) = XB(K)
          ENDDO
         ENDIF
       ENDIF
      TTu(1:3,1:3,L) = TT
5	continue
      END SUBROUTINE KPCB_One_Initial_TT

end module kbat_els_F

