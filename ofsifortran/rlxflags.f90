module rx_control_flags_f !  Must match  offset.h

!  mods for ifort 15
!  see  Compiler Reference>Mixed Language Programming
    !>Standard Tools>Global Data

! NOTE we still use logical *4

! use, intrinsic:: not just  use iso_c_binding
! common, bind(C) not bind(C) as separate line FALIED COMPILATION
! move declarations before the common block
! try without volatile and save
! use F90_CFLAGS +=  -standard-semantics

       USE, INTRINSIC::ISO_C_BINDING ! use iso_c_binding
       implicit none
      real(c_double)::    c_factor1,c_factor2, residualCap,DampingFactor
      real(c_double)::    admasmin,g_UpdateTime,g_RKDamping,g_RKEps,g_RK_H1
      INTEGER(c_int)::    Stop_Running,NompThreads, Mass_Set_Flag
      INTEGER(c_int)::    VPP_Connected
      INTEGER(c_int)::    ForceLinearDofoChildren

      LOGICAL(c_int)::    Mass_By_Test
      LOGICAL(c_int)::    SSDg_WrinklingAllowed
      LOGICAL(c_int)::    K_on_Wrinkled_Matrix
      LOGICAL(c_int)::    New_DD
      LOGICAL(c_int)::    HardCopy
      LOGICAL(c_int)::    dm
      LOGICAL(c_int)::    LinkCompAllowedFlag
      LOGICAL(c_int)::    StringCompAllowedFlag
      LOGICAL(c_int)::    Debug1,Debug2 ! debug2 is 'wrinkling allowed
      LOGICAL(c_int)::    Force_Linear,  Use_DSTF_Old

      LOGICAL(c_int)::    gPrestress  ! with ElasticModifier, controlled by the 'prestress' script command
      LOGICAL(c_int)::    form_2006
      LOGICAL(c_int)::    bumpersticker    ! /BS
      LOGICAL(c_int)::	 EdgeCurveCorr,SVD_Solver
      real (c_double), dimension(3,3) :: ElasticModifier
      COMMON/relaxflagcommon/&
 !      COMMON, BIND(C) / relaxflagcommon / &
       c_factor1,  c_factor2  , residualCap, DampingFactor, admasmin ,&
       g_UpdateTime, g_RKDamping,g_RKEps,g_RK_H1, &
       Stop_Running,    NompThreads,    Mass_Set_Flag,  &
       VPP_Connected,  &
       ForceLinearDofoChildren,  & 
       Mass_By_Test,  &
       SSDg_WrinklingAllowed,  &
       K_on_Wrinkled_Matrix,  &
       New_DD,  &
       HardCopy,  &
       dm,  &
       LinkCompAllowedFlag,  &
       StringCompAllowedFlag,  &
       Debug1,Debug2,  &
       Force_Linear,    Use_DSTF_Old,  &
       gPrestress,  &
       form_2006, &
       BumperSticker, &
       EdgeCurveCorr,&
       SVD_Solver,&
       ElasticModifier


       
      bind(C,name='relaxflagcommon') :: /relaxflagcommon/
!      volatile  /relaxflagcommon/
!      save /relaxflagcommon/
! NOTE THE DOC FOR BIND SAYS
! For variables and common blocks, BIND also implies the SAVE attribute,
!  which may be explicitly confirmed with SAVE.
      
  contains 
SUBROUTINE FlagSet(flags)  ! not interoperable
	USE, INTRINSIC :: ISO_C_BINDING
	use tanglobals_f
	use fglobals_f

	IMPLICIT NONE
        CHARACTER*(*), intent(in) :: flags
 
        CALL Zero_Flags()
        CALL Putcl(flags)
 
	
      CALL CHECK_CL('MT',Mass_By_Test)

      CALL CHECK_CL('CL', LinkCompAllowedFlag)
      LinkCompAllowedFlag = .not. LinkCompAllowedFlag

      CALL CHECK_CL('DM',dm)
      CALL CHECK_CL('CC',StringCompAllowedFlag)
      StringCompAllowedFlag= .not. StringCompAllowedFlag
      
      CALL CHECK_CL('ND',New_DD) ! never used
      CALL CHECK_CL('HC',HardCopy)
      
      K_on_Wrinkled_Matrix=.false.    
      CALL CHECK_CL('D1',Debug1)
      CALL CHECK_CL('D2',Debug2)
      IF(Debug2) THEN
          CALL CHECK_CL('WK',K_on_Wrinkled_Matrix)
      ENDIF      

      CALL CHECK_CL('NL',Force_Linear)
      Force_Linear = .NOT. Force_Linear
      CALL CHECK_CL('FI', Use_DSTF_Old)	
!      CALL CHECK_CL('PS', g_Prestress)
      CALL CHECK_CL('S6', form_2006)     
      CALL CHECK_CL('BS' ,bumpersticker)
      CALL CHECK_CL('CV' ,EdgeCurveCorr) 


      CALL CHECK_CL('DT' ,dofotrace )
      CALL CHECK_CL('LV' ,LimitV )          


  ! the stroona flags   
     call CHECK_CL( 'BO',G_Bowing)  ! dangerous ! the stroona flags
	 call CHECK_CL( 'BS',G_BowShear) ! need re-checking
         call CHECK_CL( 'NG',geomatrix); geomatrix = .not. geomatrix ! NG = No GeoMatrix! the stroona flags
	 call CHECK_CL( 'XR',g_dxdr)
	 call CHECK_CL( 'NQ',g_no_Qmatrix)
 END SUBROUTINE FlagSet
      
SUBROUTINE Write_All_Flags(unit)
	use basictypes_f
    use tanglobals_f
    IMPLICIT NONE
    integer,intent(in) :: UNIT
    character(256) commandline
    integer:: f
    CALL GET_our_CL(CommandLine)
    write(unit,*) ' commandline is ', trim(Commandline)
    f = ForceLinearDofoChildren;
#ifdef NO_DOFOUPDATEORIGIN
         write(unit,*) 'DOFOUPDATEORIGIN DISabled. FLDC ',f ,' (ch,evalLinear,NoUp) = ',BTEST( f,0),BTEST( f,1),BTEST( f,2)

#else
        write(unit,*) 'DOFOUPDATEORIGIN   enabled. FLDC ',f ,' (ch,evalLinear,NoUp) = ',BTEST( f,0),BTEST( f,1),BTEST( f,2)
#endif
    
!       write(unit,10)' Mass_Set_Flag ', (Mass_Set_Flag .ne. 0)
!      write(unit,10)' Kill_Negatives ', Kill_Negatives 
!      write(unit,10)' Mass_By_Test', Mass_By_Test
!      write(unit,10)' Wrinkling_Allowed', Wrinkling_Allowed
       write(unit,10)' K_on_Wrinkled_Matrix', K_on_Wrinkled_Matrix
!      write(unit,10)' HardCopy', HardCopy
!      write(unit,10)' Diagonalise matrix', dm
!       write(unit,10)' LinkCompAllowed (~/CL)', LinkCompAllowedFlag


    write(unit,10)' Negative String tension (~/CC)', StringCompAllowedFlag



!       write(unit,10)' Wrinkling_Allowed',Wrinkling_Allowed
    write(unit,10)' debug1',debug1

    write(unit,10)' coalesceLayers(/FI)',Use_DSTF_Old
    write(unit,10)' Force_Linear',Force_Linear
    write(unit,10)'/BO (dangerous)',G_Bowing
    write(unit,10)'/BS(esperimental)',G_BowShear
    write(unit,10)'beam geo (~/NG)',geomatrix
    write(unit,10)'/XR',g_dxdr
    write(unit,10)'/NQ',g_no_Qmatrix
    write(unit,10)'form_2006(/S6)', form_2006
    write(unit,10)' debug2(Wrinkling)',debug2
 ! ,g_dxdr,geomatrix,g_Bowing,g_BowShear
10    FORMAT(a30,L5)
    write(unit,10)' prestress (DefFile)',gPrestress
    if(gPrestress)then
       write(unit,'(20x, a)') 'ElasticModifier'
       write(unit,'(20x,3F12.6 )')  ElasticModifier
    endif

END SUBROUTINE Write_All_Flags

end  module rx_control_flags_f

