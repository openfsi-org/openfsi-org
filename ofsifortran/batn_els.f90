!* Rotations are now in an axis system local to the batten
!C defined by TTU    
!C
!C start of development of BATTEN elements.
!* the main diference is that there are no common blocks used. This is to
!* enable malocing of an indefinite number of elements. 

!*   date: Tuesday  11/1/1994
!*   translated from LAHEY FFTOSTD output

       module batn_els_f
        contains

      SUBROUTINE  PCB_One_Global_Forces(e,Rloc,Xloc,Xib,TTU, ZI,GlobalForce,ZT,Force) ! SUBROUTINE  PCB_One_Global_Forces(e,Rloc,Xloc,Xib,TTU,beta0,ZI,GlobalForce,ZT,Force)
	use batstuf_f
      IMPLICIT NONE 

      REAL*8 E(6)      ! stiffnesses Eixx,Eiyy,GJ,EA,fictitious, TI 
      REAL*8 Rloc(3,2) ! rotations of the two ends (batten axes)
      REAL*8 Xloc(3,2) ! nodal positions at each end
      REAL*8 Xib (3,2) ! nodal positions at each end
      REAL*8 TTU(3,3)  ! undeformed TT matrix
      REAL*8 ZI		! initial length. 

!*returns
      REAL*8 GlobalForce(12)
      REAL*8 ZT
      REAL*8 force(6)   ! force in locals

      REAL*8 t(12,12)
      REAL*8 Meq(12,6) , Length
      REAL*8 Uhat(12) ,Flocal(12)

       CALL PCB_TTmatrix(TTu, xloc,t,ZT) ! returns t for link L (12x12)
      
!*    Now get local displacements Uhat = T * Uglobal

      CALL PCB_Local_Disp (T,rloc,xloc,ZT,ZI,Xib,Uhat)

!*    Now generate the equilibrium matrix Meq (12x6)

      CALL PCB_Equil_matrix(Zt,Meq)

!*    Now generate the geometric stiffness matrix Kg(12x12)

        Length = ZI
        CALL PCB_Member_Forces(E,Length,Uhat,Force)

!* Now the local forces are Meq*force
!* and the global are ttrans * (localforce)

!      DO i=1,12
!      Flocal(I) = 0.0d00
!      GlobalForce(I) = 0.0d00
!      ENDDO

 !     DO i=1,12
!      DO J=1, 6
!      Flocal(I) = Flocal(I) + Meq(I,J) * Force(J)
!      ENDDO
!      ENDDO
	flocal = matmul(meq,force)
!      DO I=1,12
!      DO J=1,12
!      GlobalForce(I) = GlobalForce(I) + T (J,I) * Flocal(J)
 !     ENDDO
!      ENDDO
      globalForce = matmul(Transpose(T),flocal)
      
      END SUBROUTINE  PCB_One_Global_Forces

      SUBROUTINE  PCB_One_Global_Stiffness(e,Rloc,Xloc,Xib,TTU, GM, ZI,GlobalStiff,ZT)
	use vectors_f
	use batstuf_f
      IMPLICIT NONE 

      REAL*8 E(6)      ! stiffnesses Eixx,Eiyy,GJ,EA ,fict, ti
      REAL*8 Rloc(3,2) ! rotations of the two ends (global axes)
      REAL*8 Xloc(3,2) ! nodal positions at each end
      REAL*8 Xib(3,2) ! noal positions at each end

      REAL*8 TTU(3,3)  ! undeformed TT matrix
     ! REAL*8 beta0

      LOGICAL GM	! TRUE if ge stiffness is to be ncluded
      REAL*8 ZI		! initial length. 
   

!*returns

      REAL*8 GlobalStiff(12,12)
      REAL*8 ZT

      REAL*8 t(12,12), spare(12,12)!,ttrans(12,12)
      REAL*8 BigS(12,12)
      REAL*8 Ke(6,6),Meq(12,6)   ,Kg(12,12), force(6) , Length
      REAL*8 Uhat(12)

!*  REAL*8 Tension   ! +ve = tension (increasing stiffness)

! integer I2

      CALL PCB_TTmatrix(TTu, xloc,t,ZT) ! returns t for link L (12x12)


!*    Now get local displacements Uhat = T * Uglobal

      CALL PCB_Local_Disp (T,rloc,xloc,ZT,ZI,Xib,Uhat)

!*    Now get the local stiffness matrix  Ke (6x6, symmetrical)

      CALL PCB_Local_Stiff(E,ZI,Ke,Uhat)

!*    Now generate the equilibrium matrix Meq (12x6)

      CALL PCB_Equil_matrix(Zt,Meq)

!*    Now generate the geometric stiffness matrix Kg(12x12)

      IF (gM) THEN
        Length = ZI
        CALL PCB_Member_Forces(E,Length,Uhat,Force)

        Length = ZT
        CALL geo_stiff_Matrix( Kg,force,Length)
      ELSE
       ! I2=144
       kg=0.0d00 ! CALL Zero_ Matrix(Kg,I2)
      ENDIF
!*               t       t

!*    Finally the tangent stiffness matrix is T *( Meq * Ke * Meq  + Kg)* T


      CALL combine(bigS, Meq,Ke,Kg)

!* so combine all the above into BigS(*)


	spare = matmul(BigS,t)   		!      CALL bigmult(BigS,t,spare)

						!      CALL pc_transpose(12,t,ttrans,12)
	
	GlobalStiff= MATMUL(TRANSPOSE(t),spare)	!      CALL bigmult(TRANSPOSE(t),spare,GlobalStiff)



      END subroutine PCB_One_Global_Stiffness  !(PCB version)




      SUBROUTINE PCB_Local_Disp(T,rloc,xloc,ZT,ZI,XIb,Uhat)
      use vectors_f
      IMPLICIT NONE

      REAL*8 T(12,12)

      REAL*8 Rloc(3,2) ! rotations of the two ends (global axes)
      REAL*8 Xloc(3,2) ! noal positions at each end
      REAL*8 xib (3,2)  ! unstressed ordinates of ends
     
      REAL*8 ZI		! initial length. 

      REAL*8 ZT

! returns
      REAL(kind=double), intent(out), dimension(12) ::  Uhat

! Uhat are the displacements relative to the local axis system
! Ie angles are given relative to the chord
! so we must correct for sway angle
!
! PETER 21/2/91 DO NOT ASSUME that ttmatrix axis 1 lies along chord
!
 
!locals
      integer end,K 
      REAL*8 origin(3),XC(3),XL(3),Tsmall(3,3), xit(6), sway(2:3), DX,DY
     
      Tsmall = T(1:3,1:3)
  

! (2) u(4-6,10-12) INITIALLY are rotations in local axes

      DO end=1,2
      DO K=1,3
      XC(K) = Rloc(K,end)
      END DO
      xl = matmul(Tsmall, xc) ! CALL MultV( XL, Tsmall, XC)
      DO K=1,3
      Uhat( (end-1)*6 + K+3) = XL(K)
      END DO
      END DO


! (3)  Now get sway angles by looking at XI in local coordinates
      DO k=1,3
      origin(k) = Xloc(k,1)
      END DO

      DO end=1,2
      DO K=1,3
      XC(K) = Xib(K,end) - origin(K)
      END DO
       xl = matmul(Tsmall, xc) ! CALL MultV( XL, Tsmall, XC)
      DO K=1,3
      XIt( (end-1)*3 + K) = XL(K)  
!xit are initial coords in local axes
      END DO
      END DO

! (4)  modify u(5,6,11,12) by sway angles
      dy= xit(6) - xit(3)
      dx= xit(4) - xit(1)

      sway(2) = + PH_atan2( dy,dx)
      dy= xit(5) - xit(2)
      sway(3) = - PH_atan2( dy,dx)  
    
! sense is : sway(2) is +ve for rotation clockwise looking along Y
!     sway(3) is +ve for rotation clockwise looking along Z

      Uhat (5)  = Uhat (5)  - sway(2)  
!but check the sense !!!!!!!!!!!!!
      Uhat (6)  = Uhat (6)  - sway(3)
      Uhat (11) = Uhat (11) - sway(2)
      Uhat (12) = Uhat (12) - sway(3)


! (1) u(1-3), 7-9) are  XYZ displacements in local axes WRT end1
!  they WOULD be zero if ttmatrix aligned to chord.

! BUT DO NOT ASSUME THIS Below is a correction in case there is not exact
! alignment
!
      Uhat(1:3) = 0.0d00
      Uhat(8:9) = 0.0d00


      Uhat(7) = ZT - ZI

      DO K=1,3
      XC(K) = (Xloc(K,2)-  Xloc(K,1)) - Tsmall(1,k) * ZI
      ENDDO
       xl = matmul(Tsmall, xc) ! CALL Multv(XL,Tsmall,XC)
      DO K=1,3
      Uhat(6+K) = XL(K)
      ENDDO

      END SUBROUTINE PCB_Local_Disp
       
      SUBROUTINE PCB_Local_Stiff(ei,L,K,Uhat)
      IMPLICIT NONE
!***************************************************************
!*    gets the local stiffness matrix  K (6x6, symmetrical)
      REAL*8 EI(5)
      REAL*8 Uhat(12)
      REAL*8 L    ! unstressed length

!*returns
      REAL*8 K(6,6)

!*locals
      REAL*8  EI2,EI3,GJ,EA,e
      REAL*8  t12, t13, t22,t23
      integer I,J


      EI2 = EI(1)
      EI3 = EI(2)
      GJ  = EI(3)
      EA  = EI(4)


      e   = Uhat(7) - Uhat(1) 
!* otherwiseZT(elem) - ZI(elem) ! extension
      T12 = Uhat(5)  ! end 1 axis 2 rotations relative to chord
      t13 = Uhat(6)
      t22 = Uhat(11)
      t23 = Uhat(12)

!**  the following are from TAN P41-42

      K(1,1) = 4.*EI3/L + 4.*EA*e/30. &
     &  + EA*L/300. *( 8.*t13**2 - 4.*t13*t23 + 3.*t23**2) &
     &  + EA*L/900. *( 8.*t12**2 - 4.*t12*t22 + 8.*t22**2)

      K(1,2) = 2.*ei3/L - EA*e/30. &
     &  - EA*L/300. * ( 2.*t13**2 - 6.*t13*t23 + 2.*t23**2) &
     &  -EA*L/900.  * (2.*t12**2 -t12*t22  + 2. * t22**2)

      K(1,3) = EA*l/900. * ( 16.*t13*t12 - 4.*t13*t22 - 4.*t23*t12 + t23*t22)

      K(1,4) = EA*l/900. *( -4.*t13*t12 + 16.*t13*t22 + t23*t12 - 4.*t23*t22)

      K(1,5) = 0.0d00

      K(1,6) = EA/30.0 *( 4.*t13 - t23)

      K(2,2) = 4.* ei3/L + 4.*EA*e/30.&
     &  + EA*l/300. * ( 3.*t13**2 - 4.*t13*t23 + 8.*t23**2)&
     &  + EA*L/900. *(  8.*t12**2 - 4.*t12*t22 + 8. *t22**2 )


      K(2,3) = EA*L/900. *( -4.*t13*t12 + t13*t22 +16.*t23*t12 - 4.*t23*t22)

      K(2,4) = EA*L/900. *( t13*t12 - 4.*t13*t22 - 4.*t23*t12 + 16.*t23*t22)

      K(2,5) = 0.0d00

      K(2,6) = EA/30. *( -t13 + 4.*t23)

      K(3,3) = 4.*ei2/L + 4.*ea*e/30.&
     &  + EA*L/900. *( 8.*t13**2 - 4.*t13*t23 + 8.*t23**2)&
     &  + EA*L/300. *( 8.*t12**2 - 4.*t12*t22 + 3.*t22**2)

      K(3,4) = 2.*ei2/L - EA*e/30.&
     & - EA*L/900. * (2.*t13**2 - t13*t23 + 2.*t23**2)&
     & - EA*L/300. * (2.*t12**2 - 6.*t12*t22 + 2.*t22**2)

      K(3,5) = 0.0d00

      K(3,6) = EA/30. * ( 4.*t12 - t22)

      K(4,4) = 4.*ei2/L + 4.*EA*e/30.&
     & + EA*L/900. * ( 8.*t13**2 - 4.*t13*t23 + 8.*t23**2)&
     & + EA*L/300. * (3.*t12**2 - 4.*t12*t22 + 8.*t22**2)

      K(4,5) = 0.0d00

      K(4,6) = EA/30. * ( -t12 + 4.*t22)

      K(5,5) = GJ/L

      K(5,6) = 0.0d00

      K(6,6) = EA/L


!*  fill in the lower half of the matrix
      DO I=2,6  ! row
      DO J=1,I-1
      K(I,J) = K(J,I)
      END DO
      END DO

      END  SUBROUTINE PCB_Local_Stiff

 
      SUBROUTINE PCB_Member_Forces(ei,L,Uhat, Force)
      IMPLICIT NONE 
      
!*   see page 40
      REAL*8 EI(6)
      REAL*8 L 		! unstressed length
      REAL*8 Force(6)
      REAL*8 Uhat(12)

!*locals
      REAL*8  EI2,EI3,GJ,EA,e  ,M13,M23,M12,M22,P,Mt
      REAL*8  t12, t13, t22,t23, thetaT	,TI


      EI2 = EI(1)
      EI3 = EI(2)
      GJ  = EI(3)
      EA  = EI(4)
      TI = EI(6)

      e   = Uhat(7) - Uhat(1) !  extension
      T12 = Uhat(5)  ! end 1 axis 2 rotations relative to chord
      t13 = Uhat(6)
      t22 = Uhat(11)
      t23 = Uhat(12)
      ThetaT = Uhat(10) - Uhat(4)


      P = EA*e/L + EA/30.d0 * ( (2.d0*t13**2 - t13*t23 + 2.d0*t23**2 )&
     &           + (2.d0*t12**2 - t12*t22 + 2.d0*t22**2 ) ) + TI

      Mt = GJ/L * ThetaT

      M13= (4.d0*ei3/L + 4.d0*P*L/30.d0)*t13 + (2.d0*ei3/L - P*L/30.d0   )*t23

      M23= (2.d0*ei3/L - P*L/30.d0)     *t13 + (4.d0*ei3/L + 4.d0*P*L/30.d0)*t23

      M12= (4.d0*ei2/L + 4.d0*P*L/30.d0)*t12 + (2.d0*ei2/L - P*L/30.d0   )*t22

      M22= (2.d0*ei2/L - P*L/30.d0)    * t12 + (4.d0*ei2/L + 4.d0*P*L/30.d0)*t22

      Force(1) = M13
      Force(2) = M23
      Force(3) = M12
      Force(4) = M22
      Force(5) = Mt
      Force(6) = P
      END  SUBROUTINE PCB_Member_Forces

      SUBROUTINE geo_stiff_Matrix( Kg,force,Length)
      IMPLICIT NONE
!***************************************************************
!*    Now generate the geometric stiffness matrix Kg(12x12)

      REAL*8 Kg(12,12), force(6), Length ! force is F on page 43
!* locals
      integer I,J
      REAL*8 A,B,chi


      A= (Force(1) + Force(2) ) /Length/Length
      B =(Force(3) + Force(4) ) /Length/Length
      chi = Force(6)/Length

      Kg = 0.0d00


      Kg(1,2) = A
      Kg(1,3) = -B
      Kg(1,8) = -A
      Kg(1,9) = B
      Kg(2,2) = chi
      Kg(2,7) = -A
      Kg(2,8) = -chi
      Kg(3,3) = chi
      Kg(3,7) = B
      Kg(3,9) = -chi
      Kg(7,8) = A
      Kg(7,9) = -B
      Kg(8,8) = chi
      Kg(9,9) = chi

! *  fill in the lower half of the matrix
      DO I=2,12  ! row
      DO J=1,I-1
      Kg(I,J) = Kg(J,I)
      END DO
      END DO


      END  SUBROUTINE geo_stiff_Matrix


!******************************************************
      SUbroutine PCB_ttmatrix(TTund, xloc,tt,ZT)  !SUbroutine PCB_ttmatrix(TTund,Rloc,beta0,xloc,tt,ZT)
      IMPLICIT NONE
!*  returns the transformation matrix for beam ELEM, (based on BCV)
      REAL*8 TTund(3,3)
     ! REAL*8 Rloc(3,2)		!nodal rotations at ends. batten axes
      REAL*8 Xloc(3,2) 		!nodal coords at each end
    !  REAL*8 Beta0
!*!returns
       REAL*8 TT(12,12)
       REAL*8 ZT
  
      integer I, J, K, M, row,Col
      REAL*8  tt3(3,3) , A(3,3)
      integer I2
      I2 = 144
      tt = 0.0d00

      CALL PCB_TTmatrix3by3(xloc,TT3,ZT)

!C A converts from batten axes to element
!C tt3 converts from global to element

!C so A = tt3 * TTund(transposed)
      
      a = 0.0d00
      DO I=1,3
      DO J=1,3
      DO K=1,3
            A(I,K) = a(I,K) + tt3(i,j) * TTund(K,J) 
        ENDDO
      ENDDO
      ENDDO	

      DO 20 i = 0,3,2
      do 20 k = 1,3
      do 20 m = 1,3
      row = K + I*3
      Col = M + i*3
20     tt(row,col) = tt3(K,M)
      
      DO 25 i = 1,3,2
      do 25 k = 1,3
      do 25 m = 1,3
      row = K + I*3
      Col = M + i*3
25     tt(row,col) = A(K,M)


      END  SUbroutine PCB_ttmatrix

      SUBROUTINE PCB_TTmatrix3by3( xloc,TT,ZT)
      IMPLICIT NONE  

    !  REAL*8 TTund(3,3)
    !  REAL*8 Rloc(3,2)		!nodal rotations at each end
      REAL*8 Xloc(3,2) 		!nodal coords at each end
   !   REAL*8 Beta0
!*returns
       REAL*8 TT(3,3)	 	! converts from global to element axes
       REAL*8 ZT
!*locals

      REAL*8 CBV(3)
      integer K 

      DO K=1,3
        CBV(K)=Xloc(k,2) - Xloc(k,1)
      ENDDO
      zt = sqrt(cbv(1)**2 + cbv(2)**2 + cbv(3)**2)
      DO K=1,3
      	CBV(K) = CBV(K) /ZT
      ENDDO

      CALL New_PCB_Find_TTmatrix( CBV,TT)! ignore beta

!C      CALL PCB_Find_TTmatrix(TTund,Rloc,CBV,TT)

    
      END     SUBROUTINE PCB_TTmatrix3by3

      SUBROUTINE PCB_One_Initial_TT(count,Xib,B0,TTU,Zib)
	use vectors_f 
      IMPLICIT NONE
      INTEGER count
      REAL*8 Xib(3,count+1)
      REAL*8 B0(*)

!returns
      REAL*8 TTU(3,3,*)	
      REAL*8 Zib(*)

! elements
! 24-jan-1990 this routine is designed to create
! the undeformed TTUndeformed
!

      integer L
      REAL*8 TT(3,3),CBV(3)  ,BETA

      integer I,J, K
      REAL*8 sum,cob,sib, ang, dl(3,3)
      REAL*8 Xaxis(3) 
	data Xaxis / 1.0d00,2*0.0d00/
      REAL*8 xa(3),Xb(3)  , dummy, RZT, BN, BML, BA,BL
      REAL*8:: uptest = 0.7071 
      LOGICAL Error
!****************************************************
    write(outputunit,*) 'with KBatten shouldnt be here'
      DO 5 L=1,count

      CBV(1)= XIb(1,L+1)- XIb(1,L)
      CBV(2)= XIb(2,L+1)- XIb(2,L)
      CBV(3)= XIb(3,L+1)- XIb(3,L)
      SUM=CBV(1)**2+CBV(2)**2+CBV(3)**2
      ZIb(L)=SQRT(SUM)
      if(zib(L) .lt. 0.0001) THEN
        write(outputunit,*)' beam too short ',L; call RXF_Terminate(" ");
      ENDIF
      RZT = 1D0/Zib(L)
      DO  I=1,3
          CBV(I)=CBV(I)*RZT
          TT(1,I)=CBV(I)
      ENDDO

      ANG=0.0D00

      DL(1,1)=ANG
      ANG=0.0D00

      DL(1,2)=ANG

      BETA=B0(L)+ (DL(1,1)+DL(1,2))*0.5D00

      if(ABS(beta) .lt.1.0D-8) THEN
     	Beta = 0.0 
      ENDIF
      COB=COS(BETA)
      SIB=SIN(BETA)
      IF(TT(1,3).GT.1.0D0) TT(1,3)=1.0D0
      BN=TT(1,3)
      BA=SQRT(1.0D00-BN*BN)

      IF(BA .GT. UPTEST) THEN

        BML=TT(1,2)/BA
        BL=TT(1,1)/BA
        TT(2,1)=-COB*BML-BN*BL*SIB
        TT(2,2)=BL*COB-BN*BML*SIB
        TT(2,3)=BA*SIB
        TT(3,1)=BML*SIB-BN*BL*COB
        TT(3,2)=-BL*SIB-BN*BML*COB
        TT(3,3)=BA*COB
      ELSE

        CALL Vprod( CBV,Xaxis,XA)
        if(DOT_Product(XA,XA) .lt. 1.0d-8) THEN ! beam lies along X axis
	  write(outputunit,*) ' beam on X axis '
          DO j=1,3
           DO K=1,3
            TT(j,k) = 0.0d00
           ENDDO
           TT(j,j) = 1.0d00
          ENDDO

         ELSE
          CALL Normalise(XA,dummy,error)
          CALL Vprod(CBV,XA,XB)
          CALL Normalise( XB,dummy,error)
          DO  K=1,3
           Tt(1,K) = CBV(K)
           Tt(2,K) = XA(K)
           Tt(3,K) = XB(K)
          ENDDO
         ENDIF
       ENDIF
      DO 5 J=1,3
      do 5 K=1,3
5      TTu(J,K,L) = TT(j,k)

      END SUBROUTINE PCB_One_Initial_TT




      SUBROUTINE PCB_Find_TTmatrix(ttold,rLoc,cbv,t)
	use vectors_f
	use math_f
      IMPLICIT NONE
! this routine finds the local element axes (rotation about the
! axis excepted) by reference to the original(ie from XI )matrix
!and the new CBV
      REAL*8 ttold(3,3),Rloc(3,2),cbv(3) !assumed unit vector

!eturns
      REAL*8 T(3,3)

      REAL*8 :: ax1(3) ,axis(3), Betamatrix(3,3)= 0.0d00
      REAL*8 tt(3,3),  vector(3)
      REAL*8 dummy,COB,SIB,ang1,ang2 , theta,beta
  
      LOGICAL error
      integer I,K

      DO 10 k=1,3
10     AX1(K) = ttold(1,k)


      CALL Vprod(ax1,CBV,axis)
      if(DOT_product(Axis,Axis) .lt. 1.0d-18) THEN
      write(outputunit,*) ' (Find_TTM) use old matrix '
      DO 20 i=1,3
      DO 20 K=1,3
20     tt(i,k) = ttold(i,k)

      ELSE
      CALL normalise(axis,dummy,error)
      DO  K=1,3
      tt(1,k) = CBV(k)
      ENDDO

!  theta = ASIN (dummy)
      dummy = DOT_product(CBV,ax1)  ! 0 to PI
      THETA = PH_ACOS(dummy)

      DO I=2,3
      DO  K=1,3
      vector(K) = ttold(I,K)
      ENDDO

! *  We now rotate <vector> clockwise by angle <theta> about <axis>
!  * where <theta> is the true angle between CBV and BIV


      CALL rotate_vector(vector,axis,theta)

      DO K=1,3
      tt(i,k) =  vector(K)
      ENDDO
      ENDDo
      ENDIF

      ANG1=0.0D00
      ANG2=0.0D00
      DO  I=1,3
      ANG1=ANG1+CBV(I)*Rloc(I,1)
      ANG2=ANG2+CBV(I)*Rloc(I,2)
      ENDDO

!C      BETA=BETA0+ (ang1+ang2)*0.5D00
!C      if(abs(beta ) .lt. 1.0D-8) Beta = 0.0D00
!C      COB=COS(BETA)
!C      SIB=SIN(BETA)

      BETA = 0.0D00   ! WRONG if EIxx not eiyy
      COB=1.0D00
      SIB= 0.0D00

      BetaMatrix(1,1) = 1.0d00
      BetaMatrix(2,2) = COB
      BetaMatrix(3,3) = COB
      BetaMatrix(2,3) = SIB
      BetaMatrix(3,2) = -SIB

      CALL Mult(T,BetaMatrix,TT)


      END  SUBROUTINE PCB_Find_TTmatrix

      SUBROUTINE rotate_vector(v,A ,theta)
	use vectors_f
      IMPLICIT NONE 
      
!axis MUST be unit vector
    
      
      REAL*8 v(3),A(3),theta

!locals

      REAL*8 ND ! normal distance
      REAL*8 DVA, axv(3),axvxa(3)
      REAL*8 dummy
      integer K
      LOGICAL Error
      DVA=dot_product(v,A)


      ND = ( norm(V)**2 - DVA**2)
      if (ND .lt. 0.0) ND = 0.0D00
      ND = DSQRT(ND)

      CALL Vprod(A ,V,AxV)

      CALL Normalise(AxV,dummy,error)
      if(error ) return

      CALL VPROD(AxV,A ,AxVxA)
!
!      ! AxVxA will be UNIT as AXIS and AxV are
!      and they are normal to each other
      DO K=1,3
      V(K) = A (K)  * DVA   + AxV(K) * ND * SIN(theta) + AxVxA(K) * ND * COS(theta)
      ENDDO

      END  SUBROUTINE rotate_vector


      SUBROUTINE New_PCB_Find_TTmatrix(  cbv,t)
	use vectors_f
      IMPLICIT NONE
! this routine finds the local element axes (rotation about the
! axis excepted) from the new CBV   alone

      REAL*8  cbv(3) !assumed unit vectorRloc(3,2),ttold(3,3),

!returns
      REAL*8 T(3,3)

       
      REAL*8 dummy, beta
           
      REAL*8 cob,sib, ang
      REAL*8 Xaxis(3) 
	data  Xaxis / 1.0d00,2*0.0d00/
      REAL*8 xa(3),Xb(3)  , BN, BML, BA,BL
      REAL*8:: uptest = 0.7071
     
      LOGICAL error
      integer I,j,K
      
  
      DO  I=1,3
              t(1,I)=CBV(I)
      ENDDO

      ANG=0.0D00
    
      Beta = 0.0 
      COB=1.0D00
      SIB=0.0D00
      IF(t(1,3).GT.1.0D0) t(1,3)=1.0D0
      BN=t(1,3)
      BA=SQRT(1.0D00-BN*BN)


      IF(BA .GT. UPTEST) THEN

        BML=t(1,2)/BA
        BL=t(1,1)/BA
        t(2,1)=-COB*BML-BN*BL*SIB
        t(2,2)=BL*COB-BN*BML*SIB
        t(2,3)=BA*SIB
        t(3,1)=BML*SIB-BN*BL*COB
        t(3,2)=-BL*SIB-BN*BML*COB
        t(3,3)=BA*COB
      ELSE

        CALL Vprod( CBV,Xaxis,XA)
        if(DOT_product(XA,XA) .lt. 1.0d-8) THEN ! beam lies along X axis
          DO j=1,3
           DO K=1,3
            t(j,k) = 0.0d00
           ENDDO
           t(j,j) = 1.0d00
          ENDDO

         ELSE
          CALL Normalise(XA,dummy,error)
          CALL Vprod(CBV,XA,XB)
          CALL Normalise( XB,dummy,error)
          DO  K=1,3
           t(1,K) = CBV(K)
           t(2,K) = XA(K)
           t(3,K) = XB(K)
          ENDDO
         ENDIF
       ENDIF
      
   
      END SUBROUTINE New_PCB_Find_TTmatrix

       end module batn_els_f

