module dofolist_f
USE ftypes_f
use dofo_subtypes_f
USE  linklist_f
use vectors_f
 
IMPLICIT NONE
integer  DOFOLISTBLOCKSIZE
parameter (DOFOLISTBLOCKSIZE = 16 )

contains



function cf_set_dofo_eptr(sli,dofon, ptr)  result(ok) bind(C,name= "cf_set_dofo_eptr")
    USE, INTRINSIC :: ISO_C_BINDING
    use saillist_f
    use ftypes_f
    use basictypes_f
   
    implicit none
 ! arguments
    integer(kind=c_int) ,intent(in),value ::sli,dofoN    
    integer(kind=cptrsize) ,intent(in),value ::ptr

!return value
        integer (c_int) :: ok

! locals
        INTEGER  :: err



#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif
	TYPE ( dofolist) ,POINTER :: dofos

	err=16
	ok=0
	if(.not. associated(saillist)) return
	if(sli > ubound(saillist,1)) return	
	if(sli < lbound(saillist,1)) return
 
	dofos=>saillist(sli)%dofos
	err=0 ; ok=1

     d=>dofos%list(dofoN)%o 
     d%m_rxfptr=ptr
end function cf_set_dofo_eptr
    

FUNCTION   NextFreeDOFOHolder(sli,err) result(dh)
        USE realloc_f
        USE saillist_f
        IMPLICIT NONE
        integer, intent(in)   :: sli
        INTEGER,INTENT (out)  ::  err
        TYPE ( DOFOlist) ,POINTER :: dList
        type (DOFOholder), pointer:: dh
        integer:: i,error,okreturn,n
        dh=>NULL()
        okreturn = 0
        err = 0
!$OMP CRITICAL (CRdofolist)
       dlist=>saillist(sli)%dofos

!  trawl  dlist for one without 'used'
! if we get one return it.
    if(associated(dlist%list)) then
        do i=1,ubound(dlist%list,1)
            if(.not. dlist%list(i)%used) then
                dh=>dlist%list(i)
                dh%used=.true.
                dh%nn=i
                dlist%count=max(dlist%count,i)
                goto 999
            endif
        enddo
! didnt find one so reallocate
    i = ubound(dlist%list,1)
    else
    i=0
    endif
    n = i + dlist%dblock
    call reallocate(dlist,n,error)
    dlist%list(i+2:)%used=.false.
    dh=>dlist%list(i+1)
    dh%used=.true.
    dh%nn=i+1
    dlist%count=max(dlist%count,i+1)
999  continue
!$OMP END CRITICAL (CRdofolist)
END  FUNCTION NextFreeDOFOHolder

! CreateDOFO returns a virgin DOFO. The type is set but the subtype isnt allocated
! we need to create it in the sli of the slave
 
function initialise_dofo(d,p_dofotype) result(err)!  dont touch members N or sli
        implicit none
        integer, intent(in) :: p_dofotype
#ifdef NO_DOFOCLASS
       type (dofo),intent(inout) ::d   ! or class
#else
       class (dofo),intent(inout)::d  ! or type
#endif
        integer ::err
        err=0

        d% m_rxfptr  =0
        d%ndof=0
        d%m_dofotype=p_dofotype
        d%IsLinear = (p_dofotype <=5)
        d%jacobi=>NULL()
        d%invjacobi=>NULL()
        d%m_Fnodes=>NULL()
        d%t=>NULL()
        d%vt=>NULL()
        d%rt=>NULL()
        d%xmt=>NULL()
        d%bt=>NULL()
        d%d1=>NULL()
        d%d2=>NULL()
        d%d3=>NULL()
        d%d4=>NULL()
        d%d5=>NULL()
        d%d6=>NULL()
        d%m_DofoLine=""
end function initialise_dofo



recursive function DOFO_Empty(d) result(err)
use fnoderef_f
use dofoprototypes_f
use coordinates_f, only : get_Cartesian_X_By_Ptr
implicit none
#ifdef NO_DOFOCLASS
           type (dofo),pointer, intent(inout) ::d  ! or class
#else
           class (dofo),pointer, intent(inout) ::d ! or type
#endif
integer :: i,j,err,ok
type (fnoderef_p) , pointer :: nref
type (fnode) , pointer :: n
real(kind=double), dimension(3) :: xx
err=0

!    write(outputunit,'("empty dofo (",i4,",",i6,"), type=",i," {",a,"}" )',iostat=i) , d%m_dofosli  ,d%m_dofoN, d%m_dofotype,trim(d%m_DofoLine)
   if(associated(d%parent1)  .or.  associated(d%parent2)  ) then
       ok = SetParentTeesBySV(d,.FALSE.) !in DOFO_Empty
       if(associated(d%parent1)) then;
           d%parent1%child=>NULL();
        endif
       if(associated(d%parent2)) then
            d%parent2%child=>NULL()
       endif
    endif 
  
    if(  associated(d%child)  ) then
        i = d%child%m_dofosli; j= d%child%m_dofoN;
        err=   DestroyDofo (i,j) 
        d%child=>NULL()
    endif 
!DEC$ NOPARALLEL
    do while (associated(d%m_Fnodes) .and. ubound(d%m_Fnodes,1)>0 )
        i=1;
        nref=>d%m_Fnodes(i)
        n=>decodeNodeRef(nref )
        xx=  get_Cartesian_X_By_Ptr(n)

        ok = unhookdofoandnode(n,d)
        if(.not.IsUnderDofoControl(n)) then
           n%XXX= xx
      !  else
      !     write(outputunit,'("DONT SET XXX-empty dofo (",i4,",",i6,"), type=",i," {",a,"}" )',iostat=i) , d%m_dofosli  ,d%m_dofoN, d%m_dofotype,trim(d%m_DofoLine)
        endif
    enddo

        
    if(associated(d%origin ))   deallocate( d%origin)  ;d%origin=>NULL() 
	if(associated(d%m_Fnodes )) deallocate(d%m_Fnodes); d%m_Fnodes=>NULL() 	
	if(associated(	d%jacobi))  deallocate(d%jacobi);   d%jacobi=>NULL()
	if(associated(d%invjacobi)) deallocate(d%invjacobi);d%invjacobi=>NULL()
	if(associated(d%t  ))       deallocate( d%t  );     d%t =>NULL()
	if(associated(d%vt  ))      deallocate( d%vt );     d%vt =>NULL()
	if(associated(d%rt  ))      deallocate( d%rt );     d%rt =>NULL()
	if(associated(d%xmt ))      deallocate( d%xmt );    d%xmt =>NULL()
	if(associated(d%bt  ))      deallocate( d%bt  );    d%bt =>NULL()

    if(associated(d%d1))    deallocate( d%d1); d%d1=>NULL() ! these subtypes happen NOT to contain any allocated members        
    if(associated(d%d2))    deallocate(d%d2 ); d%d2=>NULL()
    if(associated(d%d3))    deallocate(d%d3 ); d%d3=>NULL()
    if(associated(d%d4))    deallocate(d%d4);  d%d4=>NULL()! these two as a guess. they seem to be omitted march 2010.
    if(associated(d%d5))  then
         call d%d5%m_spl%ClearFSpline()
         if(allocated(d%d5%OriginalSlaveTees)) deallocate(d%d5%OriginalSlaveTees )
         if(allocated(d%d5%SlaveTeeOrigin)) deallocate(d%d5%SlaveTeeOrigin )
         deallocate(d%d5)
    endif 
    d%d5=>NULL() 
    if(associated(d%d6))  then
         if(allocated(d%d6%xl)) deallocate(d%d6%xl )
         deallocate(d%d6)
    endif
    d%d6=>NULL()
    d%m_DofoLine=""
	d%m_dofotype=C_DOFO_UNDEFINED	
	d%m_rxfptr=0;   ! shoud already be zero
	d%ndof=0!;d%ns=0;   d%ke=-1    

end function DOFO_Empty
! this is  called from Bring_All
subroutine DofoAllJacobians(sli,err) bind(C,name="cf_DofoAllJacobians" )
    USE, INTRINSIC :: ISO_C_BINDING
    use dofoprototypes_f
    use coordinates_f
    USE  saillist_f
    IMPLICIT NONE
    integer(kind=c_int) ,intent(in),value ::sli
        INTEGER(kind=c_int)  , intent(inout) :: err
        integer :: ok
        INTEGER  :: k
        TYPE(Dofolist),POINTER ::Dofos
#ifdef NO_DOFOCLASS
            type (dofo),pointer :: d! or class
#else
            class (dofo),pointer ::d
#endif
        err=16
        ok=0
        if(.not. associated(saillist)) return
        if(sli > ubound(saillist,1)) return
        if(sli < lbound(saillist,1)) return

        Dofos=>saillist(sli)%Dofos

        err=8
        if(.not. ASSOCIATED(Dofos%list)) return
        if(err /=0) OK=1
    err=0
!    write(*,'("DofoAllJacobians" ,i6, "  ", $ )') sli
!DEC$ NOPARALLEL
    do k = 1,dofos%count
        if(.not.dofos%list(k)%used) cycle
        d=>dofos%list(k)%o
        ok = ComputeDOFOJacobian(d)
    enddo

    if(err /=0) OK=0
!    write(*,'("done DofoAllJacobians" ,i6 )') sli
end  subroutine DofoAllJacobians
end module dofolist_f


!extern "C"  int cf_delete_dofo (const int sli,const int dofoN);

recursive function DestroyDofo (sn, NN)  result(err) bind(C,name="cf_delete_dofo")
    USE, INTRINSIC :: ISO_C_BINDING
    USE  saillist_f
    USE realloc_f
    use dofolist_f
    use cfromf_f
    use dofoprototypes_f
    IMPLICIT NONE
         INTEGER (c_int), intent(in),value :: sn,nn
        INTEGER(c_int)  :: err,istat
	TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
        type (dofo),pointer ::d ! or class
#else
        class (dofo),pointer ::d
#endif
         INTEGER(kind=cptrsize):: ptr
	err = 16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return
	Dofos=>saillist(sn)%Dofos
!
!   What it does.
!	unsets the USED flag. realloc if  it can
!
! USUAL Conditions

!   NN =  count  >> decrement count
!    if then count < ubound-block, reallocate
!
   
! Unusual conditions
!   sailno out of range  >> err 16
!   Dofolist not associated  >>err 8
! NN < lbound or > Ubound  >>err 8
!	write(outputunit,*) ' Destroy DOFO sli,nn=',sn,nn
	err=8
	if(.not. ASSOCIATED(Dofos%list)) return

        if(NN > UBOUND(Dofos%list,1)) then
            write(*,*) ' ??? WHY destroy dofo ',nn,' of', UBOUND(Dofos%list,1); return
        endif

        if(NN  <LBOUND(Dofos%list,1)) then
            write(*,*) ' WHY destroy dofo ',nn,' < Lbound ', LBOUND(Dofos%list,1);  return
        endif

    if(.not. Dofos%list(NN)%used) return
    d=>Dofos%list(NN)%o
    if( d%m_dofosli  /= sn .or. d%m_dofoN /= nn) write(outputunit,*) ' mismatch in dofo destroy '
    if(d%m_rxfptr .ne.0) then  !WRONG. whats the point of calling fc_delete_ with a zero ptr????????
        ptr = d%m_rxfptr;
        d%m_rxfptr=0;      ! april 2012 changed the order of these two lines so a destrpy can be initiated here or in the C object's destructor
        istat=  fc_delete_object(ptr)    ! see  RXRigidBody::~RXRigidBody()
    endif

	err= DOFO_Empty(d) ! only called here
        if(Dofos%list(NN)%NN .ne. nn) write(*,*) ' NN mismatch in dofo deStroy'
	Dofos%list(NN)%used=.false.
	deallocate(Dofos%list(NN)%o,stat=istat); 
	Dofos%list(NN)%o=>NULL()

! try to free a chunk of the dofolist. Can do if the last (block) elements are not used.
     if(dofos%count .gt.0 ) then
dw:        do while(  .not.dofos%list(dofos%count)%used)
            dofos%count = dofos%count-1
            if( Dofos%count <= lbound(Dofos%list,1) ) then  ! april 2014 caught a bounds check error
                 exit dw
            endif
            if( Dofos%count+Dofos%dblock <= ubound(Dofos%list,1) ) THEN
                CALL reallocate(Dofos, ubound(Dofos%list,1) - Dofos%dblock, err)
            ENDIF
        enddo dw
     endif
END function DestroyDofo

function DeleteAllDOFOs(sli,err) result(ok) !see prototype in dofoprototypes.f
use dofolist_f
use dofoprototypes_f
USE  saillist_f
IMPLICIT NONE
	INTEGER , intent(in) :: sli
	INTEGER , intent(inout) :: err
	integer :: ok
	INTEGER  :: k,k1,k2
	TYPE ( Dofolist) ,POINTER :: Dofos
	err=16
	ok=0
	if(.not. associated(saillist)) return
	if(sli > ubound(saillist,1)) return	
	if(sli < lbound(saillist,1)) return
	Dofos=>saillist(sli)%Dofos
	
		err=8
	if(.not. ASSOCIATED(Dofos%list)) return
	k2 =UBOUND(Dofos%list,1)
	k1 =LBOUND(Dofos%list,1)
	do k = k2,k1,-1
        err= DestroyDofo (sli, k) 	
	enddo
	if(err /=0) OK=1
end  function DeleteAllDOFOs

! public, see dofoprototypes_f

function CreateDOFO(sli, p_dofotype ,err) result (d) ! does the allocation
	USE saillist_f
	use dofolist_f
	use dofoprototypes_f
	integer, intent(in)		:: sli	! sailListIndex
	integer, intent(in)		:: p_dofotype 
	integer, intent(out)	:: err
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif
	type(dofoholder), pointer :: dh
	dh=> nextFreeDOFOholder(sli,err)
	allocate(d)
	dh%o=>d

        d%m_dofosli  = sli ;d%m_dofoN = dh%nn;
   	err= initialise_dofo(d,p_dofotype)
   ! write(outputunit,'("create dofo (",i4,",",i6,"), type=",i," {",a,"}" )',iostat=i) , d%m_dofosli  ,d%m_dofoN, d%m_dofotype,trim(d%m_DofoLine)
end function CreateDOFO

subroutine DofoTransformAll(sli,mt,err) ! see 'dofos and transforms.doc'
    use basictypes_f
    use dofolist_f
    use dofoprototypes_f
    USE  saillist_f
    use connects_f
    IMPLICIT NONE   
	INTEGER , intent(in) :: sli
	REAL (kind=double), dimension(4,4):: mt 
	INTEGER , intent(inout) :: err	
	integer :: ok
	INTEGER  :: k 
	TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif
	err=16
	ok=0
	if(.not. associated(saillist)) return
	if(sli > ubound(saillist,1)) return	
	if(sli < lbound(saillist,1)) return
	
	Dofos=>saillist(sli)%Dofos
	
		err=8
	if(.not. ASSOCIATED(Dofos%list)) return
	if(err /=0) OK=1
        err=0
	do k =  LBOUND(Dofos%list,1), UBOUND(Dofos%list,1) 
	    if(.not.dofos%list(k)%used) cycle	
            d=>dofos%list(k)%o
            err=err+ TransformDofo (d ,mt)
	enddo
	if(err /=0) OK=1	
end  subroutine DofoTransformAll 
 
subroutine CollectDofosForJay( ll  ,err)  ! an experimental fn to collect dofos in a particular order
use saillist_f
IMPLICIT NONE
   INTEGER ,intent(out) :: err
   TYPE (dofolist),intent(inout) :: ll
        err=0
        call CollectDofosToList( ll  , DOFO_EITHER, DOFO_EITHER, C_DOFO_NODENODE_CONNECT, err)
        call CollectDofosToList( ll  , DOFO_EITHER, DOFO_EITHER, C_DOFO_NODESPLINE_CONNECT, err)
        call CollectDofosToList( ll  , DOFO_EITHER, DOFO_EITHER, C_DOFO_STRINGSPLINE_CONNECT, err)
        call CollectDofosToList( ll  , DOFO_EITHER, DOFO_EITHER, C_DOFO_RIGIDBODY, err)
        call CollectDofosToList( ll  , DOFO_EITHER, DOFO_EITHER, C_DOFO_FIXED_GLE, err)
        call CollectDofosToList( ll  , DOFO_EITHER, DOFO_EITHER, C_dofo_FixedPoint, err)
        call CollectDofosToList( ll  , DOFO_EITHER, DOFO_EITHER, C_dofo_SlideFixedLine, err)
        call CollectDofosToList( ll  , DOFO_EITHER, DOFO_EITHER, C_dofo_SlideFixedPlane, err)
        call CollectDofosToList( ll  , DOFO_EITHER, DOFO_EITHER, C_DOFO_SlideFixedCurve, err)
        call CollectDofosToList( ll  , DOFO_EITHER, DOFO_EITHER, C_dofo_SlideFixedSurface, err)
        call CollectDofosToList( ll  , DOFO_EITHER, DOFO_EITHER, C_DOFO_FREE_GLE, err)
        call CollectDofosToList( ll  , DOFO_EITHER, DOFO_EITHER, C_DOFO_COMPOSITE, err)
end subroutine CollectDofosForJay

subroutine CollectDofosToList( ll  , parentQ, childQ, dofoTypeFilter, err)
! parentQ and childQ can have values DOFO_ONLY or DOFO_EXCEPT
! DOFO_ONLY means take  the dofo IFF it has one
! DOFO_EXCEPT means take it IFF it doesnt have one
! IE to take all without parents , regardless of child status we go
!  parentQ = DOFO_EXCEPT, childQ= DOFO_EITHER
! dofotypefilter. If >0, we only take those with matching type (see dofo_subtypes.f90 for values.
use saillist_f
use realloc_f

IMPLICIT NONE
   INTEGER ,intent(out) :: err
   TYPE (dofolist),intent(inout) :: ll
   integer, intent(in) :: parentQ, childQ,dofoTypeFilter
   TYPE ( Dofolist) ,POINTER :: Dofos

#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif
   INTEGER :: i,ok,j,sli

   ok=0
   if(.not. associated(saillist)) return
dosli:    do sli = 1,ubound(saillist,1)
           if (0== is_Hoisted(sli) ) cycle
           if (0== is_active(sli)) cycle
           Dofos=>saillist(sli)%Dofos
           if(.not. ASSOCIATED(Dofos%list)) cycle

           do i =1, dofos%count
               if (.not. associated(Dofos%list(i)%o))  cycle
               if(.not. Dofos%list(i)%used) cycle
               d=>Dofos%list(i)%o
               if( dofoTypeFilter>0 .and. d%m_dofotype .ne. dofoTypeFilter) cycle
               if(parentQ .eq. DOFO_EXCEPT  .and. associated(d%parent1)) cycle;
               if(parentQ .eq. DOFO_ONLY  .and. .not. associated(d%parent1)) cycle;

               if(childQ .eq. DOFO_EXCEPT  .and. associated(d%child)) cycle;
               if(childQ .eq. DOFO_ONLY  .and. .not. associated(d%child)) cycle;

               ll%count=ll%count+1;
               j=  ll%count
               if(j > ubound(ll%list,1)) then
                 call reallocate(ll ,  ubound(ll%list,1) + ll%dblock,err)
                 if(err /=0) exit dosli
                endif
               ll%list(j)%o=>d
           enddo
        enddo dosli
    ok=1
    !write(*,*) '(CollectDofos ToList) N parent-less DOFOs = ', ll%count
    ! ll is now a list of all parent-less (but with children) dofos in the world.
    ! a typical SP has 41
    ! then do the Pull to adjust their tees so the common Xs agree

end subroutine CollectDofosToList

subroutine CollectDofosForPull(ll,err)
use saillist_f
use ftypes_f
use realloc_f

IMPLICIT NONE

   INTEGER ,intent(out) :: err
   TYPE (dofolist),intent(inout) :: ll
   TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
       type (dofo),pointer ::d  ! or class
#else
       class (dofo),pointer ::d ! or type
#endif

   INTEGER :: i,j,sli

   if(.not. associated(saillist)) return
dosli:    do sli = 1,ubound(saillist,1)
           if (0== is_Hoisted(sli) ) cycle
           if (0== is_active(sli)) cycle
           Dofos=>saillist(sli)%Dofos
           if(.not. ASSOCIATED(Dofos%list)) cycle

           do i =1, dofos%count
           if (.not. associated(Dofos%list(i)%o))  cycle
           if(.not. Dofos%list(i)%used) cycle
           d=>Dofos%list(i)%o
           if(associated(d%parent1)) cycle;
           if(.not. associated(d%child)) cycle; ! it has no siblings
           ll%count=ll%count+1;
           j=  ll%count
           if(j > ubound(ll%list,1)) then
             call reallocate(ll ,  ubound(ll%list,1) + ll%dblock,err)
             if(err /=0) exit dosli
            endif
           ll%list(j)%o=>d; ll%list(j)%used=.true.
       enddo
    enddo dosli

    !write(*,*) '(CollectDofosForPull) N parent-less DOFOs = ', lls%count
    ! ll is now a list of all parent-less (but with children) dofos in the world.
    ! a typical SP has 41

end subroutine CollectDofosForPull

subroutine SyncDofos() ! an experimental patch - like FillFnode. Didnt work
    use connects_f
    use saillist_f
    use ftypes_f
    use dofolist_f
    use dofoprototypes_f
    implicit none

character(len=32) :: sname
TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif
INTEGER :: i,sli,ds,dn,err
! 1) UnresolveCOnnects
! 2) trawl dofos and delete all C_DOFO_COMPOSITE
! 3)  shift origin of any fixed nodes (!)

sname = ""
call UnResolve_Connects(sname,32)

dosli:    do sli = 1,ubound(saillist,1)
           if (0== is_Hoisted(sli) ) cycle
           if (0== is_active(sli)) cycle
           Dofos=>saillist(sli)%Dofos
           if(.not. ASSOCIATED(Dofos%list)) cycle

            i = 0
dw:         do while( i < dofos%count)
                i=i+1
               if (.not. associated(Dofos%list(i)%o))  cycle
               if(.not. Dofos%list(i)%used) cycle
               d=>Dofos%list(i)%o
               if(d%m_dofotype ==C_DOFO_COMPOSITE) then
                    ds =d%m_dofosli ; dn = d%m_dofoN;
                    err=DestroyDofo ( ds ,dn)
                   i= 0
               endif
       enddo dw
    enddo dosli




end subroutine SyncDofos
