MODULE edgeList_f
USE ftypes_f
USE  linklist_f
USE  saillist_f
IMPLICIT NONE
 integer, parameter:: EDGEBLOCKSIZE=5000
CONTAINS


integer function HookAllEdges() bind(C)
        IMPLICIT NONE

        INTEGER I,sli,n
        Type(sail) ,pointer::s
        TYPE ( edgeList) ,pointer   	:: ee 
        type(Fedge) , pointer :: e  
        HookAllEdges=0
               
        do sli=1,SailCount
        if( .not. is_used(sli)) cycle
            s=>saillist(sli)  
          
          ee=>s%edges
          do i = 1, ee%count
              e=>ee%list(i)
              if (.not. e%m_Eused)  cycle
              n = e%m_L12(1);
              if(n>0) then
              e%m_ee1=>s%nodes%xlist(n)
              endif
              n = e%m_L12(2);
              if(n>0) e%m_ee2=>s%nodes%xlist(n)
                
          enddo
        ENDDO
        HookAllEdges=1
 
end function HookAllEdges

function ReserveEdgeSpace(sli, n) result(ok) bind(c)
	USE realloc_f
	USE saillist_f  
	IMPLICIT NONE
	integer, intent(in) :: sli,n
	INTEGER  ::  ok
	integer ::  istat 
	TYPE ( edgelist) ,POINTER :: edges
	ok = 0
	edges=>saillist(sli)%edges
ifna: if(.not. ASSOCIATED(edges%list)) THEN
		edges%Block= EDGEBLOCKSIZE  
		ALLOCATE(edges%list(n), stat=istat)
		g_PleaseHookElements = .true.
 		if(istat /=0) THEN
			return
		endif
		ok=1; return
!	else ifna
!		write(outputunit,*) ' edges aready started'
    endif ifna
   
end function ReserveEdgeSpace

SUBROUTINE CreateFortranEdge (sn, NN,ptr,err) 
USE realloc_f
USE, INTRINSIC :: ISO_C_BINDING
IMPLICIT NONE
	INTEGER , intent(in) :: sn,nn
	integer(kind=cptrsize), intent(in) ::ptr
	INTEGER , intent(out) :: err
	TYPE ( edgelist) ,POINTER :: edges
	INTEGER :: istat
	err = 16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return
	edges=>saillist(sn)%edges
	err=8
	if(NN < 1) return
	err = 0
!	write(outputunit,*) ' CreateFortranEdge ',sn, NN,err
!
! Normal conditions
!	just starting : 
!	NN <= count. >> set X,Y,Z used
!	NN > ubound >> realloc then set X,Y,Z used  if alloc OK. set count
!	NN > count but < Ubound  set count.set  X,Y,Z used

! error conditions
!   unable to alloc
!   NN < 0
!
	if(.not. ASSOCIATED(edges%list)) THEN
		edges%block=EDGEBLOCKSIZE
		ALLOCATE(edges%list(edges%block), stat=istat)
		g_PleaseHookElements = .true.
		if(istat /=0) THEN
		write(outputunit,*) ' cannot allocate for edges ', edges%block
			err = 1
			return
		endif
		edges%count=0
	ENDIF
	DO
		if(NN >= UBOUND(edges%list,1)) THEN
                       ! write(outputunit,*) 'N edges = ', ubound(edges%list,1) + edges%block
			CALL reallocate(edges%list, ubound(edges%list,1) + edges%block, err)
		    g_PleaseHookElements = .true.
			if(err > 0) return
		ELSE
			exit
		ENDIF
	ENDDO
		if(NN > edges%count) edges%count= NN
		NULLIFY(edges%list(NN)%m_ee1)
		NULLIFY(edges%list(NN)%m_ee2)	
		edges%list(NN)%m_Dlink=0.0
		edges%list(NN)%m_Delta0=0.0
		edges%list(NN)%m_zlink0=0.0
		edges%list(NN)%m_tlink=0.0		
		edges%list(NN)%N=NN
		edges%list(NN)%m_L12=0
		edges%list(NN)%m_Eused= .FALSE.
		edges%list(NN)%Cedgeptr = ptr

END SUBROUTINE CreateFortranEdge 

SUBROUTINE RemoveFortranEdge (sn, NN,err) 
USE realloc_f
use cfromf_f  ! for zeroptr
IMPLICIT NONE
	INTEGER , intent(in) :: sn,nn
	INTEGER , intent(out) :: err
	TYPE ( edgelist) ,POINTER :: edges
        TYPE ( nodelist) ,POINTER :: nodes
	err = 16
!	write(outputunit,*) 'RemoveFortranEdge', sn, NN,err
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return
	edges=>saillist(sn)%edges
        nodes=>saillist(sn)%nodes
!
!   What it does.
!	unsets the USED flag. realloc if  it can
!
! USUAL Conditions

!   NN =  count  >> decrement count
!    if then count < ubound-block, re-allocate
!

! Unusual conditions
!   sailno out of range  >> err 16
!   edgelist not associated  >>err 8
! NN < lbound or > Ubound  >>err 8
!
	err=8
	if(.not. ASSOCIATED(edges%list)) return
	if(NN > UBOUND(edges%list,1)) return
	if(NN  <LBOUND(edges%list,1)) return
	err=0
        edges%list(NN)%Cedgeptr=0
        edges%list(NN)%m_Eused = .FALSE.
        edges%list(NN)%m_Dlink=0.0
        edges%list(NN)%m_Delta0=0.0
        edges%list(NN)%m_zlink0=0.0
        edges%list(NN)%m_tlink=0.0
        edges%list(NN)%m_L12=0
        if(associated(nodes%xlist)) then  ! not watertight:
		if(associated( edges%list(NN)%m_ee1 )) then
		    edges%list(NN)%m_ee1%m_elementcount = edges%list(NN)%m_ee1%m_elementcount-1
		endif
		if(associated(edges%list(NN)%m_ee2 )) then		
		    edges%list(NN)%m_ee2%m_elementcount = edges%list(NN)%m_ee2%m_elementcount-1	
		endif
        endif
        NULLIFY(edges%list(NN)%m_ee1)
        NULLIFY(edges%list(NN)%m_ee2)
	
	if(NN == edges%count) THEN ! count should be the last index in edges%list which is m_used;
dw:     do while( (.not. edges%list(edges%count)%m_Eused))
 		    edges%count =edges%count-1  
 		    if( edges%count .le. 0) exit dw   
        enddo dw
		if( edges%count+edges%block < ubound(edges%list,1) ) THEN
			CALL reallocate(edges%list, ubound(edges%list,1) - edges%block, err)
		    g_PleaseHookElements = .true.
		ENDIF
	ENDIF

END SUBROUTINE RemoveFortranEdge

SUBROUTINE SetEdgeUsed   (sn, NN,flag,err)
IMPLICIT NONE
	INTEGER , intent(in) :: sn,nn,flag
	INTEGER , intent(out) :: err
	TYPE ( edgelist) ,POINTER :: edges
!
! sets the Used flag depending on the value of 'flag'
!  the usual error returns. 
!  the only twist is that if this is the last edge, we decrement count if flag=0
!  similarly, we increment count if NN is big. But only up to 
!
!	write(outputunit,*) 'SetEdgeUsed   ', sn, NN,flag
	err=16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	err=8
	edges=>saillist(sn)%edges
	if(.not. ASSOCIATED(edges%list)) return
	if(NN > UBOUND(edges%list,1)) return
	if(NN  <LBOUND(edges%list,1)) return
	err=0
	if(flag/=0) THEN
		if(edges%list(NN)%m_Eused) err=2
		edges%list(NN)%m_Eused=.TRUE.
		if(NN > edges%count) edges%count=NN 
	else
		if(.NOT. edges%list(NN)%m_Eused) err=1
		edges%list(NN)%m_Eused=.FALSE.
		if(NN ==  edges%count) edges%count=edges%count-1 
	endif

END SUBROUTINE SetEdgeUsed 

SUBROUTINE  PrintFedges (sn,unit) 
USE ftypes_f
USE fglobals_f

IMPLICIT NONE
	INTEGER , intent(in) :: sn,unit

	TYPE ( edgelist) ,POINTER :: edges
	TYPE ( Fedge ),pointer ::e
	INTEGER :: i 
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return


	edges=>saillist(sn)%edges
	if(.not. ASSOCIATED(edges%list)) return

	write(unit,*) ' F EdgeList for model ', trim(saillist(sn)%sailname)
10	format(2i4, 1x,L4,1x ,2i6,3g13.4,2x,3G10.3)
	do i =1,edges%count !   UBOUND(edges%list,1)
	    e=>edges%list(i)
	    if(.not. e%m_Eused) cycle
    
 	    write(unit,10,err=11) i,  e%N, e%m_Eused,  &
 		e%m_L12	,	e%m_DLINK,&
		e%m_DELTA0, e%m_ZLINK0 , e%m_TLINK

!		TYPE(Fnode) ,POINTER ::  e1 ,e2 
!		REAL (kind=double) ,dimension(3)::DLINK
!		REAL (kind=double)  ::DELTA0, ZLINK0 , TLINK
!		INTEGER ,dimension(2) :: L12
!		TYPE(Fnode) ,POINTER ::  e1 ,e2 
!		INTEGER :: N 
!		type(C_PTR) ::Cedgeptr
!		LOGICAL :: used
11     continue
	ENDDO
	write(unit,*) ' count is',edges%count

END  SUBROUTINE  PrintFedges 

! make node list.  Checks that the list of edges is contiguous 
! and fills in he corresponding sequence of nodes.
! doesnt re-order the edges or change their reversed flags
function Make_Node_List(sli,count,se,rev,list) result(ok)
      use coordinates_f
      IMPLICIT NONE   
      integer, intent(in) :: sli   
      integer, intent(inout) :: count ! count is no of edges in se
      INTEGER, dimension(count),intent(in) :: rev(count)
      INTEGER, dimension(count)            :: se(count)
!*returns
      INTEGER, dimension(count+1) , intent(out) :: List	! node list
 
! locals
        TYPE (Fedge), DIMENSION(:), POINTER :: eds
        TYPE (Fedge),  POINTER :: e 
      INTEGER i,LL,next	

       integer::ok; ok =1     

    eds=>saillist(sli)%edges%list
    
	if(count <=2 ) then  ! was <2 till june 2005
		write(outputunit,*)' MakeNodeList. short count  ',count
		list(1)=0
		count=0
		ok=0
		return
	endif
	
        LL = se(1); e => eds(LL)
        if(rev(1) .ne.0) then 
               list(1) = e%m_L12(2)
               list(2) = e%m_L12(1)
        else
               list(1) = e%m_L12(1)
               list(2) = e%m_L12(2)
        endif
       next= list(2)        
  
doi:   DO I = 2,count
       LL = se(i); e => eds(LL)
       if(rev(i) .eq. 0) then
             if( e%m_L12(1) .ne. next) ok=0    
             Next = e%m_L12(2)
        else
             if( e%m_L12(2) .ne. next) ok=0 
             Next = e%m_L12(1)       
        endif
        list(I+1) = Next  
        enddo doi            
        if(ok.eq.0) then
        write(outputunit,*) ' edgelist isnt contiguous '
        do i = 1,count
            write(outputunit,*) se(i), (rev(i).ne. 0),eds(se(i))%m_L12(1),eds(se(i))%m_L12(2)
        enddo
        endif
  
END function Make_Node_List



END MODULE edgeList_f


SUBROUTINE  ShowFedgeCount(sn, u,l,c, err) 
USE ftypes_f
USE edgelist_f
IMPLICIT NONE
	INTEGER , intent(in) :: sn
	INTEGER , intent(out) :: err,u,l,c

	TYPE ( edgelist) ,POINTER :: edges

	err = 16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	err=8
	edges=>saillist(sn)%edges
	if(.not. ASSOCIATED(edges%list)) return
	u =UBOUND(edges%list,1)
	L  =LBOUND(edges%list,1)
	c = edges%count

	err=0

END SUBROUTINE  ShowFedgeCount


SUBROUTINE removeAllFEdges(sn,err)
use ftypes_f
use saillist_f
use edgelist_f

IMPLICIT NONE
	INTEGER , intent(in) :: sn
	INTEGER , intent(out) :: err

	integer i,SomeErrors
	TYPE ( edgelist) ,POINTER :: edg
	err = 16
	SomeErrors=0
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	edg=>saillist(sn)%edges
	do i = edg%count,1,-1
		call RemoveFortranEdge (sn, i,err) 
		if(err /=0) SomeErrors = SomeErrors+1
	enddo
	if(associated(edg%list)) deallocate(edg%list,stat=err)
	if(SomeErrors /=0) then
		write(outputunit,*) 'model',sn,' removeFortranEdge (from remove ALL) Nerr=',SomeErrors
	endif
END SUBROUTINE removeAllFEdges





