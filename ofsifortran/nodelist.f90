MODULE nodeList_f
USE ftypes_f
USE  linklist_f
USE  saillist_f
use cfromf_f 
IMPLICIT NONE

CONTAINS
SUBROUTINE CreateFortranNode (sli, NN,ptr,err) 
USE realloc_f
USE saillist_f
use cfromf_f  ! for zeroptr
IMPLICIT NONE
	INTEGER , intent(in) :: sli,nn
    integer(kind=cptrsize) , intent(in),value  :: ptr  ! rxfesite
	INTEGER , intent(out) :: err
	TYPE ( nodelist) ,POINTER :: nodes
	INTEGER :: istat,scratch
	err = 16
	if(.not. associated(saillist)) return
	if(sli > ubound(saillist,1)) return	
	if(sli < lbound(saillist,1)) return

	nodes=>saillist(sli)%nodes

	err=8
	if(NN < 1) return
	err = 0

!
! Normal conditions
!	just starting : 
!	NN <= Ncount. >> set X,Y,Z used
!	NN > ubound >> realloc then set X,Y,Z used  if alloc OK. set Ncount
!	NN > Ncount but < Ubound  set Ncount.set  X,Y,Z used

! error conditions
!   unable to alloc
!   NN < 0
 
!$OMP CRITICAL (nodalxlist)
	if(.not. ASSOCIATED(nodes%Xlist)) THEN
	!	nodes%block=5000
		ALLOCATE(nodes%Xlist(nodes%block), stat=istat)
		if(istat /=0) THEN
			err = 1
		endif
		nodes%Ncount=0
	ENDIF
ie0:	if(err .eq. 0) then
	        DO
		        if(NN >= UBOUND(nodes%Xlist,1)) THEN
			        scratch =  ubound(nodes%Xlist,1) + nodes%block
			        CALL reallocate(nodes%Xlist, scratch, err)
			        g_PleaseHookElements=.true.
			        if(err>0) exit
		        ELSE
			        exit
		        ENDIF
	        ENDDO
	endif ie0
ie1: if(err .eq.0) then
	if(NN > nodes%Ncount) then
		nodes%Ncount= NN
                nodes%xlist(NN)%XXX=0.0
		nodes%xlist(NN)%V=0.0	
		nodes%xlist(NN)%R=0.0
				
		nodes%xlist(NN)%nn=NN
		nodes%xlist(NN)%m_sli=sli		
		nodes%xlist(NN)%m_pdofo=>NULL()

		nodes%xlist(NN)%B=0.
		nodes%xlist(NN)%xm=0.

		nodes%xlist(NN)%m_RXFEsiteptr = ptr
		nodes%xlist(NN)%m_isunderdofocontrol = .false.  ! initialisation
		nodes%xlist(NN)%masterdofoNo=-1                 ! initialisation
		nodes%xlist(NN)%m_connectSurf=0;	nodes%xlist(NN)%m_slider=0		
		nodes%xlist(NN)%m_rxeptr=0
		nodes%xlist(NN)%m_Nodal_Load=0
		nodes%xlist(NN)%m_trace= .false. 
		nodes%xlist(NN)%m_ElementCount=0 
		nodes%xlist(NN)%m_Nused= .TRUE.
		nodes%xlist(NN)%sixnoderef=0
#ifndef NOOMP
                call OMP_INIT_LOCK(nodes%xlist(NN)%m_lock)
#endif
    else
!        write(outputunit,*) ' ( CreateFortranNode  ) skip re-init of node ',nn

		if(ptr /= nodes%xlist(NN)%m_RXFEsiteptr) then
			if(0 /= nodes%xlist(NN)%m_RXFEsiteptr) then
				if(ptr /= 0) then
					 write(outputunit,*)' want NONnull to NONnull ptr on fnode ',nn
				else
				 	write(outputunit,*)' want  NONnull to null  on fnode ',nn
				endif

			else
				if(ptr /= 0) then
					 write(outputunit,*)' want  null to NONnull ptr on fnode ',nn
				endif
			endif

		endif
		if (.not. nodes%xlist(NN)%m_Nused) then
			write(outputunit,*)'setting m_Nused on fnode ',nn	
			nodes%xlist(NN)%m_Nused=.true.
		endif
    endif
    endif ie1
!$OMP END CRITICAL (nodalxlist)
END SUBROUTINE CreateFortranNode 

! august 2008 a nasty. Sometimes (!) zeroptr() seems to lose its zero value. Intel recognized a bug.
! a note (March 2014) on parameter-edits of nodes.
! An analysis node's position may be defined by a dofo, not by the node's XXX
! in that case, setting the XXX doesnt do anything: It's the dofos that need to be changed.
! here we unresolve any connects on the node's model as a stop-gap
! unresolving connects would be WRONG if this node isn't associated with a connect.
! because it wont destroy the right dofo.



subroutine  FillFNode(sli,nn,x,err) ! X is in model space
USE cfromf_f
USE saillist_f
use coordinates_f
use dofoprototypes_f  
use parse_f

IMPLICIT NONE
	INTEGER , intent(in) :: sli,nn
    real(c_Double), intent(in), dimension(3) ::x 
	INTEGER , intent(out) :: err
	TYPE ( nodelist) ,POINTER :: nodes
        type (Fnode), pointer:: theNode
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d2   ! or class
#else
           class (dofo),pointer::d2  ! or type
#endif
	integer(kind=cptrsize) :: csurf, slidecurve	
        integer ::kk ,ds,dn,count,i
        real(kind=double),dimension(3) ::oldXX,newXX
        INTEGER ,parameter :: ln=256
        CHARACTER (len=ln)::  sname
!$OMP CRITICAL (nodalxlist)
	nodes=>saillist(sli)%nodes
	err=8
	if(NN < 1) return
        if(.not. associated(nodes%xlist  )) return
	err=4
	if( lbound(nodes%xlist,1) > NN) return
	err = 0
!WRONG danger if we are calling this from Resolve, the csurf may turn out later to be unresolvable.
 	thenode=>nodes%xlist(NN) 
	if(.not. thenode%m_Nused) then
                write(outputunit,*)'WRONG filling unused  node ',nn
	endif 
        oldXX = thenode%XXX;
        oldXX = get_Cartesian_X_by_Ptr(thenode)
        newXX= Transform_CoordsBySLI(sli,X)
ch:if (any(oldXX-newXX>1E-9)) then
iudc:      if( thenode%m_isunderdofocontrol)   then
               sname = saillist(sli)%sailname
               if(.false. ) then
                   call unresolve_Connects(sname,256);
                else if (.false.) then
                    count = ubound(thenode%m_pdofo  ,1)
                    do while (count .gt.0 .and. associated(thenode%m_pdofo ) .and. ubound(thenode%m_pdofo  ,1)>0)
                        kk=1; count=count-1
                        d2=>thenode%m_pdofo(kk)%d
                        ds =d2%m_dofosli ; dn = d2%m_dofoN;
                        write(outputunit,'(a,i5,i5,a,i5,i5,a  )')"should touch dofo ",ds,dn, " while filling node (",sli,nn,")"
                        err=err+DestroyDofo ( ds ,dn)
                    enddo
                else
                    call unresolve_Connects(sname,256);
!  1) destroy any composites
                 i = 0
                 do while (associated(thenode%m_pdofo ) .and. i < ubound(thenode%m_pdofo  ,1))
                     i=i+1
                     d2=>thenode%m_pdofo(i)%d
                     if(d2%m_dofotype ==C_DOFO_COMPOSITE) then
                          ds =d2%m_dofosli ; dn = d2%m_dofoN;
                          err=DestroyDofo ( ds ,dn)
                         i= 0
                     endif
                 enddo
! 2) treat the rest

                if (associated(thenode%m_pdofo )) then
                do i=1, ubound(thenode%m_pdofo  ,1)
                if (BTEST(thenode%m_pdofo(i)%m_dflg,DOFO_FLAG_SLAVE ) )  then
                write(outputUnit,*) ' skip slave'
                cycle
                endif
                    d2=>thenode%m_pdofo(i)%d
                    select case (d2%m_dofotype)
                          case (C_DOFO_NODENODE_CONNECT,C_DOFO_NODESPLINE_CONNECT,C_DOFO_STRINGSPLINE_CONNECT)
                                write(*,*) ' (FillFNode) But we have just unresolved ! '
                          case ( C_dofo_FixedPoint, C_dofo_SlideFixedLine , C_dofo_SlideFixedPlane )
                          !    write(*,"(a ,3F13.4,' > ',3f13.4)") ' (FillFNode) setting origin on Fixed ! ', d2%origin, newXX
                              d2%origin = newXX
                              d2%t=0;
                              if(associated(d2%child)) then
                                     write(outputUnit,*) "OOPS   we changed a NNC which still has a child"
                              endif
                         case (  C_DOFO_FIXED_GLE ,  C_DOFO_SlideFixedCurve , C_dofo_SlideFixedSurface,  C_DOFO_FREE_GLE )
                                     write(outputUnit,*)'(FillFNode) dofo type ', trim(d2%m_DofoLine), ' not coded'
                         case (  C_DOFO_RIGIDBODY)
                                    write(outputUnit,*)'(FillFNode) TODO flag C_DOFO_RIGIDBODY for re-building'
                         case (  C_DOFO_UNDEFINED ,  C_DOFO_COMPOSITE  )
                                    write(outputUnit,*)'(FillFNode)WRONG dofo type ', trim(d2%m_DofoLine), ' shouldnt be here'
                        case default
                                    write(outputUnit,*)'(FillFNode)WRONG dofo type ', trim(d2%m_DofoLine), ' shouldnt be here'
                    end select
                enddo
                endif
                endif
            endif iudc
        endif ch
        flush(outputUnit)
        thenode%XXX= newXX
    csurf=0; slidecurve=0
ifsnode:if (nodes%xlist(NN)%m_RXFEsiteptr .ne.0 .and. IsSixNode(nodes%xlist(NN)) >=0) then    !IsSixNode return 0 means NO. +ve means its a XYZ rc -ve means its a RotNode 
            csurf = fc_get_contact_surface(nodes%xlist(NN)%m_RXFEsiteptr) 
            call set_csurf_ptr(nn,sli,csurf)
            slidecurve= fc_get_slidecurve(nodes%xlist(NN)%m_RXFEsiteptr)           
            if(slidecurve.ne.0.and. thenode%m_slider .eq. 0) then ! the 2nd is a guess nov 2010
                call set_slider_ptr(nn,sli,slidecurve)
            endif
 	    endif ifsnode
!$OMP END CRITICAL (nodalxlist)
end subroutine FillFNode

function SetNodalFixing( sli,nn,buffin,L) result(kk)  bind(C,name=  "cf_setnodalfixing")
! return value is the number of the dofo in sli.
    USE,INTRINSIC :: ISO_C_BINDING  
    USE cfromf_f
    USE saillist_f
    use coordinates_f
    use dofoprototypes_f ! for DOFO_FROM_FIXINGSTRING]
    use parse_f
    IMPLICIT NONE
    INTEGER(C_INT) , intent(in) ,value :: sli,nn,L
    character(len=1),dimension(L),intent(in) ::buffin
    character(len=L) ::word,buf
    INTEGER  :: err
    TYPE ( nodelist) ,POINTER :: nodes
    integer ::kk
    buf = TRANSFER(buffin,buf)
    call denull(buf)  
  
	nodes=>saillist(sli)%nodes
	err=8
	if(NN < 1) return
	if(.not. associated(nodes%xlist  )) return;
	err=4
	if( lbound(nodes%xlist,1) > NN) return
	err = 0;    kk=0;
    word=nextfixingword(buf) 
       do while(len_trim(word)>0)
            if(kk>0) write(outputunit,*)'NOT SURE if we can have multiple fixities on a single node'
            kk= dofo_from_fixingstring(sli,nodes%xlist(NN),trim( word)) ! the dofo is kk in sli
            word=nextfixingword(buf)      	      
      enddo
end function SetNodalFixing

recursive SUBROUTINE RemoveFortranNode (sn, NN,err)
USE realloc_f
use cfromf_f  ! for zeroptr
IMPLICIT NONE
	INTEGER , intent(in) :: sn,nn
	INTEGER , intent(out) :: err
	TYPE ( nodelist) ,POINTER :: nodes
	integer ::dumm
	err = 16
	! write(outputunit,*) 'RemoveFortranNode', sn, NN,err
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return
	nodes=>saillist(sn)%nodes
!
!   What it does.
!	unsets the USED flag. realloc if  it can
!
! USUAL Conditions

!   NN =  Ncount  >> decrement Ncount
!    if then Ncount < ubound-block, re-allocate
!

! Unusual conditions
!   sailno out of range  >> err 16
!   nodelist not associated  >>err 8
! NN < lbound or > Ubound  >>err 8
!
	err=8
	if(.not. ASSOCIATED(nodes%Xlist)) return
	if(NN > UBOUND(nodes%Xlist,1)) return
	if(NN  <LBOUND(nodes%Xlist,1)) return
	err=0
	
!!!!$OMP CRITICAL (nodalxlist)    
        dumm = IsSixNode(nodes%Xlist(NN)) ! rc=0 means NO. RC + means its a XYZ rc - means its a RotNode
    if( dumm >0 ) then
        if( dumm .lt.ubound(nodes%Xlist ,1)) then
             nodes%Xlist(dumm)%sixnoderef=0
             call RemoveFortranNode (sn, dumm,err)
        endif
    else if (dumm<0) then  ! NN is a rotnode, uncouple its transnode
       if(-dumm <=ubound(nodes%xlist,1)) then
           nodes%Xlist(-dumm)%sixnoderef=0
         else
        write(*,*) ' caught bounds check ',-dumm
     endif

    endif  	
	if(nodes%Xlist(NN)%m_RXFEsiteptr /=0)dumm= fc_unhookfromfea(nodes%Xlist(NN)%m_RXFEsiteptr) 
	nodes%Xlist(NN)%m_RXFEsiteptr=0
	nodes%Xlist(NN)%m_Nused = .FALSE.
#ifndef NOOMP
        call OMP_DESTROY_LOCK(nodes%xlist(NN)%m_lock)
#endif


ifn:   if(NN == nodes%Ncount) THEN
! ncount should be the last index nodes%Xlist which is m_used;
dw:        do while( (.not. nodes%Xlist(nodes%Ncount)%m_Nused))
 		    nodes%Ncount =nodes%Ncount-1  
 		    if( nodes%Ncount .le. 0) exit dw   
        enddo dw
		if( nodes%Ncount+nodes%block < ubound(nodes%Xlist,1) ) THEN ! write(outputunit,*) ' reallocate nodes downwards to ',  ubound(nodes%Xlist,1) - nodes%block
			CALL reallocate(nodes%Xlist, ubound(nodes%Xlist,1) - nodes%block, err)
			g_PleaseHookElements=.true.
		ENDIF
	ENDIF ifn

!!!!$OMP END CRITICAL (nodalxlist)    
END SUBROUTINE RemoveFortranNode


SUBROUTINE SetNodeUsed   (sn, NN,flag,err)
IMPLICIT NONE
	INTEGER , intent(in) :: sn,nn,flag
	INTEGER , intent(out) :: err
	TYPE ( nodelist) ,POINTER :: nodes
!
! sets the Used flag depending on the value of 'flag'
!  the usual error returns. 
!  the only twist is that if this is the last node, we decrement Ncount if flag=0
!  similarly, we increment Ncount if NN is big. But only up to 
!
!	write(outputunit,*) 'SetNodeUsed   ', sn, NN,flag
	err=16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	err=8
	nodes=>saillist(sn)%nodes
	if(.not. ASSOCIATED(nodes%Xlist)) return
	if(NN > UBOUND(nodes%Xlist,1)) return
	if(NN  <LBOUND(nodes%Xlist,1)) return
	err=0
	if(flag/=0) THEN
		if(nodes%Xlist(NN)%m_Nused) err=2
		nodes%Xlist(NN)%m_Nused=.TRUE.
		if(NN > nodes%Ncount) nodes%Ncount=NN 
	else
		if(.NOT. nodes%Xlist(NN)%m_Nused) err=1
		nodes%Xlist(NN)%m_Nused=.FALSE.
		write(outputunit,*) " setting used OFF on node ", NN
		if(NN ==  nodes%Ncount) nodes%Ncount=nodes%Ncount-1   !  looks wrong
	endif

END SUBROUTINE SetNodeUsed 

SUBROUTINE  PrintFNodes(sn,unit) 
USE ftypes_f
USE saillist_f
USE edgelist_f
use dofoprototypes_f
IMPLICIT NONE
	INTEGER , intent(in) :: sn,unit

	TYPE ( nodelist) ,POINTER :: nodes
	TYPE ( Fnode ),pointer ::e
	INTEGER :: i 
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return


	nodes=>saillist(sn)%nodes
	if(.not. ASSOCIATED(nodes%xlist)) return

	write(unit,*) ' F NodeList for model ', trim(saillist(sn)%sailname)
	
10	format(2i4, 1x,L4,1x ,3g13.5,2(2x,3G11.4),2(1x,L4))
	do i =1,  UBOUND(nodes%xlist,1)
	    e=>nodes%xlist(i)
	    if(.not. e%m_Nused) cycle
    
 	    write(unit,10) i,  e%Nn, e%m_Nused,  &
                e%XXX,e%V,e%R, &
!		e%m_Nodal_Load , &
        isunderdofocontrol(e),e%m_trace
	ENDDO
	write(unit,*) ' count is',nodes%ncount

END  SUBROUTINE  PrintFNodes 

!extern "C" int cf_SetToLastKnownGood(const int sn);
function  RestoreLastKnownGoodCoords(sn) result(ok) bind(C,name=  "cf_SetToLastKnownGood")  ! exported via cfrom_f
    USE,INTRINSIC :: ISO_C_BINDING  
    USE saillist_f
     IMPLICIT NONE
!args
    INTEGER(C_INT) , intent(in) ,value :: sn
!locals
	TYPE ( nodelist) ,POINTER :: nodes
	TYPE ( Fnode ),pointer ::e
	INTEGER(C_INT) :: i ,ok
	real(C_DOUBLE) , dimension(3) ::x
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return


	nodes=>saillist(sn)%nodes
	if(.not. ASSOCIATED(nodes%xlist)) return

	do i =1,  UBOUND(nodes%xlist,1)
	    e=>nodes%xlist(i)
	    if(.not. e%m_Nused) cycle
	if(e%m_RXFEsiteptr .eq. 0) cycle
        ok = fc_getlastknowngood (e%m_RXFEsiteptr,x)
        e%XXX=x
	ENDDO
	ok=1
END  function  RestoreLastKnownGoodCoords 

function IsSixNode(nn) result (rc)  ! rc=0 means NO. RC + means its a XYZ rc - means its a RotNode
    type(fnode), intent(in) ::nn
    integer :: rc
    rc = NN%sixnoderef
end function IsSixNode

END MODULE nodeList_f


SUBROUTINE removeAllFNodes(sn,err)
use ftypes_f
use saillist_f
use nodelist_f

IMPLICIT NONE
	INTEGER , intent(in) :: sn
	INTEGER , intent(out) :: err

	integer i,SomeErrors
	TYPE ( nodelist) ,POINTER :: L
	err = 16
	SomeErrors=0
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	L=>saillist(sn)%nodes
	do i = L%Ncount,1,-1
		call RemoveFortrannode (sn, i,err) 
		if(err /=0) SomeErrors = SomeErrors+1
	enddo
		if(associated(L%xlist)) deallocate(L%xlist,stat=err)
	if(SomeErrors /=0) then
		write(outputunit,*) 'model',sn,' removeFortranNode (from remove ALL) Nerr=',SomeErrors
	endif
END SUBROUTINE removeAllFNodes


