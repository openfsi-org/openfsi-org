!Cmarch 1997 LT instead of < for HP
!C 
  
    function Make_Nodal_Values(sli,P,H,Ncolors,p_nsds,cmin,cmax) result(retval)  bind(C,name= "cf_make_nodal_values"  )
    use fglobals_f
    use saillist_f
    IMPLICIT NONE
      
!*     takes the list P of element values and returns a list H
!*!     of nodal values, adjusted to fit in the range (0 to nsds-1)
!*     
!*     So H is ready for passing to Hoops
!*     
!*     Now map mean  onto  cmid and (+-Extent)*SD onto cmin,cmax
      
	integer(c_int), intent(in), value :: sli
	REAL(c_float), dimension(*), intent(inout) :: P   ! values on elements
	INTEGER(c_int), intent(in) :: ncolors	! entries in color map 
	REAL(c_float), intent(inout) :: p_nsds		   ! no of SDs to map to the half-range
!
!  returns
      REAL(c_float), dimension(*), intent(out) ::H
      REAL(c_float),  intent(out) ::cmax,cmin 
      

! locals
      type(tri) , pointer ::this
      real(kind=8), dimension(:), allocatable :: NodalArea ,ZP
      REAL*8   ZNN,SD, ZMEAN, nsds 
  	REAL(c_float) :: HN,HM
      INTEGER I,K,N,  n2,  t2, Nod,retval,ip
      REAL*4 halfrange,cmid
        nsds = p_nsds;  retval=1; !  n1 = 1
 	!write(outputunit,*) ' IN Make_Nodal_Values sli,ncolors,nsds ',sli,ncolors,p_nsds
       n2 = saillist(sli)%nodes%ncount
       t2 = saillist(sli)%tris%count
  
      if(t2 .LT. 1.or. n2 .lt. 1) then
	   retval=0
	  return
       endif
	nod = n2 
	 allocate(NodalArea(n2) )
	 allocate(zp(n2))
 
        H(:n2)=0.0; !write(outputunit,*) ' zeroed H OK'
        NodalArea=0.00
        ZP =0.0
	ip=1
      DO I=1,T2
        this=>saillist(sli)%tris%list(i)
         if(this%use_tri.and. this%used) then
              DO K=1,3
                N=this%N13(K) 
		        if(n.lt.lbound(zp,1) .or.n.gt. ubound(zp,1)) then
		             write(outputunit,*) 'Make_Nodal_Values:: n out of bounds= ',n
		             cycle
		         endif
                ZP(N)=ZP(N)+1.
                NodalArea(n)=NodalArea(n)+this%m_Area/3.000
                H(N)=H(N)+P(Ip) ! could be miss-indexed here.
              ENDDO
		Ip=Ip+1
      endif
      ENDDO
	! here H and zp are nodal values in fortran indexing 
      DO I=1, nod
      IF(ZP(I).NE.0.0) H(I)=H(I)/ZP(I)
      ENDDO


      HN=H(1)
      HM=H(1)
      ZNN=0.
      SD=0.0
      ZMEAN=0.
      DO  I=1,nod
      HM=MAX(HM,H(I))
      HN=MIN(HN,H(I))
      ZNN=ZNN + NodalArea(i)
      ZMEAN=ZMEAN+H(I) * NodalArea(i)
      ENDDO

      ZMEAN=ZMEAN/ZNN

      DO I=1,nod
      SD=SD+(H(I)-ZMEAN)**2 *NodalArea(i)
      ENDDO

      SD=SQRT ( SD/ZNN )

      if(nsds .gt. 0.0 ) then
!         write(outputunit,*) 'zmean = ',zmean
      else

         ZMEAN = (cmax + cmin)/2.0
         nsds = 1.0
         SD = cmax - ZMEAN 
!      write(outputunit,*) 'zmean = ',zmean

      endif
         cmin  = 0.0
	 cmax = FLOAT(ncolors-1) 
	 cmid = (cmax+cmin )/2.0
	 halfrange = cmid

            
!  Now map mean  onto  cmid and (+-Extent)*SD onto cmin,cmax
	  if(sd .lt. 1e-8) then
	  	DO I=1,nod
	  		H(I) = ZMEAN
			if(H(I) .lt. cmin ) H(I) = cmin
			if(H(I) .Gt. cmax ) H(I) = cmax
	  	ENDDO
	  ELSE

	  DO I=1,nod
	  	H(I) = (H(I)-ZMEAN) *halfrange / (SD * nsds) + cmid
		if(H(I) .lt. cmin ) H(I) = cmin
		if(H(I) .Gt. cmax ) H(I) = cmax
	  ENDDO
	  ENDIF
	  cmin = zmean -  SD*nsds
	  cmax = zmean  + SD*nsds
	  deallocate (NodalArea,zp)

      END  function Make_Nodal_Values


