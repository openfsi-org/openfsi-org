	MODULE nograph_f

	contains

subroutine drawonreset(kit,tcon,p_alfa,p_admas,n,X,p_dtcpu,p_dtsec,ekt)
    use basictypes_f
    use cfromf_f
    IMPLICIT NONE

        REAL (kind= double), INTENT(in)  :: tcon,p_alfa,p_admas,p_dtcpu,ekt
        real(kind=4), intent(in) :: p_dtsec
        REAL (kind= double), INTENT(in), dimension(:) :: x
        INTEGER , INTENT(in) :: kit,n
        character*64 valtxt
        integer::k
        write(valtxt,'(G16.8,A1)') tcon,char(0)
        k= Post_Summary_By_SLI(0, "residual"//char(0),valtxt//char(0))
        write(outputunit,10,err=99) kit,tcon,p_alfa,p_admas,n,x, p_dtsec/p_dtcpu , real(kit)/p_dtsec
99  return
10     format(i6,1x,g10.3,1X,f8.2,1x,f5.2,1x,i6,1x,'(',3(1x,G10.3),')',1x,f7.3,1x,f9.2)
end subroutine drawonreset

end MODULE nograph_f
