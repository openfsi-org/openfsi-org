!   date: Saturday  8/1/1994
!   translated from LAHEY FFTOSTD output
!
!   date: Wednesday  18/8/1993
!   translated to ABSOFT F90 free format
module invert_f
  use basictypes_f
  contains
  
function Minverse_e(a,error) result (b) ! uses getrf/getri for n>3

	implicit none
	real(kind=double),intent(in), dimension(:,:) :: a
	logical, intent(out),  optional :: error
	integer  :: n 
	real(kind=double), dimension(ubound(a,1),ubound(a,1)) :: b
	logical l_err

        l_err = .false.
        n = ubound(a,1)
	if(n .eq.3) then
            call INVERT3 (a,b,l_err)
	else
            call invert (n,a,b,n,l_err)
	endif
	if(present(error) ) error = l_err
end function minverse_E


function minverse(C) result(b)
    implicit none
    real(kind=double), intent(in), dimension(:,:) :: C
!    integer ::nn
     real(kind=double), dimension(ubound(c,1),ubound(c,2)) :: b  
     logical lerror
     b = minverse_e(c,lerror)
     return
 !    nn=ubound(c,1)
 !    call invert(nn,c,b,nn,lerror)
  end function minverse

 pure SUBROUTINE INVERT (N,C,B,ISIZE,error)
      use vectors_f
!          USE mkl95_LAPACK, ONLY: getrf,getri
           USE LAPACK95, ONLY: getrf,getri
      IMPLICIT NONE
!
!  FINDS THE INVERSE OF C AND CALLS IT B
!
!
      INTEGER,intent(in) :: N   ! matrix size
      INTEGER,intent(in) :: ISIZE  ! dimensioning

      real(kind=double), intent(in), dimension(isize,isize) ::  C
!returns
      real(kind=double), intent(out), dimension(isize,isize) :: B
      LOGICAL,intent(out) :: error
!locals
    integer, allocatable, dimension( :) :: ipiv
      INTEGER info
      error = .true.
      if((N .eq. 3) .and. (isize .eq. 3)) THEN
           CALL invert3(C,B,error) ! in vectors_f
           return
      ENDIF
      if((N .eq. 2) .and. (isize .eq. 3)) THEN
           CALL invert2(N,C,B,ISIZE,error)
           return
      ENDIF
      if (isize>0) then
        if(n .gt. isize) then
            info = 0; info = 1/info ! provoke a crash
        endif
              b(:n,:n) = c(:n,:n)
              allocate(ipiv(isize))
              call getrf( b(:n,:n)  ,   ipiv=ipiv,info=info )
              if(info .ne.0) then
                  b=0
              else
                  call getri(b(:n,:n) ,   ipiv=ipiv,info=info )
                  if(info .ne.0) b=0
              endif
              error=(info.ne.0)
              deallocate(ipiv)
      endif

end  SUBROUTINE INVERT

pure FUNCTION Determinant(a) result(sum)
      implicit none
      REAL(kind=double), intent(in), dimension(3,3) :: a
      REAL*8 Sum

      Sum = a(1,1)* (a(2,2)*a(3,3)-a(3,2)*a(2,3))
      sum = sum - a(1,2) * (a(2,1)*a(3,3) - a(3,1)*a(2,3))
      sum = sum + a(1,3) * (a(2,1)*a(3,2) - a(3,1)*a(2,2))

END FUNCTION Determinant ! 3 only






 pure SUBROUTINE INVERT_SSD (NN,C,B,ISIZE,error)
      use vectors_f
      IMPLICIT NONE
!
!  THIS SUBROUTINE FINDS THE INVERSE OF C AND CALLS IT B
!
!
      INTEGER,intent(in) :: NN   ! matrix size
      INTEGER,intent(in) :: ISIZE  ! dimensioning

      real(kind=double), intent(in), dimension(isize,isize) ::  C
!returns
      real(kind=double), intent(out), dimension(isize,isize) :: B
      LOGICAL,intent(out) :: error
!locals
      INTEGER LOCDIM
       PARAMETER(LOCDIM=128)
      REAL*8 A(LOCDIM,LOCDIM)
      INTEGER I,J, IPIVOT,IROW, ICOLUMN
      REAL*8 FAC

      if((NN .eq. 3) .and. (isize .eq. 3)) THEN
       CALL invert3(C,B,error) ! in vectors_f
       return
      ENDIF
      if((NN .eq. 2) .and. (isize .eq. 3)) THEN
       CALL invert2(NN,C,B,ISIZE,error)
       return
      ENDIF

      error= .FALSE.
      IF ((NN .GT.ISIZE).OR.(NN.GT.LOCDIM)) THEN
   !   write(outputunit,*)' Dimensions exceeded in INVERT'
   !   write(outputunit,*)' Array size called was ',NN
   !   write(outputunit,*)' Limit is              ',MIN(ISIZE,LOCDIM)
!      pause
      error = .TRUE.
      RETURN
      ENDIF
      DO I=1,NN
      DO J=1,NN
      A(I,J)=C(I,J)
      B(I,J)=0.0
      ENDDO
      B(I,I)=1.0
      ENDDO

      DO IPIVOT=1,NN-1
      DO IROW=IPIVOT+1,NN
      IF (A(IPIVOT,IPIVOT) .EQ. 0.0) GOTO 5000
      FAC=A(IROW,IPIVOT)/A(IPIVOT,IPIVOT)
      DO ICOLUMN=1,NN
      B(IROW,ICOLUMN)=B(IROW,ICOLUMN)-FAC*B(IPIVOT,ICOLUMN)
      A(IROW,ICOLUMN)=A(IROW,ICOLUMN)-FAC*A(IPIVOT,ICOLUMN)
      ENDDO
      ENDDO
      ENDDO

      DO IPIVOT=NN,2,-1
      DO IROW=IPIVOT-1,1,-1
      IF (A(IPIVOT,IPIVOT) .EQ. 0.0) GOTO 5000

      FAC=A(IROW,IPIVOT)/A(IPIVOT,IPIVOT)
      DO ICOLUMN=1,NN
      B(IROW,ICOLUMN)=B(IROW,ICOLUMN)-FAC*B(IPIVOT,ICOLUMN)
      A(IROW,ICOLUMN)=A(IROW,ICOLUMN)-FAC*A(IPIVOT,ICOLUMN)
      ENDDO
      ENDDO
      ENDDO

      DO IROW=1,NN
      FAC=A(IROW,IROW)
      DO ICOLUMN=1,NN
      B(IROW,ICOLUMN)=B(IROW,ICOLUMN)/FAC
      ENDDO
      ENDDO

      RETURN
5000   CONTINUE
      DO I=1,NN
      DO J=1,NN
      B(I,J)=0.0
      ENDDO
      ENDDO
      error=.TRUE.
      END SUBROUTINE INVERT_SSD


pure SUBROUTINE INVERT3 (C,B,error)
      IMPLICIT NONE
!
!  THIS SUBROUTINE FINDS THE INVERSE OF C AND CALLS IT B
!
!  By transpose of matrix of cofactors times determinant
!
      REAL(kind=double),intent(in),dimension(3,3) :: C
!returns
      REAL(kind=double),intent(out),dimension(3,3) :: B
      LOGICAL,intent(out) :: error

!     LOCALS
      REAL*8 det, cof(3,3) 

       det = determinant(C)
      error = .FALSE.
      if(abs(det) .lt. 1E-13) THEN
!        write(outputunit,*) ' zero determinant'
        B=0.0d00
        error = .TRUE.
        RETURN
      ENDIF

      cof(1,1) = c(2,2)*c(3,3) - c(2,3)*c(3,2)
      cof(1,2) = c(3,1)*c(2,3) - c(2,1)*c(3,3)
      cof(1,3) = c(2,1)*c(3,2) - c(3,1)*c(2,2)

      cof(2,1) = c(3,2)*c(1,3) - c(1,2)*c(3,3)
      cof(2,2) = c(1,1)*c(3,3) - c(3,1)*c(1,3)
      cof(2,3) = c(3,1)*c(1,2) - c(1,1)*c(3,2)

      cof(3,1) = c(1,2)*c(2,3) - c(2,2)*c(1,3)
      cof(3,2) = c(2,1)*c(1,3) - c(1,1)*c(2,3)
      cof(3,3) = c(1,1)*c(2,2) - c(2,1)*c(1,2)

		b = transpose(cof)/det

END SUBROUTINE INVERT3 


pure SUBROUTINE INVERT2 (NN,C,B,ISIZE,error)
      IMPLICIT NONE
!
!  THIS SUBROUTINE FINDS THE INVERSE OF C AND CALLS IT B
!
!  By transpose of matrix of cofactors times determinant
!
      INTEGER,intent(in) ::  NN   ! matrix size
      INTEGER,intent(in) ::  ISIZE  ! dimensioning

       real(kind=double), intent(in), dimension(isize,isize) ::  C
!returns
      real(kind=double), intent(out), dimension(isize,isize) :: B
      LOGICAL,intent(out) :: error     
      

!     LOCALS
      REAL*8 det, cof(3,3)
      INTEGER I,J

      if((NN .ne. 2) .OR. (isize .ne. 3)) I=1/0


      det = C(1,1)*c(2,2) - c(1,2)*c(2,1)
      error =  .FALSE.
      if(abs(det) .lt. 1E-13) THEN
  !      write(outputunit,*) ' zero determinant'
        error = .TRUE.
        RETURN
      ENDIF

      cof(1,1) =    c(2,2)
      cof(1,2) =  - c(2,1)
      cof(2,1) =  - c(1,2)
      cof(2,2) =    c(1,1)
      
      DO I=1,2
      DO J=1,2
       B(i,j) = cof(J,I) /det
      ENDDO
      ENDDO
      END SUBROUTINE INVERT2

end module invert_f
