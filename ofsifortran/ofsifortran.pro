#-------------------------------------------------
#
# Project created by QtCreator 2011-12-09T21:16:38
#
#-------------------------------------------------

QT       -= core gui

TARGET = ofsifortran
TEMPLATE = lib
CONFIG += staticlib
CONFIG += f90
CONFIG += F90

include (../openfsicompilerconfig.pri)

win32-g++: include  ($$[QMAKE_MKSPECS]/features/f90.prf)
else:win32: include  ($$[QMAKE_MKSPECS]/features/f90.prf)

#MODU#LE_DIR = ./mod

F90 = ifort
F90_CFLAGS += -DDOFO_CONNECTS  -DNO_DOFOCLASS -auto -assume noold_unit_star #-DDOFO_CHECKING # -DDOFO_COMBO_CHECKING -DNO_DOFOUPDATEORIGIN
F90_CFLAGS +=  -standard-semantics #includes -fpscomp[:]logicals - needed for interop

PETERDEV: F90_CFLAGS += -DPETERDEV
unix:!symbian {
CONFIG(debug,debug|release): {
        F90_CFLAGS+= -DNOOMP -D_DEBUG
        message (" flib debug build")
}
}
## NOTE -DNO_DOFOUPDATEORIGIN  is WRONG for spline dofos

###F90_CFLAGS += -gen-dep=mydeps.txt
win32-g++ {
  release:F90_CFLAGS += /fpp /recursive /free -fPIC
  debug:F90_CFLAGS   += /fpp /recursive /free -fPIC

}
else {
    win32 {
      release:F90_CFLAGS += /O3 /recursive /fpp /free
      debug:F90_CFLAGS   += /Od /debug:full /traceback /recursive /fpp /free
    }
    unix {
    F90_CFLAGS += -recursive -fpp -xHost -fPIC   # -heap-arrays 10 # -traceback
    rx64:     F90_CFLAGS += -m64
    rx32:     F90_CFLAGS += -m32
    }
}
release: {
    F90_CFLAGS +=  -DRX_PURE=    #pure
}
else {
    F90_CFLAGS +=  -DRX_PURE=
    F90_CFLAGS +=  -warn unused -ftrapuv
    F90_CFLAGS +=  -check all,arg_temp_created -traceback -openmp-stubs -fpe0 -check bounds -ftrapuv
}
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}

F90_SOURCES =   basictypes.f90 \
    bar_elements.f90 \
    cfromf_declarations.f90 \
    dofo_subtypes.f90 \
    linklist.f90 \
    ftypes.f90 \
    dofoprototypes.f90 \
    fglobals.f90 \
    gletypes.f90 \
    parse.f90 \
    rlxflags.f90 \
    vectors.f90 \
    realloc.f90 \
    connectstuf.f90 \
    saillist.f90 \
    coordinates.f90 \
    batstuf.f90 \
    mathconstants.f90 \
    math.f90 \
    invert.f90 \
      links_pure.f90 \
    batn_els.f90 \
    batten.f90 \
    edgelist.f90 \
    kbat_els.f90 \
    kbatten.f90 \
    stiffness.f90 \
    battenf90.f90 \
    beamelementtype.f90 \
     nodalrotations.f90 \
    rxtrigeomstiff.f90 \
    tanpure.f90 \
    beamElementMethods.f90 \
    hoopsinterface.f90 \
    nodelist.f90 \
    beamelementlist.f90 \
    bspline.f90 \
    dofoarithmetic.f90 \
    removeelement.f90 \
    fnoderef.f90 \
    fspline.f90 \
    fsplinecubic.f90 \
    stringsort.f90 \
    fstrings.f90 \
    rxfoperators.f90 \
    stringlist.f90 \
    connects.f90 \
    doforigidbody.f90 \
    dofo_io.f90 \
    dofolist.f90 \
    dofo.f90 \
    flinklist.f90 \
    gle.f90 \
    wrinkleFromMathematica.f90 \
    nonlin.f90 \
    tris.f90 \
    feaprint.f90 \
    nastraninterface.f90 \
    plotps.f90 \
    trilist.f90 \
    flagset.f90 \
    flopen.f90 \
    Fslidecurve.f90 \
    gravity.f90 \
    isnan.f90 \
    msvc_fortran_utils.f90 \
    mtkaElement.f90 \
    nlstuf.f90 \
    nograph.f90 \
    oonatristuf.f90 \
    sf_tris_pure.f90 \
    ttmass9.f90 \
    rximplicitsolution.f90 \
    f90_to_c.f90 \
    relaxsq.f90 \
    shapegen.f90 \
    relax32.f90 \
    shape.f90 \
    solvecubic.f90 \
    solve.f90 \
    spline.f90 \
    sspline.f90 \
    tohoops.f90 \
    tris_pure.f90 \
    angcheck.f90 \
    reset.f90 \
    ../thirdparty/sparskit/FORMATS/formats.f  \
    ../thirdparty/sparskit/BLASSM/blassm.f  \
    ../thirdparty/sparskit/FORMATS/unary.f

PETERDEV:{
 F90_SOURCES+= dofodev.f90
 message(" inc dofodev")
 }

OTHER_FILES += $$F90_SOURCES \
    $$[QMAKE_MKSPECS]/features/f90.prf \
    ../f90.prf

HEADERS +=

################ PGO details - switched on/off in our .pri file
profgen: {

#  for PGO either -prof-gen or -prof-use
CONFIG(release,debug|release):          QMAKE_CFLAGS+=  -prof-gen  -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          QMAKE_CXXFLAGS+=  -prof-gen  -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          F90_CFLAGS += -prof-gen  -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          QMAKE_LFLAGS +=  -prof-gen  -prof-dir/home/r3/Documents/qt/openFSI

}
profuse:{
#  -prof-file ../myprof
CONFIG(release,debug|release):          QMAKE_CFLAGS+=  -prof-use -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          QMAKE_CXXFLAGS+=  -prof-use -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          F90_CFLAGS += -prof-use  -prof-dir/home/r3/Documents/qt/openFSI
CONFIG(release,debug|release):          QMAKE_LFLAGS += -prof-use  -prof-dir/home/r3/Documents/qt/openFSI
}









