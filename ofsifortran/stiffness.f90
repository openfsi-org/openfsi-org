module stiffness_f
use basictypes_f

implicit none

contains

subroutine AddStiffness(s,nn,d,m)
use saillist_f
use cfromf_f
    implicit none
    type(sail), pointer  ::s 
    integer, intent(in), dimension(:):: nn
    real(kind=double), intent(in), dimension(:,:):: d
    integer(kind=cptrsize)  :: m
    type(fnode), pointer :: fn

! locals
    integer n1,n2,n3, r,c,i,j,k,L
    
    n1=ubound(nn,1); n2=ubound (d,1); n3=ubound(d,2)
   if(3*n1 .ne. n2 .or. n2 .ne. n3) then
       write(outputunit,*) ' bad addstiffness call'
   endif

do i=1,n1
    fn=>s%nodes%xlist(nn(i))
    r = fn%fnodeRowIndex
        do j=1,n1
            fn=>s%nodes%xlist(nn(j))
            c = fn%fnodeRowIndex
            do k=0,2
            do L=0,2
                call cf_AddToSparseMatrix(m,r+k,c+L, d(3*i-2+k, 3*j-2+L ) ) 
            enddo
            enddo
        enddo
enddo

end subroutine AddStiffness

end module stiffness_f
