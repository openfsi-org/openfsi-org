! this is tris_pure.f90, 
! pure functions based on stroona to get stiffness and residuals of tri elements
! with no globals  Only uses the 3 modules here at the top.
! If  function one_tri_global_stiffness_2006(p_GEOMATRIX, p_G,p_D,x1,x2,x3,p_zir,p_ps, p_area,p_tripressure) result(kgt) 
!  checks out OK in stroona, we should be able to use it directly in Relax.
!   one_tri_global_forces_2006 is slow because it calculates the edge vectors many times. but it ought also to be OK in Relax. 

!  it would be tidier to call one_tri_global_geo_stiffness_2006 (THEREFORE  one_tri_local_forces_2006)
! with a stress not with a stiffness matrix.


module sf_tris_pure_f
    use basictypes_f
    use vectors_f
    use mathconstants_f
contains
#ifdef NEVER
! this is slow because we first have to get the strain in order to get p_D so this gets done twice.

pure function one_tri_global_forces_2006(x1,x2,x3,p_g ,p_d,p_zir,p_ps,p_area,p_triPressure ) result(f_global)		 ! subtract from  r(*)

	implicit none
	real(kind=double), intent(in), dimension(3)	:: x1,x2,x3,p_zir,p_ps
	real(kind=double), intent(in), dimension(3,3)	:: p_g,p_d
	real(kind=double), intent(in) :: p_area,p_tripressure

   real*8 f_global(9) !{{x,y,z}@n1, {x,y,z}@n2, {x,y,z}@n3}

! locals
	real*8 stress(3), tedge(3), d(3), eps(3), loadvector(3), length
	integer edge,k, e1,e2
	logical error

	call sf_one_tri_local_forces_2006(x1,x2,x3,p_g ,p_d,p_zir,p_ps,p_area,p_triPressure,stress,eps, tedge,loadvector )

	if(any(isnan(tedge))) then;  tedge=0; endif
	f_global=0.0
	loadvector = loadvector/3.0d00  

   do edge=1,3

		select case (edge)
			case(1)
				d = x2 - x1 
			case(2)
				d = x3 - x2
			case(3)
				d = x1 - x3
		end select
		call normalise(d,length,error)

		e1 = edge-1
		e2 = edge
		if( e2 .gt.2) e2 = 0

		do k=1,3
			f_global(k+3*e1) = f_global(k+3*e1) - d(k)*tedge(edge)
			f_global(k+3*e2) = f_global(k+3*e2) + d(k)*tedge(edge)
			f_global(k+3*e1) = f_global(k+3*e1) + loadvector(k) 
		enddo
   enddo
end function one_tri_global_forces_2006
#endif
! should call with stress not p_d because 
pure function sf_one_tri_global_geo_stiffness_2006( x1,x2,x3,p_g,p_d,p_stress, p_zir, p_ps, p_area,p_tripressure) result(kg) ! obsolete but correct
	implicit none
!parameters
	real(kind=double), intent(in), dimension(3)	:: x1,x2,x3,p_zir,p_ps,p_stress
	real(kind=double), intent(in), dimension(3,3)	:: p_g,p_d
	real(kind=double), intent(in) :: p_area,p_tripressure
!returns
	real*8 kg(9,9)		! geometric stiffness matrix (global)

! locals
	integer j,k,e, e2
	real*8  d(3),  tedge(3), sloc(6,6)
	real*8  l_eps(3) !  l_stress(3),
	logical,parameter :: l_geomatrix = .true.
 !   real(kind=8), dimension(9) ::X    ! current   coords
 !   real(kind=8), dimension(6)  ::localforces  ! the corner forces in element local axes
    
	integer corner(0:1), b1,b2, l_r,l_c


	call  sf_one_tri_local_forces_2006(x1,x2,x3,p_g, p_d,p_zir,p_ps,p_area,p_tripressure,  p_stress,  l_eps, tedge)
	kg=0.

edo: do e=1,3
		select case (e)
			case (1)
			d = x2-x1
			case (2)
			d = x3-x2
			case (3)
			d = x1-x3
		end select 
		sloc= sf_one_triedge_global_stiffness_2006(d,tedge(e),0.0d00,1.0d00,l_geomatrix)

! last argument is zi. it may be anything because ea is zero

		e2 = e+1
		if (e2 .gt. 3) e2=e2-3

		corner(0) = e-1
		corner(1) = e2-1
		do b1=0,1
			do b2=0,1						! block 1, block 2
				do  j=1,3
					do  k=1,3
					 	l_r = j + 3* corner(b1)
					 	l_c = k + 3* corner(b2)
					 	kg(l_r,l_c) = kg(l_r,l_c) + sloc(3*b1+j,3*b2+k)
					enddo
				enddo
			enddo
		enddo
	  enddo edo
end function sf_one_tri_global_geo_stiffness_2006

! 
!	l_G=>g(1:3,1:3,n)
!	D => D_stf(1:3,1:3,n)
! triarea(n)
pure function sf_one_tri_global_stiffness_2006(p_GEOMATRIX, p_G,p_D,p_stress,x1,x2,x3,p_zir,p_ps, p_area,p_tripressure) result(kgt) ! this is the stroona validated one
	implicit none

! parameters
	logical, intent(in) :: p_geomatrix
	real(kind=double), intent(in), dimension(:,:) ::p_G,p_D
	real(kind=double), intent(in), dimension(3) :: x1,x2,x3,p_zir,p_ps,p_stress
	real(kind=double), intent(in) :: p_area, p_triPressure
!returns
	real(kind=double), dimension(9,9) ::kgt	! stiffness matrix in global coords
!
!locals
	real*8 kg(9,9) !  global geometric stiffness matrix
	real(kind=double), dimension(3,3) :: EdgeV
	real(kind=double), dimension(3,9) :: a
	integer k,e
	logical error
	real(kind=double)  L_ength

! 1) get the normalized edge vectors
! 2) construct A
! 3) K = A**T G**T D G A * area + geometric + area term. 

		EdgeV(1,1:3) =  x2-x1
		EdgeV(2,1:3) =  x3-x2
		EdgeV(3,1:3) =  x1-x3

	do e=1, 3 
		call  normalise(EdgeV(e,1:3),L_ength,error)
	enddo
	a=0.0
	do k=1,3
		a(k,3*k-2:3*k) = - EdgeV(k,1:3)
	enddo
	a(3,1:3) =  EdgeV(3,1:3)
	a(1,4:6) =  EdgeV(1,1:3)
	a(2,7:9) =  EdgeV(2,1:3)

	kgt = matmul( Transpose(A) ,matmul( Transpose(p_G) , matmul( p_D ,matmul( p_G ,A ) )) ) *p_Area


!diagonal is +ve The sign convention is the opp. to Kd
geo:	if (p_geomatrix) then
			kg= sf_one_tri_global_geo_stiffness_2006( x1,x2,x3,p_g,p_d,p_stress, p_zir, p_ps, p_area,p_tripressure)  ! in tension, diagonal is negative
		else geo
			kg=0
		endif geo

  ! if any Kg diags negative, zero it
  do k=1,9
    if(kg(k,k) <0) then
        kg=0;
        exit
    endif
  enddo

	kgt = kg + kgt
end function sf_one_tri_global_stiffness_2006

! originating from stroona, we pass IN the stress
pure subroutine sf_one_tri_local_forces_2006(x1,x2,x3,p_g ,p_dd,p_zir,p_ps,p_area,p_triPressure,stress,p_eps, tedge,loadvector )
	implicit none

	real(kind=double), intent(in), dimension(3)	:: x1,x2,x3,p_zir,p_ps,stress,p_eps
	real(kind=double), intent(in), dimension(3,3)	:: p_g,p_dd  ! P_d should be the wrinkled matrix
	real(kind=double), intent(in) :: p_area,p_tripressure
	real(kind=double), intent(out) ,dimension(3) ::  tedge    ! stress,eps,
	real(kind=double),optional, intent(out) ,dimension(3) ::  loadvector

	real(kind=double),dimension(3,3) ::  t  ,x

	real(kind=double)							:: curr_area
 
!	real*8   ldelta(3) 


        curr_area = tri_area(x1,x2,x3)

	x(1,1:3) = x1; x(2,1:3) = x2; x(3,1:3) = x3; ! not fast but easy

!
!	do e=1,3
!		ldelta(e) = 0.0d00
!		e1 = e+1; if(e1==4 ) e1=1;
!		do k=1,3
!			ldelta(e) = ldelta(e) + (x(e,k)-x(e1,k) )**2
!		enddo
!		ldelta(e) = dsqrt(ldelta(e)) - p_zir(e)
!	enddo

	!eps = matmul(p_g,delta)! + e0(1:3,n)

	!stress = matmul(p_dd,eps) + p_ps(1:3) ! SB get_wrinkled_Stress
	
	tedge = matmul( transpose(p_g), stress)  * p_Area
	if(present(loadvector)) then 
                t=transpose( scAxisTrigraph(x1,x2,x3,1))
		loadvector = t(3,1:3) * p_TriPressure * curr_area
	endif

end subroutine sf_one_tri_local_forces_2006

pure function sf_one_triedge_global_stiffness_2006(d,tension,ea, zi,p_geomatrix) result(s)

	implicit none
!	parameters
	real(kind=double), intent(in), dimension(3) :: d	! direction components unnor malised
	real(kind=double), intent(in) :: tension,ea,zi
	logical, intent(in) :: p_geomatrix
!returns

	real*8 s(6,6)
!locals

	integer e1,e2,la,j,k ,row,c
	real*8 dr(3,3),gm(3,3)
	real*8 ttt,zt, ztsq,zt3

    ztsq = Dot_Product(d,d)
    if(abs(ea).gt. 1e-10) then
	    ttt = ea / (ztsq * zi)   ! ea/l^3 was zt zt zi
	    gm = 0.0;
	    do  la=1,3
		    do  j=1,3
			    dr(la,j)=ttt*d(j)*d(la)     ! direct stiffness
		    enddo
	    enddo

	    do e1=0,1
		    do e2=0,1
			    do j=1,3
				    do k=1,3
					    row=3*e1+j
					    c=3*e2+k
					    s(row,c) = - (1.0d00-2.0d00*dble(kdel(e1,e2))) * dr(j,k)
				    enddo
			    enddo
		    enddo
	    enddo
    else  ! no EA
        s=0. 
    endif
	if(p_geomatrix  ) then
		zt = Sqrt(ztsq); zt3 = ztsq*zt
	    do  la=1,3
		    do  j=1,3
			    gm(la,j) =DBLE(kdel(la,j) ) - d(la) * d(j)
		    enddo
	    enddo	
	    gm  = gm * tension/zt3
	    s(1:3,1:3) = s(1:3,1:3) +gm
	    s(4:6,4:6) = s(4:6,4:6) +gm
	    s(1:3,4:6) = s(1:3,4:6) -gm
	    s(4:6,1:3) = s(4:6,1:3) -gm
	    
	endif	
	
	
	
end function sf_one_triedge_global_stiffness_2006


pure function sf_make_one_dstf(ex,ey,e45,nuxy) result (d)
implicit none
real(kind=double),intent(in) :: ex,ey,e45,nuxy

real(kind=double), dimension(3,3) :: d
real(kind=double)  :: facnu,nuyx

		if(ex .gt. 1d-12) then
			nuyx = nuxy *ey/ex
		else
			nuyx =0.0d00
		endif
		facnu=1.0-nuxy*nuyx
		if(ex .le. 0 .or. ey .le. 0 .or. e45 .le. 0) then
			d(3,3) = 0.0
		else
			d(3,3)=1.0/(4.0/e45-(1.0-nuxy)/ex-(1.0-nuyx)/ey)
		endif
		d(1,1)= ex/facnu
		d(2,1)= ey*nuxy/facnu
		d(2,2)= ey/facnu
		d(1,2)= d(2,1)
		d(1,3) = 0.0d00
		d(3,1) = 0.0d00
		d(2,3) = 0.0d00
		d(3,2) = 0.0d00
end function sf_make_one_dstf







end module sf_tris_pure_f
