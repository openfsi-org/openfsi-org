MODULE gle_f

!  1 dec 2003 added the write to 'toStroona.txt' for manual data transfer

USE gletypes_f
use coordinates_f

IMPLICIT NONE
! static declaration of the glelist. WRONG should be by model
	SAVE
	TYPE (GleList) :: GList
	integer, parameter :: GLE_SLI=1


CONTAINS

subroutine gle_Residuals()

	USE saillist_f
	IMPLICIT NONE
	integer :: i,j,n,ok
	REAL (kind= double), allocatable, dimension(:,:) :: Rlocal
	TYPE (gle)  ,POINTER :: this

iloop:	do i=1,GList%count
		this=>Glist%list(i)
		allocate(Rlocal(3,this%n))
		call  gle_OneLocalResiduals(Rlocal,this)
	jloop2:	do j=1,this%n
			n = this%nodenos(j)  ! increment_r(r,n,sli,base )
			ok = increment_R(n,Rlocal(1:3,j), GLE_SLI )
		enddo  jloop2
		deallocate(Rlocal)
	enddo iloop
end subroutine gle_Residuals

subroutine gle_af_name(name)  ! # to /
	IMPLICIT NONE
	character*(*) name
	integer :: i
	do i = 1, len(trim(name))
		if(name(i:i) .eq. '#' ) then
			name(i:i) ='/'
		endif
	enddo
	

end subroutine gle_af_name

subroutine gle_Export_Forces()
use cfromf_f
	IMPLICIT NONE
! updates OONA forces and disps 
! runs OONA
! asks OONA to export the substructure as an editword card. 
        integer :: i,j,n,err,k
	REAL (kind= double), allocatable, dimension(:,:) :: Rl
	TYPE (gle)  ,POINTER :: this
	real (kind=single) , dimension(3) ::r,d
	character*256 s
	character*32 na
	if(GList%count .gt. 1) then
		write(outputunit,*) ' ONLY ONE GLE'
		! pa use
	endif 	
		open(unit=401,file='toStroona.txt')
iloop:	do i=1,GList%count
		this=>Glist%list(i)
	!	if(this%blocked .or. .NOT. this%NeedToUpdate) return
		allocate(Rl(3,this%n))
		call  gle_OneLocalResiduals(Rl,this)
	jloop:	do j=1,this%n
			n = this%nodenos(j)
			r = -rl(1:3,j)
			d =( this%xlbuf(3*j-2 : 3*j ) -  this%X0(3*j-2 : 3*j ) )*1000.0  ! strOOna likes mm for displacements
!OONA likes mm for disps and N for forces
			na = trim(this%nodenames(j))
			call gle_af_name(na)

			write(s,'(a,a,3(a,f13.4),a)' )'data: ',trim(na),  ' : px=(',r(1), '*Force_Units),py=(',r(2), '*Force_Units),pz=(',r(3),'*Force_Units)'
!OONA doesnt like exponential notation
			k =min( len_trim(s) + 1 , 256)
			s(k:k) = char(0)  
                        !iret= pcc_send_as_client(s, err); write(401,*) trim(s)
			if(err .ne. 0) write(outputunit,*) ' COMMS ERROR '
	
			write(s,'(a,a,3(a,f13.4))' )'data: ',trim(na),  ' : dx=',d(1), ',dy=',d(2), ',dz=',d(3)

			k =min( len_trim(s) + 1 , 256)
			s(k:k) = char(0)  
		! 	call pcc_send_as_client(s, err) ; write(401,*) trim(s) ! this is a bad idea it puts the dx out of synch with the rotations
			!if(err .ne. 0) write(outputunit,*) ' COMMS ERROR '


	
		enddo  jloop
		deallocate(Rl)
	this%blocked  = .TRUE.
	enddo iloop
		close(401) 
		write(outputunit,*) ' forces written to toStroona.txt'
        !iret= pcc_send_as_client('run once: '//char(0),err)
        !iret= pcc_send_as_client('dont_substructure: substructure.txt: node: export : '//char(0),err)
	this%blocked  = .TRUE.
end  subroutine gle_Export_Forces

	subroutine gle_OneLocalResiduals(Rlocal,this)

	USE saillist_f
	IMPLICIT NONE
	REAL (kind= double), dimension(3,*) :: Rlocal
	TYPE (gle)  ,POINTER :: this

	REAL (kind= double), dimension(3) :: Rg 

	integer :: i,j,n,sli

    sli = this%sli
! 1 pack the X into Xlbuf, after transforming to local

iloop:	do i=1,GList%count
		this=>Glist%list(i)
	jloop1:	do j=1,this%n
			n = this%nodenos(j)
			this%xlbuf(3*j-2:3*j) = get_Cartesian_X(n,sli) 
	!		CALL  XtoLocal(saillist(this%sn)%IMT , X(n,1:3) , this%xlbuf(3*j-2:3*j)) 
		enddo  jloop1
	!	write(outputunit,*) ' xlbuf ',sngl(this%xlbuf)
		this%RLbuf = matmul(this%Kmatrix,(this%xlbuf - this%X0)) + this%F0	! Resids, local axes
		if(any((this%xlbuf - this%X0) .gt. this%tolerance) ) then
			!write(outputunit,*) 'need to send a new'
			this%NeedToUpdate = .true.
		else
			this%NeedToUpdate = .false.
		endif
	jloop2:	do j=1,this%n
			n = this%nodenos(j)
			Rg = this%RLbuf(3*j-2:3*j)
		!	CALL  VtoGlobal(saillist(this%sn)%MT ,this%RLbuf(3*j-2:3*j) ,Rg) 
			Rlocal(1:3,j) = Rg 
!			R(1:3,n) = R(1:3,n) + Rg 
		!	write(outputunit,*)' node ',n,' rg' , rg
		enddo  jloop2
	enddo iloop
end subroutine  gle_OneLocalResiduals

subroutine  gle_masses()
	USE saillist_f
	use fglobals_f
    use rx_control_flags_f
	IMPLICIT NONE
		
    REAL (kind= double), dimension(3,3) :: ml !, mg ! mass in local and global
	TYPE (gle)  ,POINTER :: this
	integer :: i,j,k,n,sli
	
	if ( g_Gle_By_Diagonal) then
iloop1:	do i=1,GList%count
		this=>Glist%list(i)

	jloop1:	do j=1,this%n
			n = this%nodenos(j)
			
			k=j 
			ml = this%Kmatrix(3*j-2:3*j, 3*k-2:3*k)
				! the transformation call is CALL  XtoLocal(this%sn , nodes%xlist(NN)%XX , xout) 
                        saillist(sli)%nodes%xlist(N)%XM(1:3,1:3) = saillist(sli)%nodes%xlist(N)%XM - ml
			! WRONG local coords and a heuristic mul;tiplier
		enddo jloop1
	enddo iloop1	
	
	else
iloop:	do i=1,GList%count
		this=>Glist%list(i)	;	sli = this%sli
	jloop:	do j=1,this%n
			n = this%nodenos(j)
	kloop:		do k=1,this%n
			ml = this%Kmatrix(3*j-2:3*j, 3*k-2:3*k)
				! the transformation call is CALL  XtoLocal(this%sn , nodes%xlist(NN)%XX , xout) 
			    saillist(sli)%nodes%xlist(N)%XM = saillist(sli)%nodes%xlist(N)%XM +abs(ml)  ! WRONG local coords
			enddo kloop
		enddo jloop
	enddo iloop
	endif
end subroutine gle_masses


subroutine printAllGle(u)
	use parse_f
	IMPLICIT NONE
	INTEGER,INTENT (in)   ::  u
	TYPE (gle)  ,POINTER :: this
	integer :: i,j
	CHARACTER (len=32)    ::  f

	do i=1,GList%count
	write(u,*)' GLE no ',i
	this=>Glist%list(i)
	write(u,'( a,a,a )')  ' Mname <', trim(this%mname),'> '
	write(u,'(a,  i6 )') '    n',  this%n
	write(u,*) ' cptr',  this%cptr
	write(u,*) ' sli',  this%sli
	write(u,'( a)')   ' nodenames'
	do j=1, this%n
		call denull(this%nodenames(j)) 
		write(u,'( a,a,a )')  'Nodename <',Trim(this%nodenames(j)),'>'
	enddo
	write(u,'( a)')   ' nodenos'
	write(u,'( i12 )')  this%nodenos
	write(u,*) ' F0'
	write(u,'(  3f14.4 )')  this%F0
	write(u,*) ' X0'
	write(u,'(  3f14.4 )')  this%X0
		write(u,*) ' T'
	write(u,'(  3f14.4 )')  this%T
		write(u,*) ' n0 n1 n2'
	write(u,'(  3(1h	i14 ))')  this%node0,this%node1,this%node2
	write(u,*) ' Diagonal '
	do j=1, this%n
		write(u,'( i5,5x,f14.5)')j,  this%Kmatrix(j,j)
	enddo

	write(f,*) '(',3*this%n,'F15.4)'
	write(u,*) ' format ', f
	write(u,*) ' Stiffness matrix'
	write(u,f)  this%Kmatrix
	write(u,'( 1h   )')  
		
	enddo

end subroutine printAllGle

INTEGER FUNCTION  Create_Gle(m,lm,cptr,nn,Kmat,nodes,F0,X0,p_has_tt, names) ! called by resolve.c 
	use vectors_f
	use math_f
	use cfromf_f
	IMPLICIT NONE
	INTEGER,INTENT (in)   ::  lm,nn, p_has_TT
	integer(kind = cptrsize), intent(in) :: cptr
	CHARACTER (len=lm) ,INTENT (in)     ::  m
	CHARACTER (len=64) ,INTENT (in)   ,dimension(64)   ::  names
	REAL  (C_DOUBLE), INTENT (in) , dimension (3*nn) :: F0, X0
	REAL  (C_DOUBLE),  INTENT (in) ,dimension (9*nn*nn) :: Kmat
	INTEGER, INTENT (in) , dimension (nn) :: nodes

	TYPE (gle)  ,POINTER :: this
	integer  :: n ,istat,err,i,j,k

	CHARACTER (len=256) :: modelname

        modelname = m; write(outputunit,*) ' Create_Gle with model = ',trim(modelname),' Needs verification '
! here the code to add it to the list. 
	n = NextFreeGle( err)
	if(err == 1) then
		write(outputunit,*) ' GLE allocation error'
	endif
	write(outputunit,*) ' mname = ', m
	write(outputunit,*) ' index= ',n,' count= ', GList%count, 'ubound= ',UBOUND(GList%list,1)


	this=>Glist%list(n)
	this%cptr=cptr
	this%Mname=m
	this%sli = 1					!only boat
	this%NeedToUpdate =	.false.
	this%blocked	=	.false. 
	this%tolerance	= 	0.2

	ALLOCATE(this%Kmatrix(3*nn,3*nn),stat= istat)
	ALLOCATE(this%F0(3*nn),stat= istat)
	ALLOCATE(this%X0(3*nn),stat= istat)
	ALLOCATE(this%RLbuf(3*nn),stat= istat)
	ALLOCATE(this%XLbuf(3*nn),stat= istat)
	ALLOCATE(this%nodeNos(nn),stat= istat)
	ALLOCATE(this%nodeNames(nn),stat= istat)
	k=1
	do i=1, nn*3
	do j=1, nn*3
		this%Kmatrix(i,j) = Kmat(k)
		k=k+1
	enddo
	enddo
	this%F0=F0
	this%X0=X0

	this%nodeNos=nodes
	this%n=nn
	write(outputunit,*) ' assign names'
	do j=1,nn
		this%nodeNames(j) = names(j)
		write(outputunit,'( i5,3x, a )') j,  trim(this%nodeNames(j) )
	enddo
	if(p_has_tt > 2) then
		write(outputunit,*) ' n0 ', trim(this%nodeNames(nn+1))
		write(outputunit,*) ' n1 ', trim(this%nodeNames(nn+2))
		write(outputunit,*) ' n2 ', trim(this%nodeNames(nn+3))
		this%node0 =getfortrannodeno(trim(modelname)//char(0),trim(this%nodeNames(nn+1))//char(0))
		this%node1 =getfortrannodeno(trim(modelname)//char(0),trim(this%nodeNames(nn+2))//char(0))
		this%node2 =getfortrannodeno(trim(modelname)//char(0),trim(this%nodeNames(nn+3))//char(0))
		this%T= IdentityMatrix(3);! wrong
		this%T = SCaxisMatrix(this%sli,this%node0,this%node1,this%node2 )
	else
		this%T= IdentityMatrix(3);
	endif

	this%used = .TRUE.
	Create_Gle=n
!	call printAllGle(outputunit)
END  FUNCTION  Create_Gle

INTEGER FUNCTION  Gle_count_By_SLI(sli) result( c)
	IMPLICIT NONE
	INTEGER,INTENT (in)   ::  sli

	TYPE (gle)  ,POINTER :: this
	integer  :: n
	c = 0
	do n=1, GList%count
		this=>Glist%list(n)
		if(this%sli .eq. sli) then 
			c = c + 1
		endif
	enddo
END  FUNCTION  Gle_count_By_SLI





subroutine Delete_Gle(i) 
	IMPLICIT NONE
	INTEGER,INTENT (in)   ::  i

	TYPE (gle)  ,POINTER :: this
	integer  :: istat
	write(outputunit,*) ' Delete GLE ',i

	this=>Glist%list(i)
	this%cptr=0
	this%Mname=' '
	this%sli = -1			
	DEALLOCATE(this%Kmatrix,stat= istat)
	DEALLOCATE(this%F0,stat= istat)
	DEALLOCATE(this%X0,stat= istat)
	DEALLOCATE(this%RLbuf,stat= istat)
	DEALLOCATE(this%XLbuf,stat= istat)
	DEALLOCATE(this%nodeNos,stat= istat)

	this%n=0
	this%used = .FALSE.
END  subroutine Delete_Gle



INTEGER FUNCTION   NextFreeGle(err)
	USE realloc_f
	IMPLICIT NONE
	INTEGER,INTENT (out)  ::  err
	integer :: i, istat
  	NextFreeGle = 0
	if(.not. ASSOCIATED(Glist%list)) THEN
		Glist%Block=5
		ALLOCATE(GList%list(Glist%block), stat=istat)
		if(istat /=0) THEN
			err = 1
			return
		endif
		Glist%list(1:ubound(Glist%list,1) )%used = .FALSE.
		Glist%list(1:ubound(Glist%list,1) )%Cptr = 0
		NextFreeGle = 1
		Glist%count =1
		Glist%list%used = .FALSE.
		Glist%list(1)%used = .TRUE.
		return
	ENDIF
	do i=1,ubound(GList%list,1)
		if(.not. Glist%list(i)%used) THEN
			NextFreeGle =i
			if(i > Glist%count) then
				Glist%count= NextFreeGle
			endif
			return
		endif		
	enddo
! we get here if no spare spaces
	i =  ubound(Glist%list,1) 
     	CALL reallocate(Glist%list, ubound(Glist%list,1) + Glist%block,err)
	if(err /=0) then
		write(outputunit,*) ' reallocation error in gle'
		STOP
	endif	
	Glist%list(i+1:ubound(Glist%list,1) )%used = .FALSE.
	Glist%list(i+1:ubound(Glist%list,1) )%Cptr = 0
	NextFreeGle = i+1
	Glist%count=NextFreeGle		

END  FUNCTION   NextFreeGle

end MODULE gle_f

