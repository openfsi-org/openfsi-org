
FUNCTION cmpptr (A, B) result ( r) ! DANGER this won't catch a test between two null pointers
   USE, INTRINSIC :: ISO_C_BINDING   
      type(C_PTR), INTENT(IN) :: A, B
      LOGICAL :: r
      !integer :: i=0
    !  write(outputunit,*) ' cmmptr', 1/i
     r = c_associated(a,b)
      
    END FUNCTION cmpptr


 
module rxfoperators_f

  INTERFACE OPERATOR(+)  ! this is the example  from ifort doc
    FUNCTION LGFUNC (A, B)
      LOGICAL, INTENT(IN) :: A(:), B(SIZE(A))
      LOGICAL :: LGFUNC(SIZE(A))
    END FUNCTION LGFUNC
  END INTERFACE

 ! INTERFACE OPERATOR(==) ! DANGER this won't catch a test between two null pointers
 !   FUNCTION cmpptr (A, B)
 !   USE, INTRINSIC :: ISO_C_BINDING   
 !   type(C_PTR), INTENT(IN) :: A, B
 !     LOGICAL :: cmpptr 
 !   END FUNCTION cmpptr
 ! END INTERFACE
  
end module rxfoperators_f

