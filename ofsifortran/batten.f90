! this is BATTEN.F . A development site for Dynamic Relaxation battens since 1992
      MODULE batten_f
	use vectors_f
	use batstuf_f
	use batn_els_f
      IMPLICIT NONE
      CONTAINS

! 10 july 2004 re-implemented after a 10 year gap. 
! The results are accurate at small deflections
!  but unstable at large deflections.  
! to improve things we should try
! 1) Use the Rodriguez formulation for  nodal rotations.
! 2) either get the beam element to accept larger distortions or adaptively subdivide them. 
! 3) a slow stopgap might be to relax the rotations at every step.  
!   Getting the matrices too  far out of date wrt the coords is one thing to watch for. 

!NB this file batten.f only used for:
!battenf90.obj : error LNK2001: unresolved external symbol _BATTEN_F_mp_PRINT_ONE_BATTEN_MATRICES@84
!battenf90.obj : error LNK2001: unresolved external symbol _BATTEN_F_mp_MAKE_ONE_BATTEN_AXES@28
!battenf90.obj : error LNK2001: unresolved external symbol _BATTEN_F_mp_MAKE_ONE_BATTEN_MATRICES@76
!battenf90.obj : error LNK2001: unresolved external symbol _BATTEN_F_mp_MAKE_ONE_BATTEN_FORCES@60 

!
!  NOTE AN IMPORTANT SPEED ITEM	line 1097 WRONG
!  A12 is banded. A(I,J) has bandwidth +-3
! (but remember its not square) 
! also Dr_Dx has bw +-3, so can we do a banded inversion


!
! DONE Zero_Moments  
! DONE slaving the rotation
! DONE the database. 
! DONE The DR scheme
!
!  String battens: Are like fixed battens except that 
!  1) The stiffness matrix is made with EA zero and axial load from 
!     string calculation
!     Then the Direct component of the STRING stiffness matrix is added
!
!  2) RESIDUALS.
!     The axial force is found by a String calc. 
!

!*************************************************************

!**********************************************************************

  
!*********************************************



      SUBROUTINE Measure_RotR(count,rb,Rmax)
      IMPLICIT NONE
      INTEGER Count
      REAL*8 rb(3,*)
!returns
      REAL*8 Rmax
!locals
      INTEGER I,k
      Rmax = 0.0D00
!      write(outputunit,*) ' Measure rotational residuals '
      DO I=1,count+1
!      write(outputunit,'(3g14.4)') (rb(k,i),K=1,3)
       DO K=2,3
        rmax = max(rmax,abs(rb(k,i)))
       ENDDO
      ENDDO
!	write(outputunit,*)' max is ',Rmax
      END     SUBROUTINE Measure_RotR
 
       SUBROUTINE Print_One_Batten_Rotations (count,rots)

      IMPLICIT NONE
      INTEGER count  
      REAL*8 rots(3,*)	       ! rotations numbered along batten
 
      INTEGER K, L
    
         write(outputunit,*) ' Rotations '
 !        if(kbflag) write(outputunit,*) ' Kbflag- shouldnt be here '
      DO L=1,count+1
        write(outputunit,'(i5,3f15.7)')L, (Rots(K,L),K=1,3)
      ENDDO
      END   SUBROUTINE Print_One_Batten_Rotations


      SUBROUTINE Zero_One_Moments(Rb,count,NDM,Dr_Dm,Rots,RF)
      IMPLICIT NONE
      REAL*8 Rb(3,*)		
      INTEGER count    ! Nodes along batten
      INTEGER NDM
      REAL*8 Dr_Dm(NDm,NDm) ! A22 inv How much to rotate nodes given 
      			  ! DM in such a way that dx is unchanged
       REAL*8 rf
! returns
      REAL*8 rots(3,*)	  ! rotations numbered along batten

!  Locals
      INTEGER I,J,K,Kj,Ir,Jr
      REAL*8 Theta

! rotate nodes to relieve rotational residuals (LINEAR) 
 
      DO I=1,Count+1			       
      DO K=2,3
        Ir = (I-1) *2 + K - 1
        theta = 0.0d00
        DO J=1,count+1
        DO Kj = 2,3
          Jr = (J-1)*2 + Kj-1
          theta= theta + Dr_Dm(ir,jr)*Rb(Kj,J)
        ENDDO
        ENDDO
        Rots(K,I)=Rots(K,I) + theta *rf
      ENDDO
!      write(outputunit,99)I, (Rots(K,i),K=1,3),(RB(K,I),K=1,3)
      ENDDO
99     format(i3, 2(1x,3G12.5))
      END SUBROUTINE Zero_One_Moments
         

      SUBROUTINE Get_Proposed_Rots(Rb,count,NDM,Dr_Dm,Rc)
      IMPLICIT NONE
      REAL*8 Rb(3,*)		
      INTEGER count    ! Nodes along batten
      INTEGER NDM
      REAL*8 Dr_Dm(NDm,NDm) ! A22 inv How much to rotate nodes given 
      			  ! DM in such a way that dx is unchanged
      
! returns
      REAL*8 rc(3,*)	  ! recommended delta-rotations

!  Locals
      INTEGER I,J,K,Kj,Ir,Jr
      REAL*8 Theta
         DO I=1,Count+1 
      	Rc(1,I) = 0.0D00			       
      	DO K=2,3
        	Ir = (I-1) *2 + K -1
        	theta = 0.0d00
        	DO J=1,count+1
         		DO Kj = 2,3
          		 Jr = (J-1)*2 + Kj -1
          		 theta= theta + Dr_Dm(ir,jr)*Rb(Kj,J)
         		ENDDO
        	ENDDO
        	Rc(K,I) = theta 
      	ENDDO
        ENDDO
      END  SUBROUTINE Get_Proposed_Rots


      SUBROUTINE Increment_Rots(count,Rots,rc,RF)
      IMPLICIT NONE
  		
      INTEGER count    ! Nodes along batten
      REAL*8 rf	      	! factor for deltas
      REAL*8 rc(3,*) 	! proposed deltas for rots
! returns - incremented
      REAL*8 rots(3,*)	  ! rotations numbered along batten

!  Locals
      INTEGER I,K
      LOGICAL big

      DO I=1,Count+1 
        big = .false.			       
        DO K=1,3
            Rots(K,I)=Rots(K,I) + rc(K,I) *rf
	    if(rots(K,I) .gt.  2.356) big=.true.
        ENDDO
	if(big) write(outputunit,'(a,3G12.4,a,f12.3)')' (increment_Rots)rots ',(rots(K,i),K=1,3) ,' rf ',rf
      ENDDO
      END SUBROUTINE Increment_Rots
  

      SUBROUTINE Update_One_Rotations(dx,list,count,NDM,Dr_Dx,Rots)
      IMPLICIT NONE
      REAL*8 dx(3,*)	       ! translations. Global numbering system	
      INTEGER list(*),count    ! Nodes along batten
      INTEGER NDM
      REAL*8 Dr_Dx(NDm,NDm) ! Slave matrix. How much to rotate nodes given 
      			  ! dx's in such a way that moments are un-changed
! returns
      REAL*8 rots(3,*)	       ! rotations numbered along batten

!  Locals
      INTEGER I,J,Kr,Ir,Kj,Jr	  ,K
      REAL*8 Theta
      LOGICAL big

! update rotations  as slaves to current K and dx's 
        DO I=1,Count+1
	big = .false.
	rots(1,i) = 0.0D00
      	DO Kr=2,3
      		Ir = (I-1) *2 + Kr - 1 
      		theta = 0.0D00  
      		DO J=1,count+1
      			DO Kj = 1,3
      			  Jr = (J-1)*3 + Kj
      			  theta= theta + Dr_Dx(ir,jr)*dx(Kj,List(J))
      			ENDDO
      		ENDDO
      		Rots(Kr,I)=Rots(Kr,I) + theta
		if(rots(Kr,I) .gt. 2.356)  big = .true.
      	ENDDO
	if(big) write(outputunit,'(a,i3,3G12.4)')'(U_O_R) rots ',i,(rots(K,i),K=1,3)

      ENDDO
99     format(3F15.5)
      END SUBROUTINE Update_One_Rotations

!      SUBROUTINE Make_One_Batten_Forces(list,count,EI,rots,TTU,Xib,Ztb,ZIB,Rb,se &
      SUBROUTINE Make_One_Batten_Forces(list,count,EI,rots,TTU,Xib,Ztb,ZIB,Rb,se &
       ,zisLT,trim,eas,SLide_Flag,Add_to_R,sli) ! adds into R
	use links_pure_f
	use coordinates_f
      IMPLICIT NONE
      INTEGER,intent(in) :: count	 !  edge count
      INTEGER list(count+1)		 ! Nodes along batten
      REAL*8 EI(6,count)	       ! bending stiffness
      REAL*8 rots(3,*)	       ! rotations numbered along batten
      REAL*8 XiB(3,count+1)	       ! Initial coords, numbered along batten

      REAL*8 TTU(3,3,count)	       ! undeformed transformation matrices
    !  REAL*8 b0(count)	       !betao's
      REAL*8 ZIb(count)
      REAL*8 Ztb(count) 
      INTEGER se(count)
      REAL*8 zisLT,trim,eas
      INTEGER Slide_Flag
	  logical :: add_to_R
	  integer, intent(in) :: sli
! returns
      REAL*8 Rb(3,count+1)	    ! rotational residuals, locally indexed
      REAL*8 TQS
! locals
      REAL*8 GL(12)
      INTEGER J,K,L
      INTEGER IN(2)
      INTEGER end,N
      real(kind=double) ::l_zt

		REAL*8 Xloc(3,2), Force(6), zis
		zis = zisLT+trim

		rb(1:3,1: (count+1)) = 0.0d00

		if(Slide_Flag.eq. 1) THEN
			CALL One_String_T(se,count,0.0D00,zis,eas,tqs,l_zt,.false.,sli) ! compressive stress allowed
			ei(6,1:count) =tqs
		ELSE
			tqs = 0.0D00
			ei(6,1:count) =tqs
		ENDIF

 ldo:   DO L=1,count
         IN(1)=list(L)
         IN(2)=List(L+1)
 
            DO j=1,2
           	 Xloc(1:3,J) = get_Cartesian_X(IN(J),sli)
            ENDDO
        write(outputunit,*) ' please verify PCB_One_Global_Forces'
        CALL PCB_One_Global_Forces(ei(1,L),Rots(1,L),Xloc,Xib(1,L),TTU(1,1,L),ZIb(L),GL,ZTb(L),Force) ! GL are global Forces


! add GL into Rb and optionally into R , ignoring any component in X-rotation

        DO end=1,2
			N = IN(end)
			Rb(1,L+end-1) = 0.0D00
			DO K=2,3
				Rb(K,L+end-1) = Rb(K,L+end-1) - gl( (end-1)*6 +3+ K)
  			ENDDO
			if(add_to_R) then
				k = increment_R(N, - gl( (end-1)*6 + 1 :(end-1)*6 + 3 )	,sli) 
			endif 
        ENDDO
      ENDDO ldo
END SUBROUTINE Make_One_Batten_Forces 
 
SUBROUTINE Print_One_Batten_Forces(Unit, list,count,EI,rots,TTU,Xib,Ztb,ZIB,sli)
	use coordinates_f
      IMPLICIT NONE
	        INTEGER Unit

	integer, intent(in) :: count  ! element count
      INTEGER list(*)  		! Nodes along batten

      REAL*8 EI(6,*)	       ! bending stiffness
      REAL*8 rots(3,*)	       ! rotations numbered along batten
      REAL*8 XiB(3,count+1)	       ! Initial coords, numbered along batten

      REAL*8 TTU(3,3,count)	       ! undeformed transformation matrices
    !  REAL*8 b0(*)	       !betao's
      REAL*8 ZIb(*)
      REAL*8 Ztb(*) 

      REAL*8 Rb(3,count+1)	    ! rotational residuals, locally indexed
    integer, intent(in) :: sli
! locals
      REAL*8 GL(12)
      INTEGER J,K,L
      INTEGER IN(2)
      INTEGER end

      REAL*8 Xloc(3,2), Force(6)

!if(kbflag) write(outputunit,*) ' Kbflag- shouldnt be here '
      RB(1:3,1:(count+1)) =0.0

      DO L=1,count

         IN(1)=list(L)
         IN(2)=List(L+1)

         DO j=1,2
           	 Xloc(1:3,J) = get_Cartesian_X(IN(J),sli)
        ENDDO
        write(outputunit,*) ' please verify PCB_One_Global_Forces'
	CALL PCB_One_Global_Forces(ei(1,L),Rots(1,L),Xloc,Xib(1:3,L:l+1), &
                TTU(1:3,1:3,L),ZIb(L),GL,ZTb(L),Force) ! GL are global Forces

! add GL into R

        DO end=1,2
          DO K=1,3
            Rb(K,L+end-1) = Rb(K,L+end-1) - gl( (end-1)*6 +3+ K)
  	  ENDDO
        ENDDO
      ENDDO
      write(unit,*) ' (POBF)Rotational Resids '
      DO L=1,count+1
      write(unit,'(3g15.4)') (Rb(K,l),K=1,3)
      ENDDO
      END SUBROUTINE Print_One_Batten_Forces 


      SUBROUTINE Print_One_Batten_Matrices (unit,sli,&
     &  list,count,EI,rots,TTU,Xib,b0,Ztb,ZIB,GeoM,NDM,Df_dx,Dr_Dm,Dr_Dx&
     &,rb,se,rev,zis,eas,SLide_Flag)
	use coordinates_f
      IMPLICIT NONE
      INTEGER Unit		! file unit
	  integer, intent(in)	:: count,sli
      INTEGER list(count+1)    ! nodes along batten
      INTEGER NDM
      REAL*8 EI(6,count)	       ! bending stiffness
      REAL*8 rots(3,count+1)	       ! rotations numbered along batten
      REAL*8 XiB(3,count+1)	       ! Initial coords, numbered along batten

      REAL*8 TTU(3,3,count)	       ! undeformed transformation matrices
      REAL*8 b0(count)	       !betao's
      REAL*8 ZIb(count)
      INTEGER GeoM	       ! true if geomatrix is to be included
      REAL*8 Rb(3,*)
      REAL*8 ZTB(count)	    ! zts for batten els of the string	
      REAL*8 Df_Dx(NDm,NDm) ! relates force to deflwith moments unchanged
      REAL*8 Dr_Dm(NDm,NDm) ! gets Dtheta from Dmoment to theta with dx=0
      REAL*8 Dr_Dx(NDm,NDm) ! Slave matrix. How much to rotate nodes given 
      			  ! dx's in such a way that moments are un-changed
      INTEGER se(count)	    ! tri-edge list
      INTEGER rev(count)	    ! true if Tri-edge reversed
      REAL*8 zis,eas	    ! overall length, ea
      INTEGER SLide_Flag    ! true if sliding

! locals
  
      INTEGER K,L ,I
      INTEGER Block ,b22
      REAL*8 gl(12),Force(6) ,Xloc(3,2)
!if(kbflag) write(outputunit,*) ' Kbflag- shouldnt be here '
!
	write(unit,*)
	write(unit,*) ' ===========================================' 			
	write(unit,*) ' BATTEN MATRICES'  

      Block = Count*3 + 3
       b22 = count*2 + 2
      write(unit,20) 'X'
20     format( 20x,3(a,30x),a)
   
      DO L=1,count+1
       if(list(L) >0) write(unit,10) L, get_Cartesian_X(list(L),sli)
      ENDDO

      write(unit,20) 'rots'
      DO L=1,count+1
        write(unit,10) L, Rots(1:3,L)
      ENDDO

      write(unit,20) 'Xib'
      DO L=1,count+1
        write(unit,10) L, (Xib(K,L),K=1,3)  
      ENDDO

      write(unit,20) 'Rb(rotational resids)'
      DO L=1,count+1
        write(unit,10) L, (rb(K,L),K=1,3) 
      ENDDO
      
      write(unit,20) 'TTU'
      DO L=1,count
        CALL mprint (unit,TTU(1,1,L),' ttu ',3,3) 
      ENDDO



      write(unit,*) ' GeoM flag',Geom,' Slide_Flag',Slide_Flag
      write(unit,'(a,2g14.4/)')' eas, zis ',eas,zis
      write(unit,'(a)') ' L,  rev,  zib,       ztb,       b0,       ei'

      DO L=1,count
            write(unit,11,err=12)se(L),rev(L),Zib(L),ZTb(L),b0(L),(EI(K,L),K=1,6)
12    ENDDO
    
10    FORMAT (i3,1x,4(1h(,3G11.4,1h)))
11    FORMAT (i5,i2,7G12.3,2F8.3)

      CALL Mdisp(Unit,Df_Dx,' Df by Dx' ,Block,NDM)
      CALL Mdisp(Unit,Dr_Dm,' Dm Dr   ' ,B22  ,NDM)
      CALL Mdisp(Unit,Dr_Dx,' Dr Dx   ' ,Block,NDM)

      WRITE(Unit,'(2a)')'    M13:        M23:       M12:      '&
     & ,'      M22:       Torque:    Tension'

      DO L=1,count
  
          Xloc(1:3,1) = get_Cartesian_X(List(L),sli)
          Xloc(1:3,2) = get_Cartesian_X(List(L+1),sli)

         CALL PCB_One_Global_Forces(ei(1,L),Rots(1,L),Xloc,Xib(1:3,L:l+1), TTU(1,1,L),ZIb(L),GL,ZTb(L),Force)

!    GL are global Forces


      write(unit,'(6G12.4,A)')(force(i),i=1,6),' (axial force doesnt show sliding)'

      ENDDO
      
      END  SUBROUTINE Print_One_Batten_Matrices

!****************************************************************

      SUBROUTINE Make_One_Batten_Axes(list,count,TTU,Xib,b0,ZIB,se,sli)
	use coordinates_f
      IMPLICIT NONE
      integer, intent(in out) :: count
      INTEGER list(count+1)   ! Nodes along batten
       REAL*8 b0(*)	       !betao's
      REAL*8 ZIb(*)
      INTEGER se(*)	       ! The Triedges 
      integer, intent(in):: sli
          	
! returns
 
      REAL*8 XiB(3,count+1)	       ! Initial coords, numbered along batten

      REAL*8 TTU(3,3,count)	       ! undeformed transformation matrices
      		       
      		   
! locals
	integer OK
 !      if(kbflag) write(outputunit,*) ' shouldnt be in  Make_One_Batten_Axes' 
       OK = PCB_New_XIB(count,list,se,xib,sli)
       CALL PCB_One_Initial_TT(count,Xib,B0,TTU,Zib) ! we've already done this. Why Twice??

      END SUBROUTINE Make_One_Batten_Axes

!****************************************************************
      SUBROUTINE Make_One_Batten_Matrices&
     &(list,c,EI,rots,TTU,Xib,b0,Ztb,ZIB,GeoM,Ndm,Df_dx,Dr_Dm,Dr_Dx&
     & ,se,rev,zis,eas,Slide_Flag,sli)
     use saillist_f
	use links_pure_f
	use coordinates_f
	use invert_f
      IMPLICIT NONE
	  integer, intent(in)	:: c	! no of edges
      INTEGER list(c+1)			    ! Nodes along batten
      INTEGER,intent(in)	:: NDM
      REAL*8 EI(6,*)	       ! bending stiffness
      REAL*8 rots(3,*)	       ! rotations numbered along batten
      REAL*8 XiB(3,c+1)	       ! Initial coords, numbered along batten

      REAL*8 TTU(3,3,c)	       ! undeformed transformation matrices
      REAL*8 b0(*)	       !betao's
      REAL*8 ZIb(*)
      INTEGER GeoM	       ! true if geomatrix is to be included
      INTEGER se(*)	       ! The Triedges 
      INTEGER rev(*)	       ! True if the TriEdges are reversed
      REAL*8 zis,eas	       ! overall length and EA 
      INTEGER, intent(in) :: sli, Slide_Flag		
! returns
      REAL*8 ZTB(*)	    ! zts for batten els of the string	
      REAL*8 Df_Dx(NDm,NDm) ! relates force to deflwith moments unchanged
      REAL*8 Dr_Dm(NDm,NDm) ! gets Dtheta from Dmoment to theta with dx=0
      REAL*8 Dr_Dx(NDm,NDm) ! Slave matrix. How much to rotate nodes given 
      			  ! dx's in such a way that moments are un-changed

! REQUIRES . a call to MAke_One_Batten_Axes immediately  before


! locals
      REAL*8 GL(12,12)
	  real(kind=double), target, dimension(2*ndm,2*ndm) :: S

      INTEGER I,J,L,N
      INTEGER EndR,endC,Kr,Kc,Ig,Jg,Il,Jl , IN(2)
      INTEGER R1,Block !, Nd

      LOGICAL SolveError ,G

      REAL*8 aa12(ndm,ndm)
	  real(kind=double), pointer, dimension(:,:)	:: a11,a12,a21,a22
      REAL*8 Xloc(3,2), De(3,ndm)
       real(kind=double)::l_zt, tqs
	  integer, dimension(4)	:: a,b
!	  character*32 f
   
     TYPE (edgelist),pointer:: eds
     ! type(Fedge),pointer ::e,e2
      eds=>saillist(sli)%edges
      		   
!if(kbflag) write(outputunit,*) ' Kbflag- shouldnt be here '

	N = NDM			! insulation
   
	a11=>s(1:n,1:n) ;		a12=>s(n+1:2*n,1:n); 
	a21=>s(1:n,n+1:2*n);	a22=>s(n+1:2*n,n+1:2*n)

	Df_Dx(1:n,1:n) = 0.0d00
	Dr_Dx(1:n,1:n) = 0.0d00
	Aa12(1:n,1:n) = 0.0D00
	s=0.0d00
! de are unit vectors   THere are ne(=count) of them
! zis is the unstressed batten length
! eas is its overall EA

        DO I = 1,c
          if(rev(i).eq.1) THEN
         		de(1:3,i) =- eds%list(se(i) )%m_DLINK
      	 ELSE
         		de(1:3,i) = eds%list(se(i) )%m_DLINK
          ENDIF
        ENDDO

      if(Slide_Flag.eq. 1) THEN
           CALL One_String_Sliding_Dir_Stiffness(NDm,Df_Dx,de,c,zis,eas)
     	   CALL One_String_T(se,c,0.0D00,zis,eas,tqs,l_zt,.false.,sli)  ! the logical is 'maybuckle
		   	s(1:ndm,1:ndm) = df_dx
!            ! compression allowed
      ELSE
       		tqs = 0.0D00
      ENDIF
      ei(6,1:c) = TQS

! now carry on ensuring force(6) is TQS and ea is 0.0
!
!        BEAM ELEMENT STIFFNESS
!
		Block = C*3 + 3

ldo:	DO L=1,c
			IN(1)=list(L)
			IN(2)=List(L+1)
			DO j=1,2
				Xloc(1:3,J) = get_Cartesian_X(IN(J),sli)
			ENDDO
			IN(1) = L
			IN(2) = L + 1
			G = (Geom .eq. 1)
                        CALL PCB_One_Global_Stiffness(ei(1:6,L),Rots(1:3,L:L+1),Xloc,Xib(1:3,L:L+1),TTU(1:3,1:3,L),G,ZIb(L),GL,ZTb(L))

! GL is in global axes. , direct plus geometric if required
			a(1) = 3*L - 2;		b(1) = 3*L
			a(2) = ndm+ a(1);	b(2) = b(1) + ndm
			a(3) = 3*L+1	;	b(3) = 3*L+3
			a(4) = ndm+a(3);	b(4) = ndm + b(3)
			do i=1,4
			do j=1,4
				s(a(i):b(i) , a(j):b(j)) = s(a(i):b(i),a(j):b(j)) + gl(3*i-2:3*i,3*j-2:3*j)
			enddo
			enddo

			cycle ldo
! Miss out X-component for rotations a12, a22 ??to avoid a singularity??

       DO  endR=1,2  
          DO Kr = 1,3
               DO  endC=1,2
                 DO Kc=1,3  ! DOF
                  Ig = Kr + 3*IN(endR)-3  ! ROW of global matrix
                  Il = Kr + 6* (EndR-1)   ! ROW of element matrix

                  Jg = Kc + 3*IN(endc)-3  ! COL global matrix
                  Jl = Kc + 6* (Endc-1)   ! COL element matrix
                  Df_Dx(Ig,Jg)= Df_Dx(Ig,Jg) + GL(Il,Jl)

                ENDDO
               ENDDO
            ENDDO
          ENDDO
         
! a12  .  a21  is transpose
       DO  endR=1,2  
          DO Kr = 1,3
               DO  endC=1,2
                 DO Kc=2,3  ! DOF
                  Ig = Kr + 3*IN(endR)-3    ! ROW of global matrix
                  Il = Kr + 6* (EndR-1)     ! ROW of element matrix
                  Jg = Kc + 2*IN(endc)-3     ! COL global matrix
                  Jl = Kc + 6* (Endc-1) + 3 ! COL element matrix
                  Aa12(Ig,Jg)= aa12(Ig,Jg) + GL(Il,Jl)
		 ENDDO
               ENDDO
            ENDDO
          ENDDO
       
! a22
      DO  endR=1,2  
          DO Kr = 2,3
               DO  endC=1,2
                 DO Kc=2,3  ! DOF
                  Ig = Kr + 2*IN(endR)-3          ! ROW of global matrix
                  Il = Kr + 6* (EndR-1)  + 3      ! ROW of element matrix
                  Jg = Kc + 2*IN(endc)-3           ! COL global matrix
                  Jl = Kc + 6* (Endc-1)  + 3      ! COL element matrix
                  Dr_Dx(Ig,Jg)= Dr_Dx(Ig,Jg) + GL(Il,Jl)

                 ENDDO
               ENDDO
            ENDDO
          ENDDO
      ENDDO ldo
       
      r1 = Block+1	   ! index of rotation start

		a22(1,1) = a22(1,1) + 500		! fix for rigid body mode.  Poor. WRONG 

! here the matrices are the negative of the stiffness matrices. 
! the leading diagonal is all positive

! condensing out the rotations

! assume A    is  |   |   |         | |      |
!                 | F |   | A11 A12 | |trans |
!                 |   | = |         | |      |
!                 |   |   |         | |      |
!                 |tor|   | A21 A22 | | rots |
!                 |   |   |         | |      |
!
!  THen             -1    -1
! trans will be (a11 - a12 A22  a21 )   * F
! and
!      -1
! rot   will be (-A22  A21) F

!		write(f,*) '(',ndm,'G15.8)'
!		write(99,*) 'a11 ';	write(99,f) a11
!		write(99,*) 'a12 '; 	write(99,f) a12
!		write(99,*) 'a21 '; 	write(99,f) a21
!		write(99,*) 'a22 '; 	write(99,f) a22


		CALL invert(N,a22,Dr_Dm,N,solveError)	 ! sb banded
		if(SolveError) then
			write(outputunit,*) ' solveError'
		endif
!		write(99,*) 'drdm '; 	write(99,f) dr_dm
		dr_dx = - matmul(dr_dm,a21)
		df_dx = a11 + matmul(a12,dr_dx) ! NB dr_dx is negative

!		write(99,*) 'drdx '; 	write(99,f) dr_dx
!		write(99,*) 'dfdx '; 	write(99,f) df_dx

	return
 END  SUBROUTINE Make_One_Batten_Matrices

END MODULE batten_f
