MODULE flinkList_f
USE ftypes_f
USE  linklist_f
USE  saillist_f
IMPLICIT NONE

! NOTE. A F-Link is  a simple bar element, used for Janet's projects.  It doesnt
! respect the creation-deletion rules. so dont try to do anything new with it.
! in particular it  is likely to be invalidated if you add any more FNodes after adding some links.



CONTAINS  
function CreateFortranFlink (sli, NN ,p_n1,p_n2,p_ea,p_zi,p_ti, p_mass,ptr,err) result(rc) ! should place it at N
USE realloc_f
USE, INTRINSIC :: ISO_C_BINDING
IMPLICIT NONE
	INTEGER , intent(in) :: sli,nn,p_n1,p_n2
	integer(kind=cptrsize), intent(in) ::ptr
	real(c_double), intent(in) ::p_ea,p_zi,p_ti,p_mass
	INTEGER , intent(out) :: err
	TYPE ( flinklist) ,POINTER :: flinks
	integer(C_int) :: rc
	INTEGER :: istat
	rc=-1
	err = 16
	if(.not. associated(saillist)) return
	if(sli > ubound(saillist,1)) return	
	if(sli < lbound(saillist,1)) return
	flinks=>saillist(sli)%flinks
	err=8
	if(NN < 1) then
	    write(outputunit,*) 'cant create a link with zero index'
	    return
	endif
	err = 0
!
! Normal conditions
!	just starting : 
!	NN <= count. >> set X,Y,Z used
!	NN > ubound >> realloc then set X,Y,Z used  if alloc OK. set count
!	NN > count but < Ubound  set count.set  X,Y,Z used

! error conditions
!   unable to alloc
!   NN < 0
!
	if(.not. ASSOCIATED(flinks%list)) THEN
	!	flinks%block=25000
		ALLOCATE(flinks%list(flinks%block), stat=istat)
		if(istat /=0) THEN
			err = 1
			return
		endif
		flinks%count=0
	ENDIF
	DO
		if(NN >= UBOUND(flinks%list,1)) THEN
			CALL reallocate(flinks%list, ubound(flinks%list,1) + flinks%block, err)
			if(err > 0) return
		ELSE
			exit
		ENDIF
	ENDDO
		if(NN > flinks%count) flinks%count= NN
	
        flinks%list(NN)%N=NN
        flinks%list(NN)%m_EA =p_ea 
        flinks%list(NN)%m_flptr = ptr
        flinks%list(NN)%m_Zlink0=p_zi
        flinks%list(NN)%m_ti=p_ti
        flinks%list(NN)%m_mul= p_mass
        flinks%list(NN)%m_e1 =>saillist(sli)%nodes%xlist( p_n1)   ! we must not reallocate saillist(sli)%nodes%xlist
        flinks%list(NN)%m_e2 =>saillist(sli)%nodes%xlist( p_n2 ) 
        flinks%list(NN)%m_e1%m_elementcount = flinks%list(NN)%m_e1%m_elementcount + 1
        flinks%list(NN)%m_e2%m_elementcount = flinks%list(NN)%m_e2%m_elementcount + 1         
        flinks%list(NN)%m_used =.true.
        flinks%list(NN)%IsTensionControlled = (p_ea<1)  ! YUCH hard-coded value. a quicky for Janet
        rc=nn

        ! If we tell the node about the links connected to it we can avoid a critical section in the residual calc.
        
#ifdef _DEBUG  
    if(mod(nn ,250) ==0) then     
        write(outputunit,'(a,i6,a,4g12.4)') 'create link ',nn, ' ea,zi,ti,mass  ', flinks%list(NN)%m_EA,flinks%list(NN)%m_Zlink0,flinks%list(NN)%m_ti,flinks%list(NN)%m_mul
    endif
#endif    
END function CreateFortranflink 

SUBROUTINE RemoveFortranflink (sn, NN,err) 
USE realloc_f
use cfromf_f  ! for zeroptr
IMPLICIT NONE
	INTEGER , intent(in) :: sn,nn
	INTEGER , intent(out) :: err
	TYPE ( flinklist) ,POINTER :: flinks
	err = 16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return
	flinks=>saillist(sn)%flinks
!
!   What it does.
!	unsets the USED flag. realloc if  it can
!
! USUAL Conditions

!   NN =  count  >> decrement count
!    if then count < ubound-block, reallocate
!

! Unusual conditions
!   sailno out of range  >> err 16
!   flinklist not associated  >>err 8
! NN < lbound or > Ubound  >>err 8
!
	err=8
	if(.not. ASSOCIATED(flinks%list)) return
	if(NN > UBOUND(flinks%list,1)) return
	if(NN  <LBOUND(flinks%list,1)) return
	err=0
	
		flinks%list(NN)%N=NN
		flinks%list(NN)%m_used= .FALSE.
	
	if(NN == flinks%count) THEN
		 flinks%count =flinks%count-1
		if( flinks%count+flinks%block < ubound(flinks%list,1) ) THEN
			CALL reallocate(flinks%list, ubound(flinks%list,1) - flinks%block, err)
		ENDIF
	ENDIF

END SUBROUTINE RemoveFortranflink



SUBROUTINE SetflinkUsed   (sn, NN,flag,err)
IMPLICIT NONE
	INTEGER , intent(in) :: sn,nn,flag
	INTEGER , intent(out) :: err
	TYPE ( flinklist) ,POINTER :: flinks
!
! sets the Used flag depending on the value of 'flag'
!  the usual error returns. 
!  the only twist is that if this is the last flink, we decrement count if flag=0
!  similarly, we increment count if NN is big. But only up to 
!
!	write(outputunit,*) 'SetflinkUsed   ', sn, NN,flag
	err=16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	err=8
	flinks=>saillist(sn)%flinks
	if(.not. ASSOCIATED(flinks%list)) return
	if(NN > UBOUND(flinks%list,1)) return
	if(NN  <LBOUND(flinks%list,1)) return
	err=0
	if(flag/=0) THEN
		if(flinks%list(NN)%m_used) err=2
		flinks%list(NN)%m_used=.TRUE.
		if(NN > flinks%count) flinks%count=NN 
	else
		if(.NOT. flinks%list(NN)%m_used) err=1
		flinks%list(NN)%m_used=.FALSE.
		if(NN ==  flinks%count) flinks%count=flinks%count-1 
	endif

END SUBROUTINE SetflinkUsed 
function AssembleLinkElts(sli,StringCompAllowed,m)result (rc)
    use saillist_f
	use coordinates_f
	use vectors_f
	use links_pure_f
	use stiffness_f
      IMPLICIT NONE

! parameters
    integer, intent(in) :: sli
    LOGICAL, intent(in) :: StringCompAllowed    ! TRUE if negative tension allowed 
    integer(kind=cptrsize)  :: m
     integer rc
  ! returns
 ! nothing, but increments nodelist%XM 
 ! if p_comp is false, we add the direct stiffness but skip the geometric.

!* locals
    INTEGER N 
 	real*8 dloc(3),zt
	real(kind=double),dimension(6,6) :: s6
     type(sail), pointer  ::sl=>NULL()
	type (flink) , pointer		:: e
	integer, dimension(2) :: nn
     if(.not. is_active(sli)) return
	sl=>saillist(sli)
    rc=0
 	if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 18'	

    do_n : DO N=1,sl%flinks%count
      	e =>sl%flinks%list(n)  

        if(.not. e%m_used) cycle
 		    dloc = get_Cartesian_X_By_Ptr(e%m_e2) - get_Cartesian_X_by_Ptr(e%m_e1)
		    zt=sqrt(dot_product(dloc,dloc))
	        e%m_TLINK =e%m_EA *(ZT -e%m_Zlink0 )/    e%m_Zlink0 +e%m_TI 	
            if(.not. StringCompAllowed .and. e%m_TLINK<0.0) then
                e%m_TLINK=0
                cycle
             endif    
            call One_Link_Global_Stiffness(Dloc,e%m_TLINK,e%m_EA, s6,ZT)
            nn(1)=e%m_e1%nn ;  nn(2)=e%m_e2%nn 
            call  AddStiffness(sl,nn,s6,m)
            rc=rc+1
      ENDDO do_n  
        
end function AssembleLinkElts

function  MakeAllLinkMasses(sli,p_Comp) result (rc)
    use saillist_f
	use coordinates_f
	use vectors_f
	use bar_elements_f
	use links_pure_f
      IMPLICIT NONE

! parameters
    integer, intent(in) :: sli
    LOGICAL, intent(in) ::  p_Comp  ! TRUE if negative tension allowed 

! returns
 ! nothing, but increments nodelist%XM 
 ! if p_comp is false, we add the direct stiffness but skip the geometric.

!* locals
    INTEGER N,j,rc
 	real*8 dloc(3),t ,zt
	real(kind=double),dimension(3,3) :: s3
     type(sail), pointer  ::sl=>NULL()
	type (flink) , pointer		:: theString,e
     if(.not. is_active(sli)) return
	sl=>saillist(sli)
    rc=0
 	if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 18'	
 !   call JanetSpecial(sli,p_comp)
    do_n : DO N=1,sl%flinks%count
      	theString =>sl%flinks%list(n) 
        if(.not. theString%m_used) cycle
            e => theString 
		    dloc = get_Cartesian_X_By_Ptr(e%m_e2) - get_Cartesian_X_by_Ptr(e%m_e1)
		    zt=sqrt(dot_product(dloc,dloc))
		    if(zt < 1e-5) then
		        write(*,*) ' short link ',n
		        cycle
		    endif
 
	        theString%m_TLINK =theString%m_EA *(ZT -theString%m_Zlink0 )/ &
                theString%m_Zlink0 +theString%m_TI 	
            if(.not. p_comp .and. theString%m_TLINK<0.0) then
                theString%m_TLINK=0
             endif    
		    t =  e%m_Zlink0  ! initial length 
		    j = Bar_Matrix(dloc,t,norm(dloc),theString%m_TLINK,theString%m_EA,s3)

            e%m_e1%XM(1:3,1:3) = e%m_e1%XM(1:3,1:3) + s3
            e%m_e2%XM(1:3,1:3) = e%m_e2%XM(1:3,1:3) + s3
      ENDDO do_n
 END function MakeAllLinkMasses
 ! start from links_pure 
#ifdef NEVER
SUBROUTINE JanetSpecial(sli,p_comp)
      use coordinates_f
      use rx_control_flags_f
      use omp_lib
      IMPLICIT NONE
	integer, intent(in) ::sli
    LOGICAL, intent(in) ::  p_Comp  ! TRUE if negative tension allowed 
 ! returns
 ! nothing, but sets the links properties accoring to the extension.

!* locals
    INTEGER N ,nn,chunksize
 	real*8 ztsq,d(3)   !, ZT
 	real(kind=double), parameter :: L0 = 0.02 ! yuch hard code two centimeter.
 	real(kind=double), parameter :: L0sq = L0*L0  
 	TYPE ( flinklist) ,POINTER :: flinks
    if(.not. is_active(sli)) return
 
	flinks=>saillist(sli)%flinks
	nn = flinks%count
	chunksize = nn/36
    if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 21'
   
!$OMP PARALLEL   PRIVATE(N,d,ztsq  ) shared(p_comp,nn,flinks,Mass_Set_Flag) default(none)  	if(nn>100)
!$OMP   DO SCHEDULE(auto)
   do_n : DO N=1,nn  
            if(flinks%list(n)% IsTensionControlled == 0 ) cycle   
!DEC$ NOVECTOR   
            d = get_Cartesian_X_By_Ptr(flinks%list(n)%m_e2) - get_Cartesian_X_by_Ptr(flinks%list(n)%m_e1)	 
            ztsq = dot_product(d,d) ;
            if(flinks%list(n)% IsTensionControlled == 1) then ! it thinks it is constant tension. lets check if it is long enough
              if(ztsq < L0sq ) then   ! make it low-mod  . Else leave it as it is.
                    flinks%list(n)%IsTensionControlled =2
                    flinks%list(n)%m_ea = flinks%list(n)%m_ti/L0
                    flinks%list(n)%m_Zlink0=L0
                    flinks%list(n)%m_Ti=0.0   
                    write(*,*)' link ',n,' is getting short' 
                    Mass_Set_Flag = 1            
              endif     
            else if(flinks%list(n)% IsTensionControlled == 2) then ! it thinks it has low modulus, lets check if its too long
                if(ztsq >= L0sq ) then   ! make it constant-tension
                    flinks%list(n)% IsTensionControlled =1
                    flinks%list(n)%m_ti = flinks%list(n)%m_ea * L0
                    flinks%list(n)%m_ea=0.0  
                    write(*,*)' link ',n,' is getting longer' 
                    Mass_Set_Flag = 1                                     
                endif
            endif
       ENDDO do_n
!$OMP END DO  NOWAIT
!$OMP END PARALLEL 
          
 END  SUBROUTINE JanetSpecial
#endif
 
SUBROUTINE Link_Residuals(sli,p_comp)
      use coordinates_f
      use omp_lib
      IMPLICIT NONE
	integer, intent(in) ::sli
    LOGICAL, intent(in) ::  p_Comp  ! TRUE if negative tension allowed 
 ! returns
 ! nothing, but increments nodelist%R

!* locals
    INTEGER N ,nn,chunksize
 	real*8 ZT,d(3) 
 	TYPE ( flinklist) ,POINTER :: flinks
    if(.not. is_active(sli)) return
   ! call JanetSpecial(sli,p_comp)
    
	flinks=>saillist(sli)%flinks
	nn = flinks%count
	chunksize = nn/12
    if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 17'
!$OMP PARALLEL   PRIVATE(N,d,zt  ) shared(p_comp,nn,flinks) default(none)  	if(nn>100)
!$OMP   DO SCHEDULE(auto)
!!!!!!!!!!!dynamic,chunksize) 
 
   do_n : DO N=1,nn  
 !!!!!!!!! if(.not. flinks%list(n)%m_used) then ;flinks%list(n)%m_d=0; write(outputunit,*)'link',N,'isnt used';  cycle; endif
!DEC$ NOVECTOR           
            d = get_Cartesian_X_By_Ptr(flinks%list(n)%m_e2) - get_Cartesian_X_by_Ptr(flinks%list(n)%m_e1)	 ! we get 'loop was vectorized'            
            ZT=SQRT(dot_product(d,d))

            flinks%list(n)%m_TLINK =flinks%list(n)%m_EA *(ZT -flinks%list(n)%m_Zlink0 )/ flinks%list(n)%m_Zlink0 +flinks%list(n)%m_TI 
            if(.not. p_comp .and. flinks%list(n)%m_TLINK<0.0) then
                flinks%list(n)%m_TLINK=0
                flinks%list(n)%m_d=0
                cycle 
             endif
           
!DEC$ NOVECTOR
            d = (flinks%list(n)%m_TLINK/ZT )*d

            call increment_r_by_ptr(flinks%list(n)%m_e1,d )
            call increment_r_by_ptr(flinks%list(n)%m_e2,-d )

       ENDDO do_n
!$OMP END DO  NOWAIT
!$OMP END PARALLEL 
          
 END  SUBROUTINE Link_Residuals
    

 function  DrawFlinks (sli,k) result(rc)
 	use hoopsinterface
 	use coordinates_f
 	use cfromf_f
  implicit none
  integer, intent(in) ::sli
      integer(kind=cptrsize) , value :: k
 	TYPE ( flinklist) ,POINTER :: flinks
	INTEGER :: n,rc,propno
	TYPE ( flink) ,POINTER :: f
	real(c_FLOAT), dimension(3) ::a,b
    real(kind=8) ::sumx,npReal,mint,maxt,avg,variance,sd,t1,t2,g_nsds=1,  t 
    logical :: start=.true.,Skip_Zero_Tension=.true.
   	integer  ihue ,  has_seg
	character *256 buf ,s 
	real(c_double) :: hue,sat,val
   hue=200; sat=1; val=1;
	if(.not. associated(saillist)) return
	if(sli > ubound(saillist,1)) return	
	if(sli < lbound(saillist,1)) return
	flinks=>saillist(sli)%flinks
	rc=0; start=.true.;Skip_Zero_Tension=.false.;hue=200 ; variance=0.0; npReal=0.0;
! get tension range	
	do n=1,flinks%count
	f=>flinks%list(n)
	if(.not. f%m_used)  cycle
	    if(start) then
		    sumx =f%m_tlink; npReal=1.; mint=sumx; maxt=sumx; start=.false.	    
	    else
	    	sumx =sumx+f%m_tlink; npReal=npReal+1.;
		mint=min(mint,f%m_tlink); maxt=max(maxt,f%m_tlink); start=.false.	    
	    endif		
	enddo
! now the variance
  if(npReal>0) then
	   avg = sumx/npReal;
	    do n=1,flinks%count
	        f=>flinks%list(n)
	        if(.not. f%m_used)  cycle
			        variance =variance + (f%m_tlink-avg)**2; 
        enddo
	   sd=sqrt(variance/npReal);
	    if(abs(avg) < 0.00000001 .and. abs(variance) < 0.0000001) Skip_Zero_Tension=.false.
	   t1=avg- g_nSDs*sd; t2=avg+g_nSDs*sd; 
	   t1=max(t1,mint); t2=min(t2,maxt); 
	   t2=max(t1+0.000000001,t2);
	   write(outputunit,'(6(a,g10.3))') ' range ',t1,' to ', t2,' sd=',sd,' avg=',avg,' min=',mint,' max=',maxt
   endif ! if np	
! now draw	
	do n=1,flinks%count
	f=>flinks%list(n)
	if(.not. f%m_used)  cycle

	if(Skip_Zero_Tension.and. f%m_tlink<=0.0)	cycle  

	if(abs(t2-t1) < 1e-8) then
		hue = 225;
	else  
		t = (f%m_tlink -t1)/(t2-t1);
		hue = 270.0 - t*270.0;  !270 =cold, descending to 0 =hot
		hue = max(hue,0.0); hue = min(hue,360.0);
	endif
	 ihue = hue;
    propno = cf_getTensyllinkPropNo(f%m_flptr);
    write(s,'(a,i3.3)') "Prop_",propno
	 has_seg = .true.  
	if(has_seg) call hf_Open_Segment(trim(s)//char(0));

	write(buf,'(a,i3.3)')'hue_', ihue  ; hue=ihue;
	 call HF_Open_Segment(trim(buf)//char(0));
       ! if(mod(	 n,100) .eq. 0) write(outputunit,'(a,3(1x,a,1x,F9.4))')trim(buf), ' hue=',hue,' sat=',sat,' val=',val
		call Hf_Set_Color_By_Value("lines"//char(0), "HSV"//char(0),hue,sat,val);

 !see plotps	 m_e1 ,m_e2
      A=get_Cartesian_X_by_ptr(f%m_e1) 
      B=get_Cartesian_X_by_ptr(f%m_e2)                   
      CALL akm_INSERT_LINE(a(1),a(2),a(3),b(1),b(2),b(3),k,hue,sat,val)
	call HF_Close_Segment();
	if(has_seg) cALL HF_Close_Segment();      
	enddo 
 end  function  DrawFlinks
 
 subroutine PrintFLinks(sli,unit)
 implicit none
 integer, intent(in):: sli,unit
 	TYPE ( flinklist) ,POINTER :: flinks
	INTEGER :: n,rc
	TYPE ( flink) ,POINTER :: f


	if(.not. associated(saillist)) return
	if(sli > ubound(saillist,1)) return	
	if(sli < lbound(saillist,1)) return
	flinks=>saillist(sli)%flinks
	rc=0
	
	do n=1,flinks%count
		if(n.eq.1) write(unit,'(A)') "f%N ,f%m_used,  f%m_e1%NN,f%m_e2%NN,f%m_EA,f%m_TLINK,f%m_Zlink0,f%m_ti,f%m_mass"
	    f=>flinks%list(n)
	    if(.not. f%m_used) cycle
	    write(unit,10) f%N ,f%m_used,  f%m_e1%NN,f%m_e2%NN,f%m_EA,f%m_TLINK,f%m_Zlink0,f%m_ti,f%m_mul
10  format( i8,L8,I8,I8,5G15.5)                     

	enddo 
 end  subroutine PrintFLinks
 SUBROUTINE   PrintFlinks_As_PC (sli,unit,length_factor) 
USE ftypes_f
USE saillist_f

IMPLICIT NONE
    INTEGER , intent(in) :: sli,unit
    real(c_double), intent(in) :: length_factor

    TYPE (flinklist) ,POINTER :: strings
    TYPE ( flink) ,POINTER :: this,t
    INTEGER :: i ,nn,n1,n2
    real(kind=8):: totlength
    if(.not. associated(saillist)) return
    if(sli > ubound(saillist,1)) return	
    if(sli < lbound(saillist,1)) return

	strings=>saillist(sli)%flinks
	if(.not. ASSOCIATED(strings%list)) return
	
	write(unit,'(/,a)') '// From FLINKS. First line is number of Links '
	write(unit,*)'//Num, StartN, EndN, SlackLength, PrpNo, Control, Boundary, Field'
    	write(unit,*) '<LINKS>'
 
! count the number of strings
    nn=0
	do i =1,  UBOUND(strings%list,1)
		this=>strings%list(i) 
		if(this%m_used) then
                 nn=nn+1
		endif
	ENDDO
    write(unit,'(i,//)') nn

    nn=0; totlength=0;
	do i =1,  UBOUND(strings%list,1)
		t=>strings%list(i) 

ifused:		if(t%m_used) then
               nn=nn+1
 !Number, n1, n2, SlackLength, PropNo, Control, Boundary, Field
 
                n1 = t%m_e1%nn; n2 = t%m_e2%nn 
                write(unit,'(3(1x,i8),F16.10,4i4)') nn,n1,n2,t%m_ZLINK0*length_factor ,1,1,0,0
                totlength = totlength + t%m_ZLINK0*length_factor

		endif ifused
	ENDDO
    	write(unit,*) '</LINKS>'
    	write(unit,*) '// total length ', totlength
END  SUBROUTINE  PrintFlinks_As_PC

SUBROUTINE  ShowFlinkCount(sn, u,l,c, err) 
USE ftypes_f
 
IMPLICIT NONE
	INTEGER , intent(in) :: sn
	INTEGER , intent(out) :: err,u,l,c

	TYPE ( flinklist) ,POINTER :: flinks

	err = 16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	err=8
	flinks=>saillist(sn)%flinks
	if(.not. ASSOCIATED(flinks%list)) return
	u =UBOUND(flinks%list,1)
	L  =LBOUND(flinks%list,1)
	c = flinks%count
	err=0
END SUBROUTINE  ShowFlinkCount
END MODULE flinkList_f 



SUBROUTINE  removeAllFlinks(sn,err)
use ftypes_f
use saillist_f

IMPLICIT NONE
	INTEGER , intent(in) :: sn
	INTEGER , intent(out) :: err

	integer SomeErrors
	TYPE ( flinklist) ,POINTER :: links
	err = 16
	SomeErrors=0
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

    links=>saillist(sn)%flinks
	if(associated(links%list)) deallocate(links%list,stat=err)
	if(SomeErrors /=0) then
		write(outputunit,*) 'model',sn,' removeFortranlinks (from remove ALL) Nerr=',SomeErrors
	endif
END SUBROUTINE removeAllFlinks
