! 10/1/96 Shear_Correction flag  /LS
! Many of these flags are never used

SUBROUTINE FlagSetOld()  ! dead version. see module rx_control_flags_f
	use tanglobals_f
	use fglobals_f
	use rx_control_flags_f
	IMPLICIT NONE

      CALL CHECK_CL('MT',Mass_By_Test)

      CALL CHECK_CL('CL', LinkCompAllowedFlag)
      LinkCompAllowedFlag = .not. LinkCompAllowedFlag

      CALL CHECK_CL('DM',dm)
      CALL CHECK_CL('CC',StringCompAllowedFlag)
      StringCompAllowedFlag= .not. StringCompAllowedFlag
      
      CALL CHECK_CL('ND',New_DD) ! never used
      CALL CHECK_CL('HC',HardCopy)
      
      K_on_Wrinkled_Matrix=.false.    
      CALL CHECK_CL('D1',Debug1)
      CALL CHECK_CL('D2',Debug2)
      IF(Debug2) THEN
          CALL CHECK_CL('WK',K_on_Wrinkled_Matrix)
      ENDIF      

      CALL CHECK_CL('NL',Force_Linear)
      Force_Linear = .NOT. Force_Linear
      CALL CHECK_CL('FI', Use_DSTF_Old)	
!      CALL CHECK_CL('PS', gPrestress)	  g_prestress is controlled by the 'prestress' script command
      CALL CHECK_CL('S6', form_2006)     
      CALL CHECK_CL('BS' ,bumpersticker)
      CALL CHECK_CL('CV' ,EdgeCurveCorr) 


      CALL CHECK_CL('DT' ,dofotrace )
      CALL CHECK_CL('LV' ,LimitV )          

  ! the stroona flags   
    call CHECK_CL( 'BO',G_Bowing)  ! dangerous  ! the stroona flags
    call CHECK_CL( 'BS',G_BowShear) ! need re-checking! the stroona flags
    call CHECK_CL( 'NG',geomatrix); geomatrix = .not. geomatrix  ! the stroona flags
    call CHECK_CL( 'XR',g_dxdr)
    call CHECK_CL( 'NQ',g_no_Qmatrix)
 END SUBROUTINE FlagSetOld

