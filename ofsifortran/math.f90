
	 MODULE math_f
	 use  mathconstants_f
         use basictypes_f
         USE, INTRINSIC :: ISO_C_BINDING
	 IMPLICIT NONE
	 CONTAINS
!   date: Saturday  8/1/1994
!*   translated from LAHEY FFTOSTD output
!*
!* date:  Wednesday 8 December 1993
!* Eigenvectors of a symmetric 3x3
!*
!* date:  Tuesday 14 September 1993  Kdel moved from DDNEW.F
!
!* date  Tues 25 Aug 1992 'include's now ok with f77l3 ver 5.0
!*       FILE: math.f
!* date:  Thursday 3 January 1991
!* Zero_Matrix added
!
!*       DATE:  Wednesday 24 May 1989
!
!*   now includes SSPLINE.F  via an INCLUDE statement
!* ktchanged 25/08/92 include filename
!
!* AND axismatriX

subroutine MinMaxEigen(m,p_admas,emin,emax,err)

use basictypes_f
use lapack95 ! ifort 2015
    implicit none
    real (kind=double), intent(inout) , dimension(:,:) :: m
    real (kind=double), intent(in) ::  p_admas
    real (kind=double), intent(out)  :: emin,emax
    integer, intent(out) :: err

    real (kind=double),allocatable , dimension(:,:) :: a
    real (kind=double),allocatable , dimension(:) :: w
    real (kind=double)::aa,bb
    integer nr,nc,i
    nr = ubound(m,1); nc = ubound (m,2); if(nr /=nc) then; err=32; return; endif
    if(nr .eq. 0) then; err=0; return; endif
    allocate(a(nr,nc))
    allocate(w(nr))
    emax=0; emin=0
    err=1

! 1) make the matrix symmetric.
        a = ( m + Transpose(m))/2.0; m=a
! 2) eigenvalues
    call syevr(a, w ,info=err) ! [,uplo] [,z] [,vl] [,vu] [,il] [,iu] [,m] [,isuppz] [,abstol] [,info])
!3) adjust the matrix so min V >= max V
    if(err .eq. 0) then

        emin = w(1); emax = w(nr)
        if( (emin .lt. emax * p_admas)  .AND. (emin .ne. emax)) THEN
              bb = (p_admas*emax - emin)/(p_admas-1.0)
              aa = emax* (p_admas-1.0)/(emin-emax)
              DO I=1,nr
                  M(i,:) = aa*M(i,:)
                  M(i,i) = M(i,i) - bb*aa
              ENDDO
           !   write(*,"( 'Trim Mass from ',g12.3,'/' ,g12.3, ' to ',g12.3  )" ) emin,emax , p_admas
        ENDIF
        else
        write(*,*) ' syevr gave info = ',err
    endif

deallocate(w,a)

end subroutine MinMaxEigen



pure function sinXoverX(x) result(rv)
! use the fact that the ratio of coeff n+2 and coeff N in the expansion is (gamma(2+n)/gamma(4+n)
! or more simply  1/ (-n (1 + n))  
! with the zeroth term = 1, naturally.
! very cute but its 16.8 times slower on intel than sin(x)/X if we go to eps(r8)
! and  ( 14       ) if we go to epsilon(real4)
! but for full accuracy we need to go to eps(r8)/2

implicit none
	real(kind=c_double), intent(in)  ::x
	real(kind=c_double) ::rv	
    real(kind=double) :: ratio,xsq, curterm
    real(kind=4), parameter :: r4v =1
    real(kind=double), parameter :: small = epsilon(r4v);
    integer :: n
    
    rv = 1.0d00; curterm = 1.0d00;
    xsq = x*x
    do n = 2,1000,2
    ratio =  REAL( (-n *(1 + n))  ,double)
    curterm = curterm * xsq/ratio
    rv=rv+curterm
     if( abs(curterm  ) < small)   return 
    enddo

end function sinXoverX 
#ifdef NEVER
    integer::n, k  ; real(kind=double):: b,tot1,tot2; 
     integer*8 clock1,clock2,clock3;
    call mkl_get_cpu_clocks( clock1 )
   
     tot2=0.0;
    do k=1,100000   
           s = -4. 
         do n=0,400
          d = sinxoverx(s)
          s = s + 0.02
          tot2=tot2 + d;
         enddo
     enddo
 
     call mkl_get_cpu_clocks( clock2 )  
    tot1=0.0;
    do k=1,100000   
           s = -4. 
         do n=0,400
           if(abs(s)>0) then
            b = sin(s)/s
          else
            b=1.0d00
         endif
         ! write(outputunit,*) s,d , b, 1.-d/b
          s = s + 0.02
          tot1=tot1+b;
         enddo
     enddo    
    call mkl_get_cpu_clocks( clock3 )
    write(outputunit,*) ' sinxoverx ', clock2-clock1
    write(outputunit,*) ' sin(x) /x ', clock3-clock2  
    write(outputunit,*) ' ratio ', real (clock2-clock1   )/(real( clock3-clock2))
    write(outputunit,*) tot1,tot2;  
    return
#endif

#ifdef _DEBUG
function CorrectedLengthFactor(c,v1,v2) result(rv) bind(C,name='cf_curvecorrection' )
#else
pure function CorrectedLengthFactor(c,v1,v2) result(rv) bind(C,name='cf_curvecorrection' ) ! returns chord/arc given normals at ends.
 ! the 3 vectors MUST be normalized.  
#endif

    implicit none
    real(kind=c_double), intent(in),dimension(3) ::c,v1,v2
    real(kind=c_double) ::rv
!locals
    real(kind=c_double) :: g1,g2,s,d,sympart,asym,dsq
    real(kind=double) , parameter :: slim = 1.5707 ! just less than Pi/2
    real(kind=double) , parameter :: da = slim/12.;
    real(kind=double) :: z,a,b
    
 
    z = sqrt(sum(c**2)); a =sqrt( sum(v1**2)); b =sqrt( sum(v2**2))
#ifdef _DEBUG
!	
!	real(kind=4) ::sn
!	integer::n
 !   
 !    if(abs(z-1) > epsilon(a) .or. abs(a-1) > epsilon(a) .or.abs(b-1) > epsilon(a)   ) then
 !    write(outputunit,*) ' non-unit vector in CorrectedLengthFactor'
   !    if(abs(z-1) > epsilon(rv)   )write(outputunit,'(a4,4g16.9 )')'C ',(1.0d00-z), c 
  !     if( abs(a-1) > epsilon(rv)  )write(outputunit,'(a4,4g16.9 )')'V1', (1.0d00-a),v1
  !     if( abs(b-1) > epsilon(rv)   )write(outputunit,'(a4,4g16.9 )')'V2', (1.0d00-b) ,v2
  !   endif

#endif   

	    g1 =  asin(dot_product(c,v1)/Z/A)
	    g2 =  asin(dot_product(c,v2)/Z/B) ! OK if v1 and v2 are roughly in the same direction

        s = g1+g2; d = g1-g2;     
        if( abs(d) .lt. epsilon(d)) then
                sympart = 1.0d00 - (d**2)/24.00 
        else
                d=d/2.0d00;
                sympart = Sin(d)/d
        endif
 ! lets disable the asympart
    !     rv = sympart
   !      return           
            
        ! the first level for the difference is
         ! a= -0.05267308314991198*(1 - Cos(s))  
           
           ! a better guess is
            dsq=d**2
            asym=    -0.34770541000033545*(1 - Cos(s)) + (0.2575380901962432 + 0.008015124262156349*dsq)*  &
                (1 - Cos((1.0757319222511288 - 0.007803169172506824*dsq)*s))
         rv = sympart+asym

end function CorrectedLengthFactor


pure REAL*8 FUNCTION PH_ACOS(a)
      IMPLICIT NONE
      REAL*8,intent(in) :: a
 
      if(a .gt. 1.0D00) THEN
	   !  a=1.0D00
	     PH_ACOS = 0.0D00
      ELSEIF(a .lt. -1.0D00) THEN
	   !  a=-1.0D00
	     PH_ACOS = PI
      ELSE
         PH_ACOS = Dacos(a)
      ENDIF
END FUNCTION PH_ACOS



function Make_Mass_Isotropic( M) result(nchanged)
      IMPLICIT NONE
	integer :: nchanged
      REAL(kind=8),intent(inout):: M(3,3) 
!*locals

      real(kind=8):: Lhigh,LLow, lsum
      REAL(kind=8):: V(3,4)

	    nchanged = 1
      CALL EigenVectors(M,V)
      Lhigh = max(v(1,4),v(2,4),v(3,4))
      LLow  = min(v(1,4),v(2,4),v(3,4))
      Lsum = sum(v(1:3,4))

    m=0;
    m(1,1) = Lsum
    m(2,2) = Lsum
    m(3,3) = Lsum
 
END function Make_Mass_Isotropic

pure SUBROUTINE Diagonalise (M)
      IMPLICIT NONE
      REAL(kind=double), dimension(:,:), intent(inout) :: M

      INTEGER J
      REAL*8 rsum
      DO J=1,ubound(m,1)
          rsum=sum(abs(M(J,:)))
          M(J,:) = 0.0d00
          M(J,J) = rsum
      ENDDO
      END SUBROUTINE Diagonalise


pure SUBROUTINE Diagonalise3(M)
      IMPLICIT NONE
      REAL(kind=double), dimension(3,3), intent(inout) :: M

      INTEGER J,K
      REAL*8 sum
      DO J=1,3
      sum = 0.0
      DO K=1,3
      sum=sum +abs(M(J,K))
      M(J,K) = 0.0d00
      ENDDO
      M(J,J) = sum
      ENDDO
      END SUBROUTINE Diagonalise3

      REAL*8 FUNCTION Determinant(a)
      implicit none
      REAL*8 a(3,3)
      REAL*8 Sum

      Sum = a(1,1)* (a(2,2)*a(3,3)-a(3,2)*a(2,3))
      sum = sum - a(1,2) * (a(2,1)*a(3,3) - a(3,1)*a(2,3))
      sum = sum + a(1,3) * (a(2,1)*a(3,2) - a(3,1)*a(2,2))
      Determinant = sum
      END FUNCTION Determinant


FUNCTION  SCaxismatrixByPtr(START,e,y) result( A)  !    zaxis=>a(1:3,3)
	use vectors_f
	use ftypes_f
	use coordinates_f
    IMPLICIT NONE 

      type(fnode), pointer, intent(in):: START       !       The start point (new origin)
      type(fnode), pointer, intent(in):: E       !       a point through which the X axis lies
      type(fnode), pointer, intent(in):: y       ! a third point to define the XY plane

      REAL(kind=double), dimension(3,3):: a   !       The matrix we are making

      REAL*8 z ;      LOGICAL Error
 
  !   xaxis=>a(1:3,1)     ! yaxis=>a(1:3,2)  !   zaxis=>a(1:3,3)
     a(1:3,3) = get_Cartesian_X_by_Ptr  ( START )  ! just to save a call
     
  	a(1:3,2) =  get_Cartesian_X_by_Ptr ( y)  - a(1:3,3)  
	a(1:3,1) =  get_Cartesian_X_by_Ptr ( e ) - a(1:3,3)

      call normalise(a(1:3,1),z,error)
  
      CALL vprod(a(1:3,1),a(1:3,2),a(1:3,3))   ! returns Zaxis
      call normalise(a(1:3,3),z,error)

      call vprod(a(1:3,3),a(1:3,1),a(1:3,2))   ! returns Yaxis
      CALL normalise(a(1:3,2),z,error)
END function SCaxismatrixByPtr

FUNCTION  SCaxismatrix(sli,START,e,yin) result( A)
	use vectors_f
	use coordinates_f
      IMPLICIT NONE 
      INTEGER, intent(in)::sli
      INTEGER, intent(in):: START       !       The start point (new origin)
      INTEGER, intent(in):: E       !       a point throug which the X axis lies
      INTEGER, intent(in):: yin       ! a third point to defing the normal

!               NOTE Y can be -ve, meaning a LH coordinate set

      REAL*8 a(3,3)   !       The matrix we are making
!       finds the axis matrix
      INTEGER  k,y 
      REAL*8 xaxis(3),yaxis(3) ,zaxis(3)
      REAL*8 z,sign
      LOGICAL Error
        y=yin
      if(y .gt. 0) THEN
      sign = 1.0d00
      ELSE
      y = - y
      sign = -1.0d00
      ENDIF

  	yaxis= ( get_Cartesian_X ( y,sli) - get_Cartesian_X ( START,sli ) ) * sign ! maybe EWRONG
	xaxis =  get_Cartesian_X ( e ,sli) - get_Cartesian_X ( START,sli )

      call normalise(xaxis,z,error)
      do 20 k=1,3
20      a(k,1) = xaxis(k) ! a(1,k) = xaxis(k)


      CALL vprod(xaxis,yaxis,zaxis)   ! returns Zaxis

      call normalise(zaxis,z,error)

      call vprod(zaxis,xaxis,yaxis)   ! returns Yaxis
      CALL normalise(yaxis,z,error)
      do 120 k=1,3
      a(k,2) = yaxis(k)       !       a(2,k) = yaxis(k)
120     a(k,3) = zaxis(k)       !       a(3,k) = zaxis(k)

END function SCaxismatrix

SUBROUTINE testeigen
      use basictypes_f  ! for outputunit
      implicit none

      real*8 a(6),x(3),xi(3)
      real*8 M(3,3)
      data  M / 10,2,1,2,10,2,1,2,10/

      REAL*8 V(3,4)
      integer i,k

      a(1) = 10.
      a(2) = 2.
      a(3) = 1.
      a(4) = 10.
      a(5) = 2.
      a(6) = 10.
      CALL EigenVectors(M,V)

      DO I=1,3
      write(outputunit,'(3f12.4,f15.4)')(v(i,k),k=1,4)
      ENDDO

      a(1) = 1.
      a(2) = -3.
      a(3) = -6.
      a(4) = 0.

!      call quad(a,x(1),x(2),xi(2))
      call Eigen_cubic(a,x,xi(2))
      xi(1) = 0.
      xi(3) = -xi(2)

      write(outputunit,*) a
      write(outputunit,*) x
      write(outputunit,*) xi
!*      pa use
      end SUBROUTINE testeigen

! solution of eigen equation
!*
!*      3 x 3 Matrix
!*
!*
pure SUBROUTINE EigenVectors(M,out)
	use vectors_f
      IMPLICIT NONE
!* parameters
      REAL*8,intent(in) :: M(3,3)
!* returns
      REAL*8,intent(out):: out(3,4)
      real*8 a(6),x(3)

      LOGICAL error
      REAL*8 B(3,3)  ,Z, V(3)
      integer i,j,k,L


      l=0

      DO i=1,3
      DO K=i,3
      l=l+1
      A(l) = M(i,k)
      ENDDO
      ENDDO

      call eigen(a,x)

      DO I=1,3

      DO J=1,3
      DO K=1,3
      B(j,k) = M(j,k)
      ENDDO
      B(J,J) = B(J,J) - x(I)    ! B is A - lambda I
      ENDDO

      V(1) = b(1,2)*(-(b(1,3)*b(2,2)) + b(1,2)*b(2,3))
      V(2) = b(1,2)*(b(1,2)*b(1,3) - b(1,1)*b(2,3))
      V(3) = -b(1,2)**3 + b(1,1)*b(1,2)*b(2,2)

      call normalise(V,z,error)
	if(error) then
		! write(outputunit,*)'(eigenVectors) cant normalize V'
	endif
      DO K=1,3
      out(I,K) = V(K)
      ENDDO
      out(I,4) = X(I)
      ENDDO
      END SUBROUTINE EigenVectors


pure SUBROUTINE eigen (a, eig)
      IMPLICIT NONE
 !     save what on earth is is doing?

      real*8,intent(in)::  a(6)
      real*8,intent(out)::  eig(3)      
      real*8 , dimension(4) ,automatic:: c
      real*8 xi

      c(1) = 1.
      c(2) = -(a(1) + a(4) + a(6))
      c(3) = a(1)*a(4) + a(1)*a(6) + a(4)*a(6) - a(2)*a(2)
      c(3) = c(3) - a(3)*a(3) - a(5)*a(5)
      c(4) = -a(1)*a(4)*a(6) - 2.*a(2)*a(3)*a(5) + a(1)*a(5)*a(5)
      c(4) = c(4) + a(4)*a(3)*a(3) + a(6)*a(2)*a(2)
      call Eigen_cubic(c,eig,xi)

      end SUBROUTINE eigen 


!* solution of cubic equation
!*
!*      a(1)*x^3 + a(2)*x^2 + a(3)*x + a(4) = 0
!*
!*
pure subroutine Eigen_cubic (a,xr,xi)
      IMPLICIT  NONE
  !    save  what on earth????????

      real*8,intent(in)::  a(4)
      
      real*8,intent(out):: xr(3),xi

      real*8  ex,q,z,p,sarg,a2,arg,aq(3)

      real*8 p1,p2,p3

      integer*4 ipath

      ipath = 2
      ex = 1.0/3
      if(a(4)) 1006,1004,1006
1004   xr(1) = 0.
!*      write(outputunit,*) 'a4 is zero'
      go to 1034
1006   a2 = a(1) * a(1)
      q=(27.*a2*a(4) - 9.*a(1)*a(2)*a(3) + 2.*a(2)**3)/(54.*a2*a(1))
      if(q) 1010,1008,1014
1008   z=0.
      go to 1032
1010  q=-q
      ipath = 1
1014  CONTINUE
      p1 =  3.0D00*a(1)*a(3)
      p2 =  a(2) * a(2)
      p3 =  9.0D00*a2
      p=(p1 - p2)/p3 
      arg = p*p*p + q*q
      if(arg) 1016,1018,1020
1016  z = -2.*sqrt(-p)*cos(atan(sqrt(-arg)/q)/3.)
      go to 1028
1018  z=-2.* q**ex
      go to 1028
1020  sarg = sqrt(arg)
      if(p) 1022, 1024, 1026

1022  z=-(q+sarg)**ex - (q-sarg)**ex
      go to 1028
1024  z=-(2.*q)**ex
      go to 1028
1026  z=(sarg - q)**ex - (sarg+q)**ex
1028  go to (1030,1032), ipath
1030  z=-z
1032  xr(1) = (3.*a(1)*z - a(2))/(3.*a(1))
1034  aq(1) = a(1)
      aq(2) = a(2) + xr(1)*a(1)
      aq(3) = a(3) + xr(1)*aq(2)
!*       write(outputunit,*) 'quad',aq,xr(2),xr(3),xi
      call quad(aq,xr(2),xr(3),xi)
!*       write(outputunit,*) 'quad',aq,xr(2),xr(3),xi
      return
      end subroutine Eigen_cubic


!* solution of quadratic equation
!*
!*      a(1)*x^3 + a(2)*x^2 + a(3)*x + a(4) = 0
!*
!*
pure subroutine quad (a,xr1,xr2,xi)
      IMPLICIT NONE
  !    save  what on earth????????????????????

      real*8,intent(in)::  a(3) 
      real*8,intent(out)::  xr1,xr2,xi
      real*8 :: disc,x1,x2
      x1 = -a(2)/(2.*a(1))
      disc = x1*x1 - a(3)/a(1)
      if(disc) 10,20,20
10    x2 = sqrt(-disc)
      xr1 = x1
      xr2 = x1
      xi = x2
      go to 30
20    x2 = sqrt(disc)
      xr1 = x1 + x2
      xr2 = x1 - x2
      xi = 0
30    return
      end subroutine  quad

    subroutine TestFloatingPoint()
    use basictypes_f
    implicit none
    real(kind=double)a,b 
    logical err
    err=.false.
    
    b  = 1.0d00
    a = b - eps_double
    
    if(a .lt. b) then
    !  write(outputunit,*) ' a lt b  OK '
    else
       write(outputunit,*) ' a lt b NOT OK ' 
       err=.true.    
    endif

    if(a .LE. b) then
    !  write(outputunit,*) ' a LE b  OK '
    else
       write(outputunit,*) ' a LE b NOT OK ' 
       err=.true.      
    endif    

    if(a .eq. b) then
      write(outputunit,*) ' a EQ b NOT OK '
      err=.true.
!    else
!       write(outputunit,*) ' a EQ b OK ' 
    endif          
 
     if(a .GE. b) then
      write(outputunit,*) ' a GE b NOT OK '
      err=.true.
!    else
!       write(outputunit,*) ' a GE b  OK ' 
    endif  
 
      if(a .GT. b) then
      write(outputunit,*) ' a GT b NOT OK '
      err=.true.
!    else
!       write(outputunit,*) ' a GT b  OK ' 
    endif     
      
      if(err) then
      write(outputunit,*) ' eps_double is too small for this compiler '
      pause
      
      endif
    
    
    end   subroutine TestFloatingPoint



	 END MODULE math_f






