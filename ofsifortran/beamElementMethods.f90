module BeamElementMethods_F
use ftypes_f
implicit none

contains

function TransformBeam (be ,mt,sn) result(err)
use tanpure_f
use saillist_f
    implicit none
    type(beamelement),pointer,  intent(inout) :: be
	REAL (kind=double),intent(in), dimension(4,4):: mt 
	integer, intent(in) :: sn
  	INTEGER   :: err,rc
  	real(kind=double), dimension(3)  :: s,e	     !       the start point (new origin)
    real(kind=double) :: lzi;
  ! BIV is ttu(1,1:3) then
    s=0; err=0;
    e = be%ttu(1,1:3);
    CALL VtoLocal(sn ,e  ,  e)
    e  = MATMUL(TRANSPOSE(mt(1:3,1:3)), e) 
    lzi= be%zi
    e=e*lzi;
    rc =one_beam_preliminaries(be,s,e) ;
    if(rc /=BEAM_OK) err=1 

    be%zi =lzi  ! to avoid drift due to rounding  
    rc = one_initial_beamaxismatrix(be,s,e) 
    if(rc /=BEAM_OK) err=1 

end function TransformBeam

 subroutine beprint(be,u) 
    implicit none
    type(beamelement),  intent(in) :: be
    integer ,intent(in) ::u
 
 		write(u,*) be%IsUpright, be%IsBar	,be%m_Currently_Active
		write(u,*) be%L, be%n3, be%npo
		write(u,*)  be%n12

		write(u,*) 'zt,zi,tq,ti,beta1', be%zt, be%zi, be%tq,be%ti, be%beta1
		write(u,*) 'ttu',be%ttu
		write(u,*) 'emp', be%emp
 
!		write(u,*)  be%xN1,be%xN2,be%tn1,be%tn2 
		write(u,*) trim(be%m_part_name)
end subroutine beprint
end module BeamElementMethods_F
