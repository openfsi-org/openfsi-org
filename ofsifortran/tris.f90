 MODULE tris_f
use basictypes_f
use  mathconstants_f
use vectors_f
use links_pure_f
 use ftypes_f
 use saillist_f
 use coordinates_f
IMPLICIT NONE
CONTAINS
!  April 2013 a note on prestress
!  stress is always calculated by :
!    call Get_NL_Stress_From_Tri(t,t%m_eps,t%m_stress,crease,state)
!    which includes if(ps)  apply ElasticModifier
!   If we
!
!

!  20/10/95 areacalc posts model area to summary
! 
!  18/8/95  Get_G_Matrix added so DK can work with material axes  

!  13/3/95  minor speed improvements to One_Tri_Local_Force
!     date:   25 September 1994
!   
!   non-linear test frame
!  
!  
!   IMPORTANT. dd must only be ramped when a state change is found
!              during analysis
!  	    dd must NOT be ramped at the start of an analysis
!  	    This is achieved by initialising ccount to a big  number
!  	    at the beginning of an analysis
!  	    AND by making the mass set ignore ccount
!  	    which is done by calling Find DD in mass set with a dummy
!              of high value
!   
!   


!     translated from LAHEY FFTOSTD output
!  
!     date: Saturday  8/1/1994
!     translated from LAHEY FFTOSTD output
!  
!   date:  Monday 13 September 1993
!   1) ZIR. order of indices changed
!   2) Delta_K added. This has been tested but not used in anger.
!   3) a note on areas
!    For correct Hookes Law we should leave AREA as unstressed area
!    But for correct form-finding we should use CURRENT area
!    So form-finding technique requires frequent updating of
!    ZIR by copying FROZEN to TRIS.
!  
!   date:  Wednesday 18 August 1993
!   matrix G is now made by a more readable method. The previous use of
!   Cramers rule was cryptic.
!   Matrix DD is no longer required.
!  
!   date:  Monday 16 August 1993
!   a big working-over of the buckling calculation.
!   Tri material stiffness matrix is generated in find DD

!   This routine is called in one_Tri_Local_Forces and One_Tri_material_Ma
!   It differs from the age-old etheta way.

!   In that method there was an assumption that for fixed strain, the wrin
!   principal stress direction was the same as the wrinkled direction.

!   in fact the wrinkled stress direction is a function of the creasing st
!   So an iterative method is used:

!   1) get stress based on full matrix. - return if no compressive stress
!      get a first estimate of buckling direction.
!      2) Condense out the lower principal stress
!      3) get new stress from (Wrinkled D * strain
!      4) if principal stress direction has altered, iterate to 2)
!  

!   the outcome of this is a material stiffness with the following propert
!    1) Under the given strain, stiffness is zero
!     in the lower Principal Stress direction
!    2) In principal stress axes, shear stiffness is NON-Zero
!  
!   The result appears to be a stable Newton-Raphson analysis.

!   There is only one thing to watch: sometimes the iterative method above
!   to converge. This MAY be to do with isotropic stress.
!  
!   date:  Thursday 12 August 1993
!   DD,G and S now have element index LAST.

!   this allows faster operation and allows passing a single 3by3 by refer
!  

!   there had been a modelling error in the wrinkling calc when prestress
!   was present. This has been fixed
!   QUESTION
!   should buckled material matrix include a component for G_Theta?
!   Best guess is that it should
!  
!     date: Thursday  24/6/1993
!     translated to ABSOFT F90 free format

!   file: tris.f
!   date:  Monday 14 June 1993
!   These are the routines for triangles in RELAXSQ


! MUST call HookAllElements 
!       at the beginning of the analysis 
!       after any user input (for now only AKM_UPDATE
!       after meshing
! its badly placed here in tris_f because it should fill in 
! all the other pointers in the elements.
! NOTE that these pointers are a speed optimisation.
! we could equally work from the indices 



integer function HookAllElements()bind(C,name="cf_hookalltris" )
    use edgelist_f
    use fbeamList_f
        IMPLICIT NONE

        INTEGER I,sli,k,n
        TYPE ( triList) ,pointer   	:: tris 
        type(tri) , pointer ::t  
        HookAllElements=0
        if( .not. g_PleaseHookElements) return    
!        write(outputunit,*) 'executing HookAllElements ' 
        do sli=1,SailCOunt
          if( .not. is_used(sli)) cycle
          tris=>saillist(sli)%tris
          do i = 1, tris%count
              t=>tris%list(i)
              if(.not. t%used) cycle
              do k=1,3
                  n = t%edgeno(k)
                  t%m_er(k)%m_fepe=>saillist(sli)%edges%list(n)
               enddo   
              do k=1,4
                  n = t%N13(k)
                  t%nr(k)%m_fnpe=>saillist(sli)%nodes%xlist(n)
               enddo                    
          enddo
        ENDDO
        HookAllElements=HookAllEdges();
        HookAllElements=HookAllBeams();
        g_PleaseHookElements=.false.
 
end function HookAllElements

SUBROUTINE Clear_Wrinkle_Count()
        IMPLICIT NONE

        INTEGER sli
        TYPE ( triList) ,pointer   	:: tris  
             
        do sli=1,SailCOunt
          if( .not. is_used(sli)) cycle
          tris=>saillist(sli)%tris
          tris%list%ccount=100
        ENDDO
END SUBROUTINE Clear_Wrinkle_Count

function Current_Edge_Lengths(t) result(z)  ! current
      use ftypes_f
      use saillist_f
      use coordinates_f
      IMPLICIT NONE
        TYPE ( tri),pointer    	:: t  
      REAL(kind=double), dimension(3) :: z
      REAL(kind=double),  dimension(3) :: d
      INTEGER j

      if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 11'
      DO j=1,3
	      d = get_Cartesian_X_by_Ptr(t%nr(j)%m_fnpe) -   get_Cartesian_X_by_Ptr(t%nr(j+1)%m_fnpe)
	      z(j) = DSQRT(dot_product(d,d))
      ENDDO

END function Current_Edge_Lengths

SUBROUTINE make_DD_and_G(sli)!    calls check_cl('GC',UseCurrentGeom)
    IMPLICIT NONE
    integer, intent(in) ::sli

    !   locals
    INTEGER N ,ok
    TYPE ( triList),pointer :: tris
    TYPE ( tri),pointer    	:: t
    logical:: UseCurrentGeom

    tris=>saillist(sli)%tris
 
   call check_cl('GC',UseCurrentGeom)

    DO N=1,tris%count
        t=>tris%list(n)
        if(.not. t%used) cycle
        ok=  Set_G_Matrix(t,UseCurrentGeom )
    ENDDO
END SUBROUTINE make_DD_and_G 

function  LocalForces(t ) result(f)
    use coordinates_f
    use invert_f
    use mathconstants_f
    IMPLICIT NONE
    TYPE ( tri),pointer    	:: t  
     real(kind=8), dimension(6)  :: f  ! the corner forces in element local axes
!  locals
        REAL*8 ze(3), a(3),   thetaloc(3), sint,cost
        INTEGER I ,i2

      forall(i=1:3) ze(i) = t%m_er(i)%m_fepe%m_Zlink0

      A(2)=(ze(1)**2+ze(2)**2-ze(3)**2)/(2.0D00*ze(1)*ze(2))
      if (abs(A(2)) .gt. 1.0D00) a(2) = sign(1.0D00,a(2))
      A(2)=ACOS(A(2))

      A(3)=(ze(2)**2+ze(3)**2-ze(1)**2) / (2.0D00*ze(2)*ze(3))
      if (abs(A(3)) .gt. 1.0D00)  A(3) = sign(1.0d00,A(3))
      A(3)=ACOS(A(3))

      THETAloc(1)=0.0
      THETAloc(2)=THETAloc(1)+ Pi -A(2)	! doubling,so  forget the pi
      THETaloc(3)=THETAloc(1)+ 2.*Pi -A(2)-A(3)	  
     f=0;
       DO I=1,3
        i2 = i+1; if(i2>3)i2=1
        SINT=  SIN( THETAloc(I))
        COST=  COS( THETAloc(I))
        f(2*i-1) =   t%tedge(i) *Cost +  f(2*i-1) 
        f(2*i  ) =   t%tedge(i) *Sint +  f(2*i  ) 
        f(2*i2-1) =- t%tedge(i) *Cost +  f(2*i2-1) 
        f(2*i2  ) =- t%tedge(i) *Sint +  f(2*i2  )       
      ENDDO
 
END function  LocalForces 

integer function  Set_G_Matrix(t,UseCurrentGeom )
    use coordinates_f
    use invert_f
    IMPLICIT NONE
    TYPE ( tri),pointer    	:: t  
    logical, intent(in) :: UseCurrentGeom

!  locals
        REAL*8 ze(3), a(3), matrix(3,3), thetaloc(3), sint,cost
        LOGICAL error
        INTEGER I 
        real(kind=double), dimension(3) ::centroid
        Set_G_Matrix=1
     if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 10'

    if ( UseCurrentGeom) then
        ze =  Current_Edge_Lengths(t)
    else
          forall(i=1:3) ze(i) = t%m_er(i)%m_fepe%m_Zlink0
    endif
      A(2)=(ze(1)**2+ze(2)**2-ze(3)**2)/(2.0D00*ze(1)*ze(2))
      if (abs(A(2)) .gt. 1.0D00) a(2) = sign(1.0D00,a(2))
      A(2)=ACOS(A(2))

      A(3)=(ze(2)**2+ze(3)**2-ze(1)**2) / (2.0D00*ze(2)*ze(3))
      if (abs(A(3)) .gt. 1.0D00)  A(3) = sign(1.0d00,A(3))
      A(3)=ACOS(A(3))

      THETAloc(1)=t%THETAE_rad
      THETAloc(2)=THETAloc(1)-A(2)	! doubling,so  forget the pi
      THETaloc(3)=THETAloc(1)-A(2)-A(3)	 ! remove the adding of two pis

       DO I=1,3
        SINT=SIN(2.0D00 * THETAloc(I))
        COST=COS(2.0D00 * THETAloc(I))
        matrix(I,1)=ze(i) * 0.5D00 *(1.0D00+cost)
        matrix(I,2)=ze(I) * 0.5D00 *(1.0D00-cost)
        matrix(I,3)=ze(I) * 0.5D00 * sint
      ENDDO

      CALL Invert3(Matrix,t%g,error)
!  G gets strain from edgelength
 
      if (error) then
     		centroid=0.0d00
     		 if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 9'
		    DO I=1,3
			    centroid = centroid + get_Cartesian_X_By_Ptr(t%nr(i)%m_fnpe)/3.0D00  
            ENDDO
		    write(outputunit,'(a,1x,i,1x,3F12.4,a,3f14.4)')'singular G matrix (N,ze) ',t%N,  ze,'    centroid= ', centroid
        Set_G_Matrix=0
        endif
END function  Set_G_Matrix 

      SUBROUTINE AreaCalc(SLI) !t%area from zlink0
      use coordinates_f
        use cfromf_f
        use fnoderef_f
      IMPLICIT NONE
	integer, intent(in) ::sli
!  returns nothing

!  locals
        REAL*8 ZSum ,ze(3),totalArea, ThisA
        character (len=255) :: atext,v_ph,nodename
        INTEGER j,K
        TYPE ( tri),pointer    	:: t  
        TYPE ( trilist),pointer    	:: tl  
        real(kind=double), dimension(3) :: centroid
        TotalArea = 0.0D00
        if(sli .lt. 1) return
        tl=>saillist(sli)%tris
        if( g_PleaseHookElements  ) then
         write(outputunit,*) 'hook crash 8'
            
         endif
 
doj:   do j=1,tl%count
        t=>tl%list(j)
          if(.not. t%used) cycle
          if(.not. t%use_tri) cycle
             
         forall(k=1:3) ze(k) = t%m_er(k)%m_fepe%m_Zlink0          
             
         Zsum=sum(ze)/2.0d00
         ThisA=Zsum*(Zsum-ze(1))*(Zsum-ze(2))*(Zsum-ze(3))
         if(ThisA .gt. 0.0d00) THEN
                 ThisA = DSQRT(ThisA)
         ELSE
                centroid=0.0d00
                DO k=1,3
                        centroid = centroid + get_Cartesian_X_By_Ptr(t%nr(k)%m_fnpe)/3.0D00
                ENDDO
                write(outputunit,'( a,i3,a,i6,a,g11.3,a,3f14.4 )') 'Model ',sli,' Small Tri ', t%N,' AreaSQ=', ThisA," at ", centroid
                ! nodes are type(Fnodeptr), dimension(4) :: nr !! t%nr(k)%m_fnpe)
               do k=1,3
                 call PrintableNodeName(t%nr(k)%m_fnpe,nodename)
                  write(outputunit,'(  i3, "  `",   a ,"`" )') k,  trim(nodename)
                enddo
                ThisA = 0.0d00
                write(outputunit,*) '(AreaCalc) turning this tri3 OFF'
                t%use_tri=.false.
        ENDIF
        T%m_Area = ThisA 
        TotalArea = TotalArea + ThisA
       ENDDO doj
 !      write(*,*) ' Model ',sli, ' area = ', TotalArea
      if(totalarea .gt. 0.00001) THEN
            write(v_ph,'(G12.5,A4,A1)') totalarea,'     ',char(0)
            atext = 'Area'//char(0)
            k= Post_Summary_By_SLI(sli, atext,v_ph)
       endif
      END SUBROUTINE AreaCalc


!  ***********************STIFFNESS************************

!SUBROUTINE One_Tri_Global_Stiffness(t,kgt) 
!    use rx_control_flags_f
!     IMPLICIT NONE
!
!!   parameters
!    type(tri) :: t
!!  returns
!      REAL*8 Kgt(9,9) ! stiffness matrix in global coords
!	if( .not. Mass_By_Test ) then  
!     	  	CALL one_tri_global_stiffness_oona(t,kgt,GeoMatrix)
! 	else
!     	  	write(outputunit,*) 'CALL One_Tri_G_S_By_Test(N,kgt)'
!		if(t%n .eq. 2) write(outputunit,*) ' Mass by test'
!	endif
!   
!!  for FE formulation, see tris.f
!END SUBROUTINE One_Tri_Global_Stiffness

#ifdef NEVER
      SUBROUTINE One_Tri_G_S_By_Test(N,kgt)
      use coordinates_f
	use fglobals_f
      IMPLICIT NONE
       

!   parameters
      INTEGER N   ! the element number
!  returns
      REAL*8 Kgt(9,9) ! stiffness matrix in global coords
!  
!   NOTE. Uses Wrinkling_Enabled courtesy og One Tri Local Forces
!  
!  locals
      REAL*8 rold(3,3),r(3,3) ,d(3) ,sum,zlink 
      REAL*8 tedge(3)	,radd ,dx ! stress(3),
      INTEGER c,e,e1 ,node 
      INTEGER J,K, L ,M,row,col
      LOGICAL rev

!   Method
!     For each corner node
!        Measure the residuals
!        move a small amount
!        Measure again 
  
           dx = 0.000005*SQRT(TriAREA(N)) 
     	   DO C=1,3
	     node = n13(N,c)
	      DO K=1,3
	    	  	g_nodelist(Node)%x(k) = g_nodelist(Node)%x(k) - dx
		  	Rold = 0.0d00
			DO e=1,3 
			      L = edge(E,N)
	  		      D=get_Cartesian_X(L12(L,2))-get_Cartesian_X(L12(L,1))
		
			      SUM=dot_product(d,d)
			      ZLINK=DSQRT(SUM)
			      DELTA0(L)=ZLINK-ZLINK0(L)
			      dlink(1:3,L)=D/ZLINK
	          	ENDDO
			CALL One_Tri_Local_Forces(N,tedge)
			DO e = 1,3
	     			e1 = e+1
				if(e1.gt. 3) e1=e1-3
	      	    	L = edge(E,N)
				rev = (L12(L,1) .eq. N13(N,e))
				DO M=1,3
  		       		RADD=tedge(e)*dlink(M,L)
					if(rev) radd=-radd
		       			Rold(M,e)=Rold(M,e)+RADD
		      			Rold(M,e1)=Rold(M,e1)-RADD
				ENDDO
	      		ENDDO
	     
!  now with moved <node ,k>
	      
	    	g_nodelist(Node)%x(k) = g_nodelist(Node)%x(k) + 2.0D00* dx
		R= 0.0d00
		DO e=1,3 
			L = edge(E,N)
	  		D=get_Cartesian_X(L12(L,2))-get_Cartesian_X(L12(L,1))
			SUM=dot_product(d,d)
			ZLINK=DSQRT(SUM)
			DELTA0(L)=ZLINK-ZLINK0(L)
			dlink(1:3,L)=D/ZLINK
	        ENDDO

		CALL One_Tri_Local_Forces(N,tedge)
		g_nodelist(Node)%x(k) = g_nodelist(Node)%x(k) -  dx

		DO e = 1,3
			e1 = e+1
			if(e1.gt. 3) e1=e1-3
		       	L = edge(E,N)
			rev = (L12(L,1) .eq. N13(N,e))
			DO M=1,3
  		       		RADD=tedge(e)*dlink(M,L)
				if(rev) radd=-radd
		      		R(M,e)=R(M,e)+RADD
		      		R(M,e1)=R(M,e1)-RADD
			ENDDO
	      	ENDDO
	    
!  Now for <node,k> we have r and rold

    	    row = (c-1)*3 + K
	    DO J=1,3
		  DO M=1,3
		  col = (j-1) * 3 + M
		  Kgt(row,col) = (R(M,J)-Rold(M,J))/(Dx*2.0D00)
		  ENDDO
           ENDDO
	 ENDDO
	 ENDDO
!       write(outputunit,*) ' Tri ',N
!       DO k=1,9
!        write(outputunit,'(9G11.3)')(kgt(k,j),J=1,9)
!       ENDDO

      CALL Make_Symmetrical(kgt,9,9)

      END  SUBROUTINE One_Tri_G_S_By_Test
#endif ! never#

SUBROUTINE One_Tri_Global_Geo_Stiffness(t,Kg)
      use coordinates_f
      use  trigeomstf_f
      use rx_control_flags_f
      IMPLICIT NONE

!  parameters
      type(tri) :: t   ! The element 
!  returns
      REAL*8 Kg(9,9)  ! Geometric stiffness matrix (global)
#ifdef NEVER
!   locals
      integer j,k,e, e2
      type(Fnode), pointer :: n1,n2
      REAL*8  D(3),tedge(3),  Sloc(6,6)
      integer corner(0:1), b1,b2, r,c
      kg=0.0d00
      CALL  One_Tri_Local_Forces(t, tedge)

     if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 7'
ee:    DO e=1,3
              N1 => t%nr(e)%m_fnpe
              N2 => t%nr(e+1)%m_fnpe
              D = get_Cartesian_X_By_Ptr(N1) - get_Cartesian_X_By_Ptr(N2)

              CALL One_Link_Global_Stiffness(D,Tedge(e),0.0d00, sloc,1.0d00)! last arg is ZI. Can be anything because EA=0
              e2 = e+1
              if (e2 .gt. 3) e2=e2-3

              corner(0) = e-1
              corner(1) = e2-1
              DO b1=0,1
              do b2=0,1      ! block 1, block 2
                  DO  j=1,3
                  DO  k=1,3
                  r = j + 3* corner(b1)
                  c = k + 3* corner(b2)
                  KG(r,c) = Kg(r,c) + sloc(3*b1+j,3*b2+k)
                  ENDDO
                  ENDDO
              ENDDO
              ENDDO
      ENDDO ee
#else
     real(kind=double), dimension(9) :: f
     call One_Tri_Global_Forces(t,f)
     Kg =  tri3geomstiff(t,f)
#endif

END SUBROUTINE One_Tri_Global_Geo_Stiffness


SUBROUTINE One_Tri_Local_Coords(t,Tt,xl)
      use coordinates_f
      IMPLICIT NONE
   
!   parameters

      type(tri) ::  t    ! el no
      REAL(kind=double), intent(in), dimension(3,3) :: Tt  ! axis matrix
!  returns
      REAL(kind=double), intent(out), dimension(3,3) :: xl(3,3) ! element local axes
!  locals
	real*8 xg(3)
      INTEGER I,J,K


      XL=0.0d00
 if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 6'
      DO I=1,3
          xg = get_Cartesian_X_By_Ptr(t%nr(I)%m_fnpe)
          DO J=1,3 ! only needed 1 to 2
          DO K=1,3
          XL(i,j) = XL(i,j) + xg(K) * Tt(K,j) ! XI is not good
          ENDDO
          ENDDO
      ENDDO
      END SUBROUTINE One_Tri_Local_Coords


      SUBROUTINE Rotate_Material(D,A)
      IMPLICIT NONE
!   parameters
      REAL*8 D(3,3) ! a material stiffness matrix
      REAL*8 A   ! an angle in rads

!  Returns    D modified	 and stores J in rmat ready for rotate_Back
!   locals
      REAL*8 J(3,3)
#ifdef TESTROTD 
      REAL*8 Dt(3,3)
      INTEGER I,Jj,K,L     
#endif
      REAL*8 M,N, m2,n2


!   as tsai equation  6.5, 5.1,5.4
!   NOTE sense of rotation
!   positive A is counter-clockwise from material X
!   when viewed along NEGATIVE material Z

      COMMON/rmat/J

      m=cos(a)
      n=sin(a)
      m2=m*m
      n2=n*n
      J(1,1) = m2
      J(1,2) = n2
      J(1,3) = 2.0d0*m*n
      J(2,1) = n2
      J(2,2) = m2
      J(2,3) = - J(1,3) 
      J(3,1) = -m*n
      J(3,2) = -J(3,1)
      J(3,3) = m2-n2
      
      
      
   
#ifndef TESTROTD   
      D  = Matmul(MatMul(J,Transpose(d)),Transpose(J))
    return
#else
	    Dt=0.0d00
          DO i=1,3
          DO Jj=i,3
          DO K=1,3
          DO L=1,3
          Dt(i,Jj) = DT(i,Jj) + J(i,k)*D(k,l)*J(Jj,l)
          ENDDO
          ENDDO
          ENDDO
          ENDDO
       Dt(3,1) = Dt(1,3)
       Dt(3,2) = Dt(2,3)
       Dt(2,1) = Dt(1,2)          
#ifdef _DEBUG   
      D  = Matmul(MatMul(J,Transpose(d)),Transpose(J))
      if(any(abs(d - dt>1e-7)) then
          write(outputunit,*) ' Rotate_Material, existing'
          write(outputunit,*) Dt
           write(outputunit,*) 'proposed'
          write(outputunit,*) D  
      endif    
#endif      
 

        D = Dt
#endif

      END SUBROUTINE Rotate_Material

      SUBROUTINE Rotate_Material_Back(D)
      IMPLICIT NONE
!   parameters
      REAL*8 D(3,3) ! a material stiffness matrix
      
!  Returns    D modified	 
!   locals
      REAL*8 J(3,3)
      REAL*8 Dt(3,3)
      INTEGER I,Jj,K,L

!   as tsai equation  6.5, 5.1,5.4
!   NOTE sense of rotation
!   positive A is counter-clockwise from material X
!   when viewed along NEGATIVE material Z

      COMMON/rmat/J

      J(1,3) = -J(1,3)
      J(2,3) = -J(2,3) 
      J(3,1) = -J(3,1)
      J(3,2) = -J(3,2)
   
      DO i=1,3
      DO Jj=i,3
       Dt(i,jj) = 0.0d00
      DO K=1,3
      DO L=1,3
      Dt(i,Jj) = DT(i,Jj) + J(i,k)*D(k,l)*J(Jj,l)
      ENDDO
      ENDDO
      ENDDO
      ENDDO

      DO i=1,3
      DO k=i,3
      D(i,k) = Dt(I,k)
      ENDDO
      ENDDO
       D(3,1) = D(1,3)
       D(3,2) = D(2,3)
       D(2,1) = D(1,2)
      
!   restore J in case it is called again
       
      J(1,3) = -J(1,3)
      J(2,3) = -J(2,3) 
      J(3,1) = -J(3,1)
      J(3,2) = -J(3,2)


      END SUBROUTINE  Rotate_Material_Back


!  *********************************RESIDUALS*****************************

      SUBROUTINE One_Tri_Global_Forces(t,F_Global)
      use coordinates_f
      IMPLICIT NONE
!   subtract from to R(*)
     
      type(tri):: t
      REAL*8 F_Global(9) !{{x,y,z}@n1, {x,y,z}@n2, {x,y,z}@n3}

!   locals
      REAL*8  tedge(3), D(3), Length  !stress(3),
      INTEGER e,k, e1,e2
      LOGICAL error


      CALL One_Tri_Local_Forces(t,Tedge)

   if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 5'
      F_Global = 0.0D00
      DO e=1,3
      e1 = e-1
      e2 = e
      if( e2 .gt.2) e2 = 0

      D = get_Cartesian_X_by_ptr( t%nr(e+1)%m_fnpe) -  get_Cartesian_X_by_ptr( t%nr(e )%m_fnpe)

      CALL Normalise(D,length,error)
      DO K=1,3
      F_Global(k+3*e1) = F_Global(k+3*e1) - D(K)*Tedge(e)
      F_Global(k+3*e2) = F_Global(k+3*e2) + D(K)*Tedge(e)
      ENDDO
      ENDDO

      END  SUBROUTINE One_Tri_Global_Forces

SUBROUTINE One_Tri_Local_Forces(t,tedge)
      use nonlin_f
      use rx_control_flags_f
      IMPLICIT NONE
!   REQUIRES current edgelengths
      type(tri), intent(inout) :: t
      REAL(kind=double), intent(out), dimension(3) ::    tedge
      INTEGER k, state  
      REAL*8 d(3),crease

      forall(k=1:3) d(k) = t%m_er(k)%m_fepe%m_delta0
      t%m_eps = matmul(t%g,d)
      call Get_NL_Stress_From_Tri(t,t%m_eps,t%m_stress,crease,state)
      tedge = matmul(transpose(t%G),t%m_stress) * t%m_Area
   
END SUBROUTINE One_Tri_Local_Forces


SUBROUTINE CurrentAreaCalcBySLI(sli)
       use saillist_f
      IMPLICIT NONE
	integer, intent(in) ::sli
!  returns nothing

!  locals
      REAL*8 tot ,ze(3),a
    type(tri) , pointer  :: t
      INTEGER i,k
 if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 3'
      DO i=1, saillist(sli)%tris%count
        t=> saillist(sli)%tris%list(i)
        if(.not. t%used) cycle
        t%m_area=0.0
         if(.not. t%use_tri) cycle


        forall(k=1:3) ze(k) = t%m_er(k)%m_fepe%m_zlink0 +  t%m_er(k)%m_fepe%m_Delta0
    
      tot =sum(ze) *0.5d00

      a=tot*(tot-ze(1))*(tot-ze(2))*(tot-ze(3))
      if(a .gt. 0.0d00) THEN
        T%m_Area = DSQRT(a)
      ELSE
        T%m_Area = 0.0d00
      ENDIF

      ENDDO

      END SUBROUTINE CurrentAreaCalcBySLI 



      SUBROUTINE Stress_to_princ(stress,princ)
      IMPLICIT NONE
      REAL*8 stress(3),princ(3)

!   locals
      REAL*8 s1,s2,root,phi,t2

      S1=0.5D00*(stress(1)+STRESS(2))
      S2=0.5D00*(stress(1)-STRESS(2))
!   NOTE.. PHI IS TWICE ANGLE OF P S FROM AXIS
      T2=STRESS(3)**2
      ROOT=DSQRT(S2**2+T2)
      princ(1) = s1+Root
      princ(2) = s1-Root

      IF(DABS(S2).GT.1.0D-12) THEN
	      PHI=DATAN2(stress(3),S2)
      ELSE

	      PHI=SIGN(PI/2.0d00,stress(3))
                     if(dabs(stress(3)) .lt. 1.0D-12) PHI=0.0D00
      ENDIF
      princ(3)=(0.5D00*PHI)
 5       FORMAT(A,3G12.4)
      END SUBROUTINE Stress_to_princ


  function One_Tri_Stress_OP(N,sli,stress,eps,princ,p_crease, pst) result (ok) bind(C,name="cf_one_tri_stress_op" )
    USE, INTRINSIC :: ISO_C_BINDING
	use nonlin_f
	use wrinkle_m_f
	use saillist_f
	use rx_control_flags_f
        IMPLICIT NONE
!   REQUIRES current edgelengths
 
        INTEGER(C_INT),intent(in),value :: N,sli
        REAL(C_double), dimension(3), intent(out) :: stress, eps ,princ
        REAL(C_double),   intent(out) :: p_crease,pst
        integer(c_int) :: ok
        LOGICAL c
        INTEGER state,k
        REAL*8 dl(3)
        type(tri),pointer :: t
!  First we get the material properties with a strain of (1,1,0);
        ok=1
        Eps(1)=1.0 ;       Eps(2)=1.0 ;       Eps(3)=0.0

        t=>saillist(sli)%tris%list(N)
        c = gPrestress; gPrestress=.false.
        call Get_NL_Stress_From_Tri(t,eps,stress,p_crease,state ) 
        gPrestress=c

        CALL Stress_To_Princ(stress,princ)
        pst = princ(1)
        if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 2'
        !  Now get the real stress with real strain 
        forall(k=1:3) dl(k) = t%m_er(k)%m_fepe%m_delta0

        eps = matmul(t%g,dl) + t%m_e0
        call Get_NL_Stress_From_Tri(t,eps,stress,p_crease,state)
        t%m_stress=stress        
        CALL Stress_To_Princ(stress,princ)
END  function One_Tri_Stress_OP

function printTri(t,u) result(rc)
    use coordinates_f
    use invert_f
    IMPLICIT NONE
    TYPE ( tri),pointer    	:: t
	integer, intent(in) :: u
	integer rc; rc=0
1	format(/,a12,i8,3L8)
3	format(a12,3(1x,g13.4))
33	format(a12,/,3(12x,3(1x,g13.5),/ ) )
        write(u,1) ' tri no ',t%N,t%use_tri,t%used,t%creaseable
		write(u,3) 'eps   ', t%m_eps 
		write(u,3) 'tedge', t%tedge
		write(u,3) 'stress',t%m_stress

		write(u,33) 'dstf  ',t%DSTFold	! unwrinkled material matrix
		write(u,33) 'G     ',t%G    		! strain from edge lengths
		write(u,3) 'ps     ',t%m_ps
 		write(u,3) 'e0     ',t%m_E0

		write(u,3) 'theta,area,p',t%THETAE_rad , t%m_AREA, t%PREST 
		write(u,3) 'cA,Ustf,wrng',t%CurrentArea, t%Ustiff, t%wr_angle 
		write(u,*)'n13', t%N13
		write(u,*)'edgeno', t% edgeNo			
	!	type(fedgeRef), dimension(3) :: er
	!	type(Fnodeptr), dimension(4) :: nr 
	!	LOGICAL ::  starboard=.false., creaseable=.true.

	!	INTEGER ::tribuckled =0, wr_state=0
	!	INTEGER :: Ccount  =0 ! count since last state change
	!	integer(kind=cptrsize) :: CtriPtr	 =0



end  function printTri

END MODULE tris_f

