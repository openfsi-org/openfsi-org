!   date: Saturday  8/1/1994
! ^!^!  translated from LAHEY FFTOSTD output
!
!  23/10/95 more writes to try to trap failed splining
! date:  Wednesday 7 October 1992   sort_Stripe now
! more independent of coordinate system.
! search is on (X dot chord)
! however, chord is still determined as the diagonal of the
! cartesian bounding box.
! to be fully independent it shold be based on Least SQUARES
! thus it is still WRONG
!
! date  Tues 25 Aug 1992 'include's now ok with f77l3 ver 5.0
! FILE: shape.f
! date:  Monday 8 April 1991  Bounds checking added
!               NOTE this should not be necessary as Get_even_stripes ca
!n
!               handle up to NDOD points on one line
!               ALSO change the heuristic "order <=11" to "<=18 "
!               this may be slower, but there is currently a problem tra
!cking
!               shapes cleanly.
!
! DATE:  Tuesday 5 December 1989
! December 1989   added  Get_Even_Stripes  - Peter
!  But getting contours off L12 is slower than getting them off N13
!  by 3 seconds

     SUBROUTINE Sort_Stripe(XG,YG,Zg,Np)
	use vectors_f
	use fglobals_f
      IMPLICIT NONE

      REAL*8 XG(*),YG(*),ZG(*)
      INTEGER Np

!       LOCALS
      REAL*8 XX(3,20000)
      REAL*8 Xmin(3),Xmax(3), c(3)
      REAL*8 length, value(20000), Vmin 
      REAL*8 threshold,CurrentValue
      INTEGER I,J,K, N
      LOGICAL Doit, error
	 
      DO  K=1,3
   	   Xmin(K) = xg(1)
           Xmax(K) = Xmin(K)
	enddo
      DO  I=1,Np
     	 XX(1,I) = XG(I)
     	 XX(2,I) = yG(I)
     	 XX(3,I) = zG(I)

     	 DO  K=1,3
     		Xmin(K) = MIN(Xmin(K),XX(K,I))
           	Xmax(K) = MAX(Xmax(K),XX(K,I))
	enddo
	enddo
      DO K=1,3
      C(K) = Xmax(K) - Xmin(K)  ! diagonal of bounding Box
      ENDDO

      CALL Normalise(C,length,error)

      DO 50 I=1,Np
50              Value(I) = DOT_product( C, XX(1:3,I) )

!****** sort by VALUE

      threshold = 0.001 *length

      N = 0

100       CONTINUE
      Vmin = 10000000.0
      J = 0
      DO I=1,Np
      IF (Value(I) .lt. Vmin) THEN
      Vmin = Value(I)
      J = I
      ENDIF
      ENDDO

      IF (j .ne. 0) THEN
      IF ( n .GT. 0) THEN
      IF ( (Vmin - CurrentValue) .gt. threshold)  THEN
      Doit = .true.
      ELSE    ! duplicate
      Doit = .FALSE.
      ENDIF
      ELSE            ! starting... so it anyway
      Doit = .TRUE.
      ENDIF
      IF (doit) THEN
      N = N + 1
      currentValue= value(J)
      XG(N) = XX(1,J)
      YG(N) = XX(2,J)
      ZG(N) = XX(3,J)
      ENDIF
      Value(J) = 10000000.000 ! cut this one out
      GOTO 100
      ENDIF
      Np = N  ! we counted the independent points

      END subroutine Sort_Stripe 

SUBROUTINE addapointtoXG(XG,Yg,Zg, n,error)
      IMPLICIT NONE
!       checks the longest segment of the curve and adds one point to it
!
!       by linear interpolation
      REAL*8 XG(*),YG(*),ZG(*)
      INTEGER N                       ! number of points
	LOGICAL error

      REAL*4 biggest,sum
      INTEGER I,  ibig
  
      biggest = -1.0
       if(n .lt.2) then
	    	! write(outputunit,*) 'ERROR addapointtoXG with n=',n
	               error = .TRUE.
	    	return 
      endif
       error = .FALSE.
       DO 10  i=1,n-1

      	sum = sqrt( ( XG(I+1) - Xg(I) ) **2 + ( YG(I+1) - Yg(I) ) **2  + ( ZG(I+1) - Zg(I) ) **2  )

         	IF (sum .gt. biggest) THEN
     		 ibig=i
     		 biggest=sum
      	ENDIF
   10      continue

!               make a gap
      	DO i=n,ibig+1,-1
     	 	XG ( i+1 ) = XG(I)
      		yG ( i+1 ) = yG(I)
    		 zG ( i+1 ) = zG(I)
	enddo


!       interpolate

      Xg(Ibig+1) =  ( xg(ibig) + Xg(Ibig+2) ) / 2.0d00
      yg(Ibig+1) =  ( yg(ibig) + yg(Ibig+2) ) / 2.0d00
      zg(Ibig+1) =  ( zg(ibig) + zg(Ibig+2) ) / 2.0d00
      N = N + 1
      END  subroutine addapointtoXG

#ifdef NEVER
      SUBROUTINE PushToStrip(X,Y,Z,XSTRIP,Nstrip,J,Tolerance)
      IMPLICIT NONE
      REAL*4 X,Y,Z, XSTRIP(12,60,3),Tolerance
      INTEGER Nstrip(15)
      INTEGER J,K,L,M
      LOGICAL LastPoint
      INTEGER INDEX1, INDEX2,INDEX3
      COMMON/shapeway/index1,index2,index3

!       Tolerance= 0.03 ! old value, now is non-dimensionalised

      LastPoint= .TRUE.

      do 20 M= 1,nstrip(J)

      if(abs(XSTRIP(j,M,INDEX1)- X) .LT. Tolerance)  THEN
      if(ABS (XSTRIP(j,M,INDEX2)- Y).LT. Tolerance) THEN ! duplicate p
      RETURN
      ENDIF
      ENDIF
      if(XSTRIP(j,M,INDEX1) .GT. X) THEN
!       PUSH the point in just BEFORE M and return
      LastPoint= .FALSE.

      GOTO 50
      ENDIF
20     CONTINUE
!
50     CONTINUE !    Here  M = Nstrip+1 if new point is the MAX point

      IF ( Nstrip(j) .lt. 50) THEN  ! Room left in array

!        Make a gap at index M
      do 30 l=Nstrip(J),M,-1
      DO 30 K=1,3 ! 3 value always the same
30        XSTRIP(J,L+1,K) = XSTRIP(J,L,K)
      Nstrip(J)=Nstrip(J)+1

      Nstrip(j)=max(nstrip(j),M)

      ELSE ! no Room left

      IF (Lastpoint) THEN
      M=50
      ELSE
      RETURN
      ENDIF
      ENDIF

      XSTRIP(J,M,INDEX1)= X
      XSTRIP(J,M,INDEX2)= Y
      XSTRIP(J,M,INDEX3)= Z

      END ! of PushToXSTRIP

  
! PURPOSE: to provide a way of extracting stripes of indefinite length
!               from  a SAILCOM and splining these down to a short numbe
!r of points
!               ( but length is limited by BSPCOM.F)

!      SUBROUTINE TEST_of_Get_Even
! was the main program for testing
!      INTEGER I,J,K
!      REAL*4 XSTRIP(12,60,3)    ! the points

!      INTEGER NumOfStripes/11/,NumOnStripe/17/

!      CALL READSAIL
!
!!      CALl getKBinteger('NumOfStripes',NumOfStripes)
!
!      CALl getKBinteger ('NumOnStripe',NumOnStripe)
!
!      CALL Get_Even_Stripes (XSTRIP ,NumOfStripes,NumOnStripe)
!
!      DO 10 i=1,NumOfStripes
!      write(outputunit,*)
!
!      DO 10 J=1,NumOnStripe
!10      write(outputunit,30) (Xstrip(i,j,k),k=1,3)
!
!30      FORMAT(3f14.4)
!
!      END ! of test module

      SUBROUTINE Get_Even_Stripes (Model,XSTRIP ,NumOfStripes,NumOnStripe)
	use fglobals_F


      IMPLICIT NONE
	integer, intent(in) ::model
!  THIS Routine  MAKES THE ARRAY XSTRIP
!
! arguments
      INTEGER NumOfStripes
! the number of contours required
      INTEGER NumOnStripe
! how many points to return for each stripe

! returns
      REAL*4 XSTRIP(12,60,3)   ! the points

#ifndef NEVER
 	write(outputunit,*) ' NO Get_Even_Stripes'
#else

! ktchanged 25/08/92 include 'filename'

      REAL*8 C(50), SS, P(3), ArcLength
      REAL*8 xg(NDOD),yg(NDOD),Zg(NDOD)

      Integer I ,K, Np, Order, Sn, Fair ,nl1,nl2 ,Is,count
      LOGICAL Failed,error

      nl1 = es(Model)
      nl2 = ee(Model)
       

      CALL Set_Contour_Heights(Model,C,NumOfStripes)
!      CALL GetInteger('Fairing',fair)
      fair = 2
     
      DO Is = 1,NumOfStripes

        count=0
      CALL Get_One_stripe (LINKDIM,L12,nl1,nl2, C(Is), xg,yg,zg,np)
! raw data, in order

      CALL Sort_Stripe(XG,YG,Zg,Np) ! ALSO REMOVES DUPLICATES
! this code borrowed from SHAPE.F

25      IF (np .lt. 4) THEN
      	CALL addapointtoxg  ( xg,yg,zg,np,error)
      	 if(error) then
		! write(outputunit,*) ' add a pt error in shape.f(1)'
	else
  	    GOTO 25
  	endif
          ENDIF

	Order = MIN ( Np/2 - fair , Np ,11)
             Order = MAX(4,order)

      Sn = 1
      CALL ThreeD_Spline_init(xg,yg,zg,np,order,sn,arclength,failed)

     	 IF (.not. Failed) THEN
            		 do i=1,NumOnStripe
      			SS = FLOAT(i-1) * arclength / FLOAT(NumOnStripe-1)
      			CALL ThreeD_Spline_Eval(ss,p,sn)
      			DO K = 1,3
                              		Xstrip(Is,I,K) = P(K)
			enddo
       		enddo

      	ELSE            ! spline initialisation failed so add a pnt by linear interp
            		if(np .gt. 149) THEN
               			Order = MAX (4,order/2) 
!                            	write(outputunit,*) ' dropping ORDER '
!				write(outputunit,*)'model=',model, ' np=',np,' order=',order
            		ELSE 
                     		CALL addapointToXG  ( xg,yg,zg,np,error)
				! if(error) write(outputunit,*) ' add a pt error shape.f(2)'
            		ENDIF
             		 count=count + 1 
             		if(count .lt. 50) GOTO 25    ! * try again
!C default X.
          		 write(outputunit,*)' stripe ',Is,' not found '
         		 do  i=1,NumOnStripe
                		Xstrip(Is,I,2) = 0.0D00
                		Xstrip(Is,I,3) = 0.0D00
                	 	Xstrip(Is,I,1) = DBLE(i)/DBLE(NumOnStripe)
      		 enddo
     	ENDIF

        enddo
#endif
      END subroutine Get_Even_Stripes

 

      SUBROUTINE Set_Contour_Heights(Model,C,NumOfStripes)
      use coordinates_f
      IMPLICIT NONE
	integer, intent(in) ::model
      REAL*8 C(*)
      INTEGER NumOfStripes

#ifndef NEVER
write(outputunit,*) 'NO  Set_Contour_Height '
#else  
! ktchanged 25/08/92 include 'filename'
      include 'offsets.f'
!      include 'nodecom.f'
      include 'tricom.f'
    
      INTEGER K, N
      REAL*8 Zmax(3),Zmin(3),space
      Zmax = -1000000.0
      Zmin= 1000000.00

      DO  N=nts(Model),Nte(Model)
         if(use_tri(N) .eq. 1) then
      		DO  K=1,3
      			Zmax = max(Zmax,get_Cartesian_X(N13(N,K)) )
      			Zmin = min(Zmin,get_Cartesian_X(N13(N,K)) )
 		enddo
      	endif
	enddo

      Space = (Zmax(3) - Zmin(3)) / float(NumOfStripes + 1)

      DO 20 N=1,NumOfStripes
      C(N) = Zmin(3) + float(N) * Space
 20   continue
#endif
      END SUBROUTINE Set_Contour_Heights

      SUBROUTINE  Get_One_stripe (LINKDIM,L12,nl1,nl2,H , xg,yg,zg,np)
	use coordinates_f
      IMPLICIT NONE
! raw data, in order

! INCLUDES
! ktchanged 25/08/92 include 'filename'
!      include 'nodecom.f'

! parameters

      INTEGER LINKDIM
      INTEGER L12(LinkDIm,2)
      INTEGER Nl1,nl2         ! range of tri-edges
      REAL*8 H                                ! contour height
! returns

      REAL*8 XG(*),yg(*),Zg(*)       ! the points
      INTEGER NP                              ! number of points


! LOCALS
      INTEGER L,l1,l2
      REAL*8 Za,Zb, Factor
  !    character*128 string
      real(kind=double), dimension(3) :: x1,x2

      Np = 0
	write(outputunit,*) ' dodgy Get_One_stripe'
      lloop : DO L=nl1,Nl2
      L1 = l12(L,1)  
      L2 = L12(L,2)  

      if_1 : if( l1 .gt. 0  .and.  l2 .gt. 0) then
            x1 = 0  !get_Cartesian_X(L1)
            x2 =  1 ! get_Cartesian_X(L2)
            Za = X1(3)
            Zb = X2(3)

      	if_2 : IF((( za .ge.H) .and.(Zb .le. H)).OR. (( zb .ge. H) .and. (Za .le.H)))THEN ! inside

   		   IF (Zb .eq.Za) THEN
     			 Factor = 0.0d00 ! doesnt matter
     		 ELSE
   			   Factor = (H-Za)/ (ZB-Za)
     		 ENDIF

!   		   IF( NP .lt. ubound(xg,1)) THEN
    			  Np = Np + 1
!     		 ELSE
 !    	 write(string,'(a,i5,a,i5,a)')'Too many points in stripe (',Np,' of
!     &	 ',np,') - call Peter'
 !    	 CALL PC_Post_Message(string//char(0))
!      ENDIF

      XG(NP) = x2(1) * Factor + X1(1) * ( 1.d0 - Factor)
      YG(NP) = x2(2) * Factor + X1(2) * ( 1.d0 - Factor)
      ZG(NP) =  H




      ENDIF if_2
!	else  if_1
!		write(outputunit,*) ' shape.f. link ',L,' l1 or l2 OOR ' , l1,l2
      	ENDIF if_1
      ENDDO lloop 
END SUBROUTINE  Get_One_stripe

#endif




