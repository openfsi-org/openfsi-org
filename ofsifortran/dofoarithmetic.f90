!  dofodev.f90
!
!  FUNCTIONS:
!  dofodev - Entry point of console application.
!
!****************************************************************************
!
! Open questions and points to watch
!We assume that the rank of W is the same as the rank of A
!when (u,w,v)=SVD(A)
!We dont have a proof for this.
!
!CARE! the mathematica SVD returns (u,w,v) and the lapack SVD returns
!(u,w,vT).  IN this implementation we transpose Vt immediately after SVD
!to get to the Mathematica notation.   
!
!****************************************************************************
module dbg
      logical :: dbgprnt  
contains
subroutine mprintd( s,x)
    USE basictypes_f
    implicit none
    character(len=*),intent(in) :: s
    real(kind=double), intent(in), dimension(:,:):: x
    real(kind=double) :: mx
    character(len=64) :: buf
    integer ::m,n,i;
    if (.not. dbgprnt) return
    mx=maxval(abs(x))
    m = ubound(x,1); n = ubound(x,2)
    if(n.gt. 32) then
        write(buf,*)'(a1,i2.2,',n,'(1x,g9.2))'
    else
        write(buf,*)'(a1,i2.2,',n,'(1x,f9.5))'   
    endif
    write(outputunit,'(/a,a,i4,a,i4)') trim(s) ,' ',m,' by ',n
    do i=1,m
    write(outputunit,buf)'r',i, x(i,1:n);
    enddo
    if(m>0 .and. n>0) write(outputunit,*) 'max is ' ,mx
end subroutine mprintd
subroutine mprint( s,x,unit)
      use basictypes_f  ! for outputunit
    USE f95_PRECISION, ONLY: WP => DP
    implicit none
    character(len=*),intent(in) :: s
    real(wp), intent(in), dimension(:,:):: x
    integer, intent(in) ::unit
    real(wp) :: mx
    character(len=64) :: buf
    integer ::m,n,i,e,wid,p
    if (.not. dbgprnt) return
    mx=maxval(abs(x))    
    m = ubound(x,1); n = ubound(x,2)
    if(.true.) then  ! mathematica version
        write(buf,*)'("{ ",g15.9,',n-1,'(",",g15.9)," }")'

        write(unit,'("(* mprint ",i4," by ",i4," *) ",a,"= {" )') m ,n, trim(s) 
        if( m .eq. 0 .or. n .eq. 0) then; write(unit,'(" }")'); return; endif
        do i=1,m
        if(i .eq.1)then; write(unit,'(" ",$)');
        else;  write(unit,'(",",$)'); endif
        write(unit,buf,iostat=e)  x(i,1:n)
        enddo 
        write(unit,'("};")')
    else
    if(n.gt. 32) then
        write(buf,*)'(a1,i2.2,',n,'(1x,g9.2))'
    else
        wid = 14;   if(n>0) wid = min(16,220/n);  p = max(3,wid-6)
        write(buf,*)'(a1,i2.2,',n,'(1x,g',wid,'.',p,'))'   
    endif
    write(unit,'(/a,a,i4,a,i4)') trim(s) ,' ',m,' by ',n
    do i=1,m
    write(unit,buf,iostat=e)'r',i, x(i,1:n);
    enddo
    endif
     if(m>0 .and. n>0)  then
        write(unit,*) '(*Max is ' ,mx, '*)'

     endif
end subroutine mprint

 subroutine  PrintOneMatrixForMtka(s, X,pu )
 USE f95_PRECISION, ONLY: WP => DP
 implicit none
 character(len=*),intent(in) :: s
 real(wp), intent(in) ,dimension(:,:) ::x
     integer,intent(in),optional ::pu
     integer ::k, nr,nc,u
     character (len=256) :: fmt

    if(present(pu)) then
        u=pu
    else
        u = 301
        fmt = trim(s)//".txt"
        open(file=fmt,unit=u, status="unknown")
     endif
     nr= ubound(X,1); nc= ubound(X,2);
    if(nr>0 .and. nc>0   ) then
          write(fmt,'("( ""{"",  ",i4,"( f25.15 ,"","") ,f25.15 ,""}"",\ )" )' )nc-1
 !         write(u,*) "(* dims ",nr," by ",nc,'*)'
 !        write(u,'( a,"={" )') trim(s)

            write(u, "( '(* dims  ',i6,' by ',i6,'*)',  '(* ' a, '=*){ ' )") nr,nc,trim(s)
            do k=1,nr
              write(u,fmt)  X (k,:)
               if(k<nr) write(u,*)','
              enddo
          write(u,'( TL2,a)') '}  '
      endif
      if(.not. present(pu)) close(u)

 end subroutine PrintOneMatrixForMtka

 subroutine  PrintOnevectorForMtka(s, X,pu )
 USE f95_PRECISION, ONLY: WP => DP
 implicit none
 character(len=*),intent(in) :: s
 real(wp), intent(in) ,dimension(:) ::x
     integer,intent(in),optional ::pu
     integer ::k, nr,u
     character (len=256) :: fmt
     if(present(pu)) then
         u=pu
     else
         u = 301
         fmt = trim(s)//".txt"
         open(file=fmt,unit=u, status="unknown")
      endif
    nr= ubound(X,1);
    if(nr>0  ) then
    fmt =' (F25.17,1x,\ )'
    write(u, "( '(* dims  ',i6,'*)',  '(* ' a, '=*){ ' )") nr,trim(s)

            do k=1,nr
              write(u,fmt)  X (k)
               if(k<nr) write(u,*)','
              enddo
          write(u,'( TL2,a)') '}'
      endif
     if(.not. present(pu)) close(u)
 end subroutine PrintOneVectorForMtka


 subroutine vprintd( s,x,punit)
    USE basictypes_f
    implicit none
    character(len=*),intent(in) :: s
    real(kind=double), intent(in), dimension(:):: x
    integer, intent(in) ::punit
    character(len=64) :: buf
    integer ::i, e, m,unit
        if (.not. dbgprnt) return
        unit=punit
 ! mathematica
 m = ubound(x,1)
         write(unit,'("(* vprintd ",i4," *) ",a,"= {" )')  ,m, trim(s) 
        
        do i=1,m
            if(i .eq.1) then ;write(unit,'("{",$)') 
            else;     write(unit,"(1h, ,$)") 
            endif
            write(unit,'(g15.8)',iostat=e)  x(i )
        enddo 
        write(unit,'("};")')
 
 return       
 
    write(buf,*)'(',1,'(1x,f13.6))'
    write(outputunit,*) trim(s) 
    write(outputunit,buf) x
end   subroutine vprintd  
subroutine vprint( s,x,punit)
      use basictypes_f  ! for outputunit
     USE F95_PRECISION, ONLY: WP => DP
    implicit none
    character(len=*),intent(in) :: s
    real(WP) , intent(in), dimension(:):: x
    integer, intent(in) ::punit
    character(len=64) :: buf
   integer ::i, e, m,unit
        if (.not. dbgprnt) return
        unit=punit
 ! mathematica
 m = ubound(x,1)
        write(unit,'("(* vprint ",i4," *) ",a,"= " )')  ,m, trim(s) 
        if(m .eq.0) then; write(unit,'("{}" )'); return; endif
        do i=1,m
        if(i .eq.1)then; write(unit,'("{",$)');
        else;        write(unit,'(",",$)'); endif
        write(unit,'(g16.9)',iostat=e)  x(i )
        enddo 
        write(unit,'("};")')
 return        
        
    write(buf,*)'(',1,'(1x,f13.6))'
    write(outputunit,*) trim(s) 
    write(outputunit,buf) x
end subroutine vprint

subroutine ivprint( s,x,punit)
      use basictypes_f  ! for outputunit
    implicit none
    character(len=*),intent(in) :: s
    integer, intent(in), dimension(:):: x
    integer, intent(in) ::punit
    character(len=64) :: buf
   integer ::i, e, m,unit
   if (.not. dbgprnt) return
   unit=punit
 ! mathematica
 m = ubound(x,1)
        write(unit,'("(* ivprint ",i4," *) ",a,"= " )')  ,m, trim(s) 
        do i=1,m
        if(i .eq.1)then; write(unit,'("{",$)');
        else;        write(unit,'(",",$)'); endif
        write(unit,'(i8)',iostat=e)  x(i )
        enddo 
        write(unit,'("};")')
  return        

    write(buf,*)'(',1,'(1x,i8))'
    write(outputunit,*) trim(s) 
    write(outputunit,buf) x
end subroutine ivprint
end module dbg  

module dofomath 
      use basictypes_f
      use  dbg  
      USE f95_PRECISION, ONLY: WP => DP
     
contains
   
function diagonalMatrix(w) result (a)
    implicit none    
     real(WP), intent(in), dimension(:):: w
 
      real, dimension(ubound(w,1),ubound(w,1)):: a
      integer  ::i,n; n = ubound(w,1)    
  
      a=0.
      forall(i=1:n)
      a(i,i)=w(i)
      end forall
end   function diagonalMatrix
#ifdef NEVer
! FitMatrix returns the LSQ matrix for matrix J
function FitMatrix(JNew) result (FitMat)
!  .. "Use Statements" .
 !   use f95_precision
       USE mkl95_LAPACK, ONLY: gesvd
!  .. "Implicit Statement" ..
     IMPLICIT NONE
!  .. "arguments" ..

    real(WP), intent(in),  dimension(:,:):: Jnew 
!  .. "return value" ..         
         real(kind=double), dimension (ubound(Jnew,2),Ubound(Jnew,1)) :: FitMat
!  .. "Local Scalars" ..
        integer i
       ! real(wp) ::residual
        integer ::m ,n,minMN   !,nr1,nr2
        integer :: rw  ,info=0
!  .. "Local Arrays" ..! m rows, n cols          
        real(wp), dimension (:,:),allocatable  :: A 
        real(wp), dimension(:)   ,allocatable  :: S
        real(wp), dimension (:,:),allocatable::  U2
        real(wp), dimension (:,:),allocatable :: V2  
        real(wp), dimension (:,:),allocatable :: winv  
  
        m = ubound(Jnew,1); n=ubound(Jnew,2);    minMN=min(m,n)
        allocate( A(m,n) )
        allocate( S(minMN) ) 
        allocate( U2(m,m) ) 
        allocate( V2 (n,n) )  
        A= Jnew;
        write(outputunit,*) 'FitMat has bizarre logic ', 1/info
        call gesvd(a, s ,u2 ,v2,info=info) 
        if(info .ne.0) write(outputunit,*) ' fitmat failure'
    ; write(*,*) 'f singular values bnds=',ubound(s) ; write(*,*) s
        v2=transpose(v2)  
      !  call vprint("S=",s);
        rw = count(s .gt. 1.0E-6) ;  
! let's keep winv at (n,n) and only populate the diaginal with 1/(non-zero    S)    
        do i=1,n;if(s(i) > 1.0e-6) then;
        s(i) = 1./s(i);
        else;s(i)=0.0;
        endif;
        enddo
        
        allocate( winv(n,n) )  
        winv = diagonalmatrix(s); 
      !  call mprint(' u   ', u2)  
     !   call mprint('winv   ', winv)      
      !  call mprint(' V2   ', v2)   
               
    FitMat =  matmul(matmul(v2,Transpose(winv)) ,  transpose(u2(:,1 : rw)))  
 
 ! tidy
        deallocate( A,S,U2,V2,winv )
end function FitMatrix
#endif

 ! MakeCommonRows counts the number of common values in vectors r1 and r2 
 !  r1 and r2 are indices into the result
 ! c1, c2 are indices into R1 and R2.
function  MakeCommonRows(r1,r2,c1,c2) result (nc)
!  .. "Use Statements" ..

!  .. "Implicit Statement" ..
     IMPLICIT NONE
!  .. "arguments" ..
        integer,           intent(in), dimension(:)  :: r1, r2  
         integer,          intent(out), dimension(:)  ::c1, c2  
 !  .. "Local Scalars" ..
        integer i,j,nc    
        nc=0;  
        c1=0; c2=0;
        do i=1,ubound(r1,1)
            do j=1,ubound(r2,1)
                if(r2(j) .eq. r1(i)) then
                nc=nc+1
                c1(nc) = i
                c2(nc)=j
                endif
            enddo
        enddo  
                
end function  MakeCommonRows

function  ReducedJmatrix1( J1,J2,p_r1,p_r2,O1,O2,t1,t2,Jnew, Onew, tnew,sv1,sv2,svOrg1,svOrg2,d1,d2) result(err)! the safe one
!  .. "Use Statements" ..
      USE LAPACK95, ONLY: gesvd
      use basictypes_f
      use vectors_f
      use  dbg
      use ftypes_f
      use dofoprototypes_f
! t1 should be sv1.tnew t2 sb sv2.tnew at the end of this.
!  .. "Implicit Statement" ..
     IMPLICIT NONE
!  .. "arguments" ..
        real(kind=double), intent(in)   ,dimension(:,:):: J1, J2
        real(kind=double), intent(in)   ,dimension(:)  :: O1 ,O2
        real(kind=double), intent(in)   ,dimension(:)  :: t1 ,t2
        integer,           intent(in)   ,dimension(:)  :: p_r1, p_r2  
! the values(input) in r1 and r2 are the rows of the result matrix (Jnew)
! which correspond to the rows of the input matrices J1 and J2
! We will equate the rows of J1 with the corresponding row
! of J2 where their corresponding R1 and R2 are equal.
!Otherwise we'd need this procedure to lay out the new matrix rows
!
!  sv1 lets us recover the tees on dofo1 via tees1 = sv1 dot tees3  + svOrg1
!  sv2 lets us recover the tees on dofo2 via tees2 = sv2 dot tees3  + svOrg2
! for the moment we can't allow a residual because then we'd also need to transmit a 'delta-Origin' for each parent
 
        real(wp), intent(out), allocatable, dimension(:,:):: Jnew ,sv1,sv2    ! allocated in this procedure   
        real(wp), intent(out), allocatable, dimension(:)::  svOrg1,svOrg2    ! allocated in this procedure  
        real(kind=double), intent(out), allocatable, dimension(:):: Onew,tnew  ! must be of ample size (bigger than M or N)

#ifdef NO_DOFOCLASS
       type (dofo),intent(in) ::d1,d2   ! or class
#else
       class (dofo),intent(in)::d1,d2  ! or type
#endif
!  .. "Local Scalars" ..
        integer err
        real(wp) ::residual !, xx
        integer ::m ,n,minMN,nc1,nc2, nc ,nrout,nr1,nr2
        integer :: rw  ,info=0,i,r 
!  .. "Local Arrays" ..! m rows, n cols          
        real(wp), dimension (:,:),allocatable  :: A 
  
        real(wp), dimension(:)   ,allocatable  :: S
        real(wp), dimension (:,:),allocatable:: U
        real(wp), dimension (:)  ,allocatable  :: WW  ! not necessarily sorted
        real(wp), dimension (:,:),allocatable :: j1out,j2out 
        real(wp), dimension (:,:),allocatable :: V  
        real(wp), dimension (:,:),allocatable :: sv  ,wd,JNewCopy
        real(wp), dimension (: ),allocatable :: X1,X2  ,orInTspace   ,bbb,Xnew
        integer, dimension (:), allocatable :: c1,c2;
        logical :: svbad   
        allocate(c1(min(ubound(p_r1,1),ubound(p_r2,1))));allocate(c2(min(ubound(p_r1,1),ubound(p_r2,1))))
        nc= MakeCommonRows(p_r1,p_r2,c1,c2)  ! number of common rows
#ifdef NEVerDOFO_COMBO_CHECKING
        write(221,*) '(*Reduced Jmatrix1 )'
        if( ubound(t1,1 ) .eq. 0 .or.  ubound(t2 ,1).eq. 0   ) then     
            write(221,*) '(*Reduced Jmatrix1 with small bounds Remember to change E to *10^   *)'
        endif
        dbgprnt = .true.
        call mprint("J1 ",J1, 221)
        call mprint("J2 ",J2, 221)
        call vprint("O1 ",O1, 221)
        call vprint("O2 ",O2, 221)
        call vprint("T1 ",t1, 221)
        call vprint("T2 ",t2, 221)
        call ivprint("R1 ",p_r1, 221)
        call ivprint("R2 ",p_r2, 221)
        call ivprint("C1 ",c1(:nc), 221)
        call ivprint("C2 ",c2(:nc), 221)

!        write(outputunit,*) ' O1 bounds',ubound(O1)
!        write(outputunit,*) ' O2 bounds',ubound(O2)
!        write(outputunit,*) ' t1 bounds',ubound(t1 )
!        write(outputunit,*) ' t2 bounds',ubound(t2 )
!        write(outputunit,*) 'p_r1 bounds',ubound(p_r1 )
!        write(outputunit,*) 'p_r2 bounds',ubound(p_r2 )
!        if( ubound(t1,1 ) .eq. 0 ) then
!         err=  ShrunkJmatrix( J2,p_r1,p_r2,O2,t2,Jnew, Onew, tnew,sv2,svOrg2,d1,d2)
!         else  if( ubound(t2,1 ) .eq. 0 ) then
!         err=  ShrunkJmatrix( J1,p_r2,p_r1,O1,t1,Jnew, Onew, tnew,sv1,svOrg1,d2,d1)
!         endif
!         return

#endif
       err=0         
! first: identify the common rows
! c1, c2 are indices into J1 and J2. r1 and r2 are indices into the result
! checking
        if(count(c1>0) .ne. count(c2>0))  err=16
        if(any(c1 >ubound(J1,1))) err=err+8
        if(any(c2 >ubound(J2,1))) err=err+4
        if(count(c1>0) .ne. count(c2>0)) err=err+2

        if(err .gt.0) then
            deallocate(c1,c2)
            return
        endif
        err=1;
 
 !end insert 13 june 2011
        m = count(c1>0)
        nc1 = ubound(J1,2) ; nc2 = ubound(J2,2)
        nr1 = ubound(J1,1) ; nr2 = ubound(J2,1)
        n = nc1 + nc2 
        minMN=min(M,n)   
        if(m .eq.0) then ! no rows in common. J is just a diagonal matrix comprising J1 and J2.
            m=nr1+nr2
            allocate(Jnew(m ,n ));  
            allocate(Onew(m))  
            allocate(Tnew(n))
            allocate(sv1(nr1,m )) ; sv1=0; sv1(:,:nr1)=Identity(nr1);
            allocate(sv2(nr2,m )) ; sv2=0; sv2(:,nr1+1:)= Identity(nr2);          
            Jnew(1: nr1 ,1:nc1) = J1;   Jnew(1: nr1 ,1+nc1:)  = 0
            Jnew(1+ nr1: ,1:nc1) = 0;  Jnew(1+ nr1: ,1+nc1:) = J2  
            Onew(:nr1) = O1;   Onew(1+nr1:m) = O2; 
            tnew  (:nc1) = t1; tnew(1+nc1:n) =t2                    
            deallocate(c1,c2)
            return      
        endif

        allocate( A(m,n) )
        allocate( S(minMN) ) 
        allocate( U(m,m) ) 
        allocate( WW(minMN) )   ! not necessarily sorted
        allocate( V (n,n) )   ! starts life as its transpose
        allocate(bbb(m))     !  bbb =  (O2 - O1)  taking the common rows.
        r=0
        do i= 1,nc
            if(c1(i) ==0) cycle
            r=r+1; A(r,1:nc1) = -J1(c1(i),:);  A(r,1+nc1:) = J2(c2(i),:)
            bbb(r) = O2(c2(i) ) -O1(c1(i) )
        enddo  

        if(ubound(A,2) .eq. 0) u=0; ! not sure it matters, but in this case u is uninitialized
!*****************************************        
        call gesvd(a, s ,u ,v,info=info)
        if(info .eq.0) err=0
!*****************************************        
! reset A its needed for the residual
        r=0
        do i= 1,nc;    if(c1(i) ==0) cycle
            r=r+1; A(r,1:nc1) = -J1(c1(i),:);  A(r,1+nc1:) = J2(c2(i),:)
        enddo
        
        rw = count(s .gt. 1.0E-6) ;! write(*,*) 'ReducedJmatrix1 singular values bnds=',ubound(s) ; write(*,*) s

        v=transpose(v);
        
        allocate(sv(n,n-rw));
        sv = v(1:n,rw+1:n)  ;
        nrout = max(maxval(p_r1),maxval(p_r2))       
        allocate(Jnew(nrout ,n-rw )) 
        allocate(Onew(nrout))  
        allocate(Xnew(nrout))
        allocate(Tnew(n-rw))   
        allocate(j1out(ubound(J1,1), ubound(sv,2)))   !assymmetric !
        if(nc1>0) then; 
                allocate(sv1(nc1,ubound(sv,2))) ; 
                sv1= sv(1:nc1,:)  ! often fully-populated
                j1out = matmul(J1,sv1) ! this is slow. really needs a sparse multiply
        else
            j1out = matmul(J1,sv(1:nc1,:)) ! this has no sense: the 2nd operand has no rows
        endif

 
! the new origin
        allocate(wd(rw,rw))
        wd =  diagonalmatrix(1.0d00/s(1:rw));
        if(maxval(wd) .gt. 1.0e4) then
            write(*,*) 'CAREFUL!!!!!! SVs close to zero. nonzero count= ',rw,'/',minMN
         write(*,'(g15.5)') s
        endif

        allocate(orInTspace(n))    
        orInTspace = matmul(matmul(matmul( v(:, 1 : rw),wd),Transpose( u(:, 1 : rw))),bbb  ) ;  call vprint('orintspace', orInTspace, 221)  
        
        residual = sum(abs(matmul(A, orInTspace) - bbb))

        if(residual .gt. 1.0e-6) then
#ifdef DOFO_COMBO_CHECKING
            write(outputunit,'(a,a,g )',iostat=i) ' inconsistent restraints(L528)' ,' manhattan residual is ',residual
             write(221,'("(* ",a,a,g," *)" )',iostat=i) ' inconsistent restraints(L528)' ,' manhattan residual is ',residual
            write(outputunit,*) ' we are merging dofos ', d1%m_dofosli, d1%m_dofoN, ' and ', d2%m_dofosli, d2%m_dofoN 
            write(outputunit,*) ' this will give an error in parent TEEs when we recompose them using SV(i). '
            write(outputunit,*) ' orInTSpace is ',orInTspace
            write(outputunit,*) ' A.orInTSpace is ',matmul(A, orInTspace)
             write(221,*) '(* A.orInTSpace is ',matmul(A, orInTspace)  , " *)"            
            write(outputunit,*) '   bbb is ',bbb    
            write(221,*) ' (*  bbb is ',bbb , " *)"        
#endif
            err=99
        endif
      
  ! now copy rows r1() of j1out into JNew also NewOrigin
 ! and rows r2() of J2out into JNew.
 ! if they are common we should verify and take the mean
        allocate(X1(ubound(O1,1)));  allocate(X2(ubound(O2,1)))  
        X1 = O1 + matmul(J1,t1);  X2 = O2 + matmul(J2,t2);  
          
        do i=1,ubound(J1,1)
            jnew(p_r1(i),:) = j1out(i,:)
            Onew(p_r1(i)) = O1(i) + Dot_Product(J1(i,:),orintspace(1:nc1))
            Xnew(p_r1(i) ) = X1(i);
        enddo  
        deallocate(j1out);  
 ! now J2            
        allocate(j2out(ubound(J2,1), ubound(sv,2))  ) 
        if(n-(nc1+1)>=0) then  !  peter jan 2014 was  if(n-(nc1+1)>0) then
            allocate(sv2(nc2,ubound(sv,2))) ;  
            sv2= sv(nc1+1:n,:)
        endif     
        j2out =    matmul(J2,(sv(nc1+1:n,:)))
        do i=1,ubound(J2,1)
            jnew(p_r2(i),:) = j2out(i,:)
            Onew(p_r2(i)) = O2(i) + Dot_Product(J2(i,:),orintspace(nc1+1:n) )
            Xnew(p_r2(i) ) = X2(i);
        enddo 
        deallocate(j2out)
         
! get tNew, the t-parameters of the intersection          

        allocate(JNewCopy(ubound(Jnew,1), ubound(Jnew,2)  )) ;JNewCopy=Jnew 
        call LeastSquaresSolution(xnew-Onew,JnewCopy,tnew) ! finds Y to minimize ||Ax - y||2 N is usually > M    It trashes A

    ! now get svOrg1,svOrg2   
    ! t1 should be svorg1 + sv1.Tnew
    ! t2 should be svorg2 + sv2.Tnew  
    if(allocated(sv1) ) then
        allocate(svorg1(ubound(t1,1)));
        svorg1 = t1 - matmul(sv1,tnew);
    endif 
    if(allocated(sv2) ) then
        allocate(svorg2(ubound(t2,1)));
        svorg2 = t2 - matmul(sv2,tnew);
    endif  
            
 ! tidy up       
        deallocate( X1,X2 ,orInTspace,xnew,JNewCopy)  
        deallocate( S,U,WW,V,wd ,sv,bbb)
 !
 !verify
 !  t1 should be sv1.tnew and t2 sb sv2.tnew at the end of this.
 !
#ifndef  DOFO_COMBO_CHECKING
        deallocate( A,c1,c2 )
        return
#endif
    svbad=.false.
      if(allocated(sv1)) then
!        call mprint("SV1",sv1,221);
            allocate(x1(ubound(sv1,1))) 
            x1 = matmul(sv1,tnew) + svorg1
            do i=1,ubound(t1,1)
                if(abs(x1(i)-t1(i)) > 1e-7) then
                    svbad=.true.
                    write(outputunit,'("SV1 warning row=",i4," /",i4," x=",f15.6," t=",f15.6," diff=",g15.8 )') i,ubound(t1,1),x1(i),t1(i), x1(i)-t1(i)
                endif   
            enddo
            deallocate( X1 )
       endif
       if(allocated(sv2)) then
 !           call mprint("SV2",sv2,221);
            allocate(x2(ubound(sv2,1)))
            x2=matmul(sv2,tnew) +svorg2
            do i=1,ubound(t2,1)
                if(i > ubound(x2,1)) then
                    write(outputunit,*) ' SV2 looks small', ubound(sv2,1),  'not' ,ubound(t2,1)
                    cycle
                endif
                if(abs(x2(i)-t2(i)) > 1e-7) then
                    svbad=.true.
                    write(outputunit,'("SV2 warning row=",i4," /",i4," x=",f15.6," t=",f15.6," diff=",g15.8 )') i,ubound(t2,1),x2(i),t2(i), x2(i)-t2(i)
                endif   
            enddo        
            deallocate(X2)
       endif
       if(svbad) then 
            write(outputunit,'(" some SV mismatches found merging dofos ",i4, " and ",i4)')d1%m_dofoN, d2%m_dofoN
            write(outputunit,*) 'write dOnE'    
        endif
        deallocate( A,c1,c2 );          dbgprnt = .false.
end function  ReducedJmatrix1


 
subroutine LeastSquaresSolution(x,ain,y)   ! uses gelsy to find Y to minimize ||Ax - y||2 N is usually > M
 !  .. "Use Statements" ..
      use LAPACK95 ,only: gelsy
      use basictypes_f
!  .. "Implicit Statement" ..
      IMPLICIT NONE
      real(kind=double), intent(in), dimension(:) ::x 
      real(kind=double), intent(in), dimension(:,:) ::ain
      real(kind=double), intent(out), dimension(:) ::y  
    real(kind=double), allocatable, dimension(:) :: b
      real(kind=double), allocatable, dimension(:,:) ::a
!  You can use the QR factorization for solving the following least squares problem:
!   minimize ||Ax - y||2 
!   where A is a full-rank m-by-n matrix (m?n). After factoring the matrix,
!    compute the solution x by solving R x = (Q1)Tb.    
      integer  n,m,info; info=0;!,rank rank=0
        allocate(a(ubound(ain,1),ubound(ain,2)))
        a=ain

      n = ubound(a,2); m = ubound(a,1)
      if(n .eq.0) then          !recent MKLs dont like n==0
            y=0;     !    write(*,*) ' N is ',n,' m is ',m,' in LSQ'
            return
        endif

      allocate(b(max(m,n)))
      if( ubound(x,1) .ne. m) write(outputunit,*) ' X length',ubound(x,1),' doesnt match  M ',m
      if( ubound(y,1) .ne. n) write(outputunit,*) ' Y length',ubound(y,1),' doesnt match  N ',n
      b(:m) =x
      call gelsy(a, b ,info=info)
      if(info .ne. 0) write(outputunit,*) '(LeastSquares, gelsy)  info gives ',info
      y = b(:n )  
      deallocate(a,b)
end subroutine LeastSquaresSolution


 end module dofomath  

subroutine testlsq()
         use basictypes_f
         use dofomath
!  .. "Implicit Statement" ..
      IMPLICIT NONE
      integer,parameter ::   n =1 ,m=7 
      real(kind=double),  dimension(n) ::t 
      real(kind=double),  dimension(m,n) ::j 
      real(kind=double),  dimension(n,m) ::a 
      real(kind=double),  dimension(m) :: cartesian  

    j= 2.; j(2,1)=-1; ! for a bit of assymetry
      t=1; cartesian= matmul(j,t)
      a=transpose(j)
    write(outputunit,'(a, 9g15.5)') 'input cartesian = ', cartesian
    write(outputunit,'(a, 9g15.5)') 'input t         = ', t
   call LeastSquaresSolution(cartesian,j,t)  
   
     write(outputunit,'(a, 9g15.5)')'LSQ   cartesian = ', cartesian 
     write(outputunit,'(a, 9g15.5)')'LSQ   t (out)   = ', t
end   subroutine testlsq
    
#ifdef NEVER


function CheckJmatrix( J1,J2,r1,r2,O1,O2,t1,t2,J3, O3,t3) result(err)
!  .. "Use Statements" ..
      use basictypes_f
      use  dbg
!  .. "Implicit Statement" ..
     IMPLICIT NONE
!  .. "arguments" ..
        real(kind=double), intent(in), dimension(:,:):: J1, J2
        real(kind=double), intent(in), dimension(:)  :: O1 ,O2
        real(kind=double), intent(in), dimension(:)  :: t1 ,t2
        integer,           intent(in), dimension(:)  :: r1, r2

        real(kind=double), intent(in), allocatable, dimension(:,:):: J3   ! allocated in this procedure
        real(kind=double), intent(in) , dimension(*) :: O3,t3  ! must be of ample size (bigger than M or N)

!  .. "Local Scalars" ..
        real(kind=double) ::  err

        integer i,nc3,nr3
        real(kind=double), dimension (: ),allocatable :: X1,X2 ,X3

        nc3=ubound(J3,1);        nr3=ubound(J3,2);
        allocate(X1(ubound(J1,1)) )
        allocate(X2(ubound(J2,1)) )
         allocate(X3(nc3))

          X1 = O1 + matmul(J1,t1);  X2 = O2 + matmul(J2,t2);
          X3 = O3(:nc3) + matmul(J3,t3(:nr3));
          err=0.
          do i=1,ubound(J1,1)
            err =err + (X3(r1(i)) - X1(i) )**2
          enddo
          do i=1,ubound(J2,1)
            err =err + (X3(r2(i)) - X2(i) )**2
          enddo
    deallocate(X1,X2,X3)
 end  function CheckJmatrix

 subroutine test1  !a group of 2 and a group of 3. 1 common
 !  .. "Use Statements" ..
      USE mkl95_PRECISION, ONLY: WP => SP
     ! USE mkl95_LAPACK, ONLY: ges vd
      use dbg
      use dofomath
      use basictypes_f
!  .. "Implicit Statement" ..
      IMPLICIT NONE
!  .. "Local Scalars" ..

!  .. "Local Arrays" ..
! m rows, n cols
    character( len=*), parameter :: headline = 'a group of 2 and a group of 3. 1 common '
    integer ::ok =0,nx,nt
    real(kind=double), dimension(6,6) ::j1 
     integer, dimension(6) :: r1     
    real(kind=double), dimension(9,6) ::j2
      integer, dimension(9) :: r2     
    real(kind=double), dimension(6) ::O1 = (/ 0.,.0,.0,  0.,1.,1. /)
    real(kind=double), dimension(9) ::O2=(/0.,1,1,0.,0.,2.,0,0.5,1.5 /) 
    real(kind=double), dimension(6) ::t1= (/ 0,0,0,0,0,0  /) ,t2= (/0,0,0,0,0,0/)
    real(kind=double), dimension(24) :: X
    real(kind=double),  allocatable, dimension(:):: neworigin,tnew 
    real(kind=double),  allocatable, dimension(:,:):: Jnew
    j1 = reshape((/ 1.0,    0.0,    0.0,    1.0,    0.0,    0.0,    &
                    0.0,    1.0,    0.0,    0.0,    1.0,    0.0,    &
                    0.0,    0.0,    1.0,    0.0,    00.,    1.0,    &
                    0.0,    0.5,    -.5,    0.0,    -.5,    0.5,    &
                    -.5,    0.0,    0.0,    0.5,    0.0,    0.0,    &
                    0.5,    0.0,    0.0,    -.5,    0.0,    0.0/),(/6,6/))

  j2 = reshape ((/  1.0,    0.0,    0.0,    1.0,    0.,    0.0,   1.0,    0.,    0.0,    &
                    0.0,    1.0,    0.0,    0.0,    1.,    0.0,  0.0,    1.,    0.0,      &
                    0.0,    0.0,    1.0,    0.0,    0.,    1.0,  0.0,    0.,    1.0,      &
                    0.0 ,   0.5,    0.5,    0.0,    -.5,   -.5,   0.0,    0.,    0.0,     &
                    -.5,    0.0,    0.0,    0.5,    0.,    0.0,   0.0,    0.,    0.0,     &
                    -.5,    0.0,    0.0,    0.5,    0.0,   0.0,   0.0,    0.,    0.0     /),(/9,6/))
       
        r1=(/1,2,3,4,5,6 /) 
        r2=(/4,5,6,7,8,9,10,11,12/) 
        write(outputunit,'(a,\ )') headline
        call mprintd("starting J1=",j1);     call mprintd(" J12=",J2); 
        ok= ReducedJmatrix1( J1,J2,r1,r2,O1,O2,t1,t2,Jnew, neworigin,tnew)

        call mprintd('jnew = ',Jnew)
        ! now verify the consistency of the result
        nx = ubound(jnew,1); nt=ubound(jnew,2)
        X( :nx) = neworigin( :nx) + matmul(Jnew,tnew(:nt))
        call vprintd("origin now", neworigin( :nx))
        call vprintd("T now", tnew(:nt) )

        write(outputunit,'( a,g12.4 )') ' err = ',  CheckJmatrix( J1,J2,r1,r2,O1,O2,t1,t2,Jnew, neworigin( :nx) , tnew(:nt) ) 
        dbgprnt=.true.; 
        call mprintd('jnew = ',Jnew)
        call vprintd("X now", X( :nx))          
        deallocate(Jnew) 
end subroutine test1
 
subroutine test2   ! 
 !  .. "Use Statements" ..
      USE mkl95_PRECISION, ONLY: WP => SP
    !  USE mkl95_LAPACK, ONLY: ge svd
      use dbg
      use dofomath
      use basictypes_f
!  .. "Implicit Statement" ..
      IMPLICIT NONE
!  .. "Local Scalars" ..

!  .. "Local Arrays" ..
! m rows, n cols
     character( len=*), parameter :: headline = 'two slide-planes - intersecting '
    integer ::ok =0,nx,nt
    real(kind=double), dimension(3,2) ::j1 
     integer, dimension(3) :: r1     
    real(kind=double), dimension(3,2) ::j2
      integer, dimension(3) :: r2     
    real(kind=double), dimension(3) ::O1 = (/ 0.,.0,.0 /)
    real(kind=double), dimension(3) ::O2=(/0.,0.,0. /) 
    real(kind=double), dimension(2) ::t1= (/ 0,1 /) ,t2= (/0,1/)
    real(kind=double), dimension(3) :: X
    real(kind=double),  allocatable, dimension(:):: neworigin,tnew 
    real(kind=double),  allocatable, dimension(:,:):: Jnew
    j1 = reshape((/ 1.0,    0.0,    0.0,   &
                    1.0,    1.0,    0.0   /),(/3,2/))

  j2 = reshape ((/  1.0,    0.0,    1.0,      &
                    1.0,    1.0,    0.0        /),(/3,2/))
       
        r1=(/1,2,3 /) 
        r2=(/1,2,3/)  
        write(outputunit,'(a,\)') headline

        ok= ReducedJmatrix1( J1,J2,r1,r2,O1,O2,t1,t2,Jnew, neworigin,tnew)

        nx = ubound(jnew,1); nt=ubound(jnew,2)
        X( :nx) = neworigin( :nx) + matmul(Jnew,tnew(:nt))

        write(outputunit,'( a,f12.5 )') ' err = ',  CheckJmatrix( J1,J2,r1,r2,O1,O2,t1,t2,Jnew, neworigin( :nx) , tnew(:nt) ) 
        dbgprnt=.true.; 
        call mprintd('jnew = ',Jnew)
        call vprintd(" X ",X(:nx))
        deallocate(Jnew) 
 end subroutine test2
 
 

 
subroutine test3
 !  .. "Use Statements" ..
      USE mkl95_PRECISION, ONLY: WP => SP
      use dbg
      use dofomath
      use basictypes_f
!  .. "Implicit Statement" ..
      IMPLICIT NONE
!  .. "Local Scalars" ..

!  .. "Local Arrays" ..
! m rows, n cols
      character( len=*), parameter :: headline = 'fully fixed - free'
    integer ::ok =0,nx,nt
    real(kind=double), dimension(3,3) ::j1 
     integer, dimension(3) :: r1     
    real(kind=double), dimension(3,0) ::j2
      integer, dimension(3) :: r2     
    real(kind=double), dimension(3) ::O1 = (/ 1.,2.,3.0 /)
    real(kind=double), dimension(3) ::O2=(/-20.,0.,3.4 /) 
    real(kind=double), dimension(3) ::t1= (/ 0,0,0 /)
    real(kind=double), dimension(0) :: t2 ! = (/0,0,0,0,0,0/)
    real(kind=double), dimension(3) :: X
    real(kind=double),  allocatable, dimension(:):: neworigin,tnew 
    real(kind=double),  allocatable, dimension(:,:):: Jnew
    j1 = reshape((/ 1.0,    0.0,    0.0,  &
                    0.0,    1.0,    0.0,  &
                    0.0,    0.0,    1.0/),(/3,3/))

!  j2 = reshape ((/  1.0,    0.0,    0.0,    1.0,    0.,    0.0,   1.0,    0.,    0.0,    &
!                    0.0,    1.0,    0.0,    0.0,    1.,    0.0,  0.0,    1.,    0.0,      &
!                    0.0,    0.0,    1.0,    0.0,    0.,    1.0,  0.0,    0.,    1.0,      &
!                    0.0 ,   0.5,    0.5,    0.0,    -.5,   -.5,   0.0,    0.,    0.0,     &
!                    -.5,    0.0,    0.0,    0.5,    0.,    0.0,   0.0,    0.,    0.0,     &
!                    -.5,    0.0,    0.0,    0.5,    0.0,   0.0,   0.0,    0.,    0.0     /),(/9,6/))
       
      r1=(/1,2,3 /) 
      r2=(/1,2,3/)
        write(outputunit,'(a,\)') headline
        call mprintd("starting J1=",j1);     call mprintd(" J12=",J2); 
        ok= ReducedJmatrix1( J1,J2,r1,r2,O1,O2,t1,t2,Jnew, neworigin,tnew)


        ! now verify the consistency of the result
        nx = ubound(jnew,1); nt=ubound(jnew,2)
        X( :nx) = neworigin( :nx) + matmul(Jnew,tnew(:nt))
        call vprintd("origin now", neworigin( :nx))
        call vprintd("T now", tnew(:nt) )
        call vprintd("X now", X( :nx)) 
        dbgprnt=.true.; 
        call mprintd('jnew = ',Jnew)  
        call vprintd(" X ",X(:nx))      
        write(outputunit,'( a,g12.3 )') ' err = ',  CheckJmatrix( J1,J2,r1,r2,O1,O2,t1,t2,Jnew, neworigin( :nx) , tnew(:nt) ) 
        deallocate(Jnew) 
 end subroutine test3
 
 
 subroutine test4
 !  .. "Use Statements" ..
      USE mkl95_PRECISION, ONLY: WP => SP
    !  USE mkl95_LAPACK, ONLY: ge svd
      use dbg
      use dofomath
      use basictypes_f
!  .. "Implicit Statement" ..
      IMPLICIT NONE
!  .. "Local Scalars" ..
    integer ::ok =0,nx,nt
!  .. "Local Arrays" ..
! m rows, n cols
     character( len=*), parameter :: headline = 'again a group of 2, a group of 3. no common '

    real(kind=double), dimension(6,6) ::j1 
     integer, dimension(6) :: r1     
    real(kind=double), dimension(9,6) ::j2
      integer, dimension(9) :: r2     
    real(kind=double), dimension(6) ::O1 = (/ 0.,.0,.0,  0.,1.,1. /)
    real(kind=double), dimension(9) ::O2=(/0.,1,1,0.,0.,2.,0,0.5,1.5 /) 
    real(kind=double), dimension(6) ::t1= (/ 0,0,0,0,0,0  /) ,t2= (/0,0,0,0,0,0/)
    real(kind=double), dimension(24) :: X
    real(kind=double),  allocatable, dimension(:):: neworigin,tnew 
    real(kind=double),  allocatable, dimension(:,:):: Jnew
    j1 = reshape((/ 1.0,    0.0,    0.0,    1.0,    0.0,    0.0,    &
                    0.0,    1.0,    0.0,    0.0,    1.0,    0.0,    &
                    0.0,    0.0,    1.0,    0.0,    00.,    1.0,    &
                    0.0,    0.5,    -.5,    0.0,    -.5,    0.5,    &
                    -.5,    0.0,    0.0,    0.5,    0.0,    0.0,    &
                    0.5,    0.0,    0.0,    -.5,    0.0,    0.0/),(/6,6/))

  j2 = reshape ((/  1.0,    0.0,    0.0,    1.0,    0.,    0.0,   1.0,    0.,    0.0,    &
                    0.0,    1.0,    0.0,    0.0,    1.,    0.0,  0.0,    1.,    0.0,      &
                    0.0,    0.0,    1.0,    0.0,    0.,    1.0,  0.0,    0.,    1.0,      &
                    0.0 ,   0.5,    0.5,    0.0,    -.5,   -.5,   0.0,    0.,    0.0,     &
                    -.5,    0.0,    0.0,    0.5,    0.,    0.0,   0.0,    0.,    0.0,     &
                    -.5,    0.0,    0.0,    0.5,    0.0,   0.0,   0.0,    0.,    0.0     /),(/9,6/))
       
      r1=(/1,2,3,4,5,6 /) 
      r2=(/4,5,6,7,8,9,10,11,12/) +3
      write(outputunit,'(a,\)') headline
      call mprintd("starting J1=",j1);     call mprintd(" J12=",J2); 
        ok= ReducedJmatrix1( J1,J2,r1,r2,O1,O2,t1,t2,Jnew, neworigin,tnew)

        ! now verify the consistency of the result
        nx = ubound(jnew,1); nt=ubound(jnew,2)
        X( :nx) = neworigin( :nx) + matmul(Jnew,tnew(:nt))
        call vprintd("New origin ", neworigin( :nx))
        call vprintd("T now", tnew(:nt) )
        call vprintd("X now", X( :nx))  
        dbgprnt=.true.; 
        call mprintd('jnew = ',Jnew)
        call vprintd(" X ",X(:nx))
        write(outputunit,'( a,g12.3 )') ' err = ',  CheckJmatrix( J1,J2,r1,r2,O1,O2,t1,t2,Jnew, neworigin( :nx) , tnew(:nt) ) 
        deallocate(Jnew) 
 end subroutine test4
 
  subroutine test5   ! 
 !  .. "Use Statements" ..
      USE mkl95_PRECISION, ONLY: WP => SP
   !   USE mkl95_LAPACK, ONLY: g esvd
      use dbg
      use dofomath
      use basictypes_f
!  .. "Implicit Statement" ..
      IMPLICIT NONE
!  .. "Local Scalars" ..

!  .. "Local Arrays" ..
! m rows, n cols
     character( len=*), parameter :: headline = 'two fully-frees, coalesced'
    integer ::ok =0,nx,nt
    real(kind=double), dimension(3,3) ::j1 
     integer, dimension(3) :: r1     
    real(kind=double), dimension(3,3) ::j2
      integer, dimension(3) :: r2     
    real(kind=double), dimension(3) ::O1 = (/ 0.,.0,.0 /)
    real(kind=double), dimension(3) ::O2=(/0.,0.,0. /) 
    real(kind=double), dimension(3) ::t1= (/ 0,0,0 /)
    real(kind=double), dimension(3) ::t2= (/0,0,0/)
    real(kind=double), dimension(3) :: X
    real(kind=double),  allocatable, dimension(:):: neworigin,tnew 
    real(kind=double),  allocatable, dimension(:,:):: Jnew
    j1 = reshape((/ 1.0,    0.0,    0.0,   &
                    0.0,    1.0,    0.0,   &
                    0.0,    0.0,    1.0   /),(/3,3/))

    j2 = reshape((/ 1.0,    0.0,    0.0,   &
                    0.0,    1.0,    0.0,   &
                    0.0,    0.0,    1.0   /),(/3,3/))
       
      r1=(/1,2,3 /) 
      r2=(/1,2,3/)  
      write(outputunit,'(a,\)') headline

        ok= ReducedJmatrix1( J1,J2,r1,r2,O1,O2,t1,t2,Jnew, neworigin,tnew)


        ! now verify the consistency of the result
        nx = ubound(jnew,1); nt=ubound(jnew,2)
        X( :nx) = neworigin( :nx) + matmul(Jnew,tnew(:nt))

        write(outputunit,'( a,f12.5 )') ' err = ',  CheckJmatrix( J1,J2,r1,r2,O1,O2,t1,t2,Jnew, neworigin( :nx) , tnew(:nt) ) 
        dbgprnt=.true.; 
        call mprintd('jnew = ',Jnew)
        call vprintd(" X ",X(:nx))
        deallocate(Jnew) 
 end subroutine test5
 
 subroutine test6   ! 
 !  .. "Use Statements" ..
      USE mkl95_PRECISION, ONLY: WP => SP
     ! USE mkl95_LAPACK, ONLY: ge svd
      use dbg
      use dofomath
      use basictypes_f
!  .. "Implicit Statement" ..
      IMPLICIT NONE
!  .. "Local Scalars" ..

!  .. "Local Arrays" ..
! m rows, n cols
     character( len=*), parameter :: headline = 'two slide-planes - parallel '
    integer ::ok =0,nx,nt
    real(kind=double), dimension(3,2) ::j1 
     integer, dimension(3) :: r1     
    real(kind=double), dimension(3,2) ::j2
      integer, dimension(3) :: r2     
    real(kind=double), dimension(3) ::O1 = (/ 0.,.0,.0 /)
    real(kind=double), dimension(3) ::O2=(/0.,6.,0. /) 
    real(kind=double), dimension(2) ::t1= (/ 0,1 /) ,t2= (/0,1/)
    real(kind=double), dimension(3) :: X
    real(kind=double),  allocatable, dimension(:):: neworigin,tnew 
    real(kind=double),  allocatable, dimension(:,:):: Jnew
    j1 = reshape((/ 1.0,    0.0,    0.0,   &
                    1.0,    1.0,    0.0   /),(/3,2/))

  j2 = reshape ((/  1.0,    0.0,    0.0,      &
                    1.0,    1.0,    0.0        /),(/3,2/))
       
      r1=(/1,2,3 /) 
      r2=(/1,2,3/)  
      write(outputunit,'(a,\)') headline

        ok= ReducedJmatrix1( J1,J2,r1,r2,O1,O2,t1,t2,Jnew, neworigin,tnew)

        nx = ubound(jnew,1); nt=ubound(jnew,2)
        X( :nx) = neworigin( :nx) + matmul(Jnew,tnew(:nt))

        write(outputunit,'( a,f12.5 )') ' err = ',  CheckJmatrix( J1,J2,r1,r2,O1,O2,t1,t2,Jnew, neworigin( :nx) , tnew(:nt) ) 
        dbgprnt=.true.; 
        call mprintd('jnew = ',Jnew)
        deallocate(Jnew) 
 end subroutine test6
 
 program dofodev
      use dbg
       IMPLICIT NONE
              dbgprnt=.true.; 
        
    dbgprnt = .false. ; call test1
    dbgprnt = .false. ; call test2
    dbgprnt = .false.  ; call  test3  ! fully-fixed - free
    dbgprnt = .false. ; call test4
    
    dbgprnt = .false. ; call test5
    dbgprnt = .false. ; call test6 ! parallel slide planes
 !   dbgprnt = .false. ; call test7
!    dbgprnt = .false. ; call test8
end program dofodev
#endif
!Testing DOFO arithmetic
!
!A pair of planar fixities - intersecting  test1   PASS
!A pair of planar fixities - parallel
!
!A pair of line fixities - intersecting
!A pair of line fixities - parallel
!
!plane-line - intersecting
!plane-line - coplanar
!plane-line - non-intersecting
!
!plane- fully free
!line- fully free
!
!plane-fully fixed, intersecting
!plane-fully fixed, non-intersecting
!
!line-fully fixed, intersecting
!line-fully fixed, non-intersecting
!
!Two free nodes.
!two fully fixed nodes - coincident
!two fully fixed nodes - different
!
!Two independent free nodes - to make a 6x6 identity dofo
!A free node and a slideline node
!A free node and a slideplane node.
!
!
!A 2-node rigid body and a fully-fixed
!
!A 2-node rigid body and a fully-free
!
!a pair of 2-node rigid bodies sharing no nodes
!a pair of 2-node rigid bodies sharing 1 node
!a pair of 2-node rigid bodies sharing 2 nodes
!
!A live slideline with the slave fixed
!A live slideline with one end fixed
!Two live slidelines sharing a common slave (which is at their common master)
!
!Minimum for KOKOMO
!
!A live slideline with one end fixed
!Two live slidelines sharing a common slave (which is at their common master)
! a fully fixed and a fully free  test3  PASS

