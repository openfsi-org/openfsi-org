module tanpure_f
use basictypes_f
use ftypes_f
use vectors_f
use betype_f
implicit none 
contains
pure function beamelement_init(be, emp,  n12,npo,zi,beta1, n3, isBar,L,part_name ) result (rc)
! we have to hook up  xN1,xN2,tn1,tn2 outside this routine.
	type(beamelement), pointer :: be
		logical ,intent(in) ::  IsBar
		integer ,intent(in):: L, n3, npo
		integer,intent(in), dimension(2) :: n12
		REAL  (kind=double),intent(in) :: zi,  beta1
		REAL  (kind=double) ,intent(in),dimension(5) :: emp
!		REAL  (kind=double) :: beta2

		character(len=64) ,intent(in) :: part_name

		integer :: rc
 
		rc=BEAM_OK
		be%m_Currently_Active =.true.
		be%ti=0;
		be%emp=emp
		be%n12=n12
		be%n3=n3
		be%npo=npo
		be%zi=zi
		be%beta1=beta1
		be%isbar=isbar
		be%L=L
		be%m_part_name=part_name

end function beamelement_init

pure function threenodeaxismatrix(s,e,yin) result(a)

	implicit none
        real(kind=double), dimension(3), intent(in):: s     !       the start point (new origin)
        real(kind=double), dimension(3), intent(in):: e     !       a point throug which the x axis lies
        real(kind=double), dimension(3), intent(in):: yin   ! a third point to define the normal

!               note y can be -ve, meaning a lh coordinate set

	real(kind=double), TARGET, dimension(3,3) :: a   !       the matrix we are making
 !   real(kind=double), intent(in), dimension(nnbd,6) ::  xorXi

	real(kind=double) ,pointer  ,dimension(:) :: xaxis,yaxis,zaxis
	real*8 length
	logical error
	xaxis=>a(1,1:3)
	yaxis=>a(2,1:3)
	zaxis=>a(3,1:3)

	a=0	! to silence a compaq compiler warning. unnecessary

	yaxis = (yin - s) 
	xaxis = e - s

	call normalise(xaxis,length,error)

	call vprod(xaxis,yaxis,zaxis)   
	call normalise(zaxis,length,error)

	call vprod(zaxis,xaxis,yaxis)   
	call normalise(yaxis,length,error)
	
end function threenodeaxismatrix

pure function geo_stiff_matrix( force,length) result(kg)
	implicit none
!**************************************************************
!    now generate the geometric stiffness matrix kg(12x12)


	real(kind=double), intent(in),dimension(6)	:: force  ! force is f on page 43
	real(kind=double), intent(in)				::length 

	real(kind=double), dimension(12,12)			:: kg
! locals
	integer i,j
	real*8 a,b,chi


	a= (force(1) + force(2) ) /length/length
	b =(force(3) + force(4) ) /length/length
	chi = force(6)/length

	kg=0.0

	kg(1,2) = a
	kg(1,3) = -b
	kg(1,8) = -a
	kg(1,9) = b
	kg(2,2) = chi
	kg(2,7) = -a
	kg(2,8) = -chi
	kg(3,3) = chi
	kg(3,7) = b
	kg(3,9) = -chi
	kg(7,8) = a
	kg(7,9) = -b
	kg(8,8) = chi
	kg(9,9) = chi

!	*  fill in the lower half of the matrix
	do i=2,12  ! row
		do j=1,i-1
			kg(i,j) = kg(j,i) 
		end do
	end do
end function geo_stiff_matrix

pure function rotational_ttmatrix(xN1,xN2,tn1,tn2 ,t0) result (tt)
use nodalrotations_f

implicit none

	real(kind=double), intent(in), dimension(:,:) :: t0
	real(kind=double), intent(in), dimension(:) ::	xN1,xN2,tn1,tn2 

        real(kind=double), dimension(3,3)			:: tt, q
	real(kind=double)							:: length
	logical :: error

! t0 has X = chord, y and Z the section axes
!  t1 is CBV normalised
!	Get the Q matrix from mean of end rotations
! t2 is Q t0(2) with parallel component removed.
! t3 is cross_product(t1,t2)
	   
        tt(1:3,1) = xn2 - xn1
        call normalise(tt(1:3,1), length,error)

	if(error) then
                tt = t0
!		call pf_post_message("zero length chord")
		return
	endif
	Q =  Qmatrix((tn1 + tn2)*0.5 ) 
        tt(1:3,2) = matmul(Q,t0(2,1:3))			! will still be unit length.

        tt(1:3,2) = tt(1:3,2) - tt(1:3,1) * dot_product(tt(1:3,2),tt(1:3,1))
        call normalise(tt(1:3,2), length,error)
        tt(:,3) = cross_product(tt(:,1),tt(:,2)) ! we've verified that the T rows are unit length
    tt=Transpose(tt)
end function rotational_ttmatrix



pure function Find_Tmatrix3x3( xN1,xN2,tn1,tn2, ttu ,Xn3) result (t)
	implicit none
 
	real(kind=double), intent(in), dimension(:)			:: xN1,xN2,tn1,tn2 
	real(kind=double), intent(in), dimension(:,:)		:: ttu
	real(kind=double), intent(in),dimension(:),optional :: xN3

	real (kind=double), dimension(3,3) :: t

	if(present(Xn3)) then  
		t = threenodeaxismatrix(xN1,xN2,xN3 )
	else
		t = rotational_ttmatrix(xN1,xN2,tn1,tn2 ,ttu) ! which is pure.
	endif
end function Find_Tmatrix3x3

pure function Find_Tmatrix9x9( xN1,xN2,Xn3) result (t9)
	implicit none
 
	real(kind=double), intent(in), dimension(3)			:: xN1,xN2
	real(kind=double), intent(in),dimension(3)		 :: xN3
!	integer, intent(in) ,optional						:: N3


	real (kind=double), dimension(9,9) :: t9

 	t9 = 0
	t9(1:3,1:3) = threenodeaxismatrix(xN1,xN2,xN3 )
	t9(4:6,4:6) =	t9(1:3,1:3) 
	t9(7:9,7:9) =	t9(1:3,1:3) 

end function Find_Tmatrix9x9

pure function ttdash9x9(p_xn1,p_xn2,p_xn3) result(ttd)
implicit none
 	real(kind=double), intent(in), dimension(3)		::	p_xN1,p_xN2,p_xn3
	real(kind=double),  dimension(9,9,9) :: ttd

	integer :: i,k
	real(kind=double),  dimension(9,9) :: t0,t1
	real(kind=double), parameter :: delta = 0.00000001
 	real(kind=double), dimension(3) ::	xN1,xN2,xn3

	xn1=p_xn1; xn2=p_xn2; xn3=p_xn3

	t0 = transpose(Find_Tmatrix9x9(xN1,xN2,xn3))
	k = 0
	do i=1,3
		k = k + 1
		xn1(i) = xn1(i) + delta
		t1 = transpose(Find_Tmatrix9x9(xN1,xN2,xn3))
		ttd(k,1:9,1:9) = ( t1 - t0)/delta
		xn1(i) = xn1(i) - delta
	enddo
	do i=1,3
		k = k + 1
		xn2(i) = xn2(i) + delta
		t1 = transpose(Find_Tmatrix9x9(xN1,xN2,xn3))
		ttd(k,1:9,1:9) = ( t1 - t0)/delta
		xn2(i) = xn2(i) - delta
	enddo
	do i=1,3
		k = k + 1
		xn3(i) = xn3(i) + delta
		t1 = transpose( Find_Tmatrix9x9(xN1,xN2,xn3))
		ttd(k,1:9,1:9) = ( t1 - t0)/delta
		xn3(i) = xn3(i) - delta
	enddo
	


end function ttdash9x9

pure function ttdash(p_xn1,p_xn2,p_tn1,p_tn2,ttu) result (ttd) ! differential of the transpose
implicit none
 	real(kind=double), intent(in), dimension(:)		::	p_xN1,p_xN2,p_tn1,p_tn2 
	real(kind=double), intent(in), dimension(:,:) :: ttu 
	real(kind=double),  dimension(12,12,12) :: ttd

	integer :: i,k

	real(kind=double),  dimension(12,12) :: t0,t1
	real(kind=double), parameter :: delta = 0.00001
 	real(kind=double), dimension(3) ::	xN1,xN2,tn1,tn2 

	xn1=p_xn1; xn2=p_xn2; tn1=p_tn1; tn2=p_tn2

	t0 = transpose(tmatrix(xN1,xN2,tn1,tn2,ttu))
	k = 0
	do i=1,3
		k = k + 1
		xn1(i) = xn1(i) + delta
		t1 = transpose( tmatrix(xN1,xN2,tn1,tn2,ttu))
		ttd(k,1:12,1:12) = ( t1 - t0)/delta
		xn1(i) = xn1(i) - delta
	enddo
	do i=1,3
		k = k + 1
		tn1(i) = tn1(i) + delta
		t1 = transpose( tmatrix(xN1,xN2,tn1,tn2,ttu))
		ttd(k,1:12,1:12) = ( t1 - t0)/delta
		tn1(i) = tn1(i) - delta
	enddo
		do i=1,3
		k = k + 1
		xn2(i) = xn2(i) + delta
		t1 = transpose( tmatrix(xN1,xN2,tn1,tn2,ttu))
		ttd(k,1:12,1:12) = ( t1 - t0)/delta
		xn2(i) = xn2(i) - delta
	enddo
		do i=1,3
		k = k + 1
		tn2(i) = tn2(i) + delta
		t1 = transpose( tmatrix(xN1,xN2,tn1,tn2,ttu))
		ttd(k,1:12,1:12) = ( t1 - t0)/delta
		tn2(i) = tn2(i) - delta
	enddo


end function ttdash
pure function tmatrix(xN1,xN2,tn1,tn2,ttu) result (tt)   ! returns the 12x12 transformation matrix for beam elem, (based on bcv)
implicit none
 
	real(kind=double), intent(in), dimension(:) ::	xN1,xN2,tn1,tn2 
	real(kind=double), intent(in), dimension(:,:) :: ttu
	real*8 tt(12,12)
	
	integer i
	real*8  tt3(3,3)

	tt=0.0

	tt3 = Find_Tmatrix3x3(xN1,xN2,tn1,tn2,ttu) 
	do i = 1,10,3
		tt(i:i+2,i:i+2) = tt3
	enddo
end function tmatrix

pure function member_forces_12(force) result(f12)
implicit none
	real(kind=double), intent(in), dimension(10) :: force

	real(kind=double)		:: m13,m23,m12,m22,p,mt
	real(kind=double) 		:: sF12,SF22 ,sF13 ,SF23 

	real(kind=double),  dimension(12) :: f12

	m13	=  force(1)
	m23	= 	force(2)  
	m12	= 	force(3)  
	m22	= 	force(4)
	mt	= 	force(5)
	p	= 	force(6) 
	sF12= 	force(7)
	SF22= 	force(8)
	sF13= 	force(9)
	SF23= 	force(10) 

	f12(1)  =  -p
	f12(2)  =  sf12
	f12(3)  =  sf13
	f12(4)  =  -mt
	f12(5)  =  m12
	f12(6)  =  m13
	f12(7)  =  p	
	f12(8)  =  sf22	
	f12(9)  =  sf23
	f12(10) =  mt	
	f12(11) =  m22
	f12(12) =  m23
end function member_forces_12

pure function member_forces(p_emp,l,uhat,p_Bowing,p_BowShear,axload) result(force)! now with P-delta shear terms. Should include Y and Z disps and
	implicit none

	real(kind=double), intent(in), dimension(5) :: p_emp
	real(kind=double), intent(in), dimension(12) ::uhat
	real(kind=double), intent(in) :: L 
        integer, intent(in) :: axload !axload >0 = tension only. <0 = compression only
	logical, intent(in) :: p_Bowing,p_BowShear

!locals   
	real(kind=double), target, dimension(10) :: force
	real(kind=double)						:: ei2,ei3,gj,ea ,ti
	real(kind=double), pointer				:: m13,m23,m12,m22,p,mt

	real(kind=double) ,pointer				:: sF12,SF22 ,sF13 ,SF23 

	real*8  t12, t13, t22,t23, thetat, e

	ea  = p_emp (1) 
	ei2 = p_emp( 2)
	ei3 = p_emp( 3)
	gj  = p_emp( 4)
    ti  = p_emp (5)

	m13	=>  force(1)
	m23	=> 	force(2)  
	m12	=> 	force(3)  
	m22	=> 	force(4)
	mt	=> 	force(5)
	p	=> 	force(6) 
	sF12=> 	force(7)
	SF22=> 	force(8)
	sF13=> 	force(9)
	SF23=> 	force(10) 

	force(7:10) =0	! to remove  a compaq compile warning


	e   = uhat(7) - uhat(1) !  extension
	t12 = uhat(5)		!	end 1 axis 2 rotations relative to chord
	t13 = uhat(6)
	t22 = uhat(11)
	t23 = uhat(12)
	thetat = uhat(10) - uhat(4)

	if(p_Bowing) then
		p =  ti + ea*e/l  + ea/30.d0 * ( (2.d0*t13**2 - t13*t23 + 2.d0*t23**2) + (2.d0*t12**2 - t12*t22 + 2.d0*t22**2 ) )
                if(axload>0 .and. p<0)  p=p/1000.0;
                if(axload<0 .and. p>0)  p=p/1000.0;
	! verified 24 May 1995 AKM  
	! July 2004 PH comments the 4th order terms are typically 4 OOM smaller. See bowing_verif.xls

		mt = gj/l * thetat
		m13= (4.d0*ei3/l + 4.d0*p*l/30.d0)*t13 + (2.d0*ei3/l - p*l/30.d0     )*t23
		m23= (2.d0*ei3/l - p*l/30.d0)     *t13 + (4.d0*ei3/l + 4.d0*p*l/30.d0)*t23
		m12= (4.d0*ei2/l + 4.d0*p*l/30.d0)*t12 + (2.d0*ei2/l - p*l/30.d0     )*t22
		m22= (2.d0*ei2/l - p*l/30.d0)    * t12 + (4.d0*ei2/l + 4.d0*p*l/30.d0)*t22

! july 2004  the bowing terms for the moments agree with MtKa. Note that
!  the sign of m12,m13 are the opposite of EI y''
! NB need to incorporate lateral deflections and shear due to bowing.
! m2 is a hogging moment.  M1 is a sagging moment

		if(p_BowShear) then
				sF12 = (P*(11.0*t12 + t22))/10.0  !  END1 NUMERICALLY +VE FOR TENSION AND HOG
				SF22 = (P*(t12 + 11.0*t22))/10.0  !  END2 NUMERICALLY -VE FOR TENSION AND HOG
				sF13 = (P*(11.0*t13 + t23))/10.0  
				SF23 = (P*(t13 + 11.0*t23))/10.0
		endif
	else
		mt = gj/l * thetat
		m13= (4.d0*ei3/l)  *t13 + (2.d0*ei3/l )*t23
		m23= (2.d0*ei3/l)  *t13 + (4.d0*ei3/l )*t23
		m12= (4.d0*ei2/l)  *t12 + (2.d0*ei2/l )*t22
		m22= (2.d0*ei2/l)  *t12 + (4.d0*ei2/l )*t22
		p =  ti + ea*e/l  
                if(axload>0 .and. p<0)  p=p/1000.0;
                if(axload<0 .and. p>0)  p=p/1000.0;
		sF12 = 0.0; SF22 =0.0;  sF13 =0.0; SF23 = 0.0
	endif

end function member_forces

pure function equil_matrix(p_length) result(meq)
	implicit none
!**************************************************************
!    now generate the equilibrium matrix meq (12x6)
	real(kind=double), intent(in)	:: p_length	! ZT(L)
	real*8 meq(12,10)

! locals
	real*8 l_inv
 
	meq=0.0
	l_inv = 1.0d00/p_length

	meq(1,6) = -1.0d00
	meq(2,1) = l_inv
	meq(2,2) = l_inv
	meq(3,3) = -l_inv
	meq(3,4) = -l_inv
	meq(4,5) = -1.0d00
	meq(5,3) = 1.0d00
	meq(6,1) = 1.0d00
	meq(7,6) = 1.0d00
	meq(8,1) = -l_inv
	meq(8,2) = -l_inv
	meq(9,3) = l_inv
	meq(9,4) = l_inv
	meq(10,5) = 1.0d00
	meq(11,4) = 1.0d00
	meq(12,2) = 1.0d00

! july 2004 the bow shear terms
	meq(5,9)	= -1.0d00	; meq(6,7)	= -1.0d00
	meq(11,10)	=  1.0d00	; meq(12,8)	=  1.0d00

end function equil_matrix 

pure function local_stiff(uhat,p_emp, p_zi) result(k)
!**************************************************************
	implicit none
!    gets the local stiffness matrix  k (6x6, symmetrical)
!	integer elem
	real(kind=double),intent(in) , dimension(12) :: uhat
	real(kind=double),intent(in) , dimension(4) :: p_emp
		real(kind=double),intent(in) :: p_zi
	real*8 k(6,6)

!locals   
	real*8 ei2,ei3,gj,ea,l_e
	real*8  t12, t13, t22,t23
	integer i


!	p_zi   =  zi(elem)   ! was zt
	ei2 = p_emp( 2)
	ei3 = p_emp( 3)
	gj  = p_emp( 4)
	ea  = p_emp (1) 
	l_e   = uhat(7) - uhat(1) !  zt(elem) - zi(elem)= extension
	t12 = uhat(5)		!	end 1 axis 2 rotations relative to chord
	t13 = uhat(6)
	t22 = uhat(11)
	t23 = uhat(12)



!*  the following are from tan p41-42

	k(1,1) = 4.*ei3/p_zi + 4.*ea*l_e/30.&
	&		+ ea*p_zi/300. *( 8.*t13**2 - 4.*t13*t23 + 3.*t23**2)&
	&		+ ea*p_zi/900. *( 8.*t12**2 - 4.*t12*t22 + 8.*t22**2)

	k(1,2) = 2.*ei3/p_zi - ea*l_e/30.- ea*p_zi/300. * ( 2.*t13**2 - 6.*t13*t23 &
	+ 2.*t23**2) -ea*p_zi/900. * (2.*t12**2 -t12*t22  + 2. * t22**2)

	k(1,3) = ea*p_zi/900. * ( 16.*t13*t12 - 4.*t13*t22 - 4.*t23*t12 + t23*t22)

	k(1,4) = ea*p_zi/900. *( -4.*t13*t12 + 16.*t13*t22 + t23*t12 - 4.*t23*t22)

	k(1,5) = 0.0d00

	k(1,6) = ea/30.0 *( 4.*t13 - t23)

	k(2,2) = 4.* ei3/p_zi + 4.*ea*l_e/30.&
	&		+ ea*p_zi/300. * ( 3.*t13**2 - 4.*t13*t23 + 8.*t23**2)&
	&		+ ea*p_zi/900. *(  8.*t12**2 - 4.*t12*t22 + 8. *t22**2 )


	k(2,3) = ea*p_zi/900. *( -4.*t13*t12 + t13*t22 +16.*t23*t12 - 4.*t23*t22)

	k(2,4) = ea*p_zi/900. *( t13*t12 - 4.*t13*t22 - 4.*t23*t12 + 16.*t23*t22)

	k(2,5) = 0.0d00

	k(2,6) = ea/30. *( -t13 + 4.*t23)

	k(3,3) = 4.*ei2/p_zi + 4.*ea*l_e/30.&
	&		+ ea*p_zi/900. *( 8.*t13**2 - 4.*t13*t23 + 8.*t23**2)&
	&		+ ea*p_zi/300. *( 8.*t12**2 - 4.*t12*t22 + 3.*t22**2)

	k(3,4) = 2.*ei2/p_zi - ea*l_e/30.&
	&	- ea*p_zi/900. * (2.*t13**2 - t13*t23 + 2.*t23**2)&
	&	- ea*p_zi/300. * (2.*t12**2 - 6.*t12*t22 + 2.*t22**2)

	k(3,5) = 0.0d00

	k(3,6) = ea/30. * ( 4.*t12 - t22)

	k(4,4) = 4.*ei2/p_zi + 4.*ea* l_e/30.&
	&	+ ea*p_zi/900. * ( 8.*t13**2 - 4.*t13*t23 + 8.*t23**2)&
	&	+ ea*p_zi/300. * (3.*t12**2 - 4.*t12*t22 + 8.*t22**2)

	k(4,5) = 0.0d00

	k(4,6) = ea/30. * ( -t12 + 4.*t22)

	k(5,5) = gj/p_zi

	k(5,6) = 0.0d00

	k(6,6) = ea/p_zi


!  fill in the lower half of the matrix
	do i=2,6  ! row
			k(i,1:i-1) = k(1:i-1,i) 
	end do

end function local_stiff

RX_PURE function local_disp(p_tsmall,p_be) result(uhat) ! uhat are the disp in local axis system
	use nodalrotations_f
	use coordinates_f
	implicit none

	real(kind=double), intent(in), dimension(:,:) :: p_tsmall
	type(beamelement), intent(in) ::  p_be

! returns
	real*8 uhat(12)


! locals
	integer,dimension(2) :: n
	real(kind=double), dimension(3) ::   tangent,t_Local, y
	real(kind=double), dimension(3,3) ::  Q1,Q2

!	biv 	=> p_be%ttu(1,1:3)
	n		=  p_be%n12

!  u(1-3,7-9)   are displacements in local axes at each end
!  u(4-6,10-12) are rotations in local axes at each end

! method.
! 1) For the xyz 
	uhat(1:3) = 0.0
	uhat(7:9) = matmul( p_tsmall, (get_Cartesian_X_By_Ptr(p_be%XN2) - get_Cartesian_X_By_Ptr(p_be%XN1)))
	uhat(7) = p_be%zt - p_be%zi

!2) For the bending deflections.  The tangent at each end is Qmatrix of the end times x 
! end 1
	Q1 = Qmatrix(get_Cartesian_X_By_Ptr(p_be%TN1))
	tangent = matmul(Q1,p_be%ttu(1,1:3))
	t_Local = matmul(p_tsmall,tangent)
	uhat(5) = - t_Local(3)	! valid for small rotations. Otherwise use atan2
	uhat(6) =   t_Local(2)	! valid for small rotations. Otherwise use atan2
	
! end 2
	Q2 = Qmatrix(get_Cartesian_X_By_Ptr(p_be%TN2))
	tangent = matmul(Q2,p_be%ttu(1,1:3))
	t_Local = matmul(p_tsmall,tangent)
	uhat(11) = - t_Local(3)	
	uhat(12) =   t_Local(2)	
	
	 
!3) For the torsion.  
!			Find  t0(2,1:3) convected due to the end node. 
!			then convert to deformed axes
!			then its's local Z component is the twist

! end 1
	y = matmul(Q1, p_be%ttu(2,1:3)) 
	y = matmul(p_tsmall, y)
	uhat(4) = y(3)
! end 2
	y = matmul(Q2, p_be%ttu(2,1:3)) 
	y = matmul(p_tsmall, y)
	uhat(10) = y(3)
	
end function local_disp

RX_PURE  function one_beam_global_stiffness(p_be, p_dxdr,p_geomatrix,p_Bowing,p_BowShear ) result(globalstiff) ! tan version

use nodalrotations_f
use coordinates_f


implicit none
	type(beamelement), intent(in) :: p_be ! the link number
	logical, intent(in)		:: p_dxdr, p_geomatrix,p_Bowing,p_BowShear 
	real(kind=double), dimension(12,12) :: globalstiff


  	real*8 t(12,12) ,drl_dx(12,12)
	real*8 bigs(12,12)
	real*8 ke(6,6),meq(12,10),kg(12,12) ! , force(10) 
	real*8 uhat(12)


		t = tmatrix (get_Cartesian_X_By_Ptr(p_be%xN1),get_Cartesian_X_By_Ptr(p_be%xN2),get_Cartesian_X_By_Ptr(p_be%tn1),get_Cartesian_X_By_Ptr(p_be%tn2) ,p_be%ttu) ! returns t for link l (12x12)

		uhat =  local_disp(t(1:3,1:3),p_be) !		now get local displacements uhat = t * uglobal

		ke= local_stiff(uhat,p_be%emp, p_be%zi)!    now get the local stiffness matrix  ke (6x6, symmetrical)
 
		meq = equil_matrix(p_be%zt)!		now generate the equilibrium matrix meq (12x6) (july 2004 12x10)

		if (p_geomatrix) then    !		now generate the geometric stiffness matrix kg(12x12)
			kg = one_global_stiffness_by_ttd(p_be,p_Bowing,p_BowShear )
		else
			kg=0.0
		endif
!														 t 					 t
!    finally the tangent stiffness matrix is t *( meq	* ke * meq  + kg)* t
!
!	write(str,*) ' Direct stiffness el ',p_be%L
!	err= check_matrix(ke,6,str)
!	if(err .ne.0) then
!		write(str,*) ' beam ',p_be%L, ' has NAN in Direct stiffness '
!		call pf_post_message(str)
!	endif
!	write(str,*) 'geometric stiffness el ',p_be%L
!	err=err+  check_matrix(kg,12,str)
!	if(err .ne.0) then
!		write(str,*) ' beam ',p_be%L, ' has NAN in  geometric stiffness '
!		call pf_post_message(str)
!	endif

	bigs = matmul(meq(1:12,1:6),matmul(ke,transpose(meq(1:12,1:6)))) + kg
         bigs(10,10) = bigs(10,10) + p_be%rotstiff;
         bigs(4,4)  = bigs(4,4)   + p_be%rotstiff;    ! a heuristic for battens. Add a rotational stiffness in beam axis direction
	if( p_dxdr) then
                drl_dx = t
		drl_dx(4:6,4:6)		= drLocalBydxGlobal  (get_Cartesian_X_By_Ptr(p_be%TN1),t(1:3,1:3)) 
		drl_dx(10:12,10:12) = drLocalBydxGlobal  (get_Cartesian_X_By_Ptr(p_be%TN2),t(1:3,1:3))
		globalstiff = matmul(TRANSPOSE(t),matmul(bigs,drl_dx))
	else
		globalstiff = matmul(TRANSPOSE(t),matmul(bigs,t))
	endif

end	function one_beam_global_stiffness

RX_PURE function one_global_stiffness_by_ttd(p_be, Bowing,BowShear ) result(globalstiff) ! tan version

use nodalrotations_f
use coordinates_f

implicit none
	type(beamelement), intent(in) :: p_be ! the link number
	logical, intent(in)		::  Bowing,BowShear 
	real(kind=double), dimension(12,12) :: globalstiff

	integer :: i,k
  	real*8 t(12,12)
	real*8  force(10) ,f12(12)
	real*8 uhat(12), ttd(12,12,12)

	t = tmatrix (get_Cartesian_X_By_Ptr(p_be%xN1),get_Cartesian_X_By_Ptr(p_be%xN2),get_Cartesian_X_By_Ptr(p_be%tn1),get_Cartesian_X_By_Ptr(p_be%tn2),p_be%ttu) ! returns t for link l (12x12)

		uhat =  local_disp(t(1:3,1:3),p_be) !		now get local displacements uhat = t * uglobal
                force = member_forces(p_be%emp,p_be%zi,uhat,Bowing,BowShear,p_be%axload)
		f12 = member_forces_12(force)

		ttd = ttdash(get_Cartesian_X_By_Ptr(p_be%xN1),get_Cartesian_X_By_Ptr(p_be%xN2),get_Cartesian_X_By_Ptr(p_be%tn1),get_Cartesian_X_By_Ptr(p_be%tn2) ,p_be%ttu)
	do i=1,12
	do k=1,12
		globalstiff(i,k) = dot_product(ttd(k,i,1:12),f12)
	enddo
	enddo

	globalstiff = (globalstiff+transpose(globalstiff) ) /2.0d00	

end	function one_global_stiffness_by_ttd




RX_PURE function one_local_element_force(p_be,length,Bowing,BowShear  ) result(force)
use coordinates_f
implicit none
	type(beamelement), intent(in) :: p_be ! the link number
	logical, intent(in)			:: Bowing,BowShear 
	real(kind=double), dimension(10) :: force

	real(kind=double), intent(in) :: length
  	real*8 t(12,12)
	real*8 uhat(12)

        t = tmatrix (get_Cartesian_X_By_Ptr(p_be%xN1),get_Cartesian_X_By_Ptr(p_be%xN2),get_Cartesian_X_By_Ptr(p_be%tn1),get_Cartesian_X_By_Ptr(p_be%tn2) ,p_be%ttu) ! returns t for link l (12x12)
        uhat = local_disp (t(1:3,1:3),p_be)	!  local displacements uhat = t * uglobal
        force = member_forces(p_be%emp,length,uhat,Bowing,BowShear,p_be%axload)

end	function one_local_element_force 

RX_PURE function one_local_element_trigraph(p_be ) result(f ) !  for paraview output, global coords
use coordinates_f
implicit none
        type(beamelement), intent(in) :: p_be ! the link number
        real(kind=double), dimension(9) :: f
        real (kind=double), dimension(3,3) :: tt3
   !     real*8 t(12,12)
   !      t = tmatrix (get_Cartesian_X_By_Ptr(p_be%xN1),get_Cartesian_X_By_Ptr(p_be%xN2),get_Cartesian_X_By_Ptr(p_be%tn1),get_Cartesian_X_By_Ptr(p_be%tn2) ,p_be%ttu) ! returns t for link l (12x12)

        tt3 = Find_Tmatrix3x3 (get_Cartesian_X_By_Ptr(p_be%xN1),get_Cartesian_X_By_Ptr(p_be%xN2),get_Cartesian_X_By_Ptr(p_be%tn1),get_Cartesian_X_By_Ptr(p_be%tn2) ,p_be%ttu)
        f(1:3) = tt3(1,1:3); !	t(1,1:3) = xn2 - xn1
        f(4:6) = tt3(2,1:3);
        f(7:9) = tt3(3,1:3);
end function one_local_element_trigraph



RX_PURE function one_beam_element_force(p_be,Bowing,BowShear) result (global_force) ! tan version. Global
use coordinates_f
	implicit none

	type(beamelement) ,pointer ::  p_be 
	logical, intent(in)			::	Bowing,BowShear 
	real(kind=double), dimension (12) :: global_force
	real(kind=double) meq(12,10),force(10), t(12,12)
	real(kind=double), dimension (12) :: uhat
		
                p_be%zt = norm(get_Cartesian_X_By_Ptr(p_be%XN2) -get_Cartesian_X_By_Ptr( p_be%xn1))
		t = tmatrix(get_Cartesian_X_By_Ptr(p_be%xN1),get_Cartesian_X_By_Ptr(p_be%xN2),get_Cartesian_X_By_Ptr(p_be%tn1),get_Cartesian_X_By_Ptr(p_be%tn2 ),p_be%ttu) ! returns t for link l (12x12)

		uhat = local_disp (t(1:3,1:3),p_be)!	 local displacements uhat = t * uglobal
                force = member_forces(p_be%emp,p_be%zi,uhat,Bowing,BowShear,p_be%axload) ! 7,8,9,10 are the shears due to bowing

!			force = one_local_element_force(p_n,zi(p_n))

!			now the local forces are meq*force
!			and the global are ttrans * (localforce)
		meq= equil_matrix(p_be%zt)!	the equilibrium matrix meq (12x6)

		global_force = matmul(transpose(t),matmul(meq,force))

end function one_beam_element_force


RX_PURE function one_initial_beamaxismatrix(be,s,e) result (rc)  ! USE UNDEFORMED COORDS
	use nodalrotations_f

	implicit none
	type(beamelement),pointer :: be
	real(kind=double), dimension(:), intent(in):: s		     !       the start point (new origin)
	real(kind=double), dimension(:), intent(in):: e			  !       a point throug which the x axis lies
	integer ::rc
	! ttundeformed is based on c_beta(nn,1) only 
! so the residual and stiffness calcs need to include ALL convection 
!  ie the local coords convected become Q((x1+x2)/2 * tt0
! The 

	real*8 biv(3)
	real*8 cob,sib    ! dl(3,3)
	real(kind=double), parameter, dimension(3) :: l_xaxis  = (/1,0,0/)

	real*8 xa(3),xb(3)	,zil, dummy, bn, bml, ba,bl
	logical error

	rc=BEAM_OK

      biv= e- s
      zil=norm(biv)
        if(zil .lt. 0.0001.or.be%zi .lt. 0.0001 ) then
                rc=BEAM_TOO_SHORT
                return
        endif

        biv = biv/ zil
        be%ttu(1,1:3)=biv

! get ttu then rotate about the chord by beta 

      cob=cos(be%beta1)
      sib=sin(be%beta1)
      be%ttu(1,3)=min(1.0d0,be%ttu(1,3))
      bn=be%ttu(1,3)
      ba=sqrt(1.0d00-bn*bn)

upr:	if (.not. be%IsUpright ) then !  elemt Z axis  (for eixx) is in vertical plane
			bml=be%ttu(1,2)/ba
			bl=be%ttu(1,1)/ba
			be%ttu(2,1)=-cob*bml-bn*bl*sib
			be%ttu(2,2)=bl*cob-bn*bml*sib
			be%ttu(2,3)=ba*sib
			be%ttu(3,1)=bml*sib-bn*bl*cob
			be%ttu(3,2)=-bl*sib-bn*bml*cob
			be%ttu(3,3)=ba*cob
      else upr
			call vprod( biv,l_xaxis,xa)
			if(DOT_PRODUCT(xa,xa) .lt. 1.0d-12) then	! beam lies along x axis
				be%ttu = identityMatrix(3)
				rc= BEAM_INCONSISTENT
                                return
			endif
			call normalise(xa,dummy,error)
			xb = cross_product(biv,xa)
			call normalise( xb,dummy,error)

			be%ttu = Qmatrix(biv* be%beta1) ! using ttu to store the beta transformation
			xa = matmul(be%ttu,xa)
			xb = matmul(be%ttu,xb)

			be%ttu(1,1:3) = biv  ! now fill in TTu
			be%ttu(2,1:3) = xa
			be%ttu(3,1:3) = xb
      endif upr

end function one_initial_beamaxismatrix

pure function one_beam_preliminaries(be,s,e) result(rc)  ! s and e are in UNDEFORMED coords
implicit none

	type(beamelement) , pointer :: be
	real(kind=double), dimension(:), intent(in):: s		     !       the start point (new origin)
	real(kind=double), dimension(:), intent(in):: e			  !       a point throug which the x axis lies

	integer ::rc
	real*8  biv(3)
	
!	real*8  bcv(3)
	real*8 :: uptest ,ba 
	
	uptest=0.7071 

		rc=BEAM_OK

	!	bcv= x(in(2),1:3)- x(in(1),1:3)
		biv=e-s
 
		be%zi= norm(biv)
		be%zt= 		be%zi ! GUESS CAN SET LATER =  norm(bcv)

		if(be%zi .lt. .0001) then
			rc= BEAM_TOO_SHORT
			 
			return
		endif


		biv =biv/norm(biv) ! was be%zi till oct 2005! was bcv till Jan 2004 

		ba = 1.0d00-biv(3)**2
		ba = max (ba,0.0d00)
		ba=sqrt(ba)
!
		if(ba .gt. uptest) then 
			be%IsUpright=.false. ! used only in  set_undeformed_ttmatrix3by3
		else
			be%IsUpright= .true.
		endif

end function one_beam_preliminaries

RX_PURE function one_rod_tension(be) result(tq)
use coordinates_f
implicit none

type(beamelement) , pointer :: be
real(kind=double), dimension(3) :: d
real(kind=double) ::sum,tq
		  d = get_Cartesian_X_By_Ptr(be%xn2) - get_Cartesian_X_By_Ptr(be%xn1)
	      sum=d(1)**2+d(2)**2+d(3)**2
	      be%zt=sqrt(sum)
	      be%tq =be%emp(1)*(be%zt-be%zi)/be%zi +  be%emp(2)
		  if(.not. be%IsBar) then
			  be%tq = max(be%tq ,0.0d00)
		  endif
		  tq=be%tq
end function one_rod_tension

RX_PURE  function one_beam_global_mass(p_be, p_mass,p_pmi ) result(globalstiff)
use nodalrotations_f
use betype_f
use coordinates_f
implicit none
	type(beamelement), intent(in) :: p_be ! the element
	real(kind=double), intent(in)		:: p_mass,p_pmi
	real(kind=double), dimension(12,12) :: globalstiff


  	real*8 t(12,12) 
	real*8 kl(12,12) ! local mass matrix 

!lumped, isotropic masses.  Wrong for MOI about axis

	real(kind=double) :: elmass, elmoi

	elmass = p_be%zi *p_mass
	elmoi = elmass * ( p_be%zi**2 )/48.d0 ! see http://www.colorado.edu/engineering/CAS/courses.d/IFEM.d/IFEM.Ch31.d/IFEM.Ch31.pdf

	globalstiff=0.
	globalstiff(1,1) = elmass/2.
	globalstiff(2,2) = elmass/2.
	globalstiff(3,3) = elmass/2.

	globalstiff(4,4) = elmoi 
	globalstiff(5,5) = elmoi 
	globalstiff(6,6) = elmoi 

	globalstiff(7:12,7:12) = globalstiff(1:6,1:6)

	return
! end of lumped version. 
	t = tmatrix (get_Cartesian_X_By_Ptr(p_be%xN1),get_Cartesian_X_By_Ptr(p_be%xN2),get_Cartesian_X_By_Ptr(p_be%tn1),get_Cartesian_X_By_Ptr(p_be%tn2),p_be%ttu) ! returns t for link l (12x12)

	kl= local_massmatrix( p_be%zi,p_mass,p_pmi )

	globalstiff = matmul(TRANSPOSE(t),matmul(kl,t))

end	function one_beam_global_mass


pure function local_massmatrix( p_zi,p_mass,p_pmi ) result(k)
!**************************************************************
	implicit none
	real(kind=double),intent(in) ::  p_zi,p_mass,p_pmi
	real*8 k(12,12) 

!locals   
	real(kind=double)  :: L,qq
	L=p_zi

	qq = p_pmi * L/2.
	k=0;
	k(1:3,1:3) = reshape(  (/ 140 , 0 , 0 , 0 , 156 , 0,0 ,0 , 156 /),(/3,3/))
	k(7:9,7:9) = k(1:3,1:3)

	k(1:3,4:6) = reshape(  (/ 0., 0. , 0. , 0. , 0. , 22. ,0. ,-22. , 0. /),(/3,3/))/L
	k(7:9,10:12) =	-k(1:3,4:6) 	 ! OK 

	k(1:3,7:9) =  reshape(  (/ 70., 0. , 0. , 0. , 54. , 0. ,0. ,0.  , 54. /),(/3,3/)) ! corrected

	k(1:3,10:12) =  reshape(  (/ 0,0,0,	   0 ,0,-13 ,   0 ,13 ,0 /),(/3,3/)) *L !OK
	k(4:6,7:9) =  k(1:3,10:12) ! corrected

	k(4:6,10:12) =  reshape(  (/ 0,0,0,	   0 ,-3,0 ,   0 ,0,-3 /),(/3,3/)) *L**2 ! OK

	k(4:6,4:6) = reshape(  (/ 0, 0 , 0 , 0 , 4 , 0,0 ,0 , 4 /),(/3,3/)) *L**2 ! OK
	k(4,4)= 0.
	k(10:12,10:12) =	k(4:6,4:6) 

! fill in the lower half
	k(4:6,1:3) = transpose(k(1:3,4:6))
	k(10:12,7:9) = transpose(k(7:9,10:12))
	k(7:12,1:6) = transpose(k(1:6,7:12))
! multipy by ro A L/420

	k = k * p_mass * L /420.

! add the torsion terms (seem far too heavy from frequency results)
	k(4,4)=qq
	k(10,10)=qq

end function local_massmatrix
end module tanpure_f
