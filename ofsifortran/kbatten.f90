         MODULE kbatten_f
          USE basictypes_f
		  use kbat_els_F
  	  USE vectors_f
	  use batstuf_f
 
         CONTAINS
! this is KBATTEN.F . A development site for Dynamic Relaxation battens
!
!
! COMPILATION NOTES.
! COMPILE WITH default integer size 4, not 2.
!
!   22/2/96 KBatten_Resids uses datan2, not datan
!    with luck, this will stop battens jamming in the 180 position
!  String battens: Are like fixed battens except that 
!
!  2) RESIDUALS.
!     The axial force is found by a String calc. 
!
  
!*********************************************
! Oct 96 the 'terminate on string doesnt line up is replaced with 
!	count = 0
!	return
!	hopefully this will just remove the batten from the analysis
!

      SUBROUTINE KPrint_One_Batten(u,count,EI,b0,se,zis &
     &  ,eas,Slide_Flag,error)
      IMPLICIT NONE
      INTEGER count,u
      REAL*8 EI(6,*),b0(*)
      INTEGER se(*)		! tri-edges
      REAL*8 zis,eas		! overall length and EA
      INTEGER Slide_Flag

! returns
      LOGICAL error
!locals
      INTEGER J,K,nl 
      character*64 a
      REAL*8 temp
    !  if(.not. kbflag) write(outputunit,*) ' NO Kbflag- shouldnt be here '
      a = 'Noslide'
      temp = zis
      if(Slide_Flag .eq. 1)THEN
        a='slide'
      temp = 0.0D00
      ENDIF
      write(U,'(2(2x,G14.4),2x,a)',err=98) Temp,eas, A
    write(outputunit,*) ' KPrintOneBatten no es'   
      DO J=1,count
    !     nl = se(j) - es(Model) + 1
          nl = se(j)
         write(u,10,err=98) nl,(ei(K,j),K=1,5),b0(j)&
     &  , (0.0D00,K=1,3)  !  ,(xib(K,j),K=1,3)
       ENDDO

10     FORMAT(i5,1x,3(1x,G12.5),2(1x,G12.4),1x,F6.2,3(1x,F8.4) )
      write(u,'(3x,3(1x,F10.4))',err=98) 0.0, 0.0, 0.0 !(xib(K,c+1),K=1,3)
        
      error = .false.  
      return
98    error = .TRUE.
      END  SUBROUTINE KPrint_One_Batten  

!****************************************************************
      SUBROUTINE KMake_One_Batten_Matrices&
     &(list,count,EI,TTU,Ztb,ZIB,Ndm,Df_dx,Dr_Dm,Dr_Dx&
     & ,se,rev,zis,eas,Slide_Flag,sli)
     use saillist_f
	use links_pure_f
	use coordinates_f
	use invert_f
      IMPLICIT NONE
      INTEGER list(*),count    ! Nodes along batten. Count is no EDGES not n nodes
      INTEGER,intent(in) :: NDM
      REAL*8 EI(6,*)	       ! bending stiffness
      REAL*8 TTU(3,3,*)	       ! undeformed transformation matrices
      REAL*8 ZIb(*)
      INTEGER se(*)	       ! The Triedges 
      INTEGER rev(*)	       ! True if the TriEdges are reversed
      REAL*8 zis,eas	       ! overall length and EA 
      INTEGER Slide_Flag
      integer, intent(in) :: sli		
! returns
      REAL*8 ZTB(*)	    ! zts for batten els of the string	
      REAL*8 Df_Dx(NDm,NDm) ! relates force to deflwith moments unchanged
      REAL*8 Dr_Dm(NDm,NDm) ! gets Dtheta from Dmoment to theta with dx=0
      REAL*8 Dr_Dx(NDm,NDm) ! Slave matrix. How much to rotate nodes given 
      			  ! dx's in such a way that moments are un-changed

! REQUIRES . a call to KMAke_One_Batten_Axes immediately  before

      		   
! locals
      REAL*8 GL(12,12)
      INTEGER I,J,L,N
      INTEGER EndR,endC,Kr,Kc,Ig,Jg,Il,Jl , IN(2)
      INTEGER R1,Block, Nr,Nd
      LOGICAL SolveError
      REAL (kind=double), allocatable, dimension (: ) ::zte
      REAL*8 a12(ndm,ndm)
      REAL*8 Xloc(3,2), De(3,count)
      real(kind=double)::l_zt,tqs
      TYPE (edgelist),pointer:: eds
      type(Fedge),pointer ::e  
      eds=>saillist(sli)%edges 
          
      N = NDM			! insulation
!      if(.not. kbflag) write(outputunit,*) 'No Kbflag- shouldnt be here '
        Df_Dx(1:n,1:n) = 0.0d00
        Dr_Dx(1:n,1:n) = 0.0d00
        A12 = 0.0D00
            allocate (zte(count))
! de are unit vectors   THere are ne(=count) of them
! zis is the unstressed batten length
! eas is its overall EA
        DO I = 1,count
          	e=>eds%list(se(i))       
          	if(rev(i).eq.1) THEN
 			    de(1:3,i) =- e%m_DLINK(1:3)
      	 	ELSE
			    de(1:3,i) = e%m_DLINK(1:3)
          	ENDIF
          	zte(I) = e%m_Zlink0 + e%m_delta0
        ENDDO

      if(Slide_Flag.eq. 1) THEN
           CALL One_String_Sliding_Dir_Stiffness(ndm, Df_Dx,de,count,zis,eas)
     	   CALL One_String_T(se,count,0.0D00,zis,eas,tqs,l_zt,.false.,sli) !compression allowed
		   CALL One_String_Geo_Stiffness(ndm, Df_dx,de,count,tqs,zte)
		   ! zte(1:count) is the lendgths of the bars
      ELSE
            write(outputunit,*) ' Battens Must slide!'
       		tqs = 0.0D00
      ENDIF

      ei(6,1:count) = TQS
    deallocate(zte)

! now carry on ensuring force(6) is TQS and ea is 0.0
!
!        BEAM ELEMENT STIFFNESS
!
      Block = Count*3 + 3

      DO L=1,count
       IN(1)=list(L)
       IN(2)=List(L+1)

        DO j=1,2
         Xloc(1:3,J) = get_Cartesian_X(IN(J),sli)
        ENDDO



       IN(1) = L
       IN(2) = L + 1
  
        CALL KPCB_One_Global_Stiffness(ei(1,L),Xloc&
     &,TTU(1,1,L),ZIb(L),GL,ZTb(L)) ! direct only

     
!  Miss out X-component for rotations a12, a22

! a11
       DO  endR=1,2  
          DO Kr = 1,3
               DO  endC=1,2
                 DO Kc=1,3  ! DOF
                  Ig = Kr + 3*IN(endR)-3  ! ROW of global matrix
                  Il = Kr + 6* (EndR-1)   ! ROW of element matrix

                  Jg = Kc + 3*IN(endc)-3  ! COL global matrix
                  Jl = Kc + 6* (Endc-1)   ! COL element matrix
                  Df_Dx(Ig,Jg)= Df_Dx(Ig,Jg) + GL(Il,Jl)

                ENDDO
               ENDDO
            ENDDO
          ENDDO
         
! a12  .  a21  is transpose
       DO  endR=1,2  
          DO Kr = 1,3
               DO  endC=1,2
                 DO Kc=2,3  ! DOF
                  Ig = Kr + 3*IN(endR)-3    ! ROW of global matrix
                  Il = Kr + 6* (EndR-1)     ! ROW of element matrix
                  Jg = Kc + 2*IN(endc)-3     ! COL global matrix
                  Jl = Kc + 6* (Endc-1) + 3 ! COL element matrix
                  A12(Ig,Jg)= a12(Ig,Jg) + GL(Il,Jl)
      	 ENDDO
               ENDDO
            ENDDO
          ENDDO
       
! a22
      DO  endR=1,2  
          DO Kr = 2,3
               DO  endC=1,2
                 DO Kc=2,3  ! DOF
                  Ig = Kr + 2*IN(endR)-3          ! ROW of global matrix
                  Il = Kr + 6* (EndR-1)  + 3      ! ROW of element matrix
                  Jg = Kc + 2*IN(endc)-3           ! COL global matrix
                  Jl = Kc + 6* (Endc-1)  + 3      ! COL element matrix
                  Dr_Dx(Ig,Jg)= Dr_Dx(Ig,Jg) + GL(Il,Jl)

                 ENDDO
               ENDDO
            ENDDO
          ENDDO
      ENDDO
       
      r1 = Block+1	   ! index of rotation start
!      Neq = (count+1) * 6  ! index of last row/col

! here the matrices are the negative of the stiffness matrices. 
! the leading diagonal is all positive

! condensing out the rotations

! assume A    is  |   |   |         | |      |
!                 | F |   | A11 A12 | |trans |
!                 |   | = |         | |      |
!                 |   |   |         | |      |
!                 |tor|   | A21 A22 | | rots |
!                 |   |   |         | |      |
!
!  THen             -1    -1
! trans will be (a11 - a12 A22  a21 )   * F
! and
!      -1
! rot   will be (-A22  A21) F


      nd = r1-1
      nr = (count+1) *2
 
! use Dr_Dm in place of A22inv. 
! and Dr_Dx for now, in place of A22

      CALL invert(nr,Dr_Dx,Dr_Dm,N,solveError)	 ! sb banded

      if (solveerror) THEN
       write(outputunit,*) ' dodgy batten with nedges = ',count
        CALL Mdisp(6,Dr_Dx,'zero pivot in a22 (Dr_Dx)',nr,N)
  
      ELSE

!      DO I=1, nd
!       DO L=1,nd
!        DO J=1,nr
!         DO K=1,nr
!          Df_Dx(i,L) = Df_Dx(i,L) - a12(i,j)* Dr_Dm(j,k)* a12(L,k) ! SLOW. Need MatMul
!         ENDDO
!        ENDDO
!       ENDDO
!      ENDDO
     Df_Dx  = Df_Dx  - matmul(a12,matmul( Dr_Dm,transpose( a12)))       
      ENDIF

!      DO I=1,nr
!       DO K=1,nd
!        Dr_Dx(i,k) = 0.0d00
!        DO J=1,nr
!         Dr_Dx(i,k)= Dr_Dx(i,k) - Dr_Dm(i,j)*a12(k,j)  !a21
!        ENDDO
!       ENDDO
!      ENDDO
       Dr_Dx  =  - matmul(Dr_Dm,transpose(a12))

END  SUBROUTINE KMake_One_Batten_Matrices


SUBROUTINE KMake_One_Batten_Forces&
     &(list,count,EI,se,rev,zisLT,trim,eas,SLide_Flag,sli)
	USE vectors_f
	use links_pure_f
	use coordinates_f
	use saillist_f
! KNUCKLE Version

      IMPLICIT NONE
      INTEGER list(*),count    ! Nodes along batten

      REAL*8 EI(6,*)	       ! bending stiffness
    
      INTEGER se(*)
      INTEGER rev(*)
      REAL*8 zisLT,trim,eas
      INTEGER, intent(in) :: sli, Slide_Flag
! returns
      REAL*8 TQS
! locals
      INTEGER  L , end,N ,eno,ok
      REAL*8 T(3,3)  ,dummy,zis
      LOGICAL error
      REAL*8 cbv(3),L_v(3),zt,fac,l_zt
      REAL*8 m12,m13,m22,m23 ,Floc(3) ,Fg(3) ,vl(3),dr(3)

       TYPE (edgelist),pointer:: eds
      type(Fedge),pointer ::ed ,e2
      eds=>saillist(sli)%edges 

! AXIAL FORCE
        zis = zisLT+trim
      if(Slide_Flag.eq. 1) THEN
        CALL One_String_T(se,count,0.0D00,zis,eas,tqs,l_zt,.false.,sli) 
      ENDIF

DO_L:    DO L=1,count
      	    eno = se(L); ed=>eds%list(eno)
      	    if(Slide_Flag.eq. 1) THEN
        	        Floc(1) = tqs 
      	    ELSE
       	            Floc(1) = eas * ed%m_delta0/ed%m_zlink0
      	    ENDIF

! local axes
            fac = 1.0D00
            if(rev(L).ne. 0) fac = - 1.0D00

            cbv = ed%m_Dlink * fac


            CALL Normalise(cbv,dummy,error)

            CALL KB_Find_TTmatrix(cbv,t)  ! to supercede
      
! END 1
L_1  :       if (L .eq. 1) THEN
                M12 = 0.0D00
                M13 = 0.0D00
            ELSE
        	    fac = 1.0D00
        	    if(rev(L-1).ne. 0) fac = - 1.0D00
                e2=>eds%list(se(L-1))
        	    L_v = e2%m_Dlink * fac

    !        	CALL MULTV (vl,t,L_v) 
			    vl = matmul(t,L_v)  
                 
        	    M12 = - datan2(vl(3),vl(1)) * ei(5,L-1)
        	    M13 = - datan2(vl(2),vl(1)) * ei(5,L-1)
           ENDIF L_1
! END 2
           if (L .eq. count) THEN
       	        M22 = 0.0D00
      	        M23 = 0.0D00
           ELSE
        	    fac = 1.0D00
        	    if(rev(L+1).ne. 0) fac = - 1.0D00
                e2=>eds%list(se(L+1))      
        	    L_v = e2%m_Dlink * fac

			    vl = matmul(t,L_v)
     !       	CALL MULTV (vl,t,L_v)  
                 
      		    M22 = datan2(vl(3),vl(1)) * ei(5,L)
        	    M23 = datan2(vl(2),vl(1)) * ei(5,L)
           ENDIF

           zt = ed%m_zlink0 + ed%m_DELTA0 
           Floc(3) =  (m12 - m22)/zt
           Floc(2) =  (m13 - m23)/zt
	        fg = matmul(transpose(t),Floc)  ! june 2003

! add GL into R	
      fac = 1.0D00 
        DO end=0,1
          	N = list(L+end)
          	dr=fg*fac
            	ok = increment_R(N, dr,sli)  
        	fac=-fac
        ENDDO		   
      ENDDO DO_L ! i think

      END  SUBROUTINE KMake_One_Batten_Forces
!  *****************************************************************
  
#ifdef NEVER
       SUBROUTINE KBatten_RESIDs(Bi,Br)
      IMPLICIT NONE

! OBJECT To return R (residual Forces) to calling function given
!existing rotations and X

      INTEGER*4 Bi(*)
      REAL*8 Br(*)

      integer J,L,N0,Nbatts, K(19)
  
      Nbatts = Bi(1)
            
       DO L = 1,Nbatts
          N0 = bi(L+2)
      	 DO J=1,19
      	 	K(J) = Bi(N0 + j)
      	ENDDO
              
      CALL KMake_One_Batten_Forces(bi(k(2)),bi(k(1)),br(k(3)),bi(k(15)),bi(k(16)),br(k(17)),&
     &   br(k(18)), bi(k(19)))

 	 ENDDO

      END  SUBROUTINE KBatten_RESIDs
#endif
!*******************************************************

SUBROUTINE KPrint_One_Batten_Forces (unit,p_howmuch,list,count,EI,se,rev,zisnotrim,trm, eas,tqs,SLide_Flag,sli)
	use basictypes_f
	use links_pure_f
	use coordinates_f
	use saillist_f
! KNUCKLE Version

      IMPLICIT NONE
      INTEGER Unit
	integer,intent(in) ::p_howmuch
      INTEGER list(*),count    ! Nodes along batten

      REAL*8 EI(6,*)	       ! bending stiffness
    
      INTEGER se(*)
      INTEGER rev(*)
      REAL(kind=double), intent(in):: zisNoTrim,eas,trm
      INTEGER ,intent(in)::Slide_Flag,sli

        REAL*8 TQS
! locals
      INTEGER I,J,L
      INTEGER eno
      REAL*8 T(3,3),dummy,zis
      LOGICAL error

      REAL*8 cbv(3),v(3),zt,l_zt
      REAL*8 m12,m13,m22,m23 ,Floc(3) ,Fg(3) ,vl(3)
      REAL*8 Fac
      
       TYPE (edgelist),pointer:: eds
      type(Fedge),pointer ::ed ,e2
      eds=>saillist(sli)%edges 
  !   if(.not. kbflag) write(outputunit,*) ' NO Kbflag- shouldnt be here '
! AXIAL FORCE
        zis = zisnotrim + trm
      if(Slide_Flag.eq. 1) THEN
        CALL One_String_T(se,count,0.0D00,zis,eas,tqs,l_zt,.false.,sli) 
      ELSE
       tqs = 0.0D00
      ENDIF
	if (p_howmuch .gt. BP_JUSTFORCES) then
                write(Unit,'(a,3(1x,en15.4))')  '(K) zis, trim, eas' ,zis,trm, eas
       		write(Unit,21) ' L','M12','m13','m22','m23 (' ,'axial','shear1','shear2 )','ei(5)'
	else
		write(Unit,'(1x,en13.3,1x,en12.2,1x,\)') zis,trm
	endif

DO_L:    DO L=1,count
      	    eno = se(L); ed =>eds%list(eno)
      	    if(Slide_Flag.eq. 1) THEN
        	    Floc(1) = tqs 
      	    ELSE
       	            Floc(1 ) = eas *ed%m_delta0  /ed%m_zlink0 
      	    ENDIF

! local axes
          fac = 1.0D00
        if(rev(L).ne. 0) fac = - 1.0D00
    
        cbv = ed%m_Dlink  * fac
  		CALL Normalise(cbv,dummy,error)
 
        CALL KB_Find_TTmatrix(cbv,t)

       if (L .eq. 1) THEN
       	M12 = 0.0D00
      	M13 = 0.0D00
       ELSE
        	fac = 1.0D00
        	if(rev(L-1).ne. 0) fac = - 1.0D00
        	e2=>eds%list(se(L-1)); 

        	v = e2%m_Dlink * fac

        	CALL MULTV (vl,t,v)   
            
        	M12 = - datan(vl(3)/vl(1)) * ei(5,L-1)
        	M13 = - datan(vl(2)/vl(1)) * ei(5,L-1)
       ENDIF
! END 2
       if (L .eq. count) THEN
       	M22 = 0.0D00
      	M23 = 0.0D00
       ELSE
        	fac = 1.0D00
        	if(rev(L+1).ne. 0) fac = - 1.0D00
        	e2=>eds%list(se(L+1));   
        	v  = e2%m_Dlink * fac
 

        	CALL MULTV (vl,t,v)  
             
        	M22 = datan(vl(3)/vl(1)) * ei(5,L)
        	M23 = datan(vl(2)/vl(1)) * ei(5,L)
       ENDIF

       zt = ed%m_zlink0  + ed%m_DELTA0 
       Floc(3) =  (m12 - m22)/zt
       Floc(2) =  (m13 - m23)/zt
      
 
	if (p_howmuch .le. BP_JUSTFORCES) then
       		write(Unit,'(EN12.2)')Floc(1) 
 		exit DO_L
	else
		write(Unit,20) L ,M12,m13,m22,m23 ,Floc ,ei(5,L)
	endif
       DO I=1,3
         fg(I) = 0.0D00
         DO J=1,3
          fg(I) = fg(I) + t(j,i)*Floc(J)
         ENDDO
       ENDDO
       ENDDO DO_L
	if(p_howmuch <= BP_NORMAL) return
	write(unit,*) '   NN	x	y	z'
	do L=1,count
		write(unit,23) L, list(l), get_Cartesian_X(list(L),sli)
	enddo
23      format(2i5,3g15.5)
10      FORMAT( a19,5G12.5)
21	FORMAT(a5,4a14,a13,2a12,1x,a12)
20      FORMAT(i5,4G14.4,3h ( ,3G12.3,3h ) ,f14.3)
       END  SUBROUTINE KPrint_One_Batten_Forces
!  *****************************************************************
  

SUBROUTINE KMake_One_Batten_Axes(list,count,TTU,Xib,b0,ZIB,se,sli)
      IMPLICIT NONE
      integer, intent(in out) :: count
      INTEGER list(*)   ! Nodes along batten
       REAL*8 b0(*)	       !betao's
      REAL*8 ZIb(*)
      INTEGER se(*)	       ! The Triedges 
      integer, intent(in) :: sli
          	
! returns
 
      REAL*8 XiB(3,count+1)      ! Initial coords, numbered along batten

      REAL*8 TTU(3,3,count)	 ! undeformed transformation matrices
     		   
	integer :: OK
         
       OK =  PCB_New_XIB(count,list,se,xib,sli)
       CALL KPCB_One_Initial_TT(count,Xib,B0,TTU,Zib)
! Jan 2005 PCB_New_XIB returns 0 if the batten is short.
 ! It would be polite to return an error value so we can notify the user. 
 ! that means making this into a function.

      END SUBROUTINE KMake_One_Batten_Axes


       
      SUBROUTINE KAdd_One_BMasses( XM,Df_Dx,NDM,list,count)
      IMPLICIT NONE
! parameters
      INTEGER NDM
      INTEGER List(*),count
      REAL*8 df_Dx(NDM,NDM)
! returns
      REAL*8 XM(3,3,*) 		! modified

! locals
      INTEGER I,ii,J,K,N
      REAL*8 seed

! NOTE. To add the absed row sum into a node is
!  a) conservative
!  B) changes the principal stiffness direction to the first octant
!  So instead we use the SIGN intrinsic, which preserves the quadrant
      		
      DO ii=1,count+1
 	N = list(ii)
          DO J=1,3
            DO K=1,3
            seed = df_dx((ii-1)*3+j,(ii-1)*3+k )
            DO i = 1,count+1      ! ROW SUM


      	      XM(j,k,n) = xm(j,k,n)&
     &    	      + sign(df_dx((ii-1)*3+j,(i-1)*3+k),seed)
      	     ENDDO
          ENDDO
        ENDDO
      ENDDO
      END   SUBROUTINE KAdd_One_BMasses
#ifdef NEVER
      SUBROUTINE KDefault_Batten(count,list,xib,zis,ModelSSD,sli)
      use coordinates_f
      IMPLICIT NONE
	integer, intent(in) ::modelSSD,sli
      INTEGER list(*), count  ! list is (count+1) nodes
      REAL*8 Xib(3,*)
      REAL*8 Zis

      INTEGER I,K ,L,L1,L2
      REAL*8 C(3), length ,Z
      LOGICAL error,Done 
! method.

! if any XIB is non-zero return
! ELSE assume that we have to fit the batten to the sail. 
! method. 
! Get a vector from the chord in nodecom space
! For each LIST
!	see if an edge is along it
! if so. set XIB
! if NOT, Terminate

      z = Zis
      if (z .GE. 1.0E-06) THEN
       DO I=1,count
          DO K=1,3
           if(Dabs(xib(k,i)) .gt. 1.0D-6) RETURN
          ENDDO
        ENDDO
      ENDIF
      z = 0.0D00

          c  = get_Cartesian_X(list(count+1),sli) -get_Cartesian_X(list(1),sli)
          Xib(1:3,1) = 0.0d00

      CALL normalise(c,length,error)
   
      if (error) CALL RXF_Terminate(' short batten  (KDefault_Batten)')

      DO I=2,count+1
       l2 = list(I)
       l1 = list(I-1)
       done = .false.
      
        DO L=es(Model),ee(Model)
         if (((l12(L,1) .eq. L1) .and. (l12(l,2) .eq. L2)) &
     &      .or. ((l12(L,2) .eq. L1) .and. (l12(L,1) .eq. L2)))  THEN
     		DO K=1,3
                  Xib(K,i) = Xib(k,i-1) + C(k) * Zlink0(L)
      	       	ENDDO
                Z= Z + Zlink0(L)
              Done = .true.
           ENDIF
         ENDDO
       if( .not. Done) Call RXF_Terminate('(battens) No matching edge')

      ENDDO
      if ( zis .lt. 1.0D-6) Zis = Z
      END  SUBROUTINE KDefault_Batten
 
      SUBROUTINE KMake_Node_List(count,se,rev,list)
      use coordinates_f
      IMPLICIT NONE
	  INTEGER :: count
      INTEGER rev(count)
      INTEGER se(count)

!returns
      INTEGER List(count+1)	! node list

      INTEGER i,LL,next	 ,temp(256)
      real(kind=double) ,dimension(3) :: dx

10      rev(1) = 0
      LL = se(1)
       list(1) = L12(LL,1)
       list(2) = L12(LL,2)
       next= L12(LL,2)
       if ((l12(se(2),1).ne.next).and.(l12(se(2),2).ne.next))THEN
         next = L12(ll,1)
         list(1) = L12(LL,2)
         list(2) = L12(LL,1)
         rev(1) = 1
         if((l12(se(2),1).ne.next).and.(l12(se(2),2).ne.next))THEN
               write(outputunit,*) ' (Kbatten 1) String does not line up'
	count=0
	return
         ENDIF
       ENDIF
       
       DO I = 2,count
       LL = se(i)
       if(L12(LL,1).eq. Next) THEN
         
         Next = L12(LL,2)
         list(I+1) = Next
         rev(i) = 0
       ELSE if(L12(LL,2).eq. Next) THEN
         Next = L12(LL,1)
         List(I+1) = Next
         rev(i) = 1
       ELSE      
              write(outputunit,*) ' (Kbatten 2) String does not line up'
	count=0
	return
       ENDIF
       ENDDO
       dx = get_Cartesian_X(List(count+1)) - get_Cartesian_X(List(1)) 
       	if(dx(1).lt. 0.0 ) THEN
      
      	 DO I=1,count
      	 temp(i) = se(i)
      	 ENDDO
      	 DO I=1,count
      	 	se(i) = temp(count-i+1)
             	 ENDDO
      	GOTO 10
      ENDIF
      END SUBROUTINE KMake_Node_List
#endif   
       END MODULE kbatten_f
