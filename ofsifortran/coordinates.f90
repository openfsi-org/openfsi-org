! include 'nodelist_definition.FI'

module coordinates_f
use basictypes_f
 USE, INTRINSIC :: ISO_C_BINDING
integer, parameter :: CONTACT_NONE =0

integer, parameter :: C_GOING_UP    = 1 ! must agree with RXContactSurface.h
integer, parameter :: C_GOING_DOWN = -1
integer, parameter :: CONTACT_HIT  =2
integer, parameter :: CONTACT_MISS =4
integer, parameter :: CONTACT_IS_ON_SURFACE=8
integer, parameter :: CONTACT_HAS_DOFO =16

contains

 
function get_centroid(vertices,sli,base) result(res)
use saillist_f
implicit none
integer, intent(in),dimension(3)	::vertices
integer, intent(in)                  ::sli 
integer, intent(in), optional	::base
real(kind=double), dimension(3)	:: res
integer		:: k
 
!DEC$ IF DEFINED( TRACEPRINT)
    write(outputunit,*) 'SLI isnt present'
 !DEC$ endif
 

		if(present(base)) then
			write(outputunit,*) 'function X - base not coded '
		endif

	if(.true.) then
! 	return a calculated X
	res=0.0
	do k=1,3
		res = get_Cartesian_X( vertices(k),sli)  + res
	enddo
	res=res/3

	endif

end function get_centroid
 

 function zero_r(sli) result (ok)
use saillist_f
implicit none
integer, intent(in)::sli
integer :: ok,j,k
ok=1
   !  write(outputunit,*) 'zero_r with saillist' 
     j =   saillist(sli)%nodes%ncount
     forall (k=1:j)
      saillist(sli)%nodes%xlist(k)%r=0
    end forall
  end function zero_r 
  
function zero_p(sli) result (ok) ! actually sets them to m_nodal_load
use saillist_f
implicit none
integer, intent(in)::sli
integer :: ok,j,k
ok=1
     j =   saillist(sli)%nodes%ncount
     forall (k=1:j)
      saillist(sli)%nodes%xlist(k)%p= saillist(sli)%nodes%xlist(k)%m_Nodal_Load
    end forall
end function zero_p

pure function get_Cartesian_R(n,sli,base ) result(res)  ! R is in global cartesians
use saillist_f
implicit none
integer, intent(in)	::n,sli
integer, intent(in), optional	::base
real(kind=double), dimension(3)	:: res 



!		if(present(base)) then
!			!write(dofounit,*) 'function X - base not coded '
!		endif
		res  = saillist(sli)%nodes%xlist(n)%r
! I think that if its under dofo control we have to dig out the cartesian component of
! the dofo residual for this node and add it. 		
		
		return
	
end function get_Cartesian_R

function get_Raw_r(n,sli,base) result(res)
use saillist_f
implicit none
integer, intent(in)	::n, sli
integer, intent(in), optional	::base
real(kind=double), dimension(3)	:: res
        res  = saillist(sli)%nodes%xlist(n)%r
!		if(present(base)) then
!			write(outputunit,*) 'function X - base not coded '
!		endif
!		res(1) = 0/0

end function get_Raw_r

 subroutine Set_r(n,sli,r) 
use saillist_f
	implicit none
	integer, intent(in)	::n,sli
	real(kind=double),intent(in), dimension(:)	:: r
	saillist(sli)%nodes%xlist(n)%r =r
end subroutine set_r

function increment_r(n,r,sli,base ) result(err)
!DEC$ ATTRIBUTES INLINE :: increment_r
use saillist_f
implicit none
integer, intent(in)	::n,sli
real(kind=double),intent(in), dimension(:)	:: r
integer, intent(in), optional	:: base
type(fnode), pointer	::theNode
integer	:: err
    err=0
    theNode=>saillist(sli)%nodes%xlist(n)
    call increment_r_by_ptr(theNode,r )
end function increment_R

subroutine increment_r_by_ptr(theNode,r )  !cant be pure with OMP
!DEC$ ATTRIBUTES INLINE :: increment_r_by_ptr
use saillist_f
implicit none
type(fnode), intent(inout)	::theNode 
real(kind=double),intent(in), dimension(:)	:: r
integer	:: err

#ifdef DEBUG
     logical:: tl
     tl = omp_test_lock(theNode%m_lock)
     if(tl) then
     write(*,*) ' Node Lock is ON in node ', thenode%nn, 'lockno=',theNode%m_lock, 'in thread ',omp_get_thread_num()
     endif
#endif
     err=0
#ifndef NOOMP
     call OMP_SET_LOCK(theNode%m_lock)
     theNode%r=theNode%r+r
     call OMP_UNSET_LOCK(theNode%m_lock)
#else
     theNode%r=theNode%r+r
#endif
end subroutine increment_r_by_ptr

RX_PURE function get_Cartesian_X_By_Ptr(theNode) result(res)
!DEC$ ATTRIBUTES INLINE :: get_Cartesian_X_By_Ptr
use saillist_f
use dofoprototypes_f
implicit none
type(fnode), intent(in)	::theNode   
real(kind=double), dimension(3)	:: res
	if(IsUnderDOFOControl(theNode)  ) then
          res = Get_DOFO_Cartesian_X(theNode, theNode%masterdofoNo)
         return
	endif
        res = theNode%XXX
end function get_Cartesian_X_By_Ptr

RX_PURE function get_Cartesian_V_By_Ptr(theNode) result(res)
use saillist_f
use dofoprototypes_f
implicit none
type(fnode), intent(in)	::theNode   
real(kind=double), dimension(3)	:: res
	if(IsUnderDOFOControl(theNode)  ) then
      	  res = Get_DOFO_Cartesian_V(theNode)
         return
	endif
	res = theNode%v
end function get_Cartesian_V_By_Ptr

RX_PURE function get_Cartesian_X(n,sli) result(res)
use saillist_f
use dofoprototypes_f
implicit none
integer, intent(in)	::n,sli
real(kind=double), dimension(3)	:: res
    
	if(IsUnderDOFOControl (saillist(sli)%nodes%xlist(n))) then
                res = Get_DOFO_Cartesian_X(saillist(sli)%nodes%xlist(n), saillist(sli)%nodes%xlist(n)%masterdofoNo)
            return
	endif
  
        res = saillist(sli)%nodes%xlist(n)%XXX
end function get_Cartesian_X

function fill_x(n,r,sli,base) result(err)
use saillist_f
use cfromf_f  ! for zeroptr
	implicit none
	integer, intent(in)	::n ,sli
	real(kind=double),intent(in), dimension(:)	:: r
	integer, intent(in), optional	:: base
	integer	:: err

	err=0

		if(present(base)) then
			write(outputunit,*) 'func put_X - base not coded '
		endif

        saillist(sli)%nodes%xlist(n)%XXX =r
!	write(outputunit,*) " Fill_X doesnt zero the nodes ptrs "

	saillist(sli)%nodes%xlist(n)%m_connectSurf=0 ! shielded
	saillist(sli)%nodes%xlist(n)%m_slider=0
end function fill_x
!elemental
 subroutine increment_coord(dx,nn, slii,rflag) ! would be simple if we didnt have contact surfaces
!DEC$ ATTRIBUTES INLINE :: increment_coord
    use cfromf_f
    use ftypes_f
    use dofoprototypes_f
    implicit none
    type(fnode),  intent(inout)	::nn   
    real(kind=double),intent(in), dimension(3)	:: dx
    integer, intent(in)	::slii  
    integer,intent(out)	:: rflag

    integer	:: error,ok, intersectresult
    logical :: l_connect_status
    real(kind=double),dimension(3)	:: intersectPtUVW, perpPointUVW, newptxyz,l_xxprop
    real(kind=double)           	:: k

#ifdef NO_DOFOCLASS
           type (dofo),pointer :: theDofo ! or class
#else
           class (dofo),pointer ::theDofo ! or type
#endif


#ifdef _DEBUG
    if(nn%m_sli .ne. slii) write(outputunit,*)' increment_coord SLI disagree (shouldnt be possible)'
#endif   
	rflag= CONTACT_NONE
	l_connect_status = IsUnderDOFOControl(nn)
	if(l_connect_status) then   !      write(dofounit,*)' shouldnt increment if node has a dofo'
		rflag = CONTACT_HAS_DOFO
		return 
	endif
! An off site may be below, above or to one side of the surface. 
! Its proposed path may pass through the surface or it may miss it. The proposed path may be going down or up.
 !(Down means the velocity DOT the surface normal is negative)

!First, test whether the next point (X + V dt) is below the surface.  
!If (endpt is below surface)
!   If(the path intersects the surface) 
!       Create a DOFO at the first intersection point.  
!       Pass it the velocity and the remaining fraction of the timestep. (see Grab)
!   Else ! we are sneaking in the side 
!      Project the endpt to the surface, create a DOFO there with velocity but with remaining timestep zero.
!   Endif intersection
!Else !end pt is above the surface
!   do the free nodal integration
!Endif

!(Note the current (Nov 2006) intersection code only gets the first one.)
  
  
       
iscs: if(.true. .and. (nn%m_connectSurf.ne.0))  then ! if its an unconnected contact ......... test and adjust
        l_xxprop= nn%XXX + dx
	isblw:  if(0 .ne. contact_test_isbelow( nn%m_connectSurf , l_xxprop, perpPointUVW)) then
!$OMP CRITICAL(contactcreate)
		!write(dofounit,*) ' 0 .ne. contact_test_isbelow', sngl(l_xxprop) !;  write(dofounit,*) ' uvwpt is ',sngl( perpPointUVW)
		rflag=0 
		newptxyz = -999.
! 		returns 0 if no intersection was found, else C_GOING_UP or C_GOING_DOWN
!		*k is the parameter on the line from oldpt to newpt 
                l_xxprop= nn%XXX + dx
                intersectresult = contact_find_intersection(nn%m_connectSurf,nn%XXX, l_xxprop,newptxyz,intersectPtUVW,k)
!		intersectresult = 0; 		
 intrsct:   	if(0 .ne.intersectresult .and. ( k<1. .and. k>0)  ) then
  			! write(dofounit,*) 'contact_find_intersection node',nn%nn  ! THe path goes through with K=' ,k
  	!	write(dofounit   ,'(i6,a,i6,4(1x,f12.5))') kit, ' intrsct    ' , nn%nn, nn%x, k
     !  write(dofodbgunit,'(i6,a,i6,4(1x,f12.5))') kit, ' intrsct    ' , nn%nn, nn%x, k
 			!write(dofounit,*) ' X        is ',nn%x
  			!write(dofounit,*) ' proposed is ',nn%x	+ dx	
  			!write(dofounit,*) ' newptxyz is ',newptxyz	
  			!write(dofounit,*) ' intersectPtUVW is ',sngl(intersectPtUVW)
!               	Create a DOFO at the first intersection point (newptuvw).  
!               	Pass it the velocity and the remaining fraction of the timestep (1-k). (see Grab)
                	theDOFO=> CreateDOFO(slii, C_dofo_SlideFixedSurface ,error) 
                	ok= Complete_SlideFixedSurfaceDOFO(theDOFO, nn,intersectPtUVW, (1.0-k)) 
            	Else intrsct ! we are sneaking in the side
            	!	write(dofounit,'(i6,a,i6,3(1x,f12.5))') kit, ' capture    ' , nn%nn, nn%x
            	!	write(dofodbgunit,'(i6,a,i6,3(1x,f12.5))') kit, ' capture    ' , nn%nn, nn%x
            		nn%m_trace=.true.
!               	newptuvw is the Projection the endpt to the surface, 
!               	create a DOFO there with velocity but with remaining timestep zero.
                	theDOFO=> CreateDOFO(slii, C_dofo_SlideFixedSurface ,error) 
                 	ok= Complete_SlideFixedSurfaceDOFO(theDOFO, nn,perpPointUVW,0.0d00)  
            	Endif intrsct 
	    	rflag=CONTACT_HAS_DOFO
 !$OMP END CRITICAL(contactcreate)   	    	
        else  isblw ! contact miss
                nn%XXX = nn%XXX + dx
		    rflag=CONTACT_MISS
        endif isblw
 	return
	endif iscs
!default behaviour.
    nn%XXX = nn%XXX + dx
end subroutine increment_coord

subroutine set_RXFEsiteptr_ptr(nn,sli, ptr)
use cfromf_f
use saillist_f
implicit none
  
    integer, intent(in) ::nn,sli
    integer(kind=cptrsize) , intent(in) :: ptr  
 !$OMP  CRITICAL (nodalxlist)       
    write(outputunit,*) 'g_NodeList set_snode_ptr'
    saillist(sli)%nodes%xlist(nn)%m_RXFEsiteptr=ptr ! omp
!$OMP END CRITICAL (nodalxlist)    
end subroutine set_RXFEsiteptr_ptr

subroutine set_csurf_ptr(nn,sli,csurf)
USE, INTRINSIC :: ISO_C_BINDING
use cfromf_f
use saillist_f
    integer, intent(in) ::nn,sli
    integer(kind=cptrsize), intent(in) ::  csurf
   
    saillist(sli)%nodes%xlist(nn)%m_connectSurf=csurf  ! omp
   
end subroutine set_csurf_ptr

subroutine set_slider_ptr(n,sli,slidecurve)
    USE, INTRINSIC :: ISO_C_BINDING
    use cfromf_f
    use saillist_f
    use dofoprototypes_f

    IMPLICIT NONE
    integer, intent(in) ::n,sli
    integer(kind=cptrsize), intent(in) ::  slidecurve
    
    integer :: rflag,error, intersectresult,ok
    real(c_double) ::u
    real(c_double),dimension(3) :: perpPt;
#ifdef NO_DOFOCLASS
           type (dofo),pointer :: theDOFO  ! or class
#else
           class (dofo),pointer ::theDOFO! or type
#endif
    saillist(sli)%nodes%xlist(n)%m_slider=slidecurve
    
	rflag=CONTACT_HAS_DOFO

    intersectresult = slidecurve_find_intersection(saillist(sli)%nodes%xlist(n)%m_slider,saillist(sli)%nodes%xlist(n)%XXX,perpPt,U)
  
!$OMP CRITICAL(contactcreate) 

        theDOFO=> CreateDOFO(sli, C_DOFO_SlideFixedCurve ,error) 
        ok= Complete_SlideFixedCurveDOFO(theDOFO, saillist(sli)%nodes%xlist(n),u) 
        
 !$OMP END CRITICAL(contactcreate)    
    
end subroutine set_slider_ptr
    
function  printNodalTrace(sli,u,kit) result(k)
use saillist_f
use dofoprototypes_f
implicit none
integer, intent(in) :: u,kit,sli
integer k,i
type(fnode), pointer ::n
real(kind=double), dimension(3) :: x
 type (doforef) , pointer :: dr

#ifdef NO_DOFOCLASS
     type (dofo),pointer ::d   ! or class
#else
     class (dofo),pointer::d  ! or type
#endif
logical dc
k=0 

do i=1,saillist(sli)%nodes%ncount
	n=>saillist(sli)%nodes%xlist(i)
	if(.not. n%m_trace) cycle
	dc = IsUnderDOFOControl(n)  
	x =   get_Cartesian_X_By_Ptr(n) 
	write(u,'( i6,1x,i6,3(2x,g15.7),1x,l8,3(1x,g15.5) ,$)') kit,i, x,dc,n%v
!	write(outputunit,'( i6,1x,i6,3(2x,g15.7),1x,l8,3(1x,g15.5) ,$)') kit,i, x,dc,n%v
	if(dc) then
	    dr=>n%m_pdofo(1); d=>dr%d  ; write(outputunit,*) ' maybe not only one m_pdofo'
	    write(u,'(a,$)')' dofo_t=  ';
	    write(u,'(1x,g15.6,$)') d%t
	    write(u,'(a,$)')' dofo_v=  ';
	    write(u,'(1x,g15.6,$)') d%vt
!	    write(outputunit,'(a,$)')' dofo_t=  ';
!	    write(outputunit,'(1x,g15.6,$)') d%t
!	    write(outputunit,'(a,$)')' dofo_v=  ';
!	    write(outputunit,'(1x,g15.6,$)') d%vt
	endif
	write(u,*)' eol'
!	write(outputunit,*)' eol'
	k=k+1
	enddo

end function  printNodalTrace 

end module coordinates_f
