!
!	links_pure.f contains string and link functions that do not require strng com. 
! 	they still use edgecom and nodecom, though
!
!
!



module links_pure_f
use basictypes_f
contains

function SP_Check_One_String_Reversal(sli,Ne,rev,se) result(iret)
use saillist_f
      IMPLICIT NONE
   	integer,intent(in) :: sli
    integer,intent(inout) :: ne	
	integer, intent(inout), dimension(ne) :: se
	logical, intent(inout), dimension(ne) :: rev

      INTEGER i,LL,next,k,edge,iret
      TYPE (edgelist),pointer:: eds
      type(Fedge),pointer ::e,e2
      eds=>saillist(sli)%edges
	iret=0
	if(ne .lt. 1) return  !  Nov 2003 
	iret=1
       if(Ne .GT. 1) THEN
           rev(1) = .false. 
           LL = se(1); e =>eds%list(LL)
           next = e%m_L12(2)
       !    next= L12(LL,2)
                e2=>eds%list(se(2))
    !       if ( (l12(se(2),1).ne.next).and.(l12(se(2),2).ne.next))THEN
            if ( (e2%m_L12(1).ne.next).and.(e2%m_L12(2).ne.next))THEN          
	            next =  e%m_L12(1)
	            rev(1) = .TRUE.
!	            if((l12(se(2),1).ne.next).and.(l12(se(2),2).ne.next))THEN
 	            if((e2%m_L12(1).ne.next).and.(e2%m_L12(2).ne.next))THEN
	            write(outputunit,*)  'Edges in String not contiguous'

                do k=1, ne
		            edge = se(k)
		         !   write(outputunit,*)' edge ' !, L12(edge,1) ,'to ',L12(edge,2), rev(k)
	            enddo
	            ne = 0; iret=0
	        ENDIF
       ENDIF
       DO I = 2,Ne
           LL = se(i); e =>eds%list(LL)
           if(e%m_L12(1).eq. Next) THEN
                Next = e%m_L12(2)
                rev(i) = .FALSE.
           ELSE if(e%m_L12(2).eq. Next) THEN
	             Next = e%m_L12(1)
	             rev(i) = .TRUE.
           ELSE   
	            write(outputunit,*)  ' String does not line UP '
                do k=1, ne
		            edge = se(k)
		        !    write(outputunit,*) ' edge ', L12(edge,1) ,'to ',L12(edge,2), rev(k)
	            enddo
	            ne = 0; iret=0
           ENDIF
       ENDDO
       else        ! only one edge in this string
	rev(1) = .FALSE.
       ENDIF
	iret = ne ! removed Dec 6 2003
END FUNCTION SP_Check_One_String_Reversal
#ifdef _DEBUG
SUBROUTINE One_String_Geo_Stiffness(nd,D,de,ne,tq,zte)
#else
pure SUBROUTINE One_String_Geo_Stiffness(nd,D,de,ne,tq,zte)
#endif

IMPLICIT NONE
    
!* parameters
    INTEGER, intent(in)                      :: ND        ! dimension for D
    INTEGER, intent(in)                      :: Ne        ! number in e(*)
    REAL(kind=double), intent(in)            :: tq 

    REAL(kind=double), intent(in), dimension(3,ne)  :: de   ! the unit vectors
    real(kind=double), intent(in), dimension(ne) :: zte     
!*returns
    REAL (kind=double) ,intent(inout) , dimension(nd,nd) :: D  ! we use 3*(ne+1) square

      INTEGER I,j,k
      REAL*8 cbv(3) ,s(6,6),zis  

      DO I=1, Ne
	   CBV  = De(1:3,i)*zte(i) ! =(Zlink0(edge(I))+ Delta0(edge(I)))  
       Zis = zte(I)  ! CHANGE  may 2008  was = Zlink0(edge(i))
       CALL One_Link_Global_Stiffness(CBV,Tq,0.0D00, s,ZIs)
       if(any(isnan(s))) then
            !  k=k
             s=0
       endif
       DO J=1,6
       DO K=1,6
       d(3*(i-1)+j,3*(i-1)+k) = d(3*(i-1)+j,3*(i-1)+k)+ s(j,k)
       ENDDO
       ENDDO
      ENDDO
END SUBROUTINE One_String_Geo_Stiffness

PURE function den(de,ned,j) result (v)
      IMPLICIT NONE
!* parameters
      INTEGER, intent(in) ::  NED                         ! number of edges
      REAL(kind=double),intent(in),dimension(3,*)::de    ! the unit vectors
       INTEGER, intent(in) ::  J                         ! Node number. Node 1 is at start of edge 1 
!*returns.
!  DE(after) - DE(before)  except at first and last
       REAL (kind=double) , dimension(3) :: V 
       
       if(j .eq. 1) then
           V = de(1:3,j)        
           return
       endif
       if(j .gt. ned) then
           V =  - de(1:3,j-1)       
           return
       endif
       V = de(1:3,j) - de(1:3,j-1)
end function den
#ifdef _DEBUG
SUBROUTINE One_String_Sliding_Dir_Stiffness(ND,D,de,ned,zi,ea) !remember our convention that D = MINUS  dr/dx
#else
PURE SUBROUTINE One_String_Sliding_Dir_Stiffness(ND,D,de,ned,zi,ea) !remember our convention that D = MINUS  dr/dx
#endif
use vectors_f
      IMPLICIT NONE
!* parameters
      INTEGER, intent(in) ::  ND                         ! dimension for D
      REAL(kind=double),intent(in),dimension(3,*)::de    ! the unit vectors
      INTEGER , intent(in) :: 				Ned          ! number in e(*)
      REAL (kind=double), intent(in) :: 	EA, zi      ! 
!*returns.
! sums the stiffness into D
        REAL (kind=double) ,intent(inout) , dimension(nd,nd) :: D  ! we use 3*(ne+1) square	      
!* THEORY

!* a string of links has the same geometric stiffness as individual links
!* but the intermediate node i has direct stiffness as follows:
!*
!*        EA/(sigma[ZI]) ( dj (dj-di)  - di ( dj-di)
!* here dj is the direction cosines of the link just before,
!* and di  is the direction cosines of the link just after

!*locals
    INTEGER I,j,r,c
    REAL(kind=double),dimension(3,ned+1):: a         
    REAL*8  Bkl  
    Bkl = EA/ZI
#ifdef _DEBUG    
    DO i=1,ned   ! I is NODE
        if(abs(dot_product(de(1:3,i)  ,de(1:3,i))-1.0)>1e-10) then
            write(outputunit,*) ' non-unit vector in One_String_Sliding_Dir_Stiffness',  de(1:3,i)
        endif
    enddo
#endif  
    if(ned <1) then
     return
    endif  
    DO i=1,ned+1  ! I is NODE
        a(1:3,i) =  den(de,ned,i)
    enddo
   
    DO i=1,ned+1  ! J is NODE
        r = 3*i-2
        DO J=1,ned+1  ! J is NODE
            c = 3*j-2
            d(r:r+2,c:c+2) = d(r:r+2,c:c+2)	+ Outer_Product (a(1:3,i),a(1:3,j),3)     * bkl
        ENDDO
    ENDDO
   END  SUBROUTINE One_String_Sliding_Dir_Stiffness
   
   
PURE SUBROUTINE One_String_Dir_StiffnessSSD(ND,D,de,ned,zi,ea)
      IMPLICIT NONE
    
!* parameters
      INTEGER, intent(in) ::  ND
   
      REAL(kind=double), intent(in), dimension(3,*) ::	de    ! the unit vectors
      INTEGER , intent(in) :: 				Ned        ! number in e(*)
      REAL (kind=double), intent(in) :: 		EA, zi 

!*returns
        REAL (kind=double) ,intent(inout) , dimension(nd,nd) :: D  ! we use 3*(ne+1) square	      
!* THEORY

!* a string of links has the same geometric stiffness as individual links
!* but the intermediate node i has direct stiffness as follows:
!*
!*        EA/(sigma[ZI]) ( dj (dj-di)  - di ( dj-di)
!* here dj is the direction cosines of the link just before,
!* and di  is the direction cosines of the link just after

!*locals
      INTEGER I,j,k,nr
         
       REAL*8  Bkl ! , V(StrngLD3,StringLD) 
       real(kind=double), allocatable, dimension(:,:) :: V 

      Bkl = EA/ZI
      nr = 3*(ned+1)
  	allocate(v (nr,ned))   
      DO J=1,ned
	I = J-1
	DO K=1,3
	 V(  3*i+k,J) = - De(k,i+1)
	 V(3+3*i+k,j) =   De(k,i+1)
	ENDDO
	V(1:3*i,j) = 0.0D00
	V(3*i+7:nr , J) = 0.0D00
      ENDDO

	d(1:nr,1:nr) = 	d(1:nr,1:nr) + matmul(v(1:nr,1:ned), Transpose(v(1:nr,1:ned)))  * bkl
 	deallocate(v)
   END  SUBROUTINE One_String_Dir_StiffnessSSD

 function One_Bar_Tension(sli,n1,n2,zi,ea,ti,d,zt) result (tql)
	use vectors_f
	use coordinates_f
      IMPLICIT NONE
	integer, intent(in) :: sli,n1,n2
	real (kind=double) , intent(in) :: zi,ea,ti
	real (kind=double) ,intent(out), dimension(3) :: d	!  unnormalised chord vector
	real (kind=double) ,intent(out) :: zt
	real (kind=double) :: tql

	  D =get_Cartesian_X(n2,sli)-get_Cartesian_X(N1,sli)
	  ZT= norm(d)
	  TQl=EA *(zt -ZI)/ZI +TI
end function One_Bar_Tension

 SUBROUTINE One_String_T(se,ne,tis,zis,eas,tq,zt,bucklepermitted,sli) ! also used by battens
      use saillist_f
      IMPLICIT NONE
!* parameters
      INTEGER se(*), ne         ! the edges
      REAL(kind=double), intent(in) :: tis,eas,zis
      LOGICAL,intent(in) :: bucklepermitted		! false if compressive stress allowed
      integer, intent(in):: sli
!* returns
      REAL(kind=double), intent(out) :: TQ,zt

!* locals
      TYPE (edgelist),pointer:: eds
      eds=>saillist(sli)%edges

!* delta0 are the extensions of the edges ( in length units, not strain)

       zt = SUM( eds%list(se(1:ne))%m_Zlink0 + eds%list(se(1:ne))%m_Delta0 )

       TQ = TIS + (zt-zis) /ZIS * EAS
       if ((bucklepermitted) .and.(TQ .LE. 0.0D00)) then
        TQ=0.0D00
       endif

END  SUBROUTINE One_String_T
#ifdef _DEBUG
SUBROUTINE One_Link_Global_Stiffness(D,Tension,EA, s,ZI)
#else
pure SUBROUTINE One_Link_Global_Stiffness(D,Tension,EA, s,ZI)
#endif

	use vectors_f
	use math_f
      IMPLICIT NONE
    
!* Parameters
	real(kind=double),intent(in),dimension(3) ::  D ! direction components UnNormalised
	real(kind=double),intent(in) ::  Tension,EA,ZI
!*returns

      	real(kind=double),intent(out),dimension(6,6) ::  s
!*locals
	real(kind=double) :: ttt   	
      integer e1,e2,la,j,k ,row,c
      REAL*8 dr(3,3),gm(3,3)
      REAL*8  ZTsq,zt

      ZTsq = dot_product(d,d)
      zt=sqrt(ztsq)
    if(zt < 1e-10) then
        s=0.
        do k=1,6
            s(k,k) = EA/ZI
        enddo
        return
    endif
      TTT = EA / (ZTsq * ZI)   ! EA/l^3 was zt zt zi

      DO  LA=1,3
      DO  J=1,3
      DR(la,j)=TTT*D(J)*D(LA)     ! DIRECT STIFFNESS
      GM(la,j) = TENSION /ZT * (dble(Kdel(LA,J) ) - D(LA)*D(J)/ ZTsq)
      ENDDO
      ENDDO

      DO e1=0,1
      do e2=0,1
      do j=1,3
      do k=1,3
      row=3*e1+j
      c=3*e2+k
      s(row,c)= -(1.0d00-2.0d00*float(kdel(e1,e2)))*(dr(j,k)+ gm(j,k))
      ENDDO
      ENDDO
      ENDDO
      ENDDO
      
 END SUBROUTINE One_Link_Global_Stiffness
      
      
end module links_pure_f
