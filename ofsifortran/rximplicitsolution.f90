module solveimplicit_f
 
	USE, INTRINSIC :: ISO_C_BINDING
      IMPLICIT NONE	
 ! values forMatrixType,   this must match the definitions in rxSparseMatrix1.h
integer, parameter :: matrixType_Jay= 2  ! RXM_RECTANGLE
integer, parameter :: matrixType_Sraw=128 ! RXM_STRUCTSYM, not RXM_POS_DEF
integer, parameter :: matrixType_RHS= 2   ! RXM_RECTANGLE
      
contains
subroutine Analysis_Prep() bind(C,name="analysis_prep" )
use saillist_f
use tris_f
use dofolist_f
use dofo_f
use connects_f
implicit none
integer OK,sli,k
#ifdef PETERDEV
    integer  err, SomethingChanged
    err=0; SomethingChanged=0;
    write(*,*) ' Analysis_Prep'
#endif
    ok=HookAllElements()

     DO sli=1,sailcount
        if(.not. is_active(sli)) cycle
!           k=zero_r(sli) aug 2013 not sure this is needed and it stuffs up residual visu
           if(associated(saillist(sli)%nodes%xlist ) ) then
           saillist(sli)%nodes%xlist%ErrIndex=0.0
           endif
           call AreaCalc(sli)    ! just to post the totals
    ENDDO
      call UpdateAllTeesBySV()   ! in analysis prep
  !    Call SyncDofos()
    call Resolve_Connects(k)
!     write(*,*) ' (resolveConn gave )' ,k

    CALL Set_Connect_Relations()
    call UpdateAllTeesBySV()   ! in analysis prep

    do sli=1,sailcount
         if (is_hoisted(sli))  call DofoAllJacobians(sli,k)
    end do
end subroutine Analysis_Prep


subroutine SolveImplicit(Ktot,pTmax,iexit,p_VDamp) 
 USE IFPORT
    use saillist_f
    use tris_f
    USE gravity_f
    use dofoprototypes_f
    use cfromf_f
    USE connects_f
    use feaprint_f
    USE nograph_f
    use rx_control_flags_f
    USE basictypes_f
    use parse_f
     use ttmass_f , only :ALL_LOADING
    IMPLICIT NONE

    INTEGER(C_INT),intent(out):: Iexit 		! return 0=OK, 1=not conv, 2=user abort
    INTEGER(c_int), intent(in):: Ktot		! PARAMETER. No of cycles this run
    REAL(kind=8) ,intent(in) :: pTmax			! convergence limit in Newtons

    real (kind=double) ,intent(in) , volatile::  p_vdamp  ! Vdamp is gravitational acceleration

! locals
    real(kind=double)  :: tcon,maxdisp,displimit, tconlast
    real(kind=double) ,dimension(3) :: l_acceleration,dum,rmax
    real(C_DOUBLE), allocatable, dimension(:) :: d
    integer::sli,kit,i,k,ok,LIslimax ,rmaxN
    character *512 path,fname

    integer(kind=cptrsize)  :: m,jay ,rhs,sparsestiff,sparsemass ! the global matrices
    integer :: flags, nrhs, solverResult  ! if <0 we get out and drop back to explicit
    real (kind=4) :: l_startSecTime,l_thisSecTime  
    real (kind=double) :: l_startcputime,l_thiscputime ,rcapvalue,maxD
    logical ::  LimitGrowth ,ExternalSolver
     real(kind=double) :: growthfactor 
     growthfactor=5.0
!Overall structure is something like:
!1) set the flags
!2) Do the hookups and connects
!2) measure  residuals
! assemble the stiffness matrix	
! solve for displacements
! all the stroona stuff for managing the step
! its mainly the ROTATIONS which need to be limited to avoid cosine explosion.
! do the each-cycle stuff
! loop to (2) if the tcon is big.

!*****************************************

!C  the damping flag causes current residual to be replaced by a
!C  combination of it and its previous
!C
dum=0

    displimit=0.0001; ! for want of anything better
    call check_cl('DC',DrawEachCycle)
    call check_cl('LV',LimitV)
    call check_cl('SV',SVD_Solver)  ! a nasty idea
    call check_cl('LG',LimitGrowth)
    forall(sli=1:sailcount, is_hoisted(sli))
     saillist(sli)%active_Originally = is_active(sli)
    end forall
    
    l_acceleration = 0.0 ;    l_acceleration(3) = p_vdamp  !   TEMP for Janet but its better to use a gravity field
     iexit = 0; KIT= 0;    K=2                 ! for call to akm_update_display

    call  Analysis_Prep()! shoulnt be needed as BringAll does it already
    saillist%tcon_this = 2.0 * pTmax

    g_converged=.TRUE.
    tcon = 2.0 * pTmax
    write(outputunit,'(a)') 'OFSI implicit solver'
 
    CALL  Write_All_Flags(outputunit)
 
      write(outputunit,1503)
      write(outputunit,52)0,0,0,pTmax
52     FORMAT(1H ,4(2X,G11.4))

1503   FORMAT('     TIME         MASS        ADDED        MAX  ',/ &
        ,'    INTERVAL     FACTOR       MASS       RESIDUAL')
        call check_cl('EX',ExternalSolver)
        call check_cl('D3',pleaseprint)
        if(pleasePrint) then
                k=  All_Loading(l_acceleration)! call defineLoading(sailcount,l_acceleration )
                ok=HookAllElements()
               iexit = 999
            return
        endif 
        
        CALL cpu_time(l_StartCpuTime)
        l_StartSecTime = secnds(0.0);
!       START OF OUTER LOOP
!
!*****************************************************
mainloop:do while( kit<ktot .or. .true. ); kit=kit+1
   maxdisp=0
	k=2; k=check_messages(k);   ok=HookAllElements() 
    if (Stop_Running .NE. 0) exit mainloop
 
! all this sums the internal and external loads into node%R and dofo%rt
    i=  All_Loading(l_acceleration)
    do sli=1,sailcount
        if(.not. is_active  (sli)) cycle
        ok=zero_r(sli);
        do  i = 1,saillist(sli)%nodes%ncount
            saillist(sli)%nodes%xlist(i)%Rold = get_Cartesian_R(i,sli)
            call Set_R(i,sli, saillist(sli)%nodes%xlist(i)%P )
        enddo
      ENDdo
  
! RESIDUALS

      k = DOFO_TestForCapture(kit) ! test for far side.  Cant test for scoot here
   
     k = DOFO_UpdateJay(  )  ! experiment to move this before all residuals
     CALL All_Residuals()
 ! should test for release after
      k = DOFO_TestForRelease( )
!  end of all this sums....
      tcon= measureResiduals(LIslimax, rmaxN,rmax,.true.)  ; tconlast=tcon
      
      if (tcon < pTmax) then
          call drawonreset(kit,tcon ,maxdisp,displimit,0,dum,l_thisCpuTime,l_ThisSecTime,0.0d00)
          write(outputunit,*) 'max resid is ', tcon, '< ',pTmax,' so finish'
          exit mainloop ; 
      endif

    call SetModelRowIndices() ! set fnodeRowIndex regardless of whether UDC
    if(LimitV .and. residualCap>0) then
        rcapvalue=residualCap
        write(outputunit,*) 'using residualcap = ',rcapvalue
    else
        rcapvalue=-1
    endif   
    call FormJayMatrix(jay,rhs,nrhs,rcapvalue)! does RHS too
    call FormStiffnessMatrix(m,0)

    call check_cl("VT",pleaseprint ); 
    if(pleaseprint)    call varioustests(sparsestiff,sparsemass, 1)

        allocate(d(0:nrhs-2),stat=k);
        if(k ==0) then
            d=0
            flags=0
            if(.not. ExternalSolver) then  ! flag '/EX'
                solverResult= cf_SparseSolveLinear(m,jay,rhs,d,flags)  ! d are disps
            else
                path = ' '
                call getcurrentdir(path,510); 	call denull(path)
                write(fname,'(a,a,a)') "$R3CODE/bin/externalsolver.sh  ",TRIM(path)," globalSmatrix.RUA globalrhsMatrix.RRA globalJayMatrix.RRA solveresult.txt"

               solverResult= 0;
               call cf_WriteSparseMatrix(m, "globalSmatrix.rsa"//char(0))
               call cf_WriteSparseMatrix(rhs, "globalrhsMatrix.rua"//char(0))
               call cf_WriteSparseMatrix(jay, "globalJayMatrix.rua"//char(0))
               i= system(fname);
               If (I .eq. -1) then
                   k = ierrno( )
                   print *, 'External Solver gave Error ', k
               end if
               open(unit=61,FILE="solveresult.txt")
               read (61,*,iostat=i) d
               close(61)
        endif


        else ! cant allocate for D
            solverResult=-10
        endif
     
        call check_cl("VT",pleaseprint ); 
        if(pleaseprint) then
             open(101,name="pardisodefls.txt")
              write(101,'(g21.14)') d
              close(101,status='keep')
             endif  

        if(solverResult /=0) then
        
            write(outputunit,*)  ' drop out of IM due to failed solver '
            call  cf_DestroySparseMatrix(m)
            call  cf_DestroySparseMatrix(jay)
            call  cf_DestroySparseMatrix(rhs) 
            deallocate(d)
            iexit=-1; return
        endif
! the stroona way is to apply the smaller of (1-df) and (c_factor2/maxD if <1)
! decide whether to apply the full displacement	
    if(DampingFactor>0) then
         write(outputunit,*)' apply dampingfactor ',DampingFactor 
         d=d*(1.0-DampingFactor)    
     endif
    if(LimitV) then
            maxD=maxval(abs(d)); !write(outputunit,'("max disp before clip=",g12.4," ",$)',iostat=k),maxD
            if(maxD>c_factor2) then
                d = d * c_factor2/maxD
            endif
    endif
! apply whatever heuristics. 
	maxdisp = maxval(abs(d));!write(outputunit,'("max disp=",g12.4)',iostat=k),maxDisp
 	  CALL cpu_time(l_thisCputime); l_thisCputime=l_thisCputime- l_startCputime
  	  l_ThisSecTime = secnds(l_StartSecTime);	
  	  dum=0
  	  if(LIslimax>0) then
  	         if(saillist(LIslimax)%nmax>0) dum= get_Cartesian_X(saillist(LIslimax)%nmax,LIslimax)
                 call drawonreset(kit,tcon ,maxdisp,displimit,saillist(LIslimax)%nmax,dum,l_thisCpuTime,l_ThisSecTime,0.0d00)
	    else
                 call drawonreset(kit,tcon ,maxdisp,displimit,0,dum,l_thisCpuTime,l_ThisSecTime,0.0d00)
	    endif 

        call ApplyDisplacements(d,.true.)
    if(LimitGrowth) then  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 lg:do k=1,10
            CALL All_Residuals()
            tcon= measureResiduals(LIslimax ,rmaxN,rmax,.true.)
            if(tcon < tconlast*5.0)  exit lg 
            d = -d/2.0;
            call ApplyDisplacements(d,.true. )
            d=-d     
    enddo lg
    endif
    call check_cl('EM',pleaseprint)
    if(pleasePrint) then
        call cf_WriteSparseMatrix(m, "globalSmatrix.rsa"//char(0))
        call cf_WriteSparseMatrix(rhs, "globalrhsMatrix.rua"//char(0))
        call cf_WriteSparseMatrix(jay, "globalJayMatrix.rua"//char(0))
    endif
    deallocate(d);
    call  cf_DestroySparseMatrix(m)
    call  cf_DestroySparseMatrix(jay)
    call  cf_DestroySparseMatrix(rhs)
    
    if(debug1) then
        if(kit .lt.2)  write(outputunit,'(a,$)')'R'
        k = printNodalTrace(sli=1,u=outputunit,kit=kit)
    endif
    if(DrawEachCycle) k = cf_postprocess()
    
    call check_cl('D5',pleaseprint)
    if(pleasePrint) then
            write(outputunit,*) ' getting out on /D5 with kit=',kit
            ok=HookAllElements() 
            CALL Set_Connect_Coords();  
            exit mainloop   
    endif 	
	if((maxdisp < displimit).and. (tcon< pTmax)) then; iexit=0; exit mainloop;endif
	if (Stop_Running .NE. 0) then;
	     iexit=2 ; write(outputunit,*) ' stop requested '
	      exit mainloop;  
	endif
enddo mainloop

! reset the active flags. was 	active = active_Originally
    do  sli=1,sailcount
    if( is_hoisted(sli))ok=Set_Active(sli,saillist(sli)%active_Originally)
    enddo

  
ifhc: IF(HardCopy) THEN
       ! call UpdateAllTeesBySV()  ! so dofoPrint is current
        CALL  PrintGlobalStuff(60)
        k=Windage_Loads(GPRINT)
        DO sli = 1,sailcount
            if(.not. is_Active(sli)) cycle
             CALL Print_Model(sli )
             CALL Print_Model_As_PC(sli )
        ENDDO
      ENDIF ifhc

    k= Draw_Resids(pTmax)
end subroutine SolveImplicit
!
! ApplyDisplacements and FormJayMatrix must use the same counting method
! DerivativeList and JaySize too
!

function JaySize() result (c) bind(C,name="cf_JaySize")
    USE ftypes_f
    use dofoprototypes_f
    use saillist_f
    IMPLICIT NONE

    integer :: c

    TYPE ( nodelist) ,POINTER :: nodes
    TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif
    type(fnode), pointer :: theNode
    INTEGER ::  sli,N, i
    c=1
sl: DO sli=1,SailCount
    if (0== is_Hoisted(sli) ) cycle
    if (0== is_active(sli)) cycle
    nodes=>saillist(sli)%nodes
    if(.not. associated(nodes%xlist)) cycle

dn:     DO  N=1,nodes%ncount
            thenode=> nodes%xlist(n)
            if(.not. thenode%m_Nused)   cycle
            if(IsUnderDOFOControl(theNode)  ) cycle
            if(theNode%m_ElementCount ==0)   cycle
            c=c+3
      ENDDO dn
    Dofos=>saillist(sli)%Dofos
    if(.not. ASSOCIATED(Dofos%list)) cycle
dfo:	do i =1, dofos%count
            if(.not. Dofos%list(i)%used) cycle
            d=>Dofos%list(i)%o
            if(associated(d%child)) cycle
            if (.not. associated(d%m_Fnodes )) cycle;
            c=c+ d%ndof
    enddo  dfo
    ENDDO sl
end function JaySize

function MeasureKE(yin,Nv)  result (ekt) bind(C,name="cf_MeasureKE")
USE, INTRINSIC :: ISO_C_BINDING
use basictypes_f
use saillist_f
use coordinates_f
use dofoprototypes_f
implicit none
integer(c_int), intent(in), value ::Nv ! X then V
real(c_double), intent(in), dimension(Nv) ::yin ! X then V

real(C_DOUBLE)  :: ekt
!
!locals
    TYPE ( nodelist) ,POINTER :: nodes
    TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif
    INTEGER ::  sli, n, i,nn,vSize
    type(fnode), pointer :: theNode
    TYPE (Fnode), DIMENSION( :), POINTER :: nlist

    ekt=0

        vSize=Nv/2
    ASSOCIATE(    X=> yIn(1:vSize),VeeIn=>yIn(vSize+1:2*vSize)  )

 ! procedure:
 ! npack the X and copy their values to nodal XXX anf Vee, and to dofo Tee

        call  ApplyDisplacements(X,.false. )
       call  ApplyVelocities(VeeIn) ! slow to do this here
    end associate

sl:	DO sli=1,SailCount
        if (0== is_Hoisted(sli) ) cycle
        if (0== is_active(sli)) cycle
        nodes=>saillist(sli)%nodes
        if(.not. associated(nodes%xlist)) cycle
        nlist=>saillist(sli)%nodes%xlist
        nn = nodes%ncount
!$OMP PARALLEL  PRIVATE (N,theNode) shared(nlist,nn) DEFAULT(none) reduction(+ : ekt)
!$OMP DO SCHEDULE(guided)
dn:     DO  N=1,nn
            thenode=> nlist(n)
            if(.not. thenode%m_Nused)   cycle
            if(IsUnderDOFOControl(theNode)  ) cycle
            if(theNode%m_ElementCount ==0)          cycle
                ekt = ekt+  theNode%v(1)**2 * theNode%xm(1,1) + theNode%v(2)**2 * theNode%XM(2,2) &
                + theNode%v(3)**2 *theNode%xm(3,3) &
                + 2* theNode%xm(1,2)*theNode%v(1)*theNode%v(2) + 2*(theNode%xm(1,3)*theNode%v(1)&
                + theNode%xm(2,3)* theNode%v(2) )*theNode%v(3)
         ENDDO dn
!$OMP END DO NOWAIT
!$OMP END PARALLEL
        Dofos=>saillist(sli)%Dofos
        if(.not. ASSOCIATED(Dofos%list)) cycle
dfo:	do i =1, dofos%count
            if(.not. Dofos%list(i)%used) cycle
            d=>Dofos%list(i)%o
            if(associated(d%child)) cycle
            if(d%ke>0) ekt =ekt+d%ke
        enddo  dfo
        ENDDO sl
end function MeasureKE

subroutine zeroVelocity() bind(C,name="cf_zeroVelocity")
USE, INTRINSIC :: ISO_C_BINDING
use basictypes_f
use saillist_f
use coordinates_f
use dofoprototypes_f
implicit none
!
!locals
    TYPE ( nodelist) ,POINTER :: nodes
    TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif
    INTEGER ::  sli, n, i,nn
    TYPE (Fnode), DIMENSION(:), POINTER :: nlist

sl:	DO sli=1,SailCount
        if (0== is_Hoisted(sli) ) cycle
        if (0== is_active(sli)) cycle
        nodes=>saillist(sli)%nodes
        if(.not. associated(nodes%xlist)) cycle
        nlist=>saillist(sli)%nodes%xlist
        nn = nodes%ncount
!$OMP PARALLEL  PRIVATE (N) shared(nlist,nn) DEFAULT(none)
!$OMP DO SCHEDULE(guided)
dn:     DO  N=1,nn
            nlist(n)%v=0
         ENDDO dn
!$OMP END DO NOWAIT
!$OMP END PARALLEL
        Dofos=>saillist(sli)%Dofos
        if(.not. ASSOCIATED(Dofos%list)) cycle
dfo:	do i =1, dofos%count
            if(.not. Dofos%list(i)%used) cycle
            d=>Dofos%list(i)%o
            if(associated(d%child)) cycle
           if(d%ke>0) d%ke=0.
          if(associated(d%vt) ) d%vt=0.
        enddo  dfo
        ENDDO sl
end subroutine zeroVelocity

! EXTERN_C void cf_PackResiduals(double*r);
subroutine PackResiduals(R ) bind(C,name="cf_PackResiduals")
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f
    use saillist_f
    use ttmass_f
    use coordinates_f
    use dofoprototypes_f
    implicit none

    real(C_DOUBLE), dimension(*), intent(out) :: R
!
!locals
    TYPE ( nodelist) ,POINTER :: nodes
    TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif
    INTEGER ::  sli, c,n, i
    type(fnode), pointer :: theNode

     c=1  ! THE ALL-IMPORTANT COUNTER  of col number Must be the same in FormJayMatrix
sl:	DO sli=1,SailCount
        if (0== is_Hoisted(sli) ) cycle
        if (0== is_active(sli)) cycle
        nodes=>saillist(sli)%nodes
        if(.not. associated(nodes%xlist)) cycle
!!!$OMP PARALLEL  PRIVATE (N,k,theNode) shared(nlist,nnn,sli) DEFAULT(none)
!!!$OMP DO SCHEDULE(guided)
dn:     DO  N=1,nodes%ncount
            thenode=> nodes%xlist(n)
            if(.not. thenode%m_Nused)   cycle
            if(IsUnderDOFOControl(theNode)  ) cycle
            if(theNode%m_ElementCount ==0)          cycle
            R(c:c+2) = theNode%r
            c=c+3

         ENDDO dn
!!!$OMP END DO NOWAIT
!!!$OMP END PARALLEL
        Dofos=>saillist(sli)%Dofos
        if(.not. ASSOCIATED(Dofos%list)) cycle
dfo:	do i =1, dofos%count
            if(.not. Dofos%list(i)%used) cycle
            d=>Dofos%list(i)%o
            if(associated(d%child)) cycle
            if (.not. associated(d%m_Fnodes )) cycle;
            if(d%ndof <1) cycle
            R(c:c+d%ndof-1) =  d%rt
            c=c+d%ndof
        enddo  dfo
        ENDDO sl
end subroutine PackResiduals

subroutine PackAccelerations(vDash,damping)
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f
    use saillist_f
    use ttmass_f
    use coordinates_f
    use dofoprototypes_f
    implicit none

! the accelerations  are set to B . ( Resid - Q.vee)
    real(C_DOUBLE), dimension(*), intent(out) :: vDash
    real(C_DOUBLE),  intent(in) :: damping
!
!locals
    real(C_DOUBLE), dimension(3)  :: dvdx
    TYPE ( nodelist) ,POINTER :: nodes
    TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif
    INTEGER ::  sli, c,n, i
    type(fnode), pointer :: theNode

     c=1  ! THE ALL-IMPORTANT COUNTER  of col number Must be the same in FormJayMatrix
sl:	DO sli=1,SailCount
        if (0== is_Hoisted(sli) ) cycle
        if (0== is_active(sli)) cycle
        nodes=>saillist(sli)%nodes
        if(.not. associated(nodes%xlist)) cycle
!!!$OMP PARALLEL  PRIVATE (N,k,theNode) shared(nlist,nnn,sli) DEFAULT(none)
!!!$OMP DO SCHEDULE(guided)
dn:     DO  N=1,nodes%ncount
            thenode=> nodes%xlist(n)
            if(.not. thenode%m_Nused)   cycle
            if(IsUnderDOFOControl(theNode)  ) cycle
            if(theNode%m_ElementCount ==0)          cycle
            if(.not. theNode%m_Nused) cycle
            if(IsUnderDOFOControl(theNode) ) cycle
            dvdx= matmul(theNode%B,theNode%r -damping* thenode%v)
            vDash(c:c+2) = dvdx
            c=c+3

         ENDDO dn
!!!$OMP END DO NOWAIT
!!!$OMP END PARALLEL
        Dofos=>saillist(sli)%Dofos
        if(.not. ASSOCIATED(Dofos%list)) cycle
dfo:	do i =1, dofos%count
            if(.not. Dofos%list(i)%used) cycle
            d=>Dofos%list(i)%o
            if(associated(d%child)) cycle


            if (.not. associated(d%m_Fnodes )) cycle;
            if(d%ndof <1) cycle
            vDash(c:c+d%ndof-1) =   matmul(d%bt , d%rt-damping*d%vt )
            c=c+d%ndof
        enddo  dfo
        ENDDO sl
end subroutine PackAccelerations


subroutine PackCoordinates(Y) bind(C,name="cf_PackCoordinates")! copy the current coords into  array Y, in our standard order
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f
    use saillist_f
    use dofoprototypes_f
    implicit none
    real(C_DOUBLE), dimension(*), intent(out) :: Y
!
!locals

    TYPE ( nodelist) ,POINTER :: nodes
    TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif
    INTEGER ::  sli, c,n, i
    type(fnode), pointer :: theNode

     c=1  ! THE ALL-IMPORTANT COUNTER  of col number Must be the same in FormJayMatrix
sl:	DO sli=1,SailCount
    if (0== is_Hoisted(sli) ) cycle
    if (0== is_active(sli)) cycle
    nodes=>saillist(sli)%nodes
    if(.not. associated(nodes%xlist)) cycle
!!!$OMP PARALLEL  PRIVATE (N,k,theNode) shared(nlist,nnn,sli) DEFAULT(none)
!!!$OMP DO SCHEDULE(guided)
dn:     DO  N=1,nodes%ncount
        thenode=> nodes%xlist(n)
        if(.not. thenode%m_Nused)   cycle
        if(IsUnderDOFOControl(theNode)  ) cycle
        if(theNode%m_ElementCount ==0)          cycle
        if(.not. theNode%m_Nused) cycle
        if(IsUnderDOFOControl(theNode) ) cycle
     !   write(*,'(i4,3f13.4)' ) c, thenode%XXX
        Y(c:c+2) = thenode%XXX
        c=c+3

     ENDDO dn
!!!$OMP END DO NOWAIT
!!!$OMP END PARALLEL
    Dofos=>saillist(sli)%Dofos
    if(.not. ASSOCIATED(Dofos%list)) cycle
dfo:	do i =1, dofos%count
        if(.not. Dofos%list(i)%used) cycle
        d=>Dofos%list(i)%o
        if(associated(d%child)) cycle


        if (.not. associated(d%m_Fnodes )) cycle;
        if(d%ndof <1) cycle
        Y(c:c+d%ndof-1) =  d%t
        c=c+d%ndof
    enddo  dfo
    ENDDO sl

end subroutine PackCoordinates

! input array disp is disps or coords, ordered as JayMatrix rows.
! disp may be absolute coordinates or it may be an increment
subroutine  ApplyDisplacements(disp, isIncrement ) bind(C,name="cf_ApplyDisplacements")
    USE ftypes_f
    use coordinates_f
    use dofoprototypes_f
    use saillist_f
    IMPLICIT NONE
    real(C_DOUBLE),intent(in),dimension(*) :: disp
    logical, intent(in), value:: isIncrement
!
!locals
    TYPE ( nodelist) ,POINTER :: nodes
    TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif
    INTEGER ::  sli, c,n, i,rflag
    type(fnode), pointer :: theNode

isi:    if (isIncrement) then
         c=1  ! THE ALL-IMPORTANT COUNTER  of col number Must be the same in FormJayMatrix
sl:	DO sli=1,SailCount
        if (0== is_Hoisted(sli) ) cycle
        if (0== is_active(sli)) cycle
    	nodes=>saillist(sli)%nodes
        if(.not. associated(nodes%xlist)) cycle

dn:     DO  N=1,nodes%ncount
            thenode=> nodes%xlist(n)
            if(.not. thenode%m_Nused)   cycle 
            if(IsUnderDOFOControl(theNode)  ) cycle
            if(theNode%m_ElementCount ==0)    cycle
             call increment_coord(disp(c:c+2),thenode ,sli,rflag )
         !    write(120,'("Inc-C",i3,1x,3G12.5,"->",3g14.7  )')  N, disp(c:c+2),thenode%xxx
             c=c+3
         ENDDO dn
	Dofos=>saillist(sli)%Dofos
	if(.not. ASSOCIATED(Dofos%list)) cycle
dfo:	do i =1, dofos%count
	    if(.not. Dofos%list(i)%used) cycle
            d=>Dofos%list(i)%o
            if(associated(d%child)) cycle
             if (.not. associated(d%m_Fnodes )) cycle;
            if(d%ndof <1) cycle
            d%t(1:d%ndof)= d%t(1:d%ndof) + disp(c:c+d%ndof-1)
            c=c+d%ndof
        enddo  dfo
	ENDDO sl 

     else isi

        c=1  ! THE ALL-IMPORTANT COUNTER  of col number Must be the same in FormJayMatrix
sl2:	DO sli=1,SailCount
       if (0== is_Hoisted(sli) ) cycle
       if (0== is_active(sli)) cycle
       nodes=>saillist(sli)%nodes
       if(.not. associated(nodes%xlist)) cycle

dn2:     DO  N=1,nodes%ncount
           thenode=> nodes%xlist(n)
           if(.not. thenode%m_Nused)   cycle
           if(IsUnderDOFOControl(theNode)  ) cycle
           if(theNode%m_ElementCount ==0)    cycle
            thenode%XXX= disp(c:c+2)
          !   write(*,'("(appDisp) node ",i4,3g13.4)') thenode%nn, thenode%xxx
            c=c+3
        ENDDO dn2
       Dofos=>saillist(sli)%Dofos
       if(.not. ASSOCIATED(Dofos%list)) cycle
dfo2:	do i =1, dofos%count
           if(.not. Dofos%list(i)%used) cycle
           d=>Dofos%list(i)%o
           if(associated(d%child)) cycle
            if (.not. associated(d%m_Fnodes )) cycle;
           if(d%ndof <1) cycle
           d%t(1:d%ndof)=  disp(c:c+d%ndof-1)
           c=c+d%ndof
       enddo  dfo2
       ENDDO sl2

endif isi
end subroutine  ApplyDisplacements 

subroutine ApplyVelocities(VeeIn) bind(C,name="cf_ApplyVelocities")
USE ftypes_f
use coordinates_f
use dofoprototypes_f
use saillist_f
IMPLICIT NONE
real(C_DOUBLE),intent(in),dimension(*) :: VeeIn
!
!locals
TYPE ( nodelist) ,POINTER :: nodes
TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif
INTEGER ::  sli, c,n, i
type(fnode), pointer :: theNode

 c=1  ! THE ALL-IMPORTANT COUNTER  of col number Must be the same in FormJayMatrix
sl:	DO sli=1,SailCount
            if (0== is_Hoisted(sli) ) cycle
            if (0== is_active(sli)) cycle
            nodes=>saillist(sli)%nodes
            if(.not. associated(nodes%xlist)) cycle

dn:          DO  N=1,nodes%ncount
                thenode=> nodes%xlist(n)
                if(.not. thenode%m_Nused)   cycle
                if(IsUnderDOFOControl(theNode)  ) cycle
                if(theNode%m_ElementCount ==0)    cycle
                 thenode%v = VeeIn(c:c+2)
                 c=c+3
             ENDDO dn
    Dofos=>saillist(sli)%Dofos
    if(.not. ASSOCIATED(Dofos%list)) cycle
dfo:    do i =1, dofos%count
            if(.not. Dofos%list(i)%used) cycle
            d=>Dofos%list(i)%o
            if(associated(d%child)) cycle
            if (.not. associated(d%m_Fnodes )) cycle;
            if(d%ndof <1) cycle
            d%vt(1:d%ndof)=   VeeIn(c:c+d%ndof-1)
            d%ke = dot_product (d%vt,matmul(d%xmt ,d%vt ))
            c=c+d%ndof
        enddo  dfo
    ENDDO sl
end subroutine  ApplyVelocities
!
! ApplyDisplacements and FormJayMatrix must use the same counting method
!
! we'd like to flag Jaymatrix columns as 'rotation' or 'translation' but for combo dofos we cant know
subroutine FormJayMatrix(jay,rhs,nrhs,rcap) bind(C,name="cf_FormJayMatrix")   ! actually, writes the transpose
! it is neither square nor symmetric
    USE ftypes_f
    use cfromf_f
    use dofoprototypes_f
    use saillist_f
    use  fnoderef_f
    IMPLICIT NONE
    integer(kind=cptrsize),intent(out) :: jay,rhs
    integer, intent(out) :: nrhs
    real(kind=double), intent(in),VALUE :: rcap !if> 0 we cap the residuals to it
    TYPE ( nodelist) ,POINTER :: nodes
    TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif
    real(kind=double) :: v,NodalR
    INTEGER ::  sli,r,c,n,k,i,j, JayRow,col,rmax,rcappedCount
    type(fnode), pointer :: theNode
    real(kind=double), dimension(3):: nodalRvec
    jay = cf_NewSparseMatrix(matrixType_Jay)
    rhs = cf_NewSparseMatrix(matrixType_RHS)
    rmax=-1; c=1 ;rcappedCount=0;
sl: DO sli=1,SailCount
        if (0== is_Hoisted(sli) ) cycle
        if (0== is_active(sli)) cycle
    	nodes=>saillist(sli)%nodes
        if(.not. associated(nodes%xlist)) cycle

dn:     DO  N=1,nodes%ncount
            thenode=> nodes%xlist(n)
            if(.not. thenode%m_Nused)   cycle 
            rmax=max(rmax, thenode%fnodeRowIndex+2)
            if(IsUnderDOFOControl(theNode)  ) cycle
            if(theNode%m_ElementCount ==0)   cycle

            r = thenode%fnodeRowIndex
            nodalRvec=thenode%r
            if(rcap>0) then
             rcappedCount=rcappedCount + count(nodalRvec>rcap)

               where(nodalRvec>rcap) 
                    nodalRvec=rcap      
               elsewhere(nodalRvec<-rcap) 
                    nodalRvec=-rcap
               endwhere
            endif
            do k=0,2
            ! if its a rotnode, IsRotationalDof(c) = .true.
                call cf_AddToSparseMatrix(rhs,c,1,nodalRvec(k+1) )   
                call cf_AddToSparseMatrix(jay,r+k,c,1.0D00 )
                c=c+1
            enddo
          ENDDO dn
	Dofos=>saillist(sli)%Dofos
	if(.not. ASSOCIATED(Dofos%list)) cycle
dfo:	do i =1, dofos%count
	    if(.not. Dofos%list(i)%used) cycle
        d=>Dofos%list(i)%o	
        if(associated(d%child)) cycle
    !    write(outputunit,*) ' dofo ',i,' (r,c,val)'
! each block of 3 rows represents a Fnode. Each column represents a Tee.  There are d%ndof columns      
        if (.not. associated(d%m_Fnodes )) cycle;  
        do col=1,d%ndof
doj:        do j=1,ubound(d%m_Fnodes,1)
                thenode=>decodenoderef(d%m_Fnodes(j))
                r = theNode%fnodeRowIndex; ! C is what it is
                JayRow = d%m_Fnodes(j)%m_fnrRow
dok:            do k=0,2
                    v = d%jacobi(JayRow+k,col)
                     call cf_AddToSparseMatrix(jay,r+k,c, v ) 
                enddo dok
            enddo doj
            nodalR=d%rt(col)
            if(rcap>0) then
                nodalR=min(rcap,nodalR)
                nodalR=max(nodalR,-rcap)
            endif
             ! if its a rotationalDOF, IsRotationalDof(c) = .true. BUT FOR COMBO WE CANT KNOW
            call cf_AddToSparseMatrix(rhs,c,1,nodalR )              
            c=c+1
      enddo
    enddo  dfo
	ENDDO sl
! finally, add something at the last row in case the last nodes
!in the list are fixed (ie their d%ndofo=0)	
    if(rmax >0 .and. c>1) call cf_AddToSparseMatrix(jay,rmax,c-1,0.0D00)

	nrhs=c
 if(rcappedCount>0)  write(outputUnit,'(i6," residuals were capped to ",g13.4 )') rcappedCount,rcap

end  subroutine  FormJayMatrix  
 
subroutine FormStiffnessMatrix(m,bytest) bind(C,name="cf_FormStiffnessMatrix") ! does RHS too
use cfromf_f
use rx_control_flags_f
use saillist_F
use tris_f
use stringlist_f
use battenf90_f
use flinkList_f
use ttmass_f
use fbeamList_f
implicit none
     integer(kind=cptrsize),intent(out) :: m
     integer(C_INT), intent(in), value ::bytest
! 1) form dRbyDx for all nodes
! 2) Also form the monster J matrix
! 3) also form the RHS (= nodal or DOFO residuals)
! IF only we had class inheritance, then we can do all the elements in a single type.
! but we don't so

    logical wsafe
  integer sli,k
  type(sail) ,pointer  ::s
!  SPD  is 9.  We could use SPD (9) if we dont have geometric stiffness
!  which is usually non-symmetric according to the literature.
 ! We usually use type RXM_RECTANGLE ( a general matrix) . However we could say Structurally Symmetric Matrices
 ! But then we cant use pardiso to 
   if(bytest .eq. 0) then
    m = cf_NewSparseMatrix(matrixType_Sraw) ! should be RSPD but we need the full matrix for the multiplications
 
!* Wrinkling allowed by default.
!* switch CS turns it off
!* If this is NOT turned off, a further switch WK turns ON
!* allowance for wrinkling in the calculation of the stiffness matrix.

!* NOTE  g_Wrinkling_Allowed doesnt do anything. debug2 is the wrinkling flag



      wsafe = debug2
      debug2=K_on_Wrinkled_Matrix
do_1: DO sli = 1,sailcount
ifa1:     if(is_Active(sli)) THEN
                s=>saillist(sli)
                CALL Make_Current_Edge_Lengths(sli) !this is in FormStiffnessMatrix
                CALL make_DD_and_G(sli)  ! uses   call check_cl('GC',UseCurrentGeom)

                CALL AssembleTriElts(s,m)   !like ttmass9
                k= AssembleBeamElts(sli,m)  
                k= AssembleBattenElts(sli,m)  ! see FMake_All_Batten_Matrices
                CALL AssembleStringElts(sli,StringCompAllowedFlag,m) ! see CS_String_Masses(sli,StringCompAllowed)
               k = AssembleLinkElts(sli,StringCompAllowedFlag,m) ! SEE MakeAllLinkMasses(sli,StringCompAllowed)
            ENDIF ifa1
      ENDDO do_1
        !CALL  AssembleGLEElts () ! see gle_masses()
       debug2 =wsafe
      else ! bytest
          write(*,*)' DONT call Form Stiffness MatrixByTest(m)'
      endif

end  subroutine FormStiffnessMatrix   


subroutine FormInverseMassMatrix( sparsemassmatrix) bind(C,name="cf_FormInverseMassMatrix")
    USE ftypes_f
    use cfromf_f
     use dofoprototypes_f
    use saillist_f
    IMPLICIT NONE
        integer(kind=cptrsize),intent(inout)  :: sparsemassmatrix
        integer :: nrhs
	TYPE ( nodelist) ,POINTER :: nodes
	TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif

	INTEGER ::  sli,c,n,k,i,j, rmax,nr,nc
	type(fnode), pointer :: theNode

        rmax=-1; c=1
sl:	DO sli=1,SailCount
	    if (0== is_Hoisted(sli) ) cycle
	    if (0== is_active(sli)) cycle
    	nodes=>saillist(sli)%nodes
        if(.not. associated(nodes%xlist)) cycle

dn:     DO  N=1,nodes%ncount
            thenode=> nodes%xlist(n)
            if(.not. thenode%m_Nused)   cycle 
            rmax=max(rmax, thenode%fnodeRowIndex+2)
            if(IsUnderDOFOControl(theNode)  ) cycle
            do j=0,2            
                do k=0,2
                    call cf_AddToSparseMatrix(sparsemassmatrix,c+k,c+j,theNode%b(k+1,j+1) )
                enddo
            enddo
           c=c+3              
          ENDDO dn
	Dofos=>saillist(sli)%Dofos
	if(.not. ASSOCIATED(Dofos%list)) cycle
	do i =1, dofos%count  
	    if(.not. Dofos%list(i)%used) cycle
        d=>Dofos%list(i)%o	
        if(associated(d%child)) cycle
!   There are d%ndof columns 
!   we fill in from (c,c) to (c+ndof-1,c+ndof-1)     
        if (.not. associated(d%bT )) cycle;
      
       nr = ubound(d%bt,1);nc = ubound(d%bt,2) ; if(nr/=nc)  write(outputunit,*)'whoops',nr,nc
        
            do j=1,nr           
                do k=1,nc
                    call cf_AddToSparseMatrix(sparsemassmatrix,c+k-1,c+j-1,d%bt(k,j) )
                enddo
            enddo
           c=c+nr
    enddo          
	ENDDO sl
! finally, add somethhng at the last row in case the last nodes
!in the list are fixed (ie their d%ndofo=0)	
	nrhs=c
    call cf_WriteSparseMatrix(sparsemassmatrix, "globalBMatrix.rua"//char(0))
end  subroutine  FormInverseMassMatrix
 
! In each model the biggest nodal residual is noted as %tcon_this and the corresponding node is saillist(sli)%Nmax
! returns rmax as the biggest (vector( resudial and rmaxSli, rmaxN as the node where it occurs

function measureResiduals(rmaxSli,rmaxN,rmax,Print) result(tcon)
use saillist_f
use dofoprototypes_f
use coordinates_f
implicit none
integer, intent(out) ::rmaxSli,rmaxN
real(kind=double), intent(out), dimension(3) :: rmax
real(kind=double):: tcon
logical, intent(in) :: Print
 !locals
    real(kind=double),dimension(3) :: ttt,rdmax
    integer ::n,sli,  smaxD,nmaxD ! smax,
    type(fnode), pointer :: theNode

    rmaxsli=-1
    rmaxN=-1
    rmax=0
    tcon= 0;
mdo2:   DO sli = 1,sailcount
            saillist(sli)%tcon_this=0
           if(.not. is_Active(sli)) cycle
       ndo : DO N=1,saillist(sli)%nodes%Ncount
            	thenode=>saillist(sli)%nodes%xlist(N)
            	if(.not. thenode%m_Nused) cycle
                if(IsUnderDOFOControl(thenode))   cycle
                TTT=ABS(get_Cartesian_R(N,sli))
                IF (any(ttt .GT. saillist(sli)%tcon_this)) THEN
                    saillist(sli)%tcon_this = maxval(ttt)
                    saillist(sli)%Nmax = N;
                ENDIF
            ENDDO ndo

        if(saillist(sli)%tcon_this > tcon) then
            tcon= saillist(sli)%tcon_this
            rmaxsli = sli
            rmaxN = saillist(sli)%Nmax
            if(rmaxN>0  ) rmax = get_Cartesian_R(rmaxN,rmaxSli)
       endif
    ENDDO  mdo2
    if(Print) then
    if(rmaxSLI>0) then
        write(outputunit,'("nodal resids,sli=",i2,i6," =",g13.4," max=",g13.4,$ )',iostat=n) rmaxsli,rmaxN,saillist(rmaxsli)%tcon_this ,tcon
    else
        write(outputunit,'("nodal resids,sli=",i2,i6," =",g13.4," max=",g13.4,$ )',iostat=n) rmaxsli,rmaxN,888. ,tcon
endif
    endif
  ! Max DOFO Residuals
! places the highest R in  saillist(sli)%tcon_this
! and records the node with the highest R as (nmax,smax)
! Note smax isnt necessarily the same as SLI.

    call Max_DOFO_Residuals( smaxd,nmaxd,rdmax)

    if(Dot_Product(  rdmax,rdmax) .gt. dot_Product(rmax,rmax)) then
        if(smaxd<=0) then
            write(outputunit,*) ' WHY is smaxD = ',smaxd

        else
        saillist(smaxd)%tcon_this=max(saillist(smaxd)%tcon_this,maxval(abs(rdmax)))
        saillist(smaxd)%nmax = nmaxD
        rmaxSli = smaxd
        rmaxN   = nmaxd
        rmax = rDmax
        tcon = maxval(abs(rdmax))
        endif
        if (Print) write(outputunit,'(" +dofos:",i3,i6, 3g14.4,$)',iostat=n)rmaxSli,rmaxN, rdmax
    endif

   if (Print) write(outputunit,'(" so max=",g14.4," at node ",2i5)',iostat=n)tcon,rmaxSLI,rmaxN

end function measureResiduals

subroutine ExtractResiduals(r)
use saillist_f
use dofoprototypes_f
use coordinates_f
implicit none
real(kind=double), intent(out), dimension(:) :: r
!locals
    TYPE ( nodelist) ,POINTER :: nodes
    TYPE ( Dofolist) ,POINTER :: Dofos
#ifdef NO_DOFOCLASS
           type (dofo),pointer ::d  ! or class
#else
           class (dofo),pointer ::d ! or type
#endif
    INTEGER ::  sli, c,n, i
    type(fnode), pointer :: theNode

        c=1  ! THE ALL-IMPORTANT COUNTER  of col number Must be the same in FormJayMatrix
sl2:	DO sli=1,SailCount
       if (0== is_Hoisted(sli) ) cycle
       if (0== is_active(sli)) cycle
       nodes=>saillist(sli)%nodes
       if(.not. associated(nodes%xlist)) cycle

dn2:     DO  N=1,nodes%ncount
           thenode=> nodes%xlist(n)
           if(.not. thenode%m_Nused)   cycle
           if(IsUnderDOFOControl(theNode)  ) cycle
           if(theNode%m_ElementCount ==0)    cycle
            r(c:c+2)= get_Cartesian_R(N,sli)
            c=c+3
        ENDDO dn2
       Dofos=>saillist(sli)%Dofos
       if(.not. ASSOCIATED(Dofos%list)) cycle
dfo2:	do i =1, dofos%count
           if(.not. Dofos%list(i)%used) cycle
           d=>Dofos%list(i)%o
           if(associated(d%child)) cycle
           if (.not. associated(d%m_Fnodes )) cycle;
           if(d%ndof <1) cycle
           r(c:c+d%ndof-1)    =    d%rt(1:d%ndof)
           c=c+d%ndof
       enddo  dfo2
       ENDDO sl2

end subroutine ExtractResiduals
 
function MeasureDisplacements() result(maxdisp)
	USE basictypes_f
real(kind=double):: maxdisp
maxdisp=0; write(outputunit,*) ' TODO  MeasureDisplacements'
end function MeasureDisplacements	


   
SUBROUTINE Make_Current_Edge_Lengths(sli)
    use coordinates_f
    use saillist_f
    use ttmass_f
    use rx_control_flags_f
    use math_f
    IMPLICIT NONE
    integer, intent(in) ::sli
 !   type(fedge),pointer :: e
    REAL(kind=8):: d(3),zlink 
    INTEGER i,NShortLinks,nn
    	TYPE (Fedge), DIMENSION(:), POINTER:: eds
	NShortLinks =0    
!ecr:   if(EdgeCurveCorr  ) THEN ! MeshInPanelPlane>0.  >1 means trace panels via the surface normal too
!        call  NodalNormals(sli)
!! this omp is OK = june 2008	
!!$OMP PARALLEL DO PRIVATE (i,e, zlink,D ) shared(saillist,sli,EdgeCurveCorr) default(none)  reduction(+ : NShortLinks) SCHEDULE(STATIC)
!	    DO i = 1,saillist(sli)%edges%count
!	        e=>saillist(sli)%edges%list(i)
!	        if(.not. e%m_Eused)  cycle
!			e%m_TLINK=0.0
!            D= get_Cartesian_X_by_PTR(e%m_e2)-get_Cartesian_X_by_PTR(e%m_e1) 	! typically we call these 4xmore than needed		
!			
!			ZLINK=DSQRT(dot_product(D,D))	 ! current chord length
!      		if(Zlink .lt. epsilon(zlink)) THEN
!				NShortLinks = NShortLinks +1
!            	e%m_DELTA0=0.0
!            	e%m_dlink =0 ! I think this will stop it setting residuals.
!            	cycle
!      		ENDIF			
!      		e%m_dlink =D / ZLINK			
!			 if(e%IsOnSurf) then ! returns chord/arc given normals at ends.
!					zlink = zlink / CorrectedLengthFactor(e%m_dlink,e%m_e1%m_Nnor, e%m_e2%m_Nnor) 
!					! zlink is now the current arc length.
!			 endif
!			e%m_DELTA0 =ZLINK - e%m_ZLINK0  / e%ChOverArcInit 
!			! delta0 is the difference between current arclength and unstressed arclength.
!      	ENDDO
!!$OMP  END PARALLEL DO    
        nn= saillist(sli)%edges%count
        eds=>saillist(sli)%edges%list
!else ecr ! no curvature correction
! this omp is OK = june 2008	 ! BUT SLOW  THERES A BIG WAIT 
!$OMP PARALLEL PRIVATE (i, zlink,D ) shared(nn,eds ) default(none)  reduction(+ : NShortLinks) 	if(nn>100)
!$OMP   DO SCHEDULE(auto)
!!!!!!!!!!!dynamic,chunksize) 

	    DO i = 1,nn
	        if(.not. eds(i)%m_Eused)  cycle

			eds(i)%m_TLINK=0.0
!DEC$ NOVECTOR   			
            D= get_Cartesian_X_by_PTR(eds(i)%m_ee2)-get_Cartesian_X_by_PTR(eds(i)%m_ee1) 	! typically we call these 4xmore than needed		
			
                ZLINK=DSQRT(dot_product(D,D))	 ! current chord length

                eds(i)%m_DELTA0 =ZLINK-eds(i)%m_ZLINK0
      		if(Zlink .lt. 1D-12) THEN
                        NShortLinks = NShortLinks +1
                        zlink = 0.001*sqrt(3.0);	d = 0.001
      		ENDIF
       		ZLINK = 1.0D00 / ZLINK
      		eds(i)%m_dlink =D * ZLINK
      	ENDDO
!$OMP END DO  NOWAIT
!$OMP END PARALLEL 

!endif ecr
 ! speed trials showed
 ! A) get_Cartesian_X_by_PTR saves about 3% (   100 cycles  1.263992 not  1.293998)
 ! B) Intel IPO saves about 20%
 ! C) gathering the zlinks so as to use VML vdsqrt was slow. 
 
 ! further ideas
 !  ifdef out the debug lines in IsUnderDOFOControl (current)
 !      replace e by saillist(sli)%edges%list(i)
 ! FORCEINLINE
 
!	if( NShortLinks >0) then
!		write(outputunit,*) ' there are ' ,NShortLinks,' short links'
!	Endif
END SUBROUTINE Make_Current_Edge_Lengths
 


SUBROUTINE All_Residuals()
    use omp_lib
    use fglobals_f
    USE connects_f
    USE gle_f
    use tris_f
    use kbatten_f
    use stringlist_f
    use fbeamList_f
    use saillist_f
    use coordinates_f
    use battenf90_f
    use flinklist_f
    use rx_control_flags_f
    IMPLICIT NONE

! * Locals
	REAL(kind=8):: radd(3) 
	INTEGER j,l ,l1,l2,sli,ok ,nnt 
	type(tri), pointer :: t
	type (Fedge), pointer :: e
        TYPE (tri),  DIMENSION(:), POINTER :: tlist
	
    if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash in allresiduals'
    
	CALL gle_Residuals()
do_m:	DO sli=1,SailCount 
         if(is_active(sli)) THEN

      		CALL Make_Current_Edge_Lengths(sli) !in All_Residuals also zeros TLINK
                CALL CS_String_Resids(sli,StringCompAllowedFlag)  ! sums into TLINK
                j= FBatten_Resids(sli) ! sums into R
		
f2006: if(.false.  ) then ! form_2006 stroona resids call
            DO L=1,saillist(sli)%tris%count
                t=>saillist(sli)%tris%list(L)
               if(t%use_tri) then
!     !               see sf_one_tri_global_forces(l,g9)  which calls
!     !
!    !                call sf_one_tri_local_forces(n,stress,eps,tedge,loadvector)

                endif
            ENDDO

        else  f2006! end stroona resids call

            tlist=>saillist(sli)%tris%list
            nnt = saillist(sli)%tris%count
!$OMP PARALLEL PRIVATE(L) shared(tlist,NNT) default(none) IF(nnt>100)
!$OMP DO SCHEDULE(guided)
            DO L=1,NNT
               if(tlist(L)%use_tri.and.tlist(L)%used ) then
                   CALL One_Tri_Local_Forces(tlist(L),tlist(L)%tedge)  ! stress is tlist(L)%stress
                endif
            ENDDO
!$OMP END  DO NOWAIT
!$OMP END PARALLEL  
    endif  f2006


  if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 14'
!$OOMP PARALLEL DO shared(saillist,sli) PRIVATE(L,e,J,t) default(none) this one gives errors
      	DO L=1,saillist(sli)%tris%count
      	    t=>saillist(sli)%tris%list(L) 
           if(t%use_tri.and.t%used ) then
              DO j=1,3
        	  e=> t%m_er(j)%m_fepe
         	  e%m_Tlink = e%m_Tlink  + t%tedge(j)
      	      ENDDO
          endif
      	ENDDO
!$OOMP  END PARALLEL DO   
       	DO L=1,saillist(sli)%edges%count
      	    e=>saillist(sli)%edges%list(L) 
           if(e%m_Eused) then
      		L1=e%m_L12(1)
      		L2=e%m_L12(2)
      		if ((l1 > 0) .and. (l2 > 0))  THEN  ! should be able to scan for this at the beginning.
      			RADD=e%m_TLINK* e%m_dlink
                        ok = increment_R(L1, Radd,sli)
                        radd=-radd
                        ok = increment_R(L2, Radd,sli)
      		ENDIF
      		endif
      	ENDDO

     CALL Link_Residuals(sli,StringCompAllowedFlag)    ! shouldnt this be LinkCompAllowedFlag
     call Beam_Residuals(sli)
      ENDIF
	ENDDO do_m

	CALL Set_Connect_R() 
	call MapDofoResiduals() 
END SUBROUTINE All_Residuals

subroutine varioustests(ss,sm, flag) ! activated by '/VT'
	USE basictypes_f
implicit none
integer(kind=cptrsize)  :: ss,sm  ! sparse stiffness,sparse mass
integer :: flag

! locals
 !   integer :: nrhs
   write(outputunit,*)  ss,sm
!real(kind=double), pointer, dimension(:,:) ::m,s
if(flag .eq. 0) return
if(flag .eq. 1) then
! For the modes of vibration:
! 1) get the nodal and dofo masses
! 2) assemble the global stiffness matrix
! 3) assemble the global mass matrix
! 4) find the eigenvalues/vectors of (S.M^-1)


    CALL Define_All_Masses()

   
return
endif ! flag 1

end subroutine varioustests 

subroutine MakeFullMatrix(m,a,err)  
     USE, INTRINSIC :: ISO_C_BINDING
      use basictypes_f
      use cfromf_f
     integer(kind=cptrsize),intent(in),value :: m
     real(kind=double), dimension(:,:),pointer :: a
     integer, intent(out) ::err
     integer(C_INT ) nr,nc,r,c
     
     call cf_SparseMatrixDimensions(m,nr,nc) 
     allocate(a(nr,nc),stat=c)
     if(c /=0) then; err=1; return; endif
     err=0
     a=0.
      do r=1,nr
      do c=1,nc
      a(r,c) = cf_SparseMatrixElement(m,r,c)  
      enddo
      enddo
end   subroutine MakeFullMatrix

!extern "C" int cf_SolveSVD(double*a,intnr,int nc, double*b) ! this is so safe that it never gets startd!
function cf_SolveSVD(a,nr,nc,b ) result(info) bind(C,name="cf_SolveSVD")  ! returns the result in b

!If info = 0, the execution is successful.
!If info = -i, the i-th parameter had an illegal value.
 
     USE, INTRINSIC :: ISO_C_BINDING
      use basictypes_f
      use cfromf_f
      USE LAPACK95 ,only: gelss,gelsy
      implicit none
      integer,intent(in),value :: nr,nc
      real(c_double),dimension(nr,nc) :: a
       real(c_double),dimension(nr,1) :: b  ! rhs on input ., result on output
         integer info      
!// we will try to use MKL's lapack dgelsy, using the f95 interface because its easier
!
!//a(lda,*) contains the m-by-n matrix A. 
!//The second dimension of a must be at least max(1, n).
!//b(ldb,*) contains the m-by-nrhs right hand side matrix B. 
!//The second dimension of b must be at least max(1, nrhs).
!//	double  a(n,m)
!// 	double b(m,1);
! 	DOUBLE PRECISION rcond;
	integer rank
	integer r !,c
	character*256 fmt
	write(fmt,"('('i5.5'g15.9,$)'   )") nc
    open(130,file='fullmatrixtosvd.txt')

    do r = 1,nr
    write(130,fmt) a(r,1:nc)
    write(130,*) " "
    enddo
    close(130)
     open(140,file='rhstosvd.txt')  
     write(140,'(g16.9)') b     
    close(140)
        
! try call gelss(a, b [,rank] [,s] [,rcond] [,info])
!call gelss(a, b ,rank=rank,rcond=0.00001d0, info=info) 
call gelss(a, b ,rank=rank,rcond=0.00001d0, info=info) 
write(outputunit,*) 'gelss gives rank= ',rank,'/',ubound(a,1),' info= ',info
end  function cf_SolveSVD


! Derivative List follows our RXODEStepper schema for arguments
! to integrate a set of second-order ODEs we re-cast them as twice as many 1st order ODEs

! t is the time - which we would use if we had time-dependent loads or BCs
! yIn is a column containing first the nodal/DOFO displacements, ordered as in FormJayMatrix,
! then the velocities, ordered the same way
! It returns dYdx , the Velocities and accelerations packed the same way



subroutine DerivativeList(t, yIn, dyDx,damping, N) bind(C,name="cf_DerivativeList")
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f
    use saillist_f
    use ttmass_f
    use coordinates_f
    use dofoprototypes_f
    use rx_control_flags_f
    implicit none

    integer(C_INT), intent(in),VALUE :: N
    real(C_DOUBLE), intent(in),value :: t  ! would be used for time-dependent loads, BCs, etc
    real(C_DOUBLE), intent(in),value :: damping
! yIn is a list of displacements ( packed as FormJayMatrix)
! followed by a list of velocities packed the same way

    real(C_DOUBLE), dimension(N), intent(in),target :: yIn

! dydx is  a list of velocities ( packed as FormJayMatrix)
! followed by a list of accelerations packed the same way
! the Vees are just copied from the input
! the accelerations  are set to B . ( Resid - Q.vee)

    real(C_DOUBLE), dimension(N), intent(out),target :: dyDx

! some shorthands
    real(C_DOUBLE), dimension(:), pointer :: VeeOut, VDash
    integer:: sli,i,vSize,ok,k,kit
    real(kind=double), dimension(3) :: l_acceleration

    vSize=ubound(yIn,1)/2
    l_acceleration=t ! to silence the compiler
    l_acceleration=0; kit=0;

    veeOut=> dydx(1:vSize)
    VDash=> dydx(vSize+1:2*vSize)

    ASSOCIATE(    X=> yIn(1:vSize),VeeIn=>yIn(vSize+1:2*vSize)  )
  !  dydx(1:2*vsize) =0
 ! procedure:
 ! 1) unpack the X and copy their values to nodal XXX and to dofo Tee
 ! 2) get the residuals
 ! 3) pack B . ( Resid - Q.vee)  into the acceleration vctor
 ! 4) copy the input velocities into the output velocities

   call  ApplyDisplacements(X,.false. )
   call  ApplyVelocities(VeeIn) ! in case any elements are sensitive to Vee

! Loads and RESIDUALS
     i=  All_Loading(l_acceleration)
    do sli=1,sailcount
        if(.not. is_active  (sli)) cycle
        ok=zero_r(sli);
        do  i = 1,saillist(sli)%nodes%ncount
            saillist(sli)%nodes%xlist(i)%Rold = get_Cartesian_R(i,sli)
            call Set_R(i,sli, saillist(sli)%nodes%xlist(i)%P )
        enddo
      ENDdo

     k = DOFO_TestForCapture(kit) ! test for far side.  Cant test for scoot here
     if(.not. BTEST( ForceLinearDofoChildren,2) )  k = DOFO_UpdateJay(  )  ! experiment to move this before all residuals
     CALL All_Residuals()
     k = DOFO_TestForRelease( )

    call PackAccelerations(vDash,damping)

    VeeOut=VeeIn

    end associate
end subroutine DerivativeList

subroutine SolveRK (Ktot,pTmax,iexit,p_VDamp)

     use saillist_f
    USE gravity_f
    use cfromf_f
     USE connects_f
    use feaprint_f
     USE nograph_f
    use rx_control_flags_f
    USE basictypes_f
    use parse_f
    use ttmass_f , only :ALL_LOADING
    IMPLICIT NONE

    INTEGER(C_INT),intent(out):: Iexit 		! return 0=OK, 1=not conv, 2=user abort
    INTEGER(c_int), intent(in):: Ktot		! PARAMETER. No of cycles this run
    REAL(kind=8) ,intent(in) :: pTmax			! convergence limit in Newtons
    real (kind=double) ,intent(in) , volatile::  p_vdamp  ! Vdamp is gravitational acceleration


    real(kind=double)  :: maxdisp,displimit, tconlast
    real(kind=double) ,dimension(3) :: rmax,myvector,l_acceleration
    integer::sli,   LIslimax ,rmaxN
    logical, parameter ::dbg = .false.
     real (kind=4) ::l_thisSecTime
    real (kind=double) :: l_thiscputime,ekt
    INTEGER ::  I,KIT, NOUT,ok
    INTEGER :: Kitstart

    LOGICAL :: load_worst, l_UseCurrentArea
    real (kind=double) ::  tcon,tmax

    iexit=0;
    tmax=pTmax
    kit=0;  tcon=-1; myvector=-1;l_thisCpuTime=-1;l_ThisSecTime=-1;ekt=0; maxdisp=0; displimit=0
    g_alfa= 1; g_admas= admasmin

#ifndef _DEBUG
     if(NompThreads >0)	  CALL OMP_SET_NUM_THREADS(NompThreads)
#endif
    if(.not. associated(saillist) ) then
        write(*,*) '(solveRK) no models!'
        iexit=0
        return
    endif

    call check_cl('DC',DrawEachCycle)
    call check_cl('LV',LimitV);

    forall(sli=1:sailcount, is_hoisted(sli))
        saillist(sli)%active_Originally = is_active(sli)
    end forall
    i=0;
    do sli=1,sailcount
        saillist(sli)%nmax=0
        if( is_active(sli)) i=i+ saillist(sli)%nodes%ncount
    end do
    load_worst=.FALSE.

    CALL CHECK_CL('WE', Windage_Each_Cycle);
    CALL CHECK_CL('GD',g_Gle_By_Diagonal)
    CALL CHECK_CL('CA',l_UsecurrentArea)
    if(gPrestress ) l_UseCurrentArea=.true.

      NOUT= 0

zv:     if( (Mass_Set_Flag.ne.0)) THEN		!  First Run these need to be called after a Bring All not here
                call  Analysis_Prep()
                saillist%tcon_this = 2.0 * Tmax
                g_converged=.TRUE.
        endif  zv
        CALL  Write_All_Flags(outputunit)

      write(outputunit,1503)

 1503   FORMAT('   Inverse     Eigenvalue          Max  ',/ &
              ,'  Timestep       Ratio       Force Imbalance')

    Mass_Set_Flag=1
    l_acceleration = 0.0 ;    l_acceleration(3) = p_vdamp  !   TEMP for Janet
     i=  All_Loading(l_acceleration)
!
!*****************************************************
!definitions  must agree with cfromf.cpp
#define STIFFSOLVE  1
#define RUNGEKUTTA  2
!*****************************************************


! ATTENTION! if node numbering changes during mainloop this will be wrong
    call SetModelRowIndices() ! set fnodeRowIndex regardless of whether UDC



mainloop:do while( kit<ktot )
            CALL Set_Connect_Coords(); ! for grandfathering ancient types of connect
            If(Mass_Set_Flag .ne.0) THEN
                  CALL Define_All_Masses();
                  Mass_Set_Flag=0
            ENDIF
            iexit= fc_runtopeak(STIFFSOLVE ,kit)
            nout=nout+1
            tcon= measureResiduals(LIslimax, rmaxN,rmax,.false.)  ; tconlast=tcon
            call drawonreset(kit,tcon ,maxdisp,displimit,0,myvector,l_thisCpuTime,l_ThisSecTime,0.0d00)
            if (tcon < pTmax) then
              write(outputunit,"('kit=',i6, ' max resid= ',g10.3, ' < ',g10.3,' so finish')" ,iostat=i )kit, tcon ,pTmax
              exit mainloop
            endif
!************************************************************************
!  question about Zero_All_Batten_Residuals(tmax)

 isr:       if (Stop_Running .NE. 0) then
                 iexit=2 ;exit mainloop
            endif isr
        enddo mainloop

!*****************************************************************
    if(Stop_Running .ne. 0) then
        write(outputunit,*) 'Analysis Killed by user'
        if(Stop_Running .eq. 99) then
                iexit = 3
        else
                IEXIT=2
        endif
    endif
     IF( KIT .ge. KTOT)  write(outputunit,*) 'Time limit exceeded',kit,ktot,kitstart


! reset the active flags. was 	active = active_Originally
    do  sli=1,sailcount
    if( is_hoisted(sli))ok=Set_Active(sli,saillist(sli)%active_Originally)
    enddo
        CALL Set_Connect_Coords()

ifhc:   IF(HardCopy) THEN
            CALL  PrintGlobalStuff(60)
            i=Windage_Loads(GPRINT)
            DO sli = 1,sailcount
                if(.not. is_Active(sli)) cycle
                 CALL Print_Model_As_PC(sli )
                 CALL Print_Model(sli )
            ENDDO
      ENDIF ifhc
end subroutine SolveRK




 function testeval(a,t) result(x) ! returns one displaced coord
     real (c_double), intent(in),dimension(:) :: a
     real(c_double) :: x ,t
     x = t + a(1)  *t + a(2)*t**2 + a(3) * t**3
 end function testeval

 function testeval2(a,t) result(x) ! returns displacement
     real (c_double), intent(in),dimension(:) :: a
     real(c_double) ,dimension(3) :: x ,t
     real(c_double) ,dimension(3,3) ::   a1,a2
     a1 = reshape(a(:9),(/3,3/))
     a2 = reshape(a(10:18),(/3,3/))
     x =  matmul(a1,t) + matmul(a2,t**2)
 end function testeval2
subroutine elementtest(a,b,c,rv)
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f
    use saillist_f
    use nodelist_f
    use coordinates_f
    use dbg, only: PrintOneMatrixForMtka,PrintOnevectorForMtka
    use fglobals_f
    use tanglobals_f
    USE tris_f
    use nonlin_f
    use rx_control_flags_f
    use stringList_f
    use tanpure_f
    use vectors_f
    use bar_elements_f
    use links_pure_f
    use cfromf_f
    IMPLICIT NONE

    LOGICAL, parameter::  StringCompAllowed = .false.  ! TRUE if negative tension allowed
    REAL (kind=double), allocatable, dimension (:,:) ::Dstring
    REAL (kind=double), allocatable, dimension (:,:) ::de
    REAL (kind=double), allocatable, dimension (: ) ::zte
    INTEGER , allocatable, dimension(:) :: nodenos

    logical :: bucklePermitted
    real*8  l_zi
    type(sail), pointer         ::sl
    type (string) , pointer	:: theString
    type (Fedge) , pointer	:: e


        real(kind=double),dimension(12,12) :: gl
        type (beamelement) , pointer	:: be
        real (c_double), intent(in),dimension(*) :: a,b,c
        INTEGER(c_int),intent(inout)  :: rv
        REAL(kind=double), dimension(3) :: X
        integer::i, k, sli, N, size,buckleEncouragement,err
        type(sail),pointer  ::theSail
        TYPE ( nodelist) ,POINTER :: nodes
        type(fnode), pointer :: nn, rn
        real(kind=double) ::fg(12)  !beam global_force

        REAL (kind= double), target  , dimension(9,9) :: S9
        LOGICAL  flag
        INTEGER   state
        REAL*8 d3(3),crease
        logical, parameter :: OldGeoMatrix = .false.
        logical, parameter :: NewGeoMatrix = .true.
        type(tri), pointer :: t
        real(kind=8), dimension(12) ::Xx    ! current   coords packed as a vector
        real(kind=8), dimension(9)  ::f9  ! the corner forces in world axes
        integer, save:: runnumber =0
        sli=1
        write(*,*) a(1),b(1),c(1)
        runnumber=runnumber+1
        FLUSH(outputUnit)
        write(outputUnit,'("myruns[[",i4, "]] ={ " )',iostat=err) runnumber;
    sl=>saillist(sli)
     rv=0;
     theSail=>saillist(sli)
     nodes=>theSail%nodes
    write(outputunit,*)
!   write the disp function.
    call PrintOnevectorForMtka("Coeffs",a(1:36),OutputUnit)
!           Apply the displacements to the nodes.
        call analysis_prep()
        CALL Make_Current_Edge_Lengths(sli) !this is in FormStiffnessMatrix
        CALL make_DD_and_G(sli)  ! uses   call check_cl('GC',UseCurrentGeom)

don:	 DO i=1,nodes%ncount
            nn=>nodes%xlist(i);  if(.not. nn%m_Nused) cycle
            k= issixnode(nn)
            if(k>0) cycle
            X = testeval2(a(1:18), nn%xxx)
            if(k <0) then
               rn=> nodes%xlist(-k)
               rn%xxx= testeval2(a(19:36), nn%xxx)
            endif
            nn%xxx= nn%xxx + x;
        enddo don

!          For each element
!                  write to mathematica..

!                          displaced nodes,
!                          strain,
!                          stress,
!                          element global forces,
!                          direct stiffness,
!                          geometric stiffness (and total stiffness)
!          end For each element
     CALL Make_Current_Edge_Lengths(sli) !this is in FormStiffnessMatrix

!    CALL AssembleTriElts(s,m)   !like ttmass9
    write(outputUnit,'(",(*tris=*) {",$)')
do_N:  DO N=1,theSail%tris%count
            flag = .false.
            t=>theSail%tris%list(N)
            if(.not. t%use_tri) cycle
            t%m_stress = 0
              write(outputUnit,'("{" )')
            geoMatrix = .false.

            forall(k=1:3) xx(3*k-2:3*k) = t%nr(k)%m_fnpe%xxx;               call PrintOneVectorForMtka("xloc",xx(:9),OutputUnit);write(outputUnit,'(",")')
            forall(k=1:3) d3(k) = t%m_er(k)%m_fepe%m_delta0
            t%m_eps = matmul(t%g,d3);                                        call PrintOneVectorForMtka("eps",t%m_eps,OutputUnit);write(outputUnit,'(",")')
            call Get_NL_Stress_From_Tri(t,t%m_eps,t%m_stress,crease,state); call PrintOneVectorForMtka("sig",t%m_stress,OutputUnit);write(outputUnit,'(",")')

            call One_Tri_Global_Forces(t,f9);            call PrintOneVectorForMtka("f9",f9,OutputUnit);write(outputUnit,'(",")')
            CALL one_tri_global_stiffness_oona(t,s9);   call PrintOneMatrixForMtka("DirectStiff",s9,OutputUnit);write(outputUnit,'(",")')

            geoMatrix = .true.
            CALL one_tri_global_stiffness_oona(t,s9)
            call PrintOneMatrixForMtka("FullStiff",s9,OutputUnit)
              if( N.eq.theSail%tris%count  )        write(outputUnit,'("}" )')
              if( N.ne.theSail%tris%count  )        write(outputUnit,'("}," )')
    ENDDO do_N
             write(outputUnit,'("}, (* a tri is xloc,eps,sig,f9,DS,FS *) " )')
!   k= AssembleBeamElts(sli,m)
    write(outputUnit,'(" (*beams=*) {",$)')
   do_b : DO N=1,theSail%fbeams%count
        be =>theSail%fbeams%list(n)
        if(.not. be%m_used) cycle
        geoMatrix = .false.;              write(outputUnit,'("{" )')
        gl = one_beam_global_stiffness(be,g_dxdr,geomatrix,g_Bowing,g_BowShear); call PrintOneMatrixForMtka("DirectStiff",gl,OutputUnit);write(outputUnit,'(",")')
        geoMatrix = .true.
        gl = one_beam_global_stiffness(be,g_dxdr,geomatrix,g_Bowing,g_BowShear); call PrintOneMatrixForMtka("totstiff",gl,OutputUnit);write(outputUnit,'(",")')
        xx(1:3)  = be%xn1%xxx; xx(4:6)  = be%tn1%xxx; xx(7:9)  = be%xn2%xxx;
        xx(10:12)= be%tn2%xxx;                                          call PrintOneVectorForMtka("xloc",xx(:12),OutputUnit);write(outputUnit,'(",")')

        fg =- one_beam_element_force(be,G_Bowing,G_Bowshear);         call PrintOneVectorForMtka("f12",fg,OutputUnit)

      if( N.eq.theSail%fbeams%count  )        write(outputUnit,'("}" )')
      if( N.ne.theSail%fbeams%count  )        write(outputUnit,'("}," )')
    ENDDO do_b
     write(outputUnit,'("}," )')
!    CALL AssembleStringElts(sli,StringCompAllowedFlag,m) ! see CS_String_Masses(sli,StringCompAllowed)

 CALL CS_Check_All_String_Order(sli)  ! this is SLOW BUT SAFE

 if(StringCompAllowed) then ;buckleEncouragement =1; else ;  buckleEncouragement =-1 ;endif

 CALL CS_String_Tensions(sli,buckleEncouragement) !last arg is now integer. 1 encourages buckling
    write(outputUnit,'(" (*strings=*) {",$)')
   do_s : DO N=1,sl%strings%count
     theString =>sl%strings%list(n)%o
     if(theString%d%Ne .le.0) cycle
     size = 3*(theString%d%Ne+1) ;              write(outputUnit,'("{" )')
     allocate(Dstring(size,size))
     allocate(de(3,TheString%d%Ne))
     allocate(nodenos(theString%d%Ne+1))
     allocate (zte(theString%d%Ne))
              Dstring = 0.0D00
         bucklePermitted =  ((theString%d%m_CanBuckle3 + buckleEncouragement) >=0)
ifb:        if ((theString%TQ_S(1) .GT. 0.0) .OR.  bucklePermitted) THEN
do_i:		 DO I = 1,TheString%d%Ne
                  e => sl%edges%list(theString%se(i))
                  if(theString%rev(i)) THEN
                         de(1:3,i) =-e%m_dlink; Nodenos(i)=e%m_L12(2);  Nodenos(i+1)= e%m_L12(1)
                  ELSE
                         de(1:3,i) = e%m_dlink; Nodenos(i)=e%m_L12(1);  Nodenos(i+1)= e%m_L12(2);
                  ENDIF
                  zte(I) = e%m_Zlink0 + e%m_delta0
              ENDDO do_i


              CALL One_String_Geo_Stiffness(size,Dstring,de,theString%d%Ne,theString%tq_s(1),zte) ! Doesnt check for buckling
              call PrintOneMatrixForMtka("stringGeostiff",dString,OutputUnit);write(outputUnit,'(",")')
if0:          if(0 .eq.  TheString%d%m_TCnst) THEN  ! ea is non-zero
                 l_zi = theString%d%m_zisLessTrim + theString%d%m_Trim
                 CALL One_String_Sliding_Dir_Stiffness(size,Dstring,de,theString%d%Ne,l_zi,theString%d%EAS)! Doesnt check for buckling
              ENDIF   if0
           ENDIF ifb
               call PrintOneMatrixForMtka("stringTotstiff",dString,OutputUnit)
        deallocate(Dstring)
        deallocate(de)
        deallocate(nodenos)
        deallocate(zte)
        if( N.eq.theSail%strings%count  )        write(outputUnit,'("}" )')
        if( N.ne.theSail%strings%count  )        write(outputUnit,'("}," )')
    ENDDO do_s
    write(outputUnit,'("}" )')
 !    k= AssembleBattenElts(sli,m)  ! see FMake_All_Batten_Matrices

 !   k = AssembleLinkElts(sli,StringCompAllowedFlag,m) ! SEE MakeAllLinkMasses(sli,StringCompAllowed)

  write(outputUnit,'("};" )')
flush(outputUnit)
end subroutine elementtest

! CAREFUL!! FormStiffnessMatrixByTest changes the load and residual values
subroutine FormStiffnessMatrixByTest(m)  bind(C, name= 'cf_FormStiffnessMatrixByTest')
use cfromf_f
use rx_control_flags_f
use saillist_f
use tris_f
use ttmass_f , only :ALL_LOADING
    use dofoprototypes_f

implicit none
     integer(kind=cptrsize),intent(out) :: m
     logical wsafe
     integer i, k,r,c,n,sli,ok,kit
    character(len=64) :: myfmt
       real(C_DOUBLE), allocatable, dimension(:) :: d, r1,r2
       real(c_double)  :: eps;
       real(kind=double) ,dimension(3) :: l_acceleration
   eps = 1.0D-5 ; kit=0
   l_acceleration = 0.0 ; ! assume independent of position

 !  SPD  is 9.  We could use SPD (9) if we dont have geometric stiffness
 !  which is usually non-symmetric according to the literature.
  ! We usually use type RXM_RECTANGLE ( a general matrix) . However we could say Structurally Symmetric Matrices
  ! But then we cant use pardiso to

     m = cf_NewSparseMatrix(matrixType_Sraw) ! should be RSPD but we need the full matrix for the multiplications

 !* Wrinkling allowed by default.
 !* switch CS turns it off
 !* If this is NOT turned off, a further switch WK turns ON
 !* allowance for wrinkling in the calculation of the stiffness matrix.

 !* NOTE  g_Wrinkling_Allowed doesnt do anything. debug2 is the wrinkling flag

        wsafe = debug2
       debug2=K_on_Wrinkled_Matrix
  n=JaySize()-1
  write(myfmt,'("(a,", i5,"(g14.5,2x))" )')n

  allocate(d(n),r1(n),r2(n),stat=k)
  d=0;

 call cf_AddToSparseMatrix(m,1,1, 0.0d00 )
  call cf_AddToSparseMatrix(m,n,n,0.0d00 )
  !method
  ! 1) generate a vector of coordinates
  ! 2) for each coordinate, perturb it, measure residuals and that gives the
  !    column of the matrix
  sl: DO sli=1,SailCount
      if (0== is_Hoisted(sli) ) cycle
      if (0== is_active(sli)) cycle
                CALL Make_Current_Edge_Lengths(sli)
                CALL make_DD_and_G(sli)
    enddo sl

do_c: do c = 1,n
 !      write(120,*) ' by test c = ',c

        d=0;        r1=0;
        d(c) =  eps/2.0d00; !      write(120,myfmt) 'D=   ',d
        call ApplyDisplacements(d,.true.)
! all this sums the internal and external loads into node%R and dofo%rt
        i=  All_Loading(l_acceleration)
        do sli=1,sailcount
            if(.not. is_active  (sli)) cycle
            ok=zero_r(sli);
            do  i = 1,saillist(sli)%nodes%ncount
                saillist(sli)%nodes%xlist(i)%Rold = get_Cartesian_R(i,sli)
                call Set_R(i,sli, saillist(sli)%nodes%xlist(i)%P )
            enddo
          ENDdo
         k = DOFO_TestForCapture(kit) ! test for far side.  Cant test for scoot here
         k = DOFO_UpdateJay(  )  ! experiment to move this before all residuals
         CALL All_Residuals()
         k = DOFO_TestForRelease( )
!  end of all this sums....
        call ExtractResiduals(r1); !       write(120,myfmt) 'R1=  ',r1

        d=0; r2=0;
        d(c) =   -eps  ;!   write(120,myfmt) 'dc)-e',d
        call ApplyDisplacements(d,.true.)
! all this sums the internal and external loads into node%R and dofo%rt
        i=  All_Loading(l_acceleration)
        do sli=1,sailcount
            if(.not. is_active  (sli)) cycle
            ok=zero_r(sli);
            do  i = 1,saillist(sli)%nodes%ncount
                saillist(sli)%nodes%xlist(i)%Rold = get_Cartesian_R(i,sli)
                call Set_R(i,sli, saillist(sli)%nodes%xlist(i)%P )
            enddo
          ENDdo
         k = DOFO_TestForCapture(kit) ! test for far side.  Cant test for scoot here
         k = DOFO_UpdateJay(  )  ! experiment to move this before all residuals
         CALL All_Residuals()
          k = DOFO_TestForRelease( )
!  end of all this sums....
        call ExtractResiduals(r2);!        write(120,myfmt) 'R2=  ',r2

        r1 = (r2 - r1)/eps ;!        write(120,myfmt) 'dr/eps ',r1
        do r=1,n
                if(abs(r1(r)) .gt. 1.0d-15) then
              !  write(120,'(2i6,g15.6)')  r,c,r1(r)
                call cf_AddToSparseMatrix(m,r,c, r1(r) )
                endif
        enddo
        d=0
        d(c) =  eps/2.0d00;  ;
        call ApplyDisplacements(d,.true.)
      !  write(120,*)
    enddo do_c
    debug2=wsafe
    deallocate(d,r1,r2)
end subroutine FormStiffnessMatrixByTest
end module solveimplicit_f
