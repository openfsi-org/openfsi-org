MODULE gletypes_f
USE basictypes_f
IMPLICIT NONE
!
! NOTE blocked is used WRONGly if there are more than one GLE

!  declaration of the data types

	TYPE :: gle
		CHARACTER (len=256) :: Mname
		LOGICAL ::  used, blocked, NeedToUpdate
  		INTEGER :: n, cptr 
		REAL  (kind=double), dimension (:,:) , POINTER ::  Kmatrix
		CHARACTER (len=64), dimension(:), POINTER ::  NodeNames
		INTEGER , dimension(:), POINTER ::  NodeNos
		REAL  (kind=double), dimension (:) , POINTER ::  F0, X0
		REAL  (kind=double), dimension (:) , POINTER ::   RLbuf,Xlbuf
		REAL (kind=double) :: tolerance
!   text for the axis nodes
!   refs to the axis nodes
		REAL  (kind=double), dimension (3,3) :: T !Tlocal  ! transformations
		integer node1,node2,node0
		integer :: sli = 0

	END TYPE gle

	TYPE :: GleList
		TYPE (Gle), DIMENSION(:)  ,POINTER :: list
		INTEGER :: count, block 
	END TYPE GleList


END MODULE gletypes_f

