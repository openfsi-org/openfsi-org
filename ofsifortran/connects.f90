! CONNECTS.F
!!

! May 2008
!
! we should connect to any Hoisted  models.

! if resolve connects finds a connect to a hoisted but NOT active model it should fully-fix the 
! connected nodes in the inactive model, after making the connect as usual
!
!Oct 2006 rnblank->len_trim

! Dec 5th while removing strng com from SetConnectCoords found a mismatch between rev in the string and strn gcom

! 26 May 2003 starting to use stringlist not strng com
! first, get resolve connects to find s1 and s2 as well as n1 and n2
! Then, when that is OK, work through replacing strn gcom
! then do the same in links, then shapegen then finally remove from readdat.
!
! 5 april 00 needs a sort-connect routine 
!	needs coding of the non-sliding string connect. get e,t by linits  ...DONE NOT tested
!	there is a question over resolving connects. Should only resolve on Hoisted sails... 
 !	Look in saillist TODO
!	There is a suspicion over whether (e and t) are correctly set ... DONE 
!	Q Where does B get set for sliders?  DONE
!

MODULE connects_f
USE ftypes_f
USE  linklist_f
IMPLICIT NONE

! static declaration of the conlist
	SAVE
	TYPE (ConnectList) :: ConList 
	integer,parameter  :: C_NODE = 29
	integer ,parameter :: C_STRING = 44
	integer,parameter :: C_SPLINE = 53
	integer,parameter :: C_ITSACURVE  =9999999
CONTAINS


subroutine addtoconnectlist(clist ,c)
implicit none
type (connectholder) , pointer,dimension(:) :: clist

type(connect), pointer:: c
type (connectholder) , pointer,dimension(:) :: temp
integer i, istat
    if(.not. associated(clist)) then
       allocate(clist( 1))
        clist(1)%o=>c
    return
    endif

    do i=1,ubound(clist,1)
        if( associated( clist(i)%o ,c ) )  then  ! its already there
            return
        endif
    enddo
    do i=1,ubound(clist,1)
        if(.not. associated(clist(i)%o)) then  ! use a blank space
            clist(i)%o=>c
            return
        endif
    enddo

    allocate(temp(1+ubound(clist,1)))  ! append
    temp(1:ubound(clist,1)) = clist
    temp(1+ubound(clist,1))%o=>c
    deallocate(clist,stat=istat)
    allocate(clist( ubound(temp,1)))
    clist=temp
    deallocate(temp)

end subroutine addtoconnectlist


subroutine  InitFixedConnect_T_S(c)
	use fstrings_f
	IMPLICIT NONE
	TYPE (connect) ,INTENT (inout)  :: c
!case not sliding
! t% n2  is the slave string
! t% n1  is the master string
!  fill in t and e by looking at the linit of n2
! First walk the slavelist adding up the edgelengths
! do the same to the masterlist
! then see in which (e,t) of the masterlist we fall


	REAL (kind=double) ::  l1,l2
	REAL  (kind=double), dimension (:) , POINTER :: t1,t2
	integer :: istat, n1,n2 ,i,j

	n1 = c%s1%d%Ne +1 
	n2 = c%s2%d%Ne +1 

	ALLOCATE(t1(n1),stat=istat)
	if(istat /=0) THEN
		write(outputunit,*) ' out of memory'
		return
	endif
	ALLOCATE(t2(n2),stat=istat)
allocok: 	if(istat ==0) THEN

	if(.not. CS_GetStringEdgeLengths(c%s1, t1)  ) write(outputunit,*) ' t1 not got' ! masters 
	if(.not. CS_GetStringEdgeLengths(c%s2, t2)  ) write(outputunit,*) ' t2 not got'! slaves 

	l1 = t1(n1)
	l2 = t2(n2)
	t2 = t2 * l1/l2	! map the slave onto the master
	l2 = l1
!debug print
	if(n2 .ne. ubound(c%slavelist,1)) then
		write(outputunit,*) ' n2 and slavelist length differ '
		write(outputunit,*) 'n1,n2, slavelistLength ',n1,n2, ubound(c%slavelist,1)
	endif

	loop1 : do i=1,ubound(c%slavelist,1)
		c%slavelist(i)%e=-5
		loop2 : do j=2,n1
			if(t1(j) .ge. t2(i) .or. (j .eq. n1)) then
				if( j .eq. n1 .and. t1(j) .lt. t2(i)) then
					write(outputunit,*) ' CAUGHT INDEX OFF END'
				endif
				c%slavelist(i)%e=j-1
				c%slavelist(i)%t = (t2(i)-t1(j-1))/(t1(j) - t1(j-1))
				exit loop2
			endif
		end do loop2	
		if(c%slavelist(i)%e .eq. -5) then
			write(outputunit,*)  ' in InitFixedConnect_T_S a slave node not hooked up '
			write(outputunit,*) ' slavelist index = ',i ,' scaled T ',  sngl(t2(j))
			write(outputunit,*) ' master Ts are ' , t1
	
		!else 
		!	write(outputunit,*) ' slave ',i,'at ', t2(i),'  hooked to (e, t) ', s%e, s%t
		endif
	end do loop1
! loop2  assumes that the numbering of both runs in the same sense.  

	DEALLOCATE(t2,stat=istat)
	else
		write(outputunit,*) ' out of memory'
	endif  allocok
	DEALLOCATE(t1,stat=istat)


end  subroutine  InitFixedConnect_T_S


subroutine InitSlaveList(c)
! case sliding
! use nodal geometry to fill in the slavelist
	IMPLICIT NONE
	TYPE (connect),INTENT (inout)  :: c
	integer  :: i 
! horrible logic!!!!!!!!
	if(c%m_sliding.or. c%type2==C_NODE) then ! find nearest point on s to node n. Fill in e and t
		do i=1,ubound(c%slavelist,1)
			!s=> c%slavelist(i)
			CALL UpdateConnect_T_S( c%slavelist(i));  
		enddo
	else  !  find the t's by reference to the Linits of the 
		call InitFixedConnect_T_S(c);  
	endif
end  subroutine InitSlaveList 

subroutine UpdateConnect_T_S( s) !irrelevant for splines
USE vectors_f
use saillist_f
use coordinates_f
use stringList_f
	IMPLICIT NONE
	TYPE (Nconntype),intent(inout)  :: s

	REAL (kind=double)  ,dimension(3) :: XXX
       
	integer  :: j, TheEdge ,index!  kkk for dbs
	logical found 
	REAL (kind=double)  ,dimension(3) :: x1,x2,v,a,xp
	REAL (kind=double)   :: score
	REAL (kind=double) ::   t,dotp,la, scoremin,lv,frac
	type (string), pointer :: TheString
	

	TYPE (edgelist),pointer:: edg
	!write(outputunit,*) ' UpdateConnect_T_S with m_sli= ', s%m_sli, ' s%n= ', s%m_n
	edg=>saillist( s%sr%sli )%edges  ! was s%m_sli
	TheString=>saillist(s%sr%sli)%Strings%list(s%sr%n)%o ;! MASTER write(outputunit,*) ' this string has ne = ', TheString%d%Ne, trim(TheString%m_text)

	xxx=get_Cartesian_X(s%m_ncn,s%m_ncsli)  ! slave X
	found = .false.
	scoremin = 10000000000.
    call FindNearestSegOnString(thestring,xxx,.true. ,index,frac)
    s%t = frac
	s%e = index
	return
    
do_j:	  do j=1,TheString%d%Ne
		score = 1000000001.

		TheEdge = TheString%se(j); !write(outputunit,*) ' edge is (no, L12) ',TheEdge,edg%list(TheEdge)%m_L12
		if(TheString%rev(j) ) then
			x2 = get_Cartesian_X(edg%list(TheEdge)%m_L12(1), theString%m_sli)
			x1 = get_Cartesian_X(edg%list(TheEdge)%m_L12(2), theString%m_sli)		
		else
			x1 = get_Cartesian_X(edg%list(TheEdge)%m_L12(1), theString%m_sli)
			x2 = get_Cartesian_X(edg%list(TheEdge)%m_L12(2), theString%m_sli)					
		endif
		v= x2-x1
		lv =  norm(v)  
		a = xxx - x1
		la = norm(a)
		dotp = dot_product(a,v) /lv
		if((dotp .lt. 0.0).and. (j .gt. 1)) cycle

		call vprod(a,v,xp)
		score = norm(xp)/ lv  ! perpendicular distance
		if(lv .gt. 1E-10) then		! march 2003 was la dont see why
			t = dotp/lv
		else
			t=0.0
		endif
ift:	if(.true.) then  ! a debug test where we don't score on perp distance just on parallel March 2003
		if((t .ge. 1.00D00) .and. ( j .lt. TheString%d%Ne)) cycle
		if(found) then
		    write(outputunit,*) ' double find '
		endif
		found = .true.	
		if(s%e .ne. j) then
			!write(outputunit,*)s%n, ' e was ',s%e,' is ', j,' t was ',s%t, ' is ',t
			!s%C = .true.

			if(j > s%e) then	! 	now move the point to the vertex. Setting t SB enough
				!t = 0.00D00; write(outputunit,*)  ' set T to 0.0'
				!X(s%n,1:3) = x1	
			else
				!t = 1.0D00; write(outputunit,*)  ' set T to 1.0'
				!X(s%n,1:3) = x2	
			endif		
		else
			!s%C = .false. 
		endif
		s%t = t
		s%e = j
 ! if iftloop is OK we can move found to here and so check for double-finds
	else	ift	! old method 
		!s%C=.false.
		if((dotp .ge. lv) .and. ( j .lt. TheString%d%Ne)) score = score+dotp-lv
		if(score .lt. scoremin) then
			if(s%e .ne. j) then
				write(outputunit,*)s%m_ncn, ' e was ',s%e,' is ', j,' t was ',s%t, ' is ',t
			endif
			scoremin = score
			s%t = t
			s%e = j
			found = .true.
		endif
	endif  ift
	enddo do_j
	if(s%e .gt. TheString%d%Ne) then
	write(outputunit,*) ' found closest point OOR at e= ',s%e ,'  t= ', s%t, 'score = ',sngl(scoremin)
	write(outputunit,*) ' where  ne is ', TheString%d%Ne
	s%e = TheString%d%Ne ! ne(s%ssd)
	endif
	if(.not. found) then
		write(outputunit,*)'connect not found:',s%m_ncn ,sngl(xxx)
	endif
!	write(outputunit,*) ' leaveUpdateConnect_T_S '
END subroutine UpdateConnect_T_S

subroutine  SetConnectB_Matrix()
	use saillist_f
	IMPLICIT NONE
! sets slave(2) B to be parallel to the master string 
	integer :: i,j 
	TYPE (Connect)  ,POINTER :: t
	TYPE (Nconntype)  ,POINTER :: s
	TYPE (string) , POINTER :: TheString
	type(fnode) , pointer   :: thenode
	REAL (kind=double)  ,dimension(3,3) :: XX
	integer :: TheEdge,see
!
!  ONLY treats sliding slaves
!   Called at the point where XM has been transferred and trimmed
! and B has been set
! This routine sets B on sliding slaves as for sliding fixities
! there is no need to adjust the master B as this has already been done
! NOTE  B is no longer the inverse of XM with its parallel component removed
!  we cant invert that XM because it is singular. 
!  we might be able to do something with CVD.
!  The result is that a connect B is a bit different from  a slider B with the same geometry.
! a simple example (slide on Y) gave about 13%. higher B (which means a smaller timestep) 
!
!

	do i = 1,  ConList%count
		t=>ConList%list(i)%o
		if(t%used .and.  t%resolved  .and. t%m_Sliding .and.Associated(t%slavelist ) )  then
		    if(t%m_isspline) then; cycle; endif
			jloop: do j=1,ubound( t%slavelist ,1)
				s=> t%slavelist(j)
				see = s%e  
				TheString=>saillist(s%sr%sli)%Strings%list(s%sr%n)%o  ! Dec 5 2003
				TheEdge =  TheString%se(see)
				thenode=>saillist(s%m_ncsli)%nodes%xlist(s%m_ncn )
				xx= thenode%b	
				call RemoveParallelComponent3(xx,  TheEdge, s%sr%sli)	
				 thenode%b =  thenode%b  - xx
			end do jloop
		endif
	enddo




end subroutine SetConnectB_Matrix

subroutine TransferConnectMasses()
	use saillist_f
	IMPLICIT NONE

! moves a part of slave(2) xm to zero to the master(1) 

 
	integer :: i,j   ,n1,n2
	TYPE (Connect)  ,POINTER :: t
	TYPE (Nconntype)  ,POINTER :: s
	TYPE (string) , POINTER :: theString
	REAL (kind=double)  ,dimension(3,3) :: XX
	integer :: TheEdge,see,l_sli
    type(Fnode) , pointer ::thenode
    TYPE (edgelist),pointer:: edg

	do i = 1,  ConList%count
		t=>ConList%list(i)%o
	
		if(t%used .and.  t%resolved)  then
		    if(t%m_IsSpline) then  
!		write(outputunit,*)' skip TransferConnectMasses for spline'; 
		        cycle
		     endif
			if((t%type1==C_NODE) .and. (t%type2==C_NODE)) then
			
#ifndef DOFO_CONNECTS
			saillist(t%m_sli1)%nodes%xlist(t%m_node1 )%xm =saillist(t%m_sli1)%nodes%xlist(t%m_node1 )%xm  + saillist(t%m_sli2)%nodes%xlist(t%m_node2 )%xm 
			saillist(t%m_sli2)%nodes%xlist(t%m_node2 )%xm =0.0
#else
			!write(outputunit,*) 'TransferConnectMasses dofo connect N-N  '
#endif
			else if(t%type1==C_STRING) then
				do j=1,ubound( t%slavelist ,1)
					s=> t%slavelist(j)
					see = s%e  
					TheString=>saillist(s%sr%sli)%Strings%list(s%sr%n)%o  ! Dec 5 2003
					TheEdge =  TheString%se(see)
					edg=>saillist(thestring%m_sli)%edges	
					if(TheString%rev(see)) then   
						n2 = edg%list(TheEdge)%m_L12(1)
			            n1 = edg%list(TheEdge)%m_L12(2)						
					else
						n1 = edg%list(TheEdge)%m_L12(1)
			            n2 = edg%list(TheEdge)%m_L12(2)
					endif
		            thenode=>saillist(s%m_ncsli)%nodes%xlist(s%m_ncn)
					xx= thenode%xm
					if(t%m_Sliding) then	
						call RemoveParallelComponent3(xx, TheEdge,s%m_ncsli)
					endif
					l_sli = thestring%m_sli
					saillist(l_sli)%nodes%xlist(n1)%xm   =  saillist(l_sli)%nodes%xlist(n1)%xm  + xx*(1.-s%t) 
					saillist(l_sli)%nodes%xlist(n2)%xm   =  saillist(l_sli)%nodes%xlist(n2)%xm  + xx* s%t
					thenode%xm  =  thenode%xm  - xx
				
				enddo
			endif
		endif
	enddo
end subroutine  TransferConnectMasses


subroutine Set_Connect_R() 
	use saillist_f
	use coordinates_f
	IMPLICIT NONE

! sets slave(2) R to zero after adding it to the master Rs(1)
  
	integer :: i,j  ,n1,n2,ok
	TYPE (Connect)  ,POINTER :: t
	TYPE (Nconntype)  ,POINTER :: s
	REAL (kind=double)  ,dimension(3) :: XX ! ,zero
	TYPE (string) , POINTER :: theString
	integer :: TheEdge,see
	logical error

TYPE (edgelist),pointer:: edg

	do i = 1,  ConList%count
		t=>ConList%list(i)%o
 		if(t%used .and.  t%resolved)  then
 			if(t%m_IsSpline) then    ! now handled by DOFO
 			    cycle; 
 			endif
		   if((t%type1==C_NODE) .and. (t%type2==C_NODE)) then ! now handled by DOFO
#ifndef DOFO_CONNECTS	
			ok=increment_r(t%m_node1,get_Cartesian_R(t%m_node2,t%m_sli2),t%m_sli1 )  commented out
			zero=0.0
			call Set_R(t%m_node2,t%m_sli2,zero)
#endif
		   else if(t%type1==C_STRING) then
			do j=1,ubound( t%slavelist ,1)
				s=> t%slavelist(j)
				see = s%e  
				TheString=>saillist(s%sr%sli)%Strings%list(s%sr%n)%o  ! Dec 5 2003
				edg=>saillist(s%sr%sli)%edges	
				TheEdge =  TheString%se(see)
				if(TheString%rev(see)) then    
					n2 = edg%list(TheEdge)%m_L12(1)
			        n1 = edg%list(TheEdge)%m_L12(2)
				else
					n1 = edg%list(TheEdge)%m_L12(1)
			        n2 = edg%list(TheEdge)%m_L12(2)
				endif
		
				xx= get_Cartesian_R(s%m_ncn,s%m_ncsli) ! old style string-string connect
				if(t%m_Sliding) then
					if(.false.) then
						write(outputunit,*) s%m_ncn,'=slave needs ker-Klunk test' 
						error = KerKlunkTest(xx,s) ! changes xx if necessary
					else	
					!call RemoveParallelComponentByE(xx,se(s%e,s%s))
						call RemoveParallelComponent(xx,s) ! smooth variety
					endif
				endif
                                ok= increment_R(n1, xx*(1.-s%t),s%sr%sli )
				ok= increment_R(n2, xx* s%t,s%sr%sli)
				ok= increment_R(s%m_ncn, -xx,s%m_ncsli)
			enddo
		   endif	
		endif
	enddo

end subroutine Set_Connect_R

function GetConnectTangent( s,error) result (v) ! normalised
	USE vectors_f
	use saillist_f
	use coordinates_f
	IMPLICIT NONE
 

	TYPE (Nconntype)  ,POINTER :: s
	REAL (kind=double)  ,dimension(3) :: v
	REAL (kind=double) ,dimension(3)  :: x1,x2
	logical :: error
	REAL (kind=double) :: lv
	TYPE (string) , POINTER :: theString
	integer :: TheEdge,see
    TYPE (edgelist),pointer:: edg
     

	see = s%e  
	TheString=>saillist(s%sr%sli)%Strings%list(s%sr%n)%o  ! Dec 5 2003
	TheEdge =  TheString%se(see)
    edg=>saillist(s%sr%sli)%edges	
	if(TheString%rev(s%e)) then
		x2 = get_Cartesian_X(edg%list(TheEdge)%m_L12(1), theString%m_sli)
		x1 = get_Cartesian_X(edg%list(TheEdge)%m_L12(2), theString%m_sli)		
		
	else
		x1 = get_Cartesian_X(edg%list(TheEdge)%m_L12(1), theString%m_sli)
		x2 = get_Cartesian_X(edg%list(TheEdge)%m_L12(2), theString%m_sli)	
	endif
	v= x2-x1 ;lv =  norm(v)
	if(lv < 1.0E-20) then
		error = .true. ; return
	endif
	error = .false.
	v = v/lv
end function GetConnectTangent

function GetSmoothedConnectTangent(  s,error) result (v)
	USE vectors_f
	use saillist_f
	IMPLICIT NONE
	TYPE (Nconntype)  ,POINTER :: s
	REAL (kind=double)  ,dimension(3) :: v,v1,v2,v0
	logical :: error
	TYPE (string) , POINTER :: theString
	integer :: TheEdge,see
	see = s%e  
	TheString=>saillist(s%sr%sli)%Strings%list(s%sr%n)%o  ! Dec 5 2003
	TheEdge =  TheString%se(see)
	v1 = GetConnectTangent( s,error)
	if(s%t > 0.5) then
 		if( s%e < TheString%d%Ne) then
			s%e = s%e + 1
			v2 = GetConnectTangent( s,error)
			s%e = s%e - 1
		else
			v2=v1
		endif
		v = v2 * (s%t-0.5D00) + v1 * (1.5D00 - s%t)
	else
 		if( s%e >1 ) then
			s%e = s%e - 1
			v0 = GetConnectTangent( s,error)
			s%e = s%e + 1
		else
			v0=v1
		endif
		v = v1 * (s%t + 0.5D00) + v0 *( 0.5D00-s%t)
	endif
end function GetSmoothedConnectTangent


subroutine RemoveParallelComponent(xx, s) ! smooth variety
	IMPLICIT NONE
	REAL (kind=double)  ,dimension(3) :: XX
	TYPE (Nconntype)  ,POINTER :: s

	REAL (kind=double)  ,dimension(3) :: v
	REAL (kind=double) ::   mag
	logical :: error

! get the edge vector v

	v=GetSmoothedConnectTangent(  s,error)

	if(error) then
		write(outputunit,*) ' cannot normalise edge '
		return
	endif
!! the parallel component   has magnitude mag and direction V
	mag  = DOT_PRODUCT(xx,v)
	xx = xx - v * mag
end subroutine  RemoveParallelComponent

logical function KerKlunkTest(r,s) result(error)
	USE saillist_f
	IMPLICIT NONE
	REAL (kind=double)  ,dimension(3) :: R
	TYPE (Nconntype)  ,POINTER :: s 
 
	REAL (kind=double)  ,dimension(3) :: vBelow,vAbove,v1
	REAL (kind=double) , dimension(3) :: v
	REAL (kind=double) ::   mag
	logical ::  onLowerFree, onUpperFree
	TYPE (string) , POINTER :: theString

	TheString=>saillist(s%sr%sli)%Strings%list(s%sr%n)%o  ! Dec 5 2003

	error = .false.
!
! 1) Identify the master segments below and above 
! 2) Find the tangents below and above vBelow,vAbove

	v1 = GetConnectTangent( s,error)
	if(s%t > 0.5) then
 		if( s%e < TheString%d%Ne) then
			s%e = s%e + 1
			vAbove = GetConnectTangent( s,error)
			s%e = s%e - 1
		else
			vAbove=v1
		endif
		vBelow = v1
	else
 		if( s%e >1 ) then
			s%e = s%e - 1
			vBelow = GetConnectTangent( s,error)
			s%e = s%e + 1
		else
			vBelow=v1
		endif
	endif
	if(error) then
		write(outputunit,*) 'KerKlunkTest error'
		return
	endif
! 3) 
	onLowerFree =  (DOT_PRODUCT(R,vbelow )< 0.0)
	onUpperFree =  (DOT_PRODUCT(R,vAbove )> 0.0)

	if(onLowerFree.and. .not. onUpperFree) then 
		write(outputunit,*) ' free on lower'
		v=vBelow
		s%kkv=-1
	else if(onUpperFree.and. .not. onLowerFree) then
		write(outputunit,*) ' free on upper'
		v=vAbove
		s%kkv=-1
	else if (.not. onUpperFree.and. .not. onLowerFree) then
		write(outputunit,*) ' Klunked'
		return 
	else 
		write(outputunit,*) ' free on upper AND lower'
		error = .true.
		return

	endif

!! the parallel component   has magnitude mag and direction V
	mag  = DOT_PRODUCT(R,v)
	R = R - v * mag
end function  KerKlunkTest


subroutine RemoveParallelComponentByE(xx, e,p_sli) ! p_sli is the model owning e
	USE vectors_f
	use coordinates_f
use saillist_f	
	IMPLICIT NONE
	REAL (kind=double)  ,dimension(3) :: XX
	INTEGER,intent(in) :: e ,p_sli  ! an edge

	REAL (kind=double)  ,dimension(3) :: v
	REAL (kind=double) :: L, mag
	logical :: error
    TYPE (edgelist),pointer:: eds
    integer ,dimension(2) :: n  
    eds=>saillist(p_sli)%edges
 
! get the edge vector v
    n = eds%list(e)%m_L12
	v=get_Cartesian_X(n(2),p_sli) - get_Cartesian_X(n(1),p_sli) 
	call normalise(v,l,error) 
	if(error) then
		write(outputunit,*) ' cannot normalise edge ',e, ' in model ',p_sli
		return
	endif
!! the parallel component   has magnitude mag and direction V
	mag  = DOT_PRODUCT(xx,v)
	xx = xx - v * mag
end subroutine  RemoveParallelComponentByE

subroutine RemoveParallelComponent3(xx, e,sli)
	USE vectors_f
	use coordinates_f
	use saillist_f
	IMPLICIT NONE
	REAL (kind=double)  ,dimension(3,3) :: XX
	INTEGER,intent(in) :: e ,sli  ! an edge

	REAL (kind=double)  ,dimension(3) :: v
	REAL (kind=double)  ,dimension(3,3) :: t, A
	REAL (kind=double) :: l
	logical :: error
	integer :: j, k
	integer, dimension(2) ::n
    TYPE (edgelist),pointer:: eds
    eds=>saillist(sli)%edges
	
! get the edge vector v
    n = eds%list(e)%m_L12
	v=get_Cartesian_X(n(2),sli) - get_Cartesian_X(n(1),sli) 	
	
	call normalise(v,l,error) 
	if(error) then
		write(outputunit,*) ' cannot normalise edge ',e
		return
	endif
! get the matrix t 
        	DO K=1,3
       	  DO J=1,3
        	    T(k,j) =  v(k) * v(j)
        	  ENDDO
        	ENDDO
! the parallel component  A
	A = matmul(T, matmul(xx,T))
	xx = xx - a
end subroutine  RemoveParallelComponent3

FUNCTION  Create_Connect(m,lm,s,ls,cptr) result(n) ! called by resolve.c 
	IMPLICIT NONE
	INTEGER,INTENT (in)  ::  lm,ls 
	integer(kind=cptrsize),intent(in) ::cptr   ! RXEntityDefault*
	CHARACTER (len=lm)    ::  m
	CHARACTER (len=ls)    ::  s	
	TYPE (Connect)  ,POINTER :: this
	integer  :: n ,err 
! here the code to add the connect to the list. 
	err = 0
	n = NextFreeConnect(err)
	if(err == 1) then
		write(outputunit,*) ' connect allocation error'
		STOP
	endif

	this=>ConList%list(n)%o
	this%cptr=cptr
	this%Mname=m
	this%string = s
	this%resolved= .FALSE.
	this%used = .TRUE.
END  FUNCTION  Create_Connect

INTEGER FUNCTION  Delete_Connect(cptr) ! called when C object deleted
use rxfoperators_f
use cfromf_f  ! for zeroptr
	IMPLICIT NONE
	integer(kind=cptrsize),intent(in) ::cptr
	TYPE (Connect)  ,POINTER :: this
	INTEGER :: n, istat

	Delete_Connect = 0
	if(.not. ASSOCIATED (ConList%list)) THEN
		write(outputunit,*) ' tried to delete connect ', N, ' but list not associated '
	else	
debugging :	if(ConList%count > ubound(conlist%list,1) ) then
			write(outputunit,*) ' COUNT HI!!!!!! should crash '
		endif debugging
		do n = 1, ConList%count
			this=>ConList%list(n)%o
			if(this%cptr == cptr) then
				this%cptr= 0
				this%resolved= .FALSE.
				Delete_Connect = 1
				write(outputunit,*) 'connect ', n, 'DELETING'
				this%used = .FALSE.
				nullify(this%s1); nullify(this%s2)
! the deallocate failed even tho ubound was non-zero. this is wierd
				istat = 1
				if( Associated( this%slavelist)  ) then
					write(outputunit,*)' Ubound(slavelist)= ',ubound(this%slavelist) 				
					DEALLOCATE(this%slavelist,stat=istat)
					NULLIFY(this%slavelist)	 ! peter added jan 2005
					if(istat /=0) THEN
						write(outputunit,*) ' deallocate slavelist failed (1)'
					endif
				!else
				!write(outputunit,*) ' leak in Delete_Connect n= ',n,' ub=',ubound(this%slavelist) 
				endif
			endif
		enddo
	endif
	if(Delete_Connect .eq. 0) then
		write(outputunit,*) ' delete_Connect FAILED' 
	endif
END  FUNCTION  Delete_Connect


INTEGER FUNCTION   NextFreeConnect(err)
	USE realloc_f
	use cfromf_f  ! for zeroptr
	IMPLICIT NONE
	INTEGER,INTENT (out)  ::  err
	integer :: i,k, istat, e
	TYPE (ConnectHolder), DIMENSION(:)  ,POINTER :: l_list
	TYPE (ConnectHolder), POINTER :: t
  	NextFreeConnect = 0
	err = 0
	l_list => ConList%list
	if(.not. ASSOCIATED(ConList%list)) THEN
		ConList%Block=50
		call reALLOCATE(conlist%list,ConList%Block,istat)
		if(istat /=0) THEN
			err = 1
			return
		endif
		l_list => ConList%list
		e = ubound(l_list,1)
		do i = 1,e
			t=> l_list(i)
			t%o%used = .FALSE.
			t%o%resolved = .FALSE.
			t%o%m_sliding= .FALSE.
			t%o%m_IsSpline=.false.
		    t%o%m_Interpolatory = .false.
		    t%o%m_SplineDegree = -1
			t%o%Mname = "unassigned"
			t%o%string = "empty"
			t%o%Cptr = 0
                        t%o%ConnectRank = 0
			t%o%model1 = "b "
			t%o%ent1= " b"
			t%o%model2= "b "
			t%o%ent2 = " b"
			t%o%type1 = 0
			t%o%type2 = 0
			t%o%m_node1 = 0
			t%o%m_node2 = 0
			NULLIFY( t%o%s1 )
			NULLIFY( t%o%s2 )
	
			NULLIFY(t%o%slavelist) ! dont dealloc. we are starting
		enddo
		NextFreeConnect = 1
		ConList%count =1

		ConList%list(1)%o%used = .TRUE.
		return
	ENDIF
	do i=1,ubound(Conlist%list,1)
		if(.not. ConList%list(i)%o%used) THEN
		t=> ConList%list(i)
! lets empty out the connect
		t%o%used = .FALSE.
		t%o%resolved = .FALSE.
		t%o%m_sliding= .FALSE.
		t%o%m_IsSpline=.false.
	    t%o%m_Interpolatory = .false.
	    t%o%m_SplineDegree = -2
		t%o%Mname = "unassigned"
		t%o%string = "empty"
		t%o%Cptr = 0
                t%o%ConnectRank = 0
		t%o%model1 = "b "
		t%o%ent1= " b"
		t%o%model2= "b "
		t%o%ent2 = " b"
		t%o%type1 = 0
		t%o%type2 = 0
		t%o%m_node1 = 0
		t%o%m_node2 = 0
		t%o%m_sli1= 0
		t%o%m_sli2 = 0
		NULLIFY( t%o%s1 )
		NULLIFY( t%o%s2 )
	!	NULLIFY(ConList%list(i )%o%slavelist)
		if(associated(t%o%slavelist)) then
			deallocate(t%o%slavelist,stat=err)
		endif
		NULLIFY(t%o%slavelist)
			NextFreeConnect =i
			if(i > ConList%count) then
				ConList%count= NextFreeConnect
			endif
			return
		endif		
	enddo
! we get here if no spare spaces
	i =  ubound(ConList%list,1) 
     	CALL reallocate(ConList%list, ubound(ConList%list,1) + ConList%block,err)
	if(err /=0) then
		write(outputunit,*) ' out of  memory in connects'
		STOP
	endif
	do k= i+1,ubound(ConList%list,1) 
		t=> ConList%list(k)	
		ConList%list(k )%o%used = .FALSE.
		ConList%list(k )%o%resolved = .FALSE.
		ConList%list(k )%o%m_sliding = .FALSE.
		ConList%list(k )%o%m_IsSpline = .FALSE.
		ConList%list(k )%o%m_Interpolatory = .FALSE.		
		ConList%list(k )%o%m_SplineDegree = -3
		ConList%list(k )%o%Mname = "unassigned"
		ConList%list(k )%o%string = "empty"
		ConList%list(k )%o%Cptr = 0
                ConList%list(k )%o%ConnectRank= 0
		ConList%list(k )%o%model1 = " b"
		ConList%list(k )%o%ent1= " b"
		ConList%list(k )%o%model2= "b "
		ConList%list(k )%o%ent2 = "b "
		ConList%list(k )%o%type1 = 0
		ConList%list(k )%o%type2 = 0
		ConList%list(k )%o%m_node1 = 0
		ConList%list(k )%o%m_node2 = 0
		ConList%list(k )%o%m_sli1=0
		ConList%list(k )%o%m_sli2=0


		NULLIFY (ConList%list(k )%o%s1 )
		NULLIFY (ConList%list(k )%o%s2  )
		NULLIFY(t%o%slavelist)
	enddo

	NextFreeConnect = i+1
	ConList%count=NextFreeConnect 
END  FUNCTION   NextFreeConnect

subroutine TypeConnects()
	IMPLICIT NONE

	call  Print_Connects(outputunit)
end  subroutine TypeConnects

subroutine Print_Connects(Uout)
    use stringList_f
    use dofoprototypes_f
	IMPLICIT NONE
	INTEGER , intent(in) :: Uout
	integer :: i,j,istat
	TYPE (Connect)  ,POINTER :: this
	TYPE (Nconntype) ,POINTER :: s
        write(Uout,*) '++++++++++++++++++++++++++++'
        write(Uout,*) 'The Global Connect List'
	write(Uout,*) '  count= ', ConList%count, 'ubound= ', UBOUND(ConList%list,1)

        write(Uout,*) '----------------Resolved Connects---------------------- '

doi1:	do i = 1, UBOUND(ConList%list,1)  ! ConList%count
		this=>ConList%list(i)%o
ifusd:	if( this%used .and. this%resolved) then
		write(Uout,*)
		write(Uout,*)' Connect no ', i, ' model name ', TRIM( this%Mname)
                write(Uout,*) ' Used = ', this%used,' resolved = ',this%resolved ,' rank = ',this%ConnectRank
		write(Uout,*) 'sliding=',this%m_sliding, 'spline=',this%m_IsSpline, 'interp=',this%m_Interpolatory
	
		write(Uout,'(a,a)') ' LINE      ', TRIM(this%string)
15      format( a12,i,a,a12,a,a12,a,i )		
		write(Uout,15)'Master type1 ', this%type1,' model1 ', trim(this%model1),' ent1 ', trim(this%ent1) , ' N1',this%m_node1
		write(Uout,15)'Slave  type2 ', this%type2,' model2 ', trim(this%model2),' ent2 ', trim(this%ent2) , ' N2',this%m_node2
		if(Associated(this%slavelist)) then
			write(Uout,*) 'slave list'
			write(Uout,12) '   j,   masterEdge,  SlaveSli, SlaveNN,    t(onEdge),      kerklVertex'
			do j=1,ubound(this%slavelist,1)
				s=> this%slavelist(j)
				write(Uout,11,iostat=istat) j, s%e, s%m_ncsli,s%m_ncn,s%t,s%kkv,s%sr%sli,s%sr%n
			enddo
			write(Uout,*)
		else
		    write(Uout,*) 'slavelist not allocated'
		endif
		if(associated(this%s1) ) then
			write(Uout,'(a,a,\ ) ') ' s1 is <',trim(this%s1%m_text),'>'
			call PrintOneFstringRelations(this%s1  ,uout)
		else
			write(Uout,'(a) ') ' s1 not associated'
		endif
		if(associated(this%s2) ) then
			write(Uout,'(a,a,\ ) ') ' s2 is <',trim(this%s2%m_text),'>'
			call PrintOneFstringRelations(this%s2  ,uout)
		else
			write(Uout,'(a) ') ' s2 not associated'			
		endif
        if(associated(this%m_cdofo)) then
  !      j= PrintOneDOFO(this%m_cdofo,Uout,.false.)
        endif
		endif ifusd
        enddo doi1

        write(Uout,*); write(Uout,*) '-------------------UnResolved Connects -----------------------'

doi2:	do i = 1, UBOUND(ConList%list,1)  ! ConList%count
            this=>ConList%list(i)%o
ifusd2:     if( this%used .and. .not. this%resolved) then
                write(Uout,*)
                write(Uout,*)' Connect no ', i, ' model name ', TRIM( this%Mname)
                write(Uout,*) ' Used = ', this%used,' resolved = ',this%resolved ,' rank = ',this%ConnectRank
                write(Uout,*) 'sliding=',this%m_sliding, 'spline=',this%m_IsSpline, 'interp=',this%m_Interpolatory

                write(Uout,'(a,a)') ' LINE      ', TRIM(this%string)
                write(Uout,15)'Master type1 ', this%type1,' model1 ', trim(this%model1),' ent1 ', trim(this%ent1) , ' N1',this%m_node1
                write(Uout,15)'Slave  type2 ', this%type2,' model2 ', trim(this%model2),' ent2 ', trim(this%ent2) , ' N2',this%m_node2
                if(Associated(this%slavelist)) then
                        write(Uout,*) 'slave list'
                        write(Uout,12) '   j,   masterEdge,  SlaveSli, SlaveNN,    t(onEdge),      kerklVertex'
                        do j=1,ubound(this%slavelist,1)
                                s=> this%slavelist(j)
                                write(Uout,11,iostat=istat) j, s%e, s%m_ncsli,s%m_ncn,s%t,s%kkv,s%sr%sli,s%sr%n
                        enddo
                        write(Uout,*)
                else
                    write(Uout,*) 'slavelist not allocated'
                endif
                if(associated(this%s1) ) then
                        write(Uout,'(a,a,\ ) ') ' s1 is <',trim(this%s1%m_text),'>'
                        call PrintOneFstringRelations(this%s1  ,uout)
                else
                        write(Uout,'(a) ') ' s1 not associated'
                endif
                if(associated(this%s2) ) then
                        write(Uout,'(a,a,\ ) ') ' s2 is <',trim(this%s2%m_text),'>'
                        call PrintOneFstringRelations(this%s2  ,uout)
                else
                        write(Uout,'(a) ') ' s2 not associated'
                endif
           !     if(associated(this%m_cdofo)) then
                 !   j= PrintOneDOFO(this%m_cdofo,Uout,.false.)
           !     endif
        endif ifusd2
     enddo doi2


10	format(4i6,f12.5,1x,i6)
11	format(4(i6,' , '),f12.5,' , ',3(1x,i6,' , '))
12  format(8(a,' , '))
end  subroutine print_connects

subroutine PrintConnectMasses(Uout)
    use saillist_f
	IMPLICIT NONE
	INTEGER , intent(in) :: Uout

	integer :: i,j,n,k ,l_sli
	TYPE (Connect)  ,POINTER :: this
	write(Uout,*) 'Connect mass'
	do i = 1, UBOUND(ConList%list,1)  
		this=>ConList%list(i)%o
iftr:	if(this%resolved) then
		if(this%type1 .eq. C_NODE) then
			n=this%m_Node1 ;   l_sli= this%m_sli1
			write(Uout,'(a,i6,a)') 'node1 ',N,'      XM                      B'  ! lahey Oct 2002
    
			do k=1,3 
				write(Uout,10) saillist(l_sli)%nodes%xlist(n)%xm(1:3,k), saillist(l_sli)%nodes%xlist(n)%b(1:3,k)
			enddo
		endif
		if(this%type2 .eq. C_NODE) then
			n=this%m_Node2;  l_sli= this%m_sli2		
			write(Uout,*) 'node2 ',N,'      XM                      B'

			do k=1,3 
				write(Uout,10) saillist(l_sli)%nodes%xlist(n)%xm(1:3,k), saillist(l_sli)%nodes%xlist(n)%b(1:3,k)
			enddo
		endif

		ass: if(Associated(this%slavelist)) then
			do j=1,ubound(this%slavelist,1)	
				n = this%slavelist(j)%m_ncn; l_sli =  this%slavelist(j)%m_ncsli
				write(Uout,*) 'node ',N,'      XM                      B'
				do k=1,3 
					write(Uout,10) saillist(l_sli)%nodes%xlist(n)%xm(1:3,k), saillist(l_sli)%nodes%xlist(n)%b(1:3,k)
				enddo
			enddo
		endif ass
		endif iftr
10	format(2(3(2x,g13.5)))
	enddo
end  subroutine PrintConnectMasses


! returns node indices in model1 and model2
SUBROUTINE Get_First_Three_Connects(model1,m1,model2, m2,nn1,nn2,c)   ! version tha doesnt need sorted list
        IMPLICIT NONE
        INTEGER , intent(in) :: m1,m2	! lengths of the model names
        CHARACTER (len=m1),intent(in) :: model1
        CHARACTER (len=m2),intent(in) ::model2
        INTEGER ,  intent(out), dimension (*)  :: nn1,nn2
        INTEGER , intent(out) :: c   ! count of nodes in x1,x2
        integer, allocatable, dimension(:) :: n1,n2,nn

! this test is case-sensitive so we must call it with lower-case arguments

        integer ::  i,L1,L2 ,nc, k
        TYPE (Connect)  ,POINTER :: this

        nc = UBOUND(ConList%list,1)
        allocate (n1(nc));allocate (n2(nc));allocate (nn(nc)); n1=-1;  n2=-2;nn=777888999
        c = 0
        do i = 1,  nc
                this=>ConList%list(i)%o
                if(this%resolved .and. this%used) then
                if(this%type1 .eq. C_NODE .and. this%type2 .eq. C_NODE ) then
                        L1 = len_trim(this%model1)
                        L2 = len_trim(this%model2)
                        if(l1 .eq.m1 .AND.  this%model1(:L1) .eq. model1  .and. l2 .eq.m2 .AND.  this%model2(:l2) .eq. model2) THEN
                                c=c+1
                                n1(c)=this%m_node1
                                n2(c)=this%m_node2
                                nn(c) = this%ConnectRank
                        else if(l1 .eq.m2 .AND.  this%model1(:l1) .eq. model2 .and. l2 .eq.m1 .AND.  this%model2(:l2) .eq. model1) THEN
                                c=c+1
                                n1(c)=this%m_node2
                                n2(c)=this%m_node1
                                nn(c) = this%ConnectRank
                        endif
                endif
                endif
        enddo
 !  we now have all the connected node-pairs, with their ranks  in nn
 ! we fill array nn1 and nn2 starting with the lowest rank, but dont return more than 3

    c = min(c,3)
    DO  i = 1, c
        k = minloc(nn,1)
        nn1(i) = n1(k)
        nn2(i) = n2(k)
        nn(k) = maxval(nn) + 1
    enddo

        deallocate( n1,n2,nn)

end SUBROUTINE Get_First_Three_Connects

!  Set_Connect_Relations is called just once from analysis_prep, typically at the beginning of rundr.
! unless its a repeat call because it went over the cycle limit
! It's here where we get the strings node-lists, etc.
! that could have been done after meshing and hoisting 
subroutine Set_Connect_Relations()
    use fstrings_f
    use dofoprototypes_f
	IMPLICIT NONE
	logical, parameter :: DropFirstAndLast=.false.
!sets slave coords(2)  to the master (1) coords
	integer :: i,j ,istat
	TYPE (Connect)  ,POINTER :: t
	
        logical ::NeedsRedoing = .true.

        integer :: nn , count
        integer, dimension(:), allocatable::  lNodeList

        count=0
doi:  do i = 1,  ConList%count
        t=>ConList%list(i)%o
ifUR:    if(t%used .and.  t%resolved)  then
       
iftype:    	if((t%type1==C_NODE) .and. (t%type2==C_NODE)) then  ! no action - whether or not its a dofo

                else if((t%type1==C_STRING) .and. (t%type2==C_NODE)) then iftype
                        if( Associated(t%slavelist)   ) THEN
                                DEALLOCATE (t%slavelist,stat=istat)
                        endif
                        ALLOCATE(t%slavelist(1),stat=istat)
                        if(istat/=0) then
                                write(outputunit,*) ' cannot allocate for connect'
                                STOP
                        endif
                        t%slavelist%m_ncn=t%m_node2; t%slavelist%m_ncsli=t%m_sli2;
                        t%slavelist%sr%sli=t%s1%m_sli
                        t%slavelist%sr%N  =t%s1%nn
                        t%slavelist%e=1
                        t%slavelist%t=0.45;
                        t%slavelist%kkv=-100;
                        CALL InitSlaveList(t)
                else if((t%type1==C_STRING) .and. (t%type2==C_STRING)) then iftype
                    if(DropFirstAndLast) then
                                write(outputunit,*) ' string-string connect - without the ends'
                            nn = t%s2%d%Ne + 1 -2 ;
                            allocate(lNodeList(nn), stat=istat) ;
                            CALL CS_GetStringNodeList(t%s2,lNodeList,nn,PleaseDropEnds=.true.) ;
                    else
                            nn = t%s2%d%Ne + 1 ;
                            allocate(lNodeList(nn), stat=istat) ;
                            CALL CS_GetStringNodeList(t%s2,lNodeList,nn,PleaseDropEnds=.false.) ;
                    endif
                    NeedsRedoing =.false.
                    if( .not. associated(t%slavelist)) then
                        NeedsRedoing =.true.
                    else if (ubound(t%slavelist,1) .ne. nn)  then
                        NeedsRedoing =.true.
                    else
doj:                    do j = 1,nn
                        if(lNodeList(j) .ne. t%slavelist(j)%m_ncn) then
                           NeedsRedoing =.true. 
                           exit doj 
                        endif
                        enddo doj
                    endif
                

 nr:                if(NeedsRedoing) then
                        if( Associated(ConList%list(i)%o%slavelist)) THEN
                             DEALLOCATE (ConList%list(i)%o%slavelist,stat=istat) !
                        endif
                        ALLOCATE(ConList%list(i)%o%slavelist(nn),stat=istat);
                        if(istat/=0) then; write(outputunit,*) 'allocation';	STOP;endif
                        do j=1,nn
                                ConList%list(i)%o%slavelist(j)%m_ncn=lNodeList(j)
                                ConList%list(i)%o%slavelist(j)%m_ncsli= t%m_sli2;
                                ConList%list(i)%o%slavelist(j)%sr%sli=t%s1%m_sli
                                ConList%list(i)%o%slavelist(j)%sr%N  =t%s1%nn
                                ConList%list(i)%o%slavelist(j)%e= 1
                                ConList%list(i)%o%slavelist(j)%t=0.5
                                ConList%list(i)%o%slavelist%kkv=-200;
                        enddo
                        Deallocate(lNodeList,stat=istat)	!; kkk = dbs(" after deallocate Nodelist i=",i)
                        CALL InitSlaveList(t)
            endif nr
            
        endif iftype
        if(t%m_IsSpline) then ! remember that s1 or s2 may be associated with another connect
              if(NeedsRedoing) then
                   if(count .eq. 0) istat= DOFO_UpdateJay( ) !result (ok)  !  Sept 2013
                   count=count+1
                   call make_spline_dofo(t)
              endif
        endif
        endif ifUR
        if( allocated(lNodeList)) deallocate(lNodeList)
      enddo doi

end subroutine Set_Connect_Relations


subroutine Set_Connect_Coords() !sets slave coords(2)  to the master (1) coords
	USE vectors_f
	use saillist_f
	use coordinates_f
	use saillist_f
	IMPLICIT NONE

	integer :: i,j 
	TYPE (Connect)  ,POINTER :: t
	TYPE (Nconntype)  ,POINTER :: s
	TYPE (string) , POINTER :: theString

	REAL (kind=double)  ,dimension(3) :: x1,x2 ,Xnew ! XX,a
	integer ::    see  ,EdgeNo,kerr
	
    TYPE (edgelist),pointer:: edg
	
	do i =  ConList%count,1,-1  ! reverse order 
            t=>ConList%list(i)%o
            if(t%used .and.  t%resolved)  then
                    if(t%m_IsSpline) then
                        cycle;

! evaluate the spline and copy result into	t%m_node2
                    else if((t%type1==C_NODE) .and. (t%type2==C_NODE)) then
#ifndef DOFO_CONNECTS			
				j=fill_x(t%m_node2,get_Cartesian_X(t%m_node1,t%m_sli1),t%m_sli2 )
                        j=0; j=1/j
#endif

			else if (t%type1==C_STRING)  then
				ass: if(Associated(t%slavelist)) then
				jdo : do j=1,ubound( t%slavelist ,1)
					s=> t%slavelist(j)
					if(t%m_Sliding) then
						call UpdateConnect_T_S( s) ;   ! This routine is slow. should seed
					endif
					see = s%e   ;! write(outputunit,*) 'see is ',see

                                        TheString=>saillist(s%sr%sli)%Strings%list(s%sr%n)%o ;
                                       ! write(outputunit,*) 'string is ',trim(thestring%m_text) ! Dec 5 2003
                                        EdgeNo =  TheString%se(see);
					
					if(EdgeNo .le. 0) then
						write(outputunit,*) ' array se out of range on string '
						write(outputunit,*) TheString%se(1:TheString%d%Ne)
						write(outputunit,*) ' ne is ',TheString%d%Ne
						write(outputunit,*) ' we are accessing edge  ', see
						write(outputunit,*) 't%Sliding is', t%m_Sliding
                                                write(outputunit,*)' The Point = ', saillist(TheString%m_sli)%nodes%xlist(s%m_ncn)%XXX
						! pa use
					endif
					if(see .gt. ubound(TheString%rev,1) ) then
						write(outputunit,*) ' bounds check failure '
					endif
				    	edg=>saillist(theString%m_sli)%edges 
					if(TheString%rev(see)) then		!WAS WRONG j not see till Dec 6 2003.
						x2=get_Cartesian_X(edg%list(EdgeNo)%m_L12(1), theString%m_sli)
							x1 = get_Cartesian_X(edg%list(EdgeNo)%m_L12(2), theString%m_sli)
					else
						x1=get_Cartesian_X(edg%list(EdgeNo)%m_L12(1), theString%m_sli)
						x2 = get_Cartesian_X(edg%list(EdgeNo)%m_L12(2), theString%m_sli)					
							
					endif
					Xnew = x2*s%t + x1* (1.0D00-s%t)
					kerr=fill_x(s%m_ncn,Xnew,s%m_ncsli);! write(outputunit,*) ' back from fillx ', s%m_n, s%m_sli,' err=',kerr
         !write(outputunit,*) ' connect STRING sets node '! ,s%n , '  from  ', sngl(X(s%n,1:3)) ,'  to  ', sngl( x2*s%t + x1* (1.0-s%t))
				enddo jdo
				endif ass
			endif
		endif
	enddo
end subroutine Set_Connect_Coords

subroutine make_spline_dofo(theConn)
    use sailList_f
    use stringlist_f
    use fstrings_f
    use fnoderef_f
    use dofoprototypes_f
    implicit none
    TYPE (Connect),POINTER,intent(inout) :: theConn
! locals
    integer ::nn,j,i,error,ok
    integer, dimension(:), allocatable::  lNodeList

    class(fsplineI), pointer ::TheSpline
    type(fnode), pointer :: thenode
    TYPE (Nconntype)  ,POINTER :: sl
  
    if(.not. associated(theConn%s1%m_pts)) then
        nn = 0
        allocate(lNodeList(theConn%s1%d%Ne + 1), stat=i)
        CALL CS_GetStringNodeList(theConn%s1,lNodeList,nn,PleaseDropEnds=.false.)
        allocate(theConn%s1%m_pts(nn)) ! should be an append call not an allocate
        do j=1,nn
            thenode=>saillist(theConn%m_sli1)%nodes%xlist(lNodeList(j))
            theConn%s1%m_pts(j)%m_fnrN       = thenode%nn     ! = createfnoderef(thenode)	
            theConn%s1%m_pts(j)%m_fnr_Sli    = theConn%m_sli1
        enddo
        deallocate(lNodeList)
    endif
    TheSpline=> GetStringSpline(theConn%s1,theConn%m_Interpolatory,theConn%m_SplineDegree);! creates one if necessary

    TheSpline%Sliding = theConn%m_Sliding
    TheSpline%Interpolatory = theConn%m_Interpolatory
    
    if(theConn%type2 .eq. C_NODE) then
            if(associated(  theConn%m_cdofo ) ) then
                i= theConn%m_cdofo% m_dofosli;  j= theConn%m_cdofo%m_dofoN
                j = DestroyDofo (i, j) 
            endif
            theConn%m_cdofo=> CreateDOFO(theConn%m_sli1, C_DOFO_NODESPLINE_CONNECT ,error)  
            sl=>theConn%slavelist(1)
            theNode=>saillist(sl%m_ncsli)%nodes%xlist(sl%m_ncn)
            if( sl%m_ncsli .ne. theConn%m_sli1) then
                  write(outputunit,'(a,i6,i6,\)' ) '(make_spline_dofo ) DOFO SLI Disagree:', sl%m_ncsli , theConn%m_sli1
                  write(outputunit,*)'  ' ,trim (theConn%string)
            endif
            ok= Complete_NodeSplineDOFO( theConn%m_cdofo, theNode,TheSpline) ! the complete__ call must make the children
    elseif(theConn%type2 .eq. C_STRING) then
            if(associated(  theConn%m_cdofo ) ) then
                i= theConn%m_cdofo% m_dofosli;  j= theConn%m_cdofo%m_dofoN
                j = DestroyDofo (i, j) 
            endif
           theConn%m_cdofo=> CreateDOFO(theConn%m_sli1, C_DOFO_STRINGSPLINE_CONNECT ,error)  ! WARNING. sli2 may be wrong
           ok= Complete_StringSplineDOFO( theConn%m_cdofo, theConn,TheSpline)               
    endif  
end subroutine make_spline_dofo

END  MODULE connects_f

subroutine Resolve_Connects(count)
    use connects_f
    use connect_intrf_f
    USE parse_f
    use fstrings_f
    use cfromf_f
    use saillist_f
    use dofoprototypes_f
    IMPLICIT NONE
! 
! NOTE
! This should only pick up Hoisted models.  
!  Get string by name only takes strings from 'active' models
!   so does get fortran nodeno - almost

        integer, intent(out) :: count
	integer :: i 
	TYPE (Connect)  ,POINTER :: this
 	CHARACTER (len=32),  dimension(12) ::  w
        CHARACTER*1  Delimiters(6)
        !data delimiters /'/', '/' , ',' ,':','	',':'/  !attx may have commas so
        data delimiters /'/', '/' ,':','	',':',':'/

	character*256 name
        integer :: ni, ns,kkkk
	integer  istat
   	integer(kind=cptrsize) :: thesail1,thesail2
   	type(fnode), pointer ::   theNode 
   	Logical:: resolved1 ,resolved2
        count=0
        if(count .eq. 0) istat= DOFO_UpdateJay( ) !result (ok)  !  Sept 2013
        iloop: do i = 1,  ConList%count
        resolved1=.false.; resolved2=.false.	
		this=>ConList%list(i)%o
		usedif :  if(this%used .and. .not. this%resolved)  then
			ni=12	
			ns=0
                        this%ConnectRank = -1
			this%resolved=.false.; w=' '
			CALL parse( this%string , Delimiters, w , ni) ! ni is dimsize on input, n words on output
			if(ni .lt.7) then
				this%type1=0
				this%type2=0				
			else if ( w(3) .eq. "node") then  ! old format. Both nodes, unsliding
				this%type1=C_NODE
				this%model1=w(5)
				this%ent1 = w(4)
				this%type2=C_NODE
				this%model2=w(7)
				this%ent2=w(6)
				ns = 8
			else if (w(3) .eq. "curve") then ! old format. Both strings
				this%type1=C_STRING
				this%model1=w(5)
				this%ent1 = w(4)
				this%type2=C_STRING
				this%model2=w(7)
				this%ent2=w(6)
				ns = 8
			else
				ns = 9
				if(ni .gt. 9) then	! last will be the number
                                    read(w(10),*,iostat=istat) this%ConnectRank
                                    if(istat .gt.  0) then
                                         this%ConnectRank  = -1
                                    else
                                        ! write(outputunit,*) ' word (ten) is the ConnectRank ', this%ConnectRank
                                    endif
				else
                                    read(w(ni),*,iostat=istat) this%ConnectRank
                                    if(istat .gt.  0)  then
                                            this%ConnectRank  = -1
                                    else
                                            !write(outputunit,*) ' word (',ni,') is the ConnectRank ', this%ConnectRank
                                    endif
				endif
				 if ((w(3) .eq. "string").or.(w(3) .eq. "%string") ) then
					this%type1=C_STRING
					this%model1=w(5)
					this%ent1 = w(4)
				else if ((w(3) .eq. "site").or. (w(3) .eq. "%site")) then
					this%type1=C_NODE
					this%model1=w(5)
					this%ent1 = w(4)
				else 
					this%type1 =0				
					write(outputunit,*) ' CONNECT dont know w3 ',w(3)
					write(outputunit,*) w(1:ni)
				endif
				 if ( (w(6) .eq. "string").or.(w(6) .eq. "%string")  ) then
					this%type2=C_STRING
					this%model2=w(8)
					this%ent2=w(7)
				else if ((w(6) .eq. "site").or. (w(6) .eq. "%site") ) then
					this%type2=C_NODE
					this%model2=w(8)
					this%ent2=w(7)
				else
					write(outputunit,*) ' undefined connect type'
					this%type2 =0
					write(outputunit,*) ' CONNECT dont know w6 ',w(6)
					write(outputunit,*) w(1:ni)
					!pause
				endif
			endif
! allow 	 1	2
!	string	string	slid/no
!	string	NODE	slid/no
!	node	node	slide always false
!	node	string	not allowed
! we could save by using thesail1, thesail2 instead of asking  getfortrannodeno to search again for the sail

	         this%m_sli1 = FindHoistedSailByName (trim(this%model1)  ,len_trim(this%model1) ,thesail1);
	         this%m_sli2 = FindHoistedSailByName (trim(this%model2)  ,len_trim(this%model2) ,thesail2);
	     
                if( this%type1 .eq. C_NODE) then
                        this%m_node1=getfortrannodeno(trim(this%model1)//char(0),trim(this%ent1)//char(0))
                        if(this%m_node1 .gt. 0) then
                            theNode=> saillist( this%m_sli1 )%nodes%xlist(this%m_node1)
                            kkkk=cf_SetParentRelation(theNode%m_RXFEsiteptr,this%Cptr )
                            resolved1=.true.
                        endif

                else if( this%type1 .eq. C_STRING) then
                        name = trim(this%model1) // ","// trim(this%ent1)
                        if(CS_Find_String_By_Name(name,this%s1)) then
                                call addtoconnectlist(this%s1%m_connects ,this)
                        ! this%s1 is type (string)
                                kkkk=cf_SetParentRelationStr(this%s1%d%m_scptr,this%Cptr )
                                this%m_node1=  C_ITSACURVE   ! HORRIBLE. What when we have  this many nodes?
                                resolved1=.true.
                        endif
                endif
                if( this%type2 .eq. C_NODE	) then
                        this%m_node2=getfortrannodeno(trim(this%model2)//char(0),trim(this%ent2)//char(0))
                        if(this%m_node2 .gt.0) then
                            theNode=> saillist( this%m_sli2 )%nodes%xlist(this%m_node2)
                            kkkk=cf_SetParentRelation(theNode%m_RXFEsiteptr,this%Cptr )
                            resolved2=.true.
                        endif
                else if( this%type2 .eq. C_STRING) then
                        name = trim(this%model2) // ","// trim(this%ent2 )
                        if(CS_Find_String_By_Name(name ,this%s2)) then
                                call addtoconnectlist(this%s2%m_connects ,this)
                                kkkk=cf_SetParentRelationStr(this%s2%d%m_scptr,this%Cptr )
                                this%m_node2= C_ITSACURVE
                                resolved2=.true.
                        endif
                endif
                if(ns .le. ni) then  ! was ge. peter found bounds-check bug  may 2012
                    if((this%type1 .eq. C_STRING) .and. (this%type2 .eq. C_STRING)) then
                            this%m_sliding=((index(w(ns),'%slide')>0) .or. (index(w(ns),'$slide')>0)) .and.  index(w(ns),'noslide')<=0
                            this%m_IsSpline = (index(w(ns),'$spline')>0 .or.index(w(ns),'%spline')>0 )
                            this%m_Interpolatory= (index(w(ns),'$interp')>0 .or.index(w(ns),'%interp')>0 )
                            this%m_SplineDegree = 3
                            if (index(w(ns),'$cspline')>0 ) this%m_SplineDegree = -3
                            this%m_SplineDegree = -3   ! FORCE the cspline formulation because (apr 2014) the old fspline needs Tee-OriginalSplineTees

                     else if((this%type1 .eq. C_NODE) .and. (this%type2 .eq. C_NODE)) then
                            this%m_sliding=.false.;this%m_IsSpline =.false. ;this%m_Interpolatory=.false.
                    else if((this%type1 .eq. C_STRING) .and. (this%type2 .eq. C_NODE)) then
                            this%m_sliding= ((index(w(ns),'%slide')>0) .or. (index(w(ns),'$slide')>0)) .and. ( index(w(ns),'noslide')<=0)
                            this%m_IsSpline = (index(w(ns),'$spline')>0 .or.index(w(ns),'%spline')>0 )
                            this%m_Interpolatory= (index(w(ns),'$interp')>0 .or.index(w(ns),'%interp')>0 )
                            this%m_SplineDegree = 3
                           ! if (index(w(ns),'$cspline')>0 ) this%m_SplineDegree = -3
                            this%m_SplineDegree = -3   ! FORCE the cspline formulation because (apr 2014) the old fspline needs Tee-OriginalSplineTees
                    else
                        write(outputunit,*) 'node to string connects not allowed'
                    endif
                else
                    if(this%type1 .ne. C_NODE) write(outputunit,*) 'Take default atts (ns=',ns,' nwds=',ni,') for ',trim(this%string)
                    this%m_sliding=.true.  ! default is 'sliding'
               endif


            this%resolved=((this%m_node1 .gt. 0 ).and. (this%m_node2 .gt. 0) )  ! I think this is why we have C_ITSACURVE
            if(this%resolved /= (resolved1 .and. resolved2)) then; istat=0; istat=1/istat; endif

          if(this%resolved .and.this%type1 .eq. C_NODE .and. this%type2 .eq. C_NODE) then
            if(this%m_node1 ==this%m_node2 .and.  this%m_sli1 ==  this%m_sli2 ) then
                 write(outputUnit,*) "  cannot connect a node to itself ! ",trim(this%model1), this%m_node1," to ",trim(this%model2), this%m_node2
            else

#ifdef DOFO_CONNECTS
                this%m_cdofo=>CreateDOFO(this%m_sli1, C_DOFO_NODENODE_CONNECT  ,istat) ! a clean intitialisation
                istat= Complete_NNCDOFO(this%m_cdofo, saillist(this%m_sli1)%nodes%xlist(this%m_node1),saillist(this%m_sli2)%nodes%xlist(this%m_node2))
#endif
            endif
          endif
        if(this%resolved) count=count+1
        endif usedif
    enddo iloop
end  subroutine Resolve_Connects


subroutine UnResolve_Connects(name, l)
! a connect can be partially resolved (master but not slave)
! yet we don't set the resolved flag until it is completely resolved
! so Unresolve has to trawl through them all
! a subtlety: a blank 'name' means do them all
use ftypes_f
use connects_f
use connect_intrf_f
	IMPLICIT NONE
	INTEGER , intent(in) :: l
	CHARACTER (len=l)    ::  name
	integer :: i 
	TYPE (Connect)  ,POINTER :: this
        integer ::  l1,l2,l3
        l3 = len_trim(name)
        if(l3.eq. 0) write(*,*) ' UnResCon with L=0'
	do i = 1,  ConList%count
		this=>ConList%list(i)%o
                if(this%used )  then
			l1 = len_trim(this%model1)
			l2 = len_trim(this%model2)
                        if(l3 .eq. 0 .or. ( (l1 .eq.l3) .AND. ( this%model1(:l1) .eq. name(:l3)) )) THEN
                                !write(outputunit,*) 'go to unresolve ', trim(ConList%list(i)%o%string),trim(ConList%list(i)%o%mname)
 				call UnResolve_One_Connect(ConList%list(i)%o)
			ENDIF
			if((l2 .eq.l3) .AND. ( this%model2(:l2) .eq. name(:l3) )) THEN
                               ! write(outputunit,*) 'go to unresolve ', trim(ConList%list(i)%o%string),trim(ConList%list(i)%o%mname)
				call UnResolve_One_Connect(ConList%list(i)%o)
			ENDIF	
			!if(.not.  this%resolved)  write(outputunit,*)' UNRESOLVED Connect no ', i	
		endif
	enddo
end  subroutine UnResolve_Connects

subroutine UnResolve_One_Connect(c) 
 use ftypes_f
 use saillist_f
 use cfromf_f
 use connects_f
 use connect_intrf_f
 use dofoprototypes_f
	IMPLICIT NONE
 	TYPE(connect),pointer, intent(inout)  :: c 
	integer :: istat,i,j,kkkk
   	type(fnode), pointer ::   theNode 

	c%resolved = .false. 	
    if(c%m_node1 .gt. 0 .and.c%m_node1.ne. C_ITSACURVE) then
        theNode=> saillist( c%m_sli1 )%nodes%xlist(c%m_node1)
        kkkk=cf_UnSetParentRelation(theNode%m_RXFEsiteptr,c%Cptr )
    endif
        if(c%m_node2 .gt. 0 .and.c%m_node2.ne. C_ITSACURVE) then
        theNode=> saillist( c%m_sli2 )%nodes%xlist(c%m_node2)
        kkkk=cf_UnSetParentRelation(theNode%m_RXFEsiteptr,c%Cptr )
    endif


    if(associated(c%s1)) then
        kkkk=cf_UnSetParentRelationStr(c%s1%d%m_scptr,c%Cptr )
    endif
        if(associated(c%s2)) then
        kkkk=cf_UnSetParentRelationStr(c%s2%d%m_scptr,c%Cptr )
    endif

	c%m_node1=0;c%m_sli1=0;
	c%m_node2=0;c%m_sli2=0;

	if(associated(c%s1) ) then
	if( associated(c%s1%m_connects) ) then
	    do i=1,ubound(c%s1%m_connects,1)
        if(associated(c%s1%m_connects(i)%o,c)  ) then
		    nullify(c%s1%m_connects(i)%o)	    	 
 !               write(outputunit,'(a,a,\)') '(unresolve connects) disconnect ', trim(c%string)
 !               write(outputunit,*) ' from string ', trim (c%s1%m_text)
		endif	    
	    enddo
	endif
	endif
 
	if(associated(c%s2) ) then
	if( associated(c%s2%m_connects) ) then
	    do i=1,ubound(c%s2%m_connects,1)
        if(associated(c%s2%m_connects(i)%o,c)  ) then
		    nullify(c%s2%m_connects(i)%o)	    	 
                ! write(outputunit,*) 'disconnect ', trim(c%mname)
                ! write(outputunit,*) 'from string  ', trim (c%s2%m_text)
		endif	    
	    enddo
	endif
	endif
	
	nullify(c%s1)
	nullify(c%s2)
	if( associated(c%slavelist))then 
		deallocate(c%slavelist,stat=istat) 
	endif
	if(associated(c%m_cdofo )) then
	    i =c%m_cdofo% m_dofosli ; j = c%m_cdofo%m_dofoN 
     	i = DestroyDofo (  i ,j)  
    	c%m_cdofo=>NULL()
	endif

end subroutine UnResolve_One_Connect
