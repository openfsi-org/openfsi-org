      SUBROUTINE SOLVE (NN,A,Y,X,ISIZE)
      IMPLICIT NONE
!
!  THIS ROUTINE SOLVES THE MATRIX EQUATION  [Y] = [A][X]
!  WHERE  [Y] AND [A] ARE KNOWN
!
      INTEGER nn
      INTEGER isize
      REAL*8 A(ISIZE,ISIZE),Y(*),X(*)

      INTEGER ipivot,irow,icolumn,i  ,KK
      REAL*8 fac
      LOGICAL SOLVEERROR
      COMMON /MATRIXERROR/SOLVeError

      SOLVEERROR=.FALSE.
      IF (NN .GT.ISIZE) THEN
      WRITE(0,*)' Dimensions exceeded in SOLVE'
      WRITE(0,*)' Array size called was ',NN
      WRITE(0,*)' Limit is              ',ISIZE
      STOP
      ENDIF
      DO 10 IPIVOT=1,NN-1
!
      DO 10 IROW=IPIVOT+1,NN
      IF (A(IPIVOT,IPIVOT) .EQ. 0.0) THEN
!               write(outputunit,*)' FATAL ERROR Pivot zero at row ',ipivot
!               write(outputunit,*)
!               write(outputunit,*)' RAW MATRIX IS:-'
!               DO 300 II=1,NN
!               300 write(outputunit,'(13F10.4)')(A(II,JJ),JJ=1,NN)
!               stop
      SOLVEERROR=.TRUE.
      RETURN
      endif
      FAC=A(IROW,IPIVOT)/A(IPIVOT,IPIVOT)
      DO 20 ICOLUMN=1,NN
20     A(IROW,ICOLUMN)=A(IROW,ICOLUMN)-FAC*A(IPIVOT,ICOLUMN)
!
10     Y(IROW)=Y(IROW)-FAC*Y(IPIVOT)
!
!    BACK SUBSTITUTION
!
      DO I=NN,1,-1
      IF (A(I,I) .EQ. 0.0) THEN
!      write(outputunit,*)' FATAL ERROR IN BACK SUBSTITUTION'
!      write(outputunit,*)' Modified pivot is zero at row' ,I
!      write(outputunit,*)' Reduced matrix is'
!      write(outputunit,*)' '
!      DO 200 II=1,NN
!  200 write(outputunit,'(13F10.4)')(A(II,JJ),JJ=1,NN)
!      STOP
      SOLVEERROR=.TRUE.
      RETURN

      ENDIF
      X(I)=Y(I)
      DO  kK=I+1,NN
          X(I)=X(I)-A(I,kK)*X(kK)
      ENDDO
      X(I)=X(I)/A(I,I)
      ENDDO
!

      END
!of solve

