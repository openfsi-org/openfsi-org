TEMPLATE = subdirs
CONFIG += ordered
#message("needs QT4.7 or 4.8 to build.  meyermatrices still hang. Try without SOQT and coin ")
include ( openfsicompilerconfig.pri)
linux:TARGET = rxserver/rxserver
win32: TARGET = openfsi
SUBDIRS      += thirdparty/hbio \
             thirdparty/MTParserLib \
             clients/userselect \
             thirdparty/opennurbs
ODETESTING:{
    SUBDIRS +=thirdparty/nrrk
    message ("SUBDIRS +=thirdparty/nrrk ")
}

SUBDIRS       += ofsifortran \
             src/rxGraphicsStub \
             src/stringtools \
             thirdparty/sailpp \
             thirdparty/triangle \
             preprocessing \
             rxkernel \
            rxserver \
 ##           rxsolver \
            clients/rxclientbase



preprocessing.depends = ofsifortran
rxserver.depends =  preprocessing
rxserver.depends =  thirdparty/nrrk
rxsolver.depends =  thirdparty/hbio
        #    thirdparty/sparskit




# on my windows setup, in a MSVC command prompt, go something like
#  "C:\Program Files (x86)\Intel\ComposerXE-2011\bin\ifortvars.bat"  ia32
#  "C:\Program Files (x86)\Intel\ComposerXE-2011\bin\iclvars.bat"  ia32
#    "C:\QtSDK\QtCreator\bin\qtcreator.exe"
# or "C:\QtSDK\QtCreator\bin\qtcreator.exe"
#Then if you do  first a qmake , then nmake to run MOC, and then a build in MSVC2005 you might be lucky
# 
#
#######      specify Qt minimum version")
message(Qt version: $$[QT_VERSION])
#version check qt
contains(QT_VERSION, ^4\\.[0-6]\\..*) {
message("Cannot build with Qt version $${QT_VERSION}.")
error("Please upgrade your Qt to Version 4.7 or 4.8")
}
contains(QT_VERSION, ^5\\.[0-6]\\..*) {
message("Experimental build with Qt version $${QT_VERSION}.")
#error("Please downgrade your Qt to Version 4.7 or 4.8")
}

######"      ensure that f90.prf is installed correctly ")
FPROFILE= $$[QMAKE_MKSPECS]/features/f90.prf

 !exists( $$FPROFILE  ) {
       message( "f90.prf file needs installing at " $$FPROFILE)
unix:  system ("sudo cp ./f90.prf $$FPROFILE")
win32: {
system ("copy ./f90.prf $$FPROFILE")
message (" copied ./f90.prf to  $$FPROFILE")
}

  }
 exists( $$FPROFILE  ) {
       message( "f90.prf file is at " $$FPROFILE)
  }
 !exists( $$FPROFILE  ) {
error ( "f90.prf file is NOT at <" $$FPROFILE) ">"
}

include (openfsicompilerconfig.pri)
rx64 {
    message(compiling for 64-bit   )
}
else  {
    message(compiling for 32-bit   )
}
DEFINES +=  RXLIB
###########DEFINES +=  RLX_64

##########DEFINES +=  RX_ICC
DEFINES +=  _OFSI_

INCLUDEPATH += ../thirdparty/opennurbs
INCLUDEPATH += ../thirdparty/MTParserLib
INCLUDEPATH += ../src/x/inc
INCLUDEPATH += ../rxserver
INCLUDEPATH += ../rxcommon
INCLUDEPATH += /usr/include/mysql

INCLUDEPATH += ../src/preprocess
INCLUDEPATH += ../src/post/inc
INCLUDEPATH += ../src/inc
INCLUDEPATH += ../thirdparty/opennurbs
INCLUDEPATH += ../thirdparty/triangle
INCLUDEPATH += ../thirdparty/MTParserLib
INCLUDEPATH += ../src/x/inc
INCLUDEPATH += ../rxserver
INCLUDEPATH += ../rxcommon
INCLUDEPATH += /usr/include/mysql


unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}



INCLUDEPATH += $$PWD/thirdparty/triangle
DEPENDPATH += $$PWD/thirdparty/triangle

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/thirdparty/triangle/release/ -ltriangle
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/thirdparty/triangle/debug/ -ltriangle
else:unix: LIBS += -L$$PWD/thirdparty/triangle/ -ltriangle


win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/thirdparty/triangle/release/libtriangle.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/thirdparty/triangle/debug/libtriangle.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/thirdparty/triangle/release/triangle.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/thirdparty/triangle/debug/triangle.lib
else:unix: PRE_TARGETDEPS += $$PWD/thirdparty/triangle/libtriangle.a
